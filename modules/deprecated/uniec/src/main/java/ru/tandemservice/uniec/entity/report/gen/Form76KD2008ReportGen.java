package ru.tandemservice.uniec.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.report.StorableReport;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.report.Form76KD2008Report;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Форма 76-КД (по форме 2008 года)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class Form76KD2008ReportGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.report.Form76KD2008Report";
    public static final String ENTITY_NAME = "form76KD2008Report";
    public static final int VERSION_HASH = -246769532;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_DEVELOP_FORM = "developForm";
    public static final String P_DATE_FROM = "dateFrom";
    public static final String P_DATE_TO = "dateTo";
    public static final String P_DEVELOP_CONDITION = "developCondition";
    public static final String P_QUALIFICATION = "qualification";
    public static final String P_STUDENT_CATEGORY = "studentCategory";
    public static final String P_EXCLUDE_PARALLEL = "excludeParallel";

    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private DevelopForm _developForm;     // Форма освоения
    private Date _dateFrom;     // Заявления с
    private Date _dateTo;     // Заявления по
    private String _developCondition;     // Условие освоения
    private String _qualification;     // Квалификация
    private String _studentCategory;     // Категория поступающего
    private Boolean _excludeParallel;     // Исключая поступивших на параллельное освоение образовательных программ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения. Свойство не может быть null.
     */
    public void setDevelopForm(DevelopForm developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     */
    @NotNull
    public Date getDateFrom()
    {
        return _dateFrom;
    }

    /**
     * @param dateFrom Заявления с. Свойство не может быть null.
     */
    public void setDateFrom(Date dateFrom)
    {
        dirty(_dateFrom, dateFrom);
        _dateFrom = dateFrom;
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     */
    @NotNull
    public Date getDateTo()
    {
        return _dateTo;
    }

    /**
     * @param dateTo Заявления по. Свойство не может быть null.
     */
    public void setDateTo(Date dateTo)
    {
        dirty(_dateTo, dateTo);
        _dateTo = dateTo;
    }

    /**
     * @return Условие освоения.
     */
    @Length(max=255)
    public String getDevelopCondition()
    {
        return _developCondition;
    }

    /**
     * @param developCondition Условие освоения.
     */
    public void setDevelopCondition(String developCondition)
    {
        dirty(_developCondition, developCondition);
        _developCondition = developCondition;
    }

    /**
     * @return Квалификация.
     */
    @Length(max=255)
    public String getQualification()
    {
        return _qualification;
    }

    /**
     * @param qualification Квалификация.
     */
    public void setQualification(String qualification)
    {
        dirty(_qualification, qualification);
        _qualification = qualification;
    }

    /**
     * @return Категория поступающего.
     */
    @Length(max=255)
    public String getStudentCategory()
    {
        return _studentCategory;
    }

    /**
     * @param studentCategory Категория поступающего.
     */
    public void setStudentCategory(String studentCategory)
    {
        dirty(_studentCategory, studentCategory);
        _studentCategory = studentCategory;
    }

    /**
     * @return Исключая поступивших на параллельное освоение образовательных программ.
     */
    public Boolean getExcludeParallel()
    {
        return _excludeParallel;
    }

    /**
     * @param excludeParallel Исключая поступивших на параллельное освоение образовательных программ.
     */
    public void setExcludeParallel(Boolean excludeParallel)
    {
        dirty(_excludeParallel, excludeParallel);
        _excludeParallel = excludeParallel;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof Form76KD2008ReportGen)
        {
            setEnrollmentCampaign(((Form76KD2008Report)another).getEnrollmentCampaign());
            setDevelopForm(((Form76KD2008Report)another).getDevelopForm());
            setDateFrom(((Form76KD2008Report)another).getDateFrom());
            setDateTo(((Form76KD2008Report)another).getDateTo());
            setDevelopCondition(((Form76KD2008Report)another).getDevelopCondition());
            setQualification(((Form76KD2008Report)another).getQualification());
            setStudentCategory(((Form76KD2008Report)another).getStudentCategory());
            setExcludeParallel(((Form76KD2008Report)another).getExcludeParallel());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends Form76KD2008ReportGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) Form76KD2008Report.class;
        }

        public T newInstance()
        {
            return (T) new Form76KD2008Report();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "developForm":
                    return obj.getDevelopForm();
                case "dateFrom":
                    return obj.getDateFrom();
                case "dateTo":
                    return obj.getDateTo();
                case "developCondition":
                    return obj.getDevelopCondition();
                case "qualification":
                    return obj.getQualification();
                case "studentCategory":
                    return obj.getStudentCategory();
                case "excludeParallel":
                    return obj.getExcludeParallel();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((DevelopForm) value);
                    return;
                case "dateFrom":
                    obj.setDateFrom((Date) value);
                    return;
                case "dateTo":
                    obj.setDateTo((Date) value);
                    return;
                case "developCondition":
                    obj.setDevelopCondition((String) value);
                    return;
                case "qualification":
                    obj.setQualification((String) value);
                    return;
                case "studentCategory":
                    obj.setStudentCategory((String) value);
                    return;
                case "excludeParallel":
                    obj.setExcludeParallel((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                        return true;
                case "developForm":
                        return true;
                case "dateFrom":
                        return true;
                case "dateTo":
                        return true;
                case "developCondition":
                        return true;
                case "qualification":
                        return true;
                case "studentCategory":
                        return true;
                case "excludeParallel":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return true;
                case "developForm":
                    return true;
                case "dateFrom":
                    return true;
                case "dateTo":
                    return true;
                case "developCondition":
                    return true;
                case "qualification":
                    return true;
                case "studentCategory":
                    return true;
                case "excludeParallel":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "developForm":
                    return DevelopForm.class;
                case "dateFrom":
                    return Date.class;
                case "dateTo":
                    return Date.class;
                case "developCondition":
                    return String.class;
                case "qualification":
                    return String.class;
                case "studentCategory":
                    return String.class;
                case "excludeParallel":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<Form76KD2008Report> _dslPath = new Path<Form76KD2008Report>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "Form76KD2008Report");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.Form76KD2008Report#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.Form76KD2008Report#getDevelopForm()
     */
    public static DevelopForm.Path<DevelopForm> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.Form76KD2008Report#getDateFrom()
     */
    public static PropertyPath<Date> dateFrom()
    {
        return _dslPath.dateFrom();
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.Form76KD2008Report#getDateTo()
     */
    public static PropertyPath<Date> dateTo()
    {
        return _dslPath.dateTo();
    }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.uniec.entity.report.Form76KD2008Report#getDevelopCondition()
     */
    public static PropertyPath<String> developCondition()
    {
        return _dslPath.developCondition();
    }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.uniec.entity.report.Form76KD2008Report#getQualification()
     */
    public static PropertyPath<String> qualification()
    {
        return _dslPath.qualification();
    }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.uniec.entity.report.Form76KD2008Report#getStudentCategory()
     */
    public static PropertyPath<String> studentCategory()
    {
        return _dslPath.studentCategory();
    }

    /**
     * @return Исключая поступивших на параллельное освоение образовательных программ.
     * @see ru.tandemservice.uniec.entity.report.Form76KD2008Report#getExcludeParallel()
     */
    public static PropertyPath<Boolean> excludeParallel()
    {
        return _dslPath.excludeParallel();
    }

    public static class Path<E extends Form76KD2008Report> extends StorableReport.Path<E>
    {
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private DevelopForm.Path<DevelopForm> _developForm;
        private PropertyPath<Date> _dateFrom;
        private PropertyPath<Date> _dateTo;
        private PropertyPath<String> _developCondition;
        private PropertyPath<String> _qualification;
        private PropertyPath<String> _studentCategory;
        private PropertyPath<Boolean> _excludeParallel;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.Form76KD2008Report#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.Form76KD2008Report#getDevelopForm()
     */
        public DevelopForm.Path<DevelopForm> developForm()
        {
            if(_developForm == null )
                _developForm = new DevelopForm.Path<DevelopForm>(L_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.Form76KD2008Report#getDateFrom()
     */
        public PropertyPath<Date> dateFrom()
        {
            if(_dateFrom == null )
                _dateFrom = new PropertyPath<Date>(Form76KD2008ReportGen.P_DATE_FROM, this);
            return _dateFrom;
        }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.Form76KD2008Report#getDateTo()
     */
        public PropertyPath<Date> dateTo()
        {
            if(_dateTo == null )
                _dateTo = new PropertyPath<Date>(Form76KD2008ReportGen.P_DATE_TO, this);
            return _dateTo;
        }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.uniec.entity.report.Form76KD2008Report#getDevelopCondition()
     */
        public PropertyPath<String> developCondition()
        {
            if(_developCondition == null )
                _developCondition = new PropertyPath<String>(Form76KD2008ReportGen.P_DEVELOP_CONDITION, this);
            return _developCondition;
        }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.uniec.entity.report.Form76KD2008Report#getQualification()
     */
        public PropertyPath<String> qualification()
        {
            if(_qualification == null )
                _qualification = new PropertyPath<String>(Form76KD2008ReportGen.P_QUALIFICATION, this);
            return _qualification;
        }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.uniec.entity.report.Form76KD2008Report#getStudentCategory()
     */
        public PropertyPath<String> studentCategory()
        {
            if(_studentCategory == null )
                _studentCategory = new PropertyPath<String>(Form76KD2008ReportGen.P_STUDENT_CATEGORY, this);
            return _studentCategory;
        }

    /**
     * @return Исключая поступивших на параллельное освоение образовательных программ.
     * @see ru.tandemservice.uniec.entity.report.Form76KD2008Report#getExcludeParallel()
     */
        public PropertyPath<Boolean> excludeParallel()
        {
            if(_excludeParallel == null )
                _excludeParallel = new PropertyPath<Boolean>(Form76KD2008ReportGen.P_EXCLUDE_PARALLEL, this);
            return _excludeParallel;
        }

        public Class getEntityClass()
        {
            return Form76KD2008Report.class;
        }

        public String getEntityName()
        {
            return "form76KD2008Report";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
