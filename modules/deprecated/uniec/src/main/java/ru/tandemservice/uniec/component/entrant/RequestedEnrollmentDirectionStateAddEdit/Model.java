/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.RequestedEnrollmentDirectionStateAddEdit;

import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;

import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

/**
 * @author vip_delete
 * @since 03.04.2009
 */
@Input({
        @Bind(key = "requestedEnrollmentDirectionId", binding = "requestedEnrollmentDirection.id")
})
public class Model
{
    private RequestedEnrollmentDirection _requestedEnrollmentDirection = new RequestedEnrollmentDirection();
    private List<EntrantState> _entrantStateList;
    private EntrantState _entrantState;

    public RequestedEnrollmentDirection getRequestedEnrollmentDirection()
    {
        return _requestedEnrollmentDirection;
    }

    public void setRequestedEnrollmentDirection(RequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        _requestedEnrollmentDirection = requestedEnrollmentDirection;
    }

    public List<EntrantState> getEntrantStateList()
    {
        return _entrantStateList;
    }

    public void setEntrantStateList(List<EntrantState> entrantStateList)
    {
        _entrantStateList = entrantStateList;
    }

    public EntrantState getEntrantState()
    {
        return _entrantState;
    }

    public void setEntrantState(EntrantState entrantState)
    {
        _entrantState = entrantState;
    }
}
