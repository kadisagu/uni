/* $Id: Model.java 4623 2014-06-11 10:13:15Z vdanilov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.wizard.NextOfKinStep;

import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Return;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.person.base.entity.PersonNextOfKin;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.RelationDegree;
import org.tandemframework.shared.person.catalog.entity.Sex;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

/**
 * @author agolubenko
 */
public class NextOfKinModel
{
    private Long personId;
    private String _personRoleName;
    private PersonNextOfKin _nextOfKin = new PersonNextOfKin();
    private List<RelationDegree> _relationDegreeList;
    private List<Sex> _sexList;
    private List<IdentityCardType> _passportTypeList;
    private List<AddressCountry> _citizenshipList;
    private ISelectModel _issuancePlaceModel;
    private ISelectModel _workPlaceModel;
    private ISelectModel _workPostModel;
    private AddressBase _address;

    private boolean editForm;

    // Getters & Setters

    public Long getPersonId()
    {
        return this.personId;
    }

    public void setPersonId(final Long personId)
    {
        this.personId = personId;
    }

    public String getPersonRoleName()
    {
        return this._personRoleName;
    }

    public void setPersonRoleName(final String personRoleName)
    {
        this._personRoleName = personRoleName;
    }

    public PersonNextOfKin getNextOfKin()
    {
        return this._nextOfKin;
    }

    public void setNextOfKin(final PersonNextOfKin nextOfKin)
    {
        this._nextOfKin = nextOfKin;
    }

    public List<RelationDegree> getRelationDegreeList()
    {
        return this._relationDegreeList;
    }

    public void setRelationDegreeList(final List<RelationDegree> relationDegreeList)
    {
        this._relationDegreeList = relationDegreeList;
    }

    public ISelectModel getIssuancePlaceModel()
    {
        return this._issuancePlaceModel;
    }

    public void setIssuancePlaceModel(final ISelectModel issuancePlaceModel)
    {
        this._issuancePlaceModel = issuancePlaceModel;
    }

    public ISelectModel getWorkPlaceModel()
    {
        return this._workPlaceModel;
    }

    public void setWorkPlaceModel(final ISelectModel workPlaceModel)
    {
        this._workPlaceModel = workPlaceModel;
    }

    public ISelectModel getWorkPostModel()
    {
        return this._workPostModel;
    }

    public void setWorkPostModel(final ISelectModel workPostModel)
    {
        this._workPostModel = workPostModel;
    }

    public boolean isEditForm()
    {
        return this.editForm;
    }

    public void setEditForm(final boolean editForm)
    {
        this.editForm = editForm;
    }

    public AddressBase getAddress()
    {
        return this._address;
    }

    public void setAddress(final AddressBase address)
    {
        this._address = address;
    }

    public List<Sex> getSexList()
    {
        return this._sexList;
    }

    public void setSexList(final List<Sex> sexList)
    {
        this._sexList = sexList;
    }

    public List<IdentityCardType> getPassportTypeList()
    {
        return this._passportTypeList;
    }

    public void setPassportTypeList(final List<IdentityCardType> passportTypeList)
    {
        this._passportTypeList = passportTypeList;
    }

    public List<AddressCountry> getCitizenshipList()
    {
        return this._citizenshipList;
    }

    public void setCitizenshipList(final List<AddressCountry> citizenshipList)
    {
        this._citizenshipList = citizenshipList;
    }

    public boolean isSexListDisabled()
    {
        // если пол не указан - то делаем выбор достпным (как правило, там будет нужный тип)
        if (null == this.getNextOfKin().getSex()) { return false; }

        final RelationDegree relationDegree = this.getNextOfKin().getRelationDegree();
        return (null != relationDegree) && (null != relationDegree.getSex());
    }


}
