/**
 *$Id:$
 */
package ru.tandemservice.uniec.base.bo.EcReport.ui.EntrantDataExportAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.base.bo.EcReport.EcReportManager;
import ru.tandemservice.uniec.base.bo.EcReport.logic.EntrantDataExport.EntrantDataExportModel;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;

/**
 * @author Alexander Shaburov
 * @since 02.08.12
 */
public class EcReportEntrantDataExportAddUI extends UIPresenter
{
    // fields

    private EntrantDataExportModel _model;

    // Presenter listeners

    @Override
    public void onComponentRefresh()
    {
        _model = new EntrantDataExportModel();

        EcReportManager.instance().entrantDataExportDAO().prepareModel(_model);
    }

    public void onChangeEnrollmentCampaign()
    {
        EcReportManager.instance().entrantDataExportDAO().onChangeEnrollmentCampaign(_model);
    }

    // Listeners

    public void onClickApply()
    {
        CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(DataAccessServices.dao().getByCode(UniecScriptItem.class, UniecDefines.ENTRANT_DATA_EXPORT),
                "template", new byte[]{}, // Шаблон не используем, делаем заглушку
                "enrollmentCampaignId", _model.getEnrollmentCampaign().getId(),
                "requestFromLong", _model.getRequestFrom().getTime(),
                "requestToLong", _model.getRequestTo().getTime(),
                "compensationTypeId", _model.isCompensationTypeActive() ? _model.getCompensationType().getId() : null,
                "statusIdList", _model.isStatusActive() ? CommonBaseEntityUtil.getPropertiesList(_model.getStatusList(), EntrantState.id().s()) : null,
                "enrollmentDirectionIdList", CommonBaseEntityUtil.getPropertiesList(MultiEnrollmentDirectionUtil.getEnrollmentDirections(_model, DataAccessServices.dao().getComponentSession()), EnrollmentDirection.id().s())
                );

    }

    // Getters & Setters

    public EntrantDataExportModel getModel()
    {
        return _model;
    }

    public void setModel(EntrantDataExportModel model)
    {
        _model = model;
    }
}
