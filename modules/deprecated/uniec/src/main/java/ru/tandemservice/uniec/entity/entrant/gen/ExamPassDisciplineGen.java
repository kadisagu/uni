package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.entrant.EntrantExamList;
import ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Дисциплина для сдачи
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ExamPassDisciplineGen extends VersionedEntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline";
    public static final String ENTITY_NAME = "examPassDiscipline";
    public static final int VERSION_HASH = -1978186845;
    private static IEntityMeta ENTITY_META;

    public static final String P_VERSION = "version";
    public static final String L_ENROLLMENT_CAMPAIGN_DISCIPLINE = "enrollmentCampaignDiscipline";
    public static final String L_SUBJECT_PASS_FORM = "subjectPassForm";
    public static final String L_ENTRANT_EXAM_LIST = "entrantExamList";
    public static final String P_AUDITORY = "auditory";
    public static final String P_PASS_DATE = "passDate";
    public static final String P_EXAM_COMMISSION_MEMBERSHIP = "examCommissionMembership";

    private int _version; 
    private Discipline2RealizationWayRelation _enrollmentCampaignDiscipline;     // Дисциплина набора вступительных испытаний
    private SubjectPassForm _subjectPassForm;     // Форма сдачи дисциплины
    private EntrantExamList _entrantExamList;     // Экзаменационный лист абитуриента
    private String _auditory;     // Аудитория
    private Date _passDate;     // Дата сдачи
    private String _examCommissionMembership;     // ФИО экзаменаторов

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public int getVersion()
    {
        return _version;
    }

    /**
     * @param version  Свойство не может быть null.
     */
    public void setVersion(int version)
    {
        dirty(_version, version);
        _version = version;
    }

    /**
     * @return Дисциплина набора вступительных испытаний. Свойство не может быть null.
     */
    @NotNull
    public Discipline2RealizationWayRelation getEnrollmentCampaignDiscipline()
    {
        return _enrollmentCampaignDiscipline;
    }

    /**
     * @param enrollmentCampaignDiscipline Дисциплина набора вступительных испытаний. Свойство не может быть null.
     */
    public void setEnrollmentCampaignDiscipline(Discipline2RealizationWayRelation enrollmentCampaignDiscipline)
    {
        dirty(_enrollmentCampaignDiscipline, enrollmentCampaignDiscipline);
        _enrollmentCampaignDiscipline = enrollmentCampaignDiscipline;
    }

    /**
     * @return Форма сдачи дисциплины. Свойство не может быть null.
     */
    @NotNull
    public SubjectPassForm getSubjectPassForm()
    {
        return _subjectPassForm;
    }

    /**
     * @param subjectPassForm Форма сдачи дисциплины. Свойство не может быть null.
     */
    public void setSubjectPassForm(SubjectPassForm subjectPassForm)
    {
        dirty(_subjectPassForm, subjectPassForm);
        _subjectPassForm = subjectPassForm;
    }

    /**
     * @return Экзаменационный лист абитуриента. Свойство не может быть null.
     */
    @NotNull
    public EntrantExamList getEntrantExamList()
    {
        return _entrantExamList;
    }

    /**
     * @param entrantExamList Экзаменационный лист абитуриента. Свойство не может быть null.
     */
    public void setEntrantExamList(EntrantExamList entrantExamList)
    {
        dirty(_entrantExamList, entrantExamList);
        _entrantExamList = entrantExamList;
    }

    /**
     * @return Аудитория.
     */
    @Length(max=255)
    public String getAuditory()
    {
        return _auditory;
    }

    /**
     * @param auditory Аудитория.
     */
    public void setAuditory(String auditory)
    {
        dirty(_auditory, auditory);
        _auditory = auditory;
    }

    /**
     * @return Дата сдачи.
     */
    public Date getPassDate()
    {
        return _passDate;
    }

    /**
     * @param passDate Дата сдачи.
     */
    public void setPassDate(Date passDate)
    {
        dirty(_passDate, passDate);
        _passDate = passDate;
    }

    /**
     * @return ФИО экзаменаторов.
     */
    @Length(max=255)
    public String getExamCommissionMembership()
    {
        return _examCommissionMembership;
    }

    /**
     * @param examCommissionMembership ФИО экзаменаторов.
     */
    public void setExamCommissionMembership(String examCommissionMembership)
    {
        dirty(_examCommissionMembership, examCommissionMembership);
        _examCommissionMembership = examCommissionMembership;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ExamPassDisciplineGen)
        {
            setVersion(((ExamPassDiscipline)another).getVersion());
            setEnrollmentCampaignDiscipline(((ExamPassDiscipline)another).getEnrollmentCampaignDiscipline());
            setSubjectPassForm(((ExamPassDiscipline)another).getSubjectPassForm());
            setEntrantExamList(((ExamPassDiscipline)another).getEntrantExamList());
            setAuditory(((ExamPassDiscipline)another).getAuditory());
            setPassDate(((ExamPassDiscipline)another).getPassDate());
            setExamCommissionMembership(((ExamPassDiscipline)another).getExamCommissionMembership());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ExamPassDisciplineGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ExamPassDiscipline.class;
        }

        public T newInstance()
        {
            return (T) new ExamPassDiscipline();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "version":
                    return obj.getVersion();
                case "enrollmentCampaignDiscipline":
                    return obj.getEnrollmentCampaignDiscipline();
                case "subjectPassForm":
                    return obj.getSubjectPassForm();
                case "entrantExamList":
                    return obj.getEntrantExamList();
                case "auditory":
                    return obj.getAuditory();
                case "passDate":
                    return obj.getPassDate();
                case "examCommissionMembership":
                    return obj.getExamCommissionMembership();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "version":
                    obj.setVersion((Integer) value);
                    return;
                case "enrollmentCampaignDiscipline":
                    obj.setEnrollmentCampaignDiscipline((Discipline2RealizationWayRelation) value);
                    return;
                case "subjectPassForm":
                    obj.setSubjectPassForm((SubjectPassForm) value);
                    return;
                case "entrantExamList":
                    obj.setEntrantExamList((EntrantExamList) value);
                    return;
                case "auditory":
                    obj.setAuditory((String) value);
                    return;
                case "passDate":
                    obj.setPassDate((Date) value);
                    return;
                case "examCommissionMembership":
                    obj.setExamCommissionMembership((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "version":
                        return true;
                case "enrollmentCampaignDiscipline":
                        return true;
                case "subjectPassForm":
                        return true;
                case "entrantExamList":
                        return true;
                case "auditory":
                        return true;
                case "passDate":
                        return true;
                case "examCommissionMembership":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "version":
                    return true;
                case "enrollmentCampaignDiscipline":
                    return true;
                case "subjectPassForm":
                    return true;
                case "entrantExamList":
                    return true;
                case "auditory":
                    return true;
                case "passDate":
                    return true;
                case "examCommissionMembership":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "version":
                    return Integer.class;
                case "enrollmentCampaignDiscipline":
                    return Discipline2RealizationWayRelation.class;
                case "subjectPassForm":
                    return SubjectPassForm.class;
                case "entrantExamList":
                    return EntrantExamList.class;
                case "auditory":
                    return String.class;
                case "passDate":
                    return Date.class;
                case "examCommissionMembership":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ExamPassDiscipline> _dslPath = new Path<ExamPassDiscipline>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ExamPassDiscipline");
    }
            

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline#getVersion()
     */
    public static PropertyPath<Integer> version()
    {
        return _dslPath.version();
    }

    /**
     * @return Дисциплина набора вступительных испытаний. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline#getEnrollmentCampaignDiscipline()
     */
    public static Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation> enrollmentCampaignDiscipline()
    {
        return _dslPath.enrollmentCampaignDiscipline();
    }

    /**
     * @return Форма сдачи дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline#getSubjectPassForm()
     */
    public static SubjectPassForm.Path<SubjectPassForm> subjectPassForm()
    {
        return _dslPath.subjectPassForm();
    }

    /**
     * @return Экзаменационный лист абитуриента. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline#getEntrantExamList()
     */
    public static EntrantExamList.Path<EntrantExamList> entrantExamList()
    {
        return _dslPath.entrantExamList();
    }

    /**
     * @return Аудитория.
     * @see ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline#getAuditory()
     */
    public static PropertyPath<String> auditory()
    {
        return _dslPath.auditory();
    }

    /**
     * @return Дата сдачи.
     * @see ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline#getPassDate()
     */
    public static PropertyPath<Date> passDate()
    {
        return _dslPath.passDate();
    }

    /**
     * @return ФИО экзаменаторов.
     * @see ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline#getExamCommissionMembership()
     */
    public static PropertyPath<String> examCommissionMembership()
    {
        return _dslPath.examCommissionMembership();
    }

    public static class Path<E extends ExamPassDiscipline> extends EntityPath<E>
    {
        private PropertyPath<Integer> _version;
        private Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation> _enrollmentCampaignDiscipline;
        private SubjectPassForm.Path<SubjectPassForm> _subjectPassForm;
        private EntrantExamList.Path<EntrantExamList> _entrantExamList;
        private PropertyPath<String> _auditory;
        private PropertyPath<Date> _passDate;
        private PropertyPath<String> _examCommissionMembership;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline#getVersion()
     */
        public PropertyPath<Integer> version()
        {
            if(_version == null )
                _version = new PropertyPath<Integer>(ExamPassDisciplineGen.P_VERSION, this);
            return _version;
        }

    /**
     * @return Дисциплина набора вступительных испытаний. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline#getEnrollmentCampaignDiscipline()
     */
        public Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation> enrollmentCampaignDiscipline()
        {
            if(_enrollmentCampaignDiscipline == null )
                _enrollmentCampaignDiscipline = new Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation>(L_ENROLLMENT_CAMPAIGN_DISCIPLINE, this);
            return _enrollmentCampaignDiscipline;
        }

    /**
     * @return Форма сдачи дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline#getSubjectPassForm()
     */
        public SubjectPassForm.Path<SubjectPassForm> subjectPassForm()
        {
            if(_subjectPassForm == null )
                _subjectPassForm = new SubjectPassForm.Path<SubjectPassForm>(L_SUBJECT_PASS_FORM, this);
            return _subjectPassForm;
        }

    /**
     * @return Экзаменационный лист абитуриента. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline#getEntrantExamList()
     */
        public EntrantExamList.Path<EntrantExamList> entrantExamList()
        {
            if(_entrantExamList == null )
                _entrantExamList = new EntrantExamList.Path<EntrantExamList>(L_ENTRANT_EXAM_LIST, this);
            return _entrantExamList;
        }

    /**
     * @return Аудитория.
     * @see ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline#getAuditory()
     */
        public PropertyPath<String> auditory()
        {
            if(_auditory == null )
                _auditory = new PropertyPath<String>(ExamPassDisciplineGen.P_AUDITORY, this);
            return _auditory;
        }

    /**
     * @return Дата сдачи.
     * @see ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline#getPassDate()
     */
        public PropertyPath<Date> passDate()
        {
            if(_passDate == null )
                _passDate = new PropertyPath<Date>(ExamPassDisciplineGen.P_PASS_DATE, this);
            return _passDate;
        }

    /**
     * @return ФИО экзаменаторов.
     * @see ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline#getExamCommissionMembership()
     */
        public PropertyPath<String> examCommissionMembership()
        {
            if(_examCommissionMembership == null )
                _examCommissionMembership = new PropertyPath<String>(ExamPassDisciplineGen.P_EXAM_COMMISSION_MEMBERSHIP, this);
            return _examCommissionMembership;
        }

        public Class getEntityClass()
        {
            return ExamPassDiscipline.class;
        }

        public String getEntityName()
        {
            return "examPassDiscipline";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
