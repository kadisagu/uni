/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.settings.ExamGroupLogicList;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Vasily Zhukov
 * @since 28.05.2010
 */
public interface IDAO extends IUniDao<Model>
{
    void updateExamGroupLogic(Model model, Long examGroupLogicId, boolean current);
}
