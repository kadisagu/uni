/* $Id$ */
package ru.tandemservice.uniec.component.menu.SplitOrdersList;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.unimove.entity.catalog.OrderStates;

import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 20.08.2012
 */
public class Model
{
    public static final String[] ORG_UNI_FULL_TITLE = new String[]{AbstractStudentOrder.L_ORG_UNIT, OrgUnit.P_FULL_TITLE};

    private ISelectModel _educationYearList;
    private List<OrderStates> _orderStateList;
    private ISelectModel _orderTypeList;
    private DynamicListDataSource _dataSource;
    private IDataSettings _settings;

    // Getters & Setters

    public ISelectModel getEducationYearList()
    {
        return _educationYearList;
    }

    public void setEducationYearList(ISelectModel educationYearList)
    {
        _educationYearList = educationYearList;
    }

    public List<OrderStates> getOrderStateList()
    {
        return _orderStateList;
    }

    public void setOrderStateList(List<OrderStates> orderStateList)
    {
        _orderStateList = orderStateList;
    }

    public ISelectModel getOrderTypeList()
    {
        return _orderTypeList;
    }

    public void setOrderTypeList(ISelectModel orderTypeList)
    {
        _orderTypeList = orderTypeList;
    }

    public DynamicListDataSource getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource dataSource)
    {
        _dataSource = dataSource;
    }

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }
}