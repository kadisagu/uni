package ru.tandemservice.uniec.entity.orders.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.orders.SplitBudgetMasterEntrantsStuListExtract;
import ru.tandemservice.uniec.entity.orders.SplitEntrantsStuListExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «О распределении зачисленных на бюджет абитуриентов по профилям (магистратура)»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SplitBudgetMasterEntrantsStuListExtractGen extends SplitEntrantsStuListExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.orders.SplitBudgetMasterEntrantsStuListExtract";
    public static final String ENTITY_NAME = "splitBudgetMasterEntrantsStuListExtract";
    public static final int VERSION_HASH = 845384795;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SplitBudgetMasterEntrantsStuListExtractGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SplitBudgetMasterEntrantsStuListExtractGen> extends SplitEntrantsStuListExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SplitBudgetMasterEntrantsStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new SplitBudgetMasterEntrantsStuListExtract();
        }
    }
    private static final Path<SplitBudgetMasterEntrantsStuListExtract> _dslPath = new Path<SplitBudgetMasterEntrantsStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SplitBudgetMasterEntrantsStuListExtract");
    }
            

    public static class Path<E extends SplitBudgetMasterEntrantsStuListExtract> extends SplitEntrantsStuListExtract.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return SplitBudgetMasterEntrantsStuListExtract.class;
        }

        public String getEntityName()
        {
            return "splitBudgetMasterEntrantsStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
