package ru.tandemservice.uniec.component.menu.StateExamImport;

import jxl.Workbook;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.*;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.person.base.entity.gen.IdentityCardGen;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author vdanilov
 */
public class ImportByCertificateWithNoFio extends UniBaseDao implements IImportByCertificate {

    public static final SpringBeanCache<IImportByCertificate> instance = new SpringBeanCache<>(ImportByCertificateWithNoFio.class.getSimpleName());

    private static final String SEPARATOR = "%";
    private static final String FILE_CHARSET = "Windows-1251";

    private static final String COMMENT_ROW_PREFIX = "Комментарий:";

    private static final String STATUS_GOOD = "Действительно";

    private static final String REPLY_IMPORTED = "ИМПОРТИРОВАНО";

    private static final int CERT_NUM_IDX = 0;
    private static final int ID_CARD_SERIA_IDX = 2;
    private static final int ID_CARD_NUMBER_IDX = 3;
    private static final int STATUS_IDX = 6;

    @Override
    public String getTitle() {
        return "По номерам свидетельств ЕГЭ (без ФИО) (ФБС)";
    }

    @Override
    public byte[] doImport(final EnrollmentCampaign enrollmentCampaign, byte[] importedContent) {
        return doExecuteImportByCertificate(enrollmentCampaign, importedContent);
    }

    public byte[] doExecuteImportByCertificate(final EnrollmentCampaign enrollmentCampaign, byte[] importedContent)
    {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            WritableWorkbook workbook = Workbook.createWorkbook(out);

            List<Row> rows = new ArrayList<>();

            WritableSheet sheet = workbook.createSheet("Результат импорта", 0);
            sheet.setColumnView(CERT_NUM_IDX, 16);
            sheet.setColumnView(STATUS_IDX, 16);

            WritableCellFormat FORMAT_RED = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 10, WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.RED));

            try
            {

                BufferedReader in = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(importedContent), FILE_CHARSET));
                String header = in.readLine();
                String[] headerItems = header.split(SEPARATOR);

                fillRow(sheet, headerItems, 0, null);

                int rownum = 1;
                while (true)
                {
                    String line = in.readLine();
                    if (line == null) break;

                    if (line.startsWith(COMMENT_ROW_PREFIX))
                    {
                        sheet.addCell(new Label(0, rownum, line));
                        continue;
                    }
                    if (line.length() == 0)
                    {
                        continue;
                    }
                    String[] rowItems = line.split(SEPARATOR);
                    String status = rowItems[STATUS_IDX];

                    boolean accepted = status.equalsIgnoreCase(STATUS_GOOD);
                    fillRow(sheet, rowItems, rownum, accepted ? null : FORMAT_RED);

                    String seria = rowItems[ID_CARD_SERIA_IDX];
                    if (!StringUtils.isEmpty(seria))
                        seria = String.format("%4s", seria).replaceAll(" ", "0");

                    String number = rowItems[ID_CARD_NUMBER_IDX];
                    if (!StringUtils.isEmpty(number))
                        number = String.format("%6s", number).replaceAll(" ", "0");

                    Row row = new Row(accepted,
                        rownum,
                        rowItems.length,
                        rowItems[CERT_NUM_IDX].replaceAll("-", ""),
                        seria,
                        number
                    );

                    rows.add(row);
                    rownum++;
                }
            }
            catch (ApplicationException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                logger.error(e.getMessage(), e);
                if (Debug.isDisplay()) { throw e; }
                throw new ApplicationException("Формат файла некорректен, загрузка данных невозможна.");
            }

            final Session session = getSession();

            // certificateNumber -> certificateId -> IdCardKey
            Map<String, Map<Long, String>> certificateCache = new HashMap<>();
            MQBuilder builder = new MQBuilder(EntrantStateExamCertificate.ENTITY_CLASS, "crt", new String[]{"id", EntrantStateExamCertificate.P_NUMBER});
            builder.addJoin("crt", EntrantStateExamCertificate.entrant().s(), "entrant");
            builder.add(MQExpression.eq("entrant", Entrant.enrollmentCampaign().s(), enrollmentCampaign));
            builder.addJoin("entrant", Entrant.person().identityCard().s(), "idCard");
            builder.addSelect("idCard", new String[] { IdentityCardGen.P_SERIA, IdentityCardGen.P_NUMBER });

            for (Object[] row : builder.<Object[]>getResultListNonFetched(getSession()))
            {
                Long certificateId = (Long) row[0];
                String certificateNumber = (String) row[1];
                String idCardSeria = (String) row[2];
                String idCardNumber = (String) row[3];

                Map<Long, String> certificateNumberData = certificateCache.get(certificateNumber);
                if (certificateNumberData == null)
                    certificateCache.put(certificateNumber, certificateNumberData = new HashMap<>());
                certificateNumberData.put(certificateId, getIdCardKey(idCardSeria, idCardNumber));
            }

            for (Row row : rows)
            {
                if (!row.isAccepted()) continue;

                Map<Long, String> certificateNumberData = certificateCache.get(row.getCertNumber());

                boolean imported = false;

                // ищем этот сертификат
                if (certificateNumberData != null)
                {
                    String idCardKey = getIdCardKey(row.getIdCardSeria(), row.getIdCardNumber());

                    for (Map.Entry<Long, String> entry : certificateNumberData.entrySet())
                    {
                        if (entry.getValue().equals(idCardKey))
                        {
                            EntrantStateExamCertificate certificate = getNotNull(entry.getKey());
                            certificate.setSent(true);
                            certificate.setAccepted(row.isAccepted());
                            session.update(certificate);

                            imported = true;
                        }
                    }
                }

                // если абитуриент прошел проверку в фбс и он найден в базе, то ставим признак импортирован
                if (row.isAccepted() && imported)
                {
                    ((Label) sheet.getCell(STATUS_IDX, row.getRownum())).setString(REPLY_IMPORTED);
                } else
                {
                    for (int i = 0; i < row.getLength(); i++)
                        ((Label) sheet.getCell(i, row.getRownum())).setCellFormat(FORMAT_RED);
                }
            }

            workbook.write();
            workbook.close();

            return out.toByteArray();
        } catch (Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    private static class Row
    {
        private boolean _accepted;
        private int _rownum;
        private int _length;
        private String _certNumber;
        private String _idCardSeria;
        private String _idCardNumber;

        private Row(boolean accepted, int rownum, int length, String certNumber, String idCardSeria, String idCardNumber)
        {
            _accepted = accepted;
            _rownum = rownum;
            _length = length;
            _certNumber = certNumber;
            _idCardSeria = idCardSeria;
            _idCardNumber = idCardNumber;
        }

        public boolean isAccepted()
        {
            return _accepted;
        }

        public int getRownum()
        {
            return _rownum;
        }

        public int getLength()
        {
            return _length;
        }

        public String getCertNumber()
        {
            return _certNumber;
        }

        public String getIdCardSeria()
        {
            return _idCardSeria;
        }

        public String getIdCardNumber()
        {
            return _idCardNumber;
        }
    }

    private static String getIdCardKey(String idCardSeria, String idCardNumber)
    {
        StringBuilder idCardKey = new StringBuilder();
        if (!StringUtils.isEmpty(idCardSeria))
            idCardKey.append(idCardSeria.toLowerCase());
        if (!StringUtils.isEmpty(idCardNumber))
            idCardKey.append(idCardNumber.toLowerCase());
        return idCardKey.toString();
    }

    private static void fillRow(WritableSheet sheet, String[] row, int rownum, WritableCellFormat format) throws Exception
    {
        for (int i = 0; i < row.length; i++)
        {
            if (format != null)
                sheet.addCell(new Label(i, rownum, row[i], format));
            else
                sheet.addCell(new Label(i, rownum, row[i]));
        }
    }

}
