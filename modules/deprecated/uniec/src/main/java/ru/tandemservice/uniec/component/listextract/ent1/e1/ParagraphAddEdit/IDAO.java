/* $Id$ */
package ru.tandemservice.uniec.component.listextract.ent1.e1.ParagraphAddEdit;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.IAbstractListParagraphAddEditDAO;
import ru.tandemservice.uniec.entity.orders.SplitBudgetBachelorEntrantsStuListExtract;

/**
 * @author Dmitry Seleznev
 * @since 13.07.2012
 */
public interface IDAO extends IAbstractListParagraphAddEditDAO<SplitBudgetBachelorEntrantsStuListExtract, Model>
{
}