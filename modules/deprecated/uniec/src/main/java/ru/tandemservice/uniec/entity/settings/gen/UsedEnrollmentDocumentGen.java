package ru.tandemservice.uniec.entity.settings.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.catalog.EnrollmentDocument;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.settings.UsedEnrollmentDocument;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Используемый документ для подачи в ОУ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsedEnrollmentDocumentGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.settings.UsedEnrollmentDocument";
    public static final String ENTITY_NAME = "usedEnrollmentDocument";
    public static final int VERSION_HASH = -2018478542;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_DOCUMENT = "enrollmentDocument";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_USED = "used";
    public static final String P_PRINT_EDUCATION_DOCUMENT_INFO = "printEducationDocumentInfo";
    public static final String P_PRINT_EDU_DOCUMENT_ATTACHMENT_INFO = "printEduDocumentAttachmentInfo";
    public static final String P_PRINT_STATE_EXAM_CERTIFICATE_NUMBER = "printStateExamCertificateNumber";
    public static final String P_PRINT_ORIGINALITY_INFO = "printOriginalityInfo";

    private EnrollmentDocument _enrollmentDocument;     // Документы для подачи в ОУ
    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private boolean _used;     // Использовать
    private boolean _printEducationDocumentInfo;     // Печатать серию и номер документа об образовании
    private boolean _printEduDocumentAttachmentInfo;     // Печатать серию и номер приложения к документу об образовании
    private boolean _printStateExamCertificateNumber;     // Печатать номер свидетельства ЕГЭ
    private boolean _printOriginalityInfo;     // Печатать сведения об оригинальности

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Документы для подачи в ОУ. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentDocument getEnrollmentDocument()
    {
        return _enrollmentDocument;
    }

    /**
     * @param enrollmentDocument Документы для подачи в ОУ. Свойство не может быть null.
     */
    public void setEnrollmentDocument(EnrollmentDocument enrollmentDocument)
    {
        dirty(_enrollmentDocument, enrollmentDocument);
        _enrollmentDocument = enrollmentDocument;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Использовать. Свойство не может быть null.
     */
    @NotNull
    public boolean isUsed()
    {
        return _used;
    }

    /**
     * @param used Использовать. Свойство не может быть null.
     */
    public void setUsed(boolean used)
    {
        dirty(_used, used);
        _used = used;
    }

    /**
     * @return Печатать серию и номер документа об образовании. Свойство не может быть null.
     */
    @NotNull
    public boolean isPrintEducationDocumentInfo()
    {
        return _printEducationDocumentInfo;
    }

    /**
     * @param printEducationDocumentInfo Печатать серию и номер документа об образовании. Свойство не может быть null.
     */
    public void setPrintEducationDocumentInfo(boolean printEducationDocumentInfo)
    {
        dirty(_printEducationDocumentInfo, printEducationDocumentInfo);
        _printEducationDocumentInfo = printEducationDocumentInfo;
    }

    /**
     * @return Печатать серию и номер приложения к документу об образовании. Свойство не может быть null.
     */
    @NotNull
    public boolean isPrintEduDocumentAttachmentInfo()
    {
        return _printEduDocumentAttachmentInfo;
    }

    /**
     * @param printEduDocumentAttachmentInfo Печатать серию и номер приложения к документу об образовании. Свойство не может быть null.
     */
    public void setPrintEduDocumentAttachmentInfo(boolean printEduDocumentAttachmentInfo)
    {
        dirty(_printEduDocumentAttachmentInfo, printEduDocumentAttachmentInfo);
        _printEduDocumentAttachmentInfo = printEduDocumentAttachmentInfo;
    }

    /**
     * @return Печатать номер свидетельства ЕГЭ. Свойство не может быть null.
     */
    @NotNull
    public boolean isPrintStateExamCertificateNumber()
    {
        return _printStateExamCertificateNumber;
    }

    /**
     * @param printStateExamCertificateNumber Печатать номер свидетельства ЕГЭ. Свойство не может быть null.
     */
    public void setPrintStateExamCertificateNumber(boolean printStateExamCertificateNumber)
    {
        dirty(_printStateExamCertificateNumber, printStateExamCertificateNumber);
        _printStateExamCertificateNumber = printStateExamCertificateNumber;
    }

    /**
     * @return Печатать сведения об оригинальности. Свойство не может быть null.
     */
    @NotNull
    public boolean isPrintOriginalityInfo()
    {
        return _printOriginalityInfo;
    }

    /**
     * @param printOriginalityInfo Печатать сведения об оригинальности. Свойство не может быть null.
     */
    public void setPrintOriginalityInfo(boolean printOriginalityInfo)
    {
        dirty(_printOriginalityInfo, printOriginalityInfo);
        _printOriginalityInfo = printOriginalityInfo;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsedEnrollmentDocumentGen)
        {
            setEnrollmentDocument(((UsedEnrollmentDocument)another).getEnrollmentDocument());
            setEnrollmentCampaign(((UsedEnrollmentDocument)another).getEnrollmentCampaign());
            setUsed(((UsedEnrollmentDocument)another).isUsed());
            setPrintEducationDocumentInfo(((UsedEnrollmentDocument)another).isPrintEducationDocumentInfo());
            setPrintEduDocumentAttachmentInfo(((UsedEnrollmentDocument)another).isPrintEduDocumentAttachmentInfo());
            setPrintStateExamCertificateNumber(((UsedEnrollmentDocument)another).isPrintStateExamCertificateNumber());
            setPrintOriginalityInfo(((UsedEnrollmentDocument)another).isPrintOriginalityInfo());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsedEnrollmentDocumentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsedEnrollmentDocument.class;
        }

        public T newInstance()
        {
            return (T) new UsedEnrollmentDocument();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentDocument":
                    return obj.getEnrollmentDocument();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "used":
                    return obj.isUsed();
                case "printEducationDocumentInfo":
                    return obj.isPrintEducationDocumentInfo();
                case "printEduDocumentAttachmentInfo":
                    return obj.isPrintEduDocumentAttachmentInfo();
                case "printStateExamCertificateNumber":
                    return obj.isPrintStateExamCertificateNumber();
                case "printOriginalityInfo":
                    return obj.isPrintOriginalityInfo();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentDocument":
                    obj.setEnrollmentDocument((EnrollmentDocument) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "used":
                    obj.setUsed((Boolean) value);
                    return;
                case "printEducationDocumentInfo":
                    obj.setPrintEducationDocumentInfo((Boolean) value);
                    return;
                case "printEduDocumentAttachmentInfo":
                    obj.setPrintEduDocumentAttachmentInfo((Boolean) value);
                    return;
                case "printStateExamCertificateNumber":
                    obj.setPrintStateExamCertificateNumber((Boolean) value);
                    return;
                case "printOriginalityInfo":
                    obj.setPrintOriginalityInfo((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentDocument":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "used":
                        return true;
                case "printEducationDocumentInfo":
                        return true;
                case "printEduDocumentAttachmentInfo":
                        return true;
                case "printStateExamCertificateNumber":
                        return true;
                case "printOriginalityInfo":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentDocument":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "used":
                    return true;
                case "printEducationDocumentInfo":
                    return true;
                case "printEduDocumentAttachmentInfo":
                    return true;
                case "printStateExamCertificateNumber":
                    return true;
                case "printOriginalityInfo":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentDocument":
                    return EnrollmentDocument.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "used":
                    return Boolean.class;
                case "printEducationDocumentInfo":
                    return Boolean.class;
                case "printEduDocumentAttachmentInfo":
                    return Boolean.class;
                case "printStateExamCertificateNumber":
                    return Boolean.class;
                case "printOriginalityInfo":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsedEnrollmentDocument> _dslPath = new Path<UsedEnrollmentDocument>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsedEnrollmentDocument");
    }
            

    /**
     * @return Документы для подачи в ОУ. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.UsedEnrollmentDocument#getEnrollmentDocument()
     */
    public static EnrollmentDocument.Path<EnrollmentDocument> enrollmentDocument()
    {
        return _dslPath.enrollmentDocument();
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.UsedEnrollmentDocument#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Использовать. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.UsedEnrollmentDocument#isUsed()
     */
    public static PropertyPath<Boolean> used()
    {
        return _dslPath.used();
    }

    /**
     * @return Печатать серию и номер документа об образовании. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.UsedEnrollmentDocument#isPrintEducationDocumentInfo()
     */
    public static PropertyPath<Boolean> printEducationDocumentInfo()
    {
        return _dslPath.printEducationDocumentInfo();
    }

    /**
     * @return Печатать серию и номер приложения к документу об образовании. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.UsedEnrollmentDocument#isPrintEduDocumentAttachmentInfo()
     */
    public static PropertyPath<Boolean> printEduDocumentAttachmentInfo()
    {
        return _dslPath.printEduDocumentAttachmentInfo();
    }

    /**
     * @return Печатать номер свидетельства ЕГЭ. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.UsedEnrollmentDocument#isPrintStateExamCertificateNumber()
     */
    public static PropertyPath<Boolean> printStateExamCertificateNumber()
    {
        return _dslPath.printStateExamCertificateNumber();
    }

    /**
     * @return Печатать сведения об оригинальности. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.UsedEnrollmentDocument#isPrintOriginalityInfo()
     */
    public static PropertyPath<Boolean> printOriginalityInfo()
    {
        return _dslPath.printOriginalityInfo();
    }

    public static class Path<E extends UsedEnrollmentDocument> extends EntityPath<E>
    {
        private EnrollmentDocument.Path<EnrollmentDocument> _enrollmentDocument;
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<Boolean> _used;
        private PropertyPath<Boolean> _printEducationDocumentInfo;
        private PropertyPath<Boolean> _printEduDocumentAttachmentInfo;
        private PropertyPath<Boolean> _printStateExamCertificateNumber;
        private PropertyPath<Boolean> _printOriginalityInfo;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Документы для подачи в ОУ. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.UsedEnrollmentDocument#getEnrollmentDocument()
     */
        public EnrollmentDocument.Path<EnrollmentDocument> enrollmentDocument()
        {
            if(_enrollmentDocument == null )
                _enrollmentDocument = new EnrollmentDocument.Path<EnrollmentDocument>(L_ENROLLMENT_DOCUMENT, this);
            return _enrollmentDocument;
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.UsedEnrollmentDocument#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Использовать. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.UsedEnrollmentDocument#isUsed()
     */
        public PropertyPath<Boolean> used()
        {
            if(_used == null )
                _used = new PropertyPath<Boolean>(UsedEnrollmentDocumentGen.P_USED, this);
            return _used;
        }

    /**
     * @return Печатать серию и номер документа об образовании. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.UsedEnrollmentDocument#isPrintEducationDocumentInfo()
     */
        public PropertyPath<Boolean> printEducationDocumentInfo()
        {
            if(_printEducationDocumentInfo == null )
                _printEducationDocumentInfo = new PropertyPath<Boolean>(UsedEnrollmentDocumentGen.P_PRINT_EDUCATION_DOCUMENT_INFO, this);
            return _printEducationDocumentInfo;
        }

    /**
     * @return Печатать серию и номер приложения к документу об образовании. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.UsedEnrollmentDocument#isPrintEduDocumentAttachmentInfo()
     */
        public PropertyPath<Boolean> printEduDocumentAttachmentInfo()
        {
            if(_printEduDocumentAttachmentInfo == null )
                _printEduDocumentAttachmentInfo = new PropertyPath<Boolean>(UsedEnrollmentDocumentGen.P_PRINT_EDU_DOCUMENT_ATTACHMENT_INFO, this);
            return _printEduDocumentAttachmentInfo;
        }

    /**
     * @return Печатать номер свидетельства ЕГЭ. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.UsedEnrollmentDocument#isPrintStateExamCertificateNumber()
     */
        public PropertyPath<Boolean> printStateExamCertificateNumber()
        {
            if(_printStateExamCertificateNumber == null )
                _printStateExamCertificateNumber = new PropertyPath<Boolean>(UsedEnrollmentDocumentGen.P_PRINT_STATE_EXAM_CERTIFICATE_NUMBER, this);
            return _printStateExamCertificateNumber;
        }

    /**
     * @return Печатать сведения об оригинальности. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.UsedEnrollmentDocument#isPrintOriginalityInfo()
     */
        public PropertyPath<Boolean> printOriginalityInfo()
        {
            if(_printOriginalityInfo == null )
                _printOriginalityInfo = new PropertyPath<Boolean>(UsedEnrollmentDocumentGen.P_PRINT_ORIGINALITY_INFO, this);
            return _printOriginalityInfo;
        }

        public Class getEntityClass()
        {
            return UsedEnrollmentDocument.class;
        }

        public String getEntityName()
        {
            return "usedEnrollmentDocument";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
