/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.block.passDisciplineData;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.EcReportPersonAddUI;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline;

import java.util.Date;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 02.07.2013
 */
public class PassDisciplineDataParam implements IReportDQLModifier
{
    private IReportParam<DataWrapper> _educationSubject = new ReportParam<>();
    private IReportParam<SubjectPassForm> _subjectPassForm = new ReportParam<>();
    private IReportParam<Date> _passDate = new ReportParam<>();

    private String entrantAlias(ReportDQL dql)
    {
        // соединяем абитуриента
        String entrantAlias = dql.innerJoinEntity(Entrant.class, Entrant.person());

        // добавляем сортировку
        dql.order(entrantAlias, Entrant.enrollmentCampaign().id());

        return entrantAlias;
    }

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {

    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (_educationSubject.isActive() || _subjectPassForm.isActive() || _passDate.isActive())
        {
            String nextAlias = dql.nextAlias();

            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(ExamPassDiscipline.class, nextAlias)
                    .predicate(DQLPredicateType.distinct)
                    .column(property(ExamPassDiscipline.id().fromAlias(nextAlias)))
                    .where(eq(
                            property(ExamPassDiscipline.entrantExamList().entrant().entrantId().fromAlias(nextAlias)),
                            property(Entrant.id().fromAlias(entrantAlias(dql)))
                    ));

            if (_educationSubject.isActive())
            {
                printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "passDisciplineData.educationSubject", _educationSubject.getData().getTitle());
                builder.where(eq(
                        property(ExamPassDiscipline.enrollmentCampaignDiscipline().id().fromAlias(nextAlias)),
                        value(_educationSubject.getData().getId())
                ));
            }

            if (_subjectPassForm.isActive())
            {
                printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "passDisciplineData.subjectPassForm", _subjectPassForm.getData().getTitle());
                builder.where(eq(
                        property(ExamPassDiscipline.subjectPassForm().id().fromAlias(nextAlias)),
                        value(_subjectPassForm.getData().getId())
                ));
            }

            if (_passDate.isActive())
            {
                printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "passDisciplineData.passDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(_passDate.getData()));
                builder.where(inDay(ExamPassDiscipline.passDate().fromAlias(nextAlias), _passDate.getData()));
            }

            dql.builder.where(exists(builder.buildQuery()));
        }
    }

    public IReportParam<DataWrapper> getEducationSubject()
    {
        return _educationSubject;
    }

    public void setEducationSubject(IReportParam<DataWrapper> educationSubject)
    {
        _educationSubject = educationSubject;
    }

    public IReportParam<SubjectPassForm> getSubjectPassForm()
    {
        return _subjectPassForm;
    }

    public void setSubjectPassForm(IReportParam<SubjectPassForm> subjectPassForm)
    {
        _subjectPassForm = subjectPassForm;
    }

    public IReportParam<Date> getPassDate()
    {
        return _passDate;
    }

    public void setPassDate(IReportParam<Date> passDate)
    {
        _passDate = passDate;
    }
}