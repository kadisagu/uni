/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcCampaign;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;

import ru.tandemservice.uniec.base.bo.EcCampaign.logic.EcCampaignDao;
import ru.tandemservice.uniec.base.bo.EcCampaign.logic.EnrollmentCampaignComboDSHandler;
import ru.tandemservice.uniec.base.bo.EcCampaign.logic.IEcCampaignDao;

/**
 * @author Vasily Zhukov
 * @since 26.05.2011
 */
@Configuration
public class EcCampaignManager extends BusinessObjectManager
{
    public static EcCampaignManager instance()
    {
        return instance(EcCampaignManager.class);
    }

    @Bean
    public IEcCampaignDao dao()
    {
        return new EcCampaignDao();
    }

    @Bean
    public IDefaultComboDataSourceHandler enrollmentCampaignComboDSHandler()
    {
        return new EnrollmentCampaignComboDSHandler(getName());
    }
}
