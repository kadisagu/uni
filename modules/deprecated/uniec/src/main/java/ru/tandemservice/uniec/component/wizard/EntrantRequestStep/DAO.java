/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.wizard.EntrantRequestStep;

import java.util.List;

import org.tandemframework.core.info.ErrorCollector;

import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI;
import ru.tandemservice.uniec.entity.catalog.IndividualProgress;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineRequestedEnrollmentDirection;

/**
 * @author vip_delete
 * @since 16.06.2009
 */
public class DAO extends ru.tandemservice.uniec.component.entrant.EntrantRequestAddEdit.DAO implements IDAO
{
    @Override
    public void prepare(ru.tandemservice.uniec.component.entrant.EntrantRequestAddEdit.Model m)
    {
        super.prepare(m);

        Model model = (Model) m;

        model.setIndividualProgressModel(new LazySimpleSelectModel<>(IndividualProgress.class));
        model.setCustomStateModel(new LazySimpleSelectModel<>(EntrantCustomStateCI.class));

        // если есть онлайн-абитуриент
        Long onlineEntrantId = model.getOnlineEntrant().getId();
        if (onlineEntrantId != null)
        {
            OnlineEntrant onlineEntrant = get(OnlineEntrant.class, onlineEntrantId);

            model.setOnlineEntrant(onlineEntrant);
            model.getEntrantRequest().setMethodDeliveryDocs(onlineEntrant.getMethodDeliveryNReturnDocs());
            model.getEntrantRequest().setMethodReturnDocs(onlineEntrant.getMethodDeliveryNReturnDocs());

            prepareOnlineEntrantRequestedEnrollmentDirections(model);
        }
    }

    protected void fixDirectionList(Model model, List<OnlineRequestedEnrollmentDirection> onlineDirections)
    {
        // Метод для переопределения в проектном слое (используется в ДВФУ)
    }

    private void prepareOnlineEntrantRequestedEnrollmentDirections(Model model)
    {
        // берем выбранные направления онлайн-абитуриента
        List<OnlineRequestedEnrollmentDirection> onlineRequestedEnrollmentDirections = UniecDAOFacade.getOnlineEntrantRegistrationDAO().getRequestedEnrollmentDirections(model.getOnlineEntrant());
        if (onlineRequestedEnrollmentDirections.isEmpty())
        {
            return;
        }

        // если количество направлений на заявление не ограничено
        if (!model.isOneDirectionPerRequest())
        {
            // ошибки выбора проигнорируем, но объект нужен
            ErrorCollector errors = new ErrorCollector();

            // для всех выбранных им направлений
            for (OnlineRequestedEnrollmentDirection onlineRequestedEnrollmentDirection : onlineRequestedEnrollmentDirections)
            {
                // проставляем их параметры в модель
                bindOnlineRequestedEnrollmentDirection(onlineRequestedEnrollmentDirection, model);

                // эмулируем нажатие кнопки "выбрать"
                prepareSelectedEnrollmentDirectionList(model, errors);

                // Чистим ошибки, чтобы не блокировали добавление следующих направлений
                errors.clear();
            }

            // Метод для проектных кастомизаций, если надо список как-то пересортировать
            fixDirectionList(model, onlineRequestedEnrollmentDirections);

            // сбрасываем значения на форме
            reset(model);
        }
        // иначе если ограничено
        else
        {
            // должно быть выбрано одно
            if (onlineRequestedEnrollmentDirections.size() > 1)
            {
                // TODO кидать ошибку, писать в лог или ничего не делать?
            }
            else
            {
                // проставляем его на форму
                bindOnlineRequestedEnrollmentDirection(onlineRequestedEnrollmentDirections.get(0), model);
            }
        }
    }

    private void bindOnlineRequestedEnrollmentDirection(OnlineRequestedEnrollmentDirection onlineRequestedEnrollmentDirection, Model model)
    {
        RequestedEnrollmentDirection requestedEnrollmentDirection = model.getRequestedEnrollmentDirection();
        requestedEnrollmentDirection.setCompensationType(onlineRequestedEnrollmentDirection.getCompensationType());
        requestedEnrollmentDirection.setCompetitionKind(onlineRequestedEnrollmentDirection.getCompetitionKind());
        requestedEnrollmentDirection.setStudentCategory(onlineRequestedEnrollmentDirection.getStudentCategory());
        requestedEnrollmentDirection.setTargetAdmission(onlineRequestedEnrollmentDirection.isTargetAdmission());
        requestedEnrollmentDirection.setProfileWorkExperienceYears(onlineRequestedEnrollmentDirection.getProfileWorkExperienceYears());
        requestedEnrollmentDirection.setProfileWorkExperienceMonths(onlineRequestedEnrollmentDirection.getProfileWorkExperienceMonths());
        requestedEnrollmentDirection.setProfileWorkExperienceDays(onlineRequestedEnrollmentDirection.getProfileWorkExperienceDays());

        EnrollmentDirection enrollmentDirection = onlineRequestedEnrollmentDirection.getEnrollmentDirection();
        EducationOrgUnit educationOrgUnit = enrollmentDirection.getEducationOrgUnit();

        model.setEducationLevelsHighSchool(educationOrgUnit.getEducationLevelHighSchool());
        model.setFormativeOrgUnit(educationOrgUnit.getFormativeOrgUnit());
        model.setTerritorialOrgUnit(educationOrgUnit.getTerritorialOrgUnit());
        model.setDevelopForm(educationOrgUnit.getDevelopForm());
        model.setDevelopCondition(educationOrgUnit.getDevelopCondition());
        model.setDevelopTech(educationOrgUnit.getDevelopTech());
        model.setDevelopPeriod(educationOrgUnit.getDevelopPeriod());
    }

    @Override
    public void update(ru.tandemservice.uniec.component.entrant.EntrantRequestAddEdit.Model m)
    {
        super.update(m);

        Model model = (Model) m;
        Entrant entrant = model.getEntrantRequest().getEntrant();

        //Индивидуальные достижения абитуриента
        model.getIndividualProgressList()
                .forEach(progress -> {
                    EntrantIndividualProgress entrantIndividualProgress = new EntrantIndividualProgress();
                    entrantIndividualProgress.setEntrant(entrant);
                    entrantIndividualProgress.setIndividualProgressType(progress);
                    entrantIndividualProgress.setMark(progress.getMaxMark());
                    save(entrantIndividualProgress);
                });

        //Дополнительные статусы абитуриента
        model.getCustomStateList()
                .forEach(customState->{
                    EntrantCustomState entrantCustomState = new EntrantCustomState();
                    entrantCustomState.setEntrant(entrant);
                    entrantCustomState.setCustomState(customState);
                    save(entrantCustomState);
                });
    }
}
