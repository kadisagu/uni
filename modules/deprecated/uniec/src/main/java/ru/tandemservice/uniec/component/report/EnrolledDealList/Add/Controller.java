package ru.tandemservice.uniec.component.report.EnrolledDealList.Add;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;

public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickApply(IBusinessComponent component)
    {
        getDao().update(getModel(component));
        deactivate(component);
        activateInRoot(component, new PublisherActivator(getModel(component).getReport()));
    }

//    public void clearDisabledValues(IBusinessComponent component)
//    {
//        Model model = getModel(component);
//        if (!model.isCompensationTypeActive())
//            model.getReport().setCompensationType(null);
//        if (!model.isDevelopConditionActive())
//            model.setDevelopConditionList(null);
//        if (!model.isDevelopFormActive())
//            model.setDevelopFormList(null);
//        if (!model.isDevelopPeriodActive())
//            model.setDevelopPeriodList(null);
//        if (!model.isDevelopTechActive())
//            model.setDevelopTechList(null);
//        if (!model.isEnrollmentOrderActive())
//            model.setEnrollmentOrderList(null);
//        if (!model.isEntrantEnrollmentOrderTypeActive())
//            model.setEntrantEnrollmentOrderTypeList(null);
//        if (!model.isFormativeOrgUnitActive())
//            model.setFormativeOrgUnitList(null);
//        if (!model.isTerritorialOrgUnitActive())
//            model.setTerritorialOrgUnitList(null);
//        if (!model.isStudentCategoryActive())
//            model.setStudentCategoryList(null);
//        if (!model.isQualificationActive())
//            model.setQualificationList(null);
//        if (!model.isParallelActive())
//            model.setParallelList(null);
//    }
}
