/* $Id$ */
package ru.tandemservice.uniec.component.report.SummaryQuotasMarksResults.Add;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.Calendar;

/**
 * @author Vasily Zhukov
 * @since 18.08.2011
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.setPrincipalContext(UserContext.getInstance().getPrincipalContext());

        getDao().prepare(model);

        onChangeEnrollmentCampaign(component);
    }

    public void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().update(model);

        deactivate(component);

        activateInRoot(component, new PublisherActivator(model.getReport()));
    }

    public void onChangeEnrollmentCampaign(IBusinessComponent component)
    {
        Model model = getModel(component);

        EnrollmentCampaign enrollmentCampaign = model.getEnrollmentCampaign();

        if (enrollmentCampaign == null) return;

        Calendar c = Calendar.getInstance();

        c.set(CoreDateUtils.get(enrollmentCampaign.getStartDate(), Calendar.YEAR), Calendar.JULY, 20, 0, 0, 0);
        model.setEnroll3007from(c.getTime());

        c.set(Calendar.DATE, 30);
        model.setEnroll3007to(c.getTime());

        model.setEnroll3007title(DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getEnroll3007to()));

        c.set(Calendar.DATE, 31);
        model.setEnroll0508from(c.getTime());
        
        c.set(Calendar.DATE, 5);
        c.set(Calendar.MONTH, Calendar.AUGUST);
        model.setEnroll0508to(c.getTime());
        
        model.setEnroll0508title(DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getEnroll0508to()));

        c.set(Calendar.DATE, 6);
        model.setEnroll3108from(c.getTime());

        c.set(Calendar.DATE, 31);
        model.setEnroll3108to(c.getTime());
        
        model.setEnroll3108title(DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getEnroll3108to()));
    }
}
