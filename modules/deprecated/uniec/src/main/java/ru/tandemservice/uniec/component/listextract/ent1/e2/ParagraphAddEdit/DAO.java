/* $Id$ */
package ru.tandemservice.uniec.component.listextract.ent1.e2.ParagraphAddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.*;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.base.entity.ecgp.EcgpDistribution;
import ru.tandemservice.uniec.base.entity.ecgp.EcgpEntrantRecommended;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.SplitContractBachelorEntrantsStuListExtract;
import ru.tandemservice.uniec.entity.orders.SplitEntrantsStuListExtract;
import ru.tandemservice.unimove.UnimoveDefines;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 21.08.2012
 */
public class DAO extends AbstractListParagraphAddEditDAO<SplitContractBachelorEntrantsStuListExtract, Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        MQBuilder builder = new MQBuilder(EnrollmentCampaign.ENTITY_CLASS, "e");
        builder.addOrder("e", EnrollmentCampaign.P_ID, OrderDirection.desc);

        List<EnrollmentCampaign> list = builder.getResultList(getSession());
        if (model.getEnrollmentCampaign() == null && !list.isEmpty())
        {
            model.setEnrollmentCampaign(list.get(0));
        }
        model.setEnrollmentCampaignList(list);

        model.setFormativeOrgUnitsListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (null == model.getEnrollmentCampaign()) return ListResult.getEmpty();

                DQLSelectBuilder idDQL = new DQLSelectBuilder().fromEntity(EcgpDistribution.class, "d").predicate(DQLPredicateType.distinct)
                        .column(property(EcgpDistribution.config().ecgItem().educationOrgUnit().formativeOrgUnit().id().fromAlias("d")))
                        .where(eq(property(EcgpDistribution.config().ecgItem().enrollmentCampaign().fromAlias("d")), value(model.getEnrollmentCampaign())));

                if (!StringUtils.isEmpty(filter))
                    idDQL.where(like(DQLFunctions.upper(property(EcgpDistribution.config().ecgItem().educationOrgUnit().formativeOrgUnit().fullTitle().fromAlias("d"))), value(CoreStringUtils.escapeLike(filter))));

                final List<OrgUnit> resultList = new DQLSelectBuilder().fromEntity(OrgUnit.class, "u").column(property("u"))
                        .where(in(property(OrgUnit.id().fromAlias("u")), idDQL.buildQuery()))
                        .order(property(OrgUnit.fullTitle().fromAlias("u")))
                        .createStatement(getSession()).list();

                return new ListResult<>(resultList);
            }
        });

        model.setTerritorialOrgUnitsListModel(new FullCheckSelectModel(OrgUnit.P_TERRITORIAL_TITLE)
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (null == model.getEnrollmentCampaign()) return ListResult.getEmpty();

                DQLSelectBuilder idDQL = new DQLSelectBuilder().fromEntity(EcgpDistribution.class, "d").predicate(DQLPredicateType.distinct)
                        .column(property(EcgpDistribution.config().ecgItem().educationOrgUnit().territorialOrgUnit().id().fromAlias("d")))
                        .where(eq(property(EcgpDistribution.config().ecgItem().enrollmentCampaign().fromAlias("d")), value(model.getEnrollmentCampaign())));

                if (!StringUtils.isEmpty(filter))
                    idDQL.where(like(DQLFunctions.upper(property(EcgpDistribution.config().ecgItem().educationOrgUnit().territorialOrgUnit().territorialTitle().fromAlias("d"))), value(CoreStringUtils.escapeLike(filter))));

                final List<OrgUnit> resultList = new DQLSelectBuilder().fromEntity(OrgUnit.class, "u").column(property("u"))
                        .where(in(property(OrgUnit.id().fromAlias("u")), idDQL.buildQuery()))
                        .order(property(OrgUnit.fullTitle().fromAlias("u")))
                        .createStatement(getSession()).list();

                return new ListResult<>(resultList);
            }
        });

        model.setEcgpDistributionsListModel(new FullCheckSelectModel()
        {
            public List<EcgpDistribution> getFilteredList()
            {
                DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(SplitEntrantsStuListExtract.class, "se")
                        .column(property(SplitEntrantsStuListExtract.ecgpEntrantRecommended().distribution().id().fromAlias("se")))
                        .where(eq(property(SplitEntrantsStuListExtract.paragraph().order().id().fromAlias("se")), value(model.getOrderId())));

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EcgpDistribution.class, "d").column("d");
                if (null != model.getCompensationType())
                    builder.where(eq(property(EcgpDistribution.config().compensationType().fromAlias("d")), value(model.getCompensationType())));
                if (null != model.getFormativeOrgUnit())
                    builder.where(eq(property(EcgpDistribution.config().ecgItem().educationOrgUnit().formativeOrgUnit().fromAlias("d")), value(model.getFormativeOrgUnit())));
                if (null != model.getTerritorialOrgUnit())
                    builder.where(eq(property(EcgpDistribution.config().ecgItem().educationOrgUnit().territorialOrgUnit().fromAlias("d")), value(model.getTerritorialOrgUnit())));
                builder.where(eq(property(EcgpDistribution.config().ecgItem().enrollmentCampaign().fromAlias("d")), value(model.getEnrollmentCampaign())));
                builder.where(notIn(property(EcgpEntrantRecommended.id().fromAlias("d")), subBuilder.buildQuery()));
                return builder.createStatement(getSession()).list();
            }

            @Override
            public ListResult findValues(String filter)
            {
                if (null == model.getEnrollmentCampaign() || null == model.getCompensationType() || null == model.getFormativeOrgUnit() || null == model.getTerritorialOrgUnit())
                    return ListResult.getEmpty();

                List<EcgpDistribution> preResult = getFilteredList();
                filter = (filter != null) ? UniStringUtils.escapePattern(CoreStringUtils.escapeLike(filter.trim())).replaceAll("%", ".*") : "";
                List<EcgpDistribution> list = new ArrayList<>();
                Pattern pattern = Pattern.compile(filter);

                for (EcgpDistribution distrib : preResult)
                    if (filter.length() == 0 || pattern.matcher(distrib.getConfig().getEcgItem().getEducationOrgUnit().getEducationLevelHighSchool().getDisplayableShortTitle().toUpperCase()).find())
                        list.add(distrib);
                Collections.sort(list, new EntityComparator<>(new EntityOrder(EcgpDistribution.config().ecgItem().educationOrgUnit().educationLevelHighSchool().displayableTitle().s(), OrderDirection.asc)));
                return new ListResult<>(list.size() <= 50 ? list : list.subList(0, 50), list.size());
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                EcgpDistribution distrib = DataAccessServices.dao().get(EcgpDistribution.class, (Long) primaryKey);

                if (getFilteredList().contains(distrib))
                {
                    EducationOrgUnit eduOrgUnit = distrib.getConfig().getEcgItem().getEducationOrgUnit();
                    eduOrgUnit.getEducationLevelHighSchool().getDisplayableTitle(); // unproxy
                    eduOrgUnit.getDevelopForm().getShortTitle();
                    eduOrgUnit.getDevelopPeriod().getTitle();
                    getLabelFor(distrib, 0);
                    return distrib;
                }
                return null;
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                EducationOrgUnit eduOrgUnit = ((EcgpDistribution) value).getConfig().getEcgItem().getEducationOrgUnit();
                return eduOrgUnit.getEducationLevelHighSchool().getPrintTitle() + " - " + eduOrgUnit.getDevelopCombinationFullTitle();
            }
        });

        // заполняем поля по умолчанию
        model.setCompensationType(UniDaoFacade.getCoreDao().getCatalogItem(CompensationType.class, CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT));

        if (model.getParagraphId() != null)
        {
            SplitEntrantsStuListExtract extract = (SplitEntrantsStuListExtract) model.getParagraph().getFirstExtract();
            model.setEnrollmentCampaign(extract.getEnrollmentExtract().getOrder().getEnrollmentCampaign());
            model.setFormativeOrgUnit(extract.getEnrollmentExtract().getEntity().getEducationOrgUnit().getFormativeOrgUnit());
            model.setTerritorialOrgUnit(extract.getEnrollmentExtract().getEntity().getEducationOrgUnit().getTerritorialOrgUnit());
            model.setEcgpDistribution(extract.getEcgpEntrantRecommended().getDistribution());
            //TODO
        }
        else
        {
            model.setTerritorialOrgUnit(TopOrgUnit.getInstance());
        }
    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        // do nothing, because it won't be used at all (we've just overrided prepareListDataSource method
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        List<Student> list = new ArrayList<>();
        model.setEnrExtractsMap(new HashMap<Long, EnrollmentExtract>());
        model.setEntrantRecommendedMap(new HashMap<Long, EcgpEntrantRecommended>());

        if (null != model.getEnrollmentCampaign() && null != model.getEcgpDistribution())
        {
            DQLSelectBuilder splitEntrantsExtractsBuilder = new DQLSelectBuilder()
                    .fromEntity(SplitEntrantsStuListExtract.class, "se").column(property(SplitEntrantsStuListExtract.ecgpEntrantRecommended().preStudent().fromAlias("se")))
                    .where(eq(property(SplitEntrantsStuListExtract.ecgpEntrantRecommended().preStudent().requestedEnrollmentDirection().entrantRequest().entrant().enrollmentCampaign().fromAlias("se")), value(model.getEnrollmentCampaign())));
            if (null != model.getParagraphId())
                splitEntrantsExtractsBuilder.where(ne(property(SplitEntrantsStuListExtract.paragraph().id().fromAlias("se")), value(model.getParagraphId())));
            List<PreliminaryEnrollmentStudent> subList = splitEntrantsExtractsBuilder.createStatement(getSession()).list();

            DQLSelectBuilder enrolmExtractStudentsBuilder = new DQLSelectBuilder()
                    .fromEntity(EnrollmentExtract.class, "ee").column("ee")
                    .joinEntity("ee", DQLJoinType.inner, EcgpEntrantRecommended.class, "er", eq(property(EnrollmentExtract.entity().fromAlias("ee")), property(EcgpEntrantRecommended.preStudent().fromAlias("er")))).column("er")
                    .where(eq(property(EnrollmentExtract.entity().compensationType().fromAlias("ee")), value(model.getCompensationType())))
                    .where(eq(property(EnrollmentExtract.entity().requestedEnrollmentDirection().entrantRequest().entrant().enrollmentCampaign().fromAlias("ee")), value(model.getEnrollmentCampaign())))
                    .where(eq(property(EnrollmentExtract.paragraph().order().state().code().fromAlias("ee")), value(UnimoveDefines.CATALOG_ORDER_STATE_FINISHED)))
                    .where(isNotNull(property(EnrollmentExtract.studentNew().fromAlias("ee"))));

            if (!subList.isEmpty())
                enrolmExtractStudentsBuilder.where(notIn(property(EnrollmentExtract.entity().fromAlias("ee")), subList));

            if (null != model.getEcgpDistribution())
                enrolmExtractStudentsBuilder.where(eq(property(EnrollmentExtract.studentNew().educationOrgUnit().fromAlias("ee")), value(model.getEcgpDistribution().getConfig().getEcgItem().getEducationOrgUnit())));

            List<Object[]> enrExtractsList = enrolmExtractStudentsBuilder.createStatement(getSession()).list();
            for (Object[] item : enrExtractsList)
            {
                EnrollmentExtract enrExtract = (EnrollmentExtract) item[0];
                EcgpEntrantRecommended entrRecom = (EcgpEntrantRecommended) item[1];
                model.getEnrExtractsMap().put(enrExtract.getStudentNew().getId(), enrExtract);
                model.getEntrantRecommendedMap().put(enrExtract.getStudentNew().getId(), entrRecom);
                list.add(enrExtract.getStudentNew());
            }
        }

        model.getDataSource().setCountRow(list.size());
        UniBaseUtils.createPage(model.getDataSource(), list);

        if (list.size() == 0)
            return;

        List<Long> disabledStudentIds = getDisabledStudentIDs(model, UniBaseDao.ids(list));

        // дизаблим чекбоксы у студентов, которые уже присутствуют в других приказах
        for (ViewWrapper<IEntity> wrapper : ViewWrapper.getPatchedList(model.getDataSource()))
        {
            wrapper.setViewProperty(AbstractListParagraphAddEditModel.P_STUDENT_DISABLED, disabledStudentIds.contains(wrapper.getId()));
            if (null != model.getEnrExtractsMap().get(wrapper.getId()))
            {
                EnrollmentExtract enrExtract = model.getEnrExtractsMap().get(wrapper.getId());
                //wrapper.setViewProperty(Model.P_ENTRANT_DIRECTION, enrExtract.getEntity().getRequestedEnrollmentDirection().getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getDisplayableTitle());
                wrapper.setViewProperty(Model.P_ENROLLMENT_ORDER_DATE, enrExtract.getOrder().getCommitDate());
                wrapper.setViewProperty(Model.P_ENROLLMENT_ORDER_NUMBER, enrExtract.getOrder().getNumber());
                wrapper.setViewProperty(Model.P_ENROLLMENT_EXTRACT, enrExtract);
            }
            if (null != model.getEntrantRecommendedMap().get(wrapper.getId()))
            {
                Student student = (Student) wrapper.getEntity();
                EcgpEntrantRecommended entRecom = model.getEntrantRecommendedMap().get(wrapper.getId());
                wrapper.setViewProperty(Model.P_ENTRANT_DIRECTION, entRecom.getProfile().getEducationOrgUnit().getEducationLevelHighSchool().getDisplayableTitle());

                // В списке отключать возможность выбора для элементов, если: для студента НПП не совпадает с НПП направления приема распределения (т.е. студент уже зачислен, но его уже перевели с направления приема)
                if (!student.getEducationOrgUnit().equals(entRecom.getDistribution().getConfig().getEcgItem().getEducationOrgUnit()))
                    wrapper.setViewProperty(AbstractListParagraphAddEditModel.P_STUDENT_DISABLED, true);
            }
        }

        Collections.sort(model.getDataSource().getEntityList(), new EntityComparator<Student>(model.getDataSource().getEntityOrder()));
    }

    @Override
    protected SplitContractBachelorEntrantsStuListExtract createNewInstance(Model model)
    {
        return new SplitContractBachelorEntrantsStuListExtract();
    }

    @Override
    protected void fillExtract(SplitContractBachelorEntrantsStuListExtract extract, Student student, Model model)
    {
        extract.setEnrollmentExtract(model.getEnrExtractsMap().get(student.getId()));
        extract.setEcgpEntrantRecommended(model.getEntrantRecommendedMap().get(student.getId()));
        extract.setEducationOrgUnitNew(extract.getEcgpEntrantRecommended().getProfile().getEducationOrgUnit());
        extract.setEducationOrgUnitOld(student.getEducationOrgUnit());
    }
}