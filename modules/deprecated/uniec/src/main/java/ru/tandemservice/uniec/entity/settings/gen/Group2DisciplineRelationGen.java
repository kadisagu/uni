package ru.tandemservice.uniec.entity.settings.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.entity.settings.DisciplinesGroup;
import ru.tandemservice.uniec.entity.settings.Group2DisciplineRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь дисциплины и группы дисциплин набора вступительных испытаний
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class Group2DisciplineRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.settings.Group2DisciplineRelation";
    public static final String ENTITY_NAME = "group2DisciplineRelation";
    public static final int VERSION_HASH = 1894173157;
    private static IEntityMeta ENTITY_META;

    public static final String L_GROUP = "group";
    public static final String L_DISCIPLINE = "discipline";

    private DisciplinesGroup _group;     // Группа дисциплин набора вступительных испытаний
    private Discipline2RealizationWayRelation _discipline;     // Дисциплина набора вступительных испытаний

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Группа дисциплин набора вступительных испытаний. Свойство не может быть null.
     */
    @NotNull
    public DisciplinesGroup getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа дисциплин набора вступительных испытаний. Свойство не может быть null.
     */
    public void setGroup(DisciplinesGroup group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Дисциплина набора вступительных испытаний. Свойство не может быть null.
     */
    @NotNull
    public Discipline2RealizationWayRelation getDiscipline()
    {
        return _discipline;
    }

    /**
     * @param discipline Дисциплина набора вступительных испытаний. Свойство не может быть null.
     */
    public void setDiscipline(Discipline2RealizationWayRelation discipline)
    {
        dirty(_discipline, discipline);
        _discipline = discipline;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof Group2DisciplineRelationGen)
        {
            setGroup(((Group2DisciplineRelation)another).getGroup());
            setDiscipline(((Group2DisciplineRelation)another).getDiscipline());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends Group2DisciplineRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) Group2DisciplineRelation.class;
        }

        public T newInstance()
        {
            return (T) new Group2DisciplineRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "group":
                    return obj.getGroup();
                case "discipline":
                    return obj.getDiscipline();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "group":
                    obj.setGroup((DisciplinesGroup) value);
                    return;
                case "discipline":
                    obj.setDiscipline((Discipline2RealizationWayRelation) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "group":
                        return true;
                case "discipline":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "group":
                    return true;
                case "discipline":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "group":
                    return DisciplinesGroup.class;
                case "discipline":
                    return Discipline2RealizationWayRelation.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<Group2DisciplineRelation> _dslPath = new Path<Group2DisciplineRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "Group2DisciplineRelation");
    }
            

    /**
     * @return Группа дисциплин набора вступительных испытаний. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.Group2DisciplineRelation#getGroup()
     */
    public static DisciplinesGroup.Path<DisciplinesGroup> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Дисциплина набора вступительных испытаний. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.Group2DisciplineRelation#getDiscipline()
     */
    public static Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation> discipline()
    {
        return _dslPath.discipline();
    }

    public static class Path<E extends Group2DisciplineRelation> extends EntityPath<E>
    {
        private DisciplinesGroup.Path<DisciplinesGroup> _group;
        private Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation> _discipline;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Группа дисциплин набора вступительных испытаний. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.Group2DisciplineRelation#getGroup()
     */
        public DisciplinesGroup.Path<DisciplinesGroup> group()
        {
            if(_group == null )
                _group = new DisciplinesGroup.Path<DisciplinesGroup>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Дисциплина набора вступительных испытаний. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.Group2DisciplineRelation#getDiscipline()
     */
        public Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation> discipline()
        {
            if(_discipline == null )
                _discipline = new Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation>(L_DISCIPLINE, this);
            return _discipline;
        }

        public Class getEntityClass()
        {
            return Group2DisciplineRelation.class;
        }

        public String getEntityName()
        {
            return "group2DisciplineRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
