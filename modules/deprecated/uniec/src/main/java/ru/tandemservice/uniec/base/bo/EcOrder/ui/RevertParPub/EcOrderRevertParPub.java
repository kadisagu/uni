/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcOrder.ui.RevertParPub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniec.entity.orders.EnrollmentRevertExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentRevertParagraph;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 05.08.2013
 */
@Configuration
public class EcOrderRevertParPub extends BusinessComponentManager
{
    public static final String EXTRACT_DS = "extractDS";
    public static final String PARAGRAPH_PARAM = "paragraph";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(EXTRACT_DS, getExtractDS(), extractDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint getExtractDS()
    {
        return columnListExtPointBuilder(EXTRACT_DS)
                .addColumn(textColumn("fio", EnrollmentRevertExtract.entity().entrantRequest().entrant().person().identityCard().fullFio()))
                .addColumn(textColumn("sex", EnrollmentRevertExtract.entity().entrantRequest().entrant().person().identityCard().sex().shortTitle()))
                .addColumn(textColumn("passport", EnrollmentRevertExtract.entity().entrantRequest().entrant().person().identityCard().fullNumber()))
                .addColumn(textColumn("direction", EnrollmentRevertExtract.educationOrgUnit().title()))
                .addColumn(textColumn("formativeOrgUnit", EnrollmentRevertExtract.educationOrgUnit().formativeOrgUnit().title()))
                .addColumn(textColumn("territorialOrgUnit", EnrollmentRevertExtract.educationOrgUnit().territorialOrgUnit().title()))
                .addColumn(textColumn("developForm", EnrollmentRevertExtract.educationOrgUnit().developForm().title()))
                .addColumn(textColumn("developCondition", EnrollmentRevertExtract.educationOrgUnit().developCondition().title()))
                .addColumn(textColumn("developTech", EnrollmentRevertExtract.educationOrgUnit().developTech().title()))
                .addColumn(textColumn("developPeriod", EnrollmentRevertExtract.educationOrgUnit().developPeriod().title()))
                .addColumn(textColumn("course", EnrollmentRevertExtract.course().title()))
                .addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onClickPrintExtract").permissionKey("printExtract_enrollmentOrder"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER,
                                        alert(EXTRACT_DS + ".delete.alert", EnrollmentRevertExtract.entity().entrantRequest().entrant().person().identityCard().fullFio()))
                                   .permissionKey("deleteExtract_enrollmentOrder").disabled("ui:paragraph.order.readonly"))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> extractDSHandler()
    {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                EnrollmentRevertParagraph paragraph = context.get(PARAGRAPH_PARAM);
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrollmentRevertExtract.class, "e")
                        .where(eq(property(EnrollmentRevertExtract.paragraph().fromAlias("e")), value(paragraph)))
                        .order(property(EnrollmentRevertExtract.entity().entrantRequest().entrant().person().identityCard().fullFio().fromAlias("e")));

                return DQLSelectOutputBuilder.get(input, builder, context.getSession()).build();
            }
        };
    }
}