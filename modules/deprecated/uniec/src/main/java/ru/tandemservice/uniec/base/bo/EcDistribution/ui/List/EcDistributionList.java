/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uniec.base.bo.EcCampaign.EcCampaignManager;
import ru.tandemservice.uniec.base.bo.EcDistribution.EcDistributionManager;
import ru.tandemservice.uniec.base.bo.EcDistribution.logic.EcSimpleDSHandler;
import ru.tandemservice.uniec.base.bo.EcDistribution.ui.Pub.EcDistributionPub;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.EcgDistributionDTO;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistributionState;
import ru.tandemservice.uniec.base.entity.ecg.gen.IPersistentEcgItemGen;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import java.util.Arrays;

/**
 * @author Vasily Zhukov
 * @since 06.07.2011
 */
@Configuration
public class EcDistributionList extends BusinessComponentManager
{
    public static final String ENROLLMENT_CAMPAIGN_DS = "enrollmentCampaignDS";
    public static final String COMPENSATION_TYPE_DS = "compensationTypeDS";
    public static final String COMPETITION_GROUP_DS = "competitionGroupDS";
    public static final String DISTRIBUTION_CATEGORY_DS = "distributionCategoryDS";
    public static final String QUALIFICATION_DS = "qualificationDS";
    public static final String WAVE_DS = "waveDS";
    public static final String STATE_DS = "stateDS";

    public static final String DISTRIBUTION_CG_DS = "distributionCGDS"; // distribution for competition group
    public static final String DISTRIBUTION_ED_DS = "distributionEDDS"; // distribution for enrollment direction

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ENROLLMENT_CAMPAIGN_DS, EcCampaignManager.instance().enrollmentCampaignComboDSHandler()))
                .addDataSource(this.selectDS(COMPENSATION_TYPE_DS, compensationTypeComboDSHandler()).addColumn(CompensationType.P_SHORT_TITLE))
                .addDataSource(selectDS(DISTRIBUTION_CATEGORY_DS, distributionCategoryComboDSHandler()))
                .addDataSource(selectDS(QUALIFICATION_DS, qualificationDSHandler()))
                .addDataSource(selectDS(WAVE_DS, waveComboDSHandler()))
                .addDataSource(selectDS(STATE_DS, stateComboDSHandler()))
                .addDataSource(selectDS(COMPETITION_GROUP_DS, competitionGroupComboDSHandler()))
                .addDataSource(searchListDS(DISTRIBUTION_CG_DS, distributionCGDS(), distributionCGDSHandler()))
                .addDataSource(searchListDS(DISTRIBUTION_ED_DS, distributionEDDS(), distributionEDDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler compensationTypeComboDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), CompensationType.class, CompensationType.P_SHORT_TITLE);
    }

    @Bean
    public IDefaultComboDataSourceHandler distributionCategoryComboDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addAll(Arrays.asList(
                        EcDistributionManager.DISTRIBUTION_CATEGORY_STUDENTS_AND_LISTENERS,
                        EcDistributionManager.DISTRIBUTION_CATEGORY_SECOND_HIGH_ADMISSION
                ));
    }

    @Bean
    public IDefaultComboDataSourceHandler qualificationDSHandler()
    {
        DefaultComboDataSourceHandler handler = new DefaultComboDataSourceHandler(getName(), Qualifications.class)
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);

                ep.dqlBuilder.where(DQLExpressions.in(DQLExpressions.property(Qualifications.id().fromAlias("e")),
                                                      new DQLSelectBuilder().fromEntity(EducationLevelsHighSchool.class, "hs")
                                                              .column(DQLExpressions.property(EducationLevelsHighSchool.educationLevel().qualification().id().fromAlias("hs")))
                                                              .buildQuery()
                ));
            }
        };
        handler.setOrderByProperty(Qualifications.P_ORDER);
        return handler;
    }

    @Bean
    public IDefaultComboDataSourceHandler waveComboDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler stateComboDSHandler()
    {
        return (IDefaultComboDataSourceHandler) new DefaultComboDataSourceHandler(getName(), EcgDistributionState.class).setOrderByProperty(EcgDistributionState.P_CODE);
    }

    @Bean
    public IDefaultComboDataSourceHandler competitionGroupComboDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .filtered(true);
    }

    @Bean
    public ColumnListExtPoint distributionCGDS()
    {
        IMergeRowIdResolver ecgItemMergeResolver = getEcgItemMergeResolver();
        IMergeRowIdResolver compensationTypeMergeResolver = getCompensationTypeMergeResolver();

        return columnListExtPointBuilder(DISTRIBUTION_CG_DS)
                .addColumn(checkboxColumn("checkbox").create())
                .addColumn(textColumn("ecgItem", EcgDistributionDTO.ECG_ITEM + "." + IPersistentEcgItemGen.P_TITLE).width("5").merger(ecgItemMergeResolver).required(true).create())
                .addColumn(textColumn("compensationType", EcgDistributionDTO.COMPENSATION_TYPE + "." + CompensationType.P_SHORT_TITLE).merger(compensationTypeMergeResolver).required(true).create())
                .addColumn(indicatorColumn("status").path(EcgDistributionDTO.STATE_CODE)
                        .addIndicator(EcgDistributionDTO.STATUS_DISTRIBUTION_FORMING, new IndicatorColumn.Item("cg_distr_root_form", "формируется"))
                        .addIndicator(EcgDistributionDTO.STATUS_DISTRIBUTION_LOCKED, new IndicatorColumn.Item("cg_distr_root_appr", "зафиксировано"))
                        .addIndicator(EcgDistributionDTO.STATUS_DISTRIBUTION_APPROVED, new IndicatorColumn.Item("cg_distr_root_appr", "утверждено"))
                        .addIndicator(EcgDistributionDTO.STATUS_DISTRIBUTION_DETAIL_FORMING, new IndicatorColumn.Item("cg_distr_child_form", "уточняющее распределение (формируется)"))
                        .addIndicator(EcgDistributionDTO.STATUS_DISTRIBUTION_DETAIL_APPROVED, new IndicatorColumn.Item("cg_distr_child_appr", "уточняющее распределение (утверждено)"))
                        .required(true).create())
                .addColumn(publisherColumn("title", EcgDistributionDTO.TITLE).publisherLinkResolver(getPublisherLinkResolver()).required(true).create())
                .addColumn(blockColumn("quota", "quotaBlockColumn").width("10").create())
                .addColumn(blockColumn("taQuota", "taQuotaBlockColumn").width("10").create())
                .addColumn(dateColumn("formingDate", EcgDistributionDTO.FORMING_DATE).width("5").create())
                .addColumn(indicatorColumn("add").path(EcgDistributionDTO.ADD_DISABLED)
                        .addIndicator(Boolean.FALSE, new IndicatorColumn.Item(CommonDefines.ICON_ADD.getName(), CommonDefines.ICON_ADD.getLabel(), "onClickAdd")).clickable(true)
                        .required(true).permissionKey("distributionSectEdit").create())
                .addColumn(indicatorColumn("addDetail").path(EcgDistributionDTO.ADD_DETAIL_DISABLED)
                        .addIndicator(Boolean.FALSE, new IndicatorColumn.Item("clone", "Добавить уточняющее распределение", "onClickAddDetail")).clickable(true)
                        .required(true).permissionKey("distributionSectEdit").create())
                .addColumn(indicatorColumn("rtf").path(EcgDistributionDTO.DOWNLOAD_DISABLED)
                        .addIndicator(Boolean.FALSE, new IndicatorColumn.Item("printer", "Скачать Rtf", "onClickRtfPrint")).clickable(true)
                        .required(true).permissionKey("distributionSectPrint").create())
                .addColumn(indicatorColumn("xml").path(EcgDistributionDTO.DOWNLOAD_DISABLED)
                        .addIndicator(Boolean.FALSE, new IndicatorColumn.Item("save", "Скачать Xml", "onClickXmlPrint")).clickable(true)
                        .required(true).permissionKey("distributionSectPrint").create())
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).disabled(EcgDistributionDTO.EDIT_DISABLED).permissionKey("distributionSectEdit").create())
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER, alert("distributionCGDS.delete.alert", EcgDistributionDTO.TITLE)).disabled(EcgDistributionDTO.DELETE_DISABLED).permissionKey("distributionSectEdit").create())
                .create();
    }

    @Bean
    public ColumnListExtPoint distributionEDDS()
    {
        IMergeRowIdResolver ecgItemMergeResolver = getEcgItemMergeResolver();
        IMergeRowIdResolver compensationTypeMergeResolver = getCompensationTypeMergeResolver();

        return columnListExtPointBuilder(DISTRIBUTION_ED_DS)
                .addColumn(checkboxColumn("checkbox").create())
                .addColumn(textColumn("ecgItem", EcgDistributionDTO.ECG_ITEM + "." + IPersistentEcgItemGen.P_TITLE).merger(ecgItemMergeResolver).width("20").required(true).create())
                .addColumn(textColumn("formativeOrgUnit", EcgDistributionDTO.ECG_ITEM + "." + EnrollmentDirection.educationOrgUnit().formativeOrgUnit().shortTitle().s()).merger(ecgItemMergeResolver).create())
                .addColumn(textColumn("territorialOrgUnit", EcgDistributionDTO.ECG_ITEM + "." + EnrollmentDirection.educationOrgUnit().territorialOrgUnit().territorialShortTitle().s()).merger(ecgItemMergeResolver).width("10").create())
                .addColumn(textColumn("fctp", EcgDistributionDTO.ECG_ITEM + "." + EnrollmentDirection.educationOrgUnit().developCombinationTitle().s()).merger(ecgItemMergeResolver).create())
                .addColumn(textColumn("compensationType", EcgDistributionDTO.COMPENSATION_TYPE + "." + CompensationType.P_SHORT_TITLE).merger(compensationTypeMergeResolver).required(true).create())
                .addColumn(indicatorColumn("status").path(EcgDistributionDTO.STATE_CODE)
                        .addIndicator(EcgDistributionDTO.STATUS_DISTRIBUTION_FORMING, new IndicatorColumn.Item("cg_distr_root_form", "формируется"))
                        .addIndicator(EcgDistributionDTO.STATUS_DISTRIBUTION_LOCKED, new IndicatorColumn.Item("cg_distr_root_appr", "зафиксировано"))
                        .addIndicator(EcgDistributionDTO.STATUS_DISTRIBUTION_APPROVED, new IndicatorColumn.Item("cg_distr_root_appr", "утверждено"))
                        .required(true).create())
                .addColumn(publisherColumn("title", EcgDistributionDTO.TITLE).publisherLinkResolver(getPublisherLinkResolver()).required(true).create())
                .addColumn(blockColumn("quota", "quotaBlockColumn").width("10").create())
                .addColumn(blockColumn("taQuota", "taQuotaBlockColumn").width("10").create())
                .addColumn(dateColumn("formingDate", EcgDistributionDTO.FORMING_DATE).width("5").create())
                .addColumn(indicatorColumn("add").path(EcgDistributionDTO.ADD_DISABLED)
                        .addIndicator(Boolean.FALSE, new IndicatorColumn.Item(CommonDefines.ICON_ADD.getName(), CommonDefines.ICON_ADD.getLabel(), "onClickAdd")).clickable(true)
                        .required(true).permissionKey("distributionSectEdit").create())
                .addColumn(indicatorColumn("rtf").path(EcgDistributionDTO.DOWNLOAD_DISABLED)
                        .addIndicator(Boolean.FALSE, new IndicatorColumn.Item("printer", "Скачать Rtf", "onClickRtfPrint")).clickable(true)
                        .required(true).permissionKey("distributionSectPrint").create())
                .addColumn(indicatorColumn("xml").path(EcgDistributionDTO.DOWNLOAD_DISABLED)
                        .addIndicator(Boolean.FALSE, new IndicatorColumn.Item("save", "Скачать Xml", "onClickXmlPrint")).clickable(true)
                        .required(true).permissionKey("distributionSectPrint").create())
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).disabled(EcgDistributionDTO.EDIT_DISABLED).permissionKey("distributionSectEdit").create())
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER, alert("distributionEDDS.delete.alert", EcgDistributionDTO.TITLE)).disabled(EcgDistributionDTO.DELETE_DISABLED).permissionKey("distributionSectEdit").create())
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> distributionCGDSHandler()
    {
        return new EcSimpleDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> distributionEDDSHandler()
    {
        return new EcSimpleDSHandler(getName());
    }

    private static IMergeRowIdResolver getEcgItemMergeResolver()
    {
        return entity -> ((EcgDistributionDTO) entity).getEcgItem().getId().toString();
    }

    private static IMergeRowIdResolver getCompensationTypeMergeResolver()
    {
        return entity -> ((EcgDistributionDTO) entity).getEcgItem().getId().toString() + "_" + ((EcgDistributionDTO) entity).getCompensationType().getId().toString();
    }

    private static IPublisherLinkResolver getPublisherLinkResolver()
    {
        return new DefaultPublisherLinkResolver()
        {
            @Override
            public String getComponentName(IEntity entity)
            {
                return EcDistributionPub.class.getSimpleName();
            }

            @Override
            public Object getParameters(IEntity entity)
            {
                return ((EcgDistributionDTO) entity).getPersistentId();
            }
        };
    }
}
