/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcOrder.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniec.dao.IPrintableEnrollmentExtract;
import ru.tandemservice.uniec.entity.catalog.EnrollmentOrderBasic;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.orders.*;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * API для работы с движением абитуриентов
 *
 * @author Vasily Zhukov
 * @since 24.05.2011
 */
public interface IEcOrderDao extends INeedPersistenceSupport
{
    /**
     * Удаляет приказ о зачислении. всем выбранным направлениям приема из выписок приказа ставится состояние "Предзачислен"
     *
     * @param order приказ о зачислении
     */
    void deleteOrder(EnrollmentOrder order);

    /**
     * Удаляет параграф о зачислении. всем выбранным направлениям приема из выписок параграфа ставится состояние "Предзачислен"
     *
     * @param paragraph параграф о зачислении
     */
    void deleteParagraph(EnrollmentParagraph paragraph);

    /**
     * Удаляет выписку о зачислении, при этом абитуриент остается предзачисленным.
     *
     * @param extract выписка о зачислении
     */
    void deleteExtract(EnrollmentExtract extract);

    /**
     * Аннулирует выписку о зачислении, при этом абитуриент больше не будет считаться предварительно зачисленным
     * Если в параграфе эта выписка была последней, то параграф удаляется
     *
     * @param extract выписка о зачислении
     */
    void executeAnnulExtract(EnrollmentExtract extract);

    /**
     * Получает шаблон приказа
     *
     * @param orderType тип приказа о зачислении
     * @return rtf шаблон приказа о зачислении
     */
    byte[] getOrderTemplate(EntrantEnrollmentOrderType orderType);

    /**
     * Получает шаблон параграфа приказа
     *
     * @param orderType тип приказа о зачислении
     * @return rtf шаблон параграфа о зачислении
     */
    byte[] getParagraphTemplate(EntrantEnrollmentOrderType orderType);

    /**
     * Получает типа приказа в приемной кампании
     *
     * @param enrollmentCampaign приемная кампания
     * @param type               тип приказа о зачислении
     * @return тип приказа в приемной кампании
     */
    EnrollmentOrderType getEnrollmentOrderType(EnrollmentCampaign enrollmentCampaign, EntrantEnrollmentOrderType type);

    /**
     * Получает используемые типы приказов в указанной приемной кампании
     *
     * @param enrollmentCampaign приемная кампания
     * @return список типов приказов о зачислении
     */
    List<EntrantEnrollmentOrderType> getOrderTypeList(EnrollmentCampaign enrollmentCampaign);

    /**
     * Сохраняет печатную форму приказа о зачислении
     *
     * @param order приказ о зачислении
     */
    void saveEnrollmentOrderText(EnrollmentOrder order);

    /**
     * Проверяет на уникальность номер приказа в рамках текущего календарного года
     *
     * @param order абитуриентский приказ
     */
    void checkOrderNumber(AbstractEntrantOrder order);

    /**
     * Сохраняет или обновляет параграф о зачислении абитуриентов
     *
     * @param order                        приказ о зачислении
     * @param paragraph                    параграф о зачислении, который следует обновить либо null, если требуется создать параграф
     * @param extractFactory               фабрика создания выписки
     * @param enrollParagraphPerEduOrgUnit true, если параграф о зачислении формируется на направление подготовки (специальность)
     * @param preStudents                  студенты предзачисления, из которых должен состоять параграф о зачислении
     * @param manager                      староста группы либо null, если старосты нет
     * @param formativeOrgUnit             формирующее подразделение на форме создания/редактирования (нужно только для валидаций)
     * @param educationLevelsHighSchool    направление подготовки (специальность) на форме создания/редактирования (нужно только для валидаций)
     * @param developForm                  форма освоения на форме создания/редактирования (нужно только для валидаций)
     * @param course                       курс, на который будут зачисляться студенты
     * @param groupTitle                   название студенческой группы, в которую будут зачисляться студенты
     */
    void saveOrUpdateEnrollmentParagraph(EnrollmentOrder order, EnrollmentParagraph paragraph, IEnrollmentExtractFactory extractFactory, boolean enrollParagraphPerEduOrgUnit, Collection<PreliminaryEnrollmentStudent> preStudents, PreliminaryEnrollmentStudent manager, OrgUnit formativeOrgUnit, EducationLevelsHighSchool educationLevelsHighSchool, DevelopForm developForm, Course course, String groupTitle);

    /**
     * Получение элемента справочника со скриптом для печати приказа
     *
     * @param order приказ
     * @return элемент справочника
     */
    IScriptItem getPrintOrderScript(EnrollmentOrder order);

    /**
     * Получение элемента справочника со скриптом печати для выписки
     *
     * @param extract выписка
     * @return элемент справочника
     */
    IScriptItem getPrintExtractScript(IPrintableEnrollmentExtract extract);

    /**
     * Приказ, напечатанный скриптом
     *
     * @param enrollmentOrder приказ
     * @return печатная форма
     */
    byte[] getOrderPrintFormCreatedByScript(EnrollmentOrder enrollmentOrder);

    /**
     * Скачивает печатную форму приказа о зачислении абитуриентов
     *
     * @param enrollmentOrderId идентификатор приказа о зачислении
     * @param printToPdf признак печати в формате PDF
     */
    void getDownloadPrintForm(Long enrollmentOrderId, boolean printToPdf);

    /**
     * Генерирурет печатную форму выписки из приказа о зачислении абитуриентов
     *
     * @param enrollmentExtractId идентификатор выписки
     */
    byte[] getExtractPrintForm(Long enrollmentExtractId);
    RtfDocument getExtractDocument(IPrintableEnrollmentExtract extract);
    RtfDocument getExtractDocument(IPrintableEnrollmentExtract extract, IScriptItem scriptItem, RtfDocument template);

    /**
     * Скачивает печатную форму выписки из приказа о зачислении абитуриентов
     *
     * @param enrollmentExtractId идентификатор выписки
     */
    void getDownloadExtractPrintForm(Long enrollmentExtractId, boolean printPdf);

    /**
     * Массовая печать выписок из приказа о зачислении
     *
     * @param orderId идентификатор приказа о зачислении
     */
    void getMassExtractPrintForms(Long orderId);

    /**
     * Массовая печать экзаменационных листов абитуриентов из приказа о зачислении
     *
     * @param orderId идентификатор приказа о зачислении
     */
    void getMassExamSheetPrintForms(Long orderId);

    /**
     * Возвращает список всех выписок из приказа отсортированных в некотором порядке
     *
     * @param orderId идентификатор приказа о зачислении
     * @return список выписок
     */
    List<AbstractEntrantExtract> getExtractList(Long orderId);

    /**
     * Создание/сохранени приказа
     *
     * @param order приказ
     * @param executor исполнитель
     * @param basic основание
     */
    void saveOrUpdateOrder(EnrollmentOrder order, EmployeePost executor, EnrollmentOrderBasic basic);

    /**
     * Отправить приказ на согласование
     *
     * @param order приказ
     * @param initiator инициатор
     */
    void doSendToCoordination(EnrollmentOrder order, IPrincipalContext initiator);

    /**
     * Отправить приказ на формирование
     *
     * @param order приказ
     */
    void doSendToFormative(EnrollmentOrder order);

    /**
     * Отклонить приказ
     *
     * @param order приказ
     */
    void doReject(EnrollmentOrder order);

    /**
     * Провести приказ
     *
     * @param order приказ
     */
    void doCommit(EnrollmentOrder order);

    /**
     * Откат приказа
     *
     * @param order приказ
     */
    void doRollback(EnrollmentOrder order);

    /**
     * Удалить параграф от омене
     *
     * @param paragraphId идентификатор параграфа
     */
    void deleteRevertParagraph(Long paragraphId);

    /**
     * Изменение порядка параграфов в приказе
     *
     * @param paragraphId идентификатор параграфа
     * @param direction направление (вверх переместить или вниз)
     */
    void updatePriority(Long paragraphId, int direction);

    /**
     * Создание или обновление параграфа приказа об изменении приказа о зачислении
     *
     * @param paragraph параграф
     * @param canceledExtracts список отменяемых выписок из приказа о зачислении
     */
    void saveOrUpdateEnrollmentRevertParagraph(EnrollmentRevertParagraph paragraph, Set<EnrollmentExtract> canceledExtracts);

    /**
     * Удаление выписки об изменении
     *
     * @param extractId выписка
     */
    void deleteRevertExtract(Long extractId);

    /**
     *
     */
}
