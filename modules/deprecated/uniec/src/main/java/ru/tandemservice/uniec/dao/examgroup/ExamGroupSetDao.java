/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.dao.examgroup;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.util.EntrantDataUtil;

import java.util.*;

/**
 * @author Vasily Zhukov
 * @since 27.05.2010
 */
public class ExamGroupSetDao extends UniBaseDao implements IExamGroupSetDao
{
    @Override
    public IAbstractExamGroupLogic getCurrentExamGroupLogic(EnrollmentCampaign enrollmentCampaign)
    {
        Criteria c = getSession().createCriteria(CurrentExamGroupLogic.class);
        c.add(Restrictions.eq(CurrentExamGroupLogic.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign));
        CurrentExamGroupLogic logic = (CurrentExamGroupLogic) c.uniqueResult();
        return logic == null ? null : (IAbstractExamGroupLogic) ApplicationRuntime.getBean(logic.getExamGroupLogic().getDaoName());
    }

    @Override
    public boolean isCanChangeExamGroupLogic(EnrollmentCampaign enrollmentCampaign)
    {
        // изменять алгоритм можно тогда и только тогда, когда нет ни одной экзам группы
        return new DQLSelectBuilder().fromEntity(ExamGroup.class, "e")
        .where(DQLExpressions.eq(DQLExpressions.property(ExamGroup.examGroupSet().enrollmentCampaign().fromAlias("e")), DQLExpressions.value(enrollmentCampaign)))
        .createStatement(getSession()).setMaxResults(1).uniqueResult() == null;
    }

    @Override
    public void openExamGroupSet(ExamGroupSet examGroupSet)
    {
        Session session = getSession();
        ExamGroupSetOpened opened = (ExamGroupSetOpened) session.createCriteria(ExamGroupSetOpened.class).add(Restrictions.eq(ExamGroupSetOpened.L_ENROLLMENT_CAMPAIGN, examGroupSet.getEnrollmentCampaign())).uniqueResult();
        if (opened != null && opened.getExamGroupSet().equals(examGroupSet)) return; // этот набор уже открыт

        if (opened != null)
            closeExamGroupSet(opened.getExamGroupSet()); // закрываем предыдущий набор

        // открываем указанный
        opened = new ExamGroupSetOpened();
        opened.setEnrollmentCampaign(examGroupSet.getEnrollmentCampaign());
        opened.setExamGroupSet(examGroupSet);
        session.save(opened);
    }

    @Override
    public void closeExamGroupSet(ExamGroupSet examGroupSet)
    {
        Session session = getSession();
        ExamGroupSetOpened opened = (ExamGroupSetOpened) session.createCriteria(ExamGroupSetOpened.class).add(Restrictions.eq(ExamGroupSetOpened.L_ENROLLMENT_CAMPAIGN, examGroupSet.getEnrollmentCampaign())).uniqueResult();
        if (opened != null && opened.getExamGroupSet().equals(examGroupSet))
            session.delete(opened);
        // делаем flush чтобы запросы удаления сразу попали в очередь выполнения, иначе может возникнуть ошибка уникальности
        session.flush();
    }

    @Override
    public List<ExamGroup> getPossibleExamGroups(EntrantRequest entrantRequest)
    {
        Session session = getSession();

        // если заявление уже куда-нибудь включено, то больше никуда его включать нельзя
        Number number = (Number) getSession().createCriteria(ExamGroupRow.class)
        .add(Restrictions.eq(ExamGroupRow.L_ENTRANT_REQUEST, entrantRequest))
        .setProjection(Projections.rowCount())
        .uniqueResult();

        if (number != null && number.intValue() > 0) return Collections.emptyList();

        // получаем открытый набор
        ExamGroupSet openedSet = (ExamGroupSet) new MQBuilder(ExamGroupSetOpened.ENTITY_CLASS, "s", new String[]{ExamGroupSetOpened.L_EXAM_GROUP_SET})
        .add(MQExpression.eq("s", ExamGroupSetOpened.L_ENROLLMENT_CAMPAIGN, entrantRequest.getEntrant().getEnrollmentCampaign()))
        .uniqueResult(session);

        // нет открытого набора, нельзя никуда включать
        if (openedSet == null) return Collections.emptyList();

        // если пересечение A и B пусто, то нельзя добавлять в экзаменационную группу, т.е. смысла в добавлении нет никакого
        if (!hasIntersectionAB(session, entrantRequest)) return Collections.emptyList();

        // получаем текущую логику работы экзаменационных групп
        IAbstractExamGroupLogic logic = getCurrentExamGroupLogic(entrantRequest.getEntrant().getEnrollmentCampaign());

        // нет текущего алгоритма, нельзя никуда включить
        if (logic == null) return Collections.emptyList();

        // получаем ключи групп
        List<IExamGroupInfo> infos = logic.getExamGroupInfo(entrantRequest);

        // нет ключей - нет экзаменационных групп
        if (infos.isEmpty()) return Collections.emptyList();

        List<ExamGroup> resultList = new ArrayList<ExamGroup>();

        // для каждого ключа находим группу c максимальным индексом в открытом наборе
        for (IExamGroupInfo info : infos)
        {
            ExamGroup group = (ExamGroup) getSession().createCriteria(ExamGroup.class)
            .add(Restrictions.eq(ExamGroup.L_EXAM_GROUP_SET, openedSet))
            .add(Restrictions.eq(ExamGroup.P_KEY, info.getKey()))
            .addOrder(Order.desc(ExamGroup.P_INDEX))
            .setMaxResults(1)
            .uniqueResult();

            if (group != null)
            {
                // если группа существует, то считаем количество строк в группе
                Number groupSizeNumber = (Number) getSession().createCriteria(ExamGroupRow.class)
                .add(Restrictions.eq(ExamGroupRow.L_EXAM_GROUP, group))
                .setProjection(Projections.rowCount())
                .uniqueResult();

                if (groupSizeNumber.intValue() < openedSet.getSize())
                {
                    // если группа не заполнена, то используем ее
                } else
                {
                    // иначе создаем группу со следующим индексом
                    int newIndex = group.getIndex() + 1;
                    String newIndexStr = Integer.toString(newIndex);

                    // группа заполнена, следует создать новую группу со следущим индексом в рамках ключа
                    group = new ExamGroup();
                    group.setExamGroupSet(openedSet);
                    group.setKey(info.getKey());
                    group.setIndex(newIndex);
                    group.setCreateDate(new Date());
                    group.setTitle(info.getTitle().replace("${index}", newIndexStr));
                    group.setShortTitle(info.getShortTitle().replace("${index}", newIndexStr));
                }
            } else
            {
                // групп с таким ключем еще нет в открытом наборе, следуем создано группу с первым индексом
                group = new ExamGroup();
                group.setExamGroupSet(openedSet);
                group.setKey(info.getKey());
                group.setIndex(1);
                group.setCreateDate(new Date());
                group.setTitle(info.getTitle().replace("${index}", "1"));
                group.setShortTitle(info.getShortTitle().replace("${index}", "1"));
            }

            resultList.add(group);
        }

        return resultList;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<ExamGroup> getEntrantRequestGroups(EntrantRequest entrantRequest)
    {
        Session session = getSession();

        // получаем все группы, в которых находится это заявление
        Criteria c = session.createCriteria(ExamGroupRow.class);
        c.add(Restrictions.eq(ExamGroupRow.L_ENTRANT_REQUEST, entrantRequest));
        c.setProjection(Projections.property(ExamGroupRow.L_EXAM_GROUP));
        List<ExamGroup> examGroupList = c.list();

        // заявления нет ни в одной группе
        if (examGroupList.isEmpty()) return Collections.emptyList();

        // получаем набор в котором содержится группа
        ExamGroupSet examGroupSet = examGroupList.get(0).getExamGroupSet();

        // проверяем консистентность базы
        // заявление не может находиться в нескольких наборах одновременно
        for (ExamGroup examGroup : examGroupList)
            if (!examGroup.getExamGroupSet().equals(examGroupSet))
                throw new RuntimeException("EntrantRequest '" + entrantRequest.getId() + "' is included in different examGroupSet!");

        return examGroupList;
    }

    @Override
    public boolean isExamGroupRowValid(ExamGroupRow row)
    {
        EntrantRequest entrantRequest = row.getEntrantRequest();

        // если пересечение A и B пусто, то абитуриент не должен находиться в группе
        if (!hasIntersectionAB(getSession(), entrantRequest)) return false;

        // получаем текущую логику работы экзаменационных групп
        IAbstractExamGroupLogic logic = getCurrentExamGroupLogic(entrantRequest.getEntrant().getEnrollmentCampaign());

        // нет алгоритма, не должно быть и самой группы
        if (logic == null) return false;

        // получаем ключи групп
        List<IExamGroupInfo> infos = logic.getExamGroupInfo(entrantRequest);

        // получаем ключ строки группы
        String key = row.getExamGroup().getKey();

        // среди ключей должен содержаться ключ группы row
        int i = 0;
        while (i < infos.size() && !infos.get(i).getKey().equals(key)) i++;
        return i < infos.size();
    }

    @Override
    public Map<PairKey<Discipline2RealizationWayRelation, SubjectPassForm>, Set<RequestedEnrollmentDirection>> getExamPassDisciplineList(ExamGroup examGroup)
    {
        List<EntrantRequest> entrantRequestList = getExamGroupEntrantRequests(examGroup);
        MQBuilder directionBuilder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "d")
        .add(MQExpression.in("d", RequestedEnrollmentDirection.L_ENTRANT_REQUEST, entrantRequestList))
        .add(MQExpression.notEq("d", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY))
        .add(MQExpression.notEq("d", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE))
        .add(MQExpression.eq("d", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, Boolean.FALSE));

        EntrantDataUtil dataUtil = new EntrantDataUtil(getSession(), examGroup.getExamGroupSet().getEnrollmentCampaign(), directionBuilder);

        Map<PairKey<Discipline2RealizationWayRelation, SubjectPassForm>, Set<RequestedEnrollmentDirection>> result = new TreeMap<PairKey<Discipline2RealizationWayRelation, SubjectPassForm>, Set<RequestedEnrollmentDirection>>(new Comparator<PairKey<Discipline2RealizationWayRelation, SubjectPassForm>>()
        {
            @Override
            public int compare(PairKey<Discipline2RealizationWayRelation, SubjectPassForm> o1, PairKey<Discipline2RealizationWayRelation, SubjectPassForm> o2)
            {
                int result = o1.getFirst().getTitle().compareTo(o2.getFirst().getTitle());
                if (result != 0) return result;
                return o1.getSecond().getCode().compareTo(o2.getSecond().getCode());
            }
        });

        for (RequestedEnrollmentDirection direction : dataUtil.getDirectionSet())
            for (ChosenEntranceDiscipline chosen : dataUtil.getChosenEntranceDisciplineSet(direction))
                for (ExamPassDiscipline examPassDiscipline : dataUtil.getExamPassDisciplineSet(chosen))
                {
                    SubjectPassForm subjectPassForm = examPassDiscipline.getSubjectPassForm();
                    if (subjectPassForm.isInternal() && !UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM_INTERNAL.equals(subjectPassForm.getCode()))
                    {
                        PairKey<Discipline2RealizationWayRelation, SubjectPassForm> key = PairKey.create(examPassDiscipline.getEnrollmentCampaignDiscipline(), examPassDiscipline.getSubjectPassForm());
                        Set<RequestedEnrollmentDirection> set = result.get(key);
                        if (set == null)
                            result.put(key, set = new HashSet<RequestedEnrollmentDirection>());
                        set.add(direction);
                    }
                }

        return result;
    }

    protected List<EntrantRequest> getExamGroupEntrantRequests(ExamGroup examGroup)
    {
        Session session = getSession();
        List<EntrantRequest> entrantRequestList = new ArrayList<EntrantRequest>();
        for (ExamGroupRow row : new MQBuilder(ExamGroupRow.ENTITY_CLASS, "r").add(MQExpression.eq("r", ExamGroupRow.L_EXAM_GROUP, examGroup)).<ExamGroupRow>getResultList(session))
            if (isExamGroupRowValid(row))
                entrantRequestList.add(row.getEntrantRequest());
        return entrantRequestList;
    }

    private static boolean hasIntersectionAB(Session session, EntrantRequest entrantRequest)
    {
        MQBuilder directionBuilder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "d")
        .add(MQExpression.eq("d", RequestedEnrollmentDirection.L_ENTRANT_REQUEST, entrantRequest));

        EntrantDataUtil dataUtil = new EntrantDataUtil(session, entrantRequest.getEntrant().getEnrollmentCampaign(), directionBuilder);

        Set<Discipline2RealizationWayRelation> a = new HashSet<Discipline2RealizationWayRelation>();
        Set<Discipline2RealizationWayRelation> b = new HashSet<Discipline2RealizationWayRelation>();

        for (RequestedEnrollmentDirection direction : dataUtil.getDirectionSet())
        {
            for (ChosenEntranceDiscipline chosen : dataUtil.getChosenEntranceDisciplineSet(direction))
            {
                a.add(chosen.getEnrollmentCampaignDiscipline());
                for (ExamPassDiscipline examPass : dataUtil.getExamPassDisciplineSet(chosen))
                    if (examPass.getSubjectPassForm().isInternal() && !UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM_INTERNAL.equals(examPass.getSubjectPassForm().getCode()))
                        b.add(examPass.getEnrollmentCampaignDiscipline());
            }
        }

        // true, если есть пересечение мн-в
        return CollectionUtils.containsAny(a, b);
    }
}
