package ru.tandemservice.uniec.entity.catalog;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.catalog.IColoredEntity;
import ru.tandemservice.uniec.entity.catalog.gen.*;

/**
 * Дополнительный статус абитуриента(Справочник)
 */
public class EntrantCustomStateCI extends EntrantCustomStateCIGen implements IColoredEntity, ITitled
{

    public static EntityComboDataSourceHandler defaultSelectDSHandler(String name)
    {
        return new EntityComboDataSourceHandler(name, EntrantCustomStateCI.class)
                .order(EntrantCustomStateCI.title())
                .filter(EntrantCustomStateCI.title());
    }
}