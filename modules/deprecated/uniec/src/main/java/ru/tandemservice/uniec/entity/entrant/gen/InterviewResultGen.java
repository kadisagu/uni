package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.entrant.InterviewResult;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Результат собеседования
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class InterviewResultGen extends VersionedEntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.InterviewResult";
    public static final String ENTITY_NAME = "interviewResult";
    public static final int VERSION_HASH = 886704398;
    private static IEntityMeta ENTITY_META;

    public static final String P_VERSION = "version";
    public static final String L_REQUESTED_ENROLLMENT_DIRECTION = "requestedEnrollmentDirection";
    public static final String P_PASSED = "passed";

    private int _version; 
    private RequestedEnrollmentDirection _requestedEnrollmentDirection;     // Выбранное направление приема
    private boolean _passed;     // Пройдено

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public int getVersion()
    {
        return _version;
    }

    /**
     * @param version  Свойство не может быть null.
     */
    public void setVersion(int version)
    {
        dirty(_version, version);
        _version = version;
    }

    /**
     * @return Выбранное направление приема. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public RequestedEnrollmentDirection getRequestedEnrollmentDirection()
    {
        return _requestedEnrollmentDirection;
    }

    /**
     * @param requestedEnrollmentDirection Выбранное направление приема. Свойство не может быть null и должно быть уникальным.
     */
    public void setRequestedEnrollmentDirection(RequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        dirty(_requestedEnrollmentDirection, requestedEnrollmentDirection);
        _requestedEnrollmentDirection = requestedEnrollmentDirection;
    }

    /**
     * @return Пройдено. Свойство не может быть null.
     */
    @NotNull
    public boolean isPassed()
    {
        return _passed;
    }

    /**
     * @param passed Пройдено. Свойство не может быть null.
     */
    public void setPassed(boolean passed)
    {
        dirty(_passed, passed);
        _passed = passed;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof InterviewResultGen)
        {
            setVersion(((InterviewResult)another).getVersion());
            setRequestedEnrollmentDirection(((InterviewResult)another).getRequestedEnrollmentDirection());
            setPassed(((InterviewResult)another).isPassed());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends InterviewResultGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) InterviewResult.class;
        }

        public T newInstance()
        {
            return (T) new InterviewResult();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "version":
                    return obj.getVersion();
                case "requestedEnrollmentDirection":
                    return obj.getRequestedEnrollmentDirection();
                case "passed":
                    return obj.isPassed();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "version":
                    obj.setVersion((Integer) value);
                    return;
                case "requestedEnrollmentDirection":
                    obj.setRequestedEnrollmentDirection((RequestedEnrollmentDirection) value);
                    return;
                case "passed":
                    obj.setPassed((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "version":
                        return true;
                case "requestedEnrollmentDirection":
                        return true;
                case "passed":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "version":
                    return true;
                case "requestedEnrollmentDirection":
                    return true;
                case "passed":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "version":
                    return Integer.class;
                case "requestedEnrollmentDirection":
                    return RequestedEnrollmentDirection.class;
                case "passed":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<InterviewResult> _dslPath = new Path<InterviewResult>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "InterviewResult");
    }
            

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.InterviewResult#getVersion()
     */
    public static PropertyPath<Integer> version()
    {
        return _dslPath.version();
    }

    /**
     * @return Выбранное направление приема. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.entrant.InterviewResult#getRequestedEnrollmentDirection()
     */
    public static RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> requestedEnrollmentDirection()
    {
        return _dslPath.requestedEnrollmentDirection();
    }

    /**
     * @return Пройдено. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.InterviewResult#isPassed()
     */
    public static PropertyPath<Boolean> passed()
    {
        return _dslPath.passed();
    }

    public static class Path<E extends InterviewResult> extends EntityPath<E>
    {
        private PropertyPath<Integer> _version;
        private RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> _requestedEnrollmentDirection;
        private PropertyPath<Boolean> _passed;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.InterviewResult#getVersion()
     */
        public PropertyPath<Integer> version()
        {
            if(_version == null )
                _version = new PropertyPath<Integer>(InterviewResultGen.P_VERSION, this);
            return _version;
        }

    /**
     * @return Выбранное направление приема. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.entrant.InterviewResult#getRequestedEnrollmentDirection()
     */
        public RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> requestedEnrollmentDirection()
        {
            if(_requestedEnrollmentDirection == null )
                _requestedEnrollmentDirection = new RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection>(L_REQUESTED_ENROLLMENT_DIRECTION, this);
            return _requestedEnrollmentDirection;
        }

    /**
     * @return Пройдено. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.InterviewResult#isPassed()
     */
        public PropertyPath<Boolean> passed()
        {
            if(_passed == null )
                _passed = new PropertyPath<Boolean>(InterviewResultGen.P_PASSED, this);
            return _passed;
        }

        public Class getEntityClass()
        {
            return InterviewResult.class;
        }

        public String getEntityName()
        {
            return "interviewResult";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
