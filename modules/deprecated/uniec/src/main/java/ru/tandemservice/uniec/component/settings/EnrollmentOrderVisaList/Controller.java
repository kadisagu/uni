/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.settings.EnrollmentOrderVisaList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDefine;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.IUniecComponents;
import ru.tandemservice.uniec.entity.settings.EnrollmentOrderVisaItem;

/**
 * @author vip_delete
 * @since 28.07.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.setSettings(component.getSettings());

        getDao().prepare(model);

        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null) return;

        IMergeRowIdResolver mergeRowIdResolver = entity -> ((EnrollmentOrderVisaItem) entity).getTitle();

        IMergeRowIdResolver groupsVisingMergeResolver = entity -> ((EnrollmentOrderVisaItem) entity).getGroupsMemberVising().getId().toString() + ((EnrollmentOrderVisaItem) entity).getTitle();

        DynamicListDataSource<EnrollmentOrderVisaItem> dataSource = UniBaseUtils.createDataSource(component, getDao());

        dataSource.addColumn(new SimpleColumn("Название", EnrollmentOrderVisaItem.P_TITLE).setMergeRows(true).setMergeRowIdResolver(mergeRowIdResolver).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Группа участников визирования", EnrollmentOrderVisaItem.groupsMemberVising().title().s()).setOrderable(false).setClickable(false).setMergeRowIdResolver(groupsVisingMergeResolver));
        dataSource.addColumn(new SimpleColumn("Название должности в документе", EnrollmentOrderVisaItem.possibleVisa().title()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("ФИО сотрудника", EnrollmentOrderVisaItem.possibleVisa().entity().person().identityCard().fullFio()).setOrderable(false).setClickable(false));
//        dataSource.addColumn(new SimpleColumn("Подразделение", new String[]{EnrollmentOrderVisaItem.L_POSSIBLE_VISA, PossibleVisa.L_ORG_UNIT, OrgUnit.P_TITLE_WITH_PARENT}).setOrderable(false).setClickable(false));

        dataSource.addColumn(new ActionColumn("Повысить приоритет", CommonBaseDefine.ICO_UP, "onClickUp").setOrderable(false));
        dataSource.addColumn(new ActionColumn("Понизить приоритет", CommonBaseDefine.ICO_DOWN, "onClickDown").setOrderable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit").setMergeRows(true).setMergeRowIdResolver(mergeRowIdResolver));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить группу виз «{0}»?", EnrollmentOrderVisaItem.P_TITLE).setMergeRows(true).setMergeRowIdResolver(mergeRowIdResolver));

        model.setDataSource(dataSource);
    }

    public void onClickAddVisaGroup(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniecComponents.ENROLLMENT_ORDER_VISA_ADD_EDIT, new ParametersMap()
                .add("title", null)
                .add("enrollmentCampaignId", null)
        ));
    }

    public void onClickEdit(IBusinessComponent component)
    {
        Long id = (Long) component.getListenerParameter();
        EnrollmentOrderVisaItem item = getDao().getNotNull(EnrollmentOrderVisaItem.class, id);
        component.createDefaultChildRegion(new ComponentActivator(IUniecComponents.ENROLLMENT_ORDER_VISA_ADD_EDIT, new ParametersMap()
                .add("title", item.getTitle())
                .add("enrollmentCampaignId", item.getEnrollmentCampaign().getId())
        ));
    }

    public void onClickUp(IBusinessComponent component)
    {
        getDao().updatePriority((Long) component.getListenerParameter(), true);
    }

    public void onClickDown(IBusinessComponent component)
    {
        getDao().updatePriority((Long) component.getListenerParameter(), false);
    }

    public void onClickDelete(IBusinessComponent component)
    {
        getDao().deleteRow(component);
        component.refresh();
    }

    public void onRefresh(IBusinessComponent component)
    {
        component.saveSettings();
        getDao().prepare(getModel(component));
    }
}
