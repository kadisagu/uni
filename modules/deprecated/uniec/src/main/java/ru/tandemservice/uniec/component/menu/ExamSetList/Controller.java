/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.menu.ExamSetList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.IUniecComponents;
import ru.tandemservice.uniec.component.menu.ExamSetPub.ExamSetPubModel;
import ru.tandemservice.uniec.entity.examset.ExamSet;

import java.util.Map;

/**
 * @author Боба
 * @since 19.08.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.setSettings(component.getSettings());

        getDao().prepare(getModel(component));

        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<ExamSet> dataSource = UniBaseUtils.createDataSource(component, getDao());

        PublisherLinkColumn linkColumn = new PublisherLinkColumn("Набор ВИ", ExamSet.P_TITLE);
        linkColumn.setFormatter(NewLineFormatter.SIMPLE);
        linkColumn.setResolver(new DefaultPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                Map<String, Object> map = new ParametersMap();
                map.put(ExamSetPubModel.EXAM_SET_ID_PARAMETER, model.getId2examSetId().get(entity.getId()));
                return map;
            }

            @Override
            public String getComponentName(IEntity entity)
            {
                return IUniecComponents.EXAM_SET_PUB;
            }
        });
        dataSource.addColumn(linkColumn);
        dataSource.addColumn(new SimpleColumn("Кол-во направлений приема", ExamSet.P_COUNT).setClickable(false));
        model.setDataSource(dataSource);
    }

    public void onClickSearch(IBusinessComponent component)
    {
        getModel(component).getDataSource().refresh();
        component.saveSettings();
    }
}
