/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.menu.StateExamExport;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.uniec.IUniecComponents;

import java.util.Calendar;
import java.util.Date;

/**
 * @author vip_delete
 * @since 06.04.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);
        
        onChangeEnrollmentCampaign(component);
    }

    public void onClickApply(IBusinessComponent component)
    {
        final Model model = getModel(component);
        activateInRoot(component, new ComponentActivator(IUniecComponents.STATE_EXAM_EXPORT_PRINT, new ParametersMap()
                .add("dateFrom", model.getDateFrom())
                .add("dateTo", model.getDateTo())
                .add("exportType", model.getExportType().getId().intValue())
                .add("accepted", model.getAccepted() == null ? null : model.getAccepted().getId())
                .add("sent", model.getSent() == null ? null : model.getSent().getId())
                .add("includeNonRF", model.isIncludeNonRF())
                .add("enrollmentCampaignId", model.getEnrollmentCampaign().getId())
                .add("certificateHoldingId", model.getCertificateHolding() == null ? null : model.getCertificateHolding().getId())
                .add("includeEntrantWithoutMark", model.isIncludeEntrantWithoutMark())
        ));
    }
    
    public void onChangeEnrollmentCampaign(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getEnrollmentCampaign() != null)
        {
            Calendar nowDate = Calendar.getInstance();
            nowDate.setTime(model.getEnrollmentCampaign().getStartDate());
            model.setDateFrom(nowDate.getTime());
        }
        model.setDateTo(new Date());
    }
    
    public void onChangeCertificateHolding(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getCertificateHolding() != null && !model.getCertificateHolding().getId().equals(Model.CERTIFICATE_HOLDING_ID))
        {
            model.setSent(null);
        }
    }
}
