/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.EntrantEnrollmentDocumentTab;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.BooleanColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniec.IUniecComponents;
import ru.tandemservice.uniec.entity.catalog.EnrollmentDocument;
import ru.tandemservice.uniec.entity.entrant.EntrantEnrollmentDocument;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;

import java.util.Hashtable;

/**
 * @author oleyba
 * @since 16.03.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        model.setDataSourceMap(new Hashtable<>());

        for (EntrantRequest entrantRequest : model.getEntrantRequestList())
            prepareEntrantEnrollmentDocumentDataSource(component, entrantRequest);
    }

    private void prepareEntrantEnrollmentDocumentDataSource(IBusinessComponent component, final EntrantRequest entrantRequest)
    {
        final Model model = getModel(component);

        DynamicListDataSource<EntrantEnrollmentDocument> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareListDataSource(model, entrantRequest);
        });

        dataSource.addColumn(new SimpleColumn("Название", EntrantEnrollmentDocument.L_ENROLLMENT_DOCUMENT + "." + EnrollmentDocument.P_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Количество", EntrantEnrollmentDocument.P_AMOUNT).setClickable(false).setOrderable(false));
        dataSource.addColumn(new BooleanColumn("Копия", EntrantEnrollmentDocument.P_COPY));

        model.getDataSourceMap().put(entrantRequest, dataSource);
    }

    public void onClickEditDocuments(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniecComponents.ENTRANT_REQUEST_DOCUMENT_ADD_EDIT, new ParametersMap()
                .add("entrantRequestId", component.getListenerParameter())
                .add("addForm", false)
        ));
    }
}