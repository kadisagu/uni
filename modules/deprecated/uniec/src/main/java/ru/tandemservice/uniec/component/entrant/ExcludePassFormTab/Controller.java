/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.entrant.ExcludePassFormTab;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

/**
 * @author Vasily Zhukov
 * @since 26.07.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.setSettings(component.getSettings());

        getDao().prepare(getModel(component));
    }

    public void onChangeDevelopForm(IBusinessComponent component)
    {
        component.saveSettings();

        getDao().prepareListDataSource(getModel(component));
    }

    public void onClickApply(IBusinessComponent component)
    {
        getDao().update(getModel(component));
    }
}