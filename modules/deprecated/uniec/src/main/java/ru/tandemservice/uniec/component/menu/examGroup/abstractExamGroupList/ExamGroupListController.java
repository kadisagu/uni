/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.menu.examGroup.abstractExamGroupList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.dao.examgroup.IAbstractExamGroupLogic;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.CurrentExamGroupLogic;
import ru.tandemservice.uniec.entity.entrant.ExamGroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Vasily Zhukov
 * @since 28.05.2010
 */
public class ExamGroupListController<IDAO extends IExamGroupListDAO<Model>, Model extends ExamGroupListModel> extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        // создаем общие сеттинги для всех наследуемых бизнес компонентов компонентов
        model.setCommon(DataSettingsFacade.getSettings("abstractExamGroupList"));

        // создаем сеттинги только для текущего бизнес компонента
        model.setSettings(component.getSettings());

        // подготавливаем данные в модели
        getDao().prepare(model);

        if (model.getRedirectComponentName() != null)
        {
            deactivate(component);

            activateInRoot(component, new ComponentActivator(model.getRedirectComponentName()));
        } else
        {
            // создаем датасурс
            prepareListDataSource(component);
        }
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;
        model.setDataSource(createListDataSource(component));
    }

    // можно переопределить, чтобы создать свой датасурс

    protected DynamicListDataSource<ExamGroup> createListDataSource(IBusinessComponent component)
    {
        DynamicListDataSource<ExamGroup> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Название", ExamGroup.P_TITLE).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Количество абитуриентов", Model.ENTRANT_COUNT).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата формирования", ExamGroup.P_CREATE_DATE, DateFormatter.DATE_FORMATTER_WITH_TIME).setOrderable(false).setClickable(false));
        dataSource.addColumn(new IndicatorColumn("Печать группы", null, "onClickPrintGroup").defaultIndicator(new IndicatorColumn.Item("printer", "Печать списка абитуриентов группы")).setImageHeader(false).setDisableSecondSubmit(false).setPermissionKey("printExamGroup").setOrderable(false));
        dataSource.addColumn(new IndicatorColumn("Печать ведомостей", null, "onClickPrintSheet").defaultIndicator(new IndicatorColumn.Item("printer", "Печать экзаменационных ведомостей группы")).setImageHeader(false).setDisableSecondSubmit(false).setPermissionKey("printExamSheet").setOrderable(false));
        dataSource.addColumn(new IndicatorColumn("Печать результатов", null, "onClickPrintMarks").defaultIndicator(new IndicatorColumn.Item("printer", "Печать результатов сдачи вступительных испытаний")).setImageHeader(false).setDisableSecondSubmit(false).setPermissionKey("printExamMarks").setOrderable(false));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить экзаменационную группу «{0}»?", ExamGroup.P_TITLE).setPermissionKey("deleteExamGroup"));
        return dataSource;
    }

    // удаление экзаменационной группы

    public void onClickDelete(IBusinessComponent component)
    {
        getDao().deleteRow(component);
    }

    // поиск фильтров

    public void onChangeEnrollmentCampaign(IBusinessComponent component)
    {
        Model model = getModel(component);

        // вначале определяем, какой бизнес компонент требуется для отображения списка экзаменационных групп
        IAbstractExamGroupLogic logic = UniecDAOFacade.getExamGroupSetDao().getCurrentExamGroupLogic(model.getEnrollmentCampaign());
        String redirectComponentName = logic.getExamGroupListBusinessComponent();
        String currentComponentName = getClass().getPackage().getName();

        if (!redirectComponentName.equals(currentComponentName))
        {
            // сохраняем сеттинги
            DataSettingsFacade.saveSettings(model.getCommon());

            // деактивируем текущий компонент
            deactivate(component);

            // загружаем новый
            activateInRoot(component, new ComponentActivator(logic.getExamGroupListBusinessComponent()));
        }
    }

    public void onClickSearch(IBusinessComponent component)
    {
        Model model = getModel(component);
        DataSettingsFacade.saveSettings(model.getCommon());
        component.saveSettings();
        model.getDataSource().refresh();
    }

    // очистка фильтров

    public void onClickClear(IBusinessComponent component)
    {
        getModel(component).getSettings().clear();
        onClickSearch(component);
    }

    // печать групп, ведомостей и результатов сдачи
    
    public void onClickPrintGroup(IBusinessComponent component)
    {
        downloadPrintExamGroup(component, UniecDefines.SCRIPT_EXAM_GROUP_PREFIX);
    }

    public void onClickPrintGroupList(IBusinessComponent component)
    {
        downloadPrintExamGroupList(component, UniecDefines.SCRIPT_EXAM_GROUP_PREFIX);
    }

    public void onClickPrintSheet(IBusinessComponent component)
    {
        downloadPrintExamGroup(component, UniecDefines.SCRIPT_EXAM_GROUP_SHEET_PREFIX);
    }

    public void onClickPrintSheetList(IBusinessComponent component)
    {
        downloadPrintExamGroupList(component, UniecDefines.SCRIPT_EXAM_GROUP_SHEET_PREFIX);
    }

    public void onClickPrintMarks(IBusinessComponent component)
    {
        downloadPrintExamGroup(component, UniecDefines.SCRIPT_EXAM_GROUP_MARKS_PREFIX);
    }

    public void onClickPrintMarksList(IBusinessComponent component)
    {
        downloadPrintExamGroupList(component, UniecDefines.SCRIPT_EXAM_GROUP_MARKS_PREFIX);
    }

    private void downloadPrintExamGroup(IBusinessComponent component, String prefix)
    {
        // получаем id экзаменационных групп для печати
        List<Long> ids = Collections.singletonList((Long) component.getListenerParameter());

        // печатаем
        downloadPrint(component, prefix, ids);
    }

    private void downloadPrintExamGroupList(IBusinessComponent component, String prefix)
    {
        // получаем id экзаменационных групп для печати
        Model model = getModel(component);
        List<Long> ids = new ArrayList<Long>();
        for (Object entity : model.getDataSource().getEntityList())
            ids.add(((IEntity) entity).getId());

        // печатаем
        downloadPrint(component, prefix, ids);
    }

    private void downloadPrint(IBusinessComponent component, String prefix, List<Long> examGroupIds)
    {
        // получаем код алгоритма работы экзаменационных групп 
        String code = DataAccessServices.dao().getNotNull(CurrentExamGroupLogic.class, CurrentExamGroupLogic.enrollmentCampaign(), getModel(component).getEnrollmentCampaign()).getExamGroupLogic().getCode();

        // получаем скрипт печати
        IScriptItem script = DataAccessServices.dao().getByCode(UniecScriptItem.class, prefix + "_code" + code);

        // печатаем
        CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(script, examGroupIds);
    }
}