package ru.tandemservice.uniec.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Способ подачи и возврата оригиналов документов"
 * Имя сущности : methodDeliveryNReturnDocs
 * Файл data.xml : uniec.catalogs.data.xml
 */
public interface MethodDeliveryNReturnDocsCodes
{
    /** Константа кода (code) элемента : Лично (title) */
    String PERSONALLY = "enr.delNRet.personally";
    /** Константа кода (code) элемента : По доверенности (title) */
    String FIDUCIARY = "enr.delNRet.fiduciary";
    /** Константа кода (code) элемента : По почте (title) */
    String MAIL = "enr.delNRet.mail";

    Set<String> CODES = ImmutableSet.of(PERSONALLY, FIDUCIARY, MAIL);
}
