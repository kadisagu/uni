package ru.tandemservice.uniec.entity.orders;

import java.util.List;

import org.tandemframework.core.common.ITitled;

import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniec.entity.orders.gen.EnrollmentParagraphGen;
import ru.tandemservice.unimove.IAbstractExtract;

/**
 * Параграф приказа о зачислении абитуриентов
 */
public class EnrollmentParagraph extends EnrollmentParagraphGen implements ITitled
{
    public static final String P_NO_EDIT = "noEdit";       // нельзя редактировать

    public static final String P_TITLE = "title";

    @Override
    public String getTitle()
    {
        return "Параграф №" + getNumber();
    }

    public static final String P_FIRST_EXTRACT = "firstExtract";

    public EnrollmentExtract getFirstExtract()
    {
        List<EnrollmentExtract> extractList = UniDaoFacade.getCoreDao().getList(EnrollmentExtract.class, IAbstractExtract.L_PARAGRAPH, this);
        return extractList.size() > 0 ? extractList.get(0) : null;
    }

    public boolean isNoEdit()
    {
        return getOrder().isReadonly();
    }
}