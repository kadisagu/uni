/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.ExamPassDisciplineEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import ru.tandemservice.uniec.entity.entrant.EntranceExamPhase;

import java.util.Date;

/**
 * @author vip_delete
 * @since 23.03.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);
    }

    public void onChangeExamPhase(IBusinessComponent component)
    {
        Model model = getModel(component);

        EntranceExamPhase examPhase = model.getExamPhase();
        if (examPhase == null) return;

        model.getExamPassDiscipline().setPassDate(examPhase.getPassDate());
        model.getExamPassDiscipline().setExamCommissionMembership(examPhase.getExamCommissionMembership());
    }

    public void onChangePassDate(IBusinessComponent component)
    {
        Model model = getModel(component);

        Date passDate = model.getExamPassDiscipline().getPassDate();
        if (passDate == null)
        {
            model.setExamPhase(null);
            return;
        }

        EntranceExamPhase examPhase = model.getExamPhase();
        if (examPhase != null && examPhase.getPassDate().getTime() == passDate.getTime()) return;

        examPhase = model.getExamPhaseList().stream().filter(phase -> passDate.getTime() == phase.getPassDate().getTime()).findAny().orElse(null);
        model.setExamPhase(examPhase);
    }

    public void onClickApply(IBusinessComponent component)
    {
        getDao().update(getModel(component));
        deactivate(component);
    }
}
