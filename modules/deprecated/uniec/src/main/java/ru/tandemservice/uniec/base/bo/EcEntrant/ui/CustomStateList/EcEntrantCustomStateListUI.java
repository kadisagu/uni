/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcEntrant.ui.CustomStateList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniec.base.bo.EcEntrant.ui.CustomStateAddEdit.EcEntrantCustomStateAddEdit;

import java.util.Date;

/**
 * @author nvankov
 * @since 4/3/13
 */
@Input({
        @Bind(key = "entrantId", binding = "entrantId", required = true),
})
public class EcEntrantCustomStateListUI extends UIPresenter
{
    private Long _entrantId;

    public Long getEntrantId()
    {
        return _entrantId;
    }

    public void setEntrantId(Long entrantId)
    {
        _entrantId = entrantId;
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(dataSource.getName().equals(EcEntrantCustomStateList.CUSTOM_STATE_LIST_DS))
        {
            dataSource.putAll(_uiSettings.getAsMap(true, "entrantCustomStateCI", "activeDateFrom", "activeDateTo"));
            dataSource.put("entrantId", _entrantId);
        }
    }

    public void onClickAddCustomState()
    {
        _uiActivation.asRegionDialog(EcEntrantCustomStateAddEdit.class).
                parameter("entrantId", _entrantId).
                activate();
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asRegionDialog(EcEntrantCustomStateAddEdit.class).
                parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong()).
                parameter("entrantId", _entrantId).
                activate();
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onClickApply()
    {
        Date activeDateFrom = _uiSettings.get("activeDateFrom");
        Date activeDateTo = _uiSettings.get("activeDateTo");
        if(null != activeDateFrom && null != activeDateTo)
        {
            if(activeDateFrom.after(activeDateTo))
            {
                _uiSupport.error("Дата активности статуса «с» должна быть не больше даты активности «по»", "activeDateFrom", "activeDateTo");
            }
        }
        if(!getUserContext().getErrorCollector().hasErrors())
        {
            saveSettings();
        }
    }
}
