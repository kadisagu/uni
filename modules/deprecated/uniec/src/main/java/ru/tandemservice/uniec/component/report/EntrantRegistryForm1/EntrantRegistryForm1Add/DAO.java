/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EntrantRegistryForm1.EntrantRegistryForm1Add;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.report.EntrantRegistryForm1Report;
import ru.tandemservice.uniec.ui.EntranceEducationOrgUnitKindAutocompleteModel;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author oleyba
 * @since 30.06.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    @SuppressWarnings("deprecation")
    public void prepare(final Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        EntrantRegistryForm1Report report = model.getReport();
        report.setDateFrom(model.getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : model.getEnrollmentCampaign().getStartDate());
        report.setDateTo(CoreDateUtils.getDayFirstTimeMoment(new Date()));
        model.setTerritorialOrgUnit(TopOrgUnit.getInstance());

        model.setStudentCategoryListModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(StudentCategory.class)));
        model.setFormativeOrgUnitModel(new EntranceEducationOrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getPrincipalContext(), model.getEnrollmentCampaign())
        {
            @Override
            public OrgUnit getFilterOrgUnit()
            {
                return null;
            }
        });

        model.setTerritorialOrgUnitModel(new EntranceEducationOrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getPrincipalContext(), model.getEnrollmentCampaign())
        {
            @Override
            public OrgUnit getFilterOrgUnit()
            {
                return model.getFormativeOrgUnit();
            }
        }.setEducationOrgUnitFilterPropertyName(EducationOrgUnit.L_FORMATIVE_ORG_UNIT));

        model.setEducationLevelsHighSchoolModel(new FullCheckSelectModel("displayableTitle")
        {
            @Override
            public ListResult findValues(String filter)
            {
                return new ListResult<>(UniecDAOFacade.getEntranceEduLevelDAO().getEducationLevelsHighSchoolList(model));
            }
        });

        model.setDevelopFormModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                return new ListResult<>(UniecDAOFacade.getEntranceEduLevelDAO().getDevelopFormList(model));
            }
        });

        model.setDevelopTechModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                return new ListResult<>(UniecDAOFacade.getEntranceEduLevelDAO().getDevelopTechList(model));
            }
        });

        model.setDevelopPeriodModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                List<DevelopPeriod> tempDevelopPeriodList = UniecDAOFacade.getEntranceEduLevelDAO().getDevelopPeriodList(model);
                Collections.sort(tempDevelopPeriodList, new EntityComparator<>(new EntityOrder(DevelopPeriod.P_PRIORITY)));
                return new ListResult<>(tempDevelopPeriodList);
            }
        });

        model.setDevelopConditionModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                return new ListResult<>(UniecDAOFacade.getEntranceEduLevelDAO().getDevelopConditionList(model));
            }
        });

        model.setCompensationTypes(getCatalogItemList(CompensationType.class));
    }

    @Override
    public void update(Model model)
    {
        Session session = getSession();

        EntrantRegistryForm1Report report = model.getReport();
        report.setFormingDate(new Date());
        report.setEnrollmentDirection(UniecDAOFacade.getEntranceEduLevelDAO().getEnrollmentDirection(model));
        if (!model.getStudentCategoryList().isEmpty())
            report.setStudentCategoryTitle(StringUtils.lowerCase(StringUtils.join(CommonBaseUtil.getPropertiesList(model.getStudentCategoryList(), "title"), "; ")));

        try
        {
            DatabaseFile content = new DatabaseFile();
            content.setContent(new EntrantRegistryForm1ReportContentGenerator(report, model.getStudentCategoryList(), session).generateReportContent());
            session.save(content);
            report.setContent(content);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }

        session.save(report);
    }

}

