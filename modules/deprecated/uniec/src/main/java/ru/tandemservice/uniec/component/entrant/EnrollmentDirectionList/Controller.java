/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EnrollmentDirectionList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.IUniecComponents;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

/**
 * @author agolubenko
 * @since 03.07.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.setSettings(component.getSettings());
        model.setShowPossibleProfileColumn(false);

        getDao().prepare(model);

        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        DynamicListDataSource<EnrollmentDirection> dataSource = UniBaseUtils.createDataSource(component, getDao());

        dataSource.addColumn(new PublisherLinkColumn("Направление подготовки (специальность)", EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().displayableTitle().s()));
        dataSource.addColumn(new SimpleColumn("Формирующее подр.", EnrollmentDirection.FORMATIVE_ORG_UNIT_SHORT_TITLE_KEY).setClickable(false).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Территориальное подр.", EnrollmentDirection.TERRITORIAL_ORG_UNIT_SHORT_TITLE_KEY).setClickable(false).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Форма освоения", EnrollmentDirection.educationOrgUnit().developForm().title().s()).setClickable(false).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Условие освоения", EnrollmentDirection.educationOrgUnit().developCondition().title().s()).setClickable(false).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Технология освоения", EnrollmentDirection.educationOrgUnit().developTech().title().s()).setClickable(false).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Срок", EnrollmentDirection.educationOrgUnit().developPeriod().title().s()).setClickable(false));

        AbstractColumn budgetColumn = new SimpleColumn("Б", EnrollmentDirection.P_MINISTERIAL_PLAN).setClickable(false);
        AbstractColumn specRightColumn = new SimpleColumn("ОП", EnrollmentDirection.P_SPEC_RIGHTS_QUOTA).setClickable(false);
        AbstractColumn taBudgetColumn = new SimpleColumn("ЦБ", EnrollmentDirection.P_TARGET_ADMISSION_PLAN_BUDGET).setClickable(false);
        AbstractColumn crimeaColumn = new SimpleColumn("Крым", EnrollmentDirection.P_CRIMEA_QUOTA).setClickable(false);
        AbstractColumn contractColumn = new SimpleColumn("К", EnrollmentDirection.P_CONTRACT_PLAN).setClickable(false);
        AbstractColumn taContractColumn = new SimpleColumn("ЦК", EnrollmentDirection.P_TARGET_ADMISSION_PLAN_CONTRACT).setClickable(false);
        dataSource.addColumn(budgetColumn);
        dataSource.addColumn(specRightColumn);
        dataSource.addColumn(taBudgetColumn);
        dataSource.addColumn(crimeaColumn);
        dataSource.addColumn(contractColumn);
        dataSource.addColumn(taContractColumn);
        budgetColumn.setStyleResolver(new PlanStyleResolver(model, EnrollmentDirection.P_MINISTERIAL_PLAN, Model.BUDGET_IDX));
        contractColumn.setStyleResolver(new PlanStyleResolver(model, EnrollmentDirection.P_CONTRACT_PLAN, Model.CONTRACT_IDX));

        dataSource.addColumn(new BooleanColumn("Собесед.", EnrollmentDirection.P_INTERVIEW));
        dataSource.addColumn(new ToggleColumn("Прием на бюджет", EnrollmentDirection.P_BUDGET, "Осуществляется", "Не осуществляется").toggleOnListener("onTurnOnBudget").toggleOffListener("onTurnOffBudget"));
        dataSource.addColumn(new ToggleColumn("Прием по договору", EnrollmentDirection.P_CONTRACT, "Осуществляется", "Не осуществляется").toggleOnListener("onTurnOnContract").toggleOffListener("onTurnOffContract"));
        dataSource.addColumn(new ToggleColumn("Прием граждан Крыма", EnrollmentDirection.P_CRIMEA, "Осуществляется", "Не осуществляется").toggleOnListener("onTurnOnCrimea").toggleOffListener("onTurnOffCrimea"));
        if (model.isShowPossibleProfileColumn())
            dataSource.addColumn(new BlockColumn("hasPossibleProfile", "Есть возможные профили"));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditEnrollmentDirection").setPermissionKey("editEnrollmentDirection"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteEnrollmentDirection", "Направление приема «{0}» возможно выбрано онлайн-абитуриентами, удалить его?", new Object[]{EnrollmentDirection.P_TITLE}).setPermissionKey("deleteEnrollmentDirection"));

        model.setDataSource(dataSource);
    }

    public void onClickSearch(IBusinessComponent context)
    {
        context.saveSettings();
        getModel(context).getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent context)
    {
        Model model = getModel(context);
        model.getSettings().clear();
        getDao().prepare(model);
        onClickSearch(context);
    }

    public void onClickEditEnrollmentDirection(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniecComponents.ENROLLMENT_DIRECTION_EDIT, new ParametersMap().add("enrollmentDirectionId", component.getListenerParameter())));
    }

    public void onClickAddEnrollmentDirection(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniecComponents.ENROLLMENT_DIRECTION_ADD));
    }

    public void onClickDeleteEnrollmentDirection(IBusinessComponent component)
    {
        getDao().deleteRow(component);
    }

    public void onTurnOnBudget(IBusinessComponent component)
    {
        getDao().doChangeBudget(component.<Long>getListenerParameter(), true);
    }

    public void onTurnOffBudget(IBusinessComponent component)
    {
        getDao().doChangeBudget(component.<Long>getListenerParameter(), false);
    }

    public void onTurnOnContract(IBusinessComponent component)
    {
        getDao().doChangeContract(component.<Long>getListenerParameter(), true);
    }

    public void onTurnOffContract(IBusinessComponent component)
    {
        getDao().doChangeContract(component.<Long>getListenerParameter(), false);
    }

    public void onTurnOnCrimea(IBusinessComponent component)
    {
        getDao().doChangeCrimea(component.<Long>getListenerParameter(), true);
    }

    public void onTurnOffCrimea(IBusinessComponent component)
    {
        getDao().doChangeCrimea(component.<Long>getListenerParameter(), false);
    }

    private static final class PlanStyleResolver implements IStyleResolver
    {
        Model model;
        String name;
        int index;

        private PlanStyleResolver(Model model, String name, int index)
        {
            this.model = model;
            this.name = name;
            this.index = index;
        }

        @Override
        public String getStyle(IEntity rowEntity)
        {
            Integer rowPlan = (Integer) rowEntity.getProperty(name);
            if (rowPlan == null) return null;
            int[] data = model.getDirectionId2profileSum().get(rowEntity.getId());
            if (data == null) return null;
            return rowPlan != data[index] ? "color:red" : null;
        }
    }
}
