package ru.tandemservice.uniec.base.entity.ecg;

import org.tandemframework.core.view.formatter.DateFormatter;

import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgDistribution;
import ru.tandemservice.uniec.base.entity.ecg.gen.EcgDistributionGen;

/**
 * Основное распределение абитуриентов
 */
public class EcgDistribution extends EcgDistributionGen implements IEcgDistribution
{
    // ITitled

    @Override
    public String getTitle()
    {
        return "Волна №" + getWave() + " " + getConfig().getEcgItem().getTitle() + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getFormingDate()) + " (" + getConfig().getCompensationType().getShortTitle() + ")";
    }

    @Override
    public EcgDistribution getDistribution()
    {
        return this;
    }

    @Override
    public String getStateTitle()
    {
        if (getState().isApproved() && null != getApprovalDate())
            return getState().getTitle() + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getApprovalDate());

        return getState().getTitle();
    }

}