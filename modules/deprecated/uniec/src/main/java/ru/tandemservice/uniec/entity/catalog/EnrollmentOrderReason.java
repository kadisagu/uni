package ru.tandemservice.uniec.entity.catalog;

import java.util.List;

import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.gen.EnrollmentOrderReasonGen;

/**
 * Причина приказа о зачислении абитуриентов
 */
public class EnrollmentOrderReason extends EnrollmentOrderReasonGen
{
    public static final String P_BASICS = "basics";

    @SuppressWarnings("unchecked")
    public List<EnrollmentOrderBasic> getBasics()
    {
        return UniecDAOFacade.getSettingsDAO().getReasonToBasicsList(this);
    }
}