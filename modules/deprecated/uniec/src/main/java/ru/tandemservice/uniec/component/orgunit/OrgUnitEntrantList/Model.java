/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.orgunit.OrgUnitEntrantList;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.tandemframework.core.component.State;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.base.bo.UniStudent.vo.AddressCountryVO;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;

/**
 * @author agolubenko
 * @since 24.07.2009
 */
@State(keys = { "orgUnitId" }, bindings = { "orgUnitId" })
public class Model implements IEnrollmentCampaignSelectModel
{
    public static final String REQUEST_NUMBER = "requestNumber";

    private Long _orgUnitId;
    private OrgUnit _orgUnit;
    private IDataSettings _settings;
    private DynamicListDataSource<Entrant> _dataSource;

    private List<EnrollmentCampaign> _enrollmentCampaignList;
    private List<CompensationType> compensationTypeList;
    private ISelectModel qualificationModel;
    private ISelectModel developFormList;
    private ISelectModel developConditionModel;
    private ISelectModel citizenShipModel;

    private AddressCountryVO _citizenship;

    private List<ArchivalSelectOption> _archivalList = new ArrayList<ArchivalSelectOption>();
    {
        _archivalList.add(new ArchivalSelectOption(true, "Показать архивных"));
        _archivalList.add(new ArchivalSelectOption(false, "Показать не архивных"));
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return (EnrollmentCampaign) getSettings().get(ENROLLMENT_CAMPAIGN_FILTER_NAME);
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        getSettings().set(ENROLLMENT_CAMPAIGN_FILTER_NAME, enrollmentCampaign);
    }
    
    public String getPrintListPermissionKey()
    {
        return "orgUnit_printEntantList_" + getOrgUnit().getOrgUnitType().getCode();
    }

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    public DynamicListDataSource<Entrant> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<Entrant> dataSource)
    {
        _dataSource = dataSource;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        this.compensationTypeList = compensationTypeList;
    }

    public ISelectModel getQualificationModel()
    {
        return qualificationModel;
    }

    public void setQualificationModel(ISelectModel qualificationModel)
    {
        this.qualificationModel = qualificationModel;
    }

    public ISelectModel getDevelopFormList() { return developFormList; }
    public void setDevelopFormList(ISelectModel developFormList) { this.developFormList = developFormList; }

    public ISelectModel getDevelopConditionModel()
    {
        return developConditionModel;
    }

    public void setDevelopConditionModel(ISelectModel developConditionModel)
    {
        this.developConditionModel = developConditionModel;
    }

    public List<ArchivalSelectOption> getArchivalList()
    {
        return _archivalList;
    }

    public void setArchivalList(List<ArchivalSelectOption> archivalList)
    {
        _archivalList = archivalList;
    }

    @Override
    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    static class ArchivalSelectOption implements Serializable
    {
        private static final long serialVersionUID = 2553474821757516218L;
        private boolean _archival;
        private String _title;

        public ArchivalSelectOption(boolean archival, String title)
        {
            _archival = archival;
            _title = title;
        }

        public boolean isArchival()
        {
            return _archival;
        }

        public String getTitle()
        {
            return _title;
        }
    }

    public ISelectModel getCitizenShipModel()
    {
        return citizenShipModel;
    }

    public void setCitizenShipModel(ISelectModel citizenShipModel)
    {
        this.citizenShipModel = citizenShipModel;
    }

    public AddressCountryVO getCitizenship()
    {
        return _citizenship;
    }

    public void setCitizenship(AddressCountryVO citizenship)
    {
        _citizenship = citizenship;
    }
}
