/* $Id: Controller.java 8737 2009-06-26 11:27:38Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.menu.CompetitionGroupDistribList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.tool.IdGen;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.RawFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.IUniecComponents;
import ru.tandemservice.uniec.entity.ecg.EcgDistribObject;
import ru.tandemservice.uniec.entity.entrant.CompetitionGroup;

import java.util.Collections;

/**
 * @author vdanilov
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = getModel(component);

        if (null != model.getCreatedObjectId())
        {
            try
            {
                final IEntity e = UniDaoFacade.getCoreDao().get(model.getCreatedObjectId());
                if (e instanceof EcgDistribObject)
                {
                    // очень хочется переходить на публикатор вновь созданного объекта
                    //activate(component, new ComponentActivator(
                    //		IUniecComponents.COMPETITION_GROUP_DISTRIB_PUB,
                    //		Collections.<String, Object>singletonMap(PublisherActivator.PUBLISHER_ID_KEY, e.getId())
                    //));
                    return;
                } //else
                //{
                    // skip it
                //}

            } finally
            {
                model.setCreatedObjectId(null);

            }
        }

        model.setSettings(component.getSettings());
        getDao().prepare(getModel(component));

        final IMergeRowIdResolver cgRowResolver = entity -> String.valueOf(((EcgDistribObject) entity).getCompetitionGroup().getId());
        final IMergeRowIdResolver cgCtRowResolver = entity -> {
            final EcgDistribObject ecgDistribObject = (EcgDistribObject) entity;
            return cgRowResolver.getMergeRowId(entity) + "-" + String.valueOf(ecgDistribObject.getCompensationType().getId());
        };

        final DynamicListDataSource<EcgDistribObject> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Группа", EcgDistribObject.competitionGroup().title().s()).setWidth(5).setMergeRows(true).setMergeRowIdResolver(cgRowResolver).setOrderable(true).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Вид возмещения затрат", EcgDistribObject.compensationType().shortTitle().s()).setWidth(7).setMergeRows(true).setMergeRowIdResolver(cgCtRowResolver).setOrderable(false).setClickable(false));

        final short DIR_CODE = EntityRuntime.getMeta(EcgDistribObject.class).getEntityCode();
        final IEntityHandler viewDisabler = entity -> ((null == entity.getId()) || (IdGen.getCode(entity.getId()) != DIR_CODE));

        dataSource.addColumn(new IndicatorColumn("Состояние распределения", "")
        {
            private final Item ROOT_APPROVED = new Item("cg_distr_root_appr", "утверждено");
            private final Item ROOT_FORMING = new Item("cg_distr_root_form", "формируется");
            private final Item CHILD_APPROVED = new Item("cg_distr_child_appr", "утверждено (уточняющее)");
            private final Item CHILD_FORMING = new Item("cg_distr_child_form", "формируется (уточняющее)");
            private final short DIR_CODE = EntityRuntime.getMeta(EcgDistribObject.class).getEntityCode();
            
            @Override
            public Item getContent(final IEntity entity)
            {
                if (((null == entity.getId()) || (IdGen.getCode(entity.getId()) != DIR_CODE)))
                    return null;
                final EcgDistribObject d = (EcgDistribObject) entity;
                if (null == d.getParent())
                {
                    return d.isApproved() ? ROOT_APPROVED : ROOT_FORMING;
                } else
                {
                    return d.isApproved() ? CHILD_APPROVED : CHILD_FORMING;
                }
            }
        }.setOrderable(false).setClickable(false));

        dataSource.addColumn(new SimpleColumn("Распределения", EcgDistribObject.title().s(), RawFormatter.INSTANCE)
        {
            @Override
            public String getContent(final IEntity entity)
            {
                if (viewDisabler.handle(entity))
                {
                    return "";
                }

                final StringBuilder sb = new StringBuilder("<nobr>");
                EcgDistribObject e = ((EcgDistribObject) entity).getParent();
                while (null != e)
                {
                    sb.append("&nbsp;&nbsp;&nbsp;&nbsp;");
                    e = e.getParent();
                }

                sb.append(((EcgDistribObject) entity).getTitle());
                sb.append("</nobr>");
                return sb.toString();
            }
        }.setDisableHandler(viewDisabler).setOrderable(false).setClickable(true));

        dataSource.addColumn(new SimpleColumn("Дата добавления", EcgDistribObject.registrationDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setWidth(5).setOrderable(false).setClickable(false));

        final IEntityHandler editDisabler = entity -> (viewDisabler.handle(entity) || ((EcgDistribObject) entity).isApproved());
        final IEntityHandler addChildDisabler = entity -> (viewDisabler.handle(entity) || (!((EcgDistribObject) entity).isApproved()) || (null != ((EcgDistribObject) entity).getParent() /* чтоб не повадно было иерархию разводить */));
        final IEntityHandler addRootDisabler = entity -> (null != ((EcgDistribObject) entity).getParent());
        ActionColumn addColumn = new ActionColumn("Добавить", "add_item", "onClickAdd");
        addColumn.setValidate(true);
        dataSource.addColumn(addColumn.setDisableHandler(addRootDisabler));
        dataSource.addColumn(new ActionColumn("Добавить уточняющее распределение", "clone", "onClickAddChild").setDisableHandler(addChildDisabler));
        dataSource.addColumn(new ActionColumn("Редактировать", "edit", "onClickEdit").setDisableHandler(editDisabler));
        dataSource.addColumn(new ActionColumn("Удалить", "delete", "onClickDelete", "Удалить распределение «{0}»?", "title").setDisableHandler(viewDisabler));

        model.setDataSource(dataSource);
    }

    public void onChangeEnrollmentCampaign(final IBusinessComponent component)
    {
        getDao().refreshCompetitionGroupFilter(getModel(component));
    }

    public void onClickSearch(IBusinessComponent component)
    {
        getModel(component).getDataSource().refresh();
        component.saveSettings();
    }

    public void onClickClear(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.getSettings().clear();
        getDao().prepare(model);
        onClickSearch(component);
    }

    public void onClickEdit(final IBusinessComponent component)
    {
        final IEntity e = UniDaoFacade.getCoreDao().get((Long) component.getListenerParameter());
        if (e instanceof EcgDistribObject)
        {
            component.createDefaultChildRegion(new ComponentActivator(
                    IUniecComponents.COMPETITION_GROUP_DISTRIB_ADD_EDIT,
                    Collections.<String, Object>singletonMap(PublisherActivator.PUBLISHER_ID_KEY, e.getId())
            ));
        }
    }

    public void onClickDelete(final IBusinessComponent component)
    {
        getDao().delete(getModel(component), (Long) component.getListenerParameter());
    }

    public void onClickAddChild(final IBusinessComponent component)
    {
        final IEntity e = UniDaoFacade.getCoreDao().get((Long) component.getListenerParameter());
        if (e instanceof EcgDistribObject)
        {
            final EcgDistribObject d = (EcgDistribObject) e;
            component.createDefaultChildRegion(new ComponentActivator(
                    IUniecComponents.COMPETITION_GROUP_DISTRIB_ADD_EDIT,
                    new ParametersMap()
                            .add(PublisherActivator.PUBLISHER_ID_KEY, d.getCompetitionGroup().getId())
                            .add("parentId", d.getId())
                            .add("compensationTypeId", d.getCompensationType().getId())
                            .add("category", d.isSecondHighAdmission())
            ));
        }
    }

    public void onClickAdd(final IBusinessComponent component)
    {
        final IEntity e = UniDaoFacade.getCoreDao().get((Long) component.getListenerParameter());
        Model model = getModel(component);
        ParametersMap parameters = new ParametersMap();
        if (e instanceof EcgDistribObject)
        {
            final EcgDistribObject d = (EcgDistribObject) e;
            IdentifiableWrapper category = model.getSettings().get("category");
            component.createDefaultChildRegion(new ComponentActivator(
                    IUniecComponents.COMPETITION_GROUP_DISTRIB_ADD_EDIT,
                    parameters
                            .add(PublisherActivator.PUBLISHER_ID_KEY, d.getCompetitionGroup().getId())
                            .add("parentId", null)
                            .add("compensationTypeId", d.getCompensationType().getId())
                            .add("category", category != null && category.getId().equals(1L))
            ));
        } else if (e instanceof CompetitionGroup)
        {
            IdentifiableWrapper category = model.getSettings().get("category");
            CompensationType compensationType = model.getSettings().get("compensationType");
            parameters.add(PublisherActivator.PUBLISHER_ID_KEY, e.getId());
            parameters.add("parentId", null);
            if (null != compensationType)
                parameters.add("compensationTypeId", compensationType.getId());
            parameters.add("category", category != null && category.getId().equals(1L));
            component.createDefaultChildRegion(new ComponentActivator(IUniecComponents.COMPETITION_GROUP_DISTRIB_ADD_EDIT, parameters));
        }
    }
}
