package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uniec.entity.entrant.ExamGroup;
import ru.tandemservice.uniec.entity.entrant.ExamGroupSet;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Экзаменационная группа
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ExamGroupGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.ExamGroup";
    public static final String ENTITY_NAME = "examGroup";
    public static final int VERSION_HASH = -1116237193;
    private static IEntityMeta ENTITY_META;

    public static final String P_CREATE_DATE = "createDate";
    public static final String P_TITLE = "title";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_KEY = "key";
    public static final String P_INDEX = "index";
    public static final String L_EXAM_GROUP_SET = "examGroupSet";

    private Date _createDate;     // Дата создания
    private String _title;     // Название группы
    private String _shortTitle;     // Сокращенное название группы
    private String _key;     // Ключ группы
    private int _index;     // Номер группы
    private ExamGroupSet _examGroupSet;     // Набор экзаменационной группы

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата создания. Свойство не может быть null.
     */
    @NotNull
    public Date getCreateDate()
    {
        return _createDate;
    }

    /**
     * @param createDate Дата создания. Свойство не может быть null.
     */
    public void setCreateDate(Date createDate)
    {
        dirty(_createDate, createDate);
        _createDate = createDate;
    }

    /**
     * @return Название группы. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название группы. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Сокращенное название группы. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название группы. Свойство не может быть null.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Ключ группы. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getKey()
    {
        return _key;
    }

    /**
     * @param key Ключ группы. Свойство не может быть null.
     */
    public void setKey(String key)
    {
        dirty(_key, key);
        _key = key;
    }

    /**
     * @return Номер группы. Свойство не может быть null.
     */
    @NotNull
    public int getIndex()
    {
        return _index;
    }

    /**
     * @param index Номер группы. Свойство не может быть null.
     */
    public void setIndex(int index)
    {
        dirty(_index, index);
        _index = index;
    }

    /**
     * @return Набор экзаменационной группы. Свойство не может быть null.
     */
    @NotNull
    public ExamGroupSet getExamGroupSet()
    {
        return _examGroupSet;
    }

    /**
     * @param examGroupSet Набор экзаменационной группы. Свойство не может быть null.
     */
    public void setExamGroupSet(ExamGroupSet examGroupSet)
    {
        dirty(_examGroupSet, examGroupSet);
        _examGroupSet = examGroupSet;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ExamGroupGen)
        {
            setCreateDate(((ExamGroup)another).getCreateDate());
            setTitle(((ExamGroup)another).getTitle());
            setShortTitle(((ExamGroup)another).getShortTitle());
            setKey(((ExamGroup)another).getKey());
            setIndex(((ExamGroup)another).getIndex());
            setExamGroupSet(((ExamGroup)another).getExamGroupSet());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ExamGroupGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ExamGroup.class;
        }

        public T newInstance()
        {
            return (T) new ExamGroup();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "createDate":
                    return obj.getCreateDate();
                case "title":
                    return obj.getTitle();
                case "shortTitle":
                    return obj.getShortTitle();
                case "key":
                    return obj.getKey();
                case "index":
                    return obj.getIndex();
                case "examGroupSet":
                    return obj.getExamGroupSet();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "createDate":
                    obj.setCreateDate((Date) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "key":
                    obj.setKey((String) value);
                    return;
                case "index":
                    obj.setIndex((Integer) value);
                    return;
                case "examGroupSet":
                    obj.setExamGroupSet((ExamGroupSet) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "createDate":
                        return true;
                case "title":
                        return true;
                case "shortTitle":
                        return true;
                case "key":
                        return true;
                case "index":
                        return true;
                case "examGroupSet":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "createDate":
                    return true;
                case "title":
                    return true;
                case "shortTitle":
                    return true;
                case "key":
                    return true;
                case "index":
                    return true;
                case "examGroupSet":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "createDate":
                    return Date.class;
                case "title":
                    return String.class;
                case "shortTitle":
                    return String.class;
                case "key":
                    return String.class;
                case "index":
                    return Integer.class;
                case "examGroupSet":
                    return ExamGroupSet.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ExamGroup> _dslPath = new Path<ExamGroup>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ExamGroup");
    }
            

    /**
     * @return Дата создания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamGroup#getCreateDate()
     */
    public static PropertyPath<Date> createDate()
    {
        return _dslPath.createDate();
    }

    /**
     * @return Название группы. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamGroup#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Сокращенное название группы. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamGroup#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Ключ группы. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamGroup#getKey()
     */
    public static PropertyPath<String> key()
    {
        return _dslPath.key();
    }

    /**
     * @return Номер группы. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamGroup#getIndex()
     */
    public static PropertyPath<Integer> index()
    {
        return _dslPath.index();
    }

    /**
     * @return Набор экзаменационной группы. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamGroup#getExamGroupSet()
     */
    public static ExamGroupSet.Path<ExamGroupSet> examGroupSet()
    {
        return _dslPath.examGroupSet();
    }

    public static class Path<E extends ExamGroup> extends EntityPath<E>
    {
        private PropertyPath<Date> _createDate;
        private PropertyPath<String> _title;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<String> _key;
        private PropertyPath<Integer> _index;
        private ExamGroupSet.Path<ExamGroupSet> _examGroupSet;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата создания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamGroup#getCreateDate()
     */
        public PropertyPath<Date> createDate()
        {
            if(_createDate == null )
                _createDate = new PropertyPath<Date>(ExamGroupGen.P_CREATE_DATE, this);
            return _createDate;
        }

    /**
     * @return Название группы. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamGroup#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(ExamGroupGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Сокращенное название группы. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamGroup#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(ExamGroupGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Ключ группы. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamGroup#getKey()
     */
        public PropertyPath<String> key()
        {
            if(_key == null )
                _key = new PropertyPath<String>(ExamGroupGen.P_KEY, this);
            return _key;
        }

    /**
     * @return Номер группы. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamGroup#getIndex()
     */
        public PropertyPath<Integer> index()
        {
            if(_index == null )
                _index = new PropertyPath<Integer>(ExamGroupGen.P_INDEX, this);
            return _index;
        }

    /**
     * @return Набор экзаменационной группы. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamGroup#getExamGroupSet()
     */
        public ExamGroupSet.Path<ExamGroupSet> examGroupSet()
        {
            if(_examGroupSet == null )
                _examGroupSet = new ExamGroupSet.Path<ExamGroupSet>(L_EXAM_GROUP_SET, this);
            return _examGroupSet;
        }

        public Class getEntityClass()
        {
            return ExamGroup.class;
        }

        public String getEntityName()
        {
            return "examGroup";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
