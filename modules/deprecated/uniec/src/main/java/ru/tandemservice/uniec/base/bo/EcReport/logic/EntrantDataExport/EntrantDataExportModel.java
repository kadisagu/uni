/**
 *$Id:$
 */
package ru.tandemservice.uniec.base.bo.EcReport.logic.EntrantDataExport;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;

import java.util.Date;
import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 02.08.12
 */
public class EntrantDataExportModel implements MultiEnrollmentDirectionUtil.Model
{
    private MultiEnrollmentDirectionUtil.Parameters _parameters;

    // поля активности фильтров отчета
    private boolean _statusActive;
    private boolean _compensationTypeActive;
    private boolean _formativeOrgUnitActive;
    private boolean _territorialOrgUnitActive;
    private boolean _educationLevelHighSchoolActive;
    private boolean _developFormActive;
    private boolean _developConditionActive;
    private boolean _developTechActive;
    private boolean _developPeriodActive;

    // поля выбранных элементов в фильтрах отчета
    private EnrollmentCampaign _enrollmentCampaign;
    private Date _requestFrom;
    private Date _requestTo;
    private CompensationType _compensationType;
    private List<EntrantState> _statusList;
    private List<OrgUnit> _formativeOrgUnitList;
    private List<OrgUnit> _territorialOrgUnitList;
    private List<EducationLevelsHighSchool> _educationLevelHighSchoolList;
    private List<DevelopForm> _developFormList;
    private List<DevelopCondition> _developConditionList;
    private List<DevelopTech> _developTechList;
    private List<DevelopPeriod> _developPeriodList;

    // модели фильтров отчета
    private ISingleSelectModel _enrollmentCampaignModel;
    private ISingleSelectModel _compensationTypeModel;
    private IMultiSelectModel _statusModel;
    private IMultiSelectModel _formativeOrgUnitModel;
    private IMultiSelectModel _territorialOrgUnitModel;
    private IMultiSelectModel _educationLevelHighSchoolModel;
    private IMultiSelectModel _developFormModel;
    private IMultiSelectModel _developConditionModel;
    private IMultiSelectModel _developTechModel;
    private IMultiSelectModel _developPeriodModel;

    // Getters & Setters

    @Override
    public List<EnrollmentCampaign> getSelectedEnrollmentCampaignList()
    {
        return MultiEnrollmentDirectionUtil.getSingletonList(_enrollmentCampaign);
    }

    @Override
    public List<OrgUnit> getSelectedFormativeOrgUnitList()
    {
        return isFormativeOrgUnitActive() ? getFormativeOrgUnitList() : null;
    }

    @Override
    public List<OrgUnit> getSelectedTerritorialOrgUnitList()
    {
        return isTerritorialOrgUnitActive() ? getTerritorialOrgUnitList() : null;
    }

    @Override
    public List<EducationLevelsHighSchool> getSelectedEducationLevelHighSchoolList()
    {
        return isEducationLevelHighSchoolActive() ? getEducationLevelHighSchoolList() : null;
    }

    @Override
    public List<DevelopForm> getSelectedDevelopFormList()
    {
        return isDevelopFormActive() ? getDevelopFormList() : null;
    }

    @Override
    public List<DevelopCondition> getSelectedDevelopConditionList()
    {
        return isDevelopConditionActive() ? getDevelopConditionList() : null;
    }

    @Override
    public List<DevelopTech> getSelectedDevelopTechList()
    {
        return isDevelopTechActive() ? getDevelopTechList() : null;
    }

    @Override
    public List<DevelopPeriod> getSelectedDevelopPeriodList()
    {
        return isDevelopPeriodActive() ? getDevelopPeriodList() : null;
    }

    public MultiEnrollmentDirectionUtil.Parameters getParameters()
    {
        return _parameters;
    }

    public void setParameters(MultiEnrollmentDirectionUtil.Parameters parameters)
    {
        _parameters = parameters;
    }

    public boolean isFormativeOrgUnitActive()
    {
        return _formativeOrgUnitActive;
    }

    public void setFormativeOrgUnitActive(boolean formativeOrgUnitActive)
    {
        _formativeOrgUnitActive = formativeOrgUnitActive;
    }

    public boolean isTerritorialOrgUnitActive()
    {
        return _territorialOrgUnitActive;
    }

    public void setTerritorialOrgUnitActive(boolean territorialOrgUnitActive)
    {
        _territorialOrgUnitActive = territorialOrgUnitActive;
    }

    public boolean isDevelopFormActive()
    {
        return _developFormActive;
    }

    public void setDevelopFormActive(boolean developFormActive)
    {
        _developFormActive = developFormActive;
    }

    public boolean isDevelopConditionActive()
    {
        return _developConditionActive;
    }

    public void setDevelopConditionActive(boolean developConditionActive)
    {
        _developConditionActive = developConditionActive;
    }

    public boolean isDevelopTechActive()
    {
        return _developTechActive;
    }

    public void setDevelopTechActive(boolean developTechActive)
    {
        _developTechActive = developTechActive;
    }

    public boolean isDevelopPeriodActive()
    {
        return _developPeriodActive;
    }

    public void setDevelopPeriodActive(boolean developPeriodActive)
    {
        _developPeriodActive = developPeriodActive;
    }

    public List<OrgUnit> getFormativeOrgUnitList()
    {
        return _formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(List<OrgUnit> formativeOrgUnitList)
    {
        _formativeOrgUnitList = formativeOrgUnitList;
    }

    public List<OrgUnit> getTerritorialOrgUnitList()
    {
        return _territorialOrgUnitList;
    }

    public void setTerritorialOrgUnitList(List<OrgUnit> territorialOrgUnitList)
    {
        _territorialOrgUnitList = territorialOrgUnitList;
    }

    public List<DevelopCondition> getDevelopConditionList()
    {
        return _developConditionList;
    }

    public void setDevelopConditionList(List<DevelopCondition> developConditionList)
    {
        _developConditionList = developConditionList;
    }

    public List<DevelopTech> getDevelopTechList()
    {
        return _developTechList;
    }

    public void setDevelopTechList(List<DevelopTech> developTechList)
    {
        _developTechList = developTechList;
    }

    public List<DevelopPeriod> getDevelopPeriodList()
    {
        return _developPeriodList;
    }

    public void setDevelopPeriodList(List<DevelopPeriod> developPeriodList)
    {
        _developPeriodList = developPeriodList;
    }

    public IMultiSelectModel getFormativeOrgUnitModel()
    {
        return _formativeOrgUnitModel;
    }

    public void setFormativeOrgUnitModel(IMultiSelectModel formativeOrgUnitModel)
    {
        _formativeOrgUnitModel = formativeOrgUnitModel;
    }

    public IMultiSelectModel getTerritorialOrgUnitModel()
    {
        return _territorialOrgUnitModel;
    }

    public void setTerritorialOrgUnitModel(IMultiSelectModel territorialOrgUnitModel)
    {
        _territorialOrgUnitModel = territorialOrgUnitModel;
    }

    public IMultiSelectModel getDevelopConditionModel()
    {
        return _developConditionModel;
    }

    public void setDevelopConditionModel(IMultiSelectModel developConditionModel)
    {
        _developConditionModel = developConditionModel;
    }

    public IMultiSelectModel getDevelopTechModel()
    {
        return _developTechModel;
    }

    public void setDevelopTechModel(IMultiSelectModel developTechModel)
    {
        _developTechModel = developTechModel;
    }

    public IMultiSelectModel getDevelopPeriodModel()
    {
        return _developPeriodModel;
    }

    public void setDevelopPeriodModel(IMultiSelectModel developPeriodModel)
    {
        _developPeriodModel = developPeriodModel;
    }

    public boolean isCompensationTypeActive()
    {
        return _compensationTypeActive;
    }

    public void setCompensationTypeActive(boolean compensationTypeActive)
    {
        _compensationTypeActive = compensationTypeActive;
    }

    public boolean isStatusActive()
    {
        return _statusActive;
    }

    public void setStatusActive(boolean statusActive)
    {
        _statusActive = statusActive;
    }

    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public Date getRequestFrom()
    {
        return _requestFrom;
    }

    public void setRequestFrom(Date requestFrom)
    {
        _requestFrom = requestFrom;
    }

    public Date getRequestTo()
    {
        return _requestTo;
    }

    public void setRequestTo(Date requestTo)
    {
        _requestTo = requestTo;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

    public List<DevelopForm> getDevelopFormList()
    {
        return _developFormList;
    }

    public void setDevelopFormList(List<DevelopForm> developFormList)
    {
        _developFormList = developFormList;
    }

    public List<EntrantState> getStatusList()
    {
        return _statusList;
    }

    public void setStatusList(List<EntrantState> statusList)
    {
        _statusList = statusList;
    }

    public ISingleSelectModel getEnrollmentCampaignModel()
    {
        return _enrollmentCampaignModel;
    }

    public void setEnrollmentCampaignModel(ISingleSelectModel enrollmentCampaignModel)
    {
        _enrollmentCampaignModel = enrollmentCampaignModel;
    }

    public ISingleSelectModel getCompensationTypeModel()
    {
        return _compensationTypeModel;
    }

    public void setCompensationTypeModel(ISingleSelectModel compensationTypeModel)
    {
        _compensationTypeModel = compensationTypeModel;
    }

    public IMultiSelectModel getDevelopFormModel()
    {
        return _developFormModel;
    }

    public void setDevelopFormModel(IMultiSelectModel developFormModel)
    {
        _developFormModel = developFormModel;
    }

    public IMultiSelectModel getStatusModel()
    {
        return _statusModel;
    }

    public void setStatusModel(IMultiSelectModel statusModel)
    {
        _statusModel = statusModel;
    }

    public boolean isEducationLevelHighSchoolActive()
    {
        return _educationLevelHighSchoolActive;
    }

    public void setEducationLevelHighSchoolActive(boolean educationLevelHighSchoolActive)
    {
        _educationLevelHighSchoolActive = educationLevelHighSchoolActive;
    }

    public List<EducationLevelsHighSchool> getEducationLevelHighSchoolList()
    {
        return _educationLevelHighSchoolList;
    }

    public void setEducationLevelHighSchoolList(List<EducationLevelsHighSchool> educationLevelHighSchoolList)
    {
        _educationLevelHighSchoolList = educationLevelHighSchoolList;
    }

    public IMultiSelectModel getEducationLevelHighSchoolModel()
    {
        return _educationLevelHighSchoolModel;
    }

    public void setEducationLevelHighSchoolModel(IMultiSelectModel educationLevelHighSchoolModel)
    {
        _educationLevelHighSchoolModel = educationLevelHighSchoolModel;
    }
}
