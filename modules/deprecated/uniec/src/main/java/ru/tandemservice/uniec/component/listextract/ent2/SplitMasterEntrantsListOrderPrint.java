/* $Id$ */
package ru.tandemservice.uniec.component.listextract.ent2;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.base.bo.EcProfileDistribution.EcProfileDistributionManager;
import ru.tandemservice.uniec.dao.UniecDAOFacade;

/**
 * @author Dmitry Seleznev
 * @since 21.08.2012
 */
public class SplitMasterEntrantsListOrderPrint implements IPrintFormCreator<StudentListOrder>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, StudentListOrder order)
    {
        EcProfileDistributionManager.instance().dao(); // Stupid hack... But what can I do with all evill thigs, that Vasya has done for groovy scripts?
        return UniecDAOFacade.getOrderDao().getOrderPrintForm(template, order, UniecDefines.SCRIPT_SPLIT_MASTER_ENTRANTS_LIST_ORDER);
    }
}