/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.util;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BooleanColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.InExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDefine;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.CompetitionGroupRestriction;
import ru.tandemservice.uniec.entity.settings.EnrollmentDirectionRestriction;

import java.util.*;

/**
 * Утиль для работы с формами создания/редактирования заявлений
 *
 * @author vip_delete
 * @since 07.06.2009
 */
public class EntrantRequestAddEditUtil
{
    public static final String REQUESTED_DIRECTIONS_COLUMN = "direction";

    public static DynamicListDataSource<RequestedEnrollmentDirection> getSelectedRequestedEnrollmentDirectionDataSource(IBusinessComponent component, IListDataSourceDelegate delegate)
    {
        DynamicListDataSource<RequestedEnrollmentDirection> dataSource = new DynamicListDataSource<>(component, delegate);
        dataSource.addColumn(new SimpleColumn("Формирующее подр.", new String[]{RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, EnrollmentDirection.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, OrgUnit.P_SHORT_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn(REQUESTED_DIRECTIONS_COLUMN,
                                              new String[]{RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, EnrollmentDirection.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, EducationLevelsHighSchool.P_DISPLAYABLE_TITLE},
                                              "Направление подготовки (специальность)").setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Форма освоения", new String[]{RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, EnrollmentDirection.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_DEVELOP_FORM, DevelopForm.P_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Вид конкурса", new String[]{RequestedEnrollmentDirection.L_COMPETITION_KIND, CompetitionKind.P_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Контракт", new String[]{RequestedEnrollmentDirection.L_COMPENSATION_TYPE, CompensationType.P_SHORT_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new BooleanColumn("Сданы оригиналы", RequestedEnrollmentDirection.P_ORIGINAL_DOCUMENT_HANDED_IN));
        dataSource.addColumn(new ActionColumn("Повысить приоритет", CommonBaseDefine.ICO_UP, "onClickUp").setOrderable(false));
        dataSource.addColumn(new ActionColumn("Понизить приоритет", CommonBaseDefine.ICO_DOWN, "onClickDown").setOrderable(false));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete").setOrderable(false));
        return dataSource;
    }

    public static void processClickUp(List<RequestedEnrollmentDirection> list, Long id)
    {
        int i = 0;
        while (i < list.size() && !list.get(i).getId().equals(id))
            i++;
        if (i == list.size() || i == 0)
            return;
        RequestedEnrollmentDirection item = list.get(i - 1);
        list.set(i - 1, list.get(i));
        list.set(i, item);
    }

    public static void processClickDown(List<RequestedEnrollmentDirection> list, Long id)
    {
        int i = 0;
        while (i < list.size() && !list.get(i).getId().equals(id))
            i++;
        if (i == list.size() || i == list.size() - 1)
            return;
        RequestedEnrollmentDirection item = list.get(i + 1);
        list.set(i + 1, list.get(i));
        list.set(i, item);
    }

    public static RequestedEnrollmentDirection processClickDelete(List<RequestedEnrollmentDirection> list, Long id, Set<Long> existedIds, List<RequestedEnrollmentDirection> forDeleteList)
    {
        int i = 0;
        while (i < list.size() && !list.get(i).getId().equals(id))
            i++;
        if (i == list.size())
            return null;
        RequestedEnrollmentDirection forDelete = list.get(i);
        if (existedIds.contains(forDelete.getId()))
            forDeleteList.add(forDelete);
        list.remove(i);
        return forDelete;
    }

    /**
     * Определяет какие направления приема и по каким видам возмещения затрат можно выбрать
     *
     * @param session                  hiberante session
     * @param directionForAddingList   Направления приема которые хотелось бы добавить в заявление, однако не все из них можно будет добавить
     * @param compensationType         вид затрат, либо null если абитуриент участвует в конкурсе на контракт
     * @param selectedRequestedEnrollmentDirectionList
     *                                 список уже выбранных направлений приема
     * @param directionPerCompTypeDiff флаг приемной кампании (true, если можно выбирать одинаковые направления на разные виды затрат)
     * @param stateExamRestriction     true, если на форме включен флаг, что свидетельства ЕГЭ накладывают ограничения на выбор направлений
     * @param entrant                  абитуриент
     * @param forDeleteList            список направлений приема помеченных для удаления
     * @return список из [направление приема, вид затрат] которые РЕАЛЬНО уже можно добавить абитуриенту
     */
    public static List<MultiKey> getAddingEnrollmentDirections(Session session,
        List<EnrollmentDirection> directionForAddingList,
        CompensationType compensationType,
        List<RequestedEnrollmentDirection> selectedRequestedEnrollmentDirectionList,
        boolean directionPerCompTypeDiff,
        boolean stateExamRestriction,
        Entrant entrant,
        List<RequestedEnrollmentDirection> forDeleteList)
        {
        List<MultiKey> result = new ArrayList<>();
        CompensationType budget = UniDaoFacade.getCoreDao().getCatalogItem(CompensationType.class, UniDefines.COMPENSATION_TYPE_BUDGET);
        CompensationType contract = UniDaoFacade.getCoreDao().getCatalogItem(CompensationType.class, UniDefines.COMPENSATION_TYPE_CONTRACT);

        for (EnrollmentDirection item : directionForAddingList)
        {
            if (compensationType != null)
            {
                // если на направлении приема есть прием на compensationType
                if ((item.isBudget() && compensationType.isBudget()) || (item.isContract() && !compensationType.isBudget()))
                    result.add(new MultiKey(item, compensationType));
            } else
            {
                // для каждого вида затрат на который есть прием на направлении приема
                if (item.isBudget())
                    result.add(new MultiKey(item, budget));
                if (item.isContract())
                    result.add(new MultiKey(item, contract));
            }
        }

        // получаем выбранные направления приема (направление приема + вид затрат)
        // если нельзя зачислять на бюджет и контракт одновременно, то добавляем ключ с направлением приема как на бюджет, так и на контракт
        Set<MultiKey> selected = new HashSet<>();
        for (RequestedEnrollmentDirection reqItem : selectedRequestedEnrollmentDirectionList)
        {
            if (directionPerCompTypeDiff)
            {
                selected.add(new MultiKey(reqItem.getEnrollmentDirection(), reqItem.getCompensationType()));
            } else
            {
                selected.add(new MultiKey(reqItem.getEnrollmentDirection(), budget));
                selected.add(new MultiKey(reqItem.getEnrollmentDirection(), contract));
            }
        }

        // получаем вообще все направления приема абитуриента по всем видам затрат, исключая, удаленные с формы
        // если нельзя зачислять на бюджет и контракт одновременно, то добавляем ключ с направлением приема как на бюджет, так и на контракт
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "r");
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT, entrant));
        if (forDeleteList.size() > 0)
            builder.add(new InExpression("r", forDeleteList.toArray(new RequestedEnrollmentDirection[forDeleteList.size()]), "not in"));

        for (RequestedEnrollmentDirection reqItem : builder.<RequestedEnrollmentDirection>getResultList(session))
        {
            if (directionPerCompTypeDiff)
            {
                selected.add(new MultiKey(reqItem.getEnrollmentDirection(), reqItem.getCompensationType()));
            } else
            {
                selected.add(new MultiKey(reqItem.getEnrollmentDirection(), budget));
                selected.add(new MultiKey(reqItem.getEnrollmentDirection(), contract));
            }
        }

        // исключаем направления приема, которые уже выбраны
        result.removeAll(selected);

        if (stateExamRestriction)
        {
            Set<EnrollmentDirection> coveredList = UniecDAOFacade.getExamSetDAO().getCoveredEnrollmentDirection(entrant);

            // исключаем направления приема, которые не покрываются сертификатами ЕГЭ
            for (Iterator<MultiKey> iterator = result.iterator(); iterator.hasNext(); )
                if (!coveredList.contains(iterator.next().getKey(0)))
                    iterator.remove();
        }

        return result;
        }

    /**
     * Вставляет только что выбранные направления в уже выбранные так, что
     * только что выбранные по бюджету оказываются после последнего по бюджету в уже выбранных
     * только что выбранные по контракту оказываются после последнего по контракту в уже выбранных
     *
     * @param alreadySelected уже выбранные направления приема
     * @param justSelected    только что выбранные направления приема (список должен быть из двух частей - по бюджету и по контракту)
     */
    public static void mergeSelectedDirections(List<RequestedEnrollmentDirection> alreadySelected, List<RequestedEnrollmentDirection> justSelected)
    {
        // разбиваем список только что выбранных направлений на две части
        List<RequestedEnrollmentDirection> budgetList = new ArrayList<>();
        List<RequestedEnrollmentDirection> contractList = new ArrayList<>();
        for (RequestedEnrollmentDirection item : justSelected)
            if (item.getCompensationType().isBudget())
                budgetList.add(item);
            else
                contractList.add(item);

        // ищем последнее направление по бюджету
        int lastBudgetIndex = alreadySelected.size() - 1;
        while (lastBudgetIndex >= 0 && !alreadySelected.get(lastBudgetIndex).getCompensationType().isBudget())
            lastBudgetIndex--;

        // пытаемся вставить направления на бюджет
        alreadySelected.addAll(lastBudgetIndex + 1, budgetList);

        // ищем последнее направление по контракту
        int lastContractIndex = alreadySelected.size() - 1;
        while (lastContractIndex >= 0 && alreadySelected.get(lastContractIndex).getCompensationType().isBudget())
            lastContractIndex--;

        // пытаемся вставить направления на контракт (если нет контр. то добавляем в самый конец)
        if (lastContractIndex < 0) lastContractIndex = alreadySelected.size() - 1;
        alreadySelected.addAll(lastContractIndex + 1, contractList);
    }

    /**
     * Проверяет добавляемые напр. подготовки по всем ограничениям, которые есть в данной приемной кампании.
     * Добавляет все ошибки в ErrorCollector.
     *
     * @param session        сессия
     * @param directions     направления подготовки (специальности), которые хотим добавить абитуриенту
     * @param entrantRequest абитуриент
     * @return true если нет ошибок, false в противном случае
     */
    public static boolean validateRequestedDirections(Session session, List<RequestedEnrollmentDirection> directions, EntrantRequest entrantRequest)
    {
        return validateRequestedDirections(session, directions, Collections.<RequestedEnrollmentDirection>emptyList(), entrantRequest);
    }

    /**
     * Проверяет добавляемые напр. подготовки по всем ограничениям, которые есть в данной приемной кампании.
     * Добавляет все ошибки в ErrorCollector.
     *
     * @param session        сессия
     * @param directions     направления подготовки (специальности), которые хотим добавить абитуриенту
     * @param forDelete      направления подготовки (специальности), которые будем удалять у абитуриента
     * @param entrantRequest заявление абитуриента
     * @return true если нет ошибок, false в противном случае
     */
    public static boolean validateRequestedDirections(Session session, List<RequestedEnrollmentDirection> directions, List<RequestedEnrollmentDirection> forDelete, EntrantRequest entrantRequest)
    {
        EnrollmentCampaign ec = entrantRequest.getEntrant().getEnrollmentCampaign();
        ErrorCollector errorCollector = ContextLocal.getUserContext().getErrorCollector();

        // получаем направления, которые __теоретически__ должны получиться
        Set<RequestedEnrollmentDirection> theoreticalDirectionSet = new HashSet<>();
        if (entrantRequest.getId() != null)
            theoreticalDirectionSet.addAll(session.createCriteria(RequestedEnrollmentDirection.class).add(Restrictions.eq(RequestedEnrollmentDirection.L_ENTRANT_REQUEST, entrantRequest)).list()); // то, что сейчас в базе
        theoreticalDirectionSet.addAll(directions);                                                                                                                                                 // то, что выбрали на форме (добавлятся только те, что были на форме и не в базе)
        theoreticalDirectionSet.removeAll(forDelete);                                                                                                                                               // исключаем удаляемые

        if (ec.isUseCompetitionGroup())
        {
            Set<CompetitionGroup> competitionGroupSet = new HashSet<>();
            for (RequestedEnrollmentDirection item : theoreticalDirectionSet)
                if (item.getEnrollmentDirection().getCompetitionGroup() != null)
                    competitionGroupSet.add(item.getEnrollmentDirection().getCompetitionGroup());

            if (ec.getMaxCompetitionGroup() != null && competitionGroupSet.size() > ec.getMaxCompetitionGroup())
                errorCollector.add("Нельзя выбрать абитуриенту указанные направления подготовки (специальности), т.к. общее число выбранных для абитуриента конкурсных групп в заявлении не может превышать " + ec.getMaxCompetitionGroup() + ".");

            // считаем статистику в группах [форма освоения, вид затрат] -> [КГ]
            Map<MultiKey, Set<CompetitionGroup>> key2data = new HashMap<>();
            for (RequestedEnrollmentDirection item : theoreticalDirectionSet)
            {
                CompetitionGroup competitionGroup = item.getEnrollmentDirection().getCompetitionGroup();
                if (competitionGroup != null)
                {
                    MultiKey key = new MultiKey(item.getEnrollmentDirection().getEducationOrgUnit().getDevelopForm(), item.getCompensationType());
                    Set<CompetitionGroup> set = key2data.get(key);
                    if (set == null)
                        key2data.put(key, set = new HashSet<>());
                    set.add(competitionGroup);
                }
            }

            // Ограничения по формам освоения и видам возмещения затрат
            for (CompetitionGroupRestriction restriction : (List<CompetitionGroupRestriction>) session.createCriteria(CompetitionGroupRestriction.class).add(Restrictions.eq(CompetitionGroupRestriction.L_ENROLLMENT_CAMPAIGN, ec)).list())
            {
                session.refresh(restriction);
                MultiKey key = new MultiKey(restriction.getDevelopForm(), restriction.getCompensationType());
                Set<CompetitionGroup> set = key2data.get(key);
                if (set != null)
                {
                    if (restriction.getMaxCompetitionGroup() != null && set.size() > restriction.getMaxCompetitionGroup())
                        errorCollector.add("Нельзя выбрать абитуриенту указанные направления подготовки (специальности), т.к. для формы освоения (" + restriction.getDevelopForm().getTitle() + ") и вида возмещения затрат (" + restriction.getCompensationType().getShortTitle() + ") общее число выбранных конкурсных групп для абитуриента не может превышать " + restriction.getMaxCompetitionGroup() + ".");
                }
            }
        } else
        {
            // получаем направления по классификатору, которые __теоретически__ должны получиться
            Map<RequestedEnrollmentDirection, EducationLevels> direction2ministerialDirectionMap = new HashMap<>();
            Set<EnrollmentDirection> enrollmentDirectionSet = new HashSet<>();
            Set<EducationLevels> ministerialDirectionSet = new HashSet<>();
            for (RequestedEnrollmentDirection item : theoreticalDirectionSet)
            {
                EducationLevels eduLevel = item.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();
                while (eduLevel != null && eduLevel.getParentLevel() != null)
                    eduLevel = eduLevel.getParentLevel();
                if (eduLevel == null)
                    throw new RuntimeException("Can't find ministerial direction for RequestedEnrollmentDirection.id='" + item.getId() + "'");
                enrollmentDirectionSet.add(item.getEnrollmentDirection());
                ministerialDirectionSet.add(eduLevel);
                direction2ministerialDirectionMap.put(item, eduLevel);
            }

            // считаем статистику в группах [форма освоения, вид затрат] -> [ВНП, направления по классификатору, формирующие подр.]
            final int ENROLLMENT_DIRECTION_INDEX = 0;  // кол-во ВНП
            final int MINISTERIAL_DIRECTION_INDEX = 1; // кол-во направлений по классификатору
            final int FORMATIVE_ORGUNIT_INDEX = 2;     // кол-во формирующих подр.
            Map<MultiKey, Set[]> key2data = new HashMap<>();
            for (RequestedEnrollmentDirection item : theoreticalDirectionSet)
            {
                MultiKey key = new MultiKey(item.getEnrollmentDirection().getEducationOrgUnit().getDevelopForm(), item.getCompensationType());
                Set[] data = key2data.get(key);
                if (data == null)
                    key2data.put(key, data = new Set[]{new HashSet(), new HashSet(), new HashSet()});

                EducationLevels eduLevel = direction2ministerialDirectionMap.get(item);
                if (eduLevel == null)
                    throw new RuntimeException("Can't find ministerial direction for RequestedEnrollmentDirection.id='" + item.getId() + "'");

                data[ENROLLMENT_DIRECTION_INDEX].add(item.getEnrollmentDirection());
                data[MINISTERIAL_DIRECTION_INDEX].add(eduLevel);
                data[FORMATIVE_ORGUNIT_INDEX].add(item.getEnrollmentDirection().getEducationOrgUnit().getFormativeOrgUnit());

            }

            // Число выбираемых в заявлении направлений подготовки (специальностей) для приема
            if (ec.getMaxEnrollmentDirection() != null && enrollmentDirectionSet.size() > ec.getMaxEnrollmentDirection())
                errorCollector.add("Нельзя выбрать абитуриенту указанное направление подготовки (специальность), т.к. общее число выбранных абитуриентом направлений подготовки для приема в заявлении не может превышать " + ec.getMaxEnrollmentDirection() + ".");

            // Число выбираемых в заявлении направлений подготовки (специальностей) по классификатору
            if (ec.getMaxMinisterialDirection() != null && ministerialDirectionSet.size() > ec.getMaxMinisterialDirection())
                errorCollector.add("Нельзя выбрать абитуриенту указанное направление подготовки (специальность), т.к. общее число выбранных абитуриентом направлений подготовки по классификатору в заявлении не может превышать " + ec.getMaxMinisterialDirection() + ".");

            // Ограничения по формам освоения и видам возмещения затрат
            for (EnrollmentDirectionRestriction restriction : (List<EnrollmentDirectionRestriction>) session.createCriteria(EnrollmentDirectionRestriction.class).add(Restrictions.eq(EnrollmentDirectionRestriction.L_ENROLLMENT_CAMPAIGN, ec)).list())
            {
                session.refresh(restriction);
                MultiKey key = new MultiKey(restriction.getDevelopForm(), restriction.getCompensationType());
                Set[] data = key2data.get(key);
                if (data != null)
                {
                    if (restriction.getMaxEnrollmentDirection() != null && data[ENROLLMENT_DIRECTION_INDEX].size() > restriction.getMaxEnrollmentDirection())
                        errorCollector.add("Нельзя выбрать абитуриенту указанное направление подготовки (специальность), т.к. для заданных формы освоения и вида возмещения затрат (" + restriction.getDevelopForm().getTitle() + ", " + restriction.getCompensationType().getShortTitle() + ") общее число выбранных абитуриентом направлений подготовки для приема не может превышать " + restriction.getMaxEnrollmentDirection() + ".");

                    if (restriction.getMaxMinisterialDirection() != null && data[MINISTERIAL_DIRECTION_INDEX].size() > restriction.getMaxMinisterialDirection())
                        errorCollector.add("Нельзя выбрать абитуриенту указанное направление подготовки (специальность), т.к. для заданных формы освоения и вида возмещения затрат (" + restriction.getDevelopForm().getTitle() + ", " + restriction.getCompensationType().getShortTitle() + ") общее число выбранных абитуриентом направлений подготовки по классификатору не может превышать " + restriction.getMaxMinisterialDirection() + ".");

                    if (restriction.getMaxFormativeOrgUnit() != null && data[FORMATIVE_ORGUNIT_INDEX].size() > restriction.getMaxFormativeOrgUnit())
                        errorCollector.add("Нельзя выбрать абитуриенту указанное направление подготовки (специальность), т.к. для заданных формы освоения и вида возмещения затрат (" + restriction.getDevelopForm().getTitle() + ", " + restriction.getCompensationType().getShortTitle() + ") общее число выбранных абитуриентом формирующих подразделений не может превышать " + restriction.getMaxFormativeOrgUnit() + ".");
                }
            }
        }

        // получаем ВНП из остальных заявлений абитуриента
        DQLSelectBuilder builder = new DQLSelectBuilder()
        .fromEntity(RequestedEnrollmentDirection.class, "e")
        .where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().entrant().fromAlias("e")), DQLExpressions.value(entrantRequest.getEntrant())));
        if (entrantRequest.getId() != null)
            builder.where(DQLExpressions.ne(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().fromAlias("e")), DQLExpressions.value(entrantRequest)));

        Set<RequestedEnrollmentDirection> theoreticalFullDirectionSet = new HashSet<>();
        theoreticalFullDirectionSet.addAll(theoreticalDirectionSet);                                               // из текущего заявления
        theoreticalFullDirectionSet.addAll(builder.createStatement(session).<RequestedEnrollmentDirection>list()); // из остальных заявлений

        // Число выбираемых направлений подготовки (специальностей) по целевому приему
        if (ec.isOneDirectionForTargetAdmission())
        {
            int count = 0;
            for (RequestedEnrollmentDirection item : theoreticalFullDirectionSet)
                if (item.isTargetAdmission())
                    count++;

            if (count > 1)
                errorCollector.add("Нельзя выбрать абитуриенту более одного направления подготовки (специальности) с условием поступления по целевому приему.");
        }

        // Число выбираемых направлений подготовки (специальностей) по внеконкурсному приему
        if (ec.isOneDirectionForOutOfCompetition())
        {
            int count = 0;
            for (RequestedEnrollmentDirection item : theoreticalFullDirectionSet)
                if (UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION.equals(item.getCompetitionKind().getCode()))
                    count++;

            if (count > 1)
                errorCollector.add("Нельзя выбрать абитуриенту более одного направления подготовки (специальности) с условием поступления по внеконкурсному приему.");
        }

        return !errorCollector.hasErrors();
    }
}
