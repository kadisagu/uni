/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.report.EnrollmentResults3NK.EnrollmentResults3NKList;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.report.EnrollmentResults3NKReport;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

/**
 * @author agolubenko
 * @since 24.04.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.setCompensationTypeList(getCatalogItemListOrderByCode(CompensationType.class));
        model.setDevelopFormList(EducationCatalogsManager.getDevelopFormSelectModel());
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        EnrollmentCampaign enrollmentCampaign = (EnrollmentCampaign) model.getSettings().get("enrollmentCampaign");
        CompensationType compensationType = (CompensationType) model.getSettings().get("compensationType");
        DevelopForm developForm = (DevelopForm) model.getSettings().get("developForm");

        MQBuilder builder = new MQBuilder(EnrollmentResults3NKReport.ENTITY_CLASS, "report");
        builder.add(MQExpression.eq("report", EnrollmentResults3NKReport.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign));
        if (compensationType != null)
            builder.add(MQExpression.eq("report", EnrollmentResults3NKReport.L_COMPENSATION_TYPE, compensationType));
        if (developForm != null)
            builder.add(MQExpression.eq("report", EnrollmentResults3NKReport.L_DEVELOP_FORM, developForm));
        new OrderDescriptionRegistry("report").applyOrder(builder, model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }
}
