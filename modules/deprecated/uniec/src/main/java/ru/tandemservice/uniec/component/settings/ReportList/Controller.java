/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.settings.ReportList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrderType;

/**
 * @author ekachanova
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<DAO.ReportDefinitionWrapper> dataSource = UniBaseUtils.createDataSource(component, getDao());

        dataSource.addColumn(new SimpleColumn("Название отчета", "title").setOrderable(false).setClickable(false));

        dataSource.addColumn(new ToggleColumn("Использовать", EnrollmentOrderType.P_USED).setListener("onClickUsed"));

        model.setDataSource(dataSource);
    }

    public void onClickUsed(IBusinessComponent component)
    {
        DAO.ReportDefinitionWrapper wrapper =  getModel(component).getWrapperById().get((Long)component.getListenerParameter());
        IDataSettings settings = DataSettingsFacade.getSettings(UniecDefines.EC_MODULE_OWNER, UniecDefines.IS_REPORT_USED);
        settings.getEntry(wrapper.getReportDefinitionId()).setValue(!wrapper.isUsed());
        DataSettingsFacade.saveSettings(settings);
        getModel(component).getDataSource().refresh();
    }
}
