/* $Id: DAO.java 13269 2010-06-17 15:59:00Z oleyba $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.stateExamEnrollmentResults2010.StateExamEnrollmentResultsAdd;

import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import org.tandemframework.shared.commonbase.utils.CommonYesNoUIObject;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.Arrays;
import java.util.Date;

/**
 * @author vip_delete
 * @since 20.04.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    @SuppressWarnings("deprecation")
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.getReport().setDateFrom(model.getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : model.getEnrollmentCampaign().getStartDate());
        model.getReport().setDateTo(CoreDateUtils.getDayLastTimeMoment(new Date()));
        model.setPrintFormList(Arrays.asList(new IdentifiableWrapper(Model.PRINT_FORM_HIGH, "Форма 1 (ВПО)"), new IdentifiableWrapper(Model.PRINT_FORM_MIDDLE, "Форма 2 (СПО)")));
        model.setPrintForm(model.getPrintFormList().get(0));
        model.setEnrollmentCampaignStageList(Arrays.asList(new IdentifiableWrapper(Model.ENROLLMENT_CAMP_STAGE_DOCUMENTS, "по ходу приема документов"), new IdentifiableWrapper(Model.ENROLLMENT_CAMP_STAGE_EXAMS, "по результатам сдачи вступительных испытаний"), new IdentifiableWrapper(Model.ENROLLMENT_CAMP_STAGE_ENROLLMENT, "по результатам зачисления")));
        model.setReportAlgorithmList(Arrays.asList(new IdentifiableWrapper(Model.REPORT_ALGORITHM_EXAM, "Экзаменов"), new IdentifiableWrapper(Model.REPORT_ALGORITHM_STATE_EXAM, "В свидетельствах ЕГЭ")));
        model.setStateExamPeriodList(Arrays.asList(new IdentifiableWrapper(Model.STATE_EXAM_PERIOD_WAVE_1, "1-ая волна"), new IdentifiableWrapper(Model.STATE_EXAM_PERIOD_WAVE_2, "2-ая волна")));
        model.setStudentCategoryListModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(StudentCategory.class)));
        model.setQualificationListModel(new QualificationModel(getSession()));
        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));
        model.setDevelopFormList(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setDevelopConditionListModel(EducationCatalogsManager.getDevelopConditionSelectModel());
        model.setFormativeOrgUnitListModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING));
        model.setTerritorialOrgUnitListModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL));
        model.setParallelList(CommonYesNoUIObject.createYesNoList());
    }

    @Override
    public void update(Model model)
    {
        // строим отчет
        Session session = getSession();

        // создаем билдер excel файла
        IStateExamEnrollmentExcelBuilder excelBuilder = model.getPrintForm().getId() == Model.PRINT_FORM_HIGH ?
                new StateExamEnrollmentReportHighExcelBuilder() :
                new StateExamEnrollmentReportMiddleExcelBuilder();

        // сохраняем файл отчета
        DatabaseFile databaseFile = new StateExamEnrollmentReportBuilder(model, session).getContent(excelBuilder);
        session.save(databaseFile);

        StateExamEnrollmentReport2010 report = model.getReport();
        report.setContent(databaseFile);
        report.setFormingDate(new Date());
        report.setPrintForm(model.getPrintForm().getTitle());
        report.setEnrollmentCampStage(model.getEnrollmentCampaignStage().getTitle());
        report.setReportAlgorithm(model.getReportAlgorithm().getTitle());
        if (model.isStateExamPeriodActive())
            model.getReport().setStateExamPeriod(model.getStateExamPeriod().getTitle());
        if (model.isStudentCategoryActive())
            report.setStudentCategoryTitle(UniStringUtils.join(model.getStudentCategoryList(), StudentCategory.P_TITLE, "; "));
        if (model.isQualificationActive())
            report.setQualificationTitle(UniStringUtils.join(model.getQualificationList(), Qualifications.P_TITLE, "; "));
        if (model.isDevelopConditionActive())
            report.setDevelopConditionTitle(UniStringUtils.join(model.getDevelopConditionList(), DevelopCondition.P_TITLE, "; "));
        if (model.isFormativeOrgUnitActive())
            report.setFormativeOrgUnitTitle(UniStringUtils.join(model.getFormativeOrgUnitList(), OrgUnit.P_FULL_TITLE, "; "));
        if (model.isTerritorialOrgUnitActive())
            report.setTerritorialOrgUnitTitle(UniStringUtils.join(model.getTerritorialOrgUnitList(), OrgUnit.P_TERRITORIAL_FULL_TITLE, "; "));
        if (model.isParallelActive())
            report.setExcludeParallel(model.getParallel().isTrue());
        session.save(report);
    }
}
