/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.util;

import java.util.Comparator;
import java.util.Map;

import org.tandemframework.shared.commonbase.utils.CommonCollator;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;

/**
 * Алгоритм сортировки аналогичный RequestedEnrollmentDirectionComparator,
 * но только оптимизированный для механизма распределений
 *
 * @author Vasily Zhukov
 * @since 23.07.2011
 */
public class EcgContegeComparator implements Comparator<IEcgCortegeDTO>
{
    private Map<CompetitionKind, Integer> _competitionKindPriorityMap;

    public EcgContegeComparator(Map<CompetitionKind, Integer> competitionKindPriorityMap)
    {
        _competitionKindPriorityMap = competitionKindPriorityMap;
    }

    @Override
    public int compare(IEcgCortegeDTO o1, IEcgCortegeDTO o2)
    {
        int result;

        if (o1.isTargetAdmission() && o2.isTargetAdmission())
        {
            // // если оба по целевому приему, то сравниваем по приоритетам целевого приема
            result = TargetAdmissionKind.ALL_LEVEL_PRIORITY_COMPARATOR.compare(o1.getTargetAdmissionKind(), o2.getTargetAdmissionKind());
            if (result != 0)
                return result;

            // сравниваем по приоритетам видов конкурса
            result = _competitionKindPriorityMap.get(o1.getCompetitionKind()) - _competitionKindPriorityMap.get(o2.getCompetitionKind());
            if (result != 0)
                return result;
        } else
        {
            // получаем виды конкурса, для целевого приема — null
            CompetitionKind c1 = o1.isTargetAdmission() ? null : o1.getCompetitionKind();
            CompetitionKind c2 = o2.isTargetAdmission() ? null : o2.getCompetitionKind();

            // сравнить по приоритетам видов конкурса
            result = _competitionKindPriorityMap.get(c1) - _competitionKindPriorityMap.get(c2);
            if (result != 0)
                return result;
        }

        // сравниваем финальные оценки
        result = -UniBaseUtils.compare(o1.getFinalMark(), o2.getFinalMark(), true);
        if (result != 0)
            return result;

        // сравниваем профильные оценки
        result = -UniBaseUtils.compare(o1.getProfileMark(), o2.getProfileMark(), true);
        if (result != 0)
            return result;

        // сравниваем окончание профильного учреждения
        result = -UniBaseUtils.compare(o1.isGraduatedProfileEduInstitution(), o2.isGraduatedProfileEduInstitution(), true);
        if (result != 0)
            return result;

        // сравниваем средний балл аттестата
        result = -UniBaseUtils.compare(o1.getCertificateAverageMark(), o2.getCertificateAverageMark(), true);
        if (result != 0)
            return result;

        // сравниваем фио
        return CommonCollator.RUSSIAN_COLLATOR.compare(o1.getFio(), o2.getFio());
    }
}
