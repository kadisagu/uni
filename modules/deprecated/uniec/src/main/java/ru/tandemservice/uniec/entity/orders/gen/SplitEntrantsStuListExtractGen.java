package ru.tandemservice.uniec.entity.orders.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.base.entity.ecgp.EcgpEntrantRecommended;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.SplitEntrantsStuListExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Абстрактный проект приказа «О распределении зачисленных абитуриентов по профилям»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SplitEntrantsStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.orders.SplitEntrantsStuListExtract";
    public static final String ENTITY_NAME = "splitEntrantsStuListExtract";
    public static final int VERSION_HASH = 37532434;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_EXTRACT = "enrollmentExtract";
    public static final String L_ECGP_ENTRANT_RECOMMENDED = "ecgpEntrantRecommended";
    public static final String L_EDUCATION_ORG_UNIT_NEW = "educationOrgUnitNew";
    public static final String L_EDUCATION_ORG_UNIT_OLD = "educationOrgUnitOld";

    private EnrollmentExtract _enrollmentExtract;     // Выписка приказа о зачислении абитуриентов
    private EcgpEntrantRecommended _ecgpEntrantRecommended;     // Рекомендованный абитуриент распределения по профилям
    private EducationOrgUnit _educationOrgUnitNew;     // Параметры обучения студентов по направлению подготовки (НПП)
    private EducationOrgUnit _educationOrgUnitOld;     // Параметры обучения студентов по направлению подготовки (НПП)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка приказа о зачислении абитуриентов. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentExtract getEnrollmentExtract()
    {
        return _enrollmentExtract;
    }

    /**
     * @param enrollmentExtract Выписка приказа о зачислении абитуриентов. Свойство не может быть null.
     */
    public void setEnrollmentExtract(EnrollmentExtract enrollmentExtract)
    {
        dirty(_enrollmentExtract, enrollmentExtract);
        _enrollmentExtract = enrollmentExtract;
    }

    /**
     * @return Рекомендованный абитуриент распределения по профилям. Свойство не может быть null.
     */
    @NotNull
    public EcgpEntrantRecommended getEcgpEntrantRecommended()
    {
        return _ecgpEntrantRecommended;
    }

    /**
     * @param ecgpEntrantRecommended Рекомендованный абитуриент распределения по профилям. Свойство не может быть null.
     */
    public void setEcgpEntrantRecommended(EcgpEntrantRecommended ecgpEntrantRecommended)
    {
        dirty(_ecgpEntrantRecommended, ecgpEntrantRecommended);
        _ecgpEntrantRecommended = ecgpEntrantRecommended;
    }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     */
    @NotNull
    public EducationOrgUnit getEducationOrgUnitNew()
    {
        return _educationOrgUnitNew;
    }

    /**
     * @param educationOrgUnitNew Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     */
    public void setEducationOrgUnitNew(EducationOrgUnit educationOrgUnitNew)
    {
        dirty(_educationOrgUnitNew, educationOrgUnitNew);
        _educationOrgUnitNew = educationOrgUnitNew;
    }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     */
    @NotNull
    public EducationOrgUnit getEducationOrgUnitOld()
    {
        return _educationOrgUnitOld;
    }

    /**
     * @param educationOrgUnitOld Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     */
    public void setEducationOrgUnitOld(EducationOrgUnit educationOrgUnitOld)
    {
        dirty(_educationOrgUnitOld, educationOrgUnitOld);
        _educationOrgUnitOld = educationOrgUnitOld;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SplitEntrantsStuListExtractGen)
        {
            setEnrollmentExtract(((SplitEntrantsStuListExtract)another).getEnrollmentExtract());
            setEcgpEntrantRecommended(((SplitEntrantsStuListExtract)another).getEcgpEntrantRecommended());
            setEducationOrgUnitNew(((SplitEntrantsStuListExtract)another).getEducationOrgUnitNew());
            setEducationOrgUnitOld(((SplitEntrantsStuListExtract)another).getEducationOrgUnitOld());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SplitEntrantsStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SplitEntrantsStuListExtract.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("SplitEntrantsStuListExtract is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentExtract":
                    return obj.getEnrollmentExtract();
                case "ecgpEntrantRecommended":
                    return obj.getEcgpEntrantRecommended();
                case "educationOrgUnitNew":
                    return obj.getEducationOrgUnitNew();
                case "educationOrgUnitOld":
                    return obj.getEducationOrgUnitOld();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "enrollmentExtract":
                    obj.setEnrollmentExtract((EnrollmentExtract) value);
                    return;
                case "ecgpEntrantRecommended":
                    obj.setEcgpEntrantRecommended((EcgpEntrantRecommended) value);
                    return;
                case "educationOrgUnitNew":
                    obj.setEducationOrgUnitNew((EducationOrgUnit) value);
                    return;
                case "educationOrgUnitOld":
                    obj.setEducationOrgUnitOld((EducationOrgUnit) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentExtract":
                        return true;
                case "ecgpEntrantRecommended":
                        return true;
                case "educationOrgUnitNew":
                        return true;
                case "educationOrgUnitOld":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentExtract":
                    return true;
                case "ecgpEntrantRecommended":
                    return true;
                case "educationOrgUnitNew":
                    return true;
                case "educationOrgUnitOld":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentExtract":
                    return EnrollmentExtract.class;
                case "ecgpEntrantRecommended":
                    return EcgpEntrantRecommended.class;
                case "educationOrgUnitNew":
                    return EducationOrgUnit.class;
                case "educationOrgUnitOld":
                    return EducationOrgUnit.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SplitEntrantsStuListExtract> _dslPath = new Path<SplitEntrantsStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SplitEntrantsStuListExtract");
    }
            

    /**
     * @return Выписка приказа о зачислении абитуриентов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.SplitEntrantsStuListExtract#getEnrollmentExtract()
     */
    public static EnrollmentExtract.Path<EnrollmentExtract> enrollmentExtract()
    {
        return _dslPath.enrollmentExtract();
    }

    /**
     * @return Рекомендованный абитуриент распределения по профилям. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.SplitEntrantsStuListExtract#getEcgpEntrantRecommended()
     */
    public static EcgpEntrantRecommended.Path<EcgpEntrantRecommended> ecgpEntrantRecommended()
    {
        return _dslPath.ecgpEntrantRecommended();
    }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.SplitEntrantsStuListExtract#getEducationOrgUnitNew()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitNew()
    {
        return _dslPath.educationOrgUnitNew();
    }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.SplitEntrantsStuListExtract#getEducationOrgUnitOld()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitOld()
    {
        return _dslPath.educationOrgUnitOld();
    }

    public static class Path<E extends SplitEntrantsStuListExtract> extends ListStudentExtract.Path<E>
    {
        private EnrollmentExtract.Path<EnrollmentExtract> _enrollmentExtract;
        private EcgpEntrantRecommended.Path<EcgpEntrantRecommended> _ecgpEntrantRecommended;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnitNew;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnitOld;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка приказа о зачислении абитуриентов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.SplitEntrantsStuListExtract#getEnrollmentExtract()
     */
        public EnrollmentExtract.Path<EnrollmentExtract> enrollmentExtract()
        {
            if(_enrollmentExtract == null )
                _enrollmentExtract = new EnrollmentExtract.Path<EnrollmentExtract>(L_ENROLLMENT_EXTRACT, this);
            return _enrollmentExtract;
        }

    /**
     * @return Рекомендованный абитуриент распределения по профилям. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.SplitEntrantsStuListExtract#getEcgpEntrantRecommended()
     */
        public EcgpEntrantRecommended.Path<EcgpEntrantRecommended> ecgpEntrantRecommended()
        {
            if(_ecgpEntrantRecommended == null )
                _ecgpEntrantRecommended = new EcgpEntrantRecommended.Path<EcgpEntrantRecommended>(L_ECGP_ENTRANT_RECOMMENDED, this);
            return _ecgpEntrantRecommended;
        }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.SplitEntrantsStuListExtract#getEducationOrgUnitNew()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitNew()
        {
            if(_educationOrgUnitNew == null )
                _educationOrgUnitNew = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT_NEW, this);
            return _educationOrgUnitNew;
        }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.SplitEntrantsStuListExtract#getEducationOrgUnitOld()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitOld()
        {
            if(_educationOrgUnitOld == null )
                _educationOrgUnitOld = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT_OLD, this);
            return _educationOrgUnitOld;
        }

        public Class getEntityClass()
        {
            return SplitEntrantsStuListExtract.class;
        }

        public String getEntityName()
        {
            return "splitEntrantsStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
