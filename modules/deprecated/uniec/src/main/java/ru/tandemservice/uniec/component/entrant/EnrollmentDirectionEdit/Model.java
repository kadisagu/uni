/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EnrollmentDirectionEdit;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tandemframework.core.component.Input;

import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

/**
 * @author agolubenko
 * @since 04.07.2008
 */
@Input(keys = "enrollmentDirectionId", bindings = "enrollmentDirection.id")
public class Model
{
    public static final String TARGET_ADM_PLAN_BUDGET_COLUMN = "targetAdmPlanColBudget";
    public static final String TARGET_ADM_PLAN_CONTRACT_COLUMN = "targetAdmPlanColContract";
    private EnrollmentDirection _enrollmentDirection = new EnrollmentDirection();
    private boolean _simpleTargetAdmissionKind;

    private List<TargetAdmissionKind> targetAdmissionKindList;
    private Map<TargetAdmissionKind, Integer> valueMapBudget = new HashMap<TargetAdmissionKind, Integer>();
    private Map<TargetAdmissionKind, Integer> valueMapContract = new HashMap<TargetAdmissionKind, Integer>();

    private TargetAdmissionKind targetAdmissionKind;

    {
        _enrollmentDirection.setEducationOrgUnit(new EducationOrgUnit());
    }

    public Integer getTargetAdmissionKindValueBudget()
    {
        return getValueMapBudget().get(getTargetAdmissionKind());
    }

    public void setTargetAdmissionKindValueBudget(Integer targetAdmissionKindValueBudget)
    {
        getValueMapBudget().put(getTargetAdmissionKind(), targetAdmissionKindValueBudget);
    }

    public Integer getTargetAdmissionKindValueContract()
    {
        return getValueMapContract().get(getTargetAdmissionKind());
    }

    public void setTargetAdmissionKindValueContract(Integer targetAdmissionKindValueContract)
    {
        getValueMapContract().put(getTargetAdmissionKind(), targetAdmissionKindValueContract);
    }

    // Getters & Setters

    public EnrollmentDirection getEnrollmentDirection()
    {
        return _enrollmentDirection;
    }

    public void setEnrollmentDirection(EnrollmentDirection enrollmentDirection)
    {
        _enrollmentDirection = enrollmentDirection;
    }

    public boolean isSimpleTargetAdmissionKind()
    {
        return _simpleTargetAdmissionKind;
    }

    public void setSimpleTargetAdmissionKind(boolean simpleTargetAdmissionKind)
    {
        _simpleTargetAdmissionKind = simpleTargetAdmissionKind;
    }

    public List<TargetAdmissionKind> getTargetAdmissionKindList()
    {
        return targetAdmissionKindList;
    }

    public void setTargetAdmissionKindList(List<TargetAdmissionKind> targetAdmissionKindList)
    {
        this.targetAdmissionKindList = targetAdmissionKindList;
    }

    public Map<TargetAdmissionKind, Integer> getValueMapBudget()
    {
        return valueMapBudget;
    }

    public void setValueMapBudget(Map<TargetAdmissionKind, Integer> valueMapBudget)
    {
        this.valueMapBudget = valueMapBudget;
    }

    public Map<TargetAdmissionKind, Integer> getValueMapContract()
    {
        return valueMapContract;
    }

    public void setValueMapContract(Map<TargetAdmissionKind, Integer> valueMapContract)
    {
        this.valueMapContract = valueMapContract;
    }

    public TargetAdmissionKind getTargetAdmissionKind()
    {
        return targetAdmissionKind;
    }

    public void setTargetAdmissionKind(TargetAdmissionKind targetAdmissionKind)
    {
        this.targetAdmissionKind = targetAdmissionKind;
    }
}
