/* $Id$ */
package ru.tandemservice.uniec.component.listextract.ent1.e2.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.IAbstractListParagraphPubDAO;
import ru.tandemservice.uniec.entity.orders.SplitContractBachelorEntrantsStuListExtract;

/**
 * @author Dmitry Seleznev
 * @since 21.08.2012
 */
public interface IDAO extends IAbstractListParagraphPubDAO<SplitContractBachelorEntrantsStuListExtract, Model>
{
}