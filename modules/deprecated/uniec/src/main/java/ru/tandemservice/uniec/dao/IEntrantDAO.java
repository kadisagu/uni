/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.dao;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.uniec.entity.catalog.ProfileKnowledge;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.ConversionScale;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Дао для работы с абитуриентом
 *
 * @author agolubenko
 * @since 08.10.2008
 */
public interface IEntrantDAO
{
    final SpringBeanCache<IEntrantDAO> instance = new SpringBeanCache<IEntrantDAO>(IEntrantDAO.class.getName());

    /**
     * получает все оценки абитуриента
     *
     * @param entrant абитуриент
     * @return все оценки абитуриента
     */
    List<ExamPassMark> getExamPassMarkList(Entrant entrant);

    /**
     * получает все выбранные дисциплины приемной кампании абитуриента
     *
     * @param entrant абитуриент
     * @return все выбранные дисциплины приемной кампании абитуриента
     */
    List<ChosenEntranceDiscipline> getChosenEntranceDisciplineList(Entrant entrant);

    /**
     * рассортировывает все оценки абитуриента в Map
     * экзам.лист -> дисциплина -> {оценки по всем формам сдачи}
     *
     * @param examPassMarkList список всех оценок абитуриента (полученный методом getExamPassMarkList)
     * @return все оценки абитуриента
     *         в результат не попадают экзам.листы в которых нет оценок
     */
    Map<EntrantExamList, Map<Discipline2RealizationWayRelation, Set<ExamPassMark>>> getExamPassMarkMap(List<ExamPassMark> examPassMarkList);

    /**
     * получает выбранные дисциплины приемной кампании по каждому заявлению
     *
     * @param entrant абитуриент
     * @return выбранные дисциплины приемной кампании по каждому заявлению
     *         в результат не попадают заявления без выбранных дисциплин приемной кампании
     */
    Map<EntrantRequest, Set<ChosenEntranceDiscipline>> getChosenEntranceDisciplineMap(Entrant entrant);

    /**
     * получает экзам. листы абитуриента по его заявлениям
     * если экзам листы создаются на абитуриента, то возвращается всегда пустой Map
     *
     * @param entrant абитуриент
     * @return экзам. листы по зявлениям абитуриента
     *         в результат не попадают заявления для которых нет экзам. листа
     */
    Map<EntrantRequest, EntrantExamList> getEntrantExamListMap(Entrant entrant);

    /**
     * получает все экзаменационные листы абитуриента по выбранным дисциплинам приемной кампании
     *
     * @param entrant абитуриент
     * @return экзам. листы по выбранным дисциплинам
     *         в результат не попадают выбранные дисциплины для которых нет экзам. листа
     */
    Map<ChosenEntranceDiscipline, EntrantExamList> getEntrantExamLists(Entrant entrant);

    /**
     * получает оценки по ЕГЭ
     *
     * @param entrant абитуриент
     * @return оценки по ЕГЭ
     *         в результат не попадают предметы ЕГЭ для которых оценок нет
     */
    Map<StateExamSubject, Set<StateExamSubjectMark>> getStateExamMarks(Entrant entrant);

    /**
     * получает шкалы перевода абитуриента по дисциплинам
     *
     * @param enrollmentCampaign приемная кампания
     * @return шкалы перевода абитуриента
     *         в результат не попадают дисциплины для которых нет шкалы перевода
     */
    Map<Discipline2RealizationWayRelation, ConversionScale> getConversionScales(EnrollmentCampaign enrollmentCampaign);

    /**
     * получает оценки по апелляциям
     *
     * @param examPassMarkList список всех оценок абитуриента (полученный методом getExamPassMarkList)
     * @return оценки по апелляциям
     *         в результат не попадают оценки для которых нет апелляции
     */
    Map<ExamPassMark, ExamPassMarkAppeal> getExamPassMarkAppeals(List<ExamPassMark> examPassMarkList);

    /**
     * получает все возможные формы сдачи дисциплин по видам возмещения затрат
     * 1. проверяется существует ли такая форма по дисциплине по виду затрат так:
     * realizationFormSet.contains(new MultiKey(discipline, subjectPassForm, compensationType))
     * если содержит, значит существует
     * 2. если разницы между бюджет и контрактом нет, проверять надо так:
     * realizationFormSet.contains(new MultiKey(discipline, subjectPassForm))
     * если содержит, значит существует
     *
     * @param enrollmentCampaign приемная кампания
     * @return все существующие дисциплины по формам сдачи по видам затрат
     */
    Set<MultiKey> getRealizationFormSet(EnrollmentCampaign enrollmentCampaign);

    /**
     * получает сумму баллов по выбранному направлению приема
     *
     * @param requestedEnrollmentDirection выбранное направление приема
     * @return сумма баллов.
     */
    Double getSumFinalMarks(RequestedEnrollmentDirection requestedEnrollmentDirection);

    /**
     * @param entrant абитуриент
     * @return студенты пред. зачисления данного абитуриента
     */
    List<PreliminaryEnrollmentStudent> getPreliminaryEnrollmentStudentList(Entrant entrant);

    /**
     * Сохраняет студента предзачисления со всеми проверками.
     * Нужно всегда пользоваться этим методом, он синхронизирован.
     *
     * @param student студент предзачисления
     * @return сохраненный\созданный студент предзачисления, и сообщение об ошибке - будет не null только что-нибудь одно
     */
    CoreCollectionUtils.Pair<String, PreliminaryEnrollmentStudent> saveOrUpdatePreliminaryStudent(PreliminaryEnrollmentStudent student);

    /**
     * Массово считает финальные оценки по выбранным направлениям приема
     *
     * @param enrollmentCampaign приемная кампани
     * @param directionIds       мн-во id выбранных направлений приема
     * @return мап id выбранного направлени -> сумма баллов по направлению
     */
    Map<Long, Double> getDirectionFinalMarks(final EnrollmentCampaign enrollmentCampaign, Set<Long> directionIds);

    /**
     * @return последняя приемная кампания
     */
    EnrollmentCampaign getLastEnrollmentCampaign();

    /**
     * Обновляет финальные оценки, состояния направлений и состояния абитуриентов
     * Не вызывайте этот метод вручную! так как он вызывает изменения в FinalMarkUpdateEventListener
     * Этот метод полностью автономен и вызывается сам при изменении нужных объектов
     *
     * @param entrantIds мн-во абитуриентов у которых надо все пересчитать
     */
    void updateEntrantData(Collection<Long> entrantIds);

    /**
     * Удаляет дисциплину для сдачи
     * Если это неактуальная дисциплина дла сдачи, то удаляются все оценки по ней
     *
     * @param examPassDiscipline дисциплина для сдачи
     */
    void deleteExamPassDiscipline(ExamPassDiscipline examPassDiscipline);

    /**
     * Удаляет абитуриента
     * Если у абитуриента есть неактуальные дисциплины для сдачи, то вначале удаляются они вместе с оценками
     *
     * @param entrant абитуриента
     */
    void deleteEntrant(Entrant entrant);

    /**
     * Проверяет логику пересчета финальных оценок. Делается это так:
     * Сохраняем все данные из таблиц uniec и uniecc
     * Обновляет всех абитуриентов их всех приемных кампаний
     * Сохраняем все данные из таблиц uniec и uniecc
     * Сравниваем данные до обновления и после. Если не совпало, значит логика неверная! (либо база изначально неконсистентная)
     * <p/>
     * Если в таблицах произошли изменения, то они не откатываются
     *
     * @throws Exception ошибка
     */
    void doCheckEntrantLogicConsistency() throws Exception;
}
