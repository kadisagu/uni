package ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.add;

import java.util.Collections;
import java.util.Map;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;

import ru.tandemservice.uniec.entity.ecg.EcgDistribObject;

/**
 * @author vdanilov
 */
@Input({
	@Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="distrib.id")
})
public class Model extends ModelBase {
	private boolean filtered;
	public boolean isFiltered() { return this.filtered; }
	public void setFiltered(final boolean filtered) { this.filtered = filtered; }

	private EcgDistribObject distrib = new EcgDistribObject();
	public EcgDistribObject getDistrib() { return this.distrib; }
	public void setDistrib(final EcgDistribObject distrib) { this.distrib = distrib; }

	private Map<Long, Integer> dir2quotaMap = Collections.emptyMap();
	public Map<Long, Integer> getDir2quotaMap() { return this.dir2quotaMap; }
	public void setDir2quotaMap(final Map<Long, Integer> dir2quotaMap) { this.dir2quotaMap = dir2quotaMap; }

	public String getNextElementIds(final Row row) {
		final StringBuilder sb = new StringBuilder();
		boolean found = false;
		for (final Row r: this.getEntrantRateRows()) {
			found |= r.equals(row);
			if (found && (r.equals(row) || r.isChecked())) {
				sb.append(r.getId()).append(',');
			}
		}
		sb.append("0");
		return sb.toString();
	}

}
