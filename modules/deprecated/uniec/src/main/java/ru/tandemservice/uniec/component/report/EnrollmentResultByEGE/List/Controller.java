/* $Id$ */
package ru.tandemservice.uniec.component.report.EnrollmentResultByEGE.List;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.report.EnrollmentResultByEGEReport;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

/**
 * @author Vasily Zhukov
 * @since 18.08.2011
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.setSettings(component.getSettings());

        getDao().prepare(model);

        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<EnrollmentResultByEGEReport> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new IndicatorColumn("Иконка", null).defaultIndicator(new IndicatorColumn.Item("report", "Отчет")).setOrderable(false).setClickable(false).setRequired(true));
        dataSource.addColumn(new SimpleColumn("Дата формирования", EnrollmentResultByEGEReport.P_FORMING_DATE, DateFormatter.DATE_FORMATTER_WITH_TIME));
        dataSource.addColumn(new SimpleColumn("Отчетный период", EnrollmentResultByEGEReport.P_PERIOD_TITLE).setOrderable(false).setClickable(false));
        dataSource.addColumn(new IndicatorColumn("Печать", null, "onClickPrintReport").defaultIndicator(new IndicatorColumn.Item("printer", "Печать")).setImageHeader(false).setDisableSecondSubmit(false).setPermissionKey("printUniecStorableReport").setOrderable(false));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteReport", "Удалить отчет от «{0}»?", EnrollmentResultByEGEReport.P_FORMING_DATE).setPermissionKey("deleteUniecStorableReport"));
        dataSource.setOrder(EnrollmentResultByEGEReport.P_FORMING_DATE, OrderDirection.desc);
        model.setDataSource(dataSource);
    }

    public void onClickAddReport(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator("ru.tandemservice.uniec.component.report.EnrollmentResultByEGE.Add"));
    }

    public void onClickSearch(IBusinessComponent component)
    {
        component.saveSettings();
        getModel(component).getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component)
    {
        EntrantFilterUtil.resetEnrollmentCampaignFilter(getModel(component));
        onClickSearch(component);
    }

    public void onClickPrintReport(IBusinessComponent component) throws Exception
    {
        activateInRoot(component, new ComponentActivator(IUniComponents.DOWNLOAD_STORABLE_REPORT, new ParametersMap()
                .add("reportId", component.getListenerParameter())
                .add("extension", "rtf")
        ));
    }

    public void onClickDeleteReport(IBusinessComponent component)
    {
        getDao().deleteRow(component);
    }
}
