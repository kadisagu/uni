/* $Id$ */
package ru.tandemservice.uniec.base.ext.ReportPerson.ui.Add.print.education;

import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.person.base.bo.PersonEduDocument.PersonEduDocumentManager;
import org.tandemframework.shared.person.base.bo.ReportPerson.ui.Add.ReportPersonAddUI;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Andrey Andreev
 * @since 05.08.2016
 */
public class EducationPrintBlockExt implements IReportPrintBlock
{
    private IReportPrintCheckbox _institutionCountry = new ReportPrintCheckbox();

    @Override
    public String getCheckJS()
    {
        List<String> ids = new ArrayList<>();
        for (int i = 0; i < 1; i++)
            ids.add("chEducationEcExt" + i);
        return ReportJavaScriptUtil.getCheckJS(ids);
    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        modify(dql, printInfo, ReportPersonAddUI.PERSON_SCHEET);
    }

    @SuppressWarnings({"deprecation"})
    public void modify(ReportDQL dql, IReportPrintInfo printInfo, String sheetName)
    {
        if (PersonEduDocumentManager.isShowLegacyEduDocuments() && _institutionCountry.isActive())
        {
            String a1 = dql.leftJoinEntity(PersonEduInstitution.class, PersonEduInstitution.person());
            final int i = dql.addListAggregateColumn(a1);

            ReportPrintColumn institutionCountryColumn = new ReportPrintColumn("institutionCountry")
            {
                @Override
                public void prefetch(List<Object[]> rows)
                {
                    prefetchPersonEduInstitution(rows, i);
                }

                @Override
                public Object createColumnValue(Object[] data)
                {
                    final PersonEduInstitution eduInstitution = (PersonEduInstitution) data[i];
                    if(eduInstitution == null) return null;

                    AddressItem addressItem = eduInstitution.getAddressItem();
                    AddressCountry addressCountry = addressItem.getCountry();
                    if (addressCountry.getCode() == IKladrDefines.RUSSIA_COUNTRY_CODE)
                        return addressCountry.getTitle() + " (" + addressItem.getInheritedRegionCode() + ")";

                    return addressCountry.getTitle();
                }
            };

            List<IReportPrintColumn> columnList = printInfo.getColumnList(sheetName);

            if (!columnList.isEmpty())
                for (int index = 0; index < columnList.size(); index++)
                    if (columnList.get(index).getName().equals("institution"))
                    {
                        printInfo.addPrintColumn(sheetName, index + 1, institutionCountryColumn);
                        break;
                    }
        }
    }


    private void prefetchPersonEduInstitution(List<Object[]> rows, int index)
    {
        // из всех найденных используем тот, который закончили последним
        for (Object[] row : rows)
        {
            Object obj = row[index];

            if (!(obj instanceof List)) break; // данные уже были профетчены

            List list = (List) obj;
            if (list.isEmpty())
            {
                row[index] = null;
            }
            else
            {
                PersonEduInstitution result = (PersonEduInstitution) list.get(0);
                for (int i = 1; i < list.size(); i++)
                {
                    PersonEduInstitution item = (PersonEduInstitution) list.get(i);
                    if (item.getYearEnd() > result.getYearEnd())
                        result = item;
                }
                row[index] = result;
            }
        }
    }

    //Getters

    public IReportPrintCheckbox getInstitutionCountry()
    {
        return _institutionCountry;
    }
}