// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;

import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.add.ModelBase;
import ru.tandemservice.uniec.dao.ecg.IEnrollmentCompetitionGroupDAO;
import ru.tandemservice.uniec.entity.ecg.EcgDistribObject;
import ru.tandemservice.uniec.entity.ecg.EcgDistribRelation;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

/**
 * @author oleyba
 * @since 31.05.2010
 */
public class CompetitionGroupDistribResultPrintFormCreator extends CompetitionGroupDistribPrintFormCreator implements IPrintFormCreator<Long>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, Long distribId)
    {
        EcgDistribObject distrib = get(EcgDistribObject.class, distribId);

        final Map<Long, Long> directionIdByEntrantId = new HashMap<Long, Long>();
        final MQBuilder builder = new MQBuilder(EcgDistribRelation.ENTITY_CLASS, "rel", new String[]{
                EcgDistribRelation.entrantDirection().entrantRequest().entrant().id().s(),
                EcgDistribRelation.entrantDirection().id().s()
        });
        builder.add(MQExpression.eq("rel", EcgDistribRelation.quota().distribution().s(), distrib));
        for (final Object[] row : builder.<Object[]>getResultList(getSession()))
            directionIdByEntrantId.put((Long) row[0], (Long) row[1]);

        ArrayList<ModelBase.Row> rows = new ArrayList<ModelBase.Row>();
        for (IEnrollmentCompetitionGroupDAO.IEntrantRateRow entrantRow : IEnrollmentCompetitionGroupDAO.INSTANCE.get().getEntrantRateRows(distrib))
        {
            final ModelBase.Row row = new ModelBase.Row(entrantRow);
            final Long directionId = directionIdByEntrantId.get(row.getRow().getEntrant().getId());
            if (null != directionId)
                row.setDirection(get(RequestedEnrollmentDirection.class, directionId));
            rows.add(row);
        }

        RtfDocument document = new RtfReader().read(template);
        buildReport(document, true, distrib, rows, "");
        return document;
    }

    @Override
    protected String getCompetitionGroupTitle()
    {
        return "Конкурсная группа";
    }
}
