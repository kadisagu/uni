/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.report.EntranceExamMeetingByCK2010.EntranceExamMeetingByCKAdd;

import com.google.common.collect.Iterables;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Declinable.DeclinableManager;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.catalog.entity.InflectorVariant;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import org.tandemframework.shared.person.catalog.entity.Benefit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.component.settings.ProtocolVisaList.IProtocolEntity;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.examset.EnrollmentDirectionExamSetData;
import ru.tandemservice.uniec.entity.report.OrgUnitCommissionChairman;
import ru.tandemservice.uniec.entity.report.OrgUnitExecutiveSecretary;
import ru.tandemservice.uniec.entity.report.OrgUnitSelectionSecretary;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.MarkDistributionUtil;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Vasily Zhukov
 * @since 04.08.2010
 */
class EntranceExamMeetingForm1RtfBuilder implements EntranceExamMeetingRtfBuilder
{
    private static Map<String, String> _developFormByCode = new HashMap<>();

    static
    {
        _developFormByCode.put(DevelopFormCodes.FULL_TIME_FORM, "очную форму обучения");
        _developFormByCode.put(DevelopFormCodes.CORESP_FORM, "заочную форму обучения");
        _developFormByCode.put(DevelopFormCodes.PART_TIME_FORM, "очно-заочную форму обучения");
        _developFormByCode.put(DevelopFormCodes.EXTERNAL_FORM, "экстернат");
        _developFormByCode.put(DevelopFormCodes.APPLICANT_FORM, "самостоятельное обучение и итоговую аттестацию");
    }

    private Session _session;
    private Model _model;
    private Map<EnrollmentDirection, EnrollmentDirectionExamSetData> _examSetDataMap;
    private RtfDocument _template;
    private Map<Person, Set<PersonBenefit>> _personBenefitsMap;
    protected Set<Long> _entrantWithRecommendationIds;
    private EntrantDataUtil _dataUtil;
    private Map<Integer, CompetitionKind> _priority2CompetitionKind;

    @Override
    public RtfDocument getTemplate()
    {
        return _template;
    }

    @Override
    public void init(Session session, Model model, Map<EnrollmentDirection, EnrollmentDirectionExamSetData> examSetDataMap, EntrantDataUtil dataUtil, MQBuilder directionBuilder, Map<Integer, CompetitionKind> priority2CompetitionKind)
    {
        _session = session;
        _model = model;
        _examSetDataMap = examSetDataMap;

        // загружаем шаблон
        MQBuilder builder = new MQBuilder(UniecScriptItem.ENTITY_CLASS, "t");
        builder.add(MQExpression.eq("t", UniecScriptItem.P_CODE, UniecDefines.TEMPLATE_ENTRANCE_EXAM_MEETING_BY_CK1));
        IScriptItem templateDocument = (UniecScriptItem) builder.uniqueResult(session);
        _template = new RtfReader().read(templateDocument.getCurrentTemplate());

        // создаем дополнительные мапы
        _personBenefitsMap = EntrantDataUtil.getPersonBenefitMap(session, directionBuilder);
        _dataUtil = dataUtil;
        _priority2CompetitionKind = priority2CompetitionKind;

        _entrantWithRecommendationIds = new HashSet<>();
        for (List<RequestedEnrollmentDirection> directions : Iterables.partition(_dataUtil.getDirectionSet(), DQL.MAX_VALUES_ROW_NUMBER))
        {
            _entrantWithRecommendationIds.addAll(
                    new DQLSelectBuilder()
                            .fromEntity(Entrant.class, "e")
                            .column(property("e", Entrant.id()))
                            .where(exists(EntrantEnrolmentRecommendation.class, EntrantEnrolmentRecommendation.entrant().s(), property("e")))
                            .where(existsByExpr(RequestedEnrollmentDirection.class, "req",
                                                and(eq(property("req", RequestedEnrollmentDirection.entrantRequest().entrant()), property("e")),
                                                    in(property("req"), directions))
                            ))
                            .createStatement(_session).<Long>list()
            );
        }
    }

    @Override
    public List<IRtfElement> buildReportPart(EnrollmentDirectionGroup group, Map<CompetitionKind, List<ReportRow>> competitionKind2RowList, final boolean structuresEqualInWholeGroup,
                                             final List<String> typeTitles, final List<String[]> titles, final boolean useIndProgress)
    {
        // кол-во предметов
        final int size = typeTitles.size();

        // data for tables
        Map<CompetitionKind, List<String[]>> competitionKind2Data = SafeMap.get(ArrayList.class);
        int num = 1;
        for (Map.Entry<Integer, CompetitionKind> entry : _priority2CompetitionKind.entrySet())
        {
            CompetitionKind entryCompetitionKind = entry.getValue(); // может быть null для целевого приема
            List<String[]> data = new ArrayList<>();
            competitionKind2Data.put(entryCompetitionKind, data);
            List<ReportRow> rowList = competitionKind2RowList.get(entryCompetitionKind);
            if (rowList == null) continue;

            for (ReportRow row : rowList)
            {
                RequestedEnrollmentDirection requestedEnrollmentDirection = row.getRequestedEnrollmentDirection();
                EnrollmentDirection enrollmentDirection = requestedEnrollmentDirection.getEnrollmentDirection();
                Entrant entrant = requestedEnrollmentDirection.getEntrantRequest().getEntrant();
                Person person = entrant.getPerson();

                //Инд.достижения учитываются - удаляем колонку
                String[] cells = new String[10 + size + (useIndProgress ? 1 :0)];

                cells[0] = Integer.toString(num++);
                cells[1] = requestedEnrollmentDirection.getStringNumber();
                cells[2] = requestedEnrollmentDirection.getEntrantRequest().getEntrant().getPerson().getFullFio();
                cells[3] = requestedEnrollmentDirection.isOriginalDocumentHandedIn() ? "О" : "К";

                CompetitionKind competitionKind = requestedEnrollmentDirection.getCompetitionKind(); // а вот этот уже не null
                if (requestedEnrollmentDirection.isTargetAdmission())
                {
                    cells[4] = "Ц";

                    if (!competitionKind.getCode().equals(UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION))
                    {
                        cells[4] += ", " + competitionKind.getShortTitle();
                    }
                } else
                {
                    cells[4] = competitionKind.getShortTitle();
                }

                List<String> benefitList = CommonBaseUtil.getPropertiesList(_personBenefitsMap.get(person), PersonBenefit.L_BENEFIT + "." + Benefit.P_SHORT_TITLE);
                Collections.sort(benefitList);
                cells[5] = benefitList.isEmpty() ? "Н" : StringUtils.join(benefitList, ", ");
                cells[6] = _entrantWithRecommendationIds.contains(entrant.getId()) ? "Д" : "Н";

                int j = 7;
                if (size == 0)
                {
                    j++;
                } else
                {
                    Set<ChosenEntranceDiscipline> chosenSet = _dataUtil.getChosenEntranceDisciplineSet(requestedEnrollmentDirection);
                    ChosenEntranceDiscipline[] distribution = MarkDistributionUtil.getChosenDistribution((chosenSet != null) ? chosenSet : Collections.<ChosenEntranceDiscipline>emptySet(), _examSetDataMap.get(enrollmentDirection).getExamSetStructure().getItemList());

                    for (int k = 0; k < size; k++)
                    {
                        ChosenEntranceDiscipline chosen = distribution[k];
                        Double mark = chosen == null ? null : row.getMarkMap().get(chosen.getEnrollmentCampaignDiscipline());

                        cells[j] = mark == null ? "x" : (mark == -1.0 ? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(mark));
                        j++;
                    }
                }
                //Инд.достижения учитываются - добавляем данные
                if (useIndProgress)
                    cells[j++] = String.valueOf(row.getIndividProgressMark());
                cells[j++] = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(row.getSumMark());
                cells[j] = "";

                data.add(cells);
            }
        }

        // direction header modifier
        String commissionChairman = (String) new MQBuilder(OrgUnitCommissionChairman.ENTITY_CLASS, "r", new String[]{OrgUnitCommissionChairman.P_FIO})
                .add(MQExpression.eq("r", IProtocolEntity.L_ORG_UNIT, group.getFormativeOrgUnit()))
                .add(MQExpression.eq("r", IProtocolEntity.L_COMPENSATION_TYPE, _model.getReport().getCompensationType()))
                .add(MQExpression.eq("r", IProtocolEntity.L_ENROLLMENT_CAMPAIGN, _model.getEnrollmentCampaign()))
                .uniqueResult(_session);

        String executiveSecretary = (String) new MQBuilder(OrgUnitExecutiveSecretary.ENTITY_CLASS, "r", new String[]{OrgUnitExecutiveSecretary.P_FIO})
                .add(MQExpression.eq("r", IProtocolEntity.L_ORG_UNIT, group.getFormativeOrgUnit()))
                .add(MQExpression.eq("r", IProtocolEntity.L_COMPENSATION_TYPE, _model.getReport().getCompensationType()))
                .add(MQExpression.eq("r", IProtocolEntity.L_ENROLLMENT_CAMPAIGN, _model.getEnrollmentCampaign()))
                .uniqueResult(_session);

        String selectionSecretary = (String) new MQBuilder(OrgUnitSelectionSecretary.ENTITY_CLASS, "r", new String[]{OrgUnitSelectionSecretary.P_FIO})
                .add(MQExpression.eq("r", IProtocolEntity.L_ORG_UNIT, group.getFormativeOrgUnit()))
                .add(MQExpression.eq("r", IProtocolEntity.L_COMPENSATION_TYPE, _model.getReport().getCompensationType()))
                .add(MQExpression.eq("r", IProtocolEntity.L_ENROLLMENT_CAMPAIGN, _model.getEnrollmentCampaign()))
                .uniqueResult(_session);

        RtfDocument document = _template.getClone();

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("HS", TopOrgUnit.getInstance().getShortTitle());
        injectModifier.put("CT", _model.getReport().getCompensationType().getCode().equals(UniDefines.COMPENSATION_TYPE_BUDGET) ? "за счет средств федерального бюджета" : "с оплатой стоимости обучения");
        injectModifier.put("DF", _developFormByCode.get(group.getDevelopForm().getCode())); //форма освоения
        injectModifier.put("FD", StringUtils.isNotEmpty(group.getFormativeOrgUnit().getNominativeCaseTitle()) ? group.getFormativeOrgUnit().getNominativeCaseTitle() : group.getFormativeOrgUnit().getTitle()); //формирующее подр.
        String territorialOrgUnitTitle = "";
        if (!_model.isGroupByEducationLevel())
        {
            OrgUnit territorialOrgUnit = group.getEnrollmentDirectionList().get(0).getEducationOrgUnit().getTerritorialOrgUnit();
            if (territorialOrgUnit != null)
                if (StringUtils.isNotEmpty(territorialOrgUnit.getNominativeCaseTitle()))
                    territorialOrgUnitTitle = territorialOrgUnit.getNominativeCaseTitle();
                else
                    territorialOrgUnitTitle = territorialOrgUnit.getTitle();
        }
        injectModifier.put("TD", territorialOrgUnitTitle); //террит. подоазделение
        injectModifier.put("ELT", group.getEducationLevels().getLevelType().getDativeCaseShortTitle()); //по спец, по направл
        injectModifier.put("commissionChairman", StringUtils.isEmpty(commissionChairman) ? "               " : commissionChairman);
        injectModifier.put("executiveSecretary", StringUtils.isEmpty(executiveSecretary) ? "               " : executiveSecretary);
        injectModifier.put("selectionSecretary", StringUtils.isEmpty(selectionSecretary) ? "               " : selectionSecretary);

        String educationLevelText = group.getTitle() + " (" + StringUtils.uncapitalize(group.getDevelopCondition().getTitle()) + ", " + group.getDevelopPeriod().getTitle() + ")";
        injectModifier.put("EL", educationLevelText); //направление приема
        injectModifier.put("EL_Alt", group.getTitle()); //направление приема
        String eduProgramKind = "";
        EduProgramSubject eduProgramSubject = group.getEducationLevels().getEduProgramSubject();
        if(eduProgramSubject != null)
        {
            eduProgramKind = DeclinableManager.instance().dao()
                    .getPropertyValue(eduProgramSubject.getEduProgramKind(), EduProgramKind.P_TITLE, IUniBaseDao.instance.get().getCatalogItem(InflectorVariant.class, InflectorVariantCodes.RU_DATIVE));
        }
        injectModifier.put("eduProgramKind", eduProgramKind);
        injectModifier.modify(document);

        RtfTable table = (RtfTable) UniRtfUtil.findElement(document.getElementList(), "H1");
        RtfRow head = table.getRowList().get(1);

        //Инд.достижения не учитываются - удаляем колонку из шаблона
        if (!useIndProgress)
        {
            UniRtfUtil.deleteCell(table.getRowList().get(0), 8, 7);
            UniRtfUtil.deleteCell(head, 8, 7);
        }
        if (size == 0)
        {
            UniRtfUtil.fillCell(head.getCellList().get(7), "");
        } else
        {
            int[] parts = new int[size];
            Arrays.fill(parts, 1);
            RtfUtil.splitRow(head, 7, (newCell, index) -> newCell.setElementList(Collections.singletonList((IRtfElement) RtfBean.getElementFactory().createRtfText(typeTitles.get(index)))), parts);
        }

        final List<Integer> headerIndexes = new ArrayList<>();
        List<String[]> tableRows = new ArrayList<>();

        for (Map.Entry<Integer, CompetitionKind> entry : _priority2CompetitionKind.entrySet())
        {
            CompetitionKind competitionKind = entry.getValue();
            List<String[]> data = competitionKind2Data.get(competitionKind);

            if (!data.isEmpty() || competitionKind == null || !competitionKind.getCode().equals(UniecDefines.COMPETITION_KIND_INTERVIEW))
            {
                headerIndexes.add(tableRows.size());
                tableRows.add(new String[]{ReportRow.getHeader(competitionKind)});
            }
            tableRows.addAll(data);
        }

        RtfTableModifier modifier = new RtfTableModifier();
        modifier.put("T", tableRows.toArray(new String[tableRows.size()][]));

        modifier.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public void beforeModify(RtfTable table, int currentRowIndex)
            {
                //Инд.достижения не учитываются - удаляем колонку из отчета
                if (!useIndProgress)
                    UniRtfUtil.deleteCell(table.getRowList().get(currentRowIndex), 8, 7);

                if (size != 0)
                {
                    // разбиваем колонку по количеству вступительных испытаний в наборе
                    int[] parts = new int[size];
                    Arrays.fill(parts, 1);

                    RtfUtil.splitRow(table.getRowList().get(currentRowIndex), 7, null, parts);
                }
            }

            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                // объединяем ячейки в строках с заголовками и выравниваем по центру
                for (Integer headerIndex : headerIndexes)
                {
                    RtfRow row = newRowList.get(startIndex + headerIndex);
                    RtfUtil.unitAllCell(row, 0);
                    SharedRtfUtil.setCellAlignment(row.getCellList().get(0), IRtfData.QR, IRtfData.QC);
                }
            }
        });
        modifier.modify(document);

        return document.getElementList();
    }
}
