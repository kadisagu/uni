package ru.tandemservice.uniec.base.entity.ecg.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistribution;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistributionConfig;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistributionState;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Основное распределение абитуриентов
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EcgDistributionGen extends EntityBase
 implements INaturalIdentifiable<EcgDistributionGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.base.entity.ecg.EcgDistribution";
    public static final String ENTITY_NAME = "ecgDistribution";
    public static final int VERSION_HASH = 1903678239;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONFIG = "config";
    public static final String P_WAVE = "wave";
    public static final String L_STATE = "state";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String P_APPROVAL_DATE = "approvalDate";

    private EcgDistributionConfig _config;     // Конфигурация распределения абитуриентов
    private int _wave;     // Волна
    private EcgDistributionState _state;     // Состояние распределения
    private Date _formingDate;     // Дата формирования
    private Date _approvalDate;     // Дата утверждения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Конфигурация распределения абитуриентов. Свойство не может быть null.
     */
    @NotNull
    public EcgDistributionConfig getConfig()
    {
        return _config;
    }

    /**
     * @param config Конфигурация распределения абитуриентов. Свойство не может быть null.
     */
    public void setConfig(EcgDistributionConfig config)
    {
        dirty(_config, config);
        _config = config;
    }

    /**
     * @return Волна. Свойство не может быть null.
     */
    @NotNull
    public int getWave()
    {
        return _wave;
    }

    /**
     * @param wave Волна. Свойство не может быть null.
     */
    public void setWave(int wave)
    {
        dirty(_wave, wave);
        _wave = wave;
    }

    /**
     * @return Состояние распределения. Свойство не может быть null.
     */
    @NotNull
    public EcgDistributionState getState()
    {
        return _state;
    }

    /**
     * @param state Состояние распределения. Свойство не может быть null.
     */
    public void setState(EcgDistributionState state)
    {
        dirty(_state, state);
        _state = state;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Дата утверждения.
     */
    public Date getApprovalDate()
    {
        return _approvalDate;
    }

    /**
     * @param approvalDate Дата утверждения.
     */
    public void setApprovalDate(Date approvalDate)
    {
        dirty(_approvalDate, approvalDate);
        _approvalDate = approvalDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EcgDistributionGen)
        {
            if (withNaturalIdProperties)
            {
                setConfig(((EcgDistribution)another).getConfig());
                setWave(((EcgDistribution)another).getWave());
            }
            setState(((EcgDistribution)another).getState());
            setFormingDate(((EcgDistribution)another).getFormingDate());
            setApprovalDate(((EcgDistribution)another).getApprovalDate());
        }
    }

    public INaturalId<EcgDistributionGen> getNaturalId()
    {
        return new NaturalId(getConfig(), getWave());
    }

    public static class NaturalId extends NaturalIdBase<EcgDistributionGen>
    {
        private static final String PROXY_NAME = "EcgDistributionNaturalProxy";

        private Long _config;
        private int _wave;

        public NaturalId()
        {}

        public NaturalId(EcgDistributionConfig config, int wave)
        {
            _config = ((IEntity) config).getId();
            _wave = wave;
        }

        public Long getConfig()
        {
            return _config;
        }

        public void setConfig(Long config)
        {
            _config = config;
        }

        public int getWave()
        {
            return _wave;
        }

        public void setWave(int wave)
        {
            _wave = wave;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EcgDistributionGen.NaturalId) ) return false;

            EcgDistributionGen.NaturalId that = (NaturalId) o;

            if( !equals(getConfig(), that.getConfig()) ) return false;
            if( !equals(getWave(), that.getWave()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getConfig());
            result = hashCode(result, getWave());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getConfig());
            sb.append("/");
            sb.append(getWave());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EcgDistributionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EcgDistribution.class;
        }

        public T newInstance()
        {
            return (T) new EcgDistribution();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "config":
                    return obj.getConfig();
                case "wave":
                    return obj.getWave();
                case "state":
                    return obj.getState();
                case "formingDate":
                    return obj.getFormingDate();
                case "approvalDate":
                    return obj.getApprovalDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "config":
                    obj.setConfig((EcgDistributionConfig) value);
                    return;
                case "wave":
                    obj.setWave((Integer) value);
                    return;
                case "state":
                    obj.setState((EcgDistributionState) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "approvalDate":
                    obj.setApprovalDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "config":
                        return true;
                case "wave":
                        return true;
                case "state":
                        return true;
                case "formingDate":
                        return true;
                case "approvalDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "config":
                    return true;
                case "wave":
                    return true;
                case "state":
                    return true;
                case "formingDate":
                    return true;
                case "approvalDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "config":
                    return EcgDistributionConfig.class;
                case "wave":
                    return Integer.class;
                case "state":
                    return EcgDistributionState.class;
                case "formingDate":
                    return Date.class;
                case "approvalDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EcgDistribution> _dslPath = new Path<EcgDistribution>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EcgDistribution");
    }
            

    /**
     * @return Конфигурация распределения абитуриентов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgDistribution#getConfig()
     */
    public static EcgDistributionConfig.Path<EcgDistributionConfig> config()
    {
        return _dslPath.config();
    }

    /**
     * @return Волна. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgDistribution#getWave()
     */
    public static PropertyPath<Integer> wave()
    {
        return _dslPath.wave();
    }

    /**
     * @return Состояние распределения. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgDistribution#getState()
     */
    public static EcgDistributionState.Path<EcgDistributionState> state()
    {
        return _dslPath.state();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgDistribution#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Дата утверждения.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgDistribution#getApprovalDate()
     */
    public static PropertyPath<Date> approvalDate()
    {
        return _dslPath.approvalDate();
    }

    public static class Path<E extends EcgDistribution> extends EntityPath<E>
    {
        private EcgDistributionConfig.Path<EcgDistributionConfig> _config;
        private PropertyPath<Integer> _wave;
        private EcgDistributionState.Path<EcgDistributionState> _state;
        private PropertyPath<Date> _formingDate;
        private PropertyPath<Date> _approvalDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Конфигурация распределения абитуриентов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgDistribution#getConfig()
     */
        public EcgDistributionConfig.Path<EcgDistributionConfig> config()
        {
            if(_config == null )
                _config = new EcgDistributionConfig.Path<EcgDistributionConfig>(L_CONFIG, this);
            return _config;
        }

    /**
     * @return Волна. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgDistribution#getWave()
     */
        public PropertyPath<Integer> wave()
        {
            if(_wave == null )
                _wave = new PropertyPath<Integer>(EcgDistributionGen.P_WAVE, this);
            return _wave;
        }

    /**
     * @return Состояние распределения. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgDistribution#getState()
     */
        public EcgDistributionState.Path<EcgDistributionState> state()
        {
            if(_state == null )
                _state = new EcgDistributionState.Path<EcgDistributionState>(L_STATE, this);
            return _state;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgDistribution#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(EcgDistributionGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Дата утверждения.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgDistribution#getApprovalDate()
     */
        public PropertyPath<Date> approvalDate()
        {
            if(_approvalDate == null )
                _approvalDate = new PropertyPath<Date>(EcgDistributionGen.P_APPROVAL_DATE, this);
            return _approvalDate;
        }

        public Class getEntityClass()
        {
            return EcgDistribution.class;
        }

        public String getEntityName()
        {
            return "ecgDistribution";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
