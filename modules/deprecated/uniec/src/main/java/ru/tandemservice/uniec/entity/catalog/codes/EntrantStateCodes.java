package ru.tandemservice.uniec.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Состояние абитуриента"
 * Имя сущности : entrantState
 * Файл data.xml : uniec.catalogs.data.xml
 */
public interface EntrantStateCodes
{
    /** Константа кода (code) элемента : Активный (title) */
    String ACTIVE = "1";
    /** Константа кода (code) элемента : Выбыл из конкурса (title) */
    String OUT_OF_COMPETITION = "2";
    /** Константа кода (code) элемента : Зачислен (title) */
    String ENROLED = "3";
    /** Константа кода (code) элемента : К зачислению (title) */
    String TO_BE_ENROLED = "4";
    /** Константа кода (code) элемента : Пред. зачислен (title) */
    String PRELIMENARY_ENROLLED = "5";
    /** Константа кода (code) элемента : В приказе (title) */
    String IN_ORDER = "6";
    /** Константа кода (code) элемента : Забрал документы (title) */
    String TAKE_DOCUMENTS_AWAY = "7";

    Set<String> CODES = ImmutableSet.of(ACTIVE, OUT_OF_COMPETITION, ENROLED, TO_BE_ENROLED, PRELIMENARY_ENROLLED, IN_ORDER, TAKE_DOCUMENTS_AWAY);
}
