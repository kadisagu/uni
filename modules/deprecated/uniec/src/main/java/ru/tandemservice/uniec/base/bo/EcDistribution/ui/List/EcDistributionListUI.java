/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.ui.List;

import org.hibernate.Session;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.catalog.gen.CompensationTypeGen;
import ru.tandemservice.uniec.base.bo.EcDistribution.EcDistributionManager;
import ru.tandemservice.uniec.base.bo.EcDistribution.logic.EcSimpleDSHandler;
import ru.tandemservice.uniec.base.bo.EcDistribution.ui.AddEdit.EcDistributionAddEdit;
import ru.tandemservice.uniec.base.bo.EcDistribution.ui.AddEdit.EcDistributionAddEditUI;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.EcgConfigDTO;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.EcgDistributionDTO;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistribution;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistributionState;
import ru.tandemservice.uniec.entity.entrant.CompetitionGroup;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Vasily Zhukov
 * @since 06.07.2011
 */
public class EcDistributionListUI extends UIPresenter implements MultiEnrollmentDirectionUtil.Model
{
    private static final String ENROLLMENT_CAMPAIGN_FILTER = "enrollmentCampaign";
    private static final String COMPENSATION_TYPE_FILTER = "compensationType";
    private static final String DISTRIBUTION_CATEGORY_FILTER = "distributionCategory";
    private static final String WAVE_FILTER = "wave";
    private static final String STATE_FILTER = "state";
    private static final String COMPETITION_GROUP_FILTER = "competitionGroup";
    private static final String QUALIFICATION_FILTER = "qualificationList";

    private static final String FORMATIVE_ORG_UNIT_FILTER = "formativeOrgUnitList";
    private static final String TERRITORIAL_ORG_UNIT_FILTER = "territorialOrgUnitList";
    private static final String EDUCATION_LEVEL_HIGH_SCHOOL_FILTER = "educationLevelHighSchoolList";
    private static final String DEVELOP_FORM_FILTER = "developFormList";
    private static final String DEVELOP_CONDITION_FILTER = "developConditionList";
    private static final String DEVELOP_TECH_FILTER = "developTechList";
    private static final String DEVELOP_PERIOD_FILTER = "developPeriodList";

    private MultiEnrollmentDirectionUtil.Parameters _parameters;
    private ISelectModel _formativeOrgUnitListModel;
    private ISelectModel _territorialOrgUnitListModel;
    private ISelectModel _educationLevelHighSchoolListModel;
    private ISelectModel _developFormListModel;
    private ISelectModel _developTechListModel;
    private ISelectModel _developConditionListModel;
    private ISelectModel _developPeriodListModel;

    private Map<Long, EcgDistributionDTO> id2valueMap = new HashMap<>();

    private Map<Long, String> _quotaMap;
    private Map<Long, String> _taQuotaMap;

    @Override
    public void onComponentRefresh()
    {
        // init select models
        _parameters = new MultiEnrollmentDirectionUtil.Parameters();
        _formativeOrgUnitListModel = MultiEnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(null, this);
        _territorialOrgUnitListModel = MultiEnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(null, this);
        _educationLevelHighSchoolListModel = MultiEnrollmentDirectionUtil.createEducationLevelsHighSchoolModel(this);
        _developFormListModel = MultiEnrollmentDirectionUtil.createDevelopFormModel(this);
        _developConditionListModel = MultiEnrollmentDirectionUtil.createDevelopConditionModel(this);
        _developTechListModel = MultiEnrollmentDirectionUtil.createDevelopTechModel(this);
        _developPeriodListModel = MultiEnrollmentDirectionUtil.createDevelopPeriodModel(this);

        initDefaults();
    }

    private void initDefaults()
    {
        // init enrollment campaign
        if (getEnrollmentCampaign() == null)
        {
            List<EnrollmentCampaign> list = DataAccessServices.dao().getList(EnrollmentCampaign.class, EnrollmentCampaign.P_ID);
            if (list != null && !list.isEmpty())
                _uiSettings.set(ENROLLMENT_CAMPAIGN_FILTER, list.get(list.size() - 1));
        }

        // compensationType
        if (_uiSettings.get(COMPENSATION_TYPE_FILTER) == null)
        {
            _uiSettings.set(COMPENSATION_TYPE_FILTER, DataAccessServices.dao().<CompensationType>getByNaturalId(new CompensationTypeGen.NaturalId(UniDefines.COMPENSATION_TYPE_BUDGET)));
        }

        // init secondHighAdmission
        if (_uiSettings.get(DISTRIBUTION_CATEGORY_FILTER) == null)
        {
            _uiSettings.set(DISTRIBUTION_CATEGORY_FILTER, EcDistributionManager.DISTRIBUTION_CATEGORY_STUDENTS_AND_LISTENERS);
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (EcDistributionList.COMPETITION_GROUP_DS.equals(dataSource.getName()))
        {
            dataSource.put(UIDefines.COMBO_OBJECT_LIST, EcDistributionManager.instance().dao().getCompetitionGroupList(getEnrollmentCampaign(), getSelectedQualificationList()));
        } else if (EcDistributionList.WAVE_DS.equals(dataSource.getName()))
        {
            Integer maxWave = EcDistributionManager.instance().dao().getDistributionMaxWave(
                    getEnrollmentCampaign(),
                    _uiSettings.<CompensationType>get(COMPENSATION_TYPE_FILTER),
                    EcDistributionManager.DISTRIBUTION_CATEGORY_SECOND_HIGH_ADMISSION.equals(_uiSettings.<IEntity>get(DISTRIBUTION_CATEGORY_FILTER))
            );

            List<DataWrapper> list = new ArrayList<>();
            if (maxWave != null)
                for (int i = 1; i <= maxWave; i++)
                    list.add(new DataWrapper(i, Integer.toString(i)));

            dataSource.put(UIDefines.COMBO_OBJECT_LIST, list);
        } else if (EcDistributionList.DISTRIBUTION_CG_DS.equals(dataSource.getName()) || EcDistributionList.DISTRIBUTION_ED_DS.equals(dataSource.getName()))
        {
            DataWrapper waveWrapper = _uiSettings.get(WAVE_FILTER);
            List<EcgDistributionDTO> list = EcDistributionManager.instance().dao().getDistributionDTOList(
                    getEnrollmentCampaign(),
                    _uiSettings.<CompensationType>get(COMPENSATION_TYPE_FILTER),
                    EcDistributionManager.DISTRIBUTION_CATEGORY_SECOND_HIGH_ADMISSION.equals(_uiSettings.<IEntity>get(DISTRIBUTION_CATEGORY_FILTER)),
                    waveWrapper == null ? null : waveWrapper.getId().intValue(),
                    _uiSettings.<EcgDistributionState>get(STATE_FILTER),
                    _uiSettings.<CompetitionGroup>get(COMPETITION_GROUP_FILTER),
                    getSelectedQualificationList(),
                    getSelectedFormativeOrgUnitList(),
                    getSelectedTerritorialOrgUnitList(),
                    getSelectedEducationLevelHighSchoolList(),
                    getSelectedDevelopFormList(),
                    getSelectedDevelopConditionList(),
                    getSelectedDevelopTechList(),
                    getSelectedDevelopPeriodList()
            );

            _quotaMap = new HashMap<>();
            _taQuotaMap = new HashMap<>();

            Map<Long, String> quotaMap = new HashMap<>();
            Map<Long, String> taQuotaMap = new HashMap<>();

            // сохраняем мап: id -> DTO
            for (EcgDistributionDTO item : list)
            {
                id2valueMap.put(item.getId(), item);

                if (item.getPersistentId() != null && !item.isDetailDistribution())
                {
                    quotaMap.clear();
                    taQuotaMap.clear();
                    EcDistributionManager.instance().dao().getQuotaHTMLDescription(DataAccessServices.dao().<EcgDistribution>getNotNull(item.getPersistentId()), quotaMap, taQuotaMap);
                    _quotaMap.put(item.getPersistentId(), quotaMap.get(0L));
                    _taQuotaMap.put(item.getPersistentId(), taQuotaMap.get(0L));
                }
            }

            dataSource.put(EcSimpleDSHandler.LIST, list);
        }
    }

    // Settings

    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _uiSettings.get(ENROLLMENT_CAMPAIGN_FILTER);
    }

    // MultiEnrollmentDirectionUtil.Model

    @Override
    public MultiEnrollmentDirectionUtil.Parameters getParameters()
    {
        return _parameters;
    }

    @Override
    public List<EnrollmentCampaign> getSelectedEnrollmentCampaignList()
    {
        return MultiEnrollmentDirectionUtil.getSingletonList(getEnrollmentCampaign());
    }

    public List<Qualifications> getSelectedQualificationList()
    {
        List<Qualifications> list = _uiSettings.get(QUALIFICATION_FILTER);
        return list == null || list.isEmpty() ? null : list;
    }

    @Override
    public List<OrgUnit> getSelectedFormativeOrgUnitList()
    {
        List<OrgUnit> list = _uiSettings.get(FORMATIVE_ORG_UNIT_FILTER);
        return list == null || list.isEmpty() ? null : list;
    }

    @Override
    public List<OrgUnit> getSelectedTerritorialOrgUnitList()
    {
        List<OrgUnit> list = _uiSettings.get(TERRITORIAL_ORG_UNIT_FILTER);
        return list == null || list.isEmpty() ? null : list;
    }

    @Override
    public List<EducationLevelsHighSchool> getSelectedEducationLevelHighSchoolList()
    {
        List<EducationLevelsHighSchool> list = _uiSettings.get(EDUCATION_LEVEL_HIGH_SCHOOL_FILTER);
        return list == null || list.isEmpty() ? null : list;
    }

    @Override
    public List<DevelopForm> getSelectedDevelopFormList()
    {
        List<DevelopForm> list = _uiSettings.get(DEVELOP_FORM_FILTER);
        return list == null || list.isEmpty() ? null : list;
    }

    @Override
    public List<DevelopCondition> getSelectedDevelopConditionList()
    {
        List<DevelopCondition> list = _uiSettings.get(DEVELOP_CONDITION_FILTER);
        return list == null || list.isEmpty() ? null : list;
    }

    @Override
    public List<DevelopTech> getSelectedDevelopTechList()
    {
        List<DevelopTech> list = _uiSettings.get(DEVELOP_TECH_FILTER);
        return list == null || list.isEmpty() ? null : list;
    }

    @Override
    public List<DevelopPeriod> getSelectedDevelopPeriodList()
    {
        List<DevelopPeriod> list = _uiSettings.get(DEVELOP_PERIOD_FILTER);
        return list == null || list.isEmpty() ? null : list;
    }

    //

    public String getQuota()
    {
        String dataSourceName = getEnrollmentCampaign().isUseCompetitionGroup() ? EcDistributionList.DISTRIBUTION_CG_DS : EcDistributionList.DISTRIBUTION_ED_DS;

        EcgDistributionDTO distributionDTO = getConfig().getDataSource(dataSourceName).getCurrent();

        if (distributionDTO.getPersistentId() == null || distributionDTO.isDetailDistribution()) return null;

        return _quotaMap.get(distributionDTO.getPersistentId());
    }

    public String getTaQuota()
    {
        String dataSourceName = getEnrollmentCampaign().isUseCompetitionGroup() ? EcDistributionList.DISTRIBUTION_CG_DS : EcDistributionList.DISTRIBUTION_ED_DS;

        EcgDistributionDTO distributionDTO = getConfig().getDataSource(dataSourceName).getCurrent();

        if (distributionDTO.getPersistentId() == null || distributionDTO.isDetailDistribution()) return null;

        return _taQuotaMap.get(distributionDTO.getPersistentId());
    }

    // Getters

    public ISelectModel getFormativeOrgUnitListModel()
    {
        return _formativeOrgUnitListModel;
    }

    public ISelectModel getTerritorialOrgUnitListModel()
    {
        return _territorialOrgUnitListModel;
    }

    public ISelectModel getEducationLevelHighSchoolListModel()
    {
        return _educationLevelHighSchoolListModel;
    }

    public ISelectModel getDevelopFormListModel()
    {
        return _developFormListModel;
    }

    public ISelectModel getDevelopTechListModel()
    {
        return _developTechListModel;
    }

    public ISelectModel getDevelopConditionListModel()
    {
        return _developConditionListModel;
    }

    public ISelectModel getDevelopPeriodListModel()
    {
        return _developPeriodListModel;
    }

    // Listeners

    public void onClickShow()
    {
        saveSettings();
    }

    public void onClickClear()
    {
        clearSettings();
        initDefaults();
        saveSettings();
    }

    public void onClickBulkMake()
    {
        Set<EcgConfigDTO> selectedConfigSet = getSelectedConfigSet();

        _uiSupport.doRefresh();

        refreshConfigSet(selectedConfigSet);

        try
        {
            EcDistributionManager.instance().dao().doBulkMakeDistribution(selectedConfigSet);
            clearSelectedConfigSet();
        } catch (ApplicationException e)
        {
            _uiSupport.doRefresh();

            throw e;
        }
    }

    public void onClickBulkFill()
    {
        Set<EcgConfigDTO> selectedConfigSet = getSelectedConfigSet();

        _uiSupport.doRefresh();

        refreshConfigSet(selectedConfigSet);

        try
        {
            EcDistributionManager.instance().dao().doBulkFillDistribution(selectedConfigSet);
            clearSelectedConfigSet();
        } catch (ApplicationException e)
        {
            _uiSupport.doRefresh();

            throw e;
        }
    }
    public void onClickBulkFillWithOriginal()
    {
        Set<EcgConfigDTO> selectedConfigSet = getSelectedConfigSet();

        _uiSupport.doRefresh();

        refreshConfigSet(selectedConfigSet);

        try
        {
            EcDistributionManager.instance().dao().doBulkFillWithOriginalDistribution(selectedConfigSet);
            clearSelectedConfigSet();
        } catch (ApplicationException e)
        {
            _uiSupport.doRefresh();

            throw e;
        }
    }

    public void onClickBulkLock()
    {
        Set<EcgConfigDTO> selectedConfigSet = getSelectedConfigSet();

        _uiSupport.doRefresh();

        refreshConfigSet(selectedConfigSet);

        try
        {
            EcDistributionManager.instance().dao().doBulkLockDistribution(selectedConfigSet);
            clearSelectedConfigSet();
        } catch (ApplicationException e)
        {
            _uiSupport.doRefresh();

            throw e;
        }
    }

    public void onClickBulkApprove()
    {
        Set<EcgConfigDTO> selectedConfigSet = getSelectedConfigSet();

        _uiSupport.doRefresh();

        refreshConfigSet(selectedConfigSet);

        try
        {
            EcDistributionManager.instance().dao().doBulkApproveDistribution(selectedConfigSet);
            clearSelectedConfigSet();
        } catch (ApplicationException e)
        {
            _uiSupport.doRefresh();

            throw e;
        }
    }

    public void onClickBulkDelete()
    {
        Set<EcgConfigDTO> selectedConfigSet = getSelectedConfigSet();

        _uiSupport.doRefresh();

        refreshConfigSet(selectedConfigSet);

        EcDistributionManager.instance().dao().doBulkDeleteDistribution(selectedConfigSet);
        clearSelectedConfigSet();
    }

    public void onClickBulkRtfPrint()
    {
        Set<Long> ids = getSelectedDistributionIds();

        _uiSupport.doRefresh();

        if (ids.isEmpty()) return;

        byte[] data = EcDistributionManager.instance().dao().getBulkRtfPrint(ids);

        String fileName = new SimpleDateFormat("yyyyMMddHHmm").format(new Date()) + "-enrollmentDistributionEnvironment.zip";

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().zip().fileName(fileName).document(data), false);
    }

    public void onClickBulkXmlPrint()
    {
        Set<Long> ids = getSelectedDistributionIds();

        _uiSupport.doRefresh();

        if (ids.isEmpty()) return;

        byte[] data = EcDistributionManager.instance().dao().getBulkXmlPrint(ids);

        String fileName = new SimpleDateFormat("yyyyMMddHHmm").format(new Date()) + "-enrollmentDistributionEnvironment.xml";

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().xml().fileName(fileName).document(data), false);
    }

    public void onClickAdd()
    {
        _uiSupport.doRefresh();

        EcgDistributionDTO item = id2valueMap.get(getListenerParameterAsLong());

        if (item == null || item.isAddDisabled()) return;

        DataAccessServices.dao().getComponentSession().refresh(item.getEcgItem());

        _uiActivation.asRegion(EcDistributionAddEdit.class).parameter(EcDistributionAddEditUI.DISTRIBUTION_DTO, item).activate();
    }

    public void onClickAddDetail()
    {
        _uiSupport.doRefresh();

        EcgDistributionDTO item = id2valueMap.get(getListenerParameterAsLong());

        if (item == null || item.isAddDetailDisabled()) return;

        EcDistributionManager.instance().dao().saveDistributionDetail(item.getPersistentId());
    }

    public void onClickRtfPrint() throws Exception
    {
        _uiSupport.doRefresh();

        EcgDistributionDTO item = id2valueMap.get(getListenerParameterAsLong());

        if (item == null || item.isDownloadDisabled()) return;

        DataAccessServices.dao().getComponentSession().refresh(item.getEcgItem());

        String fileName = EcDistributionManager.instance().dao().getDistributionFileName(item);

        byte[] data = EcDistributionManager.instance().dao().getRtfPrint(item.getPersistentId());

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName(fileName + ".rtf").document(data), false);
    }

    public void onClickXmlPrint() throws Exception
    {
        _uiSupport.doRefresh();

        EcgDistributionDTO item = id2valueMap.get(getListenerParameterAsLong());

        if (item == null || item.isDownloadDisabled()) return;

        DataAccessServices.dao().getComponentSession().refresh(item.getEcgItem());

        String fileName = EcDistributionManager.instance().dao().getDistributionFileName(item);

        byte[] data = EcDistributionManager.instance().dao().getXmlPrint(item.getPersistentId());

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().xml().fileName(fileName + ".soap.xml").document(data), false);
    }

    public void onEditEntityFromList()
    {
        _uiSupport.doRefresh();

        EcgDistributionDTO item = id2valueMap.get(getListenerParameterAsLong());

        if (item == null || item.isEditDisabled() || item.getPersistentId() == null) return;

        DataAccessServices.dao().getComponentSession().refresh(item.getEcgItem());

        _uiActivation.asRegion(EcDistributionAddEdit.class).parameter(EcDistributionAddEditUI.DISTRIBUTION_DTO, item).activate();
    }

    public void onDeleteEntityFromList()
    {
        _uiSupport.doRefresh();

        EcgDistributionDTO item = id2valueMap.get(getListenerParameterAsLong());

        if (item == null || item.isDeleteDisabled() || item.getPersistentId() == null) return;

        if (item.isDetailDistribution())
            EcDistributionManager.instance().dao().deleteDistributionDetail(item.getPersistentId());
        else
            EcDistributionManager.instance().dao().deleteDistribution(item.getPersistentId());
    }

    // Private

    private Set<EcgConfigDTO> getSelectedConfigSet()
    {
        String dataSourceName = getEnrollmentCampaign().isUseCompetitionGroup() ? EcDistributionList.DISTRIBUTION_CG_DS : EcDistributionList.DISTRIBUTION_ED_DS;

        Collection<IEntity> selected = ((CheckboxColumn) (((BaseSearchListDataSource) _uiConfig.getDataSource(dataSourceName)).getLegacyDataSource()).getColumn("checkbox")).getSelectedObjects();

        Set<EcgConfigDTO> configSet = new HashSet<>();

        for (IEntity entity : selected)
            configSet.add(((EcgDistributionDTO) entity).getConfigDTO());

        return configSet;
    }

    private void refreshConfigSet(Set<EcgConfigDTO> configSet)
    {
        Session session = DataAccessServices.dao().getComponentSession();
        for (EcgConfigDTO item : configSet)
            session.refresh(item.getEcgItem());
    }

    private Set<Long> getSelectedDistributionIds()
    {
        String dataSourceName = getEnrollmentCampaign().isUseCompetitionGroup() ? EcDistributionList.DISTRIBUTION_CG_DS : EcDistributionList.DISTRIBUTION_ED_DS;

        Collection<IEntity> selected = ((CheckboxColumn) (((BaseSearchListDataSource) _uiConfig.getDataSource(dataSourceName)).getLegacyDataSource()).getColumn("checkbox")).getSelectedObjects();

        Set<Long> distributionIds = new HashSet<>();

        for (IEntity entity : selected)
        {
            EcgDistributionDTO item = (EcgDistributionDTO) entity;
            if (!item.isDownloadDisabled())
                distributionIds.add(item.getPersistentId());
        }

        return distributionIds;
    }

    private void clearSelectedConfigSet()
    {
        String dataSourceName = getEnrollmentCampaign().isUseCompetitionGroup() ? EcDistributionList.DISTRIBUTION_CG_DS : EcDistributionList.DISTRIBUTION_ED_DS;

        ((CheckboxColumn) (((BaseSearchListDataSource) _uiConfig.getDataSource(dataSourceName)).getLegacyDataSource()).getColumn("checkbox")).getSelectedObjects().clear();
    }
}
