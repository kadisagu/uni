/* $Id$ */
package ru.tandemservice.uniec.component.report.EntrantDailyRatingByED2.EntrantDailyRatingByED2Pub;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report;

/**
 * @author Andrey Andreev
 * @since 16.06.2016
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setReport(getNotNull(EntrantDailyRatingByED2Report.class, model.getReport().getId()));
    }
}
