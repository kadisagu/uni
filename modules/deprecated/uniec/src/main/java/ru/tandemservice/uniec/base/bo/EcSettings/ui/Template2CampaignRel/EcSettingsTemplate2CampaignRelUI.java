/* $Id: $ */
package ru.tandemservice.uniec.base.bo.EcSettings.ui.Template2CampaignRel;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.shared.survey.base.entity.SurveyTemplate;
import ru.tandemservice.uniec.base.bo.EcSettings.EcSettingsManager;
import ru.tandemservice.uniec.base.bo.EcSettings.logic.Template2CampaignRelDSHandler;
import ru.tandemservice.uniec.entity.survey.SurveyTemplate2EnrCampaignRel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Andrey Andreev
 * @since 21.03.2017
 */
public class EcSettingsTemplate2CampaignRelUI extends UIPresenter
{
    private Long _editCampaignId;

    @Override
    public void onComponentRefresh()
    {
        if (getSettings().get(EcSettingsManager.EDUCATION_YEAR_PARAM) == null)
            getSettings().set(EcSettingsManager.EDUCATION_YEAR_PARAM, EducationYear.getCurrentRequired());
    }

    public void onRefresh()
    {
        saveSettings();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EcSettingsManager.EDUCATION_YEAR_PARAM, getEducationYear());
    }


    // Listeners
    public void onClickEdit()
    {
        if (_editCampaignId != null) return;

        IUIDataSource dataSource = getConfig().getDataSource(EcSettingsTemplate2CampaignRel.SEARCH_LIST_DS);
        if (dataSource != null)
        {
            Map<Long, DataWrapper> dataById = dataSource.<DataWrapper>getRecords().stream().collect(Collectors.toMap(DataWrapper::getId, wrapper -> wrapper));

            _editCampaignId = getListenerParameterAsLong();
            DataWrapper wrapper = dataById.get(_editCampaignId);

            if (wrapper != null)
            {
                SurveyTemplate2EnrCampaignRel rel = wrapper.get(Template2CampaignRelDSHandler.REL);
                _uiSettings.set(EcSettingsTemplate2CampaignRel.TEMPLATE_PARAM, rel == null ? null : rel.getTemplate());
            }
        }
    }

    public void onClickSave()
    {
        EcSettingsManager.instance().template2CampaignRelDAO()
                .setSurveyTemplate2EnrCampaignRel(getListenerParameterAsLong(), _uiSettings.get(EcSettingsTemplate2CampaignRel.TEMPLATE_PARAM));
        getSupport().setRefreshScheduled(true);
        _editCampaignId = null;
    }

    public void onClickCancel()
    {
        _editCampaignId = null;
    }


    // Getters & Setters
    public EducationYear getEducationYear()
    {
        return getSettings().get(EcSettingsManager.EDUCATION_YEAR_PARAM);
    }

    public boolean isNothingSelected()
    {
        return getEducationYear() == null;
    }


    public boolean isCurrentInEditMode()
    {
        return (_editCampaignId != null) && (getCurrent() != null) && _editCampaignId.equals(getCurrent().getId());
    }


    public boolean isEditNow()
    {
        return _editCampaignId != null;
    }

    public boolean isNotEditNow()
    {
        return !isEditNow();
    }

    public DataWrapper getCurrent()
    {
        return getConfig().getDataSource(EcSettingsTemplate2CampaignRel.SEARCH_LIST_DS).getCurrent();
    }

    public SurveyTemplate2EnrCampaignRel getCurrentRel()
    {
        return getCurrent().get(Template2CampaignRelDSHandler.REL);
    }

    public SurveyTemplate getCurrentTemplate()
    {
        SurveyTemplate2EnrCampaignRel rel = getCurrentRel();
        return rel == null ? null : rel.getTemplate();
    }

    public String getCurrentTemplateTitle()
    {
        SurveyTemplate template = getCurrentTemplate();
        return template == null ? "" : template.getTitle();
    }
}