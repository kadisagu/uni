/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.logic;

import java.util.List;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;

/**
 * @author Vasily Zhukov
 * @since 07.07.2011
 */
public class EcSimpleDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String LIST = "list";
    public static final String PAGEABLE = "pageable";

    public EcSimpleDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        List list = context.get(LIST);
        boolean pageable = Boolean.TRUE.equals(context.get(PAGEABLE));
        return ListOutputBuilder.get(input, list).pageable(pageable).build();
    }
}
