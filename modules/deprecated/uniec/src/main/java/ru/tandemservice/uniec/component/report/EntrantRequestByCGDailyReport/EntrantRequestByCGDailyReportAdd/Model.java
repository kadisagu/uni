/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EntrantRequestByCGDailyReport.EntrantRequestByCGDailyReportAdd;

import java.util.List;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.CompetitionGroup;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.report.EntrantRequestByCGDailyReport;

/**
 * @author agolubenko
 * @since Jun 10, 2010
 */
public class Model implements IEnrollmentCampaignSelectModel
{
    private EntrantRequestByCGDailyReport _report = new EntrantRequestByCGDailyReport();

    private List<CompetitionGroup> _competitionGroupList;

    private List<EnrollmentCampaign> _enrollmentCampaignList;
    private ISelectModel _developFormList;
    private ISelectModel _competitionGroupListModel;

    @Override
    public IDataSettings getSettings()
    {
        return null;
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return getReport().getEnrollmentCampaign();
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        getReport().setEnrollmentCampaign(enrollmentCampaign);
    }

    public EntrantRequestByCGDailyReport getReport()
    {
        return _report;
    }

    public void setReport(EntrantRequestByCGDailyReport report)
    {
        _report = report;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public ISelectModel getDevelopFormList() { return _developFormList; }
    public void setDevelopFormList(ISelectModel developFormList) { _developFormList = developFormList; }

    public List<CompetitionGroup> getCompetitionGroupList()
    {
        return _competitionGroupList;
    }

    public void setCompetitionGroupList(List<CompetitionGroup> competitionGroupList)
    {
        _competitionGroupList = competitionGroupList;
    }

    public ISelectModel getCompetitionGroupListModel()
    {
        return _competitionGroupListModel;
    }

    public void setCompetitionGroupListModel(ISelectModel competitionGroupListModel)
    {
        _competitionGroupListModel = competitionGroupListModel;
    }
}
