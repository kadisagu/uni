/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EnrollmentDirectionPub;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline;
import ru.tandemservice.uniec.entity.entrant.ProfileEducationOrgUnit;
import ru.tandemservice.uniec.entity.examset.ExamPriorityComparator;

import java.util.*;

/**
 * @author Боба
 * @since 30.07.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    @SuppressWarnings("unchecked")
    public void prepare(Model model)
    {
        model.setEnrollmentDirection(getNotNull(EnrollmentDirection.class, model.getEnrollmentDirection().getId()));
        model.setCompensationTypes(getCatalogItemList(CompensationType.class));
        model.setStudentCategoryList(getCatalogItemListOrderByCode(StudentCategory.class));

        Map<Boolean, Map<Long, Integer>> id2value = UniecDAOFacade.getTargetAdmissionDao().listTargetAdmissionPlanValues(model.getEnrollmentDirection());

        model.setTargetAdmissionPlanBudget(formateTargetAdmission(id2value.get(true), model.getEnrollmentDirection().getTargetAdmissionPlanBudget()));
        model.setTargetAdmissionPlanContract(formateTargetAdmission(id2value.get(false), model.getEnrollmentDirection().getTargetAdmissionPlanContract()));

        if (existsEntity(ProfileEducationOrgUnit.class, ProfileEducationOrgUnit.L_ENROLLMENT_DIRECTION, model.getEnrollmentDirection()))
        {
            Object[] sum = new DQLSelectBuilder().fromEntity(ProfileEducationOrgUnit.class, "e")
                    .column(DQLFunctions.sum(DQLExpressions.property(ProfileEducationOrgUnit.budgetPlan().fromAlias("e"))))
                    .column(DQLFunctions.sum(DQLExpressions.property(ProfileEducationOrgUnit.contractPlan().fromAlias("e"))))
                    .where(DQLExpressions.eq(DQLExpressions.property(ProfileEducationOrgUnit.enrollmentDirection().fromAlias("e")), DQLExpressions.value(model.getEnrollmentDirection())))
                    .createStatement(getSession()).uniqueResult();

            Integer budgetPlan = model.getEnrollmentDirection().getMinisterialPlan();
            Integer contractPlan = model.getEnrollmentDirection().getContractPlan();

            int sumBudgetPlan = sum[0] == null ? 0 : ((Number) sum[0]).intValue();
            int sumContractPlan = sum[1] == null ? 0 : ((Number) sum[1]).intValue();

            model.setBudgetPlanStyle(budgetPlan != null && !budgetPlan.equals(sumBudgetPlan) ? "color:red" : "");
            model.setContractPlanStyle(contractPlan != null && !contractPlan.equals(sumContractPlan) ? "color:red" : "");
        }
    }

    @SuppressWarnings("unchecked")
    private String formateTargetAdmission(Map<Long, Integer> id2value, Integer targetAdmissionPlan)
    {
        if (targetAdmissionPlan == null)
            return "";

        if (getCount(TargetAdmissionKind.class) == 1)
            return targetAdmissionPlan.toString();

        StringBuilder sb = new StringBuilder();
        sb.append(targetAdmissionPlan);


        Criteria criteria = getSession().createCriteria(TargetAdmissionKind.class);
        criteria.add(Restrictions.isNull(TargetAdmissionKind.L_PARENT));
        criteria.addOrder(Order.asc(TargetAdmissionKind.P_TITLE));
        List<TargetAdmissionKind> kinds = criteria.list();

        Iterator<TargetAdmissionKind> iter = kinds.iterator();
        boolean first = true;
        boolean added = false;
        while (iter.hasNext())
        {
            TargetAdmissionKind kind = iter.next();
            Integer value = id2value.get(kind.getId());
            if (null == value || 0 == value) continue;

            if (first)
            {
                sb.append(" (");
                added = true;
            }
            if (!first)
                sb.append(", ");
            first = false;
            sb.append(kind.getTitle());
            sb.append(" — ");
            sb.append(value.toString());
        }
        if (added)
            sb.append(")");
        return sb.toString();
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareListDataSource(Model model)
    {
        DynamicListDataSource dataSource;

        // если нет различий в наборах, то показываем набор для студентов
        if (!model.getEnrollmentDirection().getEnrollmentCampaign().isExamSetDiff())
        {
            model.setStudentCategory(getCatalogItem(StudentCategory.class, UniDefines.STUDENT_CATEGORY_STUDENT));
            dataSource = model.getDataSource();
        } else
        {
            dataSource = model.getDataSourceMap().get(model.getStudentCategory());
        }

        MQBuilder builder = new MQBuilder(EntranceDiscipline.ENTITY_CLASS, "d");
        builder.add(MQExpression.eq("d", EntranceDiscipline.L_ENROLLMENT_DIRECTION, model.getEnrollmentDirection()));
        builder.add(MQExpression.eq("d", EntranceDiscipline.L_STUDENT_CATEGORY, model.getStudentCategory()));
        List<EntranceDiscipline> result = builder.getResultList(getSession());

        Collections.sort(result, new ExamPriorityComparator());
        dataSource.setCountRow(result.size());
        UniBaseUtils.createPage(dataSource, result);
    }

    @Override
    public void prepareProfileDataSource(Model model)
    {
        List<ProfileEducationOrgUnit> list = new DQLSelectBuilder().fromEntity(ProfileEducationOrgUnit.class, "e")
                .where(DQLExpressions.eq(DQLExpressions.property(ProfileEducationOrgUnit.enrollmentDirection().fromAlias("e")), DQLExpressions.value(model.getEnrollmentDirection())))
                .createStatement(getSession()).list();

        Comparator<ProfileEducationOrgUnit> comparator = new EntityComparator<ProfileEducationOrgUnit>(
                new EntityOrder(ProfileEducationOrgUnit.educationOrgUnit().educationLevelHighSchool().fullTitle().s()),
                new EntityOrder(ProfileEducationOrgUnit.educationOrgUnit().developForm().code().s()),
                new EntityOrder(ProfileEducationOrgUnit.educationOrgUnit().developCondition().code().s()),
                new EntityOrder(ProfileEducationOrgUnit.educationOrgUnit().developTech().code().s()),
                new EntityOrder(ProfileEducationOrgUnit.educationOrgUnit().developPeriod().code().s()),
                new EntityOrder(ProfileEducationOrgUnit.educationOrgUnit().id())
        );
        Collections.sort(list, comparator);

        model.getProfileDataSource().setCountRow(list.size());
        UniBaseUtils.createPage(model.getProfileDataSource(), list);
    }
}
