// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.enrollmentextract;

import ru.tandemservice.uni.entity.employee.OrderData;
import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentRevertExtract;

/**
 * @author oleyba
 * @since 04.03.2010
 */
public interface ICommonEnrollmentExtractDao
{
    public static final SpringBeanCache<ICommonEnrollmentExtractDao> instance = new SpringBeanCache<ICommonEnrollmentExtractDao>("uniecCommonEnrollmentExtractDao");

    OrderData doCommit(EnrollmentExtract extract);
    void doRollback(EnrollmentExtract extract);
    void doCommitRevertExtract(EnrollmentRevertExtract revertExtract);
}
