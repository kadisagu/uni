/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EnrollmentResultMinzdrav2009.Add;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.utils.CommonYesNoUIObject;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.entity.report.EnrollmentResultMinzdrav2009;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.Date;

/**
 * @author vip_delete
 * @since 18.08.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    @SuppressWarnings("deprecation")
    public void prepare(final Model model)
    {
        model.setParameters(new MultiEnrollmentDirectionUtil.Parameters());
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());

        model.getReport().setDateFrom(model.getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : model.getEnrollmentCampaign().getStartDate());
        model.getReport().setDateTo(CoreDateUtils.getDayLastTimeMoment(new Date()));
        model.setStudentCategoryListModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(StudentCategory.class)));
        model.setQualificationListModel(new QualificationModel(getSession()));

        model.setFormativeOrgUnitListModel(MultiEnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setTerritorialOrgUnitListModel(MultiEnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setDevelopFormListModel(MultiEnrollmentDirectionUtil.createDevelopFormModel(model));
        model.setDevelopConditionListModel(MultiEnrollmentDirectionUtil.createDevelopConditionModel(model));
        model.setDevelopTechListModel(MultiEnrollmentDirectionUtil.createDevelopTechModel(model));
        model.setDevelopPeriodListModel(MultiEnrollmentDirectionUtil.createDevelopPeriodModel(model));

        model.setParallelList(CommonYesNoUIObject.createYesNoList());
    }

    @Override
    public void update(Model model)
    {
        final Session session = getSession();

        EnrollmentResultMinzdrav2009 report = model.getReport();
        report.setFormingDate(new Date());

        if (model.isStudentCategoryActive())
            report.setStudentCategoryTitle(UniStringUtils.join(model.getStudentCategoryList(), StudentCategory.P_TITLE, "; "));
        if (model.isQualificationActive())
            report.setQualificationTitle(StringUtils.join(CommonBaseUtil.getPropertiesList(model.getQualificationList(), Qualifications.P_TITLE), "; "));

        if (model.isFormativeOrgUnitActive())
            report.setFormativeOrgUnitTitle(StringUtils.join(CommonBaseUtil.getPropertiesList(model.getFormativeOrgUnitList(), OrgUnit.P_FULL_TITLE), "; "));
        if (model.isTerritorialOrgUnitActive())
            report.setTerritorialOrgUnitTitle(StringUtils.join(CommonBaseUtil.getPropertiesList(model.getTerritorialOrgUnitList(), OrgUnit.P_TERRITORIAL_FULL_TITLE), "; "));
        report.setDevelopFormTitle(model.getDevelopForm().getTitle());
        if (model.isDevelopConditionActive())
            report.setDevelopConditionTitle(StringUtils.join(CommonBaseUtil.getPropertiesList(model.getDevelopConditionList(), DevelopCondition.P_TITLE), "; "));
        if (model.isDevelopTechActive())
            report.setDevelopTechTitle(StringUtils.join(CommonBaseUtil.getPropertiesList(model.getDevelopTechList(), DevelopTech.P_TITLE), "; "));
        if (model.isDevelopPeriodActive())
            report.setDevelopPeriodTitle(StringUtils.join(CommonBaseUtil.getPropertiesList(model.getDevelopPeriodList(), DevelopPeriod.P_TITLE), "; "));

        if (model.isParallelActive())
            report.setExcludeParallel(model.getParallel().isTrue());

        DatabaseFile databaseFile = new EnrollmentResultMinzdrav2009ReportBuilder(model, session).getContent();
        session.save(databaseFile);

        report.setContent(databaseFile);
        session.save(report);
    }
}
