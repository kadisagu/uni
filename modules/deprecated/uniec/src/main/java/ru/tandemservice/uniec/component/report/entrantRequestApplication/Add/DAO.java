/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.report.entrantRequestApplication.Add;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.report.EntrantRequestApplicationReport;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.*;
import java.util.Map.Entry;

/**
 * @author agolubenko
 * @since 24.06.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.getReport().setDateFrom(model.getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : model.getEnrollmentCampaign().getStartDate());
        model.getReport().setDateTo(CoreDateUtils.getDayFirstTimeMoment(new Date()));
        model.setDevelopFormList(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setCompensationTypeList(getCatalogItemListOrderByCode(CompensationType.class));
        model.setDevelopConditionModel(EducationCatalogsManager.getDevelopConditionSelectModel());
        model.setQualificationListModel(new QualificationModel(getSession()));
        model.setStudentCategoryListModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(StudentCategory.class)));
        model.setFormativeOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING));
        model.setTerritorialOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL));
    }

    @Override
    public void update(Model model)
    {
        EntrantRequestApplicationReport report = model.getReport();
        report.setFormingDate(new Date());
        report.setDevelopForm(model.getDevelopForm());
        report.setCompensationType(model.getCompensationType());
        report.setDevelopCondition(UniStringUtils.join(model.getDevelopConditionList(), DevelopCondition.P_TITLE, ", "));
        report.setQualification(UniStringUtils.join(model.getQualificationList(), StudentCategory.P_TITLE, ", "));
        report.setStudentCategory(UniStringUtils.join(model.getStudentCategoryList(), StudentCategory.P_TITLE, ", "));
        if (model.isFormativeOrgUnitActive())
            report.setFormativeOrgUnit(model.getFormativeOrgUnit().getTitle());
        if (model.isTerritorialOrgUnitActive())
            report.setTerritorialOrgUnit((model.getTerritorialOrgUnit() != null) ? model.getTerritorialOrgUnit().getTerritorialTitle() : "не указано");

        DatabaseFile databaseFile = new DatabaseFile();
        databaseFile.setContent(RtfUtil.toByteArray(getDocument(model)));
        getSession().save(databaseFile);

        report.setContent(databaseFile);
        getSession().save(report);
    }

    /**
     * @param model модель
     * @return документ, сформированный по шаблону
     */
    private RtfDocument getDocument(Model model)
    {
        IScriptItem templateDocument = getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ENTRANT_REQUEST_APPLICATION);
        RtfDocument template = new RtfReader().read(templateDocument.getCurrentTemplate());
        RtfDocument document = template.getClone();

        EntrantRequestApplicationReport report = model.getReport();

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("date", DateFormatter.DEFAULT_DATE_FORMATTER.format(report.getDateTo()));
        injectModifier.modify(document);

        fillTables(model, document);

        return document;
    }

    /**
     * Заполняет таблицы отчета
     *
     * @param model    модель
     * @param document документ
     */
    private void fillTables(Model model, RtfDocument document)
    {
        // получаем шаблон таблицы и удаляем его из документа
        List<IRtfElement> elementList = document.getElementList();
        RtfTable tableTemplate = (RtfTable) UniRtfUtil.findElement(elementList, "T");
        int tableIndex = elementList.indexOf(tableTemplate);
        elementList.remove(tableIndex);

        // итоги по всему отчету
        Row reportTotal = new Row("Итого:");

        // для каждой таблицы данных
        for (Entry<OrgUnit, Map<EducationLevelsHighSchool, Row>> entry : getData(model).entrySet())
        {
            // вставить новую таблицу в документ
            RtfTable table = tableTemplate.getClone();
            elementList.add(tableIndex++, table);

            // отсортировать строки
            List<Row> rows = new ArrayList<>(entry.getValue().values());
            Collections.sort(rows);

            // посчитать итоговые результаты и преобразовать строки в ячейки
            Row total = new Row("Итого:");
            List<String[]> cells = new ArrayList<>();
            for (Row row : rows)
            {
                total.add(row);
                cells.add(row.toStringArray());
            }
            cells.add(total.toStringArray());

            // посчитать итоговые результаты по всему отчету
            reportTotal.add(total);

            // заполнить таблицу
            UniRtfUtil.modify(table, cells.toArray(new String[][]{}));

            // заполнить строку с формирующим подразделением
            new RtfInjectModifier().put("formativeOrgUnit", entry.getKey().getNominativeCaseTitle()).modify(Collections.<IRtfElement>singletonList(table));
        }

        // вставить новую таблицу для итогов по отчету
        RtfTable table = tableTemplate.getClone();
        elementList.add(tableIndex++, table);

        // заполнить ее
        UniRtfUtil.modify(table, new String[][]{reportTotal.toStringArray()});
        new RtfInjectModifier().put("formativeOrgUnit", "Итоги по отчету").modify(Collections.<IRtfElement>singletonList(table));
    }

    /**
     * @param model модель
     * @return данные отчета: формирующее подр. -> направление подготовки (специальности) ОУ -> строка таблицы
     */
    @SuppressWarnings("unchecked")
    private Map<OrgUnit, Map<EducationLevelsHighSchool, Row>> getData(Model model)
    {
        EntrantRequestApplicationReport report = model.getReport();

        // сформировать условия выборки
        Criteria criteria = getSession().createCriteria(RequestedEnrollmentDirection.class);
        criteria.createAlias(RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "request");
        criteria.createAlias(RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, "enrollmentDirection");
        criteria.createAlias("enrollmentDirection." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "educationOrgUnit");
        criteria.createAlias("educationOrgUnit." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "educationLevelHighSchool");
        criteria.createAlias("educationLevelHighSchool." + EducationLevelsHighSchool.L_EDUCATION_LEVEL, "educationLevel");
        criteria.createAlias("request." + EntrantRequest.L_ENTRANT, "entrant");

        criteria.add(Restrictions.eq("entrant." + Entrant.P_ARCHIVAL, Boolean.FALSE));
        criteria.add(Restrictions.eq("entrant." + Entrant.L_ENROLLMENT_CAMPAIGN, report.getEnrollmentCampaign()));
        criteria.add(Restrictions.eq("educationOrgUnit." + EducationOrgUnit.L_DEVELOP_FORM, report.getDevelopForm()));
        criteria.add(Restrictions.eq(RequestedEnrollmentDirection.L_COMPENSATION_TYPE, report.getCompensationType()));

        if (!model.getDevelopConditionList().isEmpty())
            criteria.add(Restrictions.in("educationOrgUnit." + EducationOrgUnit.L_DEVELOP_CONDITION, model.getDevelopConditionList()));

        if (!model.getQualificationList().isEmpty())
            criteria.add(Restrictions.in("educationLevel." + EducationLevels.L_QUALIFICATION, model.getQualificationList()));

        if (!model.getStudentCategoryList().isEmpty())
            criteria.add(Restrictions.in(RequestedEnrollmentDirection.L_STUDENT_CATEGORY, model.getStudentCategoryList()));

        if (model.isFormativeOrgUnitActive())
            criteria.add(Restrictions.eq("educationOrgUnit." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnit()));

        if (model.isTerritorialOrgUnitActive())
            criteria.add(model.getTerritorialOrgUnit() == null ?
                    Restrictions.isNull("educationOrgUnit." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT) :
                    Restrictions.eq("educationOrgUnit." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnit())
            );

        Map<OrgUnit, Map<EducationLevelsHighSchool, Row>> result = SafeMap.get(key -> SafeMap.get(key1 -> new Row(key1.getPrintTitle())));

        // для каждого выбранного направления приема, удовлетворяющего условиям отчета
        for (RequestedEnrollmentDirection requestedEnrollmentDirection : (List<RequestedEnrollmentDirection>) criteria.list())
        {
            // получить строку
            EducationOrgUnit educationOrgUnit = requestedEnrollmentDirection.getEnrollmentDirection().getEducationOrgUnit();
            Row row = result.get(educationOrgUnit.getFormativeOrgUnit()).get(educationOrgUnit.getEducationLevelHighSchool());

            Date dateTo = report.getDateTo();
            Date dateFrom = report.getDateFrom();
            Date regDate = requestedEnrollmentDirection.getEntrantRequest().getRegDate();

            // подсчитать его в нужных ячейках
            row.total++;
            if (regDate.compareTo(dateFrom) >= 0 && regDate.before(dateTo))
            {
                row.untilReportingDate++;
            }
            if (regDate.compareTo(dateTo) >= 0 && regDate.before(CoreDateUtils.add(dateTo, Calendar.DATE, 1)))
            {
                row.onReportingDate++;
            }
            if (requestedEnrollmentDirection.getState().getCode().equals(UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY))
            {
                row.takenDocumentsAway++;
            }
        }

        return result;
    }

    /**
     * Строка таблицы отчета
     *
     * @author agolubenko
     * @since 26.06.2009
     */
    private static class Row implements Comparable<Row>
    {
        String title;
        int total;
        int untilReportingDate;
        int onReportingDate;
        int takenDocumentsAway;

        public Row(String title)
        {
            this.title = title;
        }

        String[] toStringArray()
        {
            return new String[]{title, Integer.toString(total), Integer.toString(untilReportingDate), Integer.toString(onReportingDate), Integer.toString(takenDocumentsAway)};
        }

        /**
         * Добавляет результаты другой строки к текущей
         *
         * @param row другая строка
         */
        void add(Row row)
        {
            total += row.total;
            untilReportingDate += row.untilReportingDate;
            onReportingDate += row.onReportingDate;
            takenDocumentsAway += row.takenDocumentsAway;
        }

        @Override
        public int compareTo(Row o)
        {
            return title.compareTo(o.title);
        }
    }
}
