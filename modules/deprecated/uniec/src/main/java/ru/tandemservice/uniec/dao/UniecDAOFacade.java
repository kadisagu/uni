/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.dao;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.IFormatter;

import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager;
import ru.tandemservice.uniec.base.bo.EcOrder.logic.IEcOrderDao;
import ru.tandemservice.uniec.dao.examgroup.IExamGroupSetDao;
import ru.tandemservice.uniec.dao.examlist.IEntrantExamListNumberGenerator;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.ui.EntrantRequestNumberFormatter;
import ru.tandemservice.uniec.ui.RequestedEnrollmentDirectionNumberFormatter;

/**
 * @author agolubenko
 * @since 13.10.2008
 */
@SuppressWarnings("unchecked")
public class UniecDAOFacade
{
    private static IExamSetDAO _examSetDAO;
    private static IEntranceEduLevelDAO _entranceEduLevelDAO;
    private static IEntrantDAO _entrantDAO;
    private static ISettingsDAO _settingsDAO;
    private static ITargetAdmissionDAO _targetAdmissionKindDao;
    private static IEntrantExamListNumberGenerator _entrantExamListNumberGenerator;
    private static IFormatter<Integer> _entrantRequestNumberFormatter;
    private static IFormatter<RequestedEnrollmentDirection> _requestedEnrollmentDirectionNumberFormatter;
    private static IOnlineEntrantRegistrationDAO _onlineEntrantRegistrationDAO;
    private static IExamGroupSetDao _examGroupSetDao;
    private static IUniecExportDao _exportDao;
    private static IUniecOrderDAO _orderDao;

    public static IExamSetDAO getExamSetDAO()
    {
        if (_examSetDAO == null)
            _examSetDAO = (IExamSetDAO) ApplicationRuntime.getBean(IExamSetDAO.class.getName());
        return _examSetDAO;
    }

    public static IEntranceEduLevelDAO getEntranceEduLevelDAO()
    {
        if (_entranceEduLevelDAO == null)
            _entranceEduLevelDAO = (IEntranceEduLevelDAO) ApplicationRuntime.getBean(IEntranceEduLevelDAO.class.getName());
        return _entranceEduLevelDAO;
    }

    public static IEntrantDAO getEntrantDAO()
    {
        if (_entrantDAO == null)
            _entrantDAO = (IEntrantDAO) ApplicationRuntime.getBean(IEntrantDAO.class.getName());
        return _entrantDAO;
    }

    public static ISettingsDAO getSettingsDAO()
    {
        if (_settingsDAO == null)
            _settingsDAO = (ISettingsDAO) ApplicationRuntime.getBean(ISettingsDAO.class.getName());
        return _settingsDAO;
    }

    public static ITargetAdmissionDAO getTargetAdmissionDao()
    {
        if (_targetAdmissionKindDao == null)
            _targetAdmissionKindDao = (ITargetAdmissionDAO) ApplicationRuntime.getBean(ITargetAdmissionDAO.class.getName());
        return _targetAdmissionKindDao;
    }

    public static IEntrantExamListNumberGenerator getEntrantExamListNumberGenerator()
    {
        if (_entrantExamListNumberGenerator == null)
            _entrantExamListNumberGenerator = (IEntrantExamListNumberGenerator) ApplicationRuntime.getBean(IEntrantExamListNumberGenerator.class.getName());
        return _entrantExamListNumberGenerator;
    }

    /**
     * This method will be removed in the future
     * Please, use direct call: EcOrderManager.instance().dao()
     *
     * @return IEcOrderDao
     */
    @Deprecated
    public static IEcOrderDao getEnrollmentMoveDao()
    {
        return EcOrderManager.instance().dao();
    }

    public static IFormatter<Integer> getEntrantRequestNumberFormatter()
    {
        if (_entrantRequestNumberFormatter == null)
            _entrantRequestNumberFormatter = (IFormatter<Integer>) ApplicationRuntime.getBean(EntrantRequestNumberFormatter.class.getName());
        return _entrantRequestNumberFormatter;
    }

    public static IFormatter<RequestedEnrollmentDirection> getRequestedEnrollmentDirectionNumberFormatter()
    {
        if (_requestedEnrollmentDirectionNumberFormatter == null)
            _requestedEnrollmentDirectionNumberFormatter = (IFormatter<RequestedEnrollmentDirection>) ApplicationRuntime.getBean(RequestedEnrollmentDirectionNumberFormatter.class.getName());
        return _requestedEnrollmentDirectionNumberFormatter;
    }

    public static IOnlineEntrantRegistrationDAO getOnlineEntrantRegistrationDAO()
    {
        if (_onlineEntrantRegistrationDAO == null)
            _onlineEntrantRegistrationDAO = (IOnlineEntrantRegistrationDAO) ApplicationRuntime.getBean(IOnlineEntrantRegistrationDAO.class.getName());
        return _onlineEntrantRegistrationDAO;
    }

    public static IExamGroupSetDao getExamGroupSetDao()
    {
        if (_examGroupSetDao == null)
            _examGroupSetDao = (IExamGroupSetDao) ApplicationRuntime.getBean(IExamGroupSetDao.class.getName());
        return _examGroupSetDao;
    }

    public static IUniecExportDao getExportDao()
    {
        if (_exportDao == null)
            _exportDao = (IUniecExportDao) ApplicationRuntime.getBean(IUniecExportDao.BEAN_NAME);
        return _exportDao;
    }

    public static IUniecOrderDAO getOrderDao()
    {
        if (_orderDao == null)
            _orderDao = (IUniecOrderDAO) ApplicationRuntime.getBean(IUniecOrderDAO.BEAN_NAME);
        return _orderDao;
    }
}