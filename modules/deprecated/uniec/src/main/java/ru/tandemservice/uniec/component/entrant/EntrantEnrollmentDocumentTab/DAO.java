/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.EntrantEnrollmentDocumentTab;

import java.util.List;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.catalog.EnrollmentDocument;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantEnrollmentDocument;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;

/**
 * @author oleyba
 * @since 17.03.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setEntrant(getNotNull(Entrant.class, model.getEntrant().getId()));
        model.setEntrantRequestList(getList(EntrantRequest.class, EntrantRequest.L_ENTRANT, model.getEntrant(), EntrantRequest.P_REG_DATE));
    }

    @Override
    public void prepareListDataSource(Model model, EntrantRequest entrantRequest)
    {
        MQBuilder builder = new MQBuilder(EntrantEnrollmentDocument.ENTITY_CLASS, "d");
        builder.add(MQExpression.eq("d", EntrantEnrollmentDocument.L_ENTRANT_REQUEST, entrantRequest));
        builder.addOrder("d", EntrantEnrollmentDocument.L_ENROLLMENT_DOCUMENT + "." + EnrollmentDocument.P_PRIORITY);
        List<EntrantEnrollmentDocument> list = builder.getResultList(getSession());
        DynamicListDataSource<EntrantEnrollmentDocument> dataSource = model.getDataSourceMap().get(entrantRequest);
        dataSource.setCountRow(list.size());
        UniBaseUtils.createPage(dataSource, list);
    }
}