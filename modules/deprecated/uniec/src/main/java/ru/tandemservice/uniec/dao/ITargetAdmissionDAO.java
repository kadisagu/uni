/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.dao;

import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import java.util.List;
import java.util.Map;

/**
 * @author Боба
 * @since 09.09.2008
 */
public interface ITargetAdmissionDAO
{
    List<TargetAdmissionKind> listTopTargetAdmissionKinds(EnrollmentCampaign campaign);

    List<HSelectOption> getTargetAdmissionKindOptionList(EnrollmentCampaign campaign);
    List<TargetAdmissionKind> getTargetAdmissionKindList(EnrollmentCampaign campaign);

    Map<Boolean, Map<Long, Integer>> listTargetAdmissionPlanValues(EnrollmentDirection direction);
}
