/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantTransferDirectionSelection;

import java.util.Collections;
import java.util.List;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

/**
 * @author agolubenko
 * @since 06.04.2009
 */
public class DAO extends UniDao<Model>
{
    @Override
    public void prepare(Model model)
    {
        model.setEntrant(get(Entrant.class, model.getEntrantId()));
        model.setOrderType(get(EntrantEnrollmentOrderType.class, model.getOrderTypeId()));
        model.setEnrollmentDirection(get(EnrollmentDirection.class, model.getEnrollmentDirectionId()));
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "requestedEnrollmentDirection");
        builder.addJoin("requestedEnrollmentDirection", RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "request");
        builder.addJoin("requestedEnrollmentDirection", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "educationLevelHighSchool");
        builder.add(MQExpression.eq("request", EntrantRequest.L_ENTRANT, model.getEntrant()));

        DynamicListDataSource<RequestedEnrollmentDirection> dataSource = model.getDataSource();

        // сортировка
        List<RequestedEnrollmentDirection> result = builder.getResultList(getSession());
        EntityComparator<RequestedEnrollmentDirection> comparator = new EntityComparator<RequestedEnrollmentDirection>();
        comparator.addFirst(new EntityOrder(new String[] { RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, EnrollmentDirection.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, EducationLevelsHighSchool.P_DISPLAYABLE_TITLE }, dataSource.getEntityOrder().getDirection()));
        Collections.sort(result, comparator);

        UniBaseUtils.createPage(dataSource, result);
    }

    @Override
    public void update(Model model)
    {
        EntrantEnrollmentOrderType orderType = model.getOrderType();
        RequestedEnrollmentDirection direction = model.getRequestedEnrollmentDirection();
        EducationOrgUnit eduOrgUnit = model.getEnrollmentDirection().getEducationOrgUnit();

        PreliminaryEnrollmentStudent student = new PreliminaryEnrollmentStudent();
        student.setRequestedEnrollmentDirection(direction);
        student.setEducationOrgUnit(eduOrgUnit);
        student.setEntrantEnrollmentOrderType(orderType);
        student.setCompensationType(orderType.getCompensationType());
        student.setTargetAdmission(direction.isTargetAdmission());
        student.setStudentCategory(direction.getStudentCategory());

        CoreCollectionUtils.Pair<String, PreliminaryEnrollmentStudent> result = UniecDAOFacade.getEntrantDAO().saveOrUpdatePreliminaryStudent(student);
        String error = result.getX();
        if (null != error)
            throw new ApplicationException(error);
    }
}
