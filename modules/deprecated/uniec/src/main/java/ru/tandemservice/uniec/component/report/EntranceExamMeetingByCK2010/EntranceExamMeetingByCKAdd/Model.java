/* $Id: Model.java 9406 2009-07-27 09:57:20Z agolubenko $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EntranceExamMeetingByCK2010.EntranceExamMeetingByCKAdd;

import java.util.ArrayList;
import java.util.List;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil.Parameters;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil.Parameters.Filter;

/**
 * @author Vasily Zhukov
 * @since 04.08.2010
 */
public class Model implements IEnrollmentCampaignSelectModel, MultiEnrollmentDirectionUtil.Model
{
    public static final long PRINT_FORM_1 = 1;
    public static final long PRINT_FORM_2 = 2;
    public static final long PRINT_FORM_3 = 3;

    private static final Parameters FILTER_PARAMETERS = new Parameters();
    static
    {
        FILTER_PARAMETERS.setRequired(true, Filter.ENROLLMENT_CAMPAIGN, Filter.FORMATIVE_ORG_UNIT);
    }

    private EntranceExamMeetingByCKReport2010 _report = new EntranceExamMeetingByCKReport2010();

    private List<IdentifiableWrapper> _printFormList;
    private ISelectModel _formativeOrgUnitModel;
    private ISelectModel _territorialOrgUnitModel;
    private ISelectModel _educationLevelsHighSchoolModel;
    private ISelectModel _developFormModel;
    private ISelectModel _developTechModel;
    private ISelectModel _developConditionModel;
    private ISelectModel _developPeriodModel;
    private ISelectModel _qualificationListModel;
    private ISelectModel _developFormListModel;
    private ISelectModel _developConditionListModel;
    private ISelectModel _studentCategoryListModel;
    private List<CompensationType> _compensationTypes;
    private List<EnrollmentCampaign> _enrollmentCampaignList;

    private boolean _territorialOrgUnitActive;
    private boolean _educationLevelsHighSchoolActive;
    private boolean _developFormActive;
    private boolean _developConditionActive;
    private boolean _developTechActive;
    private boolean _developPeriodActive;
    private boolean _notIncludeEnrollment;

    private boolean _byAllEnrollmentDirections;
    private boolean _groupByEducationLevel;
    private IdentifiableWrapper _printForm;
    private List<OrgUnit> _formativeOrgUnitList = new ArrayList<OrgUnit>();
    private List<OrgUnit> _territorialOrgUnitList = new ArrayList<OrgUnit>();
    private List<EducationLevelsHighSchool> _educationLevelsHighSchoolList = new ArrayList<EducationLevelsHighSchool>();
    private List<Qualifications> _qualificationList = new ArrayList<Qualifications>();
    private List<DevelopForm> _developFormList = new ArrayList<DevelopForm>();
    private List<DevelopTech> _developTechList = new ArrayList<DevelopTech>();
    private List<DevelopPeriod> _developPeriodList = new ArrayList<DevelopPeriod>();
    private List<DevelopCondition> _developConditionList = new ArrayList<DevelopCondition>();
    private List<StudentCategory> _studentCategoryList = new ArrayList<StudentCategory>();
    private IPrincipalContext _principalContext;

    private ISelectModel _customStateRulesModel;
    private DataWrapper _customStateRule;
    private ISelectModel _entrantCustomStateListModel;
    private List<EntrantCustomStateCI> _entrantCustomStateList;

    // IEnrollmentCampaignModel

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _report.getEnrollmentCampaign();
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _report.setEnrollmentCampaign(enrollmentCampaign);
    }

    @Override
    public IDataSettings getSettings()
    {
        return null;
    }

    @Override
    public Parameters getParameters()
    {
        return FILTER_PARAMETERS;
    }

    // MultiEnrollmentDirectionUtil.Model

    @Override
    public List<EnrollmentCampaign> getSelectedEnrollmentCampaignList()
    {
        return MultiEnrollmentDirectionUtil.getSingletonList(getEnrollmentCampaign());
    }

    @Override
    public List<OrgUnit> getSelectedFormativeOrgUnitList()
    {
        return getFormativeOrgUnitList();
    }

    @Override
    public List<OrgUnit> getSelectedTerritorialOrgUnitList()
    {
        return isTerritorialOrgUnitActive() ? getTerritorialOrgUnitList() : null;
    }

    @Override
    public List<EducationLevelsHighSchool> getSelectedEducationLevelHighSchoolList()
    {
        return isEducationLevelsHighSchoolActive() ? getEducationLevelsHighSchoolList() : null;
    }

    @Override
    public List<DevelopForm> getSelectedDevelopFormList()
    {
        return isDevelopFormActive() ? getDevelopFormList() : null;
    }

    @Override
    public List<DevelopCondition> getSelectedDevelopConditionList()
    {
        return isDevelopConditionActive() ? getDevelopConditionList() : null;
    }

    @Override
    public List<DevelopPeriod> getSelectedDevelopPeriodList()
    {
        return isDevelopPeriodActive() ? getDevelopPeriodList() : null;
    }

    @Override
    public List<DevelopTech> getSelectedDevelopTechList()
    {
        return isDevelopTechActive() ? getDevelopTechList() : null;
    }

    // Getters & Setters

    public EntranceExamMeetingByCKReport2010 getReport()
    {
        return _report;
    }

    public void setReport(EntranceExamMeetingByCKReport2010 report)
    {
        _report = report;
    }

    public List<IdentifiableWrapper> getPrintFormList()
    {
        return _printFormList;
    }

    public void setPrintFormList(List<IdentifiableWrapper> printFormList)
    {
        _printFormList = printFormList;
    }

    public IdentifiableWrapper getPrintForm()
    {
        return _printForm;
    }

    public void setPrintForm(IdentifiableWrapper printForm)
    {
        _printForm = printForm;
    }

    public boolean isTerritorialOrgUnitActive()
    {
        return _territorialOrgUnitActive;
    }

    public void setTerritorialOrgUnitActive(boolean territorialOrgUnitActive)
    {
        _territorialOrgUnitActive = territorialOrgUnitActive;
    }

    public boolean isEducationLevelsHighSchoolActive()
    {
        return _educationLevelsHighSchoolActive;
    }

    public void setEducationLevelsHighSchoolActive(boolean educationLevelsHighSchoolActive)
    {
        _educationLevelsHighSchoolActive = educationLevelsHighSchoolActive;
    }

    public boolean isDevelopFormActive()
    {
        return _developFormActive;
    }

    public void setDevelopFormActive(boolean developFormActive)
    {
        _developFormActive = developFormActive;
    }

    public boolean isDevelopConditionActive()
    {
        return _developConditionActive;
    }

    public void setDevelopConditionActive(boolean developConditionActive)
    {
        _developConditionActive = developConditionActive;
    }

    public boolean isDevelopTechActive()
    {
        return _developTechActive;
    }

    public void setDevelopTechActive(boolean developTechActive)
    {
        _developTechActive = developTechActive;
    }

    public boolean isDevelopPeriodActive()
    {
        return _developPeriodActive;
    }

    public void setDevelopPeriodActive(boolean developPeriodActive)
    {
        _developPeriodActive = developPeriodActive;
    }

    public ISelectModel getFormativeOrgUnitModel()
    {
        return _formativeOrgUnitModel;
    }

    public void setFormativeOrgUnitModel(ISelectModel formativeOrgUnitModel)
    {
        _formativeOrgUnitModel = formativeOrgUnitModel;
    }

    public ISelectModel getTerritorialOrgUnitModel()
    {
        return _territorialOrgUnitModel;
    }

    public void setTerritorialOrgUnitModel(ISelectModel territorialOrgUnitModel)
    {
        _territorialOrgUnitModel = territorialOrgUnitModel;
    }

    public ISelectModel getEducationLevelsHighSchoolModel()
    {
        return _educationLevelsHighSchoolModel;
    }

    public void setEducationLevelsHighSchoolModel(ISelectModel educationLevelsHighSchoolModel)
    {
        _educationLevelsHighSchoolModel = educationLevelsHighSchoolModel;
    }

    public ISelectModel getDevelopFormModel()
    {
        return _developFormModel;
    }

    public void setDevelopFormModel(ISelectModel developFormModel)
    {
        _developFormModel = developFormModel;
    }

    public ISelectModel getDevelopTechModel()
    {
        return _developTechModel;
    }

    public void setDevelopTechModel(ISelectModel developTechModel)
    {
        _developTechModel = developTechModel;
    }

    public ISelectModel getDevelopConditionModel()
    {
        return _developConditionModel;
    }

    public void setDevelopConditionModel(ISelectModel developConditionModel)
    {
        _developConditionModel = developConditionModel;
    }

    public ISelectModel getDevelopPeriodModel()
    {
        return _developPeriodModel;
    }

    public void setDevelopPeriodModel(ISelectModel developPeriodModel)
    {
        _developPeriodModel = developPeriodModel;
    }

    public ISelectModel getQualificationListModel()
    {
        return _qualificationListModel;
    }

    public void setQualificationListModel(ISelectModel qualificationListModel)
    {
        _qualificationListModel = qualificationListModel;
    }

    public ISelectModel getDevelopFormListModel()
    {
        return _developFormListModel;
    }

    public void setDevelopFormListModel(ISelectModel developFormListModel)
    {
        _developFormListModel = developFormListModel;
    }

    public ISelectModel getDevelopConditionListModel()
    {
        return _developConditionListModel;
    }

    public void setDevelopConditionListModel(ISelectModel developConditionListModel)
    {
        _developConditionListModel = developConditionListModel;
    }

    public ISelectModel getStudentCategoryListModel()
    {
        return _studentCategoryListModel;
    }

    public void setStudentCategoryListModel(ISelectModel studentCategoryListModel)
    {
        _studentCategoryListModel = studentCategoryListModel;
    }

    public List<CompensationType> getCompensationTypes()
    {
        return _compensationTypes;
    }

    public void setCompensationTypes(List<CompensationType> compensationTypes)
    {
        _compensationTypes = compensationTypes;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public boolean isByAllEnrollmentDirections()
    {
        return _byAllEnrollmentDirections;
    }

    public void setByAllEnrollmentDirections(boolean byAllEnrollmentDirections)
    {
        _byAllEnrollmentDirections = byAllEnrollmentDirections;
    }

    public boolean isGroupByEducationLevel()
    {
        return _groupByEducationLevel;
    }

    public void setGroupByEducationLevel(boolean groupByEducationLevel)
    {
        _groupByEducationLevel = groupByEducationLevel;
    }

    public List<OrgUnit> getFormativeOrgUnitList()
    {
        return _formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(List<OrgUnit> formativeOrgUnitList)
    {
        _formativeOrgUnitList = formativeOrgUnitList;
    }

    public List<OrgUnit> getTerritorialOrgUnitList()
    {
        return _territorialOrgUnitList;
    }

    public void setTerritorialOrgUnitList(List<OrgUnit> territorialOrgUnitList)
    {
        _territorialOrgUnitList = territorialOrgUnitList;
    }

    public List<EducationLevelsHighSchool> getEducationLevelsHighSchoolList()
    {
        return _educationLevelsHighSchoolList;
    }

    public void setEducationLevelsHighSchoolList(List<EducationLevelsHighSchool> educationLevelsHighSchoolList)
    {
        _educationLevelsHighSchoolList = educationLevelsHighSchoolList;
    }

    public List<DevelopTech> getDevelopTechList()
    {
        return _developTechList;
    }

    public void setDevelopTechList(List<DevelopTech> developTechList)
    {
        _developTechList = developTechList;
    }

    public List<DevelopPeriod> getDevelopPeriodList()
    {
        return _developPeriodList;
    }

    public void setDevelopPeriodList(List<DevelopPeriod> developPeriodList)
    {
        _developPeriodList = developPeriodList;
    }

    public List<Qualifications> getQualificationList()
    {
        return _qualificationList;
    }

    public void setQualificationList(List<Qualifications> qualificationList)
    {
        _qualificationList = qualificationList;
    }

    public List<DevelopForm> getDevelopFormList()
    {
        return _developFormList;
    }

    public void setDevelopFormList(List<DevelopForm> developFormList)
    {
        _developFormList = developFormList;
    }

    public List<DevelopCondition> getDevelopConditionList()
    {
        return _developConditionList;
    }

    public void setDevelopConditionList(List<DevelopCondition> developConditionList)
    {
        _developConditionList = developConditionList;
    }

    public List<StudentCategory> getStudentCategoryList()
    {
        return _studentCategoryList;
    }

    public void setStudentCategoryList(List<StudentCategory> studentCategoryList)
    {
        _studentCategoryList = studentCategoryList;
    }

    public IPrincipalContext getPrincipalContext()
    {
        return _principalContext;
    }

    public void setPrincipalContext(IPrincipalContext principalContext)
    {
        _principalContext = principalContext;
    }

    public boolean isNotIncludeEnrollment()
    {
        return _notIncludeEnrollment;
    }

    public void setNotIncludeEnrollment(boolean notIncludeEnrollment)
    {
        _notIncludeEnrollment = notIncludeEnrollment;
    }

    public DataWrapper getCustomStateRule()
    {
        return _customStateRule;
    }

    public Boolean getCustomStateRuleValue()
    {
        return TwinComboDataSourceHandler.getSelectedValue(_customStateRule);
    }

    public void setCustomStateRule(DataWrapper customStateRule)
    {
        _customStateRule = customStateRule;
    }

    public ISelectModel getCustomStateRulesModel()
    {
        return _customStateRulesModel;
    }

    public void setCustomStateRulesModel(ISelectModel customStateRulesModel)
    {
        _customStateRulesModel = customStateRulesModel;
    }

    public List<EntrantCustomStateCI> getEntrantCustomStateList()
    {
        return _entrantCustomStateList;
    }

    public void setEntrantCustomStateList(List<EntrantCustomStateCI> entrantCustomStateList)
    {
        _entrantCustomStateList = entrantCustomStateList;
    }

    public ISelectModel getEntrantCustomStateListModel()
    {
        return _entrantCustomStateListModel;
    }

    public void setEntrantCustomStateListModel(ISelectModel entrantCustomStateListModel)
    {
        _entrantCustomStateListModel = entrantCustomStateListModel;
    }

    public boolean getEntrantCustomStateListDisabled()
    {
        return _customStateRule == null;
    }
}
