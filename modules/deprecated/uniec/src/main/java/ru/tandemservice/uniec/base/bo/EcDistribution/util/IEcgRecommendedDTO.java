/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.util;

import org.tandemframework.core.entity.IEntity;

/**
 * @author Vasily Zhukov
 * @since 18.07.2011
 */
public interface IEcgRecommendedDTO extends IEntity, IEcgCortegeDTO
{
    public static enum DocumentStatus
    {
        BRING_ORIGINAL, // принес оригиналы
        BRING_COPY,     // принес копии
        TOOK_AWAY       // забрал документы
    }

    final String ENTRANT_RECOMMENDED = "entrantRecommended";
    final String FIO = "fio";
    final String COMPETITION_KIND_TITLE = "competitionKindTitle";
    final String TARGET_ADMISSION_KIND_TITLE = "targetAdmissionKindTitle";
    final String FINAL_MARK = "finalMark";
    final String PROFILE_MARK = "profileMark";
    final String CERTIFICATE_AVERAGE_MARK = "certificateAverageMark";
    final String GRADUATED_PROFILE_EDU_INSTITUTION_TITLE = "graduatedProfileEduInstitutionTitle";
    final String PRIORITIES = "priorities";
    final String ORIGINAL_DOCUMENT_HANDED_IN_TITLE = "originalDocumentHandedInTitle";

    /**
     * @return рекомендованный
     */
    IEcgEntrantRecommended getEntrantRecommended();

    /**
     * @return название вид конкурса
     */
    String getCompetitionKindTitle();

    /**
     * @return название вида цп
     */
    String getTargetAdmissionKindTitle();

    /**
     * @return "Да", если закончил профильное учебное учреждение по первому направлению из directionList
     *         "", в другом случае
     */
    String getGraduatedProfileEduInstitutionTitle();

    /**
     * @return сокр. названия всех НПВ из directionList
     */
    String getPriorities();

    /**
     * @return статус документов
     */
    DocumentStatus getDocumentStatus();

    /**
     * @return "оригиналы", если сданы оригиналы документов
     *         "копии", если сданы копии
     *         "забрал документы", в других случаях
     */
    String getOriginalDocumentHandedInTitle();
}
