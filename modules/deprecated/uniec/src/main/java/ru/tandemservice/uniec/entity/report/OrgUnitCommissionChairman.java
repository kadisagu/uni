package ru.tandemservice.uniec.entity.report;

import ru.tandemservice.uniec.component.settings.ProtocolVisaList.IProtocolEntity;
import ru.tandemservice.uniec.entity.report.gen.OrgUnitCommissionChairmanGen;

/**
 * Председатель приемной комиссии
 */
public class OrgUnitCommissionChairman extends OrgUnitCommissionChairmanGen implements IProtocolEntity
{
}