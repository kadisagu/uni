/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EnrollmentDirectionAdd;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author vip_delete
 * @since 16.02.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        prepareEducationOrgUnitDataSource(component);

        prepareEducationOrgUnitSelectedDataSource(component);
    }

    private void prepareEducationOrgUnitDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getEducationOrgUnitDataSource() != null)
        {
            return;
        }

        DynamicListDataSource<EducationOrgUnit> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareEducationOrgUnitDataSource(model);
        });
        dataSource.addColumn(new BlockColumn<Boolean>(Model.CHECKBOX_COLUMN, ""));
        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", EducationOrgUnit.educationLevelHighSchool().displayableTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Формирующее подр.", EducationOrgUnit.formativeOrgUnit().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Территориальное подр.", EducationOrgUnit.territorialOrgUnit().territorialShortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Форма освоения", EducationOrgUnit.developForm().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Условие освоения", EducationOrgUnit.developCondition().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Технология освоения", EducationOrgUnit.developTech().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Срок освоения", EducationOrgUnit.developPeriod().title().s()).setClickable(false).setOrderable(false));

        model.setEducationOrgUnitDataSource(dataSource);
    }

    private void prepareEducationOrgUnitSelectedDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getEducationOrgUnitSelectedDataSource() != null)
        {
            return;
        }

        DynamicListDataSource<EducationOrgUnit> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareEducationOrgUnitSelectedDataSource(model);
        });
        dataSource.addColumn(new BlockColumn<Boolean>(Model.CHECKBOX_SELECTED_COLUMN, ""));
        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", EducationOrgUnit.educationLevelHighSchool().displayableTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Формирующее подр.", EducationOrgUnit.formativeOrgUnit().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Территориальное подр.", EducationOrgUnit.territorialOrgUnit().territorialShortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Форма освоения", EducationOrgUnit.developForm().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Условие освоения", EducationOrgUnit.developCondition().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Технология освоения", EducationOrgUnit.developTech().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Срок освоения", EducationOrgUnit.developPeriod().title().s()).setClickable(false).setOrderable(false));

        model.setEducationOrgUnitSelectedDataSource(dataSource);
    }

    public void onClickApply(IBusinessComponent component)
    {
        getDao().update(getModel(component));
        deactivate(component);
    }

    public void onClickMove(IBusinessComponent component)
    {
        getDao().selectEducationOrgUnit(getModel(component));
    }

    @SuppressWarnings({"unchecked"})
    public void onClickDeleteSelected(IBusinessComponent component)
    {
        Model model = getModel(component);
        Map<Long, Boolean> map = ((BlockColumn) model.getEducationOrgUnitSelectedDataSource().getColumn(Model.CHECKBOX_SELECTED_COLUMN)).getValueMap();
        List<IEntity> selected = new ArrayList<>();
        for (Map.Entry<Long, Boolean> entry : map.entrySet())
            if (entry.getValue())
                selected.add(getDao().getNotNull(entry.getKey()));
        model.getSelectedEducationOrgUnitSet().removeAll(selected);
    }
}
