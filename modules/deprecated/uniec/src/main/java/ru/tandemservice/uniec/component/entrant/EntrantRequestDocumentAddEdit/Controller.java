/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.EntrantRequestDocumentAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.component.impl.BusinessComponent;
import org.tandemframework.core.component.impl.ComponentRegion;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.catalog.EnrollmentDocument;

/**
 * @author vip_delete
 * @since 06.06.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        prepareListDataSource(component);

        getDao().prepare(model);
    }

    @SuppressWarnings({"unchecked"})
    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<EnrollmentDocument> dataSource = UniBaseUtils.createDataSource(component, getDao());

        dataSource.addColumn(new SimpleColumn("Документ", EnrollmentDocument.P_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn(Model.BLOCK_COLUMN_AMOUNT, "Кол-во").setHeaderAlign("center").setAlign("center"));
        dataSource.addColumn(new BlockColumn(Model.BLOCK_COLUMN_COPY, "Копия").setHeaderAlign("center").setAlign("center"));

        model.setDataSource(dataSource);
    }

    public void onClickApply(IBusinessComponent component)
    {
        getDao().update(getModel(component));

        onClickDeactivate(component);
    }

    public void onClickDeactivate(IBusinessComponent component)
    {
        // на форме добавления надо делать 2 раза deactivate
        if (getModel(component).isAddForm())
            deactivate(component, 2);
        else
            deactivate(component);

        ((BusinessComponent) ((ComponentRegion) component.getParentRegion()).getOwner()).refresh();
        component.getUserContext().getRootRegion().getActiveComponent().refresh();
    }
}
