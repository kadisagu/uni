/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.wizard.StateExamDataStep;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.uniec.component.wizard.IEntrantWizardComponents;

/**
 * @author vip_delete
 * @since 16.06.2009
 */
public class Controller extends ru.tandemservice.uniec.component.entrant.EntrantStateExamCertificateAddEdit.Controller
{
    // Wizard Actions

    public void onClickNext(IBusinessComponent component)
    {
        ru.tandemservice.uniec.component.entrant.EntrantStateExamCertificateAddEdit.Model model = getModel(component);
        
        getDao().update(model);

        deactivate(component);

        component.getParentRegion().getOwner().createDefaultChildRegion(new ComponentActivator(IEntrantWizardComponents.ENTRANT_REQUEST_STEP, new ParametersMap()
                .add("entrantId", model.getCertificate().getEntrant().getId())
                .add("onlineEntrantId", ((Model) model).getOnlineEntrant().getId())
                .add("entrantMasterPermKey", ((Model) model).getEntrantMasterPermKey())
        ));
    }

    public void onClickSkip(IBusinessComponent component)
    {
        ru.tandemservice.uniec.component.entrant.EntrantStateExamCertificateAddEdit.Model model = getModel(component);
        
        deactivate(component);

        component.getParentRegion().getOwner().createDefaultChildRegion(new ComponentActivator(IEntrantWizardComponents.ENTRANT_REQUEST_STEP, new ParametersMap()
                .add("entrantId", model.getCertificate().getEntrant().getId())
                .add("onlineEntrantId", ((Model) model).getOnlineEntrant().getId())
                .add("entrantMasterPermKey", ((Model) model).getEntrantMasterPermKey())
        ));
    }

    public void onClickStop(IBusinessComponent component)
    {
        getDao().update(getModel(component));

        deactivate(component);
    }
}
