/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.settings.Qualification2CompetitionKind;

import java.util.List;
import java.util.Map;

import org.tandemframework.tapsupport.component.matrix.CellCoordinates;

import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;

/**
 * @author agolubenko
 * @since 17.06.2008
 */
public class Model extends ru.tandemservice.uniec.component.settings.AbstractMatrixSettingsComponent.Model<CompetitionKind, Qualifications, Boolean>
{
    private List<StudentCategory> _studentCategoryList;
    private StudentCategory _studentCategory;

    private Map<StudentCategory, Map<CellCoordinates, Boolean>> _matrixMapData;

    @Override
    public void setMatrixData(Map<CellCoordinates, Boolean> matrixData)
    {
        _matrixMapData.put(_studentCategory, matrixData);
    }

    @Override
    public Map<CellCoordinates, Boolean> getMatrixData()
    {
        return _matrixMapData.get(_studentCategory);
    }

    // Getters & Setters

    public List<StudentCategory> getStudentCategoryList()
    {
        return _studentCategoryList;
    }

    public void setStudentCategoryList(List<StudentCategory> studentCategoryList)
    {
        _studentCategoryList = studentCategoryList;
    }

    public StudentCategory getStudentCategory()
    {
        return _studentCategory;
    }

    public void setStudentCategory(StudentCategory studentCategory)
    {
        _studentCategory = studentCategory;
    }

    public Map<StudentCategory, Map<CellCoordinates, Boolean>> getMatrixMapData()
    {
        return _matrixMapData;
    }

    public void setMatrixMapData(Map<StudentCategory, Map<CellCoordinates, Boolean>> matrixMapData)
    {
        _matrixMapData = matrixMapData;
    }
}
