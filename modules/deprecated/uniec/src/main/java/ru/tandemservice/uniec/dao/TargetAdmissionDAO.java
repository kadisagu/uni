/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.dao;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.settings.NotUsedTargetAdmissionKind;
import ru.tandemservice.uniec.entity.settings.TargetAdmissionPlanRelation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Боба
 * @since 09.09.2008
 */
public class TargetAdmissionDAO extends UniBaseDao implements ITargetAdmissionDAO
{
    @Override
    public List<TargetAdmissionKind> listTopTargetAdmissionKinds(EnrollmentCampaign campaign)
    {
        MQBuilder builder = new MQBuilder(TargetAdmissionKind.ENTITY_CLASS, "ta");
        builder.add(MQExpression.isNull("ta", TargetAdmissionKind.parent().s()));
        MQBuilder notUsed = new MQBuilder(NotUsedTargetAdmissionKind.ENTITY_CLASS, "nu");
        notUsed.add(MQExpression.eqProperty("nu", NotUsedTargetAdmissionKind.targetAdmissionKind().id().s(), "ta", "id"));
        notUsed.add(MQExpression.eq("nu", NotUsedTargetAdmissionKind.enrollmentCampaign().s(), campaign));
        builder.add(MQExpression.notExists(notUsed));
        builder.addOrder("ta", TargetAdmissionKind.title().s());
        return builder.getResultList(getSession());
    }

    @Override
    public List<HSelectOption> getTargetAdmissionKindOptionList(EnrollmentCampaign campaign)
    {
        MQBuilder builder = new MQBuilder(TargetAdmissionKind.ENTITY_CLASS, "ta");
        MQBuilder notUsed = new MQBuilder(NotUsedTargetAdmissionKind.ENTITY_CLASS, "nu");
        notUsed.add(MQExpression.eqProperty("nu", NotUsedTargetAdmissionKind.targetAdmissionKind().id().s(), "ta", "id"));
        notUsed.add(MQExpression.eq("nu", NotUsedTargetAdmissionKind.enrollmentCampaign().s(), campaign));
        builder.add(MQExpression.notExists(notUsed));
        builder.add(MQExpression.notExists(new MQBuilder(TargetAdmissionKind.ENTITY_CLASS, "child").add(MQExpression.eqProperty("child", TargetAdmissionKind.parent().id().s(), "ta", "id"))));
        builder.addOrder("ta", TargetAdmissionKind.priority().s());
        return HierarchyUtil.listHierarchyNodesWithParents(builder.<TargetAdmissionKind>getResultList(getSession()), TargetAdmissionKind.ALL_LEVEL_PRIORITY_COMPARATOR, false);
    }

    @Override
    public List<TargetAdmissionKind> getTargetAdmissionKindList(EnrollmentCampaign campaign)
    {
        MQBuilder builder = new MQBuilder(TargetAdmissionKind.ENTITY_CLASS, "ta");
        MQBuilder notUsed = new MQBuilder(NotUsedTargetAdmissionKind.ENTITY_CLASS, "nu");
        notUsed.add(MQExpression.eqProperty("nu", NotUsedTargetAdmissionKind.targetAdmissionKind().id().s(), "ta", "id"));
        notUsed.add(MQExpression.eq("nu", NotUsedTargetAdmissionKind.enrollmentCampaign().s(), campaign));
        builder.add(MQExpression.notExists(notUsed));
        builder.add(MQExpression.notExists(new MQBuilder(TargetAdmissionKind.ENTITY_CLASS, "child").add(MQExpression.eqProperty("child", TargetAdmissionKind.parent().id().s(), "ta", "id"))));
        builder.addOrder("ta", TargetAdmissionKind.priority().s());
        return builder.getResultList(getSession());
    }

    @Override
    public Map<Boolean, Map<Long/*TargetAdmissionKind's id*/, Integer/*value*/>> listTargetAdmissionPlanValues(EnrollmentDirection enrollmentDirection)
    {
        Map<Long, Integer> budgetMap = new HashMap<Long, Integer>();
        Map<Long, Integer> contractMap = new HashMap<Long, Integer>();
        for (TargetAdmissionPlanRelation rel : getList(TargetAdmissionPlanRelation.class, TargetAdmissionPlanRelation.L_ENTRANCE_EDUCATION_ORG_UNIT, enrollmentDirection))
        {
            Map<Long, Integer> map = rel.isBudget() ? budgetMap : contractMap;
            TargetAdmissionKind kind = rel.getTargetAdmissionKind();
            while (kind.getParent() != null)
                kind = kind.getParent();

            Integer planValue = map.get(kind.getId());
            if (planValue == null) planValue = 0;

            planValue += rel.getPlanValue();
            map.put(kind.getId(), planValue);
        }
        Map<Boolean, Map<Long, Integer>> result = new HashMap<Boolean, Map<Long, Integer>>();
        result.put(true, budgetMap);
        result.put(false, contractMap);
        return result;
    }
}
