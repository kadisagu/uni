/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.wizard.EntrantDataStep;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.IUniecComponents;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.SourceInfoAboutUniversity;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrantInfoAboutUniversity;

import java.util.Date;
import java.util.List;

/**
 * @author vip_delete
 * @since 16.06.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        // init models
        model.setEntrant(getNotNull(Entrant.class, model.getEntrant().getId()));
        model.getEntrantEditModel().setEntrant(model.getEntrant());
        model.getDiplomaModel().setEntrantId(model.getEntrant().getId());

        // run prepares
        ((ru.tandemservice.uniec.component.entrant.EntrantEdit.IDAO) BusinessComponentUtils.getDao(IUniecComponents.ENTRANT_EDIT)).prepare(model.getEntrantEditModel());
        ((ru.tandemservice.uniec.component.entrant.OlympiadDiplomaAddEdit.IDAO) BusinessComponentUtils.getDao(IUniecComponents.OLYMPIAD_DIPLOMA_ADD_EDIT)).prepare(model.getDiplomaModel());
        
        Long onlineEntrantId = model.getOnlineEntrant().getId();
        if (onlineEntrantId != null)
        {
            model.setOnlineEntrant(get(OnlineEntrant.class, onlineEntrantId));
            prepareOnlineEntrantData(model);
        }
    }

    @Override
    public void update(Model model)
    {
        // run update methods
        ((ru.tandemservice.uniec.component.entrant.EntrantEdit.IDAO) BusinessComponentUtils.getDao(IUniecComponents.ENTRANT_EDIT)).update(model.getEntrantEditModel());

        if (model.isHasDiploma())
            ((ru.tandemservice.uniec.component.entrant.OlympiadDiplomaAddEdit.IDAO) BusinessComponentUtils.getDao(IUniecComponents.OLYMPIAD_DIPLOMA_ADD_EDIT)).update(model.getDiplomaModel());
    }
    
    protected void prepareOnlineEntrantData(Model model)
    {
        Entrant entrant = model.getEntrant();
        OnlineEntrant onlineEntrant = model.getOnlineEntrant();
        
        entrant.getPerson().setNeedDormitory(onlineEntrant.isNeedHotel());
        
        List<OnlineEntrantInfoAboutUniversity> onlineInfoSources = UniecDAOFacade.getOnlineEntrantRegistrationDAO().getUniversityInfoSources(onlineEntrant);
        model.getEntrantEditModel().setSourceInfoAboutUniversityList(CommonBaseUtil.<SourceInfoAboutUniversity> getPropertiesList(onlineInfoSources, OnlineEntrantInfoAboutUniversity.sourceInfo().s()));
        
        Date beginArmy = onlineEntrant.getBeginArmy();
        Date endArmy = onlineEntrant.getEndArmy();
        Integer endArmyYear = onlineEntrant.getEndArmyYear();
        
        if (beginArmy != null || endArmy != null || endArmyYear != null)
        {
            entrant.setPassArmy(true);
            entrant.setBeginArmy(beginArmy);
            entrant.setEndArmy(endArmy);
            entrant.setEndArmyYear(endArmyYear);
        }

        if (!StringUtils.isEmpty(onlineEntrant.getOlympiadDiplomaSubject()))
        {
            model.setHasDiploma(true);
            model.getDiplomaModel().getDiploma().setOlympiad(onlineEntrant.getOlympiadDiplomaTitle());
            model.getDiplomaModel().getDiploma().setNumber(onlineEntrant.getOlympiadDiplomaNumber());
            model.getDiplomaModel().getDiploma().setIssuanceDate(onlineEntrant.getOlympiadDiplomaIssuanceDate());
            model.getDiplomaModel().getDiploma().setDiplomaType(onlineEntrant.getOlympiadDiplomaType());
            model.getDiplomaModel().getDiploma().setDegree(onlineEntrant.getOlympiadDiplomaDegree());
            model.getDiplomaModel().getDiploma().setSubject(onlineEntrant.getOlympiadDiplomaSubject());
            model.getDiplomaModel().setCountry(onlineEntrant.getOlympiadAddressCountry());
            model.getDiplomaModel().getDiploma().setSettlement(onlineEntrant.getOlympiadAddressSettlement());
        }
    }
}
