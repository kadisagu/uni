/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.OlympiadDiplomaAddEdit;

import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.bo.Fias.FiasManager;
import org.tandemframework.shared.fias.base.bo.util.CountryAutocompleteModel;
import org.tandemframework.shared.fias.base.bo.util.SettlementAutocompleteModel;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.entity.catalog.OlympiadDiplomaDegree;
import ru.tandemservice.uniec.entity.catalog.OlympiadDiplomaType;
import ru.tandemservice.uniec.entity.entrant.Discipline2OlympiadDiplomaRelation;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.OlympiadDiploma;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.ui.EnrollmentDirectionUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @author oleyba
 * @since 04.03.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {

        model.setAddForm(model.getDiploma().getId() == null);

        if (model.isAddForm())
        {
            model.getDiploma().setEntrant(getNotNull(Entrant.class, model.getEntrantId()));
            model.setHasOlympiadDiplomaRelation(true);
            model.setTerritorialOrgUnit(TopOrgUnit.getInstance());
        } else
        {
            model.setDiploma(getNotNull(OlympiadDiploma.class, model.getDiploma().getId()));

            List<Discipline2OlympiadDiplomaRelation> relationList = new DQLSelectBuilder().fromEntity(Discipline2OlympiadDiplomaRelation.class, "e").column("e")
                    .where(DQLExpressions.eq(DQLExpressions.property(Discipline2OlympiadDiplomaRelation.diploma().fromAlias("e")), DQLExpressions.value(model.getDiploma())))
                    .createStatement(new DQLExecutionContext(getSession()))
                    .list();

            model.setHasOlympiadDiplomaRelation(!relationList.isEmpty());

            if (!relationList.isEmpty())
            {
                List<EnrollmentDirection> directionList = new ArrayList<>();
                List<CompensationType> compensationTypeSet = new ArrayList<>();

                Discipline2OlympiadDiplomaRelation rel = relationList.get(0);
                directionList.add(rel.getEnrollmentDirection());
                compensationTypeSet.add(rel.getCompensationType());
                Discipline2RealizationWayRelation discipline = rel.getDiscipline();


                EnrollmentDirection direction = directionList.iterator().next();
                if (direction != null)
                {
                    EducationOrgUnit ou = direction.getEducationOrgUnit();
                    model.setFormativeOrgUnit(ou.getFormativeOrgUnit());
                    model.setTerritorialOrgUnit(ou.getTerritorialOrgUnit());
                    model.setEducationLevelsHighSchool(ou.getEducationLevelHighSchool());
                    model.setDevelopForm(ou.getDevelopForm());
                    model.setDevelopCondition(ou.getDevelopCondition());
                    model.setDevelopTech(ou.getDevelopTech());
                    model.setDevelopPeriod(ou.getDevelopPeriod());
                }
                model.setCompensationType(compensationTypeSet.iterator().next());
                model.setSelectedDiscipline(discipline);
            }
        }

        model.setDegreesList(getCatalogItemListOrderByCode(OlympiadDiplomaDegree.class));
        model.setDiplomaTypeList(getCatalogItemListOrderByCode(OlympiadDiplomaType.class));
        model.setCountry(FiasManager.instance().kladrDao().getCountry(IKladrDefines.RUSSIA_COUNTRY_CODE));
        model.setCountriesModel(new CountryAutocompleteModel());
        model.setSettlementsModel(new SettlementAutocompleteModel(model));

        model.setFormativeOrgUnitModel(EnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(model));
        model.setTerritorialOrgUnitModel(EnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(model));
        model.setEducationLevelsHighSchoolModel(EnrollmentDirectionUtil.createEducationLevelsHighSchoolModel(model));
        model.setDevelopFormModel(EnrollmentDirectionUtil.createDevelopFormModel(model));
        model.setDevelopConditionModel(EnrollmentDirectionUtil.createDevelopConditionModel(model));
        model.setDevelopTechModel(EnrollmentDirectionUtil.createDevelopTechModel(model));
        model.setDevelopPeriodModel(EnrollmentDirectionUtil.createDevelopPeriodModel(model));

        model.setCompensationTypeList(getCatalogItemListOrderByCode(CompensationType.class));

        List<Discipline2RealizationWayRelation> list = new DQLSelectBuilder().fromEntity(Discipline2RealizationWayRelation.class, "e").column("e")
                .where(DQLExpressions.eq(DQLExpressions.property(Discipline2RealizationWayRelation.enrollmentCampaign().fromAlias("e")), DQLExpressions.value(model.getEnrollmentCampaign())))
                .order(DQLExpressions.property(Discipline2RealizationWayRelation.educationSubject().title().fromAlias("e")))
                .order(DQLExpressions.property(Discipline2RealizationWayRelation.subjectPassWay().code().fromAlias("e")))
                .createStatement(new DQLExecutionContext(getSession()))
                .list();

        model.setDisciplinesModel(new LazySimpleSelectModel<>(list));
    }

    @Override
    public void update(Model model)
    {
        final Session session = getSession();

        session.saveOrUpdate(model.getDiploma());

        if (model.isHasOlympiadDiplomaRelation())
        {
            // если форма редактирования, то все удаляем (на эти релейшены никто не ссылается)
            if (!model.isAddForm())
                for (Discipline2OlympiadDiplomaRelation rel : getList(Discipline2OlympiadDiplomaRelation.class, Discipline2OlympiadDiplomaRelation.L_DIPLOMA, model.getDiploma()))
                    session.delete(rel);

            session.flush();

            // определяем направление подготовки (специальность)
            EnrollmentDirection enrollmentDirection;
            if (model.getEnrollmentCampaign().isOlympiadDiplomaForDirection())
            {
                enrollmentDirection = EnrollmentDirectionUtil.getEnrollmentDirection(model, session);
                if (enrollmentDirection == null)
                    throw new ApplicationException("Не выбрано направление подготовки (специальность).");
            } else
            {
                enrollmentDirection = null;
            }

            // всегда создаем связь с выбранной дисциплиной
            Discipline2RealizationWayRelation discipline = model.getSelectedDiscipline();
            Discipline2OlympiadDiplomaRelation rel = new Discipline2OlympiadDiplomaRelation();
            rel.setDiploma(model.getDiploma());
            rel.setEnrollmentDirection(enrollmentDirection);
            rel.setCompensationType(model.getCompensationType());
            rel.setDiscipline(discipline);
            save(rel);

        } else
        {
            if (model.isAddForm())
            {
                // ничего не сохраняем
            } else
            {
                for (Discipline2OlympiadDiplomaRelation rel : getList(Discipline2OlympiadDiplomaRelation.class, Discipline2OlympiadDiplomaRelation.L_DIPLOMA, model.getDiploma()))
                    session.delete(rel);
            }
        }
    }
}