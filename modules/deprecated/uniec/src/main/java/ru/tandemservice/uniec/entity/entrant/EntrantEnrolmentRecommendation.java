package ru.tandemservice.uniec.entity.entrant;

import org.tandemframework.core.common.ITitled;

import ru.tandemservice.uniec.entity.entrant.gen.EntrantEnrolmentRecommendationGen;

public class EntrantEnrolmentRecommendation extends EntrantEnrolmentRecommendationGen implements ITitled
{
    public static final String P_TITLE = "title";

    @Override
    public String getTitle()
    {
        return getRecommendation().getTitle();
    }
}