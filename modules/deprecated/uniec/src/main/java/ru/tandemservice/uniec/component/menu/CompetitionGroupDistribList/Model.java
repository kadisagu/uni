/* $Id: Model.java 8192 2009-05-29 08:59:33Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.menu.CompetitionGroupDistribList;

import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.ecg.EcgDistribObject;
import ru.tandemservice.uniec.entity.entrant.CompetitionGroup;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

/**
 * @author vdanilov
 */
@State({
	@Bind(key="competitionGroupDistribId", binding="createdObjectId")
})
public class Model implements IEnrollmentCampaignSelectModel {

	private Long createdObjectId;
	public Long getCreatedObjectId() { return this.createdObjectId; }
	public void setCreatedObjectId(final Long createdObjectId) { this.createdObjectId = createdObjectId; }

	private IDataSettings _settings;
	private List<EnrollmentCampaign> _enrollmentCampaignList;
    private List<CompetitionGroup> _competitionGroupList;
    private List<CompensationType> _compensationTypeList;
    private List<IdentifiableWrapper> _categoryList;
    private DynamicListDataSource<EcgDistribObject> _dataSource;

	// IEnrollmentCampaignSelectModel

	@Override public EnrollmentCampaign getEnrollmentCampaign() {
		return (EnrollmentCampaign) getSettings().get(IEnrollmentCampaignSelectModel.ENROLLMENT_CAMPAIGN_FILTER_NAME);
	}

	@Override public void setEnrollmentCampaign(final EnrollmentCampaign enrollmentCampaign) {
		getSettings().set(IEnrollmentCampaignSelectModel.ENROLLMENT_CAMPAIGN_FILTER_NAME, enrollmentCampaign);
	}

	// Getters & Setters

	@Override
    public IDataSettings getSettings() {
		return _settings;
	}

	public void setSettings(final IDataSettings settings) {
		_settings = settings;
	}

	@Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList() {
		return _enrollmentCampaignList;
	}

	@Override
    public void setEnrollmentCampaignList(final List<EnrollmentCampaign> enrollmentCampaignList) {
		_enrollmentCampaignList = enrollmentCampaignList;
	}

    public List<CompetitionGroup> getCompetitionGroupList() {
        return _competitionGroupList;
    }

    public void setCompetitionGroupList(List<CompetitionGroup> competitionGroupList) {
        _competitionGroupList = competitionGroupList;
    }

    public List<CompensationType> getCompensationTypeList() {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList) {
        _compensationTypeList = compensationTypeList;
    }

    public List<IdentifiableWrapper> getCategoryList()
    {
        return _categoryList;
    }

    public void setCategoryList(List<IdentifiableWrapper> categoryList)
    {
        _categoryList = categoryList;
    }

    public DynamicListDataSource<EcgDistribObject> getDataSource() {
		return _dataSource;
	}

	public void setDataSource(final DynamicListDataSource<EcgDistribObject> dataSource) {
		_dataSource = dataSource;
	}
}
