/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.entity.examset;

import com.google.common.collect.ImmutableList;

import java.util.ArrayList;
import java.util.List;

/**
 * Структура набора экзаменов
 * У двух разных наборов экзаменов может быть одна и та же структура, например:
 * Набор 1 для категории студент:   мат. -> ин.яз
 * Набор 2 для категории слушатель: мат  -> англ., нем, фран.
 * <p/>
 * Наборы разные для этих двух категорий, но у них одинаковая структура, состоящая из 2-х строк,
 * которые можно назвать как "мат, ин.яз", а можно "мат, англ, нем, фран."
 * <p/>
 * Там где требуется выводить название строки структуры набора экзамена должна браться строка с самым коротким названием
 *
 * @author Vasily Zhukov
 * @since 04.08.2010
 */
public class ExamSetStructure
{
    private List<ExamSetStructureItem> _itemList = new ArrayList<>();

    public ExamSetStructure(List<ExamSetStructureItem> itemList)
    {
        // структура не может содержать пустых элементов
        for (ExamSetStructureItem item : itemList)
            if (item == null)
                throw new RuntimeException("ExamSetStructureItem cannot be null!");

        _itemList = ImmutableList.copyOf(itemList);
    }


    // Getters

    public List<ExamSetStructureItem> getItemList()
    {
        return _itemList;
    }

    @Override
    public int hashCode()
    {
        return _itemList.hashCode();
    }

    /**
     * Проверяет равенство структур с точностью до вида
     *
     * @param obj структура
     * @return true, если структуры равны с точностью до вида
     */
    public boolean kindEquals(ExamSetStructure obj)
    {
        if (obj == null) return false;

        List<ExamSetStructureItem> itemList = obj.getItemList();

        int len = itemList.size();

        if (_itemList.size() != len) return false;

        int i = 0;
        while (i < len && _itemList.get(i).getKind().equals(itemList.get(i).getKind())) i++;
        return i == len;
    }

    /**
     * Проверяет полное равенство структур
     *
     * @param obj структура
     * @return true, если структуры равны
     */
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;

        if (!(obj instanceof ExamSetStructure)) return false;

        ExamSetStructure other = (ExamSetStructure) obj;

        return other._itemList.equals(_itemList);
    }
}
