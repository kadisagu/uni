/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.settings.ReasonToBasicsEnrollmentSettings;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.IUniecComponents;
import ru.tandemservice.uniec.entity.catalog.EnrollmentOrderReason;

/**
 * @author alikhanov
 * @since 05.08.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<EnrollmentOrderReason> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn(getMessage("reasonCaption"), EnrollmentOrderReason.P_TITLE).setClickable(false));
        dataSource.addColumn(new PublisherLinkColumn(getMessage("basicCaption"), "title", EnrollmentOrderReason.P_BASICS).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit"));
        model.setDataSource(dataSource);
    }

    public void onClickEdit(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IUniecComponents.ENROLLMENT_ORDER_REASON_TO_BASICS_EDIT, new ParametersMap().add("enrollmentOrderReasonId", component.getListenerParameter())));
    }
}
