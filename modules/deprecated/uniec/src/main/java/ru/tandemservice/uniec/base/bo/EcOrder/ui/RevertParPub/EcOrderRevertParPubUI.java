/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcOrder.ui.RevertParPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager;
import ru.tandemservice.uniec.base.bo.EcOrder.ui.RevertParAddEdit.EcOrderRevertParAddEdit;
import ru.tandemservice.uniec.base.bo.EcOrder.ui.RevertParAddEdit.EcOrderRevertParAddEditUI;
import ru.tandemservice.uniec.base.bo.EcOrder.ui.RevertPub.EcOrderRevertPub;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniec.entity.orders.EnrollmentRevertParagraph;

/**
 * @author Nikolay Fedorovskih
 * @since 05.08.2013
 */
@State({
               @Bind(key = UIPresenter.PUBLISHER_ID, binding = "paragraphId", required = true)
       })
public class EcOrderRevertParPubUI extends UIPresenter
{
    private Long paragraphId;
    private EnrollmentRevertParagraph paragraph;

    @Override
    public void onComponentRefresh()
    {
        paragraph = DataAccessServices.dao().getNotNull(EnrollmentRevertParagraph.class, paragraphId);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EcOrderRevertParPub.PARAGRAPH_PARAM, paragraph);
    }

    private String getInternalOrderTitle(EnrollmentOrder order)
    {
        return "«" + order.getType().getTitle() + "» " + EcOrderRevertPub.orderTitleFormatter().format(order);
    }

    // Listeners

    public void onDeleteEntityFromList()
    {
        EcOrderManager.instance().dao().deleteRevertExtract(getListenerParameterAsLong());
    }

    public void onClickEdit()
    {
        _uiActivation.asDesktopRoot(EcOrderRevertParAddEdit.class)
                .parameter(EcOrderRevertParAddEditUI.PARAMETER_PARAGRAPH_ID, paragraphId)
                .activate();
    }

    public void onClickDelete()
    {
        EcOrderManager.instance().dao().deleteRevertParagraph(paragraphId);
        deactivate();
    }

    public void onClickPrintExtract()
    {
        EcOrderManager.instance().dao().getDownloadExtractPrintForm(getListenerParameterAsLong(), false);
    }

    // Getters & Setters

    public EnrollmentRevertParagraph getParagraph()
    {
        return paragraph;
    }

    public String getOrderTitle()
    {
        return getInternalOrderTitle((EnrollmentOrder) paragraph.getOrder());
    }

    public String getCanceledOrderTitle()
    {
        return getInternalOrderTitle(paragraph.getCanceledOrder());
    }

    public Long getParagraphId()
    {
        return paragraphId;
    }

    public void setParagraphId(Long paragraphId)
    {
        this.paragraphId = paragraphId;
    }
}