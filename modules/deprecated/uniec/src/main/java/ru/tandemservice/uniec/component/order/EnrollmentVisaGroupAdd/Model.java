/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.order.EnrollmentVisaGroupAdd;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

/**
 * @author vip_delete
 * @since 28.07.2009
 */
@Input(keys = {"documentId"}, bindings = {"documentId"})
public class Model
{
    private Long _documentId;
    private ISelectModel _visaGroupListModel;
    private IdentifiableWrapper _visaGroup;

    public Long getDocumentId()
    {
        return _documentId;
    }

    public void setDocumentId(Long documentId)
    {
        _documentId = documentId;
    }

    public ISelectModel getVisaGroupListModel()
    {
        return _visaGroupListModel;
    }

    public void setVisaGroupListModel(ISelectModel visaGroupListModel)
    {
        _visaGroupListModel = visaGroupListModel;
    }

    public IdentifiableWrapper getVisaGroup()
    {
        return _visaGroup;
    }

    public void setVisaGroup(IdentifiableWrapper visaGroup)
    {
        _visaGroup = visaGroup;
    }
}
