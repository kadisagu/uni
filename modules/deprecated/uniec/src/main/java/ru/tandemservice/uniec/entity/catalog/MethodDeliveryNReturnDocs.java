package ru.tandemservice.uniec.entity.catalog;

import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniec.entity.catalog.gen.*;

/** @see ru.tandemservice.uniec.entity.catalog.gen.MethodDeliveryNReturnDocsGen */
public class MethodDeliveryNReturnDocs extends MethodDeliveryNReturnDocsGen
{
    public static EntityComboDataSourceHandler defaultSelectDSHandler(String name)
    {
        return new EntityComboDataSourceHandler(name, MethodDeliveryNReturnDocs.class)
                .order(MethodDeliveryNReturnDocs.title())
                .filter(MethodDeliveryNReturnDocs.title());
    }
}