/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.ExamAdmissionByCompetitionGroup.ExamAdmissionByCompetitionGroupAdd;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.field.RtfField;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.gen.IdentityCardGen;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

import java.util.*;

/**
 * @author ekachanova
 */
public class ExamAdmissionByCGReportBuilder
{
    private static Map<String, String> _developConditionTitles = new HashMap<>();

    static
    {
        _developConditionTitles.put("Сокращенная программа", "сокращенной программе");
        _developConditionTitles.put("Ускоренная программа", "ускоренной программе");
        _developConditionTitles.put("Сокращенная ускоренная программа", "сокращенной ускоренной программе");
    }

    private static Map<String, String> _developFormTextByCode = new HashMap<>();

    static
    {
        _developFormTextByCode.put(DevelopFormCodes.FULL_TIME_FORM, "по очной форме обучения");
        _developFormTextByCode.put(DevelopFormCodes.CORESP_FORM, "по заочной форме обучения");
        _developFormTextByCode.put(DevelopFormCodes.PART_TIME_FORM, "по очной-заочной форме обучения");
        _developFormTextByCode.put(DevelopFormCodes.EXTERNAL_FORM, "по экстернату");
        _developFormTextByCode.put(DevelopFormCodes.APPLICANT_FORM, "по самостоятельному обучению и итоговой аттестации");
    }

    private Model _model;
    private Session _session;

    ExamAdmissionByCGReportBuilder(Model model, Session session)
    {
        _model = model;
        _session = session;
    }

    DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    private byte[] buildReport()
    {
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_EXAM_ADDMISSION_BY_COMPETITION_GROUP);
        RtfDocument template = new RtfReader().read(templateDocument.getCurrentTemplate());

        RtfDocument document = template.getClone();

        RtfInjectModifier modifier = new RtfInjectModifier();
        modifier.put("cg", _model.getCompetitionGroup().getTitle());
        String compensTypeText = _model.getReport().getCompensationType().getCode().equals(UniDefines.COMPENSATION_TYPE_BUDGET) ?
                ", финансируемые из федерального бюджета" : " с оплатой стоимости обучения";
        modifier.put("ct1", compensTypeText);
        modifier.put("ct2", compensTypeText);
        modifier.modify(document);

        modifyByFullTimeCondition(document);

        RtfTable t2Template = getT2Template(document);
        modifyByNotFullTimeCondition(document, t2Template);
        modifyBySecondHigh(document, t2Template);

        return RtfUtil.toByteArray(document);
    }

    @SuppressWarnings("deprecation")
    private void modifyByFullTimeCondition(RtfDocument document)
    {
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "red");
        builder.add(MQExpression.eq("red", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_CONDITION + "." + DevelopCondition.P_CODE, UniDefines.DEVELOP_CONDITION_FULL_TIME));
        builder.add(MQExpression.eq("red", RequestedEnrollmentDirection.L_COMPENSATION_TYPE, _model.getReport().getCompensationType()));
        builder.add(MQExpression.eq("red", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_COMPETITION_GROUP, _model.getCompetitionGroup()));
        builder.add(MQExpression.notEq("red", RequestedEnrollmentDirection.L_STUDENT_CATEGORY + "." + StudentCategory.P_CODE, UniDefines.STUDENT_CATEGORY_SECOND_HIGH));
        builder.add(UniMQExpression.betweenDate("red", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));
        builder.add(MQExpression.notEq("red", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        builder.add(MQExpression.eq("red", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, false));

        builder.addJoin("red", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_PERSON + "." + Person.L_IDENTITY_CARD, "idCard");
        builder.addOrder("idCard", IdentityCardGen.P_LAST_NAME);
        builder.addOrder("idCard", IdentityCardGen.P_FIRST_NAME);
        builder.addOrder("idCard", IdentityCardGen.P_MIDDLE_NAME);
        builder.addOrder("red", RequestedEnrollmentDirection.P_PRIORITY);

        builder.addSelect("red", new String[]{RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + ".id",
                RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL,
                RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_FORM + "." + DevelopForm.P_CODE});
        builder.addSelect("idCard", new String[]{IdentityCardGen.P_LAST_NAME, IdentityCardGen.P_FIRST_NAME, IdentityCardGen.P_MIDDLE_NAME});

        Map<Long, String[]> entrantFIOById = new HashMap<>();
        Map<Long, List<String>> entrantEducationLevelShortTitlesById = new HashMap<>();
        Set<String> eduLevelTitlesSpec = new HashSet<>();   //специальности
        Set<String> eduLevelTitlesDirect = new HashSet<>(); //направления
        String developFormCode = "";
        List<Long> entrantIdsSortedByFio = new ArrayList<>();

        for (Object[] row : builder.<Object[]>getResultList(_session))
        {
            Long entrantId = (Long) row[0];
            EducationLevelsHighSchool eduLevel = (EducationLevelsHighSchool) row[1];
            developFormCode = (String) row[2];
            String lastName = (String) row[3];
            String firstName = (String) row[4];
            String middleName = (String) row[5];

            if (!entrantIdsSortedByFio.contains(entrantId))
                entrantIdsSortedByFio.add(entrantId);

            if (!entrantFIOById.containsKey(entrantId))
                entrantFIOById.put(entrantId, new String[]{lastName, firstName, middleName});

            List<String> entrantEducationLevelShortTitles = SafeMap.safeGet(entrantEducationLevelShortTitlesById, entrantId, key -> new ArrayList<>());
            String eduLevelShortTitle = eduLevel.getShortTitle();
            if (!entrantEducationLevelShortTitles.contains(eduLevelShortTitle))
            {
                entrantEducationLevelShortTitles.add(eduLevelShortTitle);
            }
            if (eduLevel.getEducationLevel().getLevelType().isSpecialityPrintTitleMode())
            {
                eduLevelTitlesSpec.add(eduLevel.getPrintTitle());
            } else
            {
                eduLevelTitlesDirect.add(eduLevel.getPrintTitle());
            }
        }

        StringBuilder educationLevelTitlesStrBuffer = new StringBuilder();
        String educationLevelTitlesStr;
        if (!eduLevelTitlesSpec.isEmpty() || !eduLevelTitlesDirect.isEmpty())
        {
            if (!eduLevelTitlesSpec.isEmpty())
            {
                educationLevelTitlesStrBuffer.append(eduLevelTitlesSpec.size()==1?"по специальности: ":"по специальностям: ");
                List<String> educationLevelTitleList = new ArrayList<>(eduLevelTitlesSpec);
                Collections.sort(educationLevelTitleList);
                for (String educationLevelTitle : educationLevelTitleList)
                {
                    educationLevelTitlesStrBuffer.append(educationLevelTitle).append(", ");
                }
            }
            if (!eduLevelTitlesDirect.isEmpty())
            {
                educationLevelTitlesStrBuffer.append(eduLevelTitlesDirect.size()==1?"по направлению: ":"по направлениям: ");
                List<String> educationLevelTitleList = new ArrayList<>(eduLevelTitlesDirect);
                Collections.sort(educationLevelTitleList);
                for (String educationLevelTitle : educationLevelTitleList)
                {
                    educationLevelTitlesStrBuffer.append(educationLevelTitle).append(", ");
                }
            }
            educationLevelTitlesStr = educationLevelTitlesStrBuffer.substring(0, Math.max(educationLevelTitlesStrBuffer.length() - 2, 1));

            RtfInjectModifier modifier = new RtfInjectModifier();
            modifier.put("e1", educationLevelTitlesStr);
            modifier.put("h1", _developFormTextByCode.get(developFormCode));
            modifier.modify(document);

            String[][] entrantData = new String[entrantFIOById.size()][];
            int i = 1;
            for (Long entrantId : entrantIdsSortedByFio)
            {
                String[] fio = entrantFIOById.get(entrantId);
                List<String> entrantEducationLevelShortTitles = entrantEducationLevelShortTitlesById.get(entrantId);
                StringBuilder entrantEducationLevelShortTitlesStrBuffer = new StringBuilder();
                for (String entrantEducationLevelShortTitle : entrantEducationLevelShortTitles)
                {
                    entrantEducationLevelShortTitlesStrBuffer.append(entrantEducationLevelShortTitle).append(", ");
                }
                String entrantEducationLevelShortTitlesStr = entrantEducationLevelShortTitlesStrBuffer.substring(0, Math.max(entrantEducationLevelShortTitlesStrBuffer.length() - 2, 1));
                entrantData[i - 1] = new String[]{(i++) + "", fio[0], fio[1], fio[2], entrantEducationLevelShortTitlesStr};
            }

            RtfTableModifier entrantModifier = new RtfTableModifier();
            entrantModifier.put("t1", entrantData);
            entrantModifier.modify(document);
        } else
        {
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Arrays.asList("e1", "h1"), false, true);
            UniRtfUtil.removeTableByName(document, "t1", true, true);
        }
    }

    private RtfTable getT2Template(RtfDocument document)
    {
        RtfTable t2Template = null;
        for (IRtfElement element : document.getElementList())
        {
            if (element instanceof RtfTable)
            {
                RtfTable table = (RtfTable) element;
                for (int currentRowIndex = 0; currentRowIndex < table.getRowList().size(); currentRowIndex++)
                {
                    RtfRow currentRow = table.getRowList().get(currentRowIndex);
                    if ("t2".equals(getRowName(currentRow)))
                    {
                        t2Template = table;
                        break;
                    }
                }
            }
        }
        return t2Template;
    }

    @SuppressWarnings("deprecation")
    private void iterateModify(RtfDocument document, MQBuilder builder, RtfTable t2Template, boolean secondHigh)
    {
        Map<CoreCollectionUtils.Pair<String, String>, Set<String[]>> entrantFIOsByEduLevelAndDevelopCond = new HashMap<>();
        Map<CoreCollectionUtils.Pair<String, String>, String> developFormCodeByEduLevelAndDevelopCond = new HashMap<>();
        for (Object[] row : builder.<Object[]>getResultList(_session))
        {
            //Long entrantId = (Long) row[0];
            EducationLevelsHighSchool eduLevel = (EducationLevelsHighSchool) row[1];
            String developConditionTitle = (String) row[2];
            String developFormTitle = (String) row[3];
            String lastName = (String) row[4];
            String firstName = (String) row[5];
            String middleName = (String) row[6];

            String eduLevelPrefix = "по " + eduLevel.getEducationLevel().getLevelType().getDativeCaseSimpleShortTitle() + " ";
            CoreCollectionUtils.Pair<String, String> pair = new CoreCollectionUtils.Pair<>(eduLevelPrefix + eduLevel.getPrintTitle(), developConditionTitle);
            Set<String[]> entrantFIOs = SafeMap.safeGet(entrantFIOsByEduLevelAndDevelopCond, pair, key -> new HashSet<>());
            entrantFIOs.add(new String[]{lastName, firstName, middleName});
            developFormCodeByEduLevelAndDevelopCond.put(pair, developFormTitle);
        }
        List<CoreCollectionUtils.Pair<String, String>> eduLevelAndDevelopCondList = new ArrayList<>(entrantFIOsByEduLevelAndDevelopCond.keySet());
        Collections.sort(eduLevelAndDevelopCondList, (o1, o2) -> {
            if (!o1.getX().equals(o2.getX())) {
                return o1.getX().compareTo(o2.getX());
            } else {
                return o1.getY().compareTo(o2.getY());
            }
        });

        UniRtfUtil.removeTableByName(document, "t2", false, true);

        if (!eduLevelAndDevelopCondList.isEmpty() && t2Template != null)
        {
            RtfRow rowTemplate = t2Template.getRowList().get(0);

            for (CoreCollectionUtils.Pair<String, String> eduLevelAndDevelopCond : eduLevelAndDevelopCondList)
            {
                String educationLevelTitle = eduLevelAndDevelopCond.getX();
                String developConditionTitle = eduLevelAndDevelopCond.getY();
                String developFormText = _developFormTextByCode.get(developFormCodeByEduLevelAndDevelopCond.get(eduLevelAndDevelopCond));

                StringBuilder buffer = new StringBuilder();
                buffer.append("\\par ");
                buffer.append(educationLevelTitle);
                if (secondHigh)
                {
                    buffer.append(", второе высшее образование");
                }
                buffer.append(", с освоением образовательной программы по ")
                        .append(_developConditionTitles.containsKey(developConditionTitle) ? _developConditionTitles.get(developConditionTitle) : "условию освоения \"" + StringUtils.uncapitalize(developConditionTitle) + "\"")
                        .append("\\par\\par \\pard \\qc").append(developFormText).append("\\par\\par");
                IRtfText text = RtfBean.getElementFactory().createRtfText(buffer.toString());
                text.setRaw(true);
                document.getElementList().add(text);

                List<String[]> entrantFIOs = new ArrayList<>(entrantFIOsByEduLevelAndDevelopCond.get(eduLevelAndDevelopCond));
                Collections.sort(entrantFIOs, new EntrantFIOComparator());
                RtfTable t2 = t2Template.getClone();
                List<RtfRow> rowList = new ArrayList<>();
                Integer i = 1;
                for (String[] entranFIO : entrantFIOs)
                {
                    RtfRow row = rowTemplate.getClone();
                    fillCell(row.getCellList().get(0), i.toString());
                    fillCell(row.getCellList().get(1), entranFIO[0]);
                    fillCell(row.getCellList().get(2), entranFIO[1]);
                    fillCell(row.getCellList().get(3), entranFIO[2]);
                    rowList.add(row);
                    i++;
                }
                t2.setRowList(rowList);
                document.getElementList().add(t2);
                IRtfText pard = RtfBean.getElementFactory().createRtfText("\\pard");
                pard.setRaw(true);
                document.getElementList().add(pard);
            }
        }
    }

    private void modifyByNotFullTimeCondition(RtfDocument document, RtfTable t2Template)
    {
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "red");
        builder.add(MQExpression.notEq("red", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_CONDITION + "." + DevelopCondition.P_CODE, UniDefines.DEVELOP_CONDITION_FULL_TIME));
        builder.add(MQExpression.eq("red", RequestedEnrollmentDirection.L_COMPENSATION_TYPE, _model.getReport().getCompensationType()));
        builder.add(MQExpression.eq("red", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_COMPETITION_GROUP, _model.getCompetitionGroup()));
        builder.add(MQExpression.notEq("red", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        builder.add(MQExpression.notEq("red", RequestedEnrollmentDirection.L_STUDENT_CATEGORY + "." + StudentCategory.P_CODE, UniDefines.STUDENT_CATEGORY_SECOND_HIGH));
        builder.add(UniMQExpression.betweenDate("red", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));

        builder.addJoin("red", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_PERSON + "." + Person.L_IDENTITY_CARD, "idCard");
        builder.addOrder("idCard", IdentityCardGen.P_LAST_NAME);
        builder.addOrder("idCard", IdentityCardGen.P_FIRST_NAME);
        builder.addOrder("idCard", IdentityCardGen.P_MIDDLE_NAME);

        builder.addSelect("red", new String[]{RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + ".id",
                RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL,
                RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_CONDITION + "." + DevelopCondition.P_TITLE,
                RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_FORM + "." + DevelopForm.P_CODE
        });
        builder.addSelect("idCard", new String[]{IdentityCardGen.P_LAST_NAME, IdentityCardGen.P_FIRST_NAME, IdentityCardGen.P_MIDDLE_NAME});

        iterateModify(document, builder, t2Template, false);
    }

    private void modifyBySecondHigh(RtfDocument document, RtfTable t2Template)
    {
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "red");
        builder.add(MQExpression.eq("red", RequestedEnrollmentDirection.L_COMPENSATION_TYPE, _model.getReport().getCompensationType()));
        builder.add(MQExpression.eq("red", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_COMPETITION_GROUP, _model.getCompetitionGroup()));
        builder.add(MQExpression.eq("red", RequestedEnrollmentDirection.L_STUDENT_CATEGORY + "." + StudentCategory.P_CODE, UniDefines.STUDENT_CATEGORY_SECOND_HIGH));
        builder.add(MQExpression.notEq("red", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        builder.add(UniMQExpression.betweenDate("red", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));

        builder.addJoin("red", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_PERSON + "." + Person.L_IDENTITY_CARD, "idCard");
        builder.addOrder("idCard", IdentityCardGen.P_LAST_NAME);
        builder.addOrder("idCard", IdentityCardGen.P_FIRST_NAME);
        builder.addOrder("idCard", IdentityCardGen.P_MIDDLE_NAME);

        builder.addSelect("red", new String[]{RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + ".id",
                RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL,
                RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_CONDITION + "." + DevelopCondition.P_TITLE,
                RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_FORM + "." + DevelopForm.P_CODE
        });
        builder.addSelect("idCard", new String[]{IdentityCardGen.P_LAST_NAME, IdentityCardGen.P_FIRST_NAME, IdentityCardGen.P_MIDDLE_NAME});

        iterateModify(document, builder, t2Template, true);
    }

    private class EntrantFIOComparator implements Comparator<String[]>
    {
        @Override
        public int compare(String[] o1, String[] o2)
        {
            if (!o1[0].equals(o2[0]))
            {
                return o1[0].compareTo(o2[0]);
            } else if (!o1[1].equals(o2[1]))
            {
                return o1[1].compareTo(o2[1]);
            } else if (o1[2] == null && o2[2] != null)
            {
                return -1;
            } else if (o1[2] != null && o2[2] == null)
            {
                return 1;
            } else
            {
                return o1[2].compareTo(o2[2]);
            }
        }
    }

    private static String getRowName(RtfRow row)
    {
        for (IRtfElement element : row.getCellList().get(0).getElementList())
            if (element instanceof RtfField)
            {
                RtfField field = (RtfField) element;
                if (field.isMark())
                    return field.getFieldName();
            }
        return null;
    }

    private static void fillCell(RtfCell cell, String value)
    {
        IRtfGroup groupValue = RtfBean.getElementFactory().createRtfGroup();
        groupValue.getElementList().add(RtfBean.getElementFactory().createRtfText(value));

        List<IRtfElement> elementList = cell.getElementList();
        int i = 0;
        while (i < elementList.size() && !(elementList.get(i) instanceof RtfField && ((RtfField) elementList.get(i)).isMark()))
            i++;

        if (i < elementList.size())
        {
            groupValue.getElementList().addAll(0, ((RtfField) elementList.get(i)).getFormatList());
            elementList.set(i, groupValue);
        } else
        {
            elementList.add(groupValue);
        }
    }
}
