package ru.tandemservice.uniec.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Вид целевого приема"
 * Имя сущности : targetAdmissionKind
 * Файл data.xml : uniec.catalogs.data.xml
 */
public interface TargetAdmissionKindCodes
{
    /** Константа кода (code) элемента : Целевой прием (title) */
    String TA_DEFAULT = "1";

    Set<String> CODES = ImmutableSet.of(TA_DEFAULT);
}
