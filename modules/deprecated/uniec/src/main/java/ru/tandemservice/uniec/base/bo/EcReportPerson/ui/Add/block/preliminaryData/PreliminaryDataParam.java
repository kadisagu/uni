package ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.block.preliminaryData;

import org.tandemframework.core.entity.IIdentifiableWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.EcReportPersonAddUI;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

import java.util.List;

/**
 * @author Vasily Zhukov
 * @since 08.02.2011
 */
public class PreliminaryDataParam implements IReportDQLModifier
{
    private IReportParam<List<Qualifications>> _qualificationPre = new ReportParam<List<Qualifications>>();
    private IReportParam<List<OrgUnit>> _formativeOrgUnitPre = new ReportParam<List<OrgUnit>>();
    private IReportParam<List<OrgUnit>> _territorialOrgUnitPre = new ReportParam<List<OrgUnit>>();
    private IReportParam<List<EducationLevelsHighSchool>> _educationOrgUnitPre = new ReportParam<List<EducationLevelsHighSchool>>();
    private IReportParam<List<DevelopForm>> _developFormPre = new ReportParam<List<DevelopForm>>();
    private IReportParam<List<DevelopCondition>> _developConditionPre = new ReportParam<List<DevelopCondition>>();
    private IReportParam<List<DevelopTech>> _developTechPre = new ReportParam<List<DevelopTech>>();
    private IReportParam<List<DevelopPeriod>> _developPeriodPre = new ReportParam<List<DevelopPeriod>>();
    private IReportParam<CompensationType> _compensationTypePre = new ReportParam<CompensationType>();
    private IReportParam<List<StudentCategory>> _studentCategoryPre = new ReportParam<List<StudentCategory>>();
    private IReportParam<IIdentifiableWrapper> _targetAdmissionPre = new ReportParam<IIdentifiableWrapper>();
    private IReportParam<IIdentifiableWrapper> _parallelPre = new ReportParam<IIdentifiableWrapper>();

    private String alias(ReportDQL dql)
    {
        // соединяем абитуриента
        String entrantAlias = dql.innerJoinEntity(Entrant.class, Entrant.person());

        // соединяем заявление
        String requestAlias = dql.innerJoinEntity(entrantAlias, EntrantRequest.class, EntrantRequest.entrant());

        // соединяем выбранное направление приема
        String directionAlias = dql.innerJoinEntity(requestAlias, RequestedEnrollmentDirection.class, RequestedEnrollmentDirection.entrantRequest());

        // соединяем студента предзачисления
        String preStudentAlias = dql.innerJoinEntity(directionAlias, PreliminaryEnrollmentStudent.class, PreliminaryEnrollmentStudent.requestedEnrollmentDirection());

        // добавляем сортировки
        dql.order(entrantAlias, Entrant.enrollmentCampaign().id());
        dql.order(requestAlias, EntrantRequest.regDate());
        dql.order(requestAlias, EntrantRequest.id());
        dql.order(directionAlias, RequestedEnrollmentDirection.priority());
        dql.order(preStudentAlias, PreliminaryEnrollmentStudent.id());

        return preStudentAlias;
    }

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {

    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (_qualificationPre.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "preliminaryData.qualificationPre", CommonBaseStringUtil.join(_qualificationPre.getData(), Qualifications.P_TITLE, ", "));

            dql.builder.where(DQLExpressions.in(DQLExpressions.property(PreliminaryEnrollmentStudent.educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().fromAlias(alias(dql))), _qualificationPre.getData()));
        }

        if (_formativeOrgUnitPre.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "preliminaryData.formativeOrgUnitPre", CommonBaseStringUtil.join(_formativeOrgUnitPre.getData(), OrgUnit.P_TITLE, ", "));

            dql.builder.where(DQLExpressions.in(DQLExpressions.property(PreliminaryEnrollmentStudent.educationOrgUnit().formativeOrgUnit().fromAlias(alias(dql))), _formativeOrgUnitPre.getData()));
        }

        if (_territorialOrgUnitPre.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "preliminaryData.territorialOrgUnitPre", CommonBaseStringUtil.join(_territorialOrgUnitPre.getData(), OrgUnit.P_TERRITORIAL_TITLE, ", "));
            
            dql.builder.where(DQLExpressions.in(DQLExpressions.property(PreliminaryEnrollmentStudent.educationOrgUnit().territorialOrgUnit().fromAlias(alias(dql))), _territorialOrgUnitPre.getData()));
        }

        if (_educationOrgUnitPre.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "preliminaryData.educationOrgUnitPre", CommonBaseStringUtil.join(_educationOrgUnitPre.getData(), EducationLevelsHighSchool.P_PRINT_TITLE, ", "));
            
            dql.builder.where(DQLExpressions.in(DQLExpressions.property(PreliminaryEnrollmentStudent.educationOrgUnit().educationLevelHighSchool().fromAlias(alias(dql))), _educationOrgUnitPre.getData()));
        }

        if (_developFormPre.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "preliminaryData.developFormPre", CommonBaseStringUtil.join(_developFormPre.getData(), DevelopForm.P_TITLE, ", "));
            
            dql.builder.where(DQLExpressions.in(DQLExpressions.property(PreliminaryEnrollmentStudent.educationOrgUnit().developForm().fromAlias(alias(dql))), _developFormPre.getData()));
        }

        if (_developConditionPre.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "preliminaryData.developConditionPre", CommonBaseStringUtil.join(_developConditionPre.getData(), DevelopCondition.P_TITLE, ", "));
            
            dql.builder.where(DQLExpressions.in(DQLExpressions.property(PreliminaryEnrollmentStudent.educationOrgUnit().developCondition().fromAlias(alias(dql))), _developConditionPre.getData()));
        }

        if (_developTechPre.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "preliminaryData.developTechPre", CommonBaseStringUtil.join(_developTechPre.getData(), DevelopTech.P_TITLE, ", "));
            
            dql.builder.where(DQLExpressions.in(DQLExpressions.property(PreliminaryEnrollmentStudent.educationOrgUnit().developTech().fromAlias(alias(dql))), _developTechPre.getData()));
        }

        if (_developPeriodPre.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "preliminaryData.developPeriodPre", CommonBaseStringUtil.join(_developPeriodPre.getData(), DevelopPeriod.P_TITLE, ", "));
            
            dql.builder.where(DQLExpressions.in(DQLExpressions.property(PreliminaryEnrollmentStudent.educationOrgUnit().developPeriod().fromAlias(alias(dql))), _developPeriodPre.getData()));
        }

        if (_compensationTypePre.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "preliminaryData.compensationTypePre", _compensationTypePre.getData().getShortTitle());
            
            dql.builder.where(DQLExpressions.eq(DQLExpressions.property(PreliminaryEnrollmentStudent.compensationType().fromAlias(alias(dql))), DQLExpressions.value(_compensationTypePre.getData())));
        }

        if (_studentCategoryPre.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "preliminaryData.studentCategoryPre", CommonBaseStringUtil.join(_studentCategoryPre.getData(), StudentCategory.P_TITLE, ", "));
            
            dql.builder.where(DQLExpressions.in(DQLExpressions.property(PreliminaryEnrollmentStudent.studentCategory().fromAlias(alias(dql))), _studentCategoryPre.getData()));
        }

        if (_targetAdmissionPre.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "preliminaryData.targetAdmissionPre", _targetAdmissionPre.getData().getTitle());
            
            dql.builder.where(DQLExpressions.eq(DQLExpressions.property(PreliminaryEnrollmentStudent.targetAdmission().fromAlias(alias(dql))), DQLExpressions.value(_targetAdmissionPre.getData().getId().equals(PreliminaryDataBlock.TARGET_ADMISSION_YES))));
        }

        if (_parallelPre.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "preliminaryData.parallelPre", _parallelPre.getData().getTitle());
            
            dql.builder.where(DQLExpressions.eq(DQLExpressions.property(PreliminaryEnrollmentStudent.parallel().fromAlias(alias(dql))), DQLExpressions.value(_parallelPre.getData().getId().equals(PreliminaryDataBlock.PARALLEL_YES))));
        }
    }

    // Getters

    public IReportParam<List<Qualifications>> getQualificationPre()
    {
        return _qualificationPre;
    }

    public IReportParam<List<OrgUnit>> getFormativeOrgUnitPre()
    {
        return _formativeOrgUnitPre;
    }

    public IReportParam<List<OrgUnit>> getTerritorialOrgUnitPre()
    {
        return _territorialOrgUnitPre;
    }

    public IReportParam<List<EducationLevelsHighSchool>> getEducationOrgUnitPre()
    {
        return _educationOrgUnitPre;
    }

    public IReportParam<List<DevelopForm>> getDevelopFormPre()
    {
        return _developFormPre;
    }

    public IReportParam<List<DevelopCondition>> getDevelopConditionPre()
    {
        return _developConditionPre;
    }

    public IReportParam<List<DevelopTech>> getDevelopTechPre()
    {
        return _developTechPre;
    }

    public IReportParam<List<DevelopPeriod>> getDevelopPeriodPre()
    {
        return _developPeriodPre;
    }

    public IReportParam<CompensationType> getCompensationTypePre()
    {
        return _compensationTypePre;
    }

    public IReportParam<List<StudentCategory>> getStudentCategoryPre()
    {
        return _studentCategoryPre;
    }

    public IReportParam<IIdentifiableWrapper> getTargetAdmissionPre()
    {
        return _targetAdmissionPre;
    }

    public IReportParam<IIdentifiableWrapper> getParallelPre()
    {
        return _parallelPre;
    }
}
