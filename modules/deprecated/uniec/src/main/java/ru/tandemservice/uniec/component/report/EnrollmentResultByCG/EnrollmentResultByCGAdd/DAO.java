/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EnrollmentResultByCG.EnrollmentResultByCGAdd;

import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.report.EnrollmentResultByCGReport;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.Date;
import java.util.List;

/**
 * @author ekachanova
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    @SuppressWarnings("deprecation")
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.getReport().setDateFrom(model.getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : model.getEnrollmentCampaign().getStartDate());
        model.getReport().setDateTo(CoreDateUtils.getDayLastTimeMoment(new Date()));
    }

    @Override
    @SuppressWarnings("unchecked")
    public void validate(Model model, ErrorCollector errors)
    {
        List<Number> developFormCountList = getSession().createQuery("select count(distinct d." + EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_FORM + ".id) from " + EnrollmentDirection.ENTITY_CLASS + " d" +
                " join d." + EnrollmentDirection.L_COMPETITION_GROUP + " cg where d." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN + ".id=:enrollmentCampaignId " +
                " group by cg").setLong("enrollmentCampaignId", model.getReport().getEnrollmentCampaign().getId()).list();
        for (Number num : developFormCountList)
        {
            if(num.intValue() > 1)
            {
                errors.add("Невозможно сформировать отчет, так как существуют группы, в которые включены направления/специальности с разными формами освоения.");
                break;
            }
        }
    }

    @Override
    public void update(Model model)
    {
        final Session session = getSession();

        EnrollmentResultByCGReport report = model.getReport();
        report.setFormingDate(new Date());

        DatabaseFile databaseFile = new EnrollmentResultByCGReportBuilder(session, model).getContent();
        session.save(databaseFile);

        report.setContent(databaseFile);
        session.save(report);
    }
}
