package ru.tandemservice.uniec.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Протокол заседания приемной комиссии (по видам конкурса)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntranceExamMeetingByCKReport2010Gen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010";
    public static final String ENTITY_NAME = "entranceExamMeetingByCKReport2010";
    public static final int VERSION_HASH = -1131513210;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONTENT = "content";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String P_PRINT_FORM = "printForm";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_DATE_FROM = "dateFrom";
    public static final String P_DATE_TO = "dateTo";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String P_STUDENT_CATEGORY_TITLE = "studentCategoryTitle";
    public static final String P_ONLY_WITH_ORIGINALS = "onlyWithOriginals";
    public static final String P_ONLY_COMPETITIVE_ADMISSION = "onlyCompetitiveAdmission";
    public static final String P_QUALIFICATION_TITLE = "qualificationTitle";
    public static final String P_FORMATIVE_ORG_UNIT_TITLE = "formativeOrgUnitTitle";
    public static final String P_TERRITORIAL_ORG_UNIT_TITLE = "territorialOrgUnitTitle";
    public static final String P_EDUCATION_LEVEL_HIGH_SCHOOL_TITLE = "educationLevelHighSchoolTitle";
    public static final String P_DEVELOP_FORM_TITLE = "developFormTitle";
    public static final String P_DEVELOP_CONDITION_TITLE = "developConditionTitle";
    public static final String P_DEVELOP_TECH_TITLE = "developTechTitle";
    public static final String P_DEVELOP_PERIOD_TITLE = "developPeriodTitle";
    public static final String P_NOT_INCLUDE_ENROLLMENT = "notIncludeEnrollment";
    public static final String P_ENTRANT_CUSTOM_STATE_TITLE = "entrantCustomStateTitle";

    private DatabaseFile _content;     // Печатная форма
    private Date _formingDate;     // Дата формирования
    private String _printForm;     // Печатная форма
    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private Date _dateFrom;     // Заявления с
    private Date _dateTo;     // Заявления по
    private CompensationType _compensationType;     // Вид возмещения затрат
    private String _studentCategoryTitle;     // Категория поступающего
    private boolean _onlyWithOriginals;     // Не включать абитуриентов без оригиналов документов
    private boolean _onlyCompetitiveAdmission;     // Выводить только абитуриентов, поступающих на общих основаниях
    private String _qualificationTitle;     // Квалификация
    private String _formativeOrgUnitTitle;     // Формирующее подр.
    private String _territorialOrgUnitTitle;     // Территориальное подр.
    private String _educationLevelHighSchoolTitle;     // Уровень образования ОУ подразделение
    private String _developFormTitle;     // Форма освоения
    private String _developConditionTitle;     // Условие освоения
    private String _developTechTitle;     // Технология освоения
    private String _developPeriodTitle;     // Срок освоения
    private boolean _notIncludeEnrollment;     // Без зачисленных
    private String _entrantCustomStateTitle;     // Дополнительные статусы абитуриентов

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPrintForm()
    {
        return _printForm;
    }

    /**
     * @param printForm Печатная форма. Свойство не может быть null.
     */
    public void setPrintForm(String printForm)
    {
        dirty(_printForm, printForm);
        _printForm = printForm;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     */
    @NotNull
    public Date getDateFrom()
    {
        return _dateFrom;
    }

    /**
     * @param dateFrom Заявления с. Свойство не может быть null.
     */
    public void setDateFrom(Date dateFrom)
    {
        dirty(_dateFrom, dateFrom);
        _dateFrom = dateFrom;
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     */
    @NotNull
    public Date getDateTo()
    {
        return _dateTo;
    }

    /**
     * @param dateTo Заявления по. Свойство не может быть null.
     */
    public void setDateTo(Date dateTo)
    {
        dirty(_dateTo, dateTo);
        _dateTo = dateTo;
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Категория поступающего.
     */
    @Length(max=255)
    public String getStudentCategoryTitle()
    {
        return _studentCategoryTitle;
    }

    /**
     * @param studentCategoryTitle Категория поступающего.
     */
    public void setStudentCategoryTitle(String studentCategoryTitle)
    {
        dirty(_studentCategoryTitle, studentCategoryTitle);
        _studentCategoryTitle = studentCategoryTitle;
    }

    /**
     * @return Не включать абитуриентов без оригиналов документов. Свойство не может быть null.
     */
    @NotNull
    public boolean isOnlyWithOriginals()
    {
        return _onlyWithOriginals;
    }

    /**
     * @param onlyWithOriginals Не включать абитуриентов без оригиналов документов. Свойство не может быть null.
     */
    public void setOnlyWithOriginals(boolean onlyWithOriginals)
    {
        dirty(_onlyWithOriginals, onlyWithOriginals);
        _onlyWithOriginals = onlyWithOriginals;
    }

    /**
     * @return Выводить только абитуриентов, поступающих на общих основаниях. Свойство не может быть null.
     */
    @NotNull
    public boolean isOnlyCompetitiveAdmission()
    {
        return _onlyCompetitiveAdmission;
    }

    /**
     * @param onlyCompetitiveAdmission Выводить только абитуриентов, поступающих на общих основаниях. Свойство не может быть null.
     */
    public void setOnlyCompetitiveAdmission(boolean onlyCompetitiveAdmission)
    {
        dirty(_onlyCompetitiveAdmission, onlyCompetitiveAdmission);
        _onlyCompetitiveAdmission = onlyCompetitiveAdmission;
    }

    /**
     * @return Квалификация.
     */
    @Length(max=255)
    public String getQualificationTitle()
    {
        return _qualificationTitle;
    }

    /**
     * @param qualificationTitle Квалификация.
     */
    public void setQualificationTitle(String qualificationTitle)
    {
        dirty(_qualificationTitle, qualificationTitle);
        _qualificationTitle = qualificationTitle;
    }

    /**
     * @return Формирующее подр..
     */
    public String getFormativeOrgUnitTitle()
    {
        return _formativeOrgUnitTitle;
    }

    /**
     * @param formativeOrgUnitTitle Формирующее подр..
     */
    public void setFormativeOrgUnitTitle(String formativeOrgUnitTitle)
    {
        dirty(_formativeOrgUnitTitle, formativeOrgUnitTitle);
        _formativeOrgUnitTitle = formativeOrgUnitTitle;
    }

    /**
     * @return Территориальное подр..
     */
    public String getTerritorialOrgUnitTitle()
    {
        return _territorialOrgUnitTitle;
    }

    /**
     * @param territorialOrgUnitTitle Территориальное подр..
     */
    public void setTerritorialOrgUnitTitle(String territorialOrgUnitTitle)
    {
        dirty(_territorialOrgUnitTitle, territorialOrgUnitTitle);
        _territorialOrgUnitTitle = territorialOrgUnitTitle;
    }

    /**
     * @return Уровень образования ОУ подразделение.
     */
    public String getEducationLevelHighSchoolTitle()
    {
        return _educationLevelHighSchoolTitle;
    }

    /**
     * @param educationLevelHighSchoolTitle Уровень образования ОУ подразделение.
     */
    public void setEducationLevelHighSchoolTitle(String educationLevelHighSchoolTitle)
    {
        dirty(_educationLevelHighSchoolTitle, educationLevelHighSchoolTitle);
        _educationLevelHighSchoolTitle = educationLevelHighSchoolTitle;
    }

    /**
     * @return Форма освоения.
     */
    @Length(max=255)
    public String getDevelopFormTitle()
    {
        return _developFormTitle;
    }

    /**
     * @param developFormTitle Форма освоения.
     */
    public void setDevelopFormTitle(String developFormTitle)
    {
        dirty(_developFormTitle, developFormTitle);
        _developFormTitle = developFormTitle;
    }

    /**
     * @return Условие освоения.
     */
    @Length(max=255)
    public String getDevelopConditionTitle()
    {
        return _developConditionTitle;
    }

    /**
     * @param developConditionTitle Условие освоения.
     */
    public void setDevelopConditionTitle(String developConditionTitle)
    {
        dirty(_developConditionTitle, developConditionTitle);
        _developConditionTitle = developConditionTitle;
    }

    /**
     * @return Технология освоения.
     */
    @Length(max=255)
    public String getDevelopTechTitle()
    {
        return _developTechTitle;
    }

    /**
     * @param developTechTitle Технология освоения.
     */
    public void setDevelopTechTitle(String developTechTitle)
    {
        dirty(_developTechTitle, developTechTitle);
        _developTechTitle = developTechTitle;
    }

    /**
     * @return Срок освоения.
     */
    @Length(max=255)
    public String getDevelopPeriodTitle()
    {
        return _developPeriodTitle;
    }

    /**
     * @param developPeriodTitle Срок освоения.
     */
    public void setDevelopPeriodTitle(String developPeriodTitle)
    {
        dirty(_developPeriodTitle, developPeriodTitle);
        _developPeriodTitle = developPeriodTitle;
    }

    /**
     * @return Без зачисленных. Свойство не может быть null.
     */
    @NotNull
    public boolean isNotIncludeEnrollment()
    {
        return _notIncludeEnrollment;
    }

    /**
     * @param notIncludeEnrollment Без зачисленных. Свойство не может быть null.
     */
    public void setNotIncludeEnrollment(boolean notIncludeEnrollment)
    {
        dirty(_notIncludeEnrollment, notIncludeEnrollment);
        _notIncludeEnrollment = notIncludeEnrollment;
    }

    /**
     * @return Дополнительные статусы абитуриентов.
     */
    @Length(max=255)
    public String getEntrantCustomStateTitle()
    {
        return _entrantCustomStateTitle;
    }

    /**
     * @param entrantCustomStateTitle Дополнительные статусы абитуриентов.
     */
    public void setEntrantCustomStateTitle(String entrantCustomStateTitle)
    {
        dirty(_entrantCustomStateTitle, entrantCustomStateTitle);
        _entrantCustomStateTitle = entrantCustomStateTitle;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntranceExamMeetingByCKReport2010Gen)
        {
            setContent(((EntranceExamMeetingByCKReport2010)another).getContent());
            setFormingDate(((EntranceExamMeetingByCKReport2010)another).getFormingDate());
            setPrintForm(((EntranceExamMeetingByCKReport2010)another).getPrintForm());
            setEnrollmentCampaign(((EntranceExamMeetingByCKReport2010)another).getEnrollmentCampaign());
            setDateFrom(((EntranceExamMeetingByCKReport2010)another).getDateFrom());
            setDateTo(((EntranceExamMeetingByCKReport2010)another).getDateTo());
            setCompensationType(((EntranceExamMeetingByCKReport2010)another).getCompensationType());
            setStudentCategoryTitle(((EntranceExamMeetingByCKReport2010)another).getStudentCategoryTitle());
            setOnlyWithOriginals(((EntranceExamMeetingByCKReport2010)another).isOnlyWithOriginals());
            setOnlyCompetitiveAdmission(((EntranceExamMeetingByCKReport2010)another).isOnlyCompetitiveAdmission());
            setQualificationTitle(((EntranceExamMeetingByCKReport2010)another).getQualificationTitle());
            setFormativeOrgUnitTitle(((EntranceExamMeetingByCKReport2010)another).getFormativeOrgUnitTitle());
            setTerritorialOrgUnitTitle(((EntranceExamMeetingByCKReport2010)another).getTerritorialOrgUnitTitle());
            setEducationLevelHighSchoolTitle(((EntranceExamMeetingByCKReport2010)another).getEducationLevelHighSchoolTitle());
            setDevelopFormTitle(((EntranceExamMeetingByCKReport2010)another).getDevelopFormTitle());
            setDevelopConditionTitle(((EntranceExamMeetingByCKReport2010)another).getDevelopConditionTitle());
            setDevelopTechTitle(((EntranceExamMeetingByCKReport2010)another).getDevelopTechTitle());
            setDevelopPeriodTitle(((EntranceExamMeetingByCKReport2010)another).getDevelopPeriodTitle());
            setNotIncludeEnrollment(((EntranceExamMeetingByCKReport2010)another).isNotIncludeEnrollment());
            setEntrantCustomStateTitle(((EntranceExamMeetingByCKReport2010)another).getEntrantCustomStateTitle());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntranceExamMeetingByCKReport2010Gen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntranceExamMeetingByCKReport2010.class;
        }

        public T newInstance()
        {
            return (T) new EntranceExamMeetingByCKReport2010();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "content":
                    return obj.getContent();
                case "formingDate":
                    return obj.getFormingDate();
                case "printForm":
                    return obj.getPrintForm();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "dateFrom":
                    return obj.getDateFrom();
                case "dateTo":
                    return obj.getDateTo();
                case "compensationType":
                    return obj.getCompensationType();
                case "studentCategoryTitle":
                    return obj.getStudentCategoryTitle();
                case "onlyWithOriginals":
                    return obj.isOnlyWithOriginals();
                case "onlyCompetitiveAdmission":
                    return obj.isOnlyCompetitiveAdmission();
                case "qualificationTitle":
                    return obj.getQualificationTitle();
                case "formativeOrgUnitTitle":
                    return obj.getFormativeOrgUnitTitle();
                case "territorialOrgUnitTitle":
                    return obj.getTerritorialOrgUnitTitle();
                case "educationLevelHighSchoolTitle":
                    return obj.getEducationLevelHighSchoolTitle();
                case "developFormTitle":
                    return obj.getDevelopFormTitle();
                case "developConditionTitle":
                    return obj.getDevelopConditionTitle();
                case "developTechTitle":
                    return obj.getDevelopTechTitle();
                case "developPeriodTitle":
                    return obj.getDevelopPeriodTitle();
                case "notIncludeEnrollment":
                    return obj.isNotIncludeEnrollment();
                case "entrantCustomStateTitle":
                    return obj.getEntrantCustomStateTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "printForm":
                    obj.setPrintForm((String) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "dateFrom":
                    obj.setDateFrom((Date) value);
                    return;
                case "dateTo":
                    obj.setDateTo((Date) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "studentCategoryTitle":
                    obj.setStudentCategoryTitle((String) value);
                    return;
                case "onlyWithOriginals":
                    obj.setOnlyWithOriginals((Boolean) value);
                    return;
                case "onlyCompetitiveAdmission":
                    obj.setOnlyCompetitiveAdmission((Boolean) value);
                    return;
                case "qualificationTitle":
                    obj.setQualificationTitle((String) value);
                    return;
                case "formativeOrgUnitTitle":
                    obj.setFormativeOrgUnitTitle((String) value);
                    return;
                case "territorialOrgUnitTitle":
                    obj.setTerritorialOrgUnitTitle((String) value);
                    return;
                case "educationLevelHighSchoolTitle":
                    obj.setEducationLevelHighSchoolTitle((String) value);
                    return;
                case "developFormTitle":
                    obj.setDevelopFormTitle((String) value);
                    return;
                case "developConditionTitle":
                    obj.setDevelopConditionTitle((String) value);
                    return;
                case "developTechTitle":
                    obj.setDevelopTechTitle((String) value);
                    return;
                case "developPeriodTitle":
                    obj.setDevelopPeriodTitle((String) value);
                    return;
                case "notIncludeEnrollment":
                    obj.setNotIncludeEnrollment((Boolean) value);
                    return;
                case "entrantCustomStateTitle":
                    obj.setEntrantCustomStateTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "content":
                        return true;
                case "formingDate":
                        return true;
                case "printForm":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "dateFrom":
                        return true;
                case "dateTo":
                        return true;
                case "compensationType":
                        return true;
                case "studentCategoryTitle":
                        return true;
                case "onlyWithOriginals":
                        return true;
                case "onlyCompetitiveAdmission":
                        return true;
                case "qualificationTitle":
                        return true;
                case "formativeOrgUnitTitle":
                        return true;
                case "territorialOrgUnitTitle":
                        return true;
                case "educationLevelHighSchoolTitle":
                        return true;
                case "developFormTitle":
                        return true;
                case "developConditionTitle":
                        return true;
                case "developTechTitle":
                        return true;
                case "developPeriodTitle":
                        return true;
                case "notIncludeEnrollment":
                        return true;
                case "entrantCustomStateTitle":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "content":
                    return true;
                case "formingDate":
                    return true;
                case "printForm":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "dateFrom":
                    return true;
                case "dateTo":
                    return true;
                case "compensationType":
                    return true;
                case "studentCategoryTitle":
                    return true;
                case "onlyWithOriginals":
                    return true;
                case "onlyCompetitiveAdmission":
                    return true;
                case "qualificationTitle":
                    return true;
                case "formativeOrgUnitTitle":
                    return true;
                case "territorialOrgUnitTitle":
                    return true;
                case "educationLevelHighSchoolTitle":
                    return true;
                case "developFormTitle":
                    return true;
                case "developConditionTitle":
                    return true;
                case "developTechTitle":
                    return true;
                case "developPeriodTitle":
                    return true;
                case "notIncludeEnrollment":
                    return true;
                case "entrantCustomStateTitle":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "content":
                    return DatabaseFile.class;
                case "formingDate":
                    return Date.class;
                case "printForm":
                    return String.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "dateFrom":
                    return Date.class;
                case "dateTo":
                    return Date.class;
                case "compensationType":
                    return CompensationType.class;
                case "studentCategoryTitle":
                    return String.class;
                case "onlyWithOriginals":
                    return Boolean.class;
                case "onlyCompetitiveAdmission":
                    return Boolean.class;
                case "qualificationTitle":
                    return String.class;
                case "formativeOrgUnitTitle":
                    return String.class;
                case "territorialOrgUnitTitle":
                    return String.class;
                case "educationLevelHighSchoolTitle":
                    return String.class;
                case "developFormTitle":
                    return String.class;
                case "developConditionTitle":
                    return String.class;
                case "developTechTitle":
                    return String.class;
                case "developPeriodTitle":
                    return String.class;
                case "notIncludeEnrollment":
                    return Boolean.class;
                case "entrantCustomStateTitle":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntranceExamMeetingByCKReport2010> _dslPath = new Path<EntranceExamMeetingByCKReport2010>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntranceExamMeetingByCKReport2010");
    }
            

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#getPrintForm()
     */
    public static PropertyPath<String> printForm()
    {
        return _dslPath.printForm();
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#getDateFrom()
     */
    public static PropertyPath<Date> dateFrom()
    {
        return _dslPath.dateFrom();
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#getDateTo()
     */
    public static PropertyPath<Date> dateTo()
    {
        return _dslPath.dateTo();
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#getStudentCategoryTitle()
     */
    public static PropertyPath<String> studentCategoryTitle()
    {
        return _dslPath.studentCategoryTitle();
    }

    /**
     * @return Не включать абитуриентов без оригиналов документов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#isOnlyWithOriginals()
     */
    public static PropertyPath<Boolean> onlyWithOriginals()
    {
        return _dslPath.onlyWithOriginals();
    }

    /**
     * @return Выводить только абитуриентов, поступающих на общих основаниях. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#isOnlyCompetitiveAdmission()
     */
    public static PropertyPath<Boolean> onlyCompetitiveAdmission()
    {
        return _dslPath.onlyCompetitiveAdmission();
    }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#getQualificationTitle()
     */
    public static PropertyPath<String> qualificationTitle()
    {
        return _dslPath.qualificationTitle();
    }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#getFormativeOrgUnitTitle()
     */
    public static PropertyPath<String> formativeOrgUnitTitle()
    {
        return _dslPath.formativeOrgUnitTitle();
    }

    /**
     * @return Территориальное подр..
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#getTerritorialOrgUnitTitle()
     */
    public static PropertyPath<String> territorialOrgUnitTitle()
    {
        return _dslPath.territorialOrgUnitTitle();
    }

    /**
     * @return Уровень образования ОУ подразделение.
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#getEducationLevelHighSchoolTitle()
     */
    public static PropertyPath<String> educationLevelHighSchoolTitle()
    {
        return _dslPath.educationLevelHighSchoolTitle();
    }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#getDevelopFormTitle()
     */
    public static PropertyPath<String> developFormTitle()
    {
        return _dslPath.developFormTitle();
    }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#getDevelopConditionTitle()
     */
    public static PropertyPath<String> developConditionTitle()
    {
        return _dslPath.developConditionTitle();
    }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#getDevelopTechTitle()
     */
    public static PropertyPath<String> developTechTitle()
    {
        return _dslPath.developTechTitle();
    }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#getDevelopPeriodTitle()
     */
    public static PropertyPath<String> developPeriodTitle()
    {
        return _dslPath.developPeriodTitle();
    }

    /**
     * @return Без зачисленных. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#isNotIncludeEnrollment()
     */
    public static PropertyPath<Boolean> notIncludeEnrollment()
    {
        return _dslPath.notIncludeEnrollment();
    }

    /**
     * @return Дополнительные статусы абитуриентов.
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#getEntrantCustomStateTitle()
     */
    public static PropertyPath<String> entrantCustomStateTitle()
    {
        return _dslPath.entrantCustomStateTitle();
    }

    public static class Path<E extends EntranceExamMeetingByCKReport2010> extends EntityPath<E>
    {
        private DatabaseFile.Path<DatabaseFile> _content;
        private PropertyPath<Date> _formingDate;
        private PropertyPath<String> _printForm;
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<Date> _dateFrom;
        private PropertyPath<Date> _dateTo;
        private CompensationType.Path<CompensationType> _compensationType;
        private PropertyPath<String> _studentCategoryTitle;
        private PropertyPath<Boolean> _onlyWithOriginals;
        private PropertyPath<Boolean> _onlyCompetitiveAdmission;
        private PropertyPath<String> _qualificationTitle;
        private PropertyPath<String> _formativeOrgUnitTitle;
        private PropertyPath<String> _territorialOrgUnitTitle;
        private PropertyPath<String> _educationLevelHighSchoolTitle;
        private PropertyPath<String> _developFormTitle;
        private PropertyPath<String> _developConditionTitle;
        private PropertyPath<String> _developTechTitle;
        private PropertyPath<String> _developPeriodTitle;
        private PropertyPath<Boolean> _notIncludeEnrollment;
        private PropertyPath<String> _entrantCustomStateTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(EntranceExamMeetingByCKReport2010Gen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#getPrintForm()
     */
        public PropertyPath<String> printForm()
        {
            if(_printForm == null )
                _printForm = new PropertyPath<String>(EntranceExamMeetingByCKReport2010Gen.P_PRINT_FORM, this);
            return _printForm;
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#getDateFrom()
     */
        public PropertyPath<Date> dateFrom()
        {
            if(_dateFrom == null )
                _dateFrom = new PropertyPath<Date>(EntranceExamMeetingByCKReport2010Gen.P_DATE_FROM, this);
            return _dateFrom;
        }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#getDateTo()
     */
        public PropertyPath<Date> dateTo()
        {
            if(_dateTo == null )
                _dateTo = new PropertyPath<Date>(EntranceExamMeetingByCKReport2010Gen.P_DATE_TO, this);
            return _dateTo;
        }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#getStudentCategoryTitle()
     */
        public PropertyPath<String> studentCategoryTitle()
        {
            if(_studentCategoryTitle == null )
                _studentCategoryTitle = new PropertyPath<String>(EntranceExamMeetingByCKReport2010Gen.P_STUDENT_CATEGORY_TITLE, this);
            return _studentCategoryTitle;
        }

    /**
     * @return Не включать абитуриентов без оригиналов документов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#isOnlyWithOriginals()
     */
        public PropertyPath<Boolean> onlyWithOriginals()
        {
            if(_onlyWithOriginals == null )
                _onlyWithOriginals = new PropertyPath<Boolean>(EntranceExamMeetingByCKReport2010Gen.P_ONLY_WITH_ORIGINALS, this);
            return _onlyWithOriginals;
        }

    /**
     * @return Выводить только абитуриентов, поступающих на общих основаниях. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#isOnlyCompetitiveAdmission()
     */
        public PropertyPath<Boolean> onlyCompetitiveAdmission()
        {
            if(_onlyCompetitiveAdmission == null )
                _onlyCompetitiveAdmission = new PropertyPath<Boolean>(EntranceExamMeetingByCKReport2010Gen.P_ONLY_COMPETITIVE_ADMISSION, this);
            return _onlyCompetitiveAdmission;
        }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#getQualificationTitle()
     */
        public PropertyPath<String> qualificationTitle()
        {
            if(_qualificationTitle == null )
                _qualificationTitle = new PropertyPath<String>(EntranceExamMeetingByCKReport2010Gen.P_QUALIFICATION_TITLE, this);
            return _qualificationTitle;
        }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#getFormativeOrgUnitTitle()
     */
        public PropertyPath<String> formativeOrgUnitTitle()
        {
            if(_formativeOrgUnitTitle == null )
                _formativeOrgUnitTitle = new PropertyPath<String>(EntranceExamMeetingByCKReport2010Gen.P_FORMATIVE_ORG_UNIT_TITLE, this);
            return _formativeOrgUnitTitle;
        }

    /**
     * @return Территориальное подр..
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#getTerritorialOrgUnitTitle()
     */
        public PropertyPath<String> territorialOrgUnitTitle()
        {
            if(_territorialOrgUnitTitle == null )
                _territorialOrgUnitTitle = new PropertyPath<String>(EntranceExamMeetingByCKReport2010Gen.P_TERRITORIAL_ORG_UNIT_TITLE, this);
            return _territorialOrgUnitTitle;
        }

    /**
     * @return Уровень образования ОУ подразделение.
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#getEducationLevelHighSchoolTitle()
     */
        public PropertyPath<String> educationLevelHighSchoolTitle()
        {
            if(_educationLevelHighSchoolTitle == null )
                _educationLevelHighSchoolTitle = new PropertyPath<String>(EntranceExamMeetingByCKReport2010Gen.P_EDUCATION_LEVEL_HIGH_SCHOOL_TITLE, this);
            return _educationLevelHighSchoolTitle;
        }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#getDevelopFormTitle()
     */
        public PropertyPath<String> developFormTitle()
        {
            if(_developFormTitle == null )
                _developFormTitle = new PropertyPath<String>(EntranceExamMeetingByCKReport2010Gen.P_DEVELOP_FORM_TITLE, this);
            return _developFormTitle;
        }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#getDevelopConditionTitle()
     */
        public PropertyPath<String> developConditionTitle()
        {
            if(_developConditionTitle == null )
                _developConditionTitle = new PropertyPath<String>(EntranceExamMeetingByCKReport2010Gen.P_DEVELOP_CONDITION_TITLE, this);
            return _developConditionTitle;
        }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#getDevelopTechTitle()
     */
        public PropertyPath<String> developTechTitle()
        {
            if(_developTechTitle == null )
                _developTechTitle = new PropertyPath<String>(EntranceExamMeetingByCKReport2010Gen.P_DEVELOP_TECH_TITLE, this);
            return _developTechTitle;
        }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#getDevelopPeriodTitle()
     */
        public PropertyPath<String> developPeriodTitle()
        {
            if(_developPeriodTitle == null )
                _developPeriodTitle = new PropertyPath<String>(EntranceExamMeetingByCKReport2010Gen.P_DEVELOP_PERIOD_TITLE, this);
            return _developPeriodTitle;
        }

    /**
     * @return Без зачисленных. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#isNotIncludeEnrollment()
     */
        public PropertyPath<Boolean> notIncludeEnrollment()
        {
            if(_notIncludeEnrollment == null )
                _notIncludeEnrollment = new PropertyPath<Boolean>(EntranceExamMeetingByCKReport2010Gen.P_NOT_INCLUDE_ENROLLMENT, this);
            return _notIncludeEnrollment;
        }

    /**
     * @return Дополнительные статусы абитуриентов.
     * @see ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010#getEntrantCustomStateTitle()
     */
        public PropertyPath<String> entrantCustomStateTitle()
        {
            if(_entrantCustomStateTitle == null )
                _entrantCustomStateTitle = new PropertyPath<String>(EntranceExamMeetingByCKReport2010Gen.P_ENTRANT_CUSTOM_STATE_TITLE, this);
            return _entrantCustomStateTitle;
        }

        public Class getEntityClass()
        {
            return EntranceExamMeetingByCKReport2010.class;
        }

        public String getEntityName()
        {
            return "entranceExamMeetingByCKReport2010";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
