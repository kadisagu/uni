/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcReportPerson.ui.util;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.ClassUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportPrintInfo;
import ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.EcReportPersonAdd;
import ru.tandemservice.uniec.entity.catalog.EnrollmentRecommendation;
import ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.util.EntrantDataUtil;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Vasily Zhukov
 * @since 02.09.2011
 */
public class EcReportPersonUtil
{

    public static List<ExamGroup> getEntrantExamGroups(IReportPrintInfo printInfo, EntrantRequest request)
    {
        if (null == request) return Collections.emptyList();

        Map<Long, List<ExamGroup>> map = printInfo.getSharedObject(EcReportPersonAdd.PREFETCH_ENTRANT_EXAMGROUP_MAP);
        if (null == map) return Collections.emptyList();

        return map.get(request.getId());
    }


    public static void prefetchEntrantExamGroups(IReportPrintInfo printInfo, List<Object[]> rows, int requestIndex)
    {
        if (printInfo.getSharedObject(EcReportPersonAdd.PREFETCH_ENTRANT_EXAMGROUP_MAP) != null) return;

        final Map<Long, List<ExamGroup>> result = new HashMap<>();

        final List<Long> requestsIds = getEntityIds(rows, requestIndex, 2, EntrantRequest.class);

        BatchUtils.execute(requestsIds, 1000, new BatchUtils.Action<Long>()
        {
            @Override
            public void execute(Collection<Long> requestsIds)
            {

                List<Object[]> rows = DataAccessServices.dao().getList(new DQLSelectBuilder()
                                                                               .fromEntity(ExamGroupRow.class, "e")
                                                                               .column(property(ExamGroupRow.entrantRequest().id().fromAlias("e")))
                                                                               .column(property(ExamGroupRow.examGroup().fromAlias("e")))
                                                                               .where(in(property(ExamGroupRow.entrantRequest().id().fromAlias("e")), requestsIds))
                );

                for (Object[] row : rows)
                {
                    SafeMap.safeGet(result, (Long) row[0], ArrayList.class).add((ExamGroup) row[1]);
                }
            }
        });

        printInfo.putSharedObject(EcReportPersonAdd.PREFETCH_ENTRANT_EXAMGROUP_MAP, result);
    }


    public static List<OlympiadDiploma> getEntrantDataOlympiad(IReportPrintInfo printInfo, RequestedEnrollmentDirection direction)
    {
        if (null == direction) return Collections.emptyList();

        Map<Long, List<OlympiadDiploma>> map = printInfo.getSharedObject(EcReportPersonAdd.PREFETCH_DIRECTION_OLYMPIAD_MAP);
        if (null == map) return Collections.emptyList();

        return map.get(direction.getId());
    }

    public static void prefetchEntrantOlympiad(IReportPrintInfo printInfo, List<Object[]> rows, int directionIndex)
    {
        if (printInfo.getSharedObject(EcReportPersonAdd.PREFETCH_DIRECTION_OLYMPIAD_MAP) != null) return;

        final Map<Long, List<OlympiadDiploma>> result = new HashMap<>();

        final List<Long> directionIds = getEntityIds(rows, directionIndex, 3, RequestedEnrollmentDirection.class);

        BatchUtils.execute(directionIds, 1000, new BatchUtils.Action<Long>()
        {
            @Override
            public void execute(Collection<Long> directionIds)
            {

                List<Object[]> rows = DataAccessServices.dao().getList(new DQLSelectBuilder()
                                                                               .fromEntity(RequestedEnrollmentDirection.class, "d")
                                                                               .where(in(property("d.id"), directionIds))
                                                                               .column(property("d.id")).order(property("d.id"))

                                                                               .fromEntity(OlympiadDiploma.class, "e")
                                                                               .column(property("e"))
                                                                               .where(eq(property(OlympiadDiploma.entrant().fromAlias("e")), property(RequestedEnrollmentDirection.entrantRequest().entrant().fromAlias("d"))))
                                                                               .order(property(OlympiadDiploma.id().fromAlias("e")), OrderDirection.asc)
                );

                for (Object[] row : rows)
                {
                    SafeMap.safeGet(result, (Long) row[0], ArrayList.class).add((OlympiadDiploma) row[1]);
                }
            }
        });

        printInfo.putSharedObject(EcReportPersonAdd.PREFETCH_DIRECTION_OLYMPIAD_MAP, result);
    }


    public static List<EnrollmentRecommendation> getEntrantEnrollmentRecommendations(IReportPrintInfo printInfo, Entrant entrant)
    {
        if (null == entrant) return Collections.emptyList();

        Map<Long, List<EnrollmentRecommendation>> map = printInfo.getSharedObject(EcReportPersonAdd.PREFETCH_ENROLLMENT_RECOMMENDATION_MAP);

        return map.get(entrant.getId());
    }

    public static void prefetchEntrantEnrollmentRecommendations(IReportPrintInfo printInfo, List<Object[]> rows, int entrantIndex)
    {
        if (printInfo.getSharedObject(EcReportPersonAdd.PREFETCH_ENROLLMENT_RECOMMENDATION_MAP) != null) return;

        final Map<Long, List<EnrollmentRecommendation>> result = new HashMap<>();
        final Map<Long, EnrollmentRecommendation> recommendationMap = CommonDAO.map(DataAccessServices.dao().getList(EnrollmentRecommendation.class));


        List<Long> entrantIds = getEntrantIds(rows, entrantIndex);
        BatchUtils.execute(entrantIds, 1000, new BatchUtils.Action<Long>()
        {
            @Override
            public void execute(Collection<Long> entrantIds)
            {

                List<Object[]> rows = DataAccessServices.dao().getList(new DQLSelectBuilder().fromEntity(EntrantEnrolmentRecommendation.class, "e")
                                                                               .column(property(EntrantEnrolmentRecommendation.entrant().id().fromAlias("e")))
                                                                               .column(property(EntrantEnrolmentRecommendation.recommendation().id().fromAlias("e")))
                                                                               .where(in(property(EntrantEnrolmentRecommendation.entrant().id().fromAlias("e")), entrantIds))
                                                                               .order(property(EntrantEnrolmentRecommendation.entrant().id().fromAlias("e")))
                                                                               .order(property(EntrantEnrolmentRecommendation.recommendation().code().fromAlias("e")))
                );

                for (Object[] row : rows)
                {
                    SafeMap.safeGet(result, (Long) row[0], ArrayList.class).add(recommendationMap.get((Long) row[1]));
                }
            }
        });

        printInfo.putSharedObject(EcReportPersonAdd.PREFETCH_ENROLLMENT_RECOMMENDATION_MAP, result);
    }

    public static List<EntrantIndividualProgress> getEntrantIndividualProgress(IReportPrintInfo printInfo, EntrantRequest request)
    {
        if (request==null) return Collections.emptyList();

        Map<Long, List<EntrantIndividualProgress>> map = printInfo.getSharedObject(EcReportPersonAdd.PREFETCH_REQUEST_INDIVIDUAL_PROGRESS_MAP);

        return map.get(request.getId());
    }

    public static void prefetchRequestIndividualProgress(IReportPrintInfo printInfo, List<Object[]> rows, int requestIndex)
    {
        if (printInfo.getSharedObject(EcReportPersonAdd.PREFETCH_REQUEST_INDIVIDUAL_PROGRESS_MAP) != null) return;

        final Map<Long, List<EntrantIndividualProgress>> result = new HashMap<>();

        List<Long> requestIds = getEntityIds(rows, requestIndex, 2, EntrantRequest.class);
        String eipAlias = "eip";
        String reqAlias = "req";
        BatchUtils.execute(requestIds, DQL.MAX_VALUES_ROW_NUMBER, ids -> {
            List<Object[]> rows1 = DataAccessServices.dao().getList(
                    new DQLSelectBuilder()
                            .fromEntity(EntrantIndividualProgress.class, eipAlias)
                            .column(property(eipAlias))

                            .joinEntity(eipAlias, DQLJoinType.inner, EntrantRequest.class, reqAlias,
                                        and(eq(property(eipAlias, EntrantIndividualProgress.entrant()), property(reqAlias, EntrantRequest.entrant())),
                                            in(property(reqAlias, EntrantRequest.id()), ids)))
                            .column(property(reqAlias, EntrantRequest.id()))

                            .where(IndProg2Request4Exclude.getExpression4ExcludeEntrantIndividualProgress(property(reqAlias), property(eipAlias)))
            );

            rows1.forEach(row -> SafeMap.safeGet(result, (Long) row[1], ArrayList.class).add((EntrantIndividualProgress) row[0]));
        });
        result.values().forEach(list -> list.sort(Comparator.comparing(eip -> eip.getIndividualProgressType().getShortTitle())));

        printInfo.putSharedObject(EcReportPersonAdd.PREFETCH_REQUEST_INDIVIDUAL_PROGRESS_MAP, result);
    }

    public static EntrantDataUtil getEntrantDataUtil(IReportPrintInfo printInfo, RequestedEnrollmentDirection direction)
    {
        if (direction == null) return null;

        Map<EnrollmentCampaign, EntrantDataUtil> map = printInfo.getSharedObject(EcReportPersonAdd.PREFETCH_ENTRANT_DATA_UTIL);

        return map.get(direction.getEnrollmentDirection().getEnrollmentCampaign());
    }

    public static void prefetchEntrantDataUtil(IReportPrintInfo printInfo, List<Object[]> rows, int entrantIndex, int directionIdForDataUtilIndex)
    {
        if (printInfo.getSharedObject(EcReportPersonAdd.PREFETCH_ENTRANT_DATA_UTIL) != null) return;

        Map<EnrollmentCampaign, List<Long>> campaign2directionIds = new HashMap<>();

        for (Object[] row : rows)
        {
            @SuppressWarnings("unchecked")
            List<Entrant> entrantList = (List<Entrant>) row[entrantIndex];

            @SuppressWarnings("unchecked")
            Map<Entrant, Map<Long, Integer>> map = (Map<Entrant, Map<Long, Integer>>) row[directionIdForDataUtilIndex];

            for (Entrant entrant : entrantList)
            {
                if (entrant != null)
                {
                    List<Long> ids = campaign2directionIds.get(entrant.getEnrollmentCampaign());

                    if (ids == null)
                        campaign2directionIds.put(entrant.getEnrollmentCampaign(), ids = new ArrayList<>());

                    ids.addAll(map.get(entrant).keySet());
                }
            }
        }

        Map<EnrollmentCampaign, EntrantDataUtil> entrantDataUtilMap = new HashMap<>();

        for (Map.Entry<EnrollmentCampaign, List<Long>> entry : campaign2directionIds.entrySet())
        {
            EnrollmentCampaign enrollmentCampaign = entry.getKey();
            List<Long> ids = entry.getValue();

            MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "e");

            if (ids.size() > 1000 && ids.size() < 1900)
                builder.add(MQExpression.in("e", RequestedEnrollmentDirection.id(), ids));
            else
                builder.add(MQExpression.eq("e", RequestedEnrollmentDirection.enrollmentDirection().enrollmentCampaign(), enrollmentCampaign));

            entrantDataUtilMap.put(enrollmentCampaign, new EntrantDataUtil(DataAccessServices.dao().getComponentSession(), enrollmentCampaign, builder, EntrantDataUtil.DetailLevel.EXAM_PASS_WITH_MARK_STATISTICS));
        }

        printInfo.putSharedObject(EcReportPersonAdd.PREFETCH_ENTRANT_DATA_UTIL, entrantDataUtilMap);
    }

    public static Map<Long, Object[]> getStateExamSubjectMarkMap(IReportPrintInfo printInfo, Entrant entrant)
    {
        Map<Long, Map<Long, Object[]>> map = printInfo.getSharedObject(EcReportPersonAdd.PREFETCH_STATE_EXAM_SUBJECT_MARK_MAP);

        return map.get(entrant.getId());
    }

    public static void prefetchStateExamSubjectMarkMap(IReportPrintInfo printInfo, List<Object[]> rows, int entrantIndex)
    {
        if (printInfo.getSharedObject(EcReportPersonAdd.PREFETCH_STATE_EXAM_SUBJECT_MARK_MAP) != null) return;

        // entrantId -> stateExamSubjectId -> [mark (int), certNumber (String)]
        Map<Long, Map<Long, Object[]>> result = new HashMap<>();

        List<Long> entrantIds = getEntrantIds(rows, entrantIndex);

        if (!entrantIds.isEmpty())
        {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StateExamSubjectMark.class, "e")
                    .column(DQLExpressions.property(StateExamSubjectMark.subject().id().fromAlias("e")))
                    .column(DQLExpressions.property(StateExamSubjectMark.mark().fromAlias("e")))
                    .column(DQLExpressions.property(StateExamSubjectMark.certificate().number().fromAlias("e")))
                    .column(DQLExpressions.property(StateExamSubjectMark.certificate().entrant().id().fromAlias("e")));

            if (entrantIds.size() < 1000)
            {
                builder.where(DQLExpressions.in(DQLExpressions.property(StateExamSubjectMark.certificate().entrant().id().fromAlias("e")), entrantIds));
            }
            else
            {
                EnrollmentCampaign enrollmentCampaign = printInfo.getSharedObject(EcReportPersonAdd.PREFETCH_ENROLLMENT_CAMPAIGN);
                if (enrollmentCampaign != null)
                    builder.where(DQLExpressions.eq(DQLExpressions.property(StateExamSubjectMark.certificate().entrant().enrollmentCampaign().fromAlias("e")), DQLExpressions.value(enrollmentCampaign)));
            }

            List<Object[]> rowList = DataAccessServices.dao().getList(builder);
            for (Object[] row : rowList)
            {
                Long subjectId = (Long) row[0];
                int mark = (Integer) row[1];
                String number = (String) row[2];
                Long entrantId = (Long) row[3];

                Map<Long, Object[]> map = result.get(entrantId);
                if (map == null)
                    result.put(entrantId, map = new HashMap<>());

                Object[] data = map.get(subjectId);
                if (data == null || mark > (Integer) data[0])
                    map.put(subjectId, new Object[]{mark, number});

                result.put(entrantId, map);
            }
        }

        printInfo.putSharedObject(EcReportPersonAdd.PREFETCH_STATE_EXAM_SUBJECT_MARK_MAP, result);
    }

    public static String getProfileKnowledgeTitle(IReportPrintInfo printInfo, RequestedEnrollmentDirection direction)
    {
        Map<Long, String> map = printInfo.getSharedObject(EcReportPersonAdd.PREFETCH_PROFILE_KNOWLEDGE_MAP);

        return map.get(direction.getId());
    }

    public static void prefetchProfileKnowledgeMap(IReportPrintInfo printInfo, List<Object[]> rows, int entrantIndex)
    {
        if (printInfo.getSharedObject(EcReportPersonAdd.PREFETCH_PROFILE_KNOWLEDGE_MAP) != null) return;

        Map<Long, String> result = new HashMap<>();

        List<Long> entrantIds = getEntrantIds(rows, entrantIndex);

        if (!entrantIds.isEmpty())
        {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RequestedProfileKnowledge.class, "e")
                    .column(DQLExpressions.property(RequestedProfileKnowledge.requestedEnrollmentDirection().id().fromAlias("e")))
                    .column(DQLExpressions.property(RequestedProfileKnowledge.profileKnowledge().title().fromAlias("e")));

            if (entrantIds.size() < 1000)
            {
                builder.where(DQLExpressions.in(DQLExpressions.property(RequestedProfileKnowledge.requestedEnrollmentDirection().entrantRequest().entrant().fromAlias("e")), entrantIds));
            }
            else
            {
                EnrollmentCampaign enrollmentCampaign = printInfo.getSharedObject(EcReportPersonAdd.PREFETCH_ENROLLMENT_CAMPAIGN);
                if (enrollmentCampaign != null)
                    builder.where(DQLExpressions.eq(DQLExpressions.property(RequestedProfileKnowledge.requestedEnrollmentDirection().entrantRequest().entrant().enrollmentCampaign().fromAlias("e")), DQLExpressions.value(enrollmentCampaign)));
            }

            Map<Long, Set<String>> map = new HashMap<>();

            List<Object[]> rowList = DataAccessServices.dao().getList(builder);

            for (Object[] row : rowList)
            {
                Long directionId = (Long) row[0];
                String title = (String) row[1];

                Set<String> set = map.get(directionId);
                if (set == null)
                    map.put(directionId, set = new TreeSet<>());
                set.add(title);
            }

            for (Map.Entry<Long, Set<String>> entry : map.entrySet())
                result.put(entry.getKey(), StringUtils.join(entry.getValue(), ", "));

        }

        printInfo.putSharedObject(EcReportPersonAdd.PREFETCH_PROFILE_KNOWLEDGE_MAP, result);
    }

    public static PreliminaryEnrollmentStudent getPreStudent(IReportPrintInfo printInfo, RequestedEnrollmentDirection direction)
    {
        Map<Long, PreliminaryEnrollmentStudent> map = printInfo.getSharedObject(EcReportPersonAdd.PREFETCH_PRE_STUDENT_MAP);

        return map.get(direction.getId());
    }

    public static void prefetchPreStudentMap(IReportPrintInfo printInfo, List<Object[]> rows, int entrantIndex)
    {
        if (printInfo.getSharedObject(EcReportPersonAdd.PREFETCH_PRE_STUDENT_MAP) != null) return;

        Map<Long, PreliminaryEnrollmentStudent> result = new HashMap<>();

        List<Long> entrantIds = getEntrantIds(rows, entrantIndex);

        if (!entrantIds.isEmpty())
        {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(PreliminaryEnrollmentStudent.class, "e");

            if (entrantIds.size() < 1000)
            {
                builder.where(DQLExpressions.in(DQLExpressions.property(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().entrantRequest().entrant().id().fromAlias("e")), entrantIds));
            }
            else
            {
                EnrollmentCampaign enrollmentCampaign = printInfo.getSharedObject(EcReportPersonAdd.PREFETCH_ENROLLMENT_CAMPAIGN);
                if (enrollmentCampaign != null)
                    builder.where(DQLExpressions.eq(DQLExpressions.property(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().entrantRequest().entrant().enrollmentCampaign().fromAlias("e")), DQLExpressions.value(enrollmentCampaign)));
            }

            List<PreliminaryEnrollmentStudent> rowList = DataAccessServices.dao().getList(builder);

            for (PreliminaryEnrollmentStudent preStudent : rowList)
                result.put(preStudent.getRequestedEnrollmentDirection().getId(), preStudent);
        }

        printInfo.putSharedObject(EcReportPersonAdd.PREFETCH_PRE_STUDENT_MAP, result);
    }

    public static EnrollmentExtract getEnrollmentExtract(IReportPrintInfo printInfo, RequestedEnrollmentDirection direction)
    {
        Map<Long, EnrollmentExtract> map = printInfo.getSharedObject(EcReportPersonAdd.PREFETCH_ENROLLMENT_EXTRACT_MAP);

        return map.get(direction.getId());
    }

    public static void prefetchEnrollmentExtractMap(IReportPrintInfo printInfo, List<Object[]> rows, int entrantIndex)
    {
        if (printInfo.getSharedObject(EcReportPersonAdd.PREFETCH_ENROLLMENT_EXTRACT_MAP) != null) return;

        Map<Long, EnrollmentExtract> result = new HashMap<>();

        List<Long> entrantIds = getEntrantIds(rows, entrantIndex);

        if (!entrantIds.isEmpty())
        {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrollmentExtract.class, "e");

            if (entrantIds.size() < 1000)
            {
                builder.where(DQLExpressions.in(DQLExpressions.property(EnrollmentExtract.entity().requestedEnrollmentDirection().entrantRequest().entrant().id().fromAlias("e")), entrantIds));
            }
            else
            {
                EnrollmentCampaign enrollmentCampaign = printInfo.getSharedObject(EcReportPersonAdd.PREFETCH_ENROLLMENT_CAMPAIGN);
                if (enrollmentCampaign != null)
                    builder.where(DQLExpressions.eq(DQLExpressions.property(EnrollmentExtract.entity().requestedEnrollmentDirection().entrantRequest().entrant().enrollmentCampaign().fromAlias("e")), DQLExpressions.value(enrollmentCampaign)));
            }

            List<EnrollmentExtract> rowList = DataAccessServices.dao().getList(builder);

            for (EnrollmentExtract extract : rowList)
                result.put(extract.getEntity().getRequestedEnrollmentDirection().getId(), extract);
        }

        printInfo.putSharedObject(EcReportPersonAdd.PREFETCH_ENROLLMENT_EXTRACT_MAP, result);
    }


    private static void _fillIdsFromMap(Collection<Long> ids, Object o, int levels, Class<?> check)
    {
        // пустые значения пропускаем
        if (null == o)
        {
            return;
        }

        // если мы должны взять значение (ключ)
        if (0 == levels)
        {

            // предварительная обработка для Map.Entry
            if (o instanceof Map.Entry)
            {
                o = ((Map.Entry) o).getKey(); // значение берется из ключа
                if (null == o)
                {
                    return;
                } // что, впринципе, бред... но чего только не случается
            }

            if (null != check)
            {
                if (!check.isInstance(o))
                {
                    throw new ClassCastException(String.valueOf(ClassUtils.getUserClass(o)) + " is not instance of " + check);
                }
            }

            // обработка ключей
            if (o instanceof Long)
            {
                ids.add((Long) o);
            }
            else if (o instanceof IEntity)
            {
                ids.add(((IEntity) o).getId());
            }
            else
            {
                throw new IllegalStateException(String.valueOf(ClassUtils.getUserClass(o)));
            }

            // значение получили - сохранили его в ids, модем выходить
            return;
        }

        // в противном случае - мы долдны получить список для дальнейшей обработки

        // предварительная обработка для Map.Entry
        if (o instanceof Map.Entry)
        {
            o = ((Map.Entry) o).getValue(); // коллекция берется из значения
            if (null == o)
            {
                return;
            } // что, впринципе, бред... но чего только не случается
        }

        // обработка значений
        if (o instanceof Collection)
        {
            for (Object item : (Collection) o)
            {
                if (null == item)
                {
                    continue;
                }
                _fillIdsFromMap(ids, item, levels - 1, check);
            }
        }
        else if (o instanceof Map)
        {
            for (Object entry : ((Map) o).entrySet())
            {
                if (null == entry)
                {
                    continue;
                }
                _fillIdsFromMap(ids, entry, levels - 1, check);
            }
        }
        else
        {
            throw new IllegalStateException(String.valueOf(ClassUtils.getUserClass(o)));
        }
    }

    private static List<Long> getEntityIds(List<Object[]> rows, int entityListIndex, int maplevel, Class<?> check)
    {
        List<Long> ids = new ArrayList<>();
        for (Object[] row : rows)
        {
            _fillIdsFromMap(ids, row[entityListIndex], maplevel, check);
        }
        return ids;
    }


    private static List<Long> getEntrantIds(List<Object[]> rows, int entrantListIndex)
    {
        return getEntityIds(rows, entrantListIndex, 1, Entrant.class);
    }

    public static void prefetchEntrantCustomState(IReportPrintInfo printInfo, List<Object[]> rows, int entrantIndex)
    {
        if (printInfo.getSharedObject(EcReportPersonAdd.PREFETCH_ENTRANT_CUSTOMSTATE_MAP) != null)
            return;

        final Map<Long, List<EntrantCustomStateCI>> result = new HashMap<>();

        List<Long> entrantIds = getEntrantIds(rows, entrantIndex);
        BatchUtils.execute(entrantIds, 1000, new BatchUtils.Action<Long>()
        {
            @Override
            public void execute(Collection<Long> entrantIds)
            {

                List<Object[]> rows = DataAccessServices.dao().getList(
                        new DQLSelectBuilder().fromEntity(EntrantCustomState.class, "e")
                                .column(property(EntrantCustomState.entrant().id().fromAlias("e")))
                                .column(property(EntrantCustomState.customState().fromAlias("e")))
                                .where(in(property(EntrantCustomState.entrant().id().fromAlias("e")), entrantIds))
                                .order(property(EntrantCustomState.entrant().id().fromAlias("e")))
                                .order(property(EntrantCustomState.customState().code().fromAlias("e")))
                );

                for (Object[] row : rows)
                {
                    final Long entrantId = (Long) row[0];
                    final EntrantCustomStateCI customState = (EntrantCustomStateCI) row[1];

                    SafeMap.safeGet(result, entrantId, ArrayList.class)
                            .add(customState);
                }
            }
        });

        printInfo.putSharedObject(EcReportPersonAdd.PREFETCH_ENTRANT_CUSTOMSTATE_MAP, result);
    }

    public static Collection<EntrantCustomStateCI> getEntrantCustomStates(IReportPrintInfo printInfo, Entrant entrant)
    {
        if (null == entrant)
            return Collections.emptyList();

        Map<Long, List<EntrantCustomStateCI>> map = printInfo.getSharedObject(EcReportPersonAdd.PREFETCH_ENTRANT_CUSTOMSTATE_MAP);

        return map.get(entrant.getId());
    }
}
