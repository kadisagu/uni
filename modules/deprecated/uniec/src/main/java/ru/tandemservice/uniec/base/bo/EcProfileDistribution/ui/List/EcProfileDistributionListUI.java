/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec.base.bo.EcProfileDistribution.ui.List;

import org.hibernate.Session;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.catalog.gen.CompensationTypeGen;
import ru.tandemservice.uniec.base.bo.EcDistribution.logic.EcSimpleDSHandler;
import ru.tandemservice.uniec.base.bo.EcProfileDistribution.EcProfileDistributionManager;
import ru.tandemservice.uniec.base.bo.EcProfileDistribution.util.EcgpConfigDTO;
import ru.tandemservice.uniec.base.bo.EcProfileDistribution.util.EcgpDistributionDTO;
import ru.tandemservice.uniec.base.bo.EcProfileDistribution.util.IEcgpQuotaFreeDTO;
import ru.tandemservice.uniec.base.entity.ecgp.EcgpDistribution;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;

import java.util.*;

/**
 * @author Vasily Zhukov
 * @since 10.05.2012
 */
public class EcProfileDistributionListUI extends UIPresenter implements MultiEnrollmentDirectionUtil.Model
{
    private static final String ENROLLMENT_CAMPAIGN_FILTER = "enrollmentCampaign";
    private static final String COMPENSATION_TYPE_FILTER = "compensationType";
    private static final String DISTRIBUTION_CATEGORY_FILTER = "distributionCategory";

    private static final String FORMATIVE_ORG_UNIT_FILTER = "formativeOrgUnitList";
    private static final String TERRITORIAL_ORG_UNIT_FILTER = "territorialOrgUnitList";
    private static final String EDUCATION_LEVEL_HIGH_SCHOOL_FILTER = "educationLevelHighSchoolList";
    private static final String DEVELOP_FORM_FILTER = "developFormList";
    private static final String DEVELOP_CONDITION_FILTER = "developConditionList";
    private static final String DEVELOP_TECH_FILTER = "developTechList";
    private static final String DEVELOP_PERIOD_FILTER = "developPeriodList";

    private MultiEnrollmentDirectionUtil.Parameters _parameters;
    private ISelectModel _formativeOrgUnitListModel;
    private ISelectModel _territorialOrgUnitListModel;
    private ISelectModel _educationLevelHighSchoolListModel;
    private ISelectModel _developFormListModel;
    private ISelectModel _developTechListModel;
    private ISelectModel _developConditionListModel;
    private ISelectModel _developPeriodListModel;

    private Map<Long, EcgpDistributionDTO> id2valueMap = new HashMap<>();
    private Map<Long, String> _quotaMap;

    @Override
    public void onComponentRefresh()
    {
        ICommonDAO dao = DataAccessServices.dao();
        Session session = dao.getComponentSession();

        // init select models
        _parameters = new MultiEnrollmentDirectionUtil.Parameters();
        _formativeOrgUnitListModel = MultiEnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(null, this);
        _territorialOrgUnitListModel = MultiEnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(null, this);
        _educationLevelHighSchoolListModel = MultiEnrollmentDirectionUtil.createEducationLevelsHighSchoolModel(this);
        _developFormListModel = MultiEnrollmentDirectionUtil.createDevelopFormModel(this);
        _developConditionListModel = MultiEnrollmentDirectionUtil.createDevelopConditionModel(this);
        _developTechListModel = MultiEnrollmentDirectionUtil.createDevelopTechModel(this);
        _developPeriodListModel = MultiEnrollmentDirectionUtil.createDevelopPeriodModel(this);

        initDefaults();
    }

    private void initDefaults()
    {
        // init enrollment campaign
        if (getEnrollmentCampaign() == null)
        {
            List<EnrollmentCampaign> list = DataAccessServices.dao().getList(EnrollmentCampaign.class, EnrollmentCampaign.P_ID);
            if (list != null && !list.isEmpty())
                _uiSettings.set(ENROLLMENT_CAMPAIGN_FILTER, list.get(list.size() - 1));
        }

        // compensationType
        if (_uiSettings.get(COMPENSATION_TYPE_FILTER) == null)
        {
            _uiSettings.set(COMPENSATION_TYPE_FILTER, DataAccessServices.dao().<CompensationType>getByNaturalId(new CompensationTypeGen.NaturalId(UniDefines.COMPENSATION_TYPE_BUDGET)));
        }

        // init secondHighAdmission
        if (_uiSettings.get(DISTRIBUTION_CATEGORY_FILTER) == null)
        {
            _uiSettings.set(DISTRIBUTION_CATEGORY_FILTER, EcProfileDistributionManager.DISTRIBUTION_CATEGORY_STUDENTS_AND_LISTENERS);
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        List<EcgpDistributionDTO> list = EcProfileDistributionManager.instance().dao().getDistributionDTOList(
                getEnrollmentCampaign(),
                _uiSettings.<CompensationType>get(COMPENSATION_TYPE_FILTER),
                EcProfileDistributionManager.DISTRIBUTION_CATEGORY_SECOND_HIGH_ADMISSION.equals(_uiSettings.<IEntity>get(DISTRIBUTION_CATEGORY_FILTER)),
                getSelectedFormativeOrgUnitList(),
                getSelectedTerritorialOrgUnitList(),
                getSelectedEducationLevelHighSchoolList(),
                getSelectedDevelopFormList(),
                getSelectedDevelopConditionList(),
                getSelectedDevelopTechList(),
                getSelectedDevelopPeriodList()
        );

        _quotaMap = new HashMap<>();

        // сохраняем мап: id -> DTO
        for (EcgpDistributionDTO item : list)
        {
            id2valueMap.put(item.getId(), item);

            if (item.getPersistentId() != null)
            {
                IEcgpQuotaFreeDTO freeDTO = EcProfileDistributionManager.instance().dao().getFreeQuotaDTO(DataAccessServices.dao().<EcgpDistribution>getNotNull(item.getPersistentId()));
                Map<Long, String> quotaMap = EcProfileDistributionManager.instance().dao().getQuotaHTMLDescription(freeDTO);
                _quotaMap.put(item.getPersistentId(), quotaMap.get(0L));
            }
        }

        dataSource.put(EcSimpleDSHandler.LIST, list);
    }

    // Settings

    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return (EnrollmentCampaign) _uiSettings.get(ENROLLMENT_CAMPAIGN_FILTER);
    }

    // MultiEnrollmentDirectionUtil.Model

    @Override
    public MultiEnrollmentDirectionUtil.Parameters getParameters()
    {
        return _parameters;
    }

    @Override
    public List<EnrollmentCampaign> getSelectedEnrollmentCampaignList()
    {
        return MultiEnrollmentDirectionUtil.getSingletonList(getEnrollmentCampaign());
    }

    @Override
    public List<OrgUnit> getSelectedFormativeOrgUnitList()
    {
        List<OrgUnit> list = _uiSettings.get(FORMATIVE_ORG_UNIT_FILTER);
        return list == null || list.isEmpty() ? null : list;
    }

    @Override
    public List<OrgUnit> getSelectedTerritorialOrgUnitList()
    {
        List<OrgUnit> list = _uiSettings.get(TERRITORIAL_ORG_UNIT_FILTER);
        return list == null || list.isEmpty() ? null : list;
    }

    @Override
    public List<EducationLevelsHighSchool> getSelectedEducationLevelHighSchoolList()
    {
        List<EducationLevelsHighSchool> list = _uiSettings.get(EDUCATION_LEVEL_HIGH_SCHOOL_FILTER);
        return list == null || list.isEmpty() ? null : list;
    }

    @Override
    public List<DevelopForm> getSelectedDevelopFormList()
    {
        List<DevelopForm> list = _uiSettings.get(DEVELOP_FORM_FILTER);
        return list == null || list.isEmpty() ? null : list;
    }

    @Override
    public List<DevelopCondition> getSelectedDevelopConditionList()
    {
        List<DevelopCondition> list = _uiSettings.get(DEVELOP_CONDITION_FILTER);
        return list == null || list.isEmpty() ? null : list;
    }

    @Override
    public List<DevelopTech> getSelectedDevelopTechList()
    {
        List<DevelopTech> list = _uiSettings.get(DEVELOP_TECH_FILTER);
        return list == null || list.isEmpty() ? null : list;
    }

    @Override
    public List<DevelopPeriod> getSelectedDevelopPeriodList()
    {
        List<DevelopPeriod> list = _uiSettings.get(DEVELOP_PERIOD_FILTER);
        return list == null || list.isEmpty() ? null : list;
    }

    //

    public String getQuota()
    {
        EcgpDistributionDTO distributionDTO = getConfig().getDataSource(EcProfileDistributionList.DISTRIBUTION_DS).getCurrent();

        if (distributionDTO.getPersistentId() == null) return null;

        return _quotaMap.get(distributionDTO.getPersistentId());
    }

    // Getters

    public ISelectModel getFormativeOrgUnitListModel()
    {
        return _formativeOrgUnitListModel;
    }

    public ISelectModel getTerritorialOrgUnitListModel()
    {
        return _territorialOrgUnitListModel;
    }

    public ISelectModel getEducationLevelHighSchoolListModel()
    {
        return _educationLevelHighSchoolListModel;
    }

    public ISelectModel getDevelopFormListModel()
    {
        return _developFormListModel;
    }

    public ISelectModel getDevelopTechListModel()
    {
        return _developTechListModel;
    }

    public ISelectModel getDevelopConditionListModel()
    {
        return _developConditionListModel;
    }

    public ISelectModel getDevelopPeriodListModel()
    {
        return _developPeriodListModel;
    }

    // Listeners

    public void onClickShow()
    {
        saveSettings();
    }

    public void onClickClear()
    {
        clearSettings();
        initDefaults();
        saveSettings();
    }

    public void onClickBulkMake()
    {
        Set<EcgpConfigDTO> selectedConfigSet = getSelectedConfigSet();

        _uiSupport.doRefresh();

        refreshConfigSet(selectedConfigSet);

        try
        {
            EcProfileDistributionManager.instance().dao().doBulkMakeDistribution(selectedConfigSet);
            clearSelectedConfigSet();
        } catch (ApplicationException e)
        {
            _uiSupport.doRefresh();

            throw e;
        }
    }

    public void onClickBulkFill()
    {
        Set<EcgpConfigDTO> selectedConfigSet = getSelectedConfigSet();

        _uiSupport.doRefresh();

        refreshConfigSet(selectedConfigSet);

        try
        {
            EcProfileDistributionManager.instance().dao().doBulkFillDistribution(selectedConfigSet);
            clearSelectedConfigSet();
        } catch (ApplicationException e)
        {
            _uiSupport.doRefresh();

            throw e;
        }
    }

    public void onClickBulkDelete()
    {
        Set<EcgpConfigDTO> selectedConfigSet = getSelectedConfigSet();

        _uiSupport.doRefresh();

        refreshConfigSet(selectedConfigSet);

        EcProfileDistributionManager.instance().dao().doBulkDeleteDistribution(selectedConfigSet);
        clearSelectedConfigSet();
    }

    public void onClickAdd()
    {
        _uiSupport.doRefresh();

        EcgpDistributionDTO item = id2valueMap.get(getListenerParameterAsLong());

        if (item == null || item.isAddDisabled()) return;

        DataAccessServices.dao().getComponentSession().refresh(item.getEcgItem());

        EcProfileDistributionManager.instance().dao().saveDistribution(item.getConfigDTO());
    }

    public void onDeleteEntityFromList()
    {
        _uiSupport.doRefresh();

        EcgpDistributionDTO item = id2valueMap.get(getListenerParameterAsLong());

        if (item == null || item.isDeleteDisabled() || item.getPersistentId() == null) return;

        EcProfileDistributionManager.instance().dao().deleteDistribution(item.getPersistentId());
    }

    // Private

    private Set<EcgpConfigDTO> getSelectedConfigSet()
    {
        Collection<IEntity> selected = ((CheckboxColumn) (((BaseSearchListDataSource) _uiConfig.getDataSource(EcProfileDistributionList.DISTRIBUTION_DS)).getLegacyDataSource()).getColumn("checkbox")).getSelectedObjects();

        Set<EcgpConfigDTO> configSet = new HashSet<>();

        for (IEntity entity : selected)
            configSet.add(((EcgpDistributionDTO) entity).getConfigDTO());

        return configSet;
    }

    private void refreshConfigSet(Set<EcgpConfigDTO> configSet)
    {
        Session session = DataAccessServices.dao().getComponentSession();
        for (EcgpConfigDTO item : configSet)
            session.refresh(item.getEcgItem());
    }

    private void clearSelectedConfigSet()
    {
        ((CheckboxColumn) (((BaseSearchListDataSource) _uiConfig.getDataSource(EcProfileDistributionList.DISTRIBUTION_DS)).getLegacyDataSource()).getColumn("checkbox")).getSelectedObjects().clear();
    }
}
