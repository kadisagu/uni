/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.util;

import java.util.List;
import java.util.Map;

import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;

/**
 * Планы приема по направлениям приема и видам цп
 *
 * @author Vasily Zhukov
 * @since 13.07.2011
 */
public interface IEcgQuotaDTO
{
    /**
     * @return список видов цп входящих в распределение
     */
    List<TargetAdmissionKind> getTaKindList();

    /**
     * @return направление приема -> план (всегда не null)
     */
    Map<Long, Integer> getQuotaMap();

    /**
     * @return направление приема -> вид цп -> план по виду цп (всегда не null)
     */
    Map<Long, Map<Long, Integer>> getTaQuotaMap();

    /**
     * @return количество свободных мест
     */
    int getTotalQuota();

    /**
     * @return true, если нет свободных мест 
     */
    boolean isEmpty();
}
