// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EntrantRegistryForTC.EntrantRegistryForTCAdd;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.fias.base.entity.Address;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import org.tandemframework.shared.person.catalog.entity.EduInstitution;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.util.EntrantDataUtil;

import java.util.*;

/**
 * @author oleyba
 * @since 18.06.2010
 */
public class EntrantRegistryForTCReportBuilder
{
    private Model model;
    private Session session;
    private String cgTitle;

    public EntrantRegistryForTCReportBuilder(Model model, String cgTitle, Session session)
    {
        this.model = model;
        this.session = session;
        this.cgTitle = cgTitle;
    }

    public DatabaseFile getContent()
    {
        IUniBaseDao dao = UniDaoFacade.getCoreDao();
        IScriptItem templateDocument = dao.getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ENTRANT_REGISTRY_FOR_TC);
        RtfDocument template = new RtfReader().read(templateDocument.getCurrentTemplate());
        RtfDocument document = template.getClone();

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("cgTitle", cgTitle);
        injectModifier.put("tc", model.getTechnicCommission().getTitle());
        injectModifier.put("date", DateFormatter.STRING_MONTHS_AND_QUOTES.format(new Date()));
        injectModifier.modify(document);

        EntrantDataUtil dataUtil = new EntrantDataUtil(session, model.getReport().getEnrollmentCampaign(), getRequestedEnrollmentDirectionBuilder());
        Map<EntrantRequest, Map<CompensationType, List<RequestedEnrollmentDirection>>> entrantRequestMap = new LinkedHashMap<EntrantRequest, Map<CompensationType, List<RequestedEnrollmentDirection>>>();
        Map<EntrantRequest, RequestedEnrollmentDirection> firstPriorityDirectionMap = new HashMap<EntrantRequest, RequestedEnrollmentDirection>();
        prepareEntrantRequestMap(entrantRequestMap, firstPriorityDirectionMap);

        List<String[]> tableRows = new ArrayList<String[]>();
        int rowNumber = 1;
        for (EntrantRequest entrantRequest : entrantRequestMap.keySet())
            for (CompensationType compensationType : dao.getCatalogItemListOrderByCode(CompensationType.class))
            {
                List<RequestedEnrollmentDirection> directions = entrantRequestMap.get(entrantRequest).get(compensationType);
                if (null != directions)
                    tableRows.add(printRow(rowNumber++, entrantRequest, firstPriorityDirectionMap.get(entrantRequest), directions, dataUtil));
            }

        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T", tableRows.toArray(new String[tableRows.size()][]));
        tableModifier.modify(document);

        DatabaseFile content = new DatabaseFile();
        content.setContent(RtfUtil.toByteArray(document));
        return content;
    }

    private static final DateFormatter YEAR_FORMATTER = new DateFormatter(DateFormatter.PATTERN_JUST_YEAR);

    private String[] printRow(
            int rowNumber,
            EntrantRequest entrantRequest,
            RequestedEnrollmentDirection firstPriority,
            List<RequestedEnrollmentDirection> directions,
            EntrantDataUtil dataUtil)
    {
        Entrant entrant = entrantRequest.getEntrant();
        Person person = entrant.getPerson();
        IdentityCard identityCard = person.getIdentityCard();
        AddressBase address = person.getAddress();
        PersonEduInstitution eduInstitution = person.getPersonEduInstitution();

        List<String> result = new ArrayList<String>();
        result.add(String.valueOf(rowNumber));
        result.add(firstPriority.getEnrollmentDirection().getCompetitionGroup().getTitle());
        result.add(UniStringUtils.join(directions, RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().shortTitle().s(), ", "));
        result.add(directions.get(0).getCompensationType().isBudget() ? "Б" : "К");
        result.add(StringUtils.uncapitalize(firstPriority.getEnrollmentDirection().getEducationOrgUnit().getDevelopForm().getTitle()));
        result.add(entrant.getPersonalNumber() + "/" + firstPriority.getEnrollmentDirection().getCompetitionGroup().getTitle() + "/" + String.valueOf(entrantRequest.getRegNumber()));
        result.add(identityCard.getFullFio());
        result.add(DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(dataUtil.getFinalMark(firstPriority)));
        result.add(identityCard.getSex().isMale() ? "м" : "ж");
        result.add(YEAR_FORMATTER.format(identityCard.getBirthDate()));
        result.add(printEduInstitution(eduInstitution));
        result.add(printDocument(eduInstitution));
        result.add(address == null ? "" : address.getTitleWithFlat());
        for (int i = 0; i < 4; i++) result.add("");
        result.add(firstPriority.isStudiedOPP() ? "да" : "нет");
        result.add(directions.get(0).isOriginalDocumentHandedIn() ? "О" : "К");
        return result.toArray(new String[result.size()]);
    }

    private void prepareEntrantRequestMap(
            Map<EntrantRequest, Map<CompensationType, List<RequestedEnrollmentDirection>>> entrantRequestMap,
            Map<EntrantRequest, RequestedEnrollmentDirection> firstPriorityDirectionMap)
    {
        MQBuilder directionBuilder = getRequestedEnrollmentDirectionBuilder();
        directionBuilder.addOrder("r", RequestedEnrollmentDirection.entrantRequest().regDate().s());
        directionBuilder.addOrder("r", RequestedEnrollmentDirection.priority().s());
        directionBuilder.addJoinFetch("r", RequestedEnrollmentDirection.enrollmentDirection().s(), "ed");
        directionBuilder.addJoinFetch("ed", EnrollmentDirection.educationOrgUnit().s(), "ou");
        directionBuilder.addJoinFetch("ou", EducationOrgUnit.educationLevelHighSchool().s(), "hs");
        directionBuilder.addJoinFetch("hs", EducationLevelsHighSchool.educationLevel().s(), "el");
        directionBuilder.addJoinFetch("r", RequestedEnrollmentDirection.entrantRequest().s(), "eq");
        directionBuilder.addJoinFetch("eq", EntrantRequest.entrant().s(), "e");
        directionBuilder.addJoinFetch("e", Entrant.person().s(), "person");
        directionBuilder.addJoinFetch("person", Person.identityCard().s(), "idc");

        for (RequestedEnrollmentDirection direction : directionBuilder.<RequestedEnrollmentDirection>getResultList(session))
        {
            EntrantRequest entrantRequest = direction.getEntrantRequest();
            RequestedEnrollmentDirection firstPriorityDirection = firstPriorityDirectionMap.get(entrantRequest);
            if (null == firstPriorityDirection)
                firstPriorityDirectionMap.put(entrantRequest, firstPriorityDirection = direction);
            // если в первом по приоритету направлении не было конкурсной группы - заявление не учитываем
            if (firstPriorityDirection.getEnrollmentDirection().getCompetitionGroup() == null)
                continue;
            // если в первом по приоритету направлении другая конкурсная группа - выбранное направление не учитываем
            if (!firstPriorityDirection.getEnrollmentDirection().getCompetitionGroup().equals(direction.getEnrollmentDirection().getCompetitionGroup())) continue;
            // фильтруем по типу возм. затрат (выбирали без фильтра, чтобы сделать проверки выше)
            CompensationType compType = direction.getCompensationType();
            if (model.getCompensationType() != null && !model.getCompensationType().equals(compType))
                continue;
            // распихиваем в мапу
            Map<CompensationType, List<RequestedEnrollmentDirection>> compTypeMap = entrantRequestMap.get(entrantRequest);
            if (null == compTypeMap)
                entrantRequestMap.put(entrantRequest, compTypeMap = new HashMap<CompensationType, List<RequestedEnrollmentDirection>>());
            List<RequestedEnrollmentDirection> directions = compTypeMap.get(compType);
            if (null == directions)
                compTypeMap.put(compType, directions = new ArrayList<RequestedEnrollmentDirection>());
            directions.add(direction);
        }
    }

    private MQBuilder getRequestedEnrollmentDirectionBuilder()
    {
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "r");

        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.entrantRequest().technicCommission().s(), model.getTechnicCommission().getTitle()));
        if (!model.getStudentCategoryList().isEmpty())
            builder.add(MQExpression.in("r", RequestedEnrollmentDirection.studentCategory().s(), model.getStudentCategoryList()));
        builder.addJoin("r", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().s(), "edu");
        if (!model.getQualificationList().isEmpty())
            builder.add(MQExpression.in("edu", EducationOrgUnit.educationLevelHighSchool().educationLevel().qualification().s(), model.getQualificationList()));

        builder.add(UniMQExpression.betweenDate("r", RequestedEnrollmentDirection.P_REG_DATE, model.getReport().getDateFrom(), model.getReport().getDateTo()));
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_ENROLLMENT_CAMPAIGN, model.getReport().getEnrollmentCampaign()));
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, Boolean.FALSE));

        return builder;
    }

    private String printEduInstitution(PersonEduInstitution personEduInstitution)
    {
        if (null == personEduInstitution)
            return "";
        StringBuilder result = new StringBuilder();
        EduInstitution eduInstitution = personEduInstitution.getEduInstitution();
        if (eduInstitution != null)
            result.append(eduInstitution.getTitleWithAddress());
        if (eduInstitution != null)
            result.append(", ");
        result.append(personEduInstitution.getYearEnd()).append(" г.");
        return result.toString();
    }

    private String printDocument(PersonEduInstitution personEduInstitution)
    {
        if (null == personEduInstitution)
            return "";
        StringBuilder result = new StringBuilder();
        result.append(personEduInstitution.getDocumentType().getTitle());
        if (personEduInstitution.getSeria() != null || personEduInstitution.getNumber() != null)
            result.append(", ").append(UniStringUtils.joinWithSeparator(" ", personEduInstitution.getSeria(), personEduInstitution.getNumber()));
        result.append(", ").append(personEduInstitution.getYearEnd()).append(" г.");
        return result.toString();
    }
}
