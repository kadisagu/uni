/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantAdd;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.uniec.entity.entrant.Entrant;

/**
 * @author agolubenko
 * @since 16.05.2008
 */
public class Controller extends org.tandemframework.shared.person.base.bo.Person.util.AbstractPersonRoleAdd.Controller<IDAO, Model, Entrant>
{
    @Override
    protected void startNewInstancePublisher(IBusinessComponent component, Entrant newInstance)
    {
        activateInRoot(component, new PublisherActivator(newInstance, new ParametersMap()
                .add("selectedTab", null)
                .add("selectedDataTab", null)
        ));
    }
}
