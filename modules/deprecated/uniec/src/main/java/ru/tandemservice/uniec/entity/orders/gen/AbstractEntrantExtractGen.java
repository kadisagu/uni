package ru.tandemservice.uniec.entity.orders.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.orders.AbstractEntrantExtract;
import ru.tandemservice.uniec.entity.orders.AbstractEntrantParagraph;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;
import ru.tandemservice.unimv.IAbstractDocument;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Абстрактная выписка на абитуриента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class AbstractEntrantExtractGen extends VersionedEntityBase
 implements IAbstractDocument{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.orders.AbstractEntrantExtract";
    public static final String ENTITY_NAME = "abstractEntrantExtract";
    public static final int VERSION_HASH = -246450361;
    private static IEntityMeta ENTITY_META;

    public static final String P_VERSION = "version";
    public static final String L_CONTENT = "content";
    public static final String P_CREATE_DATE = "createDate";
    public static final String P_NUMBER = "number";
    public static final String P_COMMITTED = "committed";
    public static final String L_ENTITY = "entity";
    public static final String L_STATE = "state";
    public static final String L_PARAGRAPH = "paragraph";

    private int _version; 
    private DatabaseFile _content;     // Сохраненная печатная форма
    private Date _createDate;     // Дата формирования
    private Integer _number;     // Номер выписки в параграфе
    private boolean _committed;     // Проведена
    private PreliminaryEnrollmentStudent _entity;     // Студент предварительного зачисления
    private ExtractStates _state;     // Состояние выписки
    private AbstractEntrantParagraph _paragraph;     // Абстрактный параграф на абитуриентов

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public int getVersion()
    {
        return _version;
    }

    /**
     * @param version  Свойство не может быть null.
     */
    public void setVersion(int version)
    {
        dirty(_version, version);
        _version = version;
    }

    /**
     * @return Сохраненная печатная форма.
     */
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Сохраненная печатная форма.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getCreateDate()
    {
        return _createDate;
    }

    /**
     * @param createDate Дата формирования. Свойство не может быть null.
     */
    public void setCreateDate(Date createDate)
    {
        dirty(_createDate, createDate);
        _createDate = createDate;
    }

    /**
     * @return Номер выписки в параграфе.
     */
    public Integer getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер выписки в параграфе.
     */
    public void setNumber(Integer number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Проведена. Свойство не может быть null.
     */
    @NotNull
    public boolean isCommitted()
    {
        return _committed;
    }

    /**
     * @param committed Проведена. Свойство не может быть null.
     */
    public void setCommitted(boolean committed)
    {
        dirty(_committed, committed);
        _committed = committed;
    }

    /**
     * @return Студент предварительного зачисления. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public PreliminaryEnrollmentStudent getEntity()
    {
        return _entity;
    }

    /**
     * @param entity Студент предварительного зачисления. Свойство не может быть null и должно быть уникальным.
     */
    public void setEntity(PreliminaryEnrollmentStudent entity)
    {
        dirty(_entity, entity);
        _entity = entity;
    }

    /**
     * @return Состояние выписки. Свойство не может быть null.
     */
    @NotNull
    public ExtractStates getState()
    {
        return _state;
    }

    /**
     * @param state Состояние выписки. Свойство не может быть null.
     */
    public void setState(ExtractStates state)
    {
        dirty(_state, state);
        _state = state;
    }

    /**
     * @return Абстрактный параграф на абитуриентов.
     */
    public AbstractEntrantParagraph getParagraph()
    {
        return _paragraph;
    }

    /**
     * @param paragraph Абстрактный параграф на абитуриентов.
     */
    public void setParagraph(AbstractEntrantParagraph paragraph)
    {
        dirty(_paragraph, paragraph);
        _paragraph = paragraph;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof AbstractEntrantExtractGen)
        {
            setVersion(((AbstractEntrantExtract)another).getVersion());
            setContent(((AbstractEntrantExtract)another).getContent());
            setCreateDate(((AbstractEntrantExtract)another).getCreateDate());
            setNumber(((AbstractEntrantExtract)another).getNumber());
            setCommitted(((AbstractEntrantExtract)another).isCommitted());
            setEntity(((AbstractEntrantExtract)another).getEntity());
            setState(((AbstractEntrantExtract)another).getState());
            setParagraph(((AbstractEntrantExtract)another).getParagraph());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends AbstractEntrantExtractGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) AbstractEntrantExtract.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("AbstractEntrantExtract is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "version":
                    return obj.getVersion();
                case "content":
                    return obj.getContent();
                case "createDate":
                    return obj.getCreateDate();
                case "number":
                    return obj.getNumber();
                case "committed":
                    return obj.isCommitted();
                case "entity":
                    return obj.getEntity();
                case "state":
                    return obj.getState();
                case "paragraph":
                    return obj.getParagraph();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "version":
                    obj.setVersion((Integer) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
                case "createDate":
                    obj.setCreateDate((Date) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
                case "committed":
                    obj.setCommitted((Boolean) value);
                    return;
                case "entity":
                    obj.setEntity((PreliminaryEnrollmentStudent) value);
                    return;
                case "state":
                    obj.setState((ExtractStates) value);
                    return;
                case "paragraph":
                    obj.setParagraph((AbstractEntrantParagraph) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "version":
                        return true;
                case "content":
                        return true;
                case "createDate":
                        return true;
                case "number":
                        return true;
                case "committed":
                        return true;
                case "entity":
                        return true;
                case "state":
                        return true;
                case "paragraph":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "version":
                    return true;
                case "content":
                    return true;
                case "createDate":
                    return true;
                case "number":
                    return true;
                case "committed":
                    return true;
                case "entity":
                    return true;
                case "state":
                    return true;
                case "paragraph":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "version":
                    return Integer.class;
                case "content":
                    return DatabaseFile.class;
                case "createDate":
                    return Date.class;
                case "number":
                    return Integer.class;
                case "committed":
                    return Boolean.class;
                case "entity":
                    return PreliminaryEnrollmentStudent.class;
                case "state":
                    return ExtractStates.class;
                case "paragraph":
                    return AbstractEntrantParagraph.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<AbstractEntrantExtract> _dslPath = new Path<AbstractEntrantExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "AbstractEntrantExtract");
    }
            

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.AbstractEntrantExtract#getVersion()
     */
    public static PropertyPath<Integer> version()
    {
        return _dslPath.version();
    }

    /**
     * @return Сохраненная печатная форма.
     * @see ru.tandemservice.uniec.entity.orders.AbstractEntrantExtract#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.AbstractEntrantExtract#getCreateDate()
     */
    public static PropertyPath<Date> createDate()
    {
        return _dslPath.createDate();
    }

    /**
     * @return Номер выписки в параграфе.
     * @see ru.tandemservice.uniec.entity.orders.AbstractEntrantExtract#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Проведена. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.AbstractEntrantExtract#isCommitted()
     */
    public static PropertyPath<Boolean> committed()
    {
        return _dslPath.committed();
    }

    /**
     * @return Студент предварительного зачисления. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.orders.AbstractEntrantExtract#getEntity()
     */
    public static PreliminaryEnrollmentStudent.Path<PreliminaryEnrollmentStudent> entity()
    {
        return _dslPath.entity();
    }

    /**
     * @return Состояние выписки. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.AbstractEntrantExtract#getState()
     */
    public static ExtractStates.Path<ExtractStates> state()
    {
        return _dslPath.state();
    }

    /**
     * @return Абстрактный параграф на абитуриентов.
     * @see ru.tandemservice.uniec.entity.orders.AbstractEntrantExtract#getParagraph()
     */
    public static AbstractEntrantParagraph.Path<AbstractEntrantParagraph> paragraph()
    {
        return _dslPath.paragraph();
    }

    public static class Path<E extends AbstractEntrantExtract> extends EntityPath<E>
    {
        private PropertyPath<Integer> _version;
        private DatabaseFile.Path<DatabaseFile> _content;
        private PropertyPath<Date> _createDate;
        private PropertyPath<Integer> _number;
        private PropertyPath<Boolean> _committed;
        private PreliminaryEnrollmentStudent.Path<PreliminaryEnrollmentStudent> _entity;
        private ExtractStates.Path<ExtractStates> _state;
        private AbstractEntrantParagraph.Path<AbstractEntrantParagraph> _paragraph;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.AbstractEntrantExtract#getVersion()
     */
        public PropertyPath<Integer> version()
        {
            if(_version == null )
                _version = new PropertyPath<Integer>(AbstractEntrantExtractGen.P_VERSION, this);
            return _version;
        }

    /**
     * @return Сохраненная печатная форма.
     * @see ru.tandemservice.uniec.entity.orders.AbstractEntrantExtract#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.AbstractEntrantExtract#getCreateDate()
     */
        public PropertyPath<Date> createDate()
        {
            if(_createDate == null )
                _createDate = new PropertyPath<Date>(AbstractEntrantExtractGen.P_CREATE_DATE, this);
            return _createDate;
        }

    /**
     * @return Номер выписки в параграфе.
     * @see ru.tandemservice.uniec.entity.orders.AbstractEntrantExtract#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(AbstractEntrantExtractGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Проведена. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.AbstractEntrantExtract#isCommitted()
     */
        public PropertyPath<Boolean> committed()
        {
            if(_committed == null )
                _committed = new PropertyPath<Boolean>(AbstractEntrantExtractGen.P_COMMITTED, this);
            return _committed;
        }

    /**
     * @return Студент предварительного зачисления. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.orders.AbstractEntrantExtract#getEntity()
     */
        public PreliminaryEnrollmentStudent.Path<PreliminaryEnrollmentStudent> entity()
        {
            if(_entity == null )
                _entity = new PreliminaryEnrollmentStudent.Path<PreliminaryEnrollmentStudent>(L_ENTITY, this);
            return _entity;
        }

    /**
     * @return Состояние выписки. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.AbstractEntrantExtract#getState()
     */
        public ExtractStates.Path<ExtractStates> state()
        {
            if(_state == null )
                _state = new ExtractStates.Path<ExtractStates>(L_STATE, this);
            return _state;
        }

    /**
     * @return Абстрактный параграф на абитуриентов.
     * @see ru.tandemservice.uniec.entity.orders.AbstractEntrantExtract#getParagraph()
     */
        public AbstractEntrantParagraph.Path<AbstractEntrantParagraph> paragraph()
        {
            if(_paragraph == null )
                _paragraph = new AbstractEntrantParagraph.Path<AbstractEntrantParagraph>(L_PARAGRAPH, this);
            return _paragraph;
        }

        public Class getEntityClass()
        {
            return AbstractEntrantExtract.class;
        }

        public String getEntityName()
        {
            return "abstractEntrantExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
