package ru.tandemservice.uniec.component.report.EntrantStudentCard;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.MQOrder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import org.tandemframework.shared.person.base.entity.PersonNextOfKin;
import org.tandemframework.shared.person.catalog.entity.codes.RelationDegreeCodes;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.util.EntrantDataUtil;

import java.util.*;

public class EntrantStudentCardPrintFactory
{
    private Session session;
    private MQBuilder studentBuilder;
    private EnrollmentCampaign campaign;
    private String studentAlias;
    private String personAlias;
    private String directionAlias;

    public EntrantStudentCardPrintFactory(Session session, EnrollmentCampaign campaign, MQBuilder studentBuilder, String studentAlias, String personAlias, String directionAlias)
    {
        this.session = session;
        this.studentBuilder = studentBuilder;
        this.campaign = campaign;
        this.studentAlias = studentAlias;
        this.personAlias = personAlias;
        this.directionAlias = directionAlias;
    }

    public byte[] getReportContent()
    {
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ENTRANT_STUDENT_CARD);
        RtfDocument template = new RtfReader().read(templateDocument.getCurrentTemplate());

        RtfDocument report = RtfBean.getElementFactory().createRtfDocument();
        report.setSettings(template.getSettings());
        report.setHeader(template.getHeader());

        // todo убрать этот хак, когда в платформе напишут клон к билдеру
        List<MQOrder> orders = new ArrayList<>(getStudentBuilder().getOrderList());

        Map<MultiKey, PersonNextOfKin> relativesMap = getRelativesMap();
        Map<Long, EnrollmentExtract> extractMap = getExtractMap();

        MQBuilder directionBuilder = getStudentBuilder();
        directionBuilder.getSelectAliasList().clear();
        directionBuilder.addSelect(directionAlias);
        directionBuilder.getOrderList().clear();

        EntrantDataUtil entrantDataUtil = new EntrantDataUtil(session, campaign, directionBuilder);

        MQBuilder builder = getStudentBuilder();
        builder.getSelectAliasList().clear();
        builder.addSelect(studentAlias);
        builder.getDomain(studentAlias).setSelectedProperties(null);
        builder.getOrderList().clear();
        builder.getOrderList().addAll(orders);

        for (PreliminaryEnrollmentStudent student : builder.<PreliminaryEnrollmentStudent>getResultList(session))
        {
            RtfDocument card = template.getClone();
            Entrant entrant = student.getRequestedEnrollmentDirection().getEntrantRequest().getEntrant();
            Person person = entrant.getPerson();
            TopOrgUnit academy = TopOrgUnit.getInstance();
            String academyAddress = (academy.getAddress() != null && academy.getAddress().getSettlement() != null) ? academy.getAddress().getSettlement().getTitleWithType() : "городе ОУ";

            RtfInjectModifier modifier = new RtfInjectModifier();

            modifier.put("academy", academy.getNominativeCaseTitle() == null ? academy.getTitle() : academy.getNominativeCaseTitle());
            modifier.put("vuzSettlement", academyAddress);
            OrgUnit orgUnit = student.getEducationOrgUnit().getFormativeOrgUnit();
            modifier.put("formativeOrgUnit", orgUnit.getNominativeCaseTitle() == null ? orgUnit.getTitle() : orgUnit.getNominativeCaseTitle());
            modifier.put("eduLevel", (student.getEducationOrgUnit().getEducationLevelHighSchool().isSpeciality() ? "Специальность " : "Направление ") + student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getDisplayableTitle());

            modifier.put("compensationType", student.getEnrollmentConditions());
            modifier.put("StudentEntranceYear", Integer.toString(student.getRequestedEnrollmentDirection().getEnrollmentDirection().getEnrollmentCampaign().getEducationYear().getEducationYear().getIntValue()));
            modifier.put("documentGenerationDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));
            modifier.put("person.address", person.getAddress() != null ? person.getAddress().getTitleWithFlat() : "");

            modifier.put("enrollWithSex", SexCodes.MALE.equals(person.getIdentityCard().getSex().getCode()) ? "зачислен" : "зачислена");

            modifier.put("lastName", getProperty(entrant, "person.identityCard.lastName"));
            modifier.put("firstName", getProperty(entrant, "person.identityCard.firstName"));
            modifier.put("middleName", getProperty(entrant, "person.identityCard.middleName"));
            modifier.put("citizenship", getProperty(entrant, "person.identityCard.citizenship.title"));
            modifier.put("sex", getProperty(entrant, "person.identityCard.sex.shortTitle"));
            if (person.getIdentityCard().getBirthPlace() == null)
                modifier.put("birthdate", getDateProperty(entrant, "person.identityCard.birthDate"));
            else
                modifier.put("birthdate", getDateProperty(entrant, "person.identityCard.birthDate") + ",");
            modifier.put("Birthdate", getDateProperty(entrant, "person.identityCard.birthDate"));

            modifier.put("birthplace", getProperty(entrant, "person.identityCard.birthPlace"));
            modifier.put("education", person.getPersonEduInstitution() == null ? null : formatEduInstitution(person.getPersonEduInstitution()));
            modifier.put("martialStatus", getProperty(entrant, "person.familyStatus.title"));
            modifier.put("children", person.getChildCount() == null ? "" : String.valueOf(person.getChildCount()));
            modifier.put("addressRegistration", getProperty(entrant, "person.identityCard.address.titleWithFlat"));
            if (academy.getAddress() != null && academy.getAddress().getSettlement() != null && person.getAddress() != null && person.getAddress() instanceof AddressDetailed && academy.getAddress().getSettlement().equals(((AddressDetailed)person.getAddress()).getSettlement()))
            {
                String phones = entrant.getPerson().getContactData().getPhoneFact();
                if (null == phones)
                    phones = "";
                modifier.put("addressTitle", getProperty(entrant, "person.address.titleWithFlat") + (phones.isEmpty() ? "": ", тел. "));
                modifier.put("phones", phones);
            }
            else
            {
                modifier.put("phones", "");
                modifier.put("addressTitle", "");
            }

            modifier.put("seria", getProperty(entrant, "person.identityCard.seria"));
            modifier.put("number", getProperty(entrant, "person.identityCard.number"));
            modifier.put("issueDate", getDateProperty(entrant, "person.identityCard.issuanceDate"));
            modifier.put("issuance", getProperty(entrant, "person.identityCard.issuancePlace"));
            modifier.put("contacts", getContacts(person));

            EnrollmentExtract extract = extractMap.get(student.getId());
            modifier.put("orderNumber", extract == null ? "" : extract.getOrder().getNumber());
            modifier.put("orderDate", extract == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getCreateDate()));
            modifier.put("course", extract == null ? "" : extract.getCourse().getTitle());

            double finalMark = entrantDataUtil.getFinalMark(student.getRequestedEnrollmentDirection());
            modifier.put("mark", String.valueOf(Math.round(finalMark)));

            appendNextOfKinData(modifier, relativesMap.get(new MultiKey(person.getId(), RelationDegreeCodes.FATHER)), "father");
            appendNextOfKinData(modifier, relativesMap.get(new MultiKey(person.getId(), RelationDegreeCodes.MOTHER)), "mother");

            modifier.put("post", getProperty(person, Person.P_WORK_PLACE_POSITION));
            modifier.put("employmentPlace", getProperty(person, Person.P_WORK_PLACE));
            modifier.put("developForm", student.getEducationOrgUnit().getDevelopForm().getTitle());

            modifier.modify(card);
            report.getElementList().addAll(card.getElementList());
        }

        return RtfUtil.toByteArray(report);
    }

    private Map<MultiKey, PersonNextOfKin> getRelativesMap()
    {
        Map<MultiKey, PersonNextOfKin> result = new HashMap<>();
        MQBuilder builder = new MQBuilder(PersonNextOfKin.ENTITY_CLASS, "e");
        MQBuilder inner = getStudentBuilder();
        inner.getOrderList().clear();
        inner.getSelectAliasList().clear();
        inner.addSelect(personAlias, new Object[]{"id"});
        builder.add(MQExpression.in("e", PersonNextOfKin.L_PERSON + ".id", inner));
        for (PersonNextOfKin pnk : builder.<PersonNextOfKin>getResultList(session))
            result.put(new MultiKey(pnk.getPerson().getId(), pnk.getRelationDegree().getCode()), pnk);
        return result;
    }

    private String formatEduInstitution(PersonEduInstitution personEduInstitution)
    {
        StringBuilder result = new StringBuilder();
        if (personEduInstitution.getEduInstitutionKind() != null)
            result.append(personEduInstitution.getEduInstitutionKind().getShortTitle());
        if (personEduInstitution.getEduInstitution() != null)
            result.append(" ").append(personEduInstitution.getEduInstitution().getTitle());
        result.append(", ");
        if (personEduInstitution.getAddressItem() != null)
            result.append(personEduInstitution.getAddressItem().getTitleWithType()).append(", ");
        result.append(personEduInstitution.getDocumentType().getTitle());
        if (personEduInstitution.getSeria() != null)
            result.append(": ").append(personEduInstitution.getSeria());
        if (personEduInstitution.getNumber() != null)
            result.append(" №").append(personEduInstitution.getNumber());
        result.append(", ").append(personEduInstitution.getYearEnd()).append(" г.");
        return result.toString();
    }

    private void appendNextOfKinData(RtfInjectModifier modifier, PersonNextOfKin relative, String prefix)
    {
        if (relative == null)
        {
            modifier
                    .put(prefix + "Fio", "")
                    .put(prefix + "Birthdate", "")
                    .put(prefix + "Workplace", "")
                    .put(prefix + "Address", "");
            return;
        }

        modifier.put(prefix + "Fio", relative.getBirthDate() == null ? relative.getFullFio() : relative.getFullFio() + ",");
        modifier.put(prefix + "Birthdate", relative.getBirthDate() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(relative.getBirthDate()));
        {
            StringBuilder workplace = new StringBuilder();
            if (relative.getEmploymentPlace() != null)
                workplace.append(relative.getEmploymentPlace());
            if (relative.getPost() != null)
                workplace.append(", ").append(relative.getPost());
            modifier.put(prefix + "Workplace", workplace.toString());
        }
        {
            StringBuilder address = new StringBuilder();
            if (relative.getAddress() != null)
                address.append(relative.getAddress().getTitleWithFlat());
            if (relative.getPhones() != null && relative.getAddress() != null)
                address.append(", тел. ");
            if (relative.getPhones() != null)
                address.append(relative.getPhones());
            modifier.put(prefix + "Address", address.toString());
        }
    }

    protected String getContacts(Person person)
    {
        List<String> result = new ArrayList<>();
        if (person.getContactData().getPhoneFact() != null)
            result.add(person.getContactData().getPhoneFact());
        if (person.getContactData().getPhoneMobile() != null)
            result.add(person.getContactData().getPhoneMobile());
        if (!result.isEmpty())
            result.set(0, "тел. " + result.get(0));
        if (person.getContactData().getEmail() != null)
            result.add("email " + person.getContactData().getEmail());
        if (person.getContactData().getOther() != null)
            result.add("другие контактные данные " + person.getContactData().getOther());
        return StringUtils.join(result, ", ");
    }

    protected String getDateProperty(IEntity student, String path)
    {
        Date value = (Date) student.getProperty(path);
        return value != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(value) : "";
    }

    protected static String getProperty(IEntity student, String path)
    {
        String value = (String) student.getProperty(path);
        return value != null ? value : "";
    }

    private Map<Long, EnrollmentExtract> getExtractMap()
    {
        Map<Long, EnrollmentExtract> result = new HashMap<>();
        MQBuilder builder = new MQBuilder(EnrollmentExtract.ENTITY_CLASS, "e");
        MQBuilder inner = getStudentBuilder();
        inner.getOrderList().clear();
        inner.getSelectAliasList().clear();
        inner.addSelect(studentAlias, new Object[]{"id"});
        builder.add(MQExpression.in("e", "entity.id", inner));
        for (EnrollmentExtract e : builder.<EnrollmentExtract>getResultList(session))
            result.put(e.getEntity().getId(), e);
        return result;
    }

    public MQBuilder getStudentBuilder()
    {
        return studentBuilder;
    }
}