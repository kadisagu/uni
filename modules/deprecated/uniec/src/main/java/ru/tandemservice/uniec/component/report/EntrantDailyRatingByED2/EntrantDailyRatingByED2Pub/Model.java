/* $Id$ */
package ru.tandemservice.uniec.component.report.EntrantDailyRatingByED2.EntrantDailyRatingByED2Pub;

import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;

import ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report;

/**
 * @author Andrey Andreev
 * @since 16.06.2016
 */
@State(keys = PublisherActivator.PUBLISHER_ID_KEY, bindings = "report.id")
public class Model
{
    private EntrantDailyRatingByED2Report _report = new EntrantDailyRatingByED2Report();

    public EntrantDailyRatingByED2Report getReport()
    {
        return _report;
    }

    public void setReport(EntrantDailyRatingByED2Report report)
    {
        _report = report;
    }
}
