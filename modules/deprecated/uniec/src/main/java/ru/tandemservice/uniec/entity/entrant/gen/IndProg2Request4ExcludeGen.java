package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.entrant.EntrantIndividualProgress;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.IndProg2Request4Exclude;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь индивидуального достижения и заявления абитуриента для исключения из учета баллов
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class IndProg2Request4ExcludeGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.IndProg2Request4Exclude";
    public static final String ENTITY_NAME = "indProg2Request4Exclude";
    public static final int VERSION_HASH = -404592747;
    private static IEntityMeta ENTITY_META;

    public static final String L_INDIVIDUAL_PROGRESS = "individualProgress";
    public static final String L_REQUEST = "request";

    private EntrantIndividualProgress _individualProgress;     // Индивидуальное достижение абитуриента
    private EntrantRequest _request;     // Заявление абитуриента

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Индивидуальное достижение абитуриента. Свойство не может быть null.
     */
    @NotNull
    public EntrantIndividualProgress getIndividualProgress()
    {
        return _individualProgress;
    }

    /**
     * @param individualProgress Индивидуальное достижение абитуриента. Свойство не может быть null.
     */
    public void setIndividualProgress(EntrantIndividualProgress individualProgress)
    {
        dirty(_individualProgress, individualProgress);
        _individualProgress = individualProgress;
    }

    /**
     * @return Заявление абитуриента. Свойство не может быть null.
     */
    @NotNull
    public EntrantRequest getRequest()
    {
        return _request;
    }

    /**
     * @param request Заявление абитуриента. Свойство не может быть null.
     */
    public void setRequest(EntrantRequest request)
    {
        dirty(_request, request);
        _request = request;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof IndProg2Request4ExcludeGen)
        {
            setIndividualProgress(((IndProg2Request4Exclude)another).getIndividualProgress());
            setRequest(((IndProg2Request4Exclude)another).getRequest());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends IndProg2Request4ExcludeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) IndProg2Request4Exclude.class;
        }

        public T newInstance()
        {
            return (T) new IndProg2Request4Exclude();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "individualProgress":
                    return obj.getIndividualProgress();
                case "request":
                    return obj.getRequest();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "individualProgress":
                    obj.setIndividualProgress((EntrantIndividualProgress) value);
                    return;
                case "request":
                    obj.setRequest((EntrantRequest) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "individualProgress":
                        return true;
                case "request":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "individualProgress":
                    return true;
                case "request":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "individualProgress":
                    return EntrantIndividualProgress.class;
                case "request":
                    return EntrantRequest.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<IndProg2Request4Exclude> _dslPath = new Path<IndProg2Request4Exclude>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "IndProg2Request4Exclude");
    }
            

    /**
     * @return Индивидуальное достижение абитуриента. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.IndProg2Request4Exclude#getIndividualProgress()
     */
    public static EntrantIndividualProgress.Path<EntrantIndividualProgress> individualProgress()
    {
        return _dslPath.individualProgress();
    }

    /**
     * @return Заявление абитуриента. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.IndProg2Request4Exclude#getRequest()
     */
    public static EntrantRequest.Path<EntrantRequest> request()
    {
        return _dslPath.request();
    }

    public static class Path<E extends IndProg2Request4Exclude> extends EntityPath<E>
    {
        private EntrantIndividualProgress.Path<EntrantIndividualProgress> _individualProgress;
        private EntrantRequest.Path<EntrantRequest> _request;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Индивидуальное достижение абитуриента. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.IndProg2Request4Exclude#getIndividualProgress()
     */
        public EntrantIndividualProgress.Path<EntrantIndividualProgress> individualProgress()
        {
            if(_individualProgress == null )
                _individualProgress = new EntrantIndividualProgress.Path<EntrantIndividualProgress>(L_INDIVIDUAL_PROGRESS, this);
            return _individualProgress;
        }

    /**
     * @return Заявление абитуриента. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.IndProg2Request4Exclude#getRequest()
     */
        public EntrantRequest.Path<EntrantRequest> request()
        {
            if(_request == null )
                _request = new EntrantRequest.Path<EntrantRequest>(L_REQUEST, this);
            return _request;
        }

        public Class getEntityClass()
        {
            return IndProg2Request4Exclude.class;
        }

        public String getEntityName()
        {
            return "indProg2Request4Exclude";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
