/* $Id$ */
package ru.tandemservice.uniec.component.report.AdmittedEntrantsDataExport.Add;

import org.tandemframework.core.settings.IDataSettings;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.Date;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: azhebko
 * Date: 24.07.12
 * Time: 15:53
 * To change this template use File | Settings | File Templates.
 */
public class Model implements IEnrollmentCampaignSelectModel
{
    private List<EnrollmentCampaign> _enrollmentCampaignList;
    private EnrollmentCampaign _enrollmentCampaign;

    private Date _dateFrom;
    private Date _dateTo;

    private boolean _dateFromActive;
    private boolean _dateToActive;

    // IEnrollmentCampaignSelectModel

    @Override
    public IDataSettings getSettings()
    {
        return null;
    }

    // Getters and Setters

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public Date getDateFrom()
    {
        return _dateFrom;
    }

    public void setDateFrom(Date dateFrom)
    {
        _dateFrom = dateFrom;
    }

    public Date getDateTo()
    {
        return _dateTo;
    }

    public void setDateTo(Date dateTo)
    {
        _dateTo = dateTo;
    }

    public boolean isDateFromActive()
    {
        return _dateFromActive;
    }

    public void setDateFromActive(boolean dateFromActive)
    {
        _dateFromActive = dateFromActive;
    }

    public boolean isDateToActive()
    {
        return _dateToActive;
    }

    public void setDateToActive(boolean dateToActive)
    {
        _dateToActive = dateToActive;
    }
}
