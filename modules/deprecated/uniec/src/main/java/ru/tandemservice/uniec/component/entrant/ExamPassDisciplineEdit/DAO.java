/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.ExamPassDisciplineEdit;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.entrant.*;


import java.util.List;

/**
 * @author vip_delete
 * @since 23.03.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        ExamPassDiscipline passDiscipline = getNotNull(ExamPassDiscipline.class, model.getExamPassDiscipline().getId());
        model.setExamPassDiscipline(passDiscipline);

        List<EntranceExamPhase> examPhases = EntranceExamPhase.getSelectBuilder4ExamPassDiscipline(passDiscipline).createStatement(getSession()).list();

        model.setExamPhaseList(examPhases);

        if(passDiscipline.getPassDate()!=null)
            model.setExamPhase(model.getExamPhaseList().stream().filter(phase -> passDiscipline.getPassDate().getTime() == phase.getPassDate().getTime())
                                       .findAny().orElse(null));


        if (examPhases.size() == 1 && passDiscipline.getPassDate() == null)
        {
            EntranceExamPhase phase = examPhases.get(0);
            passDiscipline.setPassDate(phase.getPassDate());
            model.setExamPhase(phase);
        }
    }

    @Override
    public void update(Model model)
    {
        update(model.getExamPassDiscipline());
    }
}
