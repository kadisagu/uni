/* $Id$ */
package ru.tandemservice.uniec.component.listextract.ent2.e0.ListOrderPub;

import ru.tandemservice.movestudent.component.listextract.abstractorder.ListOrderPub.AbstractListOrderPubModel;
import ru.tandemservice.movestudent.entity.StudentListOrder;

/**
 * @author Dmitry Seleznev
 * @since 21.08.2012
 */
public class Model extends AbstractListOrderPubModel<StudentListOrder>
{
    public static final String EDU_ORGUNIT_EXTENDED = "eduOrgUnitExtended";
}