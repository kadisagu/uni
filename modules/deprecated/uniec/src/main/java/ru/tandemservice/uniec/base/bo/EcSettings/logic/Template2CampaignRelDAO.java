/* $Id: $ */
package ru.tandemservice.uniec.base.bo.EcSettings.logic;

import org.tandemframework.shared.survey.base.entity.SurveyTemplate;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.survey.SurveyTemplate2EnrCampaignRel;

/**
 * @author Andrey Andreev
 * @since 21.03.2017
 */
public class Template2CampaignRelDAO extends UniBaseDao implements ITemplate2CampaignRelDAO
{

    @Override
    public void setSurveyTemplate2EnrCampaignRel(Long campaignId, SurveyTemplate template)
    {
        lock("surveyTemplate2CampaignRel." + campaignId);

        EnrollmentCampaign campaign = getNotNull(campaignId);
        SurveyTemplate2EnrCampaignRel rel = get(SurveyTemplate2EnrCampaignRel.class, SurveyTemplate2EnrCampaignRel.campaign().s(), campaign);
        if (rel == null && template != null)
        {
            save(new SurveyTemplate2EnrCampaignRel(campaign, template));
            return;
        }

        if (rel != null)
        {
            if (template == null)
            {
                delete(rel);
            }
            else if (!template.equals(rel.getTemplate()))
            {
                rel.setTemplate(template);
                saveOrUpdate(rel);
            }
        }
    }
}
