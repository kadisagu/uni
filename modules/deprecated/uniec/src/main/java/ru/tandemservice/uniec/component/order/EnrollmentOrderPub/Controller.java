/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.order.EnrollmentOrderPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDefine;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.IUniecComponents;
import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager;
import ru.tandemservice.uniec.base.bo.EcOrder.ui.RevertPub.EcOrderRevertPub;
import ru.tandemservice.uniec.base.bo.EcOrder.util.BaseEcOrderParAddEditUI;
import ru.tandemservice.uniec.base.bo.EcOrder.util.EcOrderUtil;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniec.entity.orders.EnrollmentParagraph;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.util.List;

/**
 * @author vip_delete
 * @since 08.04.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        if (!model.getOrder().isRevert())
        {
            prepareListDataSource(component);
        }
        else
        {
            deactivate(component);
            activateInRoot(component, new ComponentActivator(EcOrderRevertPub.class.getSimpleName(), new ParametersMap().add(UIPresenter.PUBLISHER_ID, model.getOrder().getId())));
        }
    }

    @SuppressWarnings({"unchecked"})
    private void prepareListDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource dataSource = UniBaseUtils.createDataSource(component, getDao());

        dataSource.addColumn(new PublisherLinkColumn("Название", EnrollmentParagraph.P_TITLE).setResolver(
                new DefaultPublisherLinkResolver()
                {
                    @Override
                    public String getComponentName(IEntity entity)
                    {
                        return EcOrderUtil.getParagraphPubComponent(((EnrollmentOrder) entity.getProperty(IAbstractParagraph.L_ORDER)).getType());
                    }
                }
        ).setFormatter(NoWrapFormatter.INSTANCE).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", Model.EDUCATION_ORG_UNIT_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Формирующее подр.", Model.FORMATIVE_ORG_UNIT_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Территориальное подр.", Model.TERRITORIAL_ORG_UNIT_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Форма освоения", Model.DEVELOP_FORM_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Условие освоения", Model.DEVELOP_CONDITION_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Технология освоения", Model.DEVELOP_TECH_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Срок освоения", Model.DEVELOP_PERIOD_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Курс", Model.COURSE_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Кол-во абитуриентов", Model.EXTRACT_COUNT).setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Повысить приоритет", CommonBaseDefine.ICO_UP, "onClickUp").setPermissionKey(model.getSecModel().getPermission("editParagraph")).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Понизить приоритет", CommonBaseDefine.ICO_DOWN, "onClickDown").setPermissionKey(model.getSecModel().getPermission("editParagraph")).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditParagraph").setPermissionKey(model.getSecModel().getPermission("editParagraph")).setDisabledProperty(EnrollmentParagraph.P_NO_EDIT));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteParagraph", "Удалить «{0}»?", IAbstractOrder.P_TITLE).setPermissionKey(model.getSecModel().getPermission("deleteParagraph")).setDisabledProperty(EnrollmentParagraph.P_NO_EDIT));

        model.setDataSource(dataSource);
    }

    // Event Listeners

    public void onClickSendToCoordination(IBusinessComponent component)
    {
        getDao().doSendToCoordination(getModel(component), component.getUserContext().getPrincipalContext());
        getDao().prepare(getModel(component));
    }

    @SuppressWarnings("unchecked")
    public void onClickSendOrderToCoordinationExtended(IBusinessComponent component)
    {
        for (IEnrollmentOrderPubControllerExtension ext : (List<IEnrollmentOrderPubControllerExtension>) ApplicationRuntime.getBean(IEnrollmentOrderPubControllerExtension.ENROLLMENT_ORDER_PUB_CONTROLLER_EXTENSIONS_BEAN_NAME))
        {
            ext.doSendToCoordinationAdditionalAction(component, getModel(component).getOrder());
        }
        getDao().prepare(getModel(component));
    }

    @SuppressWarnings("unchecked")
    public void onClickExecuteAdditionalAction(IBusinessComponent component)
    {
        for (IEnrollmentOrderPubControllerExtension ext : (List<IEnrollmentOrderPubControllerExtension>) ApplicationRuntime.getBean(IEnrollmentOrderPubControllerExtension.ENROLLMENT_ORDER_PUB_CONTROLLER_EXTENSIONS_BEAN_NAME))
        {
            ext.doExecuteAdditionalAction(component, getModel(component).getOrder());
        }
    }

    public void onClickEdit(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniecComponents.ENROLLMENT_ORDER_ADD_EDIT, new ParametersMap()
                .add("orderId", getModel(component).getOrder().getId())
        ));
    }

    public void onClickSetExecutor(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniecComponents.ENROLLMENT_ORDER_SET_EXECUTOR, new ParametersMap()
                .add("orderId", getModel(component).getOrder().getId())
        ));
    }

    public void onClickAddParagraph(IBusinessComponent component)
    {
        EnrollmentOrder order = getModel(component).getOrder();
        component.createDefaultChildRegion(new ComponentActivator(EcOrderUtil.getParagraphAddEditComponent(order.getType()), new ParametersMap()
                .add(BaseEcOrderParAddEditUI.PARAMETER_ORDER_ID, order.getId())
        ));
    }

    public void onClickEditParagraph(IBusinessComponent component)
    {
        EnrollmentOrder order = getModel(component).getOrder();
        component.createDefaultChildRegion(new ComponentActivator(EcOrderUtil.getParagraphAddEditComponent(order.getType()), new ParametersMap()
                .add(BaseEcOrderParAddEditUI.PARAMETER_PARAGRAPH_ID, component.<Long>getListenerParameter())
        ));
    }

    public void onClickReject(IBusinessComponent component)
    {
        getDao().doReject(getModel(component));
        getDao().prepare(getModel(component));
    }

    public void onClickCommit(IBusinessComponent component)
    {
        getDao().doCommit(getModel(component));
        getDao().prepare(getModel(component));
    }

    public void onClickSendToFormative(IBusinessComponent component)
    {
        getDao().doSendToFormative(getModel(component));
        getDao().prepare(getModel(component));
    }

    public void onClickRollback(IBusinessComponent component)
    {
        getDao().doRollback(getModel(component));
        getDao().prepare(getModel(component));
    }

    public void onClickDeleteOrder(IBusinessComponent component)
    {
        deactivate(component);
        EcOrderManager.instance().dao().deleteOrder(getModel(component).getOrder());
    }

    public void onClickPrintOrder(IBusinessComponent component)
    {
        EcOrderManager.instance().dao().getDownloadPrintForm(getModel(component).getOrder().getId(), false);
    }

    public void onClickPrintOrderPdf(IBusinessComponent component)
    {
        EcOrderManager.instance().dao().getDownloadPrintForm(getModel(component).getOrder().getId(), true);
    }

    public void onClickPrintExtracts(IBusinessComponent component)
    {
        EcOrderManager.instance().dao().getMassExtractPrintForms(getModel(component).getOrder().getId());
    }

    public void onClickPrintExamSheets(IBusinessComponent component)
    {
        EcOrderManager.instance().dao().getMassExamSheetPrintForms(getModel(component).getOrder().getId());
    }

    public void onClickDeleteParagraph(IBusinessComponent component)
    {
        getDao().deleteParagraph(getModel(component), (Long) component.getListenerParameter());
    }

    public void onClickUp(IBusinessComponent component)
    {
        getDao().updatePriority((Long) component.getListenerParameter(), -1);
    }

    public void onClickDown(IBusinessComponent component)
    {
        getDao().updatePriority((Long) component.getListenerParameter(), 1);
    }
}
