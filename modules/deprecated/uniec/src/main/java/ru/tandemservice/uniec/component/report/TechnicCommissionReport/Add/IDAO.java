/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.report.TechnicCommissionReport.Add;

import java.util.List;

import org.tandemframework.core.entity.IdentifiableWrapper;

import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.uniec.entity.entrant.CompetitionGroup;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

/**
 * @author agolubenko
 * @since 13.07.2009
 */
public interface IDAO extends IUniDao<Model>
{
    /**
     * @param enrollmentCampaign приемная кампания
     * @return конкурсные группы данной приемной кампании
     */
    List<CompetitionGroup> getCompetitionGroups(EnrollmentCampaign enrollmentCampaign);

    /**
     * @param enrollmentCampaign приемная кампания
     * @return технические комиссии, указанные в заявлениях данной приемной кампании
     */
    List<IdentifiableWrapper<?>> getTechnicCommissions(EnrollmentCampaign enrollmentCampaign);
}
