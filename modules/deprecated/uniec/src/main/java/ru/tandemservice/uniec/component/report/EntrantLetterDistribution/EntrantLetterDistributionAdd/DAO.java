/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EntrantLetterDistribution.EntrantLetterDistributionAdd;

import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.utils.CommonYesNoUIObject;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.entity.report.EntrantLetterDistributionReport;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

/**
 * @author oleyba
 * @since 06.07.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    @SuppressWarnings("deprecation")
    public void prepare(final Model model)
    {
        model.setEnrollmentCampaignStageList(Arrays.asList(
                new IdentifiableWrapper(Model.ENROLLMENT_CAMP_STAGE_DOCUMENTS, "по ходу приема документов"),
                new IdentifiableWrapper(Model.ENROLLMENT_CAMP_STAGE_EXAMS, "по результатам сдачи вступительных испытаний"),
                new IdentifiableWrapper(Model.ENROLLMENT_CAMP_STAGE_ENROLLMENT, "по результатам зачисления")));

        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.getReport().setDateFrom(model.getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : model.getEnrollmentCampaign().getStartDate());
        model.getReport().setDateTo(CoreDateUtils.getDayLastTimeMoment(new Date()));

        model.setQualificationListModel(new QualificationModel(getSession()));
        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));
        model.setStudentCategoryListModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(StudentCategory.class)));

        model.setFormativeOrgUnitListModel(MultiEnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setTerritorialOrgUnitListModel(MultiEnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setDevelopFormListModel(MultiEnrollmentDirectionUtil.createDevelopFormModel(model));
        model.setDevelopConditionListModel(MultiEnrollmentDirectionUtil.createDevelopConditionModel(model));
        model.setDevelopTechListModel(MultiEnrollmentDirectionUtil.createDevelopTechModel(model));
        model.setDevelopPeriodListModel(MultiEnrollmentDirectionUtil.createDevelopPeriodModel(model));

        model.setFormativeOrgUnitList(new ArrayList<>());
        model.setTerritorialOrgUnitList(new ArrayList<>());
        model.setDevelopFormList(new ArrayList<>());
        model.setDevelopConditionList(new ArrayList<>());
        model.setDevelopTechList(new ArrayList<>());
        model.setDevelopPeriodList(new ArrayList<>());

        model.setParallelList(CommonYesNoUIObject.createYesNoList());

        model.setParameters(new MultiEnrollmentDirectionUtil.Parameters());
    }

    @Override
    public void update(Model model)
    {
        final Session session = getSession();

        EntrantLetterDistributionReport report = model.getReport();
        report.setEnrollmentCampStage(model.getEnrollmentCampaignStage().getTitle());
        report.setFormingDate(new Date());
        if (model.isStudentCategoryActive())
            report.setStudentCategoryTitle(UniStringUtils.join(model.getStudentCategoryList(), "title", "; "));
        if (model.isQualificationActive())
            report.setQualificationTitle(UniStringUtils.join(model.getQualificationList(), "title", "; "));
        if (model.isCompensationTypeActive())
            report.setCompensationType(model.getCompensationType());
        if (model.isFormativeOrgUnitActive())
            report.setFormativeOrgUnitTitle(UniStringUtils.join(model.getFormativeOrgUnitList(), "fullTitle", "; "));
        if (model.isTerritorialOrgUnitActive())
            report.setTerritorialOrgUnitTitle(UniStringUtils.join(model.getTerritorialOrgUnitList(), "fullTitle", "; "));
        if (model.isDevelopFormActive())
            report.setDevelopFormTitle(UniStringUtils.join(model.getDevelopFormList(), "title", "; "));
        if (model.isDevelopConditionActive())
            report.setDevelopConditionTitle(UniStringUtils.join(model.getDevelopConditionList(), "title", "; "));
        if (model.isDevelopTechActive())
            report.setDevelopTechTitle(UniStringUtils.join(model.getDevelopTechList(), "title", "; "));
        if (model.isDevelopPeriodActive())
            report.setDevelopPeriodTitle(UniStringUtils.join(model.getDevelopPeriodList(), "title", "; "));
        if (model.isParallelActive())
            report.setExcludeParallel(model.getParallel().isTrue());

        DatabaseFile databaseFile = new EntrantLetterDistributionReportBuilder(model, session).getContent();
        session.save(databaseFile);

        report.setContent(databaseFile);
        session.save(report);
    }
}
