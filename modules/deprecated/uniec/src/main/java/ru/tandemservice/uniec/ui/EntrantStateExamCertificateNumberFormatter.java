/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.ui;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.IFormatter;

/**
 * @author agolubenko
 * @since 06.03.2009
 */
public class EntrantStateExamCertificateNumberFormatter implements IFormatter<String>
{
    /**
     * @param source номер свидетельства
     * @return номер свидетельства ЕГЭ в формате <Код территории>-<Номер>-<Год>
     */
    @Override
    public String format(String source)
    {
        return formatStatic(source);
    }

    public static String formatStatic(String source)
    {
        return StringUtils.join(split(source), "-");
    }

    /**
     * @param source номер свидетельства
     * @return номер свидетельства ЕГЭ разбитый на части [Код территории, Номер, Год]
     */
    public static String[] split(String source)
    {
        return new String[] { source.substring(0, 2), source.substring(2, 11), source.substring(11) };
    }
}
