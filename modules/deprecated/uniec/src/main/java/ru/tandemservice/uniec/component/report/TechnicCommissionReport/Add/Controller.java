/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.report.TechnicCommissionReport.Add;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.tapsupport.component.selection.StaticSelectModel;

import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

/**
 * @author agolubenko
 * @since 13.07.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().update(model);
        deactivate(component);
        activateInRoot(component, new PublisherActivator(model.getReport()));
    }

    public void onRefresh(IBusinessComponent component)
    {
        Model model = getModel(component);
        EnrollmentCampaign enrollmentCampaign = model.getEnrollmentCampaign();

        model.setCompetitionGroupList(getDao().getCompetitionGroups(enrollmentCampaign));
        model.setTechnicCommissionModel(new StaticSelectModel(IdentifiableWrapper.P_ID, new String[] { "title" }, getDao().getTechnicCommissions(enrollmentCampaign)));
    }
}
