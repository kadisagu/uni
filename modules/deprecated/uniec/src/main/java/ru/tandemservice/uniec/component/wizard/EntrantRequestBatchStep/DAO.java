/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.wizard.EntrantRequestBatchStep;

import java.util.List;

import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineRequestedEnrollmentDirection;

/**
 * @author vip_delete
 * @since 18.06.2009
 */
public class DAO extends ru.tandemservice.uniec.component.entrant.EntrantRequestBatchAddEdit.DAO implements IDAO
{
    @Override
    public void prepare(ru.tandemservice.uniec.component.entrant.EntrantRequestBatchAddEdit.Model m)
    {
        super.prepare(m);

        Model model = (Model) m;

        // если есть онлайн-абитуриент
        Long onlineEntrantId = model.getOnlineEntrant().getId();
        if (onlineEntrantId != null)
        {
            OnlineEntrant onlineEntrant = get(OnlineEntrant.class, onlineEntrantId);
            model.setOnlineEntrant(onlineEntrant);

            prepareOnlineEntrantRequestedEnrollmentDirections(model);
        }
    }

    @SuppressWarnings("unchecked")
    private void prepareOnlineEntrantRequestedEnrollmentDirections(Model model)
    {
        // ошибки выбора проигнорируем, но объект нужен
        ErrorCollector errors = new ErrorCollector();

        // для всех выбранных им направлений
        for (OnlineRequestedEnrollmentDirection onlineRequestedEnrollmentDirection : UniecDAOFacade.getOnlineEntrantRegistrationDAO().getRequestedEnrollmentDirections(model.getOnlineEntrant()))
        {
            EnrollmentDirection enrollmentDirection = onlineRequestedEnrollmentDirection.getEnrollmentDirection();
            EducationOrgUnit educationOrgUnit = enrollmentDirection.getEducationOrgUnit();

            // проставляем их параметры в модель
            model.setCompensationType(onlineRequestedEnrollmentDirection.getCompensationType());
            model.setCompetitionKind(onlineRequestedEnrollmentDirection.getCompetitionKind());
            model.setStudentCategory(onlineRequestedEnrollmentDirection.getStudentCategory());
            model.setTargetAdmission(onlineRequestedEnrollmentDirection.isTargetAdmission());

            model.setEducationLevelsHighSchool(educationOrgUnit.getEducationLevelHighSchool());
            model.setFormativeOrgUnit(educationOrgUnit.getFormativeOrgUnit());
            model.setTerritorialOrgUnit(educationOrgUnit.getTerritorialOrgUnit());
            model.setDevelopForm(educationOrgUnit.getDevelopForm());

            // инициализируем dataSource
            DynamicListDataSource<EnrollmentDirection> dataSource = model.getPossibleDirectionDataSource();
            dataSource.refresh();
            List<EnrollmentDirection> directions = dataSource.getEntityList();

            // выбираем в нем все направления
            BlockColumn<Boolean> checkboxColumn = (BlockColumn<Boolean>) dataSource.getColumn("checkbox");
            for (EnrollmentDirection direction : directions)
            {
                checkboxColumn.getValueMap().put(direction.getId(), true);
            }

            // эмулируем нажатие кнопки "выбрать"
            prepareSelectedEnrollmentDirectionList(model, errors);
        }
        
        // сбрасываем значения на форме
        reset(model);
    }
}
