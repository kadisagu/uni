/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantPub;

import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.uniec.entity.entrant.EntrantIndividualProgress;

/**
 * @author agolubenko
 * @since 16.05.2008
 */
public interface IDAO extends IUniDao<Model>
{
    void prepareOlympiadDiplomaDataSource(Model model);

    void prepareEntrantStateExamCertificatesDataSource(Model model);
    
    void preparePreliminaryEnrollmentDataSource(Model model);

    byte[] printStudentCard(Long studentId);

    void prepareIndividualProgressDataSource(Model model);

    /**
     * @param achievement Индивидуальное достижение абитуриента
     * @return Номера заявлений абитуриента в которых учитывается индивидуальное достижение через запятую
     */
    String getRequests4IndividualProgress(EntrantIndividualProgress achievement);
}
