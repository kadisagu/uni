/**
 *$Id:$
 */
package ru.tandemservice.uniec.base.bo.EcReport.ui.SummaryStateExamMarkList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import ru.tandemservice.uniec.base.bo.EcCampaign.EcCampaignManager;
import ru.tandemservice.uniec.base.bo.EcReport.EcReportManager;
import ru.tandemservice.uniec.base.bo.EcReport.logic.SummaryStateExamMarkReport.SummaryStateExamMarkReportListDSHandler;
import ru.tandemservice.uniec.base.bo.EcReport.ui.SummaryStateExamMarkPub.EcReportSummaryStateExamMarkPub;
import ru.tandemservice.uniec.entity.report.SummaryStateExamMarkReport;

/**
 * @author Alexander Shaburov
 * @since 30.07.12
 */
@Configuration
public class EcReportSummaryStateExamMarkList extends BusinessComponentManager
{
    // dataSource
    public static String ENROLLMENT_CAMPAIGN_DS = "enrollmentCampaignDS";
    public static String REPORT_LIST_DS = "reportListDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ENROLLMENT_CAMPAIGN_DS, EcCampaignManager.instance().enrollmentCampaignComboDSHandler()))
                .addDataSource(searchListDS(REPORT_LIST_DS, reportListColumnExtPoint(), EcReportManager.instance().summaryStateExamMarkReportListDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint reportListColumnExtPoint()
    {
        return columnListExtPointBuilder(REPORT_LIST_DS)
                .addColumn(indicatorColumn("ico").defaultIndicatorItem(new IndicatorColumn.Item("report", "Отчет")))
                .addColumn(publisherColumn("formingDate", SummaryStateExamMarkReport.formingDate()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).businessComponent(EcReportSummaryStateExamMarkPub.class).order())
                .addColumn(textColumn("fromTo", SummaryStateExamMarkReportListDSHandler.FROM_TO_PROPERTY))
                .addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onPrintReport").permissionKey("printUniecStorableReport"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER, alert("reportListDSHandler.delete.alert", SummaryStateExamMarkReport.formingDate().s(), DateFormatter.DATE_FORMATTER_WITH_TIME)).permissionKey("deleteUniecStorableReport"))
                .create();
    }
}
