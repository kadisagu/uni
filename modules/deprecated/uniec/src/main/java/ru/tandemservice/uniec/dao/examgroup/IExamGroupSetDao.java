/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.dao.examgroup;

import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * объект для работы с экзаменационными группами
 *
 * @author Vasily Zhukov
 * @since 27.05.2010
 */
public interface IExamGroupSetDao
{
    final SpringBeanCache<IExamGroupSetDao> instance = new SpringBeanCache<IExamGroupSetDao>(IExamGroupSetDao.class.getName());

    /**
     * Получает текущую логику работы экзаменационных групп
     *
     * @param enrollmentCampaign приемная кампания
     * @return текущая логика (DAO) работы экзаменационных групп
     */
    IAbstractExamGroupLogic getCurrentExamGroupLogic(EnrollmentCampaign enrollmentCampaign);

    /**
     * Проверяет можно ли изменять алгоритм формирования экзам. групп
     * в указанной приемной кампании
     *
     * @param enrollmentCampaign приемная кампания
     * @return true, если можно изменить алгоритм формирования экзам. групп
     */
    boolean isCanChangeExamGroupLogic(EnrollmentCampaign enrollmentCampaign);

    /**
     * открывает указанный набор экзаменационных групп и закрывает другие
     *
     * @param examGroupSet набор экзаменационных групп
     */
    void openExamGroupSet(ExamGroupSet examGroupSet);

    /**
     * закрывает указанный набор экзаменационных групп
     *
     * @param examGroupSet набор экзаменационных групп
     */
    void closeExamGroupSet(ExamGroupSet examGroupSet);

    /**
     * Находит подходящие экзаменационные группы для заявления абитуриента
     *
     * @param entrantRequest заявление абитуриента
     * @return экзаменационные группы (всегда не null). Пустой список, если никуда включить нельзя.
     */
    List<ExamGroup> getPossibleExamGroups(EntrantRequest entrantRequest);

    /**
     * Находит экзаменационные группы в которых содержится заявление абитуриента
     *
     * @param entrantRequest заявление абитуриента
     * @return экзаменационные группы (всегда не null). Пустой список, если нигде не содержится
     */
    List<ExamGroup> getEntrantRequestGroups(EntrantRequest entrantRequest);

    /**
     * Проверяет актуальность строки экзаменационной группы (разрешено ли ей находится в своей экзам. группе)
     *
     * @param row строка экзаменационной группы
     * @return true, если строка находится в правильной экзаменационной группе
     */
    boolean isExamGroupRowValid(ExamGroupRow row);

    /**
     * Получает детализацию экзаменационной группы
     *
     * @param examGroup экзаменационная группа
     * @return детализация
     */
    Map<PairKey<Discipline2RealizationWayRelation, SubjectPassForm>, Set<RequestedEnrollmentDirection>> getExamPassDisciplineList(ExamGroup examGroup);
}
