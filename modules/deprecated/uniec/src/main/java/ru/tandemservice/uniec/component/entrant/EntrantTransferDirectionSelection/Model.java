/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantTransferDirectionSelection;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

/**
 * @author agolubenko
 * @since 03.04.2009
 */
@Input( { @Bind(key = "entrantId"), @Bind(key = "enrollmentDirectionId"), @Bind(key = "orderTypeId") })
public class Model
{
    private Long _entrantId;
    private Long _orderTypeId;
    private Long _enrollmentDirectionId;

    private Entrant _entrant;
    private EntrantEnrollmentOrderType _orderType;
    private EnrollmentDirection _enrollmentDirection;
    private RequestedEnrollmentDirection _requestedEnrollmentDirection;

    private DynamicListDataSource<RequestedEnrollmentDirection> _dataSource;

    public Long getEntrantId()
    {
        return _entrantId;
    }

    public void setEntrantId(Long entrantId)
    {
        _entrantId = entrantId;
    }

    public Long getEnrollmentDirectionId()
    {
        return _enrollmentDirectionId;
    }

    public void setEnrollmentDirectionId(Long enrollmentDirectionId)
    {
        _enrollmentDirectionId = enrollmentDirectionId;
    }

    public Entrant getEntrant()
    {
        return _entrant;
    }

    public void setEntrant(Entrant entrant)
    {
        _entrant = entrant;
    }

    public EnrollmentDirection getEnrollmentDirection()
    {
        return _enrollmentDirection;
    }

    public void setEnrollmentDirection(EnrollmentDirection enrollmentDirection)
    {
        _enrollmentDirection = enrollmentDirection;
    }

    public DynamicListDataSource<RequestedEnrollmentDirection> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<RequestedEnrollmentDirection> dataSource)
    {
        _dataSource = dataSource;
    }

    public RequestedEnrollmentDirection getRequestedEnrollmentDirection()
    {
        return _requestedEnrollmentDirection;
    }

    public void setRequestedEnrollmentDirection(RequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        _requestedEnrollmentDirection = requestedEnrollmentDirection;
    }

    public Long getOrderTypeId()
    {
        return _orderTypeId;
    }

    public void setOrderTypeId(Long orderTypeId)
    {
        _orderTypeId = orderTypeId;
    }

    public EntrantEnrollmentOrderType getOrderType()
    {
        return _orderType;
    }

    public void setOrderType(EntrantEnrollmentOrderType orderType)
    {
        _orderType = orderType;
    }
}
