package ru.tandemservice.uniec.entity.report;

import org.tandemframework.core.view.formatter.DateFormatter;

import ru.tandemservice.uniec.entity.report.gen.EntrantRequestDailyReportGen;

/**
 * Ежедневная сводка по заявлениям с нарастающим итогом
 */
public class EntrantRequestDailyReport extends EntrantRequestDailyReportGen
{
    public static final String P_PERIOD_TITLE = "periodTitle";

    public String getPeriodTitle()
    {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }
}