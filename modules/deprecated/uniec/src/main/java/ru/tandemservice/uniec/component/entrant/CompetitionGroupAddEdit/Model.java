/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.CompetitionGroupAddEdit;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.context.MessageSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignModel;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.CompetitionGroup;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

/**
 * @author vip_delete
 * @since 27.05.2009
 */
@Input(@Bind(key = "competitionGroupId", binding = "competitionGroup.id"))
public class Model implements IEnrollmentCampaignSelectModel, IEnrollmentCampaignModel
{
    public static final String CHECKBOX_COLUMN = "checkbox";
    public static final String CHECKBOX_SELECTED_COLUMN = "checkboxSelected";

    private CompetitionGroup _competitionGroup = new CompetitionGroup();
    private List<EnrollmentCampaign> _enrollmentCampaignList;
    private ISelectModel _formativeOrgUnitModel;
    private OrgUnit _formativeOrgUnit;
    private ISelectModel _territorialOrgUnitModel;
    private OrgUnit _territorialOrgUnit;
    private Set<Long> _selectedEnrollmentDirectionSet = new HashSet<>();

    private MessageSource _messageSource;
    private DynamicListDataSource<EnrollmentDirection> _dataSource;
    private DynamicListDataSource<EnrollmentDirection> _choosenDataSource;

    private boolean _distributed;

    // IEnrollmentCampaignSelectModel

    @Override
    public IDataSettings getSettings()
    {
        return null;
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _competitionGroup.getEnrollmentCampaign();
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _competitionGroup.setEnrollmentCampaign(enrollmentCampaign);
    }

    // IEnrollmentCampaignModel (empty stubs)

    @Override
    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return null;
    }

    @Override
    public DevelopForm getDevelopForm()
    {
        return null;
    }

    @Override
    public DevelopCondition getDevelopCondition()
    {
        return null;
    }

    @Override
    public DevelopTech getDevelopTech()
    {
        return null;
    }

    @Override
    public DevelopPeriod getDevelopPeriod()
    {
        return null;
    }

    // Getters & Setters

    public CompetitionGroup getCompetitionGroup()
    {
        return _competitionGroup;
    }

    public void setCompetitionGroup(CompetitionGroup competitionGroup)
    {
        _competitionGroup = competitionGroup;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public ISelectModel getFormativeOrgUnitModel()
    {
        return _formativeOrgUnitModel;
    }

    public void setFormativeOrgUnitModel(ISelectModel formativeOrgUnitModel)
    {
        _formativeOrgUnitModel = formativeOrgUnitModel;
    }

    @Override
    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        _formativeOrgUnit = formativeOrgUnit;
    }

    public ISelectModel getTerritorialOrgUnitModel()
    {
        return _territorialOrgUnitModel;
    }

    public void setTerritorialOrgUnitModel(ISelectModel territorialOrgUnitModel)
    {
        _territorialOrgUnitModel = territorialOrgUnitModel;
    }

    @Override
    public OrgUnit getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    public void setTerritorialOrgUnit(OrgUnit territorialOrgUnit)
    {
        _territorialOrgUnit = territorialOrgUnit;
    }

    public DynamicListDataSource<EnrollmentDirection> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<EnrollmentDirection> dataSource)
    {
        _dataSource = dataSource;
    }

    public DynamicListDataSource<EnrollmentDirection> getChoosenDataSource()
    {
        return _choosenDataSource;
    }

    public void setChoosenDataSource(DynamicListDataSource<EnrollmentDirection> choosenDataSource)
    {
        _choosenDataSource = choosenDataSource;
    }

    public Set<Long> getSelectedEnrollmentDirectionSet()
    {
        return _selectedEnrollmentDirectionSet;
    }

    public void setSelectedEnrollmentDirectionSet(Set<Long> selectedEnrollmentDirectionSet)
    {
        _selectedEnrollmentDirectionSet = selectedEnrollmentDirectionSet;
    }

    public MessageSource getMessageSource()
    {
        return _messageSource;
    }

    public void setMessageSource(MessageSource messageSource)
    {
        _messageSource = messageSource;
    }

    public boolean isDistributed()
    {
        return _distributed;
    }

    public void setDistributed(boolean distributed)
    {
        _distributed = distributed;
    }
}
