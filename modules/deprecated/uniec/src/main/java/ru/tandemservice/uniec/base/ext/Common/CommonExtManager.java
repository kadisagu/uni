/* $Id:$ */
package ru.tandemservice.uniec.base.ext.Common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.util.IModuleStatusReporter;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;

import java.util.Arrays;

/**
 * @author oleyba
 * @since 2/11/14
 */
@Configuration
public class CommonExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private CommonManager _commonManager;

    @Bean
    public ItemListExtension<IModuleStatusReporter> moduleStatusExtPoint()
    {
        return itemListExtension(_commonManager.moduleStatusExtPoint())
            .add("uniec", () -> Arrays.asList(
                "Абитуриентов: " + IUniBaseDao.instance.get().getCount(Entrant.class, Entrant.enrollmentCampaign().educationYear().current().s(), Boolean.TRUE) + " (тек. уч. год), " + IUniBaseDao.instance.get().getCount(Entrant.class) + " (всего)",
                "Заявлений: " + IUniBaseDao.instance.get().getCount(EntrantRequest.class, EntrantRequest.entrant().enrollmentCampaign().educationYear().current().s(), Boolean.TRUE) + " (тек. уч. год), " + IUniBaseDao.instance.get().getCount(EntrantRequest.class) + " (всего)"
            ))
            .create();
    }
}
