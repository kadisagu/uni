/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.menu.StateExamExportPrint;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.Date;

/**
 * @author vip_delete
 * @since 06.04.2009
 */
@State({
        @Bind(key = "dateFrom", binding = "dateFrom"),
        @Bind(key = "dateTo", binding = "dateTo"),
        @Bind(key = "exportType", binding = "exportType"),
        @Bind(key = "accepted", binding = "accepted"),
        @Bind(key = "sent", binding = "sent"),
        @Bind(key = "includeNonRF", binding = "includeNonRF"),
        @Bind(key = "enrollmentCampaignId", binding = "enrollmentCampaign.id"),
        @Bind(key = "certificateHoldingId", binding = "certificateHoldingId"),
        @Bind(key = "includeEntrantWithoutMark", binding = "includeEntrantWithoutMark")
})
public class Model
{
    private EnrollmentCampaign _enrollmentCampaign = new EnrollmentCampaign();
    private Date _dateFrom;
    private Date _dateTo;
    private int _exportType;
    private Long _accepted;
    private Long _sent;
    private boolean _includeNonRF;
    private boolean _includeEntrantWithoutMark;
    private Long _certificateHoldingId;
    private StringBuilder _exportData;

    // Gettes & Setters

    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public Date getDateFrom()
    {
        return _dateFrom;
    }

    public void setDateFrom(Date dateFrom)
    {
        _dateFrom = dateFrom;
    }

    public Date getDateTo()
    {
        return _dateTo;
    }

    public void setDateTo(Date dateTo)
    {
        _dateTo = dateTo;
    }

    public int getExportType()
    {
        return _exportType;
    }

    public void setExportType(int exportType)
    {
        _exportType = exportType;
    }

    public Long getAccepted()
    {
        return _accepted;
    }

    public void setAccepted(Long accepted)
    {
        _accepted = accepted;
    }

    public Long getSent()
    {
        return _sent;
    }

    public void setSent(Long sent)
    {
        _sent = sent;
    }

    public boolean isIncludeNonRF()
    {
        return _includeNonRF;
    }

    public void setIncludeNonRF(boolean includeNonRF)
    {
        _includeNonRF = includeNonRF;
    }

    public boolean isIncludeEntrantWithoutMark()
    {
        return _includeEntrantWithoutMark;
    }

    public void setIncludeEntrantWithoutMark(boolean includeEntrantWithoutMark)
    {
        _includeEntrantWithoutMark = includeEntrantWithoutMark;
    }

    public Long getCertificateHoldingId()
    {
        return _certificateHoldingId;
    }

    public void setCertificateHoldingId(Long certificateHoldingId)
    {
        _certificateHoldingId = certificateHoldingId;
    }

    public StringBuilder getExportData()
    {
        return _exportData;
    }

    public void setExportData(StringBuilder exportData)
    {
        _exportData = exportData;
    }
}
