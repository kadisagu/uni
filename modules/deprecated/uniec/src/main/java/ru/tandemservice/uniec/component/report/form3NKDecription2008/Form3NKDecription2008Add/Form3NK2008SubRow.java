/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.form3NKDecription2008.Form3NKDecription2008Add;

import java.util.Collection;
import java.util.Collections;

import org.tandemframework.core.common.ITitled;

import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

/**
 * @author vip_delete
 * @since 19.05.2009
 */
class Form3NK2008SubRow implements ITitled
{
    // 1. Название
    private String _title;

    // 1. Расширенное название
    private String _fullTitle;

    // надо ли использовать расширенное название
    private boolean _useFullTitle;

    // 2. госзаказ
    private int _statePlan;

    // 3. подано заявлений
    private int _requestCount;

    // 4. принято
    private int _enrolledCount;

    // 5. в т.ч. по договору
    private int _contractCount;

    // 6. в т.ч. на второе высшее
    private int _secondEduCount;

    private Collection<RequestedEnrollmentDirection> _rowItems;
    private Collection<PreliminaryEnrollmentStudent> _enrolledRowItems;

    public Form3NK2008SubRow(String title, String fullTitle, int statePlan, Collection<RequestedEnrollmentDirection> rowItems, Collection<PreliminaryEnrollmentStudent> enrolledRowItems)
    {
        _title = title;
        _fullTitle = fullTitle;
        _statePlan = statePlan;
        _rowItems = rowItems == null ? Collections.<RequestedEnrollmentDirection>emptyList() : rowItems;
        _enrolledRowItems = enrolledRowItems == null ? Collections.<PreliminaryEnrollmentStudent>emptyList() : enrolledRowItems;
    }

    public void useFullTitle()
    {
        _useFullTitle = true;
    }

    public void buildRow()
    {
        _requestCount = _rowItems.size();
        _enrolledCount = _enrolledRowItems.size();
        _contractCount = 0;
        _secondEduCount = 0;
        for (PreliminaryEnrollmentStudent preStudent : _enrolledRowItems)
        {
            if (UniDefines.COMPENSATION_TYPE_CONTRACT.equals(preStudent.getCompensationType().getCode()))
                _contractCount++;
            if (UniDefines.STUDENT_CATEGORY_SECOND_HIGH.equals(preStudent.getStudentCategory().getCode()))
                _secondEduCount++;
        }
    }

    // Getters

    @Override
    public String getTitle()
    {
        return _title;
    }

    public String getFullTitle()
    {
        return _fullTitle;
    }

    public boolean isUseFullTitle()
    {
        return _useFullTitle;
    }

    public int getStatePlan()
    {
        return _statePlan;
    }

    public int getRequestCount()
    {
        return _requestCount;
    }

    public int getEnrolledCount()
    {
        return _enrolledCount;
    }

    public int getContractCount()
    {
        return _contractCount;
    }

    public int getSecondEduCount()
    {
        return _secondEduCount;
    }
}
