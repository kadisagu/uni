package ru.tandemservice.uniec.entity.settings.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec.entity.settings.ConversionScale;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Шкала перевода
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ConversionScaleGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.settings.ConversionScale";
    public static final String ENTITY_NAME = "conversionScale";
    public static final int VERSION_HASH = -1116715738;
    private static IEntityMeta ENTITY_META;

    public static final String L_DISCIPLINE = "discipline";
    public static final String L_SUBJECT = "subject";
    public static final String P_BUDGET_SCALE = "budgetScale";
    public static final String P_CONTRACT_SCALE = "contractScale";

    private Discipline2RealizationWayRelation _discipline;     // Дисциплина набора вступительных испытаний
    private StateExamSubject _subject;     // Предмет ЕГЭ
    private byte[] _budgetScale;     // Шкала по бюджету
    private byte[] _contractScale;     // Шкала по контракту

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дисциплина набора вступительных испытаний. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public Discipline2RealizationWayRelation getDiscipline()
    {
        return _discipline;
    }

    /**
     * @param discipline Дисциплина набора вступительных испытаний. Свойство не может быть null и должно быть уникальным.
     */
    public void setDiscipline(Discipline2RealizationWayRelation discipline)
    {
        dirty(_discipline, discipline);
        _discipline = discipline;
    }

    /**
     * @return Предмет ЕГЭ. Свойство не может быть null.
     */
    @NotNull
    public StateExamSubject getSubject()
    {
        return _subject;
    }

    /**
     * @param subject Предмет ЕГЭ. Свойство не может быть null.
     */
    public void setSubject(StateExamSubject subject)
    {
        dirty(_subject, subject);
        _subject = subject;
    }

    /**
     * @return Шкала по бюджету. Свойство не может быть null.
     */
    @NotNull
    public byte[] getBudgetScale()
    {
        return _budgetScale;
    }

    /**
     * @param budgetScale Шкала по бюджету. Свойство не может быть null.
     */
    public void setBudgetScale(byte[] budgetScale)
    {
        dirty(_budgetScale, budgetScale);
        _budgetScale = budgetScale;
    }

    /**
     * @return Шкала по контракту. Свойство не может быть null.
     */
    @NotNull
    public byte[] getContractScale()
    {
        return _contractScale;
    }

    /**
     * @param contractScale Шкала по контракту. Свойство не может быть null.
     */
    public void setContractScale(byte[] contractScale)
    {
        dirty(_contractScale, contractScale);
        _contractScale = contractScale;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ConversionScaleGen)
        {
            setDiscipline(((ConversionScale)another).getDiscipline());
            setSubject(((ConversionScale)another).getSubject());
            setBudgetScale(((ConversionScale)another).getBudgetScale());
            setContractScale(((ConversionScale)another).getContractScale());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ConversionScaleGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ConversionScale.class;
        }

        public T newInstance()
        {
            return (T) new ConversionScale();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "discipline":
                    return obj.getDiscipline();
                case "subject":
                    return obj.getSubject();
                case "budgetScale":
                    return obj.getBudgetScale();
                case "contractScale":
                    return obj.getContractScale();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "discipline":
                    obj.setDiscipline((Discipline2RealizationWayRelation) value);
                    return;
                case "subject":
                    obj.setSubject((StateExamSubject) value);
                    return;
                case "budgetScale":
                    obj.setBudgetScale((byte[]) value);
                    return;
                case "contractScale":
                    obj.setContractScale((byte[]) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "discipline":
                        return true;
                case "subject":
                        return true;
                case "budgetScale":
                        return true;
                case "contractScale":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "discipline":
                    return true;
                case "subject":
                    return true;
                case "budgetScale":
                    return true;
                case "contractScale":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "discipline":
                    return Discipline2RealizationWayRelation.class;
                case "subject":
                    return StateExamSubject.class;
                case "budgetScale":
                    return byte[].class;
                case "contractScale":
                    return byte[].class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ConversionScale> _dslPath = new Path<ConversionScale>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ConversionScale");
    }
            

    /**
     * @return Дисциплина набора вступительных испытаний. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.settings.ConversionScale#getDiscipline()
     */
    public static Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation> discipline()
    {
        return _dslPath.discipline();
    }

    /**
     * @return Предмет ЕГЭ. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.ConversionScale#getSubject()
     */
    public static StateExamSubject.Path<StateExamSubject> subject()
    {
        return _dslPath.subject();
    }

    /**
     * @return Шкала по бюджету. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.ConversionScale#getBudgetScale()
     */
    public static PropertyPath<byte[]> budgetScale()
    {
        return _dslPath.budgetScale();
    }

    /**
     * @return Шкала по контракту. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.ConversionScale#getContractScale()
     */
    public static PropertyPath<byte[]> contractScale()
    {
        return _dslPath.contractScale();
    }

    public static class Path<E extends ConversionScale> extends EntityPath<E>
    {
        private Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation> _discipline;
        private StateExamSubject.Path<StateExamSubject> _subject;
        private PropertyPath<byte[]> _budgetScale;
        private PropertyPath<byte[]> _contractScale;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дисциплина набора вступительных испытаний. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.settings.ConversionScale#getDiscipline()
     */
        public Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation> discipline()
        {
            if(_discipline == null )
                _discipline = new Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation>(L_DISCIPLINE, this);
            return _discipline;
        }

    /**
     * @return Предмет ЕГЭ. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.ConversionScale#getSubject()
     */
        public StateExamSubject.Path<StateExamSubject> subject()
        {
            if(_subject == null )
                _subject = new StateExamSubject.Path<StateExamSubject>(L_SUBJECT, this);
            return _subject;
        }

    /**
     * @return Шкала по бюджету. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.ConversionScale#getBudgetScale()
     */
        public PropertyPath<byte[]> budgetScale()
        {
            if(_budgetScale == null )
                _budgetScale = new PropertyPath<byte[]>(ConversionScaleGen.P_BUDGET_SCALE, this);
            return _budgetScale;
        }

    /**
     * @return Шкала по контракту. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.ConversionScale#getContractScale()
     */
        public PropertyPath<byte[]> contractScale()
        {
            if(_contractScale == null )
                _contractScale = new PropertyPath<byte[]>(ConversionScaleGen.P_CONTRACT_SCALE, this);
            return _contractScale;
        }

        public Class getEntityClass()
        {
            return ConversionScale.class;
        }

        public String getEntityName()
        {
            return "conversionScale";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
