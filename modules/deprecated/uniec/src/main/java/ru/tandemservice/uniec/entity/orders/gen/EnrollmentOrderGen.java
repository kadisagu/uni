package ru.tandemservice.uniec.entity.orders.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uniec.entity.catalog.EnrollmentOrderReason;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.orders.AbstractEntrantOrder;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Приказ о зачислении абитуриентов
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrollmentOrderGen extends AbstractEntrantOrder
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.orders.EnrollmentOrder";
    public static final String ENTITY_NAME = "enrollmentOrder";
    public static final int VERSION_HASH = -192431523;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_TYPE = "type";
    public static final String L_REASON = "reason";
    public static final String P_ENROLLMENT_DATE = "enrollmentDate";
    public static final String P_ORDER_BASIC_TEXT = "orderBasicText";
    public static final String P_ORDER_TEXT = "orderText";

    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private EntrantEnrollmentOrderType _type;     // Тип приказа
    private EnrollmentOrderReason _reason;     // Причина приказа о зачислении
    private Date _enrollmentDate;     // Зачислить с
    private String _orderBasicText;     // Основание приказа
    private String _orderText;     // Приказываю

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Тип приказа. Свойство не может быть null.
     */
    @NotNull
    public EntrantEnrollmentOrderType getType()
    {
        return _type;
    }

    /**
     * @param type Тип приказа. Свойство не может быть null.
     */
    public void setType(EntrantEnrollmentOrderType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Причина приказа о зачислении.
     */
    public EnrollmentOrderReason getReason()
    {
        return _reason;
    }

    /**
     * @param reason Причина приказа о зачислении.
     */
    public void setReason(EnrollmentOrderReason reason)
    {
        dirty(_reason, reason);
        _reason = reason;
    }

    /**
     * @return Зачислить с.
     */
    public Date getEnrollmentDate()
    {
        return _enrollmentDate;
    }

    /**
     * @param enrollmentDate Зачислить с.
     */
    public void setEnrollmentDate(Date enrollmentDate)
    {
        dirty(_enrollmentDate, enrollmentDate);
        _enrollmentDate = enrollmentDate;
    }

    /**
     * @return Основание приказа.
     */
    @Length(max=2000)
    public String getOrderBasicText()
    {
        return _orderBasicText;
    }

    /**
     * @param orderBasicText Основание приказа.
     */
    public void setOrderBasicText(String orderBasicText)
    {
        dirty(_orderBasicText, orderBasicText);
        _orderBasicText = orderBasicText;
    }

    /**
     * @return Приказываю.
     */
    @Length(max=2000)
    public String getOrderText()
    {
        return _orderText;
    }

    /**
     * @param orderText Приказываю.
     */
    public void setOrderText(String orderText)
    {
        dirty(_orderText, orderText);
        _orderText = orderText;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrollmentOrderGen)
        {
            setEnrollmentCampaign(((EnrollmentOrder)another).getEnrollmentCampaign());
            setType(((EnrollmentOrder)another).getType());
            setReason(((EnrollmentOrder)another).getReason());
            setEnrollmentDate(((EnrollmentOrder)another).getEnrollmentDate());
            setOrderBasicText(((EnrollmentOrder)another).getOrderBasicText());
            setOrderText(((EnrollmentOrder)another).getOrderText());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrollmentOrderGen> extends AbstractEntrantOrder.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrollmentOrder.class;
        }

        public T newInstance()
        {
            return (T) new EnrollmentOrder();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "type":
                    return obj.getType();
                case "reason":
                    return obj.getReason();
                case "enrollmentDate":
                    return obj.getEnrollmentDate();
                case "orderBasicText":
                    return obj.getOrderBasicText();
                case "orderText":
                    return obj.getOrderText();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "type":
                    obj.setType((EntrantEnrollmentOrderType) value);
                    return;
                case "reason":
                    obj.setReason((EnrollmentOrderReason) value);
                    return;
                case "enrollmentDate":
                    obj.setEnrollmentDate((Date) value);
                    return;
                case "orderBasicText":
                    obj.setOrderBasicText((String) value);
                    return;
                case "orderText":
                    obj.setOrderText((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                        return true;
                case "type":
                        return true;
                case "reason":
                        return true;
                case "enrollmentDate":
                        return true;
                case "orderBasicText":
                        return true;
                case "orderText":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return true;
                case "type":
                    return true;
                case "reason":
                    return true;
                case "enrollmentDate":
                    return true;
                case "orderBasicText":
                    return true;
                case "orderText":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "type":
                    return EntrantEnrollmentOrderType.class;
                case "reason":
                    return EnrollmentOrderReason.class;
                case "enrollmentDate":
                    return Date.class;
                case "orderBasicText":
                    return String.class;
                case "orderText":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrollmentOrder> _dslPath = new Path<EnrollmentOrder>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrollmentOrder");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrder#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Тип приказа. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrder#getType()
     */
    public static EntrantEnrollmentOrderType.Path<EntrantEnrollmentOrderType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Причина приказа о зачислении.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrder#getReason()
     */
    public static EnrollmentOrderReason.Path<EnrollmentOrderReason> reason()
    {
        return _dslPath.reason();
    }

    /**
     * @return Зачислить с.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrder#getEnrollmentDate()
     */
    public static PropertyPath<Date> enrollmentDate()
    {
        return _dslPath.enrollmentDate();
    }

    /**
     * @return Основание приказа.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrder#getOrderBasicText()
     */
    public static PropertyPath<String> orderBasicText()
    {
        return _dslPath.orderBasicText();
    }

    /**
     * @return Приказываю.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrder#getOrderText()
     */
    public static PropertyPath<String> orderText()
    {
        return _dslPath.orderText();
    }

    public static class Path<E extends EnrollmentOrder> extends AbstractEntrantOrder.Path<E>
    {
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private EntrantEnrollmentOrderType.Path<EntrantEnrollmentOrderType> _type;
        private EnrollmentOrderReason.Path<EnrollmentOrderReason> _reason;
        private PropertyPath<Date> _enrollmentDate;
        private PropertyPath<String> _orderBasicText;
        private PropertyPath<String> _orderText;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrder#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Тип приказа. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrder#getType()
     */
        public EntrantEnrollmentOrderType.Path<EntrantEnrollmentOrderType> type()
        {
            if(_type == null )
                _type = new EntrantEnrollmentOrderType.Path<EntrantEnrollmentOrderType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Причина приказа о зачислении.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrder#getReason()
     */
        public EnrollmentOrderReason.Path<EnrollmentOrderReason> reason()
        {
            if(_reason == null )
                _reason = new EnrollmentOrderReason.Path<EnrollmentOrderReason>(L_REASON, this);
            return _reason;
        }

    /**
     * @return Зачислить с.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrder#getEnrollmentDate()
     */
        public PropertyPath<Date> enrollmentDate()
        {
            if(_enrollmentDate == null )
                _enrollmentDate = new PropertyPath<Date>(EnrollmentOrderGen.P_ENROLLMENT_DATE, this);
            return _enrollmentDate;
        }

    /**
     * @return Основание приказа.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrder#getOrderBasicText()
     */
        public PropertyPath<String> orderBasicText()
        {
            if(_orderBasicText == null )
                _orderBasicText = new PropertyPath<String>(EnrollmentOrderGen.P_ORDER_BASIC_TEXT, this);
            return _orderBasicText;
        }

    /**
     * @return Приказываю.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrder#getOrderText()
     */
        public PropertyPath<String> orderText()
        {
            if(_orderText == null )
                _orderText = new PropertyPath<String>(EnrollmentOrderGen.P_ORDER_TEXT, this);
            return _orderText;
        }

        public Class getEntityClass()
        {
            return EnrollmentOrder.class;
        }

        public String getEntityName()
        {
            return "enrollmentOrder";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
