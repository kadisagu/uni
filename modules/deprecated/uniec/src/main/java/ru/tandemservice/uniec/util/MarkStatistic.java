/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.util;

import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.entrant.*;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * Статистика оценок для выбранного вступительного испытания
 *
 * @author Vasily Zhukov
 * @since 09.06.2010
 */
public class MarkStatistic
{
    /**
     * Выбранное вступительное испытание, хранит финальную оценку
     * Всегда не null
     */
    private ChosenEntranceDiscipline _chosenDiscipline;

    /**
     * Оценка по ЕГЭ из зачтенного сертификата ЕГЭ
     * Если таких оценок оказалось несколько, то берется с максимальным баллом
     * Либо null, если такой оценки по ЕГЭ нет
     */
    private StateExamSubjectMark _stateExamMark;

    /**
     * Оценка по ЕГЭ из зачтенного сертификата ЕГЭ, т.е. stateExamMark, переведенная по шкале вуза
     * Либо null, если такой оценки по ЕГЭ нет
     */
    private Double _scaledStateExamMark;

    /**
     * Оценка по ЕГЭ из всех сертификатов ЕГЭ (зачтенных и не зачтенных)
     * Если таких оценок оказалось несколько, то берется с максимальным баллом
     * Это, так называемая, возможная оценка по ЕГЭ
     * Либо null, если такой оценки по ЕГЭ нет
     */
    private StateExamSubjectMark _stateExamMarkPossible;

    /**
     * Оценка по ЕГЭ из всех сертификатов ЕГЭ (зачтенных и не зачтенных), т.е. stateExamMarkPossible, переведенная по шкале вуза
     * Либо null, если такой оценки по ЕГЭ нет
     */
    private Double _scaledStateExamMarkPossible;

    /**
     * Оценка по олимиаде (всегда равна максимально возможной оценке по дисциплине)
     * Либо null, если нет зачтения по олимпиаде
     */
    private Integer _olympiad;

    /**
     * Дипломы, которые используются для зачтения этого выбранного вступительного испытания
     * Либо null, если нет дипломов
     * Если не null, то всегда не пусто
     */
    private Set<OlympiadDiploma> _diplomaSet;

    /**
     * Оценки по всем внутренним формам сдачи
     * В ключах содержатся только те формы сдачи по которым есть дисциплины для сдачи
     * Если PairKey равно null, значит по дисциплине для сдачи нет оценок
     * Если ExamPassMark всегда не null. Надо смотреть на entrantAbsenceNote (поставлена неявка и апеляции быть не может) и mark (поставлена цифровая оценка)
     * Если ExamPassMarkAppeal не равно null, значит была поставлена цифровая оценка и цифровая оценка при аппеляции
     * Всегда не null
     */
    private Map<SubjectPassForm, PairKey<ExamPassMark, ExamPassMarkAppeal>> _internalMarkMap;

    // Только для приватного использования в EntrantDataUtil
    public MarkStatistic(ChosenEntranceDiscipline chosenDiscipline,
                         StateExamSubjectMark stateExamMark,
                         Double scaledStateExamMark,
                         StateExamSubjectMark stateExamMarkPossible,
                         Double scaledStateExamMarkPossible,
                         Set<OlympiadDiploma> diplomaSet,
                         LinkedHashMap<SubjectPassForm, PairKey<ExamPassMark, ExamPassMarkAppeal>> internalMarkMap)
    {
        _chosenDiscipline = chosenDiscipline;
        _stateExamMark = stateExamMark;
        _scaledStateExamMark = scaledStateExamMark;
        _stateExamMarkPossible = stateExamMarkPossible;
        _scaledStateExamMarkPossible = scaledStateExamMarkPossible;
        if (diplomaSet != null)
        {
            _olympiad = chosenDiscipline.getEnrollmentCampaignDiscipline().getMaxMark();
            _diplomaSet = Collections.unmodifiableSet(diplomaSet);
        }
        _internalMarkMap = Collections.unmodifiableMap(internalMarkMap);
    }

    // Getters

    public ChosenEntranceDiscipline getChosenDiscipline()
    {
        return _chosenDiscipline;
    }

    public StateExamSubjectMark getStateExamMark()
    {
        return _stateExamMark;
    }

    public Double getScaledStateExamMark()
    {
        return _scaledStateExamMark;
    }

    public StateExamSubjectMark getStateExamMarkPossible()
    {
        return _stateExamMarkPossible;
    }

    public Double getScaledStateExamMarkPossible()
    {
        return _scaledStateExamMarkPossible;
    }

    public Integer getOlympiad()
    {
        return _olympiad;
    }

    public Set<OlympiadDiploma> getDiplomaSet()
    {
        return _diplomaSet;
    }

    public Map<SubjectPassForm, PairKey<ExamPassMark, ExamPassMarkAppeal>> getInternalMarkMap()
    {
        return _internalMarkMap;
    }
}
