/**
 *$Id$
 */
package ru.tandemservice.uniec.base.bo.EcReport.ui.EnrollmentDataCollectAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.base.bo.EcCampaign.EcCampaignManager;

/**
 * @author Alexander Shaburov
 * @since 15.08.13
 */
@Configuration
public class EcReportEnrollmentDataCollectAdd extends BusinessComponentManager
{
    public static final String ENROLLMENT_CAMPAIGN_SELECT_DS = "enrollmentCampaignSelectDS";
    public static final String STUDENT_CATEGORY_SELECT_DS = "studentCategorySelectDS";
    public static final String COMPENSATION_TYPE_SELECT_DS = "compensationTypeSelectDS";
    
    
    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addAddon(uiAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME, UniEduProgramEducationOrgUnitAddon.class))
                .addDataSource(selectDS(ENROLLMENT_CAMPAIGN_SELECT_DS, EcCampaignManager.instance().enrollmentCampaignComboDSHandler()))
                .addDataSource(selectDS(STUDENT_CATEGORY_SELECT_DS, studentCategorySelectDSHandler()))
                .addDataSource(selectDS(COMPENSATION_TYPE_SELECT_DS, compensationTypeSelectDSHandler()))
                .addDataSource(CommonManager.instance().yesNoDSConfig())
                .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> compensationTypeSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), CompensationType.class)
                .order(CompensationType.title())
                .filter(CompensationType.title())
                .pageable(true);
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> studentCategorySelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), StudentCategory.class)
                .order(StudentCategory.title())
                .filter(StudentCategory.title())
                .pageable(true);
    }
}
