/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EntrantsForeignLangs.EntrantsForeignLangsAdd;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonForeignLanguage;
import org.tandemframework.shared.person.catalog.entity.ForeignLanguageSkill;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;

import java.util.*;

/**
 * @author ekachanova
 */
class EntrantsForeignLangsReportBuilder
{
    private Model _model;
    private Session _session;

    EntrantsForeignLangsReportBuilder(Model model, Session session)
    {
        _model = model;
        _session = session;
    }

    DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    @SuppressWarnings("unchecked")
    private byte[] buildReport()
    {
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ENTRANTS_FOREIGN_LANGS);
        RtfDocument template = new RtfReader().read(templateDocument.getCurrentTemplate());

        RtfDocument mainDocument = template.getClone();
        mainDocument.getElementList().clear();

        IRtfControl page = RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE);

        final int timesIndex = mainDocument.getHeader().getFontTable().addFont("Times New Roman");

        mainDocument.getElementList().addAll(new RtfString()
                .font(timesIndex).boldBegin().append(IRtfData.QC).fontSize(28)
                .append(TopOrgUnit.getInstance().getTitle())
                .par().par()
                .append("Список абитуриентов, изучающих иностранные языки")
                .par().par()
                .boldEnd().toList());

        Object[] orgUnits = getOrgUnits();
        List<EducationOrgUnit> educationOrgUnits = (List) orgUnits[0];
        List<OrgUnit> formativeOrgUnits = (List) orgUnits[1];

        boolean groupByDirection = _model.getReport().isGroupByDirection();

        for (int i = 0; i < formativeOrgUnits.size(); i++)
        {
            OrgUnit formativeOrgUnit = formativeOrgUnits.get(i);

            MQBuilder builder;
            if (_model.getReport().getEnrollmentCampaignStage().equals(Model.ENROLLMENT_CAMPAIGN_STAGE_ENROLLMENT))
            {
                builder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "p", new String[]{"id"});
                if (_model.isCompensationTypeActive())
                    builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.L_COMPENSATION_TYPE, _model.getReport().getCompensationType()));
                if (_model.isStudentCategoryActive())
                    builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.L_STUDENT_CATEGORY, _model.getStudentCategoryList()));
                builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, formativeOrgUnit));

                builder.addJoin("p", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, "d");
                builder.add(MQExpression.eq("d", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, _model.getReport().getEnrollmentCampaign()));
                builder.add(MQExpression.eq("d", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, Boolean.FALSE));
                builder.add(UniMQExpression.betweenDate("d", RequestedEnrollmentDirection.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));

                if (_model.isParallelActive() && _model.getParallel().isTrue())
                    builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.P_PARALLEL, Boolean.FALSE));
            } else
            {
                builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "d", new String[]{"id"});
                builder.add(MQExpression.eq("d", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, _model.getReport().getEnrollmentCampaign()));
                builder.add(MQExpression.eq("d", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, formativeOrgUnit));
                builder.add(UniMQExpression.betweenDate("d", RequestedEnrollmentDirection.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));
                builder.add(MQExpression.eq("d", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, Boolean.FALSE));
                builder.add(MQExpression.notEq("d", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
                if (_model.isCompensationTypeActive())
                    builder.add(MQExpression.eq("d", RequestedEnrollmentDirection.L_COMPENSATION_TYPE, _model.getReport().getCompensationType()));
                if (_model.isStudentCategoryActive())
                    builder.add(MQExpression.in("d", RequestedEnrollmentDirection.L_STUDENT_CATEGORY, _model.getStudentCategoryList()));

                if (_model.getReport().getEnrollmentCampaignStage().equals(Model.ENROLLMENT_CAMPAIGN_STAGE_EXAMS))
                {
                    builder.add(MQExpression.notEq("d", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE));
                    builder.add(MQExpression.notEq("d", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_ACTIVE_CODE));
                }
            }

            builder.addDomain("pfl", PersonForeignLanguage.ENTITY_CLASS);
            builder.add(MQExpression.eqProperty("pfl", PersonForeignLanguage.L_PERSON, "d", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_PERSON));
            builder.addJoin("d", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_PERSON + "." + Person.L_IDENTITY_CARD, "idCard");
            builder.addLeftJoin("pfl", PersonForeignLanguage.skill().s(), "skill");

            builder.addSelect(RequestedEnrollmentDirection.entrantRequest().entrant().id().fromAlias("d").s());
            builder.addSelect(IdentityCard.lastName().fromAlias("idCard").s());
            builder.addSelect(IdentityCard.firstName().fromAlias("idCard").s());
            builder.addSelect(IdentityCard.middleName().fromAlias("idCard").s());
            builder.addSelect(PersonForeignLanguage.language().title().fromAlias("pfl").s());
            builder.addSelect(ForeignLanguageSkill.title().fromAlias("skill").s());
            builder.addSelect(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().fromAlias("d").s());

            builder.addOrder("d", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().displayableTitle().s());
            builder.addOrder("idCard", IdentityCard.lastName().s());
            builder.addOrder("idCard", IdentityCard.firstName().s());
            builder.addOrder("idCard", IdentityCard.middleName().s());

            Map<MultiKey, String> newSkillMap = new HashMap<>();
            Map<Long, String> fioMap = new HashMap<>();
            Map<EducationOrgUnit, Map<String, Set<Long>>> entrantMap = new LinkedHashMap<>();

            List<Object[]> resultList = new ArrayList<>();
            BatchUtils.execute(educationOrgUnits, 512, elements -> {
                MQBuilder batchBuilder = new MQBuilder(builder);
                String eduOrgUnitSelectProperty = _model.getReport().getEnrollmentCampaignStage().equals(Model.ENROLLMENT_CAMPAIGN_STAGE_ENROLLMENT) ?
                        PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT : RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_EDUCATION_ORG_UNIT;
                resultList.addAll(batchBuilder.add(MQExpression.in(builder.getSelectAliasList().get(0), eduOrgUnitSelectProperty, elements)).getResultList(_session));
            });

            for (Object[] row : resultList)
            {
                Long entrantId = (Long) row[1];
                String fio = UniStringUtils.join((String) row[2], (String) row[3], (String) row[4]);
                String lang = (String) row[5];
                String skill = StringUtils.trimToEmpty((String) row[6]);
                EducationOrgUnit eduOu = (EducationOrgUnit) row[7];

                fioMap.put(entrantId, fio);

                EducationOrgUnit eduOuKey = groupByDirection ? eduOu : null;

                SafeMap.safeGet(SafeMap.safeGet(entrantMap, eduOuKey, HashMap.class), lang, LinkedHashSet.class).add(entrantId);
                newSkillMap.put(new MultiKey(lang, entrantId), skill);
            }

            // вставляем заголовок
            if (!entrantMap.isEmpty())
            {
                mainDocument.getElementList().addAll(new RtfString()
                        .boldBegin().append(IRtfData.QC).fontSize(26)
                        .append(StringUtils.isNotEmpty(formativeOrgUnit.getNominativeCaseTitle()) ? formativeOrgUnit.getNominativeCaseTitle() : formativeOrgUnit.getTitle())
                        .par().par()
                        .boldEnd().toList());
            }

            for (Map.Entry<EducationOrgUnit, Map<String, Set<Long>>> entry : entrantMap.entrySet())
            {
                EducationOrgUnit eduOu = entry.getKey();
                if (groupByDirection)
                {
                    mainDocument.getElementList().addAll(new RtfString()
                            .boldBegin().append(IRtfData.QC).fontSize(26)
                            .append(eduOu.getEducationLevelHighSchool().getDisplayableTitle() + " (" + UniStringUtils.joinWithSeparator(", ", StringUtils.uncapitalize(eduOu.getDevelopForm().getTitle()), StringUtils.uncapitalize(eduOu.getDevelopCondition().getTitle()), eduOu.getDevelopPeriod().getTitle()) + ")")
                            .par().par()
                            .boldEnd().toList());
                }
                final Map<String, Set<Long>> eduOuEntrantMap = entry.getValue();

                List<String> langs = new ArrayList<>(eduOuEntrantMap.keySet());
                Collections.sort(langs);
                for (String lang : langs)
                {
                    RtfDocument tableDocumentTemplate = template.getClone();

                    RtfInjectModifier modifier = new RtfInjectModifier();
                    modifier.put("T", lang);
                    modifier.modify(tableDocumentTemplate);

                    final Set<Long> entrantIds = eduOuEntrantMap.get(lang);
                    String[][] tableData = new String[entrantIds.size()][3];

                    int num = 0;
                    for (Long entrantId : entrantIds)
                    {
                        String fio = fioMap.get(entrantId);
                        tableData[num] = new String[]{String.valueOf(1 + num++), fio, newSkillMap.get(new MultiKey(lang, entrantId))};
                    }

                    RtfTableModifier tableModifier = new RtfTableModifier();
                    tableModifier.put("ENTRANTS", tableData);
                    tableModifier.modify(tableDocumentTemplate);

                    mainDocument.getElementList().addAll(tableDocumentTemplate.getElementList());
                }
            }

            if (i < formativeOrgUnits.size() - 1 && !entrantMap.isEmpty())
                mainDocument.getElementList().add(page);
        }

        return RtfUtil.toByteArray(mainDocument);
    }

    private Object[] getOrgUnits()
    {
        MQBuilder builder = new MQBuilder(EducationOrgUnit.ENTITY_CLASS, "ou");

        if (_model.isQualificationActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL + "." + EducationLevelsHighSchool.L_EDUCATION_LEVEL + "." + EducationLevels.L_QUALIFICATION, _model.getQualificationList()));

        if (_model.isFormativeOrgUnitActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, _model.getFormativeOrgUnitList()));

        if (_model.isTerritorialOrgUnitActive())
            builder.add(_model.getTerritorialOrgUnitList().isEmpty() ?
                    MQExpression.isNull("ou", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT) :
                        MQExpression.in("ou", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, _model.getTerritorialOrgUnitList())
            );

        if (_model.isEducationLevelHighSchoolActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, _model.getEducationLevelHighSchoolList()));

        if (_model.isDevelopFormActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_DEVELOP_FORM, _model.getDevelopFormList()));

        if (_model.isDevelopConditionActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_DEVELOP_CONDITION, _model.getDevelopConditionList()));

        if (_model.isDevelopTechActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_DEVELOP_TECH, _model.getDevelopTechList()));

        if (_model.isDevelopPeriodActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_DEVELOP_PERIOD, _model.getDevelopPeriodList()));

        List<OrgUnit> educationOrgUnitList = builder.getResultList(_session);
        List<OrgUnit> formativeOrgUnitList = CommonBaseUtil.getPropertiesList(educationOrgUnitList, EducationOrgUnit.L_FORMATIVE_ORG_UNIT);
        List<OrgUnit> difFormativeOrgUnitList = new ArrayList<>(new HashSet<>(formativeOrgUnitList));
        Collections.sort(difFormativeOrgUnitList, (o1, o2) -> {
            int result = Integer.compare(o1.getOrgUnitType().getPriority(), o2.getOrgUnitType().getPriority());
            if (result == 0)
                result = o1.getTitle().compareTo(o2.getTitle());
            return result;
        });
        return new Object[]{educationOrgUnitList, difFormativeOrgUnitList};
    }
}
