/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.report.EntrantsRating.EntrantsRatingAdd;

import java.util.ArrayList;
import java.util.List;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.CompetitionGroup;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.report.EntrantsRatingReport;

/**
 * @author agolubenko
 * @since 16.06.2009
 */
public class Model implements IEnrollmentCampaignSelectModel
{
    private EntrantsRatingReport _report = new EntrantsRatingReport();

    private List<EnrollmentCampaign> _enrollmentCampaignList;
    private List<CompensationType> _compensationTypeList;
    private List<CompetitionGroup> _competitionGroupList;
    private String _competitionGroupName;
    private CompetitionGroup _competitionGroup;
    private boolean _technicCommissionActive;
    private List<IdentifiableWrapper<?>> _technicCommissionList = new ArrayList<IdentifiableWrapper<?>>();
    private ISelectModel _technicCommissionModel;

    @Override
    public IDataSettings getSettings()
    {
        return null;
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return getReport().getEnrollmentCampaign();
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        getReport().setEnrollmentCampaign(enrollmentCampaign);
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public List<CompetitionGroup> getCompetitionGroupList()
    {
        return _competitionGroupList;
    }

    public void setCompetitionGroupList(List<CompetitionGroup> competitionGroupList)
    {
        _competitionGroupList = competitionGroupList;
    }

    public String getCompetitionGroupName()
    {
        return _competitionGroupName;
    }

    public void setCompetitionGroupName(String competitionGroupName)
    {
        _competitionGroupName = competitionGroupName;
    }

    public EntrantsRatingReport getReport()
    {
        return _report;
    }

    public void setReport(EntrantsRatingReport report)
    {
        _report = report;
    }

    public CompetitionGroup getCompetitionGroup()
    {
        return _competitionGroup;
    }

    public void setCompetitionGroup(CompetitionGroup competitionGroup)
    {
        _competitionGroup = competitionGroup;
    }

    public boolean isTechnicCommissionActive()
    {
        return _technicCommissionActive;
    }

    public void setTechnicCommissionActive(boolean technicCommissionActive)
    {
        _technicCommissionActive = technicCommissionActive;
    }

    public List<IdentifiableWrapper<?>> getTechnicCommissionList()
    {
        return _technicCommissionList;
    }

    public void setTechnicCommissionList(List<IdentifiableWrapper<?>> technicCommissionList)
    {
        _technicCommissionList = technicCommissionList;
    }

    public ISelectModel getTechnicCommissionModel()
    {
        return _technicCommissionModel;
    }

    public void setTechnicCommissionModel(ISelectModel technicCommissionModel)
    {
        _technicCommissionModel = technicCommissionModel;
    }
}
