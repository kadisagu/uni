/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec;

/**
 * @author agolubenko
 * @since 20.05.2008
 */
public interface IUniecComponents
{
    // список в алфавитном порядке
    String CHOOSE_ENTRANCE_DISCIPLINE = "ru.tandemservice.uniec.component.entrant.ChooseEntranceDiscipline";
    String CHOOSE_ENTRANCE_EDUCATION_ORG_UNIT = "ru.tandemservice.uniec.component.entrant.ChooseEntranceEducationOrgUnit";
    String CONVERSION_SCALE = "ru.tandemservice.uniec.component.settings.ConversionScale";
    String COMPETITION_GROUP_ADD_EDIT = "ru.tandemservice.uniec.component.entrant.CompetitionGroupAddEdit";
    String COMPETITION_GROUP_DISTRIB_ADD_EDIT = "ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribAddEdit";
    String COMPETITION_GROUP_DISTRIB_PUB = "ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub";
    String DISCIPLINE_MIN_MAX_MARK = "ru.tandemservice.uniec.component.settings.DisciplineMinMaxMark";
    String DISCIPLINE_TO_REALIZATION_FORM = "ru.tandemservice.uniec.component.settings.Discipline2RealizationForm";
    String DISCIPLINE_TO_REALIZATION_WAY = "ru.tandemservice.uniec.component.settings.Discipline2RealizationWay";
    String DISCIPLINES_GROUP_ADD_EDIT = "ru.tandemservice.uniec.component.settings.DisciplinesGroupAddEdit";
    String DISCIPLINES_GROUP_LIST = "ru.tandemservice.uniec.component.settings.DisciplinesGroupList";
    String ENROLLMENT_CAMPAIGN_ADD_EDIT = "ru.tandemservice.uniec.component.settings.EnrollmentCampaignAddEdit";
    String ENROLLMENT_CAMPAIGN_COPY = "ru.tandemservice.uniec.component.settings.EnrollmentCampaignCopy";
    String ENROLLMENT_DIRECTION_ADD = "ru.tandemservice.uniec.component.entrant.EnrollmentDirectionAdd";
    String ENROLLMENT_DIRECTION_EDIT = "ru.tandemservice.uniec.component.entrant.EnrollmentDirectionEdit";
    String ENROLLMENT_ORDER_ADD_EDIT = "ru.tandemservice.uniec.component.order.EnrollmentOrderAddEdit";
    String ENROLLMENT_ORDER_PUB = "ru.tandemservice.uniec.component.order.EnrollmentOrderPub";
    String ENROLLMENT_ORDER_REASON_TO_BASICS = "ru.tandemservice.uniec.component.settings.ReasonToBasicsEnrollmentSettings";
    String ENROLLMENT_ORDER_REASON_TO_BASICS_EDIT = "ru.tandemservice.uniec.component.settings.ReasonToBasicsEnrollmentSettingsEdit";
    String ENROLLMENT_ORDER_SET_EXECUTOR = "ru.tandemservice.uniec.component.order.EnrollmentOrderSetExecutor";
    String ENROLLMENT_ORDER_VISA_ADD_EDIT = "ru.tandemservice.uniec.component.settings.EnrollmentOrderVisaAddEdit";
    String ENROLLMENT_VISA_GROUP_ADD = "ru.tandemservice.uniec.component.order.EnrollmentVisaGroupAdd";
    String ENTRANCE_DISCIPLINE_ADD_EDIT = "ru.tandemservice.uniec.component.entrant.EntranceDisciplineAddEdit";
    String ENTRANT_ADD = "ru.tandemservice.uniec.component.entrant.EntrantAdd";
    String ENTRANT_EDIT = "ru.tandemservice.uniec.component.entrant.EntrantEdit";
    String ENTRANT_ENROLLMENT_CONDITITONS_EDIT = "ru.tandemservice.uniec.component.entrant.EntrantEnrollmentConditionsEdit";
    String ENTRANT_ENROLMENT_RECOMMENTDATION_ADD_EDIT = "ru.tandemservice.uniec.component.entrant.EntrantEnrolmentRecommentdationAddEdit";
    String ENTRANT_PUB = "ru.tandemservice.uniec.component.entrant.EntrantPub";
    String ENTRANT_REGISTRATION_DATE_EDIT = "ru.tandemservice.uniec.component.entrant.EntrantRegistrationDateEdit";
    String ENTRANT_REQUEST_ADD_EDIT = "ru.tandemservice.uniec.component.entrant.EntrantRequestAddEdit";
    String ENTRANT_REQUEST_DOCUMENT_ADD_EDIT = "ru.tandemservice.uniec.component.entrant.EntrantRequestDocumentAddEdit";
    String ENTRANT_STATE_EXAM_CERTIFICATE_ADD_EDIT = "ru.tandemservice.uniec.component.entrant.EntrantStateExamCertificateAddEdit";
    String ENTRANT_TRANSFER = "ru.tandemservice.uniec.component.entrant.EntrantTransfer";
    String ENTRANT_TRANSFER_DIRECTION_SELECTION = "ru.tandemservice.uniec.component.entrant.EntrantTransferDirectionSelection";
    String EXAM_PASS_DISCIPLINE_EDIT = "ru.tandemservice.uniec.component.entrant.ExamPassDisciplineEdit";
    String EXAM_SET_ITEM_ADD_EDIT = "ru.tandemservice.uniec.component.menu.ExamSetItemAddEdit";
    String EXAM_SET_PUB = "ru.tandemservice.uniec.component.menu.ExamSetPub";
    String OLYMPIAD_DIPLOMA_ADD_EDIT = "ru.tandemservice.uniec.component.entrant.OlympiadDiplomaAddEdit";
    String PROFILE_EDU_INSTITUTION_ADD = "ru.tandemservice.uniec.component.settings.ProfileEduInstitutionAdd";
    String REQUESTED_ENROLLMENT_DIRECTION_ADD_EDIT = "ru.tandemservice.uniec.component.entrant.RequestedEnrollmentDirectionAddEdit";
    String REQUESTED_ENROLLMENT_DIRECTION_PUB = "ru.tandemservice.uniec.component.entrant.RequestedEnrollmentDirectionPub";
    String REQUESTED_ENROLLMENT_DIRECTION_STATE_ADD_EDIT = "ru.tandemservice.uniec.component.entrant.RequestedEnrollmentDirectionStateAddEdit";
    String STATE_EXAM_EXPORT_PRINT = "ru.tandemservice.uniec.component.menu.StateExamExportPrint";
    String REQUESTED_ENROLLMENT_DIRECTIONS_CHANGE_PRIORITY = "ru.tandemservice.uniec.component.entrant.RequestedEnrollmentDirectionsChangePriority";
}
