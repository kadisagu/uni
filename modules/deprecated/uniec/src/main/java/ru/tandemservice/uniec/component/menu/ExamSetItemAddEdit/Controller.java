/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.menu.ExamSetItemAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.component.entrant.EntranceDisciplineAddEdit.EntranceDisciplineAddEditModel;

/**
 * @author vip_delete
 * @since 06.02.2009
 */
public class Controller extends ru.tandemservice.uniec.component.entrant.EntranceDisciplineAddEdit.Controller
{
    @Override
    public void onChangeType(IBusinessComponent component)
    {
        EntranceDisciplineAddEditModel model = getModel(component);
        if (UniecDefines.ENTRANCE_DISCIPLINE_KIND_PROFILE.equals(model.getEntranceDiscipline().getKind().getCode()))
        {
            model.getChoiceDisciplinesList().clear();
        }
    }
}
