/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec.component.entrant.EntrantDirectionPriorityTab;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.component.entrant.PriorityProfileEduOuEdit.PriorityProfileEduOuDTO;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.PriorityProfileEduOu;
import ru.tandemservice.uniec.entity.entrant.ProfileEducationOrgUnit;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

import java.util.*;

/**
 * @author Vasily Zhukov
 * @since 26.04.2012
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setEntrant(getNotNull(Entrant.class, model.getEntrant().getId()));
        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));

        CompensationType compensationType = (CompensationType)model.getSettings().get("compensationType");

        List<RequestedEnrollmentDirection> directions = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "e")
                .where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().entrant().fromAlias("e")), DQLExpressions.value(model.getEntrant())))
                .where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.compensationType().fromAlias("e")), DQLExpressions.value(compensationType)))
                .order(DQLExpressions.property(RequestedEnrollmentDirection.priorityPerEntrant().fromAlias("e")))
                .createStatement(getSession()).list();
        
        model.setDirectionList(directions);

        model.setProfileMap(new HashMap<RequestedEnrollmentDirection, List<PriorityProfileEduOuDTO>>());
        for (RequestedEnrollmentDirection direction : directions) {
            List<PriorityProfileEduOu> list = getList(PriorityProfileEduOu.class, PriorityProfileEduOu.requestedEnrollmentDirection(), direction, PriorityProfileEduOu.P_PRIORITY);
            List<ProfileEducationOrgUnit> additionalList = getList(ProfileEducationOrgUnit.class, ProfileEducationOrgUnit.enrollmentDirection(), direction.getEnrollmentDirection(), ProfileEducationOrgUnit.P_ID);

            Set<ProfileEducationOrgUnit> used = new HashSet<ProfileEducationOrgUnit>();
            List<PriorityProfileEduOuDTO> dtoList = new ArrayList<PriorityProfileEduOuDTO>();
            long id = -1;
            int priority = 1;
            for (PriorityProfileEduOu item : list)
            {
                PriorityProfileEduOuDTO dto = new PriorityProfileEduOuDTO();
                dto.setProfileEducationOrgUnit(item.getProfileEducationOrgUnit());
                dto.setPriority(priority++);
                dto.setId(id--); // id тут совсем не важен
                dtoList.add(dto);
                used.add(item.getProfileEducationOrgUnit());
            }

            for (ProfileEducationOrgUnit item : additionalList)
            {
                if (used.add(item))
                {
                    PriorityProfileEduOuDTO dto = new PriorityProfileEduOuDTO();
                    dto.setProfileEducationOrgUnit(item);
                    dto.setId(id--);
                    dtoList.add(dto);
                }
            }
            model.getProfileMap().put(direction, dtoList);
        }
    }
}
