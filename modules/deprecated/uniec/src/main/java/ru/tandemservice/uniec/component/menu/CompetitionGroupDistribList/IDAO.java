/* $Id: IDAO.java 8737 2009-06-26 11:27:38Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.menu.CompetitionGroupDistribList;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author vdanilov
 */
public interface IDAO extends IUniDao<Model>
{
    void delete(Model model, Long id);

    void refreshCompetitionGroupFilter(Model model);
}
