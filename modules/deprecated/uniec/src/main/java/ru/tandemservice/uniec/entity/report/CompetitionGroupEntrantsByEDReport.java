package ru.tandemservice.uniec.entity.report;

import org.tandemframework.core.view.formatter.DateFormatter;

import ru.tandemservice.uni.IStorableReport;
import ru.tandemservice.uniec.entity.report.gen.CompetitionGroupEntrantsByEDReportGen;

/**
 * Перечень абитуриентов по конкурсной группе (с детализацией по направлениям/специальностям
 */
public class CompetitionGroupEntrantsByEDReport extends CompetitionGroupEntrantsByEDReportGen implements IStorableReport
{
    public static final String P_PERIOD_TITLE = "periodTitle";

    public String getPeriodTitle()
    {
        if (getDateFrom() != null || getDateTo() != null)
        {
            return DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " — " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
        }
        return null;
    }
}