/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.logic;

import org.tandemframework.rtf.document.RtfDocument;

import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgDistribution;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistribution;

/**
 * @author Vasily Zhukov
 * @since 27.07.2011
 */
public interface IEcDistributionListPrint
{
    public static final SpringBeanCache<IEcDistributionListPrint> instance = new SpringBeanCache<IEcDistributionListPrint>(IEcDistributionListPrint.class.getName());

    /**
     * Печатает распределение по направлениям
     *
     * @param template     шаблон
     * @param distribution основное распределение
     * @return rtf-документ
     */
    RtfDocument getPrintFormForDistributionPerDirection(byte[] template, EcgDistribution distribution);

    /**
     * Печатает распределение по конкурсным группам
     *
     * @param template     шаблон
     * @param distribution основное или уточняющее распределение
     * @return rtf-документ
     */
    RtfDocument getPrintFormForDistributionPerCompetitionGroup(byte[] template, IEcgDistribution distribution);
}
