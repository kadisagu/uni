package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uniec.entity.entrant.DisciplineDateSetting;
import ru.tandemservice.uniec.entity.entrant.EntranceExamPhase;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Дата сдачи вступительного испытания
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntranceExamPhaseGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.EntranceExamPhase";
    public static final String ENTITY_NAME = "entranceExamPhase";
    public static final int VERSION_HASH = -1854008442;
    private static IEntityMeta ENTITY_META;

    public static final String L_SETTING = "setting";
    public static final String P_PASS_DATE = "passDate";
    public static final String P_EXAM_COMMISSION_MEMBERSHIP = "examCommissionMembership";

    private DisciplineDateSetting _setting;     // Даты сдачи вступительного испытания
    private Date _passDate;     // Дата сдачи
    private String _examCommissionMembership;     // ФИО экзаменаторов

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Даты сдачи вступительного испытания. Свойство не может быть null.
     */
    @NotNull
    public DisciplineDateSetting getSetting()
    {
        return _setting;
    }

    /**
     * @param setting Даты сдачи вступительного испытания. Свойство не может быть null.
     */
    public void setSetting(DisciplineDateSetting setting)
    {
        dirty(_setting, setting);
        _setting = setting;
    }

    /**
     * @return Дата сдачи. Свойство не может быть null.
     */
    @NotNull
    public Date getPassDate()
    {
        return _passDate;
    }

    /**
     * @param passDate Дата сдачи. Свойство не может быть null.
     */
    public void setPassDate(Date passDate)
    {
        dirty(_passDate, passDate);
        _passDate = passDate;
    }

    /**
     * @return ФИО экзаменаторов.
     */
    @Length(max=255)
    public String getExamCommissionMembership()
    {
        return _examCommissionMembership;
    }

    /**
     * @param examCommissionMembership ФИО экзаменаторов.
     */
    public void setExamCommissionMembership(String examCommissionMembership)
    {
        dirty(_examCommissionMembership, examCommissionMembership);
        _examCommissionMembership = examCommissionMembership;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntranceExamPhaseGen)
        {
            setSetting(((EntranceExamPhase)another).getSetting());
            setPassDate(((EntranceExamPhase)another).getPassDate());
            setExamCommissionMembership(((EntranceExamPhase)another).getExamCommissionMembership());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntranceExamPhaseGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntranceExamPhase.class;
        }

        public T newInstance()
        {
            return (T) new EntranceExamPhase();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "setting":
                    return obj.getSetting();
                case "passDate":
                    return obj.getPassDate();
                case "examCommissionMembership":
                    return obj.getExamCommissionMembership();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "setting":
                    obj.setSetting((DisciplineDateSetting) value);
                    return;
                case "passDate":
                    obj.setPassDate((Date) value);
                    return;
                case "examCommissionMembership":
                    obj.setExamCommissionMembership((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "setting":
                        return true;
                case "passDate":
                        return true;
                case "examCommissionMembership":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "setting":
                    return true;
                case "passDate":
                    return true;
                case "examCommissionMembership":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "setting":
                    return DisciplineDateSetting.class;
                case "passDate":
                    return Date.class;
                case "examCommissionMembership":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntranceExamPhase> _dslPath = new Path<EntranceExamPhase>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntranceExamPhase");
    }
            

    /**
     * @return Даты сдачи вступительного испытания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntranceExamPhase#getSetting()
     */
    public static DisciplineDateSetting.Path<DisciplineDateSetting> setting()
    {
        return _dslPath.setting();
    }

    /**
     * @return Дата сдачи. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntranceExamPhase#getPassDate()
     */
    public static PropertyPath<Date> passDate()
    {
        return _dslPath.passDate();
    }

    /**
     * @return ФИО экзаменаторов.
     * @see ru.tandemservice.uniec.entity.entrant.EntranceExamPhase#getExamCommissionMembership()
     */
    public static PropertyPath<String> examCommissionMembership()
    {
        return _dslPath.examCommissionMembership();
    }

    public static class Path<E extends EntranceExamPhase> extends EntityPath<E>
    {
        private DisciplineDateSetting.Path<DisciplineDateSetting> _setting;
        private PropertyPath<Date> _passDate;
        private PropertyPath<String> _examCommissionMembership;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Даты сдачи вступительного испытания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntranceExamPhase#getSetting()
     */
        public DisciplineDateSetting.Path<DisciplineDateSetting> setting()
        {
            if(_setting == null )
                _setting = new DisciplineDateSetting.Path<DisciplineDateSetting>(L_SETTING, this);
            return _setting;
        }

    /**
     * @return Дата сдачи. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntranceExamPhase#getPassDate()
     */
        public PropertyPath<Date> passDate()
        {
            if(_passDate == null )
                _passDate = new PropertyPath<Date>(EntranceExamPhaseGen.P_PASS_DATE, this);
            return _passDate;
        }

    /**
     * @return ФИО экзаменаторов.
     * @see ru.tandemservice.uniec.entity.entrant.EntranceExamPhase#getExamCommissionMembership()
     */
        public PropertyPath<String> examCommissionMembership()
        {
            if(_examCommissionMembership == null )
                _examCommissionMembership = new PropertyPath<String>(EntranceExamPhaseGen.P_EXAM_COMMISSION_MEMBERSHIP, this);
            return _examCommissionMembership;
        }

        public Class getEntityClass()
        {
            return EntranceExamPhase.class;
        }

        public String getEntityName()
        {
            return "entranceExamPhase";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
