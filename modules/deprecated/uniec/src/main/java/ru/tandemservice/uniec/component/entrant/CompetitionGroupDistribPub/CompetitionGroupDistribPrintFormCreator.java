// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.field.RtfField;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfElementFactory;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.node.IRtfText;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.add.ModelBase;
import ru.tandemservice.uniec.entity.ecg.EcgDistribObject;
import ru.tandemservice.uniec.entity.ecg.EcgDistribQuota;
import ru.tandemservice.uniec.entity.entrant.CompetitionGroup;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author oleyba
 * @since 31.05.2010
 */
public abstract class CompetitionGroupDistribPrintFormCreator extends UniBaseDao
{
    abstract String getCompetitionGroupTitle();

    @SuppressWarnings("deprecation")
    protected void buildReport(RtfDocument document, boolean showDirection, EcgDistribObject distrib, List<ModelBase.Row> entrantRateRows, String titleCaptionPostfix)
    {
        IUniBaseDao dao = UniDaoFacade.getCoreDao();

        final RtfTable detailTable = getTable(document.getElementList(), "details").getClone();

        final boolean budget = UniDefines.COMPENSATION_TYPE_BUDGET.equals(distrib.getCompensationType().getCode());
        final CompetitionGroup competitionGroup = distrib.getCompetitionGroup();
        final List<EcgDistribQuota> quotaList = dao.getList(EcgDistribQuota.class, EcgDistribQuota.distribution().s(), distrib);
        final String details = getDetailsString(budget) + titleCaptionPostfix;

        final MQBuilder dExB = new MQBuilder(EcgDistribObject.ENTITY_CLASS, "dEx", new String[]{"id"});
        dExB.setNeedDistinct(true);
        dExB.add(MQExpression.isNull("dEx", EcgDistribObject.parent().s()));
        dExB.add(MQExpression.eq("dEx", EcgDistribObject.approved().s(), Boolean.TRUE));
        dExB.add(MQExpression.eq("dEx", EcgDistribObject.compensationType().s(), distrib.getCompensationType()));
        dExB.add(MQExpression.eq("dEx", EcgDistribObject.competitionGroup().s(), distrib.getCompetitionGroup()));
        dExB.add(MQExpression.eq("dEx", EcgDistribObject.secondHighAdmission().s(), distrib.isSecondHighAdmission()));
        // dExB.add(MQExpression.lessOrEq("dEx", EcgDistribObjectGen.P_REGISTRATION_DATE, model.getDistrib().getRegistrationDate()));
        dExB.add(MQExpression.lessOrEq("dEx", "id", distrib.getId()));

        final String stage = "Стадия распределения " + (dExB.getResultCount(getSession()));
        {
            final RtfInjectModifier injectModifier = new RtfInjectModifier();
            injectModifier.put("details", details);
            injectModifier.put("completeProgramDetails", getDetails(quotaList));
            injectModifier.put("competitionGroup", getCompetitionGroupTitle());
            injectModifier.put("competitionGroupNumber", competitionGroup.getTitle());
            injectModifier.put("column", (budget) ? "Вид конкурса" : "Наличие льгот");
            injectModifier.put("stage", stage);
            injectModifier.modify(document);
        }

        final Map<EnrollmentDirection, List<String[]>> byDirections = SafeMap.get(key -> new ArrayList<>());

        final List<String[]> result = new ArrayList<>(entrantRateRows.size());
        for (final ModelBase.Row row : entrantRateRows)
        {
            final EnrollmentDirection enrollmentDirection = null == row.getDirection() ? null : row.getDirection().getEnrollmentDirection();
            final String title = row.getTitle();
            final String competitionKindOrBenifits = budget ? row.getCompetitionKind() : row.getBenefitsExistence();
            final String totalMark = row.getTotalMark();
            final String profileEduInstitution = row.isGraduatedProfileEduInstitution() ? "да" : "";
            final String certificateAverageMark = row.getCertificateAverageMark();
            final String directionsPriorities = row.getDirectionsPriorities();
            final String documentOriginals = row.isOriginalDocumentHandedIn() ? "оригиналы" : "копии";

            if (null != enrollmentDirection)
            {
                final List<String[]> enrollmentDirectionResult = byDirections.get(enrollmentDirection);
                enrollmentDirectionResult.add(new String[]{
                        String.valueOf(1 + enrollmentDirectionResult.size()),
                        title,
                        competitionKindOrBenifits,
                        totalMark,
                        profileEduInstitution,
                        certificateAverageMark,
                        directionsPriorities,
                        documentOriginals
                });
            }

            result.add(
                    showDirection ? new String[]{
                            String.valueOf(1 + result.size()),
                            title,
                            competitionKindOrBenifits,
                            totalMark,
                            profileEduInstitution,
                            certificateAverageMark,
                            directionsPriorities,
                            (null == enrollmentDirection ? "" : enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool().getShortTitle()),
                            documentOriginals
                    } : new String[]{
                            String.valueOf(1 + result.size()),
                            title,
                            competitionKindOrBenifits,
                            totalMark,
                            profileEduInstitution,
                            certificateAverageMark,
                            directionsPriorities,
                            documentOriginals
                    }
            );
        }

        final RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T1", result.toArray(new String[result.size()][]));
        tableModifier.modify(document);

        final RtfTable T2Orig = getTable(document.getElementList(), "T2");
        final RtfTable T2sOrig = getTable(document.getElementList(), "T2S");
        {
            final RtfInjectModifier injectModifier = new RtfInjectModifier();
            injectModifier.put("T2S", "");
            injectModifier.modify(Collections.<IRtfElement>singletonList(T2sOrig));
        }

        if (null != T2Orig)
        {
            final IRtfElementFactory elementFactory = RtfBean.getElementFactory();
            for (final EcgDistribQuota quota : quotaList)
            {
                final EnrollmentDirection direction = quota.getDirection();
                final List<String[]> enrollmentDirectionResult = byDirections.get(direction);
                if (!enrollmentDirectionResult.isEmpty())
                {
                    final IRtfText lineBreak = elementFactory.createRtfText("\\par\\pard\n\n");
                    lineBreak.setRaw(true);

                    {
                        final IRtfText pageBreak = elementFactory.createRtfText("\n\n\\page\\pard\n\n");
                        pageBreak.setRaw(true);
                        document.getElementList().add(lineBreak);
                        document.getElementList().add(pageBreak);
                        document.getElementList().add(lineBreak);

                        final IRtfGroup group = RtfBean.getElementFactory().createRtfGroup();
                        final RtfTable tbl = detailTable.getClone();
                        final EducationLevelsHighSchool eduHS = direction.getEducationOrgUnit().getEducationLevelHighSchool();
                        final RtfInjectModifier injectModifier = new RtfInjectModifier();
                        injectModifier.put("details", details);
                        injectModifier.put("completeProgramDetails", ((eduHS.getEducationLevel().getLevelType().isSpecialityPrintTitleMode() ? "По специальности: " : "По направлению: ") + (eduHS.getPrintTitle() + " (" + eduHS.getShortTitle() + ")")));
                        injectModifier.put("competitionGroup", getCompetitionGroupTitle());
                        injectModifier.put("competitionGroupNumber", competitionGroup.getTitle());
                        injectModifier.put("stage", stage);
                        injectModifier.modify(Collections.<IRtfElement>singletonList(tbl));

                        group.getElementList().add(tbl);
                        document.getElementList().add(group);
                        document.getElementList().add(lineBreak);
                    }

                    {
                        final IRtfGroup group = RtfBean.getElementFactory().createRtfGroup();
                        final RtfTable T2 = T2Orig.getClone();
                        final RtfTableModifier table2Modifier = new RtfTableModifier();
                        table2Modifier.put("T2", enrollmentDirectionResult.toArray(new String[enrollmentDirectionResult.size()][]));
                        table2Modifier.modify(Collections.<IRtfElement>singletonList(T2));
                        group.getElementList().add(T2);
                        document.getElementList().add(group);
                        document.getElementList().add(lineBreak);
                    }

                    {
                        final IRtfGroup group = RtfBean.getElementFactory().createRtfGroup();
                        if (null != T2sOrig)
                        {
                            final RtfTable T2s = T2sOrig.getClone();
                            group.getElementList().add(T2s);
                        }
                        document.getElementList().add(group);
                        document.getElementList().add(lineBreak);
                    }

                }
            }
            T2Orig.setRowList(new ArrayList<>());
        }
        if (null != T2sOrig)
            T2sOrig.setRowList(new ArrayList<>());
    }

    private String getDetailsString(final boolean budget)
    {
        return
            "поступающих на места" +
            (budget ? ", финансируемые из федерального бюджета, по очной форме обучения" : " с оплатой стоимости обучения по очной форме обучения") +
            " с выделением списка лиц, рекомендованных к зачислению";
    }

    private List<IRtfElement> getDetails(final List<EcgDistribQuota> quotaList)
    {
        final IRtfElementFactory elementFactory = RtfBean.getElementFactory();
        final List<IRtfElement> result = new ArrayList<>();
        final List<String> directionsTitles = new ArrayList<>();
        final List<String> specialitiesTitles = new ArrayList<>();

        // рассортировываем
        for (final EcgDistribQuota quota : quotaList)
        {
            final EnrollmentDirection direction = quota.getDirection();
            final EducationLevelsHighSchool eduHS = direction.getEducationOrgUnit().getEducationLevelHighSchool();
            (eduHS.getEducationLevel().getLevelType().isSpecialityPrintTitleMode() ? specialitiesTitles : directionsTitles).add(eduHS.getPrintTitle() + " (" + eduHS.getShortTitle() + ")");
        }

        // выводим направления
        if (!directionsTitles.isEmpty())
            result.add(elementFactory.createRtfText("по направлениям: " + StringUtils.join(directionsTitles, ", ")));

        // выводим специальности
        if (!specialitiesTitles.isEmpty())
        {
            if (!result.isEmpty())
                result.add(elementFactory.createRtfControl(IRtfData.PAR));
            result.add(elementFactory.createRtfText("по специальностям: " + StringUtils.join(specialitiesTitles, ", ")));
        }

        return result;
    }

    private static RtfTable getTable(final List<IRtfElement> elementList, final String key)
    {
        for (final IRtfElement element : elementList)
        {
            if (element instanceof IRtfGroup)
            {
                final RtfTable table = getTable(((IRtfGroup) element).getElementList(), key);
                if (null != table)
                    return table;

            } else if (element instanceof RtfTable)
            {
                final RtfTable table = (RtfTable) element;
                final RtfRow r = getTableRow(table, key);
                if (null != r)
                    return table;
            }
        }
        return null;
    }

    private static RtfRow getTableRow(final RtfTable table, final String key)
    {
        for (final RtfRow row : table.getRowList())
        {
            final IRtfElement s = getRowtElement(row, key);
            if (null != s)
                return row;
        }
        return null;
    }

    private static IRtfElement getRowtElement(final RtfRow row, final String key)
    {
        for (final RtfCell cell : row.getCellList())
            for (final IRtfElement e : cell.getElementList())
                if (e instanceof RtfField)
                {
                    final RtfField field = (RtfField) e;
                    if (field.isMark() && key.equals(field.getFieldName()))
                        return e;
                }
        return null;
    }
}
