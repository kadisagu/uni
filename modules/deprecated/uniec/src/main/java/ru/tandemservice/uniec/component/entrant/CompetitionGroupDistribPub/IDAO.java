package ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub;

import ru.tandemservice.uni.dao.IPrepareable;

/**
 * @author vdanilov
 */
public interface IDAO extends IPrepareable<Model> {
	void prepareQuotaListDataSource(Model model);
	void doApprove(Model model);
}
