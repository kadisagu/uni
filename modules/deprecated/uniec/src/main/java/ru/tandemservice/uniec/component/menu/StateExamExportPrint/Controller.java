/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.menu.StateExamExportPrint;

import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uniec.UniecDefines;

import java.io.UnsupportedEncodingException;
import java.util.Date;

/**
 * @author vip_delete
 * @since 06.04.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);
        try { BusinessComponentUtils.downloadDocument(buildDocumentRenderer(component), true); }
        catch(Throwable t) { throw CoreExceptionUtils.getRuntimeException(t); }
    }

    public IDocumentRenderer buildDocumentRenderer(IBusinessComponent component) throws UnsupportedEncodingException
    {
        final Model model = getModel(component);
        deactivate(component);

        return new CommonBaseRenderer().xls().fileName(getReportFileName(model)).document(model.getExportData().toString().getBytes("CP1251"));
    }

    private String getReportFileName(Model model)
    {
        final String datePrefix = new DateFormatter("ddMMyyyy").format(new Date());

        switch (model.getExportType())
        {
            case UniecDefines.EXPORT_TYPE_BY_CERTIFICATE_NUMBER_WITHOUT_FIO:
                return datePrefix + "_FBS_Certificate_without_fio.csv";
            case UniecDefines.EXPORT_TYPE_BY_IDENTITY_CARD_WITHOUT_FIO:
                return datePrefix + "_FBS_PASSPORT_without_fio.csv";
            case UniecDefines.EXPORT_TYPE_BY_CERTIFICATE_NUMBER:
                return datePrefix + "_FBS_Certificate.csv";
            case UniecDefines.EXPORT_TYPE_BY_IDENTITY_CARD:
                return datePrefix + "_FBS_PASSPORT.csv";
            case UniecDefines.EXPORT_TYPE_BY_CERTIFICATE_NUMBER_WITH_FIO:
                return datePrefix + "_FBS_Certificate_with_fio.csv";
            case UniecDefines.EXPORT_TYPE_BY_FIO_WITH_DOC:
                return datePrefix + "_FBS_FIO_N_DOC.csv";
        }
        throw new RuntimeException("Unknown export type");
    }

}
