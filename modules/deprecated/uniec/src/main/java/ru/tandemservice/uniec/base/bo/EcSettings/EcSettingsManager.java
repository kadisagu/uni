/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcSettings;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniec.base.bo.EcSettings.logic.EcSettingsIndividualProgressDAO;
import ru.tandemservice.uniec.base.bo.EcSettings.logic.IEcSettingsIndividualProgressDAO;
import ru.tandemservice.uniec.base.bo.EcSettings.logic.ITemplate2CampaignRelDAO;
import ru.tandemservice.uniec.base.bo.EcSettings.logic.Template2CampaignRelDAO;

/**
 * @author Ekaterina Zvereva
 * @since 31.03.2015
 */
@Configuration
public class EcSettingsManager extends BusinessObjectManager
{
    public static final String EDUCATION_YEAR_PARAM = "educationYear";

    public static EcSettingsManager instance()
    {
        return instance(EcSettingsManager.class);
    }

    @Bean
    public IEcSettingsIndividualProgressDAO individualProgressDAO()
    {
        return new EcSettingsIndividualProgressDAO();
    }


    @Bean
    public ITemplate2CampaignRelDAO template2CampaignRelDAO()
    {
        return new Template2CampaignRelDAO();
    }
}