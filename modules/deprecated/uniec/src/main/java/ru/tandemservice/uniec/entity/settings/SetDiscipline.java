package ru.tandemservice.uniec.entity.settings;

import java.util.Comparator;
import java.util.List;

import org.tandemframework.core.common.ITitled;

import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.settings.gen.SetDisciplineGen;

public abstract class SetDiscipline extends SetDisciplineGen implements ITitled
{
    public static final Comparator<SetDiscipline> COMPARATOR = new Comparator<SetDiscipline>() {
        @Override public int compare(SetDiscipline o1, SetDiscipline o2) {
            int result = EnrollmentCampaign.COMPARATOR.compare(o1.getEnrollmentCampaign(), o2.getEnrollmentCampaign());
            if (0 != result) { return result; }
            return o1.getTitle().compareTo(o2.getTitle());
        }
    };


    public static final String P_TITLE = "title";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_FULL_TITLE = "fullTitle";

    public abstract List<Discipline2RealizationWayRelation> getDisciplines();

    public abstract String getShortTitle();

    public abstract String getFullTitle();


}