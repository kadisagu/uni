/* $Id$ */
package ru.tandemservice.uniec.component.listextract.ent1.e2.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.AbstractListExtractPubModel;
import ru.tandemservice.uniec.entity.orders.SplitContractBachelorEntrantsStuListExtract;

/**
 * @author Dmitry Seleznev
 * @since 21.08.2012
 */
public class Model extends AbstractListExtractPubModel<SplitContractBachelorEntrantsStuListExtract>
{
}