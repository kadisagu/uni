/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.EntrantRequestBatchAddEdit;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StudentCategory;

import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;

import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.settings.UseExternalOrgUnitForTA;

/**
 * @author vip_delete
 * @since 07.06.2009
 */
@Input(keys = {"entrantRequestId", "entrantId"}, bindings = {"entrantRequest.id", "entrantRequest.entrant.id"})
public class Model
{
    private EntrantRequest _entrantRequest = new EntrantRequest();

    {
        _entrantRequest.setEntrant(new Entrant());
    }

    private boolean _addForm;
    private boolean _editForm;
    private boolean _regDateDisabled;
    private boolean _stateExamRestriction;
    private boolean _targetAdmission;
    private Integer _regNumber;

    private ISelectModel _formativeOrgUnitModel;
    private ISelectModel _territorialOrgUnitModel;
    private ISelectModel _educationLevelsHighSchoolModel;
    private ISelectModel _developFormModel;
    private List<CompensationType> _compensationTypeList;
    private List<StudentCategory> _studentCategoryList;
    private ISelectModel _competitionKindModel;

    private OrgUnit _formativeOrgUnit;
    private OrgUnit _territorialOrgUnit;
    private EducationLevelsHighSchool _educationLevelsHighSchool;
    private DevelopForm _developForm;
    private CompensationType _compensationType;
    private StudentCategory _studentCategory;
    private CompetitionKind _competitionKind;

    private DynamicListDataSource<EnrollmentDirection> _possibleDirectionDataSource;
    private DynamicListDataSource<RequestedEnrollmentDirection> _selectedRequestedEnrollmentDirectionDataSource;

    private List<RequestedEnrollmentDirection> _selectedRequestedEnrollmentDirectionList = new ArrayList<RequestedEnrollmentDirection>();
    private List<RequestedEnrollmentDirection> _forDelete = new ArrayList<RequestedEnrollmentDirection>();
    private Set<Long> _existedRequestedEnrollmentDirectionIds = new HashSet<Long>();

    private Set<Long> _canBeSelectedDirectionIds = new HashSet<Long>();
    private boolean _regNumberVisible;
    private ISelectModel _externalOrgUnitModel;
    private ExternalOrgUnit _externalOrgUnit;
    private UseExternalOrgUnitForTA _useExternalOrgUnitForTA;

    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _entrantRequest.getEntrant().getEnrollmentCampaign();
    }

    public boolean isUseExternalOrgUnit()
    {
        return _targetAdmission && _useExternalOrgUnitForTA != null;
    }

    // Getters & Setters

    public EntrantRequest getEntrantRequest()
    {
        return _entrantRequest;
    }

    public void setEntrantRequest(EntrantRequest entrantRequest)
    {
        _entrantRequest = entrantRequest;
    }

    public boolean isAddForm()
    {
        return _addForm;
    }

    public void setAddForm(boolean addForm)
    {
        _addForm = addForm;
    }

    public boolean isEditForm()
    {
        return _editForm;
    }

    public void setEditForm(boolean editForm)
    {
        _editForm = editForm;
    }

    public boolean isRegDateDisabled()
    {
        return _regDateDisabled;
    }

    public void setRegDateDisabled(boolean regDateDisabled)
    {
        _regDateDisabled = regDateDisabled;
    }

    public boolean isStateExamRestriction()
    {
        return _stateExamRestriction;
    }

    public void setStateExamRestriction(boolean stateExamRestriction)
    {
        _stateExamRestriction = stateExamRestriction;
    }

    public boolean isTargetAdmission()
    {
        return _targetAdmission;
    }

    public void setTargetAdmission(boolean targetAdmission)
    {
        _targetAdmission = targetAdmission;
    }

    public Integer getRegNumber()
    {
        return _regNumber;
    }

    public void setRegNumber(Integer regNumber)
    {
        _regNumber = regNumber;
    }

    public ISelectModel getFormativeOrgUnitModel()
    {
        return _formativeOrgUnitModel;
    }

    public void setFormativeOrgUnitModel(ISelectModel formativeOrgUnitModel)
    {
        _formativeOrgUnitModel = formativeOrgUnitModel;
    }

    public ISelectModel getTerritorialOrgUnitModel()
    {
        return _territorialOrgUnitModel;
    }

    public void setTerritorialOrgUnitModel(ISelectModel territorialOrgUnitModel)
    {
        _territorialOrgUnitModel = territorialOrgUnitModel;
    }

    public ISelectModel getEducationLevelsHighSchoolModel()
    {
        return _educationLevelsHighSchoolModel;
    }

    public void setEducationLevelsHighSchoolModel(ISelectModel educationLevelsHighSchoolModel)
    {
        _educationLevelsHighSchoolModel = educationLevelsHighSchoolModel;
    }

    public ISelectModel getDevelopFormModel()
    {
        return _developFormModel;
    }

    public void setDevelopFormModel(ISelectModel developFormModel)
    {
        _developFormModel = developFormModel;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public List<StudentCategory> getStudentCategoryList()
    {
        return _studentCategoryList;
    }

    public void setStudentCategoryList(List<StudentCategory> studentCategoryList)
    {
        _studentCategoryList = studentCategoryList;
    }

    public ISelectModel getCompetitionKindModel()
    {
        return _competitionKindModel;
    }

    public void setCompetitionKindModel(ISelectModel competitionKindModel)
    {
        _competitionKindModel = competitionKindModel;
    }

    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        _formativeOrgUnit = formativeOrgUnit;
    }

    public OrgUnit getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    public void setTerritorialOrgUnit(OrgUnit territorialOrgUnit)
    {
        _territorialOrgUnit = territorialOrgUnit;
    }

    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _educationLevelsHighSchool;
    }

    public void setEducationLevelsHighSchool(EducationLevelsHighSchool educationLevelsHighSchool)
    {
        _educationLevelsHighSchool = educationLevelsHighSchool;
    }

    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public void setDevelopForm(DevelopForm developForm)
    {
        _developForm = developForm;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

    public StudentCategory getStudentCategory()
    {
        return _studentCategory;
    }

    public void setStudentCategory(StudentCategory studentCategory)
    {
        _studentCategory = studentCategory;
    }

    public CompetitionKind getCompetitionKind()
    {
        return _competitionKind;
    }

    public void setCompetitionKind(CompetitionKind competitionKind)
    {
        _competitionKind = competitionKind;
    }

    public DynamicListDataSource<EnrollmentDirection> getPossibleDirectionDataSource()
    {
        return _possibleDirectionDataSource;
    }

    public void setPossibleDirectionDataSource(DynamicListDataSource<EnrollmentDirection> possibleDirectionDataSource)
    {
        _possibleDirectionDataSource = possibleDirectionDataSource;
    }

    public DynamicListDataSource<RequestedEnrollmentDirection> getSelectedRequestedEnrollmentDirectionDataSource()
    {
        return _selectedRequestedEnrollmentDirectionDataSource;
    }

    public void setSelectedRequestedEnrollmentDirectionDataSource(DynamicListDataSource<RequestedEnrollmentDirection> selectedRequestedEnrollmentDirectionDataSource)
    {
        _selectedRequestedEnrollmentDirectionDataSource = selectedRequestedEnrollmentDirectionDataSource;
    }

    public List<RequestedEnrollmentDirection> getSelectedRequestedEnrollmentDirectionList()
    {
        return _selectedRequestedEnrollmentDirectionList;
    }

    public void setSelectedRequestedEnrollmentDirectionList(List<RequestedEnrollmentDirection> selectedRequestedEnrollmentDirectionList)
    {
        _selectedRequestedEnrollmentDirectionList = selectedRequestedEnrollmentDirectionList;
    }

    public List<RequestedEnrollmentDirection> getForDelete()
    {
        return _forDelete;
    }

    public void setForDelete(List<RequestedEnrollmentDirection> forDelete)
    {
        _forDelete = forDelete;
    }

    public Set<Long> getExistedRequestedEnrollmentDirectionIds()
    {
        return _existedRequestedEnrollmentDirectionIds;
    }

    public void setExistedRequestedEnrollmentDirectionIds(Set<Long> existedRequestedEnrollmentDirectionIds)
    {
        _existedRequestedEnrollmentDirectionIds = existedRequestedEnrollmentDirectionIds;
    }

    public Set<Long> getCanBeSelectedDirectionIds()
    {
        return _canBeSelectedDirectionIds;
    }

    public void setCanBeSelectedDirectionIds(Set<Long> canBeSelectedDirectionIds)
    {
        _canBeSelectedDirectionIds = canBeSelectedDirectionIds;
    }

    public boolean isRegNumberVisible()
    {
        return _regNumberVisible;
    }

    public void setRegNumberVisible(boolean regNumberVisible)
    {
        _regNumberVisible = regNumberVisible;
    }

    public ISelectModel getExternalOrgUnitModel()
    {
        return _externalOrgUnitModel;
    }

    public void setExternalOrgUnitModel(ISelectModel externalOrgUnitModel)
    {
        _externalOrgUnitModel = externalOrgUnitModel;
    }

    public ExternalOrgUnit getExternalOrgUnit()
    {
        return _externalOrgUnit;
    }

    public void setExternalOrgUnit(ExternalOrgUnit externalOrgUnit)
    {
        _externalOrgUnit = externalOrgUnit;
    }

    public UseExternalOrgUnitForTA getUseExternalOrgUnitForTA()
    {
        return _useExternalOrgUnitForTA;
    }

    public void setUseExternalOrgUnitForTA(UseExternalOrgUnitForTA useExternalOrgUnitForTA)
    {
        _useExternalOrgUnitForTA = useExternalOrgUnitForTA;
    }
}
