/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.menu.ExamSetItemAddEdit;

import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.uniec.component.entrant.EntranceDisciplineAddEdit.EntranceDisciplineAddEditModel;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineKind;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline;
import ru.tandemservice.uniec.entity.examset.ExamSet;
import ru.tandemservice.uniec.entity.examset.ExamSetItem;
import ru.tandemservice.uniec.entity.settings.SetDiscipline;
import ru.tandemservice.uniec.util.ExamSetUtil;

import java.util.List;
import java.util.ListIterator;

/**
 * @author vip_delete
 * @since 06.02.2009
 */
public class DAO extends ru.tandemservice.uniec.component.entrant.EntranceDisciplineAddEdit.DAO implements IDAO
{
    @Override
    public void prepare(EntranceDisciplineAddEditModel childModel)
    {
        ExamSetItemAddEditModel model = (ExamSetItemAddEditModel) childModel;

        ExamSet examSet = ExamSetUtil.getExamSet(model.getExamSetId());
        if (examSet == null)
        {
            throw new ApplicationException("Выбранный набор уже не существует.");
        }
        model.setExamSet(examSet);

        if (model.getExamSetItemId() != null)
        {
            // форма редактирования
            model.setEntranceDisciplineList(ExamSetUtil.getEntranceDiscipline(model.getExamSet(), model.getExamSetItemId()));
            // для базовой модели
            model.setEntranceDiscipline(model.getEntranceDisciplineList().get(0));
        }
        else
        {
            // форма добавления
            model.setExamSetItemId(null);
            model.setEntranceEducationOrgUnitList(model.getExamSet().getList());
            // для базовой модели
            EntranceDiscipline entranceDiscipline = model.getEntranceDiscipline();
            entranceDiscipline.setEnrollmentDirection(model.getEntranceEducationOrgUnitList().get(0));
            entranceDiscipline.setStudentCategory(examSet.getStudentCategory());
        }
        super.prepare(model);
    }

    @Override
    public void update(EntranceDisciplineAddEditModel childModel)
    {
        ExamSetItemAddEditModel model = (ExamSetItemAddEditModel) childModel;

        boolean budget = model.getEntranceDiscipline().isBudget();
        boolean contract = model.getEntranceDiscipline().isContract();
        EntranceDisciplineKind kind = model.getEntranceDiscipline().getKind();
        EntranceDisciplineType type = model.getEntranceDiscipline().getType();
        SetDiscipline discipline = model.getEntranceDiscipline().getDiscipline();

        ExamSet examSet = model.getExamSet();
        List<ExamSetItem> examSetItems = examSet.getSetItemList();

        ExamSetItem examSetItem = new ExamSetItem(0L, budget, contract, kind, type, model.getEntranceDiscipline().getDiscipline(), model.getChoiceDisciplinesList());
        
        // если испытание новое, то добавляем в набор
        if (model.getExamSetItemId() == null)
        {
            examSetItems.add(examSetItem);
        }
        // ищем и заменяем старое вступительное испытание
        else
        {
            for (ListIterator<ExamSetItem> iterator = examSetItems.listIterator(); iterator.hasNext();)
            {
                ExamSetItem item = iterator.next();
                if (item.getExamSetItemId().equals(model.getExamSetItemId()))
                {
                    iterator.remove();
                    iterator.add(examSetItem);
                }
            }
        }
        String newExamSetId = new ExamSet(0L, examSet.getEnrollmentCampaign(), examSet.getStudentCategory(), examSetItems).getExamSetId();
        model.setExamSetId(newExamSetId);

        if (model.getEntranceDisciplineList() != null)
        {
            // форма редактирования
            for (EntranceDiscipline entranceDiscipline : model.getEntranceDisciplineList())
            {
                entranceDiscipline.setBudget(budget);
                entranceDiscipline.setContract(contract);
                entranceDiscipline.setKind(kind);
                entranceDiscipline.setType(type);
                entranceDiscipline.setDiscipline(discipline);

                model.setEntranceDiscipline(entranceDiscipline);
                super.update(model);
            }
        }
        else
        {
            // форма добавления
            for (EnrollmentDirection enrollmentDirection : model.getEntranceEducationOrgUnitList())
            {
                EntranceDiscipline entranceDiscipline = new EntranceDiscipline();
                entranceDiscipline.setBudget(budget);
                entranceDiscipline.setContract(contract);
                entranceDiscipline.setKind(kind);
                entranceDiscipline.setType(type);
                entranceDiscipline.setStudentCategory(examSet.getStudentCategory());
                entranceDiscipline.setDiscipline(discipline);
                entranceDiscipline.setEnrollmentDirection(enrollmentDirection);

                model.setEntranceDiscipline(entranceDiscipline);
                super.update(model);
            }
        }
    }
}
