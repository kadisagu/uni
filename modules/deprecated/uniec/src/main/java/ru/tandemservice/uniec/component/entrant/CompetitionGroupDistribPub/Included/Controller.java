package ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.Included;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.AbstractListDataSource;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.gen.EducationOrgUnitGen;
import ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.add.ModelBase;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.entrant.gen.EnrollmentDirectionGen;
import ru.tandemservice.uniec.entity.entrant.gen.RequestedEnrollmentDirectionGen;

import java.util.Date;

/**
 * @author vdanilov
 */
public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);

        prepareDataSource(model, model.getEntrantRateRowsDataSource());
    }

    private void prepareDataSource(final Model model, final AbstractListDataSource<ModelBase.Row> ds) {
        ds.getColumns().clear();

        final IMergeRowIdResolver competitionKindRowIdResolver = entity -> {
            final ModelBase.Row row = ((ModelBase.Row)entity);
            return (row.isTargetAdmission() ? "0" : String.valueOf(row.getRow().getCompetitionKind().getId()));
        };
        final IMergeRowIdResolver totalMarkRowIdResolver = entity -> competitionKindRowIdResolver.getMergeRowId(entity)+"-"+((ModelBase.Row)entity).getTotalMark();
        //		final IMergeRowIdResolver profileEduInstitutionRowIdResolver = new IMergeRowIdResolver() {
        //			@Override public String getMergeRowId(final IEntity entity) {
        //				return totalMarkRowIdResolver.getMergeRowId(entity)+"-"+Boolean.toString(((ModelBase.Row)entity).isGraduatedProfileEduInstitution());
        //			}
        //		};
        //		final IMergeRowIdResolver certificateAverageMarkRowIdResolver = new IMergeRowIdResolver() {
        //			@Override public String getMergeRowId(final IEntity entity) {
        //				return profileEduInstitutionRowIdResolver.getMergeRowId(entity)+"-"+((ModelBase.Row)entity).getCertificateAverageMark();
        //			}
        //		};

        if (UniDefines.COMPENSATION_TYPE_BUDGET.equals(model.getDistrib().getCompensationType().getCode())) {
            ds.addColumn(new SimpleColumn("Вид конкурса", "competitionKind").setWidth(1).setOrderable(false).setClickable(false).setMergeRows(true).setMergeRowIdResolver(competitionKindRowIdResolver));
        } else {
            ds.addColumn(new SimpleColumn("Льготы", "benefitsExistence").setWidth(1).setOrderable(false).setClickable(false).setMergeRows(true).setMergeRowIdResolver(competitionKindRowIdResolver));
        }
        if (UniecDAOFacade.getTargetAdmissionDao().getTargetAdmissionKindOptionList(model.getDistrib().getCompetitionGroup().getEnrollmentCampaign()).size() > 1)
            ds.addColumn(new SimpleColumn("Вид ЦП", "row.targetAdmissionKind.fullHierarhyTitle").setClickable(false).setOrderable(false).setMergeRowIdResolver(competitionKindRowIdResolver));
        ds.addColumn(new SimpleColumn("Сумма баллов", "totalMark").setWidth(5).setOrderable(false).setClickable(false).setMergeRows(true).setMergeRowIdResolver(totalMarkRowIdResolver));
        ds.addColumn(new SimpleColumn("*", "graduatedProfileEduInstitution", ModelBase.BooleanFormatter.INSTANCE).setWidth(1).setOrderable(false).setClickable(false)/*.setMergeRows(true).setMergeRowIdResolver(profileEduInstitutionRowIdResolver)*/);
        ds.addColumn(new SimpleColumn("Ср. балл аттестата", "certificateAverageMark").setWidth(3).setOrderable(false).setClickable(false)/*.setMergeRows(true).setMergeRowIdResolver(certificateAverageMarkRowIdResolver)*/);
        ds.addColumn(new SimpleColumn("ФИО", "title").setOrderable(false).setClickable(false));

        ds.addColumn(new SimpleColumn("Приоритеты", "directionsPriorities").setOrderable(false).setClickable(false));
        ds.addColumn(new SimpleColumn("Направление подготовки (специальность) для зачисления", "direction."+RequestedEnrollmentDirectionGen.L_ENROLLMENT_DIRECTION+"."+EnrollmentDirectionGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnitGen.L_EDUCATION_LEVEL_HIGH_SCHOOL+"."+EducationLevelsHighSchool.P_DISPLAYABLE_TITLE).setOrderable(false).setClickable(false));
        ds.addColumn(new SimpleColumn("Форма", "direction."+RequestedEnrollmentDirectionGen.L_ENROLLMENT_DIRECTION+"."+EnrollmentDirectionGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnitGen.L_DEVELOP_FORM+".title").setOrderable(false).setClickable(false));
        ds.addColumn(new SimpleColumn("Условие", "direction."+RequestedEnrollmentDirectionGen.L_ENROLLMENT_DIRECTION+"."+EnrollmentDirectionGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnitGen.L_DEVELOP_CONDITION+".title").setOrderable(false).setClickable(false));
        ds.addColumn(new SimpleColumn("Оригиналы документов", "originalDocumentHandedIn", ModelBase.CopiesFormatter.INSTANCE).setWidth(1).setOrderable(false).setClickable(false));
        ds.addColumn(new SimpleColumn("Состояние", "direction.state.title").setWidth(5).setOrderable(false).setClickable(false));

        ds.addColumn(new ActionColumn("Исключить из распределения", "delete", "onClickDelete", "Исключить абитуриента «{0}» из выборки?", "title").setWidth(1)
                             .setDisableHandler(entity -> model.getDistrib().isApproved()));
    }

    public void onClickPrintModel(final IBusinessComponent component) {
        final Model model = getModel(component);
        final byte[] context = getDao().createPrintModelReport(model);
        final String compType = (UniDefines.COMPENSATION_TYPE_BUDGET.equals(model.getDistrib().getCompensationType().getCode()) ? "бюджет" : "по договору");
        final Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(context, "Список лиц рекомендованных к зачислению (гр "+model.getDistrib().getCompetitionGroup().getTitle()+") "+compType+" от "+DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date())+".rtf");
        activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new ParametersMap().add("id", id).add("zip", Boolean.FALSE)));
    }

    public void onClickPrintReserved(final IBusinessComponent component) {
        final Model model = getModel(component);
        final byte[] context = getDao().createPrintReservedReport(model);
        final String compType = (UniDefines.COMPENSATION_TYPE_BUDGET.equals(model.getDistrib().getCompensationType().getCode()) ? "бюджет" : "по договору");
        final Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(context, "Резерв (гр "+model.getDistrib().getCompetitionGroup().getTitle()+") "+compType+" на "+DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date())+".rtf");
        activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new ParametersMap().add("id", id).add("zip", Boolean.FALSE)));
    }

    public void onClickPrintList(final IBusinessComponent component) {
        final Model model = getModel(component);
        final byte[] context = getDao().createPrintListReport(model);
        final Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(context, "Результаты распределения.rtf");
        activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new ParametersMap().add("id", id).add("zip", Boolean.FALSE)));
    }

    public void onClickDelete(final IBusinessComponent component) {
        getDao().delete(getModel(component), (Long)component.getListenerParameter());
    }
}
