/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.print.entrant;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.print.ReportPrintColumnIndex;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.print.ReportPrintListColumn;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.print.ReportPrintPathColumn;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.EcReportPersonAddUI;
import ru.tandemservice.uniec.base.bo.EcReportPerson.ui.util.EcReportPersonUtil;
import ru.tandemservice.uniec.entity.catalog.EnrollmentRecommendation;
import ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantIndividualProgress;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Vasily Zhukov
 * @since 31.08.2011
 */
public class EntrantPrintBlock implements IReportPrintBlock
{
    private IReportPrintCheckbox _block = new ReportPrintCheckbox();
    private IReportPrintCheckbox _enrollmentCampaign = new ReportPrintCheckbox();
    private IReportPrintCheckbox _entrantPersonalNumber = new ReportPrintCheckbox();
    private IReportPrintCheckbox _state = new ReportPrintCheckbox();
    private IReportPrintCheckbox _customState = new ReportPrintCheckbox();
    private IReportPrintCheckbox _recommendation = new ReportPrintCheckbox();
    private IReportPrintCheckbox _registration = new ReportPrintCheckbox();

    @Override
    public String getCheckJS()
    {
        List<String> ids = new ArrayList<>();
        for (int i = 1; i <= 4; i++)
            ids.add("chEntrant" + i);
        return ReportJavaScriptUtil.getCheckJS(ids);
    }

    @Override
    public void modify(ReportDQL dql, final IReportPrintInfo printInfo)
    {
        // добавляем колонку удостоверения личности
        final int idcColumnIndex = dql.addLastAggregateColumn(Person.identityCard());

        // соединяем абитуриента через left join, потому что мог быть выбран фильтр "не является абитуриентом"
        // если абитуриент был ранее соединен по inner join, то останется inner join
        String entrantAlias = dql.leftJoinEntity(Entrant.class, Entrant.person());

        // добавляем колонку entrant, агрегируем в список
        final int entrantIndex = dql.addListAggregateColumn(entrantAlias);

        // добавляем стандартные колонки персоны
        printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintColumnIndex());
        printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintPathColumn("fullFio", idcColumnIndex, IdentityCard.fullFio()));
        printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintPathColumn("sex", idcColumnIndex, IdentityCard.sex().shortTitle()));
        printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintPathColumn("birthDate", idcColumnIndex, IdentityCard.birthDate(), DateFormatter.DEFAULT_DATE_FORMATTER));
        printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintPathColumn("cardType", idcColumnIndex, IdentityCard.cardType().shortTitle()));
        printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintPathColumn("seria", idcColumnIndex, IdentityCard.seria()));
        printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintPathColumn("number", idcColumnIndex, IdentityCard.number()));
        printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintPathColumn("issuanceDate", idcColumnIndex, IdentityCard.issuanceDate(), DateFormatter.DEFAULT_DATE_FORMATTER));
        printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintPathColumn("issuancePlace", idcColumnIndex, IdentityCard.issuancePlace()));

        if (_registration.isActive())
        {
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintColumn("registration")
            {
                @Override
                public Object createColumnValue(Object[] data)
                {
                    IdentityCard idc = (IdentityCard) data[idcColumnIndex];
                    AddressBase address = idc.getAddress();
                    if(address != null && address instanceof AddressDetailed)
                    {
                        AddressCountry country = ((AddressDetailed)address).getCountry();
                        if (country.getCode() == IKladrDefines.RUSSIA_COUNTRY_CODE) {
                            AddressItem settlement = ((AddressDetailed)address).getSettlement();
                            return settlement == null ? "" : settlement.getInheritedRegionCode();
                        }
                        return country.getTitle();
                    }
                    else return "";
                }
            });
        }

        // колонки абитуриента
        if (_enrollmentCampaign.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintListColumn("enrollmentCampaign", entrantIndex, Entrant.enrollmentCampaign().title()));

        if (_entrantPersonalNumber.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintListColumn("entrantPersonalNumber", entrantIndex, Entrant.personalNumber()));

        if (_state.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintListColumn("state", entrantIndex, Entrant.state().title()));

        if (_customState.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintListColumn("customState", entrantIndex, null, entrant -> {
                if (entrant == null)
                    return null;

                Collection<EntrantCustomStateCI> customStateList= EcReportPersonUtil.getEntrantCustomStates(printInfo, (Entrant) entrant);
                if (null == customStateList)
                    return null;

                return CommonBaseStringUtil.join(customStateList, EntrantCustomStateCI.shortTitle().s(), ", ");
            })
            {
                @Override
                public void prefetch(List<Object[]> rows)
                {
                    EcReportPersonUtil.prefetchEntrantCustomState(printInfo, rows, entrantIndex);
                }
            });

        if (_recommendation.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintListColumn("recommendation", entrantIndex, null, entrant -> {
                if (entrant == null) return null;

                Collection<EnrollmentRecommendation> recommendations = EcReportPersonUtil.getEntrantEnrollmentRecommendations(printInfo, (Entrant) entrant);
                if (null == recommendations) return null;

                return StringUtils.join(CollectionUtils.collect(recommendations, EnrollmentRecommendation::getShortTitle), ", ");
            }) {
                @Override public void prefetch(List<Object[]> rows) {
                    EcReportPersonUtil.prefetchEntrantEnrollmentRecommendations(printInfo, rows, entrantIndex);
                }
            });


    }

    // Getters

    public IReportPrintCheckbox getBlock()
    {
        return _block;
    }

    public IReportPrintCheckbox getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public IReportPrintCheckbox getEntrantPersonalNumber()
    {
        return _entrantPersonalNumber;
    }

    public IReportPrintCheckbox getState()
    {
        return _state;
    }

    public IReportPrintCheckbox getRecommendation()
    {
        return _recommendation;
    }

    public IReportPrintCheckbox getRegistration()
    {
        return _registration;
    }

    public IReportPrintCheckbox getCustomState()
    {
        return _customState;
    }
}
