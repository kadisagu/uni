package ru.tandemservice.uniec.entity.settings.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.catalog.EducationSubject;
import ru.tandemservice.uniec.entity.catalog.SubjectPassWay;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.entity.settings.SetDiscipline;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Дисциплина набора вступительных испытаний
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class Discipline2RealizationWayRelationGen extends SetDiscipline
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation";
    public static final String ENTITY_NAME = "discipline2RealizationWayRelation";
    public static final int VERSION_HASH = -485903383;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDUCATION_SUBJECT = "educationSubject";
    public static final String L_SUBJECT_PASS_WAY = "subjectPassWay";
    public static final String P_MIN_MARK = "minMark";
    public static final String P_MAX_MARK = "maxMark";
    public static final String P_PASS_MARK = "passMark";
    public static final String P_INCREASED_PASS_MARK = "increasedPassMark";
    public static final String P_STEP = "step";
    public static final String P_PRIORITY = "priority";

    private EducationSubject _educationSubject;     // Предмет
    private SubjectPassWay _subjectPassWay;     // Способ сдачи предмета
    private int _minMark;     // Минимальный балл
    private int _maxMark = 100;     // Максимальный балл
    private double _passMark;     // Зачетный балл
    private Double _increasedPassMark;     // Повышенный зачетный балл
    private double _step = 1;     // Шаг
    private int _priority;     // Приоритет

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Предмет. Свойство не может быть null.
     */
    @NotNull
    public EducationSubject getEducationSubject()
    {
        return _educationSubject;
    }

    /**
     * @param educationSubject Предмет. Свойство не может быть null.
     */
    public void setEducationSubject(EducationSubject educationSubject)
    {
        dirty(_educationSubject, educationSubject);
        _educationSubject = educationSubject;
    }

    /**
     * @return Способ сдачи предмета. Свойство не может быть null.
     */
    @NotNull
    public SubjectPassWay getSubjectPassWay()
    {
        return _subjectPassWay;
    }

    /**
     * @param subjectPassWay Способ сдачи предмета. Свойство не может быть null.
     */
    public void setSubjectPassWay(SubjectPassWay subjectPassWay)
    {
        dirty(_subjectPassWay, subjectPassWay);
        _subjectPassWay = subjectPassWay;
    }

    /**
     * @return Минимальный балл. Свойство не может быть null.
     */
    @NotNull
    public int getMinMark()
    {
        return _minMark;
    }

    /**
     * @param minMark Минимальный балл. Свойство не может быть null.
     */
    public void setMinMark(int minMark)
    {
        dirty(_minMark, minMark);
        _minMark = minMark;
    }

    /**
     * @return Максимальный балл. Свойство не может быть null.
     */
    @NotNull
    public int getMaxMark()
    {
        return _maxMark;
    }

    /**
     * @param maxMark Максимальный балл. Свойство не может быть null.
     */
    public void setMaxMark(int maxMark)
    {
        dirty(_maxMark, maxMark);
        _maxMark = maxMark;
    }

    /**
     * @return Зачетный балл. Свойство не может быть null.
     */
    @NotNull
    public double getPassMark()
    {
        return _passMark;
    }

    /**
     * @param passMark Зачетный балл. Свойство не может быть null.
     */
    public void setPassMark(double passMark)
    {
        dirty(_passMark, passMark);
        _passMark = passMark;
    }

    /**
     * @return Повышенный зачетный балл.
     */
    public Double getIncreasedPassMark()
    {
        return _increasedPassMark;
    }

    /**
     * @param increasedPassMark Повышенный зачетный балл.
     */
    public void setIncreasedPassMark(Double increasedPassMark)
    {
        dirty(_increasedPassMark, increasedPassMark);
        _increasedPassMark = increasedPassMark;
    }

    /**
     * @return Шаг. Свойство не может быть null.
     */
    @NotNull
    public double getStep()
    {
        return _step;
    }

    /**
     * @param step Шаг. Свойство не может быть null.
     */
    public void setStep(double step)
    {
        dirty(_step, step);
        _step = step;
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Приоритет. Свойство не может быть null.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof Discipline2RealizationWayRelationGen)
        {
            setEducationSubject(((Discipline2RealizationWayRelation)another).getEducationSubject());
            setSubjectPassWay(((Discipline2RealizationWayRelation)another).getSubjectPassWay());
            setMinMark(((Discipline2RealizationWayRelation)another).getMinMark());
            setMaxMark(((Discipline2RealizationWayRelation)another).getMaxMark());
            setPassMark(((Discipline2RealizationWayRelation)another).getPassMark());
            setIncreasedPassMark(((Discipline2RealizationWayRelation)another).getIncreasedPassMark());
            setStep(((Discipline2RealizationWayRelation)another).getStep());
            setPriority(((Discipline2RealizationWayRelation)another).getPriority());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends Discipline2RealizationWayRelationGen> extends SetDiscipline.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) Discipline2RealizationWayRelation.class;
        }

        public T newInstance()
        {
            return (T) new Discipline2RealizationWayRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "educationSubject":
                    return obj.getEducationSubject();
                case "subjectPassWay":
                    return obj.getSubjectPassWay();
                case "minMark":
                    return obj.getMinMark();
                case "maxMark":
                    return obj.getMaxMark();
                case "passMark":
                    return obj.getPassMark();
                case "increasedPassMark":
                    return obj.getIncreasedPassMark();
                case "step":
                    return obj.getStep();
                case "priority":
                    return obj.getPriority();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "educationSubject":
                    obj.setEducationSubject((EducationSubject) value);
                    return;
                case "subjectPassWay":
                    obj.setSubjectPassWay((SubjectPassWay) value);
                    return;
                case "minMark":
                    obj.setMinMark((Integer) value);
                    return;
                case "maxMark":
                    obj.setMaxMark((Integer) value);
                    return;
                case "passMark":
                    obj.setPassMark((Double) value);
                    return;
                case "increasedPassMark":
                    obj.setIncreasedPassMark((Double) value);
                    return;
                case "step":
                    obj.setStep((Double) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "educationSubject":
                        return true;
                case "subjectPassWay":
                        return true;
                case "minMark":
                        return true;
                case "maxMark":
                        return true;
                case "passMark":
                        return true;
                case "increasedPassMark":
                        return true;
                case "step":
                        return true;
                case "priority":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "educationSubject":
                    return true;
                case "subjectPassWay":
                    return true;
                case "minMark":
                    return true;
                case "maxMark":
                    return true;
                case "passMark":
                    return true;
                case "increasedPassMark":
                    return true;
                case "step":
                    return true;
                case "priority":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "educationSubject":
                    return EducationSubject.class;
                case "subjectPassWay":
                    return SubjectPassWay.class;
                case "minMark":
                    return Integer.class;
                case "maxMark":
                    return Integer.class;
                case "passMark":
                    return Double.class;
                case "increasedPassMark":
                    return Double.class;
                case "step":
                    return Double.class;
                case "priority":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<Discipline2RealizationWayRelation> _dslPath = new Path<Discipline2RealizationWayRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "Discipline2RealizationWayRelation");
    }
            

    /**
     * @return Предмет. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation#getEducationSubject()
     */
    public static EducationSubject.Path<EducationSubject> educationSubject()
    {
        return _dslPath.educationSubject();
    }

    /**
     * @return Способ сдачи предмета. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation#getSubjectPassWay()
     */
    public static SubjectPassWay.Path<SubjectPassWay> subjectPassWay()
    {
        return _dslPath.subjectPassWay();
    }

    /**
     * @return Минимальный балл. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation#getMinMark()
     */
    public static PropertyPath<Integer> minMark()
    {
        return _dslPath.minMark();
    }

    /**
     * @return Максимальный балл. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation#getMaxMark()
     */
    public static PropertyPath<Integer> maxMark()
    {
        return _dslPath.maxMark();
    }

    /**
     * @return Зачетный балл. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation#getPassMark()
     */
    public static PropertyPath<Double> passMark()
    {
        return _dslPath.passMark();
    }

    /**
     * @return Повышенный зачетный балл.
     * @see ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation#getIncreasedPassMark()
     */
    public static PropertyPath<Double> increasedPassMark()
    {
        return _dslPath.increasedPassMark();
    }

    /**
     * @return Шаг. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation#getStep()
     */
    public static PropertyPath<Double> step()
    {
        return _dslPath.step();
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    public static class Path<E extends Discipline2RealizationWayRelation> extends SetDiscipline.Path<E>
    {
        private EducationSubject.Path<EducationSubject> _educationSubject;
        private SubjectPassWay.Path<SubjectPassWay> _subjectPassWay;
        private PropertyPath<Integer> _minMark;
        private PropertyPath<Integer> _maxMark;
        private PropertyPath<Double> _passMark;
        private PropertyPath<Double> _increasedPassMark;
        private PropertyPath<Double> _step;
        private PropertyPath<Integer> _priority;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Предмет. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation#getEducationSubject()
     */
        public EducationSubject.Path<EducationSubject> educationSubject()
        {
            if(_educationSubject == null )
                _educationSubject = new EducationSubject.Path<EducationSubject>(L_EDUCATION_SUBJECT, this);
            return _educationSubject;
        }

    /**
     * @return Способ сдачи предмета. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation#getSubjectPassWay()
     */
        public SubjectPassWay.Path<SubjectPassWay> subjectPassWay()
        {
            if(_subjectPassWay == null )
                _subjectPassWay = new SubjectPassWay.Path<SubjectPassWay>(L_SUBJECT_PASS_WAY, this);
            return _subjectPassWay;
        }

    /**
     * @return Минимальный балл. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation#getMinMark()
     */
        public PropertyPath<Integer> minMark()
        {
            if(_minMark == null )
                _minMark = new PropertyPath<Integer>(Discipline2RealizationWayRelationGen.P_MIN_MARK, this);
            return _minMark;
        }

    /**
     * @return Максимальный балл. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation#getMaxMark()
     */
        public PropertyPath<Integer> maxMark()
        {
            if(_maxMark == null )
                _maxMark = new PropertyPath<Integer>(Discipline2RealizationWayRelationGen.P_MAX_MARK, this);
            return _maxMark;
        }

    /**
     * @return Зачетный балл. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation#getPassMark()
     */
        public PropertyPath<Double> passMark()
        {
            if(_passMark == null )
                _passMark = new PropertyPath<Double>(Discipline2RealizationWayRelationGen.P_PASS_MARK, this);
            return _passMark;
        }

    /**
     * @return Повышенный зачетный балл.
     * @see ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation#getIncreasedPassMark()
     */
        public PropertyPath<Double> increasedPassMark()
        {
            if(_increasedPassMark == null )
                _increasedPassMark = new PropertyPath<Double>(Discipline2RealizationWayRelationGen.P_INCREASED_PASS_MARK, this);
            return _increasedPassMark;
        }

    /**
     * @return Шаг. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation#getStep()
     */
        public PropertyPath<Double> step()
        {
            if(_step == null )
                _step = new PropertyPath<Double>(Discipline2RealizationWayRelationGen.P_STEP, this);
            return _step;
        }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(Discipline2RealizationWayRelationGen.P_PRIORITY, this);
            return _priority;
        }

        public Class getEntityClass()
        {
            return Discipline2RealizationWayRelation.class;
        }

        public String getEntityName()
        {
            return "discipline2RealizationWayRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
