/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.order.EnrollmentOrderPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrderType;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.OrderStates;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.component.visa.VisaList.IVisaOwnerModel;

import java.util.Map;

/**
 * @author vip_delete
 * @since 08.04.2009
 */
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "order.id"),
        @Bind(key = "selectedTab", binding = "selectedTab")
})
public class Model implements IVisaOwnerModel
{
    public static final String EDUCATION_ORG_UNIT_TITLE = "educationOrgUnitTitle";
    public static final String FORMATIVE_ORG_UNIT_TITLE = "formativeOrgUnitTitle";
    public static final String TERRITORIAL_ORG_UNIT_TITLE = "territorialOrgUnitTitle";
    public static final String DEVELOP_FORM_TITLE = "developFormTitle";
    public static final String DEVELOP_CONDITION_TITLE = "developConditionTitle";
    public static final String DEVELOP_TECH_TITLE = "developTechTitle";
    public static final String DEVELOP_PERIOD_TITLE = "developPeriodTitle";
    public static final String COURSE_TITLE = "courseTitle";
    public static final String EXTRACT_COUNT = "extractCount";

    private IDataSettings _settings;
    private CommonPostfixPermissionModel _secModel;
    private EnrollmentOrderType _type;
    private EnrollmentOrder _order = new EnrollmentOrder();
    private OrderStates _state;
    private String _selectedTab;
    private Long _visingStatus;
    private DynamicListDataSource _dataSource;

    private Map<String, Object> _visaListParameters = ParametersMap.createWith("visaOwnerModel", this);

    public boolean isNeedVising()
    {
        return _visingStatus != null;
    }

    // IVisaOwnerModel

    @Override
    public IAbstractDocument getDocument()
    {
        return _order;
    }

    @Override
    public boolean isVisaListModificable()
    {
        return UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(_order.getState().getCode());
    }

    @Override
    public String getSecPostfix()
    {
        return "enrollmentOrder";
    }

    // Getters & Setters

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }

    public EnrollmentOrderType getType()
    {
        return _type;
    }

    public void setType(EnrollmentOrderType type)
    {
        _type = type;
    }

    public EnrollmentOrder getOrder()
    {
        return _order;
    }

    public void setOrder(EnrollmentOrder order)
    {
        _order = order;
    }

    public OrderStates getState()
    {
        return _state;
    }

    public void setState(OrderStates state)
    {
        _state = state;
    }

    public String getSelectedTab()
    {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        _selectedTab = selectedTab;
    }

    public Long getVisingStatus()
    {
        return _visingStatus;
    }

    public void setVisingStatus(Long visingStatus)
    {
        _visingStatus = visingStatus;
    }

    public DynamicListDataSource getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource dataSource)
    {
        _dataSource = dataSource;
    }

    public Map<String, Object> getVisaListParameters()
    {
        return _visaListParameters;
    }

    public void setVisaListParameters(Map<String, Object> visaListParameters)
    {
        _visaListParameters = visaListParameters;
    }
}
