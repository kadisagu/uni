/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantRequestAddEdit;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.transaction.TransactionAbortException;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Боба
 * @since 08.08.2008
 */
public interface IDAO extends IUniDao<Model>
{
    /**
     * 1. Добавляем в model.selectedRequestedEnrollmentDirectionList выбранного направление приема, которое потом сохраним
     * 2. Сохраняем в модель (не в базу) оценки по профильным предметам.
     * @param model модель бизнес компонента
     * @param errorCollector для сборки ошибок (в методе нельзя бросать исключения, поскольку там нет транзакции и будут проблемы с proxy - no session)
     */
    @Transactional(propagation = Propagation.SUPPORTS, noRollbackFor = TransactionAbortException.class)
    void prepareSelectedEnrollmentDirectionList(Model model, ErrorCollector errorCollector);

    /**
     * Записываем в model.profileDisciplineList список профильных дисциплин из набора выбранного направления приема
     * @param model модель бизнес компонента
     * @return true, если есть направление приема и профильные дисциплины в его наборе дисциплин
     */
    boolean prepareProfileDisciplineList(Model model);

    /**
     * @param model модель
     * @return максимальный номер выбранного направления приема на данном направлении
     */
    int getMaxRequestedEnrollmentDirectionNumber(Model model);

    /**
     * Сбрасывает значения, выбранные на форме
     * 
     * @param model модель
     */
    void reset(Model model);
}
