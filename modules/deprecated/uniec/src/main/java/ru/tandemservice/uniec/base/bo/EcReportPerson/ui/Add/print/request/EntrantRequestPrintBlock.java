/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.print.request;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.print.ReportPrintListColumn;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.print.ReportPrintMapColumn;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.EcReportPersonAdd;
import ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.EcReportPersonAddUI;
import ru.tandemservice.uniec.base.bo.EcReportPerson.ui.util.EcReportPersonUtil;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.ui.EntrantStateExamCertificateNumberFormatter;
import ru.tandemservice.uniec.util.EntrantDataUtil;

import java.util.*;

/**
 * @author Vasily Zhukov
 * @since 31.08.2011
 */
public class EntrantRequestPrintBlock implements IReportPrintBlock
{
    private IReportPrintCheckbox _block = new ReportPrintCheckbox();
    private IReportPrintCheckbox _requestNumber = new ReportPrintCheckbox();
    private IReportPrintCheckbox _requestDate = new ReportPrintCheckbox();
    private IReportPrintCheckbox _technicCommission = new ReportPrintCheckbox().disabled();
    private IReportPrintCheckbox _examGroup = new ReportPrintCheckbox();
    private IReportPrintCheckbox _formativeOrgUnit = new ReportPrintCheckbox();
    private IReportPrintCheckbox _territorialOrgUnit = new ReportPrintCheckbox();
    private IReportPrintCheckbox _regNumber = new ReportPrintCheckbox();
    private IReportPrintCheckbox _educationOrgUnit = new ReportPrintCheckbox();
    private IReportPrintCheckbox _developForm = new ReportPrintCheckbox();
    private IReportPrintCheckbox _developCondition = new ReportPrintCheckbox();
    private IReportPrintCheckbox _developPeriod = new ReportPrintCheckbox();
    private IReportPrintCheckbox _compensationType = new ReportPrintCheckbox();
    private IReportPrintCheckbox _finalMark = new ReportPrintCheckbox();
    private IReportPrintCheckbox _sumStateExam = new ReportPrintCheckbox();
    private IReportPrintCheckbox _countAcceptedStateExam = new ReportPrintCheckbox();
    private IReportPrintCheckbox _sumEntranceDisciplineMark = new ReportPrintCheckbox();
    private IReportPrintCheckbox _priority = new ReportPrintCheckbox();
    private IReportPrintCheckbox _olympiad = new ReportPrintCheckbox();
    private IReportPrintCheckbox _disciplineResult = new ReportPrintCheckbox();
    private IReportPrintCheckbox _disciplineResultDetail = new ReportPrintCheckbox();
    private IReportPrintCheckbox _studentCategory = new ReportPrintCheckbox();
    private IReportPrintCheckbox _targetAdmission = new ReportPrintCheckbox();
    private IReportPrintCheckbox _targetAdmissionKind = new ReportPrintCheckbox();
    private IReportPrintCheckbox _competitionKind = new ReportPrintCheckbox();
    private IReportPrintCheckbox _serviceLength = new ReportPrintCheckbox();
    private IReportPrintCheckbox _profileKnowledge = new ReportPrintCheckbox();
    private IReportPrintCheckbox _documentState = new ReportPrintCheckbox();
    private IReportPrintCheckbox _directionState = new ReportPrintCheckbox();
    private IReportPrintCheckbox _stateExamInternal = new ReportPrintCheckbox();
    private IReportPrintCheckbox _directionComment = new ReportPrintCheckbox();
    private IReportPrintCheckbox _requestComment = new ReportPrintCheckbox();
    private IReportPrintCheckbox _stateExam = new ReportPrintCheckbox();
    private IReportPrintCheckbox _stateExamDetail = new ReportPrintCheckbox();
    private IReportPrintCheckbox _individualProgress = new ReportPrintCheckbox();
    private IReportPrintCheckbox _individualProgressDetails = new ReportPrintCheckbox();

    @Override
    public String getCheckJS()
    {
        List<String> ids = new ArrayList<>();
        for (int i = 1; i <= 30; i++)
            ids.add("chRequest" + i);
        return ReportJavaScriptUtil.getCheckJS(ids);
    }

    @SuppressWarnings("unused")
    @Override
    public void modify(ReportDQL dql, final IReportPrintInfo printInfo)
    {
        // соединяем абитуриента через left join, потому что мог быть выбран фильтр "не является абитуриентом"
        // если абитуриент был ранее соединен по inner join, то останется inner join
        String entrantAlias = dql.leftJoinEntity(Entrant.class, Entrant.person());

        // соединяем заявление абитуриента через left join (так как заявления может не быть)
        String requestAlias = dql.leftJoinEntity(entrantAlias, EntrantRequest.class, EntrantRequest.entrant());

        // соединяем заявление абитуриента через left join (так как заявления может не быть)
        String directionAlias = dql.leftJoinEntity(requestAlias, RequestedEnrollmentDirection.class, RequestedEnrollmentDirection.entrantRequest());

        // добавляем колонку entrant, агрегируем в список
        final int entrantIndex = dql.addListAggregateColumn(entrantAlias);

        // добавляем колонку request, агрегируем в мап
        final int requestIndex = dql.addMapAggregateColumn(requestAlias, entrantIndex);

        // добавляем колонку direction, агрегируем в мап
        final int directionIndex = dql.addMapAggregateColumn(directionAlias, entrantIndex, requestIndex);

        // добавляем колонку direction.id, агрегируем в мап, для удобного создания EntrantDataUtil
        final int directionIdForDataUtilIndex = dql.addMapAggregateColumn(directionAlias, RequestedEnrollmentDirection.id(), entrantIndex);

        if (_requestNumber.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("requestNumber", requestIndex, null, new IFormatter<EntrantRequest>() {
                @Override public String format(EntrantRequest entrantRequest) {
                    return entrantRequest == null ? null : entrantRequest.getStringNumber();
                }
            }));

        if (_requestDate.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("requestDate", requestIndex, null, new IFormatter<EntrantRequest>() {
                @Override public String format(EntrantRequest entrantRequest) {
                    return entrantRequest == null ? null : DateFormatter.DEFAULT_DATE_FORMATTER.format(entrantRequest.getRegDate());
                }
            }));

        if (_individualProgressDetails.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("individualProgressDetails", requestIndex, null, request -> {
                if (request == null) return null;

                Collection<EntrantIndividualProgress> individualProgresses = EcReportPersonUtil.getEntrantIndividualProgress(printInfo, (EntrantRequest) request);
                if (individualProgresses == null) return null;

                return StringUtils.join(CollectionUtils.collect(individualProgresses, eip -> eip.getIndividualProgressType().getShortTitle()), '\n');
            })
            {
                @Override
                public void prefetch(List<Object[]> rows)
                {
                    EcReportPersonUtil.prefetchRequestIndividualProgress(printInfo, rows, requestIndex);
                }
            });
        if (_individualProgress.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("individualProgress", requestIndex, null, request -> {
                if (request == null) return null;

                Collection<EntrantIndividualProgress> individualProgresses = EcReportPersonUtil.getEntrantIndividualProgress(printInfo, (EntrantRequest) request);
                if (individualProgresses == null) return null;

                return StringUtils.join(CollectionUtils.collect(individualProgresses, pr -> String.valueOf(pr.getMark())), '\n');
            })
            {
                @Override
                public void prefetch(List<Object[]> rows)
                {
                    EcReportPersonUtil.prefetchRequestIndividualProgress(printInfo, rows, requestIndex);
                }
            });

        if (_technicCommission.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("technicCommission", requestIndex, EntrantRequest.technicCommission()));

        if (_examGroup.isActive()) {
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("examGroup", requestIndex, null, new IFormatter<EntrantRequest>() {
                @Override public String format(EntrantRequest request) {
                    if (request == null) return null;

                    final List<ExamGroup> groups = EcReportPersonUtil.getEntrantExamGroups(printInfo, request);
                    if (null == groups || groups.isEmpty()) return null;

                    final List<String> titleList = new ArrayList<>(groups.size());
                    for (ExamGroup g: groups) {
                        titleList.add(g.getTitle());
                    }

                    Collections.sort(titleList);
                    return StringUtils.join(titleList, '\n');
                }
            }) {
                @Override public void prefetch(List<Object[]> rows) {
                    EcReportPersonUtil.prefetchEntrantExamGroups(printInfo, rows, requestIndex);
                }
            });
        }

        if (_formativeOrgUnit.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("formativeOrgUnit", directionIndex, RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().formativeOrgUnit().title()));

        if (_territorialOrgUnit.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("territorialOrgUnit", directionIndex, RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().territorialOrgUnit().territorialTitle()));

        if (_regNumber.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("regNumber", directionIndex, RequestedEnrollmentDirection.number()).number());

        if (_educationOrgUnit.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("educationOrgUnit", directionIndex, RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().printTitle()));

        if (_developForm.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("developForm", directionIndex, RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developForm().title()));

        if (_developCondition.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("developCondition", directionIndex, RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developCondition().title()));

        if (_developPeriod.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("developPeriod", directionIndex, RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developPeriod().title()));

        if (_compensationType.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("compensationType", directionIndex, RequestedEnrollmentDirection.compensationType().shortTitle()));

        if (_competitionKind.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("competitionKind", directionIndex, RequestedEnrollmentDirection.competitionKind().title()));

        if (_targetAdmission.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("targetAdmission", directionIndex, RequestedEnrollmentDirection.targetAdmission(), new IFormatter<Boolean>() {
                @Override public String format(Boolean source) {
                    return source == null ? null : (source ? "да" : "нет");
                }
            }));

        if (_targetAdmissionKind.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("targetAdmissionKind", directionIndex, RequestedEnrollmentDirection.targetAdmissionKind().shortTitle()));

        if (_studentCategory.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("studentCategory", directionIndex, RequestedEnrollmentDirection.studentCategory().title()));

        if (_directionState.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("directionState", directionIndex, RequestedEnrollmentDirection.state().title()));

        if (_documentState.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("documentState", directionIndex, RequestedEnrollmentDirection.originalDocumentHandedIn(), new IFormatter<Boolean>() {
                @Override public String format(Boolean source) {
                    return source == null ? null : (source ? "оригиналы" : "копии");
                }
            }));


        if (_finalMark.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("finalMark", directionIndex, null, (IFormatter<RequestedEnrollmentDirection>) direction -> {
                if (direction == null) return null;
                EntrantDataUtil util = EcReportPersonUtil.getEntrantDataUtil(printInfo, direction);
                final double[] sum = {util.getFinalMark(direction)};
                if (direction.getEnrollmentDirection().getEnrollmentCampaign().isUseIndividualProgressInAmountMark())
                {
                    Collection<EntrantIndividualProgress> individualProgresses = EcReportPersonUtil
                            .getEntrantIndividualProgress(printInfo, direction.getEntrantRequest());
                    if (individualProgresses != null)
                        individualProgresses.forEach(eip -> sum[0] += eip.getMark());
                }
                return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(sum[0]);
            }) {
                @Override public void prefetch(List<Object[]> rows) {
                    EcReportPersonUtil.prefetchEntrantDataUtil(printInfo, rows, entrantIndex, directionIdForDataUtilIndex);
                    EcReportPersonUtil.prefetchRequestIndividualProgress(printInfo, rows, requestIndex);
                }
            }.number());

        if (_sumStateExam.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("sumStateExam", directionIndex, null, (IFormatter<RequestedEnrollmentDirection>) direction -> {
                if (direction == null) return null;
                EntrantDataUtil util = EcReportPersonUtil.getEntrantDataUtil(printInfo, direction);
                double sum = 0;
                for (ChosenEntranceDiscipline discipline : util.getChosenEntranceDisciplineSet(direction))
                {
                    Integer source = discipline.getFinalMarkSource();
                    if (source != null
                            && (source == UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL
                            || source == UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL_APPEAL
                            || source == UniecDefines.FINAL_MARK_STATE_EXAM_WAVE1
                            || source == UniecDefines.FINAL_MARK_STATE_EXAM_WAVE2))
                    {
                        Double finalMark = discipline.getFinalMark();
                        if (finalMark != null) sum += finalMark;
                    }
                }
                return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(sum);
            })
            {
                @Override
                public void prefetch(List<Object[]> rows)
                {
                    EcReportPersonUtil.prefetchEntrantDataUtil(printInfo, rows, entrantIndex, directionIdForDataUtilIndex);
                }
            }.number());

        if (_countAcceptedStateExam.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("countAcceptedStateExam", directionIndex, null, (IFormatter<RequestedEnrollmentDirection>) direction -> {
                if (direction == null) return null;
                EntrantDataUtil util = EcReportPersonUtil.getEntrantDataUtil(printInfo, direction);
                int count = 0;
                for (ChosenEntranceDiscipline discipline : util.getChosenEntranceDisciplineSet(direction))
                {
                    Integer source = discipline.getFinalMarkSource();
                    if (source != null
                            && (source == UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL
                            || source == UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL_APPEAL
                            || source == UniecDefines.FINAL_MARK_STATE_EXAM_WAVE1
                            || source == UniecDefines.FINAL_MARK_STATE_EXAM_WAVE2))
                    {
                        count++;
                    }
                }
                return String.valueOf(count);
            })
            {
                @Override public void prefetch(List<Object[]> rows) {
                    EcReportPersonUtil.prefetchEntrantDataUtil(printInfo, rows, entrantIndex, directionIdForDataUtilIndex);
                }
            }.number());

        if (_sumEntranceDisciplineMark.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("sumEntranceDisciplineMark", directionIndex, null, (IFormatter<RequestedEnrollmentDirection>) direction -> {
                if (direction == null) return null;
                return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(EcReportPersonUtil.getEntrantDataUtil(printInfo, direction).getFinalMark(direction));
            }) {
                @Override public void prefetch(List<Object[]> rows) {
                    EcReportPersonUtil.prefetchEntrantDataUtil(printInfo, rows, entrantIndex, directionIdForDataUtilIndex);
                }
            }.number());

        if (_priority.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("priority", directionIndex, RequestedEnrollmentDirection.priority()).number());

        if (_olympiad.isActive()) {
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("olympiad", directionIndex, null, new IFormatter<RequestedEnrollmentDirection>() {
                @Override public String format(RequestedEnrollmentDirection direction) {
                    if (direction == null) return null;

                    final List<OlympiadDiploma> diplomaList = EcReportPersonUtil.getEntrantDataOlympiad(printInfo, direction);
                    if (null == diplomaList || diplomaList.isEmpty()) return null;

                    // превращаем в строки
                    final List<String> diplomaTitleList = new ArrayList<>(diplomaList.size());
                    for (OlympiadDiploma diploma : diplomaList)
                    {
                        final StringBuilder sb = new StringBuilder();
                        sb.append(diploma.getSeria()).append(" №").append(diploma.getNumber());
                        if (diploma.getDiplomaType() != null && !UniecDefines.DIPLOMA_TYPE_OTHER.equals(diploma.getDiplomaType().getCode()))
                            sb.append(" всероссийская олимп.,");
                        sb.append(" ").append(diploma.getOlympiad()).append(", ").append(diploma.getSubject());

                        diplomaTitleList.add(sb.toString());
                    }

                    return StringUtils.join(diplomaTitleList, '\n');
                }
            }) {
                @Override public void prefetch(List<Object[]> rows) {
                    EcReportPersonUtil.prefetchEntrantOlympiad(printInfo, rows, directionIndex);
                }
            });
        }


        if (_disciplineResult.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("disciplineResult", directionIndex, null, new IFormatter<RequestedEnrollmentDirection>() {
                @Override public String format(RequestedEnrollmentDirection direction) {
                    if (direction == null) return null;
                    EntrantDataUtil util = EcReportPersonUtil.getEntrantDataUtil(printInfo, direction);
                    List<ChosenEntranceDiscipline> disciplineList = new ArrayList<>(util.getChosenEntranceDisciplineSet(direction));
                    Collections.sort(disciplineList, new EntityComparator<ChosenEntranceDiscipline>(new EntityOrder(ChosenEntranceDiscipline.enrollmentCampaignDiscipline().priority().s())));
                    List<String> disciplineTitleList = new ArrayList<>();
                    for (ChosenEntranceDiscipline chosen : disciplineList) {
                        String line = chosen.getTitle() + " - ";
                        if (chosen.getFinalMark() != null) {
                            line = line + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(chosen.getFinalMark());
                            if (chosen.getFinalMarkSource() == 1) {
                                line = line + " (диплом)";
                            }
                            else if (chosen.getFinalMarkSource() == 8 || chosen.getFinalMarkSource() == 9 || chosen.getFinalMarkSource() == 10 || chosen.getFinalMarkSource() == 11) {
                                line = line + " (егэ)";
                            }
                        }
                        disciplineTitleList.add(line);
                    }
                    return StringUtils.join(disciplineTitleList, '\n');
                }
            }) {
                @Override public void prefetch(List<Object[]> rows) {
                    EcReportPersonUtil.prefetchEntrantDataUtil(printInfo, rows, entrantIndex, directionIdForDataUtilIndex);
                }
            });

        // детализация по дисциплинам строится только при выбранной приемной кампании
        if (_disciplineResultDetail.isActive() && printInfo.getSharedObject(EcReportPersonAdd.PREFETCH_ENROLLMENT_CAMPAIGN) != null)
        {
            EnrollmentCampaign enrollmentCampaign = printInfo.getSharedObject(EcReportPersonAdd.PREFETCH_ENROLLMENT_CAMPAIGN);

            List<Discipline2RealizationWayRelation> disciplineWayList = DataAccessServices.dao().getList(Discipline2RealizationWayRelation.class, Discipline2RealizationWayRelation.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign, Discipline2RealizationWayRelation.P_PRIORITY);

            for (final Discipline2RealizationWayRelation discipline : disciplineWayList)
            {
                printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("disciplineResultDetail", directionIndex, null, new IFormatter<RequestedEnrollmentDirection>() {
                    @Override public String format(RequestedEnrollmentDirection direction) {
                        if (direction == null) return null;
                        EntrantDataUtil util = EcReportPersonUtil.getEntrantDataUtil(printInfo, direction);
                        for (ChosenEntranceDiscipline chosen : util.getChosenEntranceDisciplineSet(direction))
                            if (chosen.getEnrollmentCampaignDiscipline().equals(discipline))
                                return chosen.getFinalMark() == null ? null : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(chosen.getFinalMark());
                        return null;
                    }
                }) {
                    @Override public void prefetch(List<Object[]> rows) {
                        EcReportPersonUtil.prefetchEntrantDataUtil(printInfo, rows, entrantIndex, directionIdForDataUtilIndex);
                    }
                }.title(discipline.getShortTitle()).number());
            }
        }


        if (_serviceLength.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("serviceLength", directionIndex, null, new IFormatter<RequestedEnrollmentDirection>() {
                @Override public String format(RequestedEnrollmentDirection direction) {
                    if (direction == null) return null;

                    List<String> itemList = new ArrayList<>();
                    if (direction.getProfileWorkExperienceYears() != null)
                        itemList.add("лет: " + direction.getProfileWorkExperienceYears());
                    if (direction.getProfileWorkExperienceMonths() != null)
                        itemList.add("месяцев: " + direction.getProfileWorkExperienceMonths());
                    if (direction.getProfileWorkExperienceDays() != null)
                        itemList.add("дней: " + direction.getProfileWorkExperienceDays());
                    return StringUtils.join(itemList.iterator(), "; ");
                }
            }));

        if (_profileKnowledge.isActive())
        {
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("profileKnowledge", directionIndex, null, new IFormatter<RequestedEnrollmentDirection>() {
                @Override public String format(RequestedEnrollmentDirection direction) {
                    return direction == null ? null : EcReportPersonUtil.getProfileKnowledgeTitle(printInfo, direction);
                }
            }) {
                @Override public void prefetch(List<Object[]> rows) {
                    EcReportPersonUtil.prefetchProfileKnowledgeMap(printInfo, rows, entrantIndex);
                }
            });
        }

        if (_stateExamInternal.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("stateExamInternal", directionIndex, null, new IFormatter<RequestedEnrollmentDirection>() {
                @Override public String format(RequestedEnrollmentDirection direction) {
                    if (direction == null) return null;
                    EntrantDataUtil util = EcReportPersonUtil.getEntrantDataUtil(printInfo, direction);
                    List<ChosenEntranceDiscipline> disciplineList = new ArrayList<>(util.getChosenEntranceDisciplineSet(direction));
                    Collections.sort(disciplineList, new EntityComparator<ChosenEntranceDiscipline>(new EntityOrder(ChosenEntranceDiscipline.enrollmentCampaignDiscipline().priority().s())));
                    List<String> disciplineStateInternalTitleList = new ArrayList<>();
                    for (ChosenEntranceDiscipline chosen : disciplineList)
                        for (ExamPassDiscipline examPass : util.getExamPassDisciplineSet(chosen))
                        {
                            if (UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM_INTERNAL.equals(examPass.getSubjectPassForm().getCode()))
                            {
                                double finalMark = util.getFinalMark(examPass);
                                disciplineStateInternalTitleList.add(chosen.getTitle() + " - " + (finalMark == -1.0 ? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(finalMark)));
                            }
                        }
                    return StringUtils.join(disciplineStateInternalTitleList, '\n');
                }
            }) {
                @Override public void prefetch(List<Object[]> rows) {
                    EcReportPersonUtil.prefetchEntrantDataUtil(printInfo, rows, entrantIndex, directionIdForDataUtilIndex);
                }
            });

        if (_directionComment.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("directionComment", directionIndex, RequestedEnrollmentDirection.comment()));

        if (_requestComment.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("requestComment", requestIndex, EntrantRequest.comment()));

        final List<StateExamSubject> stateExamSubjectList = DataAccessServices.dao().getList(StateExamSubject.class);
        final int ENTRANT_MARK_IDX = 0;
        final int CERTIFICATE_NUMBER_IDX = 1;
        Collections.sort(stateExamSubjectList, CommonCatalogUtil.CATALOG_CODE_COMPARATOR);

        if (_stateExam.isActive())
        {
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintListColumn("stateExam", entrantIndex, null, new IFormatter<Entrant>() {
                @Override public String format(Entrant entrant) {
                    if (entrant == null) return null;

                    List<String> markTitleList = new ArrayList<>();
                    Map<Long, Object[]> markList = EcReportPersonUtil.getStateExamSubjectMarkMap(printInfo, entrant);
                    if (markList != null)
                    {
                        for (StateExamSubject subject : stateExamSubjectList)
                        {
                            Object[] data = markList.get(subject.getId());
                            if (data != null)
                                markTitleList.add(subject.getTitle() + " - " + Integer.toString((Integer) data[ENTRANT_MARK_IDX]) + " (" + EntrantStateExamCertificateNumberFormatter.formatStatic((String) data[CERTIFICATE_NUMBER_IDX]) + ')');
                        }
                    }

                    return StringUtils.join(markTitleList, '\n');
                }
            }) {
                @Override public void prefetch(List<Object[]> rows) {
                    EcReportPersonUtil.prefetchStateExamSubjectMarkMap(printInfo, rows, entrantIndex);
                }
            });
        }

        if (_stateExamDetail.isActive())
        {
            for (final StateExamSubject stateExamSubject : stateExamSubjectList)
            {
                printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintListColumn("stateExamDetail", entrantIndex, null, new IFormatter<Entrant>() {
                    @Override public String format(Entrant entrant) {
                        if (entrant == null) return null;
                        Map<Long, Object[]> markList = EcReportPersonUtil.getStateExamSubjectMarkMap(printInfo, entrant);
                        Integer mark = null;
                        if (markList != null)
                        {
                            Object[] data = markList.get(stateExamSubject.getId());
                            if (data != null)
                                mark = (Integer) data[ENTRANT_MARK_IDX];
                        }
                        return mark == null ? null : Integer.toString(mark);
                    }
                }) {
                    @Override public void prefetch(List<Object[]> rows) {
                        EcReportPersonUtil.prefetchStateExamSubjectMarkMap(printInfo, rows, entrantIndex);
                    }
                }.title(stateExamSubject.getShortTitle()).number());
            }
        }
    }

    // Getters

    public IReportPrintCheckbox getBlock()
    {
        return _block;
    }

    public IReportPrintCheckbox getRequestNumber()
    {
        return _requestNumber;
    }

    public IReportPrintCheckbox getRequestDate()
    {
        return _requestDate;
    }

    public IReportPrintCheckbox getTechnicCommission()
    {
        return _technicCommission;
    }

    public IReportPrintCheckbox getExamGroup()
    {
        return _examGroup;
    }

    public IReportPrintCheckbox getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public IReportPrintCheckbox getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    public IReportPrintCheckbox getRegNumber()
    {
        return _regNumber;
    }

    public IReportPrintCheckbox getEducationOrgUnit()
    {
        return _educationOrgUnit;
    }

    public IReportPrintCheckbox getDevelopForm()
    {
        return _developForm;
    }

    public IReportPrintCheckbox getCompensationType()
    {
        return _compensationType;
    }

    public IReportPrintCheckbox getFinalMark()
    {
        return _finalMark;
    }

    public IReportPrintCheckbox getCountAcceptedStateExam()
    {
        return _countAcceptedStateExam;
    }

    public IReportPrintCheckbox getSumEntranceDisciplineMark()
    {
        return _sumEntranceDisciplineMark;
    }

    public IReportPrintCheckbox getSumStateExam()
    {
        return _sumStateExam;
    }

    public IReportPrintCheckbox getPriority()
    {
        return _priority;
    }

    public IReportPrintCheckbox getOlympiad()
    {
        return _olympiad;
    }

    public IReportPrintCheckbox getDisciplineResult()
    {
        return _disciplineResult;
    }

    public IReportPrintCheckbox getDisciplineResultDetail()
    {
        return _disciplineResultDetail;
    }

    public IReportPrintCheckbox getStudentCategory()
    {
        return _studentCategory;
    }

    public IReportPrintCheckbox getTargetAdmission()
    {
        return _targetAdmission;
    }

    public IReportPrintCheckbox getTargetAdmissionKind()
    {
        return _targetAdmissionKind;
    }

    public IReportPrintCheckbox getCompetitionKind()
    {
        return _competitionKind;
    }

    public IReportPrintCheckbox getServiceLength()
    {
        return _serviceLength;
    }

    public IReportPrintCheckbox getProfileKnowledge()
    {
        return _profileKnowledge;
    }

    public IReportPrintCheckbox getDocumentState()
    {
        return _documentState;
    }

    public IReportPrintCheckbox getDirectionState()
    {
        return _directionState;
    }

    public IReportPrintCheckbox getStateExamInternal()
    {
        return _stateExamInternal;
    }

    public IReportPrintCheckbox getDirectionComment()
    {
        return _directionComment;
    }

    public IReportPrintCheckbox getRequestComment()
    {
        return _requestComment;
    }

    public IReportPrintCheckbox getStateExam()
    {
        return _stateExam;
    }

    public IReportPrintCheckbox getStateExamDetail()
    {
        return _stateExamDetail;
    }

    public IReportPrintCheckbox getDevelopCondition()
    {
        return _developCondition;
    }

    public IReportPrintCheckbox getDevelopPeriod()
    {
        return _developPeriod;
    }

    public IReportPrintCheckbox getIndividualProgress()
    {
        return _individualProgress;
    }

    public IReportPrintCheckbox getIndividualProgressDetails()
    {
        return _individualProgressDetails;
    }
}
