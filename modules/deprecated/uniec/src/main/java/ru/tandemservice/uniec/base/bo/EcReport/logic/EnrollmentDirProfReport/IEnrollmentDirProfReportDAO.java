/**
 *$Id$
 */
package ru.tandemservice.uniec.base.bo.EcReport.logic.EnrollmentDirProfReport;

import jxl.write.WriteException;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uniec.entity.report.EnrollmentDirProfReport;

import java.io.IOException;

/**
 * @author Alexander Shaburov
 * @since 05.07.12
 */
public interface IEnrollmentDirProfReportDAO extends INeedPersistenceSupport
{
    /**
     * Подготавливает модель формы добавления отчета.<p/>
     * Заполняет модели селектов и значения по умолчанию.
     * @param model модель
     * @return подготовленную модель
     */
    <T extends EnrollmentDirProfReportModel> T prepareModel(T model);

    /**
     * Создает отчет и заполняет его поля.
     * @param model модель
     * @param reportFile файл отчета
     * @return Отчет "Сводка по направлениям и профилям"
     */
    <T extends EnrollmentDirProfReportModel> EnrollmentDirProfReport createReport(T model, DatabaseFile reportFile);

    /**
     * Создает печатную форму отчета "Сводка по направлениям и профилям".
     * @param model модель
     * @return Файл отчета хранимый в базе данных
     */
    <T extends EnrollmentDirProfReportModel> DatabaseFile createPrintReportFile(T model) throws IOException, WriteException;
}
