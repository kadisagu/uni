/* $Id$ */
package ru.tandemservice.uniec.component.report.SummaryQuotasMarksResults.Add;

import java.util.Date;
import java.util.List;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import org.tandemframework.shared.commonbase.utils.CommonYesNoUIObject;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.report.SummaryQuotasMarksResultsReport;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;

/**
 * @author Vasily Zhukov
 * @since 18.08.2011
 */
public class Model implements MultiEnrollmentDirectionUtil.Model, IEnrollmentCampaignSelectModel
{
    final static int ENROLLMENT_CAMP_STAGE_DOCUMENTS = 0;
    final static int ENROLLMENT_CAMP_STAGE_EXAMS = 1;
    final static int ENROLLMENT_CAMP_STAGE_ENROLLMENT = 2;

    private SummaryQuotasMarksResultsReport _report = new SummaryQuotasMarksResultsReport();
    private MultiEnrollmentDirectionUtil.Parameters _parameters;
    private IPrincipalContext _principalContext;

    private boolean _studentCategoryActive;
    private boolean _qualificationActive;
    private boolean _compensationTypeActive;
    private boolean _formativeOrgUnitActive;
    private boolean _territorialOrgUnitActive;
    private boolean _educationLevelHighSchoolActive;
    private boolean _developFormActive;
    private boolean _developConditionActive;
    private boolean _developTechActive;
    private boolean _developPeriodActive;
    private boolean _parallelActive;

    private List<EnrollmentCampaign> _enrollmentCampaignList;
    private List<IdentifiableWrapper> _enrollmentCampaignStageList;
    private ISelectModel _studentCategoryListModel;
    private ISelectModel _qualificationListModel;
    private ISelectModel _compensationTypeListModel;
    private ISelectModel _formativeOrgUnitListModel;
    private ISelectModel _territorialOrgUnitListModel;
    private ISelectModel _educationLevelHighSchoolListModel;
    private ISelectModel _developFormListModel;
    private ISelectModel _developConditionListModel;
    private ISelectModel _developTechListModel;
    private ISelectModel _developPeriodListModel;
    private List<CommonYesNoUIObject> _parallelList;

    private IdentifiableWrapper _enrollmentCampaignStage;
    private List<StudentCategory> _studentCategoryList;
    private List<Qualifications> _qualificationList;
    private CompensationType _compensationType;
    private List<OrgUnit> _formativeOrgUnitList;
    private List<OrgUnit> _territorialOrgUnitList;
    private List<EducationLevelsHighSchool> _educationLevelHighSchoolList;
    private List<DevelopForm> _developFormList;
    private List<DevelopCondition> _developConditionList;
    private List<DevelopTech> _developTechList;
    private List<DevelopPeriod> _developPeriodList;
    private CommonYesNoUIObject _parallel;

    private String _enroll3007title;
    private Date _enroll3007from;
    private Date _enroll3007to;
    private String _enroll0508title;
    private Date _enroll0508from;
    private Date _enroll0508to;
    private String _enroll3108title;
    private Date _enroll3108from;
    private Date _enroll3108to;

    // MultiEnrollmentDirectionUtil.Model

    @Override
    public List<EnrollmentCampaign> getSelectedEnrollmentCampaignList()
    {
        return MultiEnrollmentDirectionUtil.getSingletonList(getReport().getEnrollmentCampaign());
    }

    @Override
    public List<OrgUnit> getSelectedFormativeOrgUnitList()
    {
        return isFormativeOrgUnitActive() ? getFormativeOrgUnitList() : null;
    }

    @Override
    public List<OrgUnit> getSelectedTerritorialOrgUnitList()
    {
        return isTerritorialOrgUnitActive() ? getTerritorialOrgUnitList() : null;
    }

    @Override
    public List<EducationLevelsHighSchool> getSelectedEducationLevelHighSchoolList()
    {
        return isEducationLevelHighSchoolActive() ? getEducationLevelHighSchoolList() : null;
    }

    @Override
    public List<DevelopForm> getSelectedDevelopFormList()
    {
        return isDevelopFormActive() ? getDevelopFormList() : null;
    }

    @Override
    public List<DevelopCondition> getSelectedDevelopConditionList()
    {
        return isDevelopConditionActive() ? getDevelopConditionList() : null;
    }

    @Override
    public List<DevelopTech> getSelectedDevelopTechList()
    {
        return isDevelopTechActive() ? getDevelopTechList() : null;
    }

    @Override
    public List<DevelopPeriod> getSelectedDevelopPeriodList()
    {
        return isDevelopPeriodActive() ? getDevelopPeriodList() : null;
    }

    // IEnrollmentCampaignSelectModel

    @Override
    public IDataSettings getSettings()
    {
        return null;
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _report.getEnrollmentCampaign();
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _report.setEnrollmentCampaign(enrollmentCampaign);
    }

    // Getters & Setters

    public SummaryQuotasMarksResultsReport getReport()
    {
        return _report;
    }

    public void setReport(SummaryQuotasMarksResultsReport report)
    {
        _report = report;
    }

    @Override
    public MultiEnrollmentDirectionUtil.Parameters getParameters()
    {
        return _parameters;
    }

    public void setParameters(MultiEnrollmentDirectionUtil.Parameters parameters)
    {
        _parameters = parameters;
    }

    public IPrincipalContext getPrincipalContext()
    {
        return _principalContext;
    }

    public void setPrincipalContext(IPrincipalContext principalContext)
    {
        _principalContext = principalContext;
    }

    public boolean isStudentCategoryActive()
    {
        return _studentCategoryActive;
    }

    public void setStudentCategoryActive(boolean studentCategoryActive)
    {
        _studentCategoryActive = studentCategoryActive;
    }

    public boolean isQualificationActive()
    {
        return _qualificationActive;
    }

    public void setQualificationActive(boolean qualificationActive)
    {
        _qualificationActive = qualificationActive;
    }

    public boolean isCompensationTypeActive()
    {
        return _compensationTypeActive;
    }

    public void setCompensationTypeActive(boolean compensationTypeActive)
    {
        _compensationTypeActive = compensationTypeActive;
    }

    public boolean isFormativeOrgUnitActive()
    {
        return _formativeOrgUnitActive;
    }

    public void setFormativeOrgUnitActive(boolean formativeOrgUnitActive)
    {
        _formativeOrgUnitActive = formativeOrgUnitActive;
    }

    public boolean isTerritorialOrgUnitActive()
    {
        return _territorialOrgUnitActive;
    }

    public void setTerritorialOrgUnitActive(boolean territorialOrgUnitActive)
    {
        _territorialOrgUnitActive = territorialOrgUnitActive;
    }

    public boolean isEducationLevelHighSchoolActive()
    {
        return _educationLevelHighSchoolActive;
    }

    public void setEducationLevelHighSchoolActive(boolean educationLevelHighSchoolActive)
    {
        _educationLevelHighSchoolActive = educationLevelHighSchoolActive;
    }

    public boolean isDevelopFormActive()
    {
        return _developFormActive;
    }

    public void setDevelopFormActive(boolean developFormActive)
    {
        _developFormActive = developFormActive;
    }

    public boolean isDevelopConditionActive()
    {
        return _developConditionActive;
    }

    public void setDevelopConditionActive(boolean developConditionActive)
    {
        _developConditionActive = developConditionActive;
    }

    public boolean isDevelopTechActive()
    {
        return _developTechActive;
    }

    public void setDevelopTechActive(boolean developTechActive)
    {
        _developTechActive = developTechActive;
    }

    public boolean isDevelopPeriodActive()
    {
        return _developPeriodActive;
    }

    public void setDevelopPeriodActive(boolean developPeriodActive)
    {
        _developPeriodActive = developPeriodActive;
    }

    public boolean isParallelActive()
    {
        return _parallelActive;
    }

    public void setParallelActive(boolean parallelActive)
    {
        _parallelActive = parallelActive;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public List<IdentifiableWrapper> getEnrollmentCampaignStageList()
    {
        return _enrollmentCampaignStageList;
    }

    public void setEnrollmentCampaignStageList(List<IdentifiableWrapper> enrollmentCampaignStageList)
    {
        _enrollmentCampaignStageList = enrollmentCampaignStageList;
    }

    public ISelectModel getStudentCategoryListModel()
    {
        return _studentCategoryListModel;
    }

    public void setStudentCategoryListModel(ISelectModel studentCategoryListModel)
    {
        _studentCategoryListModel = studentCategoryListModel;
    }

    public ISelectModel getQualificationListModel()
    {
        return _qualificationListModel;
    }

    public void setQualificationListModel(ISelectModel qualificationListModel)
    {
        _qualificationListModel = qualificationListModel;
    }

    public ISelectModel getCompensationTypeListModel()
    {
        return _compensationTypeListModel;
    }

    public void setCompensationTypeListModel(ISelectModel compensationTypeListModel)
    {
        _compensationTypeListModel = compensationTypeListModel;
    }

    public ISelectModel getFormativeOrgUnitListModel()
    {
        return _formativeOrgUnitListModel;
    }

    public void setFormativeOrgUnitListModel(ISelectModel formativeOrgUnitListModel)
    {
        _formativeOrgUnitListModel = formativeOrgUnitListModel;
    }

    public ISelectModel getTerritorialOrgUnitListModel()
    {
        return _territorialOrgUnitListModel;
    }

    public void setTerritorialOrgUnitListModel(ISelectModel territorialOrgUnitListModel)
    {
        _territorialOrgUnitListModel = territorialOrgUnitListModel;
    }

    public ISelectModel getEducationLevelHighSchoolListModel()
    {
        return _educationLevelHighSchoolListModel;
    }

    public void setEducationLevelHighSchoolListModel(ISelectModel educationLevelHighSchoolListModel)
    {
        _educationLevelHighSchoolListModel = educationLevelHighSchoolListModel;
    }

    public ISelectModel getDevelopFormListModel()
    {
        return _developFormListModel;
    }

    public void setDevelopFormListModel(ISelectModel developFormListModel)
    {
        _developFormListModel = developFormListModel;
    }

    public ISelectModel getDevelopConditionListModel()
    {
        return _developConditionListModel;
    }

    public void setDevelopConditionListModel(ISelectModel developConditionListModel)
    {
        _developConditionListModel = developConditionListModel;
    }

    public ISelectModel getDevelopTechListModel()
    {
        return _developTechListModel;
    }

    public void setDevelopTechListModel(ISelectModel developTechListModel)
    {
        _developTechListModel = developTechListModel;
    }

    public ISelectModel getDevelopPeriodListModel()
    {
        return _developPeriodListModel;
    }

    public void setDevelopPeriodListModel(ISelectModel developPeriodListModel)
    {
        _developPeriodListModel = developPeriodListModel;
    }

    public List<CommonYesNoUIObject> getParallelList()
    {
        return _parallelList;
    }

    public void setParallelList(List<CommonYesNoUIObject> parallelList)
    {
        _parallelList = parallelList;
    }

    public IdentifiableWrapper getEnrollmentCampaignStage()
    {
        return _enrollmentCampaignStage;
    }

    public void setEnrollmentCampaignStage(IdentifiableWrapper enrollmentCampaignStage)
    {
        _enrollmentCampaignStage = enrollmentCampaignStage;
    }

    public List<StudentCategory> getStudentCategoryList()
    {
        return _studentCategoryList;
    }

    public void setStudentCategoryList(List<StudentCategory> studentCategoryList)
    {
        _studentCategoryList = studentCategoryList;
    }

    public List<Qualifications> getQualificationList()
    {
        return _qualificationList;
    }

    public void setQualificationList(List<Qualifications> qualificationList)
    {
        _qualificationList = qualificationList;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

    public List<OrgUnit> getFormativeOrgUnitList()
    {
        return _formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(List<OrgUnit> formativeOrgUnitList)
    {
        _formativeOrgUnitList = formativeOrgUnitList;
    }

    public List<OrgUnit> getTerritorialOrgUnitList()
    {
        return _territorialOrgUnitList;
    }

    public void setTerritorialOrgUnitList(List<OrgUnit> territorialOrgUnitList)
    {
        _territorialOrgUnitList = territorialOrgUnitList;
    }

    public List<EducationLevelsHighSchool> getEducationLevelHighSchoolList()
    {
        return _educationLevelHighSchoolList;
    }

    public void setEducationLevelHighSchoolList(List<EducationLevelsHighSchool> educationLevelHighSchoolList)
    {
        _educationLevelHighSchoolList = educationLevelHighSchoolList;
    }

    public List<DevelopForm> getDevelopFormList()
    {
        return _developFormList;
    }

    public void setDevelopFormList(List<DevelopForm> developFormList)
    {
        _developFormList = developFormList;
    }

    public List<DevelopCondition> getDevelopConditionList()
    {
        return _developConditionList;
    }

    public void setDevelopConditionList(List<DevelopCondition> developConditionList)
    {
        _developConditionList = developConditionList;
    }

    public List<DevelopTech> getDevelopTechList()
    {
        return _developTechList;
    }

    public void setDevelopTechList(List<DevelopTech> developTechList)
    {
        _developTechList = developTechList;
    }

    public List<DevelopPeriod> getDevelopPeriodList()
    {
        return _developPeriodList;
    }

    public void setDevelopPeriodList(List<DevelopPeriod> developPeriodList)
    {
        _developPeriodList = developPeriodList;
    }

    public CommonYesNoUIObject getParallel()
    {
        return _parallel;
    }

    public void setParallel(CommonYesNoUIObject parallel)
    {
        _parallel = parallel;
    }

    public String getEnroll3007title()
    {
        return _enroll3007title;
    }

    public void setEnroll3007title(String enroll3007title)
    {
        _enroll3007title = enroll3007title;
    }

    public Date getEnroll3007from()
    {
        return _enroll3007from;
    }

    public void setEnroll3007from(Date enroll3007from)
    {
        _enroll3007from = enroll3007from;
    }

    public Date getEnroll3007to()
    {
        return _enroll3007to;
    }

    public void setEnroll3007to(Date enroll3007to)
    {
        _enroll3007to = enroll3007to;
    }

    public String getEnroll0508title()
    {
        return _enroll0508title;
    }

    public void setEnroll0508title(String enroll0508title)
    {
        _enroll0508title = enroll0508title;
    }

    public Date getEnroll0508from()
    {
        return _enroll0508from;
    }

    public void setEnroll0508from(Date enroll0508from)
    {
        _enroll0508from = enroll0508from;
    }

    public Date getEnroll0508to()
    {
        return _enroll0508to;
    }

    public void setEnroll0508to(Date enroll0508to)
    {
        _enroll0508to = enroll0508to;
    }

    public String getEnroll3108title()
    {
        return _enroll3108title;
    }

    public void setEnroll3108title(String enroll3108title)
    {
        _enroll3108title = enroll3108title;
    }

    public Date getEnroll3108from()
    {
        return _enroll3108from;
    }

    public void setEnroll3108from(Date enroll3108from)
    {
        _enroll3108from = enroll3108from;
    }

    public Date getEnroll3108to()
    {
        return _enroll3108to;
    }

    public void setEnroll3108to(Date enroll3108to)
    {
        _enroll3108to = enroll3108to;
    }
}
