/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.menu.StateExamExportPrint;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.codes.IdentityCardTypeCodes;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate;
import ru.tandemservice.uniec.entity.entrant.StateExamSubjectMark;
import ru.tandemservice.uniec.ui.EntrantStateExamCertificateNumberFormatter;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vip_delete
 * @since 06.04.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    protected static final String SEPARATOR = "%";

    @Override
    public void prepare(Model model)
    {
        model.setEnrollmentCampaign(getNotNull(EnrollmentCampaign.class, model.getEnrollmentCampaign().getId()));

        switch (model.getExportType())
        {
            case UniecDefines.EXPORT_TYPE_BY_CERTIFICATE_NUMBER_WITHOUT_FIO:
                prepareExportDataByCertificateNumberWithoutFio(model);
                return;
            case UniecDefines.EXPORT_TYPE_BY_IDENTITY_CARD_WITHOUT_FIO:
                prepareExportDataByIdentityCardWithoutFio(model);
                return;
            case UniecDefines.EXPORT_TYPE_BY_CERTIFICATE_NUMBER:
                prepareExportDataByCertificateNumber(model);
                return;
            case UniecDefines.EXPORT_TYPE_BY_IDENTITY_CARD:
                prepareExportDataByIdentityCard(model);
                return;
            case UniecDefines.EXPORT_TYPE_BY_CERTIFICATE_NUMBER_WITH_FIO:
                prepareExportDataByCertificateNumberWithFio(model);
                return;
            case UniecDefines.EXPORT_TYPE_BY_FIO_WITH_DOC:
                prepareExportDataByFioWithDoc(model);
                return;
        }

        throw new RuntimeException("Unknown export type: " + model.getExportType());
    }

    private void prepareExportDataByCertificateNumberWithoutFio(Model model)
    {
        final int IDX_ID = 0;
        final int IDX_NUM = 1;
        final int IDX_LASTNAME = 2;
        final int IDX_FIRSTNAME = 3;
        final int IDX_MIDDLENAME = 4;
        final int IDX_CODE = 5;
        final int IDX_MARK = 6;

        final MQBuilder builder = new MQBuilder(StateExamSubjectMark.ENTITY_CLASS, "m", new String[]{
            /* 0 */ StateExamSubjectMark.L_CERTIFICATE + "." + EntrantStateExamCertificate.P_ID,
            /* 1 */ StateExamSubjectMark.L_CERTIFICATE + "." + EntrantStateExamCertificate.P_NUMBER,
            /* 2 */ StateExamSubjectMark.L_CERTIFICATE + "." + EntrantStateExamCertificate.L_ENTRANT + "." + Entrant.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_LAST_NAME,
            /* 3 */ StateExamSubjectMark.L_CERTIFICATE + "." + EntrantStateExamCertificate.L_ENTRANT + "." + Entrant.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_FIRST_NAME,
            /* 4 */ StateExamSubjectMark.L_CERTIFICATE + "." + EntrantStateExamCertificate.L_ENTRANT + "." + Entrant.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_MIDDLE_NAME,
            /* 5 */ StateExamSubjectMark.L_SUBJECT + "." + StateExamSubject.P_CODE,
            /* 6 */ StateExamSubjectMark.P_MARK
        });

        if (model.getAccepted() != null)
            builder.add(MQExpression.eq("m", StateExamSubjectMark.certificate().accepted().s(), model.getAccepted().equals(ru.tandemservice.uniec.component.menu.StateExamExport.Model.CERTIFICATE_ACCEPTED_ID)));

        if (model.getSent() != null)
            builder.add(MQExpression.eq("m", StateExamSubjectMark.certificate().sent().s(), model.getSent().equals(ru.tandemservice.uniec.component.menu.StateExamExport.Model.CERTIFICATE_SENT_ID)));

        builder.add(MQExpression.eq("m", StateExamSubjectMark.L_CERTIFICATE + "." + EntrantStateExamCertificate.L_ENTRANT + "." + Entrant.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
        builder.add(UniMQExpression.betweenDate("m", StateExamSubjectMark.L_CERTIFICATE + "." + EntrantStateExamCertificate.P_REGISTRATION_DATE, model.getDateFrom(), model.getDateTo()));
        builder.addOrder("m", StateExamSubjectMark.L_CERTIFICATE + "." + EntrantStateExamCertificate.P_ID);

        List<Object[]> markList = builder.getResultList(getSession());

        //если в строке меньше 2-х баллов, то печатаем ее в конце
        List<String> normMarkList = new LinkedList<String>();
        List<String> lowMarkList = new LinkedList<String>();
        int count = getCount(StateExamSubject.class);
        Integer[] marks = new Integer[count];
        Long prevId = null;
        String number = null;
        String lastName = null;
        String firstName = null;
        String middleName = null;
        for (Object[] data : markList)
        {
            // берем очередную строку
            Long id = (Long) data[IDX_ID];
            if (id.equals(prevId))
            {
                // сертификат совпадает -> берем только оценку по предмету
                int code = Integer.parseInt((String) data[IDX_CODE]);
                int mark = (Integer) data[IDX_MARK];
                marks[code - 1] = mark; // вычитаем 1, так как коды начинаются с 1
            }
            else
            {
                // id не совпадают. делаем flush
                if (prevId != null)
                {
                    if (isLowMark(marks))
                        lowMarkList.add(flushExportDataByCertificateNumber(number, lastName, firstName, middleName, marks));
                    else
                        normMarkList.add(flushExportDataByCertificateNumber(number, lastName, firstName, middleName, marks));
                }

                // потом делаем разбор текущей строки
                marks = new Integer[count];
                int code = Integer.parseInt((String) data[IDX_CODE]);
                int mark = (Integer) data[IDX_MARK];
                marks[code - 1] = mark; // вычитаем 1, так как коды начинаются с 1

                prevId = id;
                number = (String) data[IDX_NUM];
                //original = (Boolean) data[IDX_ORIGINAL];
                lastName = (String) data[IDX_LASTNAME];
                firstName = (String) data[IDX_FIRSTNAME];
                middleName = (String) data[IDX_MIDDLENAME];
            }
        }

        if (markList.size() > 0)
        {
            if (isLowMark(marks))
                lowMarkList.add(flushExportDataByCertificateNumber(number, lastName, firstName, middleName, marks));
            else
                normMarkList.add(flushExportDataByCertificateNumber(number, lastName, firstName, middleName, marks));
        }

        StringBuilder buffer = new StringBuilder();
        buffer.append(StringUtils.join(normMarkList, '\n'));

        if (!normMarkList.isEmpty())
            buffer.append('\n');

        buffer.append(StringUtils.join(lowMarkList, '\n'));

        model.setExportData(buffer);
    }

    private static String flushExportDataByCertificateNumber(String number, String lastName, String firstName, String middleName, Integer[] marks)
    {
        StringBuilder buffer = new StringBuilder();

        // разбиваем полный номер свидетельства на части
        String[] numberParts = EntrantStateExamCertificateNumberFormatter.split(number);

        buffer.append(numberParts[0]).append('-').append(numberParts[1]).append('-').append(numberParts[2]).append('%');// номер сертификата

        for (int i = 0; i < marks.length - 1; i++)
            buffer.append(marks[i] == null ? "" : Integer.toString(marks[i])).append('%');
        buffer.append(marks[marks.length - 1] == null ? "" : Integer.toString(marks[marks.length - 1]));

        return buffer.toString();
    }

    private boolean isLowMark(Integer[] marks)
    {
        int result = 0;
        for (Integer mark : marks)
        {
            if (mark != null)
                result++;
        }
        return result < 2;
    }

    private void prepareExportDataByIdentityCardWithoutFio(Model model)
    {
        final int IDX_ENTRANT_ID = 0;
        final int IDX_SERIA = 1;
        final int IDX_NUMBER = 2;

        final int IDX_SUBJECT_CODE = 1;
        final int IDX_MARK = 2;

        MQBuilder builder = new MQBuilder(Entrant.ENTITY_CLASS, "e", new String[]
        {
            /* 0 */ Entrant.id().s(),
            /* 1 */ Entrant.person().identityCard().seria().s(),
            /* 2 */ Entrant.person().identityCard().number().s()
        });
        builder.add(UniMQExpression.betweenDate("e", Entrant.P_REGISTRATION_DATE, model.getDateFrom(), model.getDateTo()));
        builder.add(MQExpression.eq("e", Entrant.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
        if (!model.isIncludeNonRF())
            builder.add(MQExpression.eq("e", Entrant.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.L_CARD_TYPE + ".code", UniDefines.CATALOG_IDENTITYCARD_TYPE_PASSPORT));
        if (model.getCertificateHoldingId() != null)
        {
            MQBuilder certificateBuilder = new MQBuilder(EntrantStateExamCertificate.ENTITY_CLASS, "certificate");
            certificateBuilder.add(MQExpression.eqProperty("certificate", EntrantStateExamCertificate.entrant().id().s(), "e", Entrant.id().s()));

            if (model.getCertificateHoldingId().equals(ru.tandemservice.uniec.component.menu.StateExamExport.Model.CERTIFICATE_HOLDING_ID))
            {
                //                if (model.getSent() != null)
                //                    certificateBuilder.add(MQExpression.eq("certificate", EntrantStateExamCertificate.sent().s(), model.getSent().equals(ru.tandemservice.uniec.component.menu.StateExamExport.Model.CERTIFICATE_SENT_ID)));
                builder.add(MQExpression.exists(certificateBuilder));
            }
            else
            {
                builder.add(MQExpression.notExists(certificateBuilder));
            }
        }
        builder.addOrder("e", Entrant.person().identityCard().lastName().s());
        builder.addOrder("e", Entrant.person().identityCard().firstName().s());
        builder.addOrder("e", Entrant.person().identityCard().middleName().s());

        List<Object[]> entrantList = builder.getResultList(getSession());

        final int count = getCount(StateExamSubject.class);

        final Map<Long, Integer[]> entrantMarksMap = new HashMap<Long, Integer[]>();
        for (Object[] entrant : entrantList) {
            entrantMarksMap.put((Long) entrant[IDX_ENTRANT_ID], null /*нет значения*/);
        }

        // слона будем есть по-частям
        BatchUtils.execute(entrantMarksMap.keySet(), DQL.MAX_VALUES_ROW_NUMBER, entrantIdList -> {

            MQBuilder markBuilder = new MQBuilder(StateExamSubjectMark.ENTITY_CLASS, "c", new String[] {
                /* 0 */ StateExamSubjectMark.certificate().entrant().id().s(),
                /* 1 */ StateExamSubjectMark.subject().code().s(),
                /* 2 */ StateExamSubjectMark.mark().s()
            });

            markBuilder.add(MQExpression.in("c", StateExamSubjectMark.certificate().entrant().id().s(), entrantIdList));
            markBuilder.add(MQExpression.eq("c", StateExamSubjectMark.certificate().accepted().s(), true));
            List<Object[]> markList = markBuilder.getResultList(getSession());

            for (Object[] mark : markList)
            {
                Long entrantId = (Long) mark[IDX_ENTRANT_ID];
                Integer[] marks = entrantMarksMap.get(entrantId);
                if (marks == null) { marks = new Integer[count]; }

                int code = Integer.parseInt((String) mark[IDX_SUBJECT_CODE]);
                Integer oldMark = marks[code - 1];
                Integer newMark = (Integer) mark[IDX_MARK];

                marks[code - 1] = oldMark != null && newMark <= oldMark ? oldMark : newMark;

                entrantMarksMap.put(entrantId, marks);
            }
        });


        final Integer[] placeholder = new Integer[count];
        StringBuilder buffer = new StringBuilder();
        for (Object[] entrant : entrantList)
        {
            Long entrantId = (Long) entrant[IDX_ENTRANT_ID];
            Integer[] marks = entrantMarksMap.get(entrantId) != null ? entrantMarksMap.get(entrantId) : placeholder;

            String seria = (String) entrant[IDX_SERIA];
            String number = (String) entrant[IDX_NUMBER];

            buffer.append(StringUtils.trimToEmpty(seria)).append('%');
            buffer.append(StringUtils.trimToEmpty(number));

            if (model.isIncludeEntrantWithoutMark())
            {
                buffer.append("%%%%%%%%%%%%%%");
            }
            else
            {
                for (Integer mark : marks) buffer.append('%').append(mark == null ? "" : Integer.toString(mark));
            }

            if (entrantList.size() > entrantList.indexOf(entrant) + 1)
                buffer.append('\n');
        }
        model.setExportData(buffer);
    }

    private void prepareExportDataByCertificateNumber(Model model)
    {
        final int IDX_ID = 0;
        final int IDX_NUM = 1;
        final int IDX_LASTNAME = 2;
        final int IDX_FIRSTNAME = 3;
        final int IDX_MIDDLENAME = 4;
        final int IDX_CODE = 5;
        final int IDX_MARK = 6;

        final MQBuilder builder = new MQBuilder(StateExamSubjectMark.ENTITY_CLASS, "m", new String[]{
            /* 0 */ StateExamSubjectMark.L_CERTIFICATE + "." + EntrantStateExamCertificate.P_ID,
            /* 1 */ StateExamSubjectMark.L_CERTIFICATE + "." + EntrantStateExamCertificate.P_NUMBER,
            /* 2 */ StateExamSubjectMark.L_CERTIFICATE + "." + EntrantStateExamCertificate.L_ENTRANT + "." + Entrant.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_LAST_NAME,
            /* 3 */ StateExamSubjectMark.L_CERTIFICATE + "." + EntrantStateExamCertificate.L_ENTRANT + "." + Entrant.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_FIRST_NAME,
            /* 4 */ StateExamSubjectMark.L_CERTIFICATE + "." + EntrantStateExamCertificate.L_ENTRANT + "." + Entrant.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_MIDDLE_NAME,
            /* 5 */ StateExamSubjectMark.L_SUBJECT + "." + StateExamSubject.P_CODE,
            /* 6 */ StateExamSubjectMark.P_MARK
        });

        if (model.getAccepted() != null)
            builder.add(MQExpression.eq("m", StateExamSubjectMark.certificate().accepted().s(), model.getAccepted().equals(ru.tandemservice.uniec.component.menu.StateExamExport.Model.CERTIFICATE_ACCEPTED_ID)));

        if (model.getSent() != null)
            builder.add(MQExpression.eq("m", StateExamSubjectMark.certificate().sent().s(), model.getSent().equals(ru.tandemservice.uniec.component.menu.StateExamExport.Model.CERTIFICATE_SENT_ID)));

        builder.add(MQExpression.eq("m", StateExamSubjectMark.L_CERTIFICATE + "." + EntrantStateExamCertificate.L_ENTRANT + "." + Entrant.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
        builder.add(UniMQExpression.betweenDate("m", StateExamSubjectMark.L_CERTIFICATE + "." + EntrantStateExamCertificate.P_REGISTRATION_DATE, model.getDateFrom(), model.getDateTo()));
        builder.addOrder("m", StateExamSubjectMark.L_CERTIFICATE + "." + EntrantStateExamCertificate.P_ID);

        List<Object[]> markList = builder.getResultList(getSession());

        StringBuilder buffer = new StringBuilder();
        int count = getCount(StateExamSubject.class);
        Integer[] marks = new Integer[count];
        Long prevId = null;
        String number = null;
        String lastName = null;
        String firstName = null;
        String middleName = null;
        for (Object[] data : markList)
        {
            // берем очередную строку
            Long id = (Long) data[IDX_ID];
            if (id.equals(prevId))
            {
                // сертификат совпадает -> берем только оценку по предмету
                int code = Integer.parseInt((String) data[IDX_CODE]);
                int mark = (Integer) data[IDX_MARK];
                marks[code - 1] = mark; // вычитаем 1, так как коды начинаются с 1
            } else
            {
                // id не совпадают. делаем flush
                if (prevId != null)
                {
                    flushExportDataByCertificateNumber(buffer, number, lastName, firstName, middleName, marks);
                    buffer.append('\n');
                }

                // потом делаем разбор текущей строки
                marks = new Integer[count];
                int code = Integer.parseInt((String) data[IDX_CODE]);
                int mark = (Integer) data[IDX_MARK];
                marks[code - 1] = mark; // вычитаем 1, так как коды начинаются с 1

                prevId = id;
                number = (String) data[IDX_NUM];
                //original = (Boolean) data[IDX_ORIGINAL];
                lastName = (String) data[IDX_LASTNAME];
                firstName = (String) data[IDX_FIRSTNAME];
                middleName = (String) data[IDX_MIDDLENAME];
            }
        }
        if (markList.size() > 0)
            flushExportDataByCertificateNumber(buffer, number, lastName, firstName, middleName, marks);
        else
            buffer.append('\n');
        model.setExportData(buffer);
    }

    private static void flushExportDataByCertificateNumber(StringBuilder buffer, String number, String lastName, String firstName, String middleName, Integer[] marks)
    {
        // разбиваем полный номер свидетельства на части
        String[] numberParts = EntrantStateExamCertificateNumberFormatter.split(number);

        buffer.append(numberParts[0]).append('-').append(numberParts[1]).append('-').append(numberParts[2]).append('%'); // номер сертификата
        buffer.append(StringUtils.trimToEmpty(lastName)).append('%');
        buffer.append(StringUtils.trimToEmpty(firstName)).append('%');
        buffer.append(StringUtils.trimToEmpty(middleName)).append('%');
        for (int i = 0; i < marks.length - 1; i++)
            buffer.append(marks[i] == null ? "" : Integer.toString(marks[i])).append('%');
        buffer.append(marks[marks.length - 1] == null ? "" : Integer.toString(marks[marks.length - 1]));
    }

    private void prepareExportDataByIdentityCard(Model model)
    {
        final int IDX_LASTNAME = 0;
        final int IDX_FIRSTNAME = 1;
        final int IDX_MIDDLENAME = 2;
        final int IDX_SERIA = 3;
        final int IDX_NUMBER = 4;

        MQBuilder builder = new MQBuilder(Entrant.ENTITY_CLASS, "e", new String[]{
            /* 0 */ Entrant.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_LAST_NAME,
            /* 1 */ Entrant.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_FIRST_NAME,
            /* 2 */ Entrant.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_MIDDLE_NAME,
            /* 3 */ Entrant.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_SERIA,
            /* 4 */ Entrant.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_NUMBER,
        })
        .add(UniMQExpression.betweenDate("e", Entrant.P_REGISTRATION_DATE, model.getDateFrom(), model.getDateTo()));
        builder.add(MQExpression.eq("e", Entrant.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
        if (!model.isIncludeNonRF())
            builder.add(MQExpression.eq("e", Entrant.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.L_CARD_TYPE + ".code", UniDefines.CATALOG_IDENTITYCARD_TYPE_PASSPORT));
        if (model.getCertificateHoldingId() != null)
        {
            MQBuilder certificateBuilder = new MQBuilder(EntrantStateExamCertificate.ENTITY_CLASS, "certificate");
            certificateBuilder.add(MQExpression.eqProperty("certificate", EntrantStateExamCertificate.entrant().id().s(), "e", Entrant.id().s()));

            if (model.getCertificateHoldingId().equals(ru.tandemservice.uniec.component.menu.StateExamExport.Model.CERTIFICATE_HOLDING_ID))
            {
                if (model.getSent() != null)
                    certificateBuilder.add(MQExpression.eq("certificate", EntrantStateExamCertificate.sent().s(), model.getSent().equals(ru.tandemservice.uniec.component.menu.StateExamExport.Model.CERTIFICATE_SENT_ID)));
                builder.add(MQExpression.exists(certificateBuilder));
            } else
            {
                builder.add(MQExpression.notExists(certificateBuilder));
            }
        }
        List<Object[]> entrantList = builder.getResultList(getSession());

        StringBuilder buffer = new StringBuilder();
        for (Object[] data : entrantList)
        {
            String lastName = (String) data[IDX_LASTNAME];
            String firstName = (String) data[IDX_FIRSTNAME];
            String middleName = (String) data[IDX_MIDDLENAME];
            String seria = (String) data[IDX_SERIA];
            String number = (String) data[IDX_NUMBER];

            buffer.append(StringUtils.trimToEmpty(lastName)).append('%');
            buffer.append(StringUtils.trimToEmpty(firstName)).append('%');
            buffer.append(StringUtils.trimToEmpty(middleName)).append('%');
            buffer.append(StringUtils.trimToEmpty(seria)).append('%');
            buffer.append(StringUtils.trimToEmpty(number));
            buffer.append('\n');
        }
        model.setExportData(buffer);
    }

    private void prepareExportDataByCertificateNumberWithFio(Model model)
    {
        DQLSelectBuilder dqlBuilder = new DQLSelectBuilder()
                .fromEntity(StateExamSubjectMark.class, "sesm")
                .distinct()
                .column(property("sesm", StateExamSubjectMark.certificateNumber()))
                .column(property("sesm", StateExamSubjectMark.certificate().entrant().person().identityCard().lastName()))
                .column(property("sesm", StateExamSubjectMark.certificate().entrant().person().identityCard().firstName()))
                .column(property("sesm", StateExamSubjectMark.certificate().entrant().person().identityCard().middleName()))
                .where(eq(property("sesm", StateExamSubjectMark.certificate().entrant().enrollmentCampaign()), value(model.getEnrollmentCampaign())))
                .where(isNotNull(property("sesm", StateExamSubjectMark.certificateNumber())))
                .where(between(property("sesm", StateExamSubjectMark.certificate().registrationDate()), valueDate(model.getDateFrom()), valueDate(model.getDateTo())));

        if (model.getAccepted() != null)
            dqlBuilder.where(eq(property("sesm", StateExamSubjectMark.certificate().accepted()),
                                value(model.getAccepted().equals(ru.tandemservice.uniec.component.menu.StateExamExport.Model.CERTIFICATE_ACCEPTED_ID))));

        if (model.getSent() != null)
            dqlBuilder.where(eq(property("sesm", StateExamSubjectMark.certificate().sent()),
                                value(model.getSent().equals(ru.tandemservice.uniec.component.menu.StateExamExport.Model.CERTIFICATE_SENT_ID))));

        StringBuilder result = this.<Object[]>getList(dqlBuilder).stream()
                .peek(row -> row[0] = StateExamSubjectMark.getFormatedCertificateNumber((String) row[0]))
                .collect(StringBuilder::new, DAO::addRowToStringBulder, StringBuilder::append);

        model.setExportData(result);
    }

    private void prepareExportDataByFioWithDoc(Model model)
    {
        DQLSelectBuilder dqlBuilder = new DQLSelectBuilder()
                .fromEntity(Entrant.class, "e")
                .column(property("e", Entrant.person().identityCard().lastName()))
                .column(property("e", Entrant.person().identityCard().firstName()))
                .column(property("e", Entrant.person().identityCard().middleName()))
                .column(property("e", Entrant.person().identityCard().seria()))
                .column(property("e", Entrant.person().identityCard().number()))
                .where(eq(property("e", Entrant.enrollmentCampaign()), value(model.getEnrollmentCampaign())))
                .where(between(property("e", Entrant.registrationDate()), valueDate(model.getDateFrom()), valueDate(model.getDateTo())));

        if (model.getCertificateHoldingId() != null)
        {
            String cerAlias = "cer";
            IDQLExpression certificateExpression = eq(property(cerAlias, EntrantStateExamCertificate.entrant()), property("e"));
            if (model.getCertificateHoldingId().equals(ru.tandemservice.uniec.component.menu.StateExamExport.Model.CERTIFICATE_HOLDING_ID))
                dqlBuilder.where(existsByExpr(EntrantStateExamCertificate.class, cerAlias, certificateExpression));
            else if (model.getCertificateHoldingId().equals(ru.tandemservice.uniec.component.menu.StateExamExport.Model.CERTIFICATE_NOT_HOLDING_ID))
                dqlBuilder.where(notExistsByExpr(EntrantStateExamCertificate.class, cerAlias, certificateExpression));
        }

        if (!model.isIncludeNonRF())
            dqlBuilder.where(eq(property("e", Entrant.person().identityCard().cardType().code()),
                                value(IdentityCardTypeCodes.PASPORT_GRAJDANINA_ROSSIYSKOY_FEDERATSII)));

        StringBuilder result = this.<Object[]>getList(dqlBuilder).stream()
                .collect(StringBuilder::new, DAO::addRowToStringBulder, StringBuilder::append);

        model.setExportData(result);
    }


    protected static void addRowToStringBulder(StringBuilder builder, Object[] row)
    {
        for (int i = 0; i < row.length; i++)
        {
            if (row[i] != null)
                builder.append((String)row[i]);
            if (i < row.length - 1)
                builder.append(SEPARATOR);
        }
        builder.append("\n");
    }
}
