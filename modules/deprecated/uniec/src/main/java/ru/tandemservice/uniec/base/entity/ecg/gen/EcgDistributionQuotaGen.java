package ru.tandemservice.uniec.base.entity.ecg.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistribution;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistributionQuota;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Количество мест по направлению приема в основном распределении
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EcgDistributionQuotaGen extends EntityBase
 implements INaturalIdentifiable<EcgDistributionQuotaGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.base.entity.ecg.EcgDistributionQuota";
    public static final String ENTITY_NAME = "ecgDistributionQuota";
    public static final int VERSION_HASH = -1529336660;
    private static IEntityMeta ENTITY_META;

    public static final String L_DISTRIBUTION = "distribution";
    public static final String L_DIRECTION = "direction";
    public static final String P_QUOTA = "quota";

    private EcgDistribution _distribution;     // Основное распределение абитуриентов
    private EnrollmentDirection _direction;     // Направление подготовки (специальность) приема
    private int _quota;     // Количество мест

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Основное распределение абитуриентов. Свойство не может быть null.
     */
    @NotNull
    public EcgDistribution getDistribution()
    {
        return _distribution;
    }

    /**
     * @param distribution Основное распределение абитуриентов. Свойство не может быть null.
     */
    public void setDistribution(EcgDistribution distribution)
    {
        dirty(_distribution, distribution);
        _distribution = distribution;
    }

    /**
     * @return Направление подготовки (специальность) приема. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentDirection getDirection()
    {
        return _direction;
    }

    /**
     * @param direction Направление подготовки (специальность) приема. Свойство не может быть null.
     */
    public void setDirection(EnrollmentDirection direction)
    {
        dirty(_direction, direction);
        _direction = direction;
    }

    /**
     * @return Количество мест. Свойство не может быть null.
     */
    @NotNull
    public int getQuota()
    {
        return _quota;
    }

    /**
     * @param quota Количество мест. Свойство не может быть null.
     */
    public void setQuota(int quota)
    {
        dirty(_quota, quota);
        _quota = quota;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EcgDistributionQuotaGen)
        {
            if (withNaturalIdProperties)
            {
                setDistribution(((EcgDistributionQuota)another).getDistribution());
                setDirection(((EcgDistributionQuota)another).getDirection());
            }
            setQuota(((EcgDistributionQuota)another).getQuota());
        }
    }

    public INaturalId<EcgDistributionQuotaGen> getNaturalId()
    {
        return new NaturalId(getDistribution(), getDirection());
    }

    public static class NaturalId extends NaturalIdBase<EcgDistributionQuotaGen>
    {
        private static final String PROXY_NAME = "EcgDistributionQuotaNaturalProxy";

        private Long _distribution;
        private Long _direction;

        public NaturalId()
        {}

        public NaturalId(EcgDistribution distribution, EnrollmentDirection direction)
        {
            _distribution = ((IEntity) distribution).getId();
            _direction = ((IEntity) direction).getId();
        }

        public Long getDistribution()
        {
            return _distribution;
        }

        public void setDistribution(Long distribution)
        {
            _distribution = distribution;
        }

        public Long getDirection()
        {
            return _direction;
        }

        public void setDirection(Long direction)
        {
            _direction = direction;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EcgDistributionQuotaGen.NaturalId) ) return false;

            EcgDistributionQuotaGen.NaturalId that = (NaturalId) o;

            if( !equals(getDistribution(), that.getDistribution()) ) return false;
            if( !equals(getDirection(), that.getDirection()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getDistribution());
            result = hashCode(result, getDirection());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getDistribution());
            sb.append("/");
            sb.append(getDirection());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EcgDistributionQuotaGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EcgDistributionQuota.class;
        }

        public T newInstance()
        {
            return (T) new EcgDistributionQuota();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "distribution":
                    return obj.getDistribution();
                case "direction":
                    return obj.getDirection();
                case "quota":
                    return obj.getQuota();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "distribution":
                    obj.setDistribution((EcgDistribution) value);
                    return;
                case "direction":
                    obj.setDirection((EnrollmentDirection) value);
                    return;
                case "quota":
                    obj.setQuota((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "distribution":
                        return true;
                case "direction":
                        return true;
                case "quota":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "distribution":
                    return true;
                case "direction":
                    return true;
                case "quota":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "distribution":
                    return EcgDistribution.class;
                case "direction":
                    return EnrollmentDirection.class;
                case "quota":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EcgDistributionQuota> _dslPath = new Path<EcgDistributionQuota>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EcgDistributionQuota");
    }
            

    /**
     * @return Основное распределение абитуриентов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgDistributionQuota#getDistribution()
     */
    public static EcgDistribution.Path<EcgDistribution> distribution()
    {
        return _dslPath.distribution();
    }

    /**
     * @return Направление подготовки (специальность) приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgDistributionQuota#getDirection()
     */
    public static EnrollmentDirection.Path<EnrollmentDirection> direction()
    {
        return _dslPath.direction();
    }

    /**
     * @return Количество мест. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgDistributionQuota#getQuota()
     */
    public static PropertyPath<Integer> quota()
    {
        return _dslPath.quota();
    }

    public static class Path<E extends EcgDistributionQuota> extends EntityPath<E>
    {
        private EcgDistribution.Path<EcgDistribution> _distribution;
        private EnrollmentDirection.Path<EnrollmentDirection> _direction;
        private PropertyPath<Integer> _quota;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Основное распределение абитуриентов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgDistributionQuota#getDistribution()
     */
        public EcgDistribution.Path<EcgDistribution> distribution()
        {
            if(_distribution == null )
                _distribution = new EcgDistribution.Path<EcgDistribution>(L_DISTRIBUTION, this);
            return _distribution;
        }

    /**
     * @return Направление подготовки (специальность) приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgDistributionQuota#getDirection()
     */
        public EnrollmentDirection.Path<EnrollmentDirection> direction()
        {
            if(_direction == null )
                _direction = new EnrollmentDirection.Path<EnrollmentDirection>(L_DIRECTION, this);
            return _direction;
        }

    /**
     * @return Количество мест. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgDistributionQuota#getQuota()
     */
        public PropertyPath<Integer> quota()
        {
            if(_quota == null )
                _quota = new PropertyPath<Integer>(EcgDistributionQuotaGen.P_QUOTA, this);
            return _quota;
        }

        public Class getEntityClass()
        {
            return EcgDistributionQuota.class;
        }

        public String getEntityName()
        {
            return "ecgDistributionQuota";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
