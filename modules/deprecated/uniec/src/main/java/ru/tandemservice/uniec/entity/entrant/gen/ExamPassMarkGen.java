package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.catalog.EntrantAbsenceNote;
import ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline;
import ru.tandemservice.uniec.entity.entrant.ExamPassMark;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Оценка по дисциплине для сдачи
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ExamPassMarkGen extends VersionedEntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.ExamPassMark";
    public static final String ENTITY_NAME = "examPassMark";
    public static final int VERSION_HASH = 1139176522;
    private static IEntityMeta ENTITY_META;

    public static final String P_VERSION = "version";
    public static final String L_EXAM_PASS_DISCIPLINE = "examPassDiscipline";
    public static final String L_ENTRANT_ABSENCE_NOTE = "entrantAbsenceNote";
    public static final String P_MARK = "mark";
    public static final String P_PAPER_CODE = "paperCode";
    public static final String P_EXAMINERS = "examiners";

    private int _version; 
    private ExamPassDiscipline _examPassDiscipline;     // Дисциплина для сдачи
    private EntrantAbsenceNote _entrantAbsenceNote;     // Отметка в отношении отсутствующих абитуриентов
    private double _mark;     // Балл
    private String _paperCode;     // Шифр работы
    private String _examiners;     // Экзаменаторы

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public int getVersion()
    {
        return _version;
    }

    /**
     * @param version  Свойство не может быть null.
     */
    public void setVersion(int version)
    {
        dirty(_version, version);
        _version = version;
    }

    /**
     * @return Дисциплина для сдачи. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public ExamPassDiscipline getExamPassDiscipline()
    {
        return _examPassDiscipline;
    }

    /**
     * @param examPassDiscipline Дисциплина для сдачи. Свойство не может быть null и должно быть уникальным.
     */
    public void setExamPassDiscipline(ExamPassDiscipline examPassDiscipline)
    {
        dirty(_examPassDiscipline, examPassDiscipline);
        _examPassDiscipline = examPassDiscipline;
    }

    /**
     * @return Отметка в отношении отсутствующих абитуриентов.
     */
    public EntrantAbsenceNote getEntrantAbsenceNote()
    {
        return _entrantAbsenceNote;
    }

    /**
     * @param entrantAbsenceNote Отметка в отношении отсутствующих абитуриентов.
     */
    public void setEntrantAbsenceNote(EntrantAbsenceNote entrantAbsenceNote)
    {
        dirty(_entrantAbsenceNote, entrantAbsenceNote);
        _entrantAbsenceNote = entrantAbsenceNote;
    }

    /**
     * @return Балл. Свойство не может быть null.
     */
    @NotNull
    public double getMark()
    {
        return _mark;
    }

    /**
     * @param mark Балл. Свойство не может быть null.
     */
    public void setMark(double mark)
    {
        dirty(_mark, mark);
        _mark = mark;
    }

    /**
     * @return Шифр работы.
     */
    @Length(max=255)
    public String getPaperCode()
    {
        return _paperCode;
    }

    /**
     * @param paperCode Шифр работы.
     */
    public void setPaperCode(String paperCode)
    {
        dirty(_paperCode, paperCode);
        _paperCode = paperCode;
    }

    /**
     * @return Экзаменаторы.
     */
    @Length(max=255)
    public String getExaminers()
    {
        return _examiners;
    }

    /**
     * @param examiners Экзаменаторы.
     */
    public void setExaminers(String examiners)
    {
        dirty(_examiners, examiners);
        _examiners = examiners;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ExamPassMarkGen)
        {
            setVersion(((ExamPassMark)another).getVersion());
            setExamPassDiscipline(((ExamPassMark)another).getExamPassDiscipline());
            setEntrantAbsenceNote(((ExamPassMark)another).getEntrantAbsenceNote());
            setMark(((ExamPassMark)another).getMark());
            setPaperCode(((ExamPassMark)another).getPaperCode());
            setExaminers(((ExamPassMark)another).getExaminers());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ExamPassMarkGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ExamPassMark.class;
        }

        public T newInstance()
        {
            return (T) new ExamPassMark();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "version":
                    return obj.getVersion();
                case "examPassDiscipline":
                    return obj.getExamPassDiscipline();
                case "entrantAbsenceNote":
                    return obj.getEntrantAbsenceNote();
                case "mark":
                    return obj.getMark();
                case "paperCode":
                    return obj.getPaperCode();
                case "examiners":
                    return obj.getExaminers();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "version":
                    obj.setVersion((Integer) value);
                    return;
                case "examPassDiscipline":
                    obj.setExamPassDiscipline((ExamPassDiscipline) value);
                    return;
                case "entrantAbsenceNote":
                    obj.setEntrantAbsenceNote((EntrantAbsenceNote) value);
                    return;
                case "mark":
                    obj.setMark((Double) value);
                    return;
                case "paperCode":
                    obj.setPaperCode((String) value);
                    return;
                case "examiners":
                    obj.setExaminers((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "version":
                        return true;
                case "examPassDiscipline":
                        return true;
                case "entrantAbsenceNote":
                        return true;
                case "mark":
                        return true;
                case "paperCode":
                        return true;
                case "examiners":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "version":
                    return true;
                case "examPassDiscipline":
                    return true;
                case "entrantAbsenceNote":
                    return true;
                case "mark":
                    return true;
                case "paperCode":
                    return true;
                case "examiners":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "version":
                    return Integer.class;
                case "examPassDiscipline":
                    return ExamPassDiscipline.class;
                case "entrantAbsenceNote":
                    return EntrantAbsenceNote.class;
                case "mark":
                    return Double.class;
                case "paperCode":
                    return String.class;
                case "examiners":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ExamPassMark> _dslPath = new Path<ExamPassMark>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ExamPassMark");
    }
            

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamPassMark#getVersion()
     */
    public static PropertyPath<Integer> version()
    {
        return _dslPath.version();
    }

    /**
     * @return Дисциплина для сдачи. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.entrant.ExamPassMark#getExamPassDiscipline()
     */
    public static ExamPassDiscipline.Path<ExamPassDiscipline> examPassDiscipline()
    {
        return _dslPath.examPassDiscipline();
    }

    /**
     * @return Отметка в отношении отсутствующих абитуриентов.
     * @see ru.tandemservice.uniec.entity.entrant.ExamPassMark#getEntrantAbsenceNote()
     */
    public static EntrantAbsenceNote.Path<EntrantAbsenceNote> entrantAbsenceNote()
    {
        return _dslPath.entrantAbsenceNote();
    }

    /**
     * @return Балл. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamPassMark#getMark()
     */
    public static PropertyPath<Double> mark()
    {
        return _dslPath.mark();
    }

    /**
     * @return Шифр работы.
     * @see ru.tandemservice.uniec.entity.entrant.ExamPassMark#getPaperCode()
     */
    public static PropertyPath<String> paperCode()
    {
        return _dslPath.paperCode();
    }

    /**
     * @return Экзаменаторы.
     * @see ru.tandemservice.uniec.entity.entrant.ExamPassMark#getExaminers()
     */
    public static PropertyPath<String> examiners()
    {
        return _dslPath.examiners();
    }

    public static class Path<E extends ExamPassMark> extends EntityPath<E>
    {
        private PropertyPath<Integer> _version;
        private ExamPassDiscipline.Path<ExamPassDiscipline> _examPassDiscipline;
        private EntrantAbsenceNote.Path<EntrantAbsenceNote> _entrantAbsenceNote;
        private PropertyPath<Double> _mark;
        private PropertyPath<String> _paperCode;
        private PropertyPath<String> _examiners;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamPassMark#getVersion()
     */
        public PropertyPath<Integer> version()
        {
            if(_version == null )
                _version = new PropertyPath<Integer>(ExamPassMarkGen.P_VERSION, this);
            return _version;
        }

    /**
     * @return Дисциплина для сдачи. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.entrant.ExamPassMark#getExamPassDiscipline()
     */
        public ExamPassDiscipline.Path<ExamPassDiscipline> examPassDiscipline()
        {
            if(_examPassDiscipline == null )
                _examPassDiscipline = new ExamPassDiscipline.Path<ExamPassDiscipline>(L_EXAM_PASS_DISCIPLINE, this);
            return _examPassDiscipline;
        }

    /**
     * @return Отметка в отношении отсутствующих абитуриентов.
     * @see ru.tandemservice.uniec.entity.entrant.ExamPassMark#getEntrantAbsenceNote()
     */
        public EntrantAbsenceNote.Path<EntrantAbsenceNote> entrantAbsenceNote()
        {
            if(_entrantAbsenceNote == null )
                _entrantAbsenceNote = new EntrantAbsenceNote.Path<EntrantAbsenceNote>(L_ENTRANT_ABSENCE_NOTE, this);
            return _entrantAbsenceNote;
        }

    /**
     * @return Балл. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamPassMark#getMark()
     */
        public PropertyPath<Double> mark()
        {
            if(_mark == null )
                _mark = new PropertyPath<Double>(ExamPassMarkGen.P_MARK, this);
            return _mark;
        }

    /**
     * @return Шифр работы.
     * @see ru.tandemservice.uniec.entity.entrant.ExamPassMark#getPaperCode()
     */
        public PropertyPath<String> paperCode()
        {
            if(_paperCode == null )
                _paperCode = new PropertyPath<String>(ExamPassMarkGen.P_PAPER_CODE, this);
            return _paperCode;
        }

    /**
     * @return Экзаменаторы.
     * @see ru.tandemservice.uniec.entity.entrant.ExamPassMark#getExaminers()
     */
        public PropertyPath<String> examiners()
        {
            if(_examiners == null )
                _examiners = new PropertyPath<String>(ExamPassMarkGen.P_EXAMINERS, this);
            return _examiners;
        }

        public Class getEntityClass()
        {
            return ExamPassMark.class;
        }

        public String getEntityName()
        {
            return "examPassMark";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
