package ru.tandemservice.uniec.entity.entrant;

import ru.tandemservice.uniec.entity.entrant.gen.OlympiadDiplomaGen;

/**
 * Диплом участника олимпиады
 */
public class OlympiadDiploma extends OlympiadDiplomaGen
{
    public static final Object DIPLOM_KEY = "diplomKey";

    public String getDiplomKey () {
        return getSeria() !=null ? getSeria() + " " + getNumber(): getNumber();
    }
}