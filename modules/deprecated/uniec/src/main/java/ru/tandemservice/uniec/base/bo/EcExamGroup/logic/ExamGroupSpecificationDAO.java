/**
 *$Id$
 */
package ru.tandemservice.uniec.base.bo.EcExamGroup.logic;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dao.CommonDAO;
import ru.tandemservice.uniec.entity.entrant.ExamGroupSpecification;

import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 09.07.12
 */
public class ExamGroupSpecificationDAO extends CommonDAO implements IExamGroupSpecificationDAO
{
    @Override
    public ErrorCollector validate(List<ExamGroupSpecification> entityList)
    {
        ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();

        for (ExamGroupSpecification specification : entityList)
        {
            if ((specification.getStartDisciplineDate() != null || specification.getFinishDisciplineDate() != null) && (specification.getStartDisciplineDate() == null || specification.getFinishDisciplineDate() == null))
                errorCollector.add("Поля «Дата и время начала ВИ» и «Дата и время окончания ВИ» должны быть заполнены, либо оставаться пустыми.",
                        "startDisciplineDateId_" + specification.getId(), "finishDisciplineDateId_" + specification.getId());

            if ((specification.getStartDisciplineDate() != null && specification.getFinishDisciplineDate() != null) && (specification.getStartDisciplineDate().getTime() >= specification.getFinishDisciplineDate().getTime()))
                errorCollector.add("«Дата и время начала ВИ» должна быть меньше «Дата и время окончания ВИ».",
                        "startDisciplineDateId_" + specification.getId(), "finishDisciplineDateId_" + specification.getId());
        }

        return errorCollector;
    }

    @Override
    public List<ExamGroupSpecification> saveOrUpdateEntity(List<ExamGroupSpecification> entityList)
    {
        for (ExamGroupSpecification specification : entityList)
            saveOrUpdate(specification);

        return entityList;
    }
}
