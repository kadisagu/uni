package ru.tandemservice.uniec.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniec_2x10x6_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.6")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность entranceExamMeetingByCKReport2010

		// создано свойство entrantCustomStateTitle
		{
			// создать колонку
			tool.createColumn("ntrncexmmtngbyckrprt2010_t", new DBColumn("entrantcustomstatetitle_p", DBType.createVarchar(255)));

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность entrantRatingSummaryReport

		// создано свойство entrantCustomStateTitle
		{
			// создать колонку
			tool.createColumn("entrantratingsummaryreport_t", new DBColumn("entrantcustomstatetitle_p", DBType.createVarchar(255)));

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность entrantDailyRatingByED2Report

		// создано свойство entrantCustomStateTitle
		{
			// создать колонку
			tool.createColumn("enr_rating_by_ed2_report_t", new DBColumn("entrantcustomstatetitle_p", DBType.createVarchar(255)));

		}

		// создано обязательное свойство withoutEnrolled
		{
			// создать колонку
			tool.createColumn("enr_rating_by_ed2_report_t", new DBColumn("withoutenrolled_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			tool.executeUpdate("update enr_rating_by_ed2_report_t set withoutenrolled_p=? where withoutenrolled_p is null", false);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr_rating_by_ed2_report_t", "withoutenrolled_p", false);

		}

    }
}