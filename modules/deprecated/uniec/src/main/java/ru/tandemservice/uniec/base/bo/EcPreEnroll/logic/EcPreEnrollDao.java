/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcPreEnroll.logic;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager;
import ru.tandemservice.uniec.base.bo.EcPreEnroll.util.PreEntrantDTO;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.util.report.RequestedEnrollmentDirectionComparator;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.sum;

/**
 * @author Vasily Zhukov
 * @since 26.05.2011
 */
public class EcPreEnrollDao extends CommonDAO implements IEcPreEnrollDao
{
    @SuppressWarnings("deprecation")
    @Override
    public List<PreEntrantDTO> getPreEntrantDTOList(EcPreEnrollParam parameters)
    {
        if(parameters == null) return Collections.emptyList();

        EnrollmentDirection enrollmentDirection = parameters.getEnrollmentDirection();
        CompensationType compensationType = parameters.getCompensationType();

        if (enrollmentDirection == null || compensationType == null) return Collections.emptyList();

        List<String> wrongStateCodes = Arrays.asList(UniecDefines.ENTRANT_STATE_ACTIVE_CODE, UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY);
        String dirAlias = "dir";
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(RequestedEnrollmentDirection.class, dirAlias)
                .column(dirAlias)
                .where(eq(property(dirAlias, RequestedEnrollmentDirection.enrollmentDirection()), value(enrollmentDirection)))
                .where(eq(property(dirAlias, RequestedEnrollmentDirection.compensationType()), value(compensationType)))
                .where(eq(property(dirAlias, RequestedEnrollmentDirection.entrantRequest().entrant().archival()), value(Boolean.FALSE)))
                .where(notIn(property(dirAlias, RequestedEnrollmentDirection.state().code()), wrongStateCodes));

        FilterUtils.applySelectFilter(builder, dirAlias, RequestedEnrollmentDirection.studentCategory(), parameters.getStudentCategoryList());
        FilterUtils.applySelectFilter(builder, dirAlias, RequestedEnrollmentDirection.originalDocumentHandedIn(), parameters.getOriginalDocumentHandledIn());
        FilterUtils.applySelectFilter(builder, dirAlias, RequestedEnrollmentDirection.agree4Enrollment(), parameters.getAgree4Enrollment());

        String preAlias = "pre";
        builder.joinEntity(dirAlias, DQLJoinType.left, PreliminaryEnrollmentStudent.class, preAlias,
                    eq(property(preAlias, PreliminaryEnrollmentStudent.requestedEnrollmentDirection()), property(dirAlias)))
            .column(property(preAlias));

        Boolean entrantCustomStateExist = parameters.getEntrantCustomStateExist();
        if (entrantCustomStateExist != null)
        {
            String ecsAlias = "ecs";
            IDQLExpression customStateExp = EntrantCustomState.getExpression4EntrantCustomState(property(dirAlias, RequestedEnrollmentDirection.entrantRequest().entrant()),
                                                                                                ecsAlias, parameters.getEntrantCustomStateList(), new Date());
            if (entrantCustomStateExist)
                builder.where(existsByExpr(EntrantCustomState.class, ecsAlias, customStateExp));
            else builder.where(notExistsByExpr(EntrantCustomState.class, ecsAlias, customStateExp));
        }

        HashMap<RequestedEnrollmentDirection, PreliminaryEnrollmentStudent> preStudentByDir = this.<Object[]>getList(builder).stream()
                .collect(HashMap::new, (map, row) -> map.put((RequestedEnrollmentDirection) row[0], (PreliminaryEnrollmentStudent) row[1]), HashMap::putAll);

        final List<PreliminaryEnrollmentStudent> preStudentList = preStudentByDir.values().stream().filter(Objects::nonNull).collect(Collectors.toList());

        final Map<RequestedEnrollmentDirection, Object> finalMarkMap = getFinalMarkMap(preStudentByDir.keySet(), enrollmentDirection.getEnrollmentCampaign());
        final Map<PreliminaryEnrollmentStudent, String> orderNumberMap = getOrderNumberMap(preStudentList);

        return preStudentByDir.entrySet().stream()
                .map(entry -> {
                    RequestedEnrollmentDirection direction = entry.getKey();
                    PreliminaryEnrollmentStudent preStudent = entry.getValue();
                    Person person = direction.getEntrantRequest().getEntrant().getPerson();
                    PersonEduInstitution personEduInstitution = person.getPersonEduInstitution();

                    Object finalMarkObject = finalMarkMap.get(direction);
                    Double finalMark = finalMarkObject instanceof Double ? (Double) finalMarkObject : null;
                    String finalMarkStr = finalMarkObject instanceof String ? (String) finalMarkObject : (finalMarkObject instanceof Double ? DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(finalMarkObject) : null);

                    return new PreEntrantDTO(direction, preStudent, finalMarkStr, finalMark,
                                             personEduInstitution == null ? null : personEduInstitution.getAverageMark(),
                                             preStudent == null ? null : orderNumberMap.get(preStudent),
                                             preStudentList.size());
                })
                .sorted(new RequestedEnrollmentDirectionComparator(
                        UniecDAOFacade.getSettingsDAO().getCompetitionKindPriorities(enrollmentDirection.getEnrollmentCampaign())))
                .collect(Collectors.toList());
    }

    private Map<RequestedEnrollmentDirection, Object> getFinalMarkMap(Collection<RequestedEnrollmentDirection> requestedEnrollmentDirectionCollection, final EnrollmentCampaign campaign)
    {
        if (requestedEnrollmentDirectionCollection.isEmpty()) { return Collections.emptyMap(); }

        // по собеседованию
        List<RequestedEnrollmentDirection> interview = new ArrayList<>();

        // не "по собеседованию" и "без вступительных испытаний"
        List<RequestedEnrollmentDirection> other = new ArrayList<>();

        for (RequestedEnrollmentDirection row : requestedEnrollmentDirectionCollection)
        {
            String competitionKind = row.getCompetitionKind().getCode();
            if (competitionKind.equals(UniecDefines.COMPETITION_KIND_INTERVIEW)) {
                interview.add(row);
            } else if (!competitionKind.equals(UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES)) {
                other.add(row);
            }
        }

        final Map<RequestedEnrollmentDirection, Object> result = new HashMap<>();

        // по собеседованию
        BatchUtils.execute(interview, DQL.MAX_VALUES_ROW_NUMBER, elements -> {
            final List<InterviewResult> list = new DQLSelectBuilder()
            .fromEntity(InterviewResult.class, "r").column(property("r"))
            .where(in(property(InterviewResult.requestedEnrollmentDirection().fromAlias("r")), elements))
            .createStatement(getSession()).list();

            for (InterviewResult interviewResult : list)
                result.put(interviewResult.getRequestedEnrollmentDirection(), interviewResult.isPassed() ? "прошел" : "не прошел");
        });

        // остальные
        BatchUtils.execute(other, DQL.MAX_VALUES_ROW_NUMBER, elements -> {
            final List<Object[]> list = new DQLSelectBuilder()
            .fromDataSource(
                new DQLSelectBuilder()
                .fromEntity(ChosenEntranceDiscipline.class, "d")
                .column(property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("d")), "ed_id")
                .column(sum(property(ChosenEntranceDiscipline.finalMark().fromAlias("d"))), "fm_s")
                .where(isNotNull(property(ChosenEntranceDiscipline.finalMark().fromAlias("d"))))
                .where(in(property(ChosenEntranceDiscipline.chosenEnrollmentDirection().fromAlias("d")), elements))
                .group(property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("d")))
                .buildQuery(),
                "x"
            )
            .joinEntity("x", DQLJoinType.inner, RequestedEnrollmentDirection.class, "ed", eq(property("ed.id"), property("x.ed_id")))
            .column(property("ed"))
            .column(property("x.fm_s"))
            .createStatement(getSession()).list();

            final Map<Long, Integer> individProgressMap = fillIndividualProgressMap(campaign, elements);

            for (Object[] row : list)
            {
                RequestedEnrollmentDirection direction = (RequestedEnrollmentDirection) row[0];
                Integer individMark = individProgressMap.get(direction.getId());
                result.put(direction, row[1] != null ? ((Number) row[1]).doubleValue() + (individMark == null? 0 : individMark) : null);
            }
        });

        // отдаем результат
        return result;
    }

    private Map<PreliminaryEnrollmentStudent, String> getOrderNumberMap(Collection<PreliminaryEnrollmentStudent> preliminaryEnrollmentStudentCollection)
    {
        if (preliminaryEnrollmentStudentCollection.isEmpty()) return Collections.emptyMap();

        final Map<PreliminaryEnrollmentStudent, String> orderNumberMap = new HashMap<>();

        BatchUtils.execute(preliminaryEnrollmentStudentCollection,
                           DQL.MAX_VALUES_ROW_NUMBER,
                           subCollection -> getList(EnrollmentExtract.class, EnrollmentExtract.entity(), subCollection)
                                   .forEach(item -> orderNumberMap.put(item.getEntity(),
                                                                       item.getOrder().getNumber() == null ? "" : item.getOrder().getNumber()))
        );

        return orderNumberMap;
    }

    @Override
    public PreliminaryEnrollmentStudent doChangeOrderType(Long requestedEnrollmentDirectionId, EducationOrgUnit educationOrgUnit, EntrantEnrollmentOrderType orderType)
    {
        RequestedEnrollmentDirection direction = getNotNull(requestedEnrollmentDirectionId);
        EnrollmentCampaign enrollmentCampaign = direction.getEntrantRequest().getEntrant().getEnrollmentCampaign();
        PreliminaryEnrollmentStudent preStudent = get(PreliminaryEnrollmentStudent.class, PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, direction);

        // если тип приказа не задан, то следует убрать предзачисление
        if (orderType == null)
        {
            if (preStudent != null)
                getSession().delete(preStudent);
            return null;
        }

        // получаем список разрешенных типов приказов
        List<EntrantEnrollmentOrderType> orderTypeList = EcOrderManager.instance().dao().getOrderTypeList(enrollmentCampaign);

        // если тип приказа уже не разрешен, то ничего не делаем
        if (!orderTypeList.contains(orderType)) return null;

        // если нужно изменить тип приказа предзачисления
        if (preStudent != null)
        {
            // если тип уже такой, то ничего не делаем
            if (preStudent.getEntrantEnrollmentOrderType().equals(orderType)) return preStudent;

            // иначе просто удаляем студента предзачисления.
            // из связных объектов может быть только выписка, но менять тип приказа, если уже есть сам приказ запрещено
            // поэтому удаление студента предзачисления корректно
            getSession().delete(preStudent);
        }

        preStudent = new PreliminaryEnrollmentStudent();
        preStudent.setRequestedEnrollmentDirection(direction);
        preStudent.setEducationOrgUnit(educationOrgUnit);
        preStudent.setEntrantEnrollmentOrderType(orderType);
        preStudent.setStudiedOPP(direction.isStudiedOPP());

        // небольшой хак с типами приказов: для приказов "О зачислении в число студентов" и "О зачислении в число студентов по целевому приему"
        // нужно брать вид затрат, вид целевого приема и категорию обучаемого из выбранного направления приема
        // для всех остальных типов приказов эти данные берем из условий зачисления (т.е. из самого студента предзачисления)
        if (UniecDefines.ORDER_TYPE_STUDENT.equals(orderType.getCode()) || UniecDefines.ORDER_TYPE_TARGET.equals(orderType.getCode()))
        {
            preStudent.setCompensationType(direction.getCompensationType());
            preStudent.setTargetAdmission((direction.isTargetAdmission()));
            preStudent.setStudentCategory(direction.getStudentCategory());
        } else
        {
            preStudent.setCompensationType(orderType.getCompensationType());
            preStudent.setTargetAdmission(orderType.isTargetAdmission() || direction.isTargetAdmission());
            preStudent.setStudentCategory(orderType.getStudentCategory());
        }

        CoreCollectionUtils.Pair<String, PreliminaryEnrollmentStudent> result = UniecDAOFacade.getEntrantDAO().saveOrUpdatePreliminaryStudent(preStudent);
        String error = result.getX();

        if (null != error)
            throw new ApplicationException(error);

        return result.getY();
    }

    public Map<Long, Integer> fillIndividualProgressMap(EnrollmentCampaign campaign, Collection<RequestedEnrollmentDirection> elements)
    {
        return new HashMap();
    }

    /**
     * @param directions Выбранные направления приема
     * @return мапа id направления - суммарный балл индивидуальных достижений
     */
    public Map<Long, Integer> getIndividualProgressByDirectionMap(Collection<RequestedEnrollmentDirection> directions)
    {
        String eipAlias = "eip";
        String reqAlias = "req";
        String dirAlias = "dir";
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EntrantIndividualProgress.class, eipAlias)
                .column(DQLFunctions.sum(property(eipAlias, EntrantIndividualProgress.mark())))

                .joinEntity(eipAlias, DQLJoinType.inner, EntrantRequest.class, reqAlias,
                            and(eq(property(eipAlias, EntrantIndividualProgress.entrant()),
                                   property(reqAlias, EntrantRequest.entrant())),
                                IndProg2Request4Exclude.getExpression4ExcludeEntrantIndividualProgress(property(reqAlias), property(eipAlias))
                            ))

                .joinEntity(reqAlias, DQLJoinType.inner, RequestedEnrollmentDirection.class, dirAlias,
                            and(eq(property(reqAlias), property(dirAlias, RequestedEnrollmentDirection.entrantRequest())),
                                in(property(dirAlias), directions))
                )
                .column(property(dirAlias, RequestedEnrollmentDirection.id()))
                .group(property(dirAlias, RequestedEnrollmentDirection.id()))

                .where(exists(EnrCampaignEntrantIndividualProgress.class,
                              EnrCampaignEntrantIndividualProgress.individualProgress().s(),
                              property(eipAlias, EntrantIndividualProgress.individualProgressType()),
                              EnrCampaignEntrantIndividualProgress.enrollmentCampaign().s(),
                              property(dirAlias, RequestedEnrollmentDirection.entrantRequest().entrant().enrollmentCampaign())
                ));

        return builder
                .createStatement(getSession()).<Object[]>list()
                .stream().collect(Collectors.toMap(raw -> (Long) raw[1], raw -> ((Number) raw[0]).intValue()));
    }
}
