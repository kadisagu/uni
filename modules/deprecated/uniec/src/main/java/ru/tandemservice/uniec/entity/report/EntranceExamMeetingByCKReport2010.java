package ru.tandemservice.uniec.entity.report;

import org.tandemframework.core.view.formatter.DateFormatter;

import ru.tandemservice.uni.IStorableReport;
import ru.tandemservice.uniec.entity.report.gen.EntranceExamMeetingByCKReport2010Gen;

/**
 * Протокол заседания приемной комиссии (по видам конкурса)
 */
public class EntranceExamMeetingByCKReport2010 extends EntranceExamMeetingByCKReport2010Gen implements IStorableReport
{
    public static final String P_PERIOD_TITLE = "periodTitle";

    public String getPeriodTitle()
    {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }
}