/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.settings.EnrollmentOrderVisaAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniec.entity.settings.EnrollmentOrderVisaItem;
import ru.tandemservice.unimv.IPossibleVisa;

import java.util.LinkedList;
import java.util.List;

/**
 * @author vip_delete
 * @since 28.07.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.setSettings(component.getSettings());

        getDao().prepare(model);

        prepareVisaGroupDataSource(component);
    }

    public void onClickApply(IBusinessComponent component)
    {
        getDao().update(getModel(component));

        deactivate(component);
    }

    public void prepareVisaGroupDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        if (model.getRowDataSource() != null)
            return;

        DynamicListDataSource<Row> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareVisaGroupDataSource(model);
        });

        IMergeRowIdResolver mergeRowIdResolver = entity -> {
            if (((Row) entity).getVisaItem() == null)
                return "0";

            return ((Row) entity).getVisaItem().getGroupsMemberVising().getId().toString();
        };
        dataSource.addColumn(new BlockColumn("groupsMember", "Группа участников визирования").setMergeRowIdResolver(mergeRowIdResolver));
        dataSource.addColumn(new BlockColumn("possibleVisaList", "Список виз"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteRow").setMergeRowIdResolver(mergeRowIdResolver));

        model.setRowDataSource(dataSource);
    }

    public void onClickAddRow(IBusinessComponent component)
    {
        Model model = getModel(component);
        ErrorCollector errorCollector = component.getUserContext().getErrorCollector();

        if (model.getPossibleVisaList().isEmpty() || model.getGroupsMemberVising() == null)
            errorCollector.add("Необходимо указать Группу участников визирования, Список виз и Название.", "possibleVisaList", "groupsMember");

        if (errorCollector.hasErrors())
            return;

        int i = 1;
        for (Row row : model.getRowList())
            if (row.getVisaItem() != null && row.getVisaItem().getPriority() >= i)
                i = row.getVisaItem().getPriority() + 1;

        for (IPossibleVisa possibleVisa : model.getPossibleVisaList())
        {
            EnrollmentOrderVisaItem item = new EnrollmentOrderVisaItem();
            item.setEnrollmentCampaign(model.getEnrollmentCampaign());
            item.setPossibleVisa(possibleVisa);
            item.setGroupsMemberVising(model.getGroupsMemberVising());
            item.setPriority(i++);

            Row row = new Row(Row.getNewId(model.getRowList()), item, false);

            model.getRowList().add(row);
        }

        model.getExcludeGroupsMemberVising().add(model.getGroupsMemberVising());

        model.setGroupsMemberVising(null);
        model.setPossibleVisaList(null);

        model.getRowDataSource().refresh();
    }

    public void onClickDeleteRow(IBusinessComponent component)
    {
        Model model = getModel(component);

        Row currentRow = null;
        for (Row row : model.getRowList())
        {
            if (row.getId().equals(component.getListenerParameter()))
            {
                currentRow = row;
                break;
            }
        }

        if (currentRow == null)
            return;

        if (currentRow.isFirst())
        {
            model.setGroupsMemberVising(null);
            model.setPossibleVisaList(null);
            return;
        }

        List<Row> rowList = new LinkedList<>(model.getRowList());
        for (Row row : rowList)
        {
            if (row.getVisaItem() != null && row.getVisaItem().getGroupsMemberVising().equals(currentRow.getVisaItem().getGroupsMemberVising()))
                model.getRowList().remove(row);
        }

        model.getExcludeGroupsMemberVising().remove(currentRow.getVisaItem().getGroupsMemberVising());

        model.getRowDataSource().refresh();
    }
}
