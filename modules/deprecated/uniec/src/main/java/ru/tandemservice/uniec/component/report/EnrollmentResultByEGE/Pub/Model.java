/* $Id$ */
package ru.tandemservice.uniec.component.report.EnrollmentResultByEGE.Pub;

import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;

import ru.tandemservice.uniec.entity.report.EnrollmentResultByEGEReport;

/**
 * @author Vasily Zhukov
 * @since 18.08.2011
 */
@State(keys = PublisherActivator.PUBLISHER_ID_KEY, bindings = "report.id")
public class Model
{
    private EnrollmentResultByEGEReport _report = new EnrollmentResultByEGEReport();

    public EnrollmentResultByEGEReport getReport()
    {
        return _report;
    }

    public void setReport(EnrollmentResultByEGEReport report)
    {
        _report = report;
    }
}
