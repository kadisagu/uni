// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub;

import java.util.ArrayList;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;

import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.add.ModelBase;
import ru.tandemservice.uniec.dao.ecg.IEnrollmentCompetitionGroupDAO;
import ru.tandemservice.uniec.entity.ecg.EcgDistribObject;

/**
 * @author oleyba
 * @since 31.05.2010
 */
public class CompetitionGroupDistribReservePrintFormCreator extends CompetitionGroupDistribPrintFormCreator implements IPrintFormCreator<Long>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, Long distribId)
    {
        EcgDistribObject distrib = get(EcgDistribObject.class, distribId);

        ArrayList<ModelBase.Row> rows = new ArrayList<ModelBase.Row>();
        for (IEnrollmentCompetitionGroupDAO.IEntrantRateRow entrantRow : IEnrollmentCompetitionGroupDAO.INSTANCE.get().getEntrantRateRows4Add(distrib, null))
            rows.add(new ModelBase.Row(entrantRow));

        final RtfDocument document = new RtfReader().read(template);
        buildReport(document, true, distrib, rows, " находящихся в резерве");
        return document;
    }

    @Override
    protected String getCompetitionGroupTitle()
    {
        return "Конкурсная группа";
    }
}
