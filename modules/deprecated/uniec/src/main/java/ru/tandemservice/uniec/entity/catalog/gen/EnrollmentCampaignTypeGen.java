package ru.tandemservice.uniec.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniec.entity.catalog.EnrollmentCampaignType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Тип приемной кампании
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrollmentCampaignTypeGen extends EntityBase
 implements INaturalIdentifiable<EnrollmentCampaignTypeGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.catalog.EnrollmentCampaignType";
    public static final String ENTITY_NAME = "enrollmentCampaignType";
    public static final int VERSION_HASH = -1957198185;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_TITLE = "title";
    public static final String P_UID4_FIS = "uid4Fis";

    private String _code;     // Системный код
    private String _title;     // Наименование
    private String _uid4Fis;     // Идентификатор для ФИС

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Наименование. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Наименование. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Идентификатор для ФИС.
     */
    @Length(max=255)
    public String getUid4Fis()
    {
        return _uid4Fis;
    }

    /**
     * @param uid4Fis Идентификатор для ФИС.
     */
    public void setUid4Fis(String uid4Fis)
    {
        dirty(_uid4Fis, uid4Fis);
        _uid4Fis = uid4Fis;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrollmentCampaignTypeGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EnrollmentCampaignType)another).getCode());
            }
            setTitle(((EnrollmentCampaignType)another).getTitle());
            setUid4Fis(((EnrollmentCampaignType)another).getUid4Fis());
        }
    }

    public INaturalId<EnrollmentCampaignTypeGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EnrollmentCampaignTypeGen>
    {
        private static final String PROXY_NAME = "EnrollmentCampaignTypeNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrollmentCampaignTypeGen.NaturalId) ) return false;

            EnrollmentCampaignTypeGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrollmentCampaignTypeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrollmentCampaignType.class;
        }

        public T newInstance()
        {
            return (T) new EnrollmentCampaignType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "title":
                    return obj.getTitle();
                case "uid4Fis":
                    return obj.getUid4Fis();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "uid4Fis":
                    obj.setUid4Fis((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "title":
                        return true;
                case "uid4Fis":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "title":
                    return true;
                case "uid4Fis":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "title":
                    return String.class;
                case "uid4Fis":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrollmentCampaignType> _dslPath = new Path<EnrollmentCampaignType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrollmentCampaignType");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.catalog.EnrollmentCampaignType#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Наименование. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.catalog.EnrollmentCampaignType#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Идентификатор для ФИС.
     * @see ru.tandemservice.uniec.entity.catalog.EnrollmentCampaignType#getUid4Fis()
     */
    public static PropertyPath<String> uid4Fis()
    {
        return _dslPath.uid4Fis();
    }

    public static class Path<E extends EnrollmentCampaignType> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _title;
        private PropertyPath<String> _uid4Fis;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.catalog.EnrollmentCampaignType#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EnrollmentCampaignTypeGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Наименование. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.catalog.EnrollmentCampaignType#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EnrollmentCampaignTypeGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Идентификатор для ФИС.
     * @see ru.tandemservice.uniec.entity.catalog.EnrollmentCampaignType#getUid4Fis()
     */
        public PropertyPath<String> uid4Fis()
        {
            if(_uid4Fis == null )
                _uid4Fis = new PropertyPath<String>(EnrollmentCampaignTypeGen.P_UID4_FIS, this);
            return _uid4Fis;
        }

        public Class getEntityClass()
        {
            return EnrollmentCampaignType.class;
        }

        public String getEntityName()
        {
            return "enrollmentCampaignType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
