package ru.tandemservice.uniec.entity.orders;

import ru.tandemservice.uniec.entity.orders.gen.*;

/**
 * Абстрактный проект приказа «О распределении зачисленных абитуриентов по профилям»
 */
public abstract class SplitEntrantsStuListExtract extends SplitEntrantsStuListExtractGen
{
}