/**
 *$Id$
 */
package ru.tandemservice.uniec.base.bo.EcReport.logic.EnrollmentDirProfReport;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.report.IReportRow;

import java.util.*;

/**
 * @author Alexander Shaburov
 * @since 05.07.12
 */
public class EnrollmentDirProfReportModel implements MultiEnrollmentDirectionUtil.Model
{
    public static Long ON_PROCESS_RECEPTION = 1L;
    public static Long ON_RESULT_TEST = 2L;
    public static Long ON_RESULT_ENROLLMENT = 3L;

    private MultiEnrollmentDirectionUtil.Parameters _parameters;

    // поля активности фильтров отчета
    private boolean _compensTypeActive;
    private boolean _qualificationActive;
    private boolean _studentCategoryActive;
    private boolean _formativeOrgUnitActive;
    private boolean _territorialOrgUnitActive;
    private boolean _educationLevelHighSchoolActive;
    private boolean _developFormActive;
    private boolean _developConditionActive;
    private boolean _developTechActive;
    private boolean _developPeriodActive;
    private boolean _excludeParallelActive;

    // поля выбранных элементов в фильтрах отчета
    private EnrollmentCampaign _enrollmentCampaign;
    private Date _requestFrom;
    private Date _requestTo;
    private DataWrapper _enrollmentCampaignStage;
    private CompensationType _compensationType;
    private List<StudentCategory> _studentCategoryList;
    private List<Qualifications> _qualificationList;
    private List<OrgUnit> _formativeOrgUnitList;
    private List<OrgUnit> _territorialOrgUnitList;
    private List<EducationLevelsHighSchool> _educationLevelHighSchoolList;
    private List<DevelopForm> _developFormList;
    private List<DevelopCondition> _developConditionList;
    private List<DevelopTech> _developTechList;
    private List<DevelopPeriod> _developPeriodList;
    private DataWrapper _excludeParallel;

    // модели фильтров отчета
    private ISingleSelectModel _enrollmentCampaignModel;
    private ISingleSelectModel _enrollmentCampaignStageModel;
    private ISingleSelectModel _compensationTypeModel;
    private IMultiSelectModel _studentCategoryModel;
    private IMultiSelectModel _qualificationModel;
    private IMultiSelectModel _formativeOrgUnitModel;
    private IMultiSelectModel _territorialOrgUnitModel;
    private IMultiSelectModel _educationLevelHighSchoolModel;
    private IMultiSelectModel _developFormModel;
    private IMultiSelectModel _developConditionModel;
    private IMultiSelectModel _developTechModel;
    private IMultiSelectModel _developPeriodModel;
    private ISelectModel _excludeParallelModel;

    //поля для печати
    boolean _detalizationProfile = false;
    int priorityColumnNumber = 0;
    private Map<EnrollmentDirection, List<ProfileEducationOrgUnit>> _directionProfileListMap = new HashMap<EnrollmentDirection, List<ProfileEducationOrgUnit>>();
    private Map<EducationLevels, List<EnrollmentDirection>> _educationLevelsDirectionListMap = new TreeMap<EducationLevels, List<EnrollmentDirection>>(new Comparator<EducationLevels>()
    {
        @Override
        public int compare(EducationLevels o1, EducationLevels o2)
        {
            int result = o1.getLevelType().getTitle().compareTo(o2.getLevelType().getTitle());
            if (result == 0)
                result = o1.getDisplayableTitle().compareTo(o2.getDisplayableTitle());
            if (result == 0)
                result = o1.getId().equals(o2.getId()) ? 0 : o1.getId() > o2.getId() ? 1 : -1;
            return result;
        }
    });
    private Map<EnrollmentDirection, List<RequestedEnrollmentDirection>> _directionRequestedDirectionListMap = new HashMap<EnrollmentDirection, List<RequestedEnrollmentDirection>>();
    private Map<ProfileEducationOrgUnit, List<PriorityProfileEduOu>> _profilePriorityListMap = new HashMap<ProfileEducationOrgUnit, List<PriorityProfileEduOu>>();
    private EntrantDataUtil _dataUtil;

    /**
     * Класс строки таблицы детализации Профилей.
     * Используется для сортировки.
     */
    public static class DirectionWrapper implements IReportRow
    {
        private RequestedEnrollmentDirection _requestedEnrollmentDirection;
        private int _indProgressMark;
        private Double _finalMark;
        private Double _profileMark;
        private Double averageEduInstMark;

        public DirectionWrapper(RequestedEnrollmentDirection requestedEnrollmentDirection, EntrantDataUtil entrantDataUtil)
        {
            _requestedEnrollmentDirection = requestedEnrollmentDirection;
            _indProgressMark = entrantDataUtil.getIndividualProgressMarkByRequest(requestedEnrollmentDirection.getEntrantRequest().getId());
            _finalMark = entrantDataUtil.getFinalMark(requestedEnrollmentDirection) + _indProgressMark;
            _profileMark = requestedEnrollmentDirection.getProfileChosenEntranceDiscipline() == null ? null : requestedEnrollmentDirection.getProfileChosenEntranceDiscipline().getFinalMark();
            averageEduInstMark = entrantDataUtil.getAverageEduInstMark(requestedEnrollmentDirection.getEntrantRequest().getEntrant().getId());
        }

        @Override
        public RequestedEnrollmentDirection getRequestedEnrollmentDirection()
        {
            return _requestedEnrollmentDirection;
        }

        @Override
        public Double getFinalMark()
        {
            return _finalMark;
        }

        @Override
        public Double getProfileMark()
        {
            return _profileMark;
        }

        @Override
        public String getFio()
        {
            return _requestedEnrollmentDirection.getEntrantRequest().getEntrant().getPerson().getFullFio();
        }

        @Override
        public Boolean isGraduatedProfileEduInstitution()
        {
            return Boolean.FALSE; // мы не грузим эти данные, так что по ним не сравниваем
        }

        @Override
        public Double getCertificateAverageMark()
        {
            return averageEduInstMark;
        }

        public int getIndProgressMark()
        {
            return _indProgressMark;
        }
    }

    // Getters & Setters

    public MultiEnrollmentDirectionUtil.Parameters getParameters()
    {
        return _parameters;
    }

    @Override
    public List<EnrollmentCampaign> getSelectedEnrollmentCampaignList()
    {
        return MultiEnrollmentDirectionUtil.getSingletonList(_enrollmentCampaign);
    }

    @Override
    public List<OrgUnit> getSelectedFormativeOrgUnitList()
    {
        return isFormativeOrgUnitActive() ? getFormativeOrgUnitList() : null;
    }

    @Override
    public List<OrgUnit> getSelectedTerritorialOrgUnitList()
    {
        return isTerritorialOrgUnitActive() ? getTerritorialOrgUnitList() : null;
    }

    @Override
    public List<EducationLevelsHighSchool> getSelectedEducationLevelHighSchoolList()
    {
        return isEducationLevelHighSchoolActive() ? getEducationLevelHighSchoolList() : null;
    }

    @Override
    public List<DevelopForm> getSelectedDevelopFormList()
    {
        return isDevelopFormActive() ? getDevelopFormList() : null;
    }

    @Override
    public List<DevelopCondition> getSelectedDevelopConditionList()
    {
        return isDevelopConditionActive() ? getDevelopConditionList() : null;
    }

    @Override
    public List<DevelopTech> getSelectedDevelopTechList()
    {
        return isDevelopTechActive() ? getDevelopTechList() : null;
    }

    @Override
    public List<DevelopPeriod> getSelectedDevelopPeriodList()
    {
        return isDevelopPeriodActive() ? getDevelopPeriodList() : null;
    }

    public void setParameters(MultiEnrollmentDirectionUtil.Parameters parameters)
    {
        _parameters = parameters;
    }

    public int getPriorityColumnNumber()
    {
        return priorityColumnNumber;
    }

    public void setPriorityColumnNumber(int priorityColumnNumber)
    {
        this.priorityColumnNumber = priorityColumnNumber;
    }

    public Map<EnrollmentDirection, List<ProfileEducationOrgUnit>> getDirectionProfileListMap()
    {
        return _directionProfileListMap;
    }

    public void setDirectionProfileListMap(Map<EnrollmentDirection, List<ProfileEducationOrgUnit>> directionProfileListMap)
    {
        _directionProfileListMap = directionProfileListMap;
    }

    public Map<EducationLevels, List<EnrollmentDirection>> getEducationLevelsDirectionListMap()
    {
        return _educationLevelsDirectionListMap;
    }

    public void setEducationLevelsDirectionListMap(Map<EducationLevels, List<EnrollmentDirection>> educationLevelsDirectionListMap)
    {
        _educationLevelsDirectionListMap = educationLevelsDirectionListMap;
    }

    public Map<EnrollmentDirection, List<RequestedEnrollmentDirection>> getDirectionRequestedDirectionListMap()
    {
        return _directionRequestedDirectionListMap;
    }

    public void setDirectionRequestedDirectionListMap(Map<EnrollmentDirection, List<RequestedEnrollmentDirection>> directionRequestedDirectionListMap)
    {
        _directionRequestedDirectionListMap = directionRequestedDirectionListMap;
    }

    public Map<ProfileEducationOrgUnit, List<PriorityProfileEduOu>> getProfilePriorityListMap()
    {
        return _profilePriorityListMap;
    }

    public void setProfilePriorityListMap(Map<ProfileEducationOrgUnit, List<PriorityProfileEduOu>> profilePriorityListMap)
    {
        _profilePriorityListMap = profilePriorityListMap;
    }

    public boolean isCompensTypeActive()
    {
        return _compensTypeActive;
    }

    public void setCompensTypeActive(boolean compensTypeActive)
    {
        _compensTypeActive = compensTypeActive;
    }

    public boolean isQualificationActive()
    {
        return _qualificationActive;
    }

    public void setQualificationActive(boolean qualificationActive)
    {
        _qualificationActive = qualificationActive;
    }

    public boolean isStudentCategoryActive()
    {
        return _studentCategoryActive;
    }

    public void setStudentCategoryActive(boolean studentCategoryActive)
    {
        _studentCategoryActive = studentCategoryActive;
    }

    public boolean isFormativeOrgUnitActive()
    {
        return _formativeOrgUnitActive;
    }

    public void setFormativeOrgUnitActive(boolean formativeOrgUnitActive)
    {
        _formativeOrgUnitActive = formativeOrgUnitActive;
    }

    public boolean isTerritorialOrgUnitActive()
    {
        return _territorialOrgUnitActive;
    }

    public void setTerritorialOrgUnitActive(boolean territorialOrgUnitActive)
    {
        _territorialOrgUnitActive = territorialOrgUnitActive;
    }

    public boolean isEducationLevelHighSchoolActive()
    {
        return _educationLevelHighSchoolActive;
    }

    public void setEducationLevelHighSchoolActive(boolean educationLevelHighSchoolActive)
    {
        _educationLevelHighSchoolActive = educationLevelHighSchoolActive;
    }

    public boolean isDevelopFormActive()
    {
        return _developFormActive;
    }

    public void setDevelopFormActive(boolean developFormActive)
    {
        _developFormActive = developFormActive;
    }

    public boolean isDevelopConditionActive()
    {
        return _developConditionActive;
    }

    public void setDevelopConditionActive(boolean developConditionActive)
    {
        _developConditionActive = developConditionActive;
    }

    public boolean isDevelopTechActive()
    {
        return _developTechActive;
    }

    public void setDevelopTechActive(boolean developTechActive)
    {
        _developTechActive = developTechActive;
    }

    public boolean isDevelopPeriodActive()
    {
        return _developPeriodActive;
    }

    public void setDevelopPeriodActive(boolean developPeriodActive)
    {
        _developPeriodActive = developPeriodActive;
    }

    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public Date getRequestFrom()
    {
        return _requestFrom;
    }

    public void setRequestFrom(Date requestFrom)
    {
        _requestFrom = requestFrom;
    }

    public Date getRequestTo()
    {
        return _requestTo;
    }

    public void setRequestTo(Date requestTo)
    {
        _requestTo = requestTo;
    }

    public DataWrapper getEnrollmentCampaignStage()
    {
        return _enrollmentCampaignStage;
    }

    public void setEnrollmentCampaignStage(DataWrapper enrollmentCampaignStage)
    {
        _enrollmentCampaignStage = enrollmentCampaignStage;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

    public List<StudentCategory> getStudentCategoryList()
    {
        return _studentCategoryList;
    }

    public void setStudentCategoryList(List<StudentCategory> studentCategoryList)
    {
        _studentCategoryList = studentCategoryList;
    }

    public List<Qualifications> getQualificationList()
    {
        return _qualificationList;
    }

    public void setQualificationList(List<Qualifications> qualificationList)
    {
        _qualificationList = qualificationList;
    }

    public List<OrgUnit> getFormativeOrgUnitList()
    {
        return _formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(List<OrgUnit> formativeOrgUnitList)
    {
        _formativeOrgUnitList = formativeOrgUnitList;
    }

    public List<OrgUnit> getTerritorialOrgUnitList()
    {
        return _territorialOrgUnitList;
    }

    public void setTerritorialOrgUnitList(List<OrgUnit> territorialOrgUnitList)
    {
        _territorialOrgUnitList = territorialOrgUnitList;
    }

    public List<EducationLevelsHighSchool> getEducationLevelHighSchoolList()
    {
        return _educationLevelHighSchoolList;
    }

    public void setEducationLevelHighSchoolList(List<EducationLevelsHighSchool> educationLevelHighSchoolList)
    {
        _educationLevelHighSchoolList = educationLevelHighSchoolList;
    }

    public List<DevelopForm> getDevelopFormList()
    {
        return _developFormList;
    }

    public void setDevelopFormList(List<DevelopForm> developFormList)
    {
        _developFormList = developFormList;
    }

    public List<DevelopCondition> getDevelopConditionList()
    {
        return _developConditionList;
    }

    public void setDevelopConditionList(List<DevelopCondition> developConditionList)
    {
        _developConditionList = developConditionList;
    }

    public List<DevelopTech> getDevelopTechList()
    {
        return _developTechList;
    }

    public void setDevelopTechList(List<DevelopTech> developTechList)
    {
        _developTechList = developTechList;
    }

    public List<DevelopPeriod> getDevelopPeriodList()
    {
        return _developPeriodList;
    }

    public void setDevelopPeriodList(List<DevelopPeriod> developPeriodList)
    {
        _developPeriodList = developPeriodList;
    }

    public boolean isExcludeParallelActive()
    {
        return _excludeParallelActive;
    }

    public void setExcludeParallelActive(boolean excludeParallelActive)
    {
        _excludeParallelActive = excludeParallelActive;
    }

    public DataWrapper getExcludeParallel()
    {
        return _excludeParallel;
    }

    public void setExcludeParallel(DataWrapper excludeParallel)
    {
        _excludeParallel = excludeParallel;
    }

    public ISingleSelectModel getEnrollmentCampaignModel()
    {
        return _enrollmentCampaignModel;
    }

    public void setEnrollmentCampaignModel(ISingleSelectModel enrollmentCampaignModel)
    {
        _enrollmentCampaignModel = enrollmentCampaignModel;
    }

    public ISingleSelectModel getEnrollmentCampaignStageModel()
    {
        return _enrollmentCampaignStageModel;
    }

    public void setEnrollmentCampaignStageModel(ISingleSelectModel enrollmentCampaignStageModel)
    {
        _enrollmentCampaignStageModel = enrollmentCampaignStageModel;
    }

    public ISingleSelectModel getCompensationTypeModel()
    {
        return _compensationTypeModel;
    }

    public void setCompensationTypeModel(ISingleSelectModel compensationTypeModel)
    {
        _compensationTypeModel = compensationTypeModel;
    }

    public IMultiSelectModel getStudentCategoryModel()
    {
        return _studentCategoryModel;
    }

    public void setStudentCategoryModel(IMultiSelectModel studentCategoryModel)
    {
        _studentCategoryModel = studentCategoryModel;
    }

    public IMultiSelectModel getQualificationModel()
    {
        return _qualificationModel;
    }

    public void setQualificationModel(IMultiSelectModel qualificationModel)
    {
        _qualificationModel = qualificationModel;
    }

    public IMultiSelectModel getFormativeOrgUnitModel()
    {
        return _formativeOrgUnitModel;
    }

    public void setFormativeOrgUnitModel(IMultiSelectModel formativeOrgUnitModel)
    {
        _formativeOrgUnitModel = formativeOrgUnitModel;
    }

    public IMultiSelectModel getTerritorialOrgUnitModel()
    {
        return _territorialOrgUnitModel;
    }

    public void setTerritorialOrgUnitModel(IMultiSelectModel territorialOrgUnitModel)
    {
        _territorialOrgUnitModel = territorialOrgUnitModel;
    }

    public IMultiSelectModel getEducationLevelHighSchoolModel()
    {
        return _educationLevelHighSchoolModel;
    }

    public void setEducationLevelHighSchoolModel(IMultiSelectModel educationLevelHighSchoolModel)
    {
        _educationLevelHighSchoolModel = educationLevelHighSchoolModel;
    }

    public IMultiSelectModel getDevelopFormModel()
    {
        return _developFormModel;
    }

    public void setDevelopFormModel(IMultiSelectModel developFormModel)
    {
        _developFormModel = developFormModel;
    }

    public IMultiSelectModel getDevelopConditionModel()
    {
        return _developConditionModel;
    }

    public void setDevelopConditionModel(IMultiSelectModel developConditionModel)
    {
        _developConditionModel = developConditionModel;
    }

    public IMultiSelectModel getDevelopTechModel()
    {
        return _developTechModel;
    }

    public void setDevelopTechModel(IMultiSelectModel developTechModel)
    {
        _developTechModel = developTechModel;
    }

    public IMultiSelectModel getDevelopPeriodModel()
    {
        return _developPeriodModel;
    }

    public void setDevelopPeriodModel(IMultiSelectModel developPeriodModel)
    {
        _developPeriodModel = developPeriodModel;
    }

    public ISelectModel getExcludeParallelModel()
    {
        return _excludeParallelModel;
    }

    public void setExcludeParallelModel(ISelectModel excludeParallelModel)
    {
        _excludeParallelModel = excludeParallelModel;
    }

    public EntrantDataUtil getDataUtil()
    {
        return _dataUtil;
    }

    public void setDataUtil(EntrantDataUtil dataUtil)
    {
        _dataUtil = dataUtil;
    }

    public boolean isDetalizationProfile()
    {
        return _detalizationProfile;
    }

    public void setDetalizationProfile(boolean detalizationProfile)
    {
        _detalizationProfile = detalizationProfile;
    }
}
