/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.menu.ExamSetPub;

import java.util.List;

import org.tandemframework.core.exception.ApplicationException;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline;
import ru.tandemservice.uniec.entity.examset.ExamSet;
import ru.tandemservice.uniec.util.ExamSetUtil;

/**
 * @author Боба
 * @since 19.08.2008
 */
public class DAO extends UniDao<ExamSetPubModel> implements IDAO
{
    @Override
    public void prepare(ExamSetPubModel model)
    {
        ExamSet examSet = ExamSetUtil.getExamSet(model.getExamSetId());

        if (examSet == null)
            throw new ApplicationException("Выбранный набор уже не существует.");

        model.setExamSet(examSet);
    }

    @Override
    public void deleteEntranceDiscipline(ExamSetPubModel model, Long examSetItemIndex)
    {
        ExamSet examSet = ExamSetUtil.getExamSet(model.getExamSetId());
        if (examSet == null)
            throw new ApplicationException("Выбранный набор уже не существует.");

        int index = ExamSetUtil.getExamSetItemIndex(examSet, examSetItemIndex);
        List<EntranceDiscipline> entranceDisciplineList = model.getExamSet().getSetItemList().get(index).getList();

        for (EntranceDiscipline entranceDiscipline : entranceDisciplineList)
            delete(entranceDiscipline);

        model.getExamSet().getSetItemList().remove(index);

        String examSetId = new ExamSet(0L, model.getExamSet().getEnrollmentCampaign(), model.getExamSet().getStudentCategory(), model.getExamSet().getSetItemList()).getExamSetId();
        model.setExamSetId(examSetId);
        model.setExamSet(ExamSetUtil.getExamSet(examSetId));
    }
}
