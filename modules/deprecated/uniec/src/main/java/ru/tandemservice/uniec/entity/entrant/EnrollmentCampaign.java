package ru.tandemservice.uniec.entity.entrant;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.entrant.gen.EnrollmentCampaignGen;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class EnrollmentCampaign extends EnrollmentCampaignGen implements ITitled
{
    public static final Comparator<EnrollmentCampaign> COMPARATOR = new Comparator<EnrollmentCampaign>() {
        @Override public int compare(EnrollmentCampaign o1, EnrollmentCampaign o2) {
            int result = o1.getEducationYear().getIntValue() - o2.getEducationYear().getIntValue();
            if (0 != result) { return result; }
            return o1.getTitle().compareTo(o2.getTitle());
        }
    };


    public static final String P_PERIOD_LIST = "periodList";
    public static final String P_PERIOD_LIST_TITLE = "periodListTitle";



    public Date getStartDate()
    {
        List<EnrollmentCampaignPeriod> periods = getPeriodList();
        if (periods.isEmpty()) return null;
        return periods.get(0).getDateFrom();
    }

    public List<EnrollmentCampaignPeriod> getPeriodList()
    {
        return UniDaoFacade.getCoreDao().getList(EnrollmentCampaignPeriod.class, EnrollmentCampaignPeriod.L_ENROLLMENT_CAMPAIGN, this, EnrollmentCampaignPeriod.P_DATE_FROM);
    }

    @SuppressWarnings("unchecked")
    public String getPeriodListTitle()
    {
        return CollectionFormatter.COLLECTION_FORMATTER.format(getPeriodList());
    }

    public boolean isExamListsFormingForEntrant()
    {
        return getEntrantExamListsFormingFeature().getCode().equals(UniecDefines.EXAMLIST_FORMING_FEATURE_FOR_ENTRANT);
    }
}