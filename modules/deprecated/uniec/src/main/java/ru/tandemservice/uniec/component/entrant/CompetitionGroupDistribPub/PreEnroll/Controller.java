package ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.PreEnroll;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.AbstractListDataSource;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.gen.EducationOrgUnitGen;
import ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.PreEnroll.Model.Row;
import ru.tandemservice.uniec.entity.entrant.gen.EnrollmentDirectionGen;
import ru.tandemservice.uniec.entity.entrant.gen.RequestedEnrollmentDirectionGen;

/**
 * @author vdanilov
 */
public class Controller extends AbstractBusinessController<IDAO, Model> {

	@Override
	@SuppressWarnings("unchecked")
	public void onRefreshComponent(final IBusinessComponent component) {
		final Model model = this.getModel(component);
		final IDAO dao = this.getDao();
		dao.prepare(model);
		dao.prepareStudentDataSource(model);

		final AbstractListDataSource<Row> studentDataSource = model.getStudentDataSource();
		studentDataSource.getColumns().clear();

		studentDataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", "direction."+RequestedEnrollmentDirectionGen.L_ENROLLMENT_DIRECTION+"."+EnrollmentDirectionGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnitGen.L_EDUCATION_LEVEL_HIGH_SCHOOL+"."+EducationLevelsHighSchool.P_DISPLAYABLE_TITLE).setWidth(20).setOrderable(false).setClickable(false).setMergeRows(true)
                                            .setMergeRowIdResolver(entity -> String.valueOf(((Row) entity).getDirection().getEnrollmentDirection().getId())));
		studentDataSource.addColumn(new SimpleColumn("Абитуриент", "direction.entrantRequest.entrant.person.fullFio").setOrderable(false).setClickable(false));

		studentDataSource.addColumn(new SimpleColumn("", "student.educationOrgUnit.educationLevelHighSchool.shortTitle").setWidth(1).setOrderable(false).setClickable(false));
		studentDataSource.addColumn(new BlockColumn("selector", "Тип приказа").setWidth(1).setOrderable(false).setClickable(false));



	}

	public void onClickPreEnrollStudents(final IBusinessComponent component) {
		final Model model = this.getModel(component);
		final IDAO dao = this.getDao();
		dao.doPreEnroll(model, component.getUserContext().getErrorCollector());
		dao.prepare(model);
		dao.prepareStudentDataSource(model);
	}


	public void onClickDelete(final IBusinessComponent component) {
		final Model model = this.getModel(component);
		final IDAO dao = this.getDao();
		dao.delete(model, (Long)component.getListenerParameter());
		dao.prepare(model);
		dao.prepareStudentDataSource(model);

	}
}
