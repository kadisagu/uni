/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec.base.bo.EcProfileDistribution.ui.EntrantAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniec.base.bo.EcDistribution.logic.EcSimpleDSHandler;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgEntrantRateDirectionDTO;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgEntrantRateRowDTO;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgPlanChoiceResult;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgQuotaManager;
import ru.tandemservice.uniec.base.bo.EcProfileDistribution.EcProfileDistributionManager;
import ru.tandemservice.uniec.base.bo.EcProfileDistribution.util.EcgpQuotaManager;
import ru.tandemservice.uniec.base.bo.EcProfileDistribution.util.IEcgpQuotaFreeDTO;
import ru.tandemservice.uniec.base.entity.ecgp.EcgpDistribution;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.settings.NotUsedTargetAdmissionKind;

import java.util.*;

/**
 * @author Vasily Zhukov
 * @since 10.05.2012
 */
@Input({
        @Bind(key = EcProfileDistributionEntrantAddUI.DISTRIBUTION, binding = EcProfileDistributionEntrantAddUI.DISTRIBUTION),
        @Bind(key = EcProfileDistributionEntrantAddUI.RATE_RULE, binding = EcProfileDistributionEntrantAddUI.RATE_RULE)
})
public class EcProfileDistributionEntrantAddUI extends UIPresenter
{
    public static final String DISTRIBUTION = "distribution";
    public static final String RATE_RULE = "rateRule";

    private EcgpDistribution _distribution;
    private IEcgEntrantRateRowDTO.Rule _rateRule;
    private boolean _targetAdmission;                                      // true, если добавляем целевиков
    private String _title;                                                 // название страницы

    private int _minCount;                                                 // минимальное кол-во строк рейтинга, которые нужно показать на странице
    private boolean _hasMore;                                              // true, если показаны не все строки рейтинга
    private Map<Long, IEcgEntrantRateDirectionDTO> _chosenMap;             // preStudentId -> профиль
    private Map<Long, List<IEcgEntrantRateDirectionDTO>> _chosen2listMap;  // preStudentId -> список возможных профилей
    private List<? extends IEcgEntrantRateRowDTO> _viewRateRowList;        // строки рейтинга, которые нужно отобразить

    private boolean _hasDefaultChecked;
    private Map<Long, Long> _chosenDirectionMap;                           // preStudentId -> выбранный профиль (на момент POST)

    private List<? extends IEcgEntrantRateRowDTO> _rateList;               // список строк рейтинга
    private List<IEntity> _selectedRateRowList = new ArrayList<>();       // выбранные строки рейтинга

    @Override
    public void onComponentRefresh()
    {
        // для скорости запомним не изменяемые параметры

        _targetAdmission = _rateRule.equals(IEcgEntrantRateRowDTO.Rule.RULE_TARGET_ADMISSION);

        switch (_rateRule)
        {
            case RULE_TARGET_ADMISSION:
                _title = "Добавление абитуриентов (поступающих по целевому приему) в распределение";
                break;
            case RULE_NON_COMPETITIVE:
                _title = "Добавление абитуриентов (поступающих без ВИ и вне конкурса) в распределение";
                break;
            case RULE_COMPETITIVE:
                _title = "Добавление абитуриентов (поступающих на общих основаниях) в распределение";
                break;
            default:
                throw new IllegalArgumentException("Unknown rateRule: " + _rateRule);
        }

        // пока не знаем сколько строк следует отобразить
        _minCount = -1;

        // по умолчанию ничего не выбрано
        _hasDefaultChecked = false;

        // получаем используемые виды цп
        List<TargetAdmissionKind> targetAdmissionKindList = DataAccessServices.dao().getList(new DQLSelectBuilder().fromEntity(TargetAdmissionKind.class, "e")
                .where(DQLExpressions.notIn(DQLExpressions.property(TargetAdmissionKind.id().fromAlias("e")), new DQLSelectBuilder().fromEntity(NotUsedTargetAdmissionKind.class, "nu")
                        .column(DQLExpressions.property(NotUsedTargetAdmissionKind.targetAdmissionKind().id().fromAlias("nu")))
                        .where(DQLExpressions.eq(DQLExpressions.property(NotUsedTargetAdmissionKind.enrollmentCampaign().fromAlias("nu")), DQLExpressions.value(_distribution.getConfig().getEcgItem().getEnrollmentCampaign())))
                        .buildQuery()
                )));

        Map<CompetitionKind, Integer> competitionKindPriorityMap = UniecDAOFacade.getSettingsDAO().getCompetitionKindPriorities(_distribution.getConfig().getEcgItem().getEnrollmentCampaign());

        // получаем список строк рейтинга
        _rateList = EcProfileDistributionManager.instance().dao().getEntrantRateRowList(_distribution, targetAdmissionKindList, competitionKindPriorityMap, _rateRule);

        // обновляем рейтинг абитуриентов
        doRefreshEntrantRateList();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (EcProfileDistributionEntrantAdd.PRE_STUDENT_DS.equals(dataSource.getName()))
        {
            dataSource.put(EcSimpleDSHandler.LIST, _viewRateRowList);

            ((CheckboxColumn) ((PageableSearchListDataSource) dataSource).getLegacyDataSource().getColumn("checkbox")).setSelectedObjects(_selectedRateRowList);
        }
    }

    private void doRefreshEntrantRateList()
    {
        // свободные места в распределении
        IEcgpQuotaFreeDTO freeQuotaDTO = EcProfileDistributionManager.instance().dao().getFreeQuotaDTO(_distribution);

        // получаем алгоритм заполнения планов приема
        IEcgQuotaManager quotaManager = new EcgpQuotaManager(freeQuotaDTO);

        // каждому абитуриенту выберем подходящее направление приема
        // мап: абитуриент -> выбранное направление приема
        _chosenMap = new HashMap<>();

        // для распределения по конкурсным группам каждому выделенному абитуриенту сохраним
        // список возможных для выбора направлений
        _chosen2listMap = new HashMap<>();

        // общее число строк
        int len = _rateList.size();

        int i = 0;

        _selectedRateRowList.clear();

        if (_hasDefaultChecked)
        {
            Set<Long> chosenEntrantSet = new HashSet<>();
            for (IEntity entity : ((CheckboxColumn) ((PageableSearchListDataSource) _uiConfig.getDataSource(EcProfileDistributionEntrantAdd.PRE_STUDENT_DS)).getLegacyDataSource().getColumn("checkbox")).getSelectedObjects())
                chosenEntrantSet.add(entity.getId());

            // число выбранных абитуриентов
            int checkedCount = chosenEntrantSet.size();

            // если есть выбранные по умолчанию абитуриенты и направления, то только их и пытаемся выбрать в рейтинге
            while (i < len && checkedCount > 0)
            {
                IEcgEntrantRateRowDTO rateRowDTO = _rateList.get(i);

                Long entrantId = rateRowDTO.getId();

                if (chosenEntrantSet.contains(entrantId))
                {
                    checkedCount--;
                    IEcgPlanChoiceResult choiceResult = quotaManager.getPossibleDirectionList(rateRowDTO, _chosenDirectionMap.get(entrantId));

                    IEcgEntrantRateDirectionDTO chosenDirection = choiceResult.getChosenDirection();

                    if (chosenDirection != null)
                    {
                        _chosenMap.put(entrantId, chosenDirection);
                        _chosen2listMap.put(entrantId, choiceResult.getPossibleDirectionList());
                        _selectedRateRowList.add(rateRowDTO);
                    }
                }

                i++;
            }
        } else
        {
            // общее число свободных мест
            int freeTotal = quotaManager.getFreeTotal();

            while (i < len && freeTotal > 0)
            {
                IEcgEntrantRateRowDTO rateRowDTO = _rateList.get(i);

                IEcgPlanChoiceResult choiceResult = quotaManager.getPossibleDirectionList(rateRowDTO, null);

                IEcgEntrantRateDirectionDTO chosenDirection = choiceResult.getChosenDirection();

                if (chosenDirection != null)
                {
                    freeTotal--;
                    _chosenMap.put(rateRowDTO.getId(), chosenDirection);
                    _chosen2listMap.put(rateRowDTO.getId(), choiceResult.getPossibleDirectionList());
                    _selectedRateRowList.add(rateRowDTO);
                }

                i++;
            }
        }

        int count = i <= _minCount ? _minCount : i + 25;

        _hasMore = count < _rateList.size();

        _viewRateRowList = _hasMore ? _rateList.subList(0, count) : _rateList;

        _chosenDirectionMap = new HashMap<>();
    }

    public boolean isChecked()
    {
        Long entrantId = ((IEcgEntrantRateRowDTO) getConfig().getDataSource(EcProfileDistributionEntrantAdd.PRE_STUDENT_DS).getCurrent()).getId();
        return _chosenMap.containsKey(entrantId);
    }

    // select

    public String getSelectId()
    {
        Long preStudentId = ((IEcgEntrantRateRowDTO) getConfig().getDataSource(EcProfileDistributionEntrantAdd.PRE_STUDENT_DS).getCurrent()).getId();
        return "s_" + preStudentId;
    }

    public IEcgEntrantRateDirectionDTO getDirection()
    {
        Long preStudentId = ((IEcgEntrantRateRowDTO) getConfig().getDataSource(EcProfileDistributionEntrantAdd.PRE_STUDENT_DS).getCurrent()).getId();
        return _chosenMap.get(preStudentId);
    }

    public void setDirection(IEcgEntrantRateDirectionDTO direction)
    {
        Long preStudentId = ((IEcgEntrantRateRowDTO) getConfig().getDataSource(EcProfileDistributionEntrantAdd.PRE_STUDENT_DS).getCurrent()).getId();
        _chosenDirectionMap.put(preStudentId, direction.getId());
    }

    public List<IEcgEntrantRateDirectionDTO> getDirectionList()
    {
        Long preStudentId = ((IEcgEntrantRateRowDTO) getConfig().getDataSource(EcProfileDistributionEntrantAdd.PRE_STUDENT_DS).getCurrent()).getId();
        return _chosen2listMap.get(preStudentId);
    }

    // Getters & Setters

    public EcgpDistribution getDistribution()
    {
        return _distribution;
    }

    public void setDistribution(EcgpDistribution distribution)
    {
        _distribution = distribution;
    }

    public IEcgEntrantRateRowDTO.Rule getRateRule()
    {
        return _rateRule;
    }

    public void setRateRule(IEcgEntrantRateRowDTO.Rule rateRule)
    {
        _rateRule = rateRule;
    }

    public boolean isTargetAdmission()
    {
        return _targetAdmission;
    }

    public void setTargetAdmission(boolean targetAdmission)
    {
        _targetAdmission = targetAdmission;
    }

    public String getTitle()
    {
        return _title;
    }

    public void setTitle(String title)
    {
        _title = title;
    }

    public int getMinCount()
    {
        return _minCount;
    }

    public void setMinCount(int minCount)
    {
        _minCount = minCount;
    }

    public boolean isHasMore()
    {
        return _hasMore;
    }

    public void setHasMore(boolean hasMore)
    {
        _hasMore = hasMore;
    }

    public Map<Long, IEcgEntrantRateDirectionDTO> getChosenMap()
    {
        return _chosenMap;
    }

    public void setChosenMap(Map<Long, IEcgEntrantRateDirectionDTO> chosenMap)
    {
        _chosenMap = chosenMap;
    }

    public Map<Long, List<IEcgEntrantRateDirectionDTO>> getChosen2listMap()
    {
        return _chosen2listMap;
    }

    public void setChosen2listMap(Map<Long, List<IEcgEntrantRateDirectionDTO>> chosen2listMap)
    {
        _chosen2listMap = chosen2listMap;
    }

    public List<? extends IEcgEntrantRateRowDTO> getViewRateRowList()
    {
        return _viewRateRowList;
    }

    public void setViewRateRowList(List<? extends IEcgEntrantRateRowDTO> viewRateRowList)
    {
        _viewRateRowList = viewRateRowList;
    }

    public boolean isHasDefaultChecked()
    {
        return _hasDefaultChecked;
    }

    public void setHasDefaultChecked(boolean hasDefaultChecked)
    {
        _hasDefaultChecked = hasDefaultChecked;
    }

    public Map<Long, Long> getChosenDirectionMap()
    {
        return _chosenDirectionMap;
    }

    public void setChosenDirectionMap(Map<Long, Long> chosenDirectionMap)
    {
        _chosenDirectionMap = chosenDirectionMap;
    }

    public List<? extends IEcgEntrantRateRowDTO> getRateList()
    {
        return _rateList;
    }

    public void setRateList(List<? extends IEcgEntrantRateRowDTO> rateList)
    {
        _rateList = rateList;
    }

    public List<IEntity> getSelectedRateRowList()
    {
        return _selectedRateRowList;
    }

    public void setSelectedRateRowList(List<IEntity> selectedRateRowList)
    {
        _selectedRateRowList = selectedRateRowList;
    }

    // Listeners

    public void onAddMore()
    {
        // показать на 25 строк больше, чем показывается сейчас
        _minCount = getConfig().getDataSource(EcProfileDistributionEntrantAdd.PRE_STUDENT_DS).getTotalSize() + 25;

        doRefreshEntrantRateList();
    }

    public void onRateRefresh()
    {
        // произошел выбор чекбокса или выбор из селекта
        // пока мы сидели на форме мог измениться план приема или еще что-нибудь влияющее на рейтинг
        // нужно пересчитать рейтинг, потом принудительно выделить тех абитуриентов и те направления,
        // которые пришли в POST'е, уменьшая при этом план, если нельзя выделить абитуриента или направление,
        // то пропускаем такого абитуриента, после чего пробежаться по всем не выделенным абитуриентам расставляя
        // им остатки по планам

        _hasDefaultChecked = true;

        // обновляем рейтинг
        doRefreshEntrantRateList();
    }

    public void onClickSave()
    {
        // сохраняем рекомендованных абитуриентов
        // пока мы сидели на форме, теоретически могло что угодно измениться: план, суммы баллов, наборы экзаменов
        // но мы все равно сохраняем ровно то, что выбрали на форме
        // превышение планов тут не проверяется, оно покажется уже в публикаторе распределения после сохранения

        EcProfileDistributionManager.instance().dao().saveEntrantRecommendedList(_distribution, _chosenDirectionMap.values());

        deactivate();
    }
}
