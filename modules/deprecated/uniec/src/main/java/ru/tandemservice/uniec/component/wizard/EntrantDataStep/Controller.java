/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.wizard.EntrantDataStep;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.uniec.component.wizard.IEntrantWizardComponents;

/**
 * @author vip_delete
 * @since 16.06.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    // Wizard Actions

    public void onClickNext(IBusinessComponent component)
    {
        Model model = getModel(component);
        
        getDao().update(model);

        deactivate(component);

        component.getParentRegion().getOwner().createDefaultChildRegion(new ComponentActivator(IEntrantWizardComponents.STATE_EXAM_DATA_STEP, new ParametersMap()
                .add("entrantId", model.getEntrant().getId())
                .add("onlineEntrantId", model.getOnlineEntrant().getId())
                .add("entrantMasterPermKey", model.getEntrantMasterPermKey())
        ));
    }

    public void onClickSkip(IBusinessComponent component)
    {
        Model model = getModel(component);
        
        deactivate(component);

        component.getParentRegion().getOwner().createDefaultChildRegion(new ComponentActivator(IEntrantWizardComponents.STATE_EXAM_DATA_STEP, new ParametersMap()
                .add("entrantId", model.getEntrant().getId())
                .add("onlineEntrantId", model.getOnlineEntrant().getId())
                .add("entrantMasterPermKey", model.getEntrantMasterPermKey())
        ));
    }

    public void onClickStop(IBusinessComponent component)
    {
        getDao().update(getModel(component));

        deactivate(component);
    }
}
