package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uniec.entity.catalog.EnrollmentCampaignType;
import ru.tandemservice.uniec.entity.catalog.EntrantExamListsFormingFeature;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Приемная кампания
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrollmentCampaignGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign";
    public static final String ENTITY_NAME = "enrollmentCampaign";
    public static final int VERSION_HASH = 1856772348;
    private static IEntityMeta ENTITY_META;

    public static final String P_TITLE = "title";
    public static final String P_ENROLLMENT_PER_COMP_TYPE_DIFF = "enrollmentPerCompTypeDiff";
    public static final String P_EXAM_SET_DIFF = "examSetDiff";
    public static final String P_ONE_DIRECTION_FOR_TARGET_ADMISSION = "oneDirectionForTargetAdmission";
    public static final String P_ONE_DIRECTION_FOR_OUT_OF_COMPETITION = "oneDirectionForOutOfCompetition";
    public static final String P_NUMBER_ON_REQUEST_MANUAL = "numberOnRequestManual";
    public static final String P_NUMBER_ON_DIRECTION_MANUAL = "numberOnDirectionManual";
    public static final String P_DIRECTION_PER_COMP_TYPE_DIFF = "directionPerCompTypeDiff";
    public static final String P_DISCIPLINE_FORM_RESTRICTION = "disciplineFormRestriction";
    public static final String P_USE_COMPETITION_GROUP = "useCompetitionGroup";
    public static final String P_COMPETITION_GROUP_EXAM_SET_EQUAL = "competitionGroupExamSetEqual";
    public static final String P_CONTRACT_AUTO_COMPETITION = "contractAutoCompetition";
    public static final String P_STATE_EXAM_RESTRICTION = "stateExamRestriction";
    public static final String P_STATE_EXAM_AUTO_CHECK = "stateExamAutoCheck";
    public static final String P_STATE_EXAM_WAVE1_NUMBER_REQUIRED = "stateExamWave1NumberRequired";
    public static final String P_FORMING_EXAM_GROUP_AUTO = "formingExamGroupAuto";
    public static final String P_ORIGINAL_DOCUMENT_PER_DIRECTION = "originalDocumentPerDirection";
    public static final String P_NEED_ORIGIN_DOC_FOR_PRELIMINARY = "needOriginDocForPreliminary";
    public static final String P_NEED_ORIGIN_DOC_FOR_ORDER = "needOriginDocForOrder";
    public static final String P_ENROLL_PARAGRAPH_PER_EDU_ORG_UNIT = "enrollParagraphPerEduOrgUnit";
    public static final String P_SEARCH_BEFORE_ENTRANT_REGISTRATION = "searchBeforeEntrantRegistration";
    public static final String P_USE_ONLINE_ENTRANT_REGISTRATION = "useOnlineEntrantRegistration";
    public static final String P_OLYMPIAD_DIPLOMA_FOR_DIRECTION = "olympiadDiplomaForDirection";
    public static final String P_PRIORITIES_ADMISSION_TEST = "prioritiesAdmissionTest";
    public static final String P_ALLOW_UPLOAD_DOCUMENTS = "allowUploadDocuments";
    public static final String P_ALLOW_PRINT_ONLINE_REQUEST = "allowPrintOnlineRequest";
    public static final String P_MAX_ENROLLMENT_DIRECTION = "maxEnrollmentDirection";
    public static final String P_MAX_MINISTERIAL_DIRECTION = "maxMinisterialDirection";
    public static final String P_MAX_COMPETITION_GROUP = "maxCompetitionGroup";
    public static final String P_USE_PRINT_REQUEST_BUTTON = "usePrintRequestButton";
    public static final String P_USE_PRINT_EXAM_SHEET_BUTTON = "usePrintExamSheetButton";
    public static final String P_USE_PRINT_DOCUMENTS_INVENTORY_AND_RECEIPT_BUTTON = "usePrintDocumentsInventoryAndReceiptButton";
    public static final String P_USE_PRINT_AUTH_CARD_BUTTON = "usePrintAuthCardButton";
    public static final String P_USE_PRINT_LETTERBOX_BUTTON = "usePrintLetterboxButton";
    public static final String P_USE_CHANGE_PRIORITY_BUTTON = "useChangePriorityButton";
    public static final String P_USE_INDIVIDUAL_PROGRESS_IN_AMOUNT_MARK = "useIndividualProgressInAmountMark";
    public static final String P_CLOSED = "closed";
    public static final String P_REQUIRED_AGREEMENT4_PRE_ENROLLMENT = "requiredAgreement4PreEnrollment";
    public static final String P_REQUIRED_AGREEMENT4_ORDER = "requiredAgreement4Order";
    public static final String L_ENROLLMENT_CAMPAIGN_TYPE = "enrollmentCampaignType";
    public static final String L_EDUCATION_YEAR = "educationYear";
    public static final String L_ENTRANT_EXAM_LISTS_FORMING_FEATURE = "entrantExamListsFormingFeature";
    public static final String L_DEFAULT_QUALIFICATION = "defaultQualification";

    private String _title;     // Название
    private boolean _enrollmentPerCompTypeDiff;     // Есть/нет различий в правилах набора на бюджет/контракт
    private boolean _examSetDiff;     // Разные наборы вступительных испытаний для категорий поступающих
    private boolean _oneDirectionForTargetAdmission;     // Только одно выбранное направление приема для целевого набора
    private boolean _oneDirectionForOutOfCompetition;     // Только одно выбранное направление приема для внеконкурсного набора
    private boolean _numberOnRequestManual;     // Присваивать регистрационный номер заявления вручную
    private boolean _numberOnDirectionManual;     // Присваивать регистрационный номер на направлении подготовки (специальности) вручную
    private boolean _directionPerCompTypeDiff;     // Можно выбирать направление приема с разными видами возмещения затрат
    private boolean _disciplineFormRestriction;     // Ограничение на формирование дисциплин для сдачи при наличии свидетельства ЕГЭ
    private boolean _useCompetitionGroup;     // Используются конкурсные группы
    private boolean _competitionGroupExamSetEqual;     // В конкурсную группу входят только направления подготовки с одинаковым набором вступительных испытаний
    private boolean _contractAutoCompetition;     // Возможность участия абитуриента в конкурсе на контракт
    private boolean _stateExamRestriction;     // Свидетельство ЕГЭ накладывает ограничения на выбор направлений/специальностей в заявлении
    private boolean _stateExamAutoCheck;     // Регистрировать свидетельства ЕГЭ проверенными
    private boolean _stateExamWave1NumberRequired;     // Номер для свидетельств ЕГЭ 1-ой волны не обязателен
    private boolean _formingExamGroupAuto;     // Формировать экзаменационные группы автоматически
    private boolean _originalDocumentPerDirection;     // Прием оригиналов документов на направление подготовки (специальность)
    private boolean _needOriginDocForPreliminary = true;     // Для предварительного зачисления необходимы оригиналы документов
    private boolean _needOriginDocForOrder = true;     // Для включения в приказ необходимы оригиналы документов
    private boolean _enrollParagraphPerEduOrgUnit = true;     // Параграф приказа о зачислении формируется на направление подготовки (специальность)
    private boolean _searchBeforeEntrantRegistration;     // Использовать предварительный поиск абитуриента
    private boolean _useOnlineEntrantRegistration;     // Использовать регистрацию онлайн-абитуриентов
    private boolean _olympiadDiplomaForDirection;     // Зачитывать диплом олимпиады только за одно направление приема
    private boolean _prioritiesAdmissionTest;     // Учитывать приоритеты вступительных испытаний
    private boolean _allowUploadDocuments;     // Загрузка абитуриентом файла с копиями документов
    private boolean _allowPrintOnlineRequest;     // Печать абитуриентом онлайн-заявления
    private Integer _maxEnrollmentDirection;     // Число выбираемых в заявлении направлений подготовки (специальностей) для приема
    private Integer _maxMinisterialDirection;     // Число выбираемых в заявлении направлений подготовки (специальностей) по классификатору
    private Integer _maxCompetitionGroup;     // Число выбираемых конкурсных групп в заявлении
    private boolean _usePrintRequestButton;     // Использовать кнопку печати заявления
    private boolean _usePrintExamSheetButton;     // Использовать кнопку печати экзаменационного листа
    private boolean _usePrintDocumentsInventoryAndReceiptButton;     // Использовать кнопку печати описи и расписки
    private boolean _usePrintAuthCardButton;     // Использовать кнопку печати удостоверения
    private boolean _usePrintLetterboxButton;     // Использовать кнопку печати титульного листа
    private boolean _useChangePriorityButton;     // Использовать кнопку смены приоритетов
    private boolean _useIndividualProgressInAmountMark = false;     // Учитывать индивидуальные достижения в сумме конкурсных баллов
    private boolean _closed = false;     // Закрыта
    private boolean _requiredAgreement4PreEnrollment = false;     // Обязательно согласие на зачисление для предварительного зачисления
    private boolean _requiredAgreement4Order = false;     // Обязательно согласие на зачисление для включения в приказ
    private EnrollmentCampaignType _enrollmentCampaignType;     // Тип приемной кампании
    private EducationYear _educationYear;     // Учебный год
    private EntrantExamListsFormingFeature _entrantExamListsFormingFeature;     // Особенность формирования экзаменационных листов
    private Qualifications _defaultQualification;     // Квалификация по умолчанию

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null и должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Есть/нет различий в правилах набора на бюджет/контракт. Свойство не может быть null.
     */
    @NotNull
    public boolean isEnrollmentPerCompTypeDiff()
    {
        return _enrollmentPerCompTypeDiff;
    }

    /**
     * @param enrollmentPerCompTypeDiff Есть/нет различий в правилах набора на бюджет/контракт. Свойство не может быть null.
     */
    public void setEnrollmentPerCompTypeDiff(boolean enrollmentPerCompTypeDiff)
    {
        dirty(_enrollmentPerCompTypeDiff, enrollmentPerCompTypeDiff);
        _enrollmentPerCompTypeDiff = enrollmentPerCompTypeDiff;
    }

    /**
     * @return Разные наборы вступительных испытаний для категорий поступающих. Свойство не может быть null.
     */
    @NotNull
    public boolean isExamSetDiff()
    {
        return _examSetDiff;
    }

    /**
     * @param examSetDiff Разные наборы вступительных испытаний для категорий поступающих. Свойство не может быть null.
     */
    public void setExamSetDiff(boolean examSetDiff)
    {
        dirty(_examSetDiff, examSetDiff);
        _examSetDiff = examSetDiff;
    }

    /**
     * @return Только одно выбранное направление приема для целевого набора. Свойство не может быть null.
     */
    @NotNull
    public boolean isOneDirectionForTargetAdmission()
    {
        return _oneDirectionForTargetAdmission;
    }

    /**
     * @param oneDirectionForTargetAdmission Только одно выбранное направление приема для целевого набора. Свойство не может быть null.
     */
    public void setOneDirectionForTargetAdmission(boolean oneDirectionForTargetAdmission)
    {
        dirty(_oneDirectionForTargetAdmission, oneDirectionForTargetAdmission);
        _oneDirectionForTargetAdmission = oneDirectionForTargetAdmission;
    }

    /**
     * @return Только одно выбранное направление приема для внеконкурсного набора. Свойство не может быть null.
     */
    @NotNull
    public boolean isOneDirectionForOutOfCompetition()
    {
        return _oneDirectionForOutOfCompetition;
    }

    /**
     * @param oneDirectionForOutOfCompetition Только одно выбранное направление приема для внеконкурсного набора. Свойство не может быть null.
     */
    public void setOneDirectionForOutOfCompetition(boolean oneDirectionForOutOfCompetition)
    {
        dirty(_oneDirectionForOutOfCompetition, oneDirectionForOutOfCompetition);
        _oneDirectionForOutOfCompetition = oneDirectionForOutOfCompetition;
    }

    /**
     * @return Присваивать регистрационный номер заявления вручную. Свойство не может быть null.
     */
    @NotNull
    public boolean isNumberOnRequestManual()
    {
        return _numberOnRequestManual;
    }

    /**
     * @param numberOnRequestManual Присваивать регистрационный номер заявления вручную. Свойство не может быть null.
     */
    public void setNumberOnRequestManual(boolean numberOnRequestManual)
    {
        dirty(_numberOnRequestManual, numberOnRequestManual);
        _numberOnRequestManual = numberOnRequestManual;
    }

    /**
     * @return Присваивать регистрационный номер на направлении подготовки (специальности) вручную. Свойство не может быть null.
     */
    @NotNull
    public boolean isNumberOnDirectionManual()
    {
        return _numberOnDirectionManual;
    }

    /**
     * @param numberOnDirectionManual Присваивать регистрационный номер на направлении подготовки (специальности) вручную. Свойство не может быть null.
     */
    public void setNumberOnDirectionManual(boolean numberOnDirectionManual)
    {
        dirty(_numberOnDirectionManual, numberOnDirectionManual);
        _numberOnDirectionManual = numberOnDirectionManual;
    }

    /**
     * @return Можно выбирать направление приема с разными видами возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public boolean isDirectionPerCompTypeDiff()
    {
        return _directionPerCompTypeDiff;
    }

    /**
     * @param directionPerCompTypeDiff Можно выбирать направление приема с разными видами возмещения затрат. Свойство не может быть null.
     */
    public void setDirectionPerCompTypeDiff(boolean directionPerCompTypeDiff)
    {
        dirty(_directionPerCompTypeDiff, directionPerCompTypeDiff);
        _directionPerCompTypeDiff = directionPerCompTypeDiff;
    }

    /**
     * @return Ограничение на формирование дисциплин для сдачи при наличии свидетельства ЕГЭ. Свойство не может быть null.
     */
    @NotNull
    public boolean isDisciplineFormRestriction()
    {
        return _disciplineFormRestriction;
    }

    /**
     * @param disciplineFormRestriction Ограничение на формирование дисциплин для сдачи при наличии свидетельства ЕГЭ. Свойство не может быть null.
     */
    public void setDisciplineFormRestriction(boolean disciplineFormRestriction)
    {
        dirty(_disciplineFormRestriction, disciplineFormRestriction);
        _disciplineFormRestriction = disciplineFormRestriction;
    }

    /**
     * @return Используются конкурсные группы. Свойство не может быть null.
     */
    @NotNull
    public boolean isUseCompetitionGroup()
    {
        return _useCompetitionGroup;
    }

    /**
     * @param useCompetitionGroup Используются конкурсные группы. Свойство не может быть null.
     */
    public void setUseCompetitionGroup(boolean useCompetitionGroup)
    {
        dirty(_useCompetitionGroup, useCompetitionGroup);
        _useCompetitionGroup = useCompetitionGroup;
    }

    /**
     * @return В конкурсную группу входят только направления подготовки с одинаковым набором вступительных испытаний. Свойство не может быть null.
     */
    @NotNull
    public boolean isCompetitionGroupExamSetEqual()
    {
        return _competitionGroupExamSetEqual;
    }

    /**
     * @param competitionGroupExamSetEqual В конкурсную группу входят только направления подготовки с одинаковым набором вступительных испытаний. Свойство не может быть null.
     */
    public void setCompetitionGroupExamSetEqual(boolean competitionGroupExamSetEqual)
    {
        dirty(_competitionGroupExamSetEqual, competitionGroupExamSetEqual);
        _competitionGroupExamSetEqual = competitionGroupExamSetEqual;
    }

    /**
     * @return Возможность участия абитуриента в конкурсе на контракт. Свойство не может быть null.
     */
    @NotNull
    public boolean isContractAutoCompetition()
    {
        return _contractAutoCompetition;
    }

    /**
     * @param contractAutoCompetition Возможность участия абитуриента в конкурсе на контракт. Свойство не может быть null.
     */
    public void setContractAutoCompetition(boolean contractAutoCompetition)
    {
        dirty(_contractAutoCompetition, contractAutoCompetition);
        _contractAutoCompetition = contractAutoCompetition;
    }

    /**
     * @return Свидетельство ЕГЭ накладывает ограничения на выбор направлений/специальностей в заявлении. Свойство не может быть null.
     */
    @NotNull
    public boolean isStateExamRestriction()
    {
        return _stateExamRestriction;
    }

    /**
     * @param stateExamRestriction Свидетельство ЕГЭ накладывает ограничения на выбор направлений/специальностей в заявлении. Свойство не может быть null.
     */
    public void setStateExamRestriction(boolean stateExamRestriction)
    {
        dirty(_stateExamRestriction, stateExamRestriction);
        _stateExamRestriction = stateExamRestriction;
    }

    /**
     * @return Регистрировать свидетельства ЕГЭ проверенными. Свойство не может быть null.
     */
    @NotNull
    public boolean isStateExamAutoCheck()
    {
        return _stateExamAutoCheck;
    }

    /**
     * @param stateExamAutoCheck Регистрировать свидетельства ЕГЭ проверенными. Свойство не может быть null.
     */
    public void setStateExamAutoCheck(boolean stateExamAutoCheck)
    {
        dirty(_stateExamAutoCheck, stateExamAutoCheck);
        _stateExamAutoCheck = stateExamAutoCheck;
    }

    /**
     * @return Номер для свидетельств ЕГЭ 1-ой волны не обязателен. Свойство не может быть null.
     */
    @NotNull
    public boolean isStateExamWave1NumberRequired()
    {
        return _stateExamWave1NumberRequired;
    }

    /**
     * @param stateExamWave1NumberRequired Номер для свидетельств ЕГЭ 1-ой волны не обязателен. Свойство не может быть null.
     */
    public void setStateExamWave1NumberRequired(boolean stateExamWave1NumberRequired)
    {
        dirty(_stateExamWave1NumberRequired, stateExamWave1NumberRequired);
        _stateExamWave1NumberRequired = stateExamWave1NumberRequired;
    }

    /**
     * @return Формировать экзаменационные группы автоматически. Свойство не может быть null.
     */
    @NotNull
    public boolean isFormingExamGroupAuto()
    {
        return _formingExamGroupAuto;
    }

    /**
     * @param formingExamGroupAuto Формировать экзаменационные группы автоматически. Свойство не может быть null.
     */
    public void setFormingExamGroupAuto(boolean formingExamGroupAuto)
    {
        dirty(_formingExamGroupAuto, formingExamGroupAuto);
        _formingExamGroupAuto = formingExamGroupAuto;
    }

    /**
     * @return Прием оригиналов документов на направление подготовки (специальность). Свойство не может быть null.
     */
    @NotNull
    public boolean isOriginalDocumentPerDirection()
    {
        return _originalDocumentPerDirection;
    }

    /**
     * @param originalDocumentPerDirection Прием оригиналов документов на направление подготовки (специальность). Свойство не может быть null.
     */
    public void setOriginalDocumentPerDirection(boolean originalDocumentPerDirection)
    {
        dirty(_originalDocumentPerDirection, originalDocumentPerDirection);
        _originalDocumentPerDirection = originalDocumentPerDirection;
    }

    /**
     * @return Для предварительного зачисления необходимы оригиналы документов. Свойство не может быть null.
     */
    @NotNull
    public boolean isNeedOriginDocForPreliminary()
    {
        return _needOriginDocForPreliminary;
    }

    /**
     * @param needOriginDocForPreliminary Для предварительного зачисления необходимы оригиналы документов. Свойство не может быть null.
     */
    public void setNeedOriginDocForPreliminary(boolean needOriginDocForPreliminary)
    {
        dirty(_needOriginDocForPreliminary, needOriginDocForPreliminary);
        _needOriginDocForPreliminary = needOriginDocForPreliminary;
    }

    /**
     * @return Для включения в приказ необходимы оригиналы документов. Свойство не может быть null.
     */
    @NotNull
    public boolean isNeedOriginDocForOrder()
    {
        return _needOriginDocForOrder;
    }

    /**
     * @param needOriginDocForOrder Для включения в приказ необходимы оригиналы документов. Свойство не может быть null.
     */
    public void setNeedOriginDocForOrder(boolean needOriginDocForOrder)
    {
        dirty(_needOriginDocForOrder, needOriginDocForOrder);
        _needOriginDocForOrder = needOriginDocForOrder;
    }

    /**
     * @return Параграф приказа о зачислении формируется на направление подготовки (специальность). Свойство не может быть null.
     */
    @NotNull
    public boolean isEnrollParagraphPerEduOrgUnit()
    {
        return _enrollParagraphPerEduOrgUnit;
    }

    /**
     * @param enrollParagraphPerEduOrgUnit Параграф приказа о зачислении формируется на направление подготовки (специальность). Свойство не может быть null.
     */
    public void setEnrollParagraphPerEduOrgUnit(boolean enrollParagraphPerEduOrgUnit)
    {
        dirty(_enrollParagraphPerEduOrgUnit, enrollParagraphPerEduOrgUnit);
        _enrollParagraphPerEduOrgUnit = enrollParagraphPerEduOrgUnit;
    }

    /**
     * @return Использовать предварительный поиск абитуриента. Свойство не может быть null.
     */
    @NotNull
    public boolean isSearchBeforeEntrantRegistration()
    {
        return _searchBeforeEntrantRegistration;
    }

    /**
     * @param searchBeforeEntrantRegistration Использовать предварительный поиск абитуриента. Свойство не может быть null.
     */
    public void setSearchBeforeEntrantRegistration(boolean searchBeforeEntrantRegistration)
    {
        dirty(_searchBeforeEntrantRegistration, searchBeforeEntrantRegistration);
        _searchBeforeEntrantRegistration = searchBeforeEntrantRegistration;
    }

    /**
     * @return Использовать регистрацию онлайн-абитуриентов. Свойство не может быть null.
     */
    @NotNull
    public boolean isUseOnlineEntrantRegistration()
    {
        return _useOnlineEntrantRegistration;
    }

    /**
     * @param useOnlineEntrantRegistration Использовать регистрацию онлайн-абитуриентов. Свойство не может быть null.
     */
    public void setUseOnlineEntrantRegistration(boolean useOnlineEntrantRegistration)
    {
        dirty(_useOnlineEntrantRegistration, useOnlineEntrantRegistration);
        _useOnlineEntrantRegistration = useOnlineEntrantRegistration;
    }

    /**
     * @return Зачитывать диплом олимпиады только за одно направление приема. Свойство не может быть null.
     */
    @NotNull
    public boolean isOlympiadDiplomaForDirection()
    {
        return _olympiadDiplomaForDirection;
    }

    /**
     * @param olympiadDiplomaForDirection Зачитывать диплом олимпиады только за одно направление приема. Свойство не может быть null.
     */
    public void setOlympiadDiplomaForDirection(boolean olympiadDiplomaForDirection)
    {
        dirty(_olympiadDiplomaForDirection, olympiadDiplomaForDirection);
        _olympiadDiplomaForDirection = olympiadDiplomaForDirection;
    }

    /**
     * @return Учитывать приоритеты вступительных испытаний. Свойство не может быть null.
     */
    @NotNull
    public boolean isPrioritiesAdmissionTest()
    {
        return _prioritiesAdmissionTest;
    }

    /**
     * @param prioritiesAdmissionTest Учитывать приоритеты вступительных испытаний. Свойство не может быть null.
     */
    public void setPrioritiesAdmissionTest(boolean prioritiesAdmissionTest)
    {
        dirty(_prioritiesAdmissionTest, prioritiesAdmissionTest);
        _prioritiesAdmissionTest = prioritiesAdmissionTest;
    }

    /**
     * @return Загрузка абитуриентом файла с копиями документов. Свойство не может быть null.
     */
    @NotNull
    public boolean isAllowUploadDocuments()
    {
        return _allowUploadDocuments;
    }

    /**
     * @param allowUploadDocuments Загрузка абитуриентом файла с копиями документов. Свойство не может быть null.
     */
    public void setAllowUploadDocuments(boolean allowUploadDocuments)
    {
        dirty(_allowUploadDocuments, allowUploadDocuments);
        _allowUploadDocuments = allowUploadDocuments;
    }

    /**
     * @return Печать абитуриентом онлайн-заявления. Свойство не может быть null.
     */
    @NotNull
    public boolean isAllowPrintOnlineRequest()
    {
        return _allowPrintOnlineRequest;
    }

    /**
     * @param allowPrintOnlineRequest Печать абитуриентом онлайн-заявления. Свойство не может быть null.
     */
    public void setAllowPrintOnlineRequest(boolean allowPrintOnlineRequest)
    {
        dirty(_allowPrintOnlineRequest, allowPrintOnlineRequest);
        _allowPrintOnlineRequest = allowPrintOnlineRequest;
    }

    /**
     * @return Число выбираемых в заявлении направлений подготовки (специальностей) для приема.
     */
    public Integer getMaxEnrollmentDirection()
    {
        return _maxEnrollmentDirection;
    }

    /**
     * @param maxEnrollmentDirection Число выбираемых в заявлении направлений подготовки (специальностей) для приема.
     */
    public void setMaxEnrollmentDirection(Integer maxEnrollmentDirection)
    {
        dirty(_maxEnrollmentDirection, maxEnrollmentDirection);
        _maxEnrollmentDirection = maxEnrollmentDirection;
    }

    /**
     * @return Число выбираемых в заявлении направлений подготовки (специальностей) по классификатору.
     */
    public Integer getMaxMinisterialDirection()
    {
        return _maxMinisterialDirection;
    }

    /**
     * @param maxMinisterialDirection Число выбираемых в заявлении направлений подготовки (специальностей) по классификатору.
     */
    public void setMaxMinisterialDirection(Integer maxMinisterialDirection)
    {
        dirty(_maxMinisterialDirection, maxMinisterialDirection);
        _maxMinisterialDirection = maxMinisterialDirection;
    }

    /**
     * @return Число выбираемых конкурсных групп в заявлении.
     */
    public Integer getMaxCompetitionGroup()
    {
        return _maxCompetitionGroup;
    }

    /**
     * @param maxCompetitionGroup Число выбираемых конкурсных групп в заявлении.
     */
    public void setMaxCompetitionGroup(Integer maxCompetitionGroup)
    {
        dirty(_maxCompetitionGroup, maxCompetitionGroup);
        _maxCompetitionGroup = maxCompetitionGroup;
    }

    /**
     * @return Использовать кнопку печати заявления. Свойство не может быть null.
     */
    @NotNull
    public boolean isUsePrintRequestButton()
    {
        return _usePrintRequestButton;
    }

    /**
     * @param usePrintRequestButton Использовать кнопку печати заявления. Свойство не может быть null.
     */
    public void setUsePrintRequestButton(boolean usePrintRequestButton)
    {
        dirty(_usePrintRequestButton, usePrintRequestButton);
        _usePrintRequestButton = usePrintRequestButton;
    }

    /**
     * @return Использовать кнопку печати экзаменационного листа. Свойство не может быть null.
     */
    @NotNull
    public boolean isUsePrintExamSheetButton()
    {
        return _usePrintExamSheetButton;
    }

    /**
     * @param usePrintExamSheetButton Использовать кнопку печати экзаменационного листа. Свойство не может быть null.
     */
    public void setUsePrintExamSheetButton(boolean usePrintExamSheetButton)
    {
        dirty(_usePrintExamSheetButton, usePrintExamSheetButton);
        _usePrintExamSheetButton = usePrintExamSheetButton;
    }

    /**
     * @return Использовать кнопку печати описи и расписки. Свойство не может быть null.
     */
    @NotNull
    public boolean isUsePrintDocumentsInventoryAndReceiptButton()
    {
        return _usePrintDocumentsInventoryAndReceiptButton;
    }

    /**
     * @param usePrintDocumentsInventoryAndReceiptButton Использовать кнопку печати описи и расписки. Свойство не может быть null.
     */
    public void setUsePrintDocumentsInventoryAndReceiptButton(boolean usePrintDocumentsInventoryAndReceiptButton)
    {
        dirty(_usePrintDocumentsInventoryAndReceiptButton, usePrintDocumentsInventoryAndReceiptButton);
        _usePrintDocumentsInventoryAndReceiptButton = usePrintDocumentsInventoryAndReceiptButton;
    }

    /**
     * @return Использовать кнопку печати удостоверения. Свойство не может быть null.
     */
    @NotNull
    public boolean isUsePrintAuthCardButton()
    {
        return _usePrintAuthCardButton;
    }

    /**
     * @param usePrintAuthCardButton Использовать кнопку печати удостоверения. Свойство не может быть null.
     */
    public void setUsePrintAuthCardButton(boolean usePrintAuthCardButton)
    {
        dirty(_usePrintAuthCardButton, usePrintAuthCardButton);
        _usePrintAuthCardButton = usePrintAuthCardButton;
    }

    /**
     * @return Использовать кнопку печати титульного листа. Свойство не может быть null.
     */
    @NotNull
    public boolean isUsePrintLetterboxButton()
    {
        return _usePrintLetterboxButton;
    }

    /**
     * @param usePrintLetterboxButton Использовать кнопку печати титульного листа. Свойство не может быть null.
     */
    public void setUsePrintLetterboxButton(boolean usePrintLetterboxButton)
    {
        dirty(_usePrintLetterboxButton, usePrintLetterboxButton);
        _usePrintLetterboxButton = usePrintLetterboxButton;
    }

    /**
     * @return Использовать кнопку смены приоритетов. Свойство не может быть null.
     */
    @NotNull
    public boolean isUseChangePriorityButton()
    {
        return _useChangePriorityButton;
    }

    /**
     * @param useChangePriorityButton Использовать кнопку смены приоритетов. Свойство не может быть null.
     */
    public void setUseChangePriorityButton(boolean useChangePriorityButton)
    {
        dirty(_useChangePriorityButton, useChangePriorityButton);
        _useChangePriorityButton = useChangePriorityButton;
    }

    /**
     * @return Учитывать индивидуальные достижения в сумме конкурсных баллов. Свойство не может быть null.
     */
    @NotNull
    public boolean isUseIndividualProgressInAmountMark()
    {
        return _useIndividualProgressInAmountMark;
    }

    /**
     * @param useIndividualProgressInAmountMark Учитывать индивидуальные достижения в сумме конкурсных баллов. Свойство не может быть null.
     */
    public void setUseIndividualProgressInAmountMark(boolean useIndividualProgressInAmountMark)
    {
        dirty(_useIndividualProgressInAmountMark, useIndividualProgressInAmountMark);
        _useIndividualProgressInAmountMark = useIndividualProgressInAmountMark;
    }

    /**
     * @return Закрыта. Свойство не может быть null.
     */
    @NotNull
    public boolean isClosed()
    {
        return _closed;
    }

    /**
     * @param closed Закрыта. Свойство не может быть null.
     */
    public void setClosed(boolean closed)
    {
        dirty(_closed, closed);
        _closed = closed;
    }

    /**
     * @return Обязательно согласие на зачисление для предварительного зачисления. Свойство не может быть null.
     */
    @NotNull
    public boolean isRequiredAgreement4PreEnrollment()
    {
        return _requiredAgreement4PreEnrollment;
    }

    /**
     * @param requiredAgreement4PreEnrollment Обязательно согласие на зачисление для предварительного зачисления. Свойство не может быть null.
     */
    public void setRequiredAgreement4PreEnrollment(boolean requiredAgreement4PreEnrollment)
    {
        dirty(_requiredAgreement4PreEnrollment, requiredAgreement4PreEnrollment);
        _requiredAgreement4PreEnrollment = requiredAgreement4PreEnrollment;
    }

    /**
     * @return Обязательно согласие на зачисление для включения в приказ. Свойство не может быть null.
     */
    @NotNull
    public boolean isRequiredAgreement4Order()
    {
        return _requiredAgreement4Order;
    }

    /**
     * @param requiredAgreement4Order Обязательно согласие на зачисление для включения в приказ. Свойство не может быть null.
     */
    public void setRequiredAgreement4Order(boolean requiredAgreement4Order)
    {
        dirty(_requiredAgreement4Order, requiredAgreement4Order);
        _requiredAgreement4Order = requiredAgreement4Order;
    }

    /**
     * @return Тип приемной кампании. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaignType getEnrollmentCampaignType()
    {
        return _enrollmentCampaignType;
    }

    /**
     * @param enrollmentCampaignType Тип приемной кампании. Свойство не может быть null.
     */
    public void setEnrollmentCampaignType(EnrollmentCampaignType enrollmentCampaignType)
    {
        dirty(_enrollmentCampaignType, enrollmentCampaignType);
        _enrollmentCampaignType = enrollmentCampaignType;
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     */
    @NotNull
    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    /**
     * @param educationYear Учебный год. Свойство не может быть null.
     */
    public void setEducationYear(EducationYear educationYear)
    {
        dirty(_educationYear, educationYear);
        _educationYear = educationYear;
    }

    /**
     * @return Особенность формирования экзаменационных листов. Свойство не может быть null.
     */
    @NotNull
    public EntrantExamListsFormingFeature getEntrantExamListsFormingFeature()
    {
        return _entrantExamListsFormingFeature;
    }

    /**
     * @param entrantExamListsFormingFeature Особенность формирования экзаменационных листов. Свойство не может быть null.
     */
    public void setEntrantExamListsFormingFeature(EntrantExamListsFormingFeature entrantExamListsFormingFeature)
    {
        dirty(_entrantExamListsFormingFeature, entrantExamListsFormingFeature);
        _entrantExamListsFormingFeature = entrantExamListsFormingFeature;
    }

    /**
     * @return Квалификация по умолчанию.
     */
    public Qualifications getDefaultQualification()
    {
        return _defaultQualification;
    }

    /**
     * @param defaultQualification Квалификация по умолчанию.
     */
    public void setDefaultQualification(Qualifications defaultQualification)
    {
        dirty(_defaultQualification, defaultQualification);
        _defaultQualification = defaultQualification;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrollmentCampaignGen)
        {
            setTitle(((EnrollmentCampaign)another).getTitle());
            setEnrollmentPerCompTypeDiff(((EnrollmentCampaign)another).isEnrollmentPerCompTypeDiff());
            setExamSetDiff(((EnrollmentCampaign)another).isExamSetDiff());
            setOneDirectionForTargetAdmission(((EnrollmentCampaign)another).isOneDirectionForTargetAdmission());
            setOneDirectionForOutOfCompetition(((EnrollmentCampaign)another).isOneDirectionForOutOfCompetition());
            setNumberOnRequestManual(((EnrollmentCampaign)another).isNumberOnRequestManual());
            setNumberOnDirectionManual(((EnrollmentCampaign)another).isNumberOnDirectionManual());
            setDirectionPerCompTypeDiff(((EnrollmentCampaign)another).isDirectionPerCompTypeDiff());
            setDisciplineFormRestriction(((EnrollmentCampaign)another).isDisciplineFormRestriction());
            setUseCompetitionGroup(((EnrollmentCampaign)another).isUseCompetitionGroup());
            setCompetitionGroupExamSetEqual(((EnrollmentCampaign)another).isCompetitionGroupExamSetEqual());
            setContractAutoCompetition(((EnrollmentCampaign)another).isContractAutoCompetition());
            setStateExamRestriction(((EnrollmentCampaign)another).isStateExamRestriction());
            setStateExamAutoCheck(((EnrollmentCampaign)another).isStateExamAutoCheck());
            setStateExamWave1NumberRequired(((EnrollmentCampaign)another).isStateExamWave1NumberRequired());
            setFormingExamGroupAuto(((EnrollmentCampaign)another).isFormingExamGroupAuto());
            setOriginalDocumentPerDirection(((EnrollmentCampaign)another).isOriginalDocumentPerDirection());
            setNeedOriginDocForPreliminary(((EnrollmentCampaign)another).isNeedOriginDocForPreliminary());
            setNeedOriginDocForOrder(((EnrollmentCampaign)another).isNeedOriginDocForOrder());
            setEnrollParagraphPerEduOrgUnit(((EnrollmentCampaign)another).isEnrollParagraphPerEduOrgUnit());
            setSearchBeforeEntrantRegistration(((EnrollmentCampaign)another).isSearchBeforeEntrantRegistration());
            setUseOnlineEntrantRegistration(((EnrollmentCampaign)another).isUseOnlineEntrantRegistration());
            setOlympiadDiplomaForDirection(((EnrollmentCampaign)another).isOlympiadDiplomaForDirection());
            setPrioritiesAdmissionTest(((EnrollmentCampaign)another).isPrioritiesAdmissionTest());
            setAllowUploadDocuments(((EnrollmentCampaign)another).isAllowUploadDocuments());
            setAllowPrintOnlineRequest(((EnrollmentCampaign)another).isAllowPrintOnlineRequest());
            setMaxEnrollmentDirection(((EnrollmentCampaign)another).getMaxEnrollmentDirection());
            setMaxMinisterialDirection(((EnrollmentCampaign)another).getMaxMinisterialDirection());
            setMaxCompetitionGroup(((EnrollmentCampaign)another).getMaxCompetitionGroup());
            setUsePrintRequestButton(((EnrollmentCampaign)another).isUsePrintRequestButton());
            setUsePrintExamSheetButton(((EnrollmentCampaign)another).isUsePrintExamSheetButton());
            setUsePrintDocumentsInventoryAndReceiptButton(((EnrollmentCampaign)another).isUsePrintDocumentsInventoryAndReceiptButton());
            setUsePrintAuthCardButton(((EnrollmentCampaign)another).isUsePrintAuthCardButton());
            setUsePrintLetterboxButton(((EnrollmentCampaign)another).isUsePrintLetterboxButton());
            setUseChangePriorityButton(((EnrollmentCampaign)another).isUseChangePriorityButton());
            setUseIndividualProgressInAmountMark(((EnrollmentCampaign)another).isUseIndividualProgressInAmountMark());
            setClosed(((EnrollmentCampaign)another).isClosed());
            setRequiredAgreement4PreEnrollment(((EnrollmentCampaign)another).isRequiredAgreement4PreEnrollment());
            setRequiredAgreement4Order(((EnrollmentCampaign)another).isRequiredAgreement4Order());
            setEnrollmentCampaignType(((EnrollmentCampaign)another).getEnrollmentCampaignType());
            setEducationYear(((EnrollmentCampaign)another).getEducationYear());
            setEntrantExamListsFormingFeature(((EnrollmentCampaign)another).getEntrantExamListsFormingFeature());
            setDefaultQualification(((EnrollmentCampaign)another).getDefaultQualification());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrollmentCampaignGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrollmentCampaign.class;
        }

        public T newInstance()
        {
            return (T) new EnrollmentCampaign();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "title":
                    return obj.getTitle();
                case "enrollmentPerCompTypeDiff":
                    return obj.isEnrollmentPerCompTypeDiff();
                case "examSetDiff":
                    return obj.isExamSetDiff();
                case "oneDirectionForTargetAdmission":
                    return obj.isOneDirectionForTargetAdmission();
                case "oneDirectionForOutOfCompetition":
                    return obj.isOneDirectionForOutOfCompetition();
                case "numberOnRequestManual":
                    return obj.isNumberOnRequestManual();
                case "numberOnDirectionManual":
                    return obj.isNumberOnDirectionManual();
                case "directionPerCompTypeDiff":
                    return obj.isDirectionPerCompTypeDiff();
                case "disciplineFormRestriction":
                    return obj.isDisciplineFormRestriction();
                case "useCompetitionGroup":
                    return obj.isUseCompetitionGroup();
                case "competitionGroupExamSetEqual":
                    return obj.isCompetitionGroupExamSetEqual();
                case "contractAutoCompetition":
                    return obj.isContractAutoCompetition();
                case "stateExamRestriction":
                    return obj.isStateExamRestriction();
                case "stateExamAutoCheck":
                    return obj.isStateExamAutoCheck();
                case "stateExamWave1NumberRequired":
                    return obj.isStateExamWave1NumberRequired();
                case "formingExamGroupAuto":
                    return obj.isFormingExamGroupAuto();
                case "originalDocumentPerDirection":
                    return obj.isOriginalDocumentPerDirection();
                case "needOriginDocForPreliminary":
                    return obj.isNeedOriginDocForPreliminary();
                case "needOriginDocForOrder":
                    return obj.isNeedOriginDocForOrder();
                case "enrollParagraphPerEduOrgUnit":
                    return obj.isEnrollParagraphPerEduOrgUnit();
                case "searchBeforeEntrantRegistration":
                    return obj.isSearchBeforeEntrantRegistration();
                case "useOnlineEntrantRegistration":
                    return obj.isUseOnlineEntrantRegistration();
                case "olympiadDiplomaForDirection":
                    return obj.isOlympiadDiplomaForDirection();
                case "prioritiesAdmissionTest":
                    return obj.isPrioritiesAdmissionTest();
                case "allowUploadDocuments":
                    return obj.isAllowUploadDocuments();
                case "allowPrintOnlineRequest":
                    return obj.isAllowPrintOnlineRequest();
                case "maxEnrollmentDirection":
                    return obj.getMaxEnrollmentDirection();
                case "maxMinisterialDirection":
                    return obj.getMaxMinisterialDirection();
                case "maxCompetitionGroup":
                    return obj.getMaxCompetitionGroup();
                case "usePrintRequestButton":
                    return obj.isUsePrintRequestButton();
                case "usePrintExamSheetButton":
                    return obj.isUsePrintExamSheetButton();
                case "usePrintDocumentsInventoryAndReceiptButton":
                    return obj.isUsePrintDocumentsInventoryAndReceiptButton();
                case "usePrintAuthCardButton":
                    return obj.isUsePrintAuthCardButton();
                case "usePrintLetterboxButton":
                    return obj.isUsePrintLetterboxButton();
                case "useChangePriorityButton":
                    return obj.isUseChangePriorityButton();
                case "useIndividualProgressInAmountMark":
                    return obj.isUseIndividualProgressInAmountMark();
                case "closed":
                    return obj.isClosed();
                case "requiredAgreement4PreEnrollment":
                    return obj.isRequiredAgreement4PreEnrollment();
                case "requiredAgreement4Order":
                    return obj.isRequiredAgreement4Order();
                case "enrollmentCampaignType":
                    return obj.getEnrollmentCampaignType();
                case "educationYear":
                    return obj.getEducationYear();
                case "entrantExamListsFormingFeature":
                    return obj.getEntrantExamListsFormingFeature();
                case "defaultQualification":
                    return obj.getDefaultQualification();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "enrollmentPerCompTypeDiff":
                    obj.setEnrollmentPerCompTypeDiff((Boolean) value);
                    return;
                case "examSetDiff":
                    obj.setExamSetDiff((Boolean) value);
                    return;
                case "oneDirectionForTargetAdmission":
                    obj.setOneDirectionForTargetAdmission((Boolean) value);
                    return;
                case "oneDirectionForOutOfCompetition":
                    obj.setOneDirectionForOutOfCompetition((Boolean) value);
                    return;
                case "numberOnRequestManual":
                    obj.setNumberOnRequestManual((Boolean) value);
                    return;
                case "numberOnDirectionManual":
                    obj.setNumberOnDirectionManual((Boolean) value);
                    return;
                case "directionPerCompTypeDiff":
                    obj.setDirectionPerCompTypeDiff((Boolean) value);
                    return;
                case "disciplineFormRestriction":
                    obj.setDisciplineFormRestriction((Boolean) value);
                    return;
                case "useCompetitionGroup":
                    obj.setUseCompetitionGroup((Boolean) value);
                    return;
                case "competitionGroupExamSetEqual":
                    obj.setCompetitionGroupExamSetEqual((Boolean) value);
                    return;
                case "contractAutoCompetition":
                    obj.setContractAutoCompetition((Boolean) value);
                    return;
                case "stateExamRestriction":
                    obj.setStateExamRestriction((Boolean) value);
                    return;
                case "stateExamAutoCheck":
                    obj.setStateExamAutoCheck((Boolean) value);
                    return;
                case "stateExamWave1NumberRequired":
                    obj.setStateExamWave1NumberRequired((Boolean) value);
                    return;
                case "formingExamGroupAuto":
                    obj.setFormingExamGroupAuto((Boolean) value);
                    return;
                case "originalDocumentPerDirection":
                    obj.setOriginalDocumentPerDirection((Boolean) value);
                    return;
                case "needOriginDocForPreliminary":
                    obj.setNeedOriginDocForPreliminary((Boolean) value);
                    return;
                case "needOriginDocForOrder":
                    obj.setNeedOriginDocForOrder((Boolean) value);
                    return;
                case "enrollParagraphPerEduOrgUnit":
                    obj.setEnrollParagraphPerEduOrgUnit((Boolean) value);
                    return;
                case "searchBeforeEntrantRegistration":
                    obj.setSearchBeforeEntrantRegistration((Boolean) value);
                    return;
                case "useOnlineEntrantRegistration":
                    obj.setUseOnlineEntrantRegistration((Boolean) value);
                    return;
                case "olympiadDiplomaForDirection":
                    obj.setOlympiadDiplomaForDirection((Boolean) value);
                    return;
                case "prioritiesAdmissionTest":
                    obj.setPrioritiesAdmissionTest((Boolean) value);
                    return;
                case "allowUploadDocuments":
                    obj.setAllowUploadDocuments((Boolean) value);
                    return;
                case "allowPrintOnlineRequest":
                    obj.setAllowPrintOnlineRequest((Boolean) value);
                    return;
                case "maxEnrollmentDirection":
                    obj.setMaxEnrollmentDirection((Integer) value);
                    return;
                case "maxMinisterialDirection":
                    obj.setMaxMinisterialDirection((Integer) value);
                    return;
                case "maxCompetitionGroup":
                    obj.setMaxCompetitionGroup((Integer) value);
                    return;
                case "usePrintRequestButton":
                    obj.setUsePrintRequestButton((Boolean) value);
                    return;
                case "usePrintExamSheetButton":
                    obj.setUsePrintExamSheetButton((Boolean) value);
                    return;
                case "usePrintDocumentsInventoryAndReceiptButton":
                    obj.setUsePrintDocumentsInventoryAndReceiptButton((Boolean) value);
                    return;
                case "usePrintAuthCardButton":
                    obj.setUsePrintAuthCardButton((Boolean) value);
                    return;
                case "usePrintLetterboxButton":
                    obj.setUsePrintLetterboxButton((Boolean) value);
                    return;
                case "useChangePriorityButton":
                    obj.setUseChangePriorityButton((Boolean) value);
                    return;
                case "useIndividualProgressInAmountMark":
                    obj.setUseIndividualProgressInAmountMark((Boolean) value);
                    return;
                case "closed":
                    obj.setClosed((Boolean) value);
                    return;
                case "requiredAgreement4PreEnrollment":
                    obj.setRequiredAgreement4PreEnrollment((Boolean) value);
                    return;
                case "requiredAgreement4Order":
                    obj.setRequiredAgreement4Order((Boolean) value);
                    return;
                case "enrollmentCampaignType":
                    obj.setEnrollmentCampaignType((EnrollmentCampaignType) value);
                    return;
                case "educationYear":
                    obj.setEducationYear((EducationYear) value);
                    return;
                case "entrantExamListsFormingFeature":
                    obj.setEntrantExamListsFormingFeature((EntrantExamListsFormingFeature) value);
                    return;
                case "defaultQualification":
                    obj.setDefaultQualification((Qualifications) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "title":
                        return true;
                case "enrollmentPerCompTypeDiff":
                        return true;
                case "examSetDiff":
                        return true;
                case "oneDirectionForTargetAdmission":
                        return true;
                case "oneDirectionForOutOfCompetition":
                        return true;
                case "numberOnRequestManual":
                        return true;
                case "numberOnDirectionManual":
                        return true;
                case "directionPerCompTypeDiff":
                        return true;
                case "disciplineFormRestriction":
                        return true;
                case "useCompetitionGroup":
                        return true;
                case "competitionGroupExamSetEqual":
                        return true;
                case "contractAutoCompetition":
                        return true;
                case "stateExamRestriction":
                        return true;
                case "stateExamAutoCheck":
                        return true;
                case "stateExamWave1NumberRequired":
                        return true;
                case "formingExamGroupAuto":
                        return true;
                case "originalDocumentPerDirection":
                        return true;
                case "needOriginDocForPreliminary":
                        return true;
                case "needOriginDocForOrder":
                        return true;
                case "enrollParagraphPerEduOrgUnit":
                        return true;
                case "searchBeforeEntrantRegistration":
                        return true;
                case "useOnlineEntrantRegistration":
                        return true;
                case "olympiadDiplomaForDirection":
                        return true;
                case "prioritiesAdmissionTest":
                        return true;
                case "allowUploadDocuments":
                        return true;
                case "allowPrintOnlineRequest":
                        return true;
                case "maxEnrollmentDirection":
                        return true;
                case "maxMinisterialDirection":
                        return true;
                case "maxCompetitionGroup":
                        return true;
                case "usePrintRequestButton":
                        return true;
                case "usePrintExamSheetButton":
                        return true;
                case "usePrintDocumentsInventoryAndReceiptButton":
                        return true;
                case "usePrintAuthCardButton":
                        return true;
                case "usePrintLetterboxButton":
                        return true;
                case "useChangePriorityButton":
                        return true;
                case "useIndividualProgressInAmountMark":
                        return true;
                case "closed":
                        return true;
                case "requiredAgreement4PreEnrollment":
                        return true;
                case "requiredAgreement4Order":
                        return true;
                case "enrollmentCampaignType":
                        return true;
                case "educationYear":
                        return true;
                case "entrantExamListsFormingFeature":
                        return true;
                case "defaultQualification":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "title":
                    return true;
                case "enrollmentPerCompTypeDiff":
                    return true;
                case "examSetDiff":
                    return true;
                case "oneDirectionForTargetAdmission":
                    return true;
                case "oneDirectionForOutOfCompetition":
                    return true;
                case "numberOnRequestManual":
                    return true;
                case "numberOnDirectionManual":
                    return true;
                case "directionPerCompTypeDiff":
                    return true;
                case "disciplineFormRestriction":
                    return true;
                case "useCompetitionGroup":
                    return true;
                case "competitionGroupExamSetEqual":
                    return true;
                case "contractAutoCompetition":
                    return true;
                case "stateExamRestriction":
                    return true;
                case "stateExamAutoCheck":
                    return true;
                case "stateExamWave1NumberRequired":
                    return true;
                case "formingExamGroupAuto":
                    return true;
                case "originalDocumentPerDirection":
                    return true;
                case "needOriginDocForPreliminary":
                    return true;
                case "needOriginDocForOrder":
                    return true;
                case "enrollParagraphPerEduOrgUnit":
                    return true;
                case "searchBeforeEntrantRegistration":
                    return true;
                case "useOnlineEntrantRegistration":
                    return true;
                case "olympiadDiplomaForDirection":
                    return true;
                case "prioritiesAdmissionTest":
                    return true;
                case "allowUploadDocuments":
                    return true;
                case "allowPrintOnlineRequest":
                    return true;
                case "maxEnrollmentDirection":
                    return true;
                case "maxMinisterialDirection":
                    return true;
                case "maxCompetitionGroup":
                    return true;
                case "usePrintRequestButton":
                    return true;
                case "usePrintExamSheetButton":
                    return true;
                case "usePrintDocumentsInventoryAndReceiptButton":
                    return true;
                case "usePrintAuthCardButton":
                    return true;
                case "usePrintLetterboxButton":
                    return true;
                case "useChangePriorityButton":
                    return true;
                case "useIndividualProgressInAmountMark":
                    return true;
                case "closed":
                    return true;
                case "requiredAgreement4PreEnrollment":
                    return true;
                case "requiredAgreement4Order":
                    return true;
                case "enrollmentCampaignType":
                    return true;
                case "educationYear":
                    return true;
                case "entrantExamListsFormingFeature":
                    return true;
                case "defaultQualification":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "title":
                    return String.class;
                case "enrollmentPerCompTypeDiff":
                    return Boolean.class;
                case "examSetDiff":
                    return Boolean.class;
                case "oneDirectionForTargetAdmission":
                    return Boolean.class;
                case "oneDirectionForOutOfCompetition":
                    return Boolean.class;
                case "numberOnRequestManual":
                    return Boolean.class;
                case "numberOnDirectionManual":
                    return Boolean.class;
                case "directionPerCompTypeDiff":
                    return Boolean.class;
                case "disciplineFormRestriction":
                    return Boolean.class;
                case "useCompetitionGroup":
                    return Boolean.class;
                case "competitionGroupExamSetEqual":
                    return Boolean.class;
                case "contractAutoCompetition":
                    return Boolean.class;
                case "stateExamRestriction":
                    return Boolean.class;
                case "stateExamAutoCheck":
                    return Boolean.class;
                case "stateExamWave1NumberRequired":
                    return Boolean.class;
                case "formingExamGroupAuto":
                    return Boolean.class;
                case "originalDocumentPerDirection":
                    return Boolean.class;
                case "needOriginDocForPreliminary":
                    return Boolean.class;
                case "needOriginDocForOrder":
                    return Boolean.class;
                case "enrollParagraphPerEduOrgUnit":
                    return Boolean.class;
                case "searchBeforeEntrantRegistration":
                    return Boolean.class;
                case "useOnlineEntrantRegistration":
                    return Boolean.class;
                case "olympiadDiplomaForDirection":
                    return Boolean.class;
                case "prioritiesAdmissionTest":
                    return Boolean.class;
                case "allowUploadDocuments":
                    return Boolean.class;
                case "allowPrintOnlineRequest":
                    return Boolean.class;
                case "maxEnrollmentDirection":
                    return Integer.class;
                case "maxMinisterialDirection":
                    return Integer.class;
                case "maxCompetitionGroup":
                    return Integer.class;
                case "usePrintRequestButton":
                    return Boolean.class;
                case "usePrintExamSheetButton":
                    return Boolean.class;
                case "usePrintDocumentsInventoryAndReceiptButton":
                    return Boolean.class;
                case "usePrintAuthCardButton":
                    return Boolean.class;
                case "usePrintLetterboxButton":
                    return Boolean.class;
                case "useChangePriorityButton":
                    return Boolean.class;
                case "useIndividualProgressInAmountMark":
                    return Boolean.class;
                case "closed":
                    return Boolean.class;
                case "requiredAgreement4PreEnrollment":
                    return Boolean.class;
                case "requiredAgreement4Order":
                    return Boolean.class;
                case "enrollmentCampaignType":
                    return EnrollmentCampaignType.class;
                case "educationYear":
                    return EducationYear.class;
                case "entrantExamListsFormingFeature":
                    return EntrantExamListsFormingFeature.class;
                case "defaultQualification":
                    return Qualifications.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrollmentCampaign> _dslPath = new Path<EnrollmentCampaign>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrollmentCampaign");
    }
            

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Есть/нет различий в правилах набора на бюджет/контракт. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isEnrollmentPerCompTypeDiff()
     */
    public static PropertyPath<Boolean> enrollmentPerCompTypeDiff()
    {
        return _dslPath.enrollmentPerCompTypeDiff();
    }

    /**
     * @return Разные наборы вступительных испытаний для категорий поступающих. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isExamSetDiff()
     */
    public static PropertyPath<Boolean> examSetDiff()
    {
        return _dslPath.examSetDiff();
    }

    /**
     * @return Только одно выбранное направление приема для целевого набора. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isOneDirectionForTargetAdmission()
     */
    public static PropertyPath<Boolean> oneDirectionForTargetAdmission()
    {
        return _dslPath.oneDirectionForTargetAdmission();
    }

    /**
     * @return Только одно выбранное направление приема для внеконкурсного набора. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isOneDirectionForOutOfCompetition()
     */
    public static PropertyPath<Boolean> oneDirectionForOutOfCompetition()
    {
        return _dslPath.oneDirectionForOutOfCompetition();
    }

    /**
     * @return Присваивать регистрационный номер заявления вручную. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isNumberOnRequestManual()
     */
    public static PropertyPath<Boolean> numberOnRequestManual()
    {
        return _dslPath.numberOnRequestManual();
    }

    /**
     * @return Присваивать регистрационный номер на направлении подготовки (специальности) вручную. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isNumberOnDirectionManual()
     */
    public static PropertyPath<Boolean> numberOnDirectionManual()
    {
        return _dslPath.numberOnDirectionManual();
    }

    /**
     * @return Можно выбирать направление приема с разными видами возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isDirectionPerCompTypeDiff()
     */
    public static PropertyPath<Boolean> directionPerCompTypeDiff()
    {
        return _dslPath.directionPerCompTypeDiff();
    }

    /**
     * @return Ограничение на формирование дисциплин для сдачи при наличии свидетельства ЕГЭ. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isDisciplineFormRestriction()
     */
    public static PropertyPath<Boolean> disciplineFormRestriction()
    {
        return _dslPath.disciplineFormRestriction();
    }

    /**
     * @return Используются конкурсные группы. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isUseCompetitionGroup()
     */
    public static PropertyPath<Boolean> useCompetitionGroup()
    {
        return _dslPath.useCompetitionGroup();
    }

    /**
     * @return В конкурсную группу входят только направления подготовки с одинаковым набором вступительных испытаний. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isCompetitionGroupExamSetEqual()
     */
    public static PropertyPath<Boolean> competitionGroupExamSetEqual()
    {
        return _dslPath.competitionGroupExamSetEqual();
    }

    /**
     * @return Возможность участия абитуриента в конкурсе на контракт. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isContractAutoCompetition()
     */
    public static PropertyPath<Boolean> contractAutoCompetition()
    {
        return _dslPath.contractAutoCompetition();
    }

    /**
     * @return Свидетельство ЕГЭ накладывает ограничения на выбор направлений/специальностей в заявлении. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isStateExamRestriction()
     */
    public static PropertyPath<Boolean> stateExamRestriction()
    {
        return _dslPath.stateExamRestriction();
    }

    /**
     * @return Регистрировать свидетельства ЕГЭ проверенными. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isStateExamAutoCheck()
     */
    public static PropertyPath<Boolean> stateExamAutoCheck()
    {
        return _dslPath.stateExamAutoCheck();
    }

    /**
     * @return Номер для свидетельств ЕГЭ 1-ой волны не обязателен. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isStateExamWave1NumberRequired()
     */
    public static PropertyPath<Boolean> stateExamWave1NumberRequired()
    {
        return _dslPath.stateExamWave1NumberRequired();
    }

    /**
     * @return Формировать экзаменационные группы автоматически. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isFormingExamGroupAuto()
     */
    public static PropertyPath<Boolean> formingExamGroupAuto()
    {
        return _dslPath.formingExamGroupAuto();
    }

    /**
     * @return Прием оригиналов документов на направление подготовки (специальность). Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isOriginalDocumentPerDirection()
     */
    public static PropertyPath<Boolean> originalDocumentPerDirection()
    {
        return _dslPath.originalDocumentPerDirection();
    }

    /**
     * @return Для предварительного зачисления необходимы оригиналы документов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isNeedOriginDocForPreliminary()
     */
    public static PropertyPath<Boolean> needOriginDocForPreliminary()
    {
        return _dslPath.needOriginDocForPreliminary();
    }

    /**
     * @return Для включения в приказ необходимы оригиналы документов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isNeedOriginDocForOrder()
     */
    public static PropertyPath<Boolean> needOriginDocForOrder()
    {
        return _dslPath.needOriginDocForOrder();
    }

    /**
     * @return Параграф приказа о зачислении формируется на направление подготовки (специальность). Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isEnrollParagraphPerEduOrgUnit()
     */
    public static PropertyPath<Boolean> enrollParagraphPerEduOrgUnit()
    {
        return _dslPath.enrollParagraphPerEduOrgUnit();
    }

    /**
     * @return Использовать предварительный поиск абитуриента. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isSearchBeforeEntrantRegistration()
     */
    public static PropertyPath<Boolean> searchBeforeEntrantRegistration()
    {
        return _dslPath.searchBeforeEntrantRegistration();
    }

    /**
     * @return Использовать регистрацию онлайн-абитуриентов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isUseOnlineEntrantRegistration()
     */
    public static PropertyPath<Boolean> useOnlineEntrantRegistration()
    {
        return _dslPath.useOnlineEntrantRegistration();
    }

    /**
     * @return Зачитывать диплом олимпиады только за одно направление приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isOlympiadDiplomaForDirection()
     */
    public static PropertyPath<Boolean> olympiadDiplomaForDirection()
    {
        return _dslPath.olympiadDiplomaForDirection();
    }

    /**
     * @return Учитывать приоритеты вступительных испытаний. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isPrioritiesAdmissionTest()
     */
    public static PropertyPath<Boolean> prioritiesAdmissionTest()
    {
        return _dslPath.prioritiesAdmissionTest();
    }

    /**
     * @return Загрузка абитуриентом файла с копиями документов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isAllowUploadDocuments()
     */
    public static PropertyPath<Boolean> allowUploadDocuments()
    {
        return _dslPath.allowUploadDocuments();
    }

    /**
     * @return Печать абитуриентом онлайн-заявления. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isAllowPrintOnlineRequest()
     */
    public static PropertyPath<Boolean> allowPrintOnlineRequest()
    {
        return _dslPath.allowPrintOnlineRequest();
    }

    /**
     * @return Число выбираемых в заявлении направлений подготовки (специальностей) для приема.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#getMaxEnrollmentDirection()
     */
    public static PropertyPath<Integer> maxEnrollmentDirection()
    {
        return _dslPath.maxEnrollmentDirection();
    }

    /**
     * @return Число выбираемых в заявлении направлений подготовки (специальностей) по классификатору.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#getMaxMinisterialDirection()
     */
    public static PropertyPath<Integer> maxMinisterialDirection()
    {
        return _dslPath.maxMinisterialDirection();
    }

    /**
     * @return Число выбираемых конкурсных групп в заявлении.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#getMaxCompetitionGroup()
     */
    public static PropertyPath<Integer> maxCompetitionGroup()
    {
        return _dslPath.maxCompetitionGroup();
    }

    /**
     * @return Использовать кнопку печати заявления. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isUsePrintRequestButton()
     */
    public static PropertyPath<Boolean> usePrintRequestButton()
    {
        return _dslPath.usePrintRequestButton();
    }

    /**
     * @return Использовать кнопку печати экзаменационного листа. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isUsePrintExamSheetButton()
     */
    public static PropertyPath<Boolean> usePrintExamSheetButton()
    {
        return _dslPath.usePrintExamSheetButton();
    }

    /**
     * @return Использовать кнопку печати описи и расписки. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isUsePrintDocumentsInventoryAndReceiptButton()
     */
    public static PropertyPath<Boolean> usePrintDocumentsInventoryAndReceiptButton()
    {
        return _dslPath.usePrintDocumentsInventoryAndReceiptButton();
    }

    /**
     * @return Использовать кнопку печати удостоверения. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isUsePrintAuthCardButton()
     */
    public static PropertyPath<Boolean> usePrintAuthCardButton()
    {
        return _dslPath.usePrintAuthCardButton();
    }

    /**
     * @return Использовать кнопку печати титульного листа. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isUsePrintLetterboxButton()
     */
    public static PropertyPath<Boolean> usePrintLetterboxButton()
    {
        return _dslPath.usePrintLetterboxButton();
    }

    /**
     * @return Использовать кнопку смены приоритетов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isUseChangePriorityButton()
     */
    public static PropertyPath<Boolean> useChangePriorityButton()
    {
        return _dslPath.useChangePriorityButton();
    }

    /**
     * @return Учитывать индивидуальные достижения в сумме конкурсных баллов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isUseIndividualProgressInAmountMark()
     */
    public static PropertyPath<Boolean> useIndividualProgressInAmountMark()
    {
        return _dslPath.useIndividualProgressInAmountMark();
    }

    /**
     * @return Закрыта. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isClosed()
     */
    public static PropertyPath<Boolean> closed()
    {
        return _dslPath.closed();
    }

    /**
     * @return Обязательно согласие на зачисление для предварительного зачисления. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isRequiredAgreement4PreEnrollment()
     */
    public static PropertyPath<Boolean> requiredAgreement4PreEnrollment()
    {
        return _dslPath.requiredAgreement4PreEnrollment();
    }

    /**
     * @return Обязательно согласие на зачисление для включения в приказ. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isRequiredAgreement4Order()
     */
    public static PropertyPath<Boolean> requiredAgreement4Order()
    {
        return _dslPath.requiredAgreement4Order();
    }

    /**
     * @return Тип приемной кампании. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#getEnrollmentCampaignType()
     */
    public static EnrollmentCampaignType.Path<EnrollmentCampaignType> enrollmentCampaignType()
    {
        return _dslPath.enrollmentCampaignType();
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#getEducationYear()
     */
    public static EducationYear.Path<EducationYear> educationYear()
    {
        return _dslPath.educationYear();
    }

    /**
     * @return Особенность формирования экзаменационных листов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#getEntrantExamListsFormingFeature()
     */
    public static EntrantExamListsFormingFeature.Path<EntrantExamListsFormingFeature> entrantExamListsFormingFeature()
    {
        return _dslPath.entrantExamListsFormingFeature();
    }

    /**
     * @return Квалификация по умолчанию.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#getDefaultQualification()
     */
    public static Qualifications.Path<Qualifications> defaultQualification()
    {
        return _dslPath.defaultQualification();
    }

    public static class Path<E extends EnrollmentCampaign> extends EntityPath<E>
    {
        private PropertyPath<String> _title;
        private PropertyPath<Boolean> _enrollmentPerCompTypeDiff;
        private PropertyPath<Boolean> _examSetDiff;
        private PropertyPath<Boolean> _oneDirectionForTargetAdmission;
        private PropertyPath<Boolean> _oneDirectionForOutOfCompetition;
        private PropertyPath<Boolean> _numberOnRequestManual;
        private PropertyPath<Boolean> _numberOnDirectionManual;
        private PropertyPath<Boolean> _directionPerCompTypeDiff;
        private PropertyPath<Boolean> _disciplineFormRestriction;
        private PropertyPath<Boolean> _useCompetitionGroup;
        private PropertyPath<Boolean> _competitionGroupExamSetEqual;
        private PropertyPath<Boolean> _contractAutoCompetition;
        private PropertyPath<Boolean> _stateExamRestriction;
        private PropertyPath<Boolean> _stateExamAutoCheck;
        private PropertyPath<Boolean> _stateExamWave1NumberRequired;
        private PropertyPath<Boolean> _formingExamGroupAuto;
        private PropertyPath<Boolean> _originalDocumentPerDirection;
        private PropertyPath<Boolean> _needOriginDocForPreliminary;
        private PropertyPath<Boolean> _needOriginDocForOrder;
        private PropertyPath<Boolean> _enrollParagraphPerEduOrgUnit;
        private PropertyPath<Boolean> _searchBeforeEntrantRegistration;
        private PropertyPath<Boolean> _useOnlineEntrantRegistration;
        private PropertyPath<Boolean> _olympiadDiplomaForDirection;
        private PropertyPath<Boolean> _prioritiesAdmissionTest;
        private PropertyPath<Boolean> _allowUploadDocuments;
        private PropertyPath<Boolean> _allowPrintOnlineRequest;
        private PropertyPath<Integer> _maxEnrollmentDirection;
        private PropertyPath<Integer> _maxMinisterialDirection;
        private PropertyPath<Integer> _maxCompetitionGroup;
        private PropertyPath<Boolean> _usePrintRequestButton;
        private PropertyPath<Boolean> _usePrintExamSheetButton;
        private PropertyPath<Boolean> _usePrintDocumentsInventoryAndReceiptButton;
        private PropertyPath<Boolean> _usePrintAuthCardButton;
        private PropertyPath<Boolean> _usePrintLetterboxButton;
        private PropertyPath<Boolean> _useChangePriorityButton;
        private PropertyPath<Boolean> _useIndividualProgressInAmountMark;
        private PropertyPath<Boolean> _closed;
        private PropertyPath<Boolean> _requiredAgreement4PreEnrollment;
        private PropertyPath<Boolean> _requiredAgreement4Order;
        private EnrollmentCampaignType.Path<EnrollmentCampaignType> _enrollmentCampaignType;
        private EducationYear.Path<EducationYear> _educationYear;
        private EntrantExamListsFormingFeature.Path<EntrantExamListsFormingFeature> _entrantExamListsFormingFeature;
        private Qualifications.Path<Qualifications> _defaultQualification;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EnrollmentCampaignGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Есть/нет различий в правилах набора на бюджет/контракт. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isEnrollmentPerCompTypeDiff()
     */
        public PropertyPath<Boolean> enrollmentPerCompTypeDiff()
        {
            if(_enrollmentPerCompTypeDiff == null )
                _enrollmentPerCompTypeDiff = new PropertyPath<Boolean>(EnrollmentCampaignGen.P_ENROLLMENT_PER_COMP_TYPE_DIFF, this);
            return _enrollmentPerCompTypeDiff;
        }

    /**
     * @return Разные наборы вступительных испытаний для категорий поступающих. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isExamSetDiff()
     */
        public PropertyPath<Boolean> examSetDiff()
        {
            if(_examSetDiff == null )
                _examSetDiff = new PropertyPath<Boolean>(EnrollmentCampaignGen.P_EXAM_SET_DIFF, this);
            return _examSetDiff;
        }

    /**
     * @return Только одно выбранное направление приема для целевого набора. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isOneDirectionForTargetAdmission()
     */
        public PropertyPath<Boolean> oneDirectionForTargetAdmission()
        {
            if(_oneDirectionForTargetAdmission == null )
                _oneDirectionForTargetAdmission = new PropertyPath<Boolean>(EnrollmentCampaignGen.P_ONE_DIRECTION_FOR_TARGET_ADMISSION, this);
            return _oneDirectionForTargetAdmission;
        }

    /**
     * @return Только одно выбранное направление приема для внеконкурсного набора. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isOneDirectionForOutOfCompetition()
     */
        public PropertyPath<Boolean> oneDirectionForOutOfCompetition()
        {
            if(_oneDirectionForOutOfCompetition == null )
                _oneDirectionForOutOfCompetition = new PropertyPath<Boolean>(EnrollmentCampaignGen.P_ONE_DIRECTION_FOR_OUT_OF_COMPETITION, this);
            return _oneDirectionForOutOfCompetition;
        }

    /**
     * @return Присваивать регистрационный номер заявления вручную. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isNumberOnRequestManual()
     */
        public PropertyPath<Boolean> numberOnRequestManual()
        {
            if(_numberOnRequestManual == null )
                _numberOnRequestManual = new PropertyPath<Boolean>(EnrollmentCampaignGen.P_NUMBER_ON_REQUEST_MANUAL, this);
            return _numberOnRequestManual;
        }

    /**
     * @return Присваивать регистрационный номер на направлении подготовки (специальности) вручную. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isNumberOnDirectionManual()
     */
        public PropertyPath<Boolean> numberOnDirectionManual()
        {
            if(_numberOnDirectionManual == null )
                _numberOnDirectionManual = new PropertyPath<Boolean>(EnrollmentCampaignGen.P_NUMBER_ON_DIRECTION_MANUAL, this);
            return _numberOnDirectionManual;
        }

    /**
     * @return Можно выбирать направление приема с разными видами возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isDirectionPerCompTypeDiff()
     */
        public PropertyPath<Boolean> directionPerCompTypeDiff()
        {
            if(_directionPerCompTypeDiff == null )
                _directionPerCompTypeDiff = new PropertyPath<Boolean>(EnrollmentCampaignGen.P_DIRECTION_PER_COMP_TYPE_DIFF, this);
            return _directionPerCompTypeDiff;
        }

    /**
     * @return Ограничение на формирование дисциплин для сдачи при наличии свидетельства ЕГЭ. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isDisciplineFormRestriction()
     */
        public PropertyPath<Boolean> disciplineFormRestriction()
        {
            if(_disciplineFormRestriction == null )
                _disciplineFormRestriction = new PropertyPath<Boolean>(EnrollmentCampaignGen.P_DISCIPLINE_FORM_RESTRICTION, this);
            return _disciplineFormRestriction;
        }

    /**
     * @return Используются конкурсные группы. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isUseCompetitionGroup()
     */
        public PropertyPath<Boolean> useCompetitionGroup()
        {
            if(_useCompetitionGroup == null )
                _useCompetitionGroup = new PropertyPath<Boolean>(EnrollmentCampaignGen.P_USE_COMPETITION_GROUP, this);
            return _useCompetitionGroup;
        }

    /**
     * @return В конкурсную группу входят только направления подготовки с одинаковым набором вступительных испытаний. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isCompetitionGroupExamSetEqual()
     */
        public PropertyPath<Boolean> competitionGroupExamSetEqual()
        {
            if(_competitionGroupExamSetEqual == null )
                _competitionGroupExamSetEqual = new PropertyPath<Boolean>(EnrollmentCampaignGen.P_COMPETITION_GROUP_EXAM_SET_EQUAL, this);
            return _competitionGroupExamSetEqual;
        }

    /**
     * @return Возможность участия абитуриента в конкурсе на контракт. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isContractAutoCompetition()
     */
        public PropertyPath<Boolean> contractAutoCompetition()
        {
            if(_contractAutoCompetition == null )
                _contractAutoCompetition = new PropertyPath<Boolean>(EnrollmentCampaignGen.P_CONTRACT_AUTO_COMPETITION, this);
            return _contractAutoCompetition;
        }

    /**
     * @return Свидетельство ЕГЭ накладывает ограничения на выбор направлений/специальностей в заявлении. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isStateExamRestriction()
     */
        public PropertyPath<Boolean> stateExamRestriction()
        {
            if(_stateExamRestriction == null )
                _stateExamRestriction = new PropertyPath<Boolean>(EnrollmentCampaignGen.P_STATE_EXAM_RESTRICTION, this);
            return _stateExamRestriction;
        }

    /**
     * @return Регистрировать свидетельства ЕГЭ проверенными. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isStateExamAutoCheck()
     */
        public PropertyPath<Boolean> stateExamAutoCheck()
        {
            if(_stateExamAutoCheck == null )
                _stateExamAutoCheck = new PropertyPath<Boolean>(EnrollmentCampaignGen.P_STATE_EXAM_AUTO_CHECK, this);
            return _stateExamAutoCheck;
        }

    /**
     * @return Номер для свидетельств ЕГЭ 1-ой волны не обязателен. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isStateExamWave1NumberRequired()
     */
        public PropertyPath<Boolean> stateExamWave1NumberRequired()
        {
            if(_stateExamWave1NumberRequired == null )
                _stateExamWave1NumberRequired = new PropertyPath<Boolean>(EnrollmentCampaignGen.P_STATE_EXAM_WAVE1_NUMBER_REQUIRED, this);
            return _stateExamWave1NumberRequired;
        }

    /**
     * @return Формировать экзаменационные группы автоматически. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isFormingExamGroupAuto()
     */
        public PropertyPath<Boolean> formingExamGroupAuto()
        {
            if(_formingExamGroupAuto == null )
                _formingExamGroupAuto = new PropertyPath<Boolean>(EnrollmentCampaignGen.P_FORMING_EXAM_GROUP_AUTO, this);
            return _formingExamGroupAuto;
        }

    /**
     * @return Прием оригиналов документов на направление подготовки (специальность). Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isOriginalDocumentPerDirection()
     */
        public PropertyPath<Boolean> originalDocumentPerDirection()
        {
            if(_originalDocumentPerDirection == null )
                _originalDocumentPerDirection = new PropertyPath<Boolean>(EnrollmentCampaignGen.P_ORIGINAL_DOCUMENT_PER_DIRECTION, this);
            return _originalDocumentPerDirection;
        }

    /**
     * @return Для предварительного зачисления необходимы оригиналы документов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isNeedOriginDocForPreliminary()
     */
        public PropertyPath<Boolean> needOriginDocForPreliminary()
        {
            if(_needOriginDocForPreliminary == null )
                _needOriginDocForPreliminary = new PropertyPath<Boolean>(EnrollmentCampaignGen.P_NEED_ORIGIN_DOC_FOR_PRELIMINARY, this);
            return _needOriginDocForPreliminary;
        }

    /**
     * @return Для включения в приказ необходимы оригиналы документов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isNeedOriginDocForOrder()
     */
        public PropertyPath<Boolean> needOriginDocForOrder()
        {
            if(_needOriginDocForOrder == null )
                _needOriginDocForOrder = new PropertyPath<Boolean>(EnrollmentCampaignGen.P_NEED_ORIGIN_DOC_FOR_ORDER, this);
            return _needOriginDocForOrder;
        }

    /**
     * @return Параграф приказа о зачислении формируется на направление подготовки (специальность). Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isEnrollParagraphPerEduOrgUnit()
     */
        public PropertyPath<Boolean> enrollParagraphPerEduOrgUnit()
        {
            if(_enrollParagraphPerEduOrgUnit == null )
                _enrollParagraphPerEduOrgUnit = new PropertyPath<Boolean>(EnrollmentCampaignGen.P_ENROLL_PARAGRAPH_PER_EDU_ORG_UNIT, this);
            return _enrollParagraphPerEduOrgUnit;
        }

    /**
     * @return Использовать предварительный поиск абитуриента. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isSearchBeforeEntrantRegistration()
     */
        public PropertyPath<Boolean> searchBeforeEntrantRegistration()
        {
            if(_searchBeforeEntrantRegistration == null )
                _searchBeforeEntrantRegistration = new PropertyPath<Boolean>(EnrollmentCampaignGen.P_SEARCH_BEFORE_ENTRANT_REGISTRATION, this);
            return _searchBeforeEntrantRegistration;
        }

    /**
     * @return Использовать регистрацию онлайн-абитуриентов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isUseOnlineEntrantRegistration()
     */
        public PropertyPath<Boolean> useOnlineEntrantRegistration()
        {
            if(_useOnlineEntrantRegistration == null )
                _useOnlineEntrantRegistration = new PropertyPath<Boolean>(EnrollmentCampaignGen.P_USE_ONLINE_ENTRANT_REGISTRATION, this);
            return _useOnlineEntrantRegistration;
        }

    /**
     * @return Зачитывать диплом олимпиады только за одно направление приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isOlympiadDiplomaForDirection()
     */
        public PropertyPath<Boolean> olympiadDiplomaForDirection()
        {
            if(_olympiadDiplomaForDirection == null )
                _olympiadDiplomaForDirection = new PropertyPath<Boolean>(EnrollmentCampaignGen.P_OLYMPIAD_DIPLOMA_FOR_DIRECTION, this);
            return _olympiadDiplomaForDirection;
        }

    /**
     * @return Учитывать приоритеты вступительных испытаний. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isPrioritiesAdmissionTest()
     */
        public PropertyPath<Boolean> prioritiesAdmissionTest()
        {
            if(_prioritiesAdmissionTest == null )
                _prioritiesAdmissionTest = new PropertyPath<Boolean>(EnrollmentCampaignGen.P_PRIORITIES_ADMISSION_TEST, this);
            return _prioritiesAdmissionTest;
        }

    /**
     * @return Загрузка абитуриентом файла с копиями документов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isAllowUploadDocuments()
     */
        public PropertyPath<Boolean> allowUploadDocuments()
        {
            if(_allowUploadDocuments == null )
                _allowUploadDocuments = new PropertyPath<Boolean>(EnrollmentCampaignGen.P_ALLOW_UPLOAD_DOCUMENTS, this);
            return _allowUploadDocuments;
        }

    /**
     * @return Печать абитуриентом онлайн-заявления. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isAllowPrintOnlineRequest()
     */
        public PropertyPath<Boolean> allowPrintOnlineRequest()
        {
            if(_allowPrintOnlineRequest == null )
                _allowPrintOnlineRequest = new PropertyPath<Boolean>(EnrollmentCampaignGen.P_ALLOW_PRINT_ONLINE_REQUEST, this);
            return _allowPrintOnlineRequest;
        }

    /**
     * @return Число выбираемых в заявлении направлений подготовки (специальностей) для приема.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#getMaxEnrollmentDirection()
     */
        public PropertyPath<Integer> maxEnrollmentDirection()
        {
            if(_maxEnrollmentDirection == null )
                _maxEnrollmentDirection = new PropertyPath<Integer>(EnrollmentCampaignGen.P_MAX_ENROLLMENT_DIRECTION, this);
            return _maxEnrollmentDirection;
        }

    /**
     * @return Число выбираемых в заявлении направлений подготовки (специальностей) по классификатору.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#getMaxMinisterialDirection()
     */
        public PropertyPath<Integer> maxMinisterialDirection()
        {
            if(_maxMinisterialDirection == null )
                _maxMinisterialDirection = new PropertyPath<Integer>(EnrollmentCampaignGen.P_MAX_MINISTERIAL_DIRECTION, this);
            return _maxMinisterialDirection;
        }

    /**
     * @return Число выбираемых конкурсных групп в заявлении.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#getMaxCompetitionGroup()
     */
        public PropertyPath<Integer> maxCompetitionGroup()
        {
            if(_maxCompetitionGroup == null )
                _maxCompetitionGroup = new PropertyPath<Integer>(EnrollmentCampaignGen.P_MAX_COMPETITION_GROUP, this);
            return _maxCompetitionGroup;
        }

    /**
     * @return Использовать кнопку печати заявления. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isUsePrintRequestButton()
     */
        public PropertyPath<Boolean> usePrintRequestButton()
        {
            if(_usePrintRequestButton == null )
                _usePrintRequestButton = new PropertyPath<Boolean>(EnrollmentCampaignGen.P_USE_PRINT_REQUEST_BUTTON, this);
            return _usePrintRequestButton;
        }

    /**
     * @return Использовать кнопку печати экзаменационного листа. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isUsePrintExamSheetButton()
     */
        public PropertyPath<Boolean> usePrintExamSheetButton()
        {
            if(_usePrintExamSheetButton == null )
                _usePrintExamSheetButton = new PropertyPath<Boolean>(EnrollmentCampaignGen.P_USE_PRINT_EXAM_SHEET_BUTTON, this);
            return _usePrintExamSheetButton;
        }

    /**
     * @return Использовать кнопку печати описи и расписки. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isUsePrintDocumentsInventoryAndReceiptButton()
     */
        public PropertyPath<Boolean> usePrintDocumentsInventoryAndReceiptButton()
        {
            if(_usePrintDocumentsInventoryAndReceiptButton == null )
                _usePrintDocumentsInventoryAndReceiptButton = new PropertyPath<Boolean>(EnrollmentCampaignGen.P_USE_PRINT_DOCUMENTS_INVENTORY_AND_RECEIPT_BUTTON, this);
            return _usePrintDocumentsInventoryAndReceiptButton;
        }

    /**
     * @return Использовать кнопку печати удостоверения. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isUsePrintAuthCardButton()
     */
        public PropertyPath<Boolean> usePrintAuthCardButton()
        {
            if(_usePrintAuthCardButton == null )
                _usePrintAuthCardButton = new PropertyPath<Boolean>(EnrollmentCampaignGen.P_USE_PRINT_AUTH_CARD_BUTTON, this);
            return _usePrintAuthCardButton;
        }

    /**
     * @return Использовать кнопку печати титульного листа. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isUsePrintLetterboxButton()
     */
        public PropertyPath<Boolean> usePrintLetterboxButton()
        {
            if(_usePrintLetterboxButton == null )
                _usePrintLetterboxButton = new PropertyPath<Boolean>(EnrollmentCampaignGen.P_USE_PRINT_LETTERBOX_BUTTON, this);
            return _usePrintLetterboxButton;
        }

    /**
     * @return Использовать кнопку смены приоритетов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isUseChangePriorityButton()
     */
        public PropertyPath<Boolean> useChangePriorityButton()
        {
            if(_useChangePriorityButton == null )
                _useChangePriorityButton = new PropertyPath<Boolean>(EnrollmentCampaignGen.P_USE_CHANGE_PRIORITY_BUTTON, this);
            return _useChangePriorityButton;
        }

    /**
     * @return Учитывать индивидуальные достижения в сумме конкурсных баллов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isUseIndividualProgressInAmountMark()
     */
        public PropertyPath<Boolean> useIndividualProgressInAmountMark()
        {
            if(_useIndividualProgressInAmountMark == null )
                _useIndividualProgressInAmountMark = new PropertyPath<Boolean>(EnrollmentCampaignGen.P_USE_INDIVIDUAL_PROGRESS_IN_AMOUNT_MARK, this);
            return _useIndividualProgressInAmountMark;
        }

    /**
     * @return Закрыта. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isClosed()
     */
        public PropertyPath<Boolean> closed()
        {
            if(_closed == null )
                _closed = new PropertyPath<Boolean>(EnrollmentCampaignGen.P_CLOSED, this);
            return _closed;
        }

    /**
     * @return Обязательно согласие на зачисление для предварительного зачисления. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isRequiredAgreement4PreEnrollment()
     */
        public PropertyPath<Boolean> requiredAgreement4PreEnrollment()
        {
            if(_requiredAgreement4PreEnrollment == null )
                _requiredAgreement4PreEnrollment = new PropertyPath<Boolean>(EnrollmentCampaignGen.P_REQUIRED_AGREEMENT4_PRE_ENROLLMENT, this);
            return _requiredAgreement4PreEnrollment;
        }

    /**
     * @return Обязательно согласие на зачисление для включения в приказ. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#isRequiredAgreement4Order()
     */
        public PropertyPath<Boolean> requiredAgreement4Order()
        {
            if(_requiredAgreement4Order == null )
                _requiredAgreement4Order = new PropertyPath<Boolean>(EnrollmentCampaignGen.P_REQUIRED_AGREEMENT4_ORDER, this);
            return _requiredAgreement4Order;
        }

    /**
     * @return Тип приемной кампании. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#getEnrollmentCampaignType()
     */
        public EnrollmentCampaignType.Path<EnrollmentCampaignType> enrollmentCampaignType()
        {
            if(_enrollmentCampaignType == null )
                _enrollmentCampaignType = new EnrollmentCampaignType.Path<EnrollmentCampaignType>(L_ENROLLMENT_CAMPAIGN_TYPE, this);
            return _enrollmentCampaignType;
        }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#getEducationYear()
     */
        public EducationYear.Path<EducationYear> educationYear()
        {
            if(_educationYear == null )
                _educationYear = new EducationYear.Path<EducationYear>(L_EDUCATION_YEAR, this);
            return _educationYear;
        }

    /**
     * @return Особенность формирования экзаменационных листов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#getEntrantExamListsFormingFeature()
     */
        public EntrantExamListsFormingFeature.Path<EntrantExamListsFormingFeature> entrantExamListsFormingFeature()
        {
            if(_entrantExamListsFormingFeature == null )
                _entrantExamListsFormingFeature = new EntrantExamListsFormingFeature.Path<EntrantExamListsFormingFeature>(L_ENTRANT_EXAM_LISTS_FORMING_FEATURE, this);
            return _entrantExamListsFormingFeature;
        }

    /**
     * @return Квалификация по умолчанию.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign#getDefaultQualification()
     */
        public Qualifications.Path<Qualifications> defaultQualification()
        {
            if(_defaultQualification == null )
                _defaultQualification = new Qualifications.Path<Qualifications>(L_DEFAULT_QUALIFICATION, this);
            return _defaultQualification;
        }

        public Class getEntityClass()
        {
            return EnrollmentCampaign.class;
        }

        public String getEntityName()
        {
            return "enrollmentCampaign";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
