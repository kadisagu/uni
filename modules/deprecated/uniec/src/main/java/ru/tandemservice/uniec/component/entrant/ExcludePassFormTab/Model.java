/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.entrant.ExcludePassFormTab;

import java.util.List;
import java.util.Map;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.tapsupport.component.matrix.CellCoordinates;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.entrant.Entrant;

/**
 * @author Vasily Zhukov
 * @since 26.07.2010
 */
@State({@Bind(key = "entrantId", binding = "entrant.id")})
public class Model
{
    private IDataSettings _settings;
    private Entrant _entrant = new Entrant();
    private ISelectModel _developFormList;
    private List<EntrantRequestGroup> _entrantRequestGroups;
    private List<SubjectPassForm> _subjectPassForms;
    private Map<CellCoordinates, Boolean> _matrixData;

    // Getters & Setters

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public Entrant getEntrant()
    {
        return _entrant;
    }

    public void setEntrant(Entrant entrant)
    {
        _entrant = entrant;
    }

    public ISelectModel getDevelopFormList() { return _developFormList; }
    public void setDevelopFormList(ISelectModel developFormList) { _developFormList = developFormList; }

    public List<EntrantRequestGroup> getEntrantRequestGroups()
    {
        return _entrantRequestGroups;
    }

    public void setEntrantRequestGroups(List<EntrantRequestGroup> entrantRequestGroups)
    {
        _entrantRequestGroups = entrantRequestGroups;
    }

    public List<SubjectPassForm> getSubjectPassForms()
    {
        return _subjectPassForms;
    }

    public void setSubjectPassForms(List<SubjectPassForm> subjectPassForms)
    {
        _subjectPassForms = subjectPassForms;
    }

    public Map<CellCoordinates, Boolean> getMatrixData()
    {
        return _matrixData;
    }

    public void setMatrixData(Map<CellCoordinates, Boolean> matrixData)
    {
        _matrixData = matrixData;
    }
}
