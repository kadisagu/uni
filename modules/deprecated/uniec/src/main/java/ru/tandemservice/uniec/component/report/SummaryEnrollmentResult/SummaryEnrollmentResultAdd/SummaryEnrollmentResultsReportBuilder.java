/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.report.SummaryEnrollmentResult.SummaryEnrollmentResultAdd;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.NotUsedBenefit;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import org.tandemframework.shared.person.catalog.entity.Benefit;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.ProfileKnowledge;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.NotUsedTargetAdmissionKind;
import ru.tandemservice.uniec.util.EntrantDataUtil;

import java.util.*;

/**
 * @author Vasily Zhukov
 * @since 29.07.2010
 */
class SummaryEnrollmentResultsReportBuilder
{
    private Model _model;
    private Session _session;

    SummaryEnrollmentResultsReportBuilder(Model model, Session session)
    {
        _model = model;
        _session = session;
    }

    DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    private byte[] buildReport()
    {
        // загружаем местоположение ОУ
        AddressDetailed address = TopOrgUnit.getInstance().getAddress();
        AddressItem city = address == null ? null : address.getSettlement();
        AddressItem area = city;
        if (area != null)
            while (area.getParent() != null)
                area = area.getParent();

        MQBuilder builder = new MQBuilder(EnrollmentDirection.ENTITY_CLASS, "d");
        builder.add(MQExpression.eq("d", EnrollmentDirection.enrollmentCampaign().s(), _model.getEnrollmentCampaign()));
        builder.add(MQExpression.eq("d", EnrollmentDirection.educationOrgUnit().developForm().s(), _model.getReport().getDevelopForm()));
        if (_model.isDevelopConditionActive())
            builder.add(MQExpression.in("d", EnrollmentDirection.educationOrgUnit().developCondition().s(), _model.getDevelopConditionList()));
        if (_model.isQualificationActive())
            builder.add(MQExpression.in("d", EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().s(), _model.getQualificationList()));

        // получаем список строк отчета
        Map<EducationLevels, Set<SummaryReportRow>> reportRowList = SummaryReportUtil.getReportRowList(builder.<EnrollmentDirection>getResultList(_session), createReportRowFactory(), _model.getReport().getCompensationType());

        // считаем все цифры
        for (Set<SummaryReportRow> set : reportRowList.values())
            for (SummaryReportRow row : set)
                row.buildRow(city);

        // получаем список строк отчета
        String highSchoolTitle = TopOrgUnit.getInstance().getTitle();
        String periodTitle = "c " + DateFormatter.DEFAULT_DATE_FORMATTER.format(_model.getReport().getDateFrom()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(_model.getReport().getDateTo());
        List<String> paramsTitleList = new ArrayList<>();
        paramsTitleList.add("Форма обучения: " + _model.getReport().getDevelopForm().getTitle());
        if (_model.isDevelopConditionActive())
        {
            paramsTitleList.add("Условие обучения: " + UniStringUtils.join(_model.getDevelopConditionList(), DevelopCondition.title().s(), ", "));
        }
        paramsTitleList.add("Финансирование: " + _model.getReport().getCompensationType().getShortTitle());
        if (_model.isQualificationActive())
        {
            paramsTitleList.add("Квалификация: " + UniStringUtils.join(_model.getQualificationList(), Qualifications.title().s(), ", "));
        }
        if (_model.isStudentCategoryActive())
        {
            paramsTitleList.add("Категория поступающего: " + UniStringUtils.join(_model.getStudentCategoryList(), StudentCategory.title().s(), ", "));
        }
        String paramsTitle = org.apache.commons.lang.StringUtils.join(paramsTitleList.iterator(), "  ");

        // получаем бинарное представление excel-файла
        try
        {
            List<ProfileKnowledge> profileKnowledgeList = UniDaoFacade.getCoreDao().getCatalogItemList(ProfileKnowledge.class);
            return SummaryExcelContentBuilder.buildExcelContent(StringUtils.capitalize(_model.getEnrollmentCampaignStage().getTitle()), highSchoolTitle, area, city, periodTitle, paramsTitle, getBenefitList(), getTargetAdmissionList(), profileKnowledgeList, reportRowList, true, _model.getHideColumns());
        } catch (Exception e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private List<TargetAdmissionKind> getTargetAdmissionList()
    {
        MQBuilder taBuilder = new MQBuilder(TargetAdmissionKind.ENTITY_CLASS, "ta");
        MQBuilder notUsed = new MQBuilder(NotUsedTargetAdmissionKind.ENTITY_CLASS, "nu");
        notUsed.add(MQExpression.eqProperty("nu", NotUsedTargetAdmissionKind.targetAdmissionKind().id().s(), "ta", "id"));
        notUsed.add(MQExpression.eq("nu", NotUsedTargetAdmissionKind.enrollmentCampaign().s(), _model.getEnrollmentCampaign()));
        taBuilder.add(MQExpression.notExists(notUsed));
        taBuilder.add(MQExpression.notExists(new MQBuilder(TargetAdmissionKind.ENTITY_CLASS, "child").add(MQExpression.eqProperty("child", TargetAdmissionKind.parent().id().s(), "ta", "id"))));
        List<TargetAdmissionKind> targetAdmissionKindList = taBuilder.getResultList(_session);
        Collections.sort(targetAdmissionKindList, TargetAdmissionKind.ALL_LEVEL_PRIORITY_COMPARATOR);
        return targetAdmissionKindList;
    }

    private List<Benefit> getBenefitList()
    {
        MQBuilder benefitBuilder = new MQBuilder(Benefit.ENTITY_CLASS, "b");
        benefitBuilder.add(MQExpression.notIn("b", "id", new MQBuilder(NotUsedBenefit.ENTITY_CLASS, "n", new String[]{NotUsedBenefit.benefit().id().s()}).add(MQExpression.eq("n", NotUsedBenefit.personRoleName().s(), "Entrant"))));
        List<Benefit> benefitList = benefitBuilder.getResultList(_session);
        Collections.sort(benefitList, CommonCatalogUtil.CATALOG_CODE_COMPARATOR);
        return benefitList;
    }

    private MQBuilder getRequestedEnrollmentDirectionBuilder()
    {
        int stateId = _model.getEnrollmentCampaignStage().getId().intValue();

        MQBuilder builder;

        if (Model.ENROLLMENT_CAMP_STAGE_DOCUMENTS == stateId || Model.ENROLLMENT_CAMP_STAGE_EXAMS == stateId)
        {
            builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "r");
            builder.addJoinFetch("r", RequestedEnrollmentDirection.entrantRequest().s(), "request");
            builder.addJoinFetch("request", EntrantRequest.entrant().s(), "entrant");
            builder.addJoinFetch("entrant", Entrant.person().s(), "person");
            builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.enrollmentDirection().enrollmentCampaign().s(), _model.getEnrollmentCampaign()));
            builder.add(UniMQExpression.betweenDate("r", RequestedEnrollmentDirection.regDate().s(), _model.getReport().getDateFrom(), _model.getReport().getDateTo()));
            builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developForm().s(), _model.getReport().getDevelopForm()));
            builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.compensationType().s(), _model.getReport().getCompensationType()));
            if (_model.isDevelopConditionActive())
                builder.add(MQExpression.in("r", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developCondition().s(), _model.getDevelopConditionList()));
            if (_model.isQualificationActive())
                builder.add(MQExpression.in("r", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().s(), _model.getQualificationList()));
            if (_model.isStudentCategoryActive())
                builder.add(MQExpression.in("r", RequestedEnrollmentDirection.studentCategory().s(), _model.getStudentCategoryList()));
            builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.entrantRequest().entrant().archival().s(), false));
            builder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.state().code().s(), UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
            if (Model.ENROLLMENT_CAMP_STAGE_EXAMS == stateId)
                builder.add(MQExpression.notIn("r", RequestedEnrollmentDirection.state().code().s(), UniecDefines.ENTRANT_STATE_ACTIVE_CODE, UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE));
        } else if (Model.ENROLLMENT_CAMP_STAGE_ENROLLMENT == stateId)
        {
            builder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "p", new String[]{PreliminaryEnrollmentStudent.requestedEnrollmentDirection().s()});
            builder.addJoin("p", PreliminaryEnrollmentStudent.requestedEnrollmentDirection().s(), "r");
            builder.addJoin("r", RequestedEnrollmentDirection.entrantRequest().s(), "request");
            builder.addJoin("request", EntrantRequest.entrant().s(), "entrant");
            builder.addJoin("entrant", Entrant.person().s(), "person");
            builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.requestedEnrollmentDirection().enrollmentDirection().enrollmentCampaign().s(), _model.getEnrollmentCampaign()));
            builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.educationOrgUnit().developForm().s(), _model.getReport().getDevelopForm()));
            builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.compensationType().s(), _model.getReport().getCompensationType()));
            builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.requestedEnrollmentDirection().entrantRequest().entrant().archival().s(), Boolean.FALSE));
            builder.add(UniMQExpression.betweenDate("r", RequestedEnrollmentDirection.regDate().s(), _model.getReport().getDateFrom(), _model.getReport().getDateTo()));
            if (_model.isDevelopConditionActive())
                builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.educationOrgUnit().developCondition().s(), _model.getDevelopConditionList()));
            if (_model.isQualificationActive())
                builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().s(), _model.getQualificationList()));
            if (_model.isStudentCategoryActive())
                builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.studentCategory().s(), _model.getStudentCategoryList()));
            if (_model.isParallelActive() && _model.getParallel().isTrue())
                builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.parallel().s(), Boolean.FALSE));
        } else
            throw new RuntimeException("Unknown EnrollmentCampaignStage: " + _model.getEnrollmentCampaignStage().getTitle());

        return builder;
    }

    private Map<Person, Set<Student>> getPerson2StudentMap(MQBuilder builder)
    {
        Map<Person, Set<Student>> person2studentMap = new HashMap<>();

        MQBuilder b = new MQBuilder(Student.ENTITY_CLASS, "__student");
        b.addDomain("__requestedDirection", RequestedEnrollmentDirection.ENTITY_CLASS);
        b.add(MQExpression.eqProperty("__student", Student.L_PERSON, "__requestedDirection", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_PERSON));
        b.add(MQExpression.in("__requestedDirection", "id", builder));
        b.add(MQExpression.eq("__student", Student.archival().s(), Boolean.FALSE));
        b.add(MQExpression.eq("__student", Student.status().active().s(), Boolean.TRUE));
        b.add(MQExpression.in("__student", Student.educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().code().s(), Arrays.asList(
                QualificationsCodes.MAGISTR,      // "Магистр" 68
                QualificationsCodes.BAKALAVR,    // "Бакалавр" 62
                QualificationsCodes.SPETSIALIST,    // "Специалист" 65
                QualificationsCodes.BAZOVYY_UROVEN_S_P_O, // "Базовый уровень СПО" 51
                QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O, // "Повышенный уровень СПО" 52
                QualificationsCodes.N_P_O_2,     // "НПО 2" 42
                QualificationsCodes.N_P_O_3,     // "НПО 3" 43
                QualificationsCodes.N_P_O_4      // "НПО 4" 44

        )));
        for (Student student : b.<Student>getResultList(_session))
        {
            Set<Student> set = person2studentMap.get(student.getPerson());
            if (set == null)
                person2studentMap.put(student.getPerson(), set = new HashSet<>());
            set.add(student);
        }

        return person2studentMap;
    }

    private ISummaryReportRowFactory createReportRowFactory()
    {
        MQBuilder requestedDirectionsBuilder = getRequestedEnrollmentDirectionBuilder();
        final EntrantDataUtil util = new EntrantDataUtil(_session, _model.getEnrollmentCampaign(), requestedDirectionsBuilder);

        // персона абитуриента -> список льгот
        final Map<Person, Set<PersonBenefit>> person2Benefits = EntrantDataUtil.getPersonBenefitMap(_session, requestedDirectionsBuilder);

        // выбранное направление приема -> список профильных знаний
        final Map<RequestedEnrollmentDirection, Set<RequestedProfileKnowledge>> direction2Knowledges = EntrantDataUtil.getProfileKnowledgeMap(_session, requestedDirectionsBuilder);

        // абитуриент -> список дипломов олимпиад
        final Map<Entrant, Set<OlympiadDiploma>> entrant2diplomas = EntrantDataUtil.getOlympiadDiplomaMap(_session, requestedDirectionsBuilder);

        // персона -> студенты
        final Map<Person, Set<Student>> person2students = getPerson2StudentMap(requestedDirectionsBuilder);

        // абитуриент -> рекомендации к зачислению
        final Map<Entrant, Set<EntrantEnrolmentRecommendation>> entrant2EnrollmentRecomendations = EntrantDataUtil.getEnrollmentRecomendationMap(_session, requestedDirectionsBuilder);

        if (Model.ENROLLMENT_CAMP_STAGE_ENROLLMENT == _model.getEnrollmentCampaignStage().getId())
        {
            // направление подготовки -> направление приема
            final Map<EducationOrgUnit, EnrollmentDirection> educationOrgUnit2Direction = new HashMap<>();
            {
                MQBuilder builder = new MQBuilder(EnrollmentDirection.ENTITY_CLASS, "d");
                builder.add(MQExpression.eq("d", EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, _model.getEnrollmentCampaign()));

                for (EnrollmentDirection enrollmentDirection : builder.<EnrollmentDirection>getResultList(_session))
                {
                    educationOrgUnit2Direction.put(enrollmentDirection.getEducationOrgUnit(), enrollmentDirection);
                }
            }

            // направление приема -> список студентов пред. зачисления удовл. фильтрам
            final Map<EnrollmentDirection, Set<PreliminaryEnrollmentStudent>> direction2PreliminaryEnrollmentStudents = new HashMap<>();
            {
                MQBuilder builder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "preliminaryEnrollmentStudent");
                builder.add(MQExpression.in("preliminaryEnrollmentStudent", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, requestedDirectionsBuilder));
                for (PreliminaryEnrollmentStudent preStudent : builder.<PreliminaryEnrollmentStudent>getResultList(_session))
                {
                    EnrollmentDirection enrollmentDirection = educationOrgUnit2Direction.get(preStudent.getEducationOrgUnit());
                    if (enrollmentDirection != null)
                    {
                        Set<PreliminaryEnrollmentStudent> set = direction2PreliminaryEnrollmentStudents.get(enrollmentDirection);
                        if (set == null)
                        {
                            direction2PreliminaryEnrollmentStudents.put(enrollmentDirection, set = new HashSet<>());
                        }
                        set.add(preStudent);
                    }
                }
            }

            return new ISummaryReportRowFactory()
            {
                @Override
                public List<ISummaryRowItem> createSummaryReportItem(EnrollmentDirection enrollmentDirection)
                {
                    List<ISummaryRowItem> result = new ArrayList<>();
                    Set<PreliminaryEnrollmentStudent> directionSet = direction2PreliminaryEnrollmentStudents.get(enrollmentDirection);
                    if (directionSet != null)
                    {
                        for (PreliminaryEnrollmentStudent preStudent : directionSet)
                        {
                            SummaryRowItem item = new SummaryRowItem();
                            item.setEnrollmentDirection(enrollmentDirection);
                            item.setDirection(preStudent.getRequestedEnrollmentDirection());
                            item.setEntrantRequest(preStudent.getRequestedEnrollmentDirection().getEntrantRequest());
                            item.setEntrant(item.getEntrantRequest().getEntrant());
                            item.setLastPersonEduInstitution(item.getEntrant().getPerson().getPersonEduInstitution());
                            item.setChosenEntranceDisciplineSet(util.getChosenEntranceDisciplineSet(preStudent.getRequestedEnrollmentDirection()));
                            item.setBenefitSet(UniBaseUtils.<String>getPropertiesSet(person2Benefits.get(item.getEntrant().getPerson()), PersonBenefit.benefit().code().s()));
                            item.setHasOlympiadDiplomas(entrant2diplomas.containsKey(item.getEntrant()));
                            item.setHasStudents(person2students.containsKey(item.getEntrant().getPerson()));
                            item.setHasRecommendations(entrant2EnrollmentRecomendations.containsKey(item.getEntrant()));
                            item.setHasProfileKnowledges(direction2Knowledges.containsKey(preStudent.getRequestedEnrollmentDirection()));
                            item.setTargetAdmission(preStudent.isTargetAdmission());
                            item.setTargetAdmissionKind(preStudent.getRequestedEnrollmentDirection().getTargetAdmissionKind() == null ? null : preStudent.getRequestedEnrollmentDirection().getTargetAdmissionKind().getCode());
                            item.setCompetitionKind(preStudent.getRequestedEnrollmentDirection().getCompetitionKind().getCode());
                            item.setStudentCategory(preStudent.getStudentCategory().getCode());
                            item.setHasWorkExperience((preStudent.getRequestedEnrollmentDirection().getProfileWorkExperienceDays() != null && preStudent.getRequestedEnrollmentDirection().getProfileWorkExperienceDays() > 0) || (preStudent.getRequestedEnrollmentDirection().getProfileWorkExperienceMonths() != null && preStudent.getRequestedEnrollmentDirection().getProfileWorkExperienceMonths() > 0) || (preStudent.getRequestedEnrollmentDirection().getProfileWorkExperienceYears() != null && preStudent.getRequestedEnrollmentDirection().getProfileWorkExperienceYears() > 0));
                            result.add(item);
                        }
                    }
                    return result;
                }
            };
        } else
        {
            return new ISummaryReportRowFactory()
            {
                @Override
                public List<ISummaryRowItem> createSummaryReportItem(EnrollmentDirection enrollmentDirection)
                {
                    List<ISummaryRowItem> result = new ArrayList<>();
                    Set<RequestedEnrollmentDirection> directionSet = util.getDirectionSet(enrollmentDirection);
                    if (directionSet != null)
                    {
                        for (RequestedEnrollmentDirection direction : directionSet)
                        {
                            SummaryRowItem item = new SummaryRowItem();
                            item.setEnrollmentDirection(enrollmentDirection);
                            item.setDirection(direction);
                            item.setEntrantRequest(direction.getEntrantRequest());
                            item.setEntrant(item.getEntrantRequest().getEntrant());
                            item.setLastPersonEduInstitution(item.getEntrant().getPerson().getPersonEduInstitution());
                            item.setChosenEntranceDisciplineSet(util.getChosenEntranceDisciplineSet(direction));
                            item.setBenefitSet(UniBaseUtils.<String>getPropertiesSet(person2Benefits.get(item.getEntrant().getPerson()), PersonBenefit.benefit().code().s()));
                            item.setHasOlympiadDiplomas(entrant2diplomas.containsKey(item.getEntrant()));
                            item.setHasStudents(person2students.containsKey(item.getEntrant().getPerson()));
                            item.setHasRecommendations(entrant2EnrollmentRecomendations.containsKey(item.getEntrant()));
                            item.setHasProfileKnowledges(direction2Knowledges.containsKey(direction));
                            item.setTargetAdmission(direction.isTargetAdmission());
                            item.setTargetAdmissionKind(direction.getTargetAdmissionKind() == null ? null : direction.getTargetAdmissionKind().getCode());
                            item.setCompetitionKind(direction.getCompetitionKind().getCode());
                            item.setStudentCategory(direction.getStudentCategory().getCode());
                            item.setHasWorkExperience((direction.getProfileWorkExperienceDays() != null && direction.getProfileWorkExperienceDays() > 0) || (direction.getProfileWorkExperienceMonths() != null && direction.getProfileWorkExperienceMonths() > 0) || (direction.getProfileWorkExperienceYears() != null && direction.getProfileWorkExperienceYears() > 0));
                            result.add(item);
                        }
                    }
                    return result;
                }
            };
        }
    }
}
