package ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.block.preliminaryData;

import java.util.Arrays;
import java.util.List;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.dsl.EntityPath;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

/**
 * @author Vasily Zhukov
 * @since 08.02.2011
 */
public class PreliminaryDataBlock
{
    // names
    public static final String QUALIFICATION_PRE_DS = "qualificationPreDS";
    public static final String FORMATIVE_ORGUNIT_PRE_DS = "formativeOrgUnitPreDS";
    public static final String TERRITORIAL_ORGUNIT_PRE_DS = "territorialOrgUnitPreDS";
    public static final String EDUCATION_ORGUNIT_PRE_DS = "educationOrgUnitPreDS";
    public static final String DEVELOP_FORM_PRE_DS = "developFormPreDS";
    public static final String DEVELOP_CONDITION_PRE_DS = "developConditionPreDS";
    public static final String DEVELOP_TECH_PRE_DS = "developTechPreDS";
    public static final String DEVELOP_PERIOD_PRE_DS = "developPeriodPreDS";
    public static final String COMPENSATION_TYPE_PRE_DS = "compensationTypePreDS";
    public static final String STUDENT_CATEGORY_PRE_DS = "studentCategoryPreDS";
    public static final String TARGET_ADMISSION_PRE_DS = "targetAdmissionPreDS";
    public static final String PARALLEL_PRE_DS = "parallelPreDS";

    // datasource parameter names
    public static final String PARAM_FORMATIVE_ORG_UNIT = "formativeOrgUnitList";
    public static final String PARAM_TERRITORIAL_ORG_UNIT = "territorialOrgUnitList";

    // ids
    public static final Long TARGET_ADMISSION_YES = 0L;
    public static final Long TARGET_ADMISSION_NO = 1L;
    public static final Long PARALLEL_YES = 0L;
    public static final Long PARALLEL_NO = 1L;

    public static void onBeforeDataSourceFetch(IUIDataSource dataSource, PreliminaryDataParam param)
    {
        String name = dataSource.getName();

        if (EDUCATION_ORGUNIT_PRE_DS.equals(name))
        {
            param.getFormativeOrgUnitPre().putParamIfActive(dataSource, PARAM_FORMATIVE_ORG_UNIT);
            param.getTerritorialOrgUnitPre().putParamIfActive(dataSource, PARAM_TERRITORIAL_ORG_UNIT);
        }
    }

    public static IDefaultComboDataSourceHandler createQualificationPreDS(String name)
    {
        return (IDefaultComboDataSourceHandler) createPathDS(name, Qualifications.class, EducationOrgUnit.educationLevelHighSchool().educationLevel().qualification()).setOrderByProperty(Qualifications.P_ID);
    }

    public static IDefaultComboDataSourceHandler createFormativeOrgUnitPreDS(String name)
    {
        return createPathDS(name, OrgUnit.class, EducationOrgUnit.formativeOrgUnit());
    }

    public static IDefaultComboDataSourceHandler createTerritorialOrgUnitPreDS(String name)
    {
        return createPathDS(name, OrgUnit.class, EducationOrgUnit.territorialOrgUnit());
    }

    public static IDefaultComboDataSourceHandler createEducationOrgUnitPreDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, EducationLevelsHighSchool.class)
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);

                List<OrgUnit> formativeOrgUnitList = ep.context.get(PARAM_FORMATIVE_ORG_UNIT);
                List<OrgUnit> territorialOrgUnitList = ep.context.get(PARAM_TERRITORIAL_ORG_UNIT);

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "ou")
                        .column(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().id().fromAlias("ou")));

                if (formativeOrgUnitList != null)
                    builder.where(DQLExpressions.in(DQLExpressions.property(EducationOrgUnit.formativeOrgUnit().fromAlias("ou")), formativeOrgUnitList));

                if (territorialOrgUnitList != null)
                    builder.where(DQLExpressions.in(DQLExpressions.property(EducationOrgUnit.territorialOrgUnit().fromAlias("ou")), territorialOrgUnitList));

                ep.dqlBuilder.where(DQLExpressions.in(DQLExpressions.property("e.id"),
                        builder.buildQuery()
                ));
            }
        };
    }

    public static IDefaultComboDataSourceHandler createCompensationTypePreDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, CompensationType.class, CompensationType.shortTitle());
    }

    public static IDefaultComboDataSourceHandler createStudentCategoryPreDS(String name)
    {
        return (IDefaultComboDataSourceHandler) new DefaultComboDataSourceHandler(name, StudentCategory.class).setOrderByProperty(StudentCategory.code().s());
    }

    public static IDefaultComboDataSourceHandler createTargetAdmissionPreDS(String name)
    {
        return new SimpleTitledComboDataSourceHandler(name)
                .addAll(Arrays.asList(new IdentifiableWrapper(TARGET_ADMISSION_YES, "да"), new IdentifiableWrapper(TARGET_ADMISSION_NO, "нет")))
                .filtered(true);
    }

    public static IDefaultComboDataSourceHandler createParallelPreDS(String name)
    {
        return new SimpleTitledComboDataSourceHandler(name)
                .addAll(Arrays.asList(new IdentifiableWrapper(PARALLEL_YES, "да"), new IdentifiableWrapper(PARALLEL_NO, "нет")))
                .filtered(true);
    }

    // private

    public static DefaultComboDataSourceHandler createPathDS(String name, Class<? extends IEntity> clazz, final EntityPath path)
    {
        return new DefaultComboDataSourceHandler(name, clazz)
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);

                ep.dqlBuilder.where(DQLExpressions.in(DQLExpressions.property("e.id"), new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "ou")
                        .column(DQLExpressions.property(path.id().fromAlias("ou")))
                        .buildQuery()
                ));
            }
        };
    }
}
