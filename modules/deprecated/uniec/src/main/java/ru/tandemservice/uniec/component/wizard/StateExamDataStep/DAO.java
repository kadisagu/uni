/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.wizard.StateExamDataStep;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.list.column.BlockColumn;

import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.component.entrant.EntrantStateExamCertificateAddEdit.*;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.StateExamType;
import ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;
import ru.tandemservice.uniec.ui.EntrantStateExamCertificateNumberFormatter;

/**
 * @author vip_delete
 * @since 16.06.2009
 */
public class DAO extends ru.tandemservice.uniec.component.entrant.EntrantStateExamCertificateAddEdit.DAO implements IDAO
{
    @Override
    @SuppressWarnings("unchecked")
    public void prepare(ru.tandemservice.uniec.component.entrant.EntrantStateExamCertificateAddEdit.Model m)
    {
        super.prepare(m);

        Model model = (Model) m;

        // копируем данные из онлайн-абитуриента, если он есть
        Long onlineEntrantId = model.getOnlineEntrant().getId();
        if (onlineEntrantId != null)
        {
            OnlineEntrant onlineEntrant = get(OnlineEntrant.class, onlineEntrantId);
            model.setOnlineEntrant(onlineEntrant);

            // данные сертификата
            EntrantStateExamCertificate certificate = m.getCertificate();
            certificate.setIssuanceDate(onlineEntrant.getCertificateDate());
            certificate.setStateExamType(onlineEntrant.getCertificatePlace() == null?getByCode(StateExamType.class, UniecDefines.STATE_EXAM_TYPE_FIRST) : onlineEntrant.getCertificatePlace());

            // номер
            String certificateNumber = onlineEntrant.getCertificateNumber();
            if (StringUtils.isNotEmpty(certificateNumber))
            {
                String[] numberParts = EntrantStateExamCertificateNumberFormatter.split(certificateNumber.replace("-", ""));
                model.setTerritoryCode(numberParts[0]);
                model.setNumber(numberParts[1]);
                model.setYear(numberParts[2]);
            }

            // оценки
            Map<Long, Long> markMap = new HashMap<>();
            Map<Long, Long> yearMap = new HashMap<>();
            UniecDAOFacade.getOnlineEntrantRegistrationDAO().getCertificateMarks(onlineEntrant).forEach(
                    certificateMark -> {
                        markMap.put(certificateMark.getSubject().getId(), (long) certificateMark.getMark());
                        if (certificateMark.getYear() != null)
                            yearMap.put(certificateMark.getSubject().getId(), (long) certificateMark.getYear());
                    });
            ((BlockColumn) model.getDataSource().getColumn(Controller.MARK_COLUMN_NAME)).setValueMap(markMap);
            ((BlockColumn) model.getDataSource().getColumn(Controller.YEAR_COLUMN_NAME)).setValueMap(yearMap);
        }
    }
}
