/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.settings.PassDisciplineDates;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.entrant.DisciplineDateSetting;
import ru.tandemservice.uniec.entity.entrant.EntranceExamPhase;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationFormRelation;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vip_delete
 * @since 08.06.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.setSubjectPassFormList(getList(SubjectPassForm.class, SubjectPassForm.P_INTERNAL, Boolean.TRUE, SubjectPassForm.CATALOG_ITEM_CODE));
        model.setCompensationTypeList(getList(CompensationType.class, CompensationType.title().s()));
        model.setDevelopFormList(new DQLSelectBuilder()
                                         .fromEntity(DevelopForm.class, "e")
                                         .column(property("e"))
                                         .where(isNull(property("e", DevelopForm.disabledDate())))
                                         .order(property("e", DevelopForm.title()))
                                         .createStatement(getSession()).list());

        prepareDates(model);
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public void prepareListDataSource(Model model)
    {
        String alias = "dis";
        List<Discipline2RealizationWayRelation> list = new DQLSelectBuilder()
                .fromEntity(Discipline2RealizationWayRelation.class, alias)
                .column(property(alias))
                .where(eq(property(alias, Discipline2RealizationWayRelation.enrollmentCampaign()), value(model.getEnrollmentCampaign())))
                .where(existsByExpr(Discipline2RealizationFormRelation.class, "form", and(
                        eq(property("form", Discipline2RealizationFormRelation.discipline()), property(alias)),
                        eq(property("form", Discipline2RealizationFormRelation.subjectPassForm()), value(model.getSubjectPassForm())),
                        eq(property("form", Discipline2RealizationFormRelation.type()), value(model.getCompensationType()))
                )))
                .order(property(alias, Discipline2RealizationWayRelation.educationSubject().title()))
                .createStatement(getSession()).list();

        model.getDataSource().setCountRow(list.size());
        UniBaseUtils.createPage(model.getDataSource(), list);
    }

    public void prepareDates(Model model)
    {
        String stAlias = "st";
        model.setDateSettingByDiscipline(
                new DQLSelectBuilder()
                        .fromEntity(DisciplineDateSetting.class, stAlias)
                        .column(property(stAlias))
                        .where(eq(property(stAlias, DisciplineDateSetting.discipline().enrollmentCampaign()), value(model.getEnrollmentCampaign())))
                        .where(eq(property(stAlias, DisciplineDateSetting.subjectPassForm()), value(model.getSubjectPassForm())))
                        .where(eq(property(stAlias, DisciplineDateSetting.compensationType()), value(model.getCompensationType())))
                        .where(eq(property(stAlias, DisciplineDateSetting.developForm()), value(model.getDevelopForm())))
                        .createStatement(getSession()).<DisciplineDateSetting>list()
                        .stream()
                        .collect(Collectors.toMap(setting -> setting.getDiscipline().getId(), setting -> setting))
        );


        String phAlias = "phase";
        model.setPhaseByDiscipline(
                new DQLSelectBuilder()
                        .fromEntity(EntranceExamPhase.class, phAlias)
                        .column(property(phAlias))
                        .where(eq(property(phAlias, EntranceExamPhase.setting().discipline().enrollmentCampaign()), value(model.getEnrollmentCampaign())))
                        .where(eq(property(phAlias, EntranceExamPhase.setting().subjectPassForm()), value(model.getSubjectPassForm())))
                        .where(eq(property(phAlias, EntranceExamPhase.setting().compensationType()), value(model.getCompensationType())))
                        .where(eq(property(phAlias, EntranceExamPhase.setting().developForm()), value(model.getDevelopForm())))
                        .order(property(phAlias, EntranceExamPhase.passDate()))
                        .createStatement(getSession()).<EntranceExamPhase>list()
                        .stream()
                        .collect(Collectors.groupingBy(phase -> phase.getSetting().getDiscipline().getId(), Collectors.mapping(phase -> phase, Collectors.toList())))
        );
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public void update(Model model)
    {
        MQBuilder builder = new MQBuilder(DisciplineDateSetting.ENTITY_CLASS, "d");
        builder.add(MQExpression.eq("d", DisciplineDateSetting.L_DISCIPLINE + "." + Discipline2RealizationWayRelation.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
        builder.add(MQExpression.eq("d", DisciplineDateSetting.L_SUBJECT_PASS_FORM, model.getSubjectPassForm()));
        List<DisciplineDateSetting> list = builder.getResultList(getSession());

        Map<Long, DisciplineDateSetting> map = new HashMap<>();
        for (DisciplineDateSetting settingItem : list)
            map.put(settingItem.getDiscipline().getId(), settingItem);

        Map<Long, Date> valueMap = ((BlockColumn) model.getDataSource().getColumn("passDate")).getValueMap();
        for (Map.Entry<Long, Date> entry : valueMap.entrySet())
        {
            DisciplineDateSetting setting = map.get(entry.getKey());
            Date date = entry.getValue();
            if (date == null)
            {
                if (setting != null)
                    getSession().delete(setting);
            }
            else
            {
                if (setting == null)
                {
                    setting = new DisciplineDateSetting();
                    setting.setDiscipline(getNotNull(Discipline2RealizationWayRelation.class, entry.getKey()));
                    setting.setSubjectPassForm(model.getSubjectPassForm());
                }
//                setting.setPassDate(date);
                getSession().saveOrUpdate(setting);
            }
        }
    }

    @Override
    public void update(Long discipline, Model model)
    {
        List<EntranceExamPhase> entranceExamPhases = model.getPhaseByDiscipline().get(discipline);
        DisciplineDateSetting dateSetting = model.getDateSettingByDiscipline()
                .computeIfAbsent(discipline, k -> {
                    DisciplineDateSetting newSetting = new DisciplineDateSetting();
                    newSetting.setDiscipline(get(Discipline2RealizationWayRelation.class, discipline));
                    newSetting.setSubjectPassForm(model.getSubjectPassForm());
                    newSetting.setCompensationType(model.getCompensationType());
                    newSetting.setDevelopForm(model.getDevelopForm());
                    save(newSetting);
                    return newSetting;
                });

        List<EntranceExamPhase> freshPhases = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(entranceExamPhases))
        {
            for (EntranceExamPhase phase : entranceExamPhases)
            {
                if (phase.getId() == null)
                {
                    if (phase.getPassDate() == null) continue;

                    phase.setSetting(dateSetting);
                }
                else if (phase.getPassDate() == null)
                {
                    delete(phase);
                    continue;
                }

                saveOrUpdate(phase);
                freshPhases.add(phase);
            }
        }
        freshPhases.sort(Comparator.comparing(EntranceExamPhase::getPassDate));
        model.getPhaseByDiscipline().put(discipline, freshPhases);

        List<EntranceExamPhase> phases4Delete = model.getPhases4Delete();
        if (phases4Delete.isEmpty()) return;

        phases4Delete.forEach(this::delete);
        phases4Delete.clear();
    }

    @Override
    public void refreshPhases(Long discipline, Model model)
    {
        String phAlias = "phase";
        List<EntranceExamPhase> phases = new DQLSelectBuilder()
                .fromEntity(EntranceExamPhase.class, phAlias)
                .column(property(phAlias))
                .where(eq(property(phAlias, EntranceExamPhase.setting().discipline().id()), value(discipline)))
                .where(eq(property(phAlias, EntranceExamPhase.setting().subjectPassForm()), value(model.getSubjectPassForm())))
                .where(eq(property(phAlias, EntranceExamPhase.setting().compensationType()), value(model.getCompensationType())))
                .where(eq(property(phAlias, EntranceExamPhase.setting().developForm()), value(model.getDevelopForm())))
                .order(property(phAlias, EntranceExamPhase.passDate()))
                .createStatement(getSession()).list();

        phases.forEach(this::refresh);

        model.getPhaseByDiscipline().put(discipline, phases);
        model.getPhases4Delete().clear();
    }
}
