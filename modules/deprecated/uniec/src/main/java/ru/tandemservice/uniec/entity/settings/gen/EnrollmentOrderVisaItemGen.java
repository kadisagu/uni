package ru.tandemservice.uniec.entity.settings.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.settings.EnrollmentOrderVisaItem;
import ru.tandemservice.unimv.IPossibleVisa;
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising;
import ru.tandemservice.unimv.entity.visa.gen.IPossibleVisaGen;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Виза для приказа о зачислении
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrollmentOrderVisaItemGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.settings.EnrollmentOrderVisaItem";
    public static final String ENTITY_NAME = "enrollmentOrderVisaItem";
    public static final int VERSION_HASH = 1651346336;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_POSSIBLE_VISA = "possibleVisa";
    public static final String P_TITLE = "title";
    public static final String P_PRIORITY = "priority";
    public static final String L_GROUPS_MEMBER_VISING = "groupsMemberVising";

    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private IPossibleVisa _possibleVisa;     // Возможная виза
    private String _title;     // Название группы виз
    private int _priority;     // Приоритет
    private GroupsMemberVising _groupsMemberVising;     // Группа участников визирования

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Возможная виза. Свойство не может быть null.
     */
    @NotNull
    public IPossibleVisa getPossibleVisa()
    {
        return _possibleVisa;
    }

    /**
     * @param possibleVisa Возможная виза. Свойство не может быть null.
     */
    public void setPossibleVisa(IPossibleVisa possibleVisa)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && possibleVisa!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IPossibleVisa.class);
            IEntityMeta actual =  possibleVisa instanceof IEntity ? EntityRuntime.getMeta((IEntity) possibleVisa) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_possibleVisa, possibleVisa);
        _possibleVisa = possibleVisa;
    }

    /**
     * @return Название группы виз. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название группы виз. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Приоритет. Свойство не может быть null.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    /**
     * @return Группа участников визирования. Свойство не может быть null.
     */
    @NotNull
    public GroupsMemberVising getGroupsMemberVising()
    {
        return _groupsMemberVising;
    }

    /**
     * @param groupsMemberVising Группа участников визирования. Свойство не может быть null.
     */
    public void setGroupsMemberVising(GroupsMemberVising groupsMemberVising)
    {
        dirty(_groupsMemberVising, groupsMemberVising);
        _groupsMemberVising = groupsMemberVising;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrollmentOrderVisaItemGen)
        {
            setEnrollmentCampaign(((EnrollmentOrderVisaItem)another).getEnrollmentCampaign());
            setPossibleVisa(((EnrollmentOrderVisaItem)another).getPossibleVisa());
            setTitle(((EnrollmentOrderVisaItem)another).getTitle());
            setPriority(((EnrollmentOrderVisaItem)another).getPriority());
            setGroupsMemberVising(((EnrollmentOrderVisaItem)another).getGroupsMemberVising());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrollmentOrderVisaItemGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrollmentOrderVisaItem.class;
        }

        public T newInstance()
        {
            return (T) new EnrollmentOrderVisaItem();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "possibleVisa":
                    return obj.getPossibleVisa();
                case "title":
                    return obj.getTitle();
                case "priority":
                    return obj.getPriority();
                case "groupsMemberVising":
                    return obj.getGroupsMemberVising();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "possibleVisa":
                    obj.setPossibleVisa((IPossibleVisa) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
                case "groupsMemberVising":
                    obj.setGroupsMemberVising((GroupsMemberVising) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "possibleVisa":
                        return true;
                case "title":
                        return true;
                case "priority":
                        return true;
                case "groupsMemberVising":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "possibleVisa":
                    return true;
                case "title":
                    return true;
                case "priority":
                    return true;
                case "groupsMemberVising":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "possibleVisa":
                    return IPossibleVisa.class;
                case "title":
                    return String.class;
                case "priority":
                    return Integer.class;
                case "groupsMemberVising":
                    return GroupsMemberVising.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrollmentOrderVisaItem> _dslPath = new Path<EnrollmentOrderVisaItem>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrollmentOrderVisaItem");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.EnrollmentOrderVisaItem#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Возможная виза. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.EnrollmentOrderVisaItem#getPossibleVisa()
     */
    public static IPossibleVisaGen.Path<IPossibleVisa> possibleVisa()
    {
        return _dslPath.possibleVisa();
    }

    /**
     * @return Название группы виз. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.EnrollmentOrderVisaItem#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.EnrollmentOrderVisaItem#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    /**
     * @return Группа участников визирования. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.EnrollmentOrderVisaItem#getGroupsMemberVising()
     */
    public static GroupsMemberVising.Path<GroupsMemberVising> groupsMemberVising()
    {
        return _dslPath.groupsMemberVising();
    }

    public static class Path<E extends EnrollmentOrderVisaItem> extends EntityPath<E>
    {
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private IPossibleVisaGen.Path<IPossibleVisa> _possibleVisa;
        private PropertyPath<String> _title;
        private PropertyPath<Integer> _priority;
        private GroupsMemberVising.Path<GroupsMemberVising> _groupsMemberVising;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.EnrollmentOrderVisaItem#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Возможная виза. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.EnrollmentOrderVisaItem#getPossibleVisa()
     */
        public IPossibleVisaGen.Path<IPossibleVisa> possibleVisa()
        {
            if(_possibleVisa == null )
                _possibleVisa = new IPossibleVisaGen.Path<IPossibleVisa>(L_POSSIBLE_VISA, this);
            return _possibleVisa;
        }

    /**
     * @return Название группы виз. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.EnrollmentOrderVisaItem#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EnrollmentOrderVisaItemGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.EnrollmentOrderVisaItem#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(EnrollmentOrderVisaItemGen.P_PRIORITY, this);
            return _priority;
        }

    /**
     * @return Группа участников визирования. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.EnrollmentOrderVisaItem#getGroupsMemberVising()
     */
        public GroupsMemberVising.Path<GroupsMemberVising> groupsMemberVising()
        {
            if(_groupsMemberVising == null )
                _groupsMemberVising = new GroupsMemberVising.Path<GroupsMemberVising>(L_GROUPS_MEMBER_VISING, this);
            return _groupsMemberVising;
        }

        public Class getEntityClass()
        {
            return EnrollmentOrderVisaItem.class;
        }

        public String getEntityName()
        {
            return "enrollmentOrderVisaItem";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
