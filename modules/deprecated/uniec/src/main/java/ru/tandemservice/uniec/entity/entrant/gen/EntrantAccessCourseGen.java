package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.catalog.AccessCourse;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantAccessCourse;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Подготовительный курс абитуриента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntrantAccessCourseGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.EntrantAccessCourse";
    public static final String ENTITY_NAME = "entrantAccessCourse";
    public static final int VERSION_HASH = -2084613518;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT = "entrant";
    public static final String L_COURSE = "course";

    private Entrant _entrant;     // (Старый) Абитуриент
    private AccessCourse _course;     // Курс

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return (Старый) Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public Entrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant (Старый) Абитуриент. Свойство не может быть null.
     */
    public void setEntrant(Entrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public AccessCourse getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(AccessCourse course)
    {
        dirty(_course, course);
        _course = course;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntrantAccessCourseGen)
        {
            setEntrant(((EntrantAccessCourse)another).getEntrant());
            setCourse(((EntrantAccessCourse)another).getCourse());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntrantAccessCourseGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntrantAccessCourse.class;
        }

        public T newInstance()
        {
            return (T) new EntrantAccessCourse();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entrant":
                    return obj.getEntrant();
                case "course":
                    return obj.getCourse();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entrant":
                    obj.setEntrant((Entrant) value);
                    return;
                case "course":
                    obj.setCourse((AccessCourse) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entrant":
                        return true;
                case "course":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entrant":
                    return true;
                case "course":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entrant":
                    return Entrant.class;
                case "course":
                    return AccessCourse.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntrantAccessCourse> _dslPath = new Path<EntrantAccessCourse>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntrantAccessCourse");
    }
            

    /**
     * @return (Старый) Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantAccessCourse#getEntrant()
     */
    public static Entrant.Path<Entrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantAccessCourse#getCourse()
     */
    public static AccessCourse.Path<AccessCourse> course()
    {
        return _dslPath.course();
    }

    public static class Path<E extends EntrantAccessCourse> extends EntityPath<E>
    {
        private Entrant.Path<Entrant> _entrant;
        private AccessCourse.Path<AccessCourse> _course;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return (Старый) Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantAccessCourse#getEntrant()
     */
        public Entrant.Path<Entrant> entrant()
        {
            if(_entrant == null )
                _entrant = new Entrant.Path<Entrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantAccessCourse#getCourse()
     */
        public AccessCourse.Path<AccessCourse> course()
        {
            if(_course == null )
                _course = new AccessCourse.Path<AccessCourse>(L_COURSE, this);
            return _course;
        }

        public Class getEntityClass()
        {
            return EntrantAccessCourse.class;
        }

        public String getEntityName()
        {
            return "entrantAccessCourse";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
