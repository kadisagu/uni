/* $Id: Model.java 12652 2010-05-05 12:00:19Z oleyba $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.stateExamEnrollmentResults2010.StateExamEnrollmentResultsAdd;

import java.util.List;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import org.tandemframework.shared.commonbase.utils.CommonYesNoUIObject;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010;

/**
 * @author vip_delete
 * @since 20.04.2009
 */
public class Model implements IEnrollmentCampaignSelectModel
{
    public static final long PRINT_FORM_HIGH = 0L;
    public static final long PRINT_FORM_MIDDLE = 1L;

    public static final long ENROLLMENT_CAMP_STAGE_DOCUMENTS = 0L;
    public static final long ENROLLMENT_CAMP_STAGE_EXAMS = 1L;
    public static final long ENROLLMENT_CAMP_STAGE_ENROLLMENT = 2L;

    public static final long REPORT_ALGORITHM_EXAM = 0L;
    public static final long REPORT_ALGORITHM_STATE_EXAM = 1L;

    public static final long STATE_EXAM_PERIOD_WAVE_1 = 0L;
    public static final long STATE_EXAM_PERIOD_WAVE_2 = 1L;

    // Report Instance

    private StateExamEnrollmentReport2010 _report = new StateExamEnrollmentReport2010();

    // Form Parameters

    private List<EnrollmentCampaign> _enrollmentCampaignList;

    private List<IdentifiableWrapper> _printFormList;
    private IdentifiableWrapper _printForm;

    private List<IdentifiableWrapper> _enrollmentCampaignStageList;
    private IdentifiableWrapper _enrollmentCampaignStage;

    private List<IdentifiableWrapper> _reportAlgorithmList;
    private IdentifiableWrapper _reportAlgorithm;

    private boolean _stateExamPeriodActive;
    private List<IdentifiableWrapper> _stateExamPeriodList;
    private IdentifiableWrapper _stateExamPeriod;

    private ISelectModel _developFormList;

    private boolean _developConditionActive;
    private ISelectModel _developConditionListModel;
    private List<DevelopCondition> _developConditionList;

    private List<CompensationType> _compensationTypeList;

    private boolean _qualificationActive;
    private ISelectModel _qualificationListModel;
    private List<Qualifications> _qualificationList;

    private boolean _studentCategoryActive;
    private ISelectModel _studentCategoryListModel;
    private List<StudentCategory> _studentCategoryList;

    private boolean _formativeOrgUnitActive;
    private ISelectModel _formativeOrgUnitListModel;
    private List<OrgUnit> _formativeOrgUnitList;

    private boolean _territorialOrgUnitActive;
    private ISelectModel _territorialOrgUnitListModel;
    private List<OrgUnit> _territorialOrgUnitList;

    private boolean _parallelActive;
    private List<CommonYesNoUIObject> _parallelList;
    private CommonYesNoUIObject _parallel;

    private boolean _showEduGroups;
    private boolean _groupByBranchAndRepresentation;

    // IEnrollmentCampaignSelectModel

    @Override
    public IDataSettings getSettings()
    {
        return null;
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _report.getEnrollmentCampaign();
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _report.setEnrollmentCampaign(enrollmentCampaign);
    }

    // Getters & Setters

    public StateExamEnrollmentReport2010 getReport()
    {
        return _report;
    }

    public void setReport(StateExamEnrollmentReport2010 report)
    {
        _report = report;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public List<IdentifiableWrapper> getPrintFormList()
    {
        return _printFormList;
    }

    public void setPrintFormList(List<IdentifiableWrapper> printFormList)
    {
        _printFormList = printFormList;
    }

    public IdentifiableWrapper getPrintForm()
    {
        return _printForm;
    }

    public void setPrintForm(IdentifiableWrapper printForm)
    {
        _printForm = printForm;
    }

    public List<IdentifiableWrapper> getEnrollmentCampaignStageList()
    {
        return _enrollmentCampaignStageList;
    }

    public void setEnrollmentCampaignStageList(List<IdentifiableWrapper> enrollmentCampaignStageList)
    {
        _enrollmentCampaignStageList = enrollmentCampaignStageList;
    }

    public IdentifiableWrapper getEnrollmentCampaignStage()
    {
        return _enrollmentCampaignStage;
    }

    public void setEnrollmentCampaignStage(IdentifiableWrapper enrollmentCampaignStage)
    {
        _enrollmentCampaignStage = enrollmentCampaignStage;
    }

    public List<IdentifiableWrapper> getReportAlgorithmList()
    {
        return _reportAlgorithmList;
    }

    public void setReportAlgorithmList(List<IdentifiableWrapper> reportAlgorithmList)
    {
        _reportAlgorithmList = reportAlgorithmList;
    }

    public IdentifiableWrapper getReportAlgorithm()
    {
        return _reportAlgorithm;
    }

    public void setReportAlgorithm(IdentifiableWrapper reportAlgorithm)
    {
        _reportAlgorithm = reportAlgorithm;
    }

    public boolean isStateExamPeriodActive()
    {
        return _stateExamPeriodActive;
    }

    public void setStateExamPeriodActive(boolean stateExamPeriodActive)
    {
        _stateExamPeriodActive = stateExamPeriodActive;
    }

    public List<IdentifiableWrapper> getStateExamPeriodList()
    {
        return _stateExamPeriodList;
    }

    public void setStateExamPeriodList(List<IdentifiableWrapper> stateExamPeriodList)
    {
        _stateExamPeriodList = stateExamPeriodList;
    }

    public IdentifiableWrapper getStateExamPeriod()
    {
        return _stateExamPeriod;
    }

    public void setStateExamPeriod(IdentifiableWrapper stateExamPeriod)
    {
        _stateExamPeriod = stateExamPeriod;
    }

    public ISelectModel getDevelopFormList() { return _developFormList; }
    public void setDevelopFormList(ISelectModel developFormList) { _developFormList = developFormList; }

    public boolean isDevelopConditionActive()
    {
        return _developConditionActive;
    }

    public void setDevelopConditionActive(boolean developConditionActive)
    {
        _developConditionActive = developConditionActive;
    }

    public ISelectModel getDevelopConditionListModel()
    {
        return _developConditionListModel;
    }

    public void setDevelopConditionListModel(ISelectModel developConditionListModel)
    {
        _developConditionListModel = developConditionListModel;
    }

    public List<DevelopCondition> getDevelopConditionList()
    {
        return _developConditionList;
    }

    public void setDevelopConditionList(List<DevelopCondition> developConditionList)
    {
        _developConditionList = developConditionList;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public boolean isQualificationActive()
    {
        return _qualificationActive;
    }

    public void setQualificationActive(boolean qualificationActive)
    {
        _qualificationActive = qualificationActive;
    }

    public ISelectModel getQualificationListModel()
    {
        return _qualificationListModel;
    }

    public void setQualificationListModel(ISelectModel qualificationListModel)
    {
        _qualificationListModel = qualificationListModel;
    }

    public List<Qualifications> getQualificationList()
    {
        return _qualificationList;
    }

    public void setQualificationList(List<Qualifications> qualificationList)
    {
        _qualificationList = qualificationList;
    }

    public boolean isStudentCategoryActive()
    {
        return _studentCategoryActive;
    }

    public void setStudentCategoryActive(boolean studentCategoryActive)
    {
        _studentCategoryActive = studentCategoryActive;
    }

    public ISelectModel getStudentCategoryListModel()
    {
        return _studentCategoryListModel;
    }

    public void setStudentCategoryListModel(ISelectModel studentCategoryListModel)
    {
        _studentCategoryListModel = studentCategoryListModel;
    }

    public List<StudentCategory> getStudentCategoryList()
    {
        return _studentCategoryList;
    }

    public void setStudentCategoryList(List<StudentCategory> studentCategoryList)
    {
        _studentCategoryList = studentCategoryList;
    }

    public boolean isFormativeOrgUnitActive()
    {
        return _formativeOrgUnitActive;
    }

    public void setFormativeOrgUnitActive(boolean formativeOrgUnitActive)
    {
        _formativeOrgUnitActive = formativeOrgUnitActive;
    }

    public ISelectModel getFormativeOrgUnitListModel()
    {
        return _formativeOrgUnitListModel;
    }

    public void setFormativeOrgUnitListModel(ISelectModel formativeOrgUnitListModel)
    {
        _formativeOrgUnitListModel = formativeOrgUnitListModel;
    }

    public List<OrgUnit> getFormativeOrgUnitList()
    {
        return _formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(List<OrgUnit> formativeOrgUnitList)
    {
        _formativeOrgUnitList = formativeOrgUnitList;
    }

    public boolean isTerritorialOrgUnitActive()
    {
        return _territorialOrgUnitActive;
    }

    public void setTerritorialOrgUnitActive(boolean territorialOrgUnitActive)
    {
        _territorialOrgUnitActive = territorialOrgUnitActive;
    }

    public ISelectModel getTerritorialOrgUnitListModel()
    {
        return _territorialOrgUnitListModel;
    }

    public void setTerritorialOrgUnitListModel(ISelectModel territorialOrgUnitListModel)
    {
        _territorialOrgUnitListModel = territorialOrgUnitListModel;
    }

    public List<OrgUnit> getTerritorialOrgUnitList()
    {
        return _territorialOrgUnitList;
    }

    public void setTerritorialOrgUnitList(List<OrgUnit> territorialOrgUnitList)
    {
        _territorialOrgUnitList = territorialOrgUnitList;
    }

    public boolean isParallelActive()
    {
        return _parallelActive;
    }

    public void setParallelActive(boolean parallelActive)
    {
        _parallelActive = parallelActive;
    }

    public List<CommonYesNoUIObject> getParallelList()
    {
        return _parallelList;
    }

    public void setParallelList(List<CommonYesNoUIObject> parallelList)
    {
        _parallelList = parallelList;
    }

    public CommonYesNoUIObject getParallel()
    {
        return _parallel;
    }

    public void setParallel(CommonYesNoUIObject parallel)
    {
        _parallel = parallel;
    }

    public boolean isShowEduGroups()
    {
        return _showEduGroups;
    }

    public void setShowEduGroups(boolean showEduGroups)
    {
        _showEduGroups = showEduGroups;
    }

    public boolean isGroupByBranchAndRepresentation()
    {
        return _groupByBranchAndRepresentation;
    }

    public void setGroupByBranchAndRepresentation(boolean groupByBranchAndRepresentation)
    {
        _groupByBranchAndRepresentation = groupByBranchAndRepresentation;
    }
}
