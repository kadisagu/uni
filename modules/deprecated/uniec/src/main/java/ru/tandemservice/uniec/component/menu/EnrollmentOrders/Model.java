/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.menu.EnrollmentOrders;

import java.util.List;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.unimove.entity.catalog.OrderStates;

/**
 * @author vip_delete
 * @since 07.04.2009
 */
public class Model implements IEnrollmentCampaignSelectModel
{
    public static final String ENROLLMENT_CAMPAIGN_FILTER_NAME = "enrollmentCampaign";
    public static final String CREATE_DATE_FILTER_NAME = "createDate";
    public static final String COMMIT_DATE_FILTER_NAME = "commitDate";
    public static final String NUMBER_FILTER_NAME = "number";
    public static final String ORDER_TYPE_FILTER_NAME = "orderType";
    public static final String ORDER_STATE_FILTER_NAME = "orderState";
    public static final String COMPENSATION_TYPE_FILTER_NAME = "compensationType";
    public static final String FORMATIVE_ORGUNIT_FILTER_NAME = "formativeOrgUnit";
    public static final String TERRITORIAL_ORGUNIT_FILTER_NAME = "territorialOrgUnit";
    public static final String EDUCATION_LEVEL_HIGH_SCHOOL_FILTER_NAME = "educationLevelHighSchool";
    public static final String DEVELOP_FROM_FILTER_NAME = "developForm";

    private IDataSettings _settings;
    private List<EnrollmentCampaign> _enrollmentCampaignList;
    private ISelectModel _orderTypeListModel;
    private List<OrderStates> _orderStateList;
    private List<CompensationType> _compensationTypeList;
    private ISelectModel _formativeOrgUnitListModel;
    private ISelectModel _territorialOrgUnitListModel;
    private ISelectModel _educationLevelHighSchoolListModel;
    private ISelectModel _developFormListModel;
    private DynamicListDataSource _dataSource;

    // IEnrollmentCampaignSelectModel

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return (EnrollmentCampaign) getSettings().get(ENROLLMENT_CAMPAIGN_FILTER_NAME);
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        getSettings().set(ENROLLMENT_CAMPAIGN_FILTER_NAME, enrollmentCampaign);
    }

    // Getters & Setters

    @Override
    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public ISelectModel getOrderTypeListModel()
    {
        return _orderTypeListModel;
    }

    public void setOrderTypeListModel(ISelectModel orderTypeListModel)
    {
        _orderTypeListModel = orderTypeListModel;
    }

    public List<OrderStates> getOrderStateList()
    {
        return _orderStateList;
    }

    public void setOrderStateList(List<OrderStates> orderStateList)
    {
        _orderStateList = orderStateList;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public ISelectModel getFormativeOrgUnitListModel()
    {
        return _formativeOrgUnitListModel;
    }

    public void setFormativeOrgUnitListModel(ISelectModel formativeOrgUnitListModel)
    {
        _formativeOrgUnitListModel = formativeOrgUnitListModel;
    }

    public ISelectModel getTerritorialOrgUnitListModel()
    {
        return _territorialOrgUnitListModel;
    }

    public void setTerritorialOrgUnitListModel(ISelectModel territorialOrgUnitListModel)
    {
        _territorialOrgUnitListModel = territorialOrgUnitListModel;
    }

    public ISelectModel getEducationLevelHighSchoolListModel()
    {
        return _educationLevelHighSchoolListModel;
    }

    public void setEducationLevelHighSchoolListModel(ISelectModel educationLevelHighSchoolListModel)
    {
        _educationLevelHighSchoolListModel = educationLevelHighSchoolListModel;
    }

    public ISelectModel getDevelopFormListModel()
    {
        return _developFormListModel;
    }

    public void setDevelopFormListModel(ISelectModel developFormListModel)
    {
        _developFormListModel = developFormListModel;
    }

    public DynamicListDataSource getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource dataSource)
    {
        _dataSource = dataSource;
    }
}
