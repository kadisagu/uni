package ru.tandemservice.uniec.component.report.EnrolledDealList.Pub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.uni.IUniComponents;

public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickPrint(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IUniComponents.DOWNLOAD_STORABLE_REPORT, new ParametersMap()
                .add("reportId", getModel(component).getReport().getId())
                .add("extension", "rtf")
        ));
    }
}
