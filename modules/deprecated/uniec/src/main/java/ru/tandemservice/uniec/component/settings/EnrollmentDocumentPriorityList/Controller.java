/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.settings.EnrollmentDocumentPriorityList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDefine;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.catalog.EnrollmentDocument;
import ru.tandemservice.uniec.entity.settings.UsedEnrollmentDocument;

/**
 * @author vip_delete
 * @since 06.06.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.setSettings(component.getSettings());

        getDao().prepare(model);

        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null)
        {
            return;
        }

        DynamicListDataSource<EnrollmentDocument> dataSource = UniBaseUtils.createDataSource(component, getDao());

        dataSource.addColumn(new SimpleColumn("Название", EnrollmentDocument.P_TITLE).setClickable(false).setOrderable(false));

        dataSource.addColumn(new ToggleColumn("Использовать", UsedEnrollmentDocument.P_USED).setListener("onClickChangeUsed").setWidth(10));
        dataSource.addColumn(new ToggleColumn("Печатать серию и номер документа об образовании", UsedEnrollmentDocument.P_PRINT_EDUCATION_DOCUMENT_INFO).setListener("onClickChangeEducationDocumentInfo").setWidth(10).setDisabledProperty("notUsed"));
        dataSource.addColumn(new ToggleColumn("Печатать серию и номер приложения к документу об образовании", UsedEnrollmentDocument.P_PRINT_EDU_DOCUMENT_ATTACHMENT_INFO).setListener("onClickChangeAttachmentInfo").setWidth(10).setDisabledProperty("notUsed"));
        dataSource.addColumn(new ToggleColumn("Печатать номер свидетельства ЕГЭ", UsedEnrollmentDocument.P_PRINT_STATE_EXAM_CERTIFICATE_NUMBER).setListener("onClickChangePrintStateExamCertificateNumber").setWidth(10).setDisabledProperty("notUsed"));
        dataSource.addColumn(new ToggleColumn("Печатать сведения об оригинальности", UsedEnrollmentDocument.P_PRINT_ORIGINALITY_INFO).setListener("onClickChangePrintOriginalityInfo").setWidth(10).setDisabledProperty("notUsed"));

        dataSource.addColumn(new ActionColumn("Повысить приоритет", CommonBaseDefine.ICO_UP, "onClickUp").setOrderable(false));
        dataSource.addColumn(new ActionColumn("Понизить приоритет", CommonBaseDefine.ICO_DOWN, "onClickDown").setOrderable(false));

        model.setDataSource(dataSource);
    }

    public void onClickChangeEducationDocumentInfo(IBusinessComponent component)
    {
        getDao().changeEducationDocumentInfo(getModel(component), (Long) component.getListenerParameter());
    }

    public void onClickChangeAttachmentInfo(IBusinessComponent component)
    {
        getDao().changeAttachmentInfo(getModel(component), (Long) component.getListenerParameter());
    }

    public void onClickChangePrintStateExamCertificateNumber (IBusinessComponent component)
    {
        getDao().changePrintStateExamCertificateNumber(getModel(component), (Long) component.getListenerParameter());
    }

    public void onClickChangePrintOriginalityInfo(IBusinessComponent component)
    {
        getDao().changePrintOriginalityInfo(getModel(component), (Long) component.getListenerParameter());
    }

    public void onClickUp(IBusinessComponent component)
    {
        getDao().updatePriority((Long) component.getListenerParameter(), -1);
    }

    public void onClickDown(IBusinessComponent component)
    {
        getDao().updatePriority((Long) component.getListenerParameter(), 1);
    }

    public void onClickChangeUsed(IBusinessComponent component)
    {
        getDao().changeUsed(getModel(component), (Long) component.getListenerParameter());
    }

    public void onChangeEnrollmentCampaign(IBusinessComponent component)
    {
        component.saveSettings();
    }
}
