/* $Id: ReportRow.java 12943 2010-05-27 18:43:04Z agolubenko $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.stateExamEnrollmentResults2010.StateExamEnrollmentResultsAdd;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

import org.tandemframework.core.common.ITitled;

import ru.tandemservice.uni.entity.catalog.EducationLevels;

/**
 * @author vip_delete
 * @since 28.04.2009
 */
class ReportRow implements ITitled
{
    private String _title;
    private Map<EducationLevels, Set<ReportSubRow>> _subRowMap;

    public ReportRow(String title, Map<EducationLevels, Set<ReportSubRow>> subRowMap)
    {
        _title = title;
        _subRowMap = subRowMap == null ? Collections.<EducationLevels, Set<ReportSubRow>>emptyMap() : subRowMap;
    }

    @Override
    public String getTitle()
    {
        return _title;
    }

    public Map<EducationLevels, Set<ReportSubRow>> getSubRowMap()
    {
        return _subRowMap;
    }
}
