/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcOrder.ui.RevertPub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniec.base.bo.EcOrder.ui.RevertParPub.EcOrderRevertParPub;
import ru.tandemservice.uniec.entity.orders.AbstractEntrantOrder;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniec.entity.orders.EnrollmentRevertExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentRevertParagraph;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 06.08.2013
 */
@Configuration
public class EcOrderRevertPub extends BusinessComponentManager
{
    public static final String PARAGRAPH_DS = "paragraphDS";
    public static final String ORDER_PARAM = "order";
    public static final String COUNT_VIEW_PROPERTY = "countView";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(PARAGRAPH_DS, getParagraphDS(), paragraphDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint getParagraphDS()
    {
        return columnListExtPointBuilder(PARAGRAPH_DS)
                .addColumn(publisherColumn("title", "title").businessComponent(EcOrderRevertParPub.class))
                .addColumn(textColumn("canceledOrder", EnrollmentRevertParagraph.canceledOrder().s()).formatter(orderTitleFormatter()))
                .addColumn(textColumn("count", COUNT_VIEW_PROPERTY))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("editParagraph_enrollmentOrder").disabled("ui:order." + AbstractEntrantOrder.P_READONLY))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER, alert(PARAGRAPH_DS + ".delete.alert", EnrollmentRevertParagraph.title())).permissionKey("deleteParagraph_enrollmentOrder").disabled("ui:order." + AbstractEntrantOrder.P_READONLY))
                .create();
    }

    public static IFormatter<EnrollmentOrder> orderTitleFormatter()
    {
        return source -> {
            StringBuilder ret = new StringBuilder();
            if (source.getNumber() != null)
                ret.append("№").append(source.getNumber());
            if (source.getCommitDate() != null)
            {
                if (ret.length() > 0)
                    ret.append(" ");
                ret.append("от ").append(new SimpleDateFormat("dd.MM.yyyy").format(source.getCommitDate()));
            }
            return ret.toString();
        };
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> paragraphDSHandler()
    {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                DQLSelectBuilder countBuilder = new DQLSelectBuilder().fromEntity(EnrollmentRevertExtract.class, "e")
                        .column(DQLFunctions.count("e.id"))
                        .where(eq(property(EnrollmentRevertExtract.paragraph().id().fromAlias("e")), property(EnrollmentRevertParagraph.id().fromAlias("p"))));

                List<Object[]> list = new DQLSelectBuilder()
                        .fromEntity(EnrollmentRevertParagraph.class, "p")
                        .column("p") // [0]
                        .column(countBuilder.buildQuery()) // [1]
                        .where(eq(property(EnrollmentRevertParagraph.order().fromAlias("p")), commonValue(context.get(ORDER_PARAM))))
                        .order(property(EnrollmentRevertParagraph.number().fromAlias("p")))
                        .createStatement(context.getSession()).list();

                List<DataWrapper> resultList = new ArrayList<>(list.size());
                for (Object[] item : list)
                {
                    DataWrapper wrapper = new DataWrapper(item[0]);
                    wrapper.setProperty(COUNT_VIEW_PROPERTY, item[1]);
                    resultList.add(wrapper);
                }

                return ListOutputBuilder.get(input, resultList).build();
            }
        };
    }
}