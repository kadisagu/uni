package ru.tandemservice.uniec.entity.catalog;

import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.gen.EntrantExamListsFormingFeatureGen;

/**
 * Особенности формирования экзаменационных листов
 */
public class EntrantExamListsFormingFeature extends EntrantExamListsFormingFeatureGen
{
    public boolean isFormingForEntrant()
    {
        return getCode().equals(UniecDefines.EXAMLIST_FORMING_FEATURE_FOR_ENTRANT);
    }
}