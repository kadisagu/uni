/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.util;

import java.util.List;

/**
 * @author Vasily Zhukov
 * @since 14.07.2011
 */
public interface IEcgPlanChoiceResult
{
    /**
     * @return выбранное направление приема (или профиль)
     */
    IEcgEntrantRateDirectionDTO getChosenDirection();

    /**
     * @return возможные для выбора выбранные направления приема (или профили)
     */
    List<IEcgEntrantRateDirectionDTO> getPossibleDirectionList();
}
