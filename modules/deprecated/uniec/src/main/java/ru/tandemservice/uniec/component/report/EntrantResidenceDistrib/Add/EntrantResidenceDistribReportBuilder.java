/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EntrantResidenceDistrib.Add;

import org.hibernate.Session;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.RtfParagraph;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressDistrict;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.fias.base.entity.AddressRu;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vip_delete
 * @since 03.09.2009
 */
public class EntrantResidenceDistribReportBuilder
{
    final static int COMP_TYPE_ID = 0;
    final static int SETTLEMENT_ID = 1;
    final static int DISTRICT_ID = 2;
    final static int COUNT = 3;
    final static int EDU_OU = 4;

    final static int BUDGET_IDX = 0;
    final static int CONTRACT_IDX = 1;

    private Model _model;
    private Session _session;

    EntrantResidenceDistribReportBuilder(Model model, Session session)
    {
        _model = model;
        _session = session;
    }

    DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    private byte[] buildReport()
    {
        final RtfDocument document = loadTemplate();
        final RtfDocument clone = document.getClone();


        if (_model.isSeparateEduOrgUnit())
        {
            final RtfSearchResult searchResult = UniRtfUtil.findRtfTableMark(document, "T");
            document.getElementList().remove(searchResult.getIndex());

            writeCommonHeader(document);

            final Map<EducationOrgUnit, List<Object[]>> eduOU2dataMap = getDataByEduOrgUnit();

            for (final Iterator<Map.Entry<EducationOrgUnit, List<Object[]>>> iterator = eduOU2dataMap.entrySet().iterator(); iterator.hasNext();)
            {
                final Map.Entry<EducationOrgUnit, List<Object[]>> entry = iterator.next();

                final ReportDataModel reportDataModel = prepareReportDataModel(entry.getValue());

                writeTableByEduOrgUnit(entry.getKey(), reportDataModel, document, clone);
                if (iterator.hasNext())
                    document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
            }
        }
        else
        {
            final ReportDataModel reportDataModel = prepareReportDataModel(getData());

            writeCommonHeader(document);
            writeTable(reportDataModel, document, clone);
        }

        return RtfUtil.toByteArray(document);
    }

    protected RtfDocument writeTableByEduOrgUnit(EducationOrgUnit eduOrgUnit, final ReportDataModel reportDataModel, RtfDocument document, RtfDocument emptyClone)
    {
        RtfDocument clone = emptyClone.getClone();
        // удаляем заголовок, что бы осталась таблица и метка vuz
        clone.getElementList().remove(1);
        clone.getElementList().remove(1);
        clone.getElementList().remove(1);

        // удаляем выравнивание по центру руками, т.к. по другому оно приоритетней
        if (clone.getElementList().get(0) != null && clone.getElementList().get(0).toString().contains("vuz") && clone.getElementList().get(0) instanceof RtfParagraph)
        {
            for (Iterator<IRtfElement> iterator = ((RtfParagraph) clone.getElementList().get(0)).getElementList().iterator(); iterator.hasNext(); )
            {
                if (iterator.next().toString().contains("\\qc"))
                    iterator.remove();
            }
        }

        // в метку вставляем информацию по НП для каждой таблицы
        final RtfString eduOUTitle = new RtfString().append(IRtfData.B, 0).append(IRtfData.QL)
                .append(eduOrgUnit.getEducationLevelHighSchool().getFullTitle()).append(" (").append(eduOrgUnit.getDevelopCombinationFullTitle()).append(")")
                .append(IRtfData.LINE)
                .append(eduOrgUnit.getFormativeOrgUnit().getNominativeCaseTitle()).append(" (").append(eduOrgUnit.getTerritorialOrgUnit().getTerritorialFullTitle()).append(")");
        new RtfInjectModifier()
                .put("vuz", eduOUTitle)
                .modify(clone);


        // рисуем таблицу, добавляем ее на новый лист
        final RtfDocument table = writeTable(reportDataModel, clone, emptyClone);
        document.getElementList().add(table);

        return document;
    }

    protected RtfDocument writeTable(final ReportDataModel reportDataModel, RtfDocument document, RtfDocument emptyClone)
    {
        final RtfDocument clone = emptyClone.getClone();

        final int blackIndex = document.getHeader().getColorTable().addColor(0, 0, 0);
        final int grayIndex = document.getHeader().getColorTable().addColor(153, 153, 153);

        final RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("S1", Integer.toString(reportDataModel.getSum()[BUDGET_IDX]));
        injectModifier.put("S2", Integer.toString(reportDataModel.getSum()[CONTRACT_IDX]));
        injectModifier.put("S3", Integer.toString(reportDataModel.getSum()[BUDGET_IDX] + reportDataModel.getSum()[CONTRACT_IDX]));
        injectModifier.put("S4", "100");
        injectModifier.modify(document);

        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T", new String[0][]);
        tableModifier.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public void beforeModify(RtfTable table, int currentRowIndex)
            {
                RtfRow templateRow = table.getRowList().get(currentRowIndex);

                List<RtfRow> newRowList = new ArrayList<>();

                if (reportDataModel.getAcademyCity() != null)
                {
                    // создаем строку с городом ОУ
                    if (reportDataModel.getHighSchoolCity()[BUDGET_IDX] != 0 || reportDataModel.getHighSchoolCity()[CONTRACT_IDX] != 0)
                        newRowList.add(createRow(templateRow, 0, blackIndex, reportDataModel.getAcademyCity().getTitleWithType(), reportDataModel.getHighSchoolCity()[BUDGET_IDX], reportDataModel.getHighSchoolCity()[CONTRACT_IDX], reportDataModel.getTotalSum(), null, null));

                    // создаем строку с районами города ОУ
                    for (AddressDistrict district : reportDataModel.getDistrictList())
                    {
                        int[] value = reportDataModel.getDistrict2count().get(district);
                        newRowList.add(createRow(templateRow, 181, grayIndex, district.getTitle(), value[BUDGET_IDX], value[CONTRACT_IDX], reportDataModel.getTotalSum(), null, null));
                    }

                    // создаем строку с регионом города ОУ
                    if (reportDataModel.getHighSchoolRegion()[BUDGET_IDX] != 0 || reportDataModel.getHighSchoolRegion()[CONTRACT_IDX] != 0)
                        newRowList.add(createRow(templateRow, 0, blackIndex, reportDataModel.getAcademyRegion().getTitleWithType() + (reportDataModel.getAcademyRegion().equals(reportDataModel.getAcademyCity()) ? " (регион)" : ""), reportDataModel.getHighSchoolRegion()[BUDGET_IDX], reportDataModel.getHighSchoolRegion()[CONTRACT_IDX], reportDataModel.getTotalSum(), reportDataModel.getAcademyRegion().getId(), reportDataModel.getArea2percentMap()));
                }

                // создаем строки с областями РФ
                for (AddressItem area : reportDataModel.getAreaList())
                {
                    int[] value = reportDataModel.getArea2count().get(area);
                    newRowList.add(createRow(templateRow, 0, blackIndex, area.getTitleWithType(), value[BUDGET_IDX], value[CONTRACT_IDX], reportDataModel.getTotalSum(), area.getId(), reportDataModel.getArea2percentMap()));
                }

                // создаем строки с иностранцами
                if (reportDataModel.getForeignCountry()[BUDGET_IDX] != 0 || reportDataModel.getForeignCountry()[CONTRACT_IDX] != 0)
                    newRowList.add(createRow(templateRow, 0, blackIndex, "Другие государства", reportDataModel.getForeignCountry()[BUDGET_IDX], reportDataModel.getForeignCountry()[CONTRACT_IDX], reportDataModel.getTotalSum(), null, null));

                table.getRowList().addAll(currentRowIndex + 1, newRowList);
            }
        });

        tableModifier.modify(document);

        if (_model.getDetail().isTrue())
        {
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            clone.getElementList().remove(1);
            clone.getElementList().remove(1);

            if (reportDataModel.getArea2city2count().containsKey(reportDataModel.getAcademyRegion()))
                reportDataModel.getAreaList().add(0, reportDataModel.getAcademyRegion());

            for (AddressItem area : reportDataModel.getAreaList())
            {
                final Map<AddressItem, int[]> map = reportDataModel.getArea2city2count().get(area);

                if (map != null)
                {
                    int[] subSum = new int[2];
                    for (int[] value : map.values())
                    {
                        subSum[BUDGET_IDX] += value[BUDGET_IDX];
                        subSum[CONTRACT_IDX] += value[CONTRACT_IDX];
                    }

                    final List<AddressItem> cityList = new ArrayList<>(map.keySet());
                    Collections.sort(cityList, ITitled.TITLED_COMPARATOR);

                    RtfDocument doc = clone.getClone();

                    RtfInjectModifier im = new RtfInjectModifier();
                    im.put("vuz", area.getTitleWithType() + " (детализация по населенным пунктам)");
                    im.put("S1", Integer.toString(subSum[BUDGET_IDX]));
                    im.put("S2", Integer.toString(subSum[CONTRACT_IDX]));
                    im.put("S3", Integer.toString(subSum[BUDGET_IDX] + subSum[CONTRACT_IDX]));
                    im.put("S4", reportDataModel.getArea2percentMap().get(area.getId()));

                    RtfTableModifier tm = new RtfTableModifier();
                    tm.put("T", new String[0][]);
                    tm.put("T", new RtfRowIntercepterBase()
                    {
                        @Override
                        public void beforeModify(RtfTable table, int currentRowIndex)
                        {
                            RtfRow templateRow = table.getRowList().get(currentRowIndex);

                            List<RtfRow> newRowList = new ArrayList<>();

                            // создаем строки с областями РФ
                            for (AddressItem city : cityList)
                            {
                                int[] value = map.get(city);
                                newRowList.add(createRow(templateRow, 0, blackIndex, city.getTitleWithType(), value[BUDGET_IDX], value[CONTRACT_IDX], reportDataModel.getTotalSum(), null, null));
                            }

                            table.getRowList().addAll(currentRowIndex + 1, newRowList);
                        }
                    });

                    im.modify(doc);
                    tm.modify(doc);

                    document.getElementList().add(doc);
                }
            }
        }

        return document;
    }

    protected RtfDocument writeCommonHeader(RtfDocument document)
    {
        final TopOrgUnit academy = TopOrgUnit.getInstance();

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("vuz", academy.getTitle());
        injectModifier.put("stage", _model.getReport().getEnrollmentCampaignStage());

        injectModifier.modify(document);

        return document;
    }

    private static RtfRow createRow(RtfRow templateRow, int indentValue, int colorIndex, String title, int budget, int contract, int totalSum, Long areaId, Map<Long, String> area2persentMap)
    {
        IRtfElement left = RtfBean.getElementFactory().createRtfControl(IRtfData.QL);
        IRtfElement center = RtfBean.getElementFactory().createRtfControl(IRtfData.QC);
        IRtfControl color = RtfBean.getElementFactory().createRtfControl(IRtfData.CF, colorIndex);
//        color.setValue(colorIndex);
        IRtfControl indent = RtfBean.getElementFactory().createRtfControl(IRtfData.LI, indentValue);
//        indent.setValue(indentValue);

        IRtfElement titleText = RtfBean.getElementFactory().createRtfText(title);
        IRtfElement budgetText = RtfBean.getElementFactory().createRtfText(Integer.toString(budget));
        IRtfElement contractText = RtfBean.getElementFactory().createRtfText(Integer.toString(contract));
        IRtfElement sumText = RtfBean.getElementFactory().createRtfText(Integer.toString(budget + contract));
        String percent = new DoubleFormatter(1, true).format((double) (budget + contract) * 100.0 / (double) totalSum);
        if (area2persentMap != null)
            area2persentMap.put(areaId, percent);
        IRtfElement percentText = RtfBean.getElementFactory().createRtfText(percent);

        RtfRow row = templateRow.getClone();
        List<RtfCell> list = row.getCellList();

        list.get(0).setElementList(Arrays.asList(color, left, indent, titleText));
        list.get(1).setElementList(Arrays.asList(color, center, budgetText));
        list.get(2).setElementList(Arrays.asList(color, contractText));
        list.get(3).setElementList(Arrays.asList(color, sumText));
        list.get(4).setElementList(Arrays.asList(color, percentText));
        return row;
    }

    @SuppressWarnings("SuspiciousMethodCalls")
    private Map<EducationOrgUnit, List<Object[]>> getDataByEduOrgUnit()
    {
        final DQLSelectBuilder dataDQL = createDataDQL();

        dataDQL.column(property("t.ouId"), "ouId_alias");
        dataDQL.group(property("t.ouId"));

        final List<Object[]> data = dataDQL.createStatement(_session).list();

        final Map<EducationOrgUnit, List<Object[]>> resultMap = new TreeMap<>(new EntityComparator<>(new EntityOrder(EducationOrgUnit.developForm().code().s()), new EntityOrder(EducationOrgUnit.title().s()), new EntityOrder(EducationOrgUnit.developCondition().code().s()), new EntityOrder(EducationOrgUnit.developTech().code().s()), new EntityOrder(EducationOrgUnit.developPeriod().code().s()), new EntityOrder(EducationOrgUnit.id().s())));
        for (Object[] o : data)
        {
            final Long eduOUId = (Long) o[EDU_OU];

            SafeMap.safeGet(resultMap, (EducationOrgUnit) _session.get(EducationOrgUnit.class, eduOUId), ArrayList.class)
                    .add(o);
        }

        return resultMap;
    }

    private List<Object[]> getData()
    {
        return createDataDQL().createStatement(_session).list();
    }

    private DQLSelectBuilder createDataDQL()
    {
        DQLSelectBuilder builder;

        switch (_model.getEnrollmentCampaignStage().getId().intValue())
        {
            case Model.ENROLLMENT_CAMP_STAGE_DOCUMENTS:
                builder = getRequestedBuilder();
                builder.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_STATE, "state");
                builder.where(ne(property("state." + EntrantState.P_CODE), UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
                builder.group(("request.id"));
                break;
            case Model.ENROLLMENT_CAMP_STAGE_EXAMS:
                builder = getRequestedBuilder();
                builder.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_STATE, "state");
                builder.where(notIn(property("state." + EntrantState.P_CODE), Arrays.asList(
                        UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY,
                        UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE,
                        UniecDefines.ENTRANT_STATE_ACTIVE_CODE
                )));
                builder.group(property("request.id"));
                break;
            case Model.ENROLLMENT_CAMP_STAGE_ENROLLMENT:
                builder = getPreliminaryBuilder();
                builder.group(property("p.id"));
                break;
            default:
                throw new RuntimeException("Unknown report stage!");
        }

        builder.group(property("ct.id"));
        builder.group(property("settlement.id"));
        builder.group(property("district.id"));
        builder.group(property("ou.id"));

        builder.column(property("ct.id"), "ctId");
        builder.column(property("settlement.id"), "settlementId");
        builder.column(property("district.id"), "districtId");
        builder.column(property("ou.id"), "ouId");

        return new DQLSelectBuilder()
                .column(property("t.ctId"), "ctId_alias")
                .column(property("t.settlementId"), "settlementId_alias")
                .column(property("t.districtId"), "districtId_alias")
                .column(DQLFunctions.countStar(), "count")
                .fromDataSource(builder.buildQuery(), "t")
                .group(property("t.ctId"))
                .group(property("t.settlementId"))
                .group(property("t.districtId"));
    }

    private DQLSelectBuilder getRequestedBuilder()
    {
        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(RequestedEnrollmentDirection.class, "d");
        builder.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "request");
        builder.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, "ed");
        builder.joinPath(DQLJoinType.inner, "ed." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "ou");
        builder.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_COMPENSATION_TYPE, "ct");

        patchEduOrgUnit(builder, "d." + RequestedEnrollmentDirection.L_STUDENT_CATEGORY);

        return builder;
    }

    private DQLSelectBuilder getPreliminaryBuilder()
    {
        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(PreliminaryEnrollmentStudent.class, "p");
        builder.joinPath(DQLJoinType.inner, "p." + PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, "d");
        builder.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "request");
        builder.joinPath(DQLJoinType.inner, "p." + PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT, "ou");
        builder.joinPath(DQLJoinType.inner, "p." + PreliminaryEnrollmentStudent.L_COMPENSATION_TYPE, "ct");

        if (_model.isParallelActive() && _model.getParallel().isTrue())
            builder.where(eq(property("p." + PreliminaryEnrollmentStudent.P_PARALLEL), value(Boolean.FALSE)));

        builder.where(ne(property("d." + RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE), UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        patchEduOrgUnit(builder, "p." + PreliminaryEnrollmentStudent.L_STUDENT_CATEGORY);

        return builder;
    }

    private void patchEduOrgUnit(DQLSelectBuilder builder, String studentCategoryAlias)
    {
        builder.joinPath(DQLJoinType.inner, "request." + EntrantRequest.L_ENTRANT, "entrant");
        builder.joinPath(DQLJoinType.inner, "entrant." + Entrant.L_PERSON, "person");
        builder.joinPath(DQLJoinType.inner, "person." + Person.L_IDENTITY_CARD, "identityCard");
        builder.joinPath(DQLJoinType.inner, "identityCard." + IdentityCard.L_ADDRESS, "address");
        builder.joinEntity("identityCard", DQLJoinType.inner, AddressDetailed.class, "addressDetailed",
                           eq(property("identityCard", IdentityCard.L_ADDRESS), property("addressDetailed.id"))
        );
        builder.joinEntity("addressDetailed", DQLJoinType.left, AddressRu.class, "addressRu",
                           eq(property("addressDetailed.id"), property("addressRu.id"))
        );
        builder.joinPath(DQLJoinType.inner, "addressDetailed." + AddressDetailed.L_SETTLEMENT, "settlement");
        builder.joinPath(DQLJoinType.left, "addressRu." + AddressRu.L_DISTRICT, "district");
        builder.joinPath(DQLJoinType.inner, "ou." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "hs");
        builder.joinPath(DQLJoinType.inner, "hs." + EducationLevelsHighSchool.L_EDUCATION_LEVEL, "el");
        builder.where(eq(property("entrant." + Entrant.P_ARCHIVAL), value(Boolean.FALSE)));
        builder.where(eq(property("entrant." + Entrant.L_ENROLLMENT_CAMPAIGN), value(_model.getReport().getEnrollmentCampaign())));
        builder.where(betweenDays("request." + EntrantRequest.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));

        if (_model.isStudentCategoryActive())
            builder.where(in(property(studentCategoryAlias), _model.getStudentCategoryList()));
        if (_model.isQualificationActive())
            builder.where(in(property("el." + EducationLevels.L_QUALIFICATION), _model.getQualificationList()));
        if (_model.isFormativeOrgUnitActive())
            builder.where(in(property("ou." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT), _model.getFormativeOrgUnitList()));
        if (_model.isTerritorialOrgUnitActive())
        {
            if (_model.getTerritorialOrgUnitList().isEmpty())
                builder.where(isNull(property("ou." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT)));
            else
                builder.where(in(property("ou." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT), _model.getTerritorialOrgUnitList()));
        }
        if (_model.isDevelopFormActive())
            builder.where(in(property("ou." + EducationOrgUnit.L_DEVELOP_FORM), _model.getDevelopFormList()));
        if (_model.isDevelopConditionActive())
            builder.where(in(property("ou." + EducationOrgUnit.L_DEVELOP_CONDITION), _model.getDevelopConditionList()));
        if (_model.isDevelopTechActive())
            builder.where(in(property("ou." + EducationOrgUnit.L_DEVELOP_TECH), _model.getDevelopTechList()));
        if (_model.isDevelopPeriodActive())
            builder.where(in(property("ou." + EducationOrgUnit.L_DEVELOP_PERIOD), _model.getDevelopPeriodList()));
    }

    protected ReportDataModel prepareReportDataModel(List<Object[]> data)
    {
        return new ReportDataModel(data);
    }

    protected RtfDocument loadTemplate()
    {
        // load template
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ENTRANT_RESIDENCE_DISTRIB);
        return new RtfReader().read(templateDocument.getCurrentTemplate());
    }

    protected class ReportDataModel
    {
        private final List<Object[]> _data;

        private final TopOrgUnit _academy;
        private final AddressItem _academyCity;
        private final AddressDetailed _academyAddress;
        private final AddressItem _academyRegion;

        private final int[] _highSchoolCity = new int[2];
        private final int[] _highSchoolRegion = new int[2];
        private final Map<AddressDistrict, int[]> _district2count = new HashMap<>();
        private final Map<AddressItem, int[]> _area2count = new HashMap<>();
        private final Map<AddressItem, Map<AddressItem, int[]>> _area2city2count = new HashMap<>();
        private final int[] _foreignCountry = new int[2];
        private final List<AddressDistrict> _districtList;
        private final List<AddressItem> _areaList;

        private final int[] _sum = new int[2];
        private final int _totalSum;

        private final Map<Long, String> _area2percentMap = new HashMap<>();

        public ReportDataModel(List<Object[]> data)
        {
            _data = data;

            // загружаем город и регион ОУ
            _academy = TopOrgUnit.getInstance();
            _academyAddress = _academy.getAddress();
            _academyCity = _academyAddress == null ? null : _academyAddress.getSettlement();
            AddressItem temp = _academyCity;
            if (temp != null)
                while (temp.getParent() != null)
                    temp = temp.getParent();
            _academyRegion = temp;
            long budgetId = UniDaoFacade.getCoreDao().getCatalogItem(CompensationType.class, UniDefines.COMPENSATION_TYPE_BUDGET).getId();

            // создаем структуры данных
            for (Object[] row : _data)
            {
                long settlementId = ((Number) row[SETTLEMENT_ID]).longValue();
                long compTypeId = ((Number) row[COMP_TYPE_ID]).longValue();
                Long districtId = row[DISTRICT_ID] == null ? null : ((Number) row[DISTRICT_ID]).longValue();
                int count = ((Number) row[COUNT]).intValue();

                // получаем город и регион абитуриента
                AddressItem settlement = (AddressItem) _session.get(AddressItem.class, settlementId);
                AddressItem region = settlement;
                while (region.getParent() != null) region = region.getParent();

                if (settlement.getCountry().getCode() == IKladrDefines.RUSSIA_COUNTRY_CODE)
                {
                    if (settlement.equals(_academyCity))
                    {
                        // проверяем на равенство городу ОУ
                        _highSchoolCity[compTypeId == budgetId ? BUDGET_IDX : CONTRACT_IDX] += count;

                        // проверяем район города ОУ
                        if (districtId != null)
                        {
                            AddressDistrict district = (AddressDistrict) _session.get(AddressDistrict.class, districtId);
                            int[] value = _district2count.get(district);
                            if (value == null)
                                _district2count.put(district, value = new int[2]);
                            value[compTypeId == budgetId ? BUDGET_IDX : CONTRACT_IDX] += count;
                        }
                    } else if (region.equals(_academyRegion))
                    {
                        // проверяем на равенство региону города ОУ
                        _highSchoolRegion[compTypeId == budgetId ? BUDGET_IDX : CONTRACT_IDX] += count;

                        if (_model.getDetail().isTrue())
                        {
                            Map<AddressItem, int[]> map = _area2city2count.get(region);
                            if (map == null)
                                _area2city2count.put(region, map = new HashMap<>());
                            int[] mapValue = map.get(settlement);
                            if (mapValue == null)
                                map.put(settlement, mapValue = new int[2]);
                            mapValue[compTypeId == budgetId ? BUDGET_IDX : CONTRACT_IDX] += count;
                        }
                    } else
                    {
                        // другая область
                        int[] value = _area2count.get(region);
                        if (value == null)
                            _area2count.put(region, value = new int[2]);
                        value[compTypeId == budgetId ? BUDGET_IDX : CONTRACT_IDX] += count;

                        if (_model.getDetail().isTrue())
                        {
                            Map<AddressItem, int[]> map = _area2city2count.get(region);
                            if (map == null)
                                _area2city2count.put(region, map = new HashMap<>());
                            int[] mapValue = map.get(settlement);
                            if (mapValue == null)
                                map.put(settlement, mapValue = new int[2]);
                            mapValue[compTypeId == budgetId ? BUDGET_IDX : CONTRACT_IDX] += count;
                        }
                    }
                } else
                {
                    _foreignCountry[compTypeId == budgetId ? BUDGET_IDX : CONTRACT_IDX] += count;
                }
            }

            _districtList = new ArrayList<>(_district2count.keySet());
            Collections.sort(_districtList, ITitled.TITLED_COMPARATOR);

            _areaList = new ArrayList<>(_area2count.keySet());
            Collections.sort(_areaList, ITitled.TITLED_COMPARATOR);

            // считаем сумму
            _sum[BUDGET_IDX] += _highSchoolCity[BUDGET_IDX] + _highSchoolRegion[BUDGET_IDX] + _foreignCountry[BUDGET_IDX];
            _sum[CONTRACT_IDX] += _highSchoolCity[CONTRACT_IDX] + _highSchoolRegion[CONTRACT_IDX] + _foreignCountry[CONTRACT_IDX];
            for (int[] entry : _area2count.values())
            {
                _sum[BUDGET_IDX] += entry[BUDGET_IDX];
                _sum[CONTRACT_IDX] += entry[CONTRACT_IDX];
            }
            _totalSum = _sum[BUDGET_IDX] + _sum[CONTRACT_IDX];
        }

        public TopOrgUnit getAcademy()
        {
            return _academy;
        }

        public AddressItem getAcademyCity()
        {
            return _academyCity;
        }

        public AddressItem getAcademyRegion()
        {
            return _academyRegion;
        }

        public int[] getHighSchoolCity()
        {
            return _highSchoolCity;
        }

        public int[] getHighSchoolRegion()
        {
            return _highSchoolRegion;
        }

        public Map<AddressDistrict, int[]> getDistrict2count()
        {
            return _district2count;
        }

        public Map<AddressItem, int[]> getArea2count()
        {
            return _area2count;
        }

        public Map<AddressItem, Map<AddressItem, int[]>> getArea2city2count()
        {
            return _area2city2count;
        }

        public int[] getForeignCountry()
        {
            return _foreignCountry;
        }

        public int[] getSum()
        {
            return _sum;
        }

        public int getTotalSum()
        {
            return _totalSum;
        }

        public Map<Long, String> getArea2percentMap()
        {
            return _area2percentMap;
        }

        public List<AddressDistrict> getDistrictList()
        {
            return _districtList;
        }

        public List<AddressItem> getAreaList()
        {
            return _areaList;
        }
    }
}
