/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.menu.examGroup.abstractExamGroupList;

import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.dao.examgroup.IAbstractExamGroupLogic;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.ExamGroup;
import ru.tandemservice.uniec.entity.entrant.ExamGroupRow;
import ru.tandemservice.uniec.entity.entrant.ExamGroupSet;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Vasily Zhukov
 * @since 28.05.2010
 */
public abstract class ExamGroupListDAO<Model extends ExamGroupListModel> extends UniDao<Model> implements IExamGroupListDAO<Model>
{
    @Override
    public final void prepare(final Model model)
    {
        // селект с приемными кампаниями
        EntrantFilterUtil.prepareEnrollmentCampaignFilterWithExamGroupAutoFormingAndAlgorithm(model, getSession());

        // вначале определяем, какой бизнес компонент требуется для отображения списка экзаменационных групп
        IAbstractExamGroupLogic logic = UniecDAOFacade.getExamGroupSetDao().getCurrentExamGroupLogic(model.getEnrollmentCampaign());

        if (logic != null && !getClass().getPackage().getName().equals(logic.getExamGroupListBusinessComponent()))
        {
            // если сейчас загружен не тот бизнес компонент, то делаем редирект на правильный
            model.setRedirectComponentName(logic.getExamGroupListBusinessComponent());
        } else
        {
            // загружен правильный компонент, редирект не требуется
            model.setRedirectComponentName(null);

            // наборы экзаменов
            model.setExamGroupSetModel(new FullCheckSelectModel()
            {
                @Override
                public ListResult findValues(String filter)
                {
                    if (model.getEnrollmentCampaign() == null) return ListResult.getEmpty();
                    List<ExamGroupSet> list = new MQBuilder(ExamGroupSet.ENTITY_CLASS, "e")
                            .add(MQExpression.eq("e", ExamGroupSet.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()))
                            .addOrder("e", ExamGroupSet.P_BEGIN_DATE, OrderDirection.desc)
                            .getResultList(getSession());
                    return new ListResult<>(list, list.size());
                }
            });

            // создание дополнительных фильтров
            prepareModel(model);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public final void prepareListDataSource(Model model)
    {
        // получаем значения фильтров
        final EnrollmentCampaign enrollmentCampaign = model.getEnrollmentCampaign();
        final ExamGroupSet examGroupSet = model.getSettings().get("examGroupSet");
        final Date beginDate = model.getSettings().get("beginDate");
        final Date endDate = model.getSettings().get("endDate");

        MQBuilder builder = new MQBuilder(ExamGroup.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", ExamGroup.L_EXAM_GROUP_SET + "." + ExamGroupSet.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign));
        builder.add(MQExpression.eq("e", ExamGroup.L_EXAM_GROUP_SET, examGroupSet));

        if (beginDate != null)
            builder.add(UniMQExpression.greatOrEq("e", ExamGroup.P_CREATE_DATE, beginDate));
        if (endDate != null)
            builder.add(UniMQExpression.lessOrEq("e", ExamGroup.P_CREATE_DATE, endDate));

        builder.addOrder("e", ExamGroup.P_CREATE_DATE);

        prepareGroupList(builder, "e", model);

        List<ExamGroup> list = builder.getResultList(getSession());
        model.getDataSource().setCountRow(list.size());
        UniBaseUtils.createPage(model.getDataSource(), list);

        String hql = "select e." + ExamGroupRow.L_EXAM_GROUP + ".id, count(*)  from " + ExamGroupRow.ENTITY_CLASS + " e group by e." + ExamGroupRow.L_EXAM_GROUP + ".id";
        List<Object[]> rowList = (List<Object[]>) getSession().createQuery(hql).list();
        Map<Long, Integer> group2count = new HashMap<>();
        for (Object[] row : rowList)
            group2count.put((Long) row[0], ((Number) row[1]).intValue());

        for (ViewWrapper wrapper : ViewWrapper.getPatchedList(model.getDataSource()))
        {
            Integer count = group2count.get(wrapper.getEntity().getId());
            wrapper.setViewProperty(ExamGroupListModel.ENTRANT_COUNT, count == null ? 0 : count);
        }
    }

    // подготовка модели, создание кастомных фильтров

    protected abstract void prepareModel(Model model);

    // применение кастомных фильтров (фильтры должны быть всегда, иначе в чем смысл экзаменационных групп)

    protected abstract void prepareGroupList(MQBuilder builder, String alias, Model model);
}
