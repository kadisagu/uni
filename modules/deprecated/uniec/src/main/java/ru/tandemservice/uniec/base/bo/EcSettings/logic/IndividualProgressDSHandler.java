/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcSettings.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniec.base.bo.EcSettings.ui.IndividualProgressSetting.EcSettingsIndividualProgressSettingUI;
import ru.tandemservice.uniec.entity.catalog.IndividualProgress;
import ru.tandemservice.uniec.entity.entrant.EnrCampaignEntrantIndividualProgress;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * @author Ekaterina Zvereva
 * @since 31.03.2015
 */
public class IndividualProgressDSHandler extends DefaultComboDataSourceHandler
{
    public IndividualProgressDSHandler(String ownerId)
    {
        super(ownerId, IndividualProgress.class);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final EnrollmentCampaign enrCamp = context.get(EcSettingsIndividualProgressSettingUI.PARAM_ENR_CAMPAIGN_DS);
        if (enrCamp == null)
            return ListOutputBuilder.get(input, Collections.emptyList()).build();

        final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(IndividualProgress.class, "ip").column(property("ip"))
                .order(property(IndividualProgress.title().fromAlias("ip")), OrderDirection.asc);

        final DSOutput output = DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(false).build();

        return wrap(output, input, context);
    }


    protected DSOutput wrap(DSOutput output, DSInput input, ExecutionContext context)
    {
        final EnrollmentCampaign enrCamp = context.get(EcSettingsIndividualProgressSettingUI.PARAM_ENR_CAMPAIGN_DS);

        final List<IndividualProgress> relationList = new DQLSelectBuilder().fromEntity(EnrCampaignEntrantIndividualProgress.class, "r")
                .column(property("r", EnrCampaignEntrantIndividualProgress.individualProgress()))
                .where(eq(property(EnrCampaignEntrantIndividualProgress.enrollmentCampaign().fromAlias("r")), commonValue(enrCamp)))
                .createStatement(context.getSession()).list();

        for (DataWrapper wrapper : DataWrapper.wrap(output))
            wrapper.setProperty(EnrCampaignEntrantIndividualProgress.INDIVID_ARCHIEVEMENT_USED, relationList.contains(wrapper.<IndividualProgress>getWrapped()));

        return output;
    }
}