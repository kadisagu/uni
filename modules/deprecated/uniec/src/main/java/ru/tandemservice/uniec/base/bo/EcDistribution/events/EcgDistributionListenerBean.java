package ru.tandemservice.uniec.base.bo.EcDistribution.events;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.Date;

import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;

import ru.tandemservice.uniec.base.entity.ecg.EcgDistribution;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistributionDetail;
import ru.tandemservice.uniec.base.entity.ecg.codes.EcgDistributionStateCodes;

/**
 * @author vdanilov
 */
public class EcgDistributionListenerBean {

    public static class StateChangeListener implements IDSetEventListener {
        public static final StateChangeListener INSTANCE = new StateChangeListener();

        @Override
        public void onEvent(DSetEvent event) {

            /* WAS: event.getMultitude().getAffectedProperties().contains("state") */
            if (true /* всегда, чтобы кто-нибудь не изменил дату */)
            {

                // устанавливаем дату согласования (если объект согласован, а даты нет)
                new DQLUpdateBuilder(event.getMultitude().getEntityMeta().getEntityClass())
                .set("approvalDate", value(new Date(), PropertyType.TIMESTAMP))
                // по всем // .where(event.getMultitude().getInExpression())
                .where(isNull(property("approvalDate")))
                .where(eq(property("state.code"), value(EcgDistributionStateCodes.APPROVED)))
                .createStatement(event.getContext()).execute();

                // убираем дату согласования (если объект не согласован, а дата есть)
                new DQLUpdateBuilder(event.getMultitude().getEntityMeta().getEntityClass())
                .set("approvalDate", nul(PropertyType.TIMESTAMP))
                // по всем // .where(event.getMultitude().getInExpression())
                .where(isNotNull(property("approvalDate")))
                .where(ne(property("state.code"), value(EcgDistributionStateCodes.APPROVED)))
                .createStatement(event.getContext()).execute();

            }
        }
    }

    public void init() {
        final DSetEventManager manager = DSetEventManager.getInstance();

        // основное распределение
        manager.registerListener(DSetEventType.afterInsert, EcgDistribution.class, StateChangeListener.INSTANCE);
        manager.registerListener(DSetEventType.afterUpdate, EcgDistribution.class, StateChangeListener.INSTANCE);

        // уточтяющее распределение
        manager.registerListener(DSetEventType.afterInsert, EcgDistributionDetail.class, StateChangeListener.INSTANCE);
        manager.registerListener(DSetEventType.afterUpdate, EcgDistributionDetail.class, StateChangeListener.INSTANCE);

    }

}
