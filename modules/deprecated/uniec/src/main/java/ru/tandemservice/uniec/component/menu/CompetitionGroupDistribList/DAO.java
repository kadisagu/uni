/* $Id: DAO.java 8737 2009-06-26 11:27:38Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.menu.CompetitionGroupDistribList;

import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.NumberAsStringComparator;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.DelegatePropertyComparator;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.dao.ecg.IEnrollmentCompetitionGroupDAO;
import ru.tandemservice.uniec.entity.ecg.EcgDistribObject;
import ru.tandemservice.uniec.entity.entrant.CompetitionGroup;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.*;

/**
 * @author vdanilov
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        final MQBuilder builder = new MQBuilder(EnrollmentCampaign.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", EnrollmentCampaign.P_USE_COMPETITION_GROUP, Boolean.TRUE));
        builder.addOrder("e", IEntity.P_ID, OrderDirection.desc);

        final List<EnrollmentCampaign> list = builder.getResultList(getSession());
        if ((model.getEnrollmentCampaign() == null) && !list.isEmpty())
            model.setEnrollmentCampaign(list.get(0));
        model.setEnrollmentCampaignList(list);
        model.setCompetitionGroupList(getList(CompetitionGroup.class, CompetitionGroup.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
        Collections.sort(model.getCompetitionGroupList(), new DelegatePropertyComparator(NumberAsStringComparator.INSTANCE, CompetitionGroup.title().s()));
        model.setCompensationTypeList(getCatalogItemListOrderByCode(CompensationType.class));
        model.setCategoryList(Arrays.asList(new IdentifiableWrapper(0L, "Студент/Слушатель"), new IdentifiableWrapper(1L, "Второе высшее")));
        if (model.getSettings().get("category") == null)
            model.getSettings().set("category", new IdentifiableWrapper(0L, "Студент/Слушатель"));
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareListDataSource(final Model model)
    {
        EnrollmentCampaign enrollmentCampaign = model.getEnrollmentCampaign();
        IdentifiableWrapper category = model.getSettings().get("category");
        if (enrollmentCampaign == null || category == null)
        {
            model.getDataSource().setCountRow(0);
            UniBaseUtils.createPage(model.getDataSource(), new ArrayList<>());
            return;
        }

        Map<CompetitionGroup, Map<CompensationType, List<EcgDistribObject>>> groupMap = new HashMap<>();

        // значения фильтров
        CompetitionGroup competitionGroupFilter = model.getSettings().get("competitionGroup");
        CompensationType compensationTypeFilter = model.getSettings().get("compensationType");
        Boolean secondHigh = category.getId().equals(1L) ? Boolean.TRUE : Boolean.FALSE;


        // выбираем уже созданные распределения
        final Map<EcgDistribObject, List<EcgDistribObject>> distribHierarchy = SafeMap.get(key -> new ArrayList<>(4));
        {
            MQBuilder distribBuilder = new MQBuilder(EcgDistribObject.ENTITY_CLASS, "d");
            distribBuilder.add(MQExpression.eq("d", EcgDistribObject.competitionGroup().enrollmentCampaign().s(), enrollmentCampaign));
            if (competitionGroupFilter != null)
                distribBuilder.add(MQExpression.eq("d", EcgDistribObject.competitionGroup().s(), competitionGroupFilter));
            if (compensationTypeFilter != null)
                distribBuilder.add(MQExpression.eq("d", EcgDistribObject.compensationType().s(), compensationTypeFilter));
            distribBuilder.add(MQExpression.eq("d", EcgDistribObject.secondHighAdmission().s(), secondHigh));
            distribBuilder.addOrder("d", EcgDistribObject.competitionGroup().id().s());
            distribBuilder.addOrder("d", EcgDistribObject.compensationType().code().s());
            distribBuilder.addOrder("d", EcgDistribObject.registrationDate().s());
            for (EcgDistribObject element : distribBuilder.<EcgDistribObject>getResultList(getSession()))
                distribHierarchy.get(element.getParent()).add(element);
        }
        // иерархически сортируем, распределяем в группы
        collect(groupMap, distribHierarchy, null);

        // добавляем конкурсные группы, на которые нет распределений
        {
            final MQBuilder groupBuilder = new MQBuilder(CompetitionGroup.ENTITY_CLASS, "g");
            groupBuilder.add(MQExpression.eq("g", CompetitionGroup.enrollmentCampaign().s(), enrollmentCampaign));
            if (competitionGroupFilter != null)
                groupBuilder.add(MQExpression.eq("g", CompetitionGroup.id().s(), competitionGroupFilter.getId()));
            for (CompetitionGroup group : groupBuilder.<CompetitionGroup>getResultList(getSession()))
                if (!groupMap.containsKey(group))
                    groupMap.put(group, new HashMap<>());
        }

        // список видов затрат в соответствии с фильтром
        List<CompensationType> compensationTypes = new ArrayList<>();
        CompensationType budget = getCatalogItem(CompensationType.class, UniDefines.COMPENSATION_TYPE_BUDGET);
        CompensationType contract = getCatalogItem(CompensationType.class, UniDefines.COMPENSATION_TYPE_CONTRACT);
        if (null == compensationTypeFilter || compensationTypeFilter.equals(budget))
            compensationTypes.add(budget);
        if (null == compensationTypeFilter || compensationTypeFilter.equals(contract))
            compensationTypes.add(contract);

        // сортируем группы красиво
        List<CompetitionGroup> groupList = new ArrayList<>(groupMap.keySet());
        Collections.sort(groupList, CompetitionGroup.TITLED_COMPARATOR);

        // формируем строки итогового списка
        List<EcgDistribObject> distribRows = new ArrayList<>();
        for (CompetitionGroup group : groupList)
        {
            for (CompensationType compType : compensationTypes)
            {
                List<EcgDistribObject> distributions = groupMap.get(group).get(compType);
                if ((null == distributions) || distributions.isEmpty())
                {
                    // если в группе нет распределений - создаем пустую строку, чтобы было куда добавлять
                    EcgDistribObject distribObject = new EcgDistribObject();
                    distribObject.setId(group.getId());
                    distribObject.setCompetitionGroup(group);
                    distribObject.setCompensationType(compType);
                    distribObject.setSecondHighAdmission(secondHigh);
                    distribRows.add(distribObject);
                }
                else
                    distribRows.addAll(distributions);
            }
        }
        model.getDataSource().setCountRow(distribRows.size());
        EntityOrder entityOrder = model.getDataSource().getEntityOrder();
        if (entityOrder.getKey().equals(EcgDistribObject.competitionGroup().title().s()))
        {
            List<EcgDistribObject> result = distribRows;
            Collections.sort(result, new DelegatePropertyComparator(NumberAsStringComparator.INSTANCE, EcgDistribObject.competitionGroup().title().s()));
            if (entityOrder.getDirection() == OrderDirection.desc)
            {
                Collections.reverse(result);
            }
            UniBaseUtils.createPage(model.getDataSource(), result);
        }
        else
        {
            UniBaseUtils.createPage(model.getDataSource(), distribRows);
        }
    }

    @Override
    public void delete(final Model model, final Long id)
    {
        final IEntity e = get(id);
        if (e instanceof EcgDistribObject)
        {
            final EcgDistribObject distrib = (EcgDistribObject) e;
            IEnrollmentCompetitionGroupDAO.INSTANCE.get().doDelete(distrib);
        }
    }

    @Override
    public void refreshCompetitionGroupFilter(Model model)
    {
        model.setCompetitionGroupList(getList(CompetitionGroup.class, CompetitionGroup.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
        Collections.sort(model.getCompetitionGroupList(), new DelegatePropertyComparator(NumberAsStringComparator.INSTANCE, CompetitionGroup.title().s()));
        model.getSettings().set("competitionGroup", null);
    }

    private static final SafeMap.Callback<CompetitionGroup, Map<CompensationType, List<EcgDistribObject>>> callback_0 = key -> new HashMap<>();

    private static final SafeMap.Callback<CompensationType, List<EcgDistribObject>> callback_1 = key -> new ArrayList<>();

    private void collect(final Map<CompetitionGroup, Map<CompensationType, List<EcgDistribObject>>> result, final Map<EcgDistribObject, List<EcgDistribObject>> hierarchy, final EcgDistribObject current)
    {
        if (hierarchy.containsKey(current))
        {
            for (final EcgDistribObject child : hierarchy.get(current))
            {
                SafeMap.safeGet(SafeMap.safeGet(result, child.getCompetitionGroup(), DAO.callback_0), child.getCompensationType(), DAO.callback_1).add(child);
                collect(result, hierarchy, child);
            }
        }
    }
}
