/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.InfoEduProfileReport.Add;

import org.hibernate.Session;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;

import java.util.Arrays;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vip_delete
 * @since 04.09.2009
 */
class InfoEduProfileReportBuilder
{
    private Model _model;
    private Session _session;

    InfoEduProfileReportBuilder(Model model, Session session)
    {
        _model = model;
        _session = session;
    }

    DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    private byte[] buildReport()
    {
        DQLSelectBuilder rowBuilder = new DQLSelectBuilder()
                .column(new DQLSelectBuilder().fromDataSource(getT00().buildQuery(), "t").buildCountQuery())
                .column(new DQLSelectBuilder().fromDataSource(getT01().buildQuery(), "t").buildCountQuery())
                .column(new DQLSelectBuilder().fromDataSource(getT02().buildQuery(), "t").buildCountQuery())
                .column(new DQLSelectBuilder().fromDataSource(getT03().buildQuery(), "t").buildCountQuery())
                .column(new DQLSelectBuilder().fromDataSource(getT04().buildQuery(), "t").buildCountQuery())
                .column(new DQLSelectBuilder().fromDataSource(getT05().buildQuery(), "t").buildCountQuery())
                .column(new DQLSelectBuilder().fromDataSource(getT06().buildQuery(), "t").buildCountQuery())
                .column(new DQLSelectBuilder().fromDataSource(getT07().buildQuery(), "t").buildCountQuery())
                .column(new DQLSelectBuilder().fromDataSource(getT08().buildQuery(), "t").buildCountQuery())
                .column(new DQLSelectBuilder().fromDataSource(getT09().buildQuery(), "t").buildCountQuery())
                .column(new DQLSelectBuilder().fromDataSource(getT0A().buildQuery(), "t").buildCountQuery())
                .column(new DQLSelectBuilder().fromDataSource(getT0B().buildQuery(), "t").buildCountQuery());

        Object[] rowData = (Object[]) rowBuilder.createStatement(new DQLExecutionContext(_session)).uniqueResult();

        final int T00 = ((Number) rowData[0]).intValue();
        final int T01 = ((Number) rowData[1]).intValue();
        final int T02 = ((Number) rowData[2]).intValue();
        final int T03 = ((Number) rowData[3]).intValue();
        final int T04 = ((Number) rowData[4]).intValue();
        final int T05 = ((Number) rowData[5]).intValue();
        final int T06 = ((Number) rowData[6]).intValue();
        final int T07 = ((Number) rowData[7]).intValue();
        final int T08 = ((Number) rowData[8]).intValue();
        final int T09 = ((Number) rowData[9]).intValue();
        final int T0A = ((Number) rowData[10]).intValue();
        final int T0B = ((Number) rowData[11]).intValue();

        RtfInjectModifier injectModifier = new RtfInjectModifier()
                .put("vuz", TopOrgUnit.getInstance().getTitle())
                .put("year", Integer.toString(_model.getReport().getEnrollmentCampaign().getEducationYear().getIntValue()))
                .put("T00", Integer.toString(T00))
                .put("T01", Integer.toString(T01))
                .put("T02", Integer.toString(T02))
                .put("T03", Integer.toString(T03))
                .put("T04", Integer.toString(T04))
                .put("T05", Integer.toString(T05))
                .put("T06", Integer.toString(T06))
                .put("T07", Integer.toString(T07))
                .put("T08", Integer.toString(T08))
                .put("T09", Integer.toString(T09))
                .put("T0A", Integer.toString(T0A))
                .put("T0B", Integer.toString(T0B))
                .put("T12", T00 == 0 ? null : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format((double) (T02 * 100) / (double) T00))
                .put("T13", T01 == 0 ? null : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format((double) (T03 * 100) / (double) T01))
                .put("T16", T04 == 0 ? null : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format((double) (T06 * 100) / (double) T04))
                .put("T17", T05 == 0 ? null : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format((double) (T07 * 100) / (double) T05))
                .put("T1A", T08 == 0 ? null : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format((double) (T0A * 100) / (double) T08))
                .put("T1B", T09 == 0 ? null : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format((double) (T0B * 100) / (double) T09));

        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_INFO_EDU_PROFILE_REPORT);
        return RtfUtil.toByteArray(templateDocument.getCurrentTemplate(), injectModifier, null);
    }

    private DQLSelectBuilder getT00()
    {
        return getRequestedBuilder(UniDefines.COMPENSATION_TYPE_BUDGET);
    }

    private DQLSelectBuilder getT01()
    {
        return getRequestedBuilder(UniDefines.COMPENSATION_TYPE_CONTRACT);
    }

    private DQLSelectBuilder getT02()
    {
        return getRequestedBuilder(UniDefines.COMPENSATION_TYPE_BUDGET)
                .where(eq(property("entrant." + Entrant.P_PASS_PROFILE_EDUCATION), value(Boolean.TRUE)));
    }

    private DQLSelectBuilder getT03()
    {
        return getRequestedBuilder(UniDefines.COMPENSATION_TYPE_CONTRACT)
                .where(eq(property("entrant." + Entrant.P_PASS_PROFILE_EDUCATION), value(Boolean.TRUE)));
    }

    private DQLSelectBuilder getT04()
    {
        return getRequestedBuilder(UniDefines.COMPENSATION_TYPE_BUDGET)
                .joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_STATE, "state")
                .where(notIn(property("state." + EntrantState.P_CODE), Arrays.asList(
                        UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY,
                        UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE,
                        UniecDefines.ENTRANT_STATE_ACTIVE_CODE
                )));
    }

    private DQLSelectBuilder getT05()
    {
        return getRequestedBuilder(UniDefines.COMPENSATION_TYPE_CONTRACT)
                .joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_STATE, "state")
                .where(notIn(property("state." + EntrantState.P_CODE), Arrays.asList(
                        UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY,
                        UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE,
                        UniecDefines.ENTRANT_STATE_ACTIVE_CODE
                )));
    }

    private DQLSelectBuilder getT06()
    {
        return getRequestedBuilder(UniDefines.COMPENSATION_TYPE_BUDGET)
                .joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_STATE, "state")
                .where(notIn(property("state." + EntrantState.P_CODE), Arrays.asList(
                        UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY,
                        UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE,
                        UniecDefines.ENTRANT_STATE_ACTIVE_CODE
                )))
                .where(eq(property("entrant." + Entrant.P_PASS_PROFILE_EDUCATION), value(Boolean.TRUE)));
    }

    private DQLSelectBuilder getT07()
    {
        return getRequestedBuilder(UniDefines.COMPENSATION_TYPE_CONTRACT)
                .joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_STATE, "state")
                .where(notIn(property("state." + EntrantState.P_CODE), Arrays.asList(
                        UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY,
                        UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE,
                        UniecDefines.ENTRANT_STATE_ACTIVE_CODE
                )))
                .where(eq(property("entrant." + Entrant.P_PASS_PROFILE_EDUCATION), value(Boolean.TRUE)));
    }

    private DQLSelectBuilder getT08()
    {
        return getPreliminaryBuilder(UniDefines.COMPENSATION_TYPE_BUDGET);
    }

    private DQLSelectBuilder getT09()
    {
        return getPreliminaryBuilder(UniDefines.COMPENSATION_TYPE_CONTRACT);
    }

    private DQLSelectBuilder getT0A()
    {
        return getPreliminaryBuilder(UniDefines.COMPENSATION_TYPE_BUDGET)
                .where(eq(property("entrant." + Entrant.P_PASS_PROFILE_EDUCATION), value(Boolean.TRUE)));
    }

    private DQLSelectBuilder getT0B()
    {
        return getPreliminaryBuilder(UniDefines.COMPENSATION_TYPE_CONTRACT)
                .where(eq(property("entrant." + Entrant.P_PASS_PROFILE_EDUCATION), value(Boolean.TRUE)));
    }

    private DQLSelectBuilder getRequestedBuilder(String compType)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.column(value("0"), "x");
        builder.fromEntity(RequestedEnrollmentDirection.class, "d");
        builder.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "request");
        builder.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, "ed");
        builder.joinPath(DQLJoinType.inner, "ed." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "ou");
        builder.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_COMPENSATION_TYPE, "ct");

        patchEduOrgUnit(builder, "d." + RequestedEnrollmentDirection.L_STUDENT_CATEGORY);

        builder.where(ne(property("d." + RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE), UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        builder.where(eq(property("ct." + CompensationType.P_CODE), value(compType)));
        builder.group(property("entrant.id"));
        return builder;
    }

    private DQLSelectBuilder getPreliminaryBuilder(String compType)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.column(value("0"), "x");
        builder.fromEntity(PreliminaryEnrollmentStudent.class, "p");
        builder.joinPath(DQLJoinType.inner, "p." + PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, "d");
        builder.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "request");
        builder.joinPath(DQLJoinType.inner, "p." + PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT, "ou");
        builder.joinPath(DQLJoinType.inner, "p." + PreliminaryEnrollmentStudent.L_COMPENSATION_TYPE, "ct");

        if (_model.isParallelActive() && _model.getParallel().isTrue())
            builder.where(eq(property("p." + PreliminaryEnrollmentStudent.P_PARALLEL), value(Boolean.FALSE)));

        patchEduOrgUnit(builder, "p." + PreliminaryEnrollmentStudent.L_STUDENT_CATEGORY);

        builder.where(ne(property("d." + RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE), UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        builder.where(eq(property("ct." + CompensationType.P_CODE), value(compType)));
        builder.group(property("p.id"));
        return builder;
    }

    private void patchEduOrgUnit(DQLSelectBuilder builder, String studentCategoryAlias)
    {
        builder.joinPath(DQLJoinType.inner, "request." + EntrantRequest.L_ENTRANT, "entrant");
        builder.joinPath(DQLJoinType.inner, "ou." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "hs");
        builder.joinPath(DQLJoinType.inner, "hs." + EducationLevelsHighSchool.L_EDUCATION_LEVEL, "el");
        builder.where(eq(property("entrant." + Entrant.P_ARCHIVAL), value(Boolean.FALSE)));
        builder.where(eq(property("entrant." + Entrant.L_ENROLLMENT_CAMPAIGN), value(_model.getReport().getEnrollmentCampaign())));
        builder.where(betweenDays("request." + EntrantRequest.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));

        if (_model.isStudentCategoryActive())
            builder.where(in(property(studentCategoryAlias), _model.getStudentCategoryList()));
        if (_model.isQualificationActive())
            builder.where(in(property("el." + EducationLevels.L_QUALIFICATION), _model.getQualificationList()));
        if (_model.isFormativeOrgUnitActive())
            builder.where(in(property("ou." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT), _model.getFormativeOrgUnitList()));
        if (_model.isTerritorialOrgUnitActive())
        {
            if (_model.getTerritorialOrgUnitList().isEmpty())
                builder.where(isNull(property("ou." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT)));
            else
                builder.where(in(property("ou." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT), _model.getTerritorialOrgUnitList()));
        }
        if (_model.isDevelopFormActive())
            builder.where(in(property("ou." + EducationOrgUnit.L_DEVELOP_FORM), _model.getDevelopFormList()));
        if (_model.isDevelopConditionActive())
            builder.where(in(property("ou." + EducationOrgUnit.L_DEVELOP_CONDITION), _model.getDevelopConditionList()));
        if (_model.isDevelopTechActive())
            builder.where(in(property("ou." + EducationOrgUnit.L_DEVELOP_TECH), _model.getDevelopTechList()));
        if (_model.isDevelopPeriodActive())
            builder.where(in(property("ou." + EducationOrgUnit.L_DEVELOP_PERIOD), _model.getDevelopPeriodList()));
    }
}
