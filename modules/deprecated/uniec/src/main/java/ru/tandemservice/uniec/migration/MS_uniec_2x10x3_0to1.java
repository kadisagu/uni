package ru.tandemservice.uniec.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniec_2x10x3_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность entrantDailyRatingByED2Report

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr_rating_by_ed2_report_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_4c6b24da"), 
				new DBColumn("enrollmentcampaign_id", DBType.LONG).setNullable(false), 
				new DBColumn("datefrom_p", DBType.TIMESTAMP).setNullable(false), 
				new DBColumn("dateto_p", DBType.TIMESTAMP).setNullable(false), 
				new DBColumn("compensationtype_id", DBType.LONG).setNullable(false), 
				new DBColumn("studentcategorytitle_p", DBType.createVarchar(255)), 
				new DBColumn("onlywithoriginals_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("withoutdocumentinfo_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("withoutdetailsummark_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("showdisciplinetitles_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("shwrqstddrctnprrty_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("qualificationtitle_p", DBType.createVarchar(255)), 
				new DBColumn("developformtitle_p", DBType.createVarchar(255)), 
				new DBColumn("developconditiontitle_p", DBType.createVarchar(255)), 
				new DBColumn("enrollmentdirection_id", DBType.LONG), 
				new DBColumn("notprintspeswithoutrequest_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("notprintnuminfo_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("withoutagree4enrollment_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("competitionkind_id", DBType.LONG), 
				new DBColumn("withouttargetadmission_p", DBType.BOOLEAN).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("entrantDailyRatingByED2Report");

		}


    }
}