package ru.tandemservice.uniec.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Тип приказа на зачисление абитуриента в личный состав студентов
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntrantEnrollmentOrderTypeGen extends EntityBase
 implements INaturalIdentifiable<EntrantEnrollmentOrderTypeGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType";
    public static final String ENTITY_NAME = "entrantEnrollmentOrderType";
    public static final int VERSION_HASH = -1707117949;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_DEFAULT_PRIORITY = "defaultPriority";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_TARGET_ADMISSION = "targetAdmission";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String L_STUDENT_CATEGORY = "studentCategory";
    public static final String P_COMPONENT_PREFIX = "componentPrefix";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private int _defaultPriority; 
    private String _shortTitle;     // Сокращенное название
    private boolean _targetAdmission; 
    private CompensationType _compensationType;     // Вид возмещения затрат
    private StudentCategory _studentCategory;     // Категория обучаемого
    private String _componentPrefix; 
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return  Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public int getDefaultPriority()
    {
        return _defaultPriority;
    }

    /**
     * @param defaultPriority  Свойство не может быть null и должно быть уникальным.
     */
    public void setDefaultPriority(int defaultPriority)
    {
        dirty(_defaultPriority, defaultPriority);
        _defaultPriority = defaultPriority;
    }

    /**
     * @return Сокращенное название.
     */
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public boolean isTargetAdmission()
    {
        return _targetAdmission;
    }

    /**
     * @param targetAdmission  Свойство не может быть null.
     */
    public void setTargetAdmission(boolean targetAdmission)
    {
        dirty(_targetAdmission, targetAdmission);
        _targetAdmission = targetAdmission;
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Категория обучаемого. Свойство не может быть null.
     */
    @NotNull
    public StudentCategory getStudentCategory()
    {
        return _studentCategory;
    }

    /**
     * @param studentCategory Категория обучаемого. Свойство не может быть null.
     */
    public void setStudentCategory(StudentCategory studentCategory)
    {
        dirty(_studentCategory, studentCategory);
        _studentCategory = studentCategory;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getComponentPrefix()
    {
        return _componentPrefix;
    }

    /**
     * @param componentPrefix  Свойство не может быть null.
     */
    public void setComponentPrefix(String componentPrefix)
    {
        dirty(_componentPrefix, componentPrefix);
        _componentPrefix = componentPrefix;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntrantEnrollmentOrderTypeGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EntrantEnrollmentOrderType)another).getCode());
            }
            setDefaultPriority(((EntrantEnrollmentOrderType)another).getDefaultPriority());
            setShortTitle(((EntrantEnrollmentOrderType)another).getShortTitle());
            setTargetAdmission(((EntrantEnrollmentOrderType)another).isTargetAdmission());
            setCompensationType(((EntrantEnrollmentOrderType)another).getCompensationType());
            setStudentCategory(((EntrantEnrollmentOrderType)another).getStudentCategory());
            setComponentPrefix(((EntrantEnrollmentOrderType)another).getComponentPrefix());
            setTitle(((EntrantEnrollmentOrderType)another).getTitle());
        }
    }

    public INaturalId<EntrantEnrollmentOrderTypeGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EntrantEnrollmentOrderTypeGen>
    {
        private static final String PROXY_NAME = "EntrantEnrollmentOrderTypeNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EntrantEnrollmentOrderTypeGen.NaturalId) ) return false;

            EntrantEnrollmentOrderTypeGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntrantEnrollmentOrderTypeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntrantEnrollmentOrderType.class;
        }

        public T newInstance()
        {
            return (T) new EntrantEnrollmentOrderType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "defaultPriority":
                    return obj.getDefaultPriority();
                case "shortTitle":
                    return obj.getShortTitle();
                case "targetAdmission":
                    return obj.isTargetAdmission();
                case "compensationType":
                    return obj.getCompensationType();
                case "studentCategory":
                    return obj.getStudentCategory();
                case "componentPrefix":
                    return obj.getComponentPrefix();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "defaultPriority":
                    obj.setDefaultPriority((Integer) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "targetAdmission":
                    obj.setTargetAdmission((Boolean) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "studentCategory":
                    obj.setStudentCategory((StudentCategory) value);
                    return;
                case "componentPrefix":
                    obj.setComponentPrefix((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "defaultPriority":
                        return true;
                case "shortTitle":
                        return true;
                case "targetAdmission":
                        return true;
                case "compensationType":
                        return true;
                case "studentCategory":
                        return true;
                case "componentPrefix":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "defaultPriority":
                    return true;
                case "shortTitle":
                    return true;
                case "targetAdmission":
                    return true;
                case "compensationType":
                    return true;
                case "studentCategory":
                    return true;
                case "componentPrefix":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "defaultPriority":
                    return Integer.class;
                case "shortTitle":
                    return String.class;
                case "targetAdmission":
                    return Boolean.class;
                case "compensationType":
                    return CompensationType.class;
                case "studentCategory":
                    return StudentCategory.class;
                case "componentPrefix":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntrantEnrollmentOrderType> _dslPath = new Path<EntrantEnrollmentOrderType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntrantEnrollmentOrderType");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return  Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType#getDefaultPriority()
     */
    public static PropertyPath<Integer> defaultPriority()
    {
        return _dslPath.defaultPriority();
    }

    /**
     * @return Сокращенное название.
     * @see ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType#isTargetAdmission()
     */
    public static PropertyPath<Boolean> targetAdmission()
    {
        return _dslPath.targetAdmission();
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Категория обучаемого. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType#getStudentCategory()
     */
    public static StudentCategory.Path<StudentCategory> studentCategory()
    {
        return _dslPath.studentCategory();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType#getComponentPrefix()
     */
    public static PropertyPath<String> componentPrefix()
    {
        return _dslPath.componentPrefix();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EntrantEnrollmentOrderType> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<Integer> _defaultPriority;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<Boolean> _targetAdmission;
        private CompensationType.Path<CompensationType> _compensationType;
        private StudentCategory.Path<StudentCategory> _studentCategory;
        private PropertyPath<String> _componentPrefix;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EntrantEnrollmentOrderTypeGen.P_CODE, this);
            return _code;
        }

    /**
     * @return  Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType#getDefaultPriority()
     */
        public PropertyPath<Integer> defaultPriority()
        {
            if(_defaultPriority == null )
                _defaultPriority = new PropertyPath<Integer>(EntrantEnrollmentOrderTypeGen.P_DEFAULT_PRIORITY, this);
            return _defaultPriority;
        }

    /**
     * @return Сокращенное название.
     * @see ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(EntrantEnrollmentOrderTypeGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType#isTargetAdmission()
     */
        public PropertyPath<Boolean> targetAdmission()
        {
            if(_targetAdmission == null )
                _targetAdmission = new PropertyPath<Boolean>(EntrantEnrollmentOrderTypeGen.P_TARGET_ADMISSION, this);
            return _targetAdmission;
        }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Категория обучаемого. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType#getStudentCategory()
     */
        public StudentCategory.Path<StudentCategory> studentCategory()
        {
            if(_studentCategory == null )
                _studentCategory = new StudentCategory.Path<StudentCategory>(L_STUDENT_CATEGORY, this);
            return _studentCategory;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType#getComponentPrefix()
     */
        public PropertyPath<String> componentPrefix()
        {
            if(_componentPrefix == null )
                _componentPrefix = new PropertyPath<String>(EntrantEnrollmentOrderTypeGen.P_COMPONENT_PREFIX, this);
            return _componentPrefix;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EntrantEnrollmentOrderTypeGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EntrantEnrollmentOrderType.class;
        }

        public String getEntityName()
        {
            return "entrantEnrollmentOrderType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
