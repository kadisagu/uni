/* $Id$ */
package ru.tandemservice.uniec.component.catalog.entrantCustomStateCI.EntrantCustomStateCIItemPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogItemPub.IDefaultCatalogItemPubDAO;
import ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI;

/**
 * @author nvankov
 * @since 4/3/13
 */
public interface IDAO extends IDefaultCatalogItemPubDAO<EntrantCustomStateCI, Model>
{
}
