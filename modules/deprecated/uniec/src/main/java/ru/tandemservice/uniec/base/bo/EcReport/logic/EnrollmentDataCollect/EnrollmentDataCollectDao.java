/**
 *$Id$
 */
package ru.tandemservice.uniec.base.bo.EcReport.logic.EnrollmentDataCollect;

import com.google.common.base.Function;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.*;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import jxl.write.Number;
import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.commonbase.base.util.key.TripletKey;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.codes.EntranceDisciplineTypeCodes;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.examset.EnrollmentDirectionExamSetData;
import ru.tandemservice.uniec.entity.examset.ExamSetItem;
import ru.tandemservice.uniec.entity.examset.ExamSetStructure;
import ru.tandemservice.uniec.entity.examset.ExamSetStructureItem;
import ru.tandemservice.uniec.entity.report.EnrollmentDataCollectReport;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.ExamSetUtil;
import ru.tandemservice.uniec.util.MarkDistributionUtil;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.Boolean;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

import static org.tandemframework.hibsupport.builder.expression.MQExpression.*;

/**
 * @author Alexander Shaburov
 * @since 15.08.13
 */
public class EnrollmentDataCollectDao extends UniBaseDao implements IEnrollmentDataCollectDao
{
    public static final String COLUMN_11 = "11";
    public static final String COLUMN_12 = "12";
    public static final String COLUMN_13 = "13";
    public static final String COLUMN_14 = "14";
    public static final String COLUMN_15 = "15";
    public static final String COLUMN_16 = "16";
    public static final String COLUMN_17 = "17";
    public static final String COLUMN_18 = "18";
    public static final String COLUMN_19 = "19";
    public static final String COLUMN_20 = "20";
    public static final String COLUMN_21 = "21";
    public static final String COLUMN_22 = "22";
    public static final String COLUMN_23 = "23";
    public static final String COLUMN_24 = "24";
    public static final String COLUMN_25 = "25";
    public static final String COLUMN_26 = "26";
    public static final String COLUMN_27 = "27";
    public static final String COLUMN_28 = "28";
    public static final String COLUMN_28_14 = "28_14";
    public static final String COLUMN_28_BEYOND_COMP_AND_NOT_TA = "28_BEYOND_NOT_TA";
    public static final String COLUMN_28_TA = "28_TA";



    @Override
    @SuppressWarnings("unchecked")
    public <T extends EnrollmentDataCollectReportModel> T createModel()
    {
        final EnrollmentDataCollectReportModel model = new EnrollmentDataCollectReportModel();

        final EnrollmentCampaign enrCamp = new DQLSelectBuilder().fromEntity(EnrollmentCampaign.class, "e").column("e")
        .order(DQLExpressions.property(EnrollmentCampaign.id().fromAlias("e")), OrderDirection.desc)
        .createStatement(getSession())
        .setMaxResults(1)
        .uniqueResult();
        model.setEnrollmentCampaign(enrCamp);
        model.setRequestFrom(enrCamp.getStartDate());
        model.setRequestTo(CoreDateUtils.getToday());

        return (T) model;

    }

    @Override
    public <T extends EnrollmentDataCollectReportModel> EnrollmentDataCollectReport createReport(T model, DatabaseFile reportFile)
    {
        final UniEduProgramEducationOrgUnitAddon eduFiltersUtil = model.getEduFiltersUtil();

        EnrollmentDataCollectReport report = new EnrollmentDataCollectReport();
        report.setContent(reportFile);
        report.setFormingDate(new Date());
        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setRequestDateFrom(model.getRequestFrom());
        report.setRequestDateTo(model.getRequestTo());

        if (model.isStudentCategoryActive())
            report.setStudentCategoryText(UniStringUtils.join(model.getStudentCategoryList(), StudentCategory.title().s(), "; "));
        if (model.isCompensTypeActive())
            report.setCompensationTypeText(model.getCompensationType().getTitle());
        if (eduFiltersUtil.getFilterConfig(UniEduProgramEducationOrgUnitAddon.Filters.QUALIFICATION).isCheckEnableCheckBox())
            report.setQualificationText(UniStringUtils.join((Collection) eduFiltersUtil.getValuesMap().get(UniEduProgramEducationOrgUnitAddon.Filters.QUALIFICATION), Qualifications.title().s(), "; "));
        if (eduFiltersUtil.getFilterConfig(UniEduProgramEducationOrgUnitAddon.Filters.FORMATIVE_ORG_UNIT).isCheckEnableCheckBox())
            report.setFormativeOrgUnitText(UniStringUtils.join((Collection) eduFiltersUtil.getValuesMap().get(UniEduProgramEducationOrgUnitAddon.Filters.FORMATIVE_ORG_UNIT), OrgUnit.title().s(), "; "));
        if (eduFiltersUtil.getFilterConfig(UniEduProgramEducationOrgUnitAddon.Filters.TERRITORIAL_ORG_UNIT).isCheckEnableCheckBox())
            report.setTerritorialOrgUnitText(UniStringUtils.join((Collection) eduFiltersUtil.getValuesMap().get(UniEduProgramEducationOrgUnitAddon.Filters.TERRITORIAL_ORG_UNIT), OrgUnit.title().s(), "; "));
        if (eduFiltersUtil.getFilterConfig(UniEduProgramEducationOrgUnitAddon.Filters.EDUCATION_LEVEL_HIGH_SCHOOL).isCheckEnableCheckBox())
            report.setEducationOrgUnit(UniStringUtils.join((Collection) eduFiltersUtil.getValuesMap().get(UniEduProgramEducationOrgUnitAddon.Filters.EDUCATION_LEVEL_HIGH_SCHOOL), EducationLevelsHighSchool.title().s(), "; "));
        if (eduFiltersUtil.getFilterConfig(UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_FORM).isCheckEnableCheckBox())
            report.setDevelopFormText(UniStringUtils.join((Collection) eduFiltersUtil.getValuesMap().get(UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_FORM), DevelopForm.title().s(), "; "));
        if (eduFiltersUtil.getFilterConfig(UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_CONDITION).isCheckEnableCheckBox())
            report.setDevelopConditionText(UniStringUtils.join((Collection) eduFiltersUtil.getValuesMap().get(UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_CONDITION), DevelopCondition.title().s(), "; "));
        if (eduFiltersUtil.getFilterConfig(UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_TECH).isCheckEnableCheckBox())
            report.setDevelopTechText(UniStringUtils.join((Collection) eduFiltersUtil.getValuesMap().get(UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_TECH), DevelopTech.title().s(), "; "));
        if (eduFiltersUtil.getFilterConfig(UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_PERIOD).isCheckEnableCheckBox())
            report.setDevelopPeriodText(UniStringUtils.join((Collection) eduFiltersUtil.getValuesMap().get(UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_PERIOD), DevelopPeriod.title().s(), "; "));
        if (model.isExcludeParallelActive())
            report.setExcludeParallelEdu(TwinComboDataSourceHandler.getSelectedValueNotNull(model.getExcludeParallel()));

        return report;
    }

    @Override
    public <T extends EnrollmentDataCollectReportModel> DatabaseFile createPrintReportFile(T model) throws IOException, WriteException
    {
        prepareReportModel(model);

        validate(model);

        // создаем печатную форму
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        WritableWorkbook workbook = Workbook.createWorkbook(out, ws);

        // создаем лист
        WritableSheet sheet = workbook.createSheet("Отчет", 0);

        // заголовок
        writeHeader(sheet, model);

        // таблица
        writeTable(sheet, model);

        workbook.write();
        workbook.close();

        // создаем файл
        DatabaseFile databaseFile = new DatabaseFile();
        databaseFile.setContent(out.toByteArray());
        databaseFile.setFilename("Report");
        databaseFile.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_VND_MS_EXCEL);

        out.close();

        return databaseFile;
    }

    /**
     * Подготавливает данные необходимые для печати отчета.
     * @param model модель
     * @return модель
     */
    @SuppressWarnings("unchecked")
    protected <M extends EnrollmentDataCollectReportModel> M prepareReportModel(M model)
    {
        final UniEduProgramEducationOrgUnitAddon eduFiltersUtil = model.getEduFiltersUtil();

        final MQBuilder builder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "p")
        .add(eq("p", PreliminaryEnrollmentStudent.requestedEnrollmentDirection().enrollmentDirection().enrollmentCampaign().s(), model.getEnrollmentCampaign()))
        .add(greatOrEq("p", PreliminaryEnrollmentStudent.requestedEnrollmentDirection().regDate().s(), model.getRequestFrom()))
        .add(less("p", PreliminaryEnrollmentStudent.requestedEnrollmentDirection().regDate().s(), CoreDateUtils.add(CommonBaseDateUtil.trimTime(model.getRequestTo()), Calendar.DATE, 1)));

        if (model.isStudentCategoryActive())
            builder.add(in("p", PreliminaryEnrollmentStudent.studentCategory().s(), model.getStudentCategoryList()));
        if (model.isCompensTypeActive())
            builder.add(eq("p", PreliminaryEnrollmentStudent.compensationType().s(), model.getCompensationType()));
        if (eduFiltersUtil.getFilterConfig(UniEduProgramEducationOrgUnitAddon.Filters.QUALIFICATION).isCheckEnableCheckBox())
            builder.add(in("p", PreliminaryEnrollmentStudent.requestedEnrollmentDirection().enrollmentDirection().educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().s(), (Collection) eduFiltersUtil.getValuesMap().get(UniEduProgramEducationOrgUnitAddon.Filters.QUALIFICATION)));
        if (eduFiltersUtil.getFilterConfig(UniEduProgramEducationOrgUnitAddon.Filters.FORMATIVE_ORG_UNIT).isCheckEnableCheckBox())
            builder.add(in("p", PreliminaryEnrollmentStudent.requestedEnrollmentDirection().enrollmentDirection().educationOrgUnit().formativeOrgUnit().s(), (Collection) eduFiltersUtil.getValuesMap().get(UniEduProgramEducationOrgUnitAddon.Filters.FORMATIVE_ORG_UNIT)));
        if (eduFiltersUtil.getFilterConfig(UniEduProgramEducationOrgUnitAddon.Filters.TERRITORIAL_ORG_UNIT).isCheckEnableCheckBox())
            builder.add(in("p", PreliminaryEnrollmentStudent.requestedEnrollmentDirection().enrollmentDirection().educationOrgUnit().territorialOrgUnit().s(), (Collection) eduFiltersUtil.getValuesMap().get(UniEduProgramEducationOrgUnitAddon.Filters.TERRITORIAL_ORG_UNIT)));
        if (eduFiltersUtil.getFilterConfig(UniEduProgramEducationOrgUnitAddon.Filters.EDUCATION_LEVEL_HIGH_SCHOOL).isCheckEnableCheckBox())
            builder.add(in("p", PreliminaryEnrollmentStudent.requestedEnrollmentDirection().enrollmentDirection().educationOrgUnit().educationLevelHighSchool().s(), (Collection) eduFiltersUtil.getValuesMap().get(UniEduProgramEducationOrgUnitAddon.Filters.EDUCATION_LEVEL_HIGH_SCHOOL)));
        if (eduFiltersUtil.getFilterConfig(UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_FORM).isCheckEnableCheckBox())
            builder.add(in("p", PreliminaryEnrollmentStudent.requestedEnrollmentDirection().enrollmentDirection().educationOrgUnit().developForm().s(), (Collection) eduFiltersUtil.getValuesMap().get(UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_FORM)));
        if (eduFiltersUtil.getFilterConfig(UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_CONDITION).isCheckEnableCheckBox())
            builder.add(in("p", PreliminaryEnrollmentStudent.requestedEnrollmentDirection().enrollmentDirection().educationOrgUnit().developCondition().s(), (Collection) eduFiltersUtil.getValuesMap().get(UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_CONDITION)));
        if (eduFiltersUtil.getFilterConfig(UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_TECH).isCheckEnableCheckBox())
            builder.add(in("p", PreliminaryEnrollmentStudent.requestedEnrollmentDirection().enrollmentDirection().educationOrgUnit().developTech().s(), (Collection) eduFiltersUtil.getValuesMap().get(UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_TECH)));
        if (eduFiltersUtil.getFilterConfig(UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_PERIOD).isCheckEnableCheckBox())
            builder.add(in("p", PreliminaryEnrollmentStudent.requestedEnrollmentDirection().enrollmentDirection().educationOrgUnit().developPeriod().s(), (Collection) eduFiltersUtil.getValuesMap().get(UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_PERIOD)));
        if (model.isExcludeParallelActive())
            if (TwinComboDataSourceHandler.getSelectedValueNotNull(model.getExcludeParallel()))
                builder.add(notEq("p", PreliminaryEnrollmentStudent.parallel().s(), true));

        final List<PreliminaryEnrollmentStudent> preStudList = builder.getResultList(getSession());

        final EntrantDataUtil entrantDataUtil = new EntrantDataUtil(getSession(), model.getEnrollmentCampaign(), getRequestedEnrollmentDirectionBuilder(builder, "p"));
        final Map<EnrollmentDirection, EnrollmentDirectionExamSetData> directionExamSetDataMap = ExamSetUtil.getDirectionExamSetDataByTypeAndCategoryMap(getSession(), model.getEnrollmentCampaign(), null, null);

        final Map<TripletKey<EducationLevels, CompensationType, DevelopForm>, List<PreliminaryEnrollmentStudent>> rowMap = SafeMap.get(ArrayList.class);
        for (PreliminaryEnrollmentStudent preEnrollStudent : preStudList)
        {
            rowMap.get(new TripletKey<>(preEnrollStudent.getRequestedEnrollmentDirection().getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel(), preEnrollStudent.getCompensationType(), preEnrollStudent.getRequestedEnrollmentDirection().getEnrollmentDirection().getEducationOrgUnit().getDevelopForm()))
            .add(preEnrollStudent);
        }

        final TreeMap<TripletKey<EducationLevels, CompensationType, DevelopForm>, EnrollmentDataCollectReportSumData> reportMap = new TreeMap<>((o1, o2) -> {
            // по названию НПМ с учетом ОКСО
            int result = o1.getFirst().getDisplayableTitle().compareTo(o2.getFirst().getDisplayableTitle());
            // по форме освоения (по коду в справочнике)
            if (result == 0)
                result = o1.getSecond().getCode().compareTo(o2.getSecond().getCode());
            // по виду затрат (вначале бюджет, потом договор)
            if (result == 0)
            {
                if (o1.getThird().getCode().equals(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET) && !o2.getThird().getCode().equals(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET))
                    result = -1;
                else if ((o2.getThird().getCode().equals(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET) && !o1.getThird().getCode().equals(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET)))
                    result = 1;
            }

            return result;
        });
        for (final Map.Entry<TripletKey<EducationLevels, CompensationType, DevelopForm>, List<PreliminaryEnrollmentStudent>> entry : rowMap.entrySet())
        {
            final EnrollmentDataCollectReportSumData summaryDataWrapper = new EnrollmentDataCollectReportSumData();

            final Set<EnrollmentDirection> uniqEnrDirContract11Set = new HashSet<>();
            final Set<EnrollmentDirection> uniqEnrDirBudget11Set = new HashSet<>();
            final Set<EnrollmentDirection> uniqEnrDir12Set = new HashSet<>();

            summaryDataWrapper.addIncrement(COLUMN_11, input -> {
                final EnrollmentDirection enrollmentDirection = input.getRequestedEnrollmentDirection().getEnrollmentDirection();

                if (input.getCompensationType().isBudget())
                {
                    if (uniqEnrDirBudget11Set.add(enrollmentDirection))
                        summaryDataWrapper.getValue(COLUMN_11).add(enrollmentDirection.getMinisterialPlan() == null ? 0 : enrollmentDirection.getMinisterialPlan());
                }
                else
                {
                    if (uniqEnrDirContract11Set.add(enrollmentDirection))
                        summaryDataWrapper.getValue(COLUMN_11).add(enrollmentDirection.getContractPlan() == null ? 0 : enrollmentDirection.getContractPlan());
                }

                return false;
            });
            summaryDataWrapper.addIncrement(COLUMN_12, input -> {
                if (input.getCompensationType().isBudget())
                {
                    final EnrollmentDirection enrollmentDirection = input.getRequestedEnrollmentDirection().getEnrollmentDirection();
                    if (uniqEnrDir12Set.add(enrollmentDirection))
                        summaryDataWrapper.getValue(COLUMN_12).add(enrollmentDirection.getTargetAdmissionPlanBudget() == null ? 0 : enrollmentDirection.getTargetAdmissionPlanBudget());
                }

                return false;
            });

            summaryDataWrapper.addIncrement(COLUMN_13, input -> true);
            summaryDataWrapper.addIncrement(COLUMN_14, this::isCompetitiveAdmissionWithoutTA);
            summaryDataWrapper.addIncrement(COLUMN_15, input -> isCompetitiveAdmissionWithoutTA(input) && isExamState(entrantDataUtil, input) && !isCreative(directionExamSetDataMap, input) && !isOlympiadMarkSource(entrantDataUtil, input.getRequestedEnrollmentDirection()));
            summaryDataWrapper.addIncrement(COLUMN_16, input -> isCompetitiveAdmissionWithoutTA(input) && isOlympiadMarkSource(entrantDataUtil, input.getRequestedEnrollmentDirection()));
            summaryDataWrapper.addIncrement(COLUMN_17, input -> isCompetitiveAdmissionWithoutTA(input) && !isExamState(entrantDataUtil, input));
            summaryDataWrapper.addIncrement(COLUMN_18, input -> isCompetitiveAdmissionWithoutTA(input) && isExamState(entrantDataUtil, input) && !isOlympiadMarkSource(entrantDataUtil, input.getRequestedEnrollmentDirection()) && isCreative(directionExamSetDataMap, input));

            summaryDataWrapper.addIncrement(COLUMN_19, input -> isTargetAdmission(input) && isExamState(entrantDataUtil, input));
            summaryDataWrapper.addIncrement(COLUMN_20, input -> isBeyondCompetition(input) && !isTargetAdmission(input) && isExamState(entrantDataUtil, input));
            summaryDataWrapper.addIncrement(COLUMN_21, (Function<PreliminaryEnrollmentStudent, Boolean>) input -> isWithoutEntranceDisciplines(input) && !isTargetAdmission(input));

            summaryDataWrapper.addIncrement(COLUMN_22, input -> {
                // берем студентов пред.зачисления учтенных в колонках 15
                if (isCompetitiveAdmissionWithoutTA(input) && isExamState(entrantDataUtil, input) && !isCreative(directionExamSetDataMap, input) && !isOlympiadMarkSource(entrantDataUtil, input.getRequestedEnrollmentDirection()))
                {
                    final Double avrMark = calcAvrMarkWithoutCreative(input, entrantDataUtil, directionExamSetDataMap);
                    if (avrMark != null)
                    {
                        summaryDataWrapper.getValue(COLUMN_22).add(avrMark);
                        return true;
                    }
                }

                // и 18
                if (isCompetitiveAdmissionWithoutTA(input) && isExamState(entrantDataUtil, input) && !isOlympiadMarkSource(entrantDataUtil, input.getRequestedEnrollmentDirection()) && isCreative(directionExamSetDataMap, input))
                {
                    final Double avrMark = calcAvrMarkWithoutCreative(input, entrantDataUtil, directionExamSetDataMap);
                    if (avrMark != null)
                    {
                        summaryDataWrapper.getValue(COLUMN_22).add(avrMark);
                        return true;
                    }
                }

                return false;
            });
            summaryDataWrapper.addIncrement(COLUMN_23, input -> {
                // берем студентов пред.зачисления учтенных в колонке 16
                if (isCompetitiveAdmissionWithoutTA(input) && isOlympiadMarkSource(entrantDataUtil, input.getRequestedEnrollmentDirection()))
                {
                    final Double avrMark = calcAvrMarkWithoutCreative(input, entrantDataUtil, directionExamSetDataMap);
                    if (avrMark != null)
                    {
                        summaryDataWrapper.getValue(COLUMN_23).add(avrMark);
                        return true;
                    }
                }

                return false;
            });
            summaryDataWrapper.addIncrement(COLUMN_24, input -> {
                // берем студентов пред.зачисления учтенных в колонках 15
                if (isCompetitiveAdmissionWithoutTA(input) && isExamState(entrantDataUtil, input) && !isCreative(directionExamSetDataMap, input) && !isOlympiadMarkSource(entrantDataUtil, input.getRequestedEnrollmentDirection()))
                {
                    final double avrMark = calcAvrMark(input, entrantDataUtil, directionExamSetDataMap);
                    summaryDataWrapper.getValue(COLUMN_24).add(avrMark);
                    return true;
                }

                // и 18
                if (isCompetitiveAdmissionWithoutTA(input) && isExamState(entrantDataUtil, input) && !isOlympiadMarkSource(entrantDataUtil, input.getRequestedEnrollmentDirection()) && isCreative(directionExamSetDataMap, input))
                {
                    final double avrMark = calcAvrMark(input, entrantDataUtil, directionExamSetDataMap);
                    summaryDataWrapper.getValue(COLUMN_24).add(avrMark);
                    return true;
                }

                return false;
            });
            summaryDataWrapper.addIncrement(COLUMN_25, input -> {
                // берем студентов пред.зачисления учтенных в колонке 16
                if (isCompetitiveAdmissionWithoutTA(input) && isOlympiadMarkSource(entrantDataUtil, input.getRequestedEnrollmentDirection()))
                {
                    final double avrMark = calcAvrMark(input, entrantDataUtil, directionExamSetDataMap);
                    summaryDataWrapper.getValue(COLUMN_25).add(avrMark);
                    return true;
                }

                return false;
            });
            summaryDataWrapper.addIncrement(COLUMN_26, input -> {
                // берем студентов пред.зачисления учтенных в колонке 19
                if (isTargetAdmission(input) && isExamState(entrantDataUtil, input))
                {
                    final Double avrMark = calcAvrMarkWithoutCreative(input, entrantDataUtil, directionExamSetDataMap);
                    if (avrMark != null)
                    {
                        summaryDataWrapper.getValue(COLUMN_26).add(avrMark);
                        return true;
                    }
                }

                return false;
            });
            summaryDataWrapper.addIncrement(COLUMN_27, input -> {
                // берем студентов пред.зачисления учтенных в колонке 20
                if (isBeyondCompetition(input) && !isTargetAdmission(input) && isExamState(entrantDataUtil, input))
                {
                    final Double avrMark = calcAvrMarkWithoutCreative(input, entrantDataUtil, directionExamSetDataMap);
                    if (avrMark != null)
                    {
                        summaryDataWrapper.getValue(COLUMN_27).add(avrMark);
                        return true;
                    }
                }

                return false;
            });
            summaryDataWrapper.addIncrement(COLUMN_28_14, input -> {
                // берем студентов пред.зачисления учтенных в колонке 14
                if (isCompetitiveAdmissionWithoutTA(input))
                {
                    final double rating = calcRating(input.getRequestedEnrollmentDirection(), entrantDataUtil, directionExamSetDataMap);
                    if (rating == 0)
                        return false;

                    final double beforeRating = summaryDataWrapper.getValue(COLUMN_28_14).doubleValue();

                    if (beforeRating == 0)
                        summaryDataWrapper.getValue(COLUMN_28_14).setValue(rating);
                    else if (rating < beforeRating)
                        summaryDataWrapper.getValue(COLUMN_28_14).setValue(rating);

                    return true;
                }

                return false;
            });
            summaryDataWrapper.addIncrement(COLUMN_28_TA, input -> {
                // берем студентов пред.зачисления по ЦП
                if (isTargetAdmission(input))
                {
                    final double rating = calcRating(input.getRequestedEnrollmentDirection(), entrantDataUtil, directionExamSetDataMap);
                    if (rating == 0)
                        return false;

                    final double beforeRating = summaryDataWrapper.getValue(COLUMN_28_TA).doubleValue();

                    if (beforeRating == 0)
                        summaryDataWrapper.getValue(COLUMN_28_TA).setValue(rating);
                    else if (rating < beforeRating)
                        summaryDataWrapper.getValue(COLUMN_28_TA).setValue(rating);

                    return true;
                }

                return false;
            });
            summaryDataWrapper.addIncrement(COLUMN_28_BEYOND_COMP_AND_NOT_TA, input -> {
                // берем студентов пред.зачисления вне конкурса и не Цп
                if (isBeyondCompetition(input) && !isTargetAdmission(input))
                {
                    final double rating = calcRating(input.getRequestedEnrollmentDirection(), entrantDataUtil, directionExamSetDataMap);
                    if (rating == 0)
                        return false;

                    final double beforeRating = summaryDataWrapper.getValue(COLUMN_28_BEYOND_COMP_AND_NOT_TA).doubleValue();

                    if (beforeRating == 0)
                        summaryDataWrapper.getValue(COLUMN_28_BEYOND_COMP_AND_NOT_TA).setValue(rating);
                    else if (rating < beforeRating)
                        summaryDataWrapper.getValue(COLUMN_28_BEYOND_COMP_AND_NOT_TA).setValue(rating);

                    return true;
                }

                return false;
            });



            for (PreliminaryEnrollmentStudent reportWrapepr : entry.getValue())
                summaryDataWrapper.inc(reportWrapepr);

            // COLUMN_22
            {
                final double avrMarkSumm = summaryDataWrapper.getValue(COLUMN_22).doubleValue();
                final Set<Long> matched22Ids = summaryDataWrapper.getMatchedIds(COLUMN_22);

                if (matched22Ids.isEmpty())
                    summaryDataWrapper.getValue(COLUMN_22).setValue(0);
                else
                    summaryDataWrapper.getValue(COLUMN_22).setValue(new BigDecimal(avrMarkSumm / matched22Ids.size()).setScale(2, RoundingMode.HALF_UP).doubleValue());
            }

            // COLUMN_23
            {
                final double avrMarkSumm = summaryDataWrapper.getValue(COLUMN_23).doubleValue();
                final Set<Long> matched23Ids = summaryDataWrapper.getMatchedIds(COLUMN_23);

                if (matched23Ids.isEmpty())
                    summaryDataWrapper.getValue(COLUMN_23).setValue(0);
                else
                    summaryDataWrapper.getValue(COLUMN_23).setValue(new BigDecimal(avrMarkSumm / matched23Ids.size()).setScale(2, RoundingMode.HALF_UP).doubleValue());
            }

            // COLUMN_24
            {
                final double avrMarkSumm = summaryDataWrapper.getValue(COLUMN_24).doubleValue();
                final Set<Long> matched24Ids = summaryDataWrapper.getMatchedIds(COLUMN_24);

                if (matched24Ids.isEmpty())
                    summaryDataWrapper.getValue(COLUMN_24).setValue(0);
                else
                    summaryDataWrapper.getValue(COLUMN_24).setValue(new BigDecimal(avrMarkSumm / matched24Ids.size()).setScale(2, RoundingMode.HALF_UP).doubleValue());
            }

            // COLUMN_25
            {
                final double avrMarkSumm = summaryDataWrapper.getValue(COLUMN_25).doubleValue();
                final Set<Long> matched25Ids = summaryDataWrapper.getMatchedIds(COLUMN_25);

                if (matched25Ids.isEmpty())
                    summaryDataWrapper.getValue(COLUMN_25).setValue(0);
                else
                    summaryDataWrapper.getValue(COLUMN_25).setValue(new BigDecimal(avrMarkSumm / matched25Ids.size()).setScale(2, RoundingMode.HALF_UP).doubleValue());
            }

            // COLUMN_26
            {
                final double avrMarkSumm = summaryDataWrapper.getValue(COLUMN_26).doubleValue();
                final Set<Long> matched26Ids = summaryDataWrapper.getMatchedIds(COLUMN_26);

                if (matched26Ids.isEmpty())
                    summaryDataWrapper.getValue(COLUMN_26).setValue(0);
                else
                    summaryDataWrapper.getValue(COLUMN_26).setValue(new BigDecimal(avrMarkSumm / matched26Ids.size()).setScale(2, RoundingMode.HALF_UP).doubleValue());
            }

            // COLUMN_27
            {
                final double avrMarkSumm = summaryDataWrapper.getValue(COLUMN_27).doubleValue();
                final Set<Long> matched27Ids = summaryDataWrapper.getMatchedIds(COLUMN_27);

                if (matched27Ids.isEmpty())
                    summaryDataWrapper.getValue(COLUMN_27).setValue(0);
                else
                    summaryDataWrapper.getValue(COLUMN_27).setValue(new BigDecimal(avrMarkSumm / matched27Ids.size()).setScale(2, RoundingMode.HALF_UP).doubleValue());
            }

            // COLUMN_28
            {
                final Set<Long> matched28_14Ids = summaryDataWrapper.getMatchedIds(COLUMN_28_14);
                final Set<Long> matched28_20Ids = summaryDataWrapper.getMatchedIds(COLUMN_28_BEYOND_COMP_AND_NOT_TA);
                final Set<Long> matched28_19Ids = summaryDataWrapper.getMatchedIds(COLUMN_28_TA);

                if (matched28_14Ids.size() != 0)
                    summaryDataWrapper.getValue(COLUMN_28).setValue(summaryDataWrapper.getValue(COLUMN_28_14).doubleValue());
                else if (matched28_20Ids.size() != 0)
                    summaryDataWrapper.getValue(COLUMN_28).setValue(summaryDataWrapper.getValue(COLUMN_28_BEYOND_COMP_AND_NOT_TA).doubleValue());
                else if (matched28_19Ids.size() != 0)
                    summaryDataWrapper.getValue(COLUMN_28).setValue(summaryDataWrapper.getValue(COLUMN_28_TA).doubleValue());
                else
                    summaryDataWrapper.getValue(COLUMN_28).setValue(0);
            }

            reportMap.put(entry.getKey(), summaryDataWrapper);
        }

        model.setWrapeprListMap(reportMap);

        return model;
    }

    /**
     * Производит необходимые прверки, перед печатью.<p/>
     * Если есть ошибки, то кидается <code>ApplicationException</code>.
     * @param model модель
     */
    protected <M extends EnrollmentDataCollectReportModel> void validate(M model)
    {

    }

    protected  <T extends EnrollmentDataCollectReportModel> void writeHeader(WritableSheet sheet, T model) throws WriteException
    {
        // подготавливаем форматы шрифта
        WritableFont arial14Bold = new WritableFont(WritableFont.ARIAL, 14, WritableFont.BOLD);
        WritableFont arial20Bold = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD);
        WritableFont arial10 = new WritableFont(WritableFont.ARIAL, 10);
        WritableFont arial10Bold = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);

        // подготавливаем формат ячейки
        WritableCellFormat a14BFormat = new WritableCellFormat(arial14Bold);
        a14BFormat.setAlignment(Alignment.CENTRE);
        a14BFormat.setWrap(true);
        WritableCellFormat a20BFormat = new WritableCellFormat(arial20Bold);
        a20BFormat.setAlignment(Alignment.CENTRE);
        WritableCellFormat a10HFormat = new WritableCellFormat(arial10);
        a10HFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
        a10HFormat.setAlignment(Alignment.CENTRE);
        a10HFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        a10HFormat.setWrap(true);
        WritableCellFormat a10VFormat = new WritableCellFormat(arial10);
        a10VFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
        a10VFormat.setOrientation(Orientation.PLUS_90);
        a10VFormat.setAlignment(Alignment.CENTRE);
        a10VFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        a10VFormat.setWrap(true);
        WritableCellFormat a10BFormat = new WritableCellFormat(arial10Bold);
        a10BFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
        a10BFormat.setAlignment(Alignment.CENTRE);

        sheet.mergeCells(0, 0, 28, 0);
        sheet.addCell(new Label(0, 0, "Форма 2", a20BFormat));

        sheet.mergeCells(0, 1, 28, 1);
        sheet.addCell(new Label(0, 1, "Сбор сведений от образовательного учреждения высшего профессионального образования о ходе приема на бюджетные места согласно приказу Минобрнауки России от 29 апреля 2013 г. № 313 и внебюджетные места (договоры с физическими и юридическими лицами с полным возмещением затрат)", a14BFormat));

        final String[] headers = {
            "п/п",
            "Наименование ФОИВ, ГРБС (признак ведомственной подчиненности) или учредителя", "Наименование образовательного учреждения высшего профессионального образования по уставу",
            "Наименование головного вуза, структурного подразделения или филиала (при наличии) образовательного учреждения высшего профессионального образования, где реализуется НПС",
            "Наименование субъекта РФ, в котором находится головной вуз, структурное подразделение или филиал (при наличии) образовательного учреждения высшего профессионального образования, где реализуется НПС",
            "Код направления подготовки (специальности), НПС",
            "Наименование направления подготовки (специальности), НПС",
            "Код уровня подготовки",
            "Форма  обучения",
            "Форма финансирования (Бюджетные места /Договоры с физическими и (или) юридическими лицами с полным возмещением затрат)",
            "Всего мест для приема граждан  (Бюджетные места согласно приказу Минобрнауки России от 29.04.2013 г. № 313/Внебюджетные места (договоры с физическими и юридическими лицами с полным возмещением затрат)",
            "В т.ч.  бюджетные места для целевого приема (при наличии)",
            "Всего зачисленных на  места приема граждан (рассчитывается Центром ГЗГУ)",
            /*hor*/ "В т.ч. из них (столбец 13) зачислено:",
            "по общему конкурсу",
            "по общему конкурсу, имеющих результаты ЕГЭ (без победителей и призеров олимпиад, зачисленных по общему конкурсу с учетом 100 баллов ЕГЭ по олимпиадному предмету)",
            "победители и призеры олимпиад, зачисленные по общему конкурсу с учетом 100 баллов ЕГЭ по олимпиадному предмету",
            "по общему конкурсу, не имеющих результататов ЕГЭ (абитуриенты, сдававшие вступительные испытания, форма которых определяется вузом самостоятельно)",
            "по общему конкурсу, имеющих результаты ЕГЭ и сдававших вступительные испытания творческой и (или) профессиональной направленности (без победителей и призеров олимпиад, зачисленных по общему конкурсу с учетом 100 баллов ЕГЭ по олимпиадному предмету)",
            "по целевому приему, имеющих результаты ЕГЭ",
            "льготные категории, зачисленные вне  конкурса и имеющие результаты ЕГЭ",
            "Победители и призеры олимпиад,  члены сборных команд Российской Федерации, участвовавших в международных олимпиадах по общеобразовательным предметам и сформированных в порядке, определяемом Минобрнауки России, чемпионы и призеры Олимпийских игр, Паралимпийских игр и Сурдлимпийских игр, зачисленные без вступительных испытаний",
            /*hor*/ "Средний балл зачисленных (без учета результата вступительных испытаний, форма которых определяется вузом самостоятельно и дополнительных вступительных испытаний профильной направленности):",
            "по общему конкурсу, имеющих результаты ЕГЭ  без учета вступительных испытаний творческой и (или) профессиональной направленности (без победителей и призеров олимпиад, зачисленных по общему конкурсу с учетом 100 баллов ЕГЭ по олимпиадному предмету)",
            "по общему конкурсу, имеющих результаты ЕГЭ и являющихся победителями и призерами олимпиад, зачисленных по  с учетом 100 баллов ЕГЭ по олимпиадному предмету, без учета вступительных испытаний творческой и (или) профессиональной направленности",
            "по общему конкурсу, имеющих результаты ЕГЭ с учетом вступительных испытаний творческой и (или) профессиональной направленности (без победителей и призеров олимпиад, зачисленных по общему конкурсу с учетом 100 баллов ЕГЭ по олимпиадному предмету)",
            "по общему конкурсу, имеющих результаты ЕГЭ с учетом вступительных испытаний творческой и (или) профессиональной направленности и являющихся победителями и призерами олимпиад, зачисленных по общему конкурсу с учетом 100 баллов ЕГЭ по олимпиадному предмету",
            "по целевому приему, имеющих результаты ЕГЭ без учета вступительных испытаний творческой и (или) профессиональной направленности ",
            "вне  конкурса (льготные категории), имеющих результаты ЕГЭ без учета вступительных испытаний творческой и (или) профессиональной направленности",
            "Проходной балл по НПС, приведенный к 100- бальной шкале ",
        "Примечание (заполняется при необходимости)"};

        int col = 0;
        int number = 1;
        for (String title : headers)
        {
            if (title.equalsIgnoreCase("В т.ч. из них (столбец 13) зачислено:"))
            {
                sheet.mergeCells(col, 2, col + 7, 2);
                sheet.addCell(new Label(col, 2, title, a10HFormat));
                continue;
            }
            if (title.equalsIgnoreCase("Средний балл зачисленных (без учета результата вступительных испытаний, форма которых определяется вузом самостоятельно и дополнительных вступительных испытаний профильной направленности):"))
            {
                sheet.mergeCells(col, 2, col + 5, 2);
                sheet.addCell(new Label(col, 2, title, a10HFormat));
                continue;
            }

            if (col < 13 || col > 26)
            {
                sheet.mergeCells(col, 2, col, 3);
                sheet.addCell(new Label(col, 2, title, a10VFormat));
            }
            else if ((col >= 13 && col < 21) || (col >= 21 && col < 27))
            {
                sheet.addCell(new Label(col, 3, title, a10VFormat));
            }

            sheet.addCell(new Number(col++, 4, number++, a10BFormat));
        }

        sheet.setRowView(1, 3 * 256);
        sheet.setRowView(2, 4 * 256);
        sheet.setRowView(3, 35 * 256);
        sheet.setColumnView(6, 25);
        sheet.setColumnView(28, 30);
    }

    protected  <T extends EnrollmentDataCollectReportModel> void writeTable(WritableSheet sheet, T model) throws WriteException
    {
        WritableFont arial10 = new WritableFont(WritableFont.ARIAL, 10);

        WritableCellFormat a10Format = new WritableCellFormat(arial10);
        a10Format.setBorder(Border.ALL, BorderLineStyle.THIN);

        int row = 5;
        for (Map.Entry<TripletKey<EducationLevels, CompensationType, DevelopForm>, EnrollmentDataCollectReportSumData> entry : model.getWrapeprListMap().entrySet())
        {
            for (int col = 0; col <= 28; col++)
            {
                if (Arrays.asList(1, 2, 3, 4, 28).contains(col))
                {
                    sheet.addCell(new Blank(col, row, a10Format));
                    continue;
                }

                if (col == 0)
                {
                    sheet.addCell(new Number(col, row, row - 4, a10Format));
                }
                else if (col < 10)
                {
                    writeLabelCell(sheet, entry, col, row, a10Format);
                }
                else if (col > 9)
                {
                    writeNumberCell(sheet, entry, col, row, a10Format);
                }
            }

            row++;
        }
    }

    protected void writeLabelCell(WritableSheet sheet, Map.Entry<TripletKey<EducationLevels, CompensationType, DevelopForm>, EnrollmentDataCollectReportSumData> entry, int col, int row, WritableCellFormat format) throws WriteException
    {
        if (col == 5)
        {
            sheet.addCell(new Label(col, row, entry.getKey().getFirst().getTitleCodePrefix(), format));
        }
        else if (col == 6)
        {
            sheet.addCell(new Label(col, row, entry.getKey().getFirst().getTitle(), format));
        }
        else if (col == 7)
        {
            sheet.addCell(new Label(col, row, entry.getKey().getFirst().getSafeQCode(), format));
        }
        else if (col == 8)
        {
            sheet.addCell(new Label(col, row, entry.getKey().getThird().getTitle(), format));
        }
        else if (col == 9)
        {
            sheet.addCell(new Label(col, row, entry.getKey().getSecond().getShortTitle(), format));
        }
        else
            throw new IllegalStateException();

    }

    protected void writeNumberCell(WritableSheet sheet, Map.Entry<TripletKey<EducationLevels, CompensationType, DevelopForm>, EnrollmentDataCollectReportSumData> entry, int col, int row, WritableCellFormat format) throws WriteException
    {
        if (col == 10)
        {
            sheet.addCell(new Number(col, row, entry.getValue().getValue(COLUMN_11).doubleValue(), format));
        }
        else if (col == 11)
        {
            if (!entry.getKey().getSecond().isBudget())
                sheet.addCell(new Number(col, row, 0, format));
            else
                sheet.addCell(new Number(col, row, entry.getValue().getValue(COLUMN_12).doubleValue(), format));
        }
        else if (col == 12)
        {
            sheet.addCell(new Number(col, row, entry.getValue().getMatchedIds(COLUMN_13).size(), format));
        }
        else if (col == 13)
        {
            sheet.addCell(new Number(col, row, entry.getValue().getMatchedIds(COLUMN_14).size(), format));
        }
        else if (col == 14)
        {
            sheet.addCell(new Number(col, row, entry.getValue().getMatchedIds(COLUMN_15).size(), format));
        }
        else if (col == 15)
        {
            sheet.addCell(new Number(col, row, entry.getValue().getMatchedIds(COLUMN_16).size(), format));
        }
        else if (col == 16)
        {
            sheet.addCell(new Number(col, row, entry.getValue().getMatchedIds(COLUMN_17).size(), format));
        }
        else if (col == 17)
        {
            sheet.addCell(new Number(col, row, entry.getValue().getMatchedIds(COLUMN_18).size(), format));

        }
        else if (col == 18)
        {
            sheet.addCell(new Number(col, row, entry.getValue().getMatchedIds(COLUMN_19).size(), format));
        }
        else if (col == 19)
        {
            sheet.addCell(new Number(col, row, entry.getValue().getMatchedIds(COLUMN_20).size(), format));
        }
        else if (col == 20)
        {
            sheet.addCell(new Number(col, row, entry.getValue().getMatchedIds(COLUMN_21).size(), format));
        }
        else if (col == 21)
        {
            final double val = entry.getValue().getValue(COLUMN_22).doubleValue();

            if (val == 0)
                sheet.addCell(new Label(col, row, "-", format));
            else
                sheet.addCell(new Number(col, row, val, format));
        }
        else if (col == 22)
        {
            final double val = entry.getValue().getValue(COLUMN_23).doubleValue();
            if (val == 0)
                sheet.addCell(new Label(col, row, "-", format));
            else
                sheet.addCell(new Number(col, row, val, format));
        }
        else if (col == 23)
        {
            final double val = entry.getValue().getValue(COLUMN_24).doubleValue();

            if (val == 0)
                sheet.addCell(new Label(col, row, "-", format));
            else
                sheet.addCell(new Number(col, row, val, format));
        }
        else if (col == 24)
        {
            final double val = entry.getValue().getValue(COLUMN_25).doubleValue();

            if (val == 0)
                sheet.addCell(new Label(col, row, "-", format));
            else
                sheet.addCell(new Number(col, row, val, format));
        }
        else if (col == 25)
        {
            final double val = entry.getValue().getValue(COLUMN_26).doubleValue();

            if (val == 0)
                sheet.addCell(new Label(col, row, "-", format));
            else
                sheet.addCell(new Number(col, row, val, format));
        }
        else if (col == 26)
        {
            final double val = entry.getValue().getValue(COLUMN_27).doubleValue();

            if (val == 0)
                sheet.addCell(new Label(col, row, "-", format));
            else
                sheet.addCell(new Number(col, row, val, format));
        }
        else if (col == 27)
        {
            sheet.addCell(new Number(col, row, entry.getValue().getValue(COLUMN_28).doubleValue(), format));
        }
        else
            throw new IllegalStateException();
    }

    /**
     * Подготавливает билдер для утили.
     */
    private MQBuilder getRequestedEnrollmentDirectionBuilder(MQBuilder preStudListBuilder, String alias)
    {
        final MQBuilder subId = new MQBuilder(preStudListBuilder);
        subId.addSelect(alias, new String[]{PreliminaryEnrollmentStudent.requestedEnrollmentDirection().id().s()});

        return new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "b")
        .add(in("b", "id", subId));
    }

    protected boolean isCompetitiveAdmissionWithoutTA(PreliminaryEnrollmentStudent input)
    {
        return !isTargetAdmission(input) && input.getRequestedEnrollmentDirection().getCompetitionKind().getCode().equals(UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION);
    }

    protected boolean isBeyondCompetition(PreliminaryEnrollmentStudent input)
    {
        return input.getRequestedEnrollmentDirection().getCompetitionKind().getCode().equals(UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION);
    }

    protected boolean isWithoutEntranceDisciplines(PreliminaryEnrollmentStudent input)
    {
        return input.getRequestedEnrollmentDirection().getCompetitionKind().getCode().equals(UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES);
    }

    protected boolean isOlympiadMarkSource(EntrantDataUtil entrantDataUtil, RequestedEnrollmentDirection reqDir)
    {
        final Set<ChosenEntranceDiscipline> disciplineSet = entrantDataUtil.getChosenEntranceDisciplineSet(reqDir);

        if (disciplineSet.isEmpty())
            return false;

        for (ChosenEntranceDiscipline discipline : disciplineSet)
            if (Integer.valueOf(1).equals(discipline.getFinalMarkSource()))
                return true;

        return false;
    }

    protected boolean isExamState(EntrantDataUtil entrantDataUtil, PreliminaryEnrollmentStudent input)
    {
        final Set<ChosenEntranceDiscipline> disciplineSet = entrantDataUtil.getChosenEntranceDisciplineSet(input.getRequestedEnrollmentDirection());

        if (disciplineSet.isEmpty())
            return false;

        for (ChosenEntranceDiscipline discipline : disciplineSet)
        {
            if (Arrays.asList(8, 9, 10, 11).contains(discipline.getFinalMarkSource()))
                return true;
        }

        return false;
    }

    protected boolean isCreative(Map<EnrollmentDirection, EnrollmentDirectionExamSetData> directionExamSetDataMap, PreliminaryEnrollmentStudent input)
    {
        final EnrollmentDirectionExamSetData examSetData = directionExamSetDataMap.get(input.getRequestedEnrollmentDirection().getEnrollmentDirection());

        for (ExamSetItem item : examSetData.getCategoryExamSet(input.getStudentCategory()))
            //            if (input.getCompensationType().isBudget() == item.isBudget() || (!input.getCompensationType().isBudget() && item.isContract()))
            if (EntranceDisciplineTypeCodes.CREATIVE_PROFESSIONAL.equals(item.getType().getCode()))
                return true;

        return false;
    }

    protected boolean isTargetAdmission(PreliminaryEnrollmentStudent input)
    {
        return input.isTargetAdmission();
    }

    /**
     * Определяет средний балл по всем ВВИ.
     * @return средний балл
     */
    protected double calcAvrMark(PreliminaryEnrollmentStudent input, EntrantDataUtil entrantDataUtil, Map<EnrollmentDirection, EnrollmentDirectionExamSetData> directionExamSetDataMap)
    {
        // получаем информацию по наборам экзаменов для направления приема
        final EnrollmentDirectionExamSetData examSetData = directionExamSetDataMap.get(input.getRequestedEnrollmentDirection().getEnrollmentDirection());

        // получаем структуру набора экзамена для текущего выбранного направления приема
        final ExamSetStructure examSetStructure = examSetData.getCategoryExamSetStructure(input.getStudentCategory());

        // получаем список строк структуры набора экзамена
        final List<ExamSetStructureItem> itemList = examSetStructure.getItemList();

        // находим наилучшее распределение выбранных вступительных испытаний по строкам структуры набора
        final List<ChosenEntranceDiscipline> distributionList = Arrays.asList(MarkDistributionUtil.getChosenDistribution(entrantDataUtil.getChosenEntranceDisciplineSet(input.getRequestedEnrollmentDirection()), itemList));

        double summMark = 0;
        int markCount = 0;

        for (ExamSetStructureItem item : itemList)
        {
            //            if ((item.isBudget() && input.getCompensationType().isBudget()) || (item.isContract() && !input.getCompensationType().isBudget()))
            {
                final ChosenEntranceDiscipline discipline = distributionList.get(itemList.indexOf(item));

                if (discipline == null || discipline.getFinalMark() == null)
                    continue;

                summMark += discipline.getFinalMark();
                markCount++;
            }
        }

        // баллы суммируем и делим на число баллов, округляем до двух знаков после запятой, т.о. получаем средний балл для студента пред.зачисления;
        return new BigDecimal(summMark / markCount).setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    /**
     * Определяет средний балл по всем ВВИ без учета ВИ с типом "творческой и (или) профессиональной направленности".
     * @return null, если все баллы относятся к типу ВИ "творческой и (или) профессиональной направленности",
     * иначе средний балл
     */
    protected Double calcAvrMarkWithoutCreative(PreliminaryEnrollmentStudent input, EntrantDataUtil entrantDataUtil, Map<EnrollmentDirection, EnrollmentDirectionExamSetData> directionExamSetDataMap)
    {
        // получаем информацию по наборам экзаменов для направления приема
        final EnrollmentDirectionExamSetData examSetData = directionExamSetDataMap.get(input.getRequestedEnrollmentDirection().getEnrollmentDirection());

        // получаем структуру набора экзамена для текущего выбранного направления приема
        final ExamSetStructure examSetStructure = examSetData.getCategoryExamSetStructure(input.getRequestedEnrollmentDirection().getStudentCategory());

        // получаем список строк структуры набора экзамена
        final List<ExamSetStructureItem> itemList = examSetStructure.getItemList();

        // если все баллы относятся к типу ВИ "творческой и (или) профессиональной направленности"
        final List<ExamSetStructureItem> itemListCopy = new ArrayList<>(itemList);
        for (final Iterator<ExamSetStructureItem> iterator = itemListCopy.iterator();iterator.hasNext();)
        {
            if (EntranceDisciplineTypeCodes.CREATIVE_PROFESSIONAL.equals(iterator.next().getType().getCode()))
                iterator.remove();
        }
        // то такой студент пред.зачисления далее не учитывается
        if (itemListCopy.isEmpty())
            return null;

        // находим наилучшее распределение выбранных вступительных испытаний по строкам структуры набора
        final List<ChosenEntranceDiscipline> distributionList = Arrays.asList(MarkDistributionUtil.getChosenDistribution(entrantDataUtil.getChosenEntranceDisciplineSet(input.getRequestedEnrollmentDirection()), itemList));

        double summMark = 0;
        int markCount = 0;

        for (ExamSetStructureItem item : itemList)
        {
            //            if ((item.isBudget() && input.getCompensationType().isBudget()) || (item.isContract() && !input.getCompensationType().isBudget()))
            {
                if (!EntranceDisciplineTypeCodes.CREATIVE_PROFESSIONAL.equals(item.getType().getCode()))
                {
                    final ChosenEntranceDiscipline discipline = distributionList.get(itemList.indexOf(item));

                    if (discipline == null || discipline.getFinalMark() == null)
                        continue;

                    summMark += discipline.getFinalMark();
                    markCount++;
                }
            }
        }

        // баллы суммируем и делим на число баллов, округляем до двух знаков после запятой, т.о. получаем средний балл для студента пред.зачисления;
        return new BigDecimal(summMark / markCount).setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    protected double calcRating(RequestedEnrollmentDirection reqDir, EntrantDataUtil entrantDataUtil, Map<EnrollmentDirection, EnrollmentDirectionExamSetData> directionExamSetDataMap)
    {
        double summMark = 0;

        final EnrollmentDirectionExamSetData examSetData = directionExamSetDataMap.get(reqDir.getEnrollmentDirection());
        final List<ExamSetItem> itemList = examSetData.getCategoryExamSet(reqDir.getStudentCategory());

        if (CollectionUtils.isEmpty(itemList))
            return 0;

        for (ChosenEntranceDiscipline discipline : entrantDataUtil.getChosenEntranceDisciplineSet(reqDir))
        {
            if (discipline == null || discipline.getFinalMark() == null)
                continue;

            summMark += discipline.getFinalMark();
        }

        return new BigDecimal(summMark / itemList.size()).setScale(2, RoundingMode.HALF_UP).doubleValue();
    }
}
