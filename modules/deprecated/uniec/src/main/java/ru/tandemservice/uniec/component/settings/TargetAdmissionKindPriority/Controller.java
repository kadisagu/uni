/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.settings.TargetAdmissionKindPriority;

import java.util.HashMap;
import java.util.Map;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import org.tandemframework.shared.commonbase.base.util.CommonBaseDefine;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;

/**
 * @author agolubenko
 * @since 20.06.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));

        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<TargetAdmissionKind> dataSource = UniBaseUtils.createDataSource(component, getDao());
        PublisherLinkColumn column = new PublisherLinkColumn("Название", TargetAdmissionKind.P_TITLE);
        column.setTreeColumn(true);
        column.setResolver(new IPublisherLinkResolver()
        {

            @Override
            public String getComponentName(IEntity entity)
            {
                return ru.tandemservice.uniec.component.catalog.targetAdmissionKind.TargetAdmissionKindItemPub.Controller.class.getPackage().getName();
            }

            @Override
            public Object getParameters(IEntity entity)
            {
                Map<String, Object> params = new HashMap<String, Object>();
                params.put(PublisherActivator.PUBLISHER_ID_KEY, entity.getId());
                return params;
            }
        });
        dataSource.addColumn(column);
        dataSource.addColumn(new ActionColumn("Повысить приоритет", CommonBaseDefine.ICO_UP, "onClickUp").setOrderable(false));
        dataSource.addColumn(new ActionColumn("Понизить приоритет", CommonBaseDefine.ICO_DOWN, "onClickDown").setOrderable(false));
        model.setDataSource(dataSource);
    }

    public void onClickUp(IBusinessComponent context)
    {
        getDao().updatePriorityUp((Long) context.getListenerParameter());
    }

    public void onClickDown(IBusinessComponent context)
    {
        getDao().updatePriorityDown((Long) context.getListenerParameter());
    }
}
