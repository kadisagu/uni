package ru.tandemservice.uniec.component.report.EnrolledDealList.Pub;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.report.EnrolledDealListReport;

public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setReport(getNotNull(EnrolledDealListReport.class, model.getReport().getId()));
    }
}
