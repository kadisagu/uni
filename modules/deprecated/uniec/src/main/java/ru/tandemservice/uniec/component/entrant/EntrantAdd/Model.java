/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantAdd;

import org.apache.tapestry.form.validator.BaseValidator;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;

import java.util.List;

/**
 * @author agolubenko
 * @since 16.05.2008
 */
@Input( { @Bind(key = "onlineEntrantId", binding = "onlineEntrant.id"), @Bind(key = "enrollmentCampaignId") })
@Output( { @Bind(key = "similarPersons"), @Bind(key = "personString"), @Bind(key="infoString") })
public class Model extends org.tandemframework.shared.person.base.bo.Person.util.AbstractPersonRoleAdd.Model<Entrant> implements IEnrollmentCampaignSelectModel
{
    private List<EnrollmentCampaign> _enrollmentCampaignList;

    private Entrant _entrant = new Entrant();
    private OnlineEntrant _onlineEntrant = new OnlineEntrant();
    private Long _enrollmentCampaignId;

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return getEntrant().getEnrollmentCampaign();
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        getEntrant().setEnrollmentCampaign(enrollmentCampaign);
    }

    public IdentityCard getIdentityCard()
    {
        return getPerson().getIdentityCard();
    }

    public IdentityCardType getIdentityCardType()
    {
        return getIdentityCard().getCardType();
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    @Override
    protected Entrant getNewInstance()
    {
        return getEntrant();
    }

    public Entrant getEntrant()
    {
        return _entrant;
    }

    public void setEntrant(Entrant entrant)
    {
        _entrant = entrant;
    }

    public OnlineEntrant getOnlineEntrant()
    {
        return _onlineEntrant;
    }

    public void setOnlineEntrant(OnlineEntrant onlineEntrant)
    {
        _onlineEntrant = onlineEntrant;
    }

    public Long getEnrollmentCampaignId()
    {
        return _enrollmentCampaignId;
    }

    public void setEnrollmentCampaignId(Long enrollmentCampaignId)
    {
        _enrollmentCampaignId = enrollmentCampaignId;
    }

    @Override
    public String getPersonString()
    {
        return "Добавление абитуриента: " + super.getPersonString();
    }

    @Override
    public String getInfoString()
    {
        return "В процессе добавления абитуриента найдены персоны с похожими личными данными. Вы можете добавить абитуриента на основе одной из существующих персон, или создать новую.<br/>Для того, чтобы добавить абитуриента на основе одной из существующих персон, выберите её в списке.";
    }

    @Override
    protected String getNewPersonButtonName()
    {
        return "Добавить абитуриента, создав новую персону";
    }

    @Override
    protected String getOnBasisPersonButtonName()
    {
        return "Добавить абитуриента на основе существующей персоны";
    }

    @Override
    public IDataSettings getSettings()
    {
        return null;
    }
}