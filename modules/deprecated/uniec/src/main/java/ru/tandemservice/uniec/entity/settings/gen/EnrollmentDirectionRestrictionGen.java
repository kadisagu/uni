package ru.tandemservice.uniec.entity.settings.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.settings.EnrollmentDirectionRestriction;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Ограничения по формам освоения и видам возмещения затрат для направлений приема
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrollmentDirectionRestrictionGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.settings.EnrollmentDirectionRestriction";
    public static final String ENTITY_NAME = "enrollmentDirectionRestriction";
    public static final int VERSION_HASH = 952673760;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_DEVELOP_FORM = "developForm";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String P_MAX_ENROLLMENT_DIRECTION = "maxEnrollmentDirection";
    public static final String P_MAX_MINISTERIAL_DIRECTION = "maxMinisterialDirection";
    public static final String P_MAX_FORMATIVE_ORG_UNIT = "maxFormativeOrgUnit";

    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private DevelopForm _developForm;     // Форма освоения
    private CompensationType _compensationType;     // Вид возмещения затрат
    private Integer _maxEnrollmentDirection;     // Максимальное число выбираемых направлений подготовки для приема
    private Integer _maxMinisterialDirection;     // Максимальное число выбираемых направлений подготовки по классификатору
    private Integer _maxFormativeOrgUnit;     // Максимальное число выбираемых формирующих подразделений

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения. Свойство не может быть null.
     */
    public void setDevelopForm(DevelopForm developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Максимальное число выбираемых направлений подготовки для приема.
     */
    public Integer getMaxEnrollmentDirection()
    {
        return _maxEnrollmentDirection;
    }

    /**
     * @param maxEnrollmentDirection Максимальное число выбираемых направлений подготовки для приема.
     */
    public void setMaxEnrollmentDirection(Integer maxEnrollmentDirection)
    {
        dirty(_maxEnrollmentDirection, maxEnrollmentDirection);
        _maxEnrollmentDirection = maxEnrollmentDirection;
    }

    /**
     * @return Максимальное число выбираемых направлений подготовки по классификатору.
     */
    public Integer getMaxMinisterialDirection()
    {
        return _maxMinisterialDirection;
    }

    /**
     * @param maxMinisterialDirection Максимальное число выбираемых направлений подготовки по классификатору.
     */
    public void setMaxMinisterialDirection(Integer maxMinisterialDirection)
    {
        dirty(_maxMinisterialDirection, maxMinisterialDirection);
        _maxMinisterialDirection = maxMinisterialDirection;
    }

    /**
     * @return Максимальное число выбираемых формирующих подразделений.
     */
    public Integer getMaxFormativeOrgUnit()
    {
        return _maxFormativeOrgUnit;
    }

    /**
     * @param maxFormativeOrgUnit Максимальное число выбираемых формирующих подразделений.
     */
    public void setMaxFormativeOrgUnit(Integer maxFormativeOrgUnit)
    {
        dirty(_maxFormativeOrgUnit, maxFormativeOrgUnit);
        _maxFormativeOrgUnit = maxFormativeOrgUnit;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrollmentDirectionRestrictionGen)
        {
            setEnrollmentCampaign(((EnrollmentDirectionRestriction)another).getEnrollmentCampaign());
            setDevelopForm(((EnrollmentDirectionRestriction)another).getDevelopForm());
            setCompensationType(((EnrollmentDirectionRestriction)another).getCompensationType());
            setMaxEnrollmentDirection(((EnrollmentDirectionRestriction)another).getMaxEnrollmentDirection());
            setMaxMinisterialDirection(((EnrollmentDirectionRestriction)another).getMaxMinisterialDirection());
            setMaxFormativeOrgUnit(((EnrollmentDirectionRestriction)another).getMaxFormativeOrgUnit());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrollmentDirectionRestrictionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrollmentDirectionRestriction.class;
        }

        public T newInstance()
        {
            return (T) new EnrollmentDirectionRestriction();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "developForm":
                    return obj.getDevelopForm();
                case "compensationType":
                    return obj.getCompensationType();
                case "maxEnrollmentDirection":
                    return obj.getMaxEnrollmentDirection();
                case "maxMinisterialDirection":
                    return obj.getMaxMinisterialDirection();
                case "maxFormativeOrgUnit":
                    return obj.getMaxFormativeOrgUnit();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((DevelopForm) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "maxEnrollmentDirection":
                    obj.setMaxEnrollmentDirection((Integer) value);
                    return;
                case "maxMinisterialDirection":
                    obj.setMaxMinisterialDirection((Integer) value);
                    return;
                case "maxFormativeOrgUnit":
                    obj.setMaxFormativeOrgUnit((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "developForm":
                        return true;
                case "compensationType":
                        return true;
                case "maxEnrollmentDirection":
                        return true;
                case "maxMinisterialDirection":
                        return true;
                case "maxFormativeOrgUnit":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "developForm":
                    return true;
                case "compensationType":
                    return true;
                case "maxEnrollmentDirection":
                    return true;
                case "maxMinisterialDirection":
                    return true;
                case "maxFormativeOrgUnit":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "developForm":
                    return DevelopForm.class;
                case "compensationType":
                    return CompensationType.class;
                case "maxEnrollmentDirection":
                    return Integer.class;
                case "maxMinisterialDirection":
                    return Integer.class;
                case "maxFormativeOrgUnit":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrollmentDirectionRestriction> _dslPath = new Path<EnrollmentDirectionRestriction>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrollmentDirectionRestriction");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.EnrollmentDirectionRestriction#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.EnrollmentDirectionRestriction#getDevelopForm()
     */
    public static DevelopForm.Path<DevelopForm> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.EnrollmentDirectionRestriction#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Максимальное число выбираемых направлений подготовки для приема.
     * @see ru.tandemservice.uniec.entity.settings.EnrollmentDirectionRestriction#getMaxEnrollmentDirection()
     */
    public static PropertyPath<Integer> maxEnrollmentDirection()
    {
        return _dslPath.maxEnrollmentDirection();
    }

    /**
     * @return Максимальное число выбираемых направлений подготовки по классификатору.
     * @see ru.tandemservice.uniec.entity.settings.EnrollmentDirectionRestriction#getMaxMinisterialDirection()
     */
    public static PropertyPath<Integer> maxMinisterialDirection()
    {
        return _dslPath.maxMinisterialDirection();
    }

    /**
     * @return Максимальное число выбираемых формирующих подразделений.
     * @see ru.tandemservice.uniec.entity.settings.EnrollmentDirectionRestriction#getMaxFormativeOrgUnit()
     */
    public static PropertyPath<Integer> maxFormativeOrgUnit()
    {
        return _dslPath.maxFormativeOrgUnit();
    }

    public static class Path<E extends EnrollmentDirectionRestriction> extends EntityPath<E>
    {
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private DevelopForm.Path<DevelopForm> _developForm;
        private CompensationType.Path<CompensationType> _compensationType;
        private PropertyPath<Integer> _maxEnrollmentDirection;
        private PropertyPath<Integer> _maxMinisterialDirection;
        private PropertyPath<Integer> _maxFormativeOrgUnit;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.EnrollmentDirectionRestriction#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.EnrollmentDirectionRestriction#getDevelopForm()
     */
        public DevelopForm.Path<DevelopForm> developForm()
        {
            if(_developForm == null )
                _developForm = new DevelopForm.Path<DevelopForm>(L_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.EnrollmentDirectionRestriction#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Максимальное число выбираемых направлений подготовки для приема.
     * @see ru.tandemservice.uniec.entity.settings.EnrollmentDirectionRestriction#getMaxEnrollmentDirection()
     */
        public PropertyPath<Integer> maxEnrollmentDirection()
        {
            if(_maxEnrollmentDirection == null )
                _maxEnrollmentDirection = new PropertyPath<Integer>(EnrollmentDirectionRestrictionGen.P_MAX_ENROLLMENT_DIRECTION, this);
            return _maxEnrollmentDirection;
        }

    /**
     * @return Максимальное число выбираемых направлений подготовки по классификатору.
     * @see ru.tandemservice.uniec.entity.settings.EnrollmentDirectionRestriction#getMaxMinisterialDirection()
     */
        public PropertyPath<Integer> maxMinisterialDirection()
        {
            if(_maxMinisterialDirection == null )
                _maxMinisterialDirection = new PropertyPath<Integer>(EnrollmentDirectionRestrictionGen.P_MAX_MINISTERIAL_DIRECTION, this);
            return _maxMinisterialDirection;
        }

    /**
     * @return Максимальное число выбираемых формирующих подразделений.
     * @see ru.tandemservice.uniec.entity.settings.EnrollmentDirectionRestriction#getMaxFormativeOrgUnit()
     */
        public PropertyPath<Integer> maxFormativeOrgUnit()
        {
            if(_maxFormativeOrgUnit == null )
                _maxFormativeOrgUnit = new PropertyPath<Integer>(EnrollmentDirectionRestrictionGen.P_MAX_FORMATIVE_ORG_UNIT, this);
            return _maxFormativeOrgUnit;
        }

        public Class getEntityClass()
        {
            return EnrollmentDirectionRestriction.class;
        }

        public String getEntityName()
        {
            return "enrollmentDirectionRestriction";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
