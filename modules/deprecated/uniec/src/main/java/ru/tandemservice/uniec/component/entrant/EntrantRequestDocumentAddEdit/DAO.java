/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.EntrantRequestDocumentAddEdit;

import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.catalog.EnrollmentDocument;
import ru.tandemservice.uniec.entity.entrant.EntrantEnrollmentDocument;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.settings.UsedEnrollmentDocument;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author vip_delete
 * @since 06.06.2009
 */
@SuppressWarnings("unchecked")
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setEntrantRequest(getNotNull(EntrantRequest.class, model.getEntrantRequest().getId()));

        Map<Long, Integer> amountValueMap = new HashMap<>();
        Map<Long, Boolean> copyValueMap = new HashMap<>();

        for (EntrantEnrollmentDocument document : getEntrantEnrollmentDocumentList(model))
        {
            amountValueMap.put(document.getEnrollmentDocument().getId(), document.getAmount());
            copyValueMap.put(document.getEnrollmentDocument().getId(), document.isCopy());
        }

        ((BlockColumn) model.getDataSource().getColumn(Model.BLOCK_COLUMN_AMOUNT)).setValueMap(amountValueMap);
        ((BlockColumn) model.getDataSource().getColumn(Model.BLOCK_COLUMN_COPY)).setValueMap(copyValueMap);
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        // выбираем только используемые документы для подачи в ОУ
        MQBuilder builder = new MQBuilder(UsedEnrollmentDocument.ENTITY_CLASS, "usedEnrollmentDocument", new String[] { UsedEnrollmentDocument.enrollmentDocument().s() });
        builder.add(MQExpression.eq("usedEnrollmentDocument", UsedEnrollmentDocument.enrollmentCampaign().s(), model.getEntrantRequest().getEntrant().getEnrollmentCampaign()));
        builder.add(MQExpression.eq("usedEnrollmentDocument", UsedEnrollmentDocument.used().s(), Boolean.TRUE));
        builder.addOrder("usedEnrollmentDocument", UsedEnrollmentDocument.enrollmentDocument().priority().s());

        List<EnrollmentDocument> list = builder.getResultList(getSession());
        model.getDataSource().setCountRow(list.size());
        UniBaseUtils.createPage(model.getDataSource(), list);
    }

    @Override
    public void update(Model model)
    {
        Map<Long, Number> amountValueMap = ((BlockColumn) model.getDataSource().getColumn(Model.BLOCK_COLUMN_AMOUNT)).getValueMap();
        Map<Long, Boolean> copyValueMap = ((BlockColumn) model.getDataSource().getColumn(Model.BLOCK_COLUMN_COPY)).getValueMap();

        // получаем информацию по уже сданным документам абитуриента
        Map<Long, EntrantEnrollmentDocument> documentId2entrantdocument = new HashMap<>();
        for (EntrantEnrollmentDocument entrantEnrollmentDocument : getEntrantEnrollmentDocumentList(model))
            documentId2entrantdocument.put(entrantEnrollmentDocument.getEnrollmentDocument().getId(), entrantEnrollmentDocument);

        // бежим по всем документам для подачи в ОУ
        for (EnrollmentDocument enrollmentDocument : getCatalogItemList(EnrollmentDocument.class))
        {
            // возможно у абитуриента уже есть такой документ
            EntrantEnrollmentDocument entrantEnrollmentDocument = documentId2entrantdocument.get(enrollmentDocument.getId());

            // получаем с формы их количество
            Number amount = amountValueMap.get(enrollmentDocument.getId());

            // получаем с формы это копия (true), или оригинал (false)
            Boolean copy = copyValueMap.get(enrollmentDocument.getId());

            if (amount == null)
            {
                // если не выбрано количество документов, то удаляем такой выбранный документ абитуриента совсем
                if (entrantEnrollmentDocument != null)
                    delete(entrantEnrollmentDocument);
            }
            else
            {
                // иначе либо обновляем его значения, либо сохраняем новый
                if (entrantEnrollmentDocument == null)
                    entrantEnrollmentDocument = new EntrantEnrollmentDocument();

                entrantEnrollmentDocument.setEntrantRequest(model.getEntrantRequest());
                entrantEnrollmentDocument.setEnrollmentDocument(enrollmentDocument);
                entrantEnrollmentDocument.setAmount(amount.intValue());
                entrantEnrollmentDocument.setCopy(copy);
                getSession().saveOrUpdate(entrantEnrollmentDocument);
            }
        }
    }

    private List<EntrantEnrollmentDocument> getEntrantEnrollmentDocumentList(Model model)
    {
        MQBuilder builder = new MQBuilder(EntrantEnrollmentDocument.ENTITY_CLASS, "d");
        builder.add(MQExpression.eq("d", EntrantEnrollmentDocument.L_ENTRANT_REQUEST, model.getEntrantRequest()));
        builder.addOrder("d", EntrantEnrollmentDocument.L_ENROLLMENT_DOCUMENT + "." + EnrollmentDocument.P_PRIORITY);
        return builder.getResultList(getSession());
    }
}
