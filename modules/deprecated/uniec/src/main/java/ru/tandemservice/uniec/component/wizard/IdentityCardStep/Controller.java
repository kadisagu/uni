/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.wizard.IdentityCardStep;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInline;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInlineConfig;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInlineUI;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.utils.AddressBaseUtils;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uniec.component.wizard.IEntrantWizardComponents;

/**
 * @author vip_delete
 * @since 14.06.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    public static final String REGION_ADDRESS = "addressRegion";

    @Override
    public void onChildDeactivate(IBusinessComponent component, String regionName)
    {
        final Model model = getModel(component);

        if (model.getEntrantModel().getBasisPersonId() != null)
        {
            Long basisPersonId = model.getEntrantModel().getBasisPersonId();

            // сбрасываем полученный basisPersonId
            model.getEntrantModel().setBasisPersonId(null);

            // был поиск дублей (basisPersonId получен как return value)
            if (basisPersonId == 0L)
                getDao().createEntrant(model);//сохраняем абитуринта на базе новой персоны
            else
                getDao().createBySimilar(model.getEntrantModel().getEntrant(), basisPersonId, model.getEntrantModel().getOnlineEntrant());//сохраняем абитуринта на базе выбранной персоны

            // закрываем текущий компонент и удаляем скоуп
            deactivate(component);

            // запускаем следующий шаг
            component.getParentRegion().getOwner().createDefaultChildRegion(new ComponentActivator(IEntrantWizardComponents.EDU_INSTITUTION_STEP, new ParametersMap()
                    .add("entrantId", model.getEntrantModel().getEntrant().getId())
                    .add("entrantMasterPermKey", model.getEntrantMasterPermKey())
                    .add("onlineEntrantId", model.getEntrantModel().getOnlineEntrant().getId())
            ));
        }
    }

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        final Model model = getModel(component);
        getDao().prepare(model);

        AddressBaseEditInlineConfig addressConfig = new AddressBaseEditInlineConfig();

        addressConfig.setDetailedOnly(true);
        addressConfig.setDetailLevel(4);
        addressConfig.setInline(true);
        addressConfig.setAreaVisible(true);
        addressConfig.setFieldSetTitle("Адресные данные");
        addressConfig.setWithoutFieldSet(true);
        if(null != model.getEntrantModel().getOnlineEntrant().getId())
        {
            addressConfig.setAddressBase(AddressBaseUtils.convertFromOldFormat(model.getEntrantModel().getOnlineEntrant().getRegistrationAddress(), false));
        }
        else if(null != model.getIdentityCard() && null != model.getIdentityCard().getAddress())
        {
            addressConfig.setAddressBase(model.getIdentityCard().getAddress());
        }
        component.createChildRegion(REGION_ADDRESS, new ComponentActivator(AddressBaseEditInline.class.getSimpleName(), new ParametersMap().add(AddressBaseEditInlineUI.BIND_CONFIG, addressConfig)));
    }

    public void onChangeIdCartType(IBusinessComponent component)
    {
        final Model model = getModel(component);

        final IdentityCard identityCard = model.getEntrantModel().getIdentityCard();
        identityCard.setCitizenship(identityCard.getCardType().getCitizenshipDefault());
    }

    public void onClickNext(IBusinessComponent component)
    {
        final Model model = getModel(component);

        AddressBaseEditInlineUI addressBaseEditInlineUI = (AddressBaseEditInlineUI) component.getChildRegion(REGION_ADDRESS).getActiveComponent().getPresenter();

        if(addressBaseEditInlineUI.getResult() == null) throw new RuntimeException("Адрес обязателен");
        AddressBase address = addressBaseEditInlineUI.getResult();

        model.getAddressModel().setAddress(address);

        getDao().createOrFindSimilar(model);

        if (model.getEntrantModel().getEntrant().getId() != null)
        {
            // закрываем текущий компонент и удаляем скоуп
            deactivate(component);

            // абитуриент был создан, запускаем следующий шаг
            component.getParentRegion().getOwner().createDefaultChildRegion(new ComponentActivator(IEntrantWizardComponents.EDU_INSTITUTION_STEP, new ParametersMap()
                    .add("entrantId", model.getEntrantModel().getEntrant().getId())
                    .add("onlineEntrantId", model.getEntrantModel().getOnlineEntrant().getId())
                    .add("entrantMasterPermKey", model.getEntrantMasterPermKey())
            ));
        } else
        {
            activate(component, new ComponentActivator(IEntrantWizardComponents.PERSON_SIMILAR_STEP, new ParametersMap()
                    .add("newPersonButtonName", "Добавить абитуриента, создав новую персону")
                    .add("onBasisPersonButtonName", "Добавить абитуриента на основе существующей персоны")
                    .add("similarPersons", model.getEntrantModel().getSimilarPersons())
                    .add("personString", model.getEntrantModel().getPersonString())
                    .add("entrantMasterPermKey", model.getEntrantMasterPermKey())
            ));
        }
    }
}
