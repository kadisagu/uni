/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.dao;

import ru.tandemservice.uni.dao.IEntityExportDAO.EntityExporter;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniec.entity.entrant.CompetitionGroup;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.settings.EnrollmentCompetitionKind;
import ru.tandemservice.uniec.entity.settings.Qualification2CompetitionKindRelation;

/**
 * @author agolubenko
 * @since May 7, 2010
 */
public interface IUniecExportDao extends IUniBaseDao
{
    String BEAN_NAME = "uniecExportDao";
    
    EntityExporter<EnrollmentCampaign> createEnrollmentCampaignExporter();

    EntityExporter<EnrollmentDirection> createEnrollmentDirectionExporter();

    EntityExporter<CompetitionGroup> createСompetitionGroupExporter();

    EntityExporter<Qualification2CompetitionKindRelation> createQualification2CompetitionKindRelationExporter();

    EntityExporter<EnrollmentCompetitionKind> createEnrollmentCompetitionKindExporter();
}
