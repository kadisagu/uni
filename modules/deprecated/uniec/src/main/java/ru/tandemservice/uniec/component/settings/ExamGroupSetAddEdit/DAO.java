/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.settings.ExamGroupSetAddEdit;

import java.util.List;

import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.entrant.ExamGroupSet;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

/**
 * @author vip_delete
 * @since 10.06.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilterWithExamGroupAutoFormingAndAlgorithm(model, getSession());

        if (model.getExamGroupSet().getId() != null)
            model.setExamGroupSet(getNotNull(ExamGroupSet.class, model.getExamGroupSet().getId()));
        else
            model.getExamGroupSet().setSize(30);
    }

    @Override
    public void update(Model model)
    {
        if (!model.getExamGroupSet().getBeginDate().before(model.getExamGroupSet().getEndDate()))
            throw new ApplicationException("Дата начала периода должна быть раньше даты его окончания.");

        final Session session = getSession();
        MQBuilder builder = new MQBuilder(ExamGroupSet.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", ExamGroupSet.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
        if (model.getExamGroupSet().getId() != null)
            builder.add(MQExpression.notEq("e", ExamGroupSet.P_ID, model.getExamGroupSet().getId()));
        List<ExamGroupSet> list = builder.getResultList(session);

        int i = 0;
        while (i < list.size() && (model.getExamGroupSet().getBeginDate().after(list.get(i).getEndDate()) || model.getExamGroupSet().getEndDate().before(list.get(i).getBeginDate())))
            i++;
        if (i < list.size())
            throw new ApplicationException("Указанный период пересекается с уже существующим периодом «" + list.get(i).getTitle() + "».");

        if (model.getExamGroupSet().getId() == null)
        {
            // форма добавления

            // создаем новый набор и открываем его
            session.save(model.getExamGroupSet());
            UniecDAOFacade.getExamGroupSetDao().openExamGroupSet(model.getExamGroupSet());
        } else
        {
            // форма редактирования
            session.update(model.getExamGroupSet());
        }
    }
}
