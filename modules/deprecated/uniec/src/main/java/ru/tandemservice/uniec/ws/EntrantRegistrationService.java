/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.ws;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;

/**
 * @author agolubenko
 * @since Apr 29, 2010
 */
@WebService(serviceName = "EntrantRegistrationService")
public class EntrantRegistrationService
{
    @WebMethod
    public void createOrUpdateOnlineEntrant(long userId, EntrantData entrantData)
    {
        UniecDAOFacade.getOnlineEntrantRegistrationDAO().createOrUpdateOnlineEntrant(userId, entrantData);
    }

    @WebMethod
    public EntrantData getEntrantData(long userId)
    {
        return UniecDAOFacade.getOnlineEntrantRegistrationDAO().getEntrantData(userId);
    }

    @WebMethod
    public String getOnlineEntrantPersonalNumber(long userId)
    {
        OnlineEntrant onlineEntrant = getOnlineEntrant(userId);
        return (onlineEntrant == null) ? null :
                String.valueOf(onlineEntrant.getRegistrationYear()) + "-" + String.valueOf(onlineEntrant.getPersonalNumber());
    }

    @WebMethod
    public boolean isExistEntrant(long userId)
    {
        OnlineEntrant onlineEntrant = getOnlineEntrant(userId);
        return (onlineEntrant != null && onlineEntrant.getEntrant() != null);
    }

    @WebMethod
    public boolean isExistOnlineEntrant(long userId)
    {
        return (getOnlineEntrant(userId) != null);
    }

    @WebMethod
    public boolean isCanDeleteOnlineEntrant(long userId)
    {
        OnlineEntrant onlineEntrant = getOnlineEntrant(userId);
        return null == onlineEntrant || null == onlineEntrant.getEntrant();
    }

    @WebMethod
    public boolean deleteOnlineEntrant(long userId)
    {
        OnlineEntrant onlineEntrant = getOnlineEntrant(userId);
        if(null == onlineEntrant) return true;
        if(!isCanDeleteOnlineEntrant(userId))
            return false;
        else
        {
            UniecDAOFacade.getOnlineEntrantRegistrationDAO().delete(onlineEntrant.getId());
            return true;
        }
    }

    @WebMethod
    public UserDirectionData[] getUserDirections(long userId)
    {
        List<UserDirectionData> result = UniecDAOFacade.getOnlineEntrantRegistrationDAO().getUserDirections(userId);
        return result.toArray(new UserDirectionData[result.size()]);
    }

    @WebMethod
    public void synchronizeUserDirections(long userId, UserDirectionData[] userDirections)
    {
        UniecDAOFacade.getOnlineEntrantRegistrationDAO().updateUserDirections(userId, (userDirections != null) ? userDirections : new UserDirectionData[0]);
    }

    @WebMethod
    public byte[] printOnlineEntrantRequest(long userId) throws Exception
    {
        return UniecDAOFacade.getOnlineEntrantRegistrationDAO().getPrintOnlineEntrantRequest(userId);
    }

    @WebMethod
    public void saveDocumentCopies(long userId, byte[] zipArchive, String documentCopiesName) throws Exception
    {
        UniecDAOFacade.getOnlineEntrantRegistrationDAO().saveDocumentCopies(userId, zipArchive, documentCopiesName);
    }

    @WebMethod
    public void deleteDocumentCopies(long userId) throws Exception
    {
        UniecDAOFacade.getOnlineEntrantRegistrationDAO().deleteDocumentCopies(userId);
    }

    @WebMethod
    public String getDocumentCopiesName(long userId) throws Exception
    {
        OnlineEntrant onlineEntrant = getOnlineEntrant(userId);
        return onlineEntrant == null ? null : onlineEntrant.getDocumentCopiesName();
    }

    @WebMethod
    public byte[] getDocumentCopies(long userId) throws Exception
    {
        OnlineEntrant onlineEntrant = getOnlineEntrant(userId);
        return onlineEntrant == null || onlineEntrant.getDocumentCopies() == null ? null : onlineEntrant.getDocumentCopies().getContent();
    }

    private OnlineEntrant getOnlineEntrant(long userId)
    {
        return UniecDAOFacade.getOnlineEntrantRegistrationDAO().getOnlineEntrant(userId);
    }
}
