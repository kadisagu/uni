package ru.tandemservice.uniec.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.report.EntrantsRatingReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Ежедневный рейтинг абитуриентов по конкурсным группам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntrantsRatingReportGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.report.EntrantsRatingReport";
    public static final String ENTITY_NAME = "entrantsRatingReport";
    public static final int VERSION_HASH = 1276482192;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONTENT = "content";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_DATE_FROM = "dateFrom";
    public static final String P_DATE_TO = "dateTo";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String P_COMPETITION_GROUP_TITLE = "competitionGroupTitle";
    public static final String P_TECHNIC_COMMISSION = "technicCommission";

    private DatabaseFile _content;     // Печатная форма
    private Date _formingDate;     // Дата формирования
    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private Date _dateFrom;     // Заявления с
    private Date _dateTo;     // Заявления по
    private CompensationType _compensationType;     // Вид возмещения затрат
    private String _competitionGroupTitle;     // Конкурсная группа
    private String _technicCommission;     // Техническая комиссия

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Заявления с.
     */
    public Date getDateFrom()
    {
        return _dateFrom;
    }

    /**
     * @param dateFrom Заявления с.
     */
    public void setDateFrom(Date dateFrom)
    {
        dirty(_dateFrom, dateFrom);
        _dateFrom = dateFrom;
    }

    /**
     * @return Заявления по.
     */
    public Date getDateTo()
    {
        return _dateTo;
    }

    /**
     * @param dateTo Заявления по.
     */
    public void setDateTo(Date dateTo)
    {
        dirty(_dateTo, dateTo);
        _dateTo = dateTo;
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Конкурсная группа. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getCompetitionGroupTitle()
    {
        return _competitionGroupTitle;
    }

    /**
     * @param competitionGroupTitle Конкурсная группа. Свойство не может быть null.
     */
    public void setCompetitionGroupTitle(String competitionGroupTitle)
    {
        dirty(_competitionGroupTitle, competitionGroupTitle);
        _competitionGroupTitle = competitionGroupTitle;
    }

    /**
     * @return Техническая комиссия.
     */
    @Length(max=255)
    public String getTechnicCommission()
    {
        return _technicCommission;
    }

    /**
     * @param technicCommission Техническая комиссия.
     */
    public void setTechnicCommission(String technicCommission)
    {
        dirty(_technicCommission, technicCommission);
        _technicCommission = technicCommission;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntrantsRatingReportGen)
        {
            setContent(((EntrantsRatingReport)another).getContent());
            setFormingDate(((EntrantsRatingReport)another).getFormingDate());
            setEnrollmentCampaign(((EntrantsRatingReport)another).getEnrollmentCampaign());
            setDateFrom(((EntrantsRatingReport)another).getDateFrom());
            setDateTo(((EntrantsRatingReport)another).getDateTo());
            setCompensationType(((EntrantsRatingReport)another).getCompensationType());
            setCompetitionGroupTitle(((EntrantsRatingReport)another).getCompetitionGroupTitle());
            setTechnicCommission(((EntrantsRatingReport)another).getTechnicCommission());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntrantsRatingReportGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntrantsRatingReport.class;
        }

        public T newInstance()
        {
            return (T) new EntrantsRatingReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "content":
                    return obj.getContent();
                case "formingDate":
                    return obj.getFormingDate();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "dateFrom":
                    return obj.getDateFrom();
                case "dateTo":
                    return obj.getDateTo();
                case "compensationType":
                    return obj.getCompensationType();
                case "competitionGroupTitle":
                    return obj.getCompetitionGroupTitle();
                case "technicCommission":
                    return obj.getTechnicCommission();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "dateFrom":
                    obj.setDateFrom((Date) value);
                    return;
                case "dateTo":
                    obj.setDateTo((Date) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "competitionGroupTitle":
                    obj.setCompetitionGroupTitle((String) value);
                    return;
                case "technicCommission":
                    obj.setTechnicCommission((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "content":
                        return true;
                case "formingDate":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "dateFrom":
                        return true;
                case "dateTo":
                        return true;
                case "compensationType":
                        return true;
                case "competitionGroupTitle":
                        return true;
                case "technicCommission":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "content":
                    return true;
                case "formingDate":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "dateFrom":
                    return true;
                case "dateTo":
                    return true;
                case "compensationType":
                    return true;
                case "competitionGroupTitle":
                    return true;
                case "technicCommission":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "content":
                    return DatabaseFile.class;
                case "formingDate":
                    return Date.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "dateFrom":
                    return Date.class;
                case "dateTo":
                    return Date.class;
                case "compensationType":
                    return CompensationType.class;
                case "competitionGroupTitle":
                    return String.class;
                case "technicCommission":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntrantsRatingReport> _dslPath = new Path<EntrantsRatingReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntrantsRatingReport");
    }
            

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantsRatingReport#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantsRatingReport#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantsRatingReport#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Заявления с.
     * @see ru.tandemservice.uniec.entity.report.EntrantsRatingReport#getDateFrom()
     */
    public static PropertyPath<Date> dateFrom()
    {
        return _dslPath.dateFrom();
    }

    /**
     * @return Заявления по.
     * @see ru.tandemservice.uniec.entity.report.EntrantsRatingReport#getDateTo()
     */
    public static PropertyPath<Date> dateTo()
    {
        return _dslPath.dateTo();
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantsRatingReport#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Конкурсная группа. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantsRatingReport#getCompetitionGroupTitle()
     */
    public static PropertyPath<String> competitionGroupTitle()
    {
        return _dslPath.competitionGroupTitle();
    }

    /**
     * @return Техническая комиссия.
     * @see ru.tandemservice.uniec.entity.report.EntrantsRatingReport#getTechnicCommission()
     */
    public static PropertyPath<String> technicCommission()
    {
        return _dslPath.technicCommission();
    }

    public static class Path<E extends EntrantsRatingReport> extends EntityPath<E>
    {
        private DatabaseFile.Path<DatabaseFile> _content;
        private PropertyPath<Date> _formingDate;
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<Date> _dateFrom;
        private PropertyPath<Date> _dateTo;
        private CompensationType.Path<CompensationType> _compensationType;
        private PropertyPath<String> _competitionGroupTitle;
        private PropertyPath<String> _technicCommission;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantsRatingReport#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantsRatingReport#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(EntrantsRatingReportGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantsRatingReport#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Заявления с.
     * @see ru.tandemservice.uniec.entity.report.EntrantsRatingReport#getDateFrom()
     */
        public PropertyPath<Date> dateFrom()
        {
            if(_dateFrom == null )
                _dateFrom = new PropertyPath<Date>(EntrantsRatingReportGen.P_DATE_FROM, this);
            return _dateFrom;
        }

    /**
     * @return Заявления по.
     * @see ru.tandemservice.uniec.entity.report.EntrantsRatingReport#getDateTo()
     */
        public PropertyPath<Date> dateTo()
        {
            if(_dateTo == null )
                _dateTo = new PropertyPath<Date>(EntrantsRatingReportGen.P_DATE_TO, this);
            return _dateTo;
        }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantsRatingReport#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Конкурсная группа. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantsRatingReport#getCompetitionGroupTitle()
     */
        public PropertyPath<String> competitionGroupTitle()
        {
            if(_competitionGroupTitle == null )
                _competitionGroupTitle = new PropertyPath<String>(EntrantsRatingReportGen.P_COMPETITION_GROUP_TITLE, this);
            return _competitionGroupTitle;
        }

    /**
     * @return Техническая комиссия.
     * @see ru.tandemservice.uniec.entity.report.EntrantsRatingReport#getTechnicCommission()
     */
        public PropertyPath<String> technicCommission()
        {
            if(_technicCommission == null )
                _technicCommission = new PropertyPath<String>(EntrantsRatingReportGen.P_TECHNIC_COMMISSION, this);
            return _technicCommission;
        }

        public Class getEntityClass()
        {
            return EntrantsRatingReport.class;
        }

        public String getEntityName()
        {
            return "entrantsRatingReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
