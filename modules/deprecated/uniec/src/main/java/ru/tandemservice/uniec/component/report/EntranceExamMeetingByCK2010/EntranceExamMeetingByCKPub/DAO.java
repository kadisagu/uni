/* $Id: DAO.java 9017 2009-07-10 10:05:02Z ekachanova $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EntranceExamMeetingByCK2010.EntranceExamMeetingByCKPub;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010;

/**
 * @author Vasily Zhukov
 * @since 04.08.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setReport(getNotNull(EntranceExamMeetingByCKReport2010.class, model.getReport().getId()));
    }
}

