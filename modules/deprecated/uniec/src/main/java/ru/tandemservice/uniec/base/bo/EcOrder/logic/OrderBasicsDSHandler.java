/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcOrder.logic;

import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniec.entity.catalog.EnrollmentOrderBasic;
import ru.tandemservice.uniec.entity.catalog.EnrollmentOrderReason;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrderReasonToBasicsRel;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 06.08.2013
 */
public class OrderBasicsDSHandler extends EntityComboDataSourceHandler
{
    public static final String ORDER_REASON_PARAM = "reason";

    public OrderBasicsDSHandler(String ownerId)
    {
        super(ownerId, EnrollmentOrderBasic.class);
        order(EnrollmentOrderBasic.title());
        filter(EnrollmentOrderBasic.title());
        pageable(true);
    }

    @Override
    protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
    {
        super.applyWhereConditions(alias, dql, context);
        EnrollmentOrderReason reason = context.get(ORDER_REASON_PARAM);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrollmentOrderReasonToBasicsRel.class, "rel")
                .column(property(EnrollmentOrderReasonToBasicsRel.basic().id().fromAlias("rel")))
                .where(eq(property(EnrollmentOrderReasonToBasicsRel.reason().fromAlias("rel")), value(reason)));

        dql.where(
                in(property(EnrollmentOrderBasic.id().fromAlias(alias)), builder.buildQuery())
        );
    }

}