package ru.tandemservice.uniec.entity.ecg.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.entity.ecg.EcgDistribObject;
import ru.tandemservice.uniec.entity.entrant.CompetitionGroup;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Распределение по конкурсной группе
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EcgDistribObjectGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.ecg.EcgDistribObject";
    public static final String ENTITY_NAME = "ecgDistribObject";
    public static final int VERSION_HASH = -1394513860;
    private static IEntityMeta ENTITY_META;

    public static final String L_PARENT = "parent";
    public static final String L_COMPETITION_GROUP = "competitionGroup";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String P_SECOND_HIGH_ADMISSION = "secondHighAdmission";
    public static final String P_TITLE = "title";
    public static final String P_REGISTRATION_DATE = "registrationDate";
    public static final String P_APPROVED = "approved";
    public static final String L_RESULT_PRINT_FORM = "resultPrintForm";
    public static final String L_RESERVE_PRINT_FORM = "reservePrintForm";

    private EcgDistribObject _parent;     // основное распределение
    private CompetitionGroup _competitionGroup;     // конкурсная группа
    private CompensationType _compensationType;     // возмещение затрат
    private boolean _secondHighAdmission;     // второе высшее
    private String _title;     // заголовок
    private Date _registrationDate;     // Дата добавления
    private boolean _approved;     // Утвержден
    private DatabaseFile _resultPrintForm;     // Результаты распределения
    private DatabaseFile _reservePrintForm;     // Резерв

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return основное распределение.
     */
    public EcgDistribObject getParent()
    {
        return _parent;
    }

    /**
     * @param parent основное распределение.
     */
    public void setParent(EcgDistribObject parent)
    {
        dirty(_parent, parent);
        _parent = parent;
    }

    /**
     * @return конкурсная группа. Свойство не может быть null.
     */
    @NotNull
    public CompetitionGroup getCompetitionGroup()
    {
        return _competitionGroup;
    }

    /**
     * @param competitionGroup конкурсная группа. Свойство не может быть null.
     */
    public void setCompetitionGroup(CompetitionGroup competitionGroup)
    {
        dirty(_competitionGroup, competitionGroup);
        _competitionGroup = competitionGroup;
    }

    /**
     * @return возмещение затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType возмещение затрат. Свойство не может быть null.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return второе высшее. Свойство не может быть null.
     */
    @NotNull
    public boolean isSecondHighAdmission()
    {
        return _secondHighAdmission;
    }

    /**
     * @param secondHighAdmission второе высшее. Свойство не может быть null.
     */
    public void setSecondHighAdmission(boolean secondHighAdmission)
    {
        dirty(_secondHighAdmission, secondHighAdmission);
        _secondHighAdmission = secondHighAdmission;
    }

    /**
     * @return заголовок. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title заголовок. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Дата добавления. Свойство не может быть null.
     */
    @NotNull
    public Date getRegistrationDate()
    {
        return _registrationDate;
    }

    /**
     * @param registrationDate Дата добавления. Свойство не может быть null.
     */
    public void setRegistrationDate(Date registrationDate)
    {
        dirty(_registrationDate, registrationDate);
        _registrationDate = registrationDate;
    }

    /**
     * @return Утвержден. Свойство не может быть null.
     */
    @NotNull
    public boolean isApproved()
    {
        return _approved;
    }

    /**
     * @param approved Утвержден. Свойство не может быть null.
     */
    public void setApproved(boolean approved)
    {
        dirty(_approved, approved);
        _approved = approved;
    }

    /**
     * @return Результаты распределения.
     */
    public DatabaseFile getResultPrintForm()
    {
        return _resultPrintForm;
    }

    /**
     * @param resultPrintForm Результаты распределения.
     */
    public void setResultPrintForm(DatabaseFile resultPrintForm)
    {
        dirty(_resultPrintForm, resultPrintForm);
        _resultPrintForm = resultPrintForm;
    }

    /**
     * @return Резерв.
     */
    public DatabaseFile getReservePrintForm()
    {
        return _reservePrintForm;
    }

    /**
     * @param reservePrintForm Резерв.
     */
    public void setReservePrintForm(DatabaseFile reservePrintForm)
    {
        dirty(_reservePrintForm, reservePrintForm);
        _reservePrintForm = reservePrintForm;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EcgDistribObjectGen)
        {
            setParent(((EcgDistribObject)another).getParent());
            setCompetitionGroup(((EcgDistribObject)another).getCompetitionGroup());
            setCompensationType(((EcgDistribObject)another).getCompensationType());
            setSecondHighAdmission(((EcgDistribObject)another).isSecondHighAdmission());
            setTitle(((EcgDistribObject)another).getTitle());
            setRegistrationDate(((EcgDistribObject)another).getRegistrationDate());
            setApproved(((EcgDistribObject)another).isApproved());
            setResultPrintForm(((EcgDistribObject)another).getResultPrintForm());
            setReservePrintForm(((EcgDistribObject)another).getReservePrintForm());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EcgDistribObjectGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EcgDistribObject.class;
        }

        public T newInstance()
        {
            return (T) new EcgDistribObject();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "parent":
                    return obj.getParent();
                case "competitionGroup":
                    return obj.getCompetitionGroup();
                case "compensationType":
                    return obj.getCompensationType();
                case "secondHighAdmission":
                    return obj.isSecondHighAdmission();
                case "title":
                    return obj.getTitle();
                case "registrationDate":
                    return obj.getRegistrationDate();
                case "approved":
                    return obj.isApproved();
                case "resultPrintForm":
                    return obj.getResultPrintForm();
                case "reservePrintForm":
                    return obj.getReservePrintForm();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "parent":
                    obj.setParent((EcgDistribObject) value);
                    return;
                case "competitionGroup":
                    obj.setCompetitionGroup((CompetitionGroup) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "secondHighAdmission":
                    obj.setSecondHighAdmission((Boolean) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "registrationDate":
                    obj.setRegistrationDate((Date) value);
                    return;
                case "approved":
                    obj.setApproved((Boolean) value);
                    return;
                case "resultPrintForm":
                    obj.setResultPrintForm((DatabaseFile) value);
                    return;
                case "reservePrintForm":
                    obj.setReservePrintForm((DatabaseFile) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "parent":
                        return true;
                case "competitionGroup":
                        return true;
                case "compensationType":
                        return true;
                case "secondHighAdmission":
                        return true;
                case "title":
                        return true;
                case "registrationDate":
                        return true;
                case "approved":
                        return true;
                case "resultPrintForm":
                        return true;
                case "reservePrintForm":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "parent":
                    return true;
                case "competitionGroup":
                    return true;
                case "compensationType":
                    return true;
                case "secondHighAdmission":
                    return true;
                case "title":
                    return true;
                case "registrationDate":
                    return true;
                case "approved":
                    return true;
                case "resultPrintForm":
                    return true;
                case "reservePrintForm":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "parent":
                    return EcgDistribObject.class;
                case "competitionGroup":
                    return CompetitionGroup.class;
                case "compensationType":
                    return CompensationType.class;
                case "secondHighAdmission":
                    return Boolean.class;
                case "title":
                    return String.class;
                case "registrationDate":
                    return Date.class;
                case "approved":
                    return Boolean.class;
                case "resultPrintForm":
                    return DatabaseFile.class;
                case "reservePrintForm":
                    return DatabaseFile.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EcgDistribObject> _dslPath = new Path<EcgDistribObject>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EcgDistribObject");
    }
            

    /**
     * @return основное распределение.
     * @see ru.tandemservice.uniec.entity.ecg.EcgDistribObject#getParent()
     */
    public static EcgDistribObject.Path<EcgDistribObject> parent()
    {
        return _dslPath.parent();
    }

    /**
     * @return конкурсная группа. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.ecg.EcgDistribObject#getCompetitionGroup()
     */
    public static CompetitionGroup.Path<CompetitionGroup> competitionGroup()
    {
        return _dslPath.competitionGroup();
    }

    /**
     * @return возмещение затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.ecg.EcgDistribObject#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return второе высшее. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.ecg.EcgDistribObject#isSecondHighAdmission()
     */
    public static PropertyPath<Boolean> secondHighAdmission()
    {
        return _dslPath.secondHighAdmission();
    }

    /**
     * @return заголовок. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.ecg.EcgDistribObject#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Дата добавления. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.ecg.EcgDistribObject#getRegistrationDate()
     */
    public static PropertyPath<Date> registrationDate()
    {
        return _dslPath.registrationDate();
    }

    /**
     * @return Утвержден. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.ecg.EcgDistribObject#isApproved()
     */
    public static PropertyPath<Boolean> approved()
    {
        return _dslPath.approved();
    }

    /**
     * @return Результаты распределения.
     * @see ru.tandemservice.uniec.entity.ecg.EcgDistribObject#getResultPrintForm()
     */
    public static DatabaseFile.Path<DatabaseFile> resultPrintForm()
    {
        return _dslPath.resultPrintForm();
    }

    /**
     * @return Резерв.
     * @see ru.tandemservice.uniec.entity.ecg.EcgDistribObject#getReservePrintForm()
     */
    public static DatabaseFile.Path<DatabaseFile> reservePrintForm()
    {
        return _dslPath.reservePrintForm();
    }

    public static class Path<E extends EcgDistribObject> extends EntityPath<E>
    {
        private EcgDistribObject.Path<EcgDistribObject> _parent;
        private CompetitionGroup.Path<CompetitionGroup> _competitionGroup;
        private CompensationType.Path<CompensationType> _compensationType;
        private PropertyPath<Boolean> _secondHighAdmission;
        private PropertyPath<String> _title;
        private PropertyPath<Date> _registrationDate;
        private PropertyPath<Boolean> _approved;
        private DatabaseFile.Path<DatabaseFile> _resultPrintForm;
        private DatabaseFile.Path<DatabaseFile> _reservePrintForm;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return основное распределение.
     * @see ru.tandemservice.uniec.entity.ecg.EcgDistribObject#getParent()
     */
        public EcgDistribObject.Path<EcgDistribObject> parent()
        {
            if(_parent == null )
                _parent = new EcgDistribObject.Path<EcgDistribObject>(L_PARENT, this);
            return _parent;
        }

    /**
     * @return конкурсная группа. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.ecg.EcgDistribObject#getCompetitionGroup()
     */
        public CompetitionGroup.Path<CompetitionGroup> competitionGroup()
        {
            if(_competitionGroup == null )
                _competitionGroup = new CompetitionGroup.Path<CompetitionGroup>(L_COMPETITION_GROUP, this);
            return _competitionGroup;
        }

    /**
     * @return возмещение затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.ecg.EcgDistribObject#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return второе высшее. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.ecg.EcgDistribObject#isSecondHighAdmission()
     */
        public PropertyPath<Boolean> secondHighAdmission()
        {
            if(_secondHighAdmission == null )
                _secondHighAdmission = new PropertyPath<Boolean>(EcgDistribObjectGen.P_SECOND_HIGH_ADMISSION, this);
            return _secondHighAdmission;
        }

    /**
     * @return заголовок. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.ecg.EcgDistribObject#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EcgDistribObjectGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Дата добавления. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.ecg.EcgDistribObject#getRegistrationDate()
     */
        public PropertyPath<Date> registrationDate()
        {
            if(_registrationDate == null )
                _registrationDate = new PropertyPath<Date>(EcgDistribObjectGen.P_REGISTRATION_DATE, this);
            return _registrationDate;
        }

    /**
     * @return Утвержден. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.ecg.EcgDistribObject#isApproved()
     */
        public PropertyPath<Boolean> approved()
        {
            if(_approved == null )
                _approved = new PropertyPath<Boolean>(EcgDistribObjectGen.P_APPROVED, this);
            return _approved;
        }

    /**
     * @return Результаты распределения.
     * @see ru.tandemservice.uniec.entity.ecg.EcgDistribObject#getResultPrintForm()
     */
        public DatabaseFile.Path<DatabaseFile> resultPrintForm()
        {
            if(_resultPrintForm == null )
                _resultPrintForm = new DatabaseFile.Path<DatabaseFile>(L_RESULT_PRINT_FORM, this);
            return _resultPrintForm;
        }

    /**
     * @return Резерв.
     * @see ru.tandemservice.uniec.entity.ecg.EcgDistribObject#getReservePrintForm()
     */
        public DatabaseFile.Path<DatabaseFile> reservePrintForm()
        {
            if(_reservePrintForm == null )
                _reservePrintForm = new DatabaseFile.Path<DatabaseFile>(L_RESERVE_PRINT_FORM, this);
            return _reservePrintForm;
        }

        public Class getEntityClass()
        {
            return EcgDistribObject.class;
        }

        public String getEntityName()
        {
            return "ecgDistribObject";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
