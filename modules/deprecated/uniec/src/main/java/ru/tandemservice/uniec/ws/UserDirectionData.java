/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.ws;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * ВНП онлайн абитуриента (DTO)
 *
 * @author agolubenko
 * @since May 11, 2010
 */
@XmlRootElement
public class UserDirectionData
{
    public long enrollmentDirectionId;
    public long compensationTypeId;
    public long studentCategoryId;
    public long competitionKindId;
    public boolean targetAdmission;
    public Integer profileWorkExperienceYears;
    public Integer profileWorkExperienceMonths;
    public Integer profileWorkExperienceDays;
}
