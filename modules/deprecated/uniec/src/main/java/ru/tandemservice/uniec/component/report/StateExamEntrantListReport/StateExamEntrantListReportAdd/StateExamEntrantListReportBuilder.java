// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.StateExamEntrantListReport.StateExamEntrantListReportAdd;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.write.*;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.JExcelUtil;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.ConversionScale;

import java.io.ByteArrayOutputStream;
import java.lang.Boolean;
import java.util.*;

/**
 * @author oleyba
 * @since 06.08.2010
 */
public class StateExamEntrantListReportBuilder
{
    protected final Log logger = LogFactory.getLog(getClass());

    private Model model;
    private Session session;

    private WritableCellFormat headerFormat;
    private WritableCellFormat totalsFormat;
    private WritableCellFormat contentFormat;
    private WritableCellFormat numbersFormat;
    private WritableCellFormat marksFormat;

    public StateExamEntrantListReportBuilder(Model model, Session session)
    {
        this.model = model;
        this.session = session;
    }

    public DatabaseFile getContentFormat() throws Exception
    {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        WritableWorkbook workbook = Workbook.createWorkbook(out, ws);

        WritableSheet sheet = workbook.createSheet("Перечень", 0);

        initFormats();

        Map<Long, StateExamSubject> discipline2stateExamSubject = getDiscipline2stateExamSubject();
        Set<Long> usedSubjects = new HashSet<>();
        Map<MultiKey, Integer> marks = new HashMap<>();

        MQBuilder stateExamMarks = new MQBuilder(StateExamSubjectMark.ENTITY_CLASS, "m", new String[]{
                StateExamSubjectMark.certificate().entrant().id().s(),
                StateExamSubjectMark.subject().id().s(),
                StateExamSubjectMark.mark().s()});
        stateExamMarks.add(MQExpression.in("m", StateExamSubjectMark.certificate().entrant().id().s(), getDirectionsBuilder()));
        for (Object[] row : stateExamMarks.<Object[]>getResultList(session))
        {
            Long entrantId = (Long) row[0];
            Long subject = (Long) row[1];
            int mark = (Integer) row[2];
            MultiKey key = new MultiKey(entrantId, subject);
            Integer previousMark = marks.get(key);
            if (null != previousMark) mark = Math.max(mark, previousMark);
            usedSubjects.add(subject);
            marks.put(key, mark);
        }

        MQBuilder markAppeals = new MQBuilder(ExamPassMarkAppeal.ENTITY_CLASS, "m", new String[] {
                ExamPassMarkAppeal.examPassMark().id().s(),
                ExamPassMarkAppeal.mark().s()});
        markAppeals.add(MQExpression.in("m", ExamPassMarkAppeal.examPassMark().examPassDiscipline().entrantExamList().entrant().id().s(), getDirectionsBuilder()));
        markAppeals.add(MQExpression.eq("m", ExamPassMarkAppeal.examPassMark().examPassDiscipline().subjectPassForm().code().s(), UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM_INTERNAL));
        Map<Long, Double> passMark2markAppeal = new HashMap<>();
        for (Object[] row : markAppeals.<Object[]>getResultList(session))
            passMark2markAppeal.put((Long) row[0], (Double) row[1]);

        MQBuilder examPassMarks = new MQBuilder(ExamPassMark.ENTITY_CLASS, "m", new String[] {
                ExamPassMark.id().s(),
                ExamPassMark.examPassDiscipline().entrantExamList().entrant().id().s(),
                ExamPassMark.examPassDiscipline().enrollmentCampaignDiscipline().id().s(),
                ExamPassMark.mark().s()});
        examPassMarks.add(MQExpression.in("m", ExamPassMark.examPassDiscipline().entrantExamList().entrant().id().s(), getDirectionsBuilder()));
        examPassMarks.add(MQExpression.eq("m", ExamPassMark.examPassDiscipline().subjectPassForm().code().s(), UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM_INTERNAL));
        for (Object[] row : examPassMarks.<Object[]>getResultList(session))
        {
            Long entrantId = (Long) row[1];
            StateExamSubject subject = discipline2stateExamSubject.get((Long) row[2]);
            if (null == subject) continue;
            Double doubleMark = (Double) row[3];
            Long markId = (Long) row[0];
            if (passMark2markAppeal.containsKey(markId))
                doubleMark = passMark2markAppeal.get(markId);
            int mark = (int) Math.round(doubleMark);
            MultiKey key = new MultiKey(entrantId, subject.getId());
            Integer previousMark = marks.get(key);
            if (null != previousMark) mark = Math.max(mark, previousMark);
            usedSubjects.add(subject.getId());
            marks.put(key, mark);
        }

        if (marks.isEmpty())
            throw new ApplicationException("Нет заявлений, подходящих под указанные параметры.");

        List<StateExamSubject> subjects = new ArrayList<>();
        for (StateExamSubject subject : UniDaoFacade.getCoreDao().getList(StateExamSubject.class))
            if (usedSubjects.contains(subject.getId()))
                subjects.add(subject);
        Collections.sort(subjects, new Comparator<StateExamSubject>(){@Override public int compare(StateExamSubject o1, StateExamSubject o2) { return Integer.valueOf(o1.getSubjectCTMOCode()).compareTo(Integer.valueOf(o2.getSubjectCTMOCode())); }});

        MQBuilder entrants = new MQBuilder(Entrant.ENTITY_CLASS, "e", new String[] {"id"});
        entrants.add(MQExpression.in("e", "id", getDirectionsBuilder()));
        entrants.add(MQExpression.or(
                MQExpression.exists(
                        new MQBuilder(ExamPassMark.ENTITY_CLASS, "m")
                        .add(MQExpression.eq("m", ExamPassMark.examPassDiscipline().subjectPassForm().code().s(), UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM_INTERNAL))
                        .add(MQExpression.eqProperty("m", ExamPassMark.examPassDiscipline().entrantExamList().entrant().id().s(), "e", "id"))),
                        MQExpression.exists(
                                new MQBuilder(StateExamSubjectMark.ENTITY_CLASS, "m")
                                .add(MQExpression.eqProperty("m", StateExamSubjectMark.certificate().entrant().id().s(), "e", "id"))
                        )));
        entrants.addJoin("e", Entrant.person().s(), "person");
        entrants.addJoin("person", Person.identityCard().s(), "idc");
        entrants.addOrder("idc", IdentityCard.lastName().s());
        entrants.addOrder("idc", IdentityCard.firstName().s());
        entrants.addOrder("idc", IdentityCard.middleName().s());
        entrants.addSelect("idc", new Object[] {
                IdentityCard.lastName().s(),
                IdentityCard.firstName().s(),
                IdentityCard.middleName().s(),
                IdentityCard.seria().s(),
                IdentityCard.number().s()});

        sheet.setColumnView(0, 27 / 7);
        sheet.setColumnView(1, 105 / 7);
        sheet.setColumnView(2, 112 / 7);
        sheet.setColumnView(3, 126 / 7);
        for (int i = 0; i < 2 + subjects.size(); i++ )
            sheet.setColumnView(i + 4, 57 / 7);
        sheet.setColumnView(6 + subjects.size(), 75 / 7);
        sheet.setRowView(1, 500);

        int column = 0;
        sheet.addCell(new Label(column++, 1, "№ п/п", headerFormat));
        sheet.addCell(new Label(column++, 1, "Фамилия", headerFormat));
        sheet.addCell(new Label(column++, 1, "Имя", headerFormat));
        sheet.addCell(new Label(column++, 1, "Отчество", headerFormat));
        sheet.addCell(new Label(column++, 1, "Серия паспорта", headerFormat));
        sheet.addCell(new Label(column++, 1, "№ паспорта", headerFormat));
        for (StateExamSubject subject : subjects)
            sheet.addCell(new Label(column++, 1, subject.getTitle(), headerFormat));
        sheet.addCell(new Label(column, 1, "Сумма", headerFormat));

        int row = 2;
        for (Object[] entrantData : entrants.<Object[]>getResultList(session))
        {
            Long entrantId = (Long) entrantData[0];

            column = 0; int sum = 0;
            sheet.addCell(new Label(column++, row, String.valueOf(row - 1), numbersFormat));
            sheet.addCell(new Label(column, row, (String) entrantData[column++], contentFormat));
            sheet.addCell(new Label(column, row, (String) entrantData[column++], contentFormat));
            sheet.addCell(new Label(column, row, (String) entrantData[column++], contentFormat));
            sheet.addCell(new Label(column, row, (String) entrantData[column++], marksFormat));
            sheet.addCell(new Label(column, row, (String) entrantData[column++], marksFormat));
            for (StateExamSubject subject : subjects)
            {
                Integer mark = marks.get(new MultiKey(entrantId, subject.getId()));
                sheet.addCell(JExcelUtil.getNumberNullableCell(column++, row, mark, marksFormat));
                if (null != mark) sum = sum + mark;
            }
            sheet.addCell(JExcelUtil.getNumberNullableCell(column, row++, sum, totalsFormat));
        }

        workbook.write();
        workbook.close();

        DatabaseFile content = new DatabaseFile();
        content.setContent(out.toByteArray());
        return content;
    }



    private MQBuilder getDirectionsBuilder()
    {
        MQBuilder builder =new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "red", new String[]{RequestedEnrollmentDirection.entrantRequest().entrant().id().s()});
        if (model.getEnrollmentCampaignStage().getId() == Model.ENROLLMENT_CAMP_STAGE_DOCUMENTS)
        {
            builder.add(MQExpression.notEq("red", RequestedEnrollmentDirection.state().code().s(), UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));

            if (model.isCompensationTypeActive())
                builder.add(MQExpression.eq("red", RequestedEnrollmentDirection.compensationType().s(), model.getCompensationType()));
            if (model.isStudentCategoryActive())
                builder.add(MQExpression.in("red", RequestedEnrollmentDirection.studentCategory().s(), model.getStudentCategoryList()));
        }
        if (model.getEnrollmentCampaignStage().getId() == Model.ENROLLMENT_CAMP_STAGE_EXAMS)
        {
            // состояние выбранной дисциплины приемной кампании должно быть любое состояние, кроме «Забрал документы», «Выбыл из конкурса», «Активный»
            builder.add(MQExpression.notIn("red", RequestedEnrollmentDirection.state().code().s(),
                    UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY,
                    UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE,
                    UniecDefines.ENTRANT_STATE_ACTIVE_CODE));
            if (model.isCompensationTypeActive())
                builder.add(MQExpression.eq("red", RequestedEnrollmentDirection.compensationType().s(), model.getCompensationType()));
            if (model.isStudentCategoryActive())
                builder.add(MQExpression.in("red", RequestedEnrollmentDirection.studentCategory().s(), model.getStudentCategoryList()));
        }
        if (model.getEnrollmentCampaignStage().getId() == Model.ENROLLMENT_CAMP_STAGE_ENROLLMENT)
        {
            builder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "p", new String[]{PreliminaryEnrollmentStudent.requestedEnrollmentDirection().entrantRequest().entrant().id().s()});
            builder.addJoin("p", PreliminaryEnrollmentStudent.requestedEnrollmentDirection().s(), "red");
            if (model.isCompensationTypeActive())
                builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.compensationType().s(), model.getCompensationType()));
            if (model.isStudentCategoryActive())
                builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.studentCategory().s(), model.getStudentCategoryList()));
            if (model.isParallelActive() && model.getParallel().isTrue())
                builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.parallel().s(), Boolean.TRUE));
        }

        builder.addJoin("red", RequestedEnrollmentDirection.enrollmentDirection().s(), "ed");
        builder.addJoin("ed", EnrollmentDirection.educationOrgUnit().s(), "eduOu");
        builder.addJoin("red", RequestedEnrollmentDirection.entrantRequest().s(), "req");

        builder.add(MQExpression.eq("ed", EnrollmentDirection.enrollmentCampaign().s(), model.getReport().getEnrollmentCampaign()));
        builder.add(UniMQExpression.betweenDate("req", EntrantRequest.regDate().s(), model.getReport().getDateFrom(), model.getReport().getDateTo()));
        builder.add(MQExpression.eq("req", EntrantRequest.entrant().archival().s(), Boolean.FALSE));
        if (model.isTechnicCommissionActive())
            builder.add(MQExpression.eq("req", EntrantRequest.technicCommission().s(), model.getTechnicCommission().getTitle()));
        if (model.isCompetitionGroupActive())
            builder.add(MQExpression.eq("ed", EnrollmentDirection.competitionGroup().s(), model.getCompetitionGroup()));
        if (model.isQualificationActive())
            builder.add(MQExpression.in("eduOu", EducationOrgUnit.educationLevelHighSchool().educationLevel().qualification().s(), model.getQualificationList()));
        if (model.isDevelopFormActive())
            builder.add(MQExpression.in("eduOu", EducationOrgUnit.developForm().s(), model.getDevelopFormList()));
        if (model.isDevelopConditionActive())
            builder.add(MQExpression.in("eduOu", EducationOrgUnit.developCondition().s(), model.getDevelopConditionList()));
        if (model.isDevelopTechActive())
            builder.add(MQExpression.in("eduOu", EducationOrgUnit.developTech().s(), model.getDevelopTechList()));
        if (model.isDevelopPeriodActive())
            builder.add(MQExpression.in("eduOu", EducationOrgUnit.developPeriod().s(), model.getDevelopPeriodList()));
        if (model.isEducationLevelHighSchoolActive())
            builder.add(MQExpression.in("eduOu", EducationOrgUnit.educationLevelHighSchool().s(), model.getEducationLevelHighSchoolList()));
        if (model.isFormativeOrgUnitActive())
            builder.add(MQExpression.in("eduOu", EducationOrgUnit.territorialOrgUnit().s(), model.getFormativeOrgUnitList()));
        if (model.isTerritorialOrgUnitActive())
            builder.add(model.getTerritorialOrgUnitList().isEmpty() ?
                    MQExpression.isNull("eduOu", EducationOrgUnit.territorialOrgUnit().s())
                    :
                        MQExpression.in("eduOu", EducationOrgUnit.territorialOrgUnit().s(), model.getTerritorialOrgUnitList())
            );

        return builder;
    }

    @SuppressWarnings("unchecked")
    private Map<Long, StateExamSubject> getDiscipline2stateExamSubject()
    {
        List<ConversionScale> list = session.createCriteria(ConversionScale.class).list();
        Map<Long, StateExamSubject> map = new HashMap<>();
        for (ConversionScale scale : list)
            map.put(scale.getDiscipline().getId(), scale.getSubject());
        return map;
    }

    private void initFormats() throws Exception
    {
        WritableFont arial10 = new WritableFont(WritableFont.ARIAL, 10);
        WritableFont arial10bold = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);

        headerFormat = new WritableCellFormat(arial10bold);
        headerFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
        headerFormat.setWrap(true);
        headerFormat.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);

        totalsFormat = new WritableCellFormat(arial10bold);
        totalsFormat.setAlignment(Alignment.CENTRE);
        totalsFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);

        contentFormat = new WritableCellFormat(arial10);
        contentFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);

        numbersFormat = new WritableCellFormat(arial10);
        numbersFormat.setAlignment(Alignment.RIGHT);
        numbersFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);

        marksFormat = new WritableCellFormat(arial10);
        marksFormat.setAlignment(Alignment.CENTRE);
        marksFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
    }
}
