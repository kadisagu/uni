/* $Id$ */
package ru.tandemservice.uniec.component.catalog.entrantCustomStateCI.EntrantCustomStateCIAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditController;
import ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI;

/**
 * @author nvankov
 * @since 4/3/13
 */
public class Controller extends DefaultCatalogAddEditController<EntrantCustomStateCI, Model, IDAO>
{
}
