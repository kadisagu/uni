/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.dao.examgroup;

import ru.tandemservice.uni.dao.UniBaseDao;

/**
 * @author Vasily Zhukov
 * @since 31.05.2010
 */
public abstract class AbstractExamGroupLogic extends UniBaseDao implements IAbstractExamGroupLogic
{
}
