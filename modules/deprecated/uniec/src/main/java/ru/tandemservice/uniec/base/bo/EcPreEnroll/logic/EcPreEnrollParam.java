/* $Id: $ */
package ru.tandemservice.uniec.base.bo.EcPreEnroll.logic;

import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import java.util.List;

/**
 * @author Andrey Andreev
 * @since 12.04.2017
 */
public class EcPreEnrollParam
{
    private EnrollmentDirection _enrollmentDirection;
    private CompensationType _compensationType;
    private List<StudentCategory> _studentCategoryList;
    private Boolean _originalDocumentHandledIn;
    private Boolean _agree4Enrollment;
    private Boolean _entrantCustomStateExist;
    private List<EntrantCustomStateCI>_entrantCustomStateList;


    public EnrollmentDirection getEnrollmentDirection()
    {
        return _enrollmentDirection;
    }

    public void setEnrollmentDirection(EnrollmentDirection enrollmentDirection)
    {
        _enrollmentDirection = enrollmentDirection;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

    public List<StudentCategory> getStudentCategoryList()
    {
        return _studentCategoryList;
    }

    public void setStudentCategoryList(List<StudentCategory> studentCategoryList)
    {
        _studentCategoryList = studentCategoryList;
    }

    public Boolean getOriginalDocumentHandledIn()
    {
        return _originalDocumentHandledIn;
    }

    public void setOriginalDocumentHandledIn(Boolean originalDocumentHandledIn)
    {
        _originalDocumentHandledIn = originalDocumentHandledIn;
    }

    public Boolean getAgree4Enrollment()
    {
        return _agree4Enrollment;
    }

    public void setAgree4Enrollment(Boolean agree4Enrollment)
    {
        _agree4Enrollment = agree4Enrollment;
    }

    public Boolean getEntrantCustomStateExist()
    {
        return _entrantCustomStateExist;
    }

    public void setEntrantCustomStateExist(Boolean entrantCustomStateExist)
    {
        _entrantCustomStateExist = entrantCustomStateExist;
    }

    public List<EntrantCustomStateCI> getEntrantCustomStateList()
    {
        return _entrantCustomStateList;
    }

    public void setEntrantCustomStateList(List<EntrantCustomStateCI> entrantCustomStateList)
    {
        _entrantCustomStateList = entrantCustomStateList;
    }
}