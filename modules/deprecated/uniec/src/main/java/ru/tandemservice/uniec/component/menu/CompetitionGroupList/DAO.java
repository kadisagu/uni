/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.menu.CompetitionGroupList;

import org.springframework.context.MessageSource;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.NumberAsStringComparator;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.DelegatePropertyComparator;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.ecg.EcgDistribObject;
import ru.tandemservice.uniec.entity.entrant.CompetitionGroup;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author vip_delete
 * @since 27.05.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("d");

    @Override
    public void prepare(Model model)
    {
        MQBuilder builder = new MQBuilder(EnrollmentCampaign.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", EnrollmentCampaign.P_USE_COMPETITION_GROUP, Boolean.TRUE));
        builder.addOrder("e", EnrollmentCampaign.P_ID, OrderDirection.desc);

        List<EnrollmentCampaign> list = builder.getResultList(getSession());
        if (model.getEnrollmentCampaign() == null && !list.isEmpty())
            model.setEnrollmentCampaign(list.get(0));
        model.setEnrollmentCampaignList(list);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void deleteEmptyCompetitionGroups(Model model)
    {
        String hql = "from " + CompetitionGroup.ENTITY_CLASS + " g where 0=(select count(*) from " + EnrollmentDirection.ENTITY_CLASS + " d where d." + EnrollmentDirection.L_COMPETITION_GROUP + "=g)";
        List<CompetitionGroup> list = getSession().createQuery(hql).list();
        for (CompetitionGroup group : list)
            delete(group);
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        List<EnrollmentDirection> list;
        if (model.getEnrollmentCampaign() == null)
        {
            list = new ArrayList<>();
            model.getDataSource().setCountRow(list.size());
            UniBaseUtils.createPage(model.getDataSource(), list);
        } else
        {
            MQBuilder builder = new MQBuilder(EnrollmentDirection.ENTITY_CLASS, "d");
            builder.add(MQExpression.eq("d", EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
            builder.add(MQExpression.isNotNull("d", EnrollmentDirection.L_COMPETITION_GROUP));

            EntityOrder entityOrder = model.getDataSource().getEntityOrder();
            if (entityOrder.getKey().equals(EnrollmentDirection.competitionGroup().title().s()))
            {
                List<EnrollmentDirection> result = builder.getResultList(getSession());
                Collections.sort(result, new DelegatePropertyComparator(NumberAsStringComparator.INSTANCE, EnrollmentDirection.competitionGroup().title().s()));
                if (entityOrder.getDirection() == OrderDirection.desc)
                {
                    Collections.reverse(result);
                }
                UniBaseUtils.createPage(model.getDataSource(), result);
            }
            else
            {
                _orderSettings.applyOrder(builder, entityOrder);
                UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
            }
            list = builder.getResultList(getSession());
        }

        {
            MQBuilder builder = new MQBuilder(EcgDistribObject.ENTITY_CLASS, "distr", new String[]{EcgDistribObject.L_COMPETITION_GROUP+".id"});
            builder.add(MQExpression.in("distr", EcgDistribObject.L_COMPETITION_GROUP, CommonBaseEntityUtil.getPropertiesList(list, EnrollmentDirection.L_COMPETITION_GROUP)));
            builder.setNeedDistinct(true);
            model.setDistributedIds(builder.<Long>getResultList(getSession()));
        }
    }

    @Override
    public void delete(Long id, MessageSource messageSource)
    {
        EnrollmentDirection direction = getNotNull(EnrollmentDirection.class, id);
        CompetitionGroup group = direction.getCompetitionGroup();
        if (existsEntity(EcgDistribObject.class, EcgDistribObject.L_COMPETITION_GROUP, group))
        {
            throw new ApplicationException(messageSource.getMessage("competition.group.distributed.exception", null, null));
        }
        for (EnrollmentDirection item : getList(EnrollmentDirection.class, EnrollmentDirection.L_COMPETITION_GROUP, direction.getCompetitionGroup()))
        {
            item.setCompetitionGroup(null);
            getSession().update(item);
        }
        getSession().delete(group);
    }
}
