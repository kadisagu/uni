/* $Id$ */
package ru.tandemservice.uniec.component.settings.OnlineRequestSetting;

import java.util.Arrays;

import org.tandemframework.core.entity.IdentifiableWrapper;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

/**
 * @author Vasily Zhukov
 * @since 03.06.2011
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());

        IdentifiableWrapper notAllow = new IdentifiableWrapper(Model.NOT_ALLOW_ID, "Запрещена");
        IdentifiableWrapper allow = new IdentifiableWrapper(Model.ALLOW_ID, "Разрешена");
        model.setYesNoList(Arrays.asList(notAllow, allow));

        EnrollmentCampaign enrollmentCampaign = model.getEnrollmentCampaign();
        if (enrollmentCampaign != null)
        {
            model.setAllowUploadDocuments(enrollmentCampaign.isAllowUploadDocuments() ? allow : notAllow);
            model.setAllowPrintOnlineRequest(enrollmentCampaign.isAllowPrintOnlineRequest() ? allow : notAllow);
        }
    }

    @Override
    public void update(Model model)
    {
        EnrollmentCampaign enrollmentCampaign = model.getEnrollmentCampaign();
        if (enrollmentCampaign != null)
        {
            enrollmentCampaign.setAllowUploadDocuments(Model.ALLOW_ID.equals(model.getAllowUploadDocuments().getId()));
            enrollmentCampaign.setAllowPrintOnlineRequest(Model.ALLOW_ID.equals(model.getAllowPrintOnlineRequest().getId()));
            update(enrollmentCampaign);
        }
    }
}
