package ru.tandemservice.uniec.entity.entrant;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.view.formatter.DateFormatter;

import ru.tandemservice.uniec.entity.entrant.gen.EnrollmentCampaignPeriodGen;

public class EnrollmentCampaignPeriod extends EnrollmentCampaignPeriodGen implements ITitled
{
    public static final String P_TITLE = "title";

    @Override
    public String getTitle()
    {
        return "c " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }
}