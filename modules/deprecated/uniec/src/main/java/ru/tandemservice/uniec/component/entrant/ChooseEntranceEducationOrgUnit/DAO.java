/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.ChooseEntranceEducationOrgUnit;

import org.tandemframework.core.CoreServices;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline;
import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline2SetDisciplineRelation;
import ru.tandemservice.uniec.entity.examset.ExamSet;
import ru.tandemservice.uniec.entity.examset.ExamSetItem;
import ru.tandemservice.uniec.entity.settings.SetDiscipline;
import ru.tandemservice.uniec.util.ExamSetUtil;

import java.util.*;

/**
 * @author Боба
 * @since 22.08.2008
 */
public class DAO extends UniDao<ChooseEntranceEducationOrgUnitModel> implements IDAO
{
    @Override
    public void prepare(final ChooseEntranceEducationOrgUnitModel model)
    {
        final ExamSet examSet = ExamSetUtil.getExamSet(model.getExamSetId());
        if (examSet == null)
            throw new ApplicationException("Выбранный набор уже не существует.");
        model.setExamSet(examSet);

        // отображает направления приема из наборов такой же категории поступающего
        // помним, что пустой набор может существовать для каждой категории поступающего
        Map<Long, String> id2title = new HashMap<>();
        for (ExamSet item : UniecDAOFacade.getExamSetDAO().getExamSetMap(examSet.getEnrollmentCampaign()).values())
            if (examSet.getStudentCategory().equals(item.getStudentCategory()))
                for (EnrollmentDirection enrollmentDirection : item.getList())
                    id2title.put(enrollmentDirection.getId(), item.getTitle());
        model.setId2title(id2title);

        model.setFormativeOrgUnitModel(new FullCheckSelectModel(OrgUnit.P_FULL_TITLE)
        {
            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(EnrollmentDirection.ENTITY_CLASS, "e");
                builder.add(MQExpression.eq("e", EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, model.getExamSet().getEnrollmentCampaign()));
                Set<EnrollmentDirection> enrollmentDirectionSet = new HashSet<>(builder.<EnrollmentDirection>getResultList(DataAccessServices.dao().getComponentSession()));
                enrollmentDirectionSet.removeAll(examSet.getList());

                Set<OrgUnit> orgUnitSet = new HashSet<>();
                for (EnrollmentDirection direction : enrollmentDirectionSet)
                {
                    OrgUnit orgUnit = direction.getEducationOrgUnit().getFormativeOrgUnit();
                    if (CoreServices.securityService().check(orgUnit, model.getPrincipalContext(), "orgUnit_viewPub_" + orgUnit.getOrgUnitType().getCode()))
                        orgUnitSet.add(orgUnit);
                }

                List<Long> ids = new ArrayList<>();
                for (OrgUnit orgUnit : orgUnitSet)
                    ids.add(orgUnit.getId());

                MQBuilder resultBuilder = new MQBuilder(OrgUnit.ENTITY_CLASS, "o");
                resultBuilder.add(MQExpression.in("o", OrgUnit.P_ID, ids));
                if (filter != null)
                    resultBuilder.add(MQExpression.like("o", OrgUnit.P_TITLE, "%" + filter));
                List<OrgUnit> result = resultBuilder.getResultList(DataAccessServices.dao().getComponentSession());

                Collections.sort(result, new EntityComparator<OrgUnit>(new EntityOrder(OrgUnit.P_FULL_TITLE)));
                return new ListResult<>(result, ids.size());
            }
        });

        model.setTerritorialOrgUnitModel(new FullCheckSelectModel(OrgUnit.P_TERRITORIAL_FULL_TITLE)
        {
            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(EnrollmentDirection.ENTITY_CLASS, "e");
                builder.add(MQExpression.eq("e", EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, model.getExamSet().getEnrollmentCampaign()));
                builder.add(MQExpression.eq("e", EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnit()));
                Set<EnrollmentDirection> enrollmentDirectionSet = new HashSet<>(builder.<EnrollmentDirection>getResultList(DataAccessServices.dao().getComponentSession()));
                enrollmentDirectionSet.removeAll(examSet.getList());

                Set<OrgUnit> orgUnitSet = new HashSet<>();
                for (EnrollmentDirection direction : enrollmentDirectionSet)
                {
                    OrgUnit orgUnit = direction.getEducationOrgUnit().getTerritorialOrgUnit();
                    if (orgUnit != null && CoreServices.securityService().check(orgUnit, model.getPrincipalContext(), "orgUnit_viewPub_" + orgUnit.getOrgUnitType().getCode()))
                        orgUnitSet.add(orgUnit);
                }

                List<Long> ids = new ArrayList<>();
                for (OrgUnit orgUnit : orgUnitSet)
                    ids.add(orgUnit.getId());

                MQBuilder resultBuilder = new MQBuilder(OrgUnit.ENTITY_CLASS, "o");
                resultBuilder.add(MQExpression.in("o", OrgUnit.P_ID, ids));
                if (filter != null)
                    resultBuilder.add(MQExpression.like("o", OrgUnit.P_TITLE, "%" + filter));
                List<OrgUnit> result = resultBuilder.getResultList(DataAccessServices.dao().getComponentSession());

                Collections.sort(result, new EntityComparator<OrgUnit>(new EntityOrder(OrgUnit.P_TERRITORIAL_FULL_TITLE)));
                return new ListResult<>(result, result.size());
            }
        });
    }

    @Override
    public void prepareListDataSource(ChooseEntranceEducationOrgUnitModel model)
    {
        MQBuilder builder = new MQBuilder(EnrollmentDirection.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, model.getExamSet().getEnrollmentCampaign()));
        builder.add(MQExpression.eq("e", EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnit()));
        builder.add(MQExpression.eq("e", EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnit()));

        Set<EnrollmentDirection> set = new HashSet<>();
        set.addAll(builder.<EnrollmentDirection>getResultList(getSession()));
        set.removeAll(model.getExamSet().getList());

        List<EnrollmentDirection> result = new ArrayList<>();
        for (EnrollmentDirection direction : set)
            if (model.getId2title().containsKey(direction.getId()))
                result.add(direction);

        Collections.sort(result, new EntityComparator<EnrollmentDirection>(model.getDataSource().getEntityOrder()));
        model.getDataSource().setCountRow(result.size());
        UniBaseUtils.createPage(model.getDataSource(), result);
    }

    @Override
    public void update(ChooseEntranceEducationOrgUnitModel model)
    {
        ExamSet examSet = ExamSetUtil.getExamSet(model.getExamSetId());
        if (examSet == null)
            throw new ApplicationException("Выбранный набор уже не существует.");

        for (IEntity entity : ((CheckboxColumn) model.getDataSource().getColumn("checkbox")).getSelectedObjects())
        {
            EnrollmentDirection enrollmentDirection = (EnrollmentDirection) entity;
            if (model.getExamSet().getList().contains(enrollmentDirection))
                throw new ApplicationException("Одно из выбранных направлений приема уже добавлено в текущий набор.");

            // удаляем все вступительные испытания из направления приема в такой же категории обучающегося.
            // дисциплины по выбору удалятся каскадно
            MQBuilder builder = new MQBuilder(EntranceDiscipline.ENTITY_CLASS, "d");
            builder.add(MQExpression.eq("d", EntranceDiscipline.L_ENROLLMENT_DIRECTION, enrollmentDirection));
            builder.add(MQExpression.eq("d", EntranceDiscipline.L_STUDENT_CATEGORY, examSet.getStudentCategory()));

            for (EntranceDiscipline entranceDiscipline : builder.<EntranceDiscipline>getResultList(getSession()))
                delete(entranceDiscipline);

            // добавляем новые вступительные испытания на это направление приема
            for (ExamSetItem examSetItem : model.getExamSet().getSetItemList())
            {
                EntranceDiscipline entranceDiscipline = new EntranceDiscipline();
                entranceDiscipline.setBudget(examSetItem.isBudget());
                entranceDiscipline.setContract(examSetItem.isContract());
                entranceDiscipline.setKind(examSetItem.getKind());
                entranceDiscipline.setType(examSetItem.getType());
                entranceDiscipline.setStudentCategory(model.getExamSet().getStudentCategory());
                entranceDiscipline.setDiscipline(examSetItem.getSubject());
                entranceDiscipline.setEnrollmentDirection(enrollmentDirection);
                save(entranceDiscipline);

                for (SetDiscipline discipline : examSetItem.getChoice())
                {
                    EntranceDiscipline2SetDisciplineRelation rel = new EntranceDiscipline2SetDisciplineRelation();
                    rel.setEntranceDiscipline(entranceDiscipline);
                    rel.setSetDiscipline(discipline);
                    save(rel);
                }
            }
        }
    }
}
