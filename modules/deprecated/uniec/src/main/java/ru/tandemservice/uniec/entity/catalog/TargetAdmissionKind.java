package ru.tandemservice.uniec.entity.catalog;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.tool.tree.IHierarchyItem;
import ru.tandemservice.uniec.entity.catalog.gen.TargetAdmissionKindGen;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * targetAdmissionKind
 */
public class TargetAdmissionKind extends TargetAdmissionKindGen implements IHierarchyItem
{
    /**
     * Сравнивает объекты одного уровня иерархии 
     */
    public static final Comparator<TargetAdmissionKind> PRIORITY_COMPARATOR = (o1, o2) -> {
        if (o1 == null && o2 == null)
            return 0;
        else if (o1 == null)
            return 1;
        else if (o2 == null)
            return -1;
        return Long.compare((long) o1.getPriority(), (long) o2.getPriority());
    };

    /**
     * Сравнивает объекты любых уровней иерархии
     */
    public static final Comparator<TargetAdmissionKind> ALL_LEVEL_PRIORITY_COMPARATOR = new Comparator<TargetAdmissionKind>()
    {
        @Override
        public int compare(TargetAdmissionKind o1, TargetAdmissionKind o2)
        {
            // проверки на null
            if (o1 == null && o2 == null)
                return 0;
            else if (o1 == null)
                return 1;
            else if (o2 == null)
                return -1;
            // один и тот же элемент?
            if (o1.equals(o2))
                return 0;
            TargetAdmissionKind p1 = o1.getParent();
            TargetAdmissionKind p2 = o2.getParent();
            // один и тот же корень?
            if (equals(p1, p2))
                return Long.compare((long) o1.getPriority(), (long) o2.getPriority());
            // o2 отец o1
            if (equals(p1, o2))
                return 1;
            // o1 отец o2
            if (equals(o1, p2))
                return -1;
            // иначе просто сравнить приоритеты корневых элементов
            return Long.compare((long) ((p1 != null) ? p1 : o1).getPriority(), (long) ((p2 != null) ? p2 : o2).getPriority());
        }

        private boolean equals(TargetAdmissionKind o1, TargetAdmissionKind o2)
        {
            return (o1 != null && o1.equals(o2));
        }
    };

    @Override
    public IHierarchyItem getHierarhyParent()
    {
        return getParent();
    }

    @Override
    @EntityDSLSupport
    public String getFullHierarhyTitle()
    {
        List<String> chain = new ArrayList<>();

        TargetAdmissionKind targetAdmissionKind = this;
        while (targetAdmissionKind != null)
        {
            chain.add(targetAdmissionKind.getTitle());
            targetAdmissionKind = targetAdmissionKind.getParent();
        }

        Collections.reverse(chain);
        return StringUtils.join(chain, " — ");
    }
}