/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.settings.ConversionScale;

import java.util.List;
import java.util.Map;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.tapsupport.component.matrix.CellCoordinates;

import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

/**
 * @author agolubenko
 * @since 25.06.2008
 */
public class Model implements IEnrollmentCampaignSelectModel
{
    private String _selectedPage;

    private List<EnrollmentCampaign> _enrollmentCampaignList;

    private Map<Matrix, Map<CellCoordinates, Integer>> _budgetMatrix2Data;
    private Map<Matrix, Map<CellCoordinates, Integer>> _contractMatrix2Data;

    private List<Matrix> _matrices;
    private Matrix _matrix;

    private IDataSettings _settings;

    public boolean isActive()
    {
        return !_matrices.isEmpty();
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return (EnrollmentCampaign) getSettings().get(ENROLLMENT_CAMPAIGN_FILTER_NAME);
    }
    
    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        getSettings().set(ENROLLMENT_CAMPAIGN_FILTER_NAME, enrollmentCampaign);
    }

    public boolean isDifferenceExist()
    {
        return getEnrollmentCampaign().isEnrollmentPerCompTypeDiff();
    }

    public String getSelectedPage()
    {
        return _selectedPage;
    }

    public void setSelectedPage(String selectedPage)
    {
        _selectedPage = selectedPage;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public Map<Matrix, Map<CellCoordinates, Integer>> getBudgetMatrix2Data()
    {
        return _budgetMatrix2Data;
    }

    public void setBudgetMatrix2Data(Map<Matrix, Map<CellCoordinates, Integer>> budgetMatrix2Data)
    {
        _budgetMatrix2Data = budgetMatrix2Data;
    }

    public Map<Matrix, Map<CellCoordinates, Integer>> getContractMatrix2Data()
    {
        return _contractMatrix2Data;
    }

    public void setContractMatrix2Data(Map<Matrix, Map<CellCoordinates, Integer>> contractMatrix2Data)
    {
        _contractMatrix2Data = contractMatrix2Data;
    }

    public List<Matrix> getMatrices()
    {
        return _matrices;
    }

    public void setMatrices(List<Matrix> matrices)
    {
        _matrices = matrices;
    }

    public Matrix getMatrix()
    {
        return _matrix;
    }

    public void setMatrix(Matrix matrix)
    {
        _matrix = matrix;
    }

    @Override
    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }
}