/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.InfoSources.InfoSourcesAdd;

import org.hibernate.Session;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.SourceInfoAboutUniversity;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author ekachanova
 */
class InfoSourcesReportBuilder
{
    private Model _model;
    private Session _session;

    InfoSourcesReportBuilder(Model model, Session session)
    {
        _model = model;
        _session = session;
    }

    DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    private byte[] buildReport()
    {
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_INFO_SOURCES);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());
        TopOrgUnit academy = TopOrgUnit.getInstance();

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("academyTitle", academy.getTitle());
        injectModifier.put("academyShortTitle", academy.getShortTitle());
        injectModifier.put("year", _model.getReport().getEnrollmentCampaign().getEducationYear().getTitle().split("/")[0]);
        injectModifier.put("enrollmentCampaignStage", _model.getReport().getEnrollmentCampaignStage());

        injectModifier.modify(document);

        final List<SourceInfoAboutUniversity> infoSources = UniDaoFacade.getCoreDao().getCatalogItemList(SourceInfoAboutUniversity.class);

        Map<SourceInfoAboutUniversity, Set<Long>> infoSource2entrantIds = new HashMap<>();
        Set<Long> total = new HashSet<>();

        DQLSelectBuilder builder;
        switch (_model.getEnrollmentCampaignStage().getId().intValue())
        {
            case Model.ENROLLMENT_CAMP_STAGE_DOCUMENTS:
                builder = getRequestedBuilder();
                builder.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_STATE, "state");
                builder.where(ne(property("state." + EntrantState.P_CODE), UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
                break;
            case Model.ENROLLMENT_CAMP_STAGE_EXAMS:
                builder = getRequestedBuilder();
                builder.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_STATE, "state");
                builder.where(notIn(property("state." + EntrantState.P_CODE), Arrays.asList(
                        UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY,
                        UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE,
                        UniecDefines.ENTRANT_STATE_ACTIVE_CODE
                )));
                break;
            case Model.ENROLLMENT_CAMP_STAGE_ENROLLMENT:
                builder = getPreliminaryBuilder();
                break;
            default:
                throw new RuntimeException("Unknown report stage!");
        }

        builder.joinEntity("entrant", DQLJoinType.left, EntrantInfoAboutUniversity.class, "infoSource", "entrant.id=infoSource.entrant");
        builder.joinPath(DQLJoinType.left, "infoSource." + EntrantInfoAboutUniversity.L_SOURCE_INFO, "infoItem");
        builder.column(property("entrant.id"), "entrantId");
        builder.column(property("infoItem"), "infoItem");
        process(builder, infoSource2entrantIds, total);

        RtfTable table = (RtfTable) UniRtfUtil.findElement(document.getElementList(), "T");
        RtfRow header = table.getRowList().get(1);
        if (!infoSources.isEmpty())
        {
            int[] parts = new int[infoSources.size()];
            Arrays.fill(parts, 1);
            RtfUtil.splitRow(header, 1, (newCell, index) -> newCell.setElementList(Arrays.asList((IRtfElement) RtfBean.getElementFactory().createRtfText(infoSources.get(index).getTitle()))), parts);
        }
        RtfRowIntercepterBase splitIntercepter = new RtfRowIntercepterBase()
        {
            @Override
            public void beforeModify(RtfTable table, int currentRowIndex)
            {
                int[] parts = new int[infoSources.size()];
                Arrays.fill(parts, 1);
                RtfUtil.splitRow(table.getRowList().get(currentRowIndex), 1, null, parts);
            }
        };
        String[][] data = new String[2][infoSources.size() + 1];
        data[0] = new String[infoSources.size() + 1];
        data[0][0] = String.valueOf(total.size());
        data[1] = new String[infoSources.size() + 1];
        data[1][0] = "в %";
        int i = 0;
        int totalSize = total.size();
        for (SourceInfoAboutUniversity infoSource : infoSources)
        {
            Set<Long> set = infoSource2entrantIds.get(infoSource);
            data[0][i + 1] = set == null ? "0" : String.valueOf(set.size());
            data[1][i + 1] = set == null || totalSize == 0 ? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(((double) set.size() * 100) / totalSize);
            i++;
        }

        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T", data);
        tableModifier.put("T", splitIntercepter);
        tableModifier.modify(document);

        RtfCell percent = table.getRowList().get(3).getCellList().get(0);
        IRtfControl b = RtfBean.getElementFactory().createRtfControl(IRtfData.B);
        IRtfControl b0 = RtfBean.getElementFactory().createRtfControl(IRtfData.B, 0);
//        b0.setValue(0);
        percent.getElementList().add(9, b);
        percent.getElementList().add(b0);

        return RtfUtil.toByteArray(document);
    }

    private void process(DQLSelectBuilder builder, Map<SourceInfoAboutUniversity, Set<Long>> infoSource2entrantIds, Set<Long> total)
    {
        for (Object obj : builder.createStatement(new DQLExecutionContext(_session)).list())
        {
            long humanId = ((Number) ((Object[]) obj)[0]).longValue();  //абитуриет или студент предзачисления
            SourceInfoAboutUniversity infoSource = (SourceInfoAboutUniversity) ((Object[]) obj)[1];
            Set<Long> set = infoSource2entrantIds.get(infoSource);
            if (set == null)
                infoSource2entrantIds.put(infoSource, set = new HashSet<>());
            set.add(humanId);
            total.add(humanId);
        }
    }

    private DQLSelectBuilder getRequestedBuilder()
    {
        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(RequestedEnrollmentDirection.class, "d");
        builder.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, "ed");
        builder.joinPath(DQLJoinType.inner, "ed." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "ou");
        builder.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_COMPENSATION_TYPE, "ct");

        patchEduOrgUnit(builder, "d." + PreliminaryEnrollmentStudent.L_STUDENT_CATEGORY);

        return builder;
    }

    private DQLSelectBuilder getPreliminaryBuilder()
    {
        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(PreliminaryEnrollmentStudent.class, "p");
        builder.joinPath(DQLJoinType.inner, "p." + PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, "d");
        builder.joinPath(DQLJoinType.inner, "p." + PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT, "ou");
        builder.joinPath(DQLJoinType.inner, "p." + PreliminaryEnrollmentStudent.L_COMPENSATION_TYPE, "ct");

        patchEduOrgUnit(builder, "p." + PreliminaryEnrollmentStudent.L_STUDENT_CATEGORY);

        if (_model.isParallelActive() && _model.getReport().getExcludeParallel())
            builder.where(eq(property("p." + PreliminaryEnrollmentStudent.P_PARALLEL), value(Boolean.FALSE)));
        return builder;
    }

    private void patchEduOrgUnit(DQLSelectBuilder builder, String studentCategoryAlias)
    {
        builder.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "request");
        builder.joinPath(DQLJoinType.inner, "request." + EntrantRequest.L_ENTRANT, "entrant");
        builder.joinPath(DQLJoinType.inner, "ou." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "hs");
        builder.joinPath(DQLJoinType.inner, "hs." + EducationLevelsHighSchool.L_EDUCATION_LEVEL, "el");
        builder.where(eq(property("entrant." + Entrant.P_ARCHIVAL), value(Boolean.FALSE)));
        builder.where(eq(property("entrant." + Entrant.L_ENROLLMENT_CAMPAIGN), value(_model.getReport().getEnrollmentCampaign())));
        builder.where(betweenDays("request." + EntrantRequest.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));

        if (_model.isCompensationTypeActive())
            builder.where(eq(property("ct." + CompensationType.P_ID), value(_model.getCompensationType().getId())));
        if (_model.isStudentCategoryActive())
            builder.where(in(property(studentCategoryAlias), _model.getStudentCategoryList()));
        if (_model.isQualificationActive())
            builder.where(in(property("el." + EducationLevels.L_QUALIFICATION), _model.getQualificationList()));
        if (_model.isFormativeOrgUnitActive())
            builder.where(in(property("ou." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT), _model.getFormativeOrgUnitList()));
        if (_model.isTerritorialOrgUnitActive())
        {
            if (_model.getTerritorialOrgUnitList().isEmpty())
                builder.where(isNull(property("ou." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT)));
            else
                builder.where(in(property("ou." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT), _model.getTerritorialOrgUnitList()));
        }
        if (_model.isDevelopFormActive())
            builder.where(in(property("ou." + EducationOrgUnit.L_DEVELOP_FORM), _model.getDevelopFormList()));
        if (_model.isDevelopConditionActive())
            builder.where(in(property("ou." + EducationOrgUnit.L_DEVELOP_CONDITION), _model.getDevelopConditionList()));
        if (_model.isDevelopTechActive())
            builder.where(in(property("ou." + EducationOrgUnit.L_DEVELOP_TECH), _model.getDevelopTechList()));
        if (_model.isDevelopPeriodActive())
            builder.where(in(property("ou." + EducationOrgUnit.L_DEVELOP_PERIOD), _model.getDevelopPeriodList()));
    }
}
