/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uniec.base.bo.EcDistribution.logic.EcSimpleDSHandler;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

/**
 * @author Vasily Zhukov
 * @since 07.07.2011
 */
@Configuration
public class EcDistributionAddEdit extends BusinessComponentManager
{
    public static final String DIRECTION_DS = "directionDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(DIRECTION_DS, directionDS(), directionDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint directionDS()
    {
        return columnListExtPointBuilder(DIRECTION_DS)
                .addColumn(textColumn("code", EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().shortTitle()))
                .addColumn(textColumn("title", EnrollmentDirection.title()))
                .addColumn(textColumn("formativeOrgUnit", EnrollmentDirection.educationOrgUnit().formativeOrgUnit().title()))
                .addColumn(textColumn("territorialOrgUnit", EnrollmentDirection.educationOrgUnit().territorialOrgUnit().territorialTitle()))
                .addColumn(textColumn("developForm", EnrollmentDirection.educationOrgUnit().developForm().title()))
                .addColumn(textColumn("developCondition", EnrollmentDirection.educationOrgUnit().developCondition().title()))
                .addColumn(textColumn("developTech", EnrollmentDirection.educationOrgUnit().developTech().title()))
                .addColumn(textColumn("developPeriod", EnrollmentDirection.educationOrgUnit().developPeriod().title()))
                .addColumn(blockColumn("quota", "quotaBlockColumn"))
                .addColumn(blockColumn("taQuota", "taQuotaBlockColumn"))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> directionDSHandler()
    {
        return new EcSimpleDSHandler(getName());
    }
}
