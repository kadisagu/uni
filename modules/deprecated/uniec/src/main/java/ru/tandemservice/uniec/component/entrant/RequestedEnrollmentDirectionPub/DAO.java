/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.RequestedEnrollmentDirectionPub;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.EducationSubject;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

import java.util.*;

/**
 * @author agolubenko
 * @since 16.03.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setRequestedEnrollmentDirection(getNotNull(RequestedEnrollmentDirection.class, model.getRequestedEnrollmentDirection().getId()));
        List<RequestedProfileKnowledge> list = getList(RequestedProfileKnowledge.class, RequestedProfileKnowledge.L_REQUESTED_ENROLLMENT_DIRECTION, model.getRequestedEnrollmentDirection());
        List<String> titleList = new ArrayList<String>();
        for (RequestedProfileKnowledge item : list)
            titleList.add(item.getProfileKnowledge().getTitle());
        Collections.sort(titleList);
        model.setProfileKnowledgeLisTitle(StringUtils.join(titleList.iterator(), "\n"));
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        Entrant entrant = model.getEntrantRequest().getEntrant();
        EnrollmentDirection enrollmentDirection = model.getEnrollmentDirection();
        List<ProfileExaminationMark> marks = getMarks(entrant, UniecDAOFacade.getSettingsDAO().getProfileDisciplines(enrollmentDirection));

        DynamicListDataSource<ProfileExaminationMark> marksDataSource = model.getMarksDataSource();
        marksDataSource.setCountRow(Math.max(1, marks.size()));
        UniBaseUtils.createPage(marksDataSource, marks);
    }

    @Override
    public void preparePriorityProfileEduOu(Model model)
    {
        List<PriorityProfileEduOu> list = getList(PriorityProfileEduOu.class, PriorityProfileEduOu.requestedEnrollmentDirection(), model.getRequestedEnrollmentDirection(), PriorityProfileEduOu.P_PRIORITY);
        List<ProfileEducationOrgUnit> additionalList = getList(ProfileEducationOrgUnit.class, ProfileEducationOrgUnit.enrollmentDirection(), model.getRequestedEnrollmentDirection().getEnrollmentDirection(), ProfileEducationOrgUnit.P_ID);

        Set<ProfileEducationOrgUnit> used = new HashSet<ProfileEducationOrgUnit>();
        List<PriorityProfileEduOuDTO> dtoList = new ArrayList<PriorityProfileEduOuDTO>();
        long id = -1;
        int priority = 1;
        for (PriorityProfileEduOu item : list)
        {
            PriorityProfileEduOuDTO dto = new PriorityProfileEduOuDTO();
            dto.setProfileEducationOrgUnit(item.getProfileEducationOrgUnit());
            dto.setPriority(priority++);
            dto.setId(id--); // id тут совсем не важен
            dtoList.add(dto);
            used.add(item.getProfileEducationOrgUnit());
        }

        for (ProfileEducationOrgUnit item : additionalList)
        {
            if (used.add(item))
            {
                PriorityProfileEduOuDTO dto = new PriorityProfileEduOuDTO();
                dto.setProfileEducationOrgUnit(item);
                dto.setId(id--);
                dtoList.add(dto);
            }
        }

        model.getDataSource().setCountRow(dtoList.size());
        UniBaseUtils.createPage(model.getDataSource(), dtoList);
    }

    @SuppressWarnings("unchecked")
    private List<ProfileExaminationMark> getMarks(Entrant entrant, Collection<Discipline2RealizationWayRelation> profileDisciplines)
    {
        if (profileDisciplines.isEmpty())
        {
            return Collections.emptyList();
        }

        Criteria criteria = getSession().createCriteria(ProfileExaminationMark.class);
        criteria.createAlias(ProfileExaminationMark.L_DISCIPLINE, "discipline");
        criteria.createAlias("discipline." + Discipline2RealizationWayRelation.L_EDUCATION_SUBJECT, "subject");
        criteria.add(Restrictions.eq(ProfileExaminationMark.L_ENTRANT, entrant));
        criteria.add(Restrictions.in(ProfileExaminationMark.L_DISCIPLINE, profileDisciplines));
        criteria.addOrder(Order.asc("subject." + EducationSubject.P_TITLE));
        return criteria.list();
    }
}
