/* $Id$ */
package ru.tandemservice.uniec.ws.distribution;

import org.apache.commons.lang.mutable.MutableDouble;
import org.hibernate.Session;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.person.base.entity.NotUsedBenefit;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import org.tandemframework.shared.person.catalog.entity.Benefit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.gen.StudentCategoryGen;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.base.bo.EcDistribution.EcDistributionManager;
import ru.tandemservice.uniec.base.bo.EcDistribution.logic.IEcDistributionDao;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.*;
import ru.tandemservice.uniec.base.entity.ecg.*;
import ru.tandemservice.uniec.base.entity.ecg.gen.EcgDistributionGen;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.catalog.codes.TargetAdmissionKindCodes;
import ru.tandemservice.uniec.entity.catalog.gen.EntrantStateGen;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrderType;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.entity.settings.EnrollmentCompetitionKind;
import ru.tandemservice.uniec.entity.settings.NotUsedTargetAdmissionKind;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.ExamSetUtil;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Vasily Zhukov
 * @since 24.07.2011
 */
public class EnrollmentDistributionServiceDao extends CommonDAO implements IEnrollmentDistributionServiceDao
{
    public static final String RECOMMENDED = "1";
    public static final String RETIRED = "2";
    public static final String ENROLLED = "3";

    @Override
    public EnrollmentDistributionEnvironmentNode getEnrollmentDistributionEnvironmentNode(Set<IEcgDistribution> distributionSet)
    {
        if (distributionSet == null || distributionSet.isEmpty())
            throw new RuntimeException("DistributionSet is empty.");

        Session session = getSession();

        Set<Long> usedEnrollmentCampaignIds = new HashSet<>();
        for (IEcgDistribution distribution : distributionSet)
        {
            session.refresh(distribution);
            if (distribution instanceof EcgDistributionDetail)
                session.refresh(distribution.getDistribution());
            usedEnrollmentCampaignIds.add(distribution.getConfig().getEcgItem().getEnrollmentCampaign().getId());
        }

        if (usedEnrollmentCampaignIds.size() > 1)
            throw new RuntimeException("DistributionSet contains two or more enrollmentCampaigns.");

        EnrollmentCampaign enrollmentCampaign = distributionSet.iterator().next().getConfig().getEcgItem().getEnrollmentCampaign();

        EnrollmentDistributionEnvironmentNode env = new EnrollmentDistributionEnvironmentNode();

        env.enrollmentCampaignTitle = enrollmentCampaign.getTitle();

        env.educationYearTitle = enrollmentCampaign.getEducationYear().getTitle();

        env.currentDate = new Date();

        // получаем все не используемые виды целевого приема
        Set<Long> notUsedTargetAdmissionKindIds = new HashSet<>(new DQLSelectBuilder().fromEntity(NotUsedTargetAdmissionKind.class, "e")
        .column(property(NotUsedTargetAdmissionKind.targetAdmissionKind().id().fromAlias("e")))
        .where(eq(property(NotUsedTargetAdmissionKind.enrollmentCampaign().fromAlias("e")), value(enrollmentCampaign)))
        .createStatement(session).<Long>list());

        // сохраняем все виды целевого приема
        for (TargetAdmissionKind row : getList(TargetAdmissionKind.class, TargetAdmissionKind.P_CODE))
            env.targetAdmissionKind.row.add(new EnrollmentDistributionEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow(row.getShortTitle(), row.getTitle(), row.getCode(), row.getParent() == null ? null : row.getParent().getCode(), row.getPriority(), !notUsedTargetAdmissionKindIds.contains(row.getId())));

        // получаем все не используемые виды конкурса
        Set<Long> notUsedCompetitionKind = new HashSet<>(new DQLSelectBuilder().fromEntity(EnrollmentCompetitionKind.class, "e")
        .column(property(EnrollmentCompetitionKind.competitionKind().id().fromAlias("e")))
        .where(eq(property(EnrollmentCompetitionKind.enrollmentCampaign().fromAlias("e")), value(enrollmentCampaign)))
        .where(eq(property(EnrollmentCompetitionKind.used().fromAlias("e")), value(false)))
        .createStatement(session).<Long>list());

        // сохраняем все виды конкурса
        for (CompetitionKind row : getList(CompetitionKind.class, CompetitionKind.P_CODE))
            env.competitionKind.row.add(new EnrollmentDistributionEnvironmentNode.CompetitionKindNode.CompetitionKindRow(row.getShortTitle(), row.getTitle(), row.getCode(), !notUsedCompetitionKind.contains(row.getId())));

        // получаем все не используемые льготы
        Set<Long> notUsedBenefitIds = new HashSet<>(new DQLSelectBuilder().fromEntity(NotUsedBenefit.class, "e")
        .column(property(NotUsedBenefit.benefit().id().fromAlias("e")))
        .where(eq(property(NotUsedBenefit.personRoleName().fromAlias("e")), value(Entrant.class.getSimpleName())))
        .createStatement(session).<Long>list());

        // сохраняем все льготы
        for (Benefit row : getList(Benefit.class, Benefit.P_CODE))
            env.benefit.row.add(new EnrollmentDistributionEnvironmentNode.BenefitNode.BenefitRow(row.getShortTitle(), row.getTitle(), row.getCode(), !notUsedBenefitIds.contains(row.getId())));

        // получаем все не используемые типы приказов
        Set<Long> notUsedOrderTypeIds = new HashSet<>(new DQLSelectBuilder().fromEntity(EnrollmentOrderType.class, "e")
        .column(property(EnrollmentOrderType.entrantEnrollmentOrderType().id().fromAlias("e")))
        .where(eq(property(EnrollmentOrderType.enrollmentCampaign().fromAlias("e")), value(enrollmentCampaign)))
        .where(eq(property(EnrollmentOrderType.used().fromAlias("e")), value(Boolean.FALSE)))
        .createStatement(session).<Long>list());

        // сохраняем типы приказов о зачислении
        for (EntrantEnrollmentOrderType row : getList(EntrantEnrollmentOrderType.class, EntrantEnrollmentOrderType.P_CODE))
            env.orderType.row.add(new EnrollmentDistributionEnvironmentNode.EnrollmentOrderTypeNode.EnrollmentOrderTypeRow(row.getShortTitle(), row.getTitle(), row.getCode(), !notUsedOrderTypeIds.contains(row.getId())));

        // сохраняем все состояния распределений
        for (EcgDistributionState row : getList(EcgDistributionState.class, EcgDistributionState.P_CODE))
            env.distributionState.row.add(new EnrollmentDistributionEnvironmentNode.Row(row.getTitle(), row.getCode()));

        // сохраняем все состояния документов
        env.documentState.row.add(new EnrollmentDistributionEnvironmentNode.Row("Оригиналы", Integer.toString(IEcgRecommendedDTO.DocumentStatus.BRING_ORIGINAL.ordinal() + 1)));
        env.documentState.row.add(new EnrollmentDistributionEnvironmentNode.Row("Копии", Integer.toString(IEcgRecommendedDTO.DocumentStatus.BRING_COPY.ordinal() + 1)));
        env.documentState.row.add(new EnrollmentDistributionEnvironmentNode.Row("Забрал документы", Integer.toString(IEcgRecommendedDTO.DocumentStatus.TOOK_AWAY.ordinal() + 1)));

        // сохраняем все состояния зачисления
        env.enrollmentState.row.add(new EnrollmentDistributionEnvironmentNode.Row("Рекомендован", RECOMMENDED));
        env.enrollmentState.row.add(new EnrollmentDistributionEnvironmentNode.Row("Выбыл", RETIRED));
        env.enrollmentState.row.add(new EnrollmentDistributionEnvironmentNode.Row("Зачислен", ENROLLED));

        for (IEcgDistribution distribution : distributionSet)
        {
            EnrollmentDistributionEnvironmentNode.DistributionNode.DistributionRow row = getDistributionRow(distribution);
            sortEntrantNodeIfNeed(row.entrant.row, distribution.getConfig(), env);
            env.distribution.row.add(row);
        }

        return env;
    }

    private EnrollmentDistributionEnvironmentNode.DistributionNode.DistributionRow getDistributionRow(IEcgDistribution distribution)
    {
        if (distribution instanceof EcgDistributionDetail && !distribution.getState().isApproved())
            throw new RuntimeException("DistributionServiceDao does not support not approved distributionDetail.");

        // LOAD DATA

        Session session = getSession();
        IEcDistributionDao dao = EcDistributionManager.instance().dao();

        // загружаем общую информацию о распределении
        EcgDistributionConfig config = distribution.getConfig();
        EnrollmentCampaign enrollmentCampaign = config.getEcgItem().getEnrollmentCampaign();
        List<StudentCategory> studentCategoryList = getStudentCategoryList(config);
        List<EnrollmentDirection> distributionDirectionList = dao.getDistributionDirectionList(distribution.getDistribution());
        IEcgQuotaDTO currentQuota = dao.getCurrentQuotaDTO(distribution.getDistribution());
        IEcgQuotaDTO beginQuota = distribution.getDistribution().getWave() == IEcDistributionDao.DISTRIBUTION_FIRST_WAVE ? currentQuota : dao.getCurrentQuotaDTO(this.<EcgDistribution>getByNaturalId(new EcgDistributionGen.NaturalId(config, IEcDistributionDao.DISTRIBUTION_FIRST_WAVE)));
        List<TargetAdmissionKind> taKindList = currentQuota.getTaKindList();
        boolean defaultTaKind = taKindList.size() == 1 && taKindList.get(0).getCode().equals(TargetAdmissionKindCodes.TA_DEFAULT);
        // загружаем все возможные в этом распределении дисциплины приемной кампании
        Collection<Discipline2RealizationWayRelation> disciplineList = getDiscipline2RealizationWayRelations(session, config, enrollmentCampaign, studentCategoryList, distributionDirectionList);

        // загружаем абитуриентов
        List<IEcgCortegeDTO> cortegeDTOList = new ArrayList<>(); // список рекомендованных (из основного или уточняющего) и резерва (или предполагаемого резерва)
        List<Entrant> nonActualEntrantList = new ArrayList<>();         // список неактуальных абитуриентов
        Comparator<IEcgCortegeDTO> comparator = new EcgContegeComparator(UniecDAOFacade.getSettingsDAO().getCompetitionKindPriorities(distribution.getConfig().getEcgItem().getEnrollmentCampaign()));
        MQBuilder builder = getDistributionBuilder(distribution);
        EntrantDataUtil dataUtil = new EntrantDataUtil(session, distribution.getConfig().getEcgItem().getEnrollmentCampaign(), builder);
        Map<Person, Set<Benefit>> benefitMap = EntrantDataUtil.getBenefitMap(session, builder); //льготы
        Map<Entrant, RequestedEnrollmentDirection> entrantDirectionMap = getEntrantDirectionMap(distribution, comparator);
        Map<EcgEntrantRecommended, IEcgHasBringOriginal> recommendedStateMap = dao.getEntrantRecommendedWithPreStudentList(distribution.getDistribution());
        Map<Long, PreliminaryEnrollmentStudent> entrantPreStudentMap = dao.getEntrantPreStudentMap(distribution);
        Map<Long, EcgPriorityInfo> entrantPriorityInfoMap = fillCortegeDTOList(distribution, dataUtil, cortegeDTOList, nonActualEntrantList, dao, entrantDirectionMap, comparator);

        // загружаем соответствия НПП -> НП из распределения
        Map<Long, EnrollmentDirection> ouId2directionMap = new HashMap<>();
        for (EnrollmentDirection direction : EcDistributionManager.instance().dao().getDistributionDirectionList(distribution.getDistribution()))
            if (null != ouId2directionMap.put(direction.getEducationOrgUnit().getId(), direction))
                throw new RuntimeException("Dublicate educationOrgUnit '" + direction.getEducationOrgUnit().getId() + "' in distribution '" + distribution.getDistribution().getId() + "'");

        // FILL DATA

        EnrollmentDistributionEnvironmentNode.DistributionNode.DistributionRow row = new EnrollmentDistributionEnvironmentNode.DistributionNode.DistributionRow();

        // сохраняем общую информацию о распределении
        row.ecgItemId = config.getEcgItem().getId().toString();
        row.ecgItemTitle = config.getEcgItem().getTitle();
        row.compensationTypeId = config.getCompensationType().getCode();
        row.secondHighAdmission = config.isSecondHighAdmission();
        row.wave = distribution.getDistribution().getWave();
        row.distributionDetail = distribution instanceof EcgDistributionDetail;
        row.state = distribution.getState().getCode();

        // сохраняем все возможные в этом распределении дисциплины приемной кампании
        for (Discipline2RealizationWayRelation discipline : disciplineList)
            row.discipline.row.add(new EnrollmentDistributionEnvironmentNode.DisciplineNode.DisciplineRow(discipline.getTitle(), discipline.getShortTitle(), discipline.getId().toString()));

        // сохраняем информацию о планах приема
        for (EnrollmentDirection enrollmentDirection : distributionDirectionList)
        {
            Long enrollmentDirectionId = enrollmentDirection.getId();
            int current = currentQuota.getQuotaMap().get(enrollmentDirectionId);
            int begin = beginQuota.getQuotaMap().get(enrollmentDirectionId);
            Map<Long, Integer> currentTaMap = currentQuota.getTaQuotaMap().get(enrollmentDirectionId);
            Map<Long, Integer> beginTaMap = beginQuota.getTaQuotaMap().get(enrollmentDirectionId);

            EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow.TaQuotaNode taQuotaNode = new EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow.TaQuotaNode();
            for (TargetAdmissionKind taKind : taKindList)
                taQuotaNode.row.add(new EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow.TaQuotaNode.TaQuotaRow(taKind.getCode(), currentTaMap.get(taKind.getId()), beginTaMap.get(taKind.getId())));
            row.quota.row.add(new EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow(enrollmentDirectionId.toString(), current, begin, defaultTaKind, taQuotaNode));
        }

        // внп, для которых требуется установить существование приказа о зачислении
        Map<Entrant, EnrollmentDistributionEnvironmentNode.EntrantNode.EntrantRow> entrantRowMap = new HashMap<>();

        // добавляем информацию об абитуриентах
        for (IEcgCortegeDTO cortegeDTO : cortegeDTOList)
        {
            RequestedEnrollmentDirection direction = getDirectionFromCortegeDTO(cortegeDTO, entrantDirectionMap);
            EntrantRequest entrantRequest = direction.getEntrantRequest();
            Entrant entrant = entrantRequest.getEntrant();
            EcgPriorityInfo priorityInfo = entrantPriorityInfoMap.get(entrant.getId());
            PreliminaryEnrollmentStudent preStudent = entrantPreStudentMap.get(entrant.getId());
            EnrollmentDirection preEnrollDirection = preStudent == null ? null : ouId2directionMap.get(preStudent.getEducationOrgUnit().getId());

            EnrollmentDistributionEnvironmentNode.EntrantNode.EntrantRow entrantRow = new EnrollmentDistributionEnvironmentNode.EntrantNode.EntrantRow();

            entrantRow.preEnrollId = preEnrollDirection == null ? null : preEnrollDirection.getId().toString();
            entrantRow.priorityIds = priorityInfo == null ? null : priorityInfo.getPriorityIdList();
            entrantRow.fio = cortegeDTO.getFio();
            entrantRow.competitionKind = cortegeDTO.getCompetitionKind().getCode();

            Set<Benefit> benefitSet = benefitMap.get(entrant.getPerson());
            if (benefitSet != null)
            {
                entrantRow.benefitList = new ArrayList<>();
                for (Benefit benefit : benefitSet)
                    entrantRow.benefitList.add(benefit.getCode());
                Collections.sort(entrantRow.benefitList);
            }

            entrantRow.averageEduInstitutionMark = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(cortegeDTO.getCertificateAverageMark());
            entrantRow.profileMark = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(cortegeDTO.getProfileMark());
            entrantRow.finalMark = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(cortegeDTO.getFinalMark());
            entrantRow.marks = new ArrayList<>();

            Map<Discipline2RealizationWayRelation, Double> markMap = dataUtil.getMarkMap(direction);
            for (Discipline2RealizationWayRelation discipline : disciplineList)
            {
                Double mark = markMap.get(discipline);
                if (mark == null)
                    entrantRow.marks.add("x");
                else if (mark == -1.0)
                    entrantRow.marks.add("-");
                else
                    entrantRow.marks.add(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(mark));
            }

            entrantRow.targetAdmissionKind = cortegeDTO.getTargetAdmissionKind() == null ? null : cortegeDTO.getTargetAdmissionKind().getCode();
            entrantRow.documentState = Integer.toString(getDocumentStatus(distribution, cortegeDTO, recommendedStateMap, entrantDirectionMap).ordinal() + 1);
            entrantRow.enrollmentState = getEnrollmentStatus(distribution, cortegeDTO, recommendedStateMap, entrantPreStudentMap.keySet());

            if (ENROLLED.equals(entrantRow.enrollmentState))
                entrantRowMap.put(entrant, entrantRow);

            entrantRow.enrollmentDirection = direction.getEnrollmentDirection().getId().toString();
            entrantRow.requestNumber = entrantRequest.getStringNumber();
            entrantRow.regNumber = direction.getStringNumber();
            entrantRow.entrantId = entrant.getId().toString();

            row.entrant.row.add(entrantRow);
        }

        // для тех кто зачислен попытаемся определить приказ
        for (EnrollmentExtract extract : new DQLSelectBuilder().fromEntity(EnrollmentExtract.class, "e").column("e")
        .where(in(property(EnrollmentExtract.entity().requestedEnrollmentDirection().entrantRequest().entrant().fromAlias("e")), entrantRowMap.keySet()))
        .where(eq(property(EnrollmentExtract.entity().compensationType().fromAlias("e")), value(config.getCompensationType())))
        .where(in(property(EnrollmentExtract.entity().studentCategory().fromAlias("e")), getStudentCategoryList(config)))
        .where(in(property(EnrollmentExtract.entity().educationOrgUnit().id().fromAlias("e")), new DQLSelectBuilder().fromEntity(EcgDistributionQuota.class, "q")
            .column(property(EcgDistributionQuota.direction().educationOrgUnit().id().fromAlias("q")))
            .where(eq(property(EcgDistributionQuota.distribution().fromAlias("q")), value(distribution.getDistribution())))
            .buildQuery()
        )).createStatement(session).<EnrollmentExtract>list())
        {
            EnrollmentDistributionEnvironmentNode.EntrantNode.EntrantRow entrantRow = entrantRowMap.get(extract.getEntity().getRequestedEnrollmentDirection().getEntrantRequest().getEntrant());

            entrantRow.orderId = extract.getOrder().getId().toString();
            entrantRow.orderNumber = extract.getOrder().getNumber();
            entrantRow.orderType = extract.getOrder().getType().getCode();
        }

        // добавляем информацию о неактуальных абитуриентах
        for (Entrant entrant : nonActualEntrantList)
        {
            EnrollmentDistributionEnvironmentNode.EntrantNode.EntrantRow entrantRow = new EnrollmentDistributionEnvironmentNode.EntrantNode.EntrantRow();

            entrantRow.fio = entrant.getPerson().getIdentityCard().getFullFio();
            entrantRow.entrantId = entrant.getId().toString();

            row.entrant.row.add(entrantRow);
        }

        sortIfNeed(row.entrant.row, config, studentCategoryList, distributionDirectionList);

        return row;
    }


    protected Collection<Discipline2RealizationWayRelation> getDiscipline2RealizationWayRelations(EcgDistributionConfig config, EnrollmentCampaign enrollmentCampaign, List<StudentCategory> studentCategoryList, List<EnrollmentDirection> distributionDirectionList)
    {
        // загружаем все возможные в этом распределении дисциплины приемной кампании
        Map<EnrollmentDirection, List<Discipline2RealizationWayRelation>> disciplineMap = ExamSetUtil.getDirection2DisciplineMap(getSession(), enrollmentCampaign, studentCategoryList, config.getCompensationType());
        Set<Discipline2RealizationWayRelation> disciplineSet = new TreeSet<>(ITitled.TITLED_COMPARATOR);
        for (EnrollmentDirection enrollmentDirection : distributionDirectionList)
        {
            disciplineSet.addAll(disciplineMap.get(enrollmentDirection));
        }
        return disciplineSet;
    }

    protected void sortEntrantNodeIfNeed(List<EnrollmentDistributionEnvironmentNode.EntrantNode.EntrantRow> list, EcgDistributionConfig config, EnrollmentDistributionEnvironmentNode env)
    {
        // not need
    }


    protected Collection<Discipline2RealizationWayRelation> getDiscipline2RealizationWayRelations(Session session, EcgDistributionConfig config, EnrollmentCampaign enrollmentCampaign, List<StudentCategory> studentCategoryList, List<EnrollmentDirection> distributionDirectionList)
    {
        // загружаем все возможные в этом распределении дисциплины приемной кампании
        return getDiscipline2RealizationWayRelations(config, enrollmentCampaign, studentCategoryList, distributionDirectionList);
    }

    protected void sortIfNeed(List<EnrollmentDistributionEnvironmentNode.EntrantNode.EntrantRow> list, EcgDistributionConfig config, List<StudentCategory> studentCategoryList, List<EnrollmentDirection> distributionDirectionList){};

    /**
     * Создает источник данных для выбранных направлений приема
     *
     * @param distribution распределение
     * @return билдер
     */
    private MQBuilder getDistributionBuilder(IEcgDistribution distribution)
    {
        // создаем источник данных для выбранных направлений
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "e");

        // добавляем все выбранные направления приема, которые удовлетворяют конфигурации распределения
        builder.add(MQExpression.eq("e", RequestedEnrollmentDirection.compensationType(), distribution.getConfig().getCompensationType()));
        builder.add(MQExpression.in("e", RequestedEnrollmentDirection.enrollmentDirection().id(), new MQBuilder(EcgDistributionQuota.ENTITY_CLASS, "q", new String[]{EcgDistributionQuota.direction().id().s()})
        .add(MQExpression.eq("q", EcgDistributionQuota.distribution().s(), distribution.getDistribution()))
        ));
        builder.add(MQExpression.in("e", RequestedEnrollmentDirection.studentCategory().id(), distribution.getConfig().isSecondHighAdmission() ?
            Collections.singletonList(this.<StudentCategory>getByNaturalId(new StudentCategoryGen.NaturalId(UniDefines.STUDENT_CATEGORY_SECOND_HIGH)).getId()) :
                Arrays.asList(this.<StudentCategory>getByNaturalId(new StudentCategoryGen.NaturalId(UniDefines.STUDENT_CATEGORY_STUDENT)).getId(), this.<StudentCategory>getByNaturalId(new StudentCategoryGen.NaturalId(UniDefines.STUDENT_CATEGORY_LISTENER)).getId())
        ));

        return builder;
    }

    private List<StudentCategory> getStudentCategoryList(EcgDistributionConfig config)
    {
        return config.isSecondHighAdmission() ?
            Collections.singletonList(this.<StudentCategory>getByNaturalId(new StudentCategoryGen.NaturalId(UniDefines.STUDENT_CATEGORY_SECOND_HIGH))) :
                Arrays.asList(this.<StudentCategory>getByNaturalId(new StudentCategoryGen.NaturalId(UniDefines.STUDENT_CATEGORY_STUDENT)), this.<StudentCategory>getByNaturalId(new StudentCategoryGen.NaturalId(UniDefines.STUDENT_CATEGORY_LISTENER)));
    }

    /**
     * для резерва или предполагаемого резерва загружаем соответствие: абитуриент -> сопоставленное внп
     * для сохраненного резерва: множество внп - это внп абитуриентов из сохр. резерва удовл. конф. распр.
     * для предполагаемого резерва: множество внп это те внп удовл. условиям:
     * 1. не архивные
     * 2. к зачислению
     * 3. абитуриент у внп не рекомендован в это распределение
     * 4. для бюджета не первой волны внп у абитуриентов из резерва предыдущей волны
     * 5. абитуриент у внп не предзачислен ни по одному нпп из распределения
     *
     * @param distribution распределение
     * @param comparator   кортежный компаратор
     * @return абитуриент -> сопоставленное внп
     */
    @SuppressWarnings("deprecation")
    private Map<Entrant, RequestedEnrollmentDirection> getEntrantDirectionMap(IEcgDistribution distribution, Comparator<IEcgCortegeDTO> comparator)
    {
        Session session = getSession();

        // берем внп
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "e");

        // совпадает вид возмещения затрат
        builder.where(eq(property(RequestedEnrollmentDirection.compensationType().fromAlias("e")), value(distribution.getConfig().getCompensationType())));

        // совпадает категория
        builder.where(in(property(RequestedEnrollmentDirection.studentCategory().fromAlias("e")), getStudentCategoryList(distribution.getConfig())));

        // среди направлений из распределения
        builder.where(in(property(RequestedEnrollmentDirection.enrollmentDirection().id().fromAlias("e")), new DQLSelectBuilder().fromEntity(EcgDistributionQuota.class, "q")
            .column(property(EcgDistributionQuota.direction().id().fromAlias("q")))
            .where(eq(property(EcgDistributionQuota.distribution().fromAlias("q")), value(distribution.getDistribution()))).buildQuery()));

        if (distribution.getDistribution().getState().isLockedOrApproved())
        {
            // для зафиксированного распределения нужно сразу ограничить внп теми, что у абитуриентов из резерва
            builder.where(in(property(RequestedEnrollmentDirection.entrantRequest().entrant().id().fromAlias("e")), new DQLSelectBuilder().fromEntity(EcgEntrantReserved.class, "r")
                .column(property(EcgEntrantReserved.entrant().id().fromAlias("r")))
                .where(eq(property(EcgEntrantReserved.distribution().fromAlias("r")), value(distribution.getDistribution())))
                .buildQuery()
            ));
        } else
        {
            // для не зафиксированного (и не утвержденного тоже, очевидно) нужно наложить еще ограничений,
            // так как мы, по сути, строим предполагаемый резерв

            // исключаем тех абитуриентов, кто уже рекомендован в это распределение
            builder.where(notIn(property(RequestedEnrollmentDirection.entrantRequest().entrant().id().fromAlias("e")), new DQLSelectBuilder().fromEntity(EcgEntrantRecommended.class, "r2")
                .column(property(EcgEntrantRecommended.direction().entrantRequest().entrant().id().fromAlias("r2")))
                .where(eq(property(EcgEntrantRecommended.distribution().fromAlias("r2")), value(distribution.getDistribution())))
                .buildQuery()
            ));

            // для первой волны берем только к зачислению и не в архиве
            if (distribution.getDistribution().getWave() == IEcDistributionDao.DISTRIBUTION_FIRST_WAVE)
            {
                builder.where(eq(property(RequestedEnrollmentDirection.entrantRequest().entrant().archival().fromAlias("e")), value(Boolean.FALSE)));
                builder.where(eq(property(RequestedEnrollmentDirection.state().fromAlias("e")), commonValue(getByNaturalId(new EntrantStateGen.NaturalId(UniecDefines.ENTRANT_STATE_TOBE_ENROLED_CODE)))));

                // исключаем тех абитуриентов, кто предзачислен хотя бы одному нпп из распределения с учетом конфигурации
                builder.where(notIn(property(RequestedEnrollmentDirection.entrantRequest().entrant().id().fromAlias("e")), new DQLSelectBuilder().fromEntity(PreliminaryEnrollmentStudent.class, "p")
                    .column(property(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().entrantRequest().entrant().id().fromAlias("p")))
                    .where(eq(property(PreliminaryEnrollmentStudent.compensationType().fromAlias("p")), value(distribution.getConfig().getCompensationType())))
                    .where(in(property(PreliminaryEnrollmentStudent.studentCategory().fromAlias("p")), getStudentCategoryList(distribution.getConfig())))
                    .where(in(property(PreliminaryEnrollmentStudent.educationOrgUnit().id().fromAlias("p")), new DQLSelectBuilder().fromEntity(EcgDistributionQuota.class, "q1")
                        .column(property(EcgDistributionQuota.direction().educationOrgUnit().id().fromAlias("q1")))
                        .where(eq(property(EcgDistributionQuota.distribution().fromAlias("q1")), value(distribution.getDistribution()))).buildQuery()))
                        .buildQuery()
                ));
            } else
            {
                // если распределение на бюджет и не первой волны, то нужно брать только абитуриентов из списка резерва предыдущей волны на эту же конфигурацию
                if (distribution.getConfig().getCompensationType().isBudget())
                {
                    builder.where(in(property(RequestedEnrollmentDirection.entrantRequest().entrant().id().fromAlias("e")), new DQLSelectBuilder().fromEntity(EcgEntrantReserved.class, "r")
                        .column(property(EcgEntrantReserved.entrant().id().fromAlias("r")))
                        .where(eq(property(EcgEntrantReserved.distribution().wave().fromAlias("r")), value(distribution.getDistribution().getWave() - 1)))
                        .where(eq(property(EcgEntrantReserved.distribution().config().fromAlias("r")), value(distribution.getConfig())))
                        .buildQuery()
                    ));
                }
            }
        }

        List<RequestedEnrollmentDirection> directionList = builder.createStatement(session).list();

        final int REQUESTED_ENROLLMENT_DIRECTION_ID_IX = 0;
        final int FINAL_MARK_IX = 1;

        builder.joinEntity("e", DQLJoinType.inner, ChosenEntranceDiscipline.class, "c", eq(property(RequestedEnrollmentDirection.id().fromAlias("e")), property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("c"))));
        builder.where(isNotNull(ChosenEntranceDiscipline.finalMark().fromAlias("c")));
        builder.group(property(RequestedEnrollmentDirection.id().fromAlias("e")));
        builder.column(property(RequestedEnrollmentDirection.id().fromAlias("e")));
        builder.column(DQLFunctions.sum(property(ChosenEntranceDiscipline.finalMark().fromAlias("c"))));

        Map<Long, MutableDouble> finalMarkMap = new HashMap<>();
        for (Object[] dataRow : builder.createStatement(session).<Object[]>list())
        {
            Long requestedEnrollmentDirectionId = (Long) dataRow[REQUESTED_ENROLLMENT_DIRECTION_ID_IX];
            Double finalMark = (Double) dataRow[FINAL_MARK_IX];
            if (null == finalMark) { finalMark = 0.0d; /* либо записей нет, либо в базе null */ }

            MutableDouble mutable = finalMarkMap.get(requestedEnrollmentDirectionId);
            if (mutable == null)
                finalMarkMap.put(requestedEnrollmentDirectionId, mutable = new MutableDouble());
            mutable.add(finalMark);
        }

        Map<Entrant, List<IEcgCortegeDTO>> directionMap = new HashMap<>();
        for (RequestedEnrollmentDirection direction : directionList)
        {
            List<IEcgCortegeDTO> list = directionMap.get(direction.getEntrantRequest().getEntrant());
            if (list == null)
                directionMap.put(direction.getEntrantRequest().getEntrant(), list = new ArrayList<>());

            Person person = direction.getEntrantRequest().getEntrant().getPerson();
            PersonEduInstitution eduInstitution = person.getPersonEduInstitution();

            MutableDouble finalMark = finalMarkMap.get(direction.getId());

            list.add(new EcgCortegeDTO(
                direction,
                false,
                null,
                direction.getCompetitionKind(),
                finalMark == null ? 0 : finalMark.doubleValue(),
                    direction.getProfileChosenEntranceDiscipline() == null ? null : direction.getProfileChosenEntranceDiscipline().getFinalMark(),
                        direction.isGraduatedProfileEduInstitution(),
                        eduInstitution == null ? null : eduInstitution.getAverageMark(),
                            person.getIdentityCard().getFullFio()
            ));
        }

        Map<Entrant, RequestedEnrollmentDirection> resultMap = new HashMap<>();

        for (Map.Entry<Entrant, List<IEcgCortegeDTO>> entry : directionMap.entrySet())
        {
            Collections.sort(entry.getValue(), comparator);

            // берем первое выбранное направление приема после кортежной сортировки
            resultMap.put(entry.getKey(), (RequestedEnrollmentDirection) entry.getValue().get(0).getEntity());
        }

        return resultMap;
    }

    private Map<Long, EcgPriorityInfo> fillCortegeDTOList(IEcgDistribution distribution, EntrantDataUtil dataUtil, List<IEcgCortegeDTO> cortegeDTOList, List<Entrant> nonActualEntrantList, IEcDistributionDao dao, Map<Entrant, RequestedEnrollmentDirection> entrantDirectionMap, Comparator<IEcgCortegeDTO> comparator)
    {
        Session session = getSession();

        EcgDistributionConfig config = distribution.getConfig();

        Set<Long> usedEntrantIds = new HashSet<>();

        // добавляем всех рекомендованных из уточняющего распределения (если печатается уточняющее распределение)
        if (distribution instanceof EcgDistributionDetail)
        {
            for (EcgEntrantRecommendedDetail recommendedDetail : new DQLSelectBuilder().fromEntity(EcgEntrantRecommendedDetail.class, "e").column("e")
            .where(eq(property(EcgEntrantRecommendedDetail.distribution().fromAlias("e")), value(distribution)))
            .createStatement(session).<EcgEntrantRecommendedDetail>list())
            {
                if (usedEntrantIds.add(recommendedDetail.getDirection().getEntrantRequest().getEntrant().getId()))
                {
                    if (dao.isConfigAllowDirection(config, recommendedDetail.getDirection()))
                    {
                        // это направление приема должно быть в статистике оценок
                        if (!dataUtil.getDirectionSet().contains(recommendedDetail.getDirection()))
                            throw new RuntimeException("Invalid EntrantDataUtil, no direction '" + recommendedDetail.getDirection().getId() + "' for distribution '" + distribution.getId() + "'");

                        cortegeDTOList.add(new EcgCortegeDTO(
                            recommendedDetail,
                            recommendedDetail.getTargetAdmissionKind() != null,
                            recommendedDetail.getTargetAdmissionKind(),
                            recommendedDetail.getDirection().getCompetitionKind(),
                            dataUtil.getFinalMark(recommendedDetail.getDirection()),
                            recommendedDetail.getDirection().getProfileChosenEntranceDiscipline() == null ? null : recommendedDetail.getDirection().getProfileChosenEntranceDiscipline().getFinalMark(),
                                recommendedDetail.getDirection().isGraduatedProfileEduInstitution(),
                                recommendedDetail.getDirection().getEntrantRequest().getEntrant().getPerson().getPersonEduInstitution() == null ? null : recommendedDetail.getDirection().getEntrantRequest().getEntrant().getPerson().getPersonEduInstitution().getAverageMark(),
                                    recommendedDetail.getDirection().getEntrantRequest().getEntrant().getPerson().getIdentityCard().getFullFio()
                        ));
                    } else
                    {
                        nonActualEntrantList.add(recommendedDetail.getDirection().getEntrantRequest().getEntrant());
                    }
                }
            }
        }

        // добавляем всех рекомендованных из основных распределений текущей и предыдущих волн, добавление начинаем с большей волны
        for (EcgEntrantRecommended recommended : new DQLSelectBuilder().fromEntity(EcgEntrantRecommended.class, "e").column("e")
        .where(eq(property(EcgEntrantRecommended.distribution().config().fromAlias("e")), value(config)))
        .where(le(property(EcgEntrantRecommended.distribution().wave().fromAlias("e")), value(distribution.getDistribution().getWave())))
        .order(property(EcgEntrantRecommended.distribution().wave().fromAlias("e")), OrderDirection.desc)
        .createStatement(session).<EcgEntrantRecommended>list())
        {
            if (usedEntrantIds.add(recommended.getDirection().getEntrantRequest().getEntrant().getId()))
            {
                if (dao.isConfigAllowDirection(config, recommended.getDirection()))
                {
                    // это направление приема должно быть в статистике оценок
                    if (!dataUtil.getDirectionSet().contains(recommended.getDirection()))
                        throw new RuntimeException("Invalid EntrantDataUtil, no direction '" + recommended.getDirection().getId() + "' for distribution '" + distribution.getId() + "'");

                    cortegeDTOList.add(new EcgCortegeDTO(
                        recommended,
                        recommended.getTargetAdmissionKind() != null,
                        recommended.getTargetAdmissionKind(),
                        recommended.getDirection().getCompetitionKind(),
                        dataUtil.getFinalMark(recommended.getDirection()),
                        recommended.getDirection().getProfileChosenEntranceDiscipline() == null ? null : recommended.getDirection().getProfileChosenEntranceDiscipline().getFinalMark(),
                            recommended.getDirection().isGraduatedProfileEduInstitution(),
                            recommended.getDirection().getEntrantRequest().getEntrant().getPerson().getPersonEduInstitution() == null ? null : recommended.getDirection().getEntrantRequest().getEntrant().getPerson().getPersonEduInstitution().getAverageMark(),
                                recommended.getDirection().getEntrantRequest().getEntrant().getPerson().getIdentityCard().getFullFio()
                    ));
                } else
                {
                    nonActualEntrantList.add(recommended.getDirection().getEntrantRequest().getEntrant());
                }
            }
        }
        // добавляем резерв
        if (distribution.getDistribution().getState().isLockedOrApproved())
        {
            // нужно брать существующий резерв
            for (EcgEntrantReserved reserved : new DQLSelectBuilder().fromEntity(EcgEntrantReserved.class, "e").column("e")
            .where(eq(property(EcgEntrantReserved.distribution().fromAlias("e")), value(distribution.getDistribution())))
            .createStatement(session).<EcgEntrantReserved>list())
            {
                RequestedEnrollmentDirection direction = entrantDirectionMap.get(reserved.getEntrant());

                if (usedEntrantIds.add(reserved.getEntrant().getId()))
                {
                    if (direction != null && dao.isConfigAllowDirection(config, direction))
                    {
                        // это направление приема должно быть в статистике оценок
                        if (!dataUtil.getDirectionSet().contains(direction))
                            throw new RuntimeException("Invalid EntrantDataUtil, no direction '" + direction.getId() + "' for distribution '" + distribution.getId() + "'");

                        cortegeDTOList.add(new EcgCortegeDTO(
                            reserved,
                            false,
                            null,
                            direction.getCompetitionKind(),
                            dataUtil.getFinalMark(direction),
                            direction.getProfileChosenEntranceDiscipline() == null ? null : direction.getProfileChosenEntranceDiscipline().getFinalMark(),
                                direction.isGraduatedProfileEduInstitution(),
                                reserved.getEntrant().getPerson().getPersonEduInstitution() == null ? null : reserved.getEntrant().getPerson().getPersonEduInstitution().getAverageMark(),
                                    reserved.getEntrant().getPerson().getIdentityCard().getFullFio()
                        ));
                    } else
                    {
                        nonActualEntrantList.add(reserved.getEntrant());
                    }
                }
            }
        } else
        {
            // нужно брать предполагаемый резерв
            for (RequestedEnrollmentDirection direction : entrantDirectionMap.values())
            {
                if (usedEntrantIds.add(direction.getEntrantRequest().getEntrant().getId()))
                {
                    if (dao.isConfigAllowDirection(config, direction))
                    {
                        // это направление приема должно быть в статистике оценок
                        if (!dataUtil.getDirectionSet().contains(direction))
                            throw new RuntimeException("Invalid EntrantDataUtil, no direction '" + direction.getId() + "' for distribution '" + distribution.getId() + "'");

                        cortegeDTOList.add(new EcgCortegeDTO(
                            direction,
                            false,
                            null,
                            direction.getCompetitionKind(),
                            dataUtil.getFinalMark(direction),
                            direction.getProfileChosenEntranceDiscipline() == null ? null : direction.getProfileChosenEntranceDiscipline().getFinalMark(),
                                direction.isGraduatedProfileEduInstitution(),
                                direction.getEntrantRequest().getEntrant().getPerson().getPersonEduInstitution() == null ? null : direction.getEntrantRequest().getEntrant().getPerson().getPersonEduInstitution().getAverageMark(),
                                    direction.getEntrantRequest().getEntrant().getPerson().getIdentityCard().getFullFio()
                        ));
                    } else
                    {
                        nonActualEntrantList.add(direction.getEntrantRequest().getEntrant());
                    }
                }
            }
        }

        // пробегаемся по резерву первого распределения
        if (distribution.getDistribution().getWave() > IEcDistributionDao.DISTRIBUTION_FIRST_WAVE)
        {
            for (EcgEntrantReserved reserved : new DQLSelectBuilder().fromEntity(EcgEntrantReserved.class, "e").column("e")
            .where(eq(property(EcgEntrantReserved.distribution().config().fromAlias("e")), value(config)))
            .where(eq(property(EcgEntrantReserved.distribution().wave().fromAlias("e")), value(IEcDistributionDao.DISTRIBUTION_FIRST_WAVE)))
            .createStatement(session).<EcgEntrantReserved>list())
            {
                if (usedEntrantIds.add(reserved.getEntrant().getId()))
                {
                    nonActualEntrantList.add(reserved.getEntrant());
                }
            }
        }

        // применяем кортежную сортировку
        Collections.sort(cortegeDTOList, comparator);

        // применяем сортировку по фио
        nonActualEntrantList.sort(CommonCollator.comparing(e -> e.getPerson().getFullFio(), true));

        return EcDistributionManager.instance().dao().getEntrantPriorityMap(distribution, usedEntrantIds);
    }

    private IEcgRecommendedDTO.DocumentStatus getDocumentStatus(IEcgDistribution distribution, IEcgCortegeDTO cortegeDTO, Map<EcgEntrantRecommended, IEcgHasBringOriginal> recommendedStateMap, Map<Entrant, RequestedEnrollmentDirection> entrantDirectionMap)
    {
        IEntity entity = cortegeDTO.getEntity();

        if (distribution.getState().isApproved())
        {
            IEcgHasBringOriginal item;

            if (entity instanceof IEcgEntrantRecommended)
            {
                if (entity instanceof EcgEntrantRecommendedDetail)
                {
                    item = (EcgEntrantRecommendedDetail) entity;
                } else
                {
                    EcgEntrantRecommended recommendedFromGeneral = (EcgEntrantRecommended) entity;

                    item = recommendedStateMap.get(recommendedFromGeneral);
                }
            } else
            {
                item = (EcgEntrantReserved) entity;
            }

            return item.getBringOriginal() == null ? IEcgRecommendedDTO.DocumentStatus.TOOK_AWAY : (item.getBringOriginal() ? IEcgRecommendedDTO.DocumentStatus.BRING_ORIGINAL : IEcgRecommendedDTO.DocumentStatus.BRING_COPY);
        } else
        {
            // для не утвержденных нужно брать текущее состояние у выбранного направления приема
            RequestedEnrollmentDirection direction = getDirectionFromCortegeDTO(cortegeDTO, entrantDirectionMap);

            return direction.getState().getCode().equals(UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY) ? IEcgRecommendedDTO.DocumentStatus.TOOK_AWAY : (direction.isOriginalDocumentHandedIn() ? IEcgRecommendedDTO.DocumentStatus.BRING_ORIGINAL : IEcgRecommendedDTO.DocumentStatus.BRING_COPY);
        }
    }

    private RequestedEnrollmentDirection getDirectionFromCortegeDTO(IEcgCortegeDTO cortegeDTO, Map<Entrant, RequestedEnrollmentDirection> entrantDirectionMap)
    {
        IEntity entity = cortegeDTO.getEntity();

        if (entity instanceof IEcgEntrantRecommended)
            return ((IEcgEntrantRecommended) entity).getDirection();
        else if (entity instanceof EcgEntrantReserved)
            return entrantDirectionMap.get(((EcgEntrantReserved) entity).getEntrant());
        else
            return (RequestedEnrollmentDirection) entity;
    }

    private String getEnrollmentStatus(IEcgDistribution distribution, IEcgCortegeDTO cortegeDTO, Map<EcgEntrantRecommended, IEcgHasBringOriginal> recommendedStateMap, Set<Long> entrantPreStudentIds)
    {
        IEntity entity = cortegeDTO.getEntity();

        if (distribution.getState().isApproved())
        {
            if (entity instanceof IEcgEntrantRecommended)
            {
                IEcgEntrantRecommended recommended = (IEcgEntrantRecommended) entity;

                if (distribution.equals(recommended.getDistribution()))
                {
                    if (recommended instanceof EcgEntrantRecommended)
                    {
                        // рекомендован из основного
                        EcgEntrantRecommended recommendedFromGeneral = (EcgEntrantRecommended) recommended;

                        // получаем состояние рекомендованного
                        IEcgHasBringOriginal state = recommendedStateMap.get(recommendedFromGeneral);

                        // если принес оригиналы или в момент утверждения было предзачисление
                        return Boolean.TRUE.equals(state.getBringOriginal()) || Boolean.TRUE.equals(state.getHasPreStudent()) ? ENROLLED : RETIRED;
                    } else
                    {
                        // рекомендован из уточняющего
                        EcgEntrantRecommendedDetail recommendedFromDetail = (EcgEntrantRecommendedDetail) recommended;

                        // если принес оригиналы или в момент утверждения было предзачисление
                        return (recommendedFromDetail.getBringOriginal() != null && recommendedFromDetail.getBringOriginal()) || recommendedFromDetail.getHasPreStudent() ? ENROLLED : RETIRED;
                    }
                } else
                {
                    // рекомендован из основного
                    EcgEntrantRecommended recommendedFromGeneral = (EcgEntrantRecommended) recommended;

                    // получаем состояние рекомендованного
                    IEcgHasBringOriginal state = recommendedStateMap.get(recommendedFromGeneral);

                    return Boolean.TRUE.equals(state.getHasPreStudent()) ? ENROLLED : RETIRED;
                }
            } else
            {
                EcgEntrantReserved reserved = (EcgEntrantReserved) entity;

                // если в момент утверждения было зачисление
                return reserved.getHasPreStudent() ? ENROLLED : null;
            }
        } else
        {
            EcgDistribution item = (EcgDistribution) distribution;

            if (entity instanceof IEcgEntrantRecommended)
            {
                // рекомендованный в основное
                EcgEntrantRecommended recommended = (EcgEntrantRecommended) entity;

                // получаем состояние рекомендованного
                IEcgHasBringOriginal state = recommendedStateMap.get(recommended);

                if (Boolean.TRUE.equals(state.getHasPreStudent())) return ENROLLED;

                return item.equals(recommended.getDistribution()) ? RECOMMENDED : RETIRED;
            } else if (entity instanceof EcgEntrantReserved)
            {
                // резерв текущего
                EcgEntrantReserved reserved = (EcgEntrantReserved) entity;

                // смотрим в текущий момент.
                return entrantPreStudentIds.contains(reserved.getEntrant().getId()) ? ENROLLED : null;
            } else
            {
                // предполагаемый резерв
                RequestedEnrollmentDirection direction = (RequestedEnrollmentDirection) entity;

                return entrantPreStudentIds.contains(direction.getEntrantRequest().getEntrant().getId()) ? ENROLLED : null;
            }
        }
    }
}
