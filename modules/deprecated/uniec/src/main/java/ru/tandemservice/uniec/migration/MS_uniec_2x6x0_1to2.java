package ru.tandemservice.uniec.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniec_2x6x0_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность entranceDiscipline

		// создано свойство passMark
		{
			// создать колонку
            if(!tool.columnExists("entrancediscipline_t", "passmark_p"))
                tool.createColumn("entrancediscipline_t", new DBColumn("passmark_p", DBType.DOUBLE));

		}


    }
}