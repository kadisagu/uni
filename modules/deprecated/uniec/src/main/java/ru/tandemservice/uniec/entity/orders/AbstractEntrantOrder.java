package ru.tandemservice.uniec.entity.orders;

import java.util.List;

import org.tandemframework.common.catalog.entity.ICatalogItem;

import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniec.entity.orders.gen.AbstractEntrantOrderGen;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.OrderStates;

/**
 * Абстрактный приказ на абитуриентов
 */
public abstract class AbstractEntrantOrder extends AbstractEntrantOrderGen implements IAbstractOrder
{
    public static final String P_PARAGRAPH_LIST = "paragraphList";
    public static final String P_READONLY = "readonly";
    public static final String P_EXTRACT_COUNT = "countExtract";

    @Override
    public List<? extends IAbstractParagraph> getParagraphList()
    {
        return UniDaoFacade.getCoreDao().getList(AbstractEntrantParagraph.class, IAbstractParagraph.L_ORDER, this, IAbstractParagraph.P_NUMBER);
    }

    @Override
    public int getParagraphCount()
    {
        return UniDaoFacade.getCoreDao().getCount(AbstractEntrantParagraph.class, IAbstractParagraph.L_ORDER, this);
    }

    public boolean isReadonly()
    {
        return !UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(getState().getCode());
    }

    @Override
    public void setState(ICatalogItem state)
    {
        setState((OrderStates) state);
    }
}