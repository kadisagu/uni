/* $Id$ */
package ru.tandemservice.uniec.component.listextract.ent1.e1.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.IAbstractListParagraphPubDAO;
import ru.tandemservice.uniec.entity.orders.SplitBudgetBachelorEntrantsStuListExtract;

/**
 * @author Dmitry Seleznev
 * @since 13.07.2012
 */
public interface IDAO extends IAbstractListParagraphPubDAO<SplitBudgetBachelorEntrantsStuListExtract, Model>
{
}