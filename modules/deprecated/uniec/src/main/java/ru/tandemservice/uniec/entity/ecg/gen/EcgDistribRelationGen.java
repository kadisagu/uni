package ru.tandemservice.uniec.entity.ecg.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.ecg.EcgDistribQuota;
import ru.tandemservice.uniec.entity.ecg.EcgDistribRelation;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь выбранного направления с планом приема в распределении по конкурсной группе
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EcgDistribRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.ecg.EcgDistribRelation";
    public static final String ENTITY_NAME = "ecgDistribRelation";
    public static final int VERSION_HASH = 1697016624;
    private static IEntityMeta ENTITY_META;

    public static final String L_QUOTA = "quota";
    public static final String L_ENTRANT_DIRECTION = "entrantDirection";

    private EcgDistribQuota _quota;     // План приема в распределении по конкурсной группе
    private RequestedEnrollmentDirection _entrantDirection;     // Выбранное направление приема

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return План приема в распределении по конкурсной группе. Свойство не может быть null.
     */
    @NotNull
    public EcgDistribQuota getQuota()
    {
        return _quota;
    }

    /**
     * @param quota План приема в распределении по конкурсной группе. Свойство не может быть null.
     */
    public void setQuota(EcgDistribQuota quota)
    {
        dirty(_quota, quota);
        _quota = quota;
    }

    /**
     * @return Выбранное направление приема. Свойство не может быть null.
     */
    @NotNull
    public RequestedEnrollmentDirection getEntrantDirection()
    {
        return _entrantDirection;
    }

    /**
     * @param entrantDirection Выбранное направление приема. Свойство не может быть null.
     */
    public void setEntrantDirection(RequestedEnrollmentDirection entrantDirection)
    {
        dirty(_entrantDirection, entrantDirection);
        _entrantDirection = entrantDirection;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EcgDistribRelationGen)
        {
            setQuota(((EcgDistribRelation)another).getQuota());
            setEntrantDirection(((EcgDistribRelation)another).getEntrantDirection());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EcgDistribRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EcgDistribRelation.class;
        }

        public T newInstance()
        {
            return (T) new EcgDistribRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "quota":
                    return obj.getQuota();
                case "entrantDirection":
                    return obj.getEntrantDirection();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "quota":
                    obj.setQuota((EcgDistribQuota) value);
                    return;
                case "entrantDirection":
                    obj.setEntrantDirection((RequestedEnrollmentDirection) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "quota":
                        return true;
                case "entrantDirection":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "quota":
                    return true;
                case "entrantDirection":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "quota":
                    return EcgDistribQuota.class;
                case "entrantDirection":
                    return RequestedEnrollmentDirection.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EcgDistribRelation> _dslPath = new Path<EcgDistribRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EcgDistribRelation");
    }
            

    /**
     * @return План приема в распределении по конкурсной группе. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.ecg.EcgDistribRelation#getQuota()
     */
    public static EcgDistribQuota.Path<EcgDistribQuota> quota()
    {
        return _dslPath.quota();
    }

    /**
     * @return Выбранное направление приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.ecg.EcgDistribRelation#getEntrantDirection()
     */
    public static RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> entrantDirection()
    {
        return _dslPath.entrantDirection();
    }

    public static class Path<E extends EcgDistribRelation> extends EntityPath<E>
    {
        private EcgDistribQuota.Path<EcgDistribQuota> _quota;
        private RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> _entrantDirection;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return План приема в распределении по конкурсной группе. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.ecg.EcgDistribRelation#getQuota()
     */
        public EcgDistribQuota.Path<EcgDistribQuota> quota()
        {
            if(_quota == null )
                _quota = new EcgDistribQuota.Path<EcgDistribQuota>(L_QUOTA, this);
            return _quota;
        }

    /**
     * @return Выбранное направление приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.ecg.EcgDistribRelation#getEntrantDirection()
     */
        public RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> entrantDirection()
        {
            if(_entrantDirection == null )
                _entrantDirection = new RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection>(L_ENTRANT_DIRECTION, this);
            return _entrantDirection;
        }

        public Class getEntityClass()
        {
            return EcgDistribRelation.class;
        }

        public String getEntityName()
        {
            return "ecgDistribRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
