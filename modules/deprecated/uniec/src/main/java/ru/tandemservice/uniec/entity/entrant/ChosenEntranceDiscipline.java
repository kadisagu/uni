package ru.tandemservice.uniec.entity.entrant;

import org.tandemframework.core.common.ITitled;

import ru.tandemservice.uniec.entity.entrant.gen.ChosenEntranceDisciplineGen;

/**
 * Выбранное вступительное испытание
 */
public class ChosenEntranceDiscipline extends ChosenEntranceDisciplineGen implements ITitled
{
    @Override
    public String getTitle()
    {
        return getEnrollmentCampaignDiscipline().getTitle();
    }
}