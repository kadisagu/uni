/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.settings.DisciplinesGroupAddEdit;

import java.util.List;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IIdentifiableWrapper;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.settings.DisciplinesGroup;

/**
 * @author agolubenko
 * @since 17.06.2008
 */
@Input(keys = {"groupId", "enrollmentCampaignId"}, bindings = {"disciplinesGroup.id", "enrollmentCampaign.id"})
public class Model
{
    private DisciplinesGroup _disciplinesGroup = new DisciplinesGroup();
    private List<IIdentifiableWrapper> _disciplinesList;
    private ISelectModel _disciplinesSelectModel;
    private EnrollmentCampaign _enrollmentCampaign = new EnrollmentCampaign();

    public DisciplinesGroup getDisciplinesGroup()
    {
        return _disciplinesGroup;
    }

    public void setDisciplinesGroup(DisciplinesGroup disciplinesGroup)
    {
        _disciplinesGroup = disciplinesGroup;
    }

    public List<IIdentifiableWrapper> getDisciplinesList()
    {
        return _disciplinesList;
    }

    public void setDisciplinesList(List<IIdentifiableWrapper> disciplinesList)
    {
        _disciplinesList = disciplinesList;
    }

    public ISelectModel getDisciplinesSelectModel()
    {
        return _disciplinesSelectModel;
    }

    public void setDisciplinesSelectModel(ISelectModel disciplinesSelectModel)
    {
        _disciplinesSelectModel = disciplinesSelectModel;
    }

    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }
}
