/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.menu.EnrollmentOrders;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectFragmentBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.services.UniServiceFacade;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniec.entity.orders.EnrollmentParagraph;
import ru.tandemservice.uniec.entity.orders.EnrollmentRevertParagraph;
import ru.tandemservice.uniec.util.EntrantFilterUtil;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.dao.MoveDaoFacade;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;
import ru.tandemservice.unimove.entity.catalog.OrderStates;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimv.entity.visa.VisaTask;
import ru.tandemservice.unimv.services.visatask.ISendToCoordinationService;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author vip_delete
 * @since 07.04.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.setOrderTypeListModel(new FullCheckSelectModel(EntrantEnrollmentOrderType.P_SHORT_TITLE)
        {
            @Override
            public ListResult findValues(String filter)
            {
                List<EntrantEnrollmentOrderType> list = EcOrderManager.instance().dao().getOrderTypeList(model.getEnrollmentCampaign());
                list.add(getNotNull(EntrantEnrollmentOrderType.class, EntrantEnrollmentOrderType.code(), EntrantEnrollmentOrderType.REVERT_ORDER_CODE));
                return new ListResult<>(list);
            }
        });
        model.setOrderStateList(getCatalogItemListOrderByCode(OrderStates.class));
        model.setCompensationTypeList(getCatalogItemListOrderByCode(CompensationType.class));
        model.setFormativeOrgUnitListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(EnrollmentDirection.ENTITY_CLASS, "d", new String[]{EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT});
                builder.add(MQExpression.eq("d", EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
                if (StringUtils.isNotEmpty(filter))
                    builder.add(MQExpression.like("d", EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT + "." + OrgUnit.P_TITLE, "%" + filter));

                Collection<OrgUnit> set = new TreeSet<>(ITitled.TITLED_COMPARATOR);
                set.addAll(builder.<OrgUnit>getResultList(getSession()));
                return new ListResult<>(set);
            }
        });
        model.setTerritorialOrgUnitListModel(new FullCheckSelectModel(OrgUnit.P_TERRITORIAL_TITLE)
        {
            @Override
            public ListResult findValues(String filter)
            {
                OrgUnit formativeOrgUnit = model.getSettings().get(Model.FORMATIVE_ORGUNIT_FILTER_NAME);

                MQBuilder builder = new MQBuilder(EnrollmentDirection.ENTITY_CLASS, "d", new String[]{EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT});
                builder.add(MQExpression.eq("d", EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
                if (formativeOrgUnit != null)
                    builder.add(MQExpression.eq("d", EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, formativeOrgUnit));
                if (StringUtils.isNotEmpty(filter))
                    builder.add(MQExpression.like("d", EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT + "." + OrgUnit.P_TERRITORIAL_TITLE, "%" + filter));
                builder.addOrder("d", EnrollmentDirection.educationOrgUnit().territorialOrgUnit().territorialTitle());
                return new ListResult<>(builder.<OrgUnit>getResultList(getSession()));
            }
        });
        model.setEducationLevelHighSchoolListModel(new FullCheckSelectModel(EducationLevelsHighSchool.P_DISPLAYABLE_TITLE)
        {
            @Override
            public ListResult findValues(String filter)
            {
                OrgUnit formativeOrgUnit = model.getSettings().get(Model.FORMATIVE_ORGUNIT_FILTER_NAME);
                OrgUnit territorialOrgUnit = model.getSettings().get(Model.TERRITORIAL_ORGUNIT_FILTER_NAME);

                MQBuilder builder = new MQBuilder(EnrollmentDirection.ENTITY_CLASS, "d", new String[]{EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL});
                builder.add(MQExpression.eq("d", EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
                if (formativeOrgUnit != null)
                    builder.add(MQExpression.eq("d", EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, formativeOrgUnit));
                if (territorialOrgUnit != null)
                    builder.add(MQExpression.eq("d", EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, territorialOrgUnit));
                if (StringUtils.isNotEmpty(filter))
                    builder.add(MQExpression.like("d", EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL + "." + EducationLevelsHighSchool.P_TITLE, "%" + filter));

                Collection<EducationLevelsHighSchool> set = new TreeSet<>(new Comparator<EducationLevelsHighSchool>()
                {
                    @Override
                    public int compare(EducationLevelsHighSchool o1, EducationLevelsHighSchool o2)
                    {
                        return o1.getDisplayableTitle().compareTo(o2.getDisplayableTitle());
                    }
                });
                set.addAll(builder.<EducationLevelsHighSchool>getResultList(getSession()));
                return new ListResult<>(set);
            }
        });
        model.setDevelopFormListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                OrgUnit formativeOrgUnit = model.getSettings().get(Model.FORMATIVE_ORGUNIT_FILTER_NAME);
                OrgUnit territorialOrgUnit = model.getSettings().get(Model.TERRITORIAL_ORGUNIT_FILTER_NAME);
                EducationLevelsHighSchool educationLevelHighSchool = model.getSettings().get(Model.EDUCATION_LEVEL_HIGH_SCHOOL_FILTER_NAME);

                MQBuilder builder = new MQBuilder(EnrollmentDirection.ENTITY_CLASS, "d", new String[]{EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_FORM});
                builder.add(MQExpression.eq("d", EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
                if (formativeOrgUnit != null)
                    builder.add(MQExpression.eq("d", EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, formativeOrgUnit));
                if (territorialOrgUnit != null)
                    builder.add(MQExpression.eq("d", EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, territorialOrgUnit));
                if (educationLevelHighSchool != null)
                    builder.add(MQExpression.eq("d", EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, educationLevelHighSchool));

                Collection<DevelopForm> set = new TreeSet<>(CommonCatalogUtil.CATALOG_CODE_COMPARATOR);
                set.addAll(builder.<DevelopForm>getResultList(getSession()));
                return new ListResult<>(set);
            }
        });
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareListDataSource(Model model)
    {
        // get filter values
        EnrollmentCampaign enrollmentCampaign = model.getEnrollmentCampaign();
        final Date createDate = model.getSettings().get(Model.CREATE_DATE_FILTER_NAME);
        final Date commitDate = model.getSettings().get(Model.COMMIT_DATE_FILTER_NAME);
        final String number = model.getSettings().get(Model.NUMBER_FILTER_NAME);
        final EntrantEnrollmentOrderType orderType = model.getSettings().get(Model.ORDER_TYPE_FILTER_NAME);
        final OrderStates orderState = model.getSettings().get(Model.ORDER_STATE_FILTER_NAME);
        final CompensationType compensationType = model.getSettings().get(Model.COMPENSATION_TYPE_FILTER_NAME);
        final OrgUnit formativeOrgUnit = model.getSettings().get(Model.FORMATIVE_ORGUNIT_FILTER_NAME);
        final OrgUnit territorialOrgUnit = model.getSettings().get(Model.TERRITORIAL_ORGUNIT_FILTER_NAME);
        final EducationLevelsHighSchool educationLevelHighSchool = model.getSettings().get(Model.EDUCATION_LEVEL_HIGH_SCHOOL_FILTER_NAME);
        final DevelopForm developForm = model.getSettings().get(Model.DEVELOP_FROM_FILTER_NAME);

        // create MQBuilder
        MQBuilder builder = new MQBuilder(EnrollmentOrder.ENTITY_CLASS, "o");
        builder.add(MQExpression.eq("o", EnrollmentOrder.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign));
        if (createDate != null)
            builder.add(UniMQExpression.eqDate("o", IAbstractOrder.P_CREATE_DATE, createDate));
        if (commitDate != null)
            builder.add(UniMQExpression.eqDate("o", EnrollmentOrder.P_COMMIT_DATE, commitDate));
        if (StringUtils.isNotEmpty(number))
            builder.add(MQExpression.like("o", IAbstractOrder.P_NUMBER, "%" + number));
        if (orderType != null)
            builder.add(MQExpression.eq("o", EnrollmentOrder.L_TYPE, orderType));
        if (orderState != null)
            builder.add(MQExpression.eq("o", IAbstractOrder.L_STATE, orderState));
        if (compensationType != null)
            builder.add(MQExpression.in("o", EnrollmentOrder.P_ID, new MQBuilder(EnrollmentExtract.ENTITY_CLASS, "e", new String[]{IAbstractExtract.L_PARAGRAPH + "." + IAbstractParagraph.L_ORDER + "." + IAbstractOrder.P_ID})
                    .add(MQExpression.eq("e", IAbstractExtract.L_ENTITY + "." + PreliminaryEnrollmentStudent.L_COMPENSATION_TYPE, compensationType))
            ));
        if (formativeOrgUnit != null)
            builder.add(MQExpression.in("o", EnrollmentOrder.P_ID, new MQBuilder(EnrollmentExtract.ENTITY_CLASS, "e", new String[]{IAbstractExtract.L_PARAGRAPH + "." + IAbstractParagraph.L_ORDER + "." + IAbstractOrder.P_ID})
                    .add(MQExpression.eq("e", IAbstractExtract.L_ENTITY + "." + PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, formativeOrgUnit))
            ));
        if (territorialOrgUnit != null)
            builder.add(MQExpression.in("o", EnrollmentOrder.P_ID, new MQBuilder(EnrollmentExtract.ENTITY_CLASS, "e", new String[]{IAbstractExtract.L_PARAGRAPH + "." + IAbstractParagraph.L_ORDER + "." + IAbstractOrder.P_ID})
                    .add(MQExpression.eq("e", IAbstractExtract.L_ENTITY + "." + PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, territorialOrgUnit))
            ));
        if (educationLevelHighSchool != null)
            builder.add(MQExpression.in("o", EnrollmentOrder.P_ID, new MQBuilder(EnrollmentExtract.ENTITY_CLASS, "e", new String[]{IAbstractExtract.L_PARAGRAPH + "." + IAbstractParagraph.L_ORDER + "." + IAbstractOrder.P_ID})
                    .add(MQExpression.eq("e", IAbstractExtract.L_ENTITY + "." + PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, educationLevelHighSchool))
            ));
        if (developForm != null)
            builder.add(MQExpression.in("o", EnrollmentOrder.P_ID, new MQBuilder(EnrollmentExtract.ENTITY_CLASS, "e", new String[]{IAbstractExtract.L_PARAGRAPH + "." + IAbstractParagraph.L_ORDER + "." + IAbstractOrder.P_ID})
                    .add(MQExpression.eq("e", IAbstractExtract.L_ENTITY + "." + PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_FORM, developForm))
            ));

        new OrderDescriptionRegistry("o").applyOrder(builder, model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());


        // Статистика по количеству параграфов
        DQLSelectBuilder countBuilder = new DQLSelectBuilder().fromEntity(EnrollmentParagraph.class, "p")
                .column(property(EnrollmentParagraph.order().id().fromAlias("p")))
                .column(DQLFunctions.count("p.id"))
                .group(property(EnrollmentParagraph.order().id().fromAlias("p")));

        DQLSelectFragmentBuilder union = new DQLSelectFragmentBuilder();
        union.fromEntity(EnrollmentRevertParagraph.class, "rp");
        union.column(property(EnrollmentRevertParagraph.order().id().fromAlias("rp")));
        union.column(DQLFunctions.count("rp.id"));
        union.group(property(EnrollmentParagraph.order().id().fromAlias("rp")));

        countBuilder.unionAll(union.buildSelectRule());

        Map<Long, Long> countMap = new HashMap<>();
        for (Object[] item : countBuilder.createStatement(getSession()).<Object[]>list())
        {
            countMap.put((Long)item[0], (Long)item[1]);
        }

        // inject view properties
        for (ViewWrapper wrapper : ViewWrapper.getPatchedList(model.getDataSource()))
        {
            EnrollmentOrder order = (EnrollmentOrder) wrapper.getEntity();
            boolean finished = UnimoveDefines.CATALOG_ORDER_STATE_FINISHED.equals(order.getState().getCode());
            Long parCount = countMap.get(order.getId());
            wrapper.setViewProperty("disabledPrint", !finished && (parCount == null || parCount == 0));
            wrapper.setViewProperty("disabledStateChange", order.isRevert() ||
                    (!order.getState().getCode().equals(UnimoveDefines.CATALOG_EXTRACT_STATE_ACCEPTED) && !order.getState().getCode().equals(UnimoveDefines.CATALOG_EXTRACT_STATE_FORMATIVE)));
        }
    }

    @Override
    public List<EnrollmentOrder> getEnrollmentOrderList(Model model)
    {
        return getEnrollmentOrderBuilder(model).getResultList(getSession());
    }

    @Override
    public void doSendCheckedToCoordination(Model model, IPersistentPersonable initiator)
    {
        List<Long> selectedIds = UniBaseUtils.getIdList(((CheckboxColumn) model.getDataSource().getColumn("selected")).getSelectedObjects());
        if (selectedIds.isEmpty())
            throw new ApplicationException("Выберите приказы для совершения данного действия.");
        List<EnrollmentOrder> orders = new MQBuilder(EnrollmentOrder.ENTITY_CLASS, "o").add(MQExpression.in("o", "id", selectedIds)).getResultList(getSession());
        for (EnrollmentOrder order : orders)
        {
            VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(order);
            if (visaTask != null)
                throw new ApplicationException("Нельзя отправить приказ на согласование, так как он уже на согласовании.");
            if (StringUtils.isEmpty(order.getNumber()))
                throw new ApplicationException("Нельзя отправить приказ на согласование без номера.");
            if (order.getCommitDate() == null)
                throw new ApplicationException("Нельзя отправить приказ на согласование без даты приказа.");
            if (order.getParagraphCount() == 0)
                throw new ApplicationException("Нельзя отправить приказ на согласование без параграфов.");
            if (!order.getState().getCode().equals(UnimoveDefines.CATALOG_EXTRACT_STATE_FORMATIVE))
                throw new ApplicationException("Не все выбранные приказы можно отправить на согласование.");
        }
        for (EnrollmentOrder order : orders)
        {
            //2. надо сохранить печатную форму приказа
            EcOrderManager.instance().dao().saveEnrollmentOrderText(order);

            //3. отправляем на согласование
            ISendToCoordinationService sendToCoordinationService = UniServiceFacade.getService(ISendToCoordinationService.SEND_TO_COORDINATION_SERVICE);
            sendToCoordinationService.init(order, initiator);
            sendToCoordinationService.execute();
        }
    }

    @Override
    public void doCommit(Model model)
    {
        List<Long> selectedIds = UniBaseUtils.getIdList(((CheckboxColumn) model.getDataSource().getColumn("selected")).getSelectedObjects());
        if (selectedIds.isEmpty())
            throw new ApplicationException("Выберите приказы для совершения данного действия.");
        List<EnrollmentOrder> orders = new MQBuilder(EnrollmentOrder.ENTITY_CLASS, "o").add(MQExpression.in("o", "id", selectedIds)).getResultList(getSession());
        for (EnrollmentOrder order : orders)
        {
            if (order.getNumber() == null)
                throw new ApplicationException("Нельзя проводить приказ без номера.");
            if (order.getCommitDate() == null)
                throw new ApplicationException("Нельзя проводить приказ без даты приказа.");
            if (!order.getState().getCode().equals(UnimoveDefines.CATALOG_EXTRACT_STATE_ACCEPTED))
                throw new ApplicationException("Не все выбранные приказы можно провести.");
        }

        OrderStates stateOrderFinished = UniDaoFacade.getCoreDao().getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FINISHED);
        ExtractStates stateExtractFinished = UniDaoFacade.getCoreDao().getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED);
        for (EnrollmentOrder order : orders)
        {
            //обновляем печатную форму
            EcOrderManager.instance().dao().saveEnrollmentOrderText(order);
            MoveDaoFacade.getMoveDao().doCommitOrder(order, stateOrderFinished, stateExtractFinished, null);
        }
    }

    private MQBuilder getEnrollmentOrderBuilder(Model model)
    {
        // get filter values
        EnrollmentCampaign enrollmentCampaign = model.getEnrollmentCampaign();
        final Date createDate = model.getSettings().get(Model.CREATE_DATE_FILTER_NAME);
        final Date commitDate = model.getSettings().get(Model.COMMIT_DATE_FILTER_NAME);
        final String number = model.getSettings().get(Model.NUMBER_FILTER_NAME);
        final EntrantEnrollmentOrderType orderType = model.getSettings().get(Model.ORDER_TYPE_FILTER_NAME);
        final OrderStates orderState = model.getSettings().get(Model.ORDER_STATE_FILTER_NAME);
        final CompensationType compensationType = model.getSettings().get(Model.COMPENSATION_TYPE_FILTER_NAME);
        final OrgUnit formativeOrgUnit = model.getSettings().get(Model.FORMATIVE_ORGUNIT_FILTER_NAME);
        final OrgUnit territorialOrgUnit = model.getSettings().get(Model.TERRITORIAL_ORGUNIT_FILTER_NAME);
        final EducationLevelsHighSchool educationLevelHighSchool = model.getSettings().get(Model.EDUCATION_LEVEL_HIGH_SCHOOL_FILTER_NAME);
        final DevelopForm developForm = model.getSettings().get(Model.DEVELOP_FROM_FILTER_NAME);

        // create MQBuilder
        MQBuilder builder = new MQBuilder(EnrollmentOrder.ENTITY_CLASS, "o");
        builder.add(MQExpression.eq("o", EnrollmentOrder.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign));
        if (createDate != null)
            builder.add(UniMQExpression.eqDate("o", IAbstractOrder.P_CREATE_DATE, createDate));
        if (commitDate != null)
            builder.add(UniMQExpression.eqDate("o", EnrollmentOrder.P_COMMIT_DATE, commitDate));
        if (StringUtils.isNotEmpty(number))
            builder.add(MQExpression.like("o", IAbstractOrder.P_NUMBER, "%" + number));
        if (orderType != null)
            builder.add(MQExpression.eq("o", EnrollmentOrder.L_TYPE, orderType));
        if (orderState != null)
            builder.add(MQExpression.eq("o", IAbstractOrder.L_STATE, orderState));
        if (compensationType != null)
            builder.add(MQExpression.in("o", EnrollmentOrder.P_ID, new MQBuilder(EnrollmentExtract.ENTITY_CLASS, "e", new String[]{IAbstractExtract.L_PARAGRAPH + "." + IAbstractParagraph.L_ORDER + "." + IAbstractOrder.P_ID})
                    .add(MQExpression.eq("e", IAbstractExtract.L_ENTITY + "." + PreliminaryEnrollmentStudent.L_COMPENSATION_TYPE, compensationType))
            ));
        if (formativeOrgUnit != null)
            builder.add(MQExpression.in("o", EnrollmentOrder.P_ID, new MQBuilder(EnrollmentExtract.ENTITY_CLASS, "e", new String[]{IAbstractExtract.L_PARAGRAPH + "." + IAbstractParagraph.L_ORDER + "." + IAbstractOrder.P_ID})
                    .add(MQExpression.eq("e", IAbstractExtract.L_ENTITY + "." + PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, formativeOrgUnit))
            ));
        if (territorialOrgUnit != null)
            builder.add(MQExpression.in("o", EnrollmentOrder.P_ID, new MQBuilder(EnrollmentExtract.ENTITY_CLASS, "e", new String[]{IAbstractExtract.L_PARAGRAPH + "." + IAbstractParagraph.L_ORDER + "." + IAbstractOrder.P_ID})
                    .add(MQExpression.eq("e", IAbstractExtract.L_ENTITY + "." + PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, territorialOrgUnit))
            ));
        if (educationLevelHighSchool != null)
            builder.add(MQExpression.in("o", EnrollmentOrder.P_ID, new MQBuilder(EnrollmentExtract.ENTITY_CLASS, "e", new String[]{IAbstractExtract.L_PARAGRAPH + "." + IAbstractParagraph.L_ORDER + "." + IAbstractOrder.P_ID})
                    .add(MQExpression.eq("e", IAbstractExtract.L_ENTITY + "." + PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, educationLevelHighSchool))
            ));
        if (developForm != null)
            builder.add(MQExpression.in("o", EnrollmentOrder.P_ID, new MQBuilder(EnrollmentExtract.ENTITY_CLASS, "e", new String[]{IAbstractExtract.L_PARAGRAPH + "." + IAbstractParagraph.L_ORDER + "." + IAbstractOrder.P_ID})
                    .add(MQExpression.eq("e", IAbstractExtract.L_ENTITY + "." + PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_FORM, developForm))
            ));

        return builder;
    }
}
