/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec.base.bo.EcProfileDistribution.util;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommended;
import ru.tandemservice.uniec.base.entity.ecgp.EcgpEntrantRecommended;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline;
import ru.tandemservice.uniec.entity.settings.EnrollmentCompetitionKind;

/**
 * @author Vasily Zhukov
 * @since 15.05.2012
 */
public class EcgpRecommendedDTO extends IdentifiableWrapper<EcgEntrantRecommended> implements IEcgpRecommendedDTO
{
    private static final long serialVersionUID = 1L;
    private EcgpEntrantRecommended _entrantRecommended;
    private String _fio;
    private CompetitionKind _competitionKind;
    private TargetAdmissionKind _targetAdmissionKind;
    private Double _finalMark;
    private String _priorities;
    private String _prioritiesShort;

    /**
     * @param entrantRecommended рекомендованный
     * @param finalMark          сумма баллов
     * @param priorities         приоритеты
     * @param prioritiesShort    приоритеты сокращенно
     */
    public EcgpRecommendedDTO(EcgpEntrantRecommended entrantRecommended, Double finalMark, String priorities, String prioritiesShort)
    {
        super(entrantRecommended.getId(), "");

        _entrantRecommended = entrantRecommended;

        _fio = _entrantRecommended.getPreStudent().getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getPerson().getIdentityCard().getFullFio();

        _competitionKind = entrantRecommended.getPreStudent().getRequestedEnrollmentDirection().getCompetitionKind();

        _targetAdmissionKind = entrantRecommended.getPreStudent().getRequestedEnrollmentDirection().getTargetAdmissionKind();

        _finalMark = finalMark;

        _priorities = priorities;

        _prioritiesShort = prioritiesShort;

    }

    // Getters

    @Override
    public EcgpEntrantRecommended getEntrantRecommended()
    {
        return _entrantRecommended;
    }

    @Override
    public String getCompetitionKindTitle()
    {
        return isTargetAdmission() && _targetAdmissionKind != null ? (EnrollmentCompetitionKind.TARGET_ADMISSION + (UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION.equals(_competitionKind.getCode()) ? "" : ", " + _competitionKind.getTitle())) : _competitionKind.getTitle();
    }

    @Override
    public String getTargetAdmissionKindTitle()
    {
        return isTargetAdmission() && _targetAdmissionKind != null ? _targetAdmissionKind.getFullHierarhyTitle() : null;
    }

    @Override
    public String getGraduatedProfileEduInstitutionTitle()
    {
        return _entrantRecommended.getPreStudent().getRequestedEnrollmentDirection().isGraduatedProfileEduInstitution() ? "Да" : null;
    }

    @Override
    public String getPriorities()
    {
        return _priorities;
    }

    public String getPrioritiesShort()
    {
        return _prioritiesShort;
    }

    @Override
    public IEntity getEntity()
    {
        return null;
    }

    @Override
    public boolean isTargetAdmission()
    {
        return _entrantRecommended.getPreStudent().isTargetAdmission();
    }

    @Override
    public TargetAdmissionKind getTargetAdmissionKind()
    {
        return isTargetAdmission() ? _targetAdmissionKind : null;
    }

    @Override
    public CompetitionKind getCompetitionKind()
    {
        return _entrantRecommended.getPreStudent().getRequestedEnrollmentDirection().getCompetitionKind();
    }

    @Override
    public Double getFinalMark()
    {
        return _finalMark;
    }

    @Override
    public Double getProfileMark()
    {
        if (_finalMark == null) return null;

        ChosenEntranceDiscipline chosen = _entrantRecommended.getPreStudent().getRequestedEnrollmentDirection().getProfileChosenEntranceDiscipline();

        return chosen == null ? null : chosen.getFinalMark();
    }

    @Override
    public Boolean isGraduatedProfileEduInstitution()
    {
        return _entrantRecommended.getPreStudent().getRequestedEnrollmentDirection().isGraduatedProfileEduInstitution();
    }

    @Override
    public Double getCertificateAverageMark()
    {
        PersonEduInstitution personEduInstitution = _entrantRecommended.getPreStudent().getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getPerson().getPersonEduInstitution();
        return personEduInstitution == null ? null : personEduInstitution.getAverageMark();
    }

    @Override
    public String getFio()
    {
        return _fio;
    }
}