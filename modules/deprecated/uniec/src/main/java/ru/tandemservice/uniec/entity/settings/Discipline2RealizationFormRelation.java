package ru.tandemservice.uniec.entity.settings;

import org.tandemframework.core.common.ITitled;

import ru.tandemservice.uniec.entity.settings.gen.Discipline2RealizationFormRelationGen;

public class Discipline2RealizationFormRelation extends Discipline2RealizationFormRelationGen implements ITitled
{
    @Override
    public String getTitle()
    {
        return getDiscipline().getTitle();
    }
}