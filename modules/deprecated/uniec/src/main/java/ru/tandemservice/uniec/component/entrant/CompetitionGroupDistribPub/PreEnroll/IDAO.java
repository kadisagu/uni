package ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.PreEnroll;

import org.tandemframework.core.info.ErrorCollector;

import ru.tandemservice.uni.dao.IPrepareable;

/**
 * @author vdanilov
 */
public interface IDAO extends IPrepareable<Model> {
	void delete(Model model, Long id);
	void prepareStudentDataSource(Model model);
	void doPreEnroll(Model model, ErrorCollector errorCollector);

}
