/**
 *$Id$
 */
package ru.tandemservice.uniec.base.bo.EcReport.logic.EnrollmentDirProfReport;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.biff.CellReferenceHelper;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.*;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import jxl.write.Number;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.JExcelUtil;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.tapsupport.component.selection.CommonMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.CommonSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uni.util.DevelopUtil;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.codes.EntrantStateCodes;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.report.EnrollmentDirProfReport;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.report.RequestedEnrollmentDirectionComparator;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.Boolean;
import java.util.*;

/**
 * @author Alexander Shaburov
 * @since 05.07.12
 */
public class EnrollmentDirProfReportDAO extends CommonDAO implements IEnrollmentDirProfReportDAO
{
    @Override
    public <T extends EnrollmentDirProfReportModel> T prepareModel(T model)
    {
        // фильтр Приемная компания
        List<EnrollmentCampaign> ecList = DataAccessServices.dao()
        .getList(new DQLSelectBuilder().fromEntity(EnrollmentCampaign.class, "b").column("b")
            .order(DQLExpressions.property(EnrollmentCampaign.id().fromAlias("b")), OrderDirection.desc));
        model.setEnrollmentCampaign(ecList.isEmpty() ? null : ecList.get(0));

        // фильтры Заявления с - по
        EnrollmentCampaign enrollmentCampaign = model.getEnrollmentCampaign();
        List<EnrollmentCampaignPeriod> periodList = enrollmentCampaign != null ? enrollmentCampaign.getPeriodList() : new LinkedList<>();
        model.setRequestFrom(periodList.isEmpty() ? null : periodList.get(0).getDateFrom());
        model.setRequestTo(periodList.isEmpty() ? null : periodList.get(0).getDateTo());

        // заполняем модели селектов
        model.setEnrollmentCampaignModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrollmentCampaign.class, "b").column("b");
                builder.where(DQLExpressions.likeUpper(DQLExpressions.property(EnrollmentCampaign.title().fromAlias("b")), DQLExpressions.value(CoreStringUtils.escapeLike(filter))));
                if (o != null)
                    builder.where(DQLExpressions.eqValue(DQLExpressions.property(EnrollmentCampaign.id().fromAlias("b")), o));
                builder.order(DQLExpressions.property(EnrollmentCampaign.id().fromAlias("b")), OrderDirection.desc);

                return new DQLListResultBuilder(builder);
            }
        });
        List<DataWrapper> ecStageWrapperList = new LinkedList<>();
        ecStageWrapperList.add(new DataWrapper(EnrollmentDirProfReportModel.ON_PROCESS_RECEPTION, "по ходу приема документов"));
        ecStageWrapperList.add(new DataWrapper(EnrollmentDirProfReportModel.ON_RESULT_TEST, "по результатам сдачи вступительных испытаний"));
        ecStageWrapperList.add(new DataWrapper(EnrollmentDirProfReportModel.ON_RESULT_ENROLLMENT, "по результатам зачисления"));
        model.setEnrollmentCampaignStageModel(new LazySimpleSelectModel<>(ecStageWrapperList));
        model.setCompensationTypeModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(CompensationType.class, "b").column("b");
                builder.where(DQLExpressions.likeUpper(DQLExpressions.property(CompensationType.title().fromAlias("b")), DQLExpressions.value(CoreStringUtils.escapeLike(filter))));
                if (o != null)
                    builder.where(DQLExpressions.eqValue(DQLExpressions.property(CompensationType.id().fromAlias("b")), o));
                builder.order(DQLExpressions.property(CompensationType.code().fromAlias("b")));

                return new DQLListResultBuilder(builder);
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((CompensationType) value).getShortTitle();
            }
        });
        model.setStudentCategoryModel(new CommonMultiSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentCategory.class, "b").column("b");
                builder.where(DQLExpressions.likeUpper(DQLExpressions.property(StudentCategory.title().fromAlias("b")), DQLExpressions.value(CoreStringUtils.escapeLike(filter))));
                if (set != null)
                    builder.where(DQLExpressions.in(DQLExpressions.property(StudentCategory.id().fromAlias("b")), set));
                builder.order(DQLExpressions.property(StudentCategory.title().fromAlias("b")));

                return new DQLListResultBuilder(builder);
            }
        });
        model.setQualificationModel(new QualificationModel(getSession()));
        model.setExcludeParallelModel(TwinComboDataSourceHandler.getYesNoDefaultSelectModel());

        //заполняем модели селекта с помощью утили
        model.setFormativeOrgUnitModel((IMultiSelectModel) MultiEnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(null, model));
        model.setTerritorialOrgUnitModel((IMultiSelectModel) MultiEnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(null, model));
        model.setEducationLevelHighSchoolModel((IMultiSelectModel) MultiEnrollmentDirectionUtil.createEducationLevelsHighSchoolModel(model));
        model.setDevelopFormModel((IMultiSelectModel) MultiEnrollmentDirectionUtil.createDevelopFormModel(model));
        model.setDevelopConditionModel((IMultiSelectModel) MultiEnrollmentDirectionUtil.createDevelopConditionModel(model));
        model.setDevelopTechModel((IMultiSelectModel) MultiEnrollmentDirectionUtil.createDevelopTechModel(model));
        model.setDevelopPeriodModel((IMultiSelectModel) MultiEnrollmentDirectionUtil.createDevelopPeriodModel(model));

        //заполняем по умолчанию пустые селекты
        model.setDetalizationProfile(false);
        model.setEnrollmentCampaignStage(null);
        model.setCompensationType(null);
        model.setStudentCategoryList(new ArrayList<>());
        model.setQualificationList(new ArrayList<>());
        model.setFormativeOrgUnitList(new ArrayList<>());
        model.setTerritorialOrgUnitList(new ArrayList<>());
        model.setEducationLevelHighSchoolList(new ArrayList<>());
        model.setDevelopFormList(new ArrayList<>());
        model.setDevelopConditionList(new ArrayList<>());
        model.setDevelopTechList(new ArrayList<>());
        model.setDevelopPeriodList(new ArrayList<>());
        model.setExcludeParallel(null);

        //запллняем активность полей по умолчанию
        model.setStudentCategoryActive(false);
        model.setQualificationActive(false);
        model.setFormativeOrgUnitActive(false);
        model.setTerritorialOrgUnitActive(false);
        model.setEducationLevelHighSchoolActive(false);
        model.setDevelopFormActive(false);
        model.setDevelopConditionActive(false);
        model.setDevelopTechActive(false);
        model.setDevelopPeriodActive(false);
        model.setExcludeParallelActive(false);

        //определяем обязательность полей для модели утили
        model.setParameters(new MultiEnrollmentDirectionUtil.Parameters());
        model.getParameters().setRequired(false, MultiEnrollmentDirectionUtil.Parameters.Filter.values());

        return model;
    }

    @Override
    public <T extends EnrollmentDirProfReportModel> EnrollmentDirProfReport createReport(T model, DatabaseFile reportFile)
    {
        EnrollmentDirProfReport report = new EnrollmentDirProfReport();
        report.setContent(reportFile);
        report.setFormingDate(new Date());
        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setRequestDateFrom(model.getRequestFrom());
        report.setRequestDateTo(model.getRequestTo());
        report.setEnrollmentCampaignStepText(model.getEnrollmentCampaignStage().getTitle());
        report.setCompensationTypeText(model.getCompensationType().getTitle());
        if (model.isStudentCategoryActive())
            report.setStudentCategoryText(UniStringUtils.join(model.getStudentCategoryList(), StudentCategory.title().s(), "; "));
        if (model.isQualificationActive())
            report.setQualificationText(UniStringUtils.join(model.getQualificationList(), Qualifications.title().s(), "; "));
        if (model.isFormativeOrgUnitActive())
            report.setFormativeOrgUnitText(UniStringUtils.join(model.getFormativeOrgUnitList(), OrgUnit.title().s(), "; "));
        if (model.isTerritorialOrgUnitActive())
            report.setTerritorialOrgUnitText(UniStringUtils.join(model.getTerritorialOrgUnitList(), OrgUnit.title().s(), "; "));
        if (model.isEducationLevelHighSchoolActive())
            report.setEducationLevelHighSchoolText(UniStringUtils.join(model.getEducationLevelHighSchoolList(), EducationLevelsHighSchool.title().s(), "; "));
        if (model.isDevelopFormActive())
            report.setDevelopFormText(UniStringUtils.join(model.getDevelopFormList(), DevelopForm.title().s(), "; "));
        if (model.isDevelopConditionActive())
            report.setDevelopConditionText(UniStringUtils.join(model.getDevelopConditionList(), DevelopCondition.title().s(), "; "));
        if (model.isDevelopTechActive())
            report.setDevelopTechText(UniStringUtils.join(model.getDevelopTechList(), DevelopTech.title().s(), "; "));
        if (model.isDevelopPeriodActive())
            report.setDevelopPeriodText(UniStringUtils.join(model.getDevelopPeriodList(), DevelopPeriod.title().s(), "; "));
        if (model.isExcludeParallelActive())
            report.setExcludeParallel(TwinComboDataSourceHandler.getSelectedValueNotNull(model.getExcludeParallel()));

        return report;
    }

    @Override
    public <T extends EnrollmentDirProfReportModel> DatabaseFile createPrintReportFile(T model) throws IOException, WriteException
    {
        prepareReportModel(model);

        validate(model);

        // создаем печатную форму
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        WritableWorkbook workbook = Workbook.createWorkbook(out, ws);

        // создаем лист
        WritableSheet sheet = workbook.createSheet("Отчет", 0);

        // заголовок
        writeHeader(sheet, model);

        // таблица
        writeTable(sheet, model);

        // детализация профилей, если выбран флаг
        if (model.isDetalizationProfile())
            writeDetalizationProfile(workbook, model);

        workbook.write();
        workbook.close();

        // создаем файл
        DatabaseFile databaseFile = new DatabaseFile();
        databaseFile.setContent(out.toByteArray());
        databaseFile.setFilename("Report");
        databaseFile.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_VND_MS_EXCEL);

        return databaseFile;
    }

    /**
     * Производит необходимые прверки, перед печатью.<p/>
     * Если есть ошибки, то кидается <code>ApplicationException</code>.
     * @param model модель
     */
    protected <M extends EnrollmentDirProfReportModel> void validate(M model)
    {
        // лимит листов, если не указан, то значение по умолчанию
        int limit = ApplicationRuntime.getProperty("uniec.enrollmentDirProfReport.profileLimit") == null ? 50 : Integer.parseInt(ApplicationRuntime.getProperty("uniec.enrollmentDirProfReport.profileLimit"));
        if (model.getProfilePriorityListMap().size() > limit && model.isDetalizationProfile())
            throw new ApplicationException("Отчет не может быть построен, т.к. число попавших в статистику отчета профилей превышает " + limit + ". Выберите дополнительные параметры для получения меньшего числа профилей.");
    }

    /**
     * Подготавливает данные необходимые для печати отчета.
     * @param model модель
     * @return модель
     */
    @SuppressWarnings("unchecked")
    protected <M extends EnrollmentDirProfReportModel> M prepareReportModel(M model)
    {
        // обнуляем поля
        model.setPriorityColumnNumber(0);
        model.setDirectionProfileListMap(new HashMap<>());
        model.setEducationLevelsDirectionListMap(new HashMap<>());
        model.setDirectionRequestedDirectionListMap(new HashMap<>());
        model.setProfilePriorityListMap(new HashMap<>());

        // поднимаем НП Приема
        DQLSelectBuilder directionBuilder = new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, "d").column("d");
        directionBuilder.where(DQLExpressions.in(DQLExpressions.property(EnrollmentDirection.id().fromAlias("d")), ids(MultiEnrollmentDirectionUtil.getEnrollmentDirections(model, getSession()))));
        if (model.getCompensationType().isBudget())
            directionBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(EnrollmentDirection.budget().fromAlias("d")), true));
        else
            directionBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(EnrollmentDirection.contract().fromAlias("d")), true));
        if (model.isQualificationActive())
            directionBuilder.where(DQLExpressions.in(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().fromAlias("d")), model.getQualificationList()));

        List<EnrollmentDirection> enrollmentDirectionList = directionBuilder.createStatement(getSession()).list();

        // поднимаем Профили НП Приема
        DQLSelectBuilder profileBuilder = new DQLSelectBuilder().fromEntity(ProfileEducationOrgUnit.class, "p").column("p");
        profileBuilder.where(DQLExpressions.in(DQLExpressions.property(ProfileEducationOrgUnit.enrollmentDirection().fromAlias("p")), enrollmentDirectionList));
        profileBuilder.order(DQLExpressions.property(ProfileEducationOrgUnit.educationOrgUnit().territorialOrgUnit().title().fromAlias("p")), OrderDirection.asc);
        List<ProfileEducationOrgUnit> profileList = profileBuilder.createStatement(getSession()).list();

        // заполняем мапу группирующую НП Приема по НП(базов.)
        Map<EducationLevels, List<EnrollmentDirection>> educationLevelsDirectionListMap = model.getEducationLevelsDirectionListMap();
        for (EnrollmentDirection enrollmentDirection : enrollmentDirectionList)
        {
            // Толик: укрупненная группа находится как элемент, у которого нет parent
            EducationLevels level = enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();
            while (null != level.getParentLevel()) { level = level.getParentLevel(); }

            List<EnrollmentDirection> directionList = educationLevelsDirectionListMap.get(level);
            if (directionList == null) { educationLevelsDirectionListMap.put(level, directionList = new ArrayList<>()); }
            directionList.add(enrollmentDirection);
        }

        // сортируем мапу и ее списки
        Comparator<EnrollmentDirection> directionComparator = (o1, o2) -> o1.getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitle().compareTo(o2.getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitle());

        for (Map.Entry<EducationLevels, List<EnrollmentDirection>> entry : educationLevelsDirectionListMap.entrySet()) {
            Collections.sort(entry.getValue(), directionComparator);
        }

        // заполняем мапу группирующую Профили НП Приема по НП Приема
        Map<EnrollmentDirection, List<ProfileEducationOrgUnit>> directionProfileListMap = model.getDirectionProfileListMap();
        for (ProfileEducationOrgUnit profile : profileList)
        {
            List<ProfileEducationOrgUnit> profList = directionProfileListMap.get(profile.getEnrollmentDirection());
            if (profList == null) { directionProfileListMap.put(profile.getEnrollmentDirection(), profList = new ArrayList<>()); }
            profList.add(profile);
        }

        // вычесляем число колнок с приоритетами
        for (Map.Entry<EnrollmentDirection, List<ProfileEducationOrgUnit>> entry : directionProfileListMap.entrySet())
            model.setPriorityColumnNumber(model.getPriorityColumnNumber() > entry.getValue().size() ? model.getPriorityColumnNumber() : entry.getValue().size());

        // поднимаем ВНП. в зависимости от выбранной Стадии ПК ВНП поднимаеются по разному
        final MQBuilder directionMqBuilder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "b");
        directionMqBuilder.add(MQExpression.in("b", RequestedEnrollmentDirection.enrollmentDirection(), enrollmentDirectionList));
        directionMqBuilder.add(MQExpression.eq("b", RequestedEnrollmentDirection.enrollmentDirection().enrollmentCampaign(), model.getEnrollmentCampaign())); // в указанной приемной кампании
        directionMqBuilder.add(MQExpression.notEq("b", RequestedEnrollmentDirection.entrantRequest().entrant().archival(), Boolean.TRUE)); // не архивные
        directionMqBuilder.add(MQExpression.lessOrEq("b", RequestedEnrollmentDirection.entrantRequest().regDate(), getRequestTo(model.getRequestTo())));
        directionMqBuilder.add(MQExpression.greatOrEq("b", RequestedEnrollmentDirection.entrantRequest().regDate(), model.getRequestFrom()));

        if (model.getStudentCategoryList() != null && !model.getStudentCategoryList().isEmpty())
            directionMqBuilder.add(MQExpression.in("b", RequestedEnrollmentDirection.studentCategory().id(), ids(model.getStudentCategoryList())));

        if (model.getEnrollmentCampaignStage().getId().equals(EnrollmentDirProfReportModel.ON_PROCESS_RECEPTION))
        {
            directionMqBuilder.add(MQExpression.notIn("b", RequestedEnrollmentDirection.state().code(), EntrantStateCodes.TAKE_DOCUMENTS_AWAY));
            directionMqBuilder.add(MQExpression.eq("b", RequestedEnrollmentDirection.compensationType(), model.getCompensationType())); // по указанному возмещению затрат
        }
        else if (model.getEnrollmentCampaignStage().getId().equals(EnrollmentDirProfReportModel.ON_RESULT_TEST))
        {
            directionMqBuilder.add(MQExpression.notIn("b", RequestedEnrollmentDirection.state().code(), EntrantStateCodes.TAKE_DOCUMENTS_AWAY, EntrantStateCodes.OUT_OF_COMPETITION, EntrantStateCodes.ACTIVE));
            directionMqBuilder.add(MQExpression.eq("b", RequestedEnrollmentDirection.compensationType(), model.getCompensationType())); // по указанному возмещению затрат
        }
        else if (model.getEnrollmentCampaignStage().getId().equals(EnrollmentDirProfReportModel.ON_RESULT_ENROLLMENT))
        {
            MQBuilder subBuilder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "pes", new String[] { PreliminaryEnrollmentStudent.requestedEnrollmentDirection().id().s() });
            subBuilder.add(MQExpression.eq("pes", PreliminaryEnrollmentStudent.compensationType(), model.getCompensationType())); // по указанному возмещению затрат (!из студента предзачисления!)

            if (model.isExcludeParallelActive())
                if (TwinComboDataSourceHandler.getSelectedValueNotNull(model.getExcludeParallel()))
                    subBuilder.add(MQExpression.notEq("pes", PreliminaryEnrollmentStudent.parallel(), Boolean.TRUE));

            directionMqBuilder.add(MQExpression.in("b", RequestedEnrollmentDirection.state().code(), EntrantStateCodes.PRELIMENARY_ENROLLED, EntrantStateCodes.IN_ORDER, EntrantStateCodes.ENROLED));
            directionMqBuilder.add(MQExpression.in("b", "id", subBuilder)); // должны быть подходящие студенты предзачисления
        }
        else
        {
            throw new IllegalStateException("unexpected stage: " + model.getEnrollmentCampaignStage().getId());
        }

        // подготавливаем EntrantDataUtil, она понадобится для подсчета Финальной суммы баллов по ВНП
        EntrantDataUtil dataUtil = new EntrantDataUtil(getSession(), model.getEnrollmentCampaign(), directionMqBuilder);
        if (dataUtil.getDirectionSet().isEmpty()) {
            throw new ApplicationException("В статистику отчета не попал ни один абитуриент. Попробуйте выбрать другие параметры отчета.");
        }

        model.setDataUtil(dataUtil);


        // полнимаем Приоритеты Профилей НП
        final Map<EnrollmentDirection, List<RequestedEnrollmentDirection>> directionRequestedDirectionListMap = model.getDirectionRequestedDirectionListMap();
        final Map<ProfileEducationOrgUnit, List<PriorityProfileEduOu>> profilePriorityListMap = model.getProfilePriorityListMap();

        for (List<RequestedEnrollmentDirection> elements : Iterables.partition(dataUtil.getDirectionSet(), 512)) {

            // заполняем мапу по ВНП
            for (RequestedEnrollmentDirection direction : elements) {
                List<RequestedEnrollmentDirection> directionList = directionRequestedDirectionListMap.get(direction.getEnrollmentDirection());
                if (directionList == null) { directionRequestedDirectionListMap.put(direction.getEnrollmentDirection(), directionList = new ArrayList<>()); }
                directionList.add(direction);
            }

            // заполняем мапу по профилям ВНП
            for (PriorityProfileEduOu priority : getList(PriorityProfileEduOu.class, PriorityProfileEduOu.requestedEnrollmentDirection(), elements)) {
                List<PriorityProfileEduOu> priorityList = profilePriorityListMap.get(priority.getProfileEducationOrgUnit());
                if (priorityList == null) { profilePriorityListMap.put(priority.getProfileEducationOrgUnit(), priorityList = new ArrayList<>()); }
                priorityList.add(priority);
            }
        }
        return model;
    }

    /**
     * Рисует шапку отчета.
     * @param sheet лист
     * @return лист
     * @throws WriteException
     */
    protected <T extends EnrollmentDirProfReportModel> WritableSheet writeHeader(WritableSheet sheet, T model) throws WriteException
    {
        // подготавливаем форматы шрифта
        WritableFont arial8 = new WritableFont(WritableFont.ARIAL, 8);
        WritableFont arial10Bold = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);

        // подготавливаем формат ячейки
        WritableCellFormat arial10BoldFormat = new WritableCellFormat(arial10Bold);
        WritableCellFormat arial8Format = new WritableCellFormat(arial8);

        //рисуем строки
        sheet.addCell(new Label(0, 0, TopOrgUnit.getInstance().getPrintTitle(), arial10BoldFormat));
        sheet.addCell(new Label(0, 1, "Информация приемной комиссии c " + DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getRequestFrom()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getRequestTo()), arial10BoldFormat));
        sheet.addCell(new Label(0, 2, "Статистика по приему (" + model.getEnrollmentCampaignStage().getTitle() + ")", arial10BoldFormat));

        //рисуем поля с фильтрами
        StringBuilder filtersBuilder = new StringBuilder();
        filtersBuilder.append("Приемная кампания: ").append(model.getEnrollmentCampaign().getTitle()).append("; ");
        filtersBuilder.append("Стадия приемной кампании: ").append(model.getEnrollmentCampaignStage().getTitle()).append("; ");
        filtersBuilder.append("Вид возмещения затрат: ").append(model.getCompensationType().getTitle()).append("; ");
        if (model.getStudentCategoryList() != null && !model.getStudentCategoryList().isEmpty())
            filtersBuilder.append("Категория поступающего: ").append(StringUtils.join(getTitleList(model.getStudentCategoryList()), ", ")).append("; ");
        if (model.getQualificationList() != null && !model.getQualificationList().isEmpty())
            filtersBuilder.append("Квалификация: ").append(StringUtils.join(getTitleList(model.getQualificationList()), ", ")).append("; ");
        if (model.getSelectedFormativeOrgUnitList() != null)
            filtersBuilder.append("Формирующее подразделение: ").append(StringUtils.join(getTitleList(model.getSelectedFormativeOrgUnitList()), ", ")).append("; ");
        if (model.getSelectedTerritorialOrgUnitList() != null)
            filtersBuilder.append("Территориальное подразделение: ").append(StringUtils.join(getTerrTitleList(model.getSelectedTerritorialOrgUnitList()), ", ")).append("; ");
        if (model.getEducationLevelHighSchoolList() != null && !model.getEducationLevelHighSchoolList().isEmpty())
            filtersBuilder.append("Направление подготовки (специальность): ").append(StringUtils.join(getTitleList(model.getEducationLevelHighSchoolList()), ", ")).append("; ");
        if (model.getSelectedDevelopFormList() != null)
            filtersBuilder.append("Форма обучения: ").append(StringUtils.join(getTitleList(model.getSelectedDevelopFormList()), ", ")).append("; ");
        if (model.getSelectedDevelopConditionList() != null)
            filtersBuilder.append("Условие обучения: ").append(StringUtils.join(getTitleList(model.getSelectedDevelopConditionList()), ", ")).append("; ");
        if (model.getSelectedDevelopTechList() != null)
            filtersBuilder.append("Технология освоения: ").append(StringUtils.join(getTitleList(model.getSelectedDevelopTechList()), ", ")).append("; ");
        if (model.getSelectedDevelopPeriodList() != null)
            filtersBuilder.append("Срок освоения: ").append(StringUtils.join(getTitleList(model.getSelectedDevelopPeriodList()), ", ")).append("; ");
        if (model.getExcludeParallel() != null)
            filtersBuilder.append("Исключая поступивших на параллельное освоение образовательных программ: ").append(model.getExcludeParallel().getTitle()).append("; ");

        sheet.addCell(new Label(0, 4, filtersBuilder.toString(), arial8Format));

        return sheet;
    }

    /**
     * Рисует основную таблицу отчета. С 8 строки.
     * @param sheet лист
     * @param model объект агрегирующий необходимые для печати отчета данные
     * @return лист
     * @throws WriteException
     */
    protected <M extends EnrollmentDirProfReportModel> WritableSheet writeTable(WritableSheet sheet, M model) throws WriteException
    {
        writeTableHeader(sheet, model.getPriorityColumnNumber());

        // подготавливаем форматы шрифта
        WritableFont arial10 = new WritableFont(WritableFont.ARIAL, 10);
        WritableFont arial10Bold = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
        WritableFont arial10BoldGrey = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.GREY_50_PERCENT);

        // подготавливаем формат ячейки
        WritableCellFormat arial10Format = new WritableCellFormat(arial10);
        WritableCellFormat arial10BoldFormat = new WritableCellFormat(arial10Bold);
        WritableCellFormat arial10BoldCFormat = new WritableCellFormat(arial10Bold);
        WritableCellFormat arial10BoldGreyFormat = new WritableCellFormat(arial10BoldGrey);
        arial10Format.setBorder(Border.ALL, BorderLineStyle.THIN);
        arial10BoldFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
        arial10BoldCFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
        arial10BoldGreyFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
        arial10Format.setWrap(true);
        arial10BoldFormat.setWrap(true);
        arial10BoldCFormat.setWrap(true);
        arial10BoldGreyFormat.setWrap(true);
        arial10BoldGreyFormat.setAlignment(Alignment.CENTRE);
        arial10BoldGreyFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        arial10BoldCFormat.setAlignment(Alignment.CENTRE);
        arial10BoldCFormat.setVerticalAlignment(VerticalAlignment.CENTRE);

        int row = 9;
        List<Integer> directionRowList = new LinkedList<>();

        // рисуем основную таблицу
        // бежим по НП(базов.)
        for (Map.Entry<EducationLevels, List<EnrollmentDirection>> directionEntry : model.getEducationLevelsDirectionListMap().entrySet())
        {
            writeEducationLine(row, sheet, arial10BoldGreyFormat, directionEntry.getKey(), directionEntry.getValue(), model);
            row++;

            // бежим по НП Приема, относящихся к отрисованному НП(базов.)
            for (EnrollmentDirection direction : directionEntry.getValue())
            {
                directionRowList.add(row);
                writeDirectionLine(row, sheet, arial10BoldFormat, direction, model);
                row++;

                List<ProfileEducationOrgUnit> profileList = model.getDirectionProfileListMap().get(direction);

                // если есть Профили у отрисованного НП Приема
                if (profileList == null)
                    continue;

                // бежим по Профилям отрисованного НП Приема
                for (ProfileEducationOrgUnit profile : profileList)
                {
                    writeProfileLine(row, sheet, arial10Format, profile, model);
                    row++;
                }
            }
        }

        writeTotalLine(row, sheet, arial10BoldCFormat, directionRowList);

        return sheet;
    }

    /**
     * Рисует заголовок таблицы.
     * @param sheet лист
     * @param priorityColumnNumber количество колонок с приоритетами
     * @return лист
     * @throws WriteException
     */
    protected WritableSheet writeTableHeader(WritableSheet sheet, int priorityColumnNumber) throws WriteException
    {
        WritableFont arial8Bold = new WritableFont(WritableFont.ARIAL, 8, WritableFont.BOLD);
        WritableCellFormat arial8BoldFormat = new WritableCellFormat(arial8Bold);
        arial8BoldFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
        arial8BoldFormat.setAlignment(Alignment.CENTRE);
        arial8BoldFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        arial8BoldFormat.setWrap(true);

        CellView view = new CellView();
        view.setSize(40 * 256);
        sheet.setColumnView(0, view);

        sheet.addCell(new Label(0, 7, "Направление подготовки (специальность)", arial8BoldFormat));
        sheet.mergeCells(0, 7, 0, 8);
        sheet.addCell(new Label(1, 7, "План", arial8BoldFormat));
        sheet.mergeCells(1, 7, 1, 8);
        sheet.addCell(new Label(2, 7, "Всего", arial8BoldFormat));
        sheet.mergeCells(2, 7, 2, 8);

        if (priorityColumnNumber > 1)
        {
            sheet.addCell(new Label(3, 7, "Распределение по приоритетам", arial8BoldFormat));
            sheet.mergeCells(3, 7, 3 + priorityColumnNumber - 1, 7);

            for (int i = 0; i < priorityColumnNumber; i++)
                sheet.addCell(new Number(3 + i, 8, (double) (i + 1), arial8BoldFormat));
        }

        return sheet;
    }

    /**
     * Рисует строку с укрупненными группами.
     * @param row номер строки
     * @param sheet лист
     * @param format формат ячейки
     * @param educationLevels НП (базов.)
     * @param directionList список НППриема, которые зависят от указанного НП (базов.)
     * @param model модел
     * @return лист
     * @throws WriteException
     */
    protected <M extends EnrollmentDirProfReportModel> WritableSheet writeEducationLine(int row, WritableSheet sheet, CellFormat format, EducationLevels educationLevels, List<EnrollmentDirection> directionList, M model) throws WriteException
    {
        int priorityColumnNumber = model.getPriorityColumnNumber();
        boolean budget = model.getCompensationType().isBudget();
        Map<EnrollmentDirection, List<RequestedEnrollmentDirection>> directionRequetsListMap = model.getDirectionRequestedDirectionListMap();

        sheet.addCell(new Label(0, row, educationLevels.getFullTitleWithRootLevel(), format));

        // вычесляем колонки План и Всего
        // План - значение из НП Приема
        // Всего - количество всех ВНП, относящихся к данному НП(базов.)
        int plan = 0;
        int total = 0;
        for (EnrollmentDirection direction : directionList)
        {
            plan += budget ? UniBaseUtils.nullToZero(direction.getMinisterialPlan()) : UniBaseUtils.nullToZero(direction.getContractPlan());

            List<RequestedEnrollmentDirection> directList = directionRequetsListMap.get(direction);
            total += directList != null ? directList.size() : 0;
        }

        sheet.addCell(new Number(1, row, plan, format));
        sheet.addCell(new Number(2, row, total, format));
        for (int i = 0; i < priorityColumnNumber; i++)
            sheet.addCell(new Blank(3 + i, row, format));

        return sheet;
    }

    /**
     * Рисует строку с НППриема.
     * @param row номер строки
     * @param sheet лист
     * @param format формат ячейки
     * @param direction НППриема
     * @param model модель
     * @return лист
     * @throws WriteException
     */
    protected <M extends EnrollmentDirProfReportModel> WritableSheet writeDirectionLine(int row, WritableSheet sheet, CellFormat format, EnrollmentDirection direction, M model) throws WriteException
    {
        boolean budget = model.getCompensationType().isBudget();
        List<RequestedEnrollmentDirection> requestDirectionList = model.getDirectionRequestedDirectionListMap().get(direction);
        int priorityColumnNumber = model.getPriorityColumnNumber();

        sheet.addCell(new Label(0, row, direction.getPrintTitle(), format));
        sheet.addCell(JExcelUtil.getNumberNullableCell(1, row, budget ? direction.getMinisterialPlan() : direction.getContractPlan(), format));
        sheet.addCell(JExcelUtil.getNumberNullableCell(2, row, requestDirectionList != null ? requestDirectionList.size() : null, format));

        Integer[] directionPriority = new Integer[priorityColumnNumber];

        // вычесляем колонки с приоритетами
        // значение - количество ВНП с определенным приоритетом
        if (requestDirectionList != null)
            for (RequestedEnrollmentDirection requestDirection : requestDirectionList)
            {
                int priority = requestDirection.getPriority() - 1;
                if (priority <= directionPriority.length - 1)
                    directionPriority[priority] = directionPriority[priority] != null ? directionPriority[priority] + 1 : 1;
            }
        for (int i = 0; i < priorityColumnNumber; i++)
            sheet.addCell(JExcelUtil.getNumberNullableCell(3 + i, row, directionPriority[i], format));

        return sheet;
    }

    /**
     * Рисует строку с Профилями НПП.
     * @param row строка
     * @param sheet лист
     * @param format формат ячейки
     * @param profile Профиль НПП
     * @param model модел
     * @return лист
     * @throws WriteException
     */
    protected <M extends EnrollmentDirProfReportModel> WritableSheet writeProfileLine(int row, WritableSheet sheet, CellFormat format, ProfileEducationOrgUnit profile, M model) throws WriteException
    {
        boolean budget = model.getCompensationType().isBudget();
        int priorityColumnNumber = model.getPriorityColumnNumber();
        List<PriorityProfileEduOu> priorityList = model.getProfilePriorityListMap().get(profile);

        sheet.addCell(new Label(0, row, profile.getEducationOrgUnit().getEducationLevelHighSchool().getTitle(), format));
        sheet.addCell(JExcelUtil.getNumberNullableCell(1, row, budget ? profile.getBudgetPlan() : profile.getContractPlan(), format));
        sheet.addCell(JExcelUtil.getNumberNullableCell(2, row, priorityList != null ? priorityList.size() : null, format));

        Integer[] profilePriority = new Integer[priorityColumnNumber];

        // вычесляем колонки приоритетов
        // значения - количество Выбранных Приоритетов НП Приема, относящихся к данному Профилю НП Приема
        if (priorityList != null)
            for (PriorityProfileEduOu priorityProfile : priorityList)
            {
                int priority = priorityProfile.getPriority() - 1;
                if (priority <= profilePriority.length - 1)
                    profilePriority[priority] = profilePriority[priority] != null ? profilePriority[priority] + 1 : 1;
            }
        for (int i = 0; i < priorityColumnNumber; i++)
            sheet.addCell(JExcelUtil.getNumberNullableCell(3 + i, row, profilePriority[i], format));

        return sheet;
    }

    /**
     * Добовляет и рисует листы с детализацией Профилей НПП.
     * @param workbook книга документа
     * @param model модель
     * @return кинга
     * @throws WriteException
     */
    protected <M extends EnrollmentDirProfReportModel> WritableWorkbook writeDetalizationProfile(WritableWorkbook workbook, M model) throws WriteException
    {
        // индекс листов
        int index = 1;
        for (ProfileEducationOrgUnit profile : model.getProfilePriorityListMap().keySet())
        {
            // создаем лист для Профиля
            String sheetTitle = profile.getEducationOrgUnit().getEducationLevelHighSchool().getDisplayableShortTitle().length() > 23 ? profile.getEducationOrgUnit().getEducationLevelHighSchool().getDisplayableShortTitle().substring(1, 23) : profile.getEducationOrgUnit().getEducationLevelHighSchool().getDisplayableShortTitle();
            sheetTitle += " - л. " + index;
            WritableSheet sheet = workbook.createSheet(sheetTitle, index++);

            writeDetalizationProfileSheet(sheet, model, profile);
        }

        return workbook;
    }

    /**
     * Рисует лист детализации Профиля НПП.<p/>
     * Заполняется шапка и таблица с данными по ВНП указанного профиля.
     * @param sheet лист
     * @param model модель
     * @param profile Профиль НПП
     * @return лист
     * @throws WriteException
     */
    protected <M extends EnrollmentDirProfReportModel> WritableSheet writeDetalizationProfileSheet(WritableSheet sheet, M model, ProfileEducationOrgUnit profile) throws WriteException
    {
        // 1. шапка листа
        // подготавливаем форматы шрифта
        WritableFont arial10 = new WritableFont(WritableFont.ARIAL, 10);
        WritableFont arial10T = new WritableFont(WritableFont.ARIAL, 10);
        WritableFont arial10Bold = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
        WritableFont arial10BoldTH = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);

        // подготавливаем формат ячейки
        WritableCellFormat arial10Format = new WritableCellFormat(arial10);
        WritableCellFormat arial10BoldFormat = new WritableCellFormat(arial10Bold);
        WritableCellFormat arial10FormatT = new WritableCellFormat(arial10T);
        arial10FormatT.setBorder(Border.ALL, BorderLineStyle.THIN);
        WritableCellFormat arial10BoldFormatTH = new WritableCellFormat(arial10BoldTH);
        arial10BoldFormatTH.setBorder(Border.ALL, BorderLineStyle.THIN);
        arial10BoldFormatTH.setAlignment(Alignment.CENTRE);
        arial10BoldFormatTH.setVerticalAlignment(VerticalAlignment.CENTRE);
        arial10BoldFormatTH.setWrap(true);

        //рисуем строки
        sheet.addCell(new Label(0, 0, TopOrgUnit.getInstance().getPrintTitle(), arial10BoldFormat));
        sheet.addCell(new Label(0, 1, "Информация приемной комиссии c " + DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getRequestFrom()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getRequestTo()), arial10BoldFormat));
        sheet.addCell(new Label(0, 2, "Статистика по приему (" + model.getEnrollmentCampaignStage().getTitle() + ")", arial10BoldFormat));
        sheet.addCell(new Label(0, 3, "по профилю: " + profile.getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitle() + " (" + DevelopUtil.getTitle(profile.getEducationOrgUnit().getDevelopForm(), profile.getEducationOrgUnit().getDevelopCondition(), profile.getEducationOrgUnit().getDevelopTech(), profile.getEducationOrgUnit().getDevelopPeriod()) + ")", arial10Format));

        // 2. шапка таблицы
        boolean useIndProgress = model.getEnrollmentCampaign().isUseIndividualProgressInAmountMark();
        String[] columnTitles;
        if (useIndProgress)
            columnTitles = new String[]{"№ п.п", " № заявления", "Фамилия Имя Отчество", "Сданы документы", "Вид конкурса", "Баллы за инд.дост.", "Сумма баллов", "Приоритет профиля"};
        else
         columnTitles = new String[]{"№ п.п", " № заявления", "Фамилия Имя Отчество", "Сданы документы", "Вид конкурса", "Сумма баллов", "Приоритет профиля"};

        CellView view = new CellView();
        view.setSize(columnTitles[0].length() * 256);
        sheet.setColumnView(0, view);
        sheet.addCell(new Label(0, 5, columnTitles[0], arial10BoldFormatTH));
        view.setSize(12 * 256);
        sheet.setColumnView(1, view);
        sheet.addCell(new Label(1, 5, columnTitles[1], arial10BoldFormatTH));
        view.setSize(40 * 256);
        sheet.setColumnView(2, view);
        sheet.addCell(new Label(2, 5, columnTitles[2], arial10BoldFormatTH));
        view.setSize(12 * 256);
        sheet.setColumnView(3, view);
        sheet.addCell(new Label(3, 5, columnTitles[3], arial10BoldFormatTH));
        view.setSize(20 * 256);
        sheet.setColumnView(4, view);
        sheet.addCell(new Label(4, 5, columnTitles[4], arial10BoldFormatTH));
        view.setSize(10 * 256);
        sheet.setColumnView(5, view);
        sheet.addCell(new Label(5, 5, columnTitles[5], arial10BoldFormatTH));

        //Колонка индивид. достижений
        if (!useIndProgress)
        {
            view.setSize(12 * 256);
            sheet.setColumnView(6, view);
            sheet.addCell(new Label(6, 5, columnTitles[6], arial10BoldFormatTH));
        }
        else
        {
            view.setSize(10 * 256);
            sheet.setColumnView(6, view);
            sheet.addCell(new Label(6, 5, columnTitles[6], arial10BoldFormatTH));
            view.setSize(12 * 256);
            sheet.setColumnView(7, view);
            sheet.addCell(new Label(7, 5, columnTitles[7], arial10BoldFormatTH));
        }


        EntrantDataUtil dataUtil = model.getDataUtil();
        // 3. таблица
        int number = 1;
        int row = 6;
        // берем Приоритет Профилей НП, которые есть по данному Профилю
        // по Приоритетам ПНП строим список ВНП
        List<RequestedEnrollmentDirection> directionList = model.getProfilePriorityListMap().containsKey(profile)
                ? CommonBaseEntityUtil.<RequestedEnrollmentDirection>getPropertiesList(model.getProfilePriorityListMap().get(profile), PriorityProfileEduOu.requestedEnrollmentDirection().s())
                : Collections.<RequestedEnrollmentDirection>emptyList();
        // по ВНП создаем список враперов строк, необходимо для сортировки
        List<EnrollmentDirProfReportModel.DirectionWrapper> directionWrapperList = new LinkedList<>();
        for (RequestedEnrollmentDirection direction : directionList)
            directionWrapperList.add(new EnrollmentDirProfReportModel.DirectionWrapper(direction, dataUtil));
        // ранжируем ВНП
        Collections.sort(directionWrapperList, new RequestedEnrollmentDirectionComparator(UniecDAOFacade.getSettingsDAO().getCompetitionKindPriorities(model.getEnrollmentCampaign())));
        // рисуем
        for (EnrollmentDirProfReportModel.DirectionWrapper directionWrapper : directionWrapperList)
        {
            sheet.addCell(new Number(0, row, number++, arial10FormatT));
            sheet.addCell(new Number(1, row, directionWrapper.getRequestedEnrollmentDirection().getNumber(), arial10FormatT));
            sheet.addCell(new Label(2, row, directionWrapper.getFio(), arial10FormatT));
            sheet.addCell(new Label(3, row, directionWrapper.getRequestedEnrollmentDirection().isOriginalDocumentHandedIn() ? "оригиналы" : "копии", arial10FormatT));
            sheet.addCell(new Label(4, row, directionWrapper.getRequestedEnrollmentDirection().getCompetitionKind().getTitle().toLowerCase(), arial10FormatT));

            if (!useIndProgress)
            {
                sheet.addCell(new Number(5, row, directionWrapper.getFinalMark(), arial10FormatT));
                sheet.addCell(new Number(6, row++, getPriority(directionWrapper.getRequestedEnrollmentDirection(), model.getProfilePriorityListMap().get(profile)), arial10FormatT));
            }
            else
            {
                sheet.addCell(new Number(5, row, directionWrapper.getIndProgressMark(), arial10FormatT));
                sheet.addCell(new Number(6, row, directionWrapper.getFinalMark(), arial10FormatT));
                sheet.addCell(new Number(7, row++, getPriority(directionWrapper.getRequestedEnrollmentDirection(), model.getProfilePriorityListMap().get(profile)), arial10FormatT));
            }
        }

        return sheet;
    }

    /**
     * Рисует строку Итого.
     * Сумма по строкам НП Приема.
     * @param row строка
     * @param sheet лист
     * @param format формат ячейки
     * @param directionRowList список строк, в которых записи про НПП
     * @return лист
     * @throws WriteException
     */
    protected WritableSheet writeTotalLine(int row, WritableSheet sheet, CellFormat format, List<Integer> directionRowList) throws WriteException
    {
        sheet.addCell(new Label(0, row, "Итого по ОУ", format));
        sheet.addCell(new Formula(1, row, getSumFormula(directionRowList, 1), format));
        sheet.addCell(new Formula(2, row, getSumFormula(directionRowList, 2), format));

        return sheet;
    }

    /**
     * Получает по объектам список их Названий.
     * @param entityList список объектов с Названием
     * @return Список строк, содержащий Названия объектов
     */
    private List<String> getTitleList(List<? extends ITitled> entityList)
    {
        List<String> resultList = new LinkedList<>();
        for (ITitled entity : entityList)
            resultList.add(entity.getTitle());

        return resultList;
    }

    private List<String> getTerrTitleList(List<? extends OrgUnit> entityList)
    {
        List<String> resultList = new LinkedList<>();
        for (OrgUnit entity : entityList)
            resultList.add(entity.getTerritorialTitle());

        return resultList;
    }


    /**
     * Получаем строку с формулой суммы ячеек в указанных стоках, указанной колонки.
     * @param rows список строк
     * @param column колонка
     * @return Строку с формулой
     */
    private String getSumFormula(List<Integer> rows, int column)
    {
        StringBuffer buffer = new StringBuffer();
        for (Integer row : rows) {
            if (buffer.length() > 0) { buffer.append("+"); }
            CellReferenceHelper.getCellReference(column, row, buffer);
        }
        return buffer.toString();
    }

    private Date getRequestTo(Date date)
    {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTime();
    }

    private Integer getPriority(RequestedEnrollmentDirection direction, List<PriorityProfileEduOu> profileEducationOrgUnitList)
    {
        for (PriorityProfileEduOu profileEduOu: profileEducationOrgUnitList)
            if (profileEduOu.getRequestedEnrollmentDirection().equals(direction))
                return profileEduOu.getPriority();
        return null;
    }
}
