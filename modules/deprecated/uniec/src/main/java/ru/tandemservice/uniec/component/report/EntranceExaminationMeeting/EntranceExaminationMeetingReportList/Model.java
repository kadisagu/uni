/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.report.EntranceExaminationMeeting.EntranceExaminationMeetingReportList;

import java.util.List;

import org.tandemframework.core.component.State;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport;
import ru.tandemservice.uniec.util.report.UniecReportSecModel;

/**
 * @author agolubenko
 * @since 10.10.2008
 */
@State(keys = {"orgUnitId"}, bindings = {"orgUnitId"})
public class Model extends UniecReportSecModel implements IEnrollmentCampaignSelectModel
{
    private DynamicListDataSource<EntranceExaminationMeetingReport> _dataSource;
    private List<EnrollmentCampaign> _enrollmentCampaignList;
    private IDataSettings _settings;

    private Long _orgUnitId;
    private OrgUnit _orgUnit;

    @Override
    public String getViewKey()
    {
        return null == getOrgUnit() ? "viewEntranceExaminationMeetingReportList" : getSecModel().getPermission("viewEntranceExaminationMeetingReportList");
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return (EnrollmentCampaign) getSettings().get(ENROLLMENT_CAMPAIGN_FILTER_NAME);
    }
    
    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        getSettings().set(ENROLLMENT_CAMPAIGN_FILTER_NAME, enrollmentCampaign);
    }

    public void setDataSource(DynamicListDataSource<EntranceExaminationMeetingReport> dataSource)
    {
        _dataSource = dataSource;
    }

    public DynamicListDataSource<EntranceExaminationMeetingReport> getDataSource()
    {
        return _dataSource;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    @Override
    public IDataSettings getSettings()
    {
        return _settings;
    }

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    @Override
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }
}
