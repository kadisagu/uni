package ru.tandemservice.uniec.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.report.StorableReport;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.report.SummaryStateExamMarkReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сводка о средних баллах абитуриентов, поступающих по ЕГЭ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SummaryStateExamMarkReportGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.report.SummaryStateExamMarkReport";
    public static final String ENTITY_NAME = "summaryStateExamMarkReport";
    public static final int VERSION_HASH = -1845554654;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_REQUEST_DATE_FROM = "requestDateFrom";
    public static final String P_REQUEST_DATE_TO = "requestDateTo";
    public static final String P_COMPENSATION_TYPE_TEXT = "compensationTypeText";
    public static final String P_STUDENT_CATEGORY_TEXT = "studentCategoryText";
    public static final String P_QUALIFICATION_TEXT = "qualificationText";
    public static final String P_FORMATIVE_ORG_UNIT_TEXT = "formativeOrgUnitText";
    public static final String P_TERRITORIAL_ORG_UNIT_TEXT = "territorialOrgUnitText";
    public static final String P_DEVELOP_FORM_TEXT = "developFormText";
    public static final String P_DEVELOP_CONDITION_TEXT = "developConditionText";
    public static final String P_DEVELOP_TECH_TEXT = "developTechText";
    public static final String P_DEVELOP_PERIOD_TEXT = "developPeriodText";

    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private Date _requestDateFrom;     // Заявления с
    private Date _requestDateTo;     // Заявления по
    private String _compensationTypeText;     // Вид возмещения затрат
    private String _studentCategoryText;     // Категория поступающего
    private String _qualificationText;     // Квалификация
    private String _formativeOrgUnitText;     // Формирующее подразделение
    private String _territorialOrgUnitText;     // Территориальное подразделение
    private String _developFormText;     // Форма освоения
    private String _developConditionText;     // Условие освоения
    private String _developTechText;     // Технология освоения
    private String _developPeriodText;     // Срок освоения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     */
    @NotNull
    public Date getRequestDateFrom()
    {
        return _requestDateFrom;
    }

    /**
     * @param requestDateFrom Заявления с. Свойство не может быть null.
     */
    public void setRequestDateFrom(Date requestDateFrom)
    {
        dirty(_requestDateFrom, requestDateFrom);
        _requestDateFrom = requestDateFrom;
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     */
    @NotNull
    public Date getRequestDateTo()
    {
        return _requestDateTo;
    }

    /**
     * @param requestDateTo Заявления по. Свойство не может быть null.
     */
    public void setRequestDateTo(Date requestDateTo)
    {
        dirty(_requestDateTo, requestDateTo);
        _requestDateTo = requestDateTo;
    }

    /**
     * @return Вид возмещения затрат.
     */
    @Length(max=255)
    public String getCompensationTypeText()
    {
        return _compensationTypeText;
    }

    /**
     * @param compensationTypeText Вид возмещения затрат.
     */
    public void setCompensationTypeText(String compensationTypeText)
    {
        dirty(_compensationTypeText, compensationTypeText);
        _compensationTypeText = compensationTypeText;
    }

    /**
     * @return Категория поступающего.
     */
    public String getStudentCategoryText()
    {
        return _studentCategoryText;
    }

    /**
     * @param studentCategoryText Категория поступающего.
     */
    public void setStudentCategoryText(String studentCategoryText)
    {
        dirty(_studentCategoryText, studentCategoryText);
        _studentCategoryText = studentCategoryText;
    }

    /**
     * @return Квалификация.
     */
    public String getQualificationText()
    {
        return _qualificationText;
    }

    /**
     * @param qualificationText Квалификация.
     */
    public void setQualificationText(String qualificationText)
    {
        dirty(_qualificationText, qualificationText);
        _qualificationText = qualificationText;
    }

    /**
     * @return Формирующее подразделение.
     */
    public String getFormativeOrgUnitText()
    {
        return _formativeOrgUnitText;
    }

    /**
     * @param formativeOrgUnitText Формирующее подразделение.
     */
    public void setFormativeOrgUnitText(String formativeOrgUnitText)
    {
        dirty(_formativeOrgUnitText, formativeOrgUnitText);
        _formativeOrgUnitText = formativeOrgUnitText;
    }

    /**
     * @return Территориальное подразделение.
     */
    public String getTerritorialOrgUnitText()
    {
        return _territorialOrgUnitText;
    }

    /**
     * @param territorialOrgUnitText Территориальное подразделение.
     */
    public void setTerritorialOrgUnitText(String territorialOrgUnitText)
    {
        dirty(_territorialOrgUnitText, territorialOrgUnitText);
        _territorialOrgUnitText = territorialOrgUnitText;
    }

    /**
     * @return Форма освоения.
     */
    public String getDevelopFormText()
    {
        return _developFormText;
    }

    /**
     * @param developFormText Форма освоения.
     */
    public void setDevelopFormText(String developFormText)
    {
        dirty(_developFormText, developFormText);
        _developFormText = developFormText;
    }

    /**
     * @return Условие освоения.
     */
    public String getDevelopConditionText()
    {
        return _developConditionText;
    }

    /**
     * @param developConditionText Условие освоения.
     */
    public void setDevelopConditionText(String developConditionText)
    {
        dirty(_developConditionText, developConditionText);
        _developConditionText = developConditionText;
    }

    /**
     * @return Технология освоения.
     */
    public String getDevelopTechText()
    {
        return _developTechText;
    }

    /**
     * @param developTechText Технология освоения.
     */
    public void setDevelopTechText(String developTechText)
    {
        dirty(_developTechText, developTechText);
        _developTechText = developTechText;
    }

    /**
     * @return Срок освоения.
     */
    public String getDevelopPeriodText()
    {
        return _developPeriodText;
    }

    /**
     * @param developPeriodText Срок освоения.
     */
    public void setDevelopPeriodText(String developPeriodText)
    {
        dirty(_developPeriodText, developPeriodText);
        _developPeriodText = developPeriodText;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SummaryStateExamMarkReportGen)
        {
            setEnrollmentCampaign(((SummaryStateExamMarkReport)another).getEnrollmentCampaign());
            setRequestDateFrom(((SummaryStateExamMarkReport)another).getRequestDateFrom());
            setRequestDateTo(((SummaryStateExamMarkReport)another).getRequestDateTo());
            setCompensationTypeText(((SummaryStateExamMarkReport)another).getCompensationTypeText());
            setStudentCategoryText(((SummaryStateExamMarkReport)another).getStudentCategoryText());
            setQualificationText(((SummaryStateExamMarkReport)another).getQualificationText());
            setFormativeOrgUnitText(((SummaryStateExamMarkReport)another).getFormativeOrgUnitText());
            setTerritorialOrgUnitText(((SummaryStateExamMarkReport)another).getTerritorialOrgUnitText());
            setDevelopFormText(((SummaryStateExamMarkReport)another).getDevelopFormText());
            setDevelopConditionText(((SummaryStateExamMarkReport)another).getDevelopConditionText());
            setDevelopTechText(((SummaryStateExamMarkReport)another).getDevelopTechText());
            setDevelopPeriodText(((SummaryStateExamMarkReport)another).getDevelopPeriodText());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SummaryStateExamMarkReportGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SummaryStateExamMarkReport.class;
        }

        public T newInstance()
        {
            return (T) new SummaryStateExamMarkReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "requestDateFrom":
                    return obj.getRequestDateFrom();
                case "requestDateTo":
                    return obj.getRequestDateTo();
                case "compensationTypeText":
                    return obj.getCompensationTypeText();
                case "studentCategoryText":
                    return obj.getStudentCategoryText();
                case "qualificationText":
                    return obj.getQualificationText();
                case "formativeOrgUnitText":
                    return obj.getFormativeOrgUnitText();
                case "territorialOrgUnitText":
                    return obj.getTerritorialOrgUnitText();
                case "developFormText":
                    return obj.getDevelopFormText();
                case "developConditionText":
                    return obj.getDevelopConditionText();
                case "developTechText":
                    return obj.getDevelopTechText();
                case "developPeriodText":
                    return obj.getDevelopPeriodText();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "requestDateFrom":
                    obj.setRequestDateFrom((Date) value);
                    return;
                case "requestDateTo":
                    obj.setRequestDateTo((Date) value);
                    return;
                case "compensationTypeText":
                    obj.setCompensationTypeText((String) value);
                    return;
                case "studentCategoryText":
                    obj.setStudentCategoryText((String) value);
                    return;
                case "qualificationText":
                    obj.setQualificationText((String) value);
                    return;
                case "formativeOrgUnitText":
                    obj.setFormativeOrgUnitText((String) value);
                    return;
                case "territorialOrgUnitText":
                    obj.setTerritorialOrgUnitText((String) value);
                    return;
                case "developFormText":
                    obj.setDevelopFormText((String) value);
                    return;
                case "developConditionText":
                    obj.setDevelopConditionText((String) value);
                    return;
                case "developTechText":
                    obj.setDevelopTechText((String) value);
                    return;
                case "developPeriodText":
                    obj.setDevelopPeriodText((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                        return true;
                case "requestDateFrom":
                        return true;
                case "requestDateTo":
                        return true;
                case "compensationTypeText":
                        return true;
                case "studentCategoryText":
                        return true;
                case "qualificationText":
                        return true;
                case "formativeOrgUnitText":
                        return true;
                case "territorialOrgUnitText":
                        return true;
                case "developFormText":
                        return true;
                case "developConditionText":
                        return true;
                case "developTechText":
                        return true;
                case "developPeriodText":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return true;
                case "requestDateFrom":
                    return true;
                case "requestDateTo":
                    return true;
                case "compensationTypeText":
                    return true;
                case "studentCategoryText":
                    return true;
                case "qualificationText":
                    return true;
                case "formativeOrgUnitText":
                    return true;
                case "territorialOrgUnitText":
                    return true;
                case "developFormText":
                    return true;
                case "developConditionText":
                    return true;
                case "developTechText":
                    return true;
                case "developPeriodText":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "requestDateFrom":
                    return Date.class;
                case "requestDateTo":
                    return Date.class;
                case "compensationTypeText":
                    return String.class;
                case "studentCategoryText":
                    return String.class;
                case "qualificationText":
                    return String.class;
                case "formativeOrgUnitText":
                    return String.class;
                case "territorialOrgUnitText":
                    return String.class;
                case "developFormText":
                    return String.class;
                case "developConditionText":
                    return String.class;
                case "developTechText":
                    return String.class;
                case "developPeriodText":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SummaryStateExamMarkReport> _dslPath = new Path<SummaryStateExamMarkReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SummaryStateExamMarkReport");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.SummaryStateExamMarkReport#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.SummaryStateExamMarkReport#getRequestDateFrom()
     */
    public static PropertyPath<Date> requestDateFrom()
    {
        return _dslPath.requestDateFrom();
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.SummaryStateExamMarkReport#getRequestDateTo()
     */
    public static PropertyPath<Date> requestDateTo()
    {
        return _dslPath.requestDateTo();
    }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.uniec.entity.report.SummaryStateExamMarkReport#getCompensationTypeText()
     */
    public static PropertyPath<String> compensationTypeText()
    {
        return _dslPath.compensationTypeText();
    }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.uniec.entity.report.SummaryStateExamMarkReport#getStudentCategoryText()
     */
    public static PropertyPath<String> studentCategoryText()
    {
        return _dslPath.studentCategoryText();
    }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.uniec.entity.report.SummaryStateExamMarkReport#getQualificationText()
     */
    public static PropertyPath<String> qualificationText()
    {
        return _dslPath.qualificationText();
    }

    /**
     * @return Формирующее подразделение.
     * @see ru.tandemservice.uniec.entity.report.SummaryStateExamMarkReport#getFormativeOrgUnitText()
     */
    public static PropertyPath<String> formativeOrgUnitText()
    {
        return _dslPath.formativeOrgUnitText();
    }

    /**
     * @return Территориальное подразделение.
     * @see ru.tandemservice.uniec.entity.report.SummaryStateExamMarkReport#getTerritorialOrgUnitText()
     */
    public static PropertyPath<String> territorialOrgUnitText()
    {
        return _dslPath.territorialOrgUnitText();
    }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.uniec.entity.report.SummaryStateExamMarkReport#getDevelopFormText()
     */
    public static PropertyPath<String> developFormText()
    {
        return _dslPath.developFormText();
    }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.uniec.entity.report.SummaryStateExamMarkReport#getDevelopConditionText()
     */
    public static PropertyPath<String> developConditionText()
    {
        return _dslPath.developConditionText();
    }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.uniec.entity.report.SummaryStateExamMarkReport#getDevelopTechText()
     */
    public static PropertyPath<String> developTechText()
    {
        return _dslPath.developTechText();
    }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.uniec.entity.report.SummaryStateExamMarkReport#getDevelopPeriodText()
     */
    public static PropertyPath<String> developPeriodText()
    {
        return _dslPath.developPeriodText();
    }

    public static class Path<E extends SummaryStateExamMarkReport> extends StorableReport.Path<E>
    {
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<Date> _requestDateFrom;
        private PropertyPath<Date> _requestDateTo;
        private PropertyPath<String> _compensationTypeText;
        private PropertyPath<String> _studentCategoryText;
        private PropertyPath<String> _qualificationText;
        private PropertyPath<String> _formativeOrgUnitText;
        private PropertyPath<String> _territorialOrgUnitText;
        private PropertyPath<String> _developFormText;
        private PropertyPath<String> _developConditionText;
        private PropertyPath<String> _developTechText;
        private PropertyPath<String> _developPeriodText;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.SummaryStateExamMarkReport#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.SummaryStateExamMarkReport#getRequestDateFrom()
     */
        public PropertyPath<Date> requestDateFrom()
        {
            if(_requestDateFrom == null )
                _requestDateFrom = new PropertyPath<Date>(SummaryStateExamMarkReportGen.P_REQUEST_DATE_FROM, this);
            return _requestDateFrom;
        }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.SummaryStateExamMarkReport#getRequestDateTo()
     */
        public PropertyPath<Date> requestDateTo()
        {
            if(_requestDateTo == null )
                _requestDateTo = new PropertyPath<Date>(SummaryStateExamMarkReportGen.P_REQUEST_DATE_TO, this);
            return _requestDateTo;
        }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.uniec.entity.report.SummaryStateExamMarkReport#getCompensationTypeText()
     */
        public PropertyPath<String> compensationTypeText()
        {
            if(_compensationTypeText == null )
                _compensationTypeText = new PropertyPath<String>(SummaryStateExamMarkReportGen.P_COMPENSATION_TYPE_TEXT, this);
            return _compensationTypeText;
        }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.uniec.entity.report.SummaryStateExamMarkReport#getStudentCategoryText()
     */
        public PropertyPath<String> studentCategoryText()
        {
            if(_studentCategoryText == null )
                _studentCategoryText = new PropertyPath<String>(SummaryStateExamMarkReportGen.P_STUDENT_CATEGORY_TEXT, this);
            return _studentCategoryText;
        }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.uniec.entity.report.SummaryStateExamMarkReport#getQualificationText()
     */
        public PropertyPath<String> qualificationText()
        {
            if(_qualificationText == null )
                _qualificationText = new PropertyPath<String>(SummaryStateExamMarkReportGen.P_QUALIFICATION_TEXT, this);
            return _qualificationText;
        }

    /**
     * @return Формирующее подразделение.
     * @see ru.tandemservice.uniec.entity.report.SummaryStateExamMarkReport#getFormativeOrgUnitText()
     */
        public PropertyPath<String> formativeOrgUnitText()
        {
            if(_formativeOrgUnitText == null )
                _formativeOrgUnitText = new PropertyPath<String>(SummaryStateExamMarkReportGen.P_FORMATIVE_ORG_UNIT_TEXT, this);
            return _formativeOrgUnitText;
        }

    /**
     * @return Территориальное подразделение.
     * @see ru.tandemservice.uniec.entity.report.SummaryStateExamMarkReport#getTerritorialOrgUnitText()
     */
        public PropertyPath<String> territorialOrgUnitText()
        {
            if(_territorialOrgUnitText == null )
                _territorialOrgUnitText = new PropertyPath<String>(SummaryStateExamMarkReportGen.P_TERRITORIAL_ORG_UNIT_TEXT, this);
            return _territorialOrgUnitText;
        }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.uniec.entity.report.SummaryStateExamMarkReport#getDevelopFormText()
     */
        public PropertyPath<String> developFormText()
        {
            if(_developFormText == null )
                _developFormText = new PropertyPath<String>(SummaryStateExamMarkReportGen.P_DEVELOP_FORM_TEXT, this);
            return _developFormText;
        }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.uniec.entity.report.SummaryStateExamMarkReport#getDevelopConditionText()
     */
        public PropertyPath<String> developConditionText()
        {
            if(_developConditionText == null )
                _developConditionText = new PropertyPath<String>(SummaryStateExamMarkReportGen.P_DEVELOP_CONDITION_TEXT, this);
            return _developConditionText;
        }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.uniec.entity.report.SummaryStateExamMarkReport#getDevelopTechText()
     */
        public PropertyPath<String> developTechText()
        {
            if(_developTechText == null )
                _developTechText = new PropertyPath<String>(SummaryStateExamMarkReportGen.P_DEVELOP_TECH_TEXT, this);
            return _developTechText;
        }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.uniec.entity.report.SummaryStateExamMarkReport#getDevelopPeriodText()
     */
        public PropertyPath<String> developPeriodText()
        {
            if(_developPeriodText == null )
                _developPeriodText = new PropertyPath<String>(SummaryStateExamMarkReportGen.P_DEVELOP_PERIOD_TEXT, this);
            return _developPeriodText;
        }

        public Class getEntityClass()
        {
            return SummaryStateExamMarkReport.class;
        }

        public String getEntityName()
        {
            return "summaryStateExamMarkReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
