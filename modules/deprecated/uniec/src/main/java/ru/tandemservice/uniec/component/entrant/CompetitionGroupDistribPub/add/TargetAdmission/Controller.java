package ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.add.TargetAdmission;

import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.AbstractListDataSource;

import ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.add.ModelBase;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;


/**
 * @author vdanilov
 */
public class Controller extends ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.add.Controller {

    @Override
    protected void addAdditionalColumns(AbstractListDataSource<ModelBase.Row> ds, IMergeRowIdResolver resolver, EnrollmentCampaign campaign)
    {
        if (UniecDAOFacade.getTargetAdmissionDao().getTargetAdmissionKindOptionList(campaign).size() <= 1)
            return;
        AbstractColumn column = new SimpleColumn("Вид ЦП", "row.targetAdmissionKind.fullHierarhyTitle").setClickable(false).setOrderable(false);
        if (null != resolver)
            column.setMergeRowIdResolver(resolver);
        ds.addColumn(column);
    }
}
