/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcOrder.ui.BlockParAddEdit;

import org.hibernate.Session;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniec.entity.orders.EnrollmentParagraph;
import ru.tandemservice.uniec.ui.EnrollmentDirectionUtil;

/**
 * @author Vasily Zhukov
 * @since 24.05.2011
 */
@Input({
        @Bind(key = EcOrderBlockParAddEditUI.PARAMETER_ORDER_ID, binding = EcOrderBlockParAddEditUI.PARAMETER_ORDER_ID),
        @Bind(key = EcOrderBlockParAddEditUI.PARAMETER_PARAGRAPH_ID, binding = EcOrderBlockParAddEditUI.PARAMETER_PARAGRAPH_ID)
})
public class EcOrderBlockParAddEditUI extends UIPresenter implements IEnrollmentCampaignModel
{
    public static final String PARAMETER_ORDER_ID = "orderId";
    public static final String PARAMETER_PARAGRAPH_ID = "paragraphId";

    private Long _orderId;
    private Long _paragraphId;

    private EnrollmentCampaign _enrollmentCampaign;
    private boolean _enrollParagraphPerEduOrgUnit;
    private boolean _editForm;

    private ISelectModel _formativeOrgUnitModel;
    private ISelectModel _territorialOrgUnitModel;
    private ISelectModel _educationLevelsHighSchoolModel;
    private ISelectModel _developFormModel;
    private ISelectModel _developTechModel;
    private ISelectModel _developConditionModel;
    private ISelectModel _developPeriodModel;

    private OrgUnit _formativeOrgUnit;
    private OrgUnit _territorialOrgUnit;
    private EducationLevelsHighSchool _educationLevelsHighSchool;
    private DevelopForm _developForm;
    private DevelopTech _developTech;
    private DevelopCondition _developCondition;
    private DevelopPeriod _developPeriod;

    @Override
    public void onComponentRefresh()
    {
        if (_orderId != null)
        {
            // создание параграфа
            _editForm = false;
            EnrollmentOrder order = DataAccessServices.dao().getNotNull(_orderId);
            _enrollmentCampaign = order.getEnrollmentCampaign();
            _enrollParagraphPerEduOrgUnit = _enrollmentCampaign.isEnrollParagraphPerEduOrgUnit();

            // заполняем все значения по умолчанию
            _territorialOrgUnit = TopOrgUnit.getInstance();
        } else
        {
            // редактирование параграфа
            _editForm = true;
            EnrollmentParagraph paragraph = DataAccessServices.dao().getNotNull(_paragraphId);
            _enrollmentCampaign = ((EnrollmentOrder) paragraph.getOrder()).getEnrollmentCampaign();
            _enrollParagraphPerEduOrgUnit = paragraph.isEnrollParagraphPerEduOrgUnit();

            // заполняем все значения по умолчанию, из менять нельзя
            EducationOrgUnit educationOrgUnit = paragraph.getFirstExtract().getEntity().getEducationOrgUnit();
            _formativeOrgUnit = educationOrgUnit.getFormativeOrgUnit();
            _territorialOrgUnit = educationOrgUnit.getTerritorialOrgUnit();
            _educationLevelsHighSchool = educationOrgUnit.getEducationLevelHighSchool();
            _developForm = educationOrgUnit.getDevelopForm();
            _developCondition = educationOrgUnit.getDevelopCondition();
            _developTech = educationOrgUnit.getDevelopTech();
            _developPeriod = educationOrgUnit.getDevelopPeriod();
        }

        // модели селектов
        Session session = DataAccessServices.dao().getComponentSession();
        _formativeOrgUnitModel = EnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(this);
        _territorialOrgUnitModel = EnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(this);
        _educationLevelsHighSchoolModel = EnrollmentDirectionUtil.createEducationLevelsHighSchoolModel(this);
        _developFormModel = EnrollmentDirectionUtil.createDevelopFormModel(this);
        _developConditionModel = EnrollmentDirectionUtil.createDevelopConditionModel(this);
        _developTechModel = EnrollmentDirectionUtil.createDevelopTechModel(this);
        _developPeriodModel = EnrollmentDirectionUtil.createDevelopPeriodModel(this);
    }

    // Getters & Setters

    public Long getOrderId()
    {
        return _orderId;
    }

    public void setOrderId(Long orderId)
    {
        _orderId = orderId;
    }

    public Long getParagraphId()
    {
        return _paragraphId;
    }

    public void setParagraphId(Long paragraphId)
    {
        _paragraphId = paragraphId;
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public boolean isEnrollParagraphPerEduOrgUnit()
    {
        return _enrollParagraphPerEduOrgUnit;
    }

    public void setEnrollParagraphPerEduOrgUnit(boolean enrollParagraphPerEduOrgUnit)
    {
        _enrollParagraphPerEduOrgUnit = enrollParagraphPerEduOrgUnit;
    }

    public boolean isEditForm()
    {
        return _editForm;
    }

    public void setEditForm(boolean editForm)
    {
        _editForm = editForm;
    }

    public ISelectModel getFormativeOrgUnitModel()
    {
        return _formativeOrgUnitModel;
    }

    public void setFormativeOrgUnitModel(ISelectModel formativeOrgUnitModel)
    {
        _formativeOrgUnitModel = formativeOrgUnitModel;
    }

    public ISelectModel getTerritorialOrgUnitModel()
    {
        return _territorialOrgUnitModel;
    }

    public void setTerritorialOrgUnitModel(ISelectModel territorialOrgUnitModel)
    {
        _territorialOrgUnitModel = territorialOrgUnitModel;
    }

    public ISelectModel getEducationLevelsHighSchoolModel()
    {
        return _educationLevelsHighSchoolModel;
    }

    public void setEducationLevelsHighSchoolModel(ISelectModel educationLevelsHighSchoolModel)
    {
        _educationLevelsHighSchoolModel = educationLevelsHighSchoolModel;
    }

    public ISelectModel getDevelopFormModel()
    {
        return _developFormModel;
    }

    public void setDevelopFormModel(ISelectModel developFormModel)
    {
        _developFormModel = developFormModel;
    }

    public ISelectModel getDevelopTechModel()
    {
        return _developTechModel;
    }

    public void setDevelopTechModel(ISelectModel developTechModel)
    {
        _developTechModel = developTechModel;
    }

    public ISelectModel getDevelopConditionModel()
    {
        return _developConditionModel;
    }

    public void setDevelopConditionModel(ISelectModel developConditionModel)
    {
        _developConditionModel = developConditionModel;
    }

    public ISelectModel getDevelopPeriodModel()
    {
        return _developPeriodModel;
    }

    public void setDevelopPeriodModel(ISelectModel developPeriodModel)
    {
        _developPeriodModel = developPeriodModel;
    }

    @Override
    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        _formativeOrgUnit = formativeOrgUnit;
    }

    @Override
    public OrgUnit getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    public void setTerritorialOrgUnit(OrgUnit territorialOrgUnit)
    {
        _territorialOrgUnit = territorialOrgUnit;
    }

    @Override
    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _educationLevelsHighSchool;
    }

    public void setEducationLevelsHighSchool(EducationLevelsHighSchool educationLevelsHighSchool)
    {
        _educationLevelsHighSchool = educationLevelsHighSchool;
    }

    @Override
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public void setDevelopForm(DevelopForm developForm)
    {
        _developForm = developForm;
    }

    @Override
    public DevelopTech getDevelopTech()
    {
        return _developTech;
    }

    public void setDevelopTech(DevelopTech developTech)
    {
        _developTech = developTech;
    }

    @Override
    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    public void setDevelopCondition(DevelopCondition developCondition)
    {
        _developCondition = developCondition;
    }

    @Override
    public DevelopPeriod getDevelopPeriod()
    {
        return _developPeriod;
    }

    public void setDevelopPeriod(DevelopPeriod developPeriod)
    {
        _developPeriod = developPeriod;
    }

    // Listeners

    public void onChangeFormingRule()
    {
        // refresh model values
        if (!_enrollParagraphPerEduOrgUnit)
        {
            _territorialOrgUnit = TopOrgUnit.getInstance();
            _developCondition = null;
            _developTech = null;
            _developPeriod = null;
        }
    }
}
