/**
 *$Id$
 */
package ru.tandemservice.uniec.base.ext.SystemAction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.SystemActionManager;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.util.SystemActionDefinition;
import ru.tandemservice.uniec.base.ext.SystemAction.ui.Pub.SystemActionPubExt;

/**
 * @author Alexander Shaburov
 * @since 13.02.13
 */
@Configuration
public class SystemActionExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private SystemActionManager _systemActionManager;

    @Bean
    public ItemListExtension<SystemActionDefinition> buttonListExtension()
    {
        return itemListExtension(_systemActionManager.buttonListExtPoint())
//  Deprecated  <button name="uniec_checkEntrantLogicConsistency" label="Проверить логику" listener="{extension}:onClickCheckEntrantLogicConsistency" alert="Проверить логику?"/>
                .add("uniec_updateEntrantFinalMarks", new SystemActionDefinition("uniec", "updateEntrantFinalMarks", "onClickUpdateEntrantData", SystemActionPubExt.EC_SYSTEM_ACTION_PUB_ADDON_NAME))
//  Deprecated  <button name="uniec_setRequestedEnrollmentDirectionProfileDisciplines" label="Ограничить профильные испытания в наборах и вычислить профильные испытания для выбранных направлений приема" listener="{extension}:onClickSetRequestedEnrollmentDirectionProfileDisciplines" alert="Ограничить профильные испытания в наборах и вычислить профильные испытания для выбранных направлений приема?"/>
//  Deprecated  <button name="uniec_сorrectExamGroupNamesMechanism1" label="Скорректировать названия экзаменационных групп, использующих Механизм 1 формирования названия групп" listener="{extension}:onClickCorrectExamGroupNamesMechanism1" alert="Скорректировать названия экзаменационных групп, использующих Механизм 1 формирования названия групп?"/>
                .add("uniec_enrollmentenvironment", new SystemActionDefinition("uniec", "enrollmentenvironment", "onClickGetEnrollmentEnvironmentXml", SystemActionPubExt.EC_SYSTEM_ACTION_PUB_ADDON_NAME))
                .create();
    }
}
