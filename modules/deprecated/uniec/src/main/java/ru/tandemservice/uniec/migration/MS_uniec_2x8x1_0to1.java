package ru.tandemservice.uniec.migration;

import org.apache.log4j.Logger;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniec_2x8x1_0to1 extends IndependentMigrationScript
{
    private final Logger logger = Logger.getLogger(this.getClass());

    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrCampaignEntrantIndividualProgress

		// удалено свойство consideredInEnrollmentCampaign
		{
            final int i = tool.executeUpdate("DELETE FROM nrcmpgnentrntindvdlprgrss_t WHERE cnsdrdinenrllmntcmpgn_p=?", false);
            logger.info("Deleted " + i + " not used enrCampaignEntrantIndividualProgress rows");

			// удалить колонку
			tool.dropColumn("nrcmpgnentrntindvdlprgrss_t", "cnsdrdinenrllmntcmpgn_p");

		}


    }
}