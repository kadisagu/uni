/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.form76KD2009.Add;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.fias.base.entity.*;
import org.tandemframework.shared.fias.base.entity.codes.AddressCountryTypeCodes;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.Sex;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.report.Form76KD2009Report;

import java.text.SimpleDateFormat;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vip_delete
 * @since 10.08.2009
 */
class Form76KD2009ReportBuilder
{
    private Session _session;
    private Model _model;

    Form76KD2009ReportBuilder(Model model, Session session)
    {
        _model = model;
        _session = session;
    }

    DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    private byte[] buildReport()
    {
        Map<String, String> code2Title_A = new HashMap<>();
        code2Title_A.put("1", "очную");
        code2Title_A.put("2", "заочную");
        code2Title_A.put("3", "очно-заочную");
        code2Title_A.put("4", "экстернат");
        code2Title_A.put("5", "самостоятельную");

        Map<String, String> code2Title_G = new HashMap<>();
        code2Title_G.put("1", "очного");
        code2Title_G.put("2", "заочного");
        code2Title_G.put("3", "очно-заочного");
        code2Title_G.put("4", "экстернатного");
        code2Title_G.put("5", "самостоятельного");

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("developForm_A", code2Title_A.get(_model.getReport().getDevelopForm().getCode()));
        injectModifier.put("developForm_G", code2Title_G.get(_model.getReport().getDevelopForm().getCode()));
        injectModifier.put("vuz", TopOrgUnit.getInstance().getTitle());
        injectModifier.put("year", _model.getReport().getEnrollmentCampaign().getEducationYear().getTitle());
        injectModifier.put("formingDate", new SimpleDateFormat("d MMMM yyyy", RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU).format(_model.getReport().getFormingDate()));

        final int[][] firstTable = getFirstTableData();
        final int[][] secondTable = getSecondTableData();

        for (int i = 0; i < firstTable.length; i++)
            for (int j = 0; j < firstTable[i].length; j++)
                injectModifier.put("T" + Integer.toString(i) + Integer.toString(j), Integer.toString(firstTable[i][j]));

        final int[] parts = new int[28];
        Arrays.fill(parts, 1);
        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T", new String[][]{{"А", "Б"}});
        tableModifier.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public void beforeModify(RtfTable table, int currentRowIndex)
            {
                RtfUtil.splitRow(table.getRowList().get(currentRowIndex - 1), 3, (newCell, index) -> {
                    String title = (index < 9 ? "0" : "") + (index + 1) + "0000";
                    newCell.setElementList(Collections.singletonList((IRtfElement) RtfBean.getElementFactory().createRtfText(title)));
                }, parts);

                for (int i = 0; i < 8; i++)
                {
                    final int rowIndex = i;
                    RtfRow row = table.getRowList().get(currentRowIndex + i);
                    RtfUtil.splitRow(row, 3, (newCell, index) -> {
                        int count = secondTable[rowIndex][index + 1];
                        String title = count == 0 ? "" : Integer.toString(count);
                        newCell.setElementList(Collections.singletonList((IRtfElement) RtfBean.getElementFactory().createRtfText(title)));
                    }, parts);
                    row.getCellList().get(2).setElementList(Collections.singletonList((IRtfElement) RtfBean.getElementFactory().createRtfText(Integer.toString(secondTable[rowIndex][0]))));
                }
            }
        });

        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_FORM_76KD_2009);
        return RtfUtil.toByteArray(templateDocument.getCurrentTemplate(), injectModifier, tableModifier);
    }

    private int[][] getFirstTableData()
    {
        int[][] data = new int[9][7];
        String[] codes = new String[]{QualificationsCodes.SPETSIALIST, QualificationsCodes.BAKALAVR, QualificationsCodes.MAGISTR};
        for (int i = 0; i < codes.length; i++)
        {
            DQLSelectBuilder rowBuilder = new DQLSelectBuilder()
            .column(getResult(getT1(), codes[i]))
            .column(getResult(getT2(), codes[i]))
            .column(getResult(getT3(codes[i]), codes[i]))
            .column(getResult(getT4(), codes[i]))
            .column(getResult(getT5(), codes[i]))
            .column(getResult(getT6(), codes[i]))
            .column(getT8().where(eq(property("q." + Qualifications.P_CODE), value(codes[i]))).buildQuery())
            .column(getResult(getT21(), codes[i]))
            .column(getResult(getT22(), codes[i]))
            .column(getResult(getT23(), codes[i]))
            .column(getResult(getT24(), codes[i]))
            .column(getResult(getT25(), codes[i]))
            .column(getResult(getT26(), codes[i]));

            Object[] rowData = rowBuilder.createStatement(new DQLExecutionContext(_session)).uniqueResult();

            // первые 6 чисел:
            for (int j = 0; j < 6; j++)
                data[i + 1][j + 1] = rowData[j] == null ? 0 : ((Number) rowData[j]).intValue();

            // следующие 7 чисел
            for (int j = 0; j < 7; j++)
                data[i + 6][j] = rowData[j + 6] == null ? 0 : ((Number) rowData[j + 6]).intValue();
        }

        Number count = new DQLSelectBuilder().fromDataSource(getT7().buildQuery(), "t").createCountStatement(new DQLExecutionContext(_session)).uniqueResult();
        data[4][1] = count == null ? 0 : count.intValue();

        for (int i = 0; i < 7; i++)
        {
            data[0][i] = data[1][i] + data[2][i] + data[3][i];
            data[5][i] = data[6][i] + data[7][i] + data[8][i];
        }
        return data;
    }

    // TODO: DEV-4281 используется ОКСО, которого нет в классификаторах 2013 - нужно исправить логику данного метода
    // TODO: скорее всего здесь можно заменить getInheritedOkso на getTitleCodePrefix
    // TODO: и вообще, этот отчет 2009ого года издох
    private int[][] getSecondTableData()
    {
        int[][] data = new int[8][29];
        for (int i = 0; i < 29; i++) data[0][i] = i + 1;

        Map<Long, Integer> id2okso = new HashMap<>();
        MQBuilder mqBuilder = new MQBuilder(EducationLevels.ENTITY_CLASS, "e");
        mqBuilder.addLeftJoinFetch("e", EducationLevels.L_PARENT_LEVEL, "p");
        for (EducationLevels item : mqBuilder.<EducationLevels>getResultList(_session))
        {
            String inheritedOkso = StringUtils.trimToNull(item.getInheritedOkso());
            if (null != inheritedOkso)
            {
                try
                {
                    int parseInt = Integer.parseInt(inheritedOkso.substring(0, 2));
                    id2okso.put(item.getId(), parseInt);
                }
                catch (Throwable t)
                {
                    // пока забили, если потом где-то оно будет юзаться - сругаемся
                }
            }
            else
            {
                // пока забили, если потом где-то оно будет юзаться - сругаемся
            }
        }

        DQLSelectBuilder builder = getPreliminaryBuilder()
        .column(property("q." + Qualifications.P_CODE), "qCode")
        .column(property("ct." + CompensationType.P_CODE), "cCode")
        .column(property("el." + EducationLevels.P_ID), "eId")
        .joinPath(DQLJoinType.inner, "p." + PreliminaryEnrollmentStudent.L_COMPENSATION_TYPE, "ct")
        .joinPath(DQLJoinType.inner, "entrant." + Entrant.L_PERSON, "person")
        .joinPath(DQLJoinType.inner, "person." + Person.L_IDENTITY_CARD, "card")
        .joinPath(DQLJoinType.inner, "card." + IdentityCard.L_CARD_TYPE, "cardType")
        .where(not(DQLExpressions.and(
                eq(property("p." + PreliminaryEnrollmentStudent.P_TARGET_ADMISSION), value(Boolean.TRUE)),
                or(
                        eq(property("cardType." + IdentityCardType.P_CODE), value(UniDefines.CATALOG_IDENTITYCARD_TYPE_PASSPORT_NOT_RF)),
                        eq(property("cardType." + IdentityCardType.P_CODE), value(UniDefines.CATALOG_IDENTITYCARD_TYPE_RESIDENCE))
                )
        )));

        List<Object[]> rowList = builder.createStatement(new DQLExecutionContext(_session)).list();
        for (Object[] row : rowList)
        {
            String qualificationCode = (String) row[1];
            String compensationCode = (String) row[2];
            Long educationLevelId = ((Number) row[3]).longValue();

            Integer oksoInt = id2okso.get(educationLevelId);
            if (null == oksoInt)
            {
                // извините, но мы юзаем направление без ОКСО
                throw new ApplicationException("Нельзя сформировать отчет, т.к. не для всех направлений приема, вошедших в статистику отчета, задан код ОКСО.");
            }

            int columnIndex = oksoInt;
            if (columnIndex < 0)
            {
                // извините, но мы юзаем направление без ОКСО
                throw new ApplicationException("Нельзя сформировать отчет, т.к. не для всех направлений приема, вошедших в статистику отчета, задан код ОКСО.");
            }

            int rowIndex = 0;
            switch (qualificationCode)
            {
                case QualificationsCodes.SPETSIALIST:
                    rowIndex = 2;
                    break;
                case QualificationsCodes.BAKALAVR:
                    rowIndex = 4;
                    break;
                case QualificationsCodes.MAGISTR:
                    rowIndex = 6;
                    break;
            }

            if (rowIndex != 0)
            {
                data[rowIndex][columnIndex]++;
                if (compensationCode.equals(UniDefines.COMPENSATION_TYPE_BUDGET))
                    data[rowIndex + 1][columnIndex]++;
            }
        }

        for (int i = 1; i < 8; i++)
            for (int j = 1; j < 29; j++)
                data[i][0] += data[i][j];

        for (int i = 0; i < 29; i++)
            data[1][i] = data[2][i] + data[4][i] + data[6][i];

        return data;
    }

    private static IDQLSelectableQuery getResult(DQLSelectBuilder builder, String qualificationCode)
    {
        builder.where(eq(property("q." + Qualifications.P_CODE), value(qualificationCode)));
        return new DQLSelectBuilder().fromDataSource(builder.buildQuery(), "t").buildCountQuery();
    }

    private DQLSelectBuilder getT1()
    {
        return getRequestedBuilder()
        .group(property("d." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST))
        .group(property("d." + RequestedEnrollmentDirection.L_COMPENSATION_TYPE));
    }

    private DQLSelectBuilder getT2()
    {
        return getRequestedBuilder()
        .joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_COMPENSATION_TYPE, "ct")
        .where(or(
                eq(property("ct." + CompensationType.P_CODE), value(UniDefines.COMPENSATION_TYPE_CONTRACT)),
                eq(property("d." + RequestedEnrollmentDirection.P_TARGET_ADMISSION), value(Boolean.TRUE))
        ))
        .group(property("d." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST));
    }

    private DQLSelectBuilder getT3(String qualificationCode)
    {
        DQLSelectBuilder subBuilder = new DQLSelectBuilder();
        subBuilder.fromEntity(RequestedEnrollmentDirection.class, "sub_d");
        subBuilder.column(property("sub_d.id"));
        subBuilder.joinPath(DQLJoinType.inner, "sub_d." + RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, "sub_ed");
        subBuilder.joinPath(DQLJoinType.inner, "sub_ed." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "sub_ou");
        subBuilder.joinPath(DQLJoinType.inner, "sub_ou." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "sub_hs");
        subBuilder.joinPath(DQLJoinType.inner, "sub_hs." + EducationLevelsHighSchool.L_EDUCATION_LEVEL, "sub_el");
        subBuilder.joinPath(DQLJoinType.inner, "sub_el." + EducationLevels.L_QUALIFICATION, "sub_q");
        subBuilder.where(eq(property("sub_d." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST), property("request.id")));
        subBuilder.where(eq(property("sub_d." + RequestedEnrollmentDirection.P_TARGET_ADMISSION), value(Boolean.TRUE)));
        subBuilder.where(eq(property("sub_q." + Qualifications.P_CODE), value(qualificationCode)));

        return getRequestedBuilder()
        .joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_COMPENSATION_TYPE, "ct")
        .where(eq(property("ct." + CompensationType.P_CODE), value(UniDefines.COMPENSATION_TYPE_CONTRACT)))
        .where(not(exists(subBuilder.buildQuery())))
        .group(property("d." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST));
    }

    private DQLSelectBuilder getT4()
    {
        return getRequestedBuilder()
        .joinPath(DQLJoinType.inner, "entrant." + Entrant.L_PERSON, "person")
        .joinPath(DQLJoinType.inner, "person." + Person.L_IDENTITY_CARD, "card")
        .joinPath(DQLJoinType.left, "card." + IdentityCard.L_ADDRESS, "baseAddress")
        .joinEntity("baseAddress", DQLJoinType.inner, AddressDetailed.class, "detailedAddress",
                eq(AddressBase.id().fromAlias("baseAddress"), AddressDetailed.id().fromAlias("detailedAddress")))
        .where(eq(property("detailedAddress", AddressDetailed.country().countryType().code()), value(AddressCountryTypeCodes.NEAR)))
        .group(property("d." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST))
        .group(property("d." + RequestedEnrollmentDirection.L_COMPENSATION_TYPE));
    }

    private DQLSelectBuilder getT5()
    {
        return getT4()
        .joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_COMPENSATION_TYPE, "ct")
        .where(eq(property("ct." + CompensationType.P_CODE), value(UniDefines.COMPENSATION_TYPE_CONTRACT)));
    }

    private DQLSelectBuilder getT6()
    {
        return getT1()
        .joinPath(DQLJoinType.inner, "entrant." + Entrant.L_PERSON, "person")
        .joinPath(DQLJoinType.inner, "person." + Person.L_IDENTITY_CARD, "card")
        .joinPath(DQLJoinType.inner, "card." + IdentityCard.L_SEX, "sex")
        .where(eq(property("sex." + Sex.P_CODE), value(SexCodes.MALE)));
    }

    private DQLSelectBuilder getT21()
    {
        return getPreliminaryBuilder();
    }

    private DQLSelectBuilder getT22()
    {
        return getPreliminaryBuilder()
        .joinPath(DQLJoinType.inner, "p." + PreliminaryEnrollmentStudent.L_COMPENSATION_TYPE, "ct")
        .where(or(
                eq(property("ct." + CompensationType.P_CODE), value(UniDefines.COMPENSATION_TYPE_CONTRACT)),
                eq(property("p." + PreliminaryEnrollmentStudent.P_TARGET_ADMISSION), value(Boolean.TRUE))
        ));
    }

    private DQLSelectBuilder getT23()
    {
        return getPreliminaryBuilder()
        .joinPath(DQLJoinType.inner, "p." + PreliminaryEnrollmentStudent.L_COMPENSATION_TYPE, "ct")
        .where(eq(property("ct." + CompensationType.P_CODE), value(UniDefines.COMPENSATION_TYPE_CONTRACT)))
        .where(eq(property("p." + PreliminaryEnrollmentStudent.P_TARGET_ADMISSION), value(Boolean.FALSE)));
    }

    private DQLSelectBuilder getT24()
    {
        return getPreliminaryBuilder()
        .joinPath(DQLJoinType.inner, "entrant." + Entrant.L_PERSON, "person")
        .joinPath(DQLJoinType.inner, "person." + Person.L_IDENTITY_CARD, "card")
        .joinPath(DQLJoinType.left, "card." + IdentityCard.L_ADDRESS, "baseAddress")
        .joinEntity("baseAddress", DQLJoinType.inner, AddressDetailed.class, "detailedAddress",
                eq(AddressBase.id().fromAlias("baseAddress"), AddressDetailed.id().fromAlias("detailedAddress")))
        .where(eq(property("detailedAddress", AddressDetailed.country().countryType().code()), value(AddressCountryTypeCodes.NEAR)));
    }

    private DQLSelectBuilder getT25()
    {
        return getT24()
        .joinPath(DQLJoinType.inner, "p." + PreliminaryEnrollmentStudent.L_COMPENSATION_TYPE, "ct")
        .where(eq(property("ct." + CompensationType.P_CODE), value(UniDefines.COMPENSATION_TYPE_CONTRACT)))
        .where(eq(property("p." + PreliminaryEnrollmentStudent.P_TARGET_ADMISSION), value(Boolean.FALSE)));
    }

    private DQLSelectBuilder getT26()
    {
        return getPreliminaryBuilder()
        .joinPath(DQLJoinType.inner, "entrant." + Entrant.L_PERSON, "person")
        .joinPath(DQLJoinType.inner, "person." + Person.L_IDENTITY_CARD, "card")
        .joinPath(DQLJoinType.inner, "card." + IdentityCard.L_SEX, "sex")
        .where(eq(property("sex." + Sex.P_CODE), value(SexCodes.MALE)));
    }

    private DQLSelectBuilder getT7()
    {
        return getT1()
        .joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_STATE, "state")
        .where(notIn(property("state." + EntrantState.P_CODE), Arrays.asList(
                UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY,
                UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE,
                UniecDefines.ENTRANT_STATE_ACTIVE_CODE
        )));
    }

    private DQLSelectBuilder getT8()
    {
        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.column(DQLFunctions.sum(property("ed." + EnrollmentDirection.P_MINISTERIAL_PLAN)));
        builder.fromEntity(EnrollmentDirection.class, "ed");
        builder.joinPath(DQLJoinType.inner, "ed." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "ou");
        builder.where(eq(property("ed." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN), value(_model.getReport().getEnrollmentCampaign())));

        patchEduOrgUnit(builder);

        return builder;
    }

    private DQLSelectBuilder getRequestedBuilder()
    {
        Form76KD2009Report report = _model.getReport();

        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.column(value("0"), "x");
        builder.fromEntity(RequestedEnrollmentDirection.class, "d");
        builder.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "request");
        builder.joinPath(DQLJoinType.inner, "request." + EntrantRequest.L_ENTRANT, "entrant");
        builder.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, "ed");
        builder.joinPath(DQLJoinType.inner, "ed." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "ou");

        builder.where(betweenDays("request." + EntrantRequest.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));
        builder.where(eq(property("entrant." + Entrant.P_ARCHIVAL), value(Boolean.FALSE)));
        builder.where(ne(property("d." + RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE), UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        builder.where(eq(property("entrant." + Entrant.L_ENROLLMENT_CAMPAIGN), value(report.getEnrollmentCampaign())));

        if (_model.isStudentCategoryActive())
            builder.where(in(property("d." + RequestedEnrollmentDirection.L_STUDENT_CATEGORY), _model.getStudentCategoryList()));

        patchEduOrgUnit(builder);

        return builder;
    }

    private DQLSelectBuilder getPreliminaryBuilder()
    {
        Form76KD2009Report report = _model.getReport();

        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.column(value("0"), "x");
        builder.fromEntity(PreliminaryEnrollmentStudent.class, "p");
        builder.joinPath(DQLJoinType.inner, "p." + PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, "d");
        builder.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "request");
        builder.joinPath(DQLJoinType.inner, "request." + EntrantRequest.L_ENTRANT, "entrant");
        builder.joinPath(DQLJoinType.inner, "p." + PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT, "ou");

        builder.where(betweenDays("request." + EntrantRequest.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));
        builder.where(eq(property("entrant." + Entrant.P_ARCHIVAL), value(Boolean.FALSE)));
        builder.where(eq(property("entrant." + Entrant.L_ENROLLMENT_CAMPAIGN), value(report.getEnrollmentCampaign())));

        if (_model.isStudentCategoryActive())
            builder.where(in(property("p." + PreliminaryEnrollmentStudent.L_STUDENT_CATEGORY), _model.getStudentCategoryList()));

        if (_model.isParallelActive() && _model.getParallel().isTrue())
            builder.where(eq(property("p." + PreliminaryEnrollmentStudent.P_PARALLEL), value(Boolean.FALSE)));

        patchEduOrgUnit(builder);

        return builder;
    }

    private void patchEduOrgUnit(DQLSelectBuilder builder)
    {
        builder.joinPath(DQLJoinType.inner, "ou." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "hs");
        builder.joinPath(DQLJoinType.inner, "hs." + EducationLevelsHighSchool.L_EDUCATION_LEVEL, "el");
        builder.joinPath(DQLJoinType.inner, "el." + EducationLevels.L_QUALIFICATION, "q");
        builder.where(eq(property("ou." + EducationOrgUnit.L_DEVELOP_FORM), value(_model.getReport().getDevelopForm())));
        if (_model.isDevelopConditionActive())
            builder.where(in(property("ou." + EducationOrgUnit.L_DEVELOP_CONDITION), _model.getDevelopConditionList()));
    }
}
