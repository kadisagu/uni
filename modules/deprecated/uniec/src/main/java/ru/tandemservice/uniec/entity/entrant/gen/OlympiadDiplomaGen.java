package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import ru.tandemservice.uniec.entity.catalog.OlympiadDiplomaDegree;
import ru.tandemservice.uniec.entity.catalog.OlympiadDiplomaType;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.OlympiadDiploma;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Диплом участника олимпиады
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OlympiadDiplomaGen extends VersionedEntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.OlympiadDiploma";
    public static final String ENTITY_NAME = "olympiadDiploma";
    public static final int VERSION_HASH = 1572215970;
    private static IEntityMeta ENTITY_META;

    public static final String P_VERSION = "version";
    public static final String L_ENTRANT = "entrant";
    public static final String P_NUMBER = "number";
    public static final String P_SERIA = "seria";
    public static final String L_DEGREE = "degree";
    public static final String P_OLYMPIAD = "olympiad";
    public static final String P_SUBJECT = "subject";
    public static final String P_ISSUANCE_DATE = "issuanceDate";
    public static final String L_SETTLEMENT = "settlement";
    public static final String L_DIPLOMA_TYPE = "diplomaType";

    private int _version; 
    private Entrant _entrant;     // (Старый) Абитуриент
    private String _number;     // Номер
    private String _seria;     // Серия
    private OlympiadDiplomaDegree _degree;     // Степень отличия в дипломе участника олимпиады
    private String _olympiad;     // Название олимпиады
    private String _subject;     // Предмет
    private Date _issuanceDate;     // Дата выдачи
    private AddressItem _settlement;     // Элемент административно-территориального деления
    private OlympiadDiplomaType _diplomaType;     // Тип олимпиады

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public int getVersion()
    {
        return _version;
    }

    /**
     * @param version  Свойство не может быть null.
     */
    public void setVersion(int version)
    {
        dirty(_version, version);
        _version = version;
    }

    /**
     * @return (Старый) Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public Entrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant (Старый) Абитуриент. Свойство не может быть null.
     */
    public void setEntrant(Entrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Номер.
     */
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Серия.
     */
    @Length(max=255)
    public String getSeria()
    {
        return _seria;
    }

    /**
     * @param seria Серия.
     */
    public void setSeria(String seria)
    {
        dirty(_seria, seria);
        _seria = seria;
    }

    /**
     * @return Степень отличия в дипломе участника олимпиады. Свойство не может быть null.
     */
    @NotNull
    public OlympiadDiplomaDegree getDegree()
    {
        return _degree;
    }

    /**
     * @param degree Степень отличия в дипломе участника олимпиады. Свойство не может быть null.
     */
    public void setDegree(OlympiadDiplomaDegree degree)
    {
        dirty(_degree, degree);
        _degree = degree;
    }

    /**
     * @return Название олимпиады. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getOlympiad()
    {
        return _olympiad;
    }

    /**
     * @param olympiad Название олимпиады. Свойство не может быть null.
     */
    public void setOlympiad(String olympiad)
    {
        dirty(_olympiad, olympiad);
        _olympiad = olympiad;
    }

    /**
     * @return Предмет. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getSubject()
    {
        return _subject;
    }

    /**
     * @param subject Предмет. Свойство не может быть null.
     */
    public void setSubject(String subject)
    {
        dirty(_subject, subject);
        _subject = subject;
    }

    /**
     * @return Дата выдачи.
     */
    public Date getIssuanceDate()
    {
        return _issuanceDate;
    }

    /**
     * @param issuanceDate Дата выдачи.
     */
    public void setIssuanceDate(Date issuanceDate)
    {
        dirty(_issuanceDate, issuanceDate);
        _issuanceDate = issuanceDate;
    }

    /**
     * @return Элемент административно-территориального деления.
     */
    public AddressItem getSettlement()
    {
        return _settlement;
    }

    /**
     * @param settlement Элемент административно-территориального деления.
     */
    public void setSettlement(AddressItem settlement)
    {
        dirty(_settlement, settlement);
        _settlement = settlement;
    }

    /**
     * @return Тип олимпиады.
     */
    public OlympiadDiplomaType getDiplomaType()
    {
        return _diplomaType;
    }

    /**
     * @param diplomaType Тип олимпиады.
     */
    public void setDiplomaType(OlympiadDiplomaType diplomaType)
    {
        dirty(_diplomaType, diplomaType);
        _diplomaType = diplomaType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof OlympiadDiplomaGen)
        {
            setVersion(((OlympiadDiploma)another).getVersion());
            setEntrant(((OlympiadDiploma)another).getEntrant());
            setNumber(((OlympiadDiploma)another).getNumber());
            setSeria(((OlympiadDiploma)another).getSeria());
            setDegree(((OlympiadDiploma)another).getDegree());
            setOlympiad(((OlympiadDiploma)another).getOlympiad());
            setSubject(((OlympiadDiploma)another).getSubject());
            setIssuanceDate(((OlympiadDiploma)another).getIssuanceDate());
            setSettlement(((OlympiadDiploma)another).getSettlement());
            setDiplomaType(((OlympiadDiploma)another).getDiplomaType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OlympiadDiplomaGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OlympiadDiploma.class;
        }

        public T newInstance()
        {
            return (T) new OlympiadDiploma();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "version":
                    return obj.getVersion();
                case "entrant":
                    return obj.getEntrant();
                case "number":
                    return obj.getNumber();
                case "seria":
                    return obj.getSeria();
                case "degree":
                    return obj.getDegree();
                case "olympiad":
                    return obj.getOlympiad();
                case "subject":
                    return obj.getSubject();
                case "issuanceDate":
                    return obj.getIssuanceDate();
                case "settlement":
                    return obj.getSettlement();
                case "diplomaType":
                    return obj.getDiplomaType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "version":
                    obj.setVersion((Integer) value);
                    return;
                case "entrant":
                    obj.setEntrant((Entrant) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "seria":
                    obj.setSeria((String) value);
                    return;
                case "degree":
                    obj.setDegree((OlympiadDiplomaDegree) value);
                    return;
                case "olympiad":
                    obj.setOlympiad((String) value);
                    return;
                case "subject":
                    obj.setSubject((String) value);
                    return;
                case "issuanceDate":
                    obj.setIssuanceDate((Date) value);
                    return;
                case "settlement":
                    obj.setSettlement((AddressItem) value);
                    return;
                case "diplomaType":
                    obj.setDiplomaType((OlympiadDiplomaType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "version":
                        return true;
                case "entrant":
                        return true;
                case "number":
                        return true;
                case "seria":
                        return true;
                case "degree":
                        return true;
                case "olympiad":
                        return true;
                case "subject":
                        return true;
                case "issuanceDate":
                        return true;
                case "settlement":
                        return true;
                case "diplomaType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "version":
                    return true;
                case "entrant":
                    return true;
                case "number":
                    return true;
                case "seria":
                    return true;
                case "degree":
                    return true;
                case "olympiad":
                    return true;
                case "subject":
                    return true;
                case "issuanceDate":
                    return true;
                case "settlement":
                    return true;
                case "diplomaType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "version":
                    return Integer.class;
                case "entrant":
                    return Entrant.class;
                case "number":
                    return String.class;
                case "seria":
                    return String.class;
                case "degree":
                    return OlympiadDiplomaDegree.class;
                case "olympiad":
                    return String.class;
                case "subject":
                    return String.class;
                case "issuanceDate":
                    return Date.class;
                case "settlement":
                    return AddressItem.class;
                case "diplomaType":
                    return OlympiadDiplomaType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OlympiadDiploma> _dslPath = new Path<OlympiadDiploma>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OlympiadDiploma");
    }
            

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.OlympiadDiploma#getVersion()
     */
    public static PropertyPath<Integer> version()
    {
        return _dslPath.version();
    }

    /**
     * @return (Старый) Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.OlympiadDiploma#getEntrant()
     */
    public static Entrant.Path<Entrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Номер.
     * @see ru.tandemservice.uniec.entity.entrant.OlympiadDiploma#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Серия.
     * @see ru.tandemservice.uniec.entity.entrant.OlympiadDiploma#getSeria()
     */
    public static PropertyPath<String> seria()
    {
        return _dslPath.seria();
    }

    /**
     * @return Степень отличия в дипломе участника олимпиады. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.OlympiadDiploma#getDegree()
     */
    public static OlympiadDiplomaDegree.Path<OlympiadDiplomaDegree> degree()
    {
        return _dslPath.degree();
    }

    /**
     * @return Название олимпиады. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.OlympiadDiploma#getOlympiad()
     */
    public static PropertyPath<String> olympiad()
    {
        return _dslPath.olympiad();
    }

    /**
     * @return Предмет. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.OlympiadDiploma#getSubject()
     */
    public static PropertyPath<String> subject()
    {
        return _dslPath.subject();
    }

    /**
     * @return Дата выдачи.
     * @see ru.tandemservice.uniec.entity.entrant.OlympiadDiploma#getIssuanceDate()
     */
    public static PropertyPath<Date> issuanceDate()
    {
        return _dslPath.issuanceDate();
    }

    /**
     * @return Элемент административно-территориального деления.
     * @see ru.tandemservice.uniec.entity.entrant.OlympiadDiploma#getSettlement()
     */
    public static AddressItem.Path<AddressItem> settlement()
    {
        return _dslPath.settlement();
    }

    /**
     * @return Тип олимпиады.
     * @see ru.tandemservice.uniec.entity.entrant.OlympiadDiploma#getDiplomaType()
     */
    public static OlympiadDiplomaType.Path<OlympiadDiplomaType> diplomaType()
    {
        return _dslPath.diplomaType();
    }

    public static class Path<E extends OlympiadDiploma> extends EntityPath<E>
    {
        private PropertyPath<Integer> _version;
        private Entrant.Path<Entrant> _entrant;
        private PropertyPath<String> _number;
        private PropertyPath<String> _seria;
        private OlympiadDiplomaDegree.Path<OlympiadDiplomaDegree> _degree;
        private PropertyPath<String> _olympiad;
        private PropertyPath<String> _subject;
        private PropertyPath<Date> _issuanceDate;
        private AddressItem.Path<AddressItem> _settlement;
        private OlympiadDiplomaType.Path<OlympiadDiplomaType> _diplomaType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.OlympiadDiploma#getVersion()
     */
        public PropertyPath<Integer> version()
        {
            if(_version == null )
                _version = new PropertyPath<Integer>(OlympiadDiplomaGen.P_VERSION, this);
            return _version;
        }

    /**
     * @return (Старый) Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.OlympiadDiploma#getEntrant()
     */
        public Entrant.Path<Entrant> entrant()
        {
            if(_entrant == null )
                _entrant = new Entrant.Path<Entrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Номер.
     * @see ru.tandemservice.uniec.entity.entrant.OlympiadDiploma#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(OlympiadDiplomaGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Серия.
     * @see ru.tandemservice.uniec.entity.entrant.OlympiadDiploma#getSeria()
     */
        public PropertyPath<String> seria()
        {
            if(_seria == null )
                _seria = new PropertyPath<String>(OlympiadDiplomaGen.P_SERIA, this);
            return _seria;
        }

    /**
     * @return Степень отличия в дипломе участника олимпиады. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.OlympiadDiploma#getDegree()
     */
        public OlympiadDiplomaDegree.Path<OlympiadDiplomaDegree> degree()
        {
            if(_degree == null )
                _degree = new OlympiadDiplomaDegree.Path<OlympiadDiplomaDegree>(L_DEGREE, this);
            return _degree;
        }

    /**
     * @return Название олимпиады. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.OlympiadDiploma#getOlympiad()
     */
        public PropertyPath<String> olympiad()
        {
            if(_olympiad == null )
                _olympiad = new PropertyPath<String>(OlympiadDiplomaGen.P_OLYMPIAD, this);
            return _olympiad;
        }

    /**
     * @return Предмет. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.OlympiadDiploma#getSubject()
     */
        public PropertyPath<String> subject()
        {
            if(_subject == null )
                _subject = new PropertyPath<String>(OlympiadDiplomaGen.P_SUBJECT, this);
            return _subject;
        }

    /**
     * @return Дата выдачи.
     * @see ru.tandemservice.uniec.entity.entrant.OlympiadDiploma#getIssuanceDate()
     */
        public PropertyPath<Date> issuanceDate()
        {
            if(_issuanceDate == null )
                _issuanceDate = new PropertyPath<Date>(OlympiadDiplomaGen.P_ISSUANCE_DATE, this);
            return _issuanceDate;
        }

    /**
     * @return Элемент административно-территориального деления.
     * @see ru.tandemservice.uniec.entity.entrant.OlympiadDiploma#getSettlement()
     */
        public AddressItem.Path<AddressItem> settlement()
        {
            if(_settlement == null )
                _settlement = new AddressItem.Path<AddressItem>(L_SETTLEMENT, this);
            return _settlement;
        }

    /**
     * @return Тип олимпиады.
     * @see ru.tandemservice.uniec.entity.entrant.OlympiadDiploma#getDiplomaType()
     */
        public OlympiadDiplomaType.Path<OlympiadDiplomaType> diplomaType()
        {
            if(_diplomaType == null )
                _diplomaType = new OlympiadDiplomaType.Path<OlympiadDiplomaType>(L_DIPLOMA_TYPE, this);
            return _diplomaType;
        }

        public Class getEntityClass()
        {
            return OlympiadDiploma.class;
        }

        public String getEntityName()
        {
            return "olympiadDiploma";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
