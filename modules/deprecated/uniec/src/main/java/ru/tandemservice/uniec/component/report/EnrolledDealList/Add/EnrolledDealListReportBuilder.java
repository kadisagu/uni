package ru.tandemservice.uniec.component.report.EnrolledDealList.Add;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.MergeType;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfElementFactory;
import org.tandemframework.rtf.node.RtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * User: amakarova
 * Date: 17.07.13
 * Отчет "Перечень дел зачисленных абитуриентов"
 */
public class EnrolledDealListReportBuilder
{

    private Model _model;
    private Session _session;

    private int numColumn = 7;
    private int num = 0;
    private int numReg = 1;
    private int numFio = 2;
    private int numEduTitle = 3;
    private int numEduSeria = 4;
    private int numEduNumber = 5;
    private int numOrig = 6;

    public EnrolledDealListReportBuilder(Model model, Session session)
    {
        _model = model;
        _session = session;
    }

    public DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    private byte[] buildReport()
    {
        List<PreliminaryEnrollmentStudent> studentList = getStudentList();
        if (studentList.size() == 0)
            throw new ApplicationException("Нет данных для построения отчета.");
        String sumAll = String.valueOf(studentList.size());
        Map<String, Map<EnrollmentDirection, List<PreliminaryEnrollmentStudent>>> ouMap = new HashMap<>();
        for (PreliminaryEnrollmentStudent preStudent : studentList)
        {
            EducationOrgUnit educationOrgUnit = preStudent.getEducationOrgUnit();
            String titleOu = educationOrgUnit.getFormativeOrgUnit().getPrintTitle() + " (" + educationOrgUnit.getTerritorialOrgUnit().getFullTitle() + ")";
            SafeMap.safeGet(SafeMap.safeGet(ouMap, titleOu, HashMap.class), preStudent.getRequestedEnrollmentDirection().getEnrollmentDirection(), ArrayList.class).add(preStudent);
        }
        List<String> formativeOuList = new ArrayList<>(ouMap.keySet());
        Collections.sort(formativeOuList);

        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniecDefines.SCRIPT_ENROLLED_DEAL_LIST);
        RtfDocument result = new RtfReader().read(templateDocument.getCurrentTemplate());

        RtfInjectModifier injectModifier = new RtfInjectModifier();

        List<IRtfElement> elementList = result.getElementList();
        RtfElement tableTitleOu = (RtfElement) UniRtfUtil.findElement(elementList, "titleOu");
        int tableTitleOuIndex = elementList.indexOf(tableTitleOu);
        result.getElementList().remove(tableTitleOuIndex);
        RtfTable tableT = (RtfTable) UniRtfUtil.findElement(elementList, "T");
        int tableTIndex = elementList.indexOf(tableT);
        result.getElementList().remove(tableTIndex);
        RtfTable tableLast = (RtfTable) UniRtfUtil.findElement(elementList, "last");
        int tableLastIndex = elementList.indexOf(tableLast);
        result.getElementList().remove(tableLastIndex);

        RtfTableModifier tableModifier = new RtfTableModifier();
        IRtfElementFactory elementFactory = RtfBean.getElementFactory();
        for (String titleOu : formativeOuList)
        {
            Map<EnrollmentDirection, List<PreliminaryEnrollmentStudent>> preStudentList = ouMap.get(titleOu);
            int indexOu = 0;
            List<EnrollmentDirection> directionList = new ArrayList<>(preStudentList.keySet());
            directionList.sort(CommonCollator.comparing(o -> o.getEducationOrgUnit().getEducationLevelHighSchool().getTitle(), true));

            for (EnrollmentDirection direction : directionList)
            {
                final List<String[]> rows = new ArrayList<>();
                int indexRequest = 1;
                List<PreliminaryEnrollmentStudent> preStudents = preStudentList.get(direction);
                preStudents.sort(CommonCollator.TITLED_WITH_ID_COMPARATOR);
                if (indexOu == 0)
                {
                    result.getElementList().add(tableTitleOu.getClone());
                    result.getElementList().add(elementFactory.createRtfControl(IRtfData.PAR));
                    result.getElementList().add(elementFactory.createRtfControl(IRtfData.PARD));
                }
                result.getElementList().add(tableT.getClone());

                List<Integer> rowIndex4Merge = new ArrayList<>(); // номер строки приложения документа об образовании для объединения с документом
                injectModifier.put("titleOu", titleOu);
                injectModifier.modify(result);
                for (PreliminaryEnrollmentStudent preStudent : preStudents)
                {
                    String[] data = new String[numColumn];
                    Entrant entrant = preStudent.getRequestedEnrollmentDirection().getEntrantRequest().getEntrant();
                    PersonEduInstitution lastEduInstitution = entrant.getPerson().getPersonEduInstitution();
                    data[num] = Integer.toString(indexRequest++);
                    data[numReg] = entrant.getPersonalNumber();
                    data[numFio] = preStudent.getTitle();
                    data[numEduTitle] = lastEduInstitution == null ? "" : lastEduInstitution.getDocumentType().getTitle();
                    data[numEduSeria] = lastEduInstitution == null ? "" : lastEduInstitution.getSeria();
                    data[numEduNumber] = lastEduInstitution == null ? "" : lastEduInstitution.getNumber();
                    data[numOrig] = preStudent.getRequestedEnrollmentDirection().isOriginalDocumentHandedIn() ? "п" : "к";
                    rows.add(data);

                    if (lastEduInstitution != null)
                    {
                        String seriaApplication = lastEduInstitution.getSeriaApplication();
                        String numberApplication = lastEduInstitution.getNumberApplication();
                        if (!StringUtils.isEmpty(seriaApplication) || !StringUtils.isEmpty(numberApplication))
                        {
                            data = new String[numColumn];
                            data[numEduTitle] = "Приложение";
                            data[numEduSeria] = seriaApplication == null ? "" : seriaApplication;
                            data[numEduNumber] = numberApplication == null ? "" : numberApplication;
                            rowIndex4Merge.add(rows.size());
                            rows.add(data);
                        }
                    }
                }
                String[] hsData = new String[1];
                hsData[0] = direction.getEducationOrgUnit().getEducationLevelHighSchool().getTitle();
                tableModifier.put("T1", new String[][]{hsData});
                tableModifier.put("T", rows.toArray(new String[][]{}));
                tableModifier.put("T", new RtfRowIntercepterBase()
                {
                    @Override
                    public void beforeModify(RtfTable table, int currentRowIndex)
                    {
                        List<RtfCell> cellList = table.getRowList().get(currentRowIndex).getCellList();
                        SharedRtfUtil.setCellAlignment(cellList.get(numFio), IRtfData.QC, IRtfData.QL);
                        SharedRtfUtil.setCellAlignment(cellList.get(numEduTitle), IRtfData.QL, IRtfData.QC);
                    }

                    @Override
                    public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
                    {
                        rowIndex4Merge.forEach(number -> {
                            newRowList.get(startIndex + number - 1).getCellList().get(num).setMergeType(MergeType.VERTICAL_MERGED_FIRST);
                            newRowList.get(startIndex + number).getCellList().get(num).setMergeType(MergeType.VERTICAL_MERGED_NEXT);
                            newRowList.get(startIndex + number - 1).getCellList().get(numReg).setMergeType(MergeType.VERTICAL_MERGED_FIRST);
                            newRowList.get(startIndex + number).getCellList().get(numReg).setMergeType(MergeType.VERTICAL_MERGED_NEXT);
                            newRowList.get(startIndex + number - 1).getCellList().get(numFio).setMergeType(MergeType.VERTICAL_MERGED_FIRST);
                            newRowList.get(startIndex + number).getCellList().get(numFio).setMergeType(MergeType.VERTICAL_MERGED_NEXT);
                            newRowList.get(startIndex + number - 1).getCellList().get(numOrig).setMergeType(MergeType.VERTICAL_MERGED_FIRST);
                            newRowList.get(startIndex + number).getCellList().get(numOrig).setMergeType(MergeType.VERTICAL_MERGED_NEXT);
                        });
                    }
                });
                indexOu++;
                tableModifier.modify(result);
            }
            result.getElementList().add(elementFactory.createRtfControl(IRtfData.PARD));
            result.getElementList().add(elementFactory.createRtfControl(IRtfData.PAR));
        }
        result.getElementList().add(elementFactory.createRtfControl(IRtfData.PAR));
        result.addElement(tableLast);
        List<String[]> rowsLast = new ArrayList<>();
        String[] dataLast1 = new String[3];
        String[] dataLastEmpty = new String[3];
        for (int i = 0; i < 3; i++)
        {
            dataLast1[i] = "______________________________";
            dataLastEmpty[i] = "";
        }
        String[] dataLast2 = new String[3];
        dataLast2[0] = "должность";
        dataLast2[1] = "подпись";
        dataLast2[2] = "ФИО";
        rowsLast.add(dataLast1);
        rowsLast.add(dataLast2);
        rowsLast.add(dataLastEmpty);
        rowsLast.add(dataLastEmpty);
        rowsLast.add(dataLast1);
        rowsLast.add(dataLast2);
        tableModifier.put("last", rowsLast.toArray(new String[][]{}));
        tableModifier.modify(result);

        injectModifier.put("sumAll", sumAll);
        injectModifier.put("year", DateFormatter.STRING_MONTHS_AND_QUOTES.format(_model.getReport().getFormingDate()));
        injectModifier.put("orders", getOrders(studentList));

        PreliminaryEnrollmentStudent first = studentList.get(0);
        injectModifier.put("eduProgramKind", getEduProgramKind(first));
        injectModifier.put("developForm_G", getDevelopForm(first));
        injectModifier.put("compensationType", first.getCompensationType().isBudget()
                ? "за счет средств Федерального бюджета"
                : "на места с оплатой стоимости обучения");

        injectModifier.modify(result);
        return RtfUtil.toByteArray(result);
    }

    protected String getEduProgramKind(PreliminaryEnrollmentStudent first)
    {
        Qualifications qualification = first.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getQualification();
        String code = qualification == null ? "" : qualification.getCode();
        switch (code)
        {
            case QualificationsCodes.BAKALAVR:
                return "бакалавриата";
            case QualificationsCodes.MAGISTR:
                return "магистратуры";
            case QualificationsCodes.ASPIRANTURA:
                return "подготовки научно-педагогических кадров в аспирантуре";
            case QualificationsCodes.BAZOVYY_UROVEN_S_P_O:
            case QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O:
                return "среднего профессионального образования";
            default:
                return "";
        }
    }

    protected String getDevelopForm(PreliminaryEnrollmentStudent first)
    {
        String developForm = first.getEducationOrgUnit().getDevelopForm().getCode();
        switch (developForm){
            case DevelopFormCodes.FULL_TIME_FORM: return "очной";
            case DevelopFormCodes.CORESP_FORM: return "заочной";
            case DevelopFormCodes.PART_TIME_FORM: return "очно-заочной";
            case DevelopFormCodes.EXTERNAL_FORM: return "экстернат";
            case DevelopFormCodes.APPLICANT_FORM: return "самостоятельной";
            default: return "";
        }
    }

    protected RtfString getOrders(List<PreliminaryEnrollmentStudent> students)
    {
        RtfString rtfString = new RtfString();
        students.stream()
                .map(PreliminaryEnrollmentStudent::getEnrollmentOrder)
                .distinct()
                .map(order -> "Пр. № " + order.getNumber() + " от " + new DateFormatter().format(order.getCommitDate()) + " г.")
                .forEach(s -> rtfString.append(s).append(IRtfData.PAR));
        return rtfString;
    }

    public List<PreliminaryEnrollmentStudent> getStudentList()
    {
        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(EnrollmentExtract.class, "ee");
        builder.joinPath(DQLJoinType.inner, "ee." + IAbstractExtract.L_ENTITY, "p");
        builder.column("p");
        builder.joinPath(DQLJoinType.inner, "p." + PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, "d");
        builder.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "request");
        builder.joinPath(DQLJoinType.inner, "p." + PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT, "ou");
        builder.where(DQLExpressions.eq(DQLExpressions.property("ee." + IAbstractExtract.P_COMMITTED), value(true)));

        if (_model.isCompensationTypeActive())
            builder.where(DQLExpressions.eq(DQLExpressions.property("p." + PreliminaryEnrollmentStudent.L_COMPENSATION_TYPE), DQLExpressions.value(_model.getReport().getCompensationType())));
        if (_model.isParallelActive() && _model.getParallel().isTrue())
            builder.where(DQLExpressions.eq(DQLExpressions.property("p." + PreliminaryEnrollmentStudent.P_PARALLEL), DQLExpressions.value(Boolean.FALSE)));
        if (_model.isEntrantEnrollmentOrderTypeActive())
            builder.where(DQLExpressions.in(DQLExpressions.property("p." + PreliminaryEnrollmentStudent.L_ENTRANT_ENROLLMENT_ORDER_TYPE), _model.getEntrantEnrollmentOrderTypeList()));
        if (_model.isEnrollmentOrderActive())
        {
            builder.where(DQLExpressions.in(DQLExpressions.property("ee." + IAbstractExtract.L_PARAGRAPH + "." + IAbstractParagraph.L_ORDER), _model.getEnrollmentOrderList()));
        }
        builder.where(DQLExpressions.ne(DQLExpressions.property("d." + RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE), UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));

        patchEduOrgUnit(builder, "p." + PreliminaryEnrollmentStudent.L_STUDENT_CATEGORY);

        return builder.createStatement(_session).list();
    }

    private void patchEduOrgUnit(DQLSelectBuilder builder, String studentCategoryAlias)
    {
        builder.joinPath(DQLJoinType.inner, "request." + EntrantRequest.L_ENTRANT, "entrant");
        builder.joinPath(DQLJoinType.inner, "entrant." + Entrant.L_PERSON, "person");
        builder.joinPath(DQLJoinType.inner, "person." + Person.L_IDENTITY_CARD, "card");
        builder.joinPath(DQLJoinType.inner, "ou." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "hs");
        builder.joinPath(DQLJoinType.inner, "hs." + EducationLevelsHighSchool.L_EDUCATION_LEVEL, "el");
        builder.where(DQLExpressions.eq(DQLExpressions.property("entrant." + Entrant.P_ARCHIVAL), DQLExpressions.value(Boolean.FALSE)));
        builder.where(DQLExpressions.eq(DQLExpressions.property("entrant." + Entrant.L_ENROLLMENT_CAMPAIGN), DQLExpressions.value(_model.getReport().getEnrollmentCampaign())));
        if (_model.isStudentCategoryActive())
            builder.where(DQLExpressions.in(DQLExpressions.property(studentCategoryAlias), _model.getStudentCategoryList()));
        if (_model.isQualificationActive())
            builder.where(DQLExpressions.in(DQLExpressions.property("el." + EducationLevels.L_QUALIFICATION), _model.getQualificationList()));
        if (_model.isFormativeOrgUnitActive())
            builder.where(DQLExpressions.in(DQLExpressions.property("ou." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT), _model.getFormativeOrgUnitList()));
        if (_model.isTerritorialOrgUnitActive())
        {
            if (_model.getTerritorialOrgUnitList().isEmpty())
                builder.where(DQLExpressions.isNull(DQLExpressions.property("ou." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT)));
            else
                builder.where(DQLExpressions.in(DQLExpressions.property("ou." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT), _model.getTerritorialOrgUnitList()));
        }
        if (_model.isDevelopFormActive())
            builder.where(DQLExpressions.in(DQLExpressions.property("ou." + EducationOrgUnit.L_DEVELOP_FORM), _model.getDevelopFormList()));
        if (_model.isDevelopConditionActive())
            builder.where(DQLExpressions.in(DQLExpressions.property("ou." + EducationOrgUnit.L_DEVELOP_CONDITION), _model.getDevelopConditionList()));
        if (_model.isDevelopTechActive())
            builder.where(DQLExpressions.in(DQLExpressions.property("ou." + EducationOrgUnit.L_DEVELOP_TECH), _model.getDevelopTechList()));
        if (_model.isDevelopPeriodActive())
            builder.where(DQLExpressions.in(DQLExpressions.property("ou." + EducationOrgUnit.L_DEVELOP_PERIOD), _model.getDevelopPeriodList()));
        if (!_model.isIncludeForeignPerson())
        {
            builder.where(DQLExpressions.eq(DQLExpressions.property("card." + IdentityCard.citizenship().code()), DQLExpressions.value(IKladrDefines.RUSSIA_COUNTRY_CODE)));
        }
    }
}
