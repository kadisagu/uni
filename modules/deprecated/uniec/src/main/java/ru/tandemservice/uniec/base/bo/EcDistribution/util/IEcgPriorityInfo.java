/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec.base.bo.EcDistribution.util;

import java.util.List;

/**
 * @author Vasily Zhukov
 * @since 29.05.2012
 */
public interface IEcgPriorityInfo
{
    /**
     * @return идентификатор абитуриента
     */
    Long getEntrantId();

    /**
     * @return идентификаторы НП приоритетов
     */
    List<Long> getPriorityIdList();

    /**
     * @return сокр. названия НП приоритетов 
     */
    List<String> getPriorityList();
}
