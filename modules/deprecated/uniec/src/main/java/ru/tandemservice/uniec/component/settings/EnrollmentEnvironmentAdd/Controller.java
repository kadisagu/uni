package ru.tandemservice.uniec.component.settings.EnrollmentEnvironmentAdd;

import java.io.ByteArrayOutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uniec.ws.enrollment.EnrollmentEnvironmentNode;
import ru.tandemservice.uniec.ws.enrollment.IEnrollmentServiceDao;

/**
 * @author Vasily Zhukov
 * @since 17.02.2011
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.setSettings(component.getSettings());
        getDao().prepare(model);
    }

    public void onClickApply(IBusinessComponent component) throws Exception
    {
        Model model = getModel(component);

        ByteArrayOutputStream result = new ByteArrayOutputStream();
        JAXBContext context = JAXBContext.newInstance(EnrollmentEnvironmentNode.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(IEnrollmentServiceDao.INSTANCE.get().getEnrollmentEnvironmentData(model.getEnrollmentCampaign().getTitle()), result);

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().xml().fileName("enrollmentEnvironment.soap.xml").document(result), true);
    }
}
