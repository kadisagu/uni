/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.settings.ExamGroupLogicList;

import java.util.List;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.ExamGroupLogic;
import ru.tandemservice.uniec.entity.entrant.CurrentExamGroupLogic;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

/**
 * @author Vasily Zhukov
 * @since 28.05.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilterWithExamGroupAutoForming(model, getSession());
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        // приемная кампании
        EnrollmentCampaign enrollmentCampaign = model.getEnrollmentCampaign();

        // нельзя изменить алгоритм, если в приемной кампании экзаменационные группы формируются вручную
        boolean disabled = enrollmentCampaign == null || !enrollmentCampaign.isFormingExamGroupAuto();

        // нельзя изменить алгоритм, если уже есть экзаменационные группы
        if (!disabled) disabled = !UniecDAOFacade.getExamGroupSetDao().isCanChangeExamGroupLogic(enrollmentCampaign);

        // получаем текущую логику работы экзаменационных групп
        MQBuilder builder = new MQBuilder(CurrentExamGroupLogic.ENTITY_CLASS, "g");
        builder.add(MQExpression.eq("g", CurrentExamGroupLogic.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
        CurrentExamGroupLogic logic = (CurrentExamGroupLogic) builder.uniqueResult(getSession());

        List<ExamGroupLogic> list = getList(ExamGroupLogic.class, ExamGroupLogic.P_TITLE);
        model.getDataSource().setCountRow(list.size());
        UniBaseUtils.createPage(model.getDataSource(), list);

        for (ViewWrapper viewWrapper : ViewWrapper.getPatchedList(model.getDataSource()))
        {
            viewWrapper.setViewProperty(Model.EXAM_GROUP_LOGIC_CURRENT_PROPERTY, logic != null && viewWrapper.getId().equals(logic.getExamGroupLogic().getId()));
            viewWrapper.setViewProperty(Model.EXAM_GROUP_LOGIC_DISABLED_PROPERTY, disabled);
        }
    }

    @Override
    public void updateExamGroupLogic(Model model, Long examGroupLogicId, boolean current)
    {
        // приемная кампании
        EnrollmentCampaign enrollmentCampaign = model.getEnrollmentCampaign();

        // нельзя изменить алгоритм, если в приемной кампании экзаменационные группы формируются вручную
        if (!enrollmentCampaign.isFormingExamGroupAuto()) return;

        // нельзя изменить алгоритм, если уже есть экзаменационные группы
        boolean canChangeExamGroupLogic = UniecDAOFacade.getExamGroupSetDao().isCanChangeExamGroupLogic(enrollmentCampaign);

        // уже нельзя изменять алгоритм
        if (!canChangeExamGroupLogic) return;

        // получаем текущую логику работы экзаменационных групп
        MQBuilder builder = new MQBuilder(CurrentExamGroupLogic.ENTITY_CLASS, "g");
        builder.add(MQExpression.eq("g", CurrentExamGroupLogic.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
        CurrentExamGroupLogic logic = (CurrentExamGroupLogic) builder.uniqueResult(getSession());

        if (current)
        {
            // установка текущей логики, либо используем уже созданные объект, либо создаем новый
            if (logic == null)
                logic = new CurrentExamGroupLogic();

            logic.setEnrollmentCampaign(enrollmentCampaign);
            logic.setExamGroupLogic(getNotNull(ExamGroupLogic.class, examGroupLogicId));

            getSession().saveOrUpdate(logic);
        } else
        {
            if (logic != null && logic.getExamGroupLogic().getId().equals(examGroupLogicId))
                getSession().delete(logic);
        }
    }
}
