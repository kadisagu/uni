/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.settings.AbstractMatrixSettingsComponent;

import java.util.List;

import org.tandemframework.core.common.IIdentifiable;
import org.tandemframework.core.settings.IDataSettings;

import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

/**
 * @author agolubenko
 * @since 19.06.2008
 */
public abstract class Model<Vertical extends IIdentifiable, Horizontal extends IIdentifiable, FieldType> extends org.tandemframework.shared.commonbase.base.util.AbstractMatrixComponent.Model<Vertical, Horizontal, FieldType> implements IEnrollmentCampaignSelectModel
{
    private List<EnrollmentCampaign> _enrollmentCampaignList;
    private IDataSettings _settings;

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return (EnrollmentCampaign) getSettings().get(ENROLLMENT_CAMPAIGN_FILTER_NAME);
    }
    
    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        getSettings().set(ENROLLMENT_CAMPAIGN_FILTER_NAME, enrollmentCampaign);
    }

    @Override
    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }
}
