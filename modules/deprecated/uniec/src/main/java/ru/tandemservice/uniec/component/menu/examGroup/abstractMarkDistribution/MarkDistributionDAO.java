/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.menu.examGroup.abstractMarkDistribution;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.dao.examgroup.IAbstractExamGroupLogic;
import ru.tandemservice.uniec.entity.catalog.EntrantAbsenceNote;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.util.EntrantFilterUtil;
import ru.tandemservice.uniec.util.EntrantMarkUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * @author Vasily Zhukov
 * @since 31.05.2010
 */
public abstract class MarkDistributionDAO<Model extends MarkDistributionModel> extends UniDao<Model> implements IMarkDistributionDAO<Model>
{
    @Override
    public final void prepare(final Model model)
    {
        // селект с приемными кампаниями
        EntrantFilterUtil.prepareEnrollmentCampaignFilterWithExamGroupAutoFormingAndAlgorithm(model, getSession());

        // вначале определяем, какой бизнес компонент требуется для отображения списка экзаменационных групп
        IAbstractExamGroupLogic logic = UniecDAOFacade.getExamGroupSetDao().getCurrentExamGroupLogic(model.getEnrollmentCampaign());

        if (logic != null && !getClass().getPackage().getName().equals(logic.getMarkDistributionBusinessComponent()))
        {
            // если сейчас загружен не тот бизнес компонент, то делаем редирект на правильный
            model.setRedirectComponentName(logic.getMarkDistributionBusinessComponent());
        } else
        {
            // наборы экзаменов
            model.setExamGroupSetModel(new FullCheckSelectModel()
            {
                @Override
                public ListResult findValues(String filter)
                {
                    if (model.getEnrollmentCampaign() == null) return ListResult.getEmpty();
                    MQBuilder builder = new MQBuilder(ExamGroupSet.ENTITY_CLASS, "s");
                    builder.add(MQExpression.eq("s", ExamGroupSet.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
                    builder.addOrder("s", ExamGroupSet.P_BEGIN_DATE, OrderDirection.desc);

                    return new ListResult<ExamGroupSet>(builder.<ExamGroupSet>getResultList(getSession()));
                }
            });

            // создание дополнительных фильтров
            prepareModel(model);

            // селект для выбора группы
            model.setGroupModel(new FullCheckSelectModel()
            {
                @Override
                public ListResult findValues(String filter)
                {
                    // нет набора, нет списка экзаменационных групп
                    ExamGroupSet examGroupSet = (ExamGroupSet) model.getSettings().get("examGroupSet");
                    if (examGroupSet == null) return ListResult.getEmpty();

                    // создаем список экзаменационных групп, применяя фильтры
                    MQBuilder builder = new MQBuilder(ExamGroup.ENTITY_CLASS, "g");
                    builder.add(MQExpression.eq("g", ExamGroup.L_EXAM_GROUP_SET, examGroupSet));
                    prepareGroupList(builder, "g", model);
                    return new ListResult<ExamGroup>(builder.<ExamGroup>getResultList(getSession()));
                }
            });

            // селект для выбора дисциплины
            model.setDisciplineModel(new FullCheckSelectModel()
            {
                @Override
                public ListResult findValues(String filter)
                {
                    // нет группы, нет списка дисциплин
                    ExamGroup examGroup = (ExamGroup) model.getSettings().get("examGroup");
                    if (examGroup == null) return ListResult.getEmpty();

                    // создаем мн-во всех дисциплин по форме сдачи
                    Set<PairKey<Discipline2RealizationWayRelation, SubjectPassForm>> disciplineList = UniecDAOFacade.getExamGroupSetDao().getExamPassDisciplineList(examGroup).keySet();

                    List<DisciplineWrapper> result = new ArrayList<DisciplineWrapper>();
                    for (PairKey<Discipline2RealizationWayRelation, SubjectPassForm> key : disciplineList)
                        result.add(new DisciplineWrapper(key.getFirst(), key.getSecond()));
                    return new ListResult<DisciplineWrapper>(result);
                }
            });
        }
    }

    @Override
    public final void prepareListDataSource(Model model)
    {
        DynamicListDataSource<EntrantDisciplineMarkWrapper> dataSource = model.getDataSource();
        List<EntrantDisciplineMarkWrapper> list = new ArrayList<EntrantDisciplineMarkWrapper>();

        if (model.getDiscipline() != null)
        {
            // экзаменационная группа
            ExamGroup examGroup = (ExamGroup) model.getSettings().get("examGroup");

            // экзаменационный лист создан на абитуриента
            boolean formingForEntrant = UniecDefines.EXAMLIST_FORMING_FEATURE_FOR_ENTRANT.equals(model.getEnrollmentCampaign().getEntrantExamListsFormingFeature().getCode());

            // получаем список дисциплин для сдачи
            MQBuilder builder = new MQBuilder(ExamPassDiscipline.ENTITY_CLASS, "d");
            builder.addDomain("r", ExamGroupRow.ENTITY_CLASS);
            builder.add(MQExpression.eq("d", ExamPassDiscipline.L_ENROLLMENT_CAMPAIGN_DISCIPLINE, model.getDiscipline().getDiscipline()));
            builder.add(MQExpression.eq("d", ExamPassDiscipline.L_SUBJECT_PASS_FORM, model.getDiscipline().getSubjectPassForm()));
            builder.add(MQExpression.eqProperty("d", ExamPassDiscipline.L_ENTRANT_EXAM_LIST + "." + EntrantExamList.L_ENTRANT + ".id", "r", ExamGroupRow.L_ENTRANT_REQUEST + (formingForEntrant ? "." + EntrantRequest.L_ENTRANT : "") + ".id"));
            builder.add(MQExpression.eq("r", ExamGroupRow.L_EXAM_GROUP, examGroup));
            builder.add(MQExpression.eq("r", ExamGroupRow.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, Boolean.FALSE));

            builder.getSelectAliasList().clear();
            builder.setNeedDistinct(true);
            builder.addSelect("d");
            builder.addSelect("r", new Object[]{ExamGroupRow.L_ENTRANT_REQUEST});

            List<Object[]> data = builder.getResultList(getSession());

            for (Object[] row : data)
            {
                ExamPassDiscipline examPassDiscipline = (ExamPassDiscipline) row[0];
                ExamPassMark examPassMark = get(ExamPassMark.class, ExamPassMark.L_EXAM_PASS_DISCIPLINE, examPassDiscipline);
                // examPassMark могла уже измениться, надо загрузить актуальную оценку
                if (examPassMark != null) getSession().refresh(examPassMark);
                String mark = examPassMark == null ? "" : examPassMark.getEntrantAbsenceNote() != null ? "н" : DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS.format(examPassMark.getMark());
                String paperCode = examPassMark == null ? "" : examPassMark.getPaperCode();
                String examiners = examPassMark == null ? ""  : examPassMark.getExaminers();
                list.add(new EntrantDisciplineMarkWrapper(examPassDiscipline, (EntrantRequest) row[1], mark, paperCode, examiners));
            }
        }
        dataSource.setCountRow(list.size());
        Collections.sort(list, ITitled.TITLED_COMPARATOR);
        UniBaseUtils.createPage(dataSource, list);
    }

    @Override
    public final void update(Model model)
    {
        final Session session = getSession();

        List<EntrantDisciplineMarkWrapper> wrapperList = model.getDataSource().getEntityList();
        List<EntrantAbsenceNote> entrantAbsenceNoteList = getCatalogItemList(EntrantAbsenceNote.class);
        for (EntrantDisciplineMarkWrapper wrapper : wrapperList)
        {
            String mark = StringUtils.trimToNull(wrapper.getMark());

            // получаем оценку по дисциплине для сдачи + апелляцию, если она есть
            ExamPassMark examPassMark = get(ExamPassMark.class, ExamPassMark.L_EXAM_PASS_DISCIPLINE, wrapper.getExamPassDiscipline());
            ExamPassMarkAppeal examPassMarkAppeal = null;
            if (examPassMark != null)
                examPassMarkAppeal = get(ExamPassMarkAppeal.class, ExamPassMarkAppeal.L_EXAM_PASS_MARK, examPassMark);

            // если на форме поставили пусто, то удаляем оценку для сдачи (если была апелляция по ней, то она тоже удалится)
            if (StringUtils.isEmpty(mark))
            {
                if (examPassMarkAppeal != null)
                    session.delete(examPassMarkAppeal);
                if (examPassMark != null)
                    session.delete(examPassMark);
            } else
            {
                if (examPassMark == null)
                {
                    examPassMark = new ExamPassMark();
                    examPassMark.setExamPassDiscipline(wrapper.getExamPassDiscipline());
                }
                EntrantMarkUtil.parseAndSetMark(examPassMark, entrantAbsenceNoteList, mark);
                examPassMark.setPaperCode(wrapper.getPaperCode());
                examPassMark.setExaminers(wrapper.getExaminers());
                session.saveOrUpdate(examPassMark);
            }
        }
    }

    // подготовка модели, создание кастомных фильтров

    protected abstract void prepareModel(Model model);

    // применение кастомных фильтров (фильтры должны быть всегда, иначе в чем смысл экзаменационных групп)

    protected abstract void prepareGroupList(MQBuilder builder, String alias, Model model);
}
