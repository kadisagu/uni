/* $Id$ */
package ru.tandemservice.uniec.component.menu.SplitOrdersList;

import org.apache.cxf.common.util.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.commons.IMoveStudentMQBuilder;
import ru.tandemservice.movestudent.component.commons.MoveStudentMQBuilder;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.StudentExtractTypeToGroup;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentListParagraph;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.codes.StudentExtractGroupCodes;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.dao.MoveDaoFacade;
import ru.tandemservice.unimove.entity.catalog.OrderStates;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 20.08.2012
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _orderDescriptionRegistry = new OrderDescriptionRegistry(IMoveStudentMQBuilder.ORDER_ALIAS);

    static
    {
        _orderDescriptionRegistry.setOrders(Model.ORG_UNI_FULL_TITLE, new OrderDescription(IMoveStudentMQBuilder.ORDER_ALIAS, new String[]{StudentListOrder.L_ORG_UNIT, OrgUnit.P_TITLE}), new OrderDescription(IMoveStudentMQBuilder.ORDER_ALIAS, new String[]{StudentListOrder.L_ORG_UNIT, OrgUnit.L_ORG_UNIT_TYPE, OrgUnitType.P_TITLE}));
    }

    @Override
    public void prepare(Model model)
    {
        model.setEducationYearList(new EducationYearModel());
        model.setOrderStateList(getCatalogItemList(OrderStates.class));

        model.setOrderTypeList(new BaseSingleSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                DQLSelectBuilder subBuilder = new DQLSelectBuilder()
                        .fromEntity(StudentExtractTypeToGroup.class, "eg")
                        .column(property(StudentExtractTypeToGroup.type().id().fromAlias("eg")))
                        .where(eq(property(StudentExtractTypeToGroup.type().active().fromAlias("eg")), value(Boolean.TRUE)))
                        .where(eq(property(StudentExtractTypeToGroup.group().code().fromAlias("eg")), value(StudentExtractGroupCodes.PROFILE_DISTRIBUTION)));

                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(StudentExtractType.class, "e").column(property(StudentExtractType.parent().fromAlias("e")))
                        .where(eq(property(StudentExtractType.parent().parent().code().fromAlias("e")), value(StudentExtractTypeCodes.LIST_ORDER)))
                        .where(in(property(StudentExtractType.id().fromAlias("e")), subBuilder.buildQuery()));

                if(!StringUtils.isEmpty(filter))
                    builder.where(likeUpper(property(StudentExtractType.parent().title().fromAlias("e")), value(CoreStringUtils.escapeLike(filter, true))));

                builder.order(property(StudentExtractType.title().fromAlias("e")));

                return new ListResult<>(builder.createStatement(getSession()).list());
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                IEntity entity = get(StudentExtractType.class, (Long) primaryKey);
                if (findValues("").getObjects().contains(entity))
                    return entity;
                return null;
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((StudentExtractType) value).getTitle();
            }
        });
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareListDataSource(Model model)
    {
        // init datasource
        IMoveStudentMQBuilder builder = MoveStudentMQBuilder.createOrderMQBuilder(StudentListOrder.class, model.getSettings());

        List<Long> orderTypesId = new DQLSelectBuilder().fromEntity(StudentExtractTypeToGroup.class, "eg")
                .column(property(StudentExtractTypeToGroup.type().parent().id().fromAlias("eg")))
                .where(isNotNull(property(StudentExtractTypeToGroup.type().parent().fromAlias("eg"))))
                .where(eq(property(StudentExtractTypeToGroup.group().code().fromAlias("eg")), value("17")))
                .createStatement(getSession()).list();

        // apply user defined restrictions
        //builder.getMQBuilder().add(MQExpression.notEq(IMoveStudentMQBuilder.STATE_ALIAS, ICatalogItem.CATALOG_ITEM_CODE, UnimoveDefines.CATALOG_ORDER_STATE_FINISHED));
        builder.getMQBuilder().add(MQExpression.in(IMoveStudentMQBuilder.ORDER_ALIAS, StudentListOrder.type().id().s(), orderTypesId));

        // apply filters
        builder.applyListOrderEducationYear();
        builder.applyOrderCommitDate();
        builder.applyOrderCreateDate();
        builder.applyOrderNumber();
        builder.applyOrderState();
        builder.applyListOrderType();

        // apply order
        _orderDescriptionRegistry.applyOrder(builder.getMQBuilder(), model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), builder.getMQBuilder(), getSession());

        // get statictics for view properties
        String hql = "select " + IAbstractParagraph.L_ORDER + ".id, count(*) from " + StudentListParagraph.ENTITY_CLASS + " group by " + IAbstractParagraph.L_ORDER;
        Map<Long, Integer> id2count = new HashMap<>();
        for (Object[] row : (List<Object[]>) getSession().createQuery(hql).list())
        {
            Long id = (Long) row[0];
            Number count = (Number) row[1];
            id2count.put(id, count == null ? 0 : count.intValue());
        }

        // inject view properties
        for (ViewWrapper wrapper : ViewWrapper.getPatchedList(model.getDataSource()))
        {
            Integer value = id2count.get(wrapper.getId());
            wrapper.setViewProperty(AbstractStudentOrder.P_COUNT_PARAGRAPH, value == null ? 0 : value);
            wrapper.setViewProperty("disabledPrint", value == null);
        }
    }

    @Override
    public void deleteRow(IBusinessComponent context)
    {
        IAbstractOrder order = get((Long) context.getListenerParameter());
        MoveDaoFacade.getMoveDao().deleteOrderWithExtracts(order);
    }
}
