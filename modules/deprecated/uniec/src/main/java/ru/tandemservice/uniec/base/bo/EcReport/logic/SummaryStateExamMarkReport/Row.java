/**
 *$Id:$
 */
package ru.tandemservice.uniec.base.bo.EcReport.logic.SummaryStateExamMarkReport;

import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.util.EntrantDataUtil;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

/**
 * Класс агрегирующий данные для строчки отчета. Содержит методы вычисляющие значения отчета.
 *
 * @author Alexander Shaburov
 * @since 30.07.12
 */
public class Row
{
    // constructor

    /**
     * Конструктор. Иницализирует список НВП.
     * @param dataUtil инициализированная утиль. ВНП в утили - это все ВНП на основе которых строится отчет
     */
    public Row(EntrantDataUtil dataUtil)
    {
        _requestedEnrollmentDirectionList = new LinkedList<RequestedEnrollmentDirection>();
        _dataUtil = dataUtil;
    }

    // fields

    private List<RequestedEnrollmentDirection> _requestedEnrollmentDirectionList; // список НВП строки
    private EntrantDataUtil _dataUtil;

    // methods

    /**
     * Добавляет ВНП в список ВНП строки.
     * @param direction ВНП
     * @return this
     */
    public Row addDirection(RequestedEnrollmentDirection direction)
    {
        _requestedEnrollmentDirectionList.add(direction);

        return this;
    }

    /**
     * Название НПВ.
     */
    public String getEducationLevelHighSchoolTitle()
    {
        if (_requestedEnrollmentDirectionList.isEmpty())
            return "";

        return _requestedEnrollmentDirectionList.get(0).getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getTitle();
    }

    /**
     * Специалитет, бакалавриат или магистратура.
     */
    public String getLevelTypeTitle()
    {
        if (_requestedEnrollmentDirectionList.isEmpty())
            return "";

        StructureEducationLevels levelType = _requestedEnrollmentDirectionList.get(0).getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType();

        if (levelType.isMaster())
            return "магистратура";
        else if (levelType.isBachelor())
            return "бакалавриат";
        else
            return "специалитет";
    }

    /**
     * Форма освоения и Вид возмещения затрат (через запятую).
     */
    public String getDevelopFormAndCompensationTypeString()
    {
        if (_requestedEnrollmentDirectionList.isEmpty())
            return "";

        RequestedEnrollmentDirection direction = _requestedEnrollmentDirectionList.get(0);

        return direction.getEnrollmentDirection().getEducationOrgUnit().getDevelopForm().getTitle() + ", " + direction.getCompensationType().getTitle();
    }

    /**
     * Максимальный рейтинг ВНП (сумма баллов).
     */
    public double getMaxRating()
    {
        Double result = 0d;
        for (RequestedEnrollmentDirection direction : _requestedEnrollmentDirectionList)
            result = Math.max(_dataUtil.getFinalMark(direction), result);

        BigDecimal x = new java.math.BigDecimal(result);
        x = x.setScale(2, BigDecimal.ROUND_HALF_UP);
        result = x.doubleValue();

        return result;
    }

    /**
     * Минимальный рейтинг ВНП (сумма баллов).
     */
    public double getMinRating()
    {
        Double result = Double.MAX_VALUE;
        for (RequestedEnrollmentDirection direction : _requestedEnrollmentDirectionList)
            result = Math.min(_dataUtil.getFinalMark(direction), result);

        BigDecimal x = new java.math.BigDecimal(result);
        x = x.setScale(2, BigDecimal.ROUND_HALF_UP);
        result = x.doubleValue();

        return result.equals(Double.MAX_VALUE) ? 0d : result;
    }

    /**
     * Средний рейтинг ВНП (сумма баллов).
     */
    public double getAvrRating()
    {
        Double result = 0.0d;
        for (RequestedEnrollmentDirection direction : _requestedEnrollmentDirectionList)
            result += _dataUtil.getFinalMark(direction);

        result =_requestedEnrollmentDirectionList.isEmpty() ? 0d : result / _requestedEnrollmentDirectionList.size();

        BigDecimal x = new java.math.BigDecimal(result);
        x = x.setScale(2, BigDecimal.ROUND_HALF_UP);
        result = x.doubleValue();

        return result;
    }

    // Getters & Setters

    public List<RequestedEnrollmentDirection> getRequestedEnrollmentDirectionList()
    {
        return _requestedEnrollmentDirectionList;
    }

    public void setRequestedEnrollmentDirectionList(List<RequestedEnrollmentDirection> requestedEnrollmentDirectionList)
    {
        _requestedEnrollmentDirectionList = requestedEnrollmentDirectionList;
    }

    public EntrantDataUtil getDataUtil()
    {
        return _dataUtil;
    }

    public void setDataUtil(EntrantDataUtil dataUtil)
    {
        _dataUtil = dataUtil;
    }
}
