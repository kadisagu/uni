/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.entrant.ExcludePassFormTab;

import java.util.Collections;
import java.util.List;

import org.tandemframework.core.common.IGrouped;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IdentifiableWrapper;

import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

/**
 * @author Vasily Zhukov
 * @since 26.07.2010
 */
class EnrollmentDirectionGroup extends IdentifiableWrapper<RequestedEnrollmentDirection> implements IGrouped
{
    private static final long serialVersionUID = 1L;
    private List<ChosenEntranceDiscipline> _chosenEntranceDisciplines;

    EnrollmentDirectionGroup(RequestedEnrollmentDirection direction, List<ChosenEntranceDiscipline> chosenEntranceDisciplines)
    {
        super(direction, getDirectionTitle(direction));
        _chosenEntranceDisciplines = chosenEntranceDisciplines;
    }

    @Override
    public List<? extends IGrouped> getGroupList()
    {
        return Collections.emptyList();
    }

    @Override
    public List<? extends ITitled> getGroupItemList()
    {
        return _chosenEntranceDisciplines;
    }

    @Override
    public String getName()
    {
        return getId() + "";
    }

    private static String getDirectionTitle(RequestedEnrollmentDirection direction)
    {
        EducationOrgUnit educationOrgUnit = direction.getEnrollmentDirection().getEducationOrgUnit();
        return direction.getTitle() + " (" + educationOrgUnit.getDevelopForm().getTitle() + ", " + educationOrgUnit.getDevelopCondition().getTitle() + ", " + educationOrgUnit.getDevelopTech().getTitle() + ", " + educationOrgUnit.getDevelopPeriod().getTitle() + ") " + direction.getCompensationType().getShortTitle();
    }
}
