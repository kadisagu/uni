/* $Id$ */
package ru.tandemservice.uniec.component.report.SummaryQuotasMarksResults.Add;

import org.tandemframework.core.common.ITitled;

import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

/**
 * @author Vasily Zhukov
 * @since 19.08.2011
 */
interface ISumLev extends ITitled, Comparable<ISumLev>
{
    @Override
    /**
     * @return Направления подготовки по УГС
     */
    String getTitle();

    /**
     * @return Направление подготовки (специальность)
     */
    String getSubTitle();

    /**
     * @return Бюджетные места (Количество  мест)
     */
    int getBudgetPlan();

    /**
     * @return Бюджетные места (Количество заявлений)
     */
    int getBudgetCount();

    /**
     * @return Целевой прием (Количество  мест)
     */
    int getTargetPlan();

    /**
     * @return Целевой прием (Количество заявлений)
     */
    int getTargetCount();

    /**
     * @return Договора с физ. и юр. лицами с полным возмещением затрат (Количество  мест)
     */
    int getContractPlan();

    /**
     * @return Договора с физ. и юр. лицами с полным возмещением затрат (Количество заявлений)
     */
    int getContractCount();

    /**
     * @return уровень: 0 - укрупненная группа, 1 - группируемое направление, 2 - направление приема
     */
    int getLevel();

    /**
     * @param stateExamSubject предмет егэ
     * @return средняя оценка по предмету егэ
     */
    double getAvgStateExamMark(StateExamSubject stateExamSubject);

    /**
     * @param discipline дисциплина приемной кампании
     * @return средняя оценка по дисциплине
     */
    double getAvgDisciplineMark(Discipline2RealizationWayRelation discipline);

    /**
     * @param wave номер волны (с нуля)
     * @return число зачисленных
     */
    int getEnrollCount(int wave);
}
