package ru.tandemservice.uniec.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.PreparedStatement;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniec_2x10x1_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrollmentCampaignType

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("enrollmentcampaigntype_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_enrollmentcampaigntype"),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("uid4fis_p", DBType.createVarchar(255))
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("enrollmentCampaignType");

            PreparedStatement insert = tool.prepareStatement("insert into enrollmentcampaigntype_t (id, discriminator, code_p, title_p, uid4fis_p) values (?, ?, ?, ?, ?)");
            insert.setLong(1, EntityIDGenerator.generateNewId(entityCode));
            insert.setShort(2, entityCode);
            insert.setString(3, "enr.cmpType.indefin");
            insert.setString(4, "Не определен");
            insert.setString(5, "0");
            insert.executeUpdate();
        }


        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrollmentCampaign

        // создано обязательное свойство closed
        {
            // создать колонку
            tool.createColumn("enrollmentcampaign_t", new DBColumn("closed_p", DBType.BOOLEAN));

            // задать значение по умолчанию
            tool.executeUpdate(
                    "UPDATE enrollmentcampaign_t" +
                            " SET closed_p=" +
                            "(CASE WHEN" +
                            " (SELECT educationyear_t.intvalue_p FROM educationyear_t WHERE educationyear_t.id = educationyear_id)<?" +
                            " THEN ? ELSE ? END)" +
                            " WHERE closed_p IS null",
                    2016, true, false);

            // сделать колонку NOT NULL
            tool.setColumnNullable("enrollmentcampaign_t", "closed_p", false);
        }

        // создано обязательное свойство enrollmentCampaignType
        {
            // создать колонку
            tool.createColumn("enrollmentcampaign_t", new DBColumn("enrollmentcampaigntype_id", DBType.LONG));

            // задать значение по умолчанию
            Long enrollmentCampaignType_id = tool.getNumericResult("SELECT id FROM enrollmentcampaigntype_t WHERE code_p = ?",
                                                                   "enr.cmpType.indefin");
            tool.executeUpdate("UPDATE enrollmentcampaign_t" +
                                       " SET enrollmentcampaigntype_id=?" +
                                       " WHERE enrollmentcampaigntype_id IS null",
                               enrollmentCampaignType_id);

            // сделать колонку NOT NULL
            tool.setColumnNullable("enrollmentcampaign_t", "enrollmentcampaigntype_id", false);
        }
    }
}