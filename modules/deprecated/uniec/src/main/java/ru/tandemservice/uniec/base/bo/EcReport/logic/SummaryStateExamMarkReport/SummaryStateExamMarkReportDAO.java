/**
 *$Id:$
 */
package ru.tandemservice.uniec.base.bo.EcReport.logic.SummaryStateExamMarkReport;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import jxl.write.Number;
import org.apache.commons.collections.map.MultiKeyMap;
import org.tandemframework.caf.logic.dao.BaseModifyAggregateDAO;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.tapsupport.component.selection.CommonMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.CommonSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.codes.EntrantStateCodes;
import ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaignPeriod;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.report.SummaryStateExamMarkReport;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantDataUtil;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;

/**
 * @author Alexander Shaburov
 * @since 30.07.12
 */
public class SummaryStateExamMarkReportDAO extends BaseModifyAggregateDAO implements ISummaryStateExamMarkReportDAO
{
    /**
     * Сортируем строки таблицы:
     * 1. по названию НПВ
     * 2. по форме освоения (по коду элементов справочника, т.е. очная впереди)
     * 3. по виду возмещения затрат (бюджет впереди)
     */
    private Comparator<Row> _rowComparator = new Comparator<Row>()
    {
        @Override
        public int compare(Row o1, Row o2)
        {
            int result = o1.getEducationLevelHighSchoolTitle().compareTo(o2.getEducationLevelHighSchoolTitle());
            if (result == 0)
            {
                String code1 = o1.getRequestedEnrollmentDirectionList().get(0).getEnrollmentDirection().getEducationOrgUnit().getDevelopForm().getCode();
                String code2 = o2.getRequestedEnrollmentDirectionList().get(0).getEnrollmentDirection().getEducationOrgUnit().getDevelopForm().getCode();
                result = code1.compareTo(code2);
            }
            if (result == 0)
            {
                boolean budget1 = o1.getRequestedEnrollmentDirectionList().get(0).getCompensationType().isBudget();
                boolean budget2 = o1.getRequestedEnrollmentDirectionList().get(0).getCompensationType().isBudget();
                result = !budget1 && budget2 ? 1 : (budget1 && !budget2 ? -1 : 0);
            }

            return result;
        }
    };

    @Override
    public <M extends SummaryStateExamMarkReportModel> M prepareModel(M model)
    {
        // фильтр Приемная компания
        List<EnrollmentCampaign> ecList = DataAccessServices.dao()
                .getList(new DQLSelectBuilder().fromEntity(EnrollmentCampaign.class, "b").column("b")
                        .order(DQLExpressions.property(EnrollmentCampaign.id().fromAlias("b")), OrderDirection.desc));
        model.setEnrollmentCampaign(ecList.isEmpty() ? null : ecList.get(0));

        // фильтры Заявления с - по
        EnrollmentCampaign enrollmentCampaign = model.getEnrollmentCampaign();
        List<EnrollmentCampaignPeriod> periodList = enrollmentCampaign != null ? enrollmentCampaign.getPeriodList() : new LinkedList<EnrollmentCampaignPeriod>();
        model.setRequestFrom(periodList.isEmpty() ? null : periodList.get(0).getDateFrom());
        model.setRequestTo(new Date());

        // заполняем модели селектов
        model.setEnrollmentCampaignModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrollmentCampaign.class, "b").column("b");
                builder.where(DQLExpressions.likeUpper(DQLExpressions.property(EnrollmentCampaign.title().fromAlias("b")), DQLExpressions.value(CoreStringUtils.escapeLike(filter))));
                if (o != null)
                    builder.where(DQLExpressions.eqValue(DQLExpressions.property(EnrollmentCampaign.id().fromAlias("b")), o));
                builder.order(DQLExpressions.property(EnrollmentCampaign.id().fromAlias("b")), OrderDirection.desc);

                return new DQLListResultBuilder(builder);
            }
        });
        model.setCompensationTypeModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(CompensationType.class, "b").column("b");
                builder.where(DQLExpressions.likeUpper(DQLExpressions.property(CompensationType.title().fromAlias("b")), DQLExpressions.value(CoreStringUtils.escapeLike(filter))));
                if (o != null)
                    builder.where(DQLExpressions.eqValue(DQLExpressions.property(CompensationType.id().fromAlias("b")), o));
                builder.order(DQLExpressions.property(CompensationType.code().fromAlias("b")));

                return new DQLListResultBuilder(builder);
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((CompensationType) value).getShortTitle();
            }
        });
        model.setStudentCategoryModel(new CommonMultiSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentCategory.class, "b").column("b");
                builder.where(DQLExpressions.likeUpper(DQLExpressions.property(StudentCategory.title().fromAlias("b")), DQLExpressions.value(CoreStringUtils.escapeLike(filter))));
                if (set != null)
                    builder.where(DQLExpressions.in(DQLExpressions.property(StudentCategory.id().fromAlias("b")), set));
                builder.order(DQLExpressions.property(StudentCategory.title().fromAlias("b")));

                return new DQLListResultBuilder(builder);
            }
        });
        model.setQualificationModel(new QualificationModel(getSession()));

        // заполняем модели селекта с помощью утили
        model.setFormativeOrgUnitModel((IMultiSelectModel) MultiEnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(null, model));
        model.setTerritorialOrgUnitModel((IMultiSelectModel) MultiEnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(null, model));
        model.setDevelopFormModel((IMultiSelectModel) MultiEnrollmentDirectionUtil.createDevelopFormModel(model));
        model.setDevelopConditionModel((IMultiSelectModel) MultiEnrollmentDirectionUtil.createDevelopConditionModel(model));
        model.setDevelopTechModel((IMultiSelectModel) MultiEnrollmentDirectionUtil.createDevelopTechModel(model));
        model.setDevelopPeriodModel((IMultiSelectModel) MultiEnrollmentDirectionUtil.createDevelopPeriodModel(model));

        // заполняем по умолчанию пустые селекты
        model.setCompensationType(null);
        model.setStudentCategoryList(new ArrayList<StudentCategory>());
        model.setQualificationList(new ArrayList<Qualifications>());
        model.setFormativeOrgUnitList(new ArrayList<OrgUnit>());
        model.setTerritorialOrgUnitList(new ArrayList<OrgUnit>());
        model.setDevelopFormList(new ArrayList<DevelopForm>());
        model.setDevelopConditionList(new ArrayList<DevelopCondition>());
        model.setDevelopTechList(new ArrayList<DevelopTech>());
        model.setDevelopPeriodList(new ArrayList<DevelopPeriod>());

        // запллняем активность полей по умолчанию
        model.setCompensTypeActive(false);
        model.setStudentCategoryActive(false);
        model.setQualificationActive(false);
        model.setFormativeOrgUnitActive(false);
        model.setTerritorialOrgUnitActive(false);
        model.setDevelopFormActive(false);
        model.setDevelopConditionActive(false);
        model.setDevelopTechActive(false);
        model.setDevelopPeriodActive(false);

        // определяем обязательность полей для модели утили
        model.setParameters(new MultiEnrollmentDirectionUtil.Parameters());
        model.getParameters().setRequired(false, MultiEnrollmentDirectionUtil.Parameters.Filter.values());

        return model;
    }

    @Override
    public <M extends SummaryStateExamMarkReportModel> M onChangeEnrollmentCampaign(M model)
    {
        // подставляем даты выбранной ПК
        EnrollmentCampaign enrollmentCampaign = model.getEnrollmentCampaign();
        List<EnrollmentCampaignPeriod> periodList = enrollmentCampaign != null ? enrollmentCampaign.getPeriodList() : new LinkedList<EnrollmentCampaignPeriod>();
        model.setRequestFrom(periodList.isEmpty() ? null : periodList.get(0).getDateFrom());
        model.setRequestTo(periodList.isEmpty() ? null : periodList.get(0).getDateTo());

        return model;
    }

    @Override
    public <M extends SummaryStateExamMarkReportModel> SummaryStateExamMarkReport createReport(M model, DatabaseFile reportFile)
    {
        SummaryStateExamMarkReport report = new SummaryStateExamMarkReport();
        report.setContent(reportFile);
        report.setFormingDate(new Date());
        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setRequestDateFrom(model.getRequestFrom());
        report.setRequestDateTo(model.getRequestTo());
        if (model.isCompensTypeActive())
            report.setCompensationTypeText(model.getCompensationType().getTitle());
        if (model.isStudentCategoryActive())
            report.setStudentCategoryText(UniStringUtils.join(model.getStudentCategoryList(), StudentCategory.title().s(), "; "));
        if (model.isQualificationActive())
            report.setQualificationText(UniStringUtils.join(model.getQualificationList(), Qualifications.title().s(), "; "));
        if (model.isFormativeOrgUnitActive())
        {
            String formativeOrgUnit = UniStringUtils.join(model.getFormativeOrgUnitList(), OrgUnit.shortTitle().s(), "; ");
            report.setFormativeOrgUnitText(formativeOrgUnit.length() > 1000 ? formativeOrgUnit.substring(0, 997) + "..." : formativeOrgUnit);
        }
        if (model.isTerritorialOrgUnitActive())
            report.setTerritorialOrgUnitText(UniStringUtils.join(model.getTerritorialOrgUnitList(), OrgUnit.territorialFullTitle().s(), "; "));
        if (model.isDevelopFormActive())
            report.setDevelopFormText(UniStringUtils.join(model.getDevelopFormList(), DevelopForm.title().s(), "; "));
        if (model.isDevelopConditionActive())
            report.setDevelopConditionText(UniStringUtils.join(model.getDevelopConditionList(), DevelopCondition.title().s(), "; "));
        if (model.isDevelopTechActive())
            report.setDevelopTechText(UniStringUtils.join(model.getDevelopTechList(), DevelopTech.title().s(), "; "));
        if (model.isDevelopPeriodActive())
            report.setDevelopPeriodText(UniStringUtils.join(model.getDevelopPeriodList(), DevelopPeriod.title().s(), "; "));

        return report;
    }

    @Override
    public <M extends SummaryStateExamMarkReportModel> DatabaseFile createPrintReportFile(M model) throws IOException, WriteException
    {
        prepareReportModel(model);

        // создаем печатную форму
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        WritableWorkbook workbook = Workbook.createWorkbook(out, ws);

        // создаем лист
        WritableSheet sheet = workbook.createSheet("Отчет", 0);

        // заголовок
        writeHeader(sheet, model);

        // таблица
        writeTable(sheet, model);

        workbook.write();
        workbook.close();

        // создаем файл
        DatabaseFile databaseFile = new DatabaseFile();
        databaseFile.setContent(out.toByteArray());
        databaseFile.setFilename("Report");
        databaseFile.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_VND_MS_EXCEL);

        return databaseFile;
    }

    /**
     * Рисует шапку отчета.
     * @param sheet лист
     * @param model модель
     * @return лист
     */
    protected <M extends SummaryStateExamMarkReportModel> WritableSheet writeHeader(WritableSheet sheet, M model) throws WriteException
    {
        // подготавливаем форматы шрифта
        WritableFont arial10 = new WritableFont(WritableFont.ARIAL, 10);

        // подготавливаем формат ячейки
        WritableCellFormat arial10Format = new WritableCellFormat(arial10);

        //рисуем строки
        sheet.addCell(new Label(0, 0, "Проходной балл по ЕГЭ по приемной кампании " + model.getEnrollmentCampaign().getTitle(), arial10Format));
        sheet.addCell(new Label(0, 1, TopOrgUnit.getInstance().getPrintTitle(), arial10Format));

        return sheet;
    }

    /**
     * Рисует основную таблицу отчета.
     * @param sheet лист
     * @param model модель
     * @return лист
     */
    protected <M extends SummaryStateExamMarkReportModel> WritableSheet writeTable(WritableSheet sheet, M model) throws WriteException
    {
        writeTableHeader(sheet);

        // подготавливаем форматы шрифта
        WritableFont arial10 = new WritableFont(WritableFont.ARIAL, 10);


        // подготавливаем формат ячейки
        WritableCellFormat arial10Format = new WritableCellFormat(arial10);
        arial10Format.setBorder(Border.ALL, BorderLineStyle.THIN);
        arial10Format.setWrap(true);

        int rowNumber = 6;
        for (Row row : model.getRowList())
        {
            sheet.addCell(new Label(0, rowNumber, row.getEducationLevelHighSchoolTitle(), arial10Format));
            sheet.addCell(new Label(1, rowNumber, row.getDevelopFormAndCompensationTypeString(), arial10Format));
            sheet.addCell(new Label(2, rowNumber, row.getLevelTypeTitle(), arial10Format));
            sheet.addCell(new Number(3, rowNumber, row.getMinRating(), arial10Format));
            sheet.addCell(new Number(4, rowNumber, row.getMaxRating(), arial10Format));
            sheet.addCell(new Number(5, rowNumber, row.getAvrRating(), arial10Format));
            sheet.addCell(new Blank(6, rowNumber, arial10Format));
            sheet.addCell(new Blank(7, rowNumber, arial10Format));
            sheet.addCell(new Blank(8, rowNumber, arial10Format));
            sheet.addCell(new Blank(9, rowNumber++, arial10Format));
        }

        return sheet;
    }

    /**
     * Рисует шапку таблицы.
     * @param sheet лист
     * @return лист
     */
    protected WritableSheet writeTableHeader(WritableSheet sheet) throws WriteException
    {
        // подготавливаем форматы шрифта
        WritableFont arial10 = new WritableFont(WritableFont.ARIAL, 10);

        // подготавливаем формат ячейки
        WritableCellFormat arial10Format = new WritableCellFormat(arial10);
        arial10Format.setBorder(Border.ALL, BorderLineStyle.THIN);
        arial10Format.setAlignment(Alignment.CENTRE);
        arial10Format.setVerticalAlignment(VerticalAlignment.CENTRE);
        arial10Format.setWrap(true);

        // устанавливаем ширину коолнок
        CellView view = new CellView();
        view.setSize(30 * 256);
        sheet.setColumnView(0, view);
        view.setSize(20 * 256);
        sheet.setColumnView(1, view);
        view.setSize(15 * 256);
        sheet.setColumnView(2, view);
        view.setSize(5 * 256);
        sheet.setColumnView(3, view);
        view.setSize(5 * 256);
        sheet.setColumnView(4, view);
        view.setSize(10 * 256);
        sheet.setColumnView(5, view);
        view.setSize(10 * 256);
        sheet.setColumnView(6, view);
        view.setSize(20 * 256);
        sheet.setColumnView(7, view);
        view.setSize(10 * 256);
        sheet.setColumnView(8, view);
        view.setSize(10 * 256);
        sheet.setColumnView(9, view);

        // рисуем шапку таблицы
        sheet.addCell(new Label(0, 3, "Наименование образовательной программы", arial10Format));
        sheet.mergeCells(0, 3, 0, 5);
        sheet.addCell(new Label(1, 3, "Форма обучения", arial10Format));
        sheet.mergeCells(1, 3, 1, 5);
        sheet.addCell(new Label(2, 3, "Специалитет, бакалавриат, магистратура", arial10Format));
        sheet.mergeCells(2, 3, 2, 5);
        sheet.addCell(new Label(3, 3, "Min", arial10Format));
        sheet.mergeCells(3, 3, 3, 5);
        sheet.addCell(new Label(4, 3, "Max", arial10Format));
        sheet.mergeCells(4, 3, 4, 5);
        sheet.addCell(new Label(5, 3, "Средний балл", arial10Format));
        sheet.mergeCells(5, 3, 5, 5);
        sheet.addCell(new Label(6, 3, "Место в рейтинге в субъекте", arial10Format));
        sheet.mergeCells(6, 3, 6, 5);
        sheet.addCell(new Label(7, 3, "ВУЗы", arial10Format));
        sheet.mergeCells(7, 3, 9, 3);
        sheet.addCell(new Label(7, 4, "Первый ВУЗ в рейтинге в субъекте", arial10Format));
        sheet.addCell(new Label(8, 4, "Основной конкурент", arial10Format));
        sheet.mergeCells(8, 4, 9, 4);
        sheet.addCell(new Label(7, 5, "Средний балл", arial10Format));
        sheet.addCell(new Label(8, 5, "Место в рейтинге в субъекте", arial10Format));
        sheet.addCell(new Label(9, 5, "Средний балл", arial10Format));

        return sheet;
    }

    /**
     * Подготавливает данные необходимые для печати отчета.
     * @param model модель
     * @return модель
     */
    @SuppressWarnings("unchecked")
    protected <M extends SummaryStateExamMarkReportModel> M prepareReportModel(M model)
    {
        // поднимаем ВНП:
        // удовлетворяющие параметрам отчета,
        // в любых состояниях кроме "забрал документы", "выбыл из конкурса", "активный"
        MQBuilder directionBuilder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "rd");
        directionBuilder.add(MQExpression.eq("rd", RequestedEnrollmentDirection.entrantRequest().entrant().archival().s(), false));
        directionBuilder.add(MQExpression.in("rd", RequestedEnrollmentDirection.enrollmentDirection().s(), MultiEnrollmentDirectionUtil.getEnrollmentDirections(model, getSession())));
        if (model.isCompensTypeActive())
            directionBuilder.add(MQExpression.eq("rd", RequestedEnrollmentDirection.compensationType().id().s(), model.getCompensationType().getId()));
        if (model.isStudentCategoryActive())
            directionBuilder.add(MQExpression.in("rd", RequestedEnrollmentDirection.studentCategory().s(), model.getStudentCategoryList()));
        if (model.isQualificationActive())
            directionBuilder.add(MQExpression.in("rd", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().s(), model.getQualificationList()));
        directionBuilder.add(MQExpression.notIn("rd", RequestedEnrollmentDirection.state().code().s(), Arrays.asList(EntrantStateCodes.TAKE_DOCUMENTS_AWAY, EntrantStateCodes.OUT_OF_COMPETITION, EntrantStateCodes.ACTIVE)));
        directionBuilder.add(MQExpression.lessOrEq("rd", RequestedEnrollmentDirection.entrantRequest().regDate().s(), getRequestTo(model.getRequestTo())));
        directionBuilder.add(MQExpression.greatOrEq("rd", RequestedEnrollmentDirection.entrantRequest().regDate().s(), model.getRequestFrom()));

        EntrantDataUtil dataUtil = new EntrantDataUtil(getSession(), model.getEnrollmentCampaign(), directionBuilder);

        // отбираем те ВНП по которым все ВВИ с итоговыми баллами полученными в форме сдачи "ЕГЭ" или "ЕГЭ(ВУЗ)"
        // группируем ВНП и создаем враперы строк, которые агрегируют данные
        // группировка по ключу: [вид возмещения затрат, НПВ, форма освоения]
        List<Integer> stateExamSourceCodeList = Arrays.asList(UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL, UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL_APPEAL, UniecDefines.FINAL_MARK_STATE_EXAM_WAVE1, UniecDefines.FINAL_MARK_STATE_EXAM_WAVE2);
        MultiKeyMap rowMap = new MultiKeyMap();
        for (RequestedEnrollmentDirection direction : dataUtil.getDirectionSet())
        {
            // если нет ВВИ, то ВНП нас не интерисует
            Set<ChosenEntranceDiscipline> chosenEntranceDisciplineSet = dataUtil.getChosenEntranceDisciplineSet(direction);
            if (chosenEntranceDisciplineSet.isEmpty())
                continue;

            boolean take = true;// если источник оценки в ВВИ не ЕГЭ, то ВНП нас не интерисует
            for (ChosenEntranceDiscipline discipline : chosenEntranceDisciplineSet)
            {
                if (!stateExamSourceCodeList.contains(discipline.getFinalMarkSource()))
                {
                    take = false;
                    break;
                }
            }

            if (take)
            {
                Row row = (Row) rowMap.get(direction.getCompensationType().getId(), direction.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getId(), direction.getEnrollmentDirection().getEducationOrgUnit().getDevelopForm().getId());
                if (row == null)
                    rowMap.put(direction.getCompensationType().getId(), direction.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getId(), direction.getEnrollmentDirection().getEducationOrgUnit().getDevelopForm().getId(), row = new Row(dataUtil));
                row.addDirection(direction);
            }
        }

        List<Row> rowList = Arrays.asList((Row[]) rowMap.values().toArray(new Row[rowMap.values().size()]));

        // сортируем строки
        Collections.sort(rowList, _rowComparator);

        model.setRowList(rowList);

        return model;
    }

    private Date getRequestTo(Date date)
    {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTime();
    }
}
