/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.settings.EnrollmentOrderVisaAddEdit;

import org.hibernate.Session;
import org.tandemframework.common.component.selection.MQListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.tapsupport.component.selection.CommonSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.settings.EnrollmentOrderVisaItem;
import ru.tandemservice.uniec.util.EntrantFilterUtil;
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising;
import ru.tandemservice.unimv.ui.PossibleVisaListModel;

import java.util.List;

/**
 * @author vip_delete
 * @since 28.07.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        if (model.isEditForm())
        {
            model.setEnrollmentCampaign(getNotNull(EnrollmentCampaign.class, model.getEnrollmentCampaignId()));

            if (model.getVisaItemList() == null || model.getVisaItemList().isEmpty())
            {
                List<EnrollmentOrderVisaItem> itemList = new MQBuilder(EnrollmentOrderVisaItem.ENTITY_CLASS, "e")
                        .add(MQExpression.eq("e", EnrollmentOrderVisaItem.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()))
                        .add(MQExpression.eq("e", EnrollmentOrderVisaItem.P_TITLE, model.getTitle()))
                        .addOrder("e", EnrollmentOrderVisaItem.P_PRIORITY)
                        .addOrder("e", EnrollmentOrderVisaItem.groupsMemberVising().index().s())
                        .getResultList(getSession());


                model.setVisaItemList(itemList);

                for (EnrollmentOrderVisaItem item : itemList)
                {
                    Row row = new Row(Row.getNewId(model.getRowList()), item, false);
                    model.getRowList().add(row);

                    if (!model.getExcludeGroupsMemberVising().contains(item.getGroupsMemberVising()))
                        model.getExcludeGroupsMemberVising().add(item.getGroupsMemberVising());
                }
            }
        }

        model.setPossibleVisaListModel(new PossibleVisaListModel(getSession()));

        model.setGroupsMemberModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                MQBuilder builder = new MQBuilder(GroupsMemberVising.ENTITY_CLASS, "b");
                builder.add(MQExpression.notIn("b", GroupsMemberVising.id().s(), ids(model.getExcludeGroupsMemberVising())));
                builder.add(MQExpression.like("b", GroupsMemberVising.title().s(), CoreStringUtils.escapeLike(filter)));
                if (o != null)
                    builder.add(MQExpression.eq("b", GroupsMemberVising.id().s(), o));

                return new MQListResultBuilder(builder);
            }
        });


        Row firstElement = new Row(0L, null, true);
        if (!model.getRowList().contains(firstElement))
            model.getRowList().add(0, firstElement);
    }

    @Override
    public void update(Model model)
    {
        final Session session = getSession();

        if (model.getRowList() == null || model.getRowList().size() == 1)
            throw new ApplicationException("Группа виз не может быть пустой.");

        MQBuilder builder = new MQBuilder(EnrollmentOrderVisaItem.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", EnrollmentOrderVisaItem.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
        builder.add(MQExpression.eq("e", EnrollmentOrderVisaItem.P_TITLE, model.getTitle().trim()));
        if (model.isEditForm())
            builder.add(MQExpression.notIn("e", "", model.getVisaItemList()));

        if (builder.getResultCount(session) > 0)
            throw new ApplicationException("Название группы виз должно быть уникальным.");

        // удаляем группу виз
        if (model.isEditForm())
            for (EnrollmentOrderVisaItem forDelete : model.getVisaItemList())
                session.delete(forDelete);

        // после всех delete надо делать flush, иначе будет ошибка уникальности
        session.flush();

        // создаем группу виз заново
        for (Row row : model.getRowList())
            if (!row.isFirst())
            {
                row.getVisaItem().setTitle(model.getTitle());
                session.save(row.getVisaItem());
            }
    }

    @Override
    public void prepareVisaGroupDataSource(Model model)
    {
        UniBaseUtils.createPage(model.getRowDataSource(), model.getRowList());
    }
}
