/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.settings.ProfileEduInstitutionList;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.fias.base.bo.util.IKladrModel;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.person.base.bo.PersonSetting.util.IEduInstitutionTypeModel;
import org.tandemframework.shared.person.catalog.entity.EduInstitution;
import org.tandemframework.shared.person.catalog.entity.EducationalInstitutionTypeKind;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

/**
 * @author vip_delete
 * @since 04.06.2009
 */
public class Model implements IEduInstitutionTypeModel, IKladrModel
{
    private IDataSettings _settings;
    private ISelectModel _countriesList;
    private ISelectModel _settlementsList;
    private ISelectModel _eduInstitutionTypesList;
    private ISelectModel _eduInstitutionKindsList;
    private DynamicListDataSource<EduInstitution> _dataSource;

    // IEduInstitutionTypeModel

    @Override
    public EducationalInstitutionTypeKind getEduInstitutionKind()
    {
        return (EducationalInstitutionTypeKind)getSettings().get("eduInstitutionKind");
    }

    @Override
    public EducationalInstitutionTypeKind getEduInstitutionType()
    {
        return (EducationalInstitutionTypeKind)getSettings().get("eduInstitutionType");
    }

    // IKladrModel

    @Override
    public AddressCountry getAddressCountry()
    {
        return (AddressCountry)getSettings().get("country");
    }

    @Override
    public AddressItem getAddressItem()
    {
        return (AddressItem)getSettings().get("settlement");
    }

    // Getters & Setters

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public ISelectModel getCountriesList()
    {
        return _countriesList;
    }

    public void setCountriesList(ISelectModel countriesList)
    {
        _countriesList = countriesList;
    }

    public ISelectModel getSettlementsList()
    {
        return _settlementsList;
    }

    public void setSettlementsList(ISelectModel settlementsList)
    {
        _settlementsList = settlementsList;
    }

    public ISelectModel getEduInstitutionTypesList()
    {
        return _eduInstitutionTypesList;
    }

    public void setEduInstitutionTypesList(ISelectModel eduInstitutionTypesList)
    {
        _eduInstitutionTypesList = eduInstitutionTypesList;
    }

    public ISelectModel getEduInstitutionKindsList()
    {
        return _eduInstitutionKindsList;
    }

    public void setEduInstitutionKindsList(ISelectModel eduInstitutionKindsList)
    {
        _eduInstitutionKindsList = eduInstitutionKindsList;
    }

    public DynamicListDataSource<EduInstitution> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<EduInstitution> dataSource)
    {
        _dataSource = dataSource;
    }
}
