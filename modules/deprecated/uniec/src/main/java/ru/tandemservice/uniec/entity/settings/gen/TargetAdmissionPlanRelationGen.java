package ru.tandemservice.uniec.entity.settings.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.settings.TargetAdmissionPlanRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * План приема по ЦП
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TargetAdmissionPlanRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.settings.TargetAdmissionPlanRelation";
    public static final String ENTITY_NAME = "targetAdmissionPlanRelation";
    public static final int VERSION_HASH = -1204478530;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANCE_EDUCATION_ORG_UNIT = "entranceEducationOrgUnit";
    public static final String L_TARGET_ADMISSION_KIND = "targetAdmissionKind";
    public static final String P_BUDGET = "budget";
    public static final String P_PLAN_VALUE = "planValue";

    private EnrollmentDirection _entranceEducationOrgUnit;     // Направление подготовки (специальность) приема
    private TargetAdmissionKind _targetAdmissionKind;     // Вид целевого приема
    private boolean _budget;     // План приема по ЦП для бюджета
    private int _planValue;     // Значение плана приема

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Направление подготовки (специальность) приема. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentDirection getEntranceEducationOrgUnit()
    {
        return _entranceEducationOrgUnit;
    }

    /**
     * @param entranceEducationOrgUnit Направление подготовки (специальность) приема. Свойство не может быть null.
     */
    public void setEntranceEducationOrgUnit(EnrollmentDirection entranceEducationOrgUnit)
    {
        dirty(_entranceEducationOrgUnit, entranceEducationOrgUnit);
        _entranceEducationOrgUnit = entranceEducationOrgUnit;
    }

    /**
     * @return Вид целевого приема. Свойство не может быть null.
     */
    @NotNull
    public TargetAdmissionKind getTargetAdmissionKind()
    {
        return _targetAdmissionKind;
    }

    /**
     * @param targetAdmissionKind Вид целевого приема. Свойство не может быть null.
     */
    public void setTargetAdmissionKind(TargetAdmissionKind targetAdmissionKind)
    {
        dirty(_targetAdmissionKind, targetAdmissionKind);
        _targetAdmissionKind = targetAdmissionKind;
    }

    /**
     * @return План приема по ЦП для бюджета. Свойство не может быть null.
     */
    @NotNull
    public boolean isBudget()
    {
        return _budget;
    }

    /**
     * @param budget План приема по ЦП для бюджета. Свойство не может быть null.
     */
    public void setBudget(boolean budget)
    {
        dirty(_budget, budget);
        _budget = budget;
    }

    /**
     * @return Значение плана приема. Свойство не может быть null.
     */
    @NotNull
    public int getPlanValue()
    {
        return _planValue;
    }

    /**
     * @param planValue Значение плана приема. Свойство не может быть null.
     */
    public void setPlanValue(int planValue)
    {
        dirty(_planValue, planValue);
        _planValue = planValue;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof TargetAdmissionPlanRelationGen)
        {
            setEntranceEducationOrgUnit(((TargetAdmissionPlanRelation)another).getEntranceEducationOrgUnit());
            setTargetAdmissionKind(((TargetAdmissionPlanRelation)another).getTargetAdmissionKind());
            setBudget(((TargetAdmissionPlanRelation)another).isBudget());
            setPlanValue(((TargetAdmissionPlanRelation)another).getPlanValue());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TargetAdmissionPlanRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TargetAdmissionPlanRelation.class;
        }

        public T newInstance()
        {
            return (T) new TargetAdmissionPlanRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entranceEducationOrgUnit":
                    return obj.getEntranceEducationOrgUnit();
                case "targetAdmissionKind":
                    return obj.getTargetAdmissionKind();
                case "budget":
                    return obj.isBudget();
                case "planValue":
                    return obj.getPlanValue();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entranceEducationOrgUnit":
                    obj.setEntranceEducationOrgUnit((EnrollmentDirection) value);
                    return;
                case "targetAdmissionKind":
                    obj.setTargetAdmissionKind((TargetAdmissionKind) value);
                    return;
                case "budget":
                    obj.setBudget((Boolean) value);
                    return;
                case "planValue":
                    obj.setPlanValue((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entranceEducationOrgUnit":
                        return true;
                case "targetAdmissionKind":
                        return true;
                case "budget":
                        return true;
                case "planValue":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entranceEducationOrgUnit":
                    return true;
                case "targetAdmissionKind":
                    return true;
                case "budget":
                    return true;
                case "planValue":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entranceEducationOrgUnit":
                    return EnrollmentDirection.class;
                case "targetAdmissionKind":
                    return TargetAdmissionKind.class;
                case "budget":
                    return Boolean.class;
                case "planValue":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TargetAdmissionPlanRelation> _dslPath = new Path<TargetAdmissionPlanRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TargetAdmissionPlanRelation");
    }
            

    /**
     * @return Направление подготовки (специальность) приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.TargetAdmissionPlanRelation#getEntranceEducationOrgUnit()
     */
    public static EnrollmentDirection.Path<EnrollmentDirection> entranceEducationOrgUnit()
    {
        return _dslPath.entranceEducationOrgUnit();
    }

    /**
     * @return Вид целевого приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.TargetAdmissionPlanRelation#getTargetAdmissionKind()
     */
    public static TargetAdmissionKind.Path<TargetAdmissionKind> targetAdmissionKind()
    {
        return _dslPath.targetAdmissionKind();
    }

    /**
     * @return План приема по ЦП для бюджета. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.TargetAdmissionPlanRelation#isBudget()
     */
    public static PropertyPath<Boolean> budget()
    {
        return _dslPath.budget();
    }

    /**
     * @return Значение плана приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.TargetAdmissionPlanRelation#getPlanValue()
     */
    public static PropertyPath<Integer> planValue()
    {
        return _dslPath.planValue();
    }

    public static class Path<E extends TargetAdmissionPlanRelation> extends EntityPath<E>
    {
        private EnrollmentDirection.Path<EnrollmentDirection> _entranceEducationOrgUnit;
        private TargetAdmissionKind.Path<TargetAdmissionKind> _targetAdmissionKind;
        private PropertyPath<Boolean> _budget;
        private PropertyPath<Integer> _planValue;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Направление подготовки (специальность) приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.TargetAdmissionPlanRelation#getEntranceEducationOrgUnit()
     */
        public EnrollmentDirection.Path<EnrollmentDirection> entranceEducationOrgUnit()
        {
            if(_entranceEducationOrgUnit == null )
                _entranceEducationOrgUnit = new EnrollmentDirection.Path<EnrollmentDirection>(L_ENTRANCE_EDUCATION_ORG_UNIT, this);
            return _entranceEducationOrgUnit;
        }

    /**
     * @return Вид целевого приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.TargetAdmissionPlanRelation#getTargetAdmissionKind()
     */
        public TargetAdmissionKind.Path<TargetAdmissionKind> targetAdmissionKind()
        {
            if(_targetAdmissionKind == null )
                _targetAdmissionKind = new TargetAdmissionKind.Path<TargetAdmissionKind>(L_TARGET_ADMISSION_KIND, this);
            return _targetAdmissionKind;
        }

    /**
     * @return План приема по ЦП для бюджета. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.TargetAdmissionPlanRelation#isBudget()
     */
        public PropertyPath<Boolean> budget()
        {
            if(_budget == null )
                _budget = new PropertyPath<Boolean>(TargetAdmissionPlanRelationGen.P_BUDGET, this);
            return _budget;
        }

    /**
     * @return Значение плана приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.TargetAdmissionPlanRelation#getPlanValue()
     */
        public PropertyPath<Integer> planValue()
        {
            if(_planValue == null )
                _planValue = new PropertyPath<Integer>(TargetAdmissionPlanRelationGen.P_PLAN_VALUE, this);
            return _planValue;
        }

        public Class getEntityClass()
        {
            return TargetAdmissionPlanRelation.class;
        }

        public String getEntityName()
        {
            return "targetAdmissionPlanRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
