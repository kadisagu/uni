/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.settings.ExamGroupLogicList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.core.view.formatter.RawFormatter;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniec.entity.catalog.ExamGroupLogic;

/**
 * @author Vasily Zhukov
 * @since 28.05.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.setSettings(component.getSettings());

        getDao().prepare(model);

        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<ExamGroupLogic> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareListDataSource(model);
        });

        ToggleColumn column = (ToggleColumn) new ToggleColumn("Текущий", Model.EXAM_GROUP_LOGIC_CURRENT_PROPERTY)
                .toggleOffListener("onClickTurnOff")
                .toggleOnListener("onClickTurnOn")
                .setDisabledProperty(Model.EXAM_GROUP_LOGIC_DISABLED_PROPERTY);

        dataSource.addColumn(new SimpleColumn("Название", ExamGroupLogic.P_TITLE).setFormatter(NoWrapFormatter.INSTANCE).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Описание", ExamGroupLogic.P_DESCRIPTION, RawFormatter.INSTANCE).setOrderable(false).setClickable(false));
        dataSource.addColumn(column);
        model.setDataSource(dataSource);
    }

    public void onClickTurnOff(IBusinessComponent component)
    {
        getDao().updateExamGroupLogic(getModel(component), (Long) component.getListenerParameter(), false);
    }

    public void onClickTurnOn(IBusinessComponent component)
    {
        getDao().updateExamGroupLogic(getModel(component), (Long) component.getListenerParameter(), true);
    }

    public void onRefresh(IBusinessComponent component)
    {
        getModel(component).getDataSource().refresh();
    }
}