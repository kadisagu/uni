/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcCampaign.logic;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.hibsupport.dao.CommonDAO;

import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;

/**
 * @author Vasily Zhukov
 * @since 15.06.2011
 */
public class EcCampaignDao extends CommonDAO implements IEcCampaignDao
{
    @Override
    public String getUniqueEntrantNumber(EnrollmentCampaign enrollmentCampaign)
    {
        NamedSyncInTransactionCheckLocker.register(getSession(), "ecEntrantNumberSync" + enrollmentCampaign.getId(), 120);

        int year = enrollmentCampaign.getEducationYear().getIntValue();
        String xx = String.format("%02d", year % 100);

        Criteria criteria = getSession().createCriteria(Entrant.class);
        criteria.add(Restrictions.eq(Entrant.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign));
        criteria.add(Restrictions.like(Entrant.P_PERSONAL_NUMBER, xx + "%"));
        criteria.setProjection(Projections.max(Entrant.P_PERSONAL_NUMBER));

        String max = (String) criteria.uniqueResult();
        return (max != null) ? String.format("%07d", Integer.valueOf(max) + 1) : xx + "00001";
    }
}
