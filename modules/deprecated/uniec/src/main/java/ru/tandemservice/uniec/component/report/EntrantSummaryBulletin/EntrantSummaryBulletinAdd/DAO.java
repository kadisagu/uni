/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.report.EntrantSummaryBulletin.EntrantSummaryBulletinAdd;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.report.EntrantSummaryBulletinReport;
import ru.tandemservice.uniec.ui.EntranceEducationOrgUnitKindAutocompleteModel;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author oleyba
 * @since 20.06.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    @SuppressWarnings("deprecation")
    public void prepare(final Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        EntrantSummaryBulletinReport report = model.getReport();
        model.getReport().setDateFrom(model.getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : model.getEnrollmentCampaign().getStartDate());
        report.setDateTo(CoreDateUtils.getDayFirstTimeMoment(new Date()));
        model.setTerritorialOrgUnit(TopOrgUnit.getInstance());

        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));
        model.setStudentCategoryListModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(StudentCategory.class)));
        model.setDevelopFormListModel(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setDevelopConditionListModel(EducationCatalogsManager.getDevelopConditionSelectModel());
        model.setQualificationListModel(new QualificationModel(getSession()));
        model.setFormativeOrgUnitModel(new EntranceEducationOrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getPrincipalContext(), model.getEnrollmentCampaign())
        {
            @Override
            public OrgUnit getFilterOrgUnit()
            {
                return null;
            }
        });

        model.setTerritorialOrgUnitModel(new EntranceEducationOrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getPrincipalContext(), model.getEnrollmentCampaign())
        {
            @Override
            public OrgUnit getFilterOrgUnit()
            {
                return model.getFormativeOrgUnit();
            }
        }.setEducationOrgUnitFilterPropertyName(EducationOrgUnit.L_FORMATIVE_ORG_UNIT));

        model.setEducationLevelsHighSchoolModel(new FullCheckSelectModel("displayableTitle")
        {
            @Override
            public ListResult findValues(String filter)
            {
                return new ListResult<>(UniecDAOFacade.getEntranceEduLevelDAO().getEducationLevelsHighSchoolList(model));
            }
        });

        model.setDevelopFormModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                return new ListResult<>(UniecDAOFacade.getEntranceEduLevelDAO().getDevelopFormList(model));
            }
        });

        model.setDevelopTechModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                return new ListResult<>(UniecDAOFacade.getEntranceEduLevelDAO().getDevelopTechList(model));
            }
        });

        model.setDevelopPeriodModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                List<DevelopPeriod> tempDevelopPeriodList = UniecDAOFacade.getEntranceEduLevelDAO().getDevelopPeriodList(model);
                Collections.sort(tempDevelopPeriodList, CommonBaseUtil.PRIORITY_SIMPLE_SAFE_COMPARATOR);
                return new ListResult<>(tempDevelopPeriodList);
            }
        });

        model.setDevelopConditionModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                return new ListResult<>(UniecDAOFacade.getEntranceEduLevelDAO().getDevelopConditionList(model));
            }
        });
    }

    @Override
    public void update(final Model model, final ErrorCollector errors)
    {
        final Session session = getSession();

        final EntrantSummaryBulletinReport report = model.getReport();
        report.setFormingDate(new Date());
        report.setStudentCategoryTitle(StringUtils.lowerCase(StringUtils.join(CommonBaseUtil.getPropertiesList(model.getStudentCategoryList(), "title"), "; ")));

        if (!model.isAllEnrollmentDirectionsReport())
        {
            report.setEnrollmentDirection(UniecDAOFacade.getEntranceEduLevelDAO().getEnrollmentDirection(model));
        } else
        {
            report.setDevelopFormTitle(UniStringUtils.join(model.getDevelopFormList(), "title", "; "));
            report.setDevelopConditionTitle(UniStringUtils.join(model.getDevelopConditionList(), "title", "; "));
            report.setQualificationTitle(UniStringUtils.join(model.getQualificationList(), "title", "; "));
        }

        try
        {
            byte[] content = new EntrantSummaryBulletinReportContentGenerator(report, model, session).generateReportContent(errors);
            if (errors.hasErrors()) return;
            DatabaseFile databaseFile = new DatabaseFile();
            databaseFile.setContent(content);
            session.save(databaseFile);
            report.setContent(databaseFile);
        }
        catch (Throwable e)
        {
            throw new RuntimeException(e);
        }

        session.save(report);
    }

}
