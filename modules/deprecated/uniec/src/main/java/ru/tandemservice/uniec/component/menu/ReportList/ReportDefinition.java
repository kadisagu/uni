/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.menu.ReportList;

/**
 * @author ekachanova
 */
public class ReportDefinition implements IReportDefinition
{
    private String _id;
    private String _title;
    private String _permissionKey;
    private String _componentName;
    private Object _parameters;

    // Getters & Setters

    public String getId()
    {
        return _id;
    }

    public void setId(String id)
    {
        _id = id;
    }

    public String getTitle()
    {
        return _title;
    }

    public void setTitle(String title)
    {
        _title = title;
    }

    public String getPermissionKey()
    {
        return _permissionKey;
    }

    public void setPermissionKey(String permissionKey)
    {
        _permissionKey = permissionKey;
    }

    public String getComponentName()
    {
        return _componentName;
    }

    public void setComponentName(String componentName)
    {
        _componentName = componentName;
    }

    public Object getParameters()
    {
        return _parameters;
    }

    public void setParameters(Object parameters)
    {
        _parameters = parameters;
    }
}
