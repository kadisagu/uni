/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EntrantRequestByCGDailyReport.EntrantRequestByCGDailyReportAdd;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.MergeType;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfElementFactory;
import org.tandemframework.rtf.util.IRtfRowSplitInterceptor;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.report.EntrantRequestByCGDailyReport;

import java.util.*;
import java.util.Map.Entry;

/**
 * @author agolubenko
 * @since Jun 10, 2010
 */
public class EntrantRequestByCGDailyReportContentGenerator
{
    private Session _session;
    private EntrantRequestByCGDailyReport _report;
    private List<CompetitionGroup> _competitionGroups;
    private List<CompensationType> _compensationTypes;

    public EntrantRequestByCGDailyReportContentGenerator(Session session, EntrantRequestByCGDailyReport report, List<CompetitionGroup> competitionGroups)
    {
        _session = session;
        _report = report;
        _compensationTypes = getCompensationTypes();

        _competitionGroups = new ArrayList<>(competitionGroups);
        Collections.sort(_competitionGroups, CompetitionGroup.TITLED_COMPARATOR);
    }

    public byte[] generateReportContent()
    {
        // получаем шаблон
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ENTRANT_REQUEST_BY_CG_DAILY_REPORT);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate()).getClone();

        // получаем данные
        List<EnrollmentDirection> enrollmentDirections = getEnrollmentDirections();
        Map<CompensationType, TreeMap<Date, TreeMap<CompetitionGroup, int[]>>> data = getData(getSortedEntrantRequests(enrollmentDirections));
        Map<CompetitionGroup, List<EnrollmentDirection>> competitionGroup2EnrollmentDirections = getCompetitionGroup2EnrollmentDirections(enrollmentDirections);

        // подготавливаем шаблоны страниц
        List<IRtfElement> elementList = document.getElementList();
        List<IRtfElement> budgetTemplate = getTemplateClone(elementList);
        List<IRtfElement> contractTemplate = getTemplateClone(elementList);
        elementList.clear();

        // заполняем страницу бюджета
        fillPage(elementList, budgetTemplate, data, competitionGroup2EnrollmentDirections, UniDefines.COMPENSATION_TYPE_BUDGET);

        // вставляем разрыв страниц
        insertPageBreak(elementList);

        // заполняем страницу контракта
        fillPage(elementList, contractTemplate, data, competitionGroup2EnrollmentDirections, UniDefines.COMPENSATION_TYPE_CONTRACT);

        // возвращаем файл
        return RtfUtil.toByteArray(document);
    }

    /**
     * @return наименование учебного заведения
     */
    private String getAcademyTitle()
    {
        TopOrgUnit academy = TopOrgUnit.getInstance();
        return StringUtils.defaultIfEmpty(academy.getNominativeCaseTitle(), academy.getTitle());
    }

    /**
     * @return период, за который строится отчет
     */
    private String getPeriod()
    {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(_report.getDateFrom()) + " — " + DateFormatter.DEFAULT_DATE_FORMATTER.format(_report.getDateTo());
    }

    /**
     * @param template шаблон
     * @return копия шаблона
     */
    private List<IRtfElement> getTemplateClone(List<IRtfElement> template)
    {
        List<IRtfElement> result = new ArrayList<>(template.size());
        for (IRtfElement element : template)
        {
            result.add(element.getClone());
        }
        return result;
    }

    /**
     * Подсчитывает количество заявлений по конкурсным группам
     *
     * @param sortedEntrantRequests вид возмещения затрат -> дата -> конкурсная группа -> список заявлений
     * @return вид возмещения затрат -> дата -> конкурсная группа - > количество заявлений
     */
    private Map<CompensationType, TreeMap<Date, TreeMap<CompetitionGroup, int[]>>> getData(Map<CompensationType, Map<Date, Map<CompetitionGroup, Map<Boolean, Set<EntrantRequest>>>>> sortedEntrantRequests)
    {
        Map<CompensationType, TreeMap<Date, TreeMap<CompetitionGroup, int[]>>> result = new HashMap<>();
        for (Entry<CompensationType, Map<Date, Map<CompetitionGroup, Map<Boolean, Set<EntrantRequest>>>>> compensationType2Data : sortedEntrantRequests.entrySet())
        {
            TreeMap<Date, TreeMap<CompetitionGroup, int[]>> date2CompetitionGroup2EntrantRequestCount = new TreeMap<>();
            result.put(compensationType2Data.getKey(), date2CompetitionGroup2EntrantRequestCount);

            for (Entry<Date, Map<CompetitionGroup, Map<Boolean, Set<EntrantRequest>>>> date2Data : compensationType2Data.getValue().entrySet())
            {
                TreeMap<CompetitionGroup, int[]> competitionGroup2EntrantRequestCount = new TreeMap<>(CompetitionGroup.TITLED_COMPARATOR);
                date2CompetitionGroup2EntrantRequestCount.put(date2Data.getKey(), competitionGroup2EntrantRequestCount);

                for (CompetitionGroup competitionGroup : _competitionGroups)
                {
                    Map<Boolean, Set<EntrantRequest>> boolean2entrantRequestMap = date2Data.getValue().get(competitionGroup);

                    Set<EntrantRequest> entrantRequestWithOriginals = boolean2entrantRequestMap.get(Boolean.TRUE);
                    Set<EntrantRequest> entrantRequestWithoutOriginals = boolean2entrantRequestMap.get(Boolean.FALSE);

                    int withOriginals = entrantRequestWithOriginals == null ? 0 : entrantRequestWithOriginals.size();
                    int withoutOriginals = entrantRequestWithoutOriginals == null ? 0 : entrantRequestWithoutOriginals.size();

                    competitionGroup2EntrantRequestCount.put(competitionGroup, new int[]{withOriginals, withoutOriginals});
                }
            }
        }
        return result;
    }

    /**
     * Вставляет разрыв страницы в шаблон
     *
     * @param elementList rtf-шаблон
     */
    private void insertPageBreak(List<IRtfElement> elementList)
    {
        IRtfElementFactory elementFactory = RtfBean.getElementFactory();
        elementList.add(elementFactory.createRtfControl(IRtfData.PARD));
        elementList.add(elementFactory.createRtfControl(IRtfData.PAR));
        elementList.add(elementFactory.createRtfControl(IRtfData.PAGE));
    }

    /**
     * Заполняет страницу данными
     *
     * @param elementList          rtf-шаблон
     * @param template             шаблон страницы
     * @param data                 данные
     * @param competitionGroup2EnrollmentDirections
     *                             конкурсная группа -> направления приема
     * @param compensationTypeCode код возмещения затрат
     */
    private void fillPage(List<IRtfElement> elementList, List<IRtfElement> template, Map<CompensationType, TreeMap<Date, TreeMap<CompetitionGroup, int[]>>> data, Map<CompetitionGroup, List<EnrollmentDirection>> competitionGroup2EnrollmentDirections, String compensationTypeCode)
    {
        CompensationType compensationType = getCompensationType(compensationTypeCode);
        fillParams(template, compensationType);
        fillTable(template, data.get(compensationType), compensationType, competitionGroup2EnrollmentDirections);
        elementList.addAll(template);
    }

    /**
     * Заполняет документ параметрами отчета
     *
     * @param elementList      rtf-шаблон
     * @param compensationType вид возмещения затрат
     */
    private void fillParams(List<IRtfElement> elementList, CompensationType compensationType)
    {
        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("academyTitle", getAcademyTitle());
        injectModifier.put("enrollmentCampaign", _report.getEnrollmentCampaign().getTitle());
        injectModifier.put("compensationType", (compensationType.getCode().equals(UniDefines.COMPENSATION_TYPE_BUDGET)) ? ", финансируемые из федерального бюджета" : "с оплатой стоимости обучения на договорной основе");
        injectModifier.put("period", getPeriod());
        injectModifier.put("competitionGroups", CollectionFormatter.COLLECTION_FORMATTER.format(_competitionGroups));
        injectModifier.modify(elementList);
    }

    /**
     * Заполняет таблицу данными отчета
     *
     * @param elementList      rtf-шаблон
     * @param data             данные
     * @param compensationType вид возмещения затрат
     * @param competitionGroup2EnrollmentDirections
     *                         конкурсная группа -> направления приема
     */
    private void fillTable(List<IRtfElement> elementList, Map<Date, TreeMap<CompetitionGroup, int[]>> data, CompensationType compensationType, Map<CompetitionGroup, List<EnrollmentDirection>> competitionGroup2EnrollmentDirections)
    {
        RtfTableModifier tableModifier = new RtfTableModifier();

        // если есть данные
        if (data != null)
        {
            // считаем кол-во заявлений с оригиналами
            Map<CompetitionGroup, Integer> competitionGroup2entrantRequestWithOriginalMap = getCompetitionGroup2entrantRequestWithOriginalMap(data);

            // получаем таблицу с числами
            int[][] dataTable = getDataTable(data);

            // получаем группы, по которым есть заявления
            List<CompetitionGroup> competitionGroupsHavingRequests = getCompetitionGroupsHavingRequests(_competitionGroups, dataTable);

            // если таковые есть
            if (!competitionGroupsHavingRequests.isEmpty())
            {
                // получаем список строк таблицы
                List<RtfRow> rows = ((RtfTable) UniRtfUtil.findElement(elementList, "H")).getRowList();
                IRtfRowSplitInterceptor splitInterceptor = (newCell, index) -> {
                    if (index > 0)
                    {
                        newCell.getElementList().clear();
                    }
                };

                // разбиваем строки на конкурсные группы
                int[] parts = new int[competitionGroupsHavingRequests.size()];
                Arrays.fill(parts, 1);

                RtfUtil.splitRow(rows.get(1), 1, splitInterceptor, parts);
                RtfUtil.splitRow(rows.get(2), 1, splitInterceptor, parts);

                Map<CompetitionGroup, Integer> competitionGroupPlanMap = new HashMap<>();

                // записываем шапку таблицы
                tableModifier.put("H", getHeadTable(competitionGroupPlanMap, competitionGroupsHavingRequests, competitionGroup2EnrollmentDirections, (compensationType.isBudget()) ? EnrollmentDirection.ministerialPlan().s() : EnrollmentDirection.contractPlan().s()));
                tableModifier.put("H", new RtfRowIntercepterBase()
                {
                    @Override
                    public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
                    {
                        // объединяем первую ячейку по вертикали
                        newRowList.get(0).getCellList().get(0).setMergeType(MergeType.VERTICAL_MERGED_FIRST);
                        newRowList.get(1).getCellList().get(0).setMergeType(MergeType.VERTICAL_MERGED_NEXT);

                        // нужно повторять эти строки на каждом листе
                        newRowList.get(1).getFormatting().setInHeader(true);
                        newRowList.get(2).getFormatting().setInHeader(true);

                        // изменяем выравнивание
                        for (RtfCell cell : UniBaseUtils.getLast(newRowList).getCellList())
                        {
                            SharedRtfUtil.setCellAlignment(cell, IRtfData.QC, IRtfData.QR);
                        }
                    }
                });

                // получаем список дат
                Date[] dates = new ArrayList<>(data.keySet()).toArray(new Date[data.size()]);

                // записываем данные таблицы
                tableModifier.put("T", getDataTable(competitionGroupPlanMap, dates, dataTable, competitionGroupsHavingRequests, competitionGroup2entrantRequestWithOriginalMap));
            }
            // иначе
            else
            {
                // обнуляем метки
                tableModifier.put("H", new String[][]{});
                tableModifier.put("T", new String[][]{});
            }
        }
        // иначе
        else
        {
            // обнуляем метки
            tableModifier.put("H", new String[][]{});
            tableModifier.put("T", new String[][]{});
        }

        // записываем «легенду» по группам
        tableModifier.put("G", getGroupTable(competitionGroup2EnrollmentDirections));
        tableModifier.modify(elementList);
    }

    /**
     * @param table дата -> конкурсная группа -> количество заявлений
     * @return таблица с количеством заявлений нарастающим этогом
     */
    private int[][] getDataTable(Map<Date, TreeMap<CompetitionGroup, int[]>> table)
    {
        List<int[]> result = new ArrayList<>();

        // для каждой строки
        for (Map<CompetitionGroup, int[]> map : table.values())
        {
            // берем последнюю перед ней
            int[] lastRow = UniBaseUtils.getLast(result);

            // создаём новую
            int[] newRow = new int[map.size()];

            // для каждого значения в строке
            int i = 0;
            for (int[] value : map.values())
            {
                // суммируем значение с последний строкой, если она есть
                newRow[i] = ((lastRow != null) ? lastRow[i] : 0) + value[0] + value[1];
                i++;
            }

            // добавляем в список строк
            result.add(newRow);
        }
        return result.toArray(new int[result.size()][]);
    }

    private Map<CompetitionGroup, Integer> getCompetitionGroup2entrantRequestWithOriginalMap(Map<Date, TreeMap<CompetitionGroup, int[]>> data)
    {
        Map<CompetitionGroup, Integer> map = new HashMap<>();
        for (TreeMap<CompetitionGroup, int[]> item : data.values())
        {
            for (Map.Entry<CompetitionGroup, int[]> entry : item.entrySet())
            {
                Integer value = map.get(entry.getKey());
                map.put(entry.getKey(), value == null ? entry.getValue()[0] : value + entry.getValue()[0]);
            }
        }
        return map;
    }

    /**
     * @param competitionGroups конкурсные группы
     * @param table             таблица с данными
     * @return конкурсные группы, по которым есть заявления
     */
    private List<CompetitionGroup> getCompetitionGroupsHavingRequests(List<CompetitionGroup> competitionGroups, int[][] table)
    {
        if (table.length == 0)
        {
            return Collections.emptyList();
        }

        // берем последнюю строку и смотрим, ненулевое ли там значение
        List<CompetitionGroup> result = new ArrayList<>();
        int[] lastRow = table[table.length - 1];
        for (int i = 0; i < lastRow.length; i++)
        {
            if (lastRow[i] > 0)
            {
                result.add(competitionGroups.get(i));
            }
        }
        return result;
    }

    /**
     * @param competitionGroupPlanMap конк.группа -> план приема
     * @param dates                   даты
     * @param table                   таблица с данными
     * @param competitionGroupsHavingRequests
     *                                конкурсные группы, по которым есть заявления
     * @param withOriginalMap         конкурсные группы и кол-во заявлений с оригиналами по ним
     * @return таблица для файла
     */
    private String[][] getDataTable(Map<CompetitionGroup, Integer> competitionGroupPlanMap, Date[] dates, int[][] table, Collection<CompetitionGroup> competitionGroupsHavingRequests, Map<CompetitionGroup, Integer> withOriginalMap)
    {
        List<String[]> result = new ArrayList<>(table.length);
        DateFormatter dateFormatter = new DateFormatter("dd.MM");

        // находим, каким индексам соответствуют группы с заявлениями
        int[] indices = new int[competitionGroupsHavingRequests.size()];
        for (int i = 0, j = 0; i < _competitionGroups.size(); i++)
        {
            if (competitionGroupsHavingRequests.contains(_competitionGroups.get(i)))
            {
                indices[j++] = i;
            }
        }

        // для каждой строки
        for (int i = 0; i < table.length; i++)
        {
            String[] row = new String[table[i].length];
            for (int j = 0; j < table[i].length; j++)
            {
                row[j] = Integer.toString(table[i][j]);
            }

            // формируем итоговую из даты и ненулевых значений
            result.add((String[]) ArrayUtils.addAll(new String[]{dateFormatter.format(dates[i])}, UniBaseUtils.get(row, indices)));
        }

        if (table.length > 0)
        {
            String[] row = new String[indices.length];
            for (int j = 0; j < row.length; j++)
            {
                CompetitionGroup competitionGroup = _competitionGroups.get(indices[j]); // competitionGroup должно быть не null

                int count = withOriginalMap.get(competitionGroup); // кол-во оригиналов
                int plan = competitionGroupPlanMap.get(competitionGroup); // план приема

                row[j] = count + " (" + (plan == 0 ? 0 : (DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(100.0 * count / (double) plan))) + "%)";
            }
            result.add((String[]) ArrayUtils.addAll(new String[]{"с оригиналами (% от плана) на " + dateFormatter.format(new Date())}, row));
        }
        return result.toArray(new String[table.length][]);
    }

    /**
     * @param competitionGroupPlanMap конк. группа -> план приема
     * @param competitionGroups       конкурсные группы
     * @param competitionGroup2EnrollmentDirections
     *                                конкурсная группа -> направления приема
     * @param planPath                путь до значения плана
     * @return шапка таблицы
     */
    private String[][] getHeadTable(Map<CompetitionGroup, Integer> competitionGroupPlanMap, Collection<CompetitionGroup> competitionGroups, Map<CompetitionGroup, List<EnrollmentDirection>> competitionGroup2EnrollmentDirections, String planPath)
    {
        String[][] result = new String[3][];

        // заполняем названиями конкурсных групп
        result[0] = (String[]) ArrayUtils.addAll(new String[1], CommonBaseUtil.<String>getPropertiesList(competitionGroups, CompetitionGroup.title().s()).toArray(new String[competitionGroups.size()]));

        List<String> developForms = new ArrayList<>();
        List<String> plans = new ArrayList<>();

        // для каждой конкурсной группы
        for (CompetitionGroup competitionGroup : competitionGroups)
        {
            // берем её направления приема
            List<EnrollmentDirection> enrollmentDirections = competitionGroup2EnrollmentDirections.get(competitionGroup);
            if (null == enrollmentDirections)
            {
                enrollmentDirections = Collections.emptyList();
            }

            // получаем форму освоения
            developForms.add(enrollmentDirections.isEmpty() ? null : enrollmentDirections.get(0).getEducationOrgUnit().getDevelopForm().getTitle());

            // находим суммарный план
            int summaryPlan = 0;
            for (EnrollmentDirection enrollmentDirection : enrollmentDirections)
            {
                Integer plan = (Integer) enrollmentDirection.getProperty(planPath);
                summaryPlan += (plan != null) ? plan : 0;
            }
            plans.add(Integer.toString(summaryPlan));
            competitionGroupPlanMap.put(competitionGroup, summaryPlan);
        }

        // заполняем названиями форм освоения
        result[1] = (String[]) ArrayUtils.addAll(new String[]{"Форма обучения"}, developForms.toArray(new String[competitionGroups.size()]));

        // заполняем суммарными планами
        result[2] = (String[]) ArrayUtils.addAll(new String[]{"План приема"}, plans.toArray(new String[competitionGroups.size()]));

        return result;
    }

    /**
     * @param competitionGroup2EnrollmentDirections
     *         конкурсная группа -> направления приема
     * @return таблица с «легендой» по конкурсным группам
     */
    private String[][] getGroupTable(Map<CompetitionGroup, List<EnrollmentDirection>> competitionGroup2EnrollmentDirections)
    {
        // находим количество строк
        String[][] result = new String[(competitionGroup2EnrollmentDirections.size() + 2) / 3][];
        for (int i = 0; i < result.length; i++)
        {
            result[i] = new String[3];
        }

        // разбиваем группы по колонкам
        int i = 0;
        for (Entry<CompetitionGroup, List<EnrollmentDirection>> entry : competitionGroup2EnrollmentDirections.entrySet())
        {
            result[i % result.length][i / result.length] = entry.getKey().getTitle() + " — " + UniStringUtils.join(entry.getValue(), EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().shortTitle().s(), ", ");
            i++;
        }
        return result;
    }

    /**
     * @return направления приема с учетом параметров отчета
     */
    @SuppressWarnings("unchecked")
    private List<EnrollmentDirection> getEnrollmentDirections()
    {
        if (_competitionGroups.isEmpty())
        {
            return Collections.emptyList();
        }

        Criteria criteria = _session.createCriteria(EnrollmentDirection.class);
        criteria.add(Restrictions.in(EnrollmentDirection.competitionGroup().s(), _competitionGroups));
        if (_report.getDevelopForm() != null)
        {
            criteria.createAlias(EnrollmentDirection.educationOrgUnit().s(), "educationOrgUnit");
            criteria.add(Restrictions.eq(EducationOrgUnit.developForm().fromAlias("educationOrgUnit").s(), _report.getDevelopForm()));
        }
        return criteria.list();
    }

    /**
     * @param enrollmentDirections направления приема
     * @return конкурсная группа -> направления приема
     */
    private Map<CompetitionGroup, List<EnrollmentDirection>> getCompetitionGroup2EnrollmentDirections(List<EnrollmentDirection> enrollmentDirections)
    {
        Map<CompetitionGroup, List<EnrollmentDirection>> result = new TreeMap<>(CompetitionGroup.TITLED_COMPARATOR);
        SafeMap.Callback<CompetitionGroup, List<EnrollmentDirection>> callback = key -> new ArrayList<>();

        for (EnrollmentDirection enrollmentDirection : enrollmentDirections)
        {
            SafeMap.safeGet(result, enrollmentDirection.getCompetitionGroup(), callback).add(enrollmentDirection);
        }
        return result;
    }

    /**
     * @param enrollmentDirections направления приема
     * @return вид возмещения затрат -> дата -> конкурсная группа -> заявления абитуриентов
     */
    @SuppressWarnings({"unchecked", "deprecation"})
    private Map<CompensationType, Map<Date, Map<CompetitionGroup, Map<Boolean, Set<EntrantRequest>>>>> getSortedEntrantRequests(Collection<EnrollmentDirection> enrollmentDirections)
    {
        if (enrollmentDirections.isEmpty())
        {
            return Collections.emptyMap();
        }

        Criteria criteria = _session.createCriteria(RequestedEnrollmentDirection.class);
        criteria.createAlias(RequestedEnrollmentDirection.entrantRequest().s(), "entrantRequest");
        criteria.createAlias(EntrantRequest.entrant().fromAlias("entrantRequest").s(), "entrant");
        criteria.createAlias(RequestedEnrollmentDirection.state().s(), "state");
        criteria.add(Restrictions.in(RequestedEnrollmentDirection.enrollmentDirection().s(), enrollmentDirections));
        criteria.add(Restrictions.between(EntrantRequest.regDate().fromAlias("entrantRequest").s(), CoreDateUtils.getDayFirstTimeMoment(_report.getDateFrom()), CoreDateUtils.getDayLastTimeMoment(_report.getDateTo())));
        criteria.add(Restrictions.eq(Entrant.archival().fromAlias("entrant").s(), false));
        criteria.add(Restrictions.ne(EntrantState.code().fromAlias("state").s(), UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));

        Map<CompensationType, Map<Date, Map<CompetitionGroup, Map<Boolean, Set<EntrantRequest>>>>> result = new HashMap<>();
        for (CompensationType compensationType : _compensationTypes)
        {
            result.put(compensationType, SafeMap.get(key -> SafeMap.get(key1 -> SafeMap.get(key2 -> new HashSet<>()))));
        }

        for (RequestedEnrollmentDirection requestedEnrollmentDirection : (List<RequestedEnrollmentDirection>) criteria.list())
        {
            boolean originalDocumentHandedIn = requestedEnrollmentDirection.isOriginalDocumentHandedIn();
            EntrantRequest entrantRequest = requestedEnrollmentDirection.getEntrantRequest();
            CompetitionGroup competitionGroup = requestedEnrollmentDirection.getEnrollmentDirection().getCompetitionGroup();
            Date date = entrantRequest.getRegDate();

            result.get(requestedEnrollmentDirection.getCompensationType()).get(CoreDateUtils.getDayFirstTimeMoment(date)).get(competitionGroup).get(originalDocumentHandedIn).add(entrantRequest);
        }
        return result;
    }

    /**
     * @return виды возмещения затрат
     */
    @SuppressWarnings("unchecked")
    private List<CompensationType> getCompensationTypes()
    {
        return _session.createCriteria(CompensationType.class).list();
    }

    /**
     * @param code код вида возмещения затрат
     * @return вид возмещения затрат
     */
    private CompensationType getCompensationType(String code)
    {
        for (CompensationType compensationType : _compensationTypes)
        {
            if (code.equals(compensationType.getCode()))
            {
                return compensationType;
            }
        }
        return null;
    }
}
