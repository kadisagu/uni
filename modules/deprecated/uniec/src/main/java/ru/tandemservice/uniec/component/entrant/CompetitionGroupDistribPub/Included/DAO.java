package ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.Included;

import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.add.ModelBase;
import ru.tandemservice.uniec.dao.ecg.IEnrollmentCompetitionGroupDAO;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.ecg.EcgDistribObject;
import ru.tandemservice.uniec.entity.ecg.EcgDistribRelation;
import ru.tandemservice.uniec.entity.ecg.gen.EcgDistribObjectGen;
import ru.tandemservice.uniec.entity.ecg.gen.EcgDistribQuotaGen;
import ru.tandemservice.uniec.entity.ecg.gen.EcgDistribRelationGen;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.gen.EntrantRequestGen;
import ru.tandemservice.uniec.entity.entrant.gen.RequestedEnrollmentDirectionGen;

import java.util.*;

/**
 * @author vdanilov
 */
public class DAO extends UniBaseDao implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setDistrib(this.get(EcgDistribObject.class, model.getDistrib().getId()));

        final Session session = getSession();
        final Map<Long, Long> data = new HashMap<Long, Long>();
        final MQBuilder builder = new MQBuilder(EcgDistribRelationGen.ENTITY_CLASS, "rel", new String[]{
                EcgDistribRelationGen.L_ENTRANT_DIRECTION + "." + RequestedEnrollmentDirectionGen.L_ENTRANT_REQUEST + "." + EntrantRequestGen.L_ENTRANT + ".id",
                EcgDistribRelationGen.L_ENTRANT_DIRECTION + ".id"
        });
        builder.add(MQExpression.eq("rel", EcgDistribRelationGen.L_QUOTA + "." + EcgDistribQuotaGen.L_DISTRIBUTION, model.getDistrib()));
        for (final Object[] row : builder.<Object[]>getResultList(session))
            data.put((Long) row[0], (Long) row[1]);

        ArrayList<ModelBase.Row> rows = new ArrayList<ModelBase.Row>();
        for (IEnrollmentCompetitionGroupDAO.IEntrantRateRow entrantRow : IEnrollmentCompetitionGroupDAO.INSTANCE.get().getEntrantRateRows(model.getDistrib()))
        {
            final ModelBase.Row row = new ModelBase.Row(entrantRow);
            final Long id = data.get(row.getRow().getEntrant().getId());
            if (null != id)
                row.setDirection((RequestedEnrollmentDirection) session.get(RequestedEnrollmentDirection.class, id));
            rows.add(row);
        }
        model.setEntrantRateRows(rows);
    }

    @Override
    public void delete(final Model model, final Long entrantId)
    {
        if (model.getDistrib().isApproved())
            throw new ApplicationException("Нельзя изменять утвержденное распределение.");
        if (null == entrantId)
            return;

        final Session session = getSession();
        final Iterator<ModelBase.Row> it = model.getEntrantRateRows().iterator();
        while (it.hasNext())
        {
            final ModelBase.Row n = it.next();
            if (entrantId.equals(n.getId()))
            {
                final MQBuilder builder = new MQBuilder(EcgDistribRelationGen.ENTITY_CLASS, "r");
                builder.add(MQExpression.notEq("r", EcgDistribRelationGen.L_QUOTA + "." + EcgDistribQuotaGen.L_DISTRIBUTION + "." + EcgDistribObjectGen.P_APPROVED, Boolean.TRUE)); /* важно проверить прямо в sql */
                builder.add(MQExpression.eq("r", EcgDistribRelationGen.L_QUOTA + "." + EcgDistribQuotaGen.L_DISTRIBUTION, model.getDistrib()));
                builder.add(MQExpression.eq("r", EcgDistribRelationGen.L_ENTRANT_DIRECTION, n.getDirection()));

                final List<EcgDistribRelation> relations = builder.getResultList(session);
                if (!relations.isEmpty())
                {
                    for (final EcgDistribRelation rel : relations)
                        session.delete(rel);
                    it.remove();
                }
            }
        }
    }


    // todo сохранять печатные формы при первой печати

    @Override
    @SuppressWarnings("unchecked")
    public byte[] createPrintModelReport(Model model)
    {
        if (model.getDistrib().isApproved() && model.getDistrib().getResultPrintForm() != null && model.getDistrib().getResultPrintForm().getContent() != null)
            return model.getDistrib().getResultPrintForm().getContent();
        IPrintFormCreator<Long> formCreator = (IPrintFormCreator<Long>) ApplicationRuntime.getBean("competitionGroupDistribResultPrint");
        IScriptItem templateDocument = getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ENTRANTS_RATING_CG);
        byte[] content = RtfUtil.toByteArray(formCreator.createPrintForm(templateDocument.getCurrentTemplate(), model.getDistrib().getId()));
        // если распределение утверждено, а печатной формы еще нет
        // (утвердили до того, как сделали функциональность сохранения формы при утверждении),
        // то сохраняем печатную форму - лучше поздно, чем никогда
        if (model.getDistrib().isApproved())
        {
            Session session = getSession();
            DatabaseFile file = new DatabaseFile();
            file.setContent(content);
            session.save(file);
            model.getDistrib().setResultPrintForm(file);
            session.saveOrUpdate(model.getDistrib());
        }
        return content;
    }

    @Override
    @SuppressWarnings("unchecked")
    public byte[] createPrintReservedReport(Model model)
    {
        if (model.getDistrib().isApproved() && model.getDistrib().getReservePrintForm() != null && model.getDistrib().getResultPrintForm().getContent() != null)
            return model.getDistrib().getReservePrintForm().getContent();
        IPrintFormCreator<Long> formCreator = (IPrintFormCreator<Long>) ApplicationRuntime.getBean("competitionGroupDistribReservePrint");
        IScriptItem templateDocument = getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ENTRANTS_RATING_CG_RES);
        byte[] content = RtfUtil.toByteArray(formCreator.createPrintForm(templateDocument.getCurrentTemplate(), model.getDistrib().getId()));
        // если распределение утверждено, а печатной формы еще нет
        // (утвердили до того, как сделали функциональность сохранения формы при утверждении),
        // то сохраняем печатную форму - лучше поздно, чем никогда
        if (model.getDistrib().isApproved())
        {
            Session session = getSession();
            DatabaseFile file = new DatabaseFile();
            file.setContent(content);
            session.save(file);
            model.getDistrib().setReservePrintForm(file);
            session.saveOrUpdate(model.getDistrib());
        }
        return content;
    }

    @Override
    @SuppressWarnings("unchecked")
    public byte[] createPrintListReport(Model model)
    {
        if (!model.getDistrib().isApproved())
            throw new ApplicationException("Нельзя печатать список для распределения, которое еще не утверждено.");
        IPrintFormCreator<Long> formCreator = (IPrintFormCreator<Long>) ApplicationRuntime.getBean("competitionGroupDistribListPrint");
        IScriptItem templateDocument = getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ECG_DISTRIB_LIST);
        return RtfUtil.toByteArray(formCreator.createPrintForm(templateDocument.getCurrentTemplate(), model.getDistrib().getId()));
    }
}
