/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.wizard.ContactDataStep;

import org.hibernate.Session;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.bo.AddressBase.AddressBaseManager;
import org.tandemframework.shared.fias.base.entity.Address;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressCountry;

import org.tandemframework.shared.fias.utils.AddressBaseUtils;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonContactData;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;

/**
 * @author vip_delete
 * @since 16.06.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setEntrant(getNotNull(Entrant.class, model.getEntrant().getId()));
        model.setAddress(model.getEntrant().getPerson().getAddress());

        Long onlineEntrantId = model.getOnlineEntrant().getId();
        if (onlineEntrantId != null)
        {
            model.setOnlineEntrant(get(OnlineEntrant.class, onlineEntrantId));
            prepareOnlineEntrantActualAddress(model);
        }

        super.prepare(model);
    }

    @Override
    public void update(Model model)
    {
        final Session session = getSession();
        final Person person = model.getEntrant().getPerson();

        AddressBase address = model.getAddress();
        AddressBaseManager.instance().dao().saveOrUpdateAddressWithoutEntityUpdate(person, address, Person.L_ADDRESS);

        person.getContactData().update(model.getContactData());
        session.saveOrUpdate(person.getContactData());
        session.update(person);
    }

    @Override
    public void doSkip(Model model)
    {
        PersonContactData contactData = model.getEntrant().getPerson().getContactData();
        contactData.setEmail(null);
        contactData.setOther(null);
        contactData.setPhoneFact(null);
        contactData.setPhoneMobile(null);
        contactData.setPhoneRelatives(null);
        contactData.setPhoneWork(null);
        contactData.setPhoneDefault(null);
        contactData.setPhoneReg(null);
        contactData.setPhoneRegTemp(null);
        update(contactData);
    }

    private void prepareOnlineEntrantActualAddress(Model model)
    {
        AddressBase personAddress = model.getAddress();
        OnlineEntrant onlineEntrant = model.getOnlineEntrant();

        if (personAddress == null)
            model.setAddress(AddressBaseUtils.convertFromOldFormat(onlineEntrant.getActualAddress(), false));
        else
            model.setAddress(personAddress);

        Person person = model.getPerson();
        PersonContactData contactData = model.getContactData();
        contactData.update(person.getContactData());

        contactData.setEmail(onlineEntrant.getEmail());
        contactData.setPhoneMobile(onlineEntrant.getPhoneMobile());
        contactData.setPhoneFact(onlineEntrant.getPhoneFact());
        contactData.setPhoneRelatives(onlineEntrant.getPhoneRelatives() != null ? onlineEntrant.getPhoneRelatives() : onlineEntrant.getNextOfKinPhone());
        contactData.setPhoneWork(onlineEntrant.getPhoneWork());
    }
}
