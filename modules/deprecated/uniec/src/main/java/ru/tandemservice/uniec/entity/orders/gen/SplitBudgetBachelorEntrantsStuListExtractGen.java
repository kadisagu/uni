package ru.tandemservice.uniec.entity.orders.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.orders.SplitBudgetBachelorEntrantsStuListExtract;
import ru.tandemservice.uniec.entity.orders.SplitEntrantsStuListExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «О распределении зачисленных на бюджет абитуриентов по профилям (бакалавриат)»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SplitBudgetBachelorEntrantsStuListExtractGen extends SplitEntrantsStuListExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.orders.SplitBudgetBachelorEntrantsStuListExtract";
    public static final String ENTITY_NAME = "splitBudgetBachelorEntrantsStuListExtract";
    public static final int VERSION_HASH = -423675865;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SplitBudgetBachelorEntrantsStuListExtractGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SplitBudgetBachelorEntrantsStuListExtractGen> extends SplitEntrantsStuListExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SplitBudgetBachelorEntrantsStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new SplitBudgetBachelorEntrantsStuListExtract();
        }
    }
    private static final Path<SplitBudgetBachelorEntrantsStuListExtract> _dslPath = new Path<SplitBudgetBachelorEntrantsStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SplitBudgetBachelorEntrantsStuListExtract");
    }
            

    public static class Path<E extends SplitBudgetBachelorEntrantsStuListExtract> extends SplitEntrantsStuListExtract.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return SplitBudgetBachelorEntrantsStuListExtract.class;
        }

        public String getEntityName()
        {
            return "splitBudgetBachelorEntrantsStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
