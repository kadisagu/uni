/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.ws;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.*;

/**
 * Данные онлайн абитуриента (DTO)
 *
 * @author agolubenko
 * @since Apr 29, 2010
 */
@XmlRootElement
public class EntrantData
{
    // ФИО

    public String lastName;

    public String firstName;

    public String middleName;

    // Удостоверение личности

    public Long passportTypeId;

    public String passportSeria;

    public String passportNumber;

    public Long passportCitizenshipId;

    public String passportIssuancePlace;

    public String passportIssuanceCode;

    public Date passportIssuanceDate;

    public Long sexId;

    public Date birthDate;

    public String birthPlace;

    // Адрес регистрации по паспорту

    public Long registrationAddressCountryId;

    public Long registrationAddressSettlementId;

    public Long registrationAddressStreetId;

    public String registrationAddressHouseNumber;

    public String registrationAddressHouseUnitNumber;

    public String registrationAddressFlatNumber;

    // Свидетельство ЕГЭ

    public String certificateNumber;

    public String certificatePlaceCode;

    public Date certificateDate;

    public Map<Long, Integer[]> subjectId2Mark = new HashMap<>();

    // Диплом участника олимпиады

    public String olympiadDiplomaNumber;

    public Date olympiadDiplomaIssuanceDate;

    public Long olympiadDiplomaTypeId;

    public Long olympiadDiplomaDegreeId;

    public String olympiadDiplomaTitle;

    public String olympiadDiplomaSubject;

    public Long olympiadAddressCountryId;

    public Long olympiadAddressSettlementId;

    // Закончил образовательное учреждение

    public Long eduInstitutionCountryId;

    public Long eduInstitutionSettlementId;

    public Long eduInstitutionId;

    public String eduInstitutionStr;

    // Документ об образовании

    public Long eduDocumentLevelId;

    public Long eduDocumentTypeId;

    public String eduDocumentSeria;

    public String eduDocumentNumber;

    public Integer eduDocumentYearEnd;

    public Date eduDocumentDate;

    public Long eduDocumentGraduationHonourId;

    public Integer mark5;

    public Integer mark4;

    public Integer mark3;

    // Ближайшие родственники

    public Long nextOfKinRelationDegreeId;

    public String nextOfKinLastName;

    public String nextOfKinFirstName;

    public String nextOfKinMiddleName;

    public String nextOfKinWorkPlace;

    public String nextOfKinWorkPost;

    public String nextOfKinPhone;

    // Контактные данные

    public String homePhoneNumber;

    public String mobilePhoneNumber;

    public String contactEmail;

    public String relativePhoneNumber;

    // Фактический адрес

    public boolean liveAtRegistration;

    public Long actualAddressCountryId;

    public Long actualAddressSettlementId;

    public Long actualAddressStreetId;

    public String actualAddressHouseNumber;

    public String actualAddressHouseUnitNumber;

    public String actualAddressFlatNumber;

    // Иностранный язык

    public Long foreignLanguageId;

    // Спортивные достижения

    public Long sportTypeId;

    public Long sportRankId;

    // Льгота при поступлении

    public Long benefitId;

    public String benefitDocument;

    public Date benefitDate;

    public String benefitBasic;

    // Дополнительные сведения

    public Long familyStatusId;

    public Integer childCount;

    public String workPlace;

    public String workPost;

    public boolean needHotel;

    public Date beginArmy;

    public Date endArmy;

    public Integer endArmyYear;

    public boolean participateInSecondWaveUSE;

    public boolean participateInEntranceTests;

    public Set<Long> universityInfoSourceIds = new HashSet<>();

    public Long methodDeliveryNReturnDocsId;
}
