/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.settings.UsedCompetitionKindList;

import java.util.List;

import org.hibernate.Session;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.settings.EnrollmentCompetitionKind;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

/**
 * @author vip_delete
 * @since 12.02.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(EnrollmentCompetitionKind.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", EnrollmentCompetitionKind.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
        builder.addOrder("e", EnrollmentCompetitionKind.P_PRIORITY);
        List<EnrollmentCompetitionKind> list = builder.getResultList(getSession());

        model.getDataSource().setCountRow(list.size());
        UniBaseUtils.createPage(model.getDataSource(), list);
    }

    @Override
    public void updateToggleUsed(Long orderTypeId)
    {
        final Session session = getSession();

        EnrollmentCompetitionKind enrollmentOrderType = getNotNull(EnrollmentCompetitionKind.class, orderTypeId);
        session.refresh(enrollmentOrderType);
        enrollmentOrderType.setUsed(!enrollmentOrderType.isUsed());
        session.update(enrollmentOrderType);
    }

    @Override
    @SuppressWarnings("deprecation")
    public void updatePriority(Model model, Long orderTypeId, boolean up)
    {
        MQBuilder builder = new MQBuilder(EnrollmentCompetitionKind.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", EnrollmentCompetitionKind.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
        builder.addOrder("e", EnrollmentCompetitionKind.P_PRIORITY);
        List<IEntity> list = builder.getResultList(getSession());

        UniBaseUtils.changePriority(orderTypeId, list, EnrollmentCompetitionKind.P_PRIORITY, up, getSession());
    }
}
