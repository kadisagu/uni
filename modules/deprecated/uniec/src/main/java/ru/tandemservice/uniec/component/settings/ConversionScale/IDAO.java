/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.settings.ConversionScale;

import org.tandemframework.core.info.ErrorCollector;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author agolubenko
 * @since 25.06.2008
 */
public interface IDAO extends IUniDao<Model>
{
    void prepareMatrices(Model model);

    /**
     * Обновляет данные шкал перевода по бюджету
     */
    void updateBudget(Model model);

    /**
     * Обновляет данные шкал перевода по контракту
     */
    void updateContract(Model model);
    
    /**
     * Валидирует данные шкал перевода по бюджету
     */
    void validateBudget(Model model, ErrorCollector errorCollector);

    /**
     * Валидирует данные шкал перевода по контракту
     */
    void validateContract(Model model, ErrorCollector errorCollector);
}
