/**
 *$Id$
 */
package ru.tandemservice.uniec.base.bo.EcReport.ui.EnrollmentDirProfPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uniec.entity.report.EnrollmentDirProfReport;

/**
 * @author Alexander Shaburov
 * @since 04.07.12
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "reportId", required = true)
})
public class EcReportEnrollmentDirProfPubUI extends UIPresenter
{
    // fields

    private Long _reportId;
    private EnrollmentDirProfReport _report;
    private String _requestFromToPeriod;


    // Presenter listeners

    @Override
    public void onComponentRefresh()
    {
        _report = DataAccessServices.dao().getNotNull(EnrollmentDirProfReport.class, _reportId);

        _requestFromToPeriod = DateFormatter.DEFAULT_DATE_FORMATTER.format(_report.getRequestDateFrom()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(_report.getRequestDateTo());
    }


    // Listeners

    public void onClickPrint()
    {
        getActivationBuilder().asDesktopRoot(IUniComponents.DOWNLOAD_STORABLE_REPORT)
                .parameter("reportId", _reportId)
                .parameter("extension", "xls")
                .activate();
    }


    // Getters & Setters

    public Long getReportId()
    {
        return _reportId;
    }

    public void setReportId(Long reportId)
    {
        _reportId = reportId;
    }

    public EnrollmentDirProfReport getReport()
    {
        return _report;
    }

    public void setReport(EnrollmentDirProfReport report)
    {
        _report = report;
    }

    public String getRequestFromToPeriod()
    {
        return _requestFromToPeriod;
    }

    public void setRequestFromToPeriod(String requestFromToPeriod)
    {
        _requestFromToPeriod = requestFromToPeriod;
    }
}
