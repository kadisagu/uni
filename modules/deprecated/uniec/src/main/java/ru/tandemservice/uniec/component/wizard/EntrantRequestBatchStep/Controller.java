/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.wizard.EntrantRequestBatchStep;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.uniec.component.wizard.IEntrantWizardComponents;

/**
 * @author vip_delete
 * @since 18.06.2009
 */
public class Controller extends ru.tandemservice.uniec.component.entrant.EntrantRequestBatchAddEdit.Controller
{
    // Wizard Actions

    @Override
    public void onClickNext(IBusinessComponent component)
    {
        getDao().update(getModel(component));

        deactivate(component);

        component.getParentRegion().getOwner().createDefaultChildRegion(new ComponentActivator(IEntrantWizardComponents.ENTRANT_DOCUMENT_STEP, new ParametersMap()
                .add("entrantRequestId", getModel(component).getEntrantRequest().getId())
                .add("addForm", Boolean.TRUE)
                .add("entrantMasterPermKey", ((Model) getModel(component)).getEntrantMasterPermKey())
        ));
    }

    public void onClickStop(IBusinessComponent component)
    {
        getDao().update(getModel(component));

        deactivate(component);

        activateInRoot(component, new PublisherActivator(getModel(component).getEntrantRequest().getEntrant(), new ParametersMap()
                .add("selectedTab", "entrantTab")
                .add("selectedDataTab", "entranceDisciplineTab")
                .add("entrantMasterPermKey", ((Model)getModel(component)).getEntrantMasterPermKey())
        ));
    }
}
