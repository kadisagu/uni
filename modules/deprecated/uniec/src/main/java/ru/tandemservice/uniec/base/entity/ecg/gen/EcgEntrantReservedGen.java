package ru.tandemservice.uniec.base.entity.ecg.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistribution;
import ru.tandemservice.uniec.base.entity.ecg.EcgEntrantReserved;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Резервный абитуриент основного распределения
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EcgEntrantReservedGen extends EntityBase
 implements INaturalIdentifiable<EcgEntrantReservedGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.base.entity.ecg.EcgEntrantReserved";
    public static final String ENTITY_NAME = "ecgEntrantReserved";
    public static final int VERSION_HASH = -354388929;
    private static IEntityMeta ENTITY_META;

    public static final String L_DISTRIBUTION = "distribution";
    public static final String L_ENTRANT = "entrant";
    public static final String P_BRING_ORIGINAL = "bringOriginal";
    public static final String P_HAS_PRE_STUDENT = "hasPreStudent";

    private EcgDistribution _distribution;     // Основное распределение абитуриентов
    private Entrant _entrant;     // (Старый) Абитуриент
    private Boolean _bringOriginal;     // Принес оригиналы
    private Boolean _hasPreStudent;     // Есть предзачисление

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Основное распределение абитуриентов. Свойство не может быть null.
     */
    @NotNull
    public EcgDistribution getDistribution()
    {
        return _distribution;
    }

    /**
     * @param distribution Основное распределение абитуриентов. Свойство не может быть null.
     */
    public void setDistribution(EcgDistribution distribution)
    {
        dirty(_distribution, distribution);
        _distribution = distribution;
    }

    /**
     * @return (Старый) Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public Entrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant (Старый) Абитуриент. Свойство не может быть null.
     */
    public void setEntrant(Entrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Принес оригиналы.
     */
    public Boolean getBringOriginal()
    {
        return _bringOriginal;
    }

    /**
     * @param bringOriginal Принес оригиналы.
     */
    public void setBringOriginal(Boolean bringOriginal)
    {
        dirty(_bringOriginal, bringOriginal);
        _bringOriginal = bringOriginal;
    }

    /**
     * @return Есть предзачисление.
     */
    public Boolean getHasPreStudent()
    {
        return _hasPreStudent;
    }

    /**
     * @param hasPreStudent Есть предзачисление.
     */
    public void setHasPreStudent(Boolean hasPreStudent)
    {
        dirty(_hasPreStudent, hasPreStudent);
        _hasPreStudent = hasPreStudent;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EcgEntrantReservedGen)
        {
            if (withNaturalIdProperties)
            {
                setDistribution(((EcgEntrantReserved)another).getDistribution());
                setEntrant(((EcgEntrantReserved)another).getEntrant());
            }
            setBringOriginal(((EcgEntrantReserved)another).getBringOriginal());
            setHasPreStudent(((EcgEntrantReserved)another).getHasPreStudent());
        }
    }

    public INaturalId<EcgEntrantReservedGen> getNaturalId()
    {
        return new NaturalId(getDistribution(), getEntrant());
    }

    public static class NaturalId extends NaturalIdBase<EcgEntrantReservedGen>
    {
        private static final String PROXY_NAME = "EcgEntrantReservedNaturalProxy";

        private Long _distribution;
        private Long _entrant;

        public NaturalId()
        {}

        public NaturalId(EcgDistribution distribution, Entrant entrant)
        {
            _distribution = ((IEntity) distribution).getId();
            _entrant = ((IEntity) entrant).getId();
        }

        public Long getDistribution()
        {
            return _distribution;
        }

        public void setDistribution(Long distribution)
        {
            _distribution = distribution;
        }

        public Long getEntrant()
        {
            return _entrant;
        }

        public void setEntrant(Long entrant)
        {
            _entrant = entrant;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EcgEntrantReservedGen.NaturalId) ) return false;

            EcgEntrantReservedGen.NaturalId that = (NaturalId) o;

            if( !equals(getDistribution(), that.getDistribution()) ) return false;
            if( !equals(getEntrant(), that.getEntrant()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getDistribution());
            result = hashCode(result, getEntrant());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getDistribution());
            sb.append("/");
            sb.append(getEntrant());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EcgEntrantReservedGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EcgEntrantReserved.class;
        }

        public T newInstance()
        {
            return (T) new EcgEntrantReserved();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "distribution":
                    return obj.getDistribution();
                case "entrant":
                    return obj.getEntrant();
                case "bringOriginal":
                    return obj.getBringOriginal();
                case "hasPreStudent":
                    return obj.getHasPreStudent();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "distribution":
                    obj.setDistribution((EcgDistribution) value);
                    return;
                case "entrant":
                    obj.setEntrant((Entrant) value);
                    return;
                case "bringOriginal":
                    obj.setBringOriginal((Boolean) value);
                    return;
                case "hasPreStudent":
                    obj.setHasPreStudent((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "distribution":
                        return true;
                case "entrant":
                        return true;
                case "bringOriginal":
                        return true;
                case "hasPreStudent":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "distribution":
                    return true;
                case "entrant":
                    return true;
                case "bringOriginal":
                    return true;
                case "hasPreStudent":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "distribution":
                    return EcgDistribution.class;
                case "entrant":
                    return Entrant.class;
                case "bringOriginal":
                    return Boolean.class;
                case "hasPreStudent":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EcgEntrantReserved> _dslPath = new Path<EcgEntrantReserved>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EcgEntrantReserved");
    }
            

    /**
     * @return Основное распределение абитуриентов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgEntrantReserved#getDistribution()
     */
    public static EcgDistribution.Path<EcgDistribution> distribution()
    {
        return _dslPath.distribution();
    }

    /**
     * @return (Старый) Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgEntrantReserved#getEntrant()
     */
    public static Entrant.Path<Entrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Принес оригиналы.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgEntrantReserved#getBringOriginal()
     */
    public static PropertyPath<Boolean> bringOriginal()
    {
        return _dslPath.bringOriginal();
    }

    /**
     * @return Есть предзачисление.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgEntrantReserved#getHasPreStudent()
     */
    public static PropertyPath<Boolean> hasPreStudent()
    {
        return _dslPath.hasPreStudent();
    }

    public static class Path<E extends EcgEntrantReserved> extends EntityPath<E>
    {
        private EcgDistribution.Path<EcgDistribution> _distribution;
        private Entrant.Path<Entrant> _entrant;
        private PropertyPath<Boolean> _bringOriginal;
        private PropertyPath<Boolean> _hasPreStudent;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Основное распределение абитуриентов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgEntrantReserved#getDistribution()
     */
        public EcgDistribution.Path<EcgDistribution> distribution()
        {
            if(_distribution == null )
                _distribution = new EcgDistribution.Path<EcgDistribution>(L_DISTRIBUTION, this);
            return _distribution;
        }

    /**
     * @return (Старый) Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgEntrantReserved#getEntrant()
     */
        public Entrant.Path<Entrant> entrant()
        {
            if(_entrant == null )
                _entrant = new Entrant.Path<Entrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Принес оригиналы.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgEntrantReserved#getBringOriginal()
     */
        public PropertyPath<Boolean> bringOriginal()
        {
            if(_bringOriginal == null )
                _bringOriginal = new PropertyPath<Boolean>(EcgEntrantReservedGen.P_BRING_ORIGINAL, this);
            return _bringOriginal;
        }

    /**
     * @return Есть предзачисление.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgEntrantReserved#getHasPreStudent()
     */
        public PropertyPath<Boolean> hasPreStudent()
        {
            if(_hasPreStudent == null )
                _hasPreStudent = new PropertyPath<Boolean>(EcgEntrantReservedGen.P_HAS_PRE_STUDENT, this);
            return _hasPreStudent;
        }

        public Class getEntityClass()
        {
            return EcgEntrantReserved.class;
        }

        public String getEntityName()
        {
            return "ecgEntrantReserved";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
