/* $Id: $ */
package ru.tandemservice.uniec.base.bo.EcSettings.logic;

import org.tandemframework.shared.survey.base.entity.SurveyTemplate;

/**
 * @author Andrey Andreev
 * @since 21.03.2017
 */
public interface ITemplate2CampaignRelDAO
{

    /**
     * Устанавливает для выбранной приемной кампании шаблон анкетирования.
     */
    void setSurveyTemplate2EnrCampaignRel(Long campaignId, SurveyTemplate template);
}
