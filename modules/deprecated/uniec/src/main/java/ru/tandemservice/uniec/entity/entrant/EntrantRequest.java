package ru.tandemservice.uniec.entity.entrant;

import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.entrant.gen.EntrantRequestGen;

public class EntrantRequest extends EntrantRequestGen
{
    public static final String P_STRING_NUMBER = "stringNumber";

    public String getTitle()
    {
        return "Заявление №" + getStringNumber();
    }

    public String getStringNumber()
    {
        return UniecDAOFacade.getEntrantRequestNumberFormatter().format(getRegNumber());
    }

    public static EntityComboDataSourceHandler defaultSelectDSHandler(String name)
    {
        return new EntityComboDataSourceHandler(name, EntrantRequest.class)
                .order(EntrantRequest.regNumber())
                .filter(EntrantRequest.regNumber());
    }
}