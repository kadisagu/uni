/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec.base.bo.EcProfileDistribution.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniec.base.bo.EcDistribution.logic.EcSimpleDSHandler;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgEntrantRateRowDTO;
import ru.tandemservice.uniec.base.bo.EcProfileDistribution.EcProfileDistributionManager;
import ru.tandemservice.uniec.base.bo.EcProfileDistribution.ui.EntrantAdd.EcProfileDistributionEntrantAdd;
import ru.tandemservice.uniec.base.bo.EcProfileDistribution.ui.EntrantAdd.EcProfileDistributionEntrantAddUI;
import ru.tandemservice.uniec.base.bo.EcProfileDistribution.util.EcgpConfigDTO;
import ru.tandemservice.uniec.base.bo.EcProfileDistribution.util.EcgpDistributionDTO;
import ru.tandemservice.uniec.base.bo.EcProfileDistribution.util.IEcgpQuotaFreeDTO;
import ru.tandemservice.uniec.base.bo.EcProfileDistribution.util.IEcgpRecommendedDTO;
import ru.tandemservice.uniec.base.entity.ecgp.EcgpDistribution;
import ru.tandemservice.uniec.base.entity.ecgp.EcgpEntrantRecommended;
import ru.tandemservice.uniec.entity.entrant.ProfileEducationOrgUnit;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Vasily Zhukov
 * @since 10.05.2012
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "distributionId", required = true),
        @Bind(key = "selectedTab", binding = "selectedTab")
})
public class EcProfileDistributionPubUI extends UIPresenter
{
    private Long _distributionId;
    private EcgpDistribution _distribution;
    private String _selectedTab;
    private String _distributionCategoryTitle;
    private IEcgpQuotaFreeDTO _freeQuotaDTO;
    private Map<Long, String> _quotaMap;
    private Set<Long> _recommendedIds;

    @Override
    public void onComponentRefresh()
    {
        _distribution = DataAccessServices.dao().getNotNull(_distributionId);

        // категория поступающего
        _distributionCategoryTitle = _distribution.getConfig().isSecondHighAdmission() ? EcProfileDistributionManager.DISTRIBUTION_CATEGORY_SECOND_HIGH_ADMISSION.getTitle() : EcProfileDistributionManager.DISTRIBUTION_CATEGORY_STUDENTS_AND_LISTENERS.getTitle();

        _freeQuotaDTO = EcProfileDistributionManager.instance().dao().getFreeQuotaDTO(_distribution);

        _quotaMap = EcProfileDistributionManager.instance().dao().getQuotaHTMLDescription(_freeQuotaDTO);

        _recommendedIds = new HashSet<Long>();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (EcProfileDistributionPub.DIRECTION_DS.equals(dataSource.getName()))
        {
            List<ProfileEducationOrgUnit> list = EcProfileDistributionManager.instance().dao().getDistributionProfileList(new EcgpConfigDTO(_distribution.getConfig()));

            if (_distribution.getConfig().getEcgItem().getEnrollmentCampaign().isUseCompetitionGroup())
            {
                ProfileEducationOrgUnit emptyLine = new ProfileEducationOrgUnit();
                emptyLine.setId(0L);
                list.add(emptyLine);
            }

            dataSource.put(EcSimpleDSHandler.LIST, list);
        } else if (EcProfileDistributionPub.RECOMMENDED_DS.equals(dataSource.getName()))
        {
            List<IEcgpRecommendedDTO> list = EcProfileDistributionManager.instance().dao().getEntrantRecommendedRowList(_distribution);
            dataSource.put(EcSimpleDSHandler.LIST, list);
            dataSource.put(EcSimpleDSHandler.PAGEABLE, true);
        }
    }

    // quota

    public String getQuota()
    {
        ProfileEducationOrgUnit item = (ProfileEducationOrgUnit) getConfig().getDataSource(EcProfileDistributionPub.DIRECTION_DS).getCurrent();
        return _quotaMap.get(item.getId());
    }

    // used

    public String getUsed()
    {
        ProfileEducationOrgUnit item = (ProfileEducationOrgUnit) getConfig().getDataSource(EcProfileDistributionPub.DIRECTION_DS).getCurrent();
        if (item.getId() == 0L) // последняя строка итого
            return _freeQuotaDTO.getQuotaUsedDTO().getTotalUsed() == 0 ? null : Integer.toString(_freeQuotaDTO.getQuotaUsedDTO().getTotalUsed());
        Integer used = _freeQuotaDTO.getQuotaUsedDTO().getUsedMap().get(item.getId());
        return used == null || used == 0 ? null : Integer.toString(used);
    }

    // Getters & Setters

    public Long getDistributionId()
    {
        return _distributionId;
    }

    public void setDistributionId(Long distributionId)
    {
        _distributionId = distributionId;
    }

    public EcgpDistribution getDistribution()
    {
        return _distribution;
    }

    public void setDistribution(EcgpDistribution distribution)
    {
        _distribution = distribution;
    }

    public String getSelectedTab()
    {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        _selectedTab = selectedTab;
    }

    public String getDistributionCategoryTitle()
    {
        return _distributionCategoryTitle;
    }

    public void setDistributionCategoryTitle(String distributionCategoryTitle)
    {
        _distributionCategoryTitle = distributionCategoryTitle;
    }

    public IEcgpQuotaFreeDTO getFreeQuotaDTO()
    {
        return _freeQuotaDTO;
    }

    public void setFreeQuotaDTO(IEcgpQuotaFreeDTO freeQuotaDTO)
    {
        _freeQuotaDTO = freeQuotaDTO;
    }

    public Map<Long, String> getQuotaMap()
    {
        return _quotaMap;
    }

    public void setQuotaMap(Map<Long, String> quotaMap)
    {
        _quotaMap = quotaMap;
    }

    public Set<Long> getRecommendedIds()
    {
        return _recommendedIds;
    }

    public void setRecommendedIds(Set<Long> recommendedIds)
    {
        _recommendedIds = recommendedIds;
    }

    // Listeners

    public void onClickAddByTA()
    {
        _uiSupport.doRefresh();

        _uiActivation.asRegion(EcProfileDistributionEntrantAdd.class)
                .parameter(EcProfileDistributionEntrantAddUI.DISTRIBUTION, _distribution)
                .parameter(EcProfileDistributionEntrantAddUI.RATE_RULE, IEcgEntrantRateRowDTO.Rule.RULE_TARGET_ADMISSION)
                .activate();
    }

    public void onClickAddByOutOfCompetition()
    {
        _uiSupport.doRefresh();

        _uiActivation.asRegion(EcProfileDistributionEntrantAdd.class)
                .parameter(EcProfileDistributionEntrantAddUI.DISTRIBUTION, _distribution)
                .parameter(EcProfileDistributionEntrantAddUI.RATE_RULE, IEcgEntrantRateRowDTO.Rule.RULE_NON_COMPETITIVE)
                .activate();
    }

    public void onClickAddByCompetition()
    {
        _uiSupport.doRefresh();

        _uiActivation.asRegion(EcProfileDistributionEntrantAdd.class)
                .parameter(EcProfileDistributionEntrantAddUI.DISTRIBUTION, _distribution)
                .parameter(EcProfileDistributionEntrantAddUI.RATE_RULE, IEcgEntrantRateRowDTO.Rule.RULE_COMPETITIVE)
                .activate();
    }

    public void onDeleteEntityFromList()
    {
        _uiSupport.doRefresh();

        try
        {
            EcProfileDistributionManager.instance().dao().deleteEntrantRecommended((EcgpEntrantRecommended) DataAccessServices.dao().getNotNull(getListenerParameterAsLong()));
        } finally
        {
            onComponentRefresh();
        }
    }
}
