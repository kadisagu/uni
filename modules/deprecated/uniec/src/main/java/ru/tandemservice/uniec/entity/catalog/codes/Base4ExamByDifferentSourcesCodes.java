package ru.tandemservice.uniec.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Основание для сдачи ВИ по материалам ОУ"
 * Имя сущности : base4ExamByDifferentSources
 * Файл data.xml : uniec.catalogs.data.xml
 */
public interface Base4ExamByDifferentSourcesCodes
{
    /** Константа кода (code) элемента : Иностранные граждане (title) */
    String FOREIGN = "enr.base4Exam.foreign";
    /** Константа кода (code) элемента : Лица, получившие диплом СПО (title) */
    String SPO = "enr.base4Exam.spo";
    /** Константа кода (code) элемента : Лица, постоянно проживающие в Крыму (title) */
    String CRIMEA = "enr.base4Exam.crimea";
    /** Константа кода (code) элемента : Лица с ограниченными возможностями здоровья (title) */
    String HEALTH = "enr.base4Exam.health";

    Set<String> CODES = ImmutableSet.of(FOREIGN, SPO, CRIMEA, HEALTH);
}
