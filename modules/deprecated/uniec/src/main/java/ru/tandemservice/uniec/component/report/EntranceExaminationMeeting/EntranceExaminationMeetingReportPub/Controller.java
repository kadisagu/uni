/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.report.EntranceExaminationMeeting.EntranceExaminationMeetingReportPub;

import java.util.HashMap;
import java.util.Map;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport;

/**
 * @author agolubenko
 * @since 11.11.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickPrint(IBusinessComponent component)
    {
        EntranceExaminationMeetingReport report = getModel(component).getReport();

        boolean doZip = report.getEducationLevelsHighSchool() != null;
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("reportId", report.getId());
        parameters.put("extension", (report.getEducationLevelsHighSchool() != null) ? "xls" : "zip");
        parameters.put("zip", doZip);
        activateInRoot(component, new ComponentActivator(IUniComponents.DOWNLOAD_STORABLE_REPORT, parameters));
    }
}
