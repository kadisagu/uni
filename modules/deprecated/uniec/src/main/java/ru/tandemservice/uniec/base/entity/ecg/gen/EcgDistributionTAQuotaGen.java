package ru.tandemservice.uniec.base.entity.ecg.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistributionQuota;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistributionTAQuota;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Количество мест по целевому приему по направлению приема в основном распределении
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EcgDistributionTAQuotaGen extends EntityBase
 implements INaturalIdentifiable<EcgDistributionTAQuotaGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.base.entity.ecg.EcgDistributionTAQuota";
    public static final String ENTITY_NAME = "ecgDistributionTAQuota";
    public static final int VERSION_HASH = -1504242617;
    private static IEntityMeta ENTITY_META;

    public static final String L_DISTRIBUTION_QUOTA = "distributionQuota";
    public static final String L_TARGET_ADMISSION_KIND = "targetAdmissionKind";
    public static final String P_QUOTA = "quota";

    private EcgDistributionQuota _distributionQuota;     // Количество мест по направлению приема в основном распределении
    private TargetAdmissionKind _targetAdmissionKind;     // Вид целевого приема
    private int _quota;     // Количество мест

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Количество мест по направлению приема в основном распределении. Свойство не может быть null.
     */
    @NotNull
    public EcgDistributionQuota getDistributionQuota()
    {
        return _distributionQuota;
    }

    /**
     * @param distributionQuota Количество мест по направлению приема в основном распределении. Свойство не может быть null.
     */
    public void setDistributionQuota(EcgDistributionQuota distributionQuota)
    {
        dirty(_distributionQuota, distributionQuota);
        _distributionQuota = distributionQuota;
    }

    /**
     * @return Вид целевого приема. Свойство не может быть null.
     */
    @NotNull
    public TargetAdmissionKind getTargetAdmissionKind()
    {
        return _targetAdmissionKind;
    }

    /**
     * @param targetAdmissionKind Вид целевого приема. Свойство не может быть null.
     */
    public void setTargetAdmissionKind(TargetAdmissionKind targetAdmissionKind)
    {
        dirty(_targetAdmissionKind, targetAdmissionKind);
        _targetAdmissionKind = targetAdmissionKind;
    }

    /**
     * @return Количество мест. Свойство не может быть null.
     */
    @NotNull
    public int getQuota()
    {
        return _quota;
    }

    /**
     * @param quota Количество мест. Свойство не может быть null.
     */
    public void setQuota(int quota)
    {
        dirty(_quota, quota);
        _quota = quota;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EcgDistributionTAQuotaGen)
        {
            if (withNaturalIdProperties)
            {
                setDistributionQuota(((EcgDistributionTAQuota)another).getDistributionQuota());
                setTargetAdmissionKind(((EcgDistributionTAQuota)another).getTargetAdmissionKind());
            }
            setQuota(((EcgDistributionTAQuota)another).getQuota());
        }
    }

    public INaturalId<EcgDistributionTAQuotaGen> getNaturalId()
    {
        return new NaturalId(getDistributionQuota(), getTargetAdmissionKind());
    }

    public static class NaturalId extends NaturalIdBase<EcgDistributionTAQuotaGen>
    {
        private static final String PROXY_NAME = "EcgDistributionTAQuotaNaturalProxy";

        private Long _distributionQuota;
        private Long _targetAdmissionKind;

        public NaturalId()
        {}

        public NaturalId(EcgDistributionQuota distributionQuota, TargetAdmissionKind targetAdmissionKind)
        {
            _distributionQuota = ((IEntity) distributionQuota).getId();
            _targetAdmissionKind = ((IEntity) targetAdmissionKind).getId();
        }

        public Long getDistributionQuota()
        {
            return _distributionQuota;
        }

        public void setDistributionQuota(Long distributionQuota)
        {
            _distributionQuota = distributionQuota;
        }

        public Long getTargetAdmissionKind()
        {
            return _targetAdmissionKind;
        }

        public void setTargetAdmissionKind(Long targetAdmissionKind)
        {
            _targetAdmissionKind = targetAdmissionKind;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EcgDistributionTAQuotaGen.NaturalId) ) return false;

            EcgDistributionTAQuotaGen.NaturalId that = (NaturalId) o;

            if( !equals(getDistributionQuota(), that.getDistributionQuota()) ) return false;
            if( !equals(getTargetAdmissionKind(), that.getTargetAdmissionKind()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getDistributionQuota());
            result = hashCode(result, getTargetAdmissionKind());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getDistributionQuota());
            sb.append("/");
            sb.append(getTargetAdmissionKind());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EcgDistributionTAQuotaGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EcgDistributionTAQuota.class;
        }

        public T newInstance()
        {
            return (T) new EcgDistributionTAQuota();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "distributionQuota":
                    return obj.getDistributionQuota();
                case "targetAdmissionKind":
                    return obj.getTargetAdmissionKind();
                case "quota":
                    return obj.getQuota();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "distributionQuota":
                    obj.setDistributionQuota((EcgDistributionQuota) value);
                    return;
                case "targetAdmissionKind":
                    obj.setTargetAdmissionKind((TargetAdmissionKind) value);
                    return;
                case "quota":
                    obj.setQuota((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "distributionQuota":
                        return true;
                case "targetAdmissionKind":
                        return true;
                case "quota":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "distributionQuota":
                    return true;
                case "targetAdmissionKind":
                    return true;
                case "quota":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "distributionQuota":
                    return EcgDistributionQuota.class;
                case "targetAdmissionKind":
                    return TargetAdmissionKind.class;
                case "quota":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EcgDistributionTAQuota> _dslPath = new Path<EcgDistributionTAQuota>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EcgDistributionTAQuota");
    }
            

    /**
     * @return Количество мест по направлению приема в основном распределении. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgDistributionTAQuota#getDistributionQuota()
     */
    public static EcgDistributionQuota.Path<EcgDistributionQuota> distributionQuota()
    {
        return _dslPath.distributionQuota();
    }

    /**
     * @return Вид целевого приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgDistributionTAQuota#getTargetAdmissionKind()
     */
    public static TargetAdmissionKind.Path<TargetAdmissionKind> targetAdmissionKind()
    {
        return _dslPath.targetAdmissionKind();
    }

    /**
     * @return Количество мест. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgDistributionTAQuota#getQuota()
     */
    public static PropertyPath<Integer> quota()
    {
        return _dslPath.quota();
    }

    public static class Path<E extends EcgDistributionTAQuota> extends EntityPath<E>
    {
        private EcgDistributionQuota.Path<EcgDistributionQuota> _distributionQuota;
        private TargetAdmissionKind.Path<TargetAdmissionKind> _targetAdmissionKind;
        private PropertyPath<Integer> _quota;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Количество мест по направлению приема в основном распределении. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgDistributionTAQuota#getDistributionQuota()
     */
        public EcgDistributionQuota.Path<EcgDistributionQuota> distributionQuota()
        {
            if(_distributionQuota == null )
                _distributionQuota = new EcgDistributionQuota.Path<EcgDistributionQuota>(L_DISTRIBUTION_QUOTA, this);
            return _distributionQuota;
        }

    /**
     * @return Вид целевого приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgDistributionTAQuota#getTargetAdmissionKind()
     */
        public TargetAdmissionKind.Path<TargetAdmissionKind> targetAdmissionKind()
        {
            if(_targetAdmissionKind == null )
                _targetAdmissionKind = new TargetAdmissionKind.Path<TargetAdmissionKind>(L_TARGET_ADMISSION_KIND, this);
            return _targetAdmissionKind;
        }

    /**
     * @return Количество мест. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgDistributionTAQuota#getQuota()
     */
        public PropertyPath<Integer> quota()
        {
            if(_quota == null )
                _quota = new PropertyPath<Integer>(EcgDistributionTAQuotaGen.P_QUOTA, this);
            return _quota;
        }

        public Class getEntityClass()
        {
            return EcgDistributionTAQuota.class;
        }

        public String getEntityName()
        {
            return "ecgDistributionTAQuota";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
