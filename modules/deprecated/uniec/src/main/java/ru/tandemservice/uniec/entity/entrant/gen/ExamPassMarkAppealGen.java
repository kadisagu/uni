package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.entrant.ExamPassMark;
import ru.tandemservice.uniec.entity.entrant.ExamPassMarkAppeal;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Апелляция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ExamPassMarkAppealGen extends VersionedEntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.ExamPassMarkAppeal";
    public static final String ENTITY_NAME = "examPassMarkAppeal";
    public static final int VERSION_HASH = -161772639;
    private static IEntityMeta ENTITY_META;

    public static final String P_VERSION = "version";
    public static final String L_EXAM_PASS_MARK = "examPassMark";
    public static final String P_MARK = "mark";

    private int _version; 
    private ExamPassMark _examPassMark;     // Оценка по дисциплине для сдачи
    private double _mark;     // Балл

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public int getVersion()
    {
        return _version;
    }

    /**
     * @param version  Свойство не может быть null.
     */
    public void setVersion(int version)
    {
        dirty(_version, version);
        _version = version;
    }

    /**
     * @return Оценка по дисциплине для сдачи. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public ExamPassMark getExamPassMark()
    {
        return _examPassMark;
    }

    /**
     * @param examPassMark Оценка по дисциплине для сдачи. Свойство не может быть null и должно быть уникальным.
     */
    public void setExamPassMark(ExamPassMark examPassMark)
    {
        dirty(_examPassMark, examPassMark);
        _examPassMark = examPassMark;
    }

    /**
     * @return Балл. Свойство не может быть null.
     */
    @NotNull
    public double getMark()
    {
        return _mark;
    }

    /**
     * @param mark Балл. Свойство не может быть null.
     */
    public void setMark(double mark)
    {
        dirty(_mark, mark);
        _mark = mark;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ExamPassMarkAppealGen)
        {
            setVersion(((ExamPassMarkAppeal)another).getVersion());
            setExamPassMark(((ExamPassMarkAppeal)another).getExamPassMark());
            setMark(((ExamPassMarkAppeal)another).getMark());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ExamPassMarkAppealGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ExamPassMarkAppeal.class;
        }

        public T newInstance()
        {
            return (T) new ExamPassMarkAppeal();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "version":
                    return obj.getVersion();
                case "examPassMark":
                    return obj.getExamPassMark();
                case "mark":
                    return obj.getMark();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "version":
                    obj.setVersion((Integer) value);
                    return;
                case "examPassMark":
                    obj.setExamPassMark((ExamPassMark) value);
                    return;
                case "mark":
                    obj.setMark((Double) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "version":
                        return true;
                case "examPassMark":
                        return true;
                case "mark":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "version":
                    return true;
                case "examPassMark":
                    return true;
                case "mark":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "version":
                    return Integer.class;
                case "examPassMark":
                    return ExamPassMark.class;
                case "mark":
                    return Double.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ExamPassMarkAppeal> _dslPath = new Path<ExamPassMarkAppeal>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ExamPassMarkAppeal");
    }
            

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamPassMarkAppeal#getVersion()
     */
    public static PropertyPath<Integer> version()
    {
        return _dslPath.version();
    }

    /**
     * @return Оценка по дисциплине для сдачи. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.entrant.ExamPassMarkAppeal#getExamPassMark()
     */
    public static ExamPassMark.Path<ExamPassMark> examPassMark()
    {
        return _dslPath.examPassMark();
    }

    /**
     * @return Балл. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamPassMarkAppeal#getMark()
     */
    public static PropertyPath<Double> mark()
    {
        return _dslPath.mark();
    }

    public static class Path<E extends ExamPassMarkAppeal> extends EntityPath<E>
    {
        private PropertyPath<Integer> _version;
        private ExamPassMark.Path<ExamPassMark> _examPassMark;
        private PropertyPath<Double> _mark;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamPassMarkAppeal#getVersion()
     */
        public PropertyPath<Integer> version()
        {
            if(_version == null )
                _version = new PropertyPath<Integer>(ExamPassMarkAppealGen.P_VERSION, this);
            return _version;
        }

    /**
     * @return Оценка по дисциплине для сдачи. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.entrant.ExamPassMarkAppeal#getExamPassMark()
     */
        public ExamPassMark.Path<ExamPassMark> examPassMark()
        {
            if(_examPassMark == null )
                _examPassMark = new ExamPassMark.Path<ExamPassMark>(L_EXAM_PASS_MARK, this);
            return _examPassMark;
        }

    /**
     * @return Балл. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamPassMarkAppeal#getMark()
     */
        public PropertyPath<Double> mark()
        {
            if(_mark == null )
                _mark = new PropertyPath<Double>(ExamPassMarkAppealGen.P_MARK, this);
            return _mark;
        }

        public Class getEntityClass()
        {
            return ExamPassMarkAppeal.class;
        }

        public String getEntityName()
        {
            return "examPassMarkAppeal";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
