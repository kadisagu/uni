/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.wizard.PersonSimilarStep;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;

/**
 * @author vip_delete
 * @since 14.06.2009
 */
@Input(@Bind(key = "entrantMasterPermKey", binding = "entrantMasterPermKey"))
public class Model extends org.tandemframework.shared.person.base.bo.Person.ui.SimilarAdd.Model
{
    private String _entrantMasterPermKey;

    public String getEntrantMasterPermKey()
    {
        if(StringUtils.isEmpty(_entrantMasterPermKey)) return "addEntrantMaster";
        return _entrantMasterPermKey;
    }

    public void setEntrantMasterPermKey(String entrantMasterPermKey)
    {
        _entrantMasterPermKey = entrantMasterPermKey;
    }
}
