package ru.tandemservice.uniec.entity.entrant;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.entrant.gen.EntranceDisciplineGen;
import ru.tandemservice.uniec.entity.examset.IExamSetItem;
import ru.tandemservice.uniec.entity.settings.SetDiscipline;

import java.util.List;

public class EntranceDiscipline extends EntranceDisciplineGen implements IExamSetItem, ITitled
{
    public static final String L_CHOICABLE_DISCIPLINES = "choicableDisciplines";

    public List<SetDiscipline> getChoicableDisciplines()
    {
        return UniecDAOFacade.getExamSetDAO().getChoicableDisciplines(this);
    }

    @Override
    public String getTitle()
    {
        return getDiscipline().getTitle();
    }

    @Override
    @SuppressWarnings("unchecked")
    public String getChoiceTitle()
    {
        return CollectionFormatter.COLLECTION_FORMATTER.format(getChoicableDisciplines());
    }
    
    public boolean isProfile()
    {
        return getKind().getCode().equals(UniecDefines.ENTRANCE_DISCIPLINE_KIND_PROFILE);
    }
}