/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.catalog.targetAdmissionKind.TargetAdmissionKindAddEdit;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.settings.NotUsedTargetAdmissionKind;

/**
 * @author agolubenko
 * @since 01.06.2008
 */
public class DAO extends DefaultCatalogAddEditDAO<TargetAdmissionKind, Model> implements IDAO
{
    @Override
    @SuppressWarnings("unchecked")
    public void prepare(Model model)
    {
        super.prepare(model);

        Criteria criteria = getSession().createCriteria(TargetAdmissionKind.class);
        criteria.add(Restrictions.isNull(TargetAdmissionKind.L_PARENT));
        model.setParentList(criteria.list());
    }

    @Override
    public void update(Model model)
    {
        if (model.isAddForm())
        {
            TargetAdmissionKind targetAdmissionKind = model.getCatalogItem();

            Criteria criteria = getSession().createCriteria(TargetAdmissionKind.class);
            criteria.add(targetAdmissionKind.getParent() == null ? Restrictions.isNull(TargetAdmissionKind.L_PARENT) : Restrictions.eq(TargetAdmissionKind.L_PARENT, targetAdmissionKind.getParent()));
            criteria.setProjection(Projections.max(TargetAdmissionKind.P_PRIORITY));
            Number max = (Number) criteria.uniqueResult();
            targetAdmissionKind.setPriority((max == null) ? 1 : max.intValue() + 1);
        }
        super.update(model);
        if (model.isAddForm())
        {
            for (EnrollmentCampaign campaign : getList(EnrollmentCampaign.class))
            {
                NotUsedTargetAdmissionKind settings = new NotUsedTargetAdmissionKind();
                settings.setEnrollmentCampaign(campaign);
                settings.setTargetAdmissionKind(model.getCatalogItem());
                save(settings);
            }

        }
    }
}
