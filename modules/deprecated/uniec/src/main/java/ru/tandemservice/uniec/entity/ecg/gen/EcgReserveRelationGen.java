package ru.tandemservice.uniec.entity.ecg.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.ecg.EcgDistribObject;
import ru.tandemservice.uniec.entity.ecg.EcgReserveRelation;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь абитуриента с распределением по конкурсной группе для находящихся в резерве
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EcgReserveRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.ecg.EcgReserveRelation";
    public static final String ENTITY_NAME = "ecgReserveRelation";
    public static final int VERSION_HASH = 631926518;
    private static IEntityMeta ENTITY_META;

    public static final String L_DISTRIBUTION = "distribution";
    public static final String L_ENTRANT = "entrant";

    private EcgDistribObject _distribution;     // Распределение
    private Entrant _entrant;     // Абитуриент

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Распределение. Свойство не может быть null.
     */
    @NotNull
    public EcgDistribObject getDistribution()
    {
        return _distribution;
    }

    /**
     * @param distribution Распределение. Свойство не может быть null.
     */
    public void setDistribution(EcgDistribObject distribution)
    {
        dirty(_distribution, distribution);
        _distribution = distribution;
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public Entrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant Абитуриент. Свойство не может быть null.
     */
    public void setEntrant(Entrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EcgReserveRelationGen)
        {
            setDistribution(((EcgReserveRelation)another).getDistribution());
            setEntrant(((EcgReserveRelation)another).getEntrant());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EcgReserveRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EcgReserveRelation.class;
        }

        public T newInstance()
        {
            return (T) new EcgReserveRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "distribution":
                    return obj.getDistribution();
                case "entrant":
                    return obj.getEntrant();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "distribution":
                    obj.setDistribution((EcgDistribObject) value);
                    return;
                case "entrant":
                    obj.setEntrant((Entrant) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "distribution":
                        return true;
                case "entrant":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "distribution":
                    return true;
                case "entrant":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "distribution":
                    return EcgDistribObject.class;
                case "entrant":
                    return Entrant.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EcgReserveRelation> _dslPath = new Path<EcgReserveRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EcgReserveRelation");
    }
            

    /**
     * @return Распределение. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.ecg.EcgReserveRelation#getDistribution()
     */
    public static EcgDistribObject.Path<EcgDistribObject> distribution()
    {
        return _dslPath.distribution();
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.ecg.EcgReserveRelation#getEntrant()
     */
    public static Entrant.Path<Entrant> entrant()
    {
        return _dslPath.entrant();
    }

    public static class Path<E extends EcgReserveRelation> extends EntityPath<E>
    {
        private EcgDistribObject.Path<EcgDistribObject> _distribution;
        private Entrant.Path<Entrant> _entrant;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Распределение. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.ecg.EcgReserveRelation#getDistribution()
     */
        public EcgDistribObject.Path<EcgDistribObject> distribution()
        {
            if(_distribution == null )
                _distribution = new EcgDistribObject.Path<EcgDistribObject>(L_DISTRIBUTION, this);
            return _distribution;
        }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.ecg.EcgReserveRelation#getEntrant()
     */
        public Entrant.Path<Entrant> entrant()
        {
            if(_entrant == null )
                _entrant = new Entrant.Path<Entrant>(L_ENTRANT, this);
            return _entrant;
        }

        public Class getEntityClass()
        {
            return EcgReserveRelation.class;
        }

        public String getEntityName()
        {
            return "ecgReserveRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
