/* $Id: IDAO.java 7713 2009-04-24 09:18:08Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.stateExamEnrollmentResults2010.StateExamEnrollmentResultsList;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author vip_delete
 * @since 20.04.2009
 */
public interface IDAO extends IUniDao<Model>
{
}
