package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.entity.entrant.Discipline2OlympiadDiplomaRelation;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.OlympiadDiploma;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Зачтение предмета олимпиады
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class Discipline2OlympiadDiplomaRelationGen extends VersionedEntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.Discipline2OlympiadDiplomaRelation";
    public static final String ENTITY_NAME = "discipline2OlympiadDiplomaRelation";
    public static final int VERSION_HASH = -1471939971;
    private static IEntityMeta ENTITY_META;

    public static final String P_VERSION = "version";
    public static final String L_ENROLLMENT_DIRECTION = "enrollmentDirection";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String L_DISCIPLINE = "discipline";
    public static final String L_DIPLOMA = "diploma";

    private int _version; 
    private EnrollmentDirection _enrollmentDirection;     // Направление подготовки (специальность) приема
    private CompensationType _compensationType;     // Вид возмещения затрат
    private Discipline2RealizationWayRelation _discipline;     // Дисциплина набора вступительных испытаний
    private OlympiadDiploma _diploma;     // Диплом участника олимпиады

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public int getVersion()
    {
        return _version;
    }

    /**
     * @param version  Свойство не может быть null.
     */
    public void setVersion(int version)
    {
        dirty(_version, version);
        _version = version;
    }

    /**
     * @return Направление подготовки (специальность) приема.
     */
    public EnrollmentDirection getEnrollmentDirection()
    {
        return _enrollmentDirection;
    }

    /**
     * @param enrollmentDirection Направление подготовки (специальность) приема.
     */
    public void setEnrollmentDirection(EnrollmentDirection enrollmentDirection)
    {
        dirty(_enrollmentDirection, enrollmentDirection);
        _enrollmentDirection = enrollmentDirection;
    }

    /**
     * @return Вид возмещения затрат.
     */
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Дисциплина набора вступительных испытаний. Свойство не может быть null.
     */
    @NotNull
    public Discipline2RealizationWayRelation getDiscipline()
    {
        return _discipline;
    }

    /**
     * @param discipline Дисциплина набора вступительных испытаний. Свойство не может быть null.
     */
    public void setDiscipline(Discipline2RealizationWayRelation discipline)
    {
        dirty(_discipline, discipline);
        _discipline = discipline;
    }

    /**
     * @return Диплом участника олимпиады. Свойство не может быть null.
     */
    @NotNull
    public OlympiadDiploma getDiploma()
    {
        return _diploma;
    }

    /**
     * @param diploma Диплом участника олимпиады. Свойство не может быть null.
     */
    public void setDiploma(OlympiadDiploma diploma)
    {
        dirty(_diploma, diploma);
        _diploma = diploma;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof Discipline2OlympiadDiplomaRelationGen)
        {
            setVersion(((Discipline2OlympiadDiplomaRelation)another).getVersion());
            setEnrollmentDirection(((Discipline2OlympiadDiplomaRelation)another).getEnrollmentDirection());
            setCompensationType(((Discipline2OlympiadDiplomaRelation)another).getCompensationType());
            setDiscipline(((Discipline2OlympiadDiplomaRelation)another).getDiscipline());
            setDiploma(((Discipline2OlympiadDiplomaRelation)another).getDiploma());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends Discipline2OlympiadDiplomaRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) Discipline2OlympiadDiplomaRelation.class;
        }

        public T newInstance()
        {
            return (T) new Discipline2OlympiadDiplomaRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "version":
                    return obj.getVersion();
                case "enrollmentDirection":
                    return obj.getEnrollmentDirection();
                case "compensationType":
                    return obj.getCompensationType();
                case "discipline":
                    return obj.getDiscipline();
                case "diploma":
                    return obj.getDiploma();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "version":
                    obj.setVersion((Integer) value);
                    return;
                case "enrollmentDirection":
                    obj.setEnrollmentDirection((EnrollmentDirection) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "discipline":
                    obj.setDiscipline((Discipline2RealizationWayRelation) value);
                    return;
                case "diploma":
                    obj.setDiploma((OlympiadDiploma) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "version":
                        return true;
                case "enrollmentDirection":
                        return true;
                case "compensationType":
                        return true;
                case "discipline":
                        return true;
                case "diploma":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "version":
                    return true;
                case "enrollmentDirection":
                    return true;
                case "compensationType":
                    return true;
                case "discipline":
                    return true;
                case "diploma":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "version":
                    return Integer.class;
                case "enrollmentDirection":
                    return EnrollmentDirection.class;
                case "compensationType":
                    return CompensationType.class;
                case "discipline":
                    return Discipline2RealizationWayRelation.class;
                case "diploma":
                    return OlympiadDiploma.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<Discipline2OlympiadDiplomaRelation> _dslPath = new Path<Discipline2OlympiadDiplomaRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "Discipline2OlympiadDiplomaRelation");
    }
            

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.Discipline2OlympiadDiplomaRelation#getVersion()
     */
    public static PropertyPath<Integer> version()
    {
        return _dslPath.version();
    }

    /**
     * @return Направление подготовки (специальность) приема.
     * @see ru.tandemservice.uniec.entity.entrant.Discipline2OlympiadDiplomaRelation#getEnrollmentDirection()
     */
    public static EnrollmentDirection.Path<EnrollmentDirection> enrollmentDirection()
    {
        return _dslPath.enrollmentDirection();
    }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.uniec.entity.entrant.Discipline2OlympiadDiplomaRelation#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Дисциплина набора вступительных испытаний. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.Discipline2OlympiadDiplomaRelation#getDiscipline()
     */
    public static Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation> discipline()
    {
        return _dslPath.discipline();
    }

    /**
     * @return Диплом участника олимпиады. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.Discipline2OlympiadDiplomaRelation#getDiploma()
     */
    public static OlympiadDiploma.Path<OlympiadDiploma> diploma()
    {
        return _dslPath.diploma();
    }

    public static class Path<E extends Discipline2OlympiadDiplomaRelation> extends EntityPath<E>
    {
        private PropertyPath<Integer> _version;
        private EnrollmentDirection.Path<EnrollmentDirection> _enrollmentDirection;
        private CompensationType.Path<CompensationType> _compensationType;
        private Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation> _discipline;
        private OlympiadDiploma.Path<OlympiadDiploma> _diploma;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.Discipline2OlympiadDiplomaRelation#getVersion()
     */
        public PropertyPath<Integer> version()
        {
            if(_version == null )
                _version = new PropertyPath<Integer>(Discipline2OlympiadDiplomaRelationGen.P_VERSION, this);
            return _version;
        }

    /**
     * @return Направление подготовки (специальность) приема.
     * @see ru.tandemservice.uniec.entity.entrant.Discipline2OlympiadDiplomaRelation#getEnrollmentDirection()
     */
        public EnrollmentDirection.Path<EnrollmentDirection> enrollmentDirection()
        {
            if(_enrollmentDirection == null )
                _enrollmentDirection = new EnrollmentDirection.Path<EnrollmentDirection>(L_ENROLLMENT_DIRECTION, this);
            return _enrollmentDirection;
        }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.uniec.entity.entrant.Discipline2OlympiadDiplomaRelation#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Дисциплина набора вступительных испытаний. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.Discipline2OlympiadDiplomaRelation#getDiscipline()
     */
        public Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation> discipline()
        {
            if(_discipline == null )
                _discipline = new Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation>(L_DISCIPLINE, this);
            return _discipline;
        }

    /**
     * @return Диплом участника олимпиады. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.Discipline2OlympiadDiplomaRelation#getDiploma()
     */
        public OlympiadDiploma.Path<OlympiadDiploma> diploma()
        {
            if(_diploma == null )
                _diploma = new OlympiadDiploma.Path<OlympiadDiploma>(L_DIPLOMA, this);
            return _diploma;
        }

        public Class getEntityClass()
        {
            return Discipline2OlympiadDiplomaRelation.class;
        }

        public String getEntityName()
        {
            return "discipline2OlympiadDiplomaRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
