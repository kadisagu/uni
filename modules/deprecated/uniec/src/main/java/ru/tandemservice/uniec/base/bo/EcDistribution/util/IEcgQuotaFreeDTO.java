/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.util;

import java.util.Map;

/**
 * Свободных мест по направлениям приема и видам цп
 *
 * @author Vasily Zhukov
 * @since 13.07.2011
 */
public interface IEcgQuotaFreeDTO
{
    /**
     * @return планы приема по направлениям приема и видам цп
     */
    IEcgQuotaDTO getQuotaDTO();

    /**
     * @return занятые места по направлениям приема и видам цп
     */
    IEcgQuotaUsedDTO getQuotaUsedDTO();

    /**
     * @return направление приема -> свободно мест (всегда не null, может быть отрицательно)
     */
    Map<Long, Integer> getFreeMap();

    /**
     * @return направление приема -> вид цп -> свободно мест (всегда не null, может быть отрицательно)
     */
    Map<Long, Map<Long, Integer>> getTaFreeMap();
}
