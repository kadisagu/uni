package ru.tandemservice.uniec.entity.report;

import ru.tandemservice.uniec.component.settings.ProtocolVisaList.IProtocolEntity;
import ru.tandemservice.uniec.entity.report.gen.OrgUnitExecutiveSecretaryGen;

/**
 * Ответственный секретарь
 */
public class OrgUnitExecutiveSecretary extends OrgUnitExecutiveSecretaryGen implements IProtocolEntity
{
}