/* $Id: Model.java 10428 2009-10-29 14:08:07Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.stateExamEnrollmentResults2010.StateExamEnrollmentResultsPub;

import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;

import ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010;

/**
 * @author vip_delete
 * @since 20.04.2009
 */
@State(keys = PublisherActivator.PUBLISHER_ID_KEY, bindings = "report.id")
public class Model
{
    private StateExamEnrollmentReport2010 _report = new StateExamEnrollmentReport2010();

    public StateExamEnrollmentReport2010 getReport()
    {
        return _report;
    }

    public void setReport(StateExamEnrollmentReport2010 report)
    {
        _report = report;
    }
}
