package ru.tandemservice.uniec.dao;

import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.uniec.entity.orders.*;

/**
 * @author Alexey Lopatin
 * @since 14.11.2013
 */
public interface IGeneratedCommentForEntrantOrders
{
    final String BEAN_NAME = "generatedCommentForEntrantOrders";
    final SpringBeanCache<IGeneratedCommentForEntrantOrders> instance = new SpringBeanCache<>(BEAN_NAME);

    // 1. О зачислении в число студентов class
    String getComment(EntrStudentBugetExtract extrac, boolean isPrintOrdert);

    // 2. О зачислении в число студентов (по договору)
    String getComment(EntrStudentContractExtract extract, boolean isPrintOrder);

    // 3. О зачислении в число магистрантов (бюджет)
    String getComment(EntrMasterBugetExtract extract, boolean isPrintOrder);

    // 4. О зачислении в число магистрантов (по договору)
    String getComment(EntrMasterContractExtract extract, boolean isPrintOrder);

    // 5. О зачислении в число слушателей (по договору)
    String getComment(EntrListenerContractExtract extract, boolean isPrintOrder);

    // 6. О зачислении по целевому набору (бюджет)
    String getComment(EntrTargetBudgetExtract extract, boolean isPrintOrder);

    // 7. О зачислении по целевому набору (по договору)
    String getComment(EntrTargetContractExtract extract, boolean isPrintOrder);

    // 8. О зачислении в число студентов (бюджет, сокр. прогр.)
    String getComment(EntrStudentShBugetExtract extract, boolean isPrintOrder);

    // 9. О зачислении в число студентов (по договору, сокр. прогр.)
    String getComment(EntrStudentShContractExtract extract, boolean isPrintOrder);

    // 10. О зачислении на второе высшее
    String getComment(EntrHighShortExtract extract, boolean isPrintOrder);

    // 11. О зачислении в число слушателей (паралл. освоение)
    String getComment(EntrListenerParExtract extract, boolean isPrintOrder);

    // 12. О зачислении в число студентов (по договору, после ОПП)
    String getComment(EntrStudentOPPExtract extract, boolean isPrintOrder);

    // 13. О зачислении в число студентов
    String getComment(EntrStudentExtract extract, boolean isPrintOrder);

    // 14. О зачислении в число студентов по целевому приему
    String getComment(EntrTargetExtract extract, boolean isPrintOrder);
}
