package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.catalog.EnrollmentDocument;
import ru.tandemservice.uniec.entity.entrant.EntrantEnrollmentDocument;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Документ абитуриента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntrantEnrollmentDocumentGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.EntrantEnrollmentDocument";
    public static final String ENTITY_NAME = "entrantEnrollmentDocument";
    public static final int VERSION_HASH = -761896815;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT_REQUEST = "entrantRequest";
    public static final String L_ENROLLMENT_DOCUMENT = "enrollmentDocument";
    public static final String P_AMOUNT = "amount";
    public static final String P_COPY = "copy";

    private EntrantRequest _entrantRequest;     // Заявление абитуриента
    private EnrollmentDocument _enrollmentDocument;     // Документы для подачи в ОУ
    private int _amount;     // Количество
    private boolean _copy;     // Копия

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Заявление абитуриента. Свойство не может быть null.
     */
    @NotNull
    public EntrantRequest getEntrantRequest()
    {
        return _entrantRequest;
    }

    /**
     * @param entrantRequest Заявление абитуриента. Свойство не может быть null.
     */
    public void setEntrantRequest(EntrantRequest entrantRequest)
    {
        dirty(_entrantRequest, entrantRequest);
        _entrantRequest = entrantRequest;
    }

    /**
     * @return Документы для подачи в ОУ. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentDocument getEnrollmentDocument()
    {
        return _enrollmentDocument;
    }

    /**
     * @param enrollmentDocument Документы для подачи в ОУ. Свойство не может быть null.
     */
    public void setEnrollmentDocument(EnrollmentDocument enrollmentDocument)
    {
        dirty(_enrollmentDocument, enrollmentDocument);
        _enrollmentDocument = enrollmentDocument;
    }

    /**
     * @return Количество. Свойство не может быть null.
     */
    @NotNull
    public int getAmount()
    {
        return _amount;
    }

    /**
     * @param amount Количество. Свойство не может быть null.
     */
    public void setAmount(int amount)
    {
        dirty(_amount, amount);
        _amount = amount;
    }

    /**
     * @return Копия. Свойство не может быть null.
     */
    @NotNull
    public boolean isCopy()
    {
        return _copy;
    }

    /**
     * @param copy Копия. Свойство не может быть null.
     */
    public void setCopy(boolean copy)
    {
        dirty(_copy, copy);
        _copy = copy;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntrantEnrollmentDocumentGen)
        {
            setEntrantRequest(((EntrantEnrollmentDocument)another).getEntrantRequest());
            setEnrollmentDocument(((EntrantEnrollmentDocument)another).getEnrollmentDocument());
            setAmount(((EntrantEnrollmentDocument)another).getAmount());
            setCopy(((EntrantEnrollmentDocument)another).isCopy());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntrantEnrollmentDocumentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntrantEnrollmentDocument.class;
        }

        public T newInstance()
        {
            return (T) new EntrantEnrollmentDocument();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entrantRequest":
                    return obj.getEntrantRequest();
                case "enrollmentDocument":
                    return obj.getEnrollmentDocument();
                case "amount":
                    return obj.getAmount();
                case "copy":
                    return obj.isCopy();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entrantRequest":
                    obj.setEntrantRequest((EntrantRequest) value);
                    return;
                case "enrollmentDocument":
                    obj.setEnrollmentDocument((EnrollmentDocument) value);
                    return;
                case "amount":
                    obj.setAmount((Integer) value);
                    return;
                case "copy":
                    obj.setCopy((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entrantRequest":
                        return true;
                case "enrollmentDocument":
                        return true;
                case "amount":
                        return true;
                case "copy":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entrantRequest":
                    return true;
                case "enrollmentDocument":
                    return true;
                case "amount":
                    return true;
                case "copy":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entrantRequest":
                    return EntrantRequest.class;
                case "enrollmentDocument":
                    return EnrollmentDocument.class;
                case "amount":
                    return Integer.class;
                case "copy":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntrantEnrollmentDocument> _dslPath = new Path<EntrantEnrollmentDocument>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntrantEnrollmentDocument");
    }
            

    /**
     * @return Заявление абитуриента. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantEnrollmentDocument#getEntrantRequest()
     */
    public static EntrantRequest.Path<EntrantRequest> entrantRequest()
    {
        return _dslPath.entrantRequest();
    }

    /**
     * @return Документы для подачи в ОУ. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantEnrollmentDocument#getEnrollmentDocument()
     */
    public static EnrollmentDocument.Path<EnrollmentDocument> enrollmentDocument()
    {
        return _dslPath.enrollmentDocument();
    }

    /**
     * @return Количество. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantEnrollmentDocument#getAmount()
     */
    public static PropertyPath<Integer> amount()
    {
        return _dslPath.amount();
    }

    /**
     * @return Копия. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantEnrollmentDocument#isCopy()
     */
    public static PropertyPath<Boolean> copy()
    {
        return _dslPath.copy();
    }

    public static class Path<E extends EntrantEnrollmentDocument> extends EntityPath<E>
    {
        private EntrantRequest.Path<EntrantRequest> _entrantRequest;
        private EnrollmentDocument.Path<EnrollmentDocument> _enrollmentDocument;
        private PropertyPath<Integer> _amount;
        private PropertyPath<Boolean> _copy;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Заявление абитуриента. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantEnrollmentDocument#getEntrantRequest()
     */
        public EntrantRequest.Path<EntrantRequest> entrantRequest()
        {
            if(_entrantRequest == null )
                _entrantRequest = new EntrantRequest.Path<EntrantRequest>(L_ENTRANT_REQUEST, this);
            return _entrantRequest;
        }

    /**
     * @return Документы для подачи в ОУ. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantEnrollmentDocument#getEnrollmentDocument()
     */
        public EnrollmentDocument.Path<EnrollmentDocument> enrollmentDocument()
        {
            if(_enrollmentDocument == null )
                _enrollmentDocument = new EnrollmentDocument.Path<EnrollmentDocument>(L_ENROLLMENT_DOCUMENT, this);
            return _enrollmentDocument;
        }

    /**
     * @return Количество. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantEnrollmentDocument#getAmount()
     */
        public PropertyPath<Integer> amount()
        {
            if(_amount == null )
                _amount = new PropertyPath<Integer>(EntrantEnrollmentDocumentGen.P_AMOUNT, this);
            return _amount;
        }

    /**
     * @return Копия. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantEnrollmentDocument#isCopy()
     */
        public PropertyPath<Boolean> copy()
        {
            if(_copy == null )
                _copy = new PropertyPath<Boolean>(EntrantEnrollmentDocumentGen.P_COPY, this);
            return _copy;
        }

        public Class getEntityClass()
        {
            return EntrantEnrollmentDocument.class;
        }

        public String getEntityName()
        {
            return "entrantEnrollmentDocument";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
