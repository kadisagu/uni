/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcOrder.logic;

import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;

/**
 * @author Vasily Zhukov
 * @since 25.05.2011
 */
public interface IEnrollmentExtractFactory
{
    EnrollmentExtract createEnrollmentExtract(PreliminaryEnrollmentStudent preStudent);
}
