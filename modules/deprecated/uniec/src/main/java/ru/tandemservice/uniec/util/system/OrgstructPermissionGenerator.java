/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.util.system;

/**
 * @author agolubenko
 * @since 27.07.2009
 */
public class OrgstructPermissionGenerator
{
    public static void main(String[] args)
    {
        //    System.out.println("uniec.orgstruct.sec.xml");
        //    System.out.println("\n\n\n");
        //
        //    for (OrgUnitDefines.IOrgUnitDescription description : OrgUnitDefines.getCode2description().values())
        //    {
        //        writePermissions(description.getCode());
        //        writeRelations(description.getCode());
        //        System.out.println();
        //    }
        //}
        //
        //private static void writePermissions(String code)
        //{
        //    System.out.println(
        //            "    <permission-group name=\"" + code + "EntrantsTabPermissionGroup\" title=\"Вкладка «Абитуриенты»\">\n" +
        //            "        <permission name=\"orgUnit_viewEntrantTab_" + code + "\" title=\"Просмотр\"/>\n" +
        //            "        <permission name=\"orgUnit_printEntantList_" + code + "\" title=\"Печать списка абитуриентов\"/>\n" +
        //            "    </permission-group>");
        //}
        //
        //private static void writeRelations(String code)
        //{
        //    System.out.println("    <group-relation permission-group-name=\"" + code + "EntrantsTabPermissionGroup\" group-name=\"" + code + "PermissionClassGroup\"/>");
        //    System.out.println("    <group-relation permission-group-name=\"" + code + "EntrantsTabPermissionGroup\" group-name=\"" + code + "LocalClassGroup\"/>");
    }
}
