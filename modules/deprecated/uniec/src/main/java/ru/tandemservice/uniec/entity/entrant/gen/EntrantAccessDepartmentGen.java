package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.catalog.AccessDepartment;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantAccessDepartment;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Подготовительное отделение абитуриента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntrantAccessDepartmentGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.EntrantAccessDepartment";
    public static final String ENTITY_NAME = "entrantAccessDepartment";
    public static final int VERSION_HASH = 189375835;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT = "entrant";
    public static final String L_ACCESS_DEPARTMENT = "accessDepartment";

    private Entrant _entrant;     // (Старый) Абитуриент
    private AccessDepartment _accessDepartment;     // Подготовительное отделение

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return (Старый) Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public Entrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant (Старый) Абитуриент. Свойство не может быть null.
     */
    public void setEntrant(Entrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Подготовительное отделение. Свойство не может быть null.
     */
    @NotNull
    public AccessDepartment getAccessDepartment()
    {
        return _accessDepartment;
    }

    /**
     * @param accessDepartment Подготовительное отделение. Свойство не может быть null.
     */
    public void setAccessDepartment(AccessDepartment accessDepartment)
    {
        dirty(_accessDepartment, accessDepartment);
        _accessDepartment = accessDepartment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntrantAccessDepartmentGen)
        {
            setEntrant(((EntrantAccessDepartment)another).getEntrant());
            setAccessDepartment(((EntrantAccessDepartment)another).getAccessDepartment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntrantAccessDepartmentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntrantAccessDepartment.class;
        }

        public T newInstance()
        {
            return (T) new EntrantAccessDepartment();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entrant":
                    return obj.getEntrant();
                case "accessDepartment":
                    return obj.getAccessDepartment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entrant":
                    obj.setEntrant((Entrant) value);
                    return;
                case "accessDepartment":
                    obj.setAccessDepartment((AccessDepartment) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entrant":
                        return true;
                case "accessDepartment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entrant":
                    return true;
                case "accessDepartment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entrant":
                    return Entrant.class;
                case "accessDepartment":
                    return AccessDepartment.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntrantAccessDepartment> _dslPath = new Path<EntrantAccessDepartment>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntrantAccessDepartment");
    }
            

    /**
     * @return (Старый) Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantAccessDepartment#getEntrant()
     */
    public static Entrant.Path<Entrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Подготовительное отделение. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantAccessDepartment#getAccessDepartment()
     */
    public static AccessDepartment.Path<AccessDepartment> accessDepartment()
    {
        return _dslPath.accessDepartment();
    }

    public static class Path<E extends EntrantAccessDepartment> extends EntityPath<E>
    {
        private Entrant.Path<Entrant> _entrant;
        private AccessDepartment.Path<AccessDepartment> _accessDepartment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return (Старый) Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantAccessDepartment#getEntrant()
     */
        public Entrant.Path<Entrant> entrant()
        {
            if(_entrant == null )
                _entrant = new Entrant.Path<Entrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Подготовительное отделение. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantAccessDepartment#getAccessDepartment()
     */
        public AccessDepartment.Path<AccessDepartment> accessDepartment()
        {
            if(_accessDepartment == null )
                _accessDepartment = new AccessDepartment.Path<AccessDepartment>(L_ACCESS_DEPARTMENT, this);
            return _accessDepartment;
        }

        public Class getEntityClass()
        {
            return EntrantAccessDepartment.class;
        }

        public String getEntityName()
        {
            return "entrantAccessDepartment";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
