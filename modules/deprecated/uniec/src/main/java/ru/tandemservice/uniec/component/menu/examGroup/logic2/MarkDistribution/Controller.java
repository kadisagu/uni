/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.menu.examGroup.logic2.MarkDistribution;

import org.tandemframework.core.component.IBusinessComponent;

import ru.tandemservice.uniec.component.menu.examGroup.abstractMarkDistribution.MarkDistributionController;

/**
 * @author Vasily Zhukov
 * @since 31.05.2010
 */
public class Controller extends MarkDistributionController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getModel(component).setPrincipalContext(component.getUserContext().getPrincipalContext());
        super.onRefreshComponent(component);
    }
}