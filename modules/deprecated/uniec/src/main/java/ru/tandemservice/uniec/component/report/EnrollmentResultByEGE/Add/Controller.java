/* $Id$ */
package ru.tandemservice.uniec.component.report.EnrollmentResultByEGE.Add;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.UserContext;

/**
 * @author Vasily Zhukov
 * @since 18.08.2011
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.setPrincipalContext(UserContext.getInstance().getPrincipalContext());

        getDao().prepare(model);
    }

    public void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().update(model);

        deactivate(component);

        activateInRoot(component, new PublisherActivator(model.getReport()));
    }
}
