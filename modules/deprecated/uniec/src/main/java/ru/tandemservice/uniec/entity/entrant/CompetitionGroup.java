package ru.tandemservice.uniec.entity.entrant;

import java.util.Comparator;

import org.tandemframework.core.common.ITitled;

import ru.tandemservice.uniec.entity.entrant.gen.CompetitionGroupGen;

/**
 * Конкурсная группа
 */
public class CompetitionGroup extends CompetitionGroupGen implements ITitled
{
    public static Comparator<CompetitionGroup> TITLED_COMPARATOR = new Comparator<CompetitionGroup>()
    {
        @Override
        public int compare(CompetitionGroup o1, CompetitionGroup o2)
        {
            String title1 = o1.getTitle();
            String title2 = o2.getTitle();
            int lengthDiff = title1.length() - title2.length();
            if (lengthDiff > 0)
            {
                return 1;
            }
            else if (lengthDiff < 0)
            {
                return -1;
            }
            else
            {
                return title1.compareTo(title2);
            }
        }
    };
}