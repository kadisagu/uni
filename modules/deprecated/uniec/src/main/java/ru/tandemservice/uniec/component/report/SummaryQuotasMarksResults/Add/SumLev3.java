/* $Id$ */
package ru.tandemservice.uniec.component.report.SummaryQuotasMarksResults.Add;

import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.util.EntrantDataUtil;

/**
 * @author Vasily Zhukov
 * @since 19.08.2011
 */
class SumLev3 extends SumLevBase
{
    SumLev3(EntrantDataUtil dataUtil, CompensationType compensationType, EducationLevels educationLevel, EnrollmentDirection enrollmentDirection)
    {
        super(enrollmentDirection.getId(), dataUtil, educationLevel.getDisplayableTitle(), enrollmentDirection.getPrintTitle());

        setBudgetPlan(enrollmentDirection.getMinisterialPlan() == null ? 0 : enrollmentDirection.getMinisterialPlan());

        int targetBudgetPlan = enrollmentDirection.getTargetAdmissionPlanBudget() == null ? 0 : enrollmentDirection.getTargetAdmissionPlanBudget();
        int targetContractPlan = enrollmentDirection.getTargetAdmissionPlanContract() == null ? 0 : enrollmentDirection.getTargetAdmissionPlanContract();

        setTargetPlan(compensationType == null ? targetBudgetPlan + targetContractPlan : (compensationType.isBudget() ? targetBudgetPlan : targetContractPlan));

        setContractPlan(enrollmentDirection.getContractPlan() == null ? 0 : enrollmentDirection.getContractPlan());
    }

    @Override
    public int getLevel()
    {
        return 2;
    }
}
