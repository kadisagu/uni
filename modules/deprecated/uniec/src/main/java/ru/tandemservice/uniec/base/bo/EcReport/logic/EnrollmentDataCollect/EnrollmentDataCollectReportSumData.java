/**
 *$Id$
 */
package ru.tandemservice.uniec.base.bo.EcReport.logic.EnrollmentDataCollect;

import org.apache.commons.lang.mutable.MutableDouble;
import ru.tandemservice.uni.report.summaryReports.SummaryDataWrapper;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Alexander Shaburov
 * @since 15.08.13
 */
public class EnrollmentDataCollectReportSumData extends SummaryDataWrapper<PreliminaryEnrollmentStudent>
{
    private Map<String, MutableDouble> _selfValueMap = new HashMap<>();

    // Accessors

    public MutableDouble getValue(String valueName)
    {
        MutableDouble mutableInt = _selfValueMap.get(valueName);
        if (mutableInt == null)
            _selfValueMap.put(valueName, mutableInt = new MutableDouble(0d));

        return mutableInt;
    }
}
