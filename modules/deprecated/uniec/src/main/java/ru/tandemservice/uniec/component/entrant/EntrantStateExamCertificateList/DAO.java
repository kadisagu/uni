/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantStateExamCertificateList;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.dao.IStateExamCertificateDAO;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate;
import ru.tandemservice.uniec.entity.entrant.StateExamSubjectMark;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.Calendar;
import java.util.Date;

/**
 * @author agolubenko
 * @since 06.03.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("certificate");

    static
    {
        _orderSettings.setOrders(Entrant.P_FULLFIO, new OrderDescription("identityCard", IdentityCard.P_LAST_NAME), new OrderDescription("identityCard", IdentityCard.P_FIRST_NAME), new OrderDescription("identityCard", IdentityCard.P_MIDDLE_NAME));
    }

    private IStateExamCertificateDAO _certificateDAO;

    @Override
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilterWithSecurity(model, getSession());
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        DynamicListDataSource<EntrantStateExamCertificate> dataSource = model.getDataSource();

        MQBuilder builder = new MQBuilder(EntrantStateExamCertificate.ENTITY_CLASS, "certificate");
        builder.addJoin("certificate", EntrantStateExamCertificate.L_ENTRANT, "entrant");
        builder.addJoin("entrant", Entrant.L_PERSON + "." + Person.L_IDENTITY_CARD, "identityCard");

        applyFilters(builder, model.getSettings());
        _orderSettings.applyOrder(builder, dataSource.getEntityOrder());
        UniBaseUtils.createPage(dataSource, builder, getSession());

        model.setCertificate2SubjectsMarks(_certificateDAO.prepareCertificatesSubjectsMarks(model.getDataSource().getEntityList()));
    }

    @SuppressWarnings("unchecked")
    private void applyFilters(MQBuilder builder, IDataSettings settings)
    {
        EnrollmentCampaign enrollmentCampaign = (EnrollmentCampaign) settings.get("enrollmentCampaign");
        String entrantLastName = (String) settings.get("entrantLastName");
        String identityCardSeria = (String) settings.get("identityCardSeria");
        String identityCardNumber = (String) settings.get("identityCardNumber");
        String certificateNumber = createNumberMask(settings.get("certificateTerritoryCode"), settings.get("certificateNumber"), settings.get("certificateYear"));
        Date registeredFrom = (Date) settings.get("registeredFrom");
        Date registeredTo = (Date) settings.get("registeredTo");
        registeredTo = (registeredTo != null) ? CoreDateUtils.add(registeredTo, Calendar.DATE, 1) : null;
        IdentifiableWrapper<IEntity> accepted = (IdentifiableWrapper<IEntity>) settings.get("certificateAccepted");
        IdentifiableWrapper<IEntity> sent = (IdentifiableWrapper<IEntity>) settings.get("certificateSent");

        builder.add(MQExpression.eq("entrant", Entrant.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign));
        if (entrantLastName != null)
            builder.add(MQExpression.like("identityCard", IdentityCard.P_LAST_NAME, "%" + entrantLastName));
        if (identityCardSeria != null)
            builder.add(MQExpression.eq("identityCard", IdentityCard.P_SERIA, identityCardSeria));
        if (identityCardNumber != null)
            builder.add(MQExpression.eq("identityCard", IdentityCard.P_NUMBER, identityCardNumber));
        if (certificateNumber != null)
        {
            MQBuilder markBuilder = new MQBuilder(StateExamSubjectMark.ENTITY_CLASS, "mark")
                    .add(MQExpression.like("mark", StateExamSubjectMark.certificateNumber(), certificateNumber))
                    .add(MQExpression.eqProperty("mark", StateExamSubjectMark.certificate().id(), "certificate", EntrantStateExamCertificate.id()));
            builder.add(MQExpression.or(
                    MQExpression.like("certificate", EntrantStateExamCertificate.P_NUMBER, certificateNumber),
                    MQExpression.exists(markBuilder)
            ));
        }
        if (registeredFrom != null && registeredTo != null)
        {
            builder.add(MQExpression.between("certificate", EntrantStateExamCertificate.P_REGISTRATION_DATE, registeredFrom, registeredTo));
        } else
        {
            if (registeredFrom != null)
            {
                builder.add(MQExpression.greatOrEq("certificate", EntrantStateExamCertificate.P_REGISTRATION_DATE, registeredFrom));
            }
            if (registeredTo != null)
            {
                builder.add(MQExpression.less("certificate", EntrantStateExamCertificate.P_REGISTRATION_DATE, registeredTo));
            }
        }

        if (accepted != null)
            builder.add(MQExpression.eq("certificate", EntrantStateExamCertificate.P_ACCEPTED, accepted.getId().equals(Model.STATUS_ACCEPTED_ID)));

        if (sent != null)
            builder.add(MQExpression.eq("certificate", EntrantStateExamCertificate.P_SENT, sent.getId().equals(Model.STATUS_SENT_ID)));
    }

    /**
     * @param territoryCode код территории
     * @param number        номер
     * @param year          год
     * @return маска для поиска свидетельства по номеру
     */
    private String createNumberMask(String territoryCode, String number, String year)
    {
        if (territoryCode == null && number == null && year == null)
            return null;

        String result = "";
        result += (territoryCode != null) ? territoryCode : "__"; // 2 символа
        result += (number != null) ? number : "_________"; // 9 символов
        result += (year != null) ? year : "__"; // 2 символа
        return result;
    }

    public void setCertificateDAO(IStateExamCertificateDAO certificateDAO)
    {
        _certificateDAO = certificateDAO;
    }
}
