/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcOrder.ui.RevertAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager;
import ru.tandemservice.uniec.base.bo.EcOrder.ui.RevertPub.EcOrderRevertPub;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.EnrollmentOrderBasic;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;

import java.util.Date;

/**
 * @author Nikolay Fedorovskih
 * @since 06.08.2013
 */
@Input({
               @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "orderId"),
       })
public class EcOrderRevertAddEditUI extends UIPresenter
{
    private Long orderId;
    private EnrollmentOrder order;
    private boolean isAddForm;
    private EnrollmentOrderBasic orderBasic;
    private EmployeePost executor;

    @Override
    public void onComponentRefresh()
    {
        isAddForm = getOrderId() == null;
        if (isAddForm)
        {
            order = new EnrollmentOrder();
            order.setEnrollmentCampaign(UniecDAOFacade.getEntrantDAO().getLastEnrollmentCampaign());
            order.setCreateDate(new Date());
            order.setType(DataAccessServices.dao().getByCode(EntrantEnrollmentOrderType.class, EntrantEnrollmentOrderType.REVERT_ORDER_CODE));

            IPrincipalContext principalContext = UserContext.getInstance().getPrincipalContext();
            if (principalContext instanceof EmployeePost)
                executor = (EmployeePost) principalContext;
        }
        else
        {
            order = DataAccessServices.dao().getNotNull(EnrollmentOrder.class, getOrderId());
            if (!order.isRevert())
                throw new RuntimeException("Invalid order type.");
            orderBasic = order.getBasic();
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (EcOrderRevertAddEdit.ORDER_BASICS_DS.equals(dataSource.getName()))
        {
            dataSource.put(EcOrderRevertAddEdit.ORDER_REASON_PARAM, order.getReason());
        }
    }

    // Listeners

    public void onClickSave()
    {
        EcOrderManager.instance().dao().saveOrUpdateOrder(order, executor, orderBasic);

        deactivate();

        _uiActivation.asDesktopRoot(EcOrderRevertPub.class)
                .parameter(UIPresenter.PUBLISHER_ID, order.getId())
                .activate();
    }

    // Getters & Setters

    public String getPermissionKey()
    {
        return isAddForm ? "addEnrollmentRevertOrder" : "edit_enrollmentOrder";
    }

    public String getFormTitle()
    {
        return (isAddForm ? "Добавление" : "Редактирование") + " приказа «Об изменении приказа о зачислении»";
    }

    public Long getOrderId()
    {
        return orderId;
    }

    public void setOrderId(Long orderId)
    {
        this.orderId = orderId;
    }

    public EnrollmentOrder getOrder()
    {
        return order;
    }

    public EmployeePost getExecutor()
    {
        return executor;
    }

    public void setExecutor(EmployeePost executor)
    {
        this.executor = executor;
    }

    public EnrollmentOrderBasic getOrderBasic()
    {
        return orderBasic;
    }

    public void setOrderBasic(EnrollmentOrderBasic orderBasic)
    {
        this.orderBasic = orderBasic;
    }
}