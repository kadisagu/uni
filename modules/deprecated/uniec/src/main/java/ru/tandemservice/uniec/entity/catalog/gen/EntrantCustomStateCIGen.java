package ru.tandemservice.uniec.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Дополнительный статус абитуриента(Справочник)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntrantCustomStateCIGen extends EntityBase
 implements INaturalIdentifiable<EntrantCustomStateCIGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI";
    public static final String ENTITY_NAME = "entrantCustomStateCI";
    public static final int VERSION_HASH = 1479210921;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_DESCRIPTION = "description";
    public static final String P_HTML_COLOR = "htmlColor";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _shortTitle;     // Сокращенное название
    private String _description;     // Описание
    private String _htmlColor;     // Цвет(AABBCC)
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название. Свойство не может быть null.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Описание.
     */
    public String getDescription()
    {
        return _description;
    }

    /**
     * @param description Описание.
     */
    public void setDescription(String description)
    {
        dirty(_description, description);
        _description = description;
    }

    /**
     * @return Цвет(AABBCC).
     */
    @Length(max=255)
    public String getHtmlColor()
    {
        return _htmlColor;
    }

    /**
     * @param htmlColor Цвет(AABBCC).
     */
    public void setHtmlColor(String htmlColor)
    {
        dirty(_htmlColor, htmlColor);
        _htmlColor = htmlColor;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntrantCustomStateCIGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EntrantCustomStateCI)another).getCode());
            }
            setShortTitle(((EntrantCustomStateCI)another).getShortTitle());
            setDescription(((EntrantCustomStateCI)another).getDescription());
            setHtmlColor(((EntrantCustomStateCI)another).getHtmlColor());
            setTitle(((EntrantCustomStateCI)another).getTitle());
        }
    }

    public INaturalId<EntrantCustomStateCIGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EntrantCustomStateCIGen>
    {
        private static final String PROXY_NAME = "EntrantCustomStateCINaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EntrantCustomStateCIGen.NaturalId) ) return false;

            EntrantCustomStateCIGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntrantCustomStateCIGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntrantCustomStateCI.class;
        }

        public T newInstance()
        {
            return (T) new EntrantCustomStateCI();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "shortTitle":
                    return obj.getShortTitle();
                case "description":
                    return obj.getDescription();
                case "htmlColor":
                    return obj.getHtmlColor();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "description":
                    obj.setDescription((String) value);
                    return;
                case "htmlColor":
                    obj.setHtmlColor((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "shortTitle":
                        return true;
                case "description":
                        return true;
                case "htmlColor":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "shortTitle":
                    return true;
                case "description":
                    return true;
                case "htmlColor":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "shortTitle":
                    return String.class;
                case "description":
                    return String.class;
                case "htmlColor":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntrantCustomStateCI> _dslPath = new Path<EntrantCustomStateCI>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntrantCustomStateCI");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Описание.
     * @see ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI#getDescription()
     */
    public static PropertyPath<String> description()
    {
        return _dslPath.description();
    }

    /**
     * @return Цвет(AABBCC).
     * @see ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI#getHtmlColor()
     */
    public static PropertyPath<String> htmlColor()
    {
        return _dslPath.htmlColor();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EntrantCustomStateCI> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<String> _description;
        private PropertyPath<String> _htmlColor;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EntrantCustomStateCIGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(EntrantCustomStateCIGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Описание.
     * @see ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI#getDescription()
     */
        public PropertyPath<String> description()
        {
            if(_description == null )
                _description = new PropertyPath<String>(EntrantCustomStateCIGen.P_DESCRIPTION, this);
            return _description;
        }

    /**
     * @return Цвет(AABBCC).
     * @see ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI#getHtmlColor()
     */
        public PropertyPath<String> htmlColor()
        {
            if(_htmlColor == null )
                _htmlColor = new PropertyPath<String>(EntrantCustomStateCIGen.P_HTML_COLOR, this);
            return _htmlColor;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EntrantCustomStateCIGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EntrantCustomStateCI.class;
        }

        public String getEntityName()
        {
            return "entrantCustomStateCI";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
