/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.wizard.PersonalDataStep;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.person.base.entity.*;
import org.tandemframework.shared.person.catalog.entity.*;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import org.tandemframework.shared.person.catalog.entity.DormitoryBenefit;
import org.tandemframework.shared.person.base.entity.PersonDormitoryBenefit;
import org.tandemframework.shared.person.base.entity.PersonMilitaryStatus;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;

/**
 * @author vip_delete
 * @since 15.06.2009
 */
@Input({
        @Bind(key = "entrantId", binding = "entrant.id"),
        @Bind(key = "onlineEntrantId", binding = "onlineEntrant.id"),
        @Bind(key = "entrantMasterPermKey", binding = "entrantMasterPermKey")
})
public class Model
{
    private Entrant _entrant = new Entrant();
    private OnlineEntrant _onlineEntrant = new OnlineEntrant();
    
    // льготы
    private boolean _hasBenefit;
    private PersonBenefit _personBenefit = new PersonBenefit();
    private List<Benefit> _benefitList;   //select
    private List<PersonBenefit> _selectedBenefitList = new ArrayList<PersonBenefit>();
    private DynamicListDataSource<PersonBenefit> _benefitDataSource; //search list

    // иностранные языки
    private boolean _hasLanguage;
    private PersonForeignLanguage _foreignLanguage = new PersonForeignLanguage();
    private ISelectModel _languageModel;
    private List<ForeignLanguageSkill> _skillList;
    private List<PersonForeignLanguage> _selectedLanguageList = new ArrayList<PersonForeignLanguage>();
    private DynamicListDataSource<PersonForeignLanguage> _languageDataSource;

    // спортивные достижения
    private boolean _hasSport;
    private PersonSportAchievement _achievement = new PersonSportAchievement();
    private ISelectModel _sportTypeModel;
    private List<SportRank> _sportRankList;
    private List<PersonSportAchievement> _selectedSportList = new ArrayList<PersonSportAchievement>();
    private DynamicListDataSource<PersonSportAchievement> _sportDataSource;

    // сведения о персоне
    private List<FamilyStatus> _familyStatusList;
    private List<PensionType> _pensionTypeList;
    private List<FlatPresence> _flatPresenceList;
    private ISelectModel _workPlaceModel;
    private ISelectModel _workPostModel;
    
    // льготы для поселения в общежитие
    private boolean _hasDormitoryBenefits;
    private List<DormitoryBenefit> _dormitoryBenefitList;
    private PersonDormitoryBenefit _personDormitoryBenefit = new PersonDormitoryBenefit();
    private List<PersonDormitoryBenefit> _personDormitoryBenefitList = new ArrayList<PersonDormitoryBenefit>();
    private DynamicListDataSource<PersonDormitoryBenefit> _dormitoryBenefitDataSource;
    
    // воинский учет
    private AddressCountry _country;
    private PersonMilitaryStatus _militaryStatus = new PersonMilitaryStatus();
    private ISelectModel _militaryRegDataListModel;
    private ISelectModel _militaryAbilityStatusListModel;
    private ISelectModel _countryListModel;
    private ISelectModel _militaryOfficeListModel;
    private ISelectModel _militaryRankListModel;

    private String _entrantMasterPermKey;

    public String getEntrantMasterPermKey()
    {
        if(StringUtils.isEmpty(_entrantMasterPermKey)) return "addEntrantMaster";
        return _entrantMasterPermKey;
    }

    public void setEntrantMasterPermKey(String entrantMasterPermKey)
    {
        _entrantMasterPermKey = entrantMasterPermKey;
    }

    // Getters & Setters

    public Entrant getEntrant()
    {
        return _entrant;
    }

    public void setEntrant(Entrant entrant)
    {
        _entrant = entrant;
    }

    public OnlineEntrant getOnlineEntrant()
    {
        return _onlineEntrant;
    }

    public void setOnlineEntrant(OnlineEntrant onlineEntrant)
    {
        _onlineEntrant = onlineEntrant;
    }

    public boolean isHasBenefit()
    {
        return _hasBenefit;
    }

    public void setHasBenefit(boolean hasBenefit)
    {
        _hasBenefit = hasBenefit;
    }

    public PersonBenefit getPersonBenefit()
    {
        return _personBenefit;
    }

    public void setPersonBenefit(PersonBenefit personBenefit)
    {
        _personBenefit = personBenefit;
    }

    public List<Benefit> getBenefitList()
    {
        return _benefitList;
    }

    public void setBenefitList(List<Benefit> benefitList)
    {
        _benefitList = benefitList;
    }

    public List<PersonBenefit> getSelectedBenefitList()
    {
        return _selectedBenefitList;
    }

    public void setSelectedBenefitList(List<PersonBenefit> selectedBenefitList)
    {
        _selectedBenefitList = selectedBenefitList;
    }

    public DynamicListDataSource<PersonBenefit> getBenefitDataSource()
    {
        return _benefitDataSource;
    }

    public void setBenefitDataSource(DynamicListDataSource<PersonBenefit> benefitDataSource)
    {
        _benefitDataSource = benefitDataSource;
    }

    public boolean isHasLanguage()
    {
        return _hasLanguage;
    }

    public void setHasLanguage(boolean hasLanguage)
    {
        _hasLanguage = hasLanguage;
    }

    public PersonForeignLanguage getForeignLanguage()
    {
        return _foreignLanguage;
    }

    public void setForeignLanguage(PersonForeignLanguage foreignLanguage)
    {
        _foreignLanguage = foreignLanguage;
    }

    public ISelectModel getLanguageModel()
    {
        return _languageModel;
    }

    public void setLanguageModel(ISelectModel languageModel)
    {
        _languageModel = languageModel;
    }

    public List<ForeignLanguageSkill> getSkillList()
    {
        return _skillList;
    }

    public void setSkillList(List<ForeignLanguageSkill> skillList)
    {
        _skillList = skillList;
    }

    public List<PersonForeignLanguage> getSelectedLanguageList()
    {
        return _selectedLanguageList;
    }

    public void setSelectedLanguageList(List<PersonForeignLanguage> selectedLanguageList)
    {
        _selectedLanguageList = selectedLanguageList;
    }

    public DynamicListDataSource<PersonForeignLanguage> getLanguageDataSource()
    {
        return _languageDataSource;
    }

    public void setLanguageDataSource(DynamicListDataSource<PersonForeignLanguage> languageDataSource)
    {
        _languageDataSource = languageDataSource;
    }

    public boolean isHasSport()
    {
        return _hasSport;
    }

    public void setHasSport(boolean hasSport)
    {
        _hasSport = hasSport;
    }

    public PersonSportAchievement getAchievement()
    {
        return _achievement;
    }

    public void setAchievement(PersonSportAchievement achievement)
    {
        _achievement = achievement;
    }

    public ISelectModel getSportTypeModel()
    {
        return _sportTypeModel;
    }

    public void setSportTypeModel(ISelectModel sportTypeModel)
    {
        _sportTypeModel = sportTypeModel;
    }

    public List<SportRank> getSportRankList()
    {
        return _sportRankList;
    }

    public void setSportRankList(List<SportRank> sportRankList)
    {
        _sportRankList = sportRankList;
    }

    public List<PersonSportAchievement> getSelectedSportList()
    {
        return _selectedSportList;
    }

    public void setSelectedSportList(List<PersonSportAchievement> selectedSportList)
    {
        _selectedSportList = selectedSportList;
    }

    public DynamicListDataSource<PersonSportAchievement> getSportDataSource()
    {
        return _sportDataSource;
    }

    public void setSportDataSource(DynamicListDataSource<PersonSportAchievement> sportDataSource)
    {
        _sportDataSource = sportDataSource;
    }

    public List<FamilyStatus> getFamilyStatusList()
    {
        return _familyStatusList;
    }

    public void setFamilyStatusList(List<FamilyStatus> familyStatusList)
    {
        _familyStatusList = familyStatusList;
    }

    public List<PensionType> getPensionTypeList()
    {
        return _pensionTypeList;
    }

    public void setPensionTypeList(List<PensionType> pensionTypeList)
    {
        _pensionTypeList = pensionTypeList;
    }

    public List<FlatPresence> getFlatPresenceList()
    {
        return _flatPresenceList;
    }

    public void setFlatPresenceList(List<FlatPresence> flatPresenceList)
    {
        _flatPresenceList = flatPresenceList;
    }

    public ISelectModel getWorkPlaceModel()
    {
        return _workPlaceModel;
    }

    public void setWorkPlaceModel(ISelectModel workPlaceModel)
    {
        _workPlaceModel = workPlaceModel;
    }

    public ISelectModel getWorkPostModel()
    {
        return _workPostModel;
    }

    public void setWorkPostModel(ISelectModel workPostModel)
    {
        _workPostModel = workPostModel;
    }

    public boolean isHasDormitoryBenefits()
    {
        return _hasDormitoryBenefits;
    }

    public void setHasDormitoryBenefits(boolean hasDormitoryBenefits)
    {
        _hasDormitoryBenefits = hasDormitoryBenefits;
    }

    public List<DormitoryBenefit> getDormitoryBenefitList()
    {
        return _dormitoryBenefitList;
    }

    public void setDormitoryBenefitList(List<DormitoryBenefit> dormitoryBenefitList)
    {
        _dormitoryBenefitList = dormitoryBenefitList;
    }

    public PersonDormitoryBenefit getPersonDormitoryBenefit()
    {
        return _personDormitoryBenefit;
    }

    public void setPersonDormitoryBenefit(PersonDormitoryBenefit personDormitoryBenefit)
    {
        _personDormitoryBenefit = personDormitoryBenefit;
    }

    public List<PersonDormitoryBenefit> getPersonDormitoryBenefitList()
    {
        return _personDormitoryBenefitList;
    }

    public void setPersonDormitoryBenefitList(List<PersonDormitoryBenefit> personDormitoryBenefitList)
    {
        _personDormitoryBenefitList = personDormitoryBenefitList;
    }

    public DynamicListDataSource<PersonDormitoryBenefit> getDormitoryBenefitDataSource()
    {
        return _dormitoryBenefitDataSource;
    }

    public void setDormitoryBenefitDataSource(DynamicListDataSource<PersonDormitoryBenefit> dormitoryBenefitDataSource)
    {
        _dormitoryBenefitDataSource = dormitoryBenefitDataSource;
    }

    public AddressCountry getCountry()
    {
        return _country;
    }

    public void setCountry(AddressCountry country)
    {
        _country = country;
    }

    public PersonMilitaryStatus getMilitaryStatus()
    {
        return _militaryStatus;
    }

    public void setMilitaryStatus(PersonMilitaryStatus militaryStatus)
    {
        _militaryStatus = militaryStatus;
    }

    public ISelectModel getMilitaryRegDataListModel()
    {
        return _militaryRegDataListModel;
    }

    public void setMilitaryRegDataListModel(ISelectModel militaryRegDataListModel)
    {
        _militaryRegDataListModel = militaryRegDataListModel;
    }

    public ISelectModel getMilitaryAbilityStatusListModel()
    {
        return _militaryAbilityStatusListModel;
    }

    public void setMilitaryAbilityStatusListModel(ISelectModel militaryAbilityStatusListModel)
    {
        _militaryAbilityStatusListModel = militaryAbilityStatusListModel;
    }

    public ISelectModel getCountryListModel()
    {
        return _countryListModel;
    }

    public void setCountryListModel(ISelectModel countryListModel)
    {
        _countryListModel = countryListModel;
    }

    public ISelectModel getMilitaryOfficeListModel()
    {
        return _militaryOfficeListModel;
    }

    public void setMilitaryOfficeListModel(ISelectModel militaryOfficeListModel)
    {
        _militaryOfficeListModel = militaryOfficeListModel;
    }

    public ISelectModel getMilitaryRankListModel()
    {
        return _militaryRankListModel;
    }

    public void setMilitaryRankListModel(ISelectModel militaryRankListModel)
    {
        _militaryRankListModel = militaryRankListModel;
    }
}
