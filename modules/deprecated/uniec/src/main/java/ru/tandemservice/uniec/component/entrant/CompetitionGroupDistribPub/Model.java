package ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub;

import org.tandemframework.core.bean.IFastBean;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uniec.entity.ecg.EcgDistribObject;
import ru.tandemservice.uniec.entity.ecg.EcgDistribQuota;

/**
 * @author vdanilov
 */
@State({
	@Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="distrib.id"),
    @Bind(key="selectedTab", binding="selectedTab")
})
public class Model {
	private String selectedTab;
	public String getSelectedTab() { return this.selectedTab; }
	public void setSelectedTab(final String selectedTab) { this.selectedTab = selectedTab; }

	private EcgDistribObject distrib = new EcgDistribObject();
	public EcgDistribObject getDistrib() { return this.distrib; }
	public void setDistrib(final EcgDistribObject distrib) { this.distrib = distrib; }

	@SuppressWarnings("serial")
	public static class Wrapper extends EcgDistribQuota {
		@SuppressWarnings("unchecked")
		@Override public IFastBean getFastBean() { return null; }

		private final int realQuota;
		public int getRealQuota() { return this.realQuota; }
		public String getQuotaString() { return (this.realQuota + " / " + this.getQuota()); }

		public Wrapper(final EcgDistribQuota quota, final Integer realQuota) {
			this.update(quota);
			this.realQuota = (null == realQuota ? 0 : realQuota.intValue());
		}

		/* total row */
		public Wrapper(final int real, final int quota) {
			realQuota = real;
			this.setQuota(quota);
		}
	}


	private DynamicListDataSource<Wrapper> quotaDataSource;
	public DynamicListDataSource<Wrapper> getQuotaDataSource() { return this.quotaDataSource; }
	public void setQuotaDataSource(final DynamicListDataSource<Wrapper> quotaDataSource) { this.quotaDataSource = quotaDataSource; }

}
