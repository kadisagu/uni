package ru.tandemservice.uniec.entity.report;

import org.tandemframework.core.view.formatter.DateFormatter;

import ru.tandemservice.uniec.entity.report.gen.EntrantDailyRatingByTAReportGen;

/**
 * Ежедневный рейтинг абитуриентов (с выделением групп «Общий прием» и «Целевой прием»)
 */
public class EntrantDailyRatingByTAReport extends EntrantDailyRatingByTAReportGen
{
    public static final String P_PERIOD_TITLE = "periodTitle";

    public String getPeriodTitle()
    {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }
}