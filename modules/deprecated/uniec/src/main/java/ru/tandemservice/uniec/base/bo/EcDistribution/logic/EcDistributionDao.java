/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.logic;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableDouble;
import org.hibernate.Session;
import org.springframework.util.Assert;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.catalog.gen.CompensationTypeGen;
import ru.tandemservice.uni.entity.catalog.gen.StudentCategoryGen;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.*;
import ru.tandemservice.uniec.base.bo.EcPreEnroll.EcPreEnrollManager;
import ru.tandemservice.uniec.base.entity.ecg.*;
import ru.tandemservice.uniec.base.entity.ecg.codes.EcgDistributionStateCodes;
import ru.tandemservice.uniec.base.entity.ecg.gen.EcgDistributionConfigGen;
import ru.tandemservice.uniec.base.entity.ecg.gen.EcgDistributionDetailGen;
import ru.tandemservice.uniec.base.entity.ecg.gen.EcgDistributionGen;
import ru.tandemservice.uniec.base.entity.ecg.gen.EcgDistributionStateGen;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.catalog.codes.TargetAdmissionKindCodes;
import ru.tandemservice.uniec.entity.catalog.gen.CompetitionKindGen;
import ru.tandemservice.uniec.entity.catalog.gen.EntrantStateGen;
import ru.tandemservice.uniec.entity.catalog.gen.TargetAdmissionKindGen;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniec.entity.settings.NotUsedTargetAdmissionKind;
import ru.tandemservice.uniec.entity.settings.TargetAdmissionPlanRelation;
import ru.tandemservice.uniec.ws.distribution.EnrollmentDistributionEnvironmentNode;
import ru.tandemservice.uniec.ws.distribution.IEnrollmentDistributionServiceDao;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Vasily Zhukov
 * @since 06.07.2011
 */
public class EcDistributionDao extends CommonDAO implements IEcDistributionDao
{
    /**
     * нельзя в паралельных транзакциях добавлять или удалять распределения на одну и ту же конфигурацию,
     * так как распределения должны идти в точности одно за другим по номеру волны
     */
    private static final String DISTRIBUTION_SAVE_DELETE_LOCK = "EcgDistributionSaveDeleteLock-";

    @Override
    public List<CompetitionGroup> getCompetitionGroupList(EnrollmentCampaign enrollmentCampaign, List<Qualifications> qualificationList)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(CompetitionGroup.class, "e")
                .where(eq(property(CompetitionGroup.enrollmentCampaign().fromAlias("e")), value(enrollmentCampaign)));

        if (qualificationList != null && !qualificationList.isEmpty())
        {
            DQLSelectBuilder cgBuilder = new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, "cgDir")
                    .predicate(DQLPredicateType.distinct)
                    .column(property(EnrollmentDirection.competitionGroup().id().fromAlias("cgDir")))
                    .where(in(property(EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().fromAlias("cgDir")), qualificationList));

            builder.where(in(property(CompetitionGroup.id().fromAlias("e")), cgBuilder.buildQuery()));
        }

        List<CompetitionGroup> list = builder.createStatement(getSession()).list();
        Collections.sort(list, CompetitionGroup.TITLED_COMPARATOR);

        return list;
    }

    @Override
    public Integer getDistributionMaxWave(EnrollmentCampaign enrollmentCampaign, CompensationType compensationType, boolean secondHighAdmission)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EcgDistribution.class, "e")
        .column(property(EcgDistribution.wave().fromAlias("e")))
        .where(eq(property(EcgDistribution.config().ecgItem().enrollmentCampaign().fromAlias("e")), value(enrollmentCampaign)))
        .where(eq(property(EcgDistribution.config().secondHighAdmission().fromAlias("e")), value(secondHighAdmission)));

        if (compensationType != null)
            builder.where(eq(property(EcgDistribution.config().compensationType().fromAlias("e")), value(compensationType)));

        builder.order(property(EcgDistribution.wave().fromAlias("e")), OrderDirection.desc);

        Number number = builder.createStatement(getSession()).setMaxResults(1).uniqueResult();

        return number == null ? null : number.intValue();
    }

    @Override
    public String getDistributionFileName(IEcgDistribution distribution)
    {
        return distribution.getConfig().getEcgItem().getId() + "-" +
        (distribution.getConfig().getCompensationType().isBudget() ? "0" : "1") + "-" +
        (distribution.getConfig().isSecondHighAdmission() ? "1" : "0") + "-" +
        (distribution instanceof EcgDistributionDetail ? "1" : "0") + "-" +
        distribution.getDistribution().getWave() + "-" +
        new SimpleDateFormat("yyyyMMddHHmm").format(new Date());
    }

    @Override
    public String getDistributionFileName(EcgDistributionDTO distributionDTO)
    {
        return distributionDTO.getEcgItem().getId() + "-" +
        (distributionDTO.getCompensationType().isBudget() ? "0" : "1") + "-" +
        (distributionDTO.isSecondHighAdmission() ? "1" : "0") + "-" +
        (distributionDTO.isDetailDistribution() ? "1" : "0") + "-" +
        distributionDTO.getWave() + "-" +
        new SimpleDateFormat("yyyyMMddHHmm").format(new Date());
    }

    @Override
    public List<EnrollmentDirection> getDistributionDirectionList(EcgConfigDTO configDTO)
    {
        // для распределений по направлениям все просто
        if (configDTO.getEcgItem() instanceof EnrollmentDirection)
            return Collections.singletonList((EnrollmentDirection) configDTO.getEcgItem());

        EcgDistributionConfig config = getByNaturalId(new EcgDistributionConfigGen.NaturalId(configDTO.getEcgItem(), configDTO.getCompensationType(), configDTO.isSecondHighAdmission()));

        if (config != null)
        {
            EcgDistribution distribution = getByNaturalId(new EcgDistributionGen.NaturalId(config, DISTRIBUTION_FIRST_WAVE));

            if (distribution != null)
                return getDistributionDirectionList(distribution);
        }

        return new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, "e").column("e")
        .where(eq(property(EnrollmentDirection.competitionGroup().fromAlias("e")), value(configDTO.getEcgItem())))
        .order(property(EnrollmentDirection.title().fromAlias("e")))
        .createStatement(getSession()).list();
    }

    @Override
    public List<TargetAdmissionKind> getDistributionTaKindList(EcgConfigDTO configDTO)
    {
        EcgDistributionConfig config = getByNaturalId(new EcgDistributionConfigGen.NaturalId(configDTO.getEcgItem(), configDTO.getCompensationType(), configDTO.isSecondHighAdmission()));

        if (config != null)
        {
            EcgDistribution distribution = getByNaturalId(new EcgDistributionGen.NaturalId(config, DISTRIBUTION_FIRST_WAVE));

            if (distribution != null)
                return getDistributionTaKindList(distribution);
        }

        List<TargetAdmissionKind> list = new DQLSelectBuilder().fromEntity(TargetAdmissionKind.class, "e").column("e")
        .where(notIn(property(TargetAdmissionKind.id().fromAlias("e")),
            new DQLSelectBuilder().fromEntity(NotUsedTargetAdmissionKind.class, "t").column(property(NotUsedTargetAdmissionKind.targetAdmissionKind().id().fromAlias("t")))
            .where(eq(property(NotUsedTargetAdmissionKind.enrollmentCampaign().fromAlias("t")), value(configDTO.getEcgItem().getEnrollmentCampaign())))
            .buildQuery()
        ))
        .createStatement(getSession()).list();

        // берем только листовые виды цп
        Set<TargetAdmissionKind> removeSet = new HashSet<>();
        for (TargetAdmissionKind item : list)
        {
            TargetAdmissionKind kind = item;
            while ((kind = kind.getParent()) != null) removeSet.add(kind);
        }

        list.removeAll(removeSet);

        // если видов цп нет, то используется вид цп по умолчанию
        if (list.isEmpty())
            list.add(this.<TargetAdmissionKind>getByNaturalId(new TargetAdmissionKindGen.NaturalId(TargetAdmissionKindCodes.TA_DEFAULT)));

        Collections.sort(list, TargetAdmissionKind.ALL_LEVEL_PRIORITY_COMPARATOR);

        return list;
    }

    @Override
    public List<EnrollmentDirection> getDistributionDirectionList(EcgDistribution distribution)
    {
        if (distribution.getConfig().getEcgItem() instanceof EnrollmentDirection)
            return Collections.singletonList((EnrollmentDirection) distribution.getConfig().getEcgItem());

        List<EnrollmentDirection> directionList = new DQLSelectBuilder().fromEntity(EcgDistributionQuota.class, "e")
        .column(property(EcgDistributionQuota.direction().fromAlias("e")))
        .where(eq(property(EcgDistributionQuota.distribution().fromAlias("e")), value(distribution)))
        .order(property(EcgDistributionQuota.direction().title().fromAlias("e")))
        .createStatement(getSession()).list();

        // небольшие проверки консистентности данных

        if (directionList.isEmpty())
            throw new RuntimeException("DirectionList is empty for distribution: " + distribution.getId());

        if (directionList.size() != new HashSet<>(directionList).size())
            throw new RuntimeException("DirectionList contains dublicates for distribution: " + distribution.getId());

        return directionList;
    }

    @Override
    public List<TargetAdmissionKind> getDistributionTaKindList(EcgDistribution distribution)
    {
        List<TargetAdmissionKind> taKindList = new DQLSelectBuilder().fromEntity(EcgDistributionTAQuota.class, "e")
        .column(property(EcgDistributionTAQuota.targetAdmissionKind().fromAlias("e")))
        .where(eq(property(EcgDistributionTAQuota.distributionQuota().distribution().fromAlias("e")), value(distribution)))
        .createStatement(getSession()).list();

        // небольшие проверки консистентности данных

        if (taKindList.isEmpty())
            throw new RuntimeException("TaKindList is empty for distribution: " + distribution.getId());

        taKindList = new ArrayList<>(new HashSet<>(taKindList));

        Collections.sort(taKindList, TargetAdmissionKind.ALL_LEVEL_PRIORITY_COMPARATOR);

        return taKindList;
    }

    @Override
    public List<EcgDistributionTAQuota> getDistributionTAQuotaList(Long distributionId)
    {
        return new DQLSelectBuilder().fromEntity(EcgDistributionTAQuota.class, "e").column("e")
        .where(eq(property(EcgDistributionTAQuota.distributionQuota().distribution().id().fromAlias("e")), value(distributionId)))
        .createStatement(getSession()).list();
    }

    @Override
    public List<EcgDistributionDTO> getDistributionDTOList(EnrollmentCampaign enrollmentCampaign,
        CompensationType compensationType,
        boolean secondHighAdmission,
        Integer wave,
        EcgDistributionState state,
        CompetitionGroup competitionGroup,
        List<Qualifications> qualificationList,
        List<OrgUnit> formativeOrgUnitList,
        List<OrgUnit> territorialOrgUnitList,
        List<EducationLevelsHighSchool> educationLevelHighSchoolList,
        List<DevelopForm> developFormList,
        List<DevelopCondition> developConditionList,
        List<DevelopTech> developTechList,
        List<DevelopPeriod> developPeriodList)
        {
        if (enrollmentCampaign == null) return Collections.emptyList();

        List<EcgDistribution> distributionList;
        List<EcgDistributionDetail> distributionDetailList;
        List<? extends IPersistentEcgItem> ecgItemList;

        if (enrollmentCampaign.isUseCompetitionGroup())
        {
            // получаем сохраненные основные распределения
            distributionList = getDistributionList(enrollmentCampaign, compensationType, secondHighAdmission, wave, state, competitionGroup, qualificationList);

            // получаем сохраненные уточняющие распределения
            distributionDetailList = getDistributionDetailList(distributionList);

            // получаем либо все конкурсные группы, либо только указанную
            ecgItemList = competitionGroup == null ? getCompetitionGroupList(enrollmentCampaign, qualificationList) : Arrays.asList(competitionGroup);
        } else
        {
            // получаем сохраненные основные распределения
            distributionList = getDistributionList(enrollmentCampaign, compensationType, secondHighAdmission, wave, state, formativeOrgUnitList, territorialOrgUnitList, educationLevelHighSchoolList, developFormList, developConditionList, developTechList, developPeriodList, qualificationList);

            // уточняющих распределений быть не может
            distributionDetailList = Collections.emptyList();

            // получаем фильтрованные направления приема
            ecgItemList = getEnrollmentDirectionList(enrollmentCampaign, formativeOrgUnitList, territorialOrgUnitList, educationLevelHighSchoolList, developFormList, developConditionList, developTechList, developPeriodList, qualificationList);
        }

        // определяем какие ячейки будут показываться в видах затрат
        List<CompensationType> compensationTypeList = compensationType == null ? Arrays.asList(this.<CompensationType>getByNaturalId(new CompensationTypeGen.NaturalId(UniDefines.COMPENSATION_TYPE_BUDGET)), this.<CompensationType>getByNaturalId(new CompensationTypeGen.NaturalId(UniDefines.COMPENSATION_TYPE_CONTRACT))) : Arrays.asList(compensationType);

        // создаем мап: конфигурация распределения -> список основных распределений
        Map<EcgConfigDTO, List<EcgDistribution>> configKey2distributionList = new HashMap<>();

        for (EcgDistribution distribution : distributionList)
        {
            EcgDistributionConfig config = distribution.getConfig();

            EcgConfigDTO configDTO = new EcgConfigDTO(config);

            List<EcgDistribution> list = configKey2distributionList.get(configDTO);

            if (list == null)
                configKey2distributionList.put(configDTO, list = new ArrayList<>());

            list.add(distribution);
        }

        Comparator<EcgDistribution> waveComparator = (o1, o2) -> Integer.compare(o1.getWave(), o2.getWave());

        // список основных распределений сортируем по волнам
        for (List<EcgDistribution> value : configKey2distributionList.values())
        {
            Collections.sort(value, waveComparator);
        }

        // создаем мап: основное распределение -> уточняющее распределение
        Map<EcgDistribution, EcgDistributionDetail> distribution2detail = new HashMap<>();

        for (EcgDistributionDetail distributionDetail : distributionDetailList)
            distribution2detail.put(distributionDetail.getDistribution(), distributionDetail);

        // подготавливаем итоговый список строк
        List<EcgDistributionDTO> result = new ArrayList<>();

        // счетчик идентификаторов для загрушек (id отрицательный)
        long id = 0;

        boolean showEmptyConfig = wave == null && state == null;

        // создаем строки для отображения
        for (IPersistentEcgItem ecgItem : ecgItemList)
        {
            for (CompensationType type : compensationTypeList)
            {
                EcgConfigDTO configDTO = new EcgConfigDTO(ecgItem, type, secondHighAdmission);

                List<EcgDistribution> list = configKey2distributionList.get(configDTO);

                if (list != null || showEmptyConfig)
                {
                    // добавляем все распределения
                    if (list != null)
                    {
                        for (EcgDistribution item : list)
                        {
                            EcgDistributionDetail distributionDetail = distribution2detail.get(item);

                            result.add(new EcgDistributionDTO(id++, item, distributionDetail));

                            // если есть уточняющее, то добавляем его сразу после основного
                            if (distributionDetail != null)
                                result.add(new EcgDistributionDTO(id++, distributionDetail));
                        }
                    }

                    // создаем пустую строку для добавления нового уточняющего распределения
                    result.add(new EcgDistributionDTO(id++, ecgItem, type, secondHighAdmission));
                }
            }
        }

        return result;
        }

    @Override
    public EcgDistribution saveDistribution(EcgConfigDTO configDTO, int wave, Map<Long, Integer> quotaMap, Map<Long, Map<Long, Integer>> taQuotaMap)
    {
        if (configDTO == null)
            throw new IllegalArgumentException("Parameter 'configDTO' can't be null.");

        if (wave < DISTRIBUTION_FIRST_WAVE)
            throw new IllegalArgumentException("Parameter 'wave' must be greater or equals '" + DISTRIBUTION_FIRST_WAVE + "'.");

        // нельзя одновременно сохранять и удалять распределения на одинаковую конфигурацию
        NamedSyncInTransactionCheckLocker.register(getSession(), DISTRIBUTION_SAVE_DELETE_LOCK + configDTO.toString());

        Session session = getSession();

        // получаем необходимые направления приема
        List<EnrollmentDirection> directionList = getDistributionDirectionList(configDTO);

        // получаем необходимые виды цп
        List<TargetAdmissionKind> kindList = getDistributionTaKindList(configDTO);

        // получаем конфигурацию распределения
        EcgDistributionConfig config = getByNaturalId(new EcgDistributionConfigGen.NaturalId(configDTO.getEcgItem(), configDTO.getCompensationType(), configDTO.isSecondHighAdmission()));

        // получаем распределение последней волны или null
        EcgDistribution maxDistribution = null;

        if (config != null)
        {
            // получаем распределение последней волны
            maxDistribution = getDistributionWithMaxWave(config);

            if (maxDistribution != null)
            {
                if (maxDistribution.getWave() + DISTRIBUTION_FIRST_WAVE != wave)
                    throw new ApplicationException("Распределение «" + maxDistribution.getTitle() + "» не является предыдущим к добавляемому.");

                if (!maxDistribution.getState().isApproved())
                    throw new ApplicationException("Основное распределение «" + maxDistribution.getTitle() + "» не утверждено.");

                // DEV-2858
//                if (config.getEcgItem() instanceof CompetitionGroup)
//                {
//                    EcgDistributionDetail distributionDetail = getByNaturalId(new EcgDistributionDetailGen.NaturalId(maxDistribution));
//
//                    if (distributionDetail == null)
//                        throw new ApplicationException("На основное распределение «" + maxDistribution.getTitle() + "» еще не создано уточняющее.");
//
//                    if (!distributionDetail.getState().isApproved())
//                        throw new ApplicationException("У основного распределения «" + maxDistribution.getTitle() + "» уточняющее не утверждено.");
//                }
            } else
            {
                if (wave != DISTRIBUTION_FIRST_WAVE)
                    throw new ApplicationException("Добавляемое распределение должно быть первой волны.");
            }
        } else
        {
            if (wave != DISTRIBUTION_FIRST_WAVE)
                throw new ApplicationException("Добавляемое распределение должно быть первой волны.");

            // если конфигурации нет - создаем ее
            config = new EcgDistributionConfig();
            config.setEcgItem(configDTO.getEcgItem());
            config.setCompensationType(configDTO.getCompensationType());
            config.setSecondHighAdmission(configDTO.isSecondHighAdmission());
            session.save(config);
        }

        EcgDistribution distribution = new EcgDistribution();
        distribution.setConfig(config);
        distribution.setWave(maxDistribution == null ? DISTRIBUTION_FIRST_WAVE : maxDistribution.getWave() + 1);
        distribution.setState(this.<EcgDistributionState>getByNaturalId(new EcgDistributionStateGen.NaturalId(EcgDistributionStateCodes.FORMATIVE)));
        distribution.setFormingDate(new Date());
        session.save(distribution);

        for (EnrollmentDirection direction : directionList)
        {
            EcgDistributionQuota quota = new EcgDistributionQuota();
            quota.setDistribution(distribution);
            quota.setDirection(direction);

            Integer plan = quotaMap.get(direction.getId());
            quota.setQuota(plan == null || plan < 0 ? 0 : plan);
            session.save(quota);

            Map<Long, Integer> taQuotaSubMap = taQuotaMap.get(direction.getId());
            if (taQuotaSubMap == null) taQuotaSubMap = Collections.emptyMap();

            for (TargetAdmissionKind kind : kindList)
            {
                EcgDistributionTAQuota taQuota = new EcgDistributionTAQuota();
                taQuota.setDistributionQuota(quota);
                taQuota.setTargetAdmissionKind(kind);

                Integer targetPlan = taQuotaSubMap.get(kind.getId());
                taQuota.setQuota(targetPlan == null || targetPlan < 0 ? 0 : targetPlan);

                session.save(taQuota);
            }
        }

        return distribution;
    }

    @Override
    public EcgDistributionDetail saveDistributionDetail(Long distributionId)
    {
        if (distributionId == null)
            throw new IllegalArgumentException("Parameter 'distributionId' can't be null.");

        Session session = getSession();

        EcgDistribution distribution = getNotNull(EcgDistribution.class, distributionId);

        EcgConfigDTO configDTO = new EcgConfigDTO(distribution.getConfig());

        // нельзя одновременно сохранять и удалять распределения на одинаковую конфигурацию
        NamedSyncInTransactionCheckLocker.register(getSession(), DISTRIBUTION_SAVE_DELETE_LOCK + configDTO.toString());

        session.refresh(distribution);

        if (!distribution.getState().isApproved())
            throw new ApplicationException("Распределение «" + distribution.getTitle() + "» должно быть утверждено.");

        // получаем распределение последней волны
        EcgDistribution maxDistribution = getDistributionWithMaxWave(distribution.getConfig());

        // нет ни одного распределения
        if (maxDistribution == null) return null;

        // создавать уточнящее распределение можно только на распределение последней волны
        if (!maxDistribution.equals(distribution))
            throw new ApplicationException("Создавать уточняющее распределение можно только на распределение последней волны.");

        EcgDistributionDetail distributionDetail = new EcgDistributionDetail();
        distributionDetail.setState(getByCode(EcgDistributionState.class, EcgDistributionStateCodes.FORMATIVE));
        distributionDetail.setDistribution(distribution);
        distributionDetail.setFormingDate(new Date());
        session.save(distributionDetail);

        return distributionDetail;
    }

    @Override
    public void updateApprovalDate(Long distributionId, Date approvalDate)
    {
        Assert.notNull(distributionId, "Parameter 'distributionId' can't be null");

        IEcgDistribution distribution = getNotNull(distributionId);

        getSession().refresh(distribution);

        if (null == approvalDate)
        {
            if (distribution.getState().isApproved())
                throw new ApplicationException("Распределение «" + distribution.getTitle() + "» согласовано. ");
        }
        else
        {
            if (!distribution.getState().isApproved())
                throw new ApplicationException("Распределение «" + distribution.getTitle() + "» не согласовано.");
        }

        distribution.setProperty("approvalDate", approvalDate);

        update(distribution);
    }

    @Override
    public void updateDistribution(Long distributionId, Map<Long, Integer> quotaMap, Map<Long, Map<Long, Integer>> taQuotaMap)
    {
        Assert.notNull(distributionId, "Parameter 'distributionId' can't be null");

        EcgDistribution distribution = getNotNull(EcgDistribution.class, distributionId);

        getSession().refresh(distribution);

        if (distribution.getState().isLockedOrApproved())
            throw new ApplicationException("Распределение «" + distribution.getTitle() + "» уже нельзя изменять.");

        List<EcgDistributionTAQuota> taQuotaList = getDistributionTAQuotaList(distributionId);

        for (EcgDistributionTAQuota taQuota : taQuotaList)
        {
            Long directionId = taQuota.getDistributionQuota().getDirection().getId();
            Integer plan = quotaMap.get(directionId);
            taQuota.getDistributionQuota().setQuota(plan == null || plan < 0 ? 0 : plan);

            update(taQuota.getDistributionQuota());

            Map<Long, Integer> targetPlanMap = taQuotaMap.get(directionId);
            if (targetPlanMap != null)
            {
                Integer targetPlan = targetPlanMap.get(taQuota.getTargetAdmissionKind().getId());
                taQuota.setQuota(targetPlan == null || targetPlan < 0 ? 0 : targetPlan);
            }

            update(taQuota);
        }
    }

    @Override
    public void deleteDistribution(Long distributionId)
    {
        EcgDistribution distribution = getNotNull(EcgDistribution.class, distributionId);

        EcgConfigDTO configDTO = new EcgConfigDTO(distribution.getConfig());

        // нельзя одновременно сохранять и удалять распределения на одинаковую конфигурацию
        NamedSyncInTransactionCheckLocker.register(getSession(), DISTRIBUTION_SAVE_DELETE_LOCK + configDTO.toString());

        Session session = getSession();

        // получаем распределение последней волны
        EcgDistribution maxDistribution = getDistributionWithMaxWave(distribution.getConfig());

        // распределение уже было удалено
        if (maxDistribution == null) return;

        // можно удалять только распределение последней волны
        if (!maxDistribution.equals(distribution))
            throw new ApplicationException("Можно удалить только распределение последней волны.");

        // получаем возможное уточняющее распределение
        EcgDistributionDetail distributionDetail = get(EcgDistributionDetail.class, EcgDistributionDetail.distribution(), maxDistribution);

        if (distributionDetail != null)
            throw new ApplicationException("Можно удалить только распределение без уточняющего.");

        session.delete(distribution);
    }

    @Override
    public void deleteDistributionDetail(Long distributionDetailId)
    {
        EcgDistributionDetail distributionDetail = getNotNull(EcgDistributionDetail.class, distributionDetailId);

        EcgConfigDTO configDTO = new EcgConfigDTO(distributionDetail.getDistribution().getConfig());

        // нельзя одновременно сохранять и удалять распределения на одинаковую конфигурацию
        NamedSyncInTransactionCheckLocker.register(getSession(), DISTRIBUTION_SAVE_DELETE_LOCK + configDTO.toString());

        Session session = getSession();

        // получаем распределение последней волны
        EcgDistribution maxDistribution = getDistributionWithMaxWave(distributionDetail.getDistribution().getConfig());

        // распределение уже было удалено
        if (maxDistribution == null) return;

        // можно удалять только уточняющее распределение последней волны
        if (!maxDistribution.equals(distributionDetail.getDistribution()))
            throw new ApplicationException("Можно удалить только уточняющее распределение последней волны.");

        session.delete(distributionDetail);
    }

    @Override
    public EcgDistribution getDistributionWithMaxWave(EcgConfigDTO configDTO)
    {
        EcgDistributionConfig config = getByNaturalId(new EcgDistributionConfigGen.NaturalId(configDTO.getEcgItem(), configDTO.getCompensationType(), configDTO.isSecondHighAdmission()));

        if (config == null) return null;

        return getDistributionWithMaxWave(config);
    }

    @Override
    public EcgDistribution getDistributionWithMaxWave(EcgDistributionConfig config)
    {
        return new DQLSelectBuilder().fromEntity(EcgDistribution.class, "e").column("e")
        .where(eq(property(EcgDistribution.config().fromAlias("e")), value(config)))
        .order(property(EcgDistribution.wave().fromAlias("e")), OrderDirection.desc)
        .createStatement(getSession()).setMaxResults(1).uniqueResult();
    }

    @Override
    public IEcgQuotaDTO getRecomendedQuotaDTO(EcgConfigDTO configDTO, boolean isFirstWave)
    {
        // получаем все интересующие нас направления
        List<EnrollmentDirection> directionList = getDistributionDirectionList(configDTO);

        // получаем все интересующие нас виды цп
        List<TargetAdmissionKind> taKindList = getDistributionTaKindList(configDTO);

        // все нпп
        Set<EducationOrgUnit> educationOrgUnitSet = new HashSet<>();
        for (EnrollmentDirection enrollmentDirection : directionList)
            educationOrgUnitSet.add(enrollmentDirection.getEducationOrgUnit());

        // планы приема по направлениям из распределения первой волны
        Map<Long, Integer> direction2quotaMap = new HashMap<>();

        // планы приема по видам цп по направлениям из распределения первой волны
        Map<Long, Map<Long, Integer>> direction2kind2quotaMap = new HashMap<>();

        // конфигурация распределения
        EcgDistributionConfig config = getByNaturalId(new EcgDistributionConfigGen.NaturalId(configDTO.getEcgItem(), configDTO.getCompensationType(), configDTO.isSecondHighAdmission()));

        if (config != null && !isFirstWave)
        {
            EcgDistribution distribution = getByNaturalId(new EcgDistributionGen.NaturalId(config, DISTRIBUTION_FIRST_WAVE)); // первая волна

            if (distribution != null)
            {
                List<EcgDistributionQuota> quotaList = getList(EcgDistributionQuota.class, EcgDistributionQuota.distribution(), distribution);

                for (EcgDistributionQuota quota : quotaList)
                    direction2quotaMap.put(quota.getDirection().getId(), quota.getQuota());

                List<EcgDistributionTAQuota> taQuotaList = getList(EcgDistributionTAQuota.class, EcgDistributionTAQuota.distributionQuota(), quotaList);

                for (EcgDistributionTAQuota taQuota : taQuotaList)
                {
                    Long directionId = taQuota.getDistributionQuota().getDirection().getId();

                    Map<Long, Integer> kind2quotaMap = direction2kind2quotaMap.get(directionId);

                    if (kind2quotaMap == null)
                        direction2kind2quotaMap.put(directionId, kind2quotaMap = new HashMap<>());

                    kind2quotaMap.put(taQuota.getTargetAdmissionKind().getId(), taQuota.getQuota());
                }
            }
        }

        // планы приема по видам цп из направлений приема
        Map<Long, Map<Long, Integer>> direction2kind2planMap = new HashMap<>();

        for (TargetAdmissionPlanRelation planRelation : new DQLSelectBuilder().fromEntity(TargetAdmissionPlanRelation.class, "e").column("e")
                .where(in(property(TargetAdmissionPlanRelation.entranceEducationOrgUnit().fromAlias("e")), directionList))
                .where(eq(property(TargetAdmissionPlanRelation.budget().fromAlias("e")), value(configDTO.getCompensationType().isBudget())))
                .createStatement(getSession()).<TargetAdmissionPlanRelation>list())
        {
            Long directionId = planRelation.getEntranceEducationOrgUnit().getId();
            Map<Long, Integer> map = direction2kind2planMap.get(directionId);
            if (map == null)
                direction2kind2planMap.put(directionId, map = new HashMap<>());
            map.put(planRelation.getTargetAdmissionKind().getId(), planRelation.getPlanValue());
        }

        // нпп -> кол-во предзачисленных
        Map<Long, Integer> ou2countMap = new HashMap<>();

        // нпп -> вид цп -> кол-во предзачисленных
        Map<Long, Map<Long, Integer>> ou2kind2countMap = new HashMap<>();

        // вид цп по умолчанию (на случай когда зачислен по цп, а в выбранном направлении приема нет вида цп)
        TargetAdmissionKind defaultTargetAdmissionKind = getByNaturalId(new TargetAdmissionKindGen.NaturalId(TargetAdmissionKindCodes.TA_DEFAULT));

        for (PreliminaryEnrollmentStudent preStudent : new DQLSelectBuilder().fromEntity(PreliminaryEnrollmentStudent.class, "e").column("e")
                .where(eq(property(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().enrollmentDirection().enrollmentCampaign().fromAlias("e")), value(configDTO.getEcgItem().getEnrollmentCampaign())))
                .where(in(property(PreliminaryEnrollmentStudent.educationOrgUnit().fromAlias("e")), educationOrgUnitSet))
                .where(eq(property(PreliminaryEnrollmentStudent.compensationType().fromAlias("e")), value(configDTO.getCompensationType())))
                .where(in(property(PreliminaryEnrollmentStudent.studentCategory().code().fromAlias("e")), configDTO.isSecondHighAdmission() ? Collections.singletonList(UniDefines.STUDENT_CATEGORY_SECOND_HIGH) : Arrays.asList(UniDefines.STUDENT_CATEGORY_STUDENT, UniDefines.STUDENT_CATEGORY_LISTENER)))
                .createStatement(getSession()).<PreliminaryEnrollmentStudent>list())
        {
            Long ouId = preStudent.getEducationOrgUnit().getId();

            Integer plan = ou2countMap.get(ouId);
            ou2countMap.put(ouId, plan == null ? 1 : plan + 1);

            if (preStudent.isTargetAdmission())
            {
                // целевик
                Map<Long, Integer> kind2countMap = ou2kind2countMap.get(ouId);
                if (kind2countMap == null)
                    ou2kind2countMap.put(ouId, kind2countMap = new HashMap<>());
                TargetAdmissionKind kind = preStudent.getRequestedEnrollmentDirection().getTargetAdmissionKind();
                if (kind == null) kind = defaultTargetAdmissionKind;
                plan = kind2countMap.get(kind.getId());
                kind2countMap.put(kind.getId(), plan == null ? 1 : plan + 1);
            }
        }

        // итоговые планы приема по направлениям приема
        Map<Long, Integer> quotaMap = new HashMap<>();

        // итоговые планы приема по видам цп по направлениям приема
        Map<Long, Map<Long, Integer>> taQuotaMap = new HashMap<>();

        for (EnrollmentDirection direction : directionList)
        {
            Long directionId = direction.getId();
            Long ouId = direction.getEducationOrgUnit().getId();

            // ПЛАНЫ ПРИЕМА ПО НАПРАВЛЕНИЮ

            // получаем план приема по направлению из распределения первой волны
            Integer quotaA = direction2quotaMap.get(directionId);

            // если план приема нельзя определить (нет распределения первой волны), то берем план приема из направления приема с учетом вида затрат
            if (quotaA == null)
                quotaA = configDTO.getCompensationType().isBudget() ? direction.getMinisterialPlan() : direction.getContractPlan();

            if (quotaA == null) quotaA = 0;

            // получаем кол-во предзачисленных
            Integer quotaB = ou2countMap.get(ouId);

            if (quotaB == null) quotaB = 0;

            int quota = quotaA - quotaB;

            if (quota < 0) quota = 0;

            // сохраняем итоговый план по направлению
            quotaMap.put(directionId, quota);

            // ПЛАНЫ ПРИЕМА ПО НАПРАВЛЕНИЮ ПО ВИДАМ ЦП

            // получаем планы приема по видам цп из распределения первой волны
            Map<Long, Integer> kind2quotaMap = direction2kind2quotaMap.get(directionId);
            if (kind2quotaMap == null) kind2quotaMap = Collections.emptyMap();

            // получаем планы приема по видам цп из направлений приема
            Map<Long, Integer> kind2quotaFromDirectionMap = direction2kind2planMap.get(direction.getId());
            if (kind2quotaFromDirectionMap == null) kind2quotaFromDirectionMap = Collections.emptyMap();

            // получаем планы приема по видам цп из предзачисленных
            Map<Long, Integer> kind2quotaFromPreStudentMap = ou2kind2countMap.get(ouId);
            if (kind2quotaFromPreStudentMap == null) kind2quotaFromPreStudentMap = Collections.emptyMap();

            // итоговые планы приема по видам цп
            Map<Long, Integer> taQuotaResultMap = taQuotaMap.get(directionId);
            if (taQuotaResultMap == null)
                taQuotaMap.put(directionId, taQuotaResultMap = new HashMap<>());

            for (TargetAdmissionKind taKind : taKindList)
            {
                // получаем план приема по виду цп из первого распределения
                Integer taQuotaA = kind2quotaMap.get(taKind.getId());

                // если план приема нельзя определить, то берем план приема из направления приема с учетом вида затрат
                if (taQuotaA == null)
                    taQuotaA = kind2quotaFromDirectionMap.get(taKind.getId());

                if (taQuotaA == null) taQuotaA = 0;

                Integer taQuotaB = kind2quotaFromPreStudentMap.get(taKind.getId());

                if (taQuotaB == null) taQuotaB = 0;

                int taQuota = taQuotaA - taQuotaB;

                if (taQuota < 0) taQuota = 0;

                // сохраняем итоговый план приема по виду цп
                taQuotaResultMap.put(taKind.getId(), taQuota);
            }
        }

        return new EcgQuotaDTO(taKindList, quotaMap, taQuotaMap);
    }

    @Override
    public IEcgQuotaDTO getNextQuotaDTO(EcgConfigDTO configDTO)
    {
        // получаем все интересующие нас направления
        List<EnrollmentDirection> directionList = getDistributionDirectionList(configDTO);

        // получаем все интересующие нас виды цп
        List<TargetAdmissionKind> taKindList = getDistributionTaKindList(configDTO);

        // все нпп
        Set<EducationOrgUnit> educationOrgUnitSet = new HashSet<>();
        for (EnrollmentDirection enrollmentDirection : directionList)
            educationOrgUnitSet.add(enrollmentDirection.getEducationOrgUnit());

        // планы приема по направлениям из распределения первой волны
        Map<Long, Integer> direction2quotaMap = new HashMap<>();

        // планы приема по видам цп по направлениям из распределения первой волны
        Map<Long, Map<Long, Integer>> direction2kind2quotaMap = new HashMap<>();

        // конфигурация распределения
        EcgDistributionConfig config = getByNaturalId(new EcgDistributionConfigGen.NaturalId(configDTO.getEcgItem(), configDTO.getCompensationType(), configDTO.isSecondHighAdmission()));

        if (config != null)
        {
            EcgDistribution distribution = getByNaturalId(new EcgDistributionGen.NaturalId(config, DISTRIBUTION_FIRST_WAVE)); // первая волна

            if (distribution != null)
            {
                List<EcgDistributionQuota> quotaList = getList(EcgDistributionQuota.class, EcgDistributionQuota.distribution(), distribution);

                for (EcgDistributionQuota quota : quotaList)
                    direction2quotaMap.put(quota.getDirection().getId(), quota.getQuota());

                List<EcgDistributionTAQuota> taQuotaList = getList(EcgDistributionTAQuota.class, EcgDistributionTAQuota.distributionQuota(), quotaList);

                for (EcgDistributionTAQuota taQuota : taQuotaList)
                {
                    Long directionId = taQuota.getDistributionQuota().getDirection().getId();

                    Map<Long, Integer> kind2quotaMap = direction2kind2quotaMap.get(directionId);

                    if (kind2quotaMap == null)
                        direction2kind2quotaMap.put(directionId, kind2quotaMap = new HashMap<>());

                    kind2quotaMap.put(taQuota.getTargetAdmissionKind().getId(), taQuota.getQuota());
                }
            }
        }

        // планы приема по видам цп из направлений приема
        Map<Long, Map<Long, Integer>> direction2kind2planMap = new HashMap<>();

        for (TargetAdmissionPlanRelation planRelation : new DQLSelectBuilder().fromEntity(TargetAdmissionPlanRelation.class, "e").column("e")
        .where(in(property(TargetAdmissionPlanRelation.entranceEducationOrgUnit().fromAlias("e")), directionList))
        .where(eq(property(TargetAdmissionPlanRelation.budget().fromAlias("e")), value(configDTO.getCompensationType().isBudget())))
        .createStatement(getSession()).<TargetAdmissionPlanRelation>list())
        {
            Long directionId = planRelation.getEntranceEducationOrgUnit().getId();
            Map<Long, Integer> map = direction2kind2planMap.get(directionId);
            if (map == null)
                direction2kind2planMap.put(directionId, map = new HashMap<>());
            map.put(planRelation.getTargetAdmissionKind().getId(), planRelation.getPlanValue());
        }

        // нпп -> кол-во предзачисленных
        Map<Long, Integer> ou2countMap = new HashMap<>();

        // нпп -> вид цп -> кол-во предзачисленных
        Map<Long, Map<Long, Integer>> ou2kind2countMap = new HashMap<>();

        // вид цп по умолчанию (на случай когда зачислен по цп, а в выбранном направлении приема нет вида цп)
        TargetAdmissionKind defaultTargetAdmissionKind = getByNaturalId(new TargetAdmissionKindGen.NaturalId(TargetAdmissionKindCodes.TA_DEFAULT));

        for (PreliminaryEnrollmentStudent preStudent : new DQLSelectBuilder().fromEntity(PreliminaryEnrollmentStudent.class, "e").column("e")
        .where(eq(property(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().enrollmentDirection().enrollmentCampaign().fromAlias("e")), value(configDTO.getEcgItem().getEnrollmentCampaign())))
        .where(in(property(PreliminaryEnrollmentStudent.educationOrgUnit().fromAlias("e")), educationOrgUnitSet))
        .where(eq(property(PreliminaryEnrollmentStudent.compensationType().fromAlias("e")), value(configDTO.getCompensationType())))
        .where(in(property(PreliminaryEnrollmentStudent.studentCategory().code().fromAlias("e")), configDTO.isSecondHighAdmission() ? Collections.singletonList(UniDefines.STUDENT_CATEGORY_SECOND_HIGH) : Arrays.asList(UniDefines.STUDENT_CATEGORY_STUDENT, UniDefines.STUDENT_CATEGORY_LISTENER)))
        .createStatement(getSession()).<PreliminaryEnrollmentStudent>list())
        {
            Long ouId = preStudent.getEducationOrgUnit().getId();

            Integer plan = ou2countMap.get(ouId);
            ou2countMap.put(ouId, plan == null ? 1 : plan + 1);

            if (preStudent.isTargetAdmission())
            {
                // целевик
                Map<Long, Integer> kind2countMap = ou2kind2countMap.get(ouId);
                if (kind2countMap == null)
                    ou2kind2countMap.put(ouId, kind2countMap = new HashMap<>());
                TargetAdmissionKind kind = preStudent.getRequestedEnrollmentDirection().getTargetAdmissionKind();
                if (kind == null) kind = defaultTargetAdmissionKind;
                plan = kind2countMap.get(kind.getId());
                kind2countMap.put(kind.getId(), plan == null ? 1 : plan + 1);
            }
        }

        // итоговые планы приема по направлениям приема
        Map<Long, Integer> quotaMap = new HashMap<>();

        // итоговые планы приема по видам цп по направлениям приема
        Map<Long, Map<Long, Integer>> taQuotaMap = new HashMap<>();

        for (EnrollmentDirection direction : directionList)
        {
            Long directionId = direction.getId();
            Long ouId = direction.getEducationOrgUnit().getId();

            // ПЛАНЫ ПРИЕМА ПО НАПРАВЛЕНИЮ

            // получаем план приема по направлению из распределения первой волны
            Integer quotaA = direction2quotaMap.get(directionId);

            // если план приема нельзя определить (нет распределения первой волны), то берем план приема из направления приема с учетом вида затрат
            if (quotaA == null)
                quotaA = configDTO.getCompensationType().isBudget() ? direction.getMinisterialPlan() : direction.getContractPlan();

                if (quotaA == null) quotaA = 0;

                // получаем кол-во предзачисленных
                Integer quotaB = ou2countMap.get(ouId);

                if (quotaB == null) quotaB = 0;

                int quota = quotaA - quotaB;

                if (quota < 0) quota = 0;

                // сохраняем итоговый план по направлению
                quotaMap.put(directionId, quota);

                // ПЛАНЫ ПРИЕМА ПО НАПРАВЛЕНИЮ ПО ВИДАМ ЦП

                // получаем планы приема по видам цп из распределения первой волны
                Map<Long, Integer> kind2quotaMap = direction2kind2quotaMap.get(directionId);
                if (kind2quotaMap == null) kind2quotaMap = Collections.emptyMap();

                // получаем планы приема по видам цп из направлений приема
                Map<Long, Integer> kind2quotaFromDirectionMap = direction2kind2planMap.get(direction.getId());
                if (kind2quotaFromDirectionMap == null) kind2quotaFromDirectionMap = Collections.emptyMap();

                // получаем планы приема по видам цп из предзачисленных
                Map<Long, Integer> kind2quotaFromPreStudentMap = ou2kind2countMap.get(ouId);
                if (kind2quotaFromPreStudentMap == null) kind2quotaFromPreStudentMap = Collections.emptyMap();

                // итоговые планы приема по видам цп
                Map<Long, Integer> taQuotaResultMap = taQuotaMap.get(directionId);
                if (taQuotaResultMap == null)
                    taQuotaMap.put(directionId, taQuotaResultMap = new HashMap<>());

                for (TargetAdmissionKind taKind : taKindList)
                {
                    // получаем план приема по виду цп из первого распределения
                    Integer taQuotaA = kind2quotaMap.get(taKind.getId());

                    // если план приема нельзя определить, то берем план приема из направления приема с учетом вида затрат
                    if (taQuotaA == null)
                        taQuotaA = kind2quotaFromDirectionMap.get(taKind.getId());

                    if (taQuotaA == null) taQuotaA = 0;

                    Integer taQuotaB = kind2quotaFromPreStudentMap.get(taKind.getId());

                    if (taQuotaB == null) taQuotaB = 0;

                    int taQuota = taQuotaA - taQuotaB;

                    if (taQuota < 0) taQuota = 0;

                    // сохраняем итоговый план приема по виду цп
                    taQuotaResultMap.put(taKind.getId(), taQuota);
                }
        }

        return new EcgQuotaDTO(taKindList, quotaMap, taQuotaMap);
    }

    @Override
    public IEcgQuotaDTO getCurrentQuotaDTO(EcgDistribution distribution)
    {
        List<EcgDistributionTAQuota> taQuotaList = getDistributionTAQuotaList(distribution.getId());

        // итоговые планы приема по направлениям приема
        Map<Long, Integer> quotaMap = new HashMap<>();

        // итоговые планы приема по видам цп по направлениям приема
        Map<Long, Map<Long, Integer>> taQuotaMap = new HashMap<>();

        Set<TargetAdmissionKind> taKindSet = new HashSet<>();
        Set<Long> usedQuotaIds = new HashSet<>();

        for (EcgDistributionTAQuota taQuota : taQuotaList)
        {
            taKindSet.add(taQuota.getTargetAdmissionKind());

            EcgDistributionQuota quota = taQuota.getDistributionQuota();

            Long directionId = quota.getDirection().getId();

            if (usedQuotaIds.add(quota.getId()))
                quotaMap.put(directionId, quota.getQuota());

            Map<Long, Integer> map = taQuotaMap.get(directionId);
            if (map == null)
                taQuotaMap.put(directionId, map = new HashMap<>());
            map.put(taQuota.getTargetAdmissionKind().getId(), taQuota.getQuota());
        }

        List<TargetAdmissionKind> taKindList = new ArrayList<>(taKindSet);
        Collections.sort(taKindList, TargetAdmissionKind.ALL_LEVEL_PRIORITY_COMPARATOR);

        return new EcgQuotaDTO(taKindList, quotaMap, taQuotaMap);
    }

    @Override
    public IEcgQuotaUsedDTO getUsedQuotaDTO(IEcgDistribution distribution)
    {
        // занятые места по направлениям приема
        Map<Long, Integer> usedMap = new HashMap<>();

        // занятые места по видам цп по направлениям приема
        Map<Long, Map<Long, Integer>> taUsedMap = new HashMap<>();

        List<? extends IEcgEntrantRecommended> list;

        if (distribution instanceof EcgDistribution)
            list = new DQLSelectBuilder().fromEntity(EcgEntrantRecommended.class, "e").column("e")
            .where(eq(property(EcgEntrantRecommended.distribution().fromAlias("e")), value(distribution)))
            .createStatement(getSession()).list();
        else
            list = new DQLSelectBuilder().fromEntity(EcgEntrantRecommendedDetail.class, "e").column("e")
            .where(eq(property(EcgEntrantRecommendedDetail.distribution().fromAlias("e")), value(distribution)))
            .createStatement(getSession()).list();

        for (IEcgEntrantRecommended entrantRecommended : list)
        {
            // рекомендован по этому направлению приема
            Long directionId = entrantRecommended.getDirection().getEnrollmentDirection().getId();

            // рекомендован по этому виду цп
            TargetAdmissionKind kind = entrantRecommended.getTargetAdmissionKind();

            Integer used = usedMap.get(directionId);
            usedMap.put(directionId, used == null ? 1 : used + 1);

            if (kind != null)
            {
                // рекомендован по цп
                Map<Long, Integer> taUsedSubMap = taUsedMap.get(directionId);
                if (taUsedSubMap == null)
                    taUsedMap.put(directionId, taUsedSubMap = new HashMap<>());
                used = taUsedSubMap.get(kind.getId());
                taUsedSubMap.put(kind.getId(), used == null ? 1 : used + 1);
            }
        }

        // теперь не учитываем сутд пред. зачисления, т.к. могут получиться отрицательные цифры в Св. мастах (DEV-3414)
//        if (distribution instanceof EcgDistributionDetail)
//        {
//            // id абитуриентов из основного распределения имеющие предзачисление
//            IDQLSelectableQuery preStudentQuery = getPreStudentQuery(distribution);
//
//            // id абитуриентов из основного распределения имеющие оригиналы документов
//            IDQLSelectableQuery reserveQuery = new DQLSelectBuilder().fromEntity(EcgEntrantRecommendedState.class, "r")
//            .column(property(EcgEntrantRecommendedState.recommended().direction().entrantRequest().entrant().id().fromAlias("r")))
//            .where(eq(property(EcgEntrantRecommendedState.distribution().fromAlias("r")), value(distribution.getDistribution())))
//            .where(eq(property(EcgEntrantRecommendedState.bringOriginal().fromAlias("r")), value(Boolean.TRUE)))
//            .buildQuery();
//
//            // id НПП студентов предзачисления
//            IDQLSelectableQuery ouQuery = new DQLSelectBuilder().fromEntity(PreliminaryEnrollmentStudent.class, "e")
//            .column(property(PreliminaryEnrollmentStudent.educationOrgUnit().id().fromAlias("e")))
//            .where(in(property(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().entrantRequest().entrant().id().fromAlias("e")), preStudentQuery))
//            .where(in(property(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().entrantRequest().entrant().id().fromAlias("e")), reserveQuery))
//            .buildQuery();
//
//            List<Long> ids = new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, "dir")
//            .column(property(EnrollmentDirection.id().fromAlias("dir")))
//            .where(in(property(EnrollmentDirection.educationOrgUnit().id().fromAlias("dir")), ouQuery))
//            .where(in(property(EnrollmentDirection.id().fromAlias("dir")), new DQLSelectBuilder().fromEntity(EcgDistributionQuota.class, "q2")
//                .column(property(EcgDistributionQuota.direction().id().fromAlias("q2")))
//                .where(eq(property(EcgDistributionQuota.distribution().fromAlias("q2")), value(distribution.getDistribution())))
//                .buildQuery()
//            )).createStatement(getSession()).list();
//
//            for (Long directionId : ids)
//            {
//                Integer used = usedMap.get(directionId);
//                usedMap.put(directionId, used == null ? 1 : used + 1);
//            }
//        }

        return new EcgQuotaUsedDTO(usedMap, taUsedMap);
    }

    @Override
    public IEcgQuotaFreeDTO getFreeQuotaDTO(IEcgDistribution distribution)
    {
        IEcgQuotaDTO quotaDTO = getCurrentQuotaDTO(distribution.getDistribution());

        IEcgQuotaUsedDTO quotaUsedDTO = getUsedQuotaDTO(distribution);

        Map<Long, Integer> freeMap = new HashMap<>();

        Map<Long, Map<Long, Integer>> taFreeMap = new HashMap<>();

        for (Map.Entry<Long, Integer> entry : quotaDTO.getQuotaMap().entrySet())
        {
            Long directionId = entry.getKey();

            int quota = entry.getValue();
            Integer quotaUsed = quotaUsedDTO.getUsedMap().get(directionId);
            freeMap.put(directionId, quota - (quotaUsed == null ? 0 : quotaUsed));

            Map<Long, Integer> taFreeSubMap = new HashMap<>();

            for (Map.Entry<Long, Integer> taEntry : quotaDTO.getTaQuotaMap().get(directionId).entrySet())
            {
                Long taKindId = taEntry.getKey();

                Map<Long, Integer> map = quotaUsedDTO.getTaUsedMap().get(directionId);
                if (map == null) map = Collections.emptyMap();

                int taQuota = taEntry.getValue();
                Integer taQuotaUsed = map.get(taKindId);
                taFreeSubMap.put(taKindId, taQuota - (taQuotaUsed == null ? 0 : taQuotaUsed));
            }

            taFreeMap.put(directionId, taFreeSubMap);
        }

        return new EcgQuotaFreeDTO(quotaDTO, quotaUsedDTO, freeMap, taFreeMap);
    }

    @SuppressWarnings("deprecation")
    @Override
    public List<? extends IEcgEntrantRateRowDTO> getEntrantRateRowList(IEcgDistribution distribution, List<TargetAdmissionKind> usedTaKindList, IEcgEntrantRateRowDTO.Rule rateRule)
    {
        Map<Long, TargetAdmissionKind> taKindMap = new HashMap<>();
        for (TargetAdmissionKind item : usedTaKindList)
            taKindMap.put(item.getId(), item);

        final Map<CompetitionKind, Integer> competitionKindPriorityMap = UniecDAOFacade.getSettingsDAO().getCompetitionKindPriorities(distribution.getConfig().getEcgItem().getEnrollmentCampaign());

        Map<Long, CompetitionKind> coKindMap = new HashMap<>();
        for (CompetitionKind item : competitionKindPriorityMap.keySet())
            if (item != null)
                coKindMap.put(item.getId(), item);

        DQLSelectBuilder builder = distribution instanceof EcgDistribution ? getDistributionRateBuilder(true, (EcgDistribution) distribution) : getDistributionDetailRateBuilder((EcgDistributionDetail) distribution, rateRule);

        boolean targetAdmission = rateRule.equals(IEcgEntrantRateRowDTO.Rule.RULE_TARGET_ADMISSION);

        // учитываем правило выборки строк рейтинга
        switch (rateRule)
        {
            case RULE_TARGET_ADMISSION:
                // по целевому приему
                builder.where(eq(property(RequestedEnrollmentDirection.targetAdmission().fromAlias("e")), value(Boolean.TRUE)));
                break;
            case RULE_NON_COMPETITIVE:
                // не по общим основаниям (не по конкурсному приему)
                builder.where(ne(property(RequestedEnrollmentDirection.competitionKind().fromAlias("e")), commonValue(getByNaturalId(new CompetitionKindGen.NaturalId(UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION)))));
                break;
            case RULE_COMPETITIVE:
                // по общим основаниям (по конкурсному приему)
                builder.where(eq(property(RequestedEnrollmentDirection.competitionKind().fromAlias("e")), commonValue(getByNaturalId(new CompetitionKindGen.NaturalId(UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION)))));
                break;
            case RULE_COMPETITIVE_WITH_ORIGINAL:
                // добавляются по общим основаниям (по конкурсному приему) с оригиналами
                builder.where(eq(property(RequestedEnrollmentDirection.competitionKind().fromAlias("e")), commonValue(getByNaturalId(new CompetitionKindGen.NaturalId(UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION)))));
                builder.where(eq(property(RequestedEnrollmentDirection.originalDocumentHandedIn().fromAlias("e")), value(Boolean.TRUE)));
                break;
            default:
                throw new IllegalArgumentException("Unknown rateRule: " + rateRule);
        }

        builder.joinPath(DQLJoinType.inner, RequestedEnrollmentDirection.entrantRequest().fromAlias("e"), "req");
        builder.joinPath(DQLJoinType.inner, RequestedEnrollmentDirection.competitionKind().fromAlias("e"), "com");
        builder.joinPath(DQLJoinType.left, RequestedEnrollmentDirection.targetAdmissionKind().fromAlias("e"), "tar");
        builder.joinPath(DQLJoinType.inner, RequestedEnrollmentDirection.enrollmentDirection().fromAlias("e"), "dir");
        builder.joinPath(DQLJoinType.inner, EnrollmentDirection.educationOrgUnit().fromAlias("dir"), "ou");
        builder.joinPath(DQLJoinType.inner, EducationOrgUnit.educationLevelHighSchool().fromAlias("ou"), "hs");
        builder.joinPath(DQLJoinType.inner, EntrantRequest.entrant().fromAlias("req"), "ent");
        builder.joinPath(DQLJoinType.inner, Entrant.person().fromAlias("ent"), "per");
        builder.joinPath(DQLJoinType.inner, Person.identityCard().fromAlias("per"), "card");
        builder.joinPath(DQLJoinType.left, Person.personEduInstitution().fromAlias("per"), "edu");
        builder.joinPath(DQLJoinType.left, RequestedEnrollmentDirection.profileChosenEntranceDiscipline().fromAlias("e"), "pcd");

        builder.joinEntity("e", DQLJoinType.left, ChosenEntranceDiscipline.class, "c",
            eq(property(RequestedEnrollmentDirection.id().fromAlias("e")), property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("c")))
        );

        final int REQUESTED_ENROLLMENT_DIRECTION_ID_IX = 0;
        final int ENROLLMENT_DIRECTION_ID_IX = 1;
        final int REQUEST_NUMBER_IX = 2;
        final int PRIORITY_NUMBER_IX = 3;
        final int GRADUATED_PROFILE_EDU_INSTITUTION_IX = 4;
        final int ORIGINAL_DOCUMENT_HANDED_IN_IX = 5;
        final int COMPETITION_KIND_IX = 6;
        final int SHORT_TITLE_IX = 7;
        final int TARGET_ADMISSION_KIND_IX = 8;
        final int ENTRANT_ID_IX = 9;
        final int FULL_FIO_IX = 10;
        final int MARK3_IX = 11;
        final int MARK4_IX = 12;
        final int MARK5_IX = 13;
        final int PROFILE_MARK_IX = 14;
        final int FINAL_MARK_IX = 15;

        builder.group(property(RequestedEnrollmentDirection.id().fromAlias("e")));
        builder.group(property(EnrollmentDirection.id().fromAlias("dir")));
        builder.group(property(EntrantRequest.regNumber().fromAlias("req")));
        builder.group(property(RequestedEnrollmentDirection.priority().fromAlias("e")));
        builder.group(property(RequestedEnrollmentDirection.graduatedProfileEduInstitution().fromAlias("e")));
        builder.group(property(RequestedEnrollmentDirection.originalDocumentHandedIn().fromAlias("e")));
        builder.group(property(CompetitionKind.id().fromAlias("com")));
        builder.group(property(EducationLevelsHighSchool.shortTitle().fromAlias("hs")));
        builder.group(property(TargetAdmissionKind.id().fromAlias("tar")));
        builder.group(property(Entrant.id().fromAlias("ent")));
        builder.group(property(IdentityCard.fullFio().fromAlias("card")));
        builder.group(property(PersonEduInstitution.mark3().fromAlias("edu")));
        builder.group(property(PersonEduInstitution.mark4().fromAlias("edu")));
        builder.group(property(PersonEduInstitution.mark5().fromAlias("edu")));
        builder.group(property(ChosenEntranceDiscipline.finalMark().fromAlias("pcd")));

        builder.column(property(RequestedEnrollmentDirection.id().fromAlias("e")));
        builder.column(property(EnrollmentDirection.id().fromAlias("dir")));
        builder.column(property(EntrantRequest.regNumber().fromAlias("req")));
        builder.column(property(RequestedEnrollmentDirection.priority().fromAlias("e")));
        builder.column(property(RequestedEnrollmentDirection.graduatedProfileEduInstitution().fromAlias("e")));
        builder.column(property(RequestedEnrollmentDirection.originalDocumentHandedIn().fromAlias("e")));
        builder.column(property(CompetitionKind.id().fromAlias("com")));
        builder.column(property(EducationLevelsHighSchool.shortTitle().fromAlias("hs")));
        builder.column(property(TargetAdmissionKind.id().fromAlias("tar")));
        builder.column(property(Entrant.id().fromAlias("ent")));
        builder.column(property(IdentityCard.fullFio().fromAlias("card")));
        builder.column(property(PersonEduInstitution.mark3().fromAlias("edu")));
        builder.column(property(PersonEduInstitution.mark4().fromAlias("edu")));
        builder.column(property(PersonEduInstitution.mark5().fromAlias("edu")));
        builder.column(property(ChosenEntranceDiscipline.finalMark().fromAlias("pcd")));

        builder.column(DQLFunctions.sum(property(ChosenEntranceDiscipline.finalMark().fromAlias("c"))));

        List<Object[]> dataRowList = builder.createStatement(getSession()).list();

        TargetAdmissionKind defaultTaKind = getByNaturalId(new TargetAdmissionKindGen.NaturalId(TargetAdmissionKindCodes.TA_DEFAULT));

        Map<Long, Map<TargetAdmissionKind, Map<CompetitionKind, List<Object[]>>>> preRateMap = new HashMap<>();

        // группируем все выбранные направления по абитуриенту, внутри абитуриента по виду цп, внутри вида цп по виду конкурса
        for (Object[] dataRow : dataRowList)
        {
            long entrantId = (Long) dataRow[ENTRANT_ID_IX];

            TargetAdmissionKind taKind = null;
            if (targetAdmission)
            {
                Long targetAdmissionKindId = (Long) dataRow[TARGET_ADMISSION_KIND_IX];
                taKind = targetAdmissionKindId == null ? defaultTaKind : taKindMap.get(targetAdmissionKindId);
                // если такого вида цп нет в планах приема, то вообще исключаем такое выбранное направление приема
                if (taKind == null) continue;
            }

            Map<TargetAdmissionKind, Map<CompetitionKind, List<Object[]>>> taMap = preRateMap.get(entrantId);
            if (taMap == null)
                preRateMap.put(entrantId, taMap = new HashMap<>());

            Map<CompetitionKind, List<Object[]>> coMap = taMap.get(taKind);
            if (coMap == null)
                taMap.put(taKind, coMap = new HashMap<>());

            long competitionKindId = (Long) dataRow[COMPETITION_KIND_IX];
            CompetitionKind competitionKind = coKindMap.get(competitionKindId);

            List<Object[]> list = coMap.get(competitionKind);
            if (list == null)
                coMap.put(competitionKind, list = new ArrayList<>());

            list.add(dataRow);
        }

        Comparator<CompetitionKind> competitionKindComparator = new Comparator<CompetitionKind>()
        {
            @Override
            public int compare(CompetitionKind o1, CompetitionKind o2)
            {
                return competitionKindPriorityMap.get(o1) - competitionKindPriorityMap.get(o2);
            }
        };

        Comparator<Object[]> directionComparator = new Comparator<Object[]>()
        {
            @Override
            public int compare(Object[] o1, Object[] o2)
            {
                int result = ((Integer) o1[REQUEST_NUMBER_IX]).compareTo((Integer) o2[REQUEST_NUMBER_IX]);
                if (result != 0) return result;
                return ((Integer) o1[PRIORITY_NUMBER_IX]).compareTo((Integer) o2[PRIORITY_NUMBER_IX]);
            }
        };

        List<EcgEntrantRateRowDTO> resultList = new ArrayList<>();

        for (Map.Entry<Long, Map<TargetAdmissionKind, Map<CompetitionKind, List<Object[]>>>> entry : preRateMap.entrySet())
        {
            // из всех существующих находим вид цп с максимальным приоритетом
            Map<TargetAdmissionKind, Map<CompetitionKind, List<Object[]>>> taMap = entry.getValue();
            List<TargetAdmissionKind> taKindList = new ArrayList<>(taMap.keySet());
            if (taKindList.size() > 1)
                Collections.sort(taKindList, TargetAdmissionKind.ALL_LEVEL_PRIORITY_COMPARATOR);
            TargetAdmissionKind taKind = taKindList.get(0);

            // из всех существующих находим вид конкурса с максимальным приоритетом
            Map<CompetitionKind, List<Object[]>> coMap = taMap.get(taKind);
            List<CompetitionKind> coList = new ArrayList<>(coMap.keySet());
            if (coList.size() > 1)
                Collections.sort(coList, competitionKindComparator);
            CompetitionKind competitionKind = coList.get(0);

            // сортируем список выбранных направлений по рейтингу
            List<Object[]> list = coMap.get(competitionKind);
            Collections.sort(list, directionComparator);

            List<IEcgEntrantRateDirectionDTO> directionList = new ArrayList<>();

            for (Object[] row : list)
            {
                Long requestedEnrollmentDirectionId = (Long) row[REQUESTED_ENROLLMENT_DIRECTION_ID_IX];
                Long enrollmentDirectionId = (Long) row[ENROLLMENT_DIRECTION_ID_IX];
                String shortTitle = (String) row[SHORT_TITLE_IX];
                directionList.add(new EcgEntrantRateDirectionDTO(requestedEnrollmentDirectionId, enrollmentDirectionId, shortTitle));
            }

            Object[] dataRow = list.get(0);

            String fio = (String) dataRow[FULL_FIO_IX];
            boolean originalDocumentHandedIn = (Boolean) dataRow[ORIGINAL_DOCUMENT_HANDED_IN_IX];
            Double profileMark = (Double) dataRow[PROFILE_MARK_IX];
            Double finalMark = (Double) dataRow[FINAL_MARK_IX];
            boolean graduatedProfileEduInstitution = (Boolean) dataRow[GRADUATED_PROFILE_EDU_INSTITUTION_IX];

            Integer m3 = (Integer) dataRow[MARK3_IX];
            Integer m4 = (Integer) dataRow[MARK4_IX];
            Integer m5 = (Integer) dataRow[MARK5_IX];
            m3 = (m3 != null) ? m3 : 0;
            m4 = (m4 != null) ? m4 : 0;
            m5 = (m5 != null) ? m5 : 0;
            int sum = m3 + m4 + m5;
            double total = m3 * 3 + m4 * 4 + m5 * 5;
            Double averageMark = (sum != 0) ? total / sum : null;

            resultList.add(new EcgEntrantRateRowDTO(entry.getKey(), fio, finalMark == null ? 0 : finalMark, profileMark, averageMark, originalDocumentHandedIn, graduatedProfileEduInstitution, targetAdmission, taKind, competitionKind, directionList));
        }

        Collections.sort(resultList, new EcgContegeComparator(competitionKindPriorityMap));

        return resultList;
    }

    @Override
    public void saveEntrantRecommendedList(IEcgDistribution distribution, boolean targetAdmission, Collection<Long> chosenDirectionIds)
    {
        Session session = getSession();

        session.refresh(distribution);

        if (distribution.getState().isLockedOrApproved())
            throw new ApplicationException("Распределение «" + distribution.getTitle() + "» уже нельзя изменять.");

        IPersistentEcgItem ecgItem = distribution.getConfig().getEcgItem();
        EnrollmentCampaign enrollmentCampaign = ecgItem.getEnrollmentCampaign();
        Set<EnrollmentDirection> directionSet = new HashSet<>(getDistributionDirectionList(distribution.getDistribution()));

        Set<Long> usedDirectionIds;
        if (distribution instanceof EcgDistribution)
            usedDirectionIds = new HashSet<>(new DQLSelectBuilder().fromEntity(EcgEntrantRecommended.class, "e")
            .column(property(EcgEntrantRecommended.direction().id().fromAlias("e")))
            .where(eq(property(EcgEntrantRecommended.distribution().fromAlias("e")), value(distribution)))
            .createStatement(session).<Long>list());
        else
            usedDirectionIds = new HashSet<>(new DQLSelectBuilder().fromEntity(EcgEntrantRecommendedDetail.class, "e")
            .column(property(EcgEntrantRecommendedDetail.direction().id().fromAlias("e")))
            .where(eq(property(EcgEntrantRecommendedDetail.distribution().fromAlias("e")), value(distribution)))
            .createStatement(session).<Long>list());

        Set<Long> usedEntrantIds = new HashSet<>();

        List<RequestedEnrollmentDirection> directionList = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "e").column("e")
        .where(in(property(RequestedEnrollmentDirection.id().fromAlias("e")), chosenDirectionIds))
        .createStatement(session).list();

        TargetAdmissionKind defaultTargetAdmissionKind = targetAdmission ? this.<TargetAdmissionKind>getByNaturalId(new TargetAdmissionKindGen.NaturalId(TargetAdmissionKindCodes.TA_DEFAULT)) : null;

        for (RequestedEnrollmentDirection direction : directionList)
        {
            EnrollmentDirection enrollmentDirection = direction.getEnrollmentDirection();
            TargetAdmissionKind targetAdmissionKind = direction.getTargetAdmissionKind();

            // один и тот же абитуриент не должен попадать в распределение
            if (!usedEntrantIds.add(direction.getEntrantRequest().getEntrant().getId()))
                throw new RuntimeException("Duplicate entrant '" + direction.getEntrantRequest().getEntrant().getId() + "' in distribution '" + distribution.getId() + "'.");

            // должны совпадать приемные кампании
            if (!enrollmentDirection.getEnrollmentCampaign().equals(enrollmentCampaign))
                throw new RuntimeException("EnrollmentCampaign '" + enrollmentDirection.getEnrollmentCampaign() + "' is not allowed for distribution '" + distribution.getId() + "'.");

            // направление должно быть в распределении
            if (!directionSet.contains(direction.getEnrollmentDirection()))
                throw new RuntimeException("EnrollmentDirection '" + direction.getEnrollmentDirection().getId() + "' is not allowed for distribution '" + distribution.getId() + "'.");

            // если рекомендуем по целевому приему, то он должен быть задан в выбранном направлении приема
            if (targetAdmission && targetAdmissionKind == null) {
                targetAdmissionKind = defaultTargetAdmissionKind;
                // throw new ApplicationException("У абитуриента «" + direction.getEntrantRequest().getEntrant().getPerson().getFullFio() + "» по направлению «" + direction.getTitle() + "» не указан вид целевого приема.");
            }

            // направление не должно быть в этом распределении
            if (usedDirectionIds.contains(direction.getId()))
                throw new ApplicationException("Абитуриента «" + direction.getEntrantRequest().getEntrant().getPerson().getFullFio() + "» по направлению «" + direction.getTitle() + "» уже добавили в это распределение.");

            // конфигурация распределения должна поддерживать выбранное направление приема (вид затрат и категория поступающего должна быть соответствующей)
            if (!isConfigAllowDirection(distribution.getConfig(), direction))
                throw new ApplicationException("У абитуриента «" + direction.getEntrantRequest().getEntrant().getPerson().getFullFio() + "» направление «" + direction.getTitle() + "» не может быть добавлено в распределение: не подходящие вид затрат или категория поступающего.");

            if (distribution instanceof EcgDistribution)
            {
                EcgEntrantRecommended item = new EcgEntrantRecommended();
                item.setDistribution((EcgDistribution) distribution);
                item.setDirection(direction);
                if (targetAdmission)
                    item.setTargetAdmissionKind(targetAdmissionKind);
                session.save(item);
            } else
            {
                EcgEntrantRecommendedDetail item = new EcgEntrantRecommendedDetail();
                item.setDistribution((EcgDistributionDetail) distribution);
                item.setDirection(direction);
                if (targetAdmission)
                    item.setTargetAdmissionKind(targetAdmissionKind);
                item.setDirection(direction);
                session.save(item);
            }
        }
    }

    @Override
    public List<IEcgRecommendedDTO> getEntrantRecommendedRowList(IEcgDistribution distribution)
    {
        // получаем всех рекомендованных
        List<IEcgEntrantRecommended> entrantRecommendedList;

        if (distribution instanceof EcgDistribution)
            entrantRecommendedList = new DQLSelectBuilder().fromEntity(EcgEntrantRecommended.class, "e").column("e")
            .where(eq(property(EcgEntrantRecommended.distribution().fromAlias("e")), value(distribution)))
            .createStatement(getSession()).list();
        else
            entrantRecommendedList = new DQLSelectBuilder().fromEntity(EcgEntrantRecommendedDetail.class, "e").column("e")
            .where(eq(property(EcgEntrantRecommendedDetail.distribution().fromAlias("e")), value(distribution)))
            .createStatement(getSession()).list();

        // разредяем их на активных и не активных
        List<IEcgEntrantRecommended> activeList = new ArrayList<>();
        List<IEcgEntrantRecommended> nonActiveList = new ArrayList<>();
        for (IEcgEntrantRecommended recommended : entrantRecommendedList)
            (isConfigAllowDirection(distribution.getConfig(), recommended.getDirection()) ? activeList : nonActiveList).add(recommended);

        // у активных получаем id выбранных направлений приема
        List<Long> directionIds = new ArrayList<>();
        for (IEcgEntrantRecommended recommended : activeList)
            directionIds.add(recommended.getDirection().getId());

        // у активных получаем id абитуриентов
        List<Long> entrantIds = new ArrayList<>();
        for (IEcgEntrantRecommended recommended : activeList)
            entrantIds.add(recommended.getDirection().getEntrantRequest().getEntrant().getId());

        // определяем данные для колонки приоритеты - все направления рекомендованного абитуриента, удовлетворяющие конфигурации распределения
        Map<Long, EcgPriorityInfo> entrantPriorityInfoMap = getEntrantPriorityMap(distribution, entrantIds);
        Map<Long, MutableDouble> finalMarkMap = new HashMap<>();

        final int REQUESTED_ENROLLMENT_DIRECTION_ID_IX = 0;
        final int FINAL_MARK_IX = 1;
        for (Object[] dataRow : new DQLSelectBuilder().fromEntity(ChosenEntranceDiscipline.class, "e")
        .column(property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("e")))
        .column(property(ChosenEntranceDiscipline.finalMark().fromAlias("e")))
        .where(in(property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("e")), directionIds))
        .where(isNotNull(ChosenEntranceDiscipline.finalMark().fromAlias("e")))
        .createStatement(getSession()).<Object[]>list())
        {
            Long requestedEnrollmentDirectionId = (Long) dataRow[REQUESTED_ENROLLMENT_DIRECTION_ID_IX];
            Double finalMark = (Double) dataRow[FINAL_MARK_IX];
            MutableDouble mutable = finalMarkMap.get(requestedEnrollmentDirectionId);
            if (mutable == null)
                finalMarkMap.put(requestedEnrollmentDirectionId, mutable = new MutableDouble());
            mutable.add(finalMark);
        }

        // каждому активному рекомендованному можно сопоставить строку рейтинга
        List<IEcgRecommendedDTO> rateList = new ArrayList<>();

        // для основного утвержденного распределения нужно дополнительно загрузить информацию о сданных оригиналах для рекомендованных
        Map<EcgEntrantRecommended, Boolean> recommendedBringOriginalMap = null;
        if (distribution instanceof EcgDistribution && distribution.getState().isApproved())
        {
            recommendedBringOriginalMap = new HashMap<>();

            List<EcgEntrantRecommendedState> stateList = new DQLSelectBuilder().fromEntity(EcgEntrantRecommendedState.class, "e")
            .where(eq(property(EcgEntrantRecommendedState.distribution().fromAlias("e")), value(distribution)))
            .where(eq(property(EcgEntrantRecommendedState.recommended().distribution().fromAlias("e")), value(distribution)))
            .createStatement(getSession()).list();

            for (EcgEntrantRecommendedState item : stateList)
                recommendedBringOriginalMap.put(item.getRecommended(), item.getBringOriginal());
        }

        for (IEcgEntrantRecommended recommended : activeList)
        {
            EcgPriorityInfo ecgPriorityInfo = entrantPriorityInfoMap.get(recommended.getDirection().getEntrantRequest().getEntrant().getId());
            String priorities = ecgPriorityInfo == null || ecgPriorityInfo.getPriorityList() == null ? null : StringUtils.join(ecgPriorityInfo.getPriorityList(), ", ");
            MutableDouble finalMark = finalMarkMap.get(recommended.getDirection().getId());

            IEcgRecommendedDTO.DocumentStatus documentStatus;
            if (distribution.getState().isApproved())
            {
                Boolean bringOriginal;

                if (recommended instanceof EcgEntrantRecommended)
                {
                    // рекомендован из основного
                    EcgEntrantRecommended recommendedFromGeneral = (EcgEntrantRecommended) recommended;

                    // состояние берем на момент утверждения
                    bringOriginal = recommendedBringOriginalMap.get(recommendedFromGeneral);
                }
                else
                {
                    // рекомендован из уточняющего
                    EcgEntrantRecommendedDetail recommendedFromDetail = (EcgEntrantRecommendedDetail) recommended;

                    // состояние берем на момент утверждения
                    bringOriginal = recommendedFromDetail.getBringOriginal();
                }

                documentStatus = bringOriginal == null ? IEcgRecommendedDTO.DocumentStatus.TOOK_AWAY : (bringOriginal ? IEcgRecommendedDTO.DocumentStatus.BRING_ORIGINAL : IEcgRecommendedDTO.DocumentStatus.BRING_COPY);
            } else
            {
                // состояние берем на текущий момент
                documentStatus = recommended.getDirection().getState().getCode().equals(UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY) ? IEcgRecommendedDTO.DocumentStatus.TOOK_AWAY : (recommended.getDirection().isOriginalDocumentHandedIn() ? IEcgRecommendedDTO.DocumentStatus.BRING_ORIGINAL : IEcgRecommendedDTO.DocumentStatus.BRING_COPY);
            }

            rateList.add(new EcgRecommendedDTO(recommended, finalMark == null ? 0.0 : finalMark.doubleValue(), priorities, documentStatus));
        }

        // строки рейтинга активных рекомендованных сортируем стандартно
        rateList.sort(new EcgContegeComparator(UniecDAOFacade.getSettingsDAO().getCompetitionKindPriorities(distribution.getConfig().getEcgItem().getEnrollmentCampaign())));

        // неактивных рекомендованных сортируем по фио
        nonActiveList.sort(CommonCollator.comparing(o -> o.getDirection().getEntrantRequest().getEntrant().getFullFio(), true));

        // к списку рекомендованных добавляем всех неактивных рекомендованных
        for (IEcgEntrantRecommended recommended : nonActiveList)
            rateList.add(new EcgRecommendedDTO(recommended, null, null, null));

        return rateList;
    }

    @Override
    public List<IEcgRecommendedPreStudentDTO> getEntrantRecommendedPreStudentRowList(IEcgDistribution distribution)
    {
        Session session = getSession();

        session.refresh(distribution);

        if (!distribution.getState().isApproved())
            throw new ApplicationException("Распределение «" + distribution.getTitle() + "» должно быть утверждено.");

        boolean canEnroll = (distribution instanceof EcgDistribution && distribution.getConfig().getEcgItem() instanceof EnrollmentDirection) ||
        (distribution instanceof EcgDistributionDetail && distribution.getConfig().getEcgItem() instanceof CompetitionGroup);

        if (!canEnroll)
            throw new ApplicationException("Для распределения «" + distribution.getTitle() + "» не выполнены условия, разрешающие предварительное зачисление.");

        List<IEcgRecommendedDTO> recommendedDTOList = getEntrantRecommendedRowList(distribution);

        // получаем направления у рекомендованных, кто сдал оригиналы
        List<RequestedEnrollmentDirection> directionList = new ArrayList<>();
        for (IEcgRecommendedDTO item : recommendedDTOList)
            if (IEcgRecommendedDTO.DocumentStatus.BRING_ORIGINAL.equals(item.getDocumentStatus()))
                directionList.add(item.getEntrantRecommended().getDirection());

        // получаем предзачисления по указанным направлениям
        List<PreliminaryEnrollmentStudent> preStudentList = new DQLSelectBuilder().fromEntity(PreliminaryEnrollmentStudent.class, "e").column("e")
        .where(in(property(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().fromAlias("e")), directionList))
        .createStatement(session).list();

        Map<RequestedEnrollmentDirection, PreliminaryEnrollmentStudent> directionPreStudentMap = new HashMap<>();
        for (PreliminaryEnrollmentStudent preStudent : preStudentList)
            directionPreStudentMap.put(preStudent.getRequestedEnrollmentDirection(), preStudent);

        // получаем приказы по указаным предзачислениям
        List<EnrollmentExtract> extractList = new DQLSelectBuilder().fromEntity(EnrollmentExtract.class, "e").column("e")
        .where(in(property(EnrollmentExtract.entity().fromAlias("e")), preStudentList))
        .createStatement(session).list();

        Map<PreliminaryEnrollmentStudent, EnrollmentOrder> preStudentOrderMap = new HashMap<>();
        for (EnrollmentExtract extract : extractList)
            preStudentOrderMap.put(extract.getEntity(), extract.getOrder());

        List<IEcgRecommendedPreStudentDTO> result = new ArrayList<>();
        for (IEcgRecommendedDTO item : recommendedDTOList)
        {
            PreliminaryEnrollmentStudent preStudent = directionPreStudentMap.get(item.getEntrantRecommended().getDirection());
            EnrollmentOrder enrollmentOrder = preStudent == null ? null : preStudentOrderMap.get(preStudent);
            String orderNumber = enrollmentOrder == null ? null : (enrollmentOrder.getNumber() == null ? "" : enrollmentOrder.getNumber());
            result.add(new EcgRecommendedPreStudentDTO(item.getEntrantRecommended(), item.getFinalMark(), item.getPriorities(), item.getDocumentStatus(), preStudent, orderNumber));
        }
        return result;
    }

    @Override
    public void deleteEntrantRecommended(IEcgEntrantRecommended entrantRecommended)
    {
        Session session = getSession();

        IEcgDistribution distribution = entrantRecommended.getDistribution();

        session.refresh(distribution);

        if (distribution.getState().isLockedOrApproved())
            throw new ApplicationException("Распределение «" + distribution.getTitle() + "» уже нельзя изменять.");

        delete(entrantRecommended);
    }

    @Override
    public void doDistributionFill(EcgDistribution distribution)
    {
        Session session = getSession();

        session.refresh(distribution);

        if (distribution.getState().isLockedOrApproved())
            throw new ApplicationException("Распределение «" + distribution.getTitle() + "» уже нельзя изменять.");

        // свободные места в распределении
        IEcgQuotaFreeDTO freeQuotaDTO = getFreeQuotaDTO(distribution);

        // получаем используемые виды цп
        List<TargetAdmissionKind> targetAdmissionKindList = freeQuotaDTO.getQuotaDTO().getTaKindList();

        // алгоритм заполнения планов
        IEcgQuotaManager quotaManager = new EcgQuotaManager(freeQuotaDTO);

        // абитуриенты рекомендуемые по цп
        List<Long> taDirectionList = new ArrayList<>();

        // абитуриенты рекомендуемые не по цп
        List<Long> directionList = new ArrayList<>();

        // уже рекомендованные абитуриенты
        Set<Long> usedEntrantIds = new HashSet<>();

        // добавляем по цп
        accumulateRateRow(distribution, targetAdmissionKindList, usedEntrantIds, quotaManager, IEcgEntrantRateRowDTO.Rule.RULE_TARGET_ADMISSION, taDirectionList);

        // добавляем не по общим основаниям
        accumulateRateRow(distribution, targetAdmissionKindList, usedEntrantIds, quotaManager, IEcgEntrantRateRowDTO.Rule.RULE_NON_COMPETITIVE, directionList);

        // добавляем по общим основаниям
        accumulateRateRow(distribution, targetAdmissionKindList, usedEntrantIds, quotaManager, IEcgEntrantRateRowDTO.Rule.RULE_COMPETITIVE, directionList);

        // сохраняем рекомендованных по цп
        saveEntrantRecommendedList(distribution, true, taDirectionList);

        // сохраняем рекомендованных не по цп
        saveEntrantRecommendedList(distribution, false, directionList);
    }

    @Override
    public void doDistributionFillWithOriginal(EcgDistribution distribution)
    {
        Session session = getSession();

        session.refresh(distribution);

        if (distribution.getState().isLockedOrApproved())
            throw new ApplicationException("Распределение «" + distribution.getTitle() + "» уже нельзя изменять.");

        // свободные места в распределении
        IEcgQuotaFreeDTO freeQuotaDTO = getFreeQuotaDTO(distribution);

        // получаем используемые виды цп
        List<TargetAdmissionKind> targetAdmissionKindList = freeQuotaDTO.getQuotaDTO().getTaKindList();

        // алгоритм заполнения планов
        IEcgQuotaManager quotaManager = new EcgQuotaManager(freeQuotaDTO);

        // абитуриенты рекомендуемые не по цп
        List<Long> directionList = new ArrayList<>();

        // уже рекомендованные абитуриенты
        Set<Long> usedEntrantIds = new HashSet<>();

        // добавляем по общим основаниям с оригиналами документов
        accumulateRateRow(distribution, targetAdmissionKindList, usedEntrantIds, quotaManager, IEcgEntrantRateRowDTO.Rule.RULE_COMPETITIVE_WITH_ORIGINAL, directionList);

        // сохраняем рекомендованных не по цп
        saveEntrantRecommendedList(distribution, false, directionList);
    }

    @Override
    public void doDistributionLock(EcgDistribution distribution)
    {
        Session session = getSession();

        session.refresh(distribution);

        if (distribution.getState().isLockedOrApproved())
            throw new ApplicationException("Распределение «" + distribution.getTitle() + "» уже нельзя изменять.");

        // получаем количество рекомендованных
        Number number = new DQLSelectBuilder().fromEntity(EcgEntrantRecommended.class, "e")
        .where(eq(property(EcgEntrantRecommended.distribution().fromAlias("e")), value(distribution)))
        .createCountStatement(new DQLExecutionContext(session)).uniqueResult();

        if (number == null || number.intValue() == 0)
            throw new ApplicationException("В распределение должен быть включен хотя бы один абитуриент.");

        // всех абитуриентов, попавших в рейтинг добавляем в список резерва
        List<Entrant> entrantList = getDistributionRateBuilder(false, distribution)
        .column(property(RequestedEnrollmentDirection.entrantRequest().entrant().fromAlias("e")))
        .createStatement(session).list();

        Set<Long> usedEntrantIds = new HashSet<>();

        for (Entrant entrant : entrantList)
        {
            if (usedEntrantIds.add(entrant.getId()))
            {
                EcgEntrantReserved reserved = new EcgEntrantReserved();
                reserved.setDistribution(distribution);
                reserved.setEntrant(entrant);
                session.save(reserved);
            }
        }

        distribution.setState(this.<EcgDistributionState>getByNaturalId(new EcgDistributionStateGen.NaturalId(EcgDistributionStateCodes.LOCKED)));
        distribution.setApprovalDate(null);
        session.update(distribution);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void doDistributionApprove(IEcgDistribution distribution)
    {
        final Session session = getSession();

        session.refresh(distribution);

        if (distribution.getState().isApproved())
            throw new ApplicationException("Распределение «" + distribution.getTitle() + "» уже утверждено.");

        // в текущий момент получаем всех абитуриентов, у которых есть предзачисление по распределению
        Map<Long, PreliminaryEnrollmentStudent> entrantPreStudentMap = getEntrantPreStudentMap(distribution);

        // для уточняющего распределения все просто
        if (distribution instanceof EcgDistributionDetail)
        {
            // получаем всех рекомендованных
            List<EcgEntrantRecommendedDetail> entrantRecommendedList = getList(EcgEntrantRecommendedDetail.class, EcgEntrantRecommendedDetail.distribution().s(), distribution);

            if (entrantRecommendedList.isEmpty())
                throw new ApplicationException("В распределении «" + distribution.getTitle() + "» нет рекомендованных.");

            // расставляем признаки сдачи оригиналов рекомендованным абитуриентам
            for (EcgEntrantRecommendedDetail recommended : entrantRecommendedList)
            {
                Boolean bringOriginal = recommended.getDirection().getState().getCode().equals(UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY) ? null : recommended.getDirection().isOriginalDocumentHandedIn();

                recommended.setBringOriginal(bringOriginal);

                recommended.setHasPreStudent(entrantPreStudentMap.containsKey(recommended.getDirection().getEntrantRequest().getEntrant().getId()));

                session.update(recommended);
            }

            EcgDistributionDetail ecgDistributionDetail = (EcgDistributionDetail) distribution;
            ecgDistributionDetail.setState(this.<EcgDistributionState>getByNaturalId(new EcgDistributionStateGen.NaturalId(EcgDistributionStateCodes.APPROVED)));
            ecgDistributionDetail.setApprovalDate(new Date());

            session.update(distribution);

            return;
        }

        // для основного распределения все сложнее

        if (!distribution.getState().isLocked())
            throw new ApplicationException("Распределение «" + distribution.getTitle() + "» не зафиксировано.");

        // получаем всех рекомендованных
        List<EcgEntrantRecommended> entrantRecommendedList = getList(EcgEntrantRecommended.class, EcgEntrantRecommended.distribution().s(), distribution);

        if (entrantRecommendedList.isEmpty())
            throw new ApplicationException("В распределении «" + distribution.getTitle() + "» нет рекомендованных.");

        // для всех рекомендованных из волны не больше текущей получаем состояние оригиналов и предзачисления
        Map<EcgEntrantRecommended, IEcgHasBringOriginal> stateMap = getEntrantRecommendedWithPreStudentList((EcgDistribution) distribution);

        // сохраняем их
        for (Map.Entry<EcgEntrantRecommended, IEcgHasBringOriginal> entry : stateMap.entrySet())
        {
            EcgEntrantRecommendedState state = new EcgEntrantRecommendedState();

            state.setDistribution((EcgDistribution) distribution);
            state.setRecommended(entry.getKey());
            state.setBringOriginal(entry.getValue().getBringOriginal());
            state.setHasPreStudent(entry.getValue().getHasPreStudent());

            session.save(state);
        }

        // расставляем признаки сдачи оригиналов абитуриентам из резерва (применяем правило выбора направлений)

        // загружаем всех из резерва
        List<EcgEntrantReserved> entrantReservedList = getList(EcgEntrantReserved.class, EcgEntrantReserved.distribution(), (EcgDistribution) distribution);

        // берем выбранные направления приема удовлетворяющие конфигурации распределения для абитуриентов из резерва, по которым не забраны документы
        // т.е. абитуриенты из резерва, которые не попадут в этот источник данных, считаются неактуальными, у таких статус документов будет "забрал документы"
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "e")
        .where(eq(property(RequestedEnrollmentDirection.compensationType().fromAlias("e")), value(distribution.getConfig().getCompensationType())))
        .where(in(property(RequestedEnrollmentDirection.studentCategory().id().fromAlias("e")), getStudentCategoriesForDistribution(distribution)))
        .where(in(property(RequestedEnrollmentDirection.enrollmentDirection().id().fromAlias("e")), getEnrollmentDirectionsForDistribution(distribution)))
        .where(ne(property(RequestedEnrollmentDirection.state().fromAlias("e")), commonValue(getByNaturalId(new EntrantStateGen.NaturalId(UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY)))))
        .where(in(property(RequestedEnrollmentDirection.entrantRequest().entrant().id().fromAlias("e")), new DQLSelectBuilder().fromEntity(EcgEntrantReserved.class, "r")
            .column(property(EcgEntrantReserved.entrant().id().fromAlias("r")))
            .where(eq(property(EcgEntrantReserved.distribution().fromAlias("r")), value(distribution)))
            .buildQuery()
        ));

        List<RequestedEnrollmentDirection> directionList = builder.createStatement(session).list();

        final int REQUESTED_ENROLLMENT_DIRECTION_ID_IX = 0;
        final int FINAL_MARK_IX = 1;

        builder.joinEntity("e", DQLJoinType.inner, ChosenEntranceDiscipline.class, "c", eq(property(RequestedEnrollmentDirection.id().fromAlias("e")), property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("c"))));
        builder.where(isNotNull(ChosenEntranceDiscipline.finalMark().fromAlias("c")));
        builder.group(property(RequestedEnrollmentDirection.id().fromAlias("e")));
        builder.column(property(RequestedEnrollmentDirection.id().fromAlias("e")));
        builder.column(DQLFunctions.sum(property(ChosenEntranceDiscipline.finalMark().fromAlias("c"))));

        Map<Long, MutableDouble> finalMarkMap = new HashMap<>();
        for (Object[] dataRow : builder.createStatement(session).<Object[]>list())
        {
            Long requestedEnrollmentDirectionId = (Long) dataRow[REQUESTED_ENROLLMENT_DIRECTION_ID_IX];
            Double finalMark = (Double) dataRow[FINAL_MARK_IX];
            if (null == finalMark) { finalMark = 0.0d; /* null появляется тогда, когда баллов нет или же один из них null - тогда заменяем на 0 */ }

            MutableDouble mutable = finalMarkMap.get(requestedEnrollmentDirectionId);
            if (mutable == null)
                finalMarkMap.put(requestedEnrollmentDirectionId, mutable = new MutableDouble());
            mutable.add(finalMark);
        }

        Map<Entrant, List<IEcgCortegeDTO>> directionMap = new HashMap<>();
        for (RequestedEnrollmentDirection direction : directionList)
        {
            List<IEcgCortegeDTO> list = directionMap.get(direction.getEntrantRequest().getEntrant());
            if (list == null)
                directionMap.put(direction.getEntrantRequest().getEntrant(), list = new ArrayList<>());

            Person person = direction.getEntrantRequest().getEntrant().getPerson();
            PersonEduInstitution eduInstitution = person.getPersonEduInstitution();

            MutableDouble finalMark = finalMarkMap.get(direction.getId());

            list.add(new EcgCortegeDTO(direction,
                false,
                null,
                direction.getCompetitionKind(),
                finalMark == null ? 0 : finalMark.doubleValue(),
                    direction.getProfileChosenEntranceDiscipline() == null ? null : direction.getProfileChosenEntranceDiscipline().getFinalMark(),
                        direction.isGraduatedProfileEduInstitution(),
                        eduInstitution == null ? null : eduInstitution.getAverageMark(),
                            person.getIdentityCard().getFullFio()));
        }

        // используем компаратор кортежей
        Comparator<IEcgCortegeDTO> comparator = new EcgContegeComparator(UniecDAOFacade.getSettingsDAO().getCompetitionKindPriorities(distribution.getConfig().getEcgItem().getEnrollmentCampaign()));

        Map<Entrant, RequestedEnrollmentDirection> directionReservedMap = new HashMap<>();

        for (List<IEcgCortegeDTO> value : directionMap.values())
        {
            Collections.sort(value, comparator);

            RequestedEnrollmentDirection direction = (RequestedEnrollmentDirection) value.get(0).getEntity();

            // берем первое выбранное направление приема после кортежной сортировки
            directionReservedMap.put(direction.getEntrantRequest().getEntrant(), direction);
        }

        for (EcgEntrantReserved reserved : entrantReservedList)
        {
            RequestedEnrollmentDirection direction = directionReservedMap.get(reserved.getEntrant());
            reserved.setBringOriginal(direction == null ? null : direction.isOriginalDocumentHandedIn());
            reserved.setHasPreStudent(entrantPreStudentMap.containsKey(reserved.getEntrant().getId()));
            session.update(reserved);
        }

        EcgDistribution ecgDistribution = (EcgDistribution) distribution;
        ecgDistribution.setState(this.<EcgDistributionState>getByNaturalId(new EcgDistributionStateGen.NaturalId(EcgDistributionStateCodes.APPROVED)));
        ecgDistribution.setApprovalDate(new Date());

        session.update(distribution);
    }

    @Override
    public void doDistributionReApprove(IEcgDistribution distribution)
    {
        Session session = getSession();

        session.refresh(distribution);

        EcgConfigDTO configDTO = new EcgConfigDTO(distribution.getConfig());

        // нельзя одновременно сохранять, удалять и переутверждать распределения на одинаковую конфигурацию
        NamedSyncInTransactionCheckLocker.register(getSession(), DISTRIBUTION_SAVE_DELETE_LOCK + configDTO.toString());

        if (!isDistributionLastApproved(distribution))
            throw new ApplicationException("Распределение «" + distribution.getTitle() + "» не может быть переутверждено.");

        // отменяем утверждение
        if (distribution instanceof EcgDistributionDetail)
        {
            EcgDistributionDetail ecgDistributionDetail = (EcgDistributionDetail) distribution;
            ecgDistributionDetail.setState(this.<EcgDistributionState>getByNaturalId(new EcgDistributionStateGen.NaturalId(EcgDistributionStateCodes.FORMATIVE)));
            ecgDistributionDetail.setApprovalDate(null);
            session.update(distribution);
        }
        else
        {
            EcgDistribution ecgDistribution = (EcgDistribution) distribution;
            ecgDistribution.setState(this.<EcgDistributionState>getByNaturalId(new EcgDistributionStateGen.NaturalId(EcgDistributionStateCodes.LOCKED)));
            ecgDistribution.setApprovalDate(null);
            session.update(distribution);

            // для основного распределения еще нужно удалить статусы предзачисления
            new DQLDeleteBuilder(EcgEntrantRecommendedState.class)
            .where(eq(property(EcgEntrantRecommendedState.L_DISTRIBUTION), value(distribution)))
            .createStatement(session).execute();
        }

        // сохраняем изменения
        session.flush();

        // заново утверждаем
        doDistributionApprove(distribution);
    }

    @Override
    public void doBulkMakeDistribution(Set<EcgConfigDTO> configSet)
    {
        if (configSet.isEmpty()) return;

        EcgDistribution maxDistribution = getDistributionWithMaxWave(configSet.iterator().next());

        int wave = maxDistribution == null ? DISTRIBUTION_FIRST_WAVE : maxDistribution.getWave() + 1;

        for (EcgConfigDTO config : configSet)
        {
            IEcgQuotaDTO quotaDTO = getNextQuotaDTO(config);

            if (!quotaDTO.isEmpty())
                saveDistribution(config, wave, quotaDTO.getQuotaMap(), quotaDTO.getTaQuotaMap());
        }
    }

    @Override
    public void doBulkFillDistribution(Set<EcgConfigDTO> configSet)
    {
        for (EcgDistribution distribution : getDistributionListWithSameMaxWave(configSet))
            doDistributionFill(distribution);
    }

    @Override
    public void doBulkFillWithOriginalDistribution(Set<EcgConfigDTO> configSet)
    {
        for (EcgDistribution distribution : getDistributionListWithSameMaxWave(configSet))
            doDistributionFillWithOriginal(distribution);
    }

    @Override
    public void doBulkLockDistribution(Set<EcgConfigDTO> configSet)
    {
        for (EcgDistribution distribution : getDistributionListWithSameMaxWave(configSet))
            doDistributionLock(distribution);
    }

    @Override
    public void doBulkApproveDistribution(Set<EcgConfigDTO> configSet)
    {
        for (EcgDistribution distribution : getDistributionListWithSameMaxWave(configSet))
            doDistributionApprove(distribution);
    }

    @Override
    public void doBulkDeleteDistribution(Set<EcgConfigDTO> configSet)
    {
        for (EcgDistribution distribution : getDistributionListWithSameMaxWave(configSet))
            delete(distribution);
    }

    @Override
    public void doBulkPreEnroll(Set<Long> recommendedIds, EntrantEnrollmentOrderType orderType)
    {
        if (recommendedIds == null || recommendedIds.isEmpty()) return;

        List<IEcgEntrantRecommended> recommendedList = new ArrayList<>();
        List<RequestedEnrollmentDirection> directionList = new ArrayList<>();

        for (Long id : recommendedIds)
        {
            IEcgEntrantRecommended recommended = getNotNull(id);
            recommendedList.add(recommended);
            directionList.add(recommended.getDirection());
        }

        Set<Long> directionIds = new HashSet<>(new DQLSelectBuilder().fromEntity(PreliminaryEnrollmentStudent.class, "e")
        .column(property(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().id().fromAlias("e")))
        .where(in(property(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().fromAlias("e")), directionList))
        .createStatement(getSession()).<Long>list());

        for (IEcgEntrantRecommended recommended : recommendedList)
            if (!directionIds.contains(recommended.getDirection().getId()))
                EcPreEnrollManager.instance().dao().doChangeOrderType(recommended.getDirection().getId(), recommended.getDirection().getEnrollmentDirection().getEducationOrgUnit(), orderType);
    }

    @Override
    public byte[] getRtfPrint(Long distributionId)
    {
        IEcgDistribution distribution = getNotNull(distributionId);

        getSession().refresh(distribution);

        IEcDistributionListPrint printForm = IEcDistributionListPrint.instance.get();

        return RtfUtil.toByteArray(distribution.getConfig().getEcgItem() instanceof EnrollmentDirection ?
            printForm.getPrintFormForDistributionPerDirection(getByCode(UniecScriptItem.class, UniecDefines.TEMPLATE_ECG_DISTRIBUTION_PER_DIRECTION).getCurrentTemplate(), (EcgDistribution) distribution) :
                printForm.getPrintFormForDistributionPerCompetitionGroup(getByCode(UniecScriptItem.class, UniecDefines.TEMPLATE_ECG_DISTRIBUTION_PER_COMPETITION_GROUP).getCurrentTemplate(), distribution)
        );
    }

    @Override
    public byte[] getXmlPrint(Long distributionId)
    {
        return getBulkXmlPrint(Collections.singleton(distributionId));
    }

    @Override
    public byte[] getBulkRtfPrint(Set<Long> distributionIds)
    {
        try
        {
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            ZipOutputStream zip = new ZipOutputStream(output);

            for (Long distributionId : distributionIds)
            {
                IEcgDistribution distribution = DataAccessServices.dao().getNotNull(distributionId);
                zip.putNextEntry(new ZipEntry(getDistributionFileName(distribution) + ".rtf"));
                IOUtils.write(getRtfPrint(distributionId), zip);
                zip.closeEntry();
            }
            zip.close();

            return output.toByteArray();
        } catch (IOException e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    public byte[] getBulkXmlPrint(Set<Long> distributionIds)
    {
        if (distributionIds.isEmpty()) return null;

        Set<IEcgDistribution> distributionSet = new LinkedHashSet<>();
        for (Long id : distributionIds)
            distributionSet.add(this.<IEcgDistribution>getNotNull(id));

        try
        {
            ByteArrayOutputStream result = new ByteArrayOutputStream();
            JAXBContext context = JAXBContext.newInstance(EnrollmentDistributionEnvironmentNode.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(IEnrollmentDistributionServiceDao.INSTANCE.get().getEnrollmentDistributionEnvironmentNode(distributionSet), result);
            return result.toByteArray();
        } catch (Exception e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    public void getQuotaHTMLDescription(IEcgDistribution distribution, Map<Long, String> quotaMap, Map<Long, String> taQuotaMap)
    {
        IEcgQuotaFreeDTO quotaFreeDTO = getFreeQuotaDTO(distribution);
        IEcgQuotaDTO quotaDTO = quotaFreeDTO.getQuotaDTO();

        // список видов цп
        List<TargetAdmissionKind> taKindList = quotaDTO.getTaKindList();

        // true, если используется единственный вид цп и он по умолчанию
        // в этом случае не нужно свободные места детализировать по видам цп
        boolean defaultTaKind = taKindList.size() == 1 && taKindList.get(0).getCode().equals(TargetAdmissionKindCodes.TA_DEFAULT);
        Long defaultTaKindId = defaultTaKind ? taKindList.get(0).getId() : null;

        // для итоговой строки планов по направлениям
        int quotaGlobalSum = 0; // суммарное кол-во мест по всем направлениям
        int freeGlobalSum = 0;  // суммарное кол-во свободных мест по всем направлениям

        // для итоговой строки планов по видам цп
        Map<Long, Integer> taQuotaGlobalSumMap = new HashMap<>(); // суммарное кол-во мест по всем направлениям
        Map<Long, Integer> taFreeGlobalSumMap = new HashMap<>();  // суммарное кол-во свободных мест по всем направлениям

        String style = " style='white-space:nowrap;'";
        String styleRed = " style='white-space:nowrap;color:red;'";

        for (Map.Entry<Long, Integer> entry : quotaDTO.getQuotaMap().entrySet())
        {
            Long directionId = entry.getKey();
            int quota = entry.getValue();
            int free = quotaFreeDTO.getFreeMap().get(directionId);

            quotaGlobalSum += quota;
            freeGlobalSum += free;

            quotaMap.put(directionId, "<div" + (free < 0 ? styleRed : style) + ">" + free + " / " + quota + "</div>");

            if (defaultTaKind)
            {
                int taQuota = quotaDTO.getTaQuotaMap().get(directionId).get(defaultTaKindId);
                int taFree = quotaFreeDTO.getTaFreeMap().get(directionId).get(defaultTaKindId);

                Integer taQuotaSum = taQuotaGlobalSumMap.get(defaultTaKindId);
                taQuotaGlobalSumMap.put(defaultTaKindId, taQuota + (taQuotaSum == null ? 0 : taQuotaSum));

                Integer taFreeSum = taFreeGlobalSumMap.get(defaultTaKindId);
                taFreeGlobalSumMap.put(defaultTaKindId, taFree + (taFreeSum == null ? 0 : taFreeSum));

                taQuotaMap.put(directionId, "<div" + (taFree < 0 ? styleRed : style) + ">" + taFree + " / " + taQuota + "</div>");
            } else
            {
                StringBuilder buf = new StringBuilder("<table cellspacing='0' cellpadding='0'>");
                int taQuotaSum = 0;
                int taFreeSum = 0;

                Map<Long, Integer> taQuotaSubMap = quotaDTO.getTaQuotaMap().get(directionId);
                Map<Long, Integer> taFreeSubMap = quotaFreeDTO.getTaFreeMap().get(directionId);

                for (TargetAdmissionKind kind : taKindList)
                {
                    int taQuota = taQuotaSubMap.get(kind.getId());
                    int taFree = taFreeSubMap.get(kind.getId());

                    taQuotaSum += taQuota;
                    taFreeSum += taFree;

                    Integer taQuotaGlobalSum = taQuotaGlobalSumMap.get(kind.getId());
                    taQuotaGlobalSumMap.put(kind.getId(), taQuota + (taQuotaGlobalSum == null ? 0 : taQuotaGlobalSum));

                    Integer taFreeGlobalSum = taFreeGlobalSumMap.get(kind.getId());
                    taFreeGlobalSumMap.put(kind.getId(), taFree + (taFreeGlobalSum == null ? 0 : taFreeGlobalSum));

                    buf.append("<tr>");
                    buf.append("<td>").append(kind.getShortTitle()).append(":").append("</td>");
                    buf.append("<td").append(taFree < 0 ? styleRed : style).append(">").append(taFree).append(" / ").append(taQuota).append("</td>");
                    buf.append("</tr>");
                }

                buf.append("<tr><td>&nbsp;</td><td").append(taFreeSum < 0 ? styleRed : style).append(">").append(taFreeSum).append(" / ").append(taQuotaSum).append("</td></tr>");
                buf.append("</table>");

                taQuotaMap.put(directionId, buf.toString());
            }
        }

        quotaMap.put(0L, "<div" + (freeGlobalSum < 0 ? styleRed : style) + ">" + freeGlobalSum + " / " + quotaGlobalSum + "</div>");

        if (defaultTaKind)
        {
            int taQuota = taQuotaGlobalSumMap.get(defaultTaKindId);
            int taFree = taFreeGlobalSumMap.get(defaultTaKindId);

            taQuotaMap.put(0L, "<div " + (taFree < 0 ? styleRed : style) + ">" + taFree + " / " + taQuota + "</div>");
        } else
        {
            StringBuilder buf = new StringBuilder("<table cellspacing='0' cellpadding='0'>");

            int taQuotaSum = 0;
            int taFreeSum = 0;

            for (TargetAdmissionKind kind : taKindList)
            {
                int taQuota = taQuotaGlobalSumMap.get(kind.getId());
                int taFree = taFreeGlobalSumMap.get(kind.getId());

                taQuotaSum += taQuota;
                taFreeSum += taFree;

                buf.append("<tr>");
                buf.append("<td>").append(kind.getShortTitle()).append(":").append("</td>");
                buf.append("<td").append(taFree < 0 ? styleRed : style).append(">").append(taFree).append(" / ").append(taQuota).append("</td>");
                buf.append("</tr>");
            }

            buf.append("<tr><td>&nbsp;</td><td").append(taFreeSum < 0 ? styleRed : style).append(">").append(taFreeSum).append(" / ").append(taQuotaSum).append("</td></tr>");
            buf.append("</table>");

            taQuotaMap.put(0L, buf.toString());
        }
    }

    @Override
    public boolean isConfigAllowDirection(EcgDistributionConfig config, RequestedEnrollmentDirection direction)
    {
        if (!config.getCompensationType().equals(direction.getCompensationType()))
            return false;

        String code = direction.getStudentCategory().getCode();

        if (config.isSecondHighAdmission())
            return code.equals(UniDefines.STUDENT_CATEGORY_SECOND_HIGH);
        else
            return code.equals(UniDefines.STUDENT_CATEGORY_STUDENT) || code.equals(UniDefines.STUDENT_CATEGORY_LISTENER);
    }

    @Override
    public boolean isDistributionAllowEnrollment(IEcgDistribution distribution)
    {
        Session session = getSession();

        session.refresh(distribution);

        // предзачислять можно только в утвержденных распределениях
        if (!distribution.getState().isApproved())
            return false;

        // предзачислять можно только в распределениях последней волны
        if (distribution.getDistribution().getWave() != getDistributionWithMaxWave(distribution.getConfig()).getWave())
            return false;

        // в распределениях по направлениям можно предзачислять в основном распределении при отсутствии уточняющее, либо в уточняющем
        if (distribution.getConfig().getEcgItem() instanceof EnrollmentDirection)
            return distribution instanceof EcgDistributionDetail || getByNaturalId(new EcgDistributionDetailGen.NaturalId((EcgDistribution) distribution)) == null;

        // в распределениях по конкурсным группам можно предзачислять только в уточняющем распределении
        if (distribution.getConfig().getEcgItem() instanceof CompetitionGroup)
            return distribution instanceof EcgDistributionDetail;

        // we cannot be here
        return false;
    }

    @Override
    public boolean isDistributionLastApproved(IEcgDistribution distribution)
    {
        Session session = getSession();

        session.refresh(distribution);

        // переутверждать можно только утвержденные распределения
        if (!distribution.getState().isApproved())
            return false;

        // переутверждать можно только распределения последней волны
        if (distribution.getDistribution().getWave() != getDistributionWithMaxWave(distribution.getConfig()).getWave())
            return false;

        // можно переутверждать уточняющие распределения, основные распределения по направлениям, основные распределения по конкурсным группам без уточняющего распределения
        return distribution instanceof EcgDistributionDetail || distribution.getConfig().getEcgItem() instanceof EnrollmentDirection || getByNaturalId(new EcgDistributionDetailGen.NaturalId((EcgDistribution) distribution)) == null;
    }

    @Override
    public Map<EcgEntrantRecommended, IEcgHasBringOriginal> getEntrantRecommendedWithPreStudentList(final EcgDistribution distribution)
    {
        Map<EcgEntrantRecommended, IEcgHasBringOriginal> resultMap = new HashMap<>();

        // беру всех рекомендованных из всех распределений волны не больше текущей
        List<EcgEntrantRecommended> list = new DQLSelectBuilder().fromEntity(EcgEntrantRecommended.class, "e")
        .where(eq(property(EcgEntrantRecommended.distribution().config().fromAlias("e")), value(distribution.getConfig())))
        .where(le(property(EcgEntrantRecommended.distribution().wave().fromAlias("e")), value(distribution.getWave())))
        .createStatement(getSession()).list();

        if (distribution.getState().isApproved())
        {
            // информацию беру из состояния рекомендованного на момент утверждения распределения
            Map<Long, IEcgHasBringOriginal> id2state = new HashMap<>();
            for (EcgEntrantRecommendedState item : new DQLSelectBuilder().fromEntity(EcgEntrantRecommendedState.class, "e")
            .where(eq(property(EcgEntrantRecommendedState.distribution().fromAlias("e")), value(distribution)))
            .createStatement(getSession()).<EcgEntrantRecommendedState>list())
                id2state.put(item.getRecommended().getId(), item);

            for (EcgEntrantRecommended recommended : list)
                resultMap.put(recommended, id2state.get(recommended.getId()));
        } else
        {
            // информацию беру на текущий момент
            final Set<Long> recommendedIds = new HashSet<>(new DQLSelectBuilder().fromEntity(EcgEntrantRecommended.class, "e")
            .column(property(EcgEntrantRecommended.id().fromAlias("e")))
            .where(eq(property(EcgEntrantRecommended.distribution().config().fromAlias("e")), value(distribution.getConfig())))
            .where(le(property(EcgEntrantRecommended.distribution().wave().fromAlias("e")), value(distribution.getWave())))
            .where(in(property(EcgEntrantRecommended.direction().entrantRequest().entrant().id().fromAlias("e")), getPreStudentQuery(distribution.getDistribution())))
            .createStatement(getSession()).<Long>list());

            for (final EcgEntrantRecommended recommended : list) {
                final Boolean hasBringOriginal = recommended.getDirection().getState().getCode().equals(UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY) ? null : recommended.getDirection().isOriginalDocumentHandedIn();
                final boolean hasPreStudent = recommendedIds.contains(recommended.getId());

                resultMap.put(recommended, new IEcgHasBringOriginal() {
                    @Override public IEcgDistribution getDistribution() {
                        return distribution;
                    }
                    @Override public Boolean getBringOriginal() {
                        return hasBringOriginal;
                    }
                    @Override public Boolean getHasPreStudent() {
                        return hasPreStudent;
                    }
                });
            }
        }
        return resultMap;
    }

    @Override
    public Map<Long, PreliminaryEnrollmentStudent> getEntrantPreStudentMap(IEcgDistribution distribution)
    {
        List<PreliminaryEnrollmentStudent> preStudentList = new DQLSelectBuilder().fromEntity(PreliminaryEnrollmentStudent.class, "p1")
        .where(eq(property(PreliminaryEnrollmentStudent.compensationType().fromAlias("p1")), value(distribution.getConfig().getCompensationType())))
        .where(in(property(PreliminaryEnrollmentStudent.studentCategory().id().fromAlias("p1")), getStudentCategoriesForDistribution(distribution)))
        .where(in(property(PreliminaryEnrollmentStudent.educationOrgUnit().fromAlias("p1")), new DQLSelectBuilder().fromEntity(EcgDistributionQuota.class, "q1")
            .column(property(EcgDistributionQuota.direction().educationOrgUnit().fromAlias("q1")))
            .where(eq(property(EcgDistributionQuota.distribution().fromAlias("q1")), value(distribution.getDistribution())))
            .buildQuery()
        )).createStatement(getSession()).list();

        // для каждого абитуриента должно получаться не больше одного студента пред.зачисления

        Map<Long, PreliminaryEnrollmentStudent> result = new HashMap<>();
        for (PreliminaryEnrollmentStudent preStudent : preStudentList)
        {
            Long entrantId = preStudent.getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getId();
            if (null != result.put(entrantId, preStudent))
                throw new RuntimeException("Entrant " + entrantId + " has two preStudents in distribution " + distribution.getId());
        }
        return result;
    }

    @Override
    public Map<Long, EcgPriorityInfo> getEntrantPriorityMap(final IEcgDistribution distribution, Collection<Long> entrantIds)
    {
        // убеждаемся в уникальности
        if (!(entrantIds instanceof Set)) { entrantIds = new HashSet<>(entrantIds); }

        final List<Object[]> rows = new ArrayList<>();
        final Session session = getSession();

        BatchUtils.execute(entrantIds, 1000, new BatchUtils.Action<Long>() {
            Collection<Long> studentCategoriesForDistribution = getStudentCategoriesForDistribution(distribution);
            @Override public void execute(Collection<Long> entrantIds) {
                rows.addAll(
                    new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "e")
                    .column(property(RequestedEnrollmentDirection.entrantRequest().regNumber().fromAlias("e")))
                    .column(property(RequestedEnrollmentDirection.priority().fromAlias("e")))
                    .column(property(RequestedEnrollmentDirection.entrantRequest().entrant().id().fromAlias("e")))
                    .column(property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().shortTitle().fromAlias("e")))
                    .column(property(RequestedEnrollmentDirection.enrollmentDirection().id().fromAlias("e")))
                    .where(eq(property(RequestedEnrollmentDirection.entrantRequest().entrant().enrollmentCampaign().fromAlias("e")), value(distribution.getConfig().getEcgItem().getEnrollmentCampaign())))
                    .where(in(property(RequestedEnrollmentDirection.state().code().fromAlias("e")), Arrays.asList(
                        UniecDefines.ENTRANT_STATE_TOBE_ENROLED_CODE,
                        UniecDefines.ENTRANT_STATE_PRELIMENARY_ENROLLED_CODE,
                        UniecDefines.ENTRANT_STATE_IN_ORDER,
                        UniecDefines.ENTRANT_STATE_ENROLED_CODE
                    )))
                    .where(in(property(RequestedEnrollmentDirection.entrantRequest().entrant().id().fromAlias("e")), entrantIds))
                    .where(eq(property(RequestedEnrollmentDirection.compensationType().fromAlias("e")), value(distribution.getConfig().getCompensationType())))
                    .where(in(property(RequestedEnrollmentDirection.studentCategory().id().fromAlias("e")), studentCategoriesForDistribution))
                    .where(in(property(RequestedEnrollmentDirection.enrollmentDirection().id().fromAlias("e")), getEnrollmentDirectionsForDistribution(distribution)))
                    .order(property(RequestedEnrollmentDirection.entrantRequest().regNumber().fromAlias("e")))
                    .order(property(RequestedEnrollmentDirection.priority().fromAlias("e")))
                    .createStatement(session).<Object[]>list()
                );
            }
        });

        // собритуем глобально (это надо делать, т.к. данные были загружены порциями)
        Collections.sort(rows, new Comparator<Object[]>() {
            private int integer(Object x) {
                return (x instanceof Integer ? (Integer) x : 0);
            }

            @Override public int compare(Object[] o1, Object[] o2)
            {
                int r;
                if (0 != (r = integer(o1[0]) - integer(o2[0]))) { return r; }
                if (0 != (r = integer(o1[1]) - integer(o2[1]))) { return r; }
                return 0;
            }
        });

        Map<Long, EcgPriorityInfo> prioritiesMap = new HashMap<>();
        for (Object[] dataRow : rows)
        {
            Long entrantId = (Long) dataRow[2];
            String shortTitle = (String) dataRow[3];
            Long directionId = (Long) dataRow[4];
            EcgPriorityInfo info = prioritiesMap.get(entrantId);
            if (info == null)
                prioritiesMap.put(entrantId, info = new EcgPriorityInfo(entrantId));
            info.getPriorityIdList().add(directionId);
            info.getPriorityList().add(shortTitle);
        }

        return prioritiesMap;
    }

    private void accumulateRateRow(EcgDistribution distribution, List<TargetAdmissionKind> targetAdmissionKindList, Set<Long> usedEntrantIds, IEcgQuotaManager quotaManager, IEcgEntrantRateRowDTO.Rule rateRule, List<Long> directionList)
    {
        // получаем строки рейтинга
        List<? extends IEcgEntrantRateRowDTO> rateList = getEntrantRateRowList(distribution, targetAdmissionKindList, rateRule);

        // общее число строк
        int len = rateList.size();

        int i = 0;

        // общее число свободных мест
        int freeTotal = quotaManager.getFreeTotal();

        while (i < len && freeTotal > 0)
        {
            IEcgEntrantRateRowDTO rateRowDTO = rateList.get(i);

            if (!usedEntrantIds.contains(rateRowDTO.getId()))
            {
                IEcgPlanChoiceResult choiceResult = quotaManager.getPossibleDirectionList(rateRowDTO, null);

                IEcgEntrantRateDirectionDTO chosenDirection = choiceResult.getChosenDirection();

                if (chosenDirection != null)
                {
                    freeTotal--;
                    directionList.add(chosenDirection.getId());
                    usedEntrantIds.add(rateRowDTO.getId());
                }
            }

            i++;
        }
    }

    /**
     * Получает список всех распределений последних волн для указанных конфигураций
     * У всех распределений номер волны должен быть одинаковый
     * Если номер волны получается разный, то кидается пользовательское сообщение
     *
     * @param configDTOSet конфигурации
     * @return список распределений последних волн для указанных конфигураций
     */
    private List<EcgDistribution> getDistributionListWithSameMaxWave(Set<EcgConfigDTO> configDTOSet)
    {
        if (configDTOSet == null || configDTOSet.isEmpty()) return Collections.emptyList();

        List<EcgDistributionConfig> configList = new ArrayList<>();

        Set<Long> enrollmentCampaignIds = new HashSet<>();

        for (EcgConfigDTO configDTO : configDTOSet)
        {
            enrollmentCampaignIds.add(configDTO.getEcgItem().getEnrollmentCampaign().getId());

            EcgDistributionConfig config = getByNaturalId(new EcgDistributionConfigGen.NaturalId(configDTO.getEcgItem(), configDTO.getCompensationType(), configDTO.isSecondHighAdmission()));

            if (config != null)
                configList.add(config);
        }

        if (enrollmentCampaignIds.size() != 1)
            throw new RuntimeException("ConfigDTOSet contains EcgItems from different enrollment campaigns.");

        if (configList.isEmpty()) return Collections.emptyList();

        List<EcgDistribution> distributionList = new DQLSelectBuilder().fromEntity(EcgDistribution.class, "e")
        .where(in(property(EcgDistribution.config().fromAlias("e")), configList))
        .order(property(EcgDistribution.config().id().fromAlias("e")))
        .order(property(EcgDistribution.wave().fromAlias("e")), OrderDirection.desc)
        .createStatement(getSession()).list();

        Set<Long> usedConfigIds = new HashSet<>();

        Integer wave = null;

        List<EcgDistribution> resultList = new ArrayList<>();

        for (EcgDistribution distribution : distributionList)
        {
            if (usedConfigIds.add(distribution.getConfig().getId()))
            {
                int currentWave = distribution.getWave();

                if (wave == null)
                {
                    wave = currentWave;
                } else
                {
                    if (wave != currentWave)
                        throw new ApplicationException("Во всех выбранных конфигурациях последние распределения должны быть одинаковой волны.");
                }

                resultList.add(distribution);
            }
        }

        if (resultList.size() != configList.size())
            throw new ApplicationException("Во всех выбранных конфигурациях последние распределения должны быть одинаковой волны.");

        return resultList;
    }

    /**
     * Получает список направлений приема из указанной приемной кампании
     *
     * @param enrollmentCampaign           приемная кампания
     * @param formativeOrgUnitList         формирующие подр.
     * @param territorialOrgUnitList       территориальные подр.
     * @param educationLevelHighSchoolList направления подготовки (специальности) вуза
     * @param developFormList              формы освоения
     * @param developConditionList         условия освоения
     * @param developTechList              технологии освоения
     * @param developPeriodList            сроки освоения
     * @return список направлений приема
     */
    private List<EnrollmentDirection> getEnrollmentDirectionList(EnrollmentCampaign enrollmentCampaign, List<OrgUnit> formativeOrgUnitList, List<OrgUnit> territorialOrgUnitList, List<EducationLevelsHighSchool> educationLevelHighSchoolList, List<DevelopForm> developFormList, List<DevelopCondition> developConditionList, List<DevelopTech> developTechList, List<DevelopPeriod> developPeriodList, List<Qualifications> qualificationList)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, "e").column("e")
        .where(eq(property(EnrollmentDirection.enrollmentCampaign().fromAlias("e")), value(enrollmentCampaign)));

        builder.joinPath(DQLJoinType.inner, EnrollmentDirection.educationOrgUnit().fromAlias("e"), "ou");

        if (formativeOrgUnitList != null)
            builder.where(in(property(EducationOrgUnit.formativeOrgUnit().fromAlias("ou")), formativeOrgUnitList));

        if (territorialOrgUnitList != null)
            builder.where(in(property(EducationOrgUnit.territorialOrgUnit().fromAlias("ou")), territorialOrgUnitList));

        if (educationLevelHighSchoolList != null)
            builder.where(in(property(EducationOrgUnit.educationLevelHighSchool().fromAlias("ou")), educationLevelHighSchoolList));

        if (developFormList != null)
            builder.where(in(property(EducationOrgUnit.developForm().fromAlias("ou")), developFormList));

        if (developConditionList != null)
            builder.where(in(property(EducationOrgUnit.developCondition().fromAlias("ou")), developConditionList));

        if (developTechList != null)
            builder.where(in(property(EducationOrgUnit.developTech().fromAlias("ou")), developTechList));

        if (developPeriodList != null)
            builder.where(in(property(EducationOrgUnit.developPeriod().fromAlias("ou")), developPeriodList));

        if (qualificationList != null && !qualificationList.isEmpty())
            builder.where(in(property(EducationOrgUnit.educationLevelHighSchool().educationLevel().qualification().fromAlias("ou")), qualificationList));

        builder.order(property(EducationOrgUnit.educationLevelHighSchool().displayableTitle().fromAlias("ou")));

        return builder.createStatement(getSession()).list();
    }

    /**
     * Получает список основных распределений абитуриентов по конкурсным группам
     * Список не сортируется
     *
     * @param enrollmentCampaign  приемная кампания
     * @param compensationType    вид затрат
     * @param secondHighAdmission true, если следует выбрать распределения только по второму высшему
     * @param wave                номер волны
     * @param state               состояние распределения
     * @param competitionGroup    конкурсная группа
     * @return список DTO распределений
     */
    private List<EcgDistribution> getDistributionList(EnrollmentCampaign enrollmentCampaign,
        CompensationType compensationType,
        boolean secondHighAdmission,
        Integer wave,
        EcgDistributionState state,
        CompetitionGroup competitionGroup,
        List<Qualifications> qualificationList)
        {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EcgDistribution.class, "e").column("e");

        builder.joinPath(DQLJoinType.inner, EcgDistribution.config().fromAlias("e"), "config");

        builder.joinEntity("e", DQLJoinType.inner, CompetitionGroup.class, "g", eq(
            property(EcgDistributionConfig.ecgItem().id().fromAlias("config")),
            property(CompetitionGroup.id().fromAlias("g"))
        ));

        builder.where(eq(property(CompetitionGroup.enrollmentCampaign().fromAlias("g")), value(enrollmentCampaign)));
        builder.where(eq(property(EcgDistributionConfig.secondHighAdmission().fromAlias("config")), value(secondHighAdmission)));

        if (compensationType != null)
            builder.where(eq(property(EcgDistribution.config().compensationType().fromAlias("e")), value(compensationType)));

        if (wave != null)
            builder.where(eq(property(EcgDistribution.wave().fromAlias("e")), value(wave)));

        if (state != null)
            builder.where(eq(property(EcgDistribution.state().fromAlias("e")), value(state)));

        if (competitionGroup != null)
            builder.where(eq(property(CompetitionGroup.id().fromAlias("g")), value(competitionGroup.getId())));

        if (qualificationList != null && !qualificationList.isEmpty())
        {
            DQLSelectBuilder cgBuilder = new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, "cgDir")
                    .predicate(DQLPredicateType.distinct)
                    .column(property(EnrollmentDirection.competitionGroup().id().fromAlias("cgDir")))
                    .where(in(property(EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().fromAlias("cgDir")), qualificationList));
            builder.where(in(property(CompetitionGroup.id().fromAlias("g")), cgBuilder.buildQuery()));
        }

        return builder.createStatement(getSession()).list();
        }

    /**
     * Получает список основных распределений абитуриентов по направлениям приема
     * Список не сортируется
     *
     * @param enrollmentCampaign           приемная кампания
     * @param compensationType             вид возмещения затрат
     * @param secondHighAdmission          true, если следует выбрать распределения только по второму высшему
     * @param wave                         номер волны
     * @param state                        состояние распределения
     * @param formativeOrgUnitList         формирующие подр.
     * @param territorialOrgUnitList       территориальные подр.
     * @param educationLevelHighSchoolList направления подготовки (специальности) вуза
     * @param developFormList              формы освоения
     * @param developConditionList         условия освоения
     * @param developTechList              технологии освоения
     * @param developPeriodList            сроки освоения
     * @return список DTO распределений
     */
    private List<EcgDistribution> getDistributionList(EnrollmentCampaign enrollmentCampaign,
        CompensationType compensationType,
        boolean secondHighAdmission,
        Integer wave,
        EcgDistributionState state,
        List<OrgUnit> formativeOrgUnitList,
        List<OrgUnit> territorialOrgUnitList,
        List<EducationLevelsHighSchool> educationLevelHighSchoolList,
        List<DevelopForm> developFormList,
        List<DevelopCondition> developConditionList,
        List<DevelopTech> developTechList,
        List<DevelopPeriod> developPeriodList,
        List<Qualifications> qualificationList)
        {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EcgDistribution.class, "e").column("e");

        builder.joinPath(DQLJoinType.inner, EcgDistribution.config().fromAlias("e"), "config");

        builder.joinEntity("e", DQLJoinType.inner, EnrollmentDirection.class, "d", eq(
            property(EcgDistributionConfig.ecgItem().id().fromAlias("config")),
            property(EnrollmentDirection.id().fromAlias("d"))
        ));

        builder.joinPath(DQLJoinType.inner, EnrollmentDirection.educationOrgUnit().fromAlias("d"), "ou");

        builder.where(eq(property(EnrollmentDirection.enrollmentCampaign().fromAlias("d")), value(enrollmentCampaign)));
        builder.where(eq(property(EcgDistributionConfig.secondHighAdmission().fromAlias("config")), value(secondHighAdmission)));

        if (compensationType != null)
            builder.where(eq(property(EcgDistribution.config().compensationType().fromAlias("e")), value(compensationType)));

        if (wave != null)
            builder.where(eq(property(EcgDistribution.wave().fromAlias("e")), value(wave)));

        if (state != null)
            builder.where(eq(property(EcgDistribution.state().fromAlias("e")), value(state)));

        if (formativeOrgUnitList != null)
            builder.where(in(property(EducationOrgUnit.formativeOrgUnit().fromAlias("ou")), formativeOrgUnitList));

        if (territorialOrgUnitList != null)
            builder.where(in(property(EducationOrgUnit.territorialOrgUnit().fromAlias("ou")), territorialOrgUnitList));

        if (educationLevelHighSchoolList != null)
            builder.where(in(property(EducationOrgUnit.educationLevelHighSchool().fromAlias("ou")), educationLevelHighSchoolList));

        if (developFormList != null)
            builder.where(in(property(EducationOrgUnit.developForm().fromAlias("ou")), developFormList));

        if (developConditionList != null)
            builder.where(in(property(EducationOrgUnit.developCondition().fromAlias("ou")), developConditionList));

        if (developTechList != null)
            builder.where(in(property(EducationOrgUnit.developTech().fromAlias("ou")), developTechList));

        if (developPeriodList != null)
            builder.where(in(property(EducationOrgUnit.developPeriod().fromAlias("ou")), developPeriodList));

        if (qualificationList != null && !qualificationList.isEmpty())
            builder.where(in(property(EducationOrgUnit.educationLevelHighSchool().educationLevel().qualification().fromAlias("ou")), qualificationList));

        return builder.createStatement(getSession()).list();
        }

    /**
     * Получает список уточняющих распределений
     *
     * @param distributionList основные распределения
     * @return список уточняющих распределений
     */
    private List<EcgDistributionDetail> getDistributionDetailList(List<EcgDistribution> distributionList)
    {
        return new DQLSelectBuilder().fromEntity(EcgDistributionDetail.class, "e")
        .where(in(property(EcgDistributionDetail.distribution().fromAlias("e")), distributionList))
        .createStatement(getSession()).list();
    }

    /**
     * Создает dql билдер, который получает все выбранные направления приема для построения рейтинга
     * Используется при добавлении абитуриентов в распределении и при фиксации списка резерва распределения
     *
     * @param buildRating  true, если идет построение рейтинга
     * @param distribution распределение
     * @return dql билдер
     */
    private DQLSelectBuilder getDistributionRateBuilder(boolean buildRating, EcgDistribution distribution)
    {
        IDQLSelectableQuery distributionDirectionQuery = new DQLSelectBuilder().fromEntity(EcgDistributionQuota.class, "q")
        .column(property(EcgDistributionQuota.direction().id().fromAlias("q")))
        .where(eq(property(EcgDistributionQuota.distribution().fromAlias("q")), value(distribution))).buildQuery();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "e");

        // учитываем конфигурацию распределения
        builder.where(eq(property(RequestedEnrollmentDirection.compensationType().fromAlias("e")), value(distribution.getConfig().getCompensationType())));
        builder.where(in(property(RequestedEnrollmentDirection.enrollmentDirection().id().fromAlias("e")), distributionDirectionQuery));
        builder.where(in(property(RequestedEnrollmentDirection.studentCategory().id().fromAlias("e")), getStudentCategoriesForDistribution(distribution)));

        // исключаем тех абитуриентов, кто уже рекомендован в это распределение
        builder.where(notIn(property(RequestedEnrollmentDirection.entrantRequest().entrant().id().fromAlias("e")), new DQLSelectBuilder().fromEntity(EcgEntrantRecommended.class, "r2")
            .column(property(EcgEntrantRecommended.direction().entrantRequest().entrant().id().fromAlias("r2")))
            .where(eq(property(EcgEntrantRecommended.distribution().fromAlias("r2")), value(distribution)))
            .buildQuery()
        ));

        // для фиксации N-ой волны эти условия пропускаются
        if (distribution.getWave() == DISTRIBUTION_FIRST_WAVE || buildRating)
        {
            // состояние к зачислению и не архивные
            builder.where(eq(property(RequestedEnrollmentDirection.state().fromAlias("e")), commonValue(getByNaturalId(new EntrantStateGen.NaturalId(UniecDefines.ENTRANT_STATE_TOBE_ENROLED_CODE)))));
            builder.where(eq(property(RequestedEnrollmentDirection.entrantRequest().entrant().archival().fromAlias("e")), value(Boolean.FALSE)));

            // исключаем тех абитуриентов, кто предзачислен хотя бы одному нпп из распределения с учетом конфигурации
            builder.where(notIn(property(RequestedEnrollmentDirection.entrantRequest().entrant().id().fromAlias("e")), getPreStudentQuery(distribution)));
        }

        // если распределение на бюджет и не первой волны, то нужно брать только абитуриентов из списка резерва предыдущей волны на эту же конфигурацию
        if (distribution.getConfig().getCompensationType().isBudget() && distribution.getWave() > DISTRIBUTION_FIRST_WAVE)
        {
            builder.where(in(property(RequestedEnrollmentDirection.entrantRequest().entrant().id().fromAlias("e")), new DQLSelectBuilder().fromEntity(EcgEntrantReserved.class, "r")
                .column(property(EcgEntrantReserved.entrant().id().fromAlias("r")))
                .where(eq(property(EcgEntrantReserved.distribution().wave().fromAlias("r")), value(distribution.getWave() - 1)))
                .where(eq(property(EcgEntrantReserved.distribution().config().fromAlias("r")), value(distribution.getConfig())))
                .buildQuery()
            ));
        }

        return builder;
    }

    /**
     * Создает билдер, который получает все выбранные направления приема для построения рейтинга
     * Используется при добавлении абитуриентов в уточняющее распределении
     *
     * @param distributionDetail уточняющее распределение
     * @param rateRule           правило построения рейтинга
     * @return билдер
     */
    private DQLSelectBuilder getDistributionDetailRateBuilder(EcgDistributionDetail distributionDetail, IEcgEntrantRateRowDTO.Rule rateRule)
    {
        IDQLSelectableQuery distributionDirectionQuery = new DQLSelectBuilder().fromEntity(EcgDistributionQuota.class, "q")
        .column(property(EcgDistributionQuota.direction().id().fromAlias("q")))
        .where(eq(property(EcgDistributionQuota.distribution().fromAlias("q")), value(distributionDetail.getDistribution()))).buildQuery();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "e");

        // состояние к зачислению и не архивные
        builder.where(eq(property(RequestedEnrollmentDirection.state().fromAlias("e")), commonValue(getByNaturalId(new EntrantStateGen.NaturalId(UniecDefines.ENTRANT_STATE_TOBE_ENROLED_CODE)))));
        builder.where(eq(property(RequestedEnrollmentDirection.entrantRequest().entrant().archival().fromAlias("e")), value(Boolean.FALSE)));

        // учитываем конфигурацию распределения
        builder.where(eq(property(RequestedEnrollmentDirection.compensationType().fromAlias("e")), value(distributionDetail.getConfig().getCompensationType())));
        builder.where(in(property(RequestedEnrollmentDirection.enrollmentDirection().id().fromAlias("e")), distributionDirectionQuery));
        builder.where(in(property(RequestedEnrollmentDirection.studentCategory().id().fromAlias("e")), getStudentCategoriesForDistribution(distributionDetail)));

        // в уточняющее распределение нужно брать только с оригиналами (на текущий момент)
        builder.where(eq(property(RequestedEnrollmentDirection.originalDocumentHandedIn().fromAlias("e")), value(Boolean.TRUE)));

        // в уточняющее распределение нужно брать только рекомендованных с оригиналыми из основного утвержденного распределения
        DQLSelectBuilder recommendedBuilder = new DQLSelectBuilder().fromEntity(EcgEntrantRecommended.class, "r")
        .joinEntity("r", DQLJoinType.inner, EcgEntrantRecommendedState.class, "state",
            and(
                eq(property(EcgEntrantRecommended.id().fromAlias("r")), property(EcgEntrantRecommendedState.recommended().id().fromAlias("state"))),
                eq(property(EcgEntrantRecommendedState.bringOriginal().fromAlias("state")), value(Boolean.TRUE))
            ))
            .column(property(EcgEntrantRecommended.direction().entrantRequest().entrant().id().fromAlias("r")))
            .where(eq(property(EcgEntrantRecommended.distribution().fromAlias("r")), value(distributionDetail.getDistribution())))
            .where(eq(property(EcgEntrantRecommended.distribution().state().fromAlias("r")), value(this.<EcgDistributionState>getByNaturalId(new EcgDistributionStateGen.NaturalId(EcgDistributionStateCodes.APPROVED)))));

        // если рекомендован по цп в основном, то в уточняющем можно рекомендовать только по цп
        // если рекомендован не по цп в основном, то в уточняющем можно рекомендовать только не по цп
        switch (rateRule)
        {
            case RULE_TARGET_ADMISSION:
                recommendedBuilder.where(isNotNull(property(EcgEntrantRecommended.targetAdmissionKind().fromAlias("r"))));
                break;
            default:
                recommendedBuilder.where(isNull(property(EcgEntrantRecommended.targetAdmissionKind().fromAlias("r"))));
                break;
        }

        builder.where(in(property(RequestedEnrollmentDirection.entrantRequest().entrant().id().fromAlias("e")), recommendedBuilder.buildQuery()));

        // исключаем тех абитуриентов, кто уже рекомендован в это уточняющее распределение
        builder.where(notIn(property(RequestedEnrollmentDirection.entrantRequest().entrant().id().fromAlias("e")), new DQLSelectBuilder().fromEntity(EcgEntrantRecommendedDetail.class, "rd")
            .column(property(EcgEntrantRecommendedDetail.direction().id().fromAlias("rd")))
            .where(eq(property(EcgEntrantRecommendedDetail.distribution().fromAlias("rd")), value(distributionDetail)))
            .buildQuery()
        ));

        // исключаем тех абитуриентов, кто предзачислен хотя бы одному нпп из распределения с учетом конфигурации
        builder.where(notIn(property(RequestedEnrollmentDirection.entrantRequest().entrant().id().fromAlias("e")), getPreStudentQuery(distributionDetail)));

        return builder;
    }

    private Collection<Long> getStudentCategoriesForDistribution(IEcgDistribution distribution)
    {
        return distribution.getConfig().isSecondHighAdmission() ?
            Collections.singletonList(this.<StudentCategory>getByNaturalId(new StudentCategoryGen.NaturalId(UniDefines.STUDENT_CATEGORY_SECOND_HIGH)).getId()) :
                Arrays.asList(this.<StudentCategory>getByNaturalId(new StudentCategoryGen.NaturalId(UniDefines.STUDENT_CATEGORY_STUDENT)).getId(), this.<StudentCategory>getByNaturalId(new StudentCategoryGen.NaturalId(UniDefines.STUDENT_CATEGORY_LISTENER)).getId());
    }

    private IDQLSelectableQuery getEnrollmentDirectionsForDistribution(IEcgDistribution distribution)
    {
        return new DQLSelectBuilder().fromEntity(EcgDistributionQuota.class, "__q")
        .column(property(EcgDistributionQuota.direction().id().fromAlias("__q")))
        .where(eq(property(EcgDistributionQuota.distribution().fromAlias("__q")), value(distribution.getDistribution())))
        .buildQuery();
    }

    private IDQLSelectableQuery getPreStudentQuery(IEcgDistribution distribution)
    {
        return new DQLSelectBuilder().fromEntity(PreliminaryEnrollmentStudent.class, "p1")
        .column(property(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().entrantRequest().entrant().id().fromAlias("p1")))
        .where(eq(property(PreliminaryEnrollmentStudent.compensationType().fromAlias("p1")), value(distribution.getConfig().getCompensationType())))
        .where(in(property(PreliminaryEnrollmentStudent.studentCategory().id().fromAlias("p1")), getStudentCategoriesForDistribution(distribution)))
        .where(in(property(PreliminaryEnrollmentStudent.educationOrgUnit().fromAlias("p1")), new DQLSelectBuilder().fromEntity(EcgDistributionQuota.class, "q1")
            .column(property(EcgDistributionQuota.direction().educationOrgUnit().fromAlias("q1")))
            .where(eq(property(EcgDistributionQuota.distribution().fromAlias("q1")), value(distribution.getDistribution())))
            .buildQuery()
        )).buildQuery();
    }
}
