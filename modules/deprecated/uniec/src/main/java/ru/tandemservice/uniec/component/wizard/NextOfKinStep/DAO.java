/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.wizard.NextOfKinStep;

import org.hibernate.Session;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.bo.AddressBase.AddressBaseManager;
import org.tandemframework.shared.fias.base.bo.Fias.FiasManager;
import org.tandemframework.shared.fias.base.entity.Address;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.person.base.bo.Person.util.IssuancePlaceSelectModel;
import org.tandemframework.shared.person.base.bo.Person.util.WorkPlaceSelectModel;
import org.tandemframework.shared.person.base.bo.Person.util.WorkPostSelectModel;
import org.tandemframework.shared.person.base.entity.NotUsedRelationDegree;
import org.tandemframework.shared.person.base.entity.PersonNextOfKin;
import org.tandemframework.shared.person.catalog.entity.RelationDegree;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;

import java.util.*;

/**
 * @author vip_delete
 * @since 15.06.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setEntrant(getNotNull(Entrant.class, model.getEntrant().getId()));
        model.setPersonNextOfKinList(getList(PersonNextOfKin.class, PersonNextOfKin.L_PERSON, model.getEntrant().getPerson(), PersonNextOfKin.P_ID));

        // типы родственных связей
        MQBuilder builder = new MQBuilder(RelationDegree.ENTITY_CLASS, "r");
        builder.add(MQExpression.notIn("r", RelationDegree.P_ID, new MQBuilder(NotUsedRelationDegree.ENTITY_CLASS, "n", new String[] { NotUsedRelationDegree.L_RELATION_DEGREE + ".id" }).add(MQExpression.eq("n", NotUsedRelationDegree.P_PERSON_ROLE_NAME, "Entrant"))));
        builder.addOrder("r", RelationDegree.P_TITLE);
        model.getKinModel().setRelationDegreeList(builder.<RelationDegree> getResultList(getSession()));
        model.getKinModel().setIssuancePlaceModel(new IssuancePlaceSelectModel());
        model.getKinModel().setWorkPlaceModel(new WorkPlaceSelectModel());
        model.getKinModel().setWorkPostModel(new WorkPostSelectModel());

        Long onlineEntrantId = model.getOnlineEntrant().getId();
        if (onlineEntrantId != null)
        {
            model.setOnlineEntrant(get(OnlineEntrant.class, onlineEntrantId));
            prepareOnlineEntrantNextOfKin(model);
        }
    }

    @Override
    public void deleteNextOfKin(Model model, Long id)
    {
        List<PersonNextOfKin> list = model.getPersonNextOfKinList();
        int i = 0;
        while (i < list.size() && !list.get(i).getId().equals(id))
        {
            i++;
        }
        if (i == list.size())
        {
            return; // id не найден
        }
        list.remove(i);

        if (id > 0)
        {
            PersonNextOfKin personNextOfKin = get(PersonNextOfKin.class, id);
            if (personNextOfKin != null)
            {
                AddressBase address = personNextOfKin.getAddress();
                getSession().delete(personNextOfKin);
                if (address != null)
                {
                    getSession().delete(address);
                }
            }
        }
    }

    @Override
    public void prepareSelectNextOfKin(Model model)
    {
        // находим минимальный id
        long minId = 0;
        for (PersonNextOfKin personNextOfKin : model.getPersonNextOfKinList())
        {
            minId = Math.min(minId, personNextOfKin.getId());
        }

        PersonNextOfKin personNextOfKin = model.getKinModel().getNextOfKin();
        personNextOfKin.setId(minId - 1);
        personNextOfKin.setPerson(model.getEntrant().getPerson());
        if (model.getKinModel().getAddress() != null)
        {
            personNextOfKin.setAddress(model.getKinModel().getAddress());
        }
        model.getPersonNextOfKinList().add(personNextOfKin);
        model.getKinModel().setNextOfKin(new PersonNextOfKin());
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        Map<RelationDegree, Set<PersonNextOfKin>> degreeMap = new HashMap<RelationDegree, Set<PersonNextOfKin>>();
        for (PersonNextOfKin personNextOfKin : model.getPersonNextOfKinList())
        {
            Set<PersonNextOfKin> byDegree = degreeMap.get(personNextOfKin.getRelationDegree());
            if (null == byDegree)
                degreeMap.put(personNextOfKin.getRelationDegree(), byDegree = new HashSet<PersonNextOfKin>());
            byDegree.add(personNextOfKin);
        }
        for (RelationDegree degree : degreeMap.keySet())
        {
            if (degree.getQuantity() == null) continue;
            if (degreeMap.get(degree).size() > degree.getQuantity())
                errors.add("Со степенью родства «" + degree.getTitle() + "» может быть добавлено родственников не более " + degree.getQuantity() + ".");
        }
    }

    @Override
    public void update(Model model)
    {
        final Session session = getSession();
        for (PersonNextOfKin personNextOfKin : model.getPersonNextOfKinList())
        {
            if (personNextOfKin.getId() < 0)
            {
                personNextOfKin.setId(null);
                if (personNextOfKin.getAddress() != null)
                {
                    AddressBase addressBase = personNextOfKin.getAddress();
                    personNextOfKin.setAddress(null);
                    AddressBaseManager.instance().dao().saveOrUpdateAddressWithoutEntityUpdate(personNextOfKin, addressBase, PersonNextOfKin.L_ADDRESS, true);
                }
                session.save(personNextOfKin);
            }
        }
    }

    /**
     * Подготавливает данные ближайшего родственника по информации из онлайн-абитуриента
     * 
     * @param model модель
     */
    private void prepareOnlineEntrantNextOfKin(Model model)
    {
        OnlineEntrant onlineEntrant = model.getOnlineEntrant();
        PersonNextOfKin personNextOfKin = model.getKinModel().getNextOfKin();

        RelationDegree relationDegree = onlineEntrant.getNextOfKinRelationDegree();
        if (relationDegree != null)
        {
            personNextOfKin.setRelationDegree(relationDegree);
            personNextOfKin.setLastName(onlineEntrant.getNextOfKinLastName());
            personNextOfKin.setFirstName(onlineEntrant.getNextOfKinFirstName());
            personNextOfKin.setMiddleName(onlineEntrant.getNextOfKinMiddleName());
            personNextOfKin.setEmploymentPlace(onlineEntrant.getNextOfKinWorkPlace());
            personNextOfKin.setPost(onlineEntrant.getNextOfKinWorkPost());
            personNextOfKin.setPhones(onlineEntrant.getNextOfKinPhone());

            prepareSelectNextOfKin(model);
        }
    }
}
