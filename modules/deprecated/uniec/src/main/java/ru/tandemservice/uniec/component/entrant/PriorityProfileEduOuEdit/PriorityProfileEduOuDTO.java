/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec.component.entrant.PriorityProfileEduOuEdit;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.uniec.entity.entrant.ProfileEducationOrgUnit;

/**
 * @author Vasily Zhukov
 * @since 27.04.2012
 */
public class PriorityProfileEduOuDTO extends DataWrapper
{
    private ProfileEducationOrgUnit _profileEducationOrgUnit;
    private Integer _priority;

    public ProfileEducationOrgUnit getProfileEducationOrgUnit()
    {
        return _profileEducationOrgUnit;
    }

    public void setProfileEducationOrgUnit(ProfileEducationOrgUnit profileEducationOrgUnit)
    {
        _profileEducationOrgUnit = profileEducationOrgUnit;
    }

    public Integer getPriority()
    {
        return _priority;
    }

    public void setPriority(Integer priority)
    {
        _priority = priority;
    }
}
