package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uniec.entity.entrant.EntrantExamList;
import ru.tandemservice.uniec.entity.entrant.IEntrant;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Экзаменационный лист абитуриента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntrantExamListGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.EntrantExamList";
    public static final String ENTITY_NAME = "entrantExamList";
    public static final int VERSION_HASH = 124918824;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT = "entrant";
    public static final String P_FORM_DATE = "formDate";
    public static final String P_GROUP = "group";

    private IEntrant _entrant;     // Абстрактный абитуриент
    private Date _formDate;     // Дата выдачи
    private String _group;     // Группа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Абстрактный абитуриент. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public IEntrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant Абстрактный абитуриент. Свойство не может быть null и должно быть уникальным.
     */
    public void setEntrant(IEntrant entrant)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && entrant!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IEntrant.class);
            IEntityMeta actual =  entrant instanceof IEntity ? EntityRuntime.getMeta((IEntity) entrant) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Дата выдачи.
     */
    public Date getFormDate()
    {
        return _formDate;
    }

    /**
     * @param formDate Дата выдачи.
     */
    public void setFormDate(Date formDate)
    {
        dirty(_formDate, formDate);
        _formDate = formDate;
    }

    /**
     * @return Группа.
     */
    @Length(max=255)
    public String getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа.
     */
    public void setGroup(String group)
    {
        dirty(_group, group);
        _group = group;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntrantExamListGen)
        {
            setEntrant(((EntrantExamList)another).getEntrant());
            setFormDate(((EntrantExamList)another).getFormDate());
            setGroup(((EntrantExamList)another).getGroup());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntrantExamListGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntrantExamList.class;
        }

        public T newInstance()
        {
            return (T) new EntrantExamList();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entrant":
                    return obj.getEntrant();
                case "formDate":
                    return obj.getFormDate();
                case "group":
                    return obj.getGroup();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entrant":
                    obj.setEntrant((IEntrant) value);
                    return;
                case "formDate":
                    obj.setFormDate((Date) value);
                    return;
                case "group":
                    obj.setGroup((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entrant":
                        return true;
                case "formDate":
                        return true;
                case "group":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entrant":
                    return true;
                case "formDate":
                    return true;
                case "group":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entrant":
                    return IEntrant.class;
                case "formDate":
                    return Date.class;
                case "group":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntrantExamList> _dslPath = new Path<EntrantExamList>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntrantExamList");
    }
            

    /**
     * @return Абстрактный абитуриент. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantExamList#getEntrant()
     */
    public static IEntrantGen.Path<IEntrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Дата выдачи.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantExamList#getFormDate()
     */
    public static PropertyPath<Date> formDate()
    {
        return _dslPath.formDate();
    }

    /**
     * @return Группа.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantExamList#getGroup()
     */
    public static PropertyPath<String> group()
    {
        return _dslPath.group();
    }

    public static class Path<E extends EntrantExamList> extends EntityPath<E>
    {
        private IEntrantGen.Path<IEntrant> _entrant;
        private PropertyPath<Date> _formDate;
        private PropertyPath<String> _group;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Абстрактный абитуриент. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantExamList#getEntrant()
     */
        public IEntrantGen.Path<IEntrant> entrant()
        {
            if(_entrant == null )
                _entrant = new IEntrantGen.Path<IEntrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Дата выдачи.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantExamList#getFormDate()
     */
        public PropertyPath<Date> formDate()
        {
            if(_formDate == null )
                _formDate = new PropertyPath<Date>(EntrantExamListGen.P_FORM_DATE, this);
            return _formDate;
        }

    /**
     * @return Группа.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantExamList#getGroup()
     */
        public PropertyPath<String> group()
        {
            if(_group == null )
                _group = new PropertyPath<String>(EntrantExamListGen.P_GROUP, this);
            return _group;
        }

        public Class getEntityClass()
        {
            return EntrantExamList.class;
        }

        public String getEntityName()
        {
            return "entrantExamList";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
