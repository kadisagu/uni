/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.ui.EntrantAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.uniec.base.bo.EcDistribution.EcDistributionManager;
import ru.tandemservice.uniec.base.bo.EcDistribution.logic.EcSimpleDSHandler;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.*;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import java.util.*;

/**
 * @author Vasily Zhukov
 * @since 12.07.2011
 */
@Input({
    @Bind(key = EcDistributionEntrantAddUI.DISTRIBUTION, binding = EcDistributionEntrantAddUI.DISTRIBUTION),
    @Bind(key = EcDistributionEntrantAddUI.RATE_RULE, binding = EcDistributionEntrantAddUI.RATE_RULE)
})
public class EcDistributionEntrantAddUI extends UIPresenter
{
    public static final String DISTRIBUTION = "distribution";
    public static final String RATE_RULE = "rateRule";

    private IEcgDistribution _distribution;
    private IEcgEntrantRateRowDTO.Rule _rateRule;
    private boolean _targetAdmission;                                      // true, добавляем целевиков
    private boolean _distributionPerDirection;                             // true, распределение по направлениям
    private String _title;                                                 // название страницы

    private int _minCount;                                                 // минимальное кол-во строк рейтинга, которые нужно показать на странице
    private boolean _hasMore;                                              // true, если показаны не все строки рейтинга
    private Map<Long, IEcgEntrantRateDirectionDTO> _chosenMap;             // абитуриент -> выбранное направление приема
    private Map<Long, List<IEcgEntrantRateDirectionDTO>> _chosen2listMap;  // абитуриент -> список возможных выбранных направлений приема
    private List<? extends IEcgEntrantRateRowDTO> _viewRateRowList;        // строки рейтинга, которые нужно отобразить

    private boolean _hasDefaultChecked;
    private Long _refreshAfterEntrantId;
    private Map<Long, Long> _chosenDirectionMap;                           // абитуриент -> выбранное направление приема (на момент POST)

    private List<? extends IEcgEntrantRateRowDTO> _rateList;               // список строк рейтинга
    private List<IEntity> _selectedRateRowList = new ArrayList<>(); // выбранные строки рейтинга
    private Set<Long> _chosenEntrantSet;

    @Override
    public void onComponentRefresh()
    {
        // для скорости запомним не изменяемые параметры

        _targetAdmission = _rateRule.equals(IEcgEntrantRateRowDTO.Rule.RULE_TARGET_ADMISSION);

        _distributionPerDirection = _distribution.getConfig().getEcgItem() instanceof EnrollmentDirection;

        switch (_rateRule)
        {
            case RULE_TARGET_ADMISSION:
                _title = "Добавление абитуриентов (поступающих по целевому приему) в распределение";
                break;
            case RULE_NON_COMPETITIVE:
                _title = "Добавление абитуриентов (поступающих без ВИ и вне конкурса) в распределение";
                break;
            case RULE_COMPETITIVE:
                _title = "Добавление абитуриентов (поступающих на общих основаниях) в распределение";
                break;
            case RULE_COMPETITIVE_WITH_ORIGINAL:
                _title = "Добавление абитуриентов (поступающих на общих основаниях) в распределение с оригиналами документов";
                break;
            default:
                throw new IllegalArgumentException("Unknown rateRule: " + _rateRule);
        }

        // пока не знаем сколько строк следует отобразить
        _minCount = -1;

        // по умолчанию ничего не выбрано
        _hasDefaultChecked = false;
        _refreshAfterEntrantId = null;

        // свободные места в распределении
        IEcgQuotaFreeDTO freeQuotaDTO = EcDistributionManager.instance().dao().getFreeQuotaDTO(_distribution);

        // получаем список строк рейтинга
        _rateList = EcDistributionManager.instance().dao().getEntrantRateRowList(_distribution, freeQuotaDTO.getQuotaDTO().getTaKindList(), _rateRule);

        _chosenEntrantSet = new HashSet<>();

        // обновляем рейтинг абитуриентов
        doRefreshEntrantRateList();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (EcDistributionEntrantAdd.ENTRANT_DS.equals(dataSource.getName()))
        {
            dataSource.put(EcSimpleDSHandler.LIST, _viewRateRowList);

            _chosenEntrantSet = new HashSet<>();
            for (IEntity entity : _selectedRateRowList)
                _chosenEntrantSet.add(entity.getId());
        }
    }

    private void doRefreshEntrantRateList()
    {
        // свободные места в распределении
        IEcgQuotaFreeDTO freeQuotaDTO = EcDistributionManager.instance().dao().getFreeQuotaDTO(_distribution);

        // получаем алгоритм заполнения планов приема
        IEcgQuotaManager quotaManager = new EcgQuotaManager(freeQuotaDTO);

        // каждому абитуриенту выберем подходящее направление приема
        // мап: абитуриент -> выбранное направление приема
        _chosenMap = new HashMap<>();

        // для распределения по конкурсным группам каждому выделенному абитуриенту сохраним
        // список возможных для выбора направлений
        _chosen2listMap = new HashMap<>();

        // общее число строк
        int len = _rateList.size();

        int i = 0;

        _selectedRateRowList.clear();

        boolean resetFlag = !_hasDefaultChecked;

        // общее число свободных мест
        int freeTotal = quotaManager.getFreeTotal();

        while (i < len && freeTotal > 0)
        {
            IEcgEntrantRateRowDTO rateRowDTO = _rateList.get(i);

            Long entrantId = rateRowDTO.getId();

            Long preferableDirectionId = null;

            boolean skip = false;

            if (_hasDefaultChecked)
            {
                if (_chosenEntrantSet.contains(entrantId))
                {
                    preferableDirectionId = resetFlag ? null : _chosenDirectionMap.get(entrantId);
                }
                else
                {
                    skip = true;
                }

                resetFlag |= entrantId.equals(_refreshAfterEntrantId);
            }

            if (!skip)
            {
                IEcgPlanChoiceResult choiceResult = quotaManager.getPossibleDirectionList(rateRowDTO, preferableDirectionId);

                IEcgEntrantRateDirectionDTO chosenDirection = choiceResult.getChosenDirection();

                if (chosenDirection != null)
                {
                    freeTotal--;
                    _chosenMap.put(entrantId, chosenDirection);
                    _chosen2listMap.put(entrantId, choiceResult.getPossibleDirectionList());
                    _selectedRateRowList.add(rateRowDTO);
                }
            }

            i++;
        }

        int count = i <= _minCount ? _minCount : i + 25;

        _hasMore = count < _rateList.size();

        _viewRateRowList = _hasMore ? _rateList.subList(0, count) : _rateList;

        _chosenDirectionMap = new HashMap<>();
    }

    public boolean isChecked()
    {
        Long entrantId = ((IEcgEntrantRateRowDTO) getConfig().getDataSource(EcDistributionEntrantAdd.ENTRANT_DS).getCurrent()).getId();
        return _chosenMap.containsKey(entrantId);
    }

    public boolean isRowChecked()
    {
        Long entrantId = ((IEcgEntrantRateRowDTO) getConfig().getDataSource(EcDistributionEntrantAdd.ENTRANT_DS).getCurrent()).getId();
        return _chosenEntrantSet.contains(entrantId);
    }

    public void setRowChecked(boolean rowChecked)
    {
        Long entrantId = ((IEcgEntrantRateRowDTO) getConfig().getDataSource(EcDistributionEntrantAdd.ENTRANT_DS).getCurrent()).getId();
        if (rowChecked)
            _chosenEntrantSet.add(entrantId);
        else
            _chosenEntrantSet.remove(entrantId);
    }

    // select

    public String getSelectId()
    {
        Long entrantId = ((IEcgEntrantRateRowDTO) getConfig().getDataSource(EcDistributionEntrantAdd.ENTRANT_DS).getCurrent()).getId();
        return "s_" + entrantId;
    }

    public IEcgEntrantRateDirectionDTO getDirection()
    {
        Long entrantId = ((IEcgEntrantRateRowDTO) getConfig().getDataSource(EcDistributionEntrantAdd.ENTRANT_DS).getCurrent()).getId();
        return _chosenMap.get(entrantId);
    }

    public void setDirection(IEcgEntrantRateDirectionDTO direction)
    {
        Long entrantId = ((IEcgEntrantRateRowDTO) getConfig().getDataSource(EcDistributionEntrantAdd.ENTRANT_DS).getCurrent()).getId();
        _chosenDirectionMap.put(entrantId, direction.getId());
    }

    public List<IEcgEntrantRateDirectionDTO> getDirectionList()
    {
        Long entrantId = ((IEcgEntrantRateRowDTO) getConfig().getDataSource(EcDistributionEntrantAdd.ENTRANT_DS).getCurrent()).getId();
        return _chosen2listMap.get(entrantId);
    }

    // Getters & Setters

    public IEcgDistribution getDistribution()
    {
        return _distribution;
    }

    public void setDistribution(IEcgDistribution distribution)
    {
        _distribution = distribution;
    }

    public IEcgEntrantRateRowDTO.Rule getRateRule()
    {
        return _rateRule;
    }

    public void setRateRule(IEcgEntrantRateRowDTO.Rule rateRule)
    {
        _rateRule = rateRule;
    }

    public boolean isTargetAdmission()
    {
        return _targetAdmission;
    }

    public void setTargetAdmission(boolean targetAdmission)
    {
        _targetAdmission = targetAdmission;
    }

    public boolean isDistributionPerDirection()
    {
        return _distributionPerDirection;
    }

    public void setDistributionPerDirection(boolean distributionPerDirection)
    {
        _distributionPerDirection = distributionPerDirection;
    }

    public String getTitle()
    {
        return _title;
    }

    public void setTitle(String title)
    {
        _title = title;
    }

    public int getMinCount()
    {
        return _minCount;
    }

    public void setMinCount(int minCount)
    {
        _minCount = minCount;
    }

    public boolean isHasMore()
    {
        return _hasMore;
    }

    public void setHasMore(boolean hasMore)
    {
        _hasMore = hasMore;
    }

    public Map<Long, IEcgEntrantRateDirectionDTO> getChosenMap()
    {
        return _chosenMap;
    }

    public void setChosenMap(Map<Long, IEcgEntrantRateDirectionDTO> chosenMap)
    {
        _chosenMap = chosenMap;
    }

    public Map<Long, List<IEcgEntrantRateDirectionDTO>> getChosen2listMap()
    {
        return _chosen2listMap;
    }

    public void setChosen2listMap(Map<Long, List<IEcgEntrantRateDirectionDTO>> chosen2listMap)
    {
        _chosen2listMap = chosen2listMap;
    }

    public List<? extends IEcgEntrantRateRowDTO> getViewRateRowList()
    {
        return _viewRateRowList;
    }

    public void setViewRateRowList(List<? extends IEcgEntrantRateRowDTO> viewRateRowList)
    {
        _viewRateRowList = viewRateRowList;
    }

    public boolean isHasDefaultChecked()
    {
        return _hasDefaultChecked;
    }

    public void setHasDefaultChecked(boolean hasDefaultChecked)
    {
        _hasDefaultChecked = hasDefaultChecked;
    }

    public Map<Long, Long> getChosenDirectionMap()
    {
        return _chosenDirectionMap;
    }

    public void setChosenDirectionMap(Map<Long, Long> chosenDirectionMap)
    {
        _chosenDirectionMap = chosenDirectionMap;
    }

    public List<? extends IEcgEntrantRateRowDTO> getRateList()
    {
        return _rateList;
    }

    public void setRateList(List<? extends IEcgEntrantRateRowDTO> rateList)
    {
        _rateList = rateList;
    }

    public List<IEntity> getSelectedRateRowList()
    {
        return _selectedRateRowList;
    }

    public void setSelectedRateRowList(List<IEntity> selectedRateRowList)
    {
        _selectedRateRowList = selectedRateRowList;
    }

    // Listeners

    public void onAddMore()
    {
        // показать на 25 строк больше, чем показывается сейчас
        _minCount = getConfig().getDataSource(EcDistributionEntrantAdd.ENTRANT_DS).getTotalSize() + 25;

        doRefreshEntrantRateList();
    }

    public void onRateRefresh()
    {
        // произошел выбор чекбокса или выбор из селекта
        // пока мы сидели на форме мог измениться план приема или еще что-нибудь влияющее на рейтинг
        // нужно пересчитать рейтинг, потом принудительно выделить тех абитуриентов и те направления,
        // которые пришли в POST'е, уменьшая при этом план, если нельзя выделить абитуриента или направление,
        // то пропускаем такого абитуриента, после чего пробежаться по всем не выделенным абитуриентам расставляя
        // им остатки по планам

        _hasDefaultChecked = true;

        // во всех селектах у абитуриентов после этого id следует сбросить значения (игнорировать POST данные)

        _refreshAfterEntrantId = getListenerParameterAsLong();

        // обновляем рейтинг
        doRefreshEntrantRateList();
    }

    public void onClickSave()
    {
        // сохраняем рекомендованных абитуриентов
        // пока мы сидели на форме, теоретически могло что угодно измениться: план, суммы баллов, наборы экзаменов
        // но мы все равно сохраняем ровно то, что выбрали на форме
        // превышение планов тут не проверяется, оно покажется уже в публикаторе распределения после сохранения

        // сохраняем распределенных абитуриентов
        if (_distributionPerDirection)
        {
            Set<Long> chosenDirectionIds = new HashSet<>();

            for (Long id : _chosenEntrantSet) {
                IEcgEntrantRateDirectionDTO rateDirectionDTO = _chosenMap.get(id);
                if (null != rateDirectionDTO) {
                    chosenDirectionIds.add(rateDirectionDTO.getId());
                }
            }

            EcDistributionManager.instance().dao().saveEntrantRecommendedList(_distribution, _targetAdmission, chosenDirectionIds);
        }
        else
        {
            EcDistributionManager.instance().dao().saveEntrantRecommendedList(_distribution, _targetAdmission, _chosenDirectionMap.values());
        }

        deactivate();
    }
}
