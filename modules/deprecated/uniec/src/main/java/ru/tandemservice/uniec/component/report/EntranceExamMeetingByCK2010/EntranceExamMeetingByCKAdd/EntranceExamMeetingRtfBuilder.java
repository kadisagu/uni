/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.report.EntranceExamMeetingByCK2010.EntranceExamMeetingByCKAdd;

import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.node.IRtfElement;

import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.examset.EnrollmentDirectionExamSetData;
import ru.tandemservice.uniec.util.EntrantDataUtil;

/**
 * @author Vasily Zhukov
 * @since 04.08.2010
 */
interface EntranceExamMeetingRtfBuilder
{
    RtfDocument getTemplate();

    void init(Session session, Model model, Map<EnrollmentDirection, EnrollmentDirectionExamSetData> examSetDataMap, EntrantDataUtil dataUtil, MQBuilder directionBuilder, Map<Integer, CompetitionKind> priority2CompetitionKind);

    List<IRtfElement> buildReportPart(EnrollmentDirectionGroup group,
                                      Map<CompetitionKind, List<ReportRow>> competitionKind2RowList,
                                      boolean structuresEqualInWholeGroup,
                                      List<String> typeTitles,
                                      List<String[]> titles,
                                      boolean useIndProgress);
}
