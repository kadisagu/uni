/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.RequestedEnrollmentDirectionsChangePriority;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.unibase.UniBaseUtils;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

/**
 * @author ekachanova
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        prepareListDataSource(component);

        getDao().prepare(getModel(component));
    }

    @SuppressWarnings({"unchecked"})
    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<RequestedEnrollmentDirection> dataSource = UniBaseUtils.createDataSource(component, getDao());

        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().displayableTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Формирующее подр.", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().formativeOrgUnit().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Территориальное подр.", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().territorialOrgUnit().territorialTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Форма освоения", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developForm().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("ЦП", RequestedEnrollmentDirection.P_TARGET_ADMISSION, YesNoFormatter.INSTANCE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Вид конкурса", RequestedEnrollmentDirection.competitionKind().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Контракт", RequestedEnrollmentDirection.compensationType().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn(Model.PRIORITY_COLUMN, "Приоритет").setClickable(false).setOrderable(false));
        model.setDataSource(dataSource);
    }

    public void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().update(model);

        deactivate(component);
    }
}
