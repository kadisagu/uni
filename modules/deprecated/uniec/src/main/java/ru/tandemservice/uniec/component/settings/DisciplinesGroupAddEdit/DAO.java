/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.settings.DisciplinesGroupAddEdit;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IIdentifiableWrapper;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.entity.settings.DisciplinesGroup;
import ru.tandemservice.uniec.entity.settings.Group2DisciplineRelation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author agolubenko
 * @since 17.06.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        if (model.getDisciplinesGroup().getId() != null)
        {
            DisciplinesGroup group = get(DisciplinesGroup.class, model.getDisciplinesGroup().getId());
            model.setDisciplinesGroup(group);
            List<IIdentifiableWrapper> options = new ArrayList<>();
            for (Discipline2RealizationWayRelation rel : group.getDisciplines())
                options.add(new IdentifiableWrapper(rel.getId(), rel.getTitle()));
            model.setDisciplinesList(options);
            model.setEnrollmentCampaign(group.getEnrollmentCampaign());
        } else
        {
            model.setEnrollmentCampaign(get(EnrollmentCampaign.class, model.getEnrollmentCampaign().getId()));
        }

        List<Discipline2RealizationWayRelation> result = getList(Discipline2RealizationWayRelation.class, Discipline2RealizationWayRelation.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign());
        Collections.sort(result, ITitled.TITLED_COMPARATOR);
        List<IIdentifiableWrapper> options = new ArrayList<>();
        for (Discipline2RealizationWayRelation rel : result)
            options.add(new IdentifiableWrapper(rel.getId(), rel.getTitle()));

        model.setDisciplinesSelectModel(new LazySimpleSelectModel<>(options));
    }

    @SuppressWarnings("unchecked")
    @Override
    public void update(Model model)
    {
        Session session = getSession();

        DisciplinesGroup group = model.getDisciplinesGroup();
        List<Discipline2RealizationWayRelation> disciplinesList = new ArrayList<>();
        for (IIdentifiableWrapper wrapper : model.getDisciplinesList())
            disciplinesList.add(getNotNull(Discipline2RealizationWayRelation.class, wrapper.getId()));

        ArrayList<Discipline2RealizationWayRelation> disciplines = new ArrayList<>(disciplinesList);
        if (disciplines.isEmpty())
            throw new ApplicationException("В группе должна быть хотя бы одна дисциплина.");

        String queryStr = "select relation." + Group2DisciplineRelation.L_GROUP + ".id from " + Group2DisciplineRelation.ENTITY_CLASS + " relation " + "where relation." + Group2DisciplineRelation.L_DISCIPLINE + " in (";
        for (Discipline2RealizationWayRelation relation : disciplines)
            queryStr += relation.getId() + ",";

        queryStr = queryStr.substring(0, queryStr.length() - 1) + ") " + "and relation." + Group2DisciplineRelation.L_GROUP + " in " + "(select g2dr." + Group2DisciplineRelation.L_GROUP + ".id from " + Group2DisciplineRelation.ENTITY_CLASS + " g2dr " + "group by " + "g2dr." + Group2DisciplineRelation.L_GROUP + ".id " + "having count(*) = :count) " + "group by relation." + Group2DisciplineRelation.L_GROUP + ".id having count(*) = :count";
        if(group.getId() != null)
        {
            queryStr += " and relation." + Group2DisciplineRelation.L_GROUP + ".id != :id";
        }
        Query query = session.createQuery(queryStr);
        query.setLong("count", (long) disciplines.size());
        if(group.getId() != null)
        {
            query.setLong("id", group.getId());
        }
        if(query.list().size() > 0)
            throw new ApplicationException("Группа с данным набором дисциплин уже существует.");

        group.setEnrollmentCampaign(model.getEnrollmentCampaign());
        session.saveOrUpdate(group);

        for (Discipline2RealizationWayRelation discipline : group.getDisciplines())
        {
            if (disciplinesList.contains(discipline))
            {
                disciplinesList.remove(discipline);
            } else
            {
                Criteria criteria = session.createCriteria(Group2DisciplineRelation.class, "e");
                criteria.add(Restrictions.eq(Group2DisciplineRelation.L_GROUP, group));
                criteria.add(Restrictions.eq(Group2DisciplineRelation.L_DISCIPLINE, discipline));
                for (Group2DisciplineRelation relation : (List<Group2DisciplineRelation>) criteria.list())
                {
                    session.delete(relation);
                }
            }
        }

        for (Discipline2RealizationWayRelation discipline : disciplinesList)
        {
            Group2DisciplineRelation relation = new Group2DisciplineRelation();
            relation.setGroup(group);
            relation.setDiscipline(discipline);
            session.save(relation);
        }
    }
}
