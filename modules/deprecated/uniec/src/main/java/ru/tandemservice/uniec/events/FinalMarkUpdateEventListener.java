/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.events;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Synchronization;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.event.single.FilteredSingleEntityEventListener;
import org.tandemframework.hibsupport.event.single.ISingleEntityEvent;
import org.tandemframework.hibsupport.event.single.type.HibernateSaveEvent;

import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline;
import ru.tandemservice.uniec.entity.entrant.Discipline2OlympiadDiplomaRelation;
import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantOriginalDocumentRelation;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate;
import ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline;
import ru.tandemservice.uniec.entity.entrant.ExamPassMark;
import ru.tandemservice.uniec.entity.entrant.ExamPassMarkAppeal;
import ru.tandemservice.uniec.entity.entrant.ExcludeSubjectPassForm;
import ru.tandemservice.uniec.entity.entrant.IEntrant;
import ru.tandemservice.uniec.entity.entrant.InterviewResult;
import ru.tandemservice.uniec.entity.entrant.OlympiadDiploma;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.StateExamSubjectMark;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;

/**
 * @author vip_delete
 * @since 27.03.2009
 */
public class FinalMarkUpdateEventListener extends FilteredSingleEntityEventListener<ISingleEntityEvent>
{
    private static final ThreadLocal<FinalMarkSyncObject> syncs = new ThreadLocal<FinalMarkSyncObject>();

    @Override
    public synchronized void onFilteredEvent(final ISingleEntityEvent event)
    {
        // считать финальные оценки по выбранным вступительным испытаниям надо 1 раз в конце транзакции
        FinalMarkSyncObject sync = syncs.get();

        if (sync == null)
        {
            final Session session = event.getSession();
            sync = new FinalMarkSyncObject(session);
            syncs.set(sync);
            session.getTransaction().registerSynchronization(sync);
        }

        sync.add(event);
    }

    private static final class FinalMarkSyncObject implements Synchronization
    {
        private Session _session;
        private List<Entrant> _entrantList = new ArrayList<Entrant>(); // добавили этих абитуриентов
        private Set<Long> _entrantIds = new HashSet<Long>();           // идентификаторы измененных абитуриентов

        private FinalMarkSyncObject(Session session)
        {
            _session = session;
        }

        private void add(ISingleEntityEvent event)
        {
            IEntity entity = event.getEntity();
            if (entity instanceof ExamPassMark)
            {
                IEntrant ientrant = ((ExamPassMark) entity).getExamPassDiscipline().getEntrantExamList().getEntrant();
                _entrantIds.add(ientrant instanceof Entrant ? ((Entrant) ientrant).getId() : ((EntrantRequest) ientrant).getEntrant().getId());
            } else if (entity instanceof ExamPassMarkAppeal)
            {
                IEntrant ientrant = ((ExamPassMarkAppeal) entity).getExamPassMark().getExamPassDiscipline().getEntrantExamList().getEntrant();
                _entrantIds.add(ientrant instanceof Entrant ? ((Entrant) ientrant).getId() : ((EntrantRequest) ientrant).getEntrant().getId());
            } else if (entity instanceof ExamPassDiscipline)
            {
                IEntrant ientrant = ((ExamPassDiscipline) entity).getEntrantExamList().getEntrant();
                _entrantIds.add(ientrant instanceof Entrant ? ((Entrant) ientrant).getId() : ((EntrantRequest) ientrant).getEntrant().getId());
            } else if (entity instanceof OlympiadDiploma)
            {
                _entrantIds.add(((OlympiadDiploma) entity).getEntrant().getId());
            } else if (entity instanceof Discipline2OlympiadDiplomaRelation)
            {
                _entrantIds.add(((Discipline2OlympiadDiplomaRelation) entity).getDiploma().getEntrant().getId());
            } else if (entity instanceof EntrantStateExamCertificate)
            {
                _entrantIds.add(((EntrantStateExamCertificate) entity).getEntrant().getId());
            } else if (entity instanceof StateExamSubjectMark)
            {
                _entrantIds.add(((StateExamSubjectMark) entity).getCertificate().getEntrant().getId());
            } else if (entity instanceof ChosenEntranceDiscipline)
            {
                _entrantIds.add(((ChosenEntranceDiscipline) entity).getChosenEnrollmentDirection().getEntrantRequest().getEntrant().getId());
            } else if (entity instanceof EntranceDiscipline)
            {
                _entrantIds.addAll(getEntrants((EntranceDiscipline) entity));
            } else if (entity instanceof RequestedEnrollmentDirection)
            {
                _entrantIds.add(((RequestedEnrollmentDirection) entity).getEntrantRequest().getEntrant().getId());
            } else if (entity instanceof PreliminaryEnrollmentStudent)
            {
                _entrantIds.add((((PreliminaryEnrollmentStudent) entity).getRequestedEnrollmentDirection()).getEntrantRequest().getEntrant().getId());
            } else if (entity instanceof EnrollmentExtract)
            {
                _entrantIds.add((((EnrollmentExtract) entity).getEntity().getRequestedEnrollmentDirection()).getEntrantRequest().getEntrant().getId());
            } else if (entity instanceof InterviewResult)
            {
                _entrantIds.add((((InterviewResult) entity).getRequestedEnrollmentDirection()).getEntrantRequest().getEntrant().getId());
            } else if (entity instanceof EntrantRequest)
            {
                _entrantIds.add(((EntrantRequest) entity).getEntrant().getId());
            } else if (entity instanceof Entrant)
            {
                if (event instanceof HibernateSaveEvent)
                    _entrantList.add((Entrant) entity);
                else
                    _entrantIds.add(entity.getId());
            } else if (entity instanceof EntrantOriginalDocumentRelation)
            {
                _entrantIds.add(((EntrantOriginalDocumentRelation) entity).getEntrant().getId());
            } else if (entity instanceof ExcludeSubjectPassForm)
            {
                _entrantIds.add(((ExcludeSubjectPassForm) entity).getChosenEntranceDiscipline().getChosenEnrollmentDirection().getEntrantRequest().getEntrant().getId());
            } else
                throw new RuntimeException("You cannot update entrant final mark by class '" + entity.getClass().getSimpleName() + "'. Not implemented!");
        }

        @Override
        public void beforeCompletion()
        {
            // у добавленных абитуриентов должны уже были появится идентификаторы
            for (Entrant entrant : _entrantList)
                _entrantIds.add(entrant.getId());

            // тут надо бы скопировать мн-во абитуриентов, так как во время пересчета финальных оценок
            // будет вызываться onFilteredEvent, что не очень хорошо
            // однако все абитуриенты, которые будут добавляться в _entitySet уже там находятся, так как
            // мн-во entrantIds замкнуто относительно операции пересчета финальных оценок
            UniecDAOFacade.getEntrantDAO().updateEntrantData(_entrantIds);

            // этот метод вызывается уже после того как hibernate сделал flush, см. метод JDBCTransaction.commmit() вызов notifyLocalSynchsBeforeTransactionCompletion идет после transactionContext.managedFlush()
            // поэтому если мы что-то поменяли в объектах, надо еще раз сделать flush руками
            _session.flush();
        }

        @Override
        public void afterCompletion(int arg)
        {
            syncs.remove();
        }

        @SuppressWarnings("unchecked")
        private List<Long> getEntrants(EntranceDiscipline entranceDiscipline)
        {
            Criteria criteria = _session.createCriteria(RequestedEnrollmentDirection.class);
            criteria.createAlias(RequestedEnrollmentDirection.entrantRequest().s(), "entrantRequest");
            criteria.add(Restrictions.eq(RequestedEnrollmentDirection.studentCategory().s(), entranceDiscipline.getStudentCategory()));
            criteria.add(Restrictions.eq(RequestedEnrollmentDirection.enrollmentDirection().s(), entranceDiscipline.getEnrollmentDirection()));
            criteria.setProjection(Projections.property(EntrantRequest.entrant().id().fromAlias("entrantRequest").s()));
            return criteria.list();
        }
    }
}
