package ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.Included;

import ru.tandemservice.uni.dao.IPrepareable;

/**
 * @author vdanilov
 */
public interface IDAO extends IPrepareable<Model>
{
    byte[] createPrintModelReport(Model model);

    byte[] createPrintReservedReport(Model model);

    public byte[] createPrintListReport(Model model);

    void delete(Model model, Long entrantId);
}
