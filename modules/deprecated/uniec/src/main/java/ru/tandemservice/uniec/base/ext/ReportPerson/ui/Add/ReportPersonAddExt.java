package ru.tandemservice.uniec.base.ext.ReportPerson.ui.Add;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.person.base.bo.ReportPerson.ui.Add.ReportPersonAdd;
import ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.EcReportPersonAdd;

/**
 * @author Vasily Zhukov
 * @since 08.02.2011
 */
@Configuration
public class ReportPersonAddExt extends BusinessComponentExtensionManager
{
    // first level tabs
    public static final String ENTRANT_TAB = "entrantTab";
    public static final String REPORT_PERSON_ADD_EXT_UI = "reportPersonAddExtUI";

    @Autowired
    private ReportPersonAdd _reportPersonAdd;

    @Bean
    public TabPanelExtension tabPanelExtension()
    {
        return tabPanelExtensionBuilder(_reportPersonAdd.tabPanelExtPoint())
                .addTab(componentTab(ENTRANT_TAB, EcReportPersonAdd.class).visible("mvel:presenter.isTabVisible('" + ENTRANT_TAB + "')"))
                .create();
    }

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_reportPersonAdd.presenterExtPoint())
                .addAddon(uiAddon(REPORT_PERSON_ADD_EXT_UI, ReportPersonAddExtUI.class))
                .create();
    }
}