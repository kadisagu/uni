/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantAdd;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.utils.AddressBaseUtils;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uniec.base.bo.EcCampaign.EcCampaignManager;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.Calendar;
import java.util.Date;

/**
 * @author agolubenko
 * @since 16.05.2008
 */
public class DAO extends org.tandemframework.shared.person.base.bo.Person.util.AbstractPersonRoleAdd.DAO<Entrant, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        Entrant entrant = model.getEntrant();
        entrant.setRegistrationDate(new Date());
        EntrantFilterUtil.prepareEnrollmentCampaignFilterWithSecurity(model, getSession());

        Long onlineEntrantId = model.getOnlineEntrant().getId();
        if (onlineEntrantId != null)
        {
            model.setOnlineEntrant(get(OnlineEntrant.class, onlineEntrantId));
            prepareOnlineEntrantIdentityCard(model);
        }

        Long enrollmentCampaignId = model.getEnrollmentCampaignId();
        if (enrollmentCampaignId != null)
        {
            model.setEnrollmentCampaign(get(EnrollmentCampaign.class, enrollmentCampaignId));
        }
    }

    @Override
    public void create(Model model)
    {
        Person person = model.getPerson();
        IdentityCard identityCard = person.getIdentityCard();

        PersonManager.instance().dao().checkPersonUnique(person);

        DatabaseFile photo = new DatabaseFile();
        getSession().save(photo);

        identityCard.setPhoto(photo);
        getSession().save(identityCard);

        getSession().save(person.getContactData());

        getSession().save(person);

        identityCard.setPerson(person);
        getSession().update(identityCard);

        createOnBasis(model.getEntrant(), person);
    }

    @Override
    public void createOnBasis(Model model)
    {
        createOnBasis(model.getEntrant(), getBasisPerson(model));
    }

    private void createOnBasis(Entrant entrant, Person person)
    {
        for (Entrant baseEntrant : getList(Entrant.class, Entrant.L_PERSON, person))
            if (baseEntrant.getEnrollmentCampaign().equals(entrant.getEnrollmentCampaign()))
                throw new ApplicationException("Данная персона уже является абитуриентом в выбранной приемной кампании.");

        entrant.setPersonalNumber(EcCampaignManager.instance().dao().getUniqueEntrantNumber(entrant.getEnrollmentCampaign()));
        entrant.setPerson(person);
        getSession().save(entrant);
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        IdentityCard identityCard = model.getPerson().getIdentityCard();
        int amount = -13;
        if (identityCard.getBirthDate() != null && identityCard.getBirthDate().after(CoreDateUtils.add(CoreDateUtils.getDayFirstTimeMoment(new Date()), Calendar.YEAR, amount)))
            errors.add("Абитуриент должен быть старше 13 лет.", "birthDate");

        PersonManager.instance().dao().checkIdentityCard(model.getPerson().getIdentityCard(), errors);

        if (model.getEntrant().getRegistrationDate().after(new Date()))
        {
            errors.add("Дата добавления должна быть не позже сегодняшней.", "registrationDate");
        }
    }

    private void prepareOnlineEntrantIdentityCard(Model model)
    {
        Person person = model.getPerson();
        OnlineEntrant onlineEntrant = model.getOnlineEntrant();
        AddressCountry citizenship = onlineEntrant.getPassportCitizenship();
        person.setAddress(AddressBaseUtils.convertFromOldFormat(onlineEntrant.getActualAddress(), false));

        IdentityCard identityCard = model.getIdentityCard();
        identityCard.setLastName(onlineEntrant.getLastName());
        identityCard.setFirstName(onlineEntrant.getFirstName());
        identityCard.setMiddleName(onlineEntrant.getMiddleName());
        identityCard.setSeria(onlineEntrant.getPassportSeria());
        identityCard.setNumber(onlineEntrant.getPassportNumber());
        identityCard.setBirthDate(onlineEntrant.getBirthDate());
        identityCard.setBirthPlace(onlineEntrant.getBirthPlace());
        identityCard.setIssuanceCode(onlineEntrant.getPassportIssuanceCode());
        identityCard.setIssuanceDate(onlineEntrant.getPassportIssuanceDate());
        identityCard.setIssuancePlace(onlineEntrant.getPassportIssuancePlace());
        if (citizenship != null)
        {
            identityCard.setCitizenship(citizenship);
        }
        identityCard.setCardType(onlineEntrant.getPassportType());
        identityCard.setSex(onlineEntrant.getSex());
        identityCard.setAddress(AddressBaseUtils.convertFromOldFormat(onlineEntrant.getRegistrationAddress(), false));

    }
}
