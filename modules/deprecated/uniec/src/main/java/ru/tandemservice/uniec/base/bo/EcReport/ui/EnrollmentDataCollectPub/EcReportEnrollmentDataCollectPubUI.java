/**
 *$Id$
 */
package ru.tandemservice.uniec.base.bo.EcReport.ui.EnrollmentDataCollectPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uniec.entity.report.EnrollmentDataCollectReport;

/**
 * @author Alexander Shaburov
 * @since 16.08.13
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "reportId", required = true)
})
public class EcReportEnrollmentDataCollectPubUI extends UIPresenter
{
    private Long _reportId;
    private EnrollmentDataCollectReport _report;
    private String _requestFromToPeriod;

    @Override
    public void onComponentRefresh()
    {
        _report = DataAccessServices.dao().getNotNull(EnrollmentDataCollectReport.class, _reportId);

        _requestFromToPeriod = DateFormatter.DEFAULT_DATE_FORMATTER.format(_report.getRequestDateFrom()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(_report.getRequestDateTo());
    }

    public void onClickPrint()
    {
        getActivationBuilder().asDesktopRoot(IUniComponents.DOWNLOAD_STORABLE_REPORT)
                .parameter("reportId", _reportId)
                .parameter("extension", "xls")
                .activate();
    }

    // Accessors

    public Long getReportId()
    {
        return _reportId;
    }

    public void setReportId(Long reportId)
    {
        _reportId = reportId;
    }

    public EnrollmentDataCollectReport getReport()
    {
        return _report;
    }

    public void setReport(EnrollmentDataCollectReport report)
    {
        _report = report;
    }

    public String getRequestFromToPeriod()
    {
        return _requestFromToPeriod;
    }

    public void setRequestFromToPeriod(String requestFromToPeriod)
    {
        _requestFromToPeriod = requestFromToPeriod;
    }
}
