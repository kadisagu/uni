/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.report.EntranceExamMeetingByCK2010.EntranceExamMeetingByCKAdd;

import java.util.Map;

import org.tandemframework.core.bean.FastBeanUtils;

import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.report.IReportRow;

/**
 * @author Vasily Zhukov
 * @since 04.08.2010
 */
class ReportRow implements IReportRow
{
    private RequestedEnrollmentDirection _direction;
    private int _individProgressMark; //балл за индивидуальные достижения
    private double _sumMark;
    private Map<Discipline2RealizationWayRelation, Double> _markMap;
    private CompetitionKind _competitionKind;
    private Double averageEduInstMark;

    ReportRow(RequestedEnrollmentDirection direction, EntrantDataUtil dataUtil)
    {
        _direction = direction;
        _individProgressMark = dataUtil.getIndividualProgressMarkByRequest(direction.getEntrantRequest().getId());
        _sumMark = dataUtil.getFinalMark(direction) + _individProgressMark;
        _markMap = dataUtil.getMarkMap(direction);
        _competitionKind = (!direction.isTargetAdmission()) ? direction.getCompetitionKind() : null;
        averageEduInstMark = dataUtil.getAverageEduInstMark(direction.getEntrantRequest().getEntrant().getId());
    }

    @Override
    public Double getFinalMark()
    {
        return getSumMark();
    }

    @Override
    public String getFio()
    {
        return getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getPerson().getFullFio();
    }

    // Getters & Setters

    @Override
    public RequestedEnrollmentDirection getRequestedEnrollmentDirection()
    {
        return _direction;
    }

    public double getSumMark()
    {
        return _sumMark;
    }

    public Map<Discipline2RealizationWayRelation, Double> getMarkMap()
    {
        return _markMap;
    }

    public CompetitionKind getCompetitionKind()
    {
        return _competitionKind;
    }

    public static String getHeader(CompetitionKind competitionKind)
    {
        if (competitionKind == null)
        {
            return "ЛИЦА, ИМЕЮЩИЕ ПРАВО НА ЗАЧИСЛЕНИЕ ПО РЕЗУЛЬТАТАМ ЦЕЛЕВОГО ПРИЕМА";
        }

        String code = competitionKind.getCode();
        switch (code)
        {
            case UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES:
                return "ЛИЦА, ИМЕЮЩИЕ ПРАВО НА ЗАЧИСЛЕНИЕ БЕЗ ВСТУПИТЕЛЬНЫХ ИСПЫТАНИЙ";
            case UniecDefines.COMPETITION_KIND_INTERVIEW:
                return "ЛИЦА, ИМЕЮЩИЕ ПРАВО НА ЗАЧИСЛЕНИЕ ПО РЕЗУЛЬТАТАМ СОБЕСЕДОВАНИЯ";
            case UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION:
                return "ЛИЦА, ИМЕЮЩИЕ ПРАВО НА ЗАЧИСЛЕНИЕ ПО ОСОБЫМ ПРАВАМ";
            case UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION:
                return "ЛИЦА, ИМЕЮЩИЕ ПРАВО НА ЗАЧИСЛЕНИЕ ПО ОБЩЕМУ КОНКУРСУ";
        }
        return null;
    }

    @Override
    public Double getProfileMark()
    {
        return (Double) FastBeanUtils.getValue(getRequestedEnrollmentDirection(), RequestedEnrollmentDirection.profileChosenEntranceDiscipline().finalMark().s());
    }

    @Override
    public Boolean isGraduatedProfileEduInstitution()
    {
        return Boolean.FALSE; // мы не грузим эти данные, так что по ним не сравниваем
    }

    @Override
    public Double getCertificateAverageMark()
    {
        return averageEduInstMark;
    }

    public int getIndividProgressMark()
    {
        return _individProgressMark;
    }
}


