/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcEntrant.ui.CustomStateList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.uni.ui.ColorRowCustomizer;
import ru.tandemservice.uniec.base.bo.EcEntrant.logic.EntrantCustomStateSearchListDSHandler;
import ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI;
import ru.tandemservice.uniec.entity.entrant.EntrantCustomState;

/**
 * @author nvankov
 * @since 4/3/13
 */
@Configuration
public class EcEntrantCustomStateList extends BusinessComponentManager
{
    public static final String CUSTOM_STATE_LIST_DS = "customStateListDS";


    @Bean
    public ColumnListExtPoint customStateListCL()
    {
        return columnListExtPointBuilder(CUSTOM_STATE_LIST_DS)
                .addColumn(textColumn("title", EntrantCustomState.customState().title()).required(true).order())
                .addColumn(dateColumn("beginDate", EntrantCustomState.beginDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order().width("250px"))
                .addColumn(dateColumn("endDate", EntrantCustomState.endDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order().width("250px"))
                .addColumn(textColumn("desc", EntrantCustomState.description()))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("ecEntrantCustomStateListEdit"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).alert("message:ui.customStatusDeleteAlert").permissionKey("ecEntrantCustomStateListDelete"))
                .create();
    }

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(CUSTOM_STATE_LIST_DS, customStateListCL(), customStateListDSHandler())
                                       .rowCustomizer(new ColorRowCustomizer<>().coloredProperty(EntrantCustomState.customState().s())))
                .addDataSource(selectDS("entrantCustomStateCIDS", entrantCustomStateCIComboDS()))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler customStateListDSHandler()
    {
        return new EntrantCustomStateSearchListDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler entrantCustomStateCIComboDS()
    {
        return new DefaultComboDataSourceHandler(getName(), EntrantCustomStateCI.class);
    }
}
