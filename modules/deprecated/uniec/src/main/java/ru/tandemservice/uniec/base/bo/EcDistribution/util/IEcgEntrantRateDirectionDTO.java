/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.util;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;

/**
 * @author Vasily Zhukov
 * @since 23.07.2011
 */
public interface IEcgEntrantRateDirectionDTO extends IEntity, ITitled
{
    /**
     * @return Направление приема (или профиль) для выбора
     */
    Long getDirectionId();
}
