/* $Id$ */
package ru.tandemservice.uniec.component.listextract.ent2.e0.ListOrderPub;

import ru.tandemservice.movestudent.component.listextract.abstractorder.ListOrderPub.IAbstractListOrderPubDAO;
import ru.tandemservice.movestudent.entity.StudentListOrder;

/**
 * @author Dmitry Seleznev
 * @since 21.08.2012
 */
public interface IDAO extends IAbstractListOrderPubDAO<StudentListOrder, Model>
{
}