/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.dao.examgroup;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.header.RtfColor;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import org.tandemframework.shared.person.catalog.entity.Benefit;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.util.EntrantDataUtil;

import java.util.*;

/**
 * Реализация стандартного алгоритма печати экзаменационной группы
 *
 * @author vip_delete
 * @since 08.07.2009
 */
public abstract class AbstractExamGroupPrintBuilder implements IExamGroupPrint
{
    private Session _session;
    private boolean _withResult;
    private IRtfControl _redColor;

    // надо ли включать в документ финальные оценки

    protected abstract boolean isWithResult();

    @Override
    public void init(Session session)
    {
        _session = session;
        _withResult = isWithResult();
    }

    protected Session getSession()
    {
        return _session;
    }

    @Override
    public final RtfDocument createPrintForm(List<Long> ids)
    {
        if (ids.isEmpty())
            throw new ApplicationException("Нет данных для печати.");

        // загружаем сокращенное название вуза
        String vuzShortTitle = TopOrgUnit.getInstance().getShortTitle();

        // загружаем шаблон
        RtfDocument template = getTemplate();

        // создаем результирующий документ, наследуя настройки из шаблона
        RtfDocument result = RtfBean.getElementFactory().createRtfDocument();
        result.setHeader(template.getHeader());
        result.setSettings(template.getSettings());

        // создаем красный цвет
        List<RtfColor> colorList = result.getHeader().getColorTable().getColorList();
        _redColor = RtfBean.getElementFactory().createRtfControl(IRtfData.CF, colorList.size());
//        _redColor.setValue(colorList.size());
        RtfColor red = new RtfColor();
        red.setRed(255);
        colorList.add(red);

        for (Long id : ids)
        {
            // загружаем экзаменационную группу
            ExamGroup group = (ExamGroup) _session.get(ExamGroup.class, id);

            // нет группы, нечего печатать
            if (group == null) continue;

            // не портим шаблон
            RtfDocument document = template.getClone();

            // модификатор документа
            RtfInjectModifier injectModifier = getRtfInjectModifier(group);

            // подставляем сокращенное название вуза
            injectModifier.put("vuzShortTitle", vuzShortTitle);

            // заменяем метки в документе
            injectModifier.modify(document);

            // заполняем таблицу документа
            getRtfTableModifier(group).modify(document);

            // добавляем получившийся документ в результирующий
            result.getElementList().addAll(document.getElementList());
        }

        if (result.getElementList().isEmpty())
            throw new ApplicationException("Нет данных для печати.");

        return result;
    }

    protected RtfDocument getTemplate()
    {
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, getTemplateCode());

        return new RtfReader().read(templateDocument.getCurrentTemplate());
    }

    protected RtfInjectModifier getRtfInjectModifier(ExamGroup group)
    {
        return new RtfInjectModifier()
                .put("reportTitle", _withResult ? "РЕЗУЛЬТАТЫ" : "СПИСОК")
                .put("subTitle", _withResult ? "сдачи" : "допущенных к сдаче")
                .put("createDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()))
                .put("groupTitle", group.getTitle());
    }

    protected RtfTableModifier getRtfTableModifier(ExamGroup group)
    {
        RtfTableModifier tableModifier = new RtfTableModifier();

        final List<Discipline2RealizationWayRelation> disciplineList = new ArrayList<>();
        final Set<Integer> invalidRowIndex = new HashSet<>();

        tableModifier.put("T", getTableData(group, invalidRowIndex, disciplineList));

        // если есть дисциплины, то их названия надо вставить в ячейку номер 5 заголовка таблицы
        final int[] splitData = new int[disciplineList.size()];
        Arrays.fill(splitData, 1);

        tableModifier.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public void beforeModify(RtfTable table, int currentRowIndex)
            {
                if (disciplineList.isEmpty()) return;
                // в заголовок таблицы вставляем названия дисциплин (колонка номер 5), разделяем ячейку в равных пропорциях
                RtfUtil.splitRow(table.getRowList().get(currentRowIndex - 1), 5, (newCell, index) -> {
                    newCell.setElementList(new ArrayList<>());
                    newCell.getElementList().add(RtfBean.getElementFactory().createRtfText(disciplineList.get(index).getTitle()));
                }, splitData);
                RtfUtil.splitRow(table.getRowList().get(currentRowIndex), 5, null, splitData);
            }

            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                for (Integer index : invalidRowIndex)
                {
                    RtfCell cell = newRowList.get(index + startIndex).getCellList().get(3);     // ФИО печатаем красным
                    IRtfGroup group = RtfBean.getElementFactory().createRtfGroup();
                    group.getElementList().add(_redColor.getClone()); // need clone!
                    group.getElementList().addAll(cell.getElementList());
                    cell.getElementList().clear();
                    cell.getElementList().add(group);
                }
            }
        });

        return tableModifier;
    }

    protected String[][] getTableData(ExamGroup examGroup, Set<Integer> invalidRowIndex, List<Discipline2RealizationWayRelation> disciplineList)
    {
        List<ExamGroupRow> rowList = new MQBuilder(ExamGroupRow.ENTITY_CLASS, "r").add(MQExpression.eq("r", ExamGroupRow.L_EXAM_GROUP, examGroup)).getResultList(_session);
        Set<Long> validRowSet = new HashSet<>();
        for (ExamGroupRow row : rowList)
            if (UniecDAOFacade.getExamGroupSetDao().isExamGroupRowValid(row))
                validRowSet.add(row.getId());
        MQBuilder directionBuilder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "d");
        directionBuilder.addJoinFetch("d", RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "request");
        directionBuilder.addJoinFetch("request", EntrantRequest.L_ENTRANT, "entrant");
        directionBuilder.addJoinFetch("entrant", Entrant.L_PERSON, "person");
        directionBuilder.addJoinFetch("person", Person.L_IDENTITY_CARD, "card");
        directionBuilder.addDomain("g", ExamGroupRow.ENTITY_CLASS);
        directionBuilder.add(MQExpression.eqProperty("d", RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "g", ExamGroupRow.L_ENTRANT_REQUEST));
        directionBuilder.add(MQExpression.eq("g", ExamGroupRow.L_EXAM_GROUP, examGroup));

        if (_withResult)
        {
            directionBuilder.add(MQExpression.in("g", ExamGroupRow.P_ID, validRowSet));
            directionBuilder.add(MQExpression.notEq("d", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
            directionBuilder.add(MQExpression.notEq("d", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE));
            directionBuilder.add(MQExpression.eq("d", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, Boolean.FALSE));
        }

        EntrantDataUtil dataUtil = new EntrantDataUtil(_session, examGroup.getExamGroupSet().getEnrollmentCampaign(), directionBuilder);

        Set<EntrantRequest> targetAdmissionEntrants = new HashSet<>();
        for (RequestedEnrollmentDirection direction : directionBuilder.<RequestedEnrollmentDirection>getResultList(_session))
            if (direction.isTargetAdmission())
                targetAdmissionEntrants.add(direction.getEntrantRequest());

        Map<Person, SortedSet<String>> benefitMap = new HashMap<>();
        for (Map.Entry<Person, Set<PersonBenefit>> entry : EntrantDataUtil.getPersonBenefitMap(_session, directionBuilder).entrySet())
            benefitMap.put(entry.getKey(), new TreeSet<>(CommonBaseUtil.<String>getPropertiesList(entry.getValue(), PersonBenefit.L_BENEFIT + "." + Benefit.P_SHORT_TITLE)));

        Set<Discipline2RealizationWayRelation> disciplineSet = new HashSet<>();
        Map<MultiKey, Double> chosenMarkData = new HashMap<>();
        Map<EntrantRequest, Double> directionMarkData = new HashMap<>();

        for (RequestedEnrollmentDirection direction : dataUtil.getDirectionSet())
        {
            for (ChosenEntranceDiscipline chosen : dataUtil.getChosenEntranceDisciplineSet(direction))
            {
                disciplineSet.add(chosen.getEnrollmentCampaignDiscipline());
                MultiKey key = new MultiKey(direction.getEntrantRequest(), chosen.getEnrollmentCampaignDiscipline());
                Double chosenMark = chosenMarkData.get(key);

                // -1 означает, что еще не сдал, но должен сдавать
                double finalMark = chosen.getFinalMark() == null ? -1 : chosen.getFinalMark();
                chosenMarkData.put(key, chosenMark == null ? finalMark : Math.max(chosenMark, finalMark));
            }

            Double directionMark = directionMarkData.get(direction.getEntrantRequest());
            double finalMark = dataUtil.getFinalMark(direction);
            if (directionMark == null || finalMark > directionMark)
                directionMarkData.put(direction.getEntrantRequest(), finalMark);
        }
        disciplineList.addAll(disciplineSet);
        Collections.sort(disciplineList, ITitled.TITLED_COMPARATOR);

        Collections.sort(rowList, (o1, o2) -> {
            int result = Person.FULL_FIO_COMPARATOR.compare(o1.getEntrantRequest().getEntrant().getPerson(), o2.getEntrantRequest().getEntrant().getPerson());
            if (result != 0) return result;
            return o1.getEntrantRequest().getRegDate().compareTo(o2.getEntrantRequest().getRegDate());
        });

        List<String[]> result = new ArrayList<>();
        for (int i = 0; i < rowList.size(); i++)
        {
            ExamGroupRow row = rowList.get(i);
            if (_withResult)
            {
                if (!validRowSet.contains(row.getId())) continue;
            } else
            {
                if (!validRowSet.contains(row.getId())) invalidRowIndex.add(i);
            }

            EntrantRequest request = row.getEntrantRequest();
            Entrant entrant = request.getEntrant();
            Person person = entrant.getPerson();
            PersonEduInstitution personEduInstitution = person.getPersonEduInstitution();
            boolean medalist = personEduInstitution != null && personEduInstitution.isMedalist();
            SortedSet<String> benefits = benefitMap.get(person);

            String[] line = new String[6 + disciplineList.size()];
            int k = 0;
            line[k++] = Integer.toString(result.size() + 1);
            line[k++] = (medalist ? "М" : "") + (targetAdmissionEntrants.contains(request) ? "Ц" : "");
            line[k++] = benefits == null ? "" : StringUtils.join(benefits.iterator(), ", ");
            line[k++] = person.getFullFio();
            line[k++] = request.getStringNumber();

            if (disciplineList.isEmpty())
                k++;
            else
                for (Discipline2RealizationWayRelation discipline : disciplineList)
                {
                    Double mark = chosenMarkData.get(new MultiKey(request, discipline));
                    line[k++] = mark == null ? "x" : (mark == -1 ? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(mark));
                }

            if (_withResult)
            {
                Double mark = directionMarkData.get(request);
                // если нет оценки, значит ни одно направление приема из этого заявления не попало в статистику
                // значит, либо абитуриент архивный, либо по всем направлениям этого заявления у него состояние 'забрал документы'
                // или 'выбыл из конкурса'
                if (mark == null) continue;
                line[k] = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(directionMarkData.get(request));
            }
            result.add(line);
        }
        result.add(new String[]{""});
        result.add(new String[]{""});
        result.add(new String[]{""});

        return result.toArray(new String[result.size()][]);
    }

    protected abstract String getTemplateCode();
}
