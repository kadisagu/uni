/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.RequestedEnrollmentDirectionPub;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.entity.entrant.*;

/**
 * @author agolubenko
 * @since 16.03.2009
 */
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "requestedEnrollmentDirection.id")
})
public class Model
{
    private RequestedEnrollmentDirection _requestedEnrollmentDirection = new RequestedEnrollmentDirection();
    private DynamicListDataSource<ProfileExaminationMark> _marksDataSource;
    private String _profileKnowledgeLisTitle;
    private DynamicListDataSource<PriorityProfileEduOuDTO> _dataSource;

    public String getRequestedEnrollmentDirectionWorkExperience()
    {
        Integer years = _requestedEnrollmentDirection.getProfileWorkExperienceYears();
        Integer months = _requestedEnrollmentDirection.getProfileWorkExperienceMonths();
        Integer days = _requestedEnrollmentDirection.getProfileWorkExperienceDays();

        List<String> periods = new ArrayList<String>();
        if (years != null)
        {
            periods.add("лет: " + years);
        }
        if (months != null)
        {
            periods.add("месяцев: " + months);
        }
        if (days != null)
        {
            periods.add("дней: " + days);
        }
        return StringUtils.join(periods, "; ");
    }

    public EntrantRequest getEntrantRequest()
    {
        return getRequestedEnrollmentDirection().getEntrantRequest();
    }

    public EnrollmentDirection getEnrollmentDirection()
    {
        return getRequestedEnrollmentDirection().getEnrollmentDirection();
    }

    public EducationOrgUnit getEducationOrgUnit()
    {
        return getEnrollmentDirection().getEducationOrgUnit();
    }

    public RequestedEnrollmentDirection getRequestedEnrollmentDirection()
    {
        return _requestedEnrollmentDirection;
    }

    public void setRequestedEnrollmentDirection(RequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        _requestedEnrollmentDirection = requestedEnrollmentDirection;
    }

    public DynamicListDataSource<ProfileExaminationMark> getMarksDataSource()
    {
        return _marksDataSource;
    }

    public void setMarksDataSource(DynamicListDataSource<ProfileExaminationMark> marksDataSource)
    {
        _marksDataSource = marksDataSource;
    }

    public String getProfileKnowledgeLisTitle()
    {
        return _profileKnowledgeLisTitle;
    }

    public void setProfileKnowledgeLisTitle(String profileKnowledgeLisTitle)
    {
        _profileKnowledgeLisTitle = profileKnowledgeLisTitle;
    }

    public DynamicListDataSource<PriorityProfileEduOuDTO> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<PriorityProfileEduOuDTO> dataSource)
    {
        _dataSource = dataSource;
    }
}
