/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec.component.report.EnrollmentResultIndexNumbers.EnrollmentResultIndexNumbersAdd;

import org.hibernate.Session;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.catalog.codes.EntrantStateCodes;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.util.EntrantDataUtil;

import java.util.*;

/**
 * @author Vasily Zhukov
 * @since 17.04.2012
 */
class EnrollmentResultIndexNumbersReportBuilder
{
    private Model model;
    private Session session;

    public EnrollmentResultIndexNumbersReportBuilder(Model model, Session session)
    {
        this.model = model;
        this.session = session;
    }

    public DatabaseFile getContent()
    {
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ENROLLMENT_RESULT_INDEX_NUMBERS);

        TopOrgUnit academy = TopOrgUnit.getInstance();

        RtfInjectModifier im = new RtfInjectModifier()
        .put("developForm", model.getDevelopForm().getTitle())
        .put("academyTitle", academy.getNominativeCaseTitle() == null ? academy.getTitle() : academy.getNominativeCaseTitle())
        .put("year", model.getReport().getEnrollmentCampaign().getEducationYear().getTitle().split("/")[0]);

        RtfTableModifier tm = new RtfTableModifier()
        .put("T", getTableRows());

        DatabaseFile content = new DatabaseFile();
        content.setContent(RtfUtil.toByteArray(templateDocument.getCurrentTemplate(), im, tm));
        return content;
    }

    public String[][] getTableRows()
    {
        // НПМ с признаком группировки -> данные строки
        Map<EducationLevels, Row> rowMap = new HashMap<>();

        // собираем статистику по ВНП
        MQBuilder directionBuilder = getDirectionBuilder();
        List<RequestedEnrollmentDirection> directionList = directionBuilder.getResultList(session);
        for (RequestedEnrollmentDirection direction : directionList)
        {
            EducationLevels eduLevel = direction.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();

            // находим парента с признаком группировки
            while (eduLevel != null && !eduLevel.getLevelType().isGroupingLevel() && eduLevel.getParentLevel() != null) eduLevel = eduLevel.getParentLevel();

            Row row = rowMap.get(eduLevel);
            if (row == null)
                rowMap.put(eduLevel, row = new Row());
            row.entrantIds.add(direction.getEntrantRequest().getEntrant().getId());
        }

        // собираем статистику по студентам пред.зачисления
        MQBuilder preBuilder = getPreliminaryBuilder();
        List<PreliminaryEnrollmentStudent> preStudentList = preBuilder.getResultList(session);
        preBuilder.getSelectAliasList().clear();
        preBuilder.addSelect("p", new Object[]{PreliminaryEnrollmentStudent.requestedEnrollmentDirection()});
        EntrantDataUtil preUtil = new EntrantDataUtil(session, model.getEnrollmentCampaign(), preBuilder, EntrantDataUtil.DetailLevel.EXAM_PASS);
        for (PreliminaryEnrollmentStudent preStudent : preStudentList)
        {
            EducationLevels eduLevel = preStudent.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();
            double finalMark = preUtil.getFinalMark(preStudent.getRequestedEnrollmentDirection());

            // находим парента с признаком группировки
            while (eduLevel != null && !eduLevel.getLevelType().isGroupingLevel() && eduLevel.getParentLevel() != null) eduLevel = eduLevel.getParentLevel();

            Row row = rowMap.get(eduLevel);
            if (row == null)
                rowMap.put(eduLevel, row = new Row());
            row.enrolled++;

            if (finalMark != 0.0 && (row.passingMark == 0.0 || row.passingMark > finalMark))
                row.passingMark = finalMark;
        }

        // заполняем планы приема
        MQBuilder builder = new MQBuilder(EnrollmentDirection.ENTITY_CLASS, "e");
        builder.addJoin("e", EnrollmentDirection.educationOrgUnit(), "ou");
        builder.add(MQExpression.eq("e", EnrollmentDirection.enrollmentCampaign(), model.getEnrollmentCampaign()));
        patchEduOuBuilder(builder);
        for (EnrollmentDirection direction : builder.<EnrollmentDirection>getResultList(session))
        {
            EducationLevels eduLevel = direction.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();

            // находим парента с признаком группировки
            while (eduLevel != null && !eduLevel.getLevelType().isGroupingLevel() && eduLevel.getParentLevel() != null) eduLevel = eduLevel.getParentLevel();

            Row row = rowMap.get(eduLevel);
            if (row != null)
            {
                Integer plan;
                Integer taPlan;
                if (model.getCompensationType().isBudget())
                {
                    plan = direction.getMinisterialPlan();
                    taPlan = direction.getTargetAdmissionPlanBudget();
                } else
                {
                    plan = direction.getContractPlan();
                    taPlan = direction.getTargetAdmissionPlanContract();
                }
                int planWithoutTaPlan = (plan == null ? 0 : plan) - (taPlan == null ? 0 : taPlan);
                if (planWithoutTaPlan < 0) planWithoutTaPlan = 0;
                row.plan += planWithoutTaPlan;
            }
        }

        // укрупненная группа НПМ -> входящие в нее строки
        Map<EducationLevels, Map<EducationLevels, Row>> group2subMap = new HashMap<>();
        for (Map.Entry<EducationLevels, Row> entry : rowMap.entrySet())
        {
            EducationLevels eduLevel = entry.getKey();

            // находим укрупненную группу НПМ
            EducationLevels group = eduLevel;
            while (group.getLevelType().getParent() != null && group.getParentLevel() != null) group = group.getParentLevel();

            Map<EducationLevels, Row> map = group2subMap.get(group);
            if (map == null)
                group2subMap.put(group, map = new HashMap<>());

            if (null != map.put(eduLevel, entry.getValue()))
                throw new RuntimeException("Dublication row"); // can't be here
        }

        List<String[]> rowList = new ArrayList<>();
        DoubleFormatter doubleFormatter = DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS;

        // определяем порядок ключей
        List<EducationLevels> keyList = new ArrayList<>(group2subMap.keySet());
        Collections.sort(keyList, (o1, o2) -> {
            // вначале по приоритету
            int r = o1.getLevelType().getPriority() - o2.getLevelType().getPriority();
            if (r != 0) return r;

            // потом по названию
            return o1.getTitle().compareTo(o2.getTitle());
        });

        for (EducationLevels eduLevel : keyList)
        {
            Map<EducationLevels, Row> subMap = group2subMap.get(eduLevel);

            // добавляем укрупненную группу
            rowList.add(new String[]{eduLevel.getTitle(), eduLevel.getTitleCodePrefix(), null, null, null, null});

            // определяем порядок ключей внутри укрупненной группы
            List<EducationLevels> subKeyList = new ArrayList<>(subMap.keySet());
            Collections.sort(subKeyList, ITitled.TITLED_COMPARATOR);

            // добавляем все подчиненные строки
            for (EducationLevels subEduLevel : subKeyList)
            {
                Row row = subMap.get(subEduLevel);
                rowList.add(new String[]{subEduLevel.getTitle(), subEduLevel.getTitleCodePrefix(), Integer.toString(row.plan), row.plan == 0.0 ? "-" : doubleFormatter.format((double) row.entrantIds.size() / row.plan), Integer.toString(row.enrolled), doubleFormatter.format(row.passingMark)});
            }
        }

        return rowList.toArray(new String[rowList.size()][]);
    }

    private MQBuilder getDirectionBuilder()
    {
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "d");
        builder.addJoin("d", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit(), "ou");

        // ограничения по фильтрам
        builder.add(MQExpression.eq("d", RequestedEnrollmentDirection.enrollmentDirection().enrollmentCampaign(), model.getEnrollmentCampaign()));
        builder.add(UniMQExpression.betweenDate("d", RequestedEnrollmentDirection.entrantRequest().regDate().s(), model.getReport().getDateFrom(), model.getReport().getDateTo()));
        builder.add(MQExpression.eq("d", RequestedEnrollmentDirection.compensationType(), model.getCompensationType()));

        if (model.isStudentCategoryActive())
            builder.add(MQExpression.in("d", RequestedEnrollmentDirection.studentCategory(), model.getStudentCategoryList()));

        patchEduOuBuilder(builder);

        // доп. ограничения
        builder.add(MQExpression.eq("d", RequestedEnrollmentDirection.entrantRequest().entrant().archival(), Boolean.FALSE));
        builder.add(MQExpression.eq("d", RequestedEnrollmentDirection.targetAdmission(), Boolean.FALSE));
        builder.add(MQExpression.in("d", RequestedEnrollmentDirection.state().code(), Arrays.asList(
            EntrantStateCodes.TO_BE_ENROLED,
            EntrantStateCodes.PRELIMENARY_ENROLLED,
            EntrantStateCodes.IN_ORDER,
            EntrantStateCodes.ENROLED
        )));

        return builder;
    }

    private MQBuilder getPreliminaryBuilder()
    {
        MQBuilder builder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "p");
        builder.addJoin("p", PreliminaryEnrollmentStudent.educationOrgUnit(), "ou");

        // ограничения по фильтрам
        builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.requestedEnrollmentDirection().enrollmentDirection().enrollmentCampaign(), model.getEnrollmentCampaign()));
        builder.add(UniMQExpression.betweenDate("p", PreliminaryEnrollmentStudent.requestedEnrollmentDirection().entrantRequest().regDate().s(), model.getReport().getDateFrom(), model.getReport().getDateTo()));
        builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.compensationType(), model.getCompensationType()));

        if (model.isStudentCategoryActive())
            builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.studentCategory(), model.getStudentCategoryList()));

        patchEduOuBuilder(builder);

        // доп. ограничения
        builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.requestedEnrollmentDirection().entrantRequest().entrant().archival(), Boolean.FALSE));
        builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.targetAdmission(), Boolean.FALSE));

        return builder;
    }

    private void patchEduOuBuilder(MQBuilder builder)
    {
        if (model.isQualificationActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.educationLevelHighSchool().educationLevel().qualification().s(), model.getQualificationList()));

        if (model.isFormativeOrgUnitActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.formativeOrgUnit().s(), model.getFormativeOrgUnitList()));

        if (model.isTerritorialOrgUnitActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.territorialOrgUnit().s(), model.getTerritorialOrgUnitList()));

        if (model.isEducationLevelHighSchoolActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.educationLevelHighSchool().s(), model.getEducationLevelHighSchoolList()));

        builder.add(MQExpression.eq("ou", EducationOrgUnit.developForm().s(), model.getDevelopForm()));

        if (model.isDevelopConditionActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.developCondition().s(), model.getDevelopConditionList()));

        if (model.isDevelopTechActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.developTech().s(), model.getDevelopTechList()));

        if (model.isDevelopPeriodActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.developPeriod().s(), model.getDevelopPeriodList()));
    }

    private static class Row
    {
        Set<Long> entrantIds; // абитуриенты
        int enrolled;         // число зачисленных
        int plan;             // план приема
        double passingMark;   // проходной балл

        private Row()
        {
            entrantIds = new TreeSet<>();
        }
    }
}
