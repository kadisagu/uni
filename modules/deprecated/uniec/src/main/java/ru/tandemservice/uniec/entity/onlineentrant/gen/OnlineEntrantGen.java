package ru.tandemservice.uniec.entity.onlineentrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.fias.base.entity.Address;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.person.catalog.entity.Benefit;
import org.tandemframework.shared.person.catalog.entity.EduInstitution;
import org.tandemframework.shared.person.catalog.entity.EducationDocumentType;
import org.tandemframework.shared.person.catalog.entity.EducationLevelStage;
import org.tandemframework.shared.person.catalog.entity.FamilyStatus;
import org.tandemframework.shared.person.catalog.entity.ForeignLanguage;
import org.tandemframework.shared.person.catalog.entity.GraduationHonour;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.RelationDegree;
import org.tandemframework.shared.person.catalog.entity.Sex;
import org.tandemframework.shared.person.catalog.entity.SportRank;
import org.tandemframework.shared.person.catalog.entity.SportType;
import ru.tandemservice.uniec.entity.catalog.MethodDeliveryNReturnDocs;
import ru.tandemservice.uniec.entity.catalog.OlympiadDiplomaDegree;
import ru.tandemservice.uniec.entity.catalog.OlympiadDiplomaType;
import ru.tandemservice.uniec.entity.catalog.StateExamType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Онлайн-абитуриент
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OnlineEntrantGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant";
    public static final String ENTITY_NAME = "onlineEntrant";
    public static final int VERSION_HASH = 1565301761;
    private static IEntityMeta ENTITY_META;

    public static final String P_USER_ID = "userId";
    public static final String L_ENTRANT = "entrant";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_PERSONAL_NUMBER = "personalNumber";
    public static final String P_REGISTRATION_YEAR = "registrationYear";
    public static final String P_REGISTRATION_DATE = "registrationDate";
    public static final String P_LAST_NAME = "lastName";
    public static final String P_FIRST_NAME = "firstName";
    public static final String P_MIDDLE_NAME = "middleName";
    public static final String L_PASSPORT_TYPE = "passportType";
    public static final String P_PASSPORT_SERIA = "passportSeria";
    public static final String P_PASSPORT_NUMBER = "passportNumber";
    public static final String L_PASSPORT_CITIZENSHIP = "passportCitizenship";
    public static final String P_PASSPORT_ISSUANCE_PLACE = "passportIssuancePlace";
    public static final String P_PASSPORT_ISSUANCE_CODE = "passportIssuanceCode";
    public static final String P_PASSPORT_ISSUANCE_DATE = "passportIssuanceDate";
    public static final String L_SEX = "sex";
    public static final String P_BIRTH_DATE = "birthDate";
    public static final String P_BIRTH_PLACE = "birthPlace";
    public static final String L_REGISTRATION_ADDRESS = "registrationAddress";
    public static final String P_LIVE_AT_REGISTRATION = "liveAtRegistration";
    public static final String L_ACTUAL_ADDRESS = "actualAddress";
    public static final String P_CERTIFICATE_NUMBER = "certificateNumber";
    public static final String L_CERTIFICATE_PLACE = "certificatePlace";
    public static final String P_CERTIFICATE_DATE = "certificateDate";
    public static final String P_OLYMPIAD_DIPLOMA_NUMBER = "olympiadDiplomaNumber";
    public static final String P_OLYMPIAD_DIPLOMA_SERIA = "olympiadDiplomaSeria";
    public static final String P_OLYMPIAD_DIPLOMA_ISSUANCE_DATE = "olympiadDiplomaIssuanceDate";
    public static final String L_OLYMPIAD_DIPLOMA_TYPE = "olympiadDiplomaType";
    public static final String L_OLYMPIAD_DIPLOMA_DEGREE = "olympiadDiplomaDegree";
    public static final String P_OLYMPIAD_DIPLOMA_TITLE = "olympiadDiplomaTitle";
    public static final String P_OLYMPIAD_DIPLOMA_SUBJECT = "olympiadDiplomaSubject";
    public static final String L_OLYMPIAD_ADDRESS_COUNTRY = "olympiadAddressCountry";
    public static final String L_OLYMPIAD_ADDRESS_SETTLEMENT = "olympiadAddressSettlement";
    public static final String L_EDU_INSTITUTION_COUNTRY = "eduInstitutionCountry";
    public static final String L_EDU_INSTITUTION_SETTLEMENT = "eduInstitutionSettlement";
    public static final String L_EDU_INSTITUTION = "eduInstitution";
    public static final String P_EDU_INSTITUTION_STR = "eduInstitutionStr";
    public static final String L_EDU_DOCUMENT_TYPE = "eduDocumentType";
    public static final String L_EDU_DOCUMENT_LEVEL = "eduDocumentLevel";
    public static final String P_EDU_DOCUMENT_SERIA = "eduDocumentSeria";
    public static final String P_EDU_DOCUMENT_NUMBER = "eduDocumentNumber";
    public static final String P_EDU_DOCUMENT_YEAR_END = "eduDocumentYearEnd";
    public static final String P_EDU_DOCUMENT_DATE = "eduDocumentDate";
    public static final String L_EDU_DOCUMENT_GRADUATION_HONOUR = "eduDocumentGraduationHonour";
    public static final String P_MARK5 = "mark5";
    public static final String P_MARK4 = "mark4";
    public static final String P_MARK3 = "mark3";
    public static final String L_NEXT_OF_KIN_RELATION_DEGREE = "nextOfKinRelationDegree";
    public static final String P_NEXT_OF_KIN_LAST_NAME = "nextOfKinLastName";
    public static final String P_NEXT_OF_KIN_FIRST_NAME = "nextOfKinFirstName";
    public static final String P_NEXT_OF_KIN_MIDDLE_NAME = "nextOfKinMiddleName";
    public static final String P_NEXT_OF_KIN_WORK_PLACE = "nextOfKinWorkPlace";
    public static final String P_NEXT_OF_KIN_WORK_POST = "nextOfKinWorkPost";
    public static final String P_NEXT_OF_KIN_PHONE = "nextOfKinPhone";
    public static final String L_FOREIGN_LANGUAGE = "foreignLanguage";
    public static final String L_SPORT_TYPE = "sportType";
    public static final String L_SPORT_RANK = "sportRank";
    public static final String L_BENEFIT = "benefit";
    public static final String P_BENEFIT_DOCUMENT = "benefitDocument";
    public static final String P_BENEFIT_DATE = "benefitDate";
    public static final String P_BENEFIT_BASIC = "benefitBasic";
    public static final String P_CHILD_COUNT = "childCount";
    public static final String L_FAMILY_STATUS = "familyStatus";
    public static final String P_WORK_POST = "workPost";
    public static final String P_WORK_PLACE = "workPlace";
    public static final String P_NEED_HOTEL = "needHotel";
    public static final String P_BEGIN_ARMY = "beginArmy";
    public static final String P_END_ARMY = "endArmy";
    public static final String P_END_ARMY_YEAR = "endArmyYear";
    public static final String P_PARTICIPATE_IN_SECOND_WAVE_U_S_E = "participateInSecondWaveUSE";
    public static final String P_PARTICIPATE_IN_ENTRANCE_TESTS = "participateInEntranceTests";
    public static final String L_METHOD_DELIVERY_N_RETURN_DOCS = "methodDeliveryNReturnDocs";
    public static final String L_DOCUMENT_COPIES = "documentCopies";
    public static final String P_DOCUMENT_COPIES_NAME = "documentCopiesName";
    public static final String P_PHONE_FACT = "phoneFact";
    public static final String P_PHONE_WORK = "phoneWork";
    public static final String P_PHONE_MOBILE = "phoneMobile";
    public static final String P_PHONE_RELATIVES = "phoneRelatives";
    public static final String P_EMAIL = "email";

    private long _userId;     // Идентификатор пользователя в econline
    private Entrant _entrant;     // (Старый) Абитуриент
    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private int _personalNumber;     // Регистрационный номер
    private int _registrationYear;     // Год регистрации
    private Date _registrationDate;     // Дата добавления
    private String _lastName;     // Фамилия
    private String _firstName;     // Имя
    private String _middleName;     // Отчество
    private IdentityCardType _passportType;     // Тип документа
    private String _passportSeria;     // Серия
    private String _passportNumber;     // Номер
    private AddressCountry _passportCitizenship;     // Гражданство
    private String _passportIssuancePlace;     // Кем выдан
    private String _passportIssuanceCode;     // Код подразделения
    private Date _passportIssuanceDate;     // Дата выдачи
    private Sex _sex;     // Пол
    private Date _birthDate;     // Дата рождения
    private String _birthPlace;     // Место рождения
    private Address _registrationAddress;     // Адрес регистрации
    private boolean _liveAtRegistration;     // Проживаю по адресу регистрации
    private Address _actualAddress;     // Фактический адрес
    private String _certificateNumber;     // Номер свидетельства ЕГЭ
    private StateExamType _certificatePlace;     // Место сдачи ЕГЭ
    private Date _certificateDate;     // Дата выдачи сертификата ЕГЭ
    private String _olympiadDiplomaNumber;     // Номер диплома
    private String _olympiadDiplomaSeria;     // Серия диплома
    private Date _olympiadDiplomaIssuanceDate;     // Дата выдачи диплома
    private OlympiadDiplomaType _olympiadDiplomaType;     // Тип олимпиады
    private OlympiadDiplomaDegree _olympiadDiplomaDegree;     // Степень отличия в дипломе
    private String _olympiadDiplomaTitle;     // Название олимпиады
    private String _olympiadDiplomaSubject;     // Предмет олимпиады
    private AddressCountry _olympiadAddressCountry;     // Страна проведения олимпиады
    private AddressItem _olympiadAddressSettlement;     // Населенный пункт проведения олимпиады
    private AddressCountry _eduInstitutionCountry;     // Страна образовательного учреждения
    private AddressItem _eduInstitutionSettlement;     // Населенный пункт образовательного учреждения
    private EduInstitution _eduInstitution;     // Образовательное учреждение
    private String _eduInstitutionStr;     // Образовательное учреждение (текст)
    private EducationDocumentType _eduDocumentType;     // Тип документа о полученном образовании
    private EducationLevelStage _eduDocumentLevel;     // Уровень документа о полученном образования
    private String _eduDocumentSeria;     // Серия документа о полученном образовании
    private String _eduDocumentNumber;     // Номер документа о полученном образовании
    private Integer _eduDocumentYearEnd;     // Год окончания образовательного учреждения
    private Date _eduDocumentDate;     // Дата выдачи документа о полученном образовании
    private GraduationHonour _eduDocumentGraduationHonour;     // Степень отличия документа о полученном образовании
    private Integer _mark5;     // Количество оценок «Отлично»
    private Integer _mark4;     // Количество оценок «Хорошо»
    private Integer _mark3;     // Количество оценок «Удовлетворительно»
    private RelationDegree _nextOfKinRelationDegree;     // Степень родства
    private String _nextOfKinLastName;     // Фамилия ближайшего родственника
    private String _nextOfKinFirstName;     // Имя ближайшего родственника
    private String _nextOfKinMiddleName;     // Отчество ближайшего родственника
    private String _nextOfKinWorkPlace;     // Место работы ближайшего родственника
    private String _nextOfKinWorkPost;     // Должность ближайшего родственника
    private String _nextOfKinPhone;     // Телефон ближайшего родственника
    private ForeignLanguage _foreignLanguage;     // Иностранный язык
    private SportType _sportType;     // Вид спорта
    private SportRank _sportRank;     // Спортивные разряды и звания
    private Benefit _benefit;     // Льгота
    private String _benefitDocument;     // Документ
    private Date _benefitDate;     // Дата выдачи документа льготы
    private String _benefitBasic;     // Основание получения льготы
    private Integer _childCount;     // Количество детей
    private FamilyStatus _familyStatus;     // Семейное положение
    private String _workPost;     // Должность
    private String _workPlace;     // Место работы в настоящее время
    private boolean _needHotel;     // Нуждается в общежитии
    private Date _beginArmy;     // Дата начала службы
    private Date _endArmy;     // Дата окончания службы
    private Integer _endArmyYear;     // Год увольнения в запас
    private Boolean _participateInSecondWaveUSE;     // Участвую во второй волне ЕГЭ
    private Boolean _participateInEntranceTests;     // Участвую во вступительных испытаниях, проводимых вузом самостоятельно
    private MethodDeliveryNReturnDocs _methodDeliveryNReturnDocs;     // Способ подачи и возврата оригиналов документов
    private DatabaseFile _documentCopies;     // Копии документов
    private String _documentCopiesName;     // Название файла с документами
    private String _phoneFact;     // Телефон по месту фактического проживания
    private String _phoneWork;     // Рабочий телефон
    private String _phoneMobile;     // Мобильный телефон
    private String _phoneRelatives;     // Телефон родственников
    private String _email;     // Email

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Идентификатор пользователя в econline. Свойство не может быть null.
     */
    @NotNull
    public long getUserId()
    {
        return _userId;
    }

    /**
     * @param userId Идентификатор пользователя в econline. Свойство не может быть null.
     */
    public void setUserId(long userId)
    {
        dirty(_userId, userId);
        _userId = userId;
    }

    /**
     * @return (Старый) Абитуриент.
     */
    public Entrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant (Старый) Абитуриент.
     */
    public void setEntrant(Entrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Регистрационный номер. Свойство не может быть null.
     */
    @NotNull
    public int getPersonalNumber()
    {
        return _personalNumber;
    }

    /**
     * @param personalNumber Регистрационный номер. Свойство не может быть null.
     */
    public void setPersonalNumber(int personalNumber)
    {
        dirty(_personalNumber, personalNumber);
        _personalNumber = personalNumber;
    }

    /**
     * @return Год регистрации. Свойство не может быть null.
     */
    @NotNull
    public int getRegistrationYear()
    {
        return _registrationYear;
    }

    /**
     * @param registrationYear Год регистрации. Свойство не может быть null.
     */
    public void setRegistrationYear(int registrationYear)
    {
        dirty(_registrationYear, registrationYear);
        _registrationYear = registrationYear;
    }

    /**
     * @return Дата добавления. Свойство не может быть null.
     */
    @NotNull
    public Date getRegistrationDate()
    {
        return _registrationDate;
    }

    /**
     * @param registrationDate Дата добавления. Свойство не может быть null.
     */
    public void setRegistrationDate(Date registrationDate)
    {
        dirty(_registrationDate, registrationDate);
        _registrationDate = registrationDate;
    }

    /**
     * @return Фамилия. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getLastName()
    {
        return _lastName;
    }

    /**
     * @param lastName Фамилия. Свойство не может быть null.
     */
    public void setLastName(String lastName)
    {
        dirty(_lastName, lastName);
        _lastName = lastName;
    }

    /**
     * @return Имя. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFirstName()
    {
        return _firstName;
    }

    /**
     * @param firstName Имя. Свойство не может быть null.
     */
    public void setFirstName(String firstName)
    {
        dirty(_firstName, firstName);
        _firstName = firstName;
    }

    /**
     * @return Отчество.
     */
    @Length(max=255)
    public String getMiddleName()
    {
        return _middleName;
    }

    /**
     * @param middleName Отчество.
     */
    public void setMiddleName(String middleName)
    {
        dirty(_middleName, middleName);
        _middleName = middleName;
    }

    /**
     * @return Тип документа. Свойство не может быть null.
     */
    @NotNull
    public IdentityCardType getPassportType()
    {
        return _passportType;
    }

    /**
     * @param passportType Тип документа. Свойство не может быть null.
     */
    public void setPassportType(IdentityCardType passportType)
    {
        dirty(_passportType, passportType);
        _passportType = passportType;
    }

    /**
     * @return Серия.
     */
    @Length(max=255)
    public String getPassportSeria()
    {
        return _passportSeria;
    }

    /**
     * @param passportSeria Серия.
     */
    public void setPassportSeria(String passportSeria)
    {
        dirty(_passportSeria, passportSeria);
        _passportSeria = passportSeria;
    }

    /**
     * @return Номер.
     */
    @Length(max=255)
    public String getPassportNumber()
    {
        return _passportNumber;
    }

    /**
     * @param passportNumber Номер.
     */
    public void setPassportNumber(String passportNumber)
    {
        dirty(_passportNumber, passportNumber);
        _passportNumber = passportNumber;
    }

    /**
     * @return Гражданство.
     */
    public AddressCountry getPassportCitizenship()
    {
        return _passportCitizenship;
    }

    /**
     * @param passportCitizenship Гражданство.
     */
    public void setPassportCitizenship(AddressCountry passportCitizenship)
    {
        dirty(_passportCitizenship, passportCitizenship);
        _passportCitizenship = passportCitizenship;
    }

    /**
     * @return Кем выдан. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPassportIssuancePlace()
    {
        return _passportIssuancePlace;
    }

    /**
     * @param passportIssuancePlace Кем выдан. Свойство не может быть null.
     */
    public void setPassportIssuancePlace(String passportIssuancePlace)
    {
        dirty(_passportIssuancePlace, passportIssuancePlace);
        _passportIssuancePlace = passportIssuancePlace;
    }

    /**
     * @return Код подразделения.
     */
    @Length(max=255)
    public String getPassportIssuanceCode()
    {
        return _passportIssuanceCode;
    }

    /**
     * @param passportIssuanceCode Код подразделения.
     */
    public void setPassportIssuanceCode(String passportIssuanceCode)
    {
        dirty(_passportIssuanceCode, passportIssuanceCode);
        _passportIssuanceCode = passportIssuanceCode;
    }

    /**
     * @return Дата выдачи. Свойство не может быть null.
     */
    @NotNull
    public Date getPassportIssuanceDate()
    {
        return _passportIssuanceDate;
    }

    /**
     * @param passportIssuanceDate Дата выдачи. Свойство не может быть null.
     */
    public void setPassportIssuanceDate(Date passportIssuanceDate)
    {
        dirty(_passportIssuanceDate, passportIssuanceDate);
        _passportIssuanceDate = passportIssuanceDate;
    }

    /**
     * @return Пол. Свойство не может быть null.
     */
    @NotNull
    public Sex getSex()
    {
        return _sex;
    }

    /**
     * @param sex Пол. Свойство не может быть null.
     */
    public void setSex(Sex sex)
    {
        dirty(_sex, sex);
        _sex = sex;
    }

    /**
     * @return Дата рождения. Свойство не может быть null.
     */
    @NotNull
    public Date getBirthDate()
    {
        return _birthDate;
    }

    /**
     * @param birthDate Дата рождения. Свойство не может быть null.
     */
    public void setBirthDate(Date birthDate)
    {
        dirty(_birthDate, birthDate);
        _birthDate = birthDate;
    }

    /**
     * @return Место рождения. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getBirthPlace()
    {
        return _birthPlace;
    }

    /**
     * @param birthPlace Место рождения. Свойство не может быть null.
     */
    public void setBirthPlace(String birthPlace)
    {
        dirty(_birthPlace, birthPlace);
        _birthPlace = birthPlace;
    }

    /**
     * @return Адрес регистрации.
     */
    public Address getRegistrationAddress()
    {
        return _registrationAddress;
    }

    /**
     * @param registrationAddress Адрес регистрации.
     */
    public void setRegistrationAddress(Address registrationAddress)
    {
        dirty(_registrationAddress, registrationAddress);
        _registrationAddress = registrationAddress;
    }

    /**
     * @return Проживаю по адресу регистрации. Свойство не может быть null.
     */
    @NotNull
    public boolean isLiveAtRegistration()
    {
        return _liveAtRegistration;
    }

    /**
     * @param liveAtRegistration Проживаю по адресу регистрации. Свойство не может быть null.
     */
    public void setLiveAtRegistration(boolean liveAtRegistration)
    {
        dirty(_liveAtRegistration, liveAtRegistration);
        _liveAtRegistration = liveAtRegistration;
    }

    /**
     * @return Фактический адрес.
     */
    public Address getActualAddress()
    {
        return _actualAddress;
    }

    /**
     * @param actualAddress Фактический адрес.
     */
    public void setActualAddress(Address actualAddress)
    {
        dirty(_actualAddress, actualAddress);
        _actualAddress = actualAddress;
    }

    /**
     * @return Номер свидетельства ЕГЭ.
     */
    @Length(max=255)
    public String getCertificateNumber()
    {
        return _certificateNumber;
    }

    /**
     * @param certificateNumber Номер свидетельства ЕГЭ.
     */
    public void setCertificateNumber(String certificateNumber)
    {
        dirty(_certificateNumber, certificateNumber);
        _certificateNumber = certificateNumber;
    }

    /**
     * @return Место сдачи ЕГЭ.
     */
    public StateExamType getCertificatePlace()
    {
        return _certificatePlace;
    }

    /**
     * @param certificatePlace Место сдачи ЕГЭ.
     */
    public void setCertificatePlace(StateExamType certificatePlace)
    {
        dirty(_certificatePlace, certificatePlace);
        _certificatePlace = certificatePlace;
    }

    /**
     * @return Дата выдачи сертификата ЕГЭ.
     */
    public Date getCertificateDate()
    {
        return _certificateDate;
    }

    /**
     * @param certificateDate Дата выдачи сертификата ЕГЭ.
     */
    public void setCertificateDate(Date certificateDate)
    {
        dirty(_certificateDate, certificateDate);
        _certificateDate = certificateDate;
    }

    /**
     * @return Номер диплома.
     */
    @Length(max=255)
    public String getOlympiadDiplomaNumber()
    {
        return _olympiadDiplomaNumber;
    }

    /**
     * @param olympiadDiplomaNumber Номер диплома.
     */
    public void setOlympiadDiplomaNumber(String olympiadDiplomaNumber)
    {
        dirty(_olympiadDiplomaNumber, olympiadDiplomaNumber);
        _olympiadDiplomaNumber = olympiadDiplomaNumber;
    }

    /**
     * @return Серия диплома.
     */
    @Length(max=255)
    public String getOlympiadDiplomaSeria()
    {
        return _olympiadDiplomaSeria;
    }

    /**
     * @param olympiadDiplomaSeria Серия диплома.
     */
    public void setOlympiadDiplomaSeria(String olympiadDiplomaSeria)
    {
        dirty(_olympiadDiplomaSeria, olympiadDiplomaSeria);
        _olympiadDiplomaSeria = olympiadDiplomaSeria;
    }

    /**
     * @return Дата выдачи диплома.
     */
    public Date getOlympiadDiplomaIssuanceDate()
    {
        return _olympiadDiplomaIssuanceDate;
    }

    /**
     * @param olympiadDiplomaIssuanceDate Дата выдачи диплома.
     */
    public void setOlympiadDiplomaIssuanceDate(Date olympiadDiplomaIssuanceDate)
    {
        dirty(_olympiadDiplomaIssuanceDate, olympiadDiplomaIssuanceDate);
        _olympiadDiplomaIssuanceDate = olympiadDiplomaIssuanceDate;
    }

    /**
     * @return Тип олимпиады.
     */
    public OlympiadDiplomaType getOlympiadDiplomaType()
    {
        return _olympiadDiplomaType;
    }

    /**
     * @param olympiadDiplomaType Тип олимпиады.
     */
    public void setOlympiadDiplomaType(OlympiadDiplomaType olympiadDiplomaType)
    {
        dirty(_olympiadDiplomaType, olympiadDiplomaType);
        _olympiadDiplomaType = olympiadDiplomaType;
    }

    /**
     * @return Степень отличия в дипломе.
     */
    public OlympiadDiplomaDegree getOlympiadDiplomaDegree()
    {
        return _olympiadDiplomaDegree;
    }

    /**
     * @param olympiadDiplomaDegree Степень отличия в дипломе.
     */
    public void setOlympiadDiplomaDegree(OlympiadDiplomaDegree olympiadDiplomaDegree)
    {
        dirty(_olympiadDiplomaDegree, olympiadDiplomaDegree);
        _olympiadDiplomaDegree = olympiadDiplomaDegree;
    }

    /**
     * @return Название олимпиады.
     */
    @Length(max=255)
    public String getOlympiadDiplomaTitle()
    {
        return _olympiadDiplomaTitle;
    }

    /**
     * @param olympiadDiplomaTitle Название олимпиады.
     */
    public void setOlympiadDiplomaTitle(String olympiadDiplomaTitle)
    {
        dirty(_olympiadDiplomaTitle, olympiadDiplomaTitle);
        _olympiadDiplomaTitle = olympiadDiplomaTitle;
    }

    /**
     * @return Предмет олимпиады.
     */
    @Length(max=255)
    public String getOlympiadDiplomaSubject()
    {
        return _olympiadDiplomaSubject;
    }

    /**
     * @param olympiadDiplomaSubject Предмет олимпиады.
     */
    public void setOlympiadDiplomaSubject(String olympiadDiplomaSubject)
    {
        dirty(_olympiadDiplomaSubject, olympiadDiplomaSubject);
        _olympiadDiplomaSubject = olympiadDiplomaSubject;
    }

    /**
     * @return Страна проведения олимпиады.
     */
    public AddressCountry getOlympiadAddressCountry()
    {
        return _olympiadAddressCountry;
    }

    /**
     * @param olympiadAddressCountry Страна проведения олимпиады.
     */
    public void setOlympiadAddressCountry(AddressCountry olympiadAddressCountry)
    {
        dirty(_olympiadAddressCountry, olympiadAddressCountry);
        _olympiadAddressCountry = olympiadAddressCountry;
    }

    /**
     * @return Населенный пункт проведения олимпиады.
     */
    public AddressItem getOlympiadAddressSettlement()
    {
        return _olympiadAddressSettlement;
    }

    /**
     * @param olympiadAddressSettlement Населенный пункт проведения олимпиады.
     */
    public void setOlympiadAddressSettlement(AddressItem olympiadAddressSettlement)
    {
        dirty(_olympiadAddressSettlement, olympiadAddressSettlement);
        _olympiadAddressSettlement = olympiadAddressSettlement;
    }

    /**
     * @return Страна образовательного учреждения.
     */
    public AddressCountry getEduInstitutionCountry()
    {
        return _eduInstitutionCountry;
    }

    /**
     * @param eduInstitutionCountry Страна образовательного учреждения.
     */
    public void setEduInstitutionCountry(AddressCountry eduInstitutionCountry)
    {
        dirty(_eduInstitutionCountry, eduInstitutionCountry);
        _eduInstitutionCountry = eduInstitutionCountry;
    }

    /**
     * @return Населенный пункт образовательного учреждения.
     */
    public AddressItem getEduInstitutionSettlement()
    {
        return _eduInstitutionSettlement;
    }

    /**
     * @param eduInstitutionSettlement Населенный пункт образовательного учреждения.
     */
    public void setEduInstitutionSettlement(AddressItem eduInstitutionSettlement)
    {
        dirty(_eduInstitutionSettlement, eduInstitutionSettlement);
        _eduInstitutionSettlement = eduInstitutionSettlement;
    }

    /**
     * @return Образовательное учреждение.
     */
    public EduInstitution getEduInstitution()
    {
        return _eduInstitution;
    }

    /**
     * @param eduInstitution Образовательное учреждение.
     */
    public void setEduInstitution(EduInstitution eduInstitution)
    {
        dirty(_eduInstitution, eduInstitution);
        _eduInstitution = eduInstitution;
    }

    /**
     * @return Образовательное учреждение (текст).
     */
    @Length(max=255)
    public String getEduInstitutionStr()
    {
        return _eduInstitutionStr;
    }

    /**
     * @param eduInstitutionStr Образовательное учреждение (текст).
     */
    public void setEduInstitutionStr(String eduInstitutionStr)
    {
        dirty(_eduInstitutionStr, eduInstitutionStr);
        _eduInstitutionStr = eduInstitutionStr;
    }

    /**
     * @return Тип документа о полученном образовании. Свойство не может быть null.
     */
    @NotNull
    public EducationDocumentType getEduDocumentType()
    {
        return _eduDocumentType;
    }

    /**
     * @param eduDocumentType Тип документа о полученном образовании. Свойство не может быть null.
     */
    public void setEduDocumentType(EducationDocumentType eduDocumentType)
    {
        dirty(_eduDocumentType, eduDocumentType);
        _eduDocumentType = eduDocumentType;
    }

    /**
     * @return Уровень документа о полученном образования. Свойство не может быть null.
     */
    @NotNull
    public EducationLevelStage getEduDocumentLevel()
    {
        return _eduDocumentLevel;
    }

    /**
     * @param eduDocumentLevel Уровень документа о полученном образования. Свойство не может быть null.
     */
    public void setEduDocumentLevel(EducationLevelStage eduDocumentLevel)
    {
        dirty(_eduDocumentLevel, eduDocumentLevel);
        _eduDocumentLevel = eduDocumentLevel;
    }

    /**
     * @return Серия документа о полученном образовании.
     */
    @Length(max=255)
    public String getEduDocumentSeria()
    {
        return _eduDocumentSeria;
    }

    /**
     * @param eduDocumentSeria Серия документа о полученном образовании.
     */
    public void setEduDocumentSeria(String eduDocumentSeria)
    {
        dirty(_eduDocumentSeria, eduDocumentSeria);
        _eduDocumentSeria = eduDocumentSeria;
    }

    /**
     * @return Номер документа о полученном образовании.
     */
    @Length(max=255)
    public String getEduDocumentNumber()
    {
        return _eduDocumentNumber;
    }

    /**
     * @param eduDocumentNumber Номер документа о полученном образовании.
     */
    public void setEduDocumentNumber(String eduDocumentNumber)
    {
        dirty(_eduDocumentNumber, eduDocumentNumber);
        _eduDocumentNumber = eduDocumentNumber;
    }

    /**
     * @return Год окончания образовательного учреждения.
     */
    public Integer getEduDocumentYearEnd()
    {
        return _eduDocumentYearEnd;
    }

    /**
     * @param eduDocumentYearEnd Год окончания образовательного учреждения.
     */
    public void setEduDocumentYearEnd(Integer eduDocumentYearEnd)
    {
        dirty(_eduDocumentYearEnd, eduDocumentYearEnd);
        _eduDocumentYearEnd = eduDocumentYearEnd;
    }

    /**
     * @return Дата выдачи документа о полученном образовании.
     */
    public Date getEduDocumentDate()
    {
        return _eduDocumentDate;
    }

    /**
     * @param eduDocumentDate Дата выдачи документа о полученном образовании.
     */
    public void setEduDocumentDate(Date eduDocumentDate)
    {
        dirty(_eduDocumentDate, eduDocumentDate);
        _eduDocumentDate = eduDocumentDate;
    }

    /**
     * @return Степень отличия документа о полученном образовании.
     */
    public GraduationHonour getEduDocumentGraduationHonour()
    {
        return _eduDocumentGraduationHonour;
    }

    /**
     * @param eduDocumentGraduationHonour Степень отличия документа о полученном образовании.
     */
    public void setEduDocumentGraduationHonour(GraduationHonour eduDocumentGraduationHonour)
    {
        dirty(_eduDocumentGraduationHonour, eduDocumentGraduationHonour);
        _eduDocumentGraduationHonour = eduDocumentGraduationHonour;
    }

    /**
     * @return Количество оценок «Отлично».
     */
    public Integer getMark5()
    {
        return _mark5;
    }

    /**
     * @param mark5 Количество оценок «Отлично».
     */
    public void setMark5(Integer mark5)
    {
        dirty(_mark5, mark5);
        _mark5 = mark5;
    }

    /**
     * @return Количество оценок «Хорошо».
     */
    public Integer getMark4()
    {
        return _mark4;
    }

    /**
     * @param mark4 Количество оценок «Хорошо».
     */
    public void setMark4(Integer mark4)
    {
        dirty(_mark4, mark4);
        _mark4 = mark4;
    }

    /**
     * @return Количество оценок «Удовлетворительно».
     */
    public Integer getMark3()
    {
        return _mark3;
    }

    /**
     * @param mark3 Количество оценок «Удовлетворительно».
     */
    public void setMark3(Integer mark3)
    {
        dirty(_mark3, mark3);
        _mark3 = mark3;
    }

    /**
     * @return Степень родства.
     */
    public RelationDegree getNextOfKinRelationDegree()
    {
        return _nextOfKinRelationDegree;
    }

    /**
     * @param nextOfKinRelationDegree Степень родства.
     */
    public void setNextOfKinRelationDegree(RelationDegree nextOfKinRelationDegree)
    {
        dirty(_nextOfKinRelationDegree, nextOfKinRelationDegree);
        _nextOfKinRelationDegree = nextOfKinRelationDegree;
    }

    /**
     * @return Фамилия ближайшего родственника.
     */
    @Length(max=255)
    public String getNextOfKinLastName()
    {
        return _nextOfKinLastName;
    }

    /**
     * @param nextOfKinLastName Фамилия ближайшего родственника.
     */
    public void setNextOfKinLastName(String nextOfKinLastName)
    {
        dirty(_nextOfKinLastName, nextOfKinLastName);
        _nextOfKinLastName = nextOfKinLastName;
    }

    /**
     * @return Имя ближайшего родственника.
     */
    @Length(max=255)
    public String getNextOfKinFirstName()
    {
        return _nextOfKinFirstName;
    }

    /**
     * @param nextOfKinFirstName Имя ближайшего родственника.
     */
    public void setNextOfKinFirstName(String nextOfKinFirstName)
    {
        dirty(_nextOfKinFirstName, nextOfKinFirstName);
        _nextOfKinFirstName = nextOfKinFirstName;
    }

    /**
     * @return Отчество ближайшего родственника.
     */
    @Length(max=255)
    public String getNextOfKinMiddleName()
    {
        return _nextOfKinMiddleName;
    }

    /**
     * @param nextOfKinMiddleName Отчество ближайшего родственника.
     */
    public void setNextOfKinMiddleName(String nextOfKinMiddleName)
    {
        dirty(_nextOfKinMiddleName, nextOfKinMiddleName);
        _nextOfKinMiddleName = nextOfKinMiddleName;
    }

    /**
     * @return Место работы ближайшего родственника.
     */
    @Length(max=255)
    public String getNextOfKinWorkPlace()
    {
        return _nextOfKinWorkPlace;
    }

    /**
     * @param nextOfKinWorkPlace Место работы ближайшего родственника.
     */
    public void setNextOfKinWorkPlace(String nextOfKinWorkPlace)
    {
        dirty(_nextOfKinWorkPlace, nextOfKinWorkPlace);
        _nextOfKinWorkPlace = nextOfKinWorkPlace;
    }

    /**
     * @return Должность ближайшего родственника.
     */
    @Length(max=255)
    public String getNextOfKinWorkPost()
    {
        return _nextOfKinWorkPost;
    }

    /**
     * @param nextOfKinWorkPost Должность ближайшего родственника.
     */
    public void setNextOfKinWorkPost(String nextOfKinWorkPost)
    {
        dirty(_nextOfKinWorkPost, nextOfKinWorkPost);
        _nextOfKinWorkPost = nextOfKinWorkPost;
    }

    /**
     * @return Телефон ближайшего родственника.
     */
    @Length(max=255)
    public String getNextOfKinPhone()
    {
        return _nextOfKinPhone;
    }

    /**
     * @param nextOfKinPhone Телефон ближайшего родственника.
     */
    public void setNextOfKinPhone(String nextOfKinPhone)
    {
        dirty(_nextOfKinPhone, nextOfKinPhone);
        _nextOfKinPhone = nextOfKinPhone;
    }

    /**
     * @return Иностранный язык.
     */
    public ForeignLanguage getForeignLanguage()
    {
        return _foreignLanguage;
    }

    /**
     * @param foreignLanguage Иностранный язык.
     */
    public void setForeignLanguage(ForeignLanguage foreignLanguage)
    {
        dirty(_foreignLanguage, foreignLanguage);
        _foreignLanguage = foreignLanguage;
    }

    /**
     * @return Вид спорта.
     */
    public SportType getSportType()
    {
        return _sportType;
    }

    /**
     * @param sportType Вид спорта.
     */
    public void setSportType(SportType sportType)
    {
        dirty(_sportType, sportType);
        _sportType = sportType;
    }

    /**
     * @return Спортивные разряды и звания.
     */
    public SportRank getSportRank()
    {
        return _sportRank;
    }

    /**
     * @param sportRank Спортивные разряды и звания.
     */
    public void setSportRank(SportRank sportRank)
    {
        dirty(_sportRank, sportRank);
        _sportRank = sportRank;
    }

    /**
     * @return Льгота.
     */
    public Benefit getBenefit()
    {
        return _benefit;
    }

    /**
     * @param benefit Льгота.
     */
    public void setBenefit(Benefit benefit)
    {
        dirty(_benefit, benefit);
        _benefit = benefit;
    }

    /**
     * @return Документ.
     */
    @Length(max=255)
    public String getBenefitDocument()
    {
        return _benefitDocument;
    }

    /**
     * @param benefitDocument Документ.
     */
    public void setBenefitDocument(String benefitDocument)
    {
        dirty(_benefitDocument, benefitDocument);
        _benefitDocument = benefitDocument;
    }

    /**
     * @return Дата выдачи документа льготы.
     */
    public Date getBenefitDate()
    {
        return _benefitDate;
    }

    /**
     * @param benefitDate Дата выдачи документа льготы.
     */
    public void setBenefitDate(Date benefitDate)
    {
        dirty(_benefitDate, benefitDate);
        _benefitDate = benefitDate;
    }

    /**
     * @return Основание получения льготы.
     */
    @Length(max=255)
    public String getBenefitBasic()
    {
        return _benefitBasic;
    }

    /**
     * @param benefitBasic Основание получения льготы.
     */
    public void setBenefitBasic(String benefitBasic)
    {
        dirty(_benefitBasic, benefitBasic);
        _benefitBasic = benefitBasic;
    }

    /**
     * @return Количество детей.
     */
    public Integer getChildCount()
    {
        return _childCount;
    }

    /**
     * @param childCount Количество детей.
     */
    public void setChildCount(Integer childCount)
    {
        dirty(_childCount, childCount);
        _childCount = childCount;
    }

    /**
     * @return Семейное положение.
     */
    public FamilyStatus getFamilyStatus()
    {
        return _familyStatus;
    }

    /**
     * @param familyStatus Семейное положение.
     */
    public void setFamilyStatus(FamilyStatus familyStatus)
    {
        dirty(_familyStatus, familyStatus);
        _familyStatus = familyStatus;
    }

    /**
     * @return Должность.
     */
    @Length(max=255)
    public String getWorkPost()
    {
        return _workPost;
    }

    /**
     * @param workPost Должность.
     */
    public void setWorkPost(String workPost)
    {
        dirty(_workPost, workPost);
        _workPost = workPost;
    }

    /**
     * @return Место работы в настоящее время.
     */
    @Length(max=255)
    public String getWorkPlace()
    {
        return _workPlace;
    }

    /**
     * @param workPlace Место работы в настоящее время.
     */
    public void setWorkPlace(String workPlace)
    {
        dirty(_workPlace, workPlace);
        _workPlace = workPlace;
    }

    /**
     * @return Нуждается в общежитии. Свойство не может быть null.
     */
    @NotNull
    public boolean isNeedHotel()
    {
        return _needHotel;
    }

    /**
     * @param needHotel Нуждается в общежитии. Свойство не может быть null.
     */
    public void setNeedHotel(boolean needHotel)
    {
        dirty(_needHotel, needHotel);
        _needHotel = needHotel;
    }

    /**
     * @return Дата начала службы.
     */
    public Date getBeginArmy()
    {
        return _beginArmy;
    }

    /**
     * @param beginArmy Дата начала службы.
     */
    public void setBeginArmy(Date beginArmy)
    {
        dirty(_beginArmy, beginArmy);
        _beginArmy = beginArmy;
    }

    /**
     * @return Дата окончания службы.
     */
    public Date getEndArmy()
    {
        return _endArmy;
    }

    /**
     * @param endArmy Дата окончания службы.
     */
    public void setEndArmy(Date endArmy)
    {
        dirty(_endArmy, endArmy);
        _endArmy = endArmy;
    }

    /**
     * @return Год увольнения в запас.
     */
    public Integer getEndArmyYear()
    {
        return _endArmyYear;
    }

    /**
     * @param endArmyYear Год увольнения в запас.
     */
    public void setEndArmyYear(Integer endArmyYear)
    {
        dirty(_endArmyYear, endArmyYear);
        _endArmyYear = endArmyYear;
    }

    /**
     * @return Участвую во второй волне ЕГЭ.
     */
    public Boolean getParticipateInSecondWaveUSE()
    {
        return _participateInSecondWaveUSE;
    }

    /**
     * @param participateInSecondWaveUSE Участвую во второй волне ЕГЭ.
     */
    public void setParticipateInSecondWaveUSE(Boolean participateInSecondWaveUSE)
    {
        dirty(_participateInSecondWaveUSE, participateInSecondWaveUSE);
        _participateInSecondWaveUSE = participateInSecondWaveUSE;
    }

    /**
     * @return Участвую во вступительных испытаниях, проводимых вузом самостоятельно.
     */
    public Boolean getParticipateInEntranceTests()
    {
        return _participateInEntranceTests;
    }

    /**
     * @param participateInEntranceTests Участвую во вступительных испытаниях, проводимых вузом самостоятельно.
     */
    public void setParticipateInEntranceTests(Boolean participateInEntranceTests)
    {
        dirty(_participateInEntranceTests, participateInEntranceTests);
        _participateInEntranceTests = participateInEntranceTests;
    }

    /**
     * @return Способ подачи и возврата оригиналов документов.
     */
    public MethodDeliveryNReturnDocs getMethodDeliveryNReturnDocs()
    {
        return _methodDeliveryNReturnDocs;
    }

    /**
     * @param methodDeliveryNReturnDocs Способ подачи и возврата оригиналов документов.
     */
    public void setMethodDeliveryNReturnDocs(MethodDeliveryNReturnDocs methodDeliveryNReturnDocs)
    {
        dirty(_methodDeliveryNReturnDocs, methodDeliveryNReturnDocs);
        _methodDeliveryNReturnDocs = methodDeliveryNReturnDocs;
    }

    /**
     * @return Копии документов.
     */
    public DatabaseFile getDocumentCopies()
    {
        return _documentCopies;
    }

    /**
     * @param documentCopies Копии документов.
     */
    public void setDocumentCopies(DatabaseFile documentCopies)
    {
        dirty(_documentCopies, documentCopies);
        _documentCopies = documentCopies;
    }

    /**
     * @return Название файла с документами.
     */
    @Length(max=255)
    public String getDocumentCopiesName()
    {
        return _documentCopiesName;
    }

    /**
     * @param documentCopiesName Название файла с документами.
     */
    public void setDocumentCopiesName(String documentCopiesName)
    {
        dirty(_documentCopiesName, documentCopiesName);
        _documentCopiesName = documentCopiesName;
    }

    /**
     * @return Телефон по месту фактического проживания.
     */
    @Length(max=255)
    public String getPhoneFact()
    {
        return _phoneFact;
    }

    /**
     * @param phoneFact Телефон по месту фактического проживания.
     */
    public void setPhoneFact(String phoneFact)
    {
        dirty(_phoneFact, phoneFact);
        _phoneFact = phoneFact;
    }

    /**
     * @return Рабочий телефон.
     */
    @Length(max=255)
    public String getPhoneWork()
    {
        return _phoneWork;
    }

    /**
     * @param phoneWork Рабочий телефон.
     */
    public void setPhoneWork(String phoneWork)
    {
        dirty(_phoneWork, phoneWork);
        _phoneWork = phoneWork;
    }

    /**
     * @return Мобильный телефон.
     */
    @Length(max=255)
    public String getPhoneMobile()
    {
        return _phoneMobile;
    }

    /**
     * @param phoneMobile Мобильный телефон.
     */
    public void setPhoneMobile(String phoneMobile)
    {
        dirty(_phoneMobile, phoneMobile);
        _phoneMobile = phoneMobile;
    }

    /**
     * @return Телефон родственников.
     */
    @Length(max=255)
    public String getPhoneRelatives()
    {
        return _phoneRelatives;
    }

    /**
     * @param phoneRelatives Телефон родственников.
     */
    public void setPhoneRelatives(String phoneRelatives)
    {
        dirty(_phoneRelatives, phoneRelatives);
        _phoneRelatives = phoneRelatives;
    }

    /**
     * @return Email.
     */
    @Length(max=255)
    public String getEmail()
    {
        return _email;
    }

    /**
     * @param email Email.
     */
    public void setEmail(String email)
    {
        dirty(_email, email);
        _email = email;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof OnlineEntrantGen)
        {
            setUserId(((OnlineEntrant)another).getUserId());
            setEntrant(((OnlineEntrant)another).getEntrant());
            setEnrollmentCampaign(((OnlineEntrant)another).getEnrollmentCampaign());
            setPersonalNumber(((OnlineEntrant)another).getPersonalNumber());
            setRegistrationYear(((OnlineEntrant)another).getRegistrationYear());
            setRegistrationDate(((OnlineEntrant)another).getRegistrationDate());
            setLastName(((OnlineEntrant)another).getLastName());
            setFirstName(((OnlineEntrant)another).getFirstName());
            setMiddleName(((OnlineEntrant)another).getMiddleName());
            setPassportType(((OnlineEntrant)another).getPassportType());
            setPassportSeria(((OnlineEntrant)another).getPassportSeria());
            setPassportNumber(((OnlineEntrant)another).getPassportNumber());
            setPassportCitizenship(((OnlineEntrant)another).getPassportCitizenship());
            setPassportIssuancePlace(((OnlineEntrant)another).getPassportIssuancePlace());
            setPassportIssuanceCode(((OnlineEntrant)another).getPassportIssuanceCode());
            setPassportIssuanceDate(((OnlineEntrant)another).getPassportIssuanceDate());
            setSex(((OnlineEntrant)another).getSex());
            setBirthDate(((OnlineEntrant)another).getBirthDate());
            setBirthPlace(((OnlineEntrant)another).getBirthPlace());
            setRegistrationAddress(((OnlineEntrant)another).getRegistrationAddress());
            setLiveAtRegistration(((OnlineEntrant)another).isLiveAtRegistration());
            setActualAddress(((OnlineEntrant)another).getActualAddress());
            setCertificateNumber(((OnlineEntrant)another).getCertificateNumber());
            setCertificatePlace(((OnlineEntrant)another).getCertificatePlace());
            setCertificateDate(((OnlineEntrant)another).getCertificateDate());
            setOlympiadDiplomaNumber(((OnlineEntrant)another).getOlympiadDiplomaNumber());
            setOlympiadDiplomaSeria(((OnlineEntrant)another).getOlympiadDiplomaSeria());
            setOlympiadDiplomaIssuanceDate(((OnlineEntrant)another).getOlympiadDiplomaIssuanceDate());
            setOlympiadDiplomaType(((OnlineEntrant)another).getOlympiadDiplomaType());
            setOlympiadDiplomaDegree(((OnlineEntrant)another).getOlympiadDiplomaDegree());
            setOlympiadDiplomaTitle(((OnlineEntrant)another).getOlympiadDiplomaTitle());
            setOlympiadDiplomaSubject(((OnlineEntrant)another).getOlympiadDiplomaSubject());
            setOlympiadAddressCountry(((OnlineEntrant)another).getOlympiadAddressCountry());
            setOlympiadAddressSettlement(((OnlineEntrant)another).getOlympiadAddressSettlement());
            setEduInstitutionCountry(((OnlineEntrant)another).getEduInstitutionCountry());
            setEduInstitutionSettlement(((OnlineEntrant)another).getEduInstitutionSettlement());
            setEduInstitution(((OnlineEntrant)another).getEduInstitution());
            setEduInstitutionStr(((OnlineEntrant)another).getEduInstitutionStr());
            setEduDocumentType(((OnlineEntrant)another).getEduDocumentType());
            setEduDocumentLevel(((OnlineEntrant)another).getEduDocumentLevel());
            setEduDocumentSeria(((OnlineEntrant)another).getEduDocumentSeria());
            setEduDocumentNumber(((OnlineEntrant)another).getEduDocumentNumber());
            setEduDocumentYearEnd(((OnlineEntrant)another).getEduDocumentYearEnd());
            setEduDocumentDate(((OnlineEntrant)another).getEduDocumentDate());
            setEduDocumentGraduationHonour(((OnlineEntrant)another).getEduDocumentGraduationHonour());
            setMark5(((OnlineEntrant)another).getMark5());
            setMark4(((OnlineEntrant)another).getMark4());
            setMark3(((OnlineEntrant)another).getMark3());
            setNextOfKinRelationDegree(((OnlineEntrant)another).getNextOfKinRelationDegree());
            setNextOfKinLastName(((OnlineEntrant)another).getNextOfKinLastName());
            setNextOfKinFirstName(((OnlineEntrant)another).getNextOfKinFirstName());
            setNextOfKinMiddleName(((OnlineEntrant)another).getNextOfKinMiddleName());
            setNextOfKinWorkPlace(((OnlineEntrant)another).getNextOfKinWorkPlace());
            setNextOfKinWorkPost(((OnlineEntrant)another).getNextOfKinWorkPost());
            setNextOfKinPhone(((OnlineEntrant)another).getNextOfKinPhone());
            setForeignLanguage(((OnlineEntrant)another).getForeignLanguage());
            setSportType(((OnlineEntrant)another).getSportType());
            setSportRank(((OnlineEntrant)another).getSportRank());
            setBenefit(((OnlineEntrant)another).getBenefit());
            setBenefitDocument(((OnlineEntrant)another).getBenefitDocument());
            setBenefitDate(((OnlineEntrant)another).getBenefitDate());
            setBenefitBasic(((OnlineEntrant)another).getBenefitBasic());
            setChildCount(((OnlineEntrant)another).getChildCount());
            setFamilyStatus(((OnlineEntrant)another).getFamilyStatus());
            setWorkPost(((OnlineEntrant)another).getWorkPost());
            setWorkPlace(((OnlineEntrant)another).getWorkPlace());
            setNeedHotel(((OnlineEntrant)another).isNeedHotel());
            setBeginArmy(((OnlineEntrant)another).getBeginArmy());
            setEndArmy(((OnlineEntrant)another).getEndArmy());
            setEndArmyYear(((OnlineEntrant)another).getEndArmyYear());
            setParticipateInSecondWaveUSE(((OnlineEntrant)another).getParticipateInSecondWaveUSE());
            setParticipateInEntranceTests(((OnlineEntrant)another).getParticipateInEntranceTests());
            setMethodDeliveryNReturnDocs(((OnlineEntrant)another).getMethodDeliveryNReturnDocs());
            setDocumentCopies(((OnlineEntrant)another).getDocumentCopies());
            setDocumentCopiesName(((OnlineEntrant)another).getDocumentCopiesName());
            setPhoneFact(((OnlineEntrant)another).getPhoneFact());
            setPhoneWork(((OnlineEntrant)another).getPhoneWork());
            setPhoneMobile(((OnlineEntrant)another).getPhoneMobile());
            setPhoneRelatives(((OnlineEntrant)another).getPhoneRelatives());
            setEmail(((OnlineEntrant)another).getEmail());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OnlineEntrantGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OnlineEntrant.class;
        }

        public T newInstance()
        {
            return (T) new OnlineEntrant();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "userId":
                    return obj.getUserId();
                case "entrant":
                    return obj.getEntrant();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "personalNumber":
                    return obj.getPersonalNumber();
                case "registrationYear":
                    return obj.getRegistrationYear();
                case "registrationDate":
                    return obj.getRegistrationDate();
                case "lastName":
                    return obj.getLastName();
                case "firstName":
                    return obj.getFirstName();
                case "middleName":
                    return obj.getMiddleName();
                case "passportType":
                    return obj.getPassportType();
                case "passportSeria":
                    return obj.getPassportSeria();
                case "passportNumber":
                    return obj.getPassportNumber();
                case "passportCitizenship":
                    return obj.getPassportCitizenship();
                case "passportIssuancePlace":
                    return obj.getPassportIssuancePlace();
                case "passportIssuanceCode":
                    return obj.getPassportIssuanceCode();
                case "passportIssuanceDate":
                    return obj.getPassportIssuanceDate();
                case "sex":
                    return obj.getSex();
                case "birthDate":
                    return obj.getBirthDate();
                case "birthPlace":
                    return obj.getBirthPlace();
                case "registrationAddress":
                    return obj.getRegistrationAddress();
                case "liveAtRegistration":
                    return obj.isLiveAtRegistration();
                case "actualAddress":
                    return obj.getActualAddress();
                case "certificateNumber":
                    return obj.getCertificateNumber();
                case "certificatePlace":
                    return obj.getCertificatePlace();
                case "certificateDate":
                    return obj.getCertificateDate();
                case "olympiadDiplomaNumber":
                    return obj.getOlympiadDiplomaNumber();
                case "olympiadDiplomaSeria":
                    return obj.getOlympiadDiplomaSeria();
                case "olympiadDiplomaIssuanceDate":
                    return obj.getOlympiadDiplomaIssuanceDate();
                case "olympiadDiplomaType":
                    return obj.getOlympiadDiplomaType();
                case "olympiadDiplomaDegree":
                    return obj.getOlympiadDiplomaDegree();
                case "olympiadDiplomaTitle":
                    return obj.getOlympiadDiplomaTitle();
                case "olympiadDiplomaSubject":
                    return obj.getOlympiadDiplomaSubject();
                case "olympiadAddressCountry":
                    return obj.getOlympiadAddressCountry();
                case "olympiadAddressSettlement":
                    return obj.getOlympiadAddressSettlement();
                case "eduInstitutionCountry":
                    return obj.getEduInstitutionCountry();
                case "eduInstitutionSettlement":
                    return obj.getEduInstitutionSettlement();
                case "eduInstitution":
                    return obj.getEduInstitution();
                case "eduInstitutionStr":
                    return obj.getEduInstitutionStr();
                case "eduDocumentType":
                    return obj.getEduDocumentType();
                case "eduDocumentLevel":
                    return obj.getEduDocumentLevel();
                case "eduDocumentSeria":
                    return obj.getEduDocumentSeria();
                case "eduDocumentNumber":
                    return obj.getEduDocumentNumber();
                case "eduDocumentYearEnd":
                    return obj.getEduDocumentYearEnd();
                case "eduDocumentDate":
                    return obj.getEduDocumentDate();
                case "eduDocumentGraduationHonour":
                    return obj.getEduDocumentGraduationHonour();
                case "mark5":
                    return obj.getMark5();
                case "mark4":
                    return obj.getMark4();
                case "mark3":
                    return obj.getMark3();
                case "nextOfKinRelationDegree":
                    return obj.getNextOfKinRelationDegree();
                case "nextOfKinLastName":
                    return obj.getNextOfKinLastName();
                case "nextOfKinFirstName":
                    return obj.getNextOfKinFirstName();
                case "nextOfKinMiddleName":
                    return obj.getNextOfKinMiddleName();
                case "nextOfKinWorkPlace":
                    return obj.getNextOfKinWorkPlace();
                case "nextOfKinWorkPost":
                    return obj.getNextOfKinWorkPost();
                case "nextOfKinPhone":
                    return obj.getNextOfKinPhone();
                case "foreignLanguage":
                    return obj.getForeignLanguage();
                case "sportType":
                    return obj.getSportType();
                case "sportRank":
                    return obj.getSportRank();
                case "benefit":
                    return obj.getBenefit();
                case "benefitDocument":
                    return obj.getBenefitDocument();
                case "benefitDate":
                    return obj.getBenefitDate();
                case "benefitBasic":
                    return obj.getBenefitBasic();
                case "childCount":
                    return obj.getChildCount();
                case "familyStatus":
                    return obj.getFamilyStatus();
                case "workPost":
                    return obj.getWorkPost();
                case "workPlace":
                    return obj.getWorkPlace();
                case "needHotel":
                    return obj.isNeedHotel();
                case "beginArmy":
                    return obj.getBeginArmy();
                case "endArmy":
                    return obj.getEndArmy();
                case "endArmyYear":
                    return obj.getEndArmyYear();
                case "participateInSecondWaveUSE":
                    return obj.getParticipateInSecondWaveUSE();
                case "participateInEntranceTests":
                    return obj.getParticipateInEntranceTests();
                case "methodDeliveryNReturnDocs":
                    return obj.getMethodDeliveryNReturnDocs();
                case "documentCopies":
                    return obj.getDocumentCopies();
                case "documentCopiesName":
                    return obj.getDocumentCopiesName();
                case "phoneFact":
                    return obj.getPhoneFact();
                case "phoneWork":
                    return obj.getPhoneWork();
                case "phoneMobile":
                    return obj.getPhoneMobile();
                case "phoneRelatives":
                    return obj.getPhoneRelatives();
                case "email":
                    return obj.getEmail();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "userId":
                    obj.setUserId((Long) value);
                    return;
                case "entrant":
                    obj.setEntrant((Entrant) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "personalNumber":
                    obj.setPersonalNumber((Integer) value);
                    return;
                case "registrationYear":
                    obj.setRegistrationYear((Integer) value);
                    return;
                case "registrationDate":
                    obj.setRegistrationDate((Date) value);
                    return;
                case "lastName":
                    obj.setLastName((String) value);
                    return;
                case "firstName":
                    obj.setFirstName((String) value);
                    return;
                case "middleName":
                    obj.setMiddleName((String) value);
                    return;
                case "passportType":
                    obj.setPassportType((IdentityCardType) value);
                    return;
                case "passportSeria":
                    obj.setPassportSeria((String) value);
                    return;
                case "passportNumber":
                    obj.setPassportNumber((String) value);
                    return;
                case "passportCitizenship":
                    obj.setPassportCitizenship((AddressCountry) value);
                    return;
                case "passportIssuancePlace":
                    obj.setPassportIssuancePlace((String) value);
                    return;
                case "passportIssuanceCode":
                    obj.setPassportIssuanceCode((String) value);
                    return;
                case "passportIssuanceDate":
                    obj.setPassportIssuanceDate((Date) value);
                    return;
                case "sex":
                    obj.setSex((Sex) value);
                    return;
                case "birthDate":
                    obj.setBirthDate((Date) value);
                    return;
                case "birthPlace":
                    obj.setBirthPlace((String) value);
                    return;
                case "registrationAddress":
                    obj.setRegistrationAddress((Address) value);
                    return;
                case "liveAtRegistration":
                    obj.setLiveAtRegistration((Boolean) value);
                    return;
                case "actualAddress":
                    obj.setActualAddress((Address) value);
                    return;
                case "certificateNumber":
                    obj.setCertificateNumber((String) value);
                    return;
                case "certificatePlace":
                    obj.setCertificatePlace((StateExamType) value);
                    return;
                case "certificateDate":
                    obj.setCertificateDate((Date) value);
                    return;
                case "olympiadDiplomaNumber":
                    obj.setOlympiadDiplomaNumber((String) value);
                    return;
                case "olympiadDiplomaSeria":
                    obj.setOlympiadDiplomaSeria((String) value);
                    return;
                case "olympiadDiplomaIssuanceDate":
                    obj.setOlympiadDiplomaIssuanceDate((Date) value);
                    return;
                case "olympiadDiplomaType":
                    obj.setOlympiadDiplomaType((OlympiadDiplomaType) value);
                    return;
                case "olympiadDiplomaDegree":
                    obj.setOlympiadDiplomaDegree((OlympiadDiplomaDegree) value);
                    return;
                case "olympiadDiplomaTitle":
                    obj.setOlympiadDiplomaTitle((String) value);
                    return;
                case "olympiadDiplomaSubject":
                    obj.setOlympiadDiplomaSubject((String) value);
                    return;
                case "olympiadAddressCountry":
                    obj.setOlympiadAddressCountry((AddressCountry) value);
                    return;
                case "olympiadAddressSettlement":
                    obj.setOlympiadAddressSettlement((AddressItem) value);
                    return;
                case "eduInstitutionCountry":
                    obj.setEduInstitutionCountry((AddressCountry) value);
                    return;
                case "eduInstitutionSettlement":
                    obj.setEduInstitutionSettlement((AddressItem) value);
                    return;
                case "eduInstitution":
                    obj.setEduInstitution((EduInstitution) value);
                    return;
                case "eduInstitutionStr":
                    obj.setEduInstitutionStr((String) value);
                    return;
                case "eduDocumentType":
                    obj.setEduDocumentType((EducationDocumentType) value);
                    return;
                case "eduDocumentLevel":
                    obj.setEduDocumentLevel((EducationLevelStage) value);
                    return;
                case "eduDocumentSeria":
                    obj.setEduDocumentSeria((String) value);
                    return;
                case "eduDocumentNumber":
                    obj.setEduDocumentNumber((String) value);
                    return;
                case "eduDocumentYearEnd":
                    obj.setEduDocumentYearEnd((Integer) value);
                    return;
                case "eduDocumentDate":
                    obj.setEduDocumentDate((Date) value);
                    return;
                case "eduDocumentGraduationHonour":
                    obj.setEduDocumentGraduationHonour((GraduationHonour) value);
                    return;
                case "mark5":
                    obj.setMark5((Integer) value);
                    return;
                case "mark4":
                    obj.setMark4((Integer) value);
                    return;
                case "mark3":
                    obj.setMark3((Integer) value);
                    return;
                case "nextOfKinRelationDegree":
                    obj.setNextOfKinRelationDegree((RelationDegree) value);
                    return;
                case "nextOfKinLastName":
                    obj.setNextOfKinLastName((String) value);
                    return;
                case "nextOfKinFirstName":
                    obj.setNextOfKinFirstName((String) value);
                    return;
                case "nextOfKinMiddleName":
                    obj.setNextOfKinMiddleName((String) value);
                    return;
                case "nextOfKinWorkPlace":
                    obj.setNextOfKinWorkPlace((String) value);
                    return;
                case "nextOfKinWorkPost":
                    obj.setNextOfKinWorkPost((String) value);
                    return;
                case "nextOfKinPhone":
                    obj.setNextOfKinPhone((String) value);
                    return;
                case "foreignLanguage":
                    obj.setForeignLanguage((ForeignLanguage) value);
                    return;
                case "sportType":
                    obj.setSportType((SportType) value);
                    return;
                case "sportRank":
                    obj.setSportRank((SportRank) value);
                    return;
                case "benefit":
                    obj.setBenefit((Benefit) value);
                    return;
                case "benefitDocument":
                    obj.setBenefitDocument((String) value);
                    return;
                case "benefitDate":
                    obj.setBenefitDate((Date) value);
                    return;
                case "benefitBasic":
                    obj.setBenefitBasic((String) value);
                    return;
                case "childCount":
                    obj.setChildCount((Integer) value);
                    return;
                case "familyStatus":
                    obj.setFamilyStatus((FamilyStatus) value);
                    return;
                case "workPost":
                    obj.setWorkPost((String) value);
                    return;
                case "workPlace":
                    obj.setWorkPlace((String) value);
                    return;
                case "needHotel":
                    obj.setNeedHotel((Boolean) value);
                    return;
                case "beginArmy":
                    obj.setBeginArmy((Date) value);
                    return;
                case "endArmy":
                    obj.setEndArmy((Date) value);
                    return;
                case "endArmyYear":
                    obj.setEndArmyYear((Integer) value);
                    return;
                case "participateInSecondWaveUSE":
                    obj.setParticipateInSecondWaveUSE((Boolean) value);
                    return;
                case "participateInEntranceTests":
                    obj.setParticipateInEntranceTests((Boolean) value);
                    return;
                case "methodDeliveryNReturnDocs":
                    obj.setMethodDeliveryNReturnDocs((MethodDeliveryNReturnDocs) value);
                    return;
                case "documentCopies":
                    obj.setDocumentCopies((DatabaseFile) value);
                    return;
                case "documentCopiesName":
                    obj.setDocumentCopiesName((String) value);
                    return;
                case "phoneFact":
                    obj.setPhoneFact((String) value);
                    return;
                case "phoneWork":
                    obj.setPhoneWork((String) value);
                    return;
                case "phoneMobile":
                    obj.setPhoneMobile((String) value);
                    return;
                case "phoneRelatives":
                    obj.setPhoneRelatives((String) value);
                    return;
                case "email":
                    obj.setEmail((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "userId":
                        return true;
                case "entrant":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "personalNumber":
                        return true;
                case "registrationYear":
                        return true;
                case "registrationDate":
                        return true;
                case "lastName":
                        return true;
                case "firstName":
                        return true;
                case "middleName":
                        return true;
                case "passportType":
                        return true;
                case "passportSeria":
                        return true;
                case "passportNumber":
                        return true;
                case "passportCitizenship":
                        return true;
                case "passportIssuancePlace":
                        return true;
                case "passportIssuanceCode":
                        return true;
                case "passportIssuanceDate":
                        return true;
                case "sex":
                        return true;
                case "birthDate":
                        return true;
                case "birthPlace":
                        return true;
                case "registrationAddress":
                        return true;
                case "liveAtRegistration":
                        return true;
                case "actualAddress":
                        return true;
                case "certificateNumber":
                        return true;
                case "certificatePlace":
                        return true;
                case "certificateDate":
                        return true;
                case "olympiadDiplomaNumber":
                        return true;
                case "olympiadDiplomaSeria":
                        return true;
                case "olympiadDiplomaIssuanceDate":
                        return true;
                case "olympiadDiplomaType":
                        return true;
                case "olympiadDiplomaDegree":
                        return true;
                case "olympiadDiplomaTitle":
                        return true;
                case "olympiadDiplomaSubject":
                        return true;
                case "olympiadAddressCountry":
                        return true;
                case "olympiadAddressSettlement":
                        return true;
                case "eduInstitutionCountry":
                        return true;
                case "eduInstitutionSettlement":
                        return true;
                case "eduInstitution":
                        return true;
                case "eduInstitutionStr":
                        return true;
                case "eduDocumentType":
                        return true;
                case "eduDocumentLevel":
                        return true;
                case "eduDocumentSeria":
                        return true;
                case "eduDocumentNumber":
                        return true;
                case "eduDocumentYearEnd":
                        return true;
                case "eduDocumentDate":
                        return true;
                case "eduDocumentGraduationHonour":
                        return true;
                case "mark5":
                        return true;
                case "mark4":
                        return true;
                case "mark3":
                        return true;
                case "nextOfKinRelationDegree":
                        return true;
                case "nextOfKinLastName":
                        return true;
                case "nextOfKinFirstName":
                        return true;
                case "nextOfKinMiddleName":
                        return true;
                case "nextOfKinWorkPlace":
                        return true;
                case "nextOfKinWorkPost":
                        return true;
                case "nextOfKinPhone":
                        return true;
                case "foreignLanguage":
                        return true;
                case "sportType":
                        return true;
                case "sportRank":
                        return true;
                case "benefit":
                        return true;
                case "benefitDocument":
                        return true;
                case "benefitDate":
                        return true;
                case "benefitBasic":
                        return true;
                case "childCount":
                        return true;
                case "familyStatus":
                        return true;
                case "workPost":
                        return true;
                case "workPlace":
                        return true;
                case "needHotel":
                        return true;
                case "beginArmy":
                        return true;
                case "endArmy":
                        return true;
                case "endArmyYear":
                        return true;
                case "participateInSecondWaveUSE":
                        return true;
                case "participateInEntranceTests":
                        return true;
                case "methodDeliveryNReturnDocs":
                        return true;
                case "documentCopies":
                        return true;
                case "documentCopiesName":
                        return true;
                case "phoneFact":
                        return true;
                case "phoneWork":
                        return true;
                case "phoneMobile":
                        return true;
                case "phoneRelatives":
                        return true;
                case "email":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "userId":
                    return true;
                case "entrant":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "personalNumber":
                    return true;
                case "registrationYear":
                    return true;
                case "registrationDate":
                    return true;
                case "lastName":
                    return true;
                case "firstName":
                    return true;
                case "middleName":
                    return true;
                case "passportType":
                    return true;
                case "passportSeria":
                    return true;
                case "passportNumber":
                    return true;
                case "passportCitizenship":
                    return true;
                case "passportIssuancePlace":
                    return true;
                case "passportIssuanceCode":
                    return true;
                case "passportIssuanceDate":
                    return true;
                case "sex":
                    return true;
                case "birthDate":
                    return true;
                case "birthPlace":
                    return true;
                case "registrationAddress":
                    return true;
                case "liveAtRegistration":
                    return true;
                case "actualAddress":
                    return true;
                case "certificateNumber":
                    return true;
                case "certificatePlace":
                    return true;
                case "certificateDate":
                    return true;
                case "olympiadDiplomaNumber":
                    return true;
                case "olympiadDiplomaSeria":
                    return true;
                case "olympiadDiplomaIssuanceDate":
                    return true;
                case "olympiadDiplomaType":
                    return true;
                case "olympiadDiplomaDegree":
                    return true;
                case "olympiadDiplomaTitle":
                    return true;
                case "olympiadDiplomaSubject":
                    return true;
                case "olympiadAddressCountry":
                    return true;
                case "olympiadAddressSettlement":
                    return true;
                case "eduInstitutionCountry":
                    return true;
                case "eduInstitutionSettlement":
                    return true;
                case "eduInstitution":
                    return true;
                case "eduInstitutionStr":
                    return true;
                case "eduDocumentType":
                    return true;
                case "eduDocumentLevel":
                    return true;
                case "eduDocumentSeria":
                    return true;
                case "eduDocumentNumber":
                    return true;
                case "eduDocumentYearEnd":
                    return true;
                case "eduDocumentDate":
                    return true;
                case "eduDocumentGraduationHonour":
                    return true;
                case "mark5":
                    return true;
                case "mark4":
                    return true;
                case "mark3":
                    return true;
                case "nextOfKinRelationDegree":
                    return true;
                case "nextOfKinLastName":
                    return true;
                case "nextOfKinFirstName":
                    return true;
                case "nextOfKinMiddleName":
                    return true;
                case "nextOfKinWorkPlace":
                    return true;
                case "nextOfKinWorkPost":
                    return true;
                case "nextOfKinPhone":
                    return true;
                case "foreignLanguage":
                    return true;
                case "sportType":
                    return true;
                case "sportRank":
                    return true;
                case "benefit":
                    return true;
                case "benefitDocument":
                    return true;
                case "benefitDate":
                    return true;
                case "benefitBasic":
                    return true;
                case "childCount":
                    return true;
                case "familyStatus":
                    return true;
                case "workPost":
                    return true;
                case "workPlace":
                    return true;
                case "needHotel":
                    return true;
                case "beginArmy":
                    return true;
                case "endArmy":
                    return true;
                case "endArmyYear":
                    return true;
                case "participateInSecondWaveUSE":
                    return true;
                case "participateInEntranceTests":
                    return true;
                case "methodDeliveryNReturnDocs":
                    return true;
                case "documentCopies":
                    return true;
                case "documentCopiesName":
                    return true;
                case "phoneFact":
                    return true;
                case "phoneWork":
                    return true;
                case "phoneMobile":
                    return true;
                case "phoneRelatives":
                    return true;
                case "email":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "userId":
                    return Long.class;
                case "entrant":
                    return Entrant.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "personalNumber":
                    return Integer.class;
                case "registrationYear":
                    return Integer.class;
                case "registrationDate":
                    return Date.class;
                case "lastName":
                    return String.class;
                case "firstName":
                    return String.class;
                case "middleName":
                    return String.class;
                case "passportType":
                    return IdentityCardType.class;
                case "passportSeria":
                    return String.class;
                case "passportNumber":
                    return String.class;
                case "passportCitizenship":
                    return AddressCountry.class;
                case "passportIssuancePlace":
                    return String.class;
                case "passportIssuanceCode":
                    return String.class;
                case "passportIssuanceDate":
                    return Date.class;
                case "sex":
                    return Sex.class;
                case "birthDate":
                    return Date.class;
                case "birthPlace":
                    return String.class;
                case "registrationAddress":
                    return Address.class;
                case "liveAtRegistration":
                    return Boolean.class;
                case "actualAddress":
                    return Address.class;
                case "certificateNumber":
                    return String.class;
                case "certificatePlace":
                    return StateExamType.class;
                case "certificateDate":
                    return Date.class;
                case "olympiadDiplomaNumber":
                    return String.class;
                case "olympiadDiplomaSeria":
                    return String.class;
                case "olympiadDiplomaIssuanceDate":
                    return Date.class;
                case "olympiadDiplomaType":
                    return OlympiadDiplomaType.class;
                case "olympiadDiplomaDegree":
                    return OlympiadDiplomaDegree.class;
                case "olympiadDiplomaTitle":
                    return String.class;
                case "olympiadDiplomaSubject":
                    return String.class;
                case "olympiadAddressCountry":
                    return AddressCountry.class;
                case "olympiadAddressSettlement":
                    return AddressItem.class;
                case "eduInstitutionCountry":
                    return AddressCountry.class;
                case "eduInstitutionSettlement":
                    return AddressItem.class;
                case "eduInstitution":
                    return EduInstitution.class;
                case "eduInstitutionStr":
                    return String.class;
                case "eduDocumentType":
                    return EducationDocumentType.class;
                case "eduDocumentLevel":
                    return EducationLevelStage.class;
                case "eduDocumentSeria":
                    return String.class;
                case "eduDocumentNumber":
                    return String.class;
                case "eduDocumentYearEnd":
                    return Integer.class;
                case "eduDocumentDate":
                    return Date.class;
                case "eduDocumentGraduationHonour":
                    return GraduationHonour.class;
                case "mark5":
                    return Integer.class;
                case "mark4":
                    return Integer.class;
                case "mark3":
                    return Integer.class;
                case "nextOfKinRelationDegree":
                    return RelationDegree.class;
                case "nextOfKinLastName":
                    return String.class;
                case "nextOfKinFirstName":
                    return String.class;
                case "nextOfKinMiddleName":
                    return String.class;
                case "nextOfKinWorkPlace":
                    return String.class;
                case "nextOfKinWorkPost":
                    return String.class;
                case "nextOfKinPhone":
                    return String.class;
                case "foreignLanguage":
                    return ForeignLanguage.class;
                case "sportType":
                    return SportType.class;
                case "sportRank":
                    return SportRank.class;
                case "benefit":
                    return Benefit.class;
                case "benefitDocument":
                    return String.class;
                case "benefitDate":
                    return Date.class;
                case "benefitBasic":
                    return String.class;
                case "childCount":
                    return Integer.class;
                case "familyStatus":
                    return FamilyStatus.class;
                case "workPost":
                    return String.class;
                case "workPlace":
                    return String.class;
                case "needHotel":
                    return Boolean.class;
                case "beginArmy":
                    return Date.class;
                case "endArmy":
                    return Date.class;
                case "endArmyYear":
                    return Integer.class;
                case "participateInSecondWaveUSE":
                    return Boolean.class;
                case "participateInEntranceTests":
                    return Boolean.class;
                case "methodDeliveryNReturnDocs":
                    return MethodDeliveryNReturnDocs.class;
                case "documentCopies":
                    return DatabaseFile.class;
                case "documentCopiesName":
                    return String.class;
                case "phoneFact":
                    return String.class;
                case "phoneWork":
                    return String.class;
                case "phoneMobile":
                    return String.class;
                case "phoneRelatives":
                    return String.class;
                case "email":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OnlineEntrant> _dslPath = new Path<OnlineEntrant>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OnlineEntrant");
    }
            

    /**
     * @return Идентификатор пользователя в econline. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getUserId()
     */
    public static PropertyPath<Long> userId()
    {
        return _dslPath.userId();
    }

    /**
     * @return (Старый) Абитуриент.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getEntrant()
     */
    public static Entrant.Path<Entrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Регистрационный номер. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getPersonalNumber()
     */
    public static PropertyPath<Integer> personalNumber()
    {
        return _dslPath.personalNumber();
    }

    /**
     * @return Год регистрации. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getRegistrationYear()
     */
    public static PropertyPath<Integer> registrationYear()
    {
        return _dslPath.registrationYear();
    }

    /**
     * @return Дата добавления. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getRegistrationDate()
     */
    public static PropertyPath<Date> registrationDate()
    {
        return _dslPath.registrationDate();
    }

    /**
     * @return Фамилия. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getLastName()
     */
    public static PropertyPath<String> lastName()
    {
        return _dslPath.lastName();
    }

    /**
     * @return Имя. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getFirstName()
     */
    public static PropertyPath<String> firstName()
    {
        return _dslPath.firstName();
    }

    /**
     * @return Отчество.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getMiddleName()
     */
    public static PropertyPath<String> middleName()
    {
        return _dslPath.middleName();
    }

    /**
     * @return Тип документа. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getPassportType()
     */
    public static IdentityCardType.Path<IdentityCardType> passportType()
    {
        return _dslPath.passportType();
    }

    /**
     * @return Серия.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getPassportSeria()
     */
    public static PropertyPath<String> passportSeria()
    {
        return _dslPath.passportSeria();
    }

    /**
     * @return Номер.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getPassportNumber()
     */
    public static PropertyPath<String> passportNumber()
    {
        return _dslPath.passportNumber();
    }

    /**
     * @return Гражданство.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getPassportCitizenship()
     */
    public static AddressCountry.Path<AddressCountry> passportCitizenship()
    {
        return _dslPath.passportCitizenship();
    }

    /**
     * @return Кем выдан. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getPassportIssuancePlace()
     */
    public static PropertyPath<String> passportIssuancePlace()
    {
        return _dslPath.passportIssuancePlace();
    }

    /**
     * @return Код подразделения.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getPassportIssuanceCode()
     */
    public static PropertyPath<String> passportIssuanceCode()
    {
        return _dslPath.passportIssuanceCode();
    }

    /**
     * @return Дата выдачи. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getPassportIssuanceDate()
     */
    public static PropertyPath<Date> passportIssuanceDate()
    {
        return _dslPath.passportIssuanceDate();
    }

    /**
     * @return Пол. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getSex()
     */
    public static Sex.Path<Sex> sex()
    {
        return _dslPath.sex();
    }

    /**
     * @return Дата рождения. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getBirthDate()
     */
    public static PropertyPath<Date> birthDate()
    {
        return _dslPath.birthDate();
    }

    /**
     * @return Место рождения. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getBirthPlace()
     */
    public static PropertyPath<String> birthPlace()
    {
        return _dslPath.birthPlace();
    }

    /**
     * @return Адрес регистрации.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getRegistrationAddress()
     */
    public static Address.Path<Address> registrationAddress()
    {
        return _dslPath.registrationAddress();
    }

    /**
     * @return Проживаю по адресу регистрации. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#isLiveAtRegistration()
     */
    public static PropertyPath<Boolean> liveAtRegistration()
    {
        return _dslPath.liveAtRegistration();
    }

    /**
     * @return Фактический адрес.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getActualAddress()
     */
    public static Address.Path<Address> actualAddress()
    {
        return _dslPath.actualAddress();
    }

    /**
     * @return Номер свидетельства ЕГЭ.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getCertificateNumber()
     */
    public static PropertyPath<String> certificateNumber()
    {
        return _dslPath.certificateNumber();
    }

    /**
     * @return Место сдачи ЕГЭ.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getCertificatePlace()
     */
    public static StateExamType.Path<StateExamType> certificatePlace()
    {
        return _dslPath.certificatePlace();
    }

    /**
     * @return Дата выдачи сертификата ЕГЭ.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getCertificateDate()
     */
    public static PropertyPath<Date> certificateDate()
    {
        return _dslPath.certificateDate();
    }

    /**
     * @return Номер диплома.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getOlympiadDiplomaNumber()
     */
    public static PropertyPath<String> olympiadDiplomaNumber()
    {
        return _dslPath.olympiadDiplomaNumber();
    }

    /**
     * @return Серия диплома.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getOlympiadDiplomaSeria()
     */
    public static PropertyPath<String> olympiadDiplomaSeria()
    {
        return _dslPath.olympiadDiplomaSeria();
    }

    /**
     * @return Дата выдачи диплома.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getOlympiadDiplomaIssuanceDate()
     */
    public static PropertyPath<Date> olympiadDiplomaIssuanceDate()
    {
        return _dslPath.olympiadDiplomaIssuanceDate();
    }

    /**
     * @return Тип олимпиады.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getOlympiadDiplomaType()
     */
    public static OlympiadDiplomaType.Path<OlympiadDiplomaType> olympiadDiplomaType()
    {
        return _dslPath.olympiadDiplomaType();
    }

    /**
     * @return Степень отличия в дипломе.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getOlympiadDiplomaDegree()
     */
    public static OlympiadDiplomaDegree.Path<OlympiadDiplomaDegree> olympiadDiplomaDegree()
    {
        return _dslPath.olympiadDiplomaDegree();
    }

    /**
     * @return Название олимпиады.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getOlympiadDiplomaTitle()
     */
    public static PropertyPath<String> olympiadDiplomaTitle()
    {
        return _dslPath.olympiadDiplomaTitle();
    }

    /**
     * @return Предмет олимпиады.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getOlympiadDiplomaSubject()
     */
    public static PropertyPath<String> olympiadDiplomaSubject()
    {
        return _dslPath.olympiadDiplomaSubject();
    }

    /**
     * @return Страна проведения олимпиады.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getOlympiadAddressCountry()
     */
    public static AddressCountry.Path<AddressCountry> olympiadAddressCountry()
    {
        return _dslPath.olympiadAddressCountry();
    }

    /**
     * @return Населенный пункт проведения олимпиады.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getOlympiadAddressSettlement()
     */
    public static AddressItem.Path<AddressItem> olympiadAddressSettlement()
    {
        return _dslPath.olympiadAddressSettlement();
    }

    /**
     * @return Страна образовательного учреждения.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getEduInstitutionCountry()
     */
    public static AddressCountry.Path<AddressCountry> eduInstitutionCountry()
    {
        return _dslPath.eduInstitutionCountry();
    }

    /**
     * @return Населенный пункт образовательного учреждения.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getEduInstitutionSettlement()
     */
    public static AddressItem.Path<AddressItem> eduInstitutionSettlement()
    {
        return _dslPath.eduInstitutionSettlement();
    }

    /**
     * @return Образовательное учреждение.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getEduInstitution()
     */
    public static EduInstitution.Path<EduInstitution> eduInstitution()
    {
        return _dslPath.eduInstitution();
    }

    /**
     * @return Образовательное учреждение (текст).
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getEduInstitutionStr()
     */
    public static PropertyPath<String> eduInstitutionStr()
    {
        return _dslPath.eduInstitutionStr();
    }

    /**
     * @return Тип документа о полученном образовании. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getEduDocumentType()
     */
    public static EducationDocumentType.Path<EducationDocumentType> eduDocumentType()
    {
        return _dslPath.eduDocumentType();
    }

    /**
     * @return Уровень документа о полученном образования. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getEduDocumentLevel()
     */
    public static EducationLevelStage.Path<EducationLevelStage> eduDocumentLevel()
    {
        return _dslPath.eduDocumentLevel();
    }

    /**
     * @return Серия документа о полученном образовании.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getEduDocumentSeria()
     */
    public static PropertyPath<String> eduDocumentSeria()
    {
        return _dslPath.eduDocumentSeria();
    }

    /**
     * @return Номер документа о полученном образовании.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getEduDocumentNumber()
     */
    public static PropertyPath<String> eduDocumentNumber()
    {
        return _dslPath.eduDocumentNumber();
    }

    /**
     * @return Год окончания образовательного учреждения.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getEduDocumentYearEnd()
     */
    public static PropertyPath<Integer> eduDocumentYearEnd()
    {
        return _dslPath.eduDocumentYearEnd();
    }

    /**
     * @return Дата выдачи документа о полученном образовании.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getEduDocumentDate()
     */
    public static PropertyPath<Date> eduDocumentDate()
    {
        return _dslPath.eduDocumentDate();
    }

    /**
     * @return Степень отличия документа о полученном образовании.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getEduDocumentGraduationHonour()
     */
    public static GraduationHonour.Path<GraduationHonour> eduDocumentGraduationHonour()
    {
        return _dslPath.eduDocumentGraduationHonour();
    }

    /**
     * @return Количество оценок «Отлично».
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getMark5()
     */
    public static PropertyPath<Integer> mark5()
    {
        return _dslPath.mark5();
    }

    /**
     * @return Количество оценок «Хорошо».
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getMark4()
     */
    public static PropertyPath<Integer> mark4()
    {
        return _dslPath.mark4();
    }

    /**
     * @return Количество оценок «Удовлетворительно».
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getMark3()
     */
    public static PropertyPath<Integer> mark3()
    {
        return _dslPath.mark3();
    }

    /**
     * @return Степень родства.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getNextOfKinRelationDegree()
     */
    public static RelationDegree.Path<RelationDegree> nextOfKinRelationDegree()
    {
        return _dslPath.nextOfKinRelationDegree();
    }

    /**
     * @return Фамилия ближайшего родственника.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getNextOfKinLastName()
     */
    public static PropertyPath<String> nextOfKinLastName()
    {
        return _dslPath.nextOfKinLastName();
    }

    /**
     * @return Имя ближайшего родственника.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getNextOfKinFirstName()
     */
    public static PropertyPath<String> nextOfKinFirstName()
    {
        return _dslPath.nextOfKinFirstName();
    }

    /**
     * @return Отчество ближайшего родственника.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getNextOfKinMiddleName()
     */
    public static PropertyPath<String> nextOfKinMiddleName()
    {
        return _dslPath.nextOfKinMiddleName();
    }

    /**
     * @return Место работы ближайшего родственника.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getNextOfKinWorkPlace()
     */
    public static PropertyPath<String> nextOfKinWorkPlace()
    {
        return _dslPath.nextOfKinWorkPlace();
    }

    /**
     * @return Должность ближайшего родственника.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getNextOfKinWorkPost()
     */
    public static PropertyPath<String> nextOfKinWorkPost()
    {
        return _dslPath.nextOfKinWorkPost();
    }

    /**
     * @return Телефон ближайшего родственника.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getNextOfKinPhone()
     */
    public static PropertyPath<String> nextOfKinPhone()
    {
        return _dslPath.nextOfKinPhone();
    }

    /**
     * @return Иностранный язык.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getForeignLanguage()
     */
    public static ForeignLanguage.Path<ForeignLanguage> foreignLanguage()
    {
        return _dslPath.foreignLanguage();
    }

    /**
     * @return Вид спорта.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getSportType()
     */
    public static SportType.Path<SportType> sportType()
    {
        return _dslPath.sportType();
    }

    /**
     * @return Спортивные разряды и звания.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getSportRank()
     */
    public static SportRank.Path<SportRank> sportRank()
    {
        return _dslPath.sportRank();
    }

    /**
     * @return Льгота.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getBenefit()
     */
    public static Benefit.Path<Benefit> benefit()
    {
        return _dslPath.benefit();
    }

    /**
     * @return Документ.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getBenefitDocument()
     */
    public static PropertyPath<String> benefitDocument()
    {
        return _dslPath.benefitDocument();
    }

    /**
     * @return Дата выдачи документа льготы.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getBenefitDate()
     */
    public static PropertyPath<Date> benefitDate()
    {
        return _dslPath.benefitDate();
    }

    /**
     * @return Основание получения льготы.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getBenefitBasic()
     */
    public static PropertyPath<String> benefitBasic()
    {
        return _dslPath.benefitBasic();
    }

    /**
     * @return Количество детей.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getChildCount()
     */
    public static PropertyPath<Integer> childCount()
    {
        return _dslPath.childCount();
    }

    /**
     * @return Семейное положение.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getFamilyStatus()
     */
    public static FamilyStatus.Path<FamilyStatus> familyStatus()
    {
        return _dslPath.familyStatus();
    }

    /**
     * @return Должность.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getWorkPost()
     */
    public static PropertyPath<String> workPost()
    {
        return _dslPath.workPost();
    }

    /**
     * @return Место работы в настоящее время.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getWorkPlace()
     */
    public static PropertyPath<String> workPlace()
    {
        return _dslPath.workPlace();
    }

    /**
     * @return Нуждается в общежитии. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#isNeedHotel()
     */
    public static PropertyPath<Boolean> needHotel()
    {
        return _dslPath.needHotel();
    }

    /**
     * @return Дата начала службы.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getBeginArmy()
     */
    public static PropertyPath<Date> beginArmy()
    {
        return _dslPath.beginArmy();
    }

    /**
     * @return Дата окончания службы.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getEndArmy()
     */
    public static PropertyPath<Date> endArmy()
    {
        return _dslPath.endArmy();
    }

    /**
     * @return Год увольнения в запас.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getEndArmyYear()
     */
    public static PropertyPath<Integer> endArmyYear()
    {
        return _dslPath.endArmyYear();
    }

    /**
     * @return Участвую во второй волне ЕГЭ.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getParticipateInSecondWaveUSE()
     */
    public static PropertyPath<Boolean> participateInSecondWaveUSE()
    {
        return _dslPath.participateInSecondWaveUSE();
    }

    /**
     * @return Участвую во вступительных испытаниях, проводимых вузом самостоятельно.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getParticipateInEntranceTests()
     */
    public static PropertyPath<Boolean> participateInEntranceTests()
    {
        return _dslPath.participateInEntranceTests();
    }

    /**
     * @return Способ подачи и возврата оригиналов документов.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getMethodDeliveryNReturnDocs()
     */
    public static MethodDeliveryNReturnDocs.Path<MethodDeliveryNReturnDocs> methodDeliveryNReturnDocs()
    {
        return _dslPath.methodDeliveryNReturnDocs();
    }

    /**
     * @return Копии документов.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getDocumentCopies()
     */
    public static DatabaseFile.Path<DatabaseFile> documentCopies()
    {
        return _dslPath.documentCopies();
    }

    /**
     * @return Название файла с документами.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getDocumentCopiesName()
     */
    public static PropertyPath<String> documentCopiesName()
    {
        return _dslPath.documentCopiesName();
    }

    /**
     * @return Телефон по месту фактического проживания.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getPhoneFact()
     */
    public static PropertyPath<String> phoneFact()
    {
        return _dslPath.phoneFact();
    }

    /**
     * @return Рабочий телефон.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getPhoneWork()
     */
    public static PropertyPath<String> phoneWork()
    {
        return _dslPath.phoneWork();
    }

    /**
     * @return Мобильный телефон.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getPhoneMobile()
     */
    public static PropertyPath<String> phoneMobile()
    {
        return _dslPath.phoneMobile();
    }

    /**
     * @return Телефон родственников.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getPhoneRelatives()
     */
    public static PropertyPath<String> phoneRelatives()
    {
        return _dslPath.phoneRelatives();
    }

    /**
     * @return Email.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getEmail()
     */
    public static PropertyPath<String> email()
    {
        return _dslPath.email();
    }

    public static class Path<E extends OnlineEntrant> extends EntityPath<E>
    {
        private PropertyPath<Long> _userId;
        private Entrant.Path<Entrant> _entrant;
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<Integer> _personalNumber;
        private PropertyPath<Integer> _registrationYear;
        private PropertyPath<Date> _registrationDate;
        private PropertyPath<String> _lastName;
        private PropertyPath<String> _firstName;
        private PropertyPath<String> _middleName;
        private IdentityCardType.Path<IdentityCardType> _passportType;
        private PropertyPath<String> _passportSeria;
        private PropertyPath<String> _passportNumber;
        private AddressCountry.Path<AddressCountry> _passportCitizenship;
        private PropertyPath<String> _passportIssuancePlace;
        private PropertyPath<String> _passportIssuanceCode;
        private PropertyPath<Date> _passportIssuanceDate;
        private Sex.Path<Sex> _sex;
        private PropertyPath<Date> _birthDate;
        private PropertyPath<String> _birthPlace;
        private Address.Path<Address> _registrationAddress;
        private PropertyPath<Boolean> _liveAtRegistration;
        private Address.Path<Address> _actualAddress;
        private PropertyPath<String> _certificateNumber;
        private StateExamType.Path<StateExamType> _certificatePlace;
        private PropertyPath<Date> _certificateDate;
        private PropertyPath<String> _olympiadDiplomaNumber;
        private PropertyPath<String> _olympiadDiplomaSeria;
        private PropertyPath<Date> _olympiadDiplomaIssuanceDate;
        private OlympiadDiplomaType.Path<OlympiadDiplomaType> _olympiadDiplomaType;
        private OlympiadDiplomaDegree.Path<OlympiadDiplomaDegree> _olympiadDiplomaDegree;
        private PropertyPath<String> _olympiadDiplomaTitle;
        private PropertyPath<String> _olympiadDiplomaSubject;
        private AddressCountry.Path<AddressCountry> _olympiadAddressCountry;
        private AddressItem.Path<AddressItem> _olympiadAddressSettlement;
        private AddressCountry.Path<AddressCountry> _eduInstitutionCountry;
        private AddressItem.Path<AddressItem> _eduInstitutionSettlement;
        private EduInstitution.Path<EduInstitution> _eduInstitution;
        private PropertyPath<String> _eduInstitutionStr;
        private EducationDocumentType.Path<EducationDocumentType> _eduDocumentType;
        private EducationLevelStage.Path<EducationLevelStage> _eduDocumentLevel;
        private PropertyPath<String> _eduDocumentSeria;
        private PropertyPath<String> _eduDocumentNumber;
        private PropertyPath<Integer> _eduDocumentYearEnd;
        private PropertyPath<Date> _eduDocumentDate;
        private GraduationHonour.Path<GraduationHonour> _eduDocumentGraduationHonour;
        private PropertyPath<Integer> _mark5;
        private PropertyPath<Integer> _mark4;
        private PropertyPath<Integer> _mark3;
        private RelationDegree.Path<RelationDegree> _nextOfKinRelationDegree;
        private PropertyPath<String> _nextOfKinLastName;
        private PropertyPath<String> _nextOfKinFirstName;
        private PropertyPath<String> _nextOfKinMiddleName;
        private PropertyPath<String> _nextOfKinWorkPlace;
        private PropertyPath<String> _nextOfKinWorkPost;
        private PropertyPath<String> _nextOfKinPhone;
        private ForeignLanguage.Path<ForeignLanguage> _foreignLanguage;
        private SportType.Path<SportType> _sportType;
        private SportRank.Path<SportRank> _sportRank;
        private Benefit.Path<Benefit> _benefit;
        private PropertyPath<String> _benefitDocument;
        private PropertyPath<Date> _benefitDate;
        private PropertyPath<String> _benefitBasic;
        private PropertyPath<Integer> _childCount;
        private FamilyStatus.Path<FamilyStatus> _familyStatus;
        private PropertyPath<String> _workPost;
        private PropertyPath<String> _workPlace;
        private PropertyPath<Boolean> _needHotel;
        private PropertyPath<Date> _beginArmy;
        private PropertyPath<Date> _endArmy;
        private PropertyPath<Integer> _endArmyYear;
        private PropertyPath<Boolean> _participateInSecondWaveUSE;
        private PropertyPath<Boolean> _participateInEntranceTests;
        private MethodDeliveryNReturnDocs.Path<MethodDeliveryNReturnDocs> _methodDeliveryNReturnDocs;
        private DatabaseFile.Path<DatabaseFile> _documentCopies;
        private PropertyPath<String> _documentCopiesName;
        private PropertyPath<String> _phoneFact;
        private PropertyPath<String> _phoneWork;
        private PropertyPath<String> _phoneMobile;
        private PropertyPath<String> _phoneRelatives;
        private PropertyPath<String> _email;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Идентификатор пользователя в econline. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getUserId()
     */
        public PropertyPath<Long> userId()
        {
            if(_userId == null )
                _userId = new PropertyPath<Long>(OnlineEntrantGen.P_USER_ID, this);
            return _userId;
        }

    /**
     * @return (Старый) Абитуриент.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getEntrant()
     */
        public Entrant.Path<Entrant> entrant()
        {
            if(_entrant == null )
                _entrant = new Entrant.Path<Entrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Регистрационный номер. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getPersonalNumber()
     */
        public PropertyPath<Integer> personalNumber()
        {
            if(_personalNumber == null )
                _personalNumber = new PropertyPath<Integer>(OnlineEntrantGen.P_PERSONAL_NUMBER, this);
            return _personalNumber;
        }

    /**
     * @return Год регистрации. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getRegistrationYear()
     */
        public PropertyPath<Integer> registrationYear()
        {
            if(_registrationYear == null )
                _registrationYear = new PropertyPath<Integer>(OnlineEntrantGen.P_REGISTRATION_YEAR, this);
            return _registrationYear;
        }

    /**
     * @return Дата добавления. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getRegistrationDate()
     */
        public PropertyPath<Date> registrationDate()
        {
            if(_registrationDate == null )
                _registrationDate = new PropertyPath<Date>(OnlineEntrantGen.P_REGISTRATION_DATE, this);
            return _registrationDate;
        }

    /**
     * @return Фамилия. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getLastName()
     */
        public PropertyPath<String> lastName()
        {
            if(_lastName == null )
                _lastName = new PropertyPath<String>(OnlineEntrantGen.P_LAST_NAME, this);
            return _lastName;
        }

    /**
     * @return Имя. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getFirstName()
     */
        public PropertyPath<String> firstName()
        {
            if(_firstName == null )
                _firstName = new PropertyPath<String>(OnlineEntrantGen.P_FIRST_NAME, this);
            return _firstName;
        }

    /**
     * @return Отчество.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getMiddleName()
     */
        public PropertyPath<String> middleName()
        {
            if(_middleName == null )
                _middleName = new PropertyPath<String>(OnlineEntrantGen.P_MIDDLE_NAME, this);
            return _middleName;
        }

    /**
     * @return Тип документа. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getPassportType()
     */
        public IdentityCardType.Path<IdentityCardType> passportType()
        {
            if(_passportType == null )
                _passportType = new IdentityCardType.Path<IdentityCardType>(L_PASSPORT_TYPE, this);
            return _passportType;
        }

    /**
     * @return Серия.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getPassportSeria()
     */
        public PropertyPath<String> passportSeria()
        {
            if(_passportSeria == null )
                _passportSeria = new PropertyPath<String>(OnlineEntrantGen.P_PASSPORT_SERIA, this);
            return _passportSeria;
        }

    /**
     * @return Номер.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getPassportNumber()
     */
        public PropertyPath<String> passportNumber()
        {
            if(_passportNumber == null )
                _passportNumber = new PropertyPath<String>(OnlineEntrantGen.P_PASSPORT_NUMBER, this);
            return _passportNumber;
        }

    /**
     * @return Гражданство.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getPassportCitizenship()
     */
        public AddressCountry.Path<AddressCountry> passportCitizenship()
        {
            if(_passportCitizenship == null )
                _passportCitizenship = new AddressCountry.Path<AddressCountry>(L_PASSPORT_CITIZENSHIP, this);
            return _passportCitizenship;
        }

    /**
     * @return Кем выдан. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getPassportIssuancePlace()
     */
        public PropertyPath<String> passportIssuancePlace()
        {
            if(_passportIssuancePlace == null )
                _passportIssuancePlace = new PropertyPath<String>(OnlineEntrantGen.P_PASSPORT_ISSUANCE_PLACE, this);
            return _passportIssuancePlace;
        }

    /**
     * @return Код подразделения.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getPassportIssuanceCode()
     */
        public PropertyPath<String> passportIssuanceCode()
        {
            if(_passportIssuanceCode == null )
                _passportIssuanceCode = new PropertyPath<String>(OnlineEntrantGen.P_PASSPORT_ISSUANCE_CODE, this);
            return _passportIssuanceCode;
        }

    /**
     * @return Дата выдачи. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getPassportIssuanceDate()
     */
        public PropertyPath<Date> passportIssuanceDate()
        {
            if(_passportIssuanceDate == null )
                _passportIssuanceDate = new PropertyPath<Date>(OnlineEntrantGen.P_PASSPORT_ISSUANCE_DATE, this);
            return _passportIssuanceDate;
        }

    /**
     * @return Пол. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getSex()
     */
        public Sex.Path<Sex> sex()
        {
            if(_sex == null )
                _sex = new Sex.Path<Sex>(L_SEX, this);
            return _sex;
        }

    /**
     * @return Дата рождения. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getBirthDate()
     */
        public PropertyPath<Date> birthDate()
        {
            if(_birthDate == null )
                _birthDate = new PropertyPath<Date>(OnlineEntrantGen.P_BIRTH_DATE, this);
            return _birthDate;
        }

    /**
     * @return Место рождения. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getBirthPlace()
     */
        public PropertyPath<String> birthPlace()
        {
            if(_birthPlace == null )
                _birthPlace = new PropertyPath<String>(OnlineEntrantGen.P_BIRTH_PLACE, this);
            return _birthPlace;
        }

    /**
     * @return Адрес регистрации.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getRegistrationAddress()
     */
        public Address.Path<Address> registrationAddress()
        {
            if(_registrationAddress == null )
                _registrationAddress = new Address.Path<Address>(L_REGISTRATION_ADDRESS, this);
            return _registrationAddress;
        }

    /**
     * @return Проживаю по адресу регистрации. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#isLiveAtRegistration()
     */
        public PropertyPath<Boolean> liveAtRegistration()
        {
            if(_liveAtRegistration == null )
                _liveAtRegistration = new PropertyPath<Boolean>(OnlineEntrantGen.P_LIVE_AT_REGISTRATION, this);
            return _liveAtRegistration;
        }

    /**
     * @return Фактический адрес.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getActualAddress()
     */
        public Address.Path<Address> actualAddress()
        {
            if(_actualAddress == null )
                _actualAddress = new Address.Path<Address>(L_ACTUAL_ADDRESS, this);
            return _actualAddress;
        }

    /**
     * @return Номер свидетельства ЕГЭ.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getCertificateNumber()
     */
        public PropertyPath<String> certificateNumber()
        {
            if(_certificateNumber == null )
                _certificateNumber = new PropertyPath<String>(OnlineEntrantGen.P_CERTIFICATE_NUMBER, this);
            return _certificateNumber;
        }

    /**
     * @return Место сдачи ЕГЭ.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getCertificatePlace()
     */
        public StateExamType.Path<StateExamType> certificatePlace()
        {
            if(_certificatePlace == null )
                _certificatePlace = new StateExamType.Path<StateExamType>(L_CERTIFICATE_PLACE, this);
            return _certificatePlace;
        }

    /**
     * @return Дата выдачи сертификата ЕГЭ.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getCertificateDate()
     */
        public PropertyPath<Date> certificateDate()
        {
            if(_certificateDate == null )
                _certificateDate = new PropertyPath<Date>(OnlineEntrantGen.P_CERTIFICATE_DATE, this);
            return _certificateDate;
        }

    /**
     * @return Номер диплома.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getOlympiadDiplomaNumber()
     */
        public PropertyPath<String> olympiadDiplomaNumber()
        {
            if(_olympiadDiplomaNumber == null )
                _olympiadDiplomaNumber = new PropertyPath<String>(OnlineEntrantGen.P_OLYMPIAD_DIPLOMA_NUMBER, this);
            return _olympiadDiplomaNumber;
        }

    /**
     * @return Серия диплома.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getOlympiadDiplomaSeria()
     */
        public PropertyPath<String> olympiadDiplomaSeria()
        {
            if(_olympiadDiplomaSeria == null )
                _olympiadDiplomaSeria = new PropertyPath<String>(OnlineEntrantGen.P_OLYMPIAD_DIPLOMA_SERIA, this);
            return _olympiadDiplomaSeria;
        }

    /**
     * @return Дата выдачи диплома.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getOlympiadDiplomaIssuanceDate()
     */
        public PropertyPath<Date> olympiadDiplomaIssuanceDate()
        {
            if(_olympiadDiplomaIssuanceDate == null )
                _olympiadDiplomaIssuanceDate = new PropertyPath<Date>(OnlineEntrantGen.P_OLYMPIAD_DIPLOMA_ISSUANCE_DATE, this);
            return _olympiadDiplomaIssuanceDate;
        }

    /**
     * @return Тип олимпиады.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getOlympiadDiplomaType()
     */
        public OlympiadDiplomaType.Path<OlympiadDiplomaType> olympiadDiplomaType()
        {
            if(_olympiadDiplomaType == null )
                _olympiadDiplomaType = new OlympiadDiplomaType.Path<OlympiadDiplomaType>(L_OLYMPIAD_DIPLOMA_TYPE, this);
            return _olympiadDiplomaType;
        }

    /**
     * @return Степень отличия в дипломе.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getOlympiadDiplomaDegree()
     */
        public OlympiadDiplomaDegree.Path<OlympiadDiplomaDegree> olympiadDiplomaDegree()
        {
            if(_olympiadDiplomaDegree == null )
                _olympiadDiplomaDegree = new OlympiadDiplomaDegree.Path<OlympiadDiplomaDegree>(L_OLYMPIAD_DIPLOMA_DEGREE, this);
            return _olympiadDiplomaDegree;
        }

    /**
     * @return Название олимпиады.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getOlympiadDiplomaTitle()
     */
        public PropertyPath<String> olympiadDiplomaTitle()
        {
            if(_olympiadDiplomaTitle == null )
                _olympiadDiplomaTitle = new PropertyPath<String>(OnlineEntrantGen.P_OLYMPIAD_DIPLOMA_TITLE, this);
            return _olympiadDiplomaTitle;
        }

    /**
     * @return Предмет олимпиады.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getOlympiadDiplomaSubject()
     */
        public PropertyPath<String> olympiadDiplomaSubject()
        {
            if(_olympiadDiplomaSubject == null )
                _olympiadDiplomaSubject = new PropertyPath<String>(OnlineEntrantGen.P_OLYMPIAD_DIPLOMA_SUBJECT, this);
            return _olympiadDiplomaSubject;
        }

    /**
     * @return Страна проведения олимпиады.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getOlympiadAddressCountry()
     */
        public AddressCountry.Path<AddressCountry> olympiadAddressCountry()
        {
            if(_olympiadAddressCountry == null )
                _olympiadAddressCountry = new AddressCountry.Path<AddressCountry>(L_OLYMPIAD_ADDRESS_COUNTRY, this);
            return _olympiadAddressCountry;
        }

    /**
     * @return Населенный пункт проведения олимпиады.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getOlympiadAddressSettlement()
     */
        public AddressItem.Path<AddressItem> olympiadAddressSettlement()
        {
            if(_olympiadAddressSettlement == null )
                _olympiadAddressSettlement = new AddressItem.Path<AddressItem>(L_OLYMPIAD_ADDRESS_SETTLEMENT, this);
            return _olympiadAddressSettlement;
        }

    /**
     * @return Страна образовательного учреждения.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getEduInstitutionCountry()
     */
        public AddressCountry.Path<AddressCountry> eduInstitutionCountry()
        {
            if(_eduInstitutionCountry == null )
                _eduInstitutionCountry = new AddressCountry.Path<AddressCountry>(L_EDU_INSTITUTION_COUNTRY, this);
            return _eduInstitutionCountry;
        }

    /**
     * @return Населенный пункт образовательного учреждения.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getEduInstitutionSettlement()
     */
        public AddressItem.Path<AddressItem> eduInstitutionSettlement()
        {
            if(_eduInstitutionSettlement == null )
                _eduInstitutionSettlement = new AddressItem.Path<AddressItem>(L_EDU_INSTITUTION_SETTLEMENT, this);
            return _eduInstitutionSettlement;
        }

    /**
     * @return Образовательное учреждение.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getEduInstitution()
     */
        public EduInstitution.Path<EduInstitution> eduInstitution()
        {
            if(_eduInstitution == null )
                _eduInstitution = new EduInstitution.Path<EduInstitution>(L_EDU_INSTITUTION, this);
            return _eduInstitution;
        }

    /**
     * @return Образовательное учреждение (текст).
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getEduInstitutionStr()
     */
        public PropertyPath<String> eduInstitutionStr()
        {
            if(_eduInstitutionStr == null )
                _eduInstitutionStr = new PropertyPath<String>(OnlineEntrantGen.P_EDU_INSTITUTION_STR, this);
            return _eduInstitutionStr;
        }

    /**
     * @return Тип документа о полученном образовании. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getEduDocumentType()
     */
        public EducationDocumentType.Path<EducationDocumentType> eduDocumentType()
        {
            if(_eduDocumentType == null )
                _eduDocumentType = new EducationDocumentType.Path<EducationDocumentType>(L_EDU_DOCUMENT_TYPE, this);
            return _eduDocumentType;
        }

    /**
     * @return Уровень документа о полученном образования. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getEduDocumentLevel()
     */
        public EducationLevelStage.Path<EducationLevelStage> eduDocumentLevel()
        {
            if(_eduDocumentLevel == null )
                _eduDocumentLevel = new EducationLevelStage.Path<EducationLevelStage>(L_EDU_DOCUMENT_LEVEL, this);
            return _eduDocumentLevel;
        }

    /**
     * @return Серия документа о полученном образовании.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getEduDocumentSeria()
     */
        public PropertyPath<String> eduDocumentSeria()
        {
            if(_eduDocumentSeria == null )
                _eduDocumentSeria = new PropertyPath<String>(OnlineEntrantGen.P_EDU_DOCUMENT_SERIA, this);
            return _eduDocumentSeria;
        }

    /**
     * @return Номер документа о полученном образовании.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getEduDocumentNumber()
     */
        public PropertyPath<String> eduDocumentNumber()
        {
            if(_eduDocumentNumber == null )
                _eduDocumentNumber = new PropertyPath<String>(OnlineEntrantGen.P_EDU_DOCUMENT_NUMBER, this);
            return _eduDocumentNumber;
        }

    /**
     * @return Год окончания образовательного учреждения.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getEduDocumentYearEnd()
     */
        public PropertyPath<Integer> eduDocumentYearEnd()
        {
            if(_eduDocumentYearEnd == null )
                _eduDocumentYearEnd = new PropertyPath<Integer>(OnlineEntrantGen.P_EDU_DOCUMENT_YEAR_END, this);
            return _eduDocumentYearEnd;
        }

    /**
     * @return Дата выдачи документа о полученном образовании.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getEduDocumentDate()
     */
        public PropertyPath<Date> eduDocumentDate()
        {
            if(_eduDocumentDate == null )
                _eduDocumentDate = new PropertyPath<Date>(OnlineEntrantGen.P_EDU_DOCUMENT_DATE, this);
            return _eduDocumentDate;
        }

    /**
     * @return Степень отличия документа о полученном образовании.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getEduDocumentGraduationHonour()
     */
        public GraduationHonour.Path<GraduationHonour> eduDocumentGraduationHonour()
        {
            if(_eduDocumentGraduationHonour == null )
                _eduDocumentGraduationHonour = new GraduationHonour.Path<GraduationHonour>(L_EDU_DOCUMENT_GRADUATION_HONOUR, this);
            return _eduDocumentGraduationHonour;
        }

    /**
     * @return Количество оценок «Отлично».
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getMark5()
     */
        public PropertyPath<Integer> mark5()
        {
            if(_mark5 == null )
                _mark5 = new PropertyPath<Integer>(OnlineEntrantGen.P_MARK5, this);
            return _mark5;
        }

    /**
     * @return Количество оценок «Хорошо».
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getMark4()
     */
        public PropertyPath<Integer> mark4()
        {
            if(_mark4 == null )
                _mark4 = new PropertyPath<Integer>(OnlineEntrantGen.P_MARK4, this);
            return _mark4;
        }

    /**
     * @return Количество оценок «Удовлетворительно».
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getMark3()
     */
        public PropertyPath<Integer> mark3()
        {
            if(_mark3 == null )
                _mark3 = new PropertyPath<Integer>(OnlineEntrantGen.P_MARK3, this);
            return _mark3;
        }

    /**
     * @return Степень родства.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getNextOfKinRelationDegree()
     */
        public RelationDegree.Path<RelationDegree> nextOfKinRelationDegree()
        {
            if(_nextOfKinRelationDegree == null )
                _nextOfKinRelationDegree = new RelationDegree.Path<RelationDegree>(L_NEXT_OF_KIN_RELATION_DEGREE, this);
            return _nextOfKinRelationDegree;
        }

    /**
     * @return Фамилия ближайшего родственника.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getNextOfKinLastName()
     */
        public PropertyPath<String> nextOfKinLastName()
        {
            if(_nextOfKinLastName == null )
                _nextOfKinLastName = new PropertyPath<String>(OnlineEntrantGen.P_NEXT_OF_KIN_LAST_NAME, this);
            return _nextOfKinLastName;
        }

    /**
     * @return Имя ближайшего родственника.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getNextOfKinFirstName()
     */
        public PropertyPath<String> nextOfKinFirstName()
        {
            if(_nextOfKinFirstName == null )
                _nextOfKinFirstName = new PropertyPath<String>(OnlineEntrantGen.P_NEXT_OF_KIN_FIRST_NAME, this);
            return _nextOfKinFirstName;
        }

    /**
     * @return Отчество ближайшего родственника.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getNextOfKinMiddleName()
     */
        public PropertyPath<String> nextOfKinMiddleName()
        {
            if(_nextOfKinMiddleName == null )
                _nextOfKinMiddleName = new PropertyPath<String>(OnlineEntrantGen.P_NEXT_OF_KIN_MIDDLE_NAME, this);
            return _nextOfKinMiddleName;
        }

    /**
     * @return Место работы ближайшего родственника.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getNextOfKinWorkPlace()
     */
        public PropertyPath<String> nextOfKinWorkPlace()
        {
            if(_nextOfKinWorkPlace == null )
                _nextOfKinWorkPlace = new PropertyPath<String>(OnlineEntrantGen.P_NEXT_OF_KIN_WORK_PLACE, this);
            return _nextOfKinWorkPlace;
        }

    /**
     * @return Должность ближайшего родственника.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getNextOfKinWorkPost()
     */
        public PropertyPath<String> nextOfKinWorkPost()
        {
            if(_nextOfKinWorkPost == null )
                _nextOfKinWorkPost = new PropertyPath<String>(OnlineEntrantGen.P_NEXT_OF_KIN_WORK_POST, this);
            return _nextOfKinWorkPost;
        }

    /**
     * @return Телефон ближайшего родственника.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getNextOfKinPhone()
     */
        public PropertyPath<String> nextOfKinPhone()
        {
            if(_nextOfKinPhone == null )
                _nextOfKinPhone = new PropertyPath<String>(OnlineEntrantGen.P_NEXT_OF_KIN_PHONE, this);
            return _nextOfKinPhone;
        }

    /**
     * @return Иностранный язык.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getForeignLanguage()
     */
        public ForeignLanguage.Path<ForeignLanguage> foreignLanguage()
        {
            if(_foreignLanguage == null )
                _foreignLanguage = new ForeignLanguage.Path<ForeignLanguage>(L_FOREIGN_LANGUAGE, this);
            return _foreignLanguage;
        }

    /**
     * @return Вид спорта.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getSportType()
     */
        public SportType.Path<SportType> sportType()
        {
            if(_sportType == null )
                _sportType = new SportType.Path<SportType>(L_SPORT_TYPE, this);
            return _sportType;
        }

    /**
     * @return Спортивные разряды и звания.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getSportRank()
     */
        public SportRank.Path<SportRank> sportRank()
        {
            if(_sportRank == null )
                _sportRank = new SportRank.Path<SportRank>(L_SPORT_RANK, this);
            return _sportRank;
        }

    /**
     * @return Льгота.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getBenefit()
     */
        public Benefit.Path<Benefit> benefit()
        {
            if(_benefit == null )
                _benefit = new Benefit.Path<Benefit>(L_BENEFIT, this);
            return _benefit;
        }

    /**
     * @return Документ.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getBenefitDocument()
     */
        public PropertyPath<String> benefitDocument()
        {
            if(_benefitDocument == null )
                _benefitDocument = new PropertyPath<String>(OnlineEntrantGen.P_BENEFIT_DOCUMENT, this);
            return _benefitDocument;
        }

    /**
     * @return Дата выдачи документа льготы.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getBenefitDate()
     */
        public PropertyPath<Date> benefitDate()
        {
            if(_benefitDate == null )
                _benefitDate = new PropertyPath<Date>(OnlineEntrantGen.P_BENEFIT_DATE, this);
            return _benefitDate;
        }

    /**
     * @return Основание получения льготы.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getBenefitBasic()
     */
        public PropertyPath<String> benefitBasic()
        {
            if(_benefitBasic == null )
                _benefitBasic = new PropertyPath<String>(OnlineEntrantGen.P_BENEFIT_BASIC, this);
            return _benefitBasic;
        }

    /**
     * @return Количество детей.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getChildCount()
     */
        public PropertyPath<Integer> childCount()
        {
            if(_childCount == null )
                _childCount = new PropertyPath<Integer>(OnlineEntrantGen.P_CHILD_COUNT, this);
            return _childCount;
        }

    /**
     * @return Семейное положение.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getFamilyStatus()
     */
        public FamilyStatus.Path<FamilyStatus> familyStatus()
        {
            if(_familyStatus == null )
                _familyStatus = new FamilyStatus.Path<FamilyStatus>(L_FAMILY_STATUS, this);
            return _familyStatus;
        }

    /**
     * @return Должность.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getWorkPost()
     */
        public PropertyPath<String> workPost()
        {
            if(_workPost == null )
                _workPost = new PropertyPath<String>(OnlineEntrantGen.P_WORK_POST, this);
            return _workPost;
        }

    /**
     * @return Место работы в настоящее время.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getWorkPlace()
     */
        public PropertyPath<String> workPlace()
        {
            if(_workPlace == null )
                _workPlace = new PropertyPath<String>(OnlineEntrantGen.P_WORK_PLACE, this);
            return _workPlace;
        }

    /**
     * @return Нуждается в общежитии. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#isNeedHotel()
     */
        public PropertyPath<Boolean> needHotel()
        {
            if(_needHotel == null )
                _needHotel = new PropertyPath<Boolean>(OnlineEntrantGen.P_NEED_HOTEL, this);
            return _needHotel;
        }

    /**
     * @return Дата начала службы.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getBeginArmy()
     */
        public PropertyPath<Date> beginArmy()
        {
            if(_beginArmy == null )
                _beginArmy = new PropertyPath<Date>(OnlineEntrantGen.P_BEGIN_ARMY, this);
            return _beginArmy;
        }

    /**
     * @return Дата окончания службы.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getEndArmy()
     */
        public PropertyPath<Date> endArmy()
        {
            if(_endArmy == null )
                _endArmy = new PropertyPath<Date>(OnlineEntrantGen.P_END_ARMY, this);
            return _endArmy;
        }

    /**
     * @return Год увольнения в запас.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getEndArmyYear()
     */
        public PropertyPath<Integer> endArmyYear()
        {
            if(_endArmyYear == null )
                _endArmyYear = new PropertyPath<Integer>(OnlineEntrantGen.P_END_ARMY_YEAR, this);
            return _endArmyYear;
        }

    /**
     * @return Участвую во второй волне ЕГЭ.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getParticipateInSecondWaveUSE()
     */
        public PropertyPath<Boolean> participateInSecondWaveUSE()
        {
            if(_participateInSecondWaveUSE == null )
                _participateInSecondWaveUSE = new PropertyPath<Boolean>(OnlineEntrantGen.P_PARTICIPATE_IN_SECOND_WAVE_U_S_E, this);
            return _participateInSecondWaveUSE;
        }

    /**
     * @return Участвую во вступительных испытаниях, проводимых вузом самостоятельно.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getParticipateInEntranceTests()
     */
        public PropertyPath<Boolean> participateInEntranceTests()
        {
            if(_participateInEntranceTests == null )
                _participateInEntranceTests = new PropertyPath<Boolean>(OnlineEntrantGen.P_PARTICIPATE_IN_ENTRANCE_TESTS, this);
            return _participateInEntranceTests;
        }

    /**
     * @return Способ подачи и возврата оригиналов документов.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getMethodDeliveryNReturnDocs()
     */
        public MethodDeliveryNReturnDocs.Path<MethodDeliveryNReturnDocs> methodDeliveryNReturnDocs()
        {
            if(_methodDeliveryNReturnDocs == null )
                _methodDeliveryNReturnDocs = new MethodDeliveryNReturnDocs.Path<MethodDeliveryNReturnDocs>(L_METHOD_DELIVERY_N_RETURN_DOCS, this);
            return _methodDeliveryNReturnDocs;
        }

    /**
     * @return Копии документов.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getDocumentCopies()
     */
        public DatabaseFile.Path<DatabaseFile> documentCopies()
        {
            if(_documentCopies == null )
                _documentCopies = new DatabaseFile.Path<DatabaseFile>(L_DOCUMENT_COPIES, this);
            return _documentCopies;
        }

    /**
     * @return Название файла с документами.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getDocumentCopiesName()
     */
        public PropertyPath<String> documentCopiesName()
        {
            if(_documentCopiesName == null )
                _documentCopiesName = new PropertyPath<String>(OnlineEntrantGen.P_DOCUMENT_COPIES_NAME, this);
            return _documentCopiesName;
        }

    /**
     * @return Телефон по месту фактического проживания.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getPhoneFact()
     */
        public PropertyPath<String> phoneFact()
        {
            if(_phoneFact == null )
                _phoneFact = new PropertyPath<String>(OnlineEntrantGen.P_PHONE_FACT, this);
            return _phoneFact;
        }

    /**
     * @return Рабочий телефон.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getPhoneWork()
     */
        public PropertyPath<String> phoneWork()
        {
            if(_phoneWork == null )
                _phoneWork = new PropertyPath<String>(OnlineEntrantGen.P_PHONE_WORK, this);
            return _phoneWork;
        }

    /**
     * @return Мобильный телефон.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getPhoneMobile()
     */
        public PropertyPath<String> phoneMobile()
        {
            if(_phoneMobile == null )
                _phoneMobile = new PropertyPath<String>(OnlineEntrantGen.P_PHONE_MOBILE, this);
            return _phoneMobile;
        }

    /**
     * @return Телефон родственников.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getPhoneRelatives()
     */
        public PropertyPath<String> phoneRelatives()
        {
            if(_phoneRelatives == null )
                _phoneRelatives = new PropertyPath<String>(OnlineEntrantGen.P_PHONE_RELATIVES, this);
            return _phoneRelatives;
        }

    /**
     * @return Email.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant#getEmail()
     */
        public PropertyPath<String> email()
        {
            if(_email == null )
                _email = new PropertyPath<String>(OnlineEntrantGen.P_EMAIL, this);
            return _email;
        }

        public Class getEntityClass()
        {
            return OnlineEntrant.class;
        }

        public String getEntityName()
        {
            return "onlineEntrant";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
