/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.settings.Discipline2RealizationWay;

import java.util.List;
import java.util.Map;

import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.tapsupport.component.matrix.CellCoordinates;

import ru.tandemservice.uniec.entity.catalog.EducationSubject;
import ru.tandemservice.uniec.entity.catalog.SubjectPassWay;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

/**
 * @author agolubenko
 * @since 07.06.2008
 */
public class DAO extends ru.tandemservice.uniec.component.settings.AbstractMatrixSettingsComponent.DAO<Model, EducationSubject, SubjectPassWay, Boolean, Discipline2RealizationWayRelation> implements IDAO
{
    @Override
    public void update(Model model)
    {
        Map<CellCoordinates, Discipline2RealizationWayRelation> ids = fillMatrixData(model);

        for (CellCoordinates cellCoordinates : model.getMatrixData().keySet())
        {
            if (model.getMatrixData().get(cellCoordinates))
            {
                if (!ids.containsKey(cellCoordinates))
                {
                    Discipline2RealizationWayRelation relation = new Discipline2RealizationWayRelation();
                    relation.setEducationSubject(get(EducationSubject.class, cellCoordinates.getRowId()));
                    relation.setSubjectPassWay(get(SubjectPassWay.class, cellCoordinates.getColumnId()));
                    relation.setEnrollmentCampaign(model.getEnrollmentCampaign());
                    getSession().save(relation);
                }
            }
            else
            {
                if (ids.containsKey(cellCoordinates))
                {
                    getSession().delete(ids.get(cellCoordinates));
                }
            }
        }
    }

    @Override
    protected List<EducationSubject> getVerticalObjects(Model model)
    {
        return CatalogManager.instance().dao().getCatalogItemList(EducationSubject.class);
    }

    @Override
    protected List<SubjectPassWay> getHorizontalObjects(Model model)
    {
        return CatalogManager.instance().dao().getCatalogItemList(SubjectPassWay.class);
    }

    @Override
    protected Long getLeftId(Discipline2RealizationWayRelation object)
    {
        return object.getEducationSubject().getId();
    }

    @Override
    protected Long getRightId(Discipline2RealizationWayRelation object)
    {
        return object.getSubjectPassWay().getId();
    }

    @Override
    protected List<Discipline2RealizationWayRelation> getObjects(Model model)
    {
        return getList(Discipline2RealizationWayRelation.class, Discipline2RealizationWayRelation.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign());
    }

    @Override
    protected Boolean getValue(Discipline2RealizationWayRelation object)
    {
        return true;
    }
}
