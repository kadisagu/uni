/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.util;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;

import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommended;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.settings.EnrollmentCompetitionKind;

/**
 * @author Vasily Zhukov
 * @since 18.07.2011
 */
public class EcgRecommendedDTO extends IdentifiableWrapper<EcgEntrantRecommended> implements IEcgRecommendedDTO
{
    private static final long serialVersionUID = 1L;
    private IEcgEntrantRecommended _entrantRecommended;
    private String _fio;
    private CompetitionKind _competitionKind;
    private TargetAdmissionKind _targetAdmissionKind;
    private Double _finalMark;
    private String _priorities;
    private DocumentStatus _documentStatus;

    /**
     * @param entrantRecommended рекомендованный
     * @param finalMark          сумма баллов
     * @param priorities         приоритеты
     * @param documentStatus     состояние документов
     */
    public EcgRecommendedDTO(IEcgEntrantRecommended entrantRecommended, Double finalMark, String priorities, DocumentStatus documentStatus)
    {
        super(entrantRecommended.getId(), "");

        _entrantRecommended = entrantRecommended;

        _fio = _entrantRecommended.getDirection().getEntrantRequest().getEntrant().getPerson().getIdentityCard().getFullFio();

        _competitionKind = entrantRecommended.getDirection().getCompetitionKind();

        _targetAdmissionKind = entrantRecommended.getTargetAdmissionKind();

        _finalMark = finalMark;

        _priorities = priorities;

        _documentStatus = documentStatus;
    }

    // Getters

    @Override
    public IEcgEntrantRecommended getEntrantRecommended()
    {
        return _entrantRecommended;
    }

    @Override
    public String getCompetitionKindTitle()
    {
        return _entrantRecommended.getTargetAdmissionKind() != null ? (EnrollmentCompetitionKind.TARGET_ADMISSION + (UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION.equals(_competitionKind.getCode()) ? "" : ", " + _competitionKind.getTitle())) : _competitionKind.getTitle();
    }

    @Override
    public String getTargetAdmissionKindTitle()
    {
        return _targetAdmissionKind != null ? _targetAdmissionKind.getFullHierarhyTitle() : null;
    }

    @Override
    public String getGraduatedProfileEduInstitutionTitle()
    {
        return _entrantRecommended.getDirection().isGraduatedProfileEduInstitution() ? "Да" : null;
    }

    @Override
    public String getPriorities()
    {
        return _priorities;
    }

    @Override
    public DocumentStatus getDocumentStatus()
    {
        return _documentStatus;
    }

    @Override
    public String getOriginalDocumentHandedInTitle()
    {
        if (_documentStatus == null)
            return null;
        else
            switch (_documentStatus)
            {
                case BRING_ORIGINAL:
                    return "оригиналы";
                case BRING_COPY:
                    return "копии";
                case TOOK_AWAY:
                    return "забрал документы";
                default:
                    throw new RuntimeException("Unknown documentStatus: " + _documentStatus);
            }
    }

    @Override
    public IEntity getEntity()
    {
        return null;
    }

    @Override
    public boolean isTargetAdmission()
    {
        return _entrantRecommended.getTargetAdmissionKind() != null;
    }

    @Override
    public TargetAdmissionKind getTargetAdmissionKind()
    {
        return _entrantRecommended.getTargetAdmissionKind();
    }

    @Override
    public CompetitionKind getCompetitionKind()
    {
        return _entrantRecommended.getDirection().getCompetitionKind();
    }

    @Override
    public Double getFinalMark()
    {
        return _finalMark;
    }

    @Override
    public Double getProfileMark()
    {
        if (_finalMark == null) return null;

        return _entrantRecommended.getDirection().getProfileChosenEntranceDiscipline() == null ? null : _entrantRecommended.getDirection().getProfileChosenEntranceDiscipline().getFinalMark();
    }

    @Override
    public Boolean isGraduatedProfileEduInstitution()
    {
        return _entrantRecommended.getDirection().isGraduatedProfileEduInstitution();
    }

    @Override
    public Double getCertificateAverageMark()
    {
        PersonEduInstitution personEduInstitution = _entrantRecommended.getDirection().getEntrantRequest().getEntrant().getPerson().getPersonEduInstitution();
        return personEduInstitution == null ? null : personEduInstitution.getAverageMark();
    }

    @Override
    public String getFio()
    {
        return _fio;
    }
}
