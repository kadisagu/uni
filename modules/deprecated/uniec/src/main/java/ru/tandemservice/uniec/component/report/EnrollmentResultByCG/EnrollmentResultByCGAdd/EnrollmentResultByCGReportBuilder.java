/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EnrollmentResultByCG.EnrollmentResultByCGAdd;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.MergeType;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;

import java.util.*;

/**
 * @author ekachanova
 */
public class EnrollmentResultByCGReportBuilder
{
    private Session _session;
    private Model _model;

    EnrollmentResultByCGReportBuilder(Session session, Model model)
    {
        _session = session;
        _model = model;
    }

    DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    @SuppressWarnings("unchecked")
    private byte[] buildReport()
    {
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ENROLLMENT_RESULT_BY_CG);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());
        String academyTitle = TopOrgUnit.getInstance().getShortTitle();

        Map<CompetitionGroup, List<EnrollmentDirection>> competitionGroup2EnrollmentDirections = getCompetitionGroup2EnrollmentDirections();
        Object[] enteredDocumentsData = getEnteredDocumentsData();
        Map<CompetitionGroup, Set<Long>> competitionGroup2BudgetEnteredRequests = (Map<CompetitionGroup, Set<Long>>) enteredDocumentsData[0];
        Map<CompetitionGroup, Set<Long>> competitionGroup2ContractEnteredRequests = (Map<CompetitionGroup, Set<Long>>) enteredDocumentsData[1];

        Map<CompetitionGroup, Map<Long, ReportRow>> competitionGroup2NumReportRows = new HashMap<>();
        Map<CompetitionGroup, Map<Long, ReportRow>> competitionGroup2ParReportRows = new HashMap<>();
        Map<CompetitionGroup, ReportRow> competitionGroup2TotalReportRow = new HashMap<>();
        Map<CompetitionGroup, ReportRow> competitionGroup2TotalParReportRow = new HashMap<>();
        Map<Long, CompetitionGroup> eduOrgUnitId2CompetitionGroup = new HashMap<>();

        Map<String, ReportRow> developForm2TotalReportRow = new TreeMap<>();
        Map<String, ReportRow> developForm2TotalParReportRow = new TreeMap<>();

        ReportRow allRow = new ReportRow("ВСЕГО " + academyTitle + ":");
        ReportRow allParRow = new ReportRow("Всего пар:", true);

        Map<String, String> developFormCode2Title = getDevelopFormCode2Title();

        for (Map.Entry<CompetitionGroup, List<EnrollmentDirection>> entry : competitionGroup2EnrollmentDirections.entrySet())
        {
            Map<Long, ReportRow> numReportRows = new HashMap<>();
            ReportRow totalCgRow = new ReportRow("Итого:");
            DevelopForm developForm = null;
            for (EnrollmentDirection dir : entry.getValue())
            {
                StringBuilder developFormFullTitle = new StringBuilder(dir.getEducationOrgUnit().getEducationLevelHighSchool().getShortTitle()).append(" ")
                        .append(StringUtils.uncapitalize(dir.getEducationOrgUnit().getDevelopForm().getTitle()));
                if (dir.getEducationOrgUnit().getDevelopCondition().getCode().equals(UniDefines.DEVELOP_CONDITION_SHORT))
                {
                    developFormFullTitle.append(" сокр");
                }
                ReportRow reportRow = new ReportRow(dir.getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitle(), developFormFullTitle.toString(), dir.getEducationOrgUnit().getDevelopForm().getCode(), dir.getMinisterialPlan(), dir.getContractPlan());

                reportRow.setBudgetEntered(competitionGroup2BudgetEnteredRequests.containsKey(entry.getKey()) ? competitionGroup2BudgetEnteredRequests.get(entry.getKey()).size() : 0);
                reportRow.setContractEntered(competitionGroup2ContractEnteredRequests.containsKey(entry.getKey()) ? competitionGroup2ContractEnteredRequests.get(entry.getKey()).size() : 0);

                numReportRows.put(dir.getEducationOrgUnit().getEducationLevelHighSchool().getId(), reportRow);

                if (totalCgRow.getBudgetEntered() == 0)
                    totalCgRow.setBudgetEntered(reportRow.getBudgetEntered());
                if (totalCgRow.getContractEntered() == 0)
                    totalCgRow.setContractEntered(reportRow.getContractEntered());
                if (dir.getMinisterialPlan() != null)
                    totalCgRow.setBudgetPlan((totalCgRow.getBudgetPlan() == null ? 0 : totalCgRow.getBudgetPlan()) + dir.getMinisterialPlan());
                if (dir.getContractPlan() != null)
                    totalCgRow.setContractPlan((totalCgRow.getContractPlan() == null ? 0 : totalCgRow.getContractPlan()) + dir.getContractPlan());
                if (developForm == null)
                    developForm = dir.getEducationOrgUnit().getDevelopForm();

                eduOrgUnitId2CompetitionGroup.put(dir.getEducationOrgUnit().getId(), entry.getKey());
            }

            if (totalCgRow.getDevelopFormTitle() == null && developForm != null)
            {
                totalCgRow.setDevelopFormTitle(StringUtils.uncapitalize(developForm.getTitle()));
                totalCgRow.setDevelopFormCode(developForm.getCode());
            }
            competitionGroup2NumReportRows.put(entry.getKey(), numReportRows);
            competitionGroup2TotalReportRow.put(entry.getKey(), totalCgRow);

            if (developForm != null)
            {
                ReportRow totalRow = developForm2TotalReportRow.get(developForm.getCode());
                if (totalRow == null)
                    developForm2TotalReportRow.put(developForm.getCode(), totalRow = new ReportRow("ИТОГО ПО " + academyTitle + ":", StringUtils.uncapitalize(developForm.getTitle()), developForm.getCode()));

                if (totalCgRow.getBudgetPlan() != null)
                {
                    totalRow.setBudgetPlan((totalRow.getBudgetPlan() == null ? 0 : totalRow.getBudgetPlan()) + totalCgRow.getBudgetPlan());
                    allRow.setBudgetPlan((allRow.getBudgetPlan() == null ? 0 : allRow.getBudgetPlan()) + totalCgRow.getBudgetPlan());
                }
                if (totalCgRow.getContractPlan() != null)
                {
                    totalRow.setContractPlan((totalRow.getContractPlan() == null ? 0 : totalRow.getContractPlan()) + totalCgRow.getContractPlan());
                    allRow.setContractPlan((allRow.getContractPlan() == null ? 0 : allRow.getContractPlan()) + totalCgRow.getContractPlan());
                }

                totalRow.setBudgetEntered(totalRow.getBudgetEntered() + totalCgRow.getBudgetEntered());
                totalRow.setContractEntered(totalRow.getContractEntered() + totalCgRow.getContractEntered());

                allRow.setBudgetEntered(allRow.getBudgetEntered() + totalCgRow.getBudgetEntered());
                allRow.setContractEntered(allRow.getContractEntered() + totalCgRow.getContractEntered());
            }
        }

        List<Object[]> preliminaryEnrollmentStudentsData = getPreliminaryEnrollmentStudents();
        for (Object[] obj : preliminaryEnrollmentStudentsData)
        {
            // PreliminaryEnrollmentStudent id, compensationTypeCode, educationOrgUnitId,  educationLevelHighSchoolId, studentCategoryCode, parallel
            String compensationTypeCode = (String) obj[1];
            Long eduOrgUnitId = (Long) obj[2];
            Long educationLevelHighSchoolId = (Long) obj[3];
            String studentCategoryCode = (String) obj[4];
            boolean parallel = (Boolean) obj[5];

            CompetitionGroup competitionGroup = eduOrgUnitId2CompetitionGroup.get(eduOrgUnitId);
            if (competitionGroup == null) continue;

            if (parallel)
            {
                ReportRow numRow = competitionGroup2NumReportRows.get(competitionGroup).get(educationLevelHighSchoolId);

                Map<Long, ReportRow> parRows = competitionGroup2ParReportRows.get(competitionGroup);
                if (parRows == null)
                    competitionGroup2ParReportRows.put(competitionGroup, parRows = new HashMap<>());

                ReportRow parRow = parRows.get(educationLevelHighSchoolId);
                if (parRow == null)
                    parRows.put(educationLevelHighSchoolId, parRow = new ReportRow(numRow.getTitle(), numRow.getDevelopFormTitle() + " пар", true));
                parRow.setContractEnrolled(parRow.getContractEnrolled() + 1);

                ReportRow totalCgParRow = competitionGroup2TotalParReportRow.get(competitionGroup);
                if (totalCgParRow == null)
                    competitionGroup2TotalParReportRow.put(competitionGroup, totalCgParRow = new ReportRow("Итого:", developFormCode2Title.get(numRow.getDevelopFormCode()) + " пар", true));
                totalCgParRow.setContractEnrolled(totalCgParRow.getContractEnrolled() + 1);

                ReportRow totalParRow = developForm2TotalParReportRow.get(numRow.getDevelopFormCode());
                if (totalParRow == null)
                    developForm2TotalParReportRow.put(numRow.getDevelopFormCode(), totalParRow = new ReportRow("ИТОГО ПО " + academyTitle + ":", developFormCode2Title.get(numRow.getDevelopFormCode()) + " пар", true));
                totalParRow.setContractEnrolled(totalParRow.getContractEnrolled() + 1);

                allParRow.setContractEnrolled(allParRow.getContractEnrolled() + 1);
            } else
            {
                ReportRow reportRow = competitionGroup2NumReportRows.get(competitionGroup).get(educationLevelHighSchoolId);

                ReportRow totalCgRow = competitionGroup2TotalReportRow.get(competitionGroup);
                ReportRow totalRow = developForm2TotalReportRow.get(totalCgRow.getDevelopFormCode());
                if (compensationTypeCode.equals(UniDefines.COMPENSATION_TYPE_BUDGET))
                {
                    reportRow.setBudgetEnrolled(reportRow.getBudgetEnrolled() + 1);
                    totalCgRow.setBudgetEnrolled(totalCgRow.getBudgetEnrolled() + 1);
                    totalRow.setBudgetEnrolled(totalRow.getBudgetEnrolled() + 1);
                    allRow.setBudgetEnrolled(allRow.getBudgetEnrolled() + 1);
                } else
                {
                    reportRow.setContractEnrolled(reportRow.getContractEnrolled() + 1);
                    totalCgRow.setContractEnrolled(totalCgRow.getContractEnrolled() + 1);
                    totalRow.setContractEnrolled(totalRow.getContractEnrolled() + 1);
                    allRow.setContractEnrolled(allRow.getContractEnrolled() + 1);
                    if (studentCategoryCode.equals(UniDefines.STUDENT_CATEGORY_LISTENER))
                    {
                        reportRow.setContractEnrolledListener(reportRow.getContractEnrolledListener() + 1);
                        totalCgRow.setContractEnrolledListener(totalCgRow.getContractEnrolledListener() + 1);
                        totalRow.setContractEnrolledListener(totalRow.getContractEnrolledListener() + 1);
                        allRow.setContractEnrolledListener(allRow.getContractEnrolledListener() + 1);
                    }
                }
            }
        }

        List<CompetitionGroup> competitionGroups = new ArrayList<>(competitionGroup2EnrollmentDirections.keySet());
        Collections.sort(competitionGroups, CompetitionGroup.TITLED_COMPARATOR);

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("highSchoolTitle", academyTitle);
        injectModifier.put("year", _model.getReport().getEnrollmentCampaign().getEducationYear().getTitle().split("/")[0]);
        injectModifier.modify(document);

        final IRtfElement par = RtfBean.getElementFactory().createRtfControl(IRtfData.PAR);
        final IRtfElement pard = RtfBean.getElementFactory().createRtfControl(IRtfData.PARD);

        RtfTable cgTableTemplate = (RtfTable) UniRtfUtil.findElement(document.getElementList(), "groupTitle");
        document.getElementList().remove(cgTableTemplate);

        RtfTable totalTableTemplate = (RtfTable) UniRtfUtil.findElement(document.getElementList(), "T4");
        document.getElementList().remove(totalTableTemplate);

        RtfTable allTableTemplate = (RtfTable) UniRtfUtil.findElement(document.getElementList(), "T5");
        document.getElementList().remove(allTableTemplate);

        document.getElementList().remove(document.getElementList().size() - 1);
        document.getElementList().remove(document.getElementList().size() - 2);

        for (CompetitionGroup cg : competitionGroups)
        {
            RtfTable cgTable = cgTableTemplate.getClone();
            UniRtfUtil.fillCell(cgTable.getRowList().get(0).getCellList().get(0), _model.getCompetitionGroupName() + " №" + cg.getTitle());

            List<ReportRow> numReportRows = new ArrayList<>(competitionGroup2NumReportRows.get(cg).values());

            List<ReportRow> parReportRows = new ArrayList<>();
            if (competitionGroup2ParReportRows.containsKey(cg))
            {
                parReportRows.addAll(competitionGroup2ParReportRows.get(cg).values());
            }

            String[][] data = new String[numReportRows.size() + parReportRows.size()][11];

            Collections.sort(numReportRows);
            for (int i = 0; i < numReportRows.size(); i++)
            {
                ReportRow row = numReportRows.get(i);
                data[i] = new String[]{String.valueOf(i + 1), row.getTitle(), row.getDevelopFormTitle(),
                        UniBaseUtils.toString(row.getBudgetPlan()), UniBaseUtils.toString(row.getBudgetEntered()), row.getBudgetCompetition(competitionGroup2TotalReportRow.get(cg).getBudgetPlan()), UniBaseUtils.toString(row.getBudgetEnrolled()),
                        UniBaseUtils.toString(row.getContractPlan()), UniBaseUtils.toString(row.getContractEntered()), row.getContractCompetition(competitionGroup2TotalReportRow.get(cg).getContractPlan()), row.getContractEnrolledStr()
                };
            }

            if (parReportRows.size() > 0)
            {
                Collections.sort(parReportRows);
                for (int i = 0; i < parReportRows.size(); i++)
                {
                    ReportRow row = parReportRows.get(i);
                    data[numReportRows.size() + i] = new String[]{"", row.getTitle(), row.getDevelopFormTitle(), "", "", "", "",
                            UniBaseUtils.toString(row.getContractEnrolled()), UniBaseUtils.toString(row.getContractEnrolled()), "1,0", row.getContractEnrolledStr()};
                }
            }

            RtfDocument cgTableDoc = RtfBean.getElementFactory().createRtfDocument();
            cgTableDoc.getElementList().add(cgTable);

            RtfTableModifier tableModifier = new RtfTableModifier();
            tableModifier.put("T1", data);
            tableModifier.modify(cgTableDoc);

            List<RtfRow> cgTableRows = ((RtfTable) cgTableDoc.getElementList().get(0)).getRowList();
            if (numReportRows.size() > 1)
            {
                cgTableRows.get(3).getCellList().get(4).setMergeType(MergeType.VERTICAL_MERGED_FIRST);
                cgTableRows.get(3).getCellList().get(5).setMergeType(MergeType.VERTICAL_MERGED_FIRST);
                cgTableRows.get(3).getCellList().get(8).setMergeType(MergeType.VERTICAL_MERGED_FIRST);
                cgTableRows.get(3).getCellList().get(9).setMergeType(MergeType.VERTICAL_MERGED_FIRST);
                for (int j = 0; j < numReportRows.size() - 1; j++)
                {
                    cgTableRows.get(j + 4).getCellList().get(4).setMergeType(MergeType.VERTICAL_MERGED_NEXT);
                    cgTableRows.get(j + 4).getCellList().get(5).setMergeType(MergeType.VERTICAL_MERGED_NEXT);
                    cgTableRows.get(j + 4).getCellList().get(8).setMergeType(MergeType.VERTICAL_MERGED_NEXT);
                    cgTableRows.get(j + 4).getCellList().get(9).setMergeType(MergeType.VERTICAL_MERGED_NEXT);
                }
            }

            ReportRow totalCgRow = competitionGroup2TotalReportRow.get(cg);
            ReportRow totalCgParRow = competitionGroup2TotalParReportRow.get(cg);
            String[][] totalCgData = new String[totalCgParRow == null ? 1 : 2][10];
            totalCgData[0] = new String[]{totalCgRow.getTitle(), totalCgRow.getDevelopFormTitle(),
                    UniBaseUtils.toString(totalCgRow.getBudgetPlan()), UniBaseUtils.toString(totalCgRow.getBudgetEntered()), totalCgRow.getBudgetCompetition(totalCgRow.getBudgetPlan()), UniBaseUtils.toString(totalCgRow.getBudgetEnrolled()),
                    UniBaseUtils.toString(totalCgRow.getContractPlan()), UniBaseUtils.toString(totalCgRow.getContractEntered()), totalCgRow.getContractCompetition(totalCgRow.getContractPlan()), totalCgRow.getContractEnrolledStr()
            };
            if (totalCgParRow != null)
            {
                totalCgData[1] = new String[]{totalCgParRow.getTitle(), totalCgParRow.getDevelopFormTitle(), "", "", "", "",
                        UniBaseUtils.toString(totalCgParRow.getContractEnrolled()), UniBaseUtils.toString(totalCgParRow.getContractEnrolled()), "1,0", totalCgParRow.getContractEnrolledStr()};
            }
            tableModifier = new RtfTableModifier();
            tableModifier.put("T3", totalCgData);
            tableModifier.modify(cgTableDoc);

            cgTableRows = ((RtfTable) cgTableDoc.getElementList().get(0)).getRowList();
            if (totalCgData.length > 1)
            {
                cgTableRows.get(3 + numReportRows.size() + parReportRows.size()).getCellList().get(0).setMergeType(MergeType.VERTICAL_MERGED_FIRST);
                cgTableRows.get(4 + numReportRows.size() + parReportRows.size()).getCellList().get(0).setMergeType(MergeType.VERTICAL_MERGED_NEXT);
            }
            document.getElementList().add(cgTableDoc);
            document.getElementList().add(pard);
            document.getElementList().add(par);
        }

        //ИТОГО
        List<ReportRow> totalParRows = new ArrayList<>();
        if (developForm2TotalParReportRow.size() > 0)
        {
            totalParRows.addAll(developForm2TotalParReportRow.values());
        }
        String[][] totalData = new String[developForm2TotalReportRow.size() + totalParRows.size()][10];
        int i = 0;
        for (Map.Entry<String, ReportRow> entry : developForm2TotalReportRow.entrySet())
        {
            ReportRow row = entry.getValue();
            totalData[i++] = new String[]{row.getTitle(), row.getDevelopFormTitle(),
                    UniBaseUtils.toString(row.getBudgetPlan()), UniBaseUtils.toString(row.getBudgetEntered()), row.getBudgetCompetition(row.getBudgetPlan()), UniBaseUtils.toString(row.getBudgetEnrolled()),
                    UniBaseUtils.toString(row.getContractPlan()), UniBaseUtils.toString(row.getContractEntered()), row.getContractCompetition(row.getContractPlan()), row.getContractEnrolledStr()};
        }
        for (Map.Entry<String, ReportRow> entry : developForm2TotalParReportRow.entrySet())
        {
            ReportRow row = entry.getValue();
            totalData[i++] = new String[]{row.getTitle(), row.getDevelopFormTitle(), "", "", "", "",
                    UniBaseUtils.toString(row.getContractEnrolled()), UniBaseUtils.toString(row.getContractEnrolled()), "1,0", row.getContractEnrolledStr()};
        }
        RtfDocument totalTableDoc = RtfBean.getElementFactory().createRtfDocument();
        totalTableDoc.getElementList().add(totalTableTemplate);

        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T4", totalData);
        tableModifier.modify(totalTableDoc);

        List<RtfRow> totalTableRows = ((RtfTable) totalTableDoc.getElementList().get(0)).getRowList();
        if (totalTableRows.size() > 1)
        {
            totalTableRows.get(0).getCellList().get(0).setMergeType(MergeType.VERTICAL_MERGED_FIRST);
            for (int j = 1; j < totalTableRows.size(); j++)
            {
                totalTableRows.get(j).getCellList().get(0).setMergeType(MergeType.VERTICAL_MERGED_NEXT);
            }
        }

        document.getElementList().add(totalTableDoc);
        document.getElementList().add(pard);
        document.getElementList().add(par);

        //ВСЕГО
        String[][] allData = new String[allParRow.getContractEnrolled() > 0 ? 2 : 1][9];
        allData[0] = new String[]{allRow.getTitle(),
                UniBaseUtils.toString(allRow.getBudgetPlan()), UniBaseUtils.toString(allRow.getBudgetEntered()), allRow.getBudgetCompetition(allRow.getBudgetPlan()), UniBaseUtils.toString(allRow.getBudgetEnrolled()),
                UniBaseUtils.toString(allRow.getContractPlan()), UniBaseUtils.toString(allRow.getContractEntered()), allRow.getContractCompetition(allRow.getContractPlan()), allRow.getContractEnrolledStr()};
        if (allParRow.getContractEnrolled() > 0)
        {
            allData[1] = new String[]{allParRow.getTitle(), "", "", "", "",
                    UniBaseUtils.toString(allParRow.getContractEnrolled()), UniBaseUtils.toString(allParRow.getContractEnrolled()), "1,0", allParRow.getContractEnrolledStr()};
        }
        RtfDocument allTableDoc = RtfBean.getElementFactory().createRtfDocument();
        allTableDoc.getElementList().add(allTableTemplate);

        tableModifier = new RtfTableModifier();
        tableModifier.put("T5", allData);
        tableModifier.modify(allTableDoc);

        document.getElementList().add(allTableDoc);
        document.getElementList().add(pard);
        document.getElementList().add(par);

        return RtfUtil.toByteArray(document);
    }

    public Map<CompetitionGroup, List<EnrollmentDirection>> getCompetitionGroup2EnrollmentDirections()
    {
        Map<CompetitionGroup, List<EnrollmentDirection>> competitionGroup2EnrollmentDirections = new HashMap<>();
        MQBuilder builder = new MQBuilder(EnrollmentDirection.ENTITY_CLASS, "d");
        builder.add(MQExpression.eq("d", EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, _model.getReport().getEnrollmentCampaign()));
        builder.add(MQExpression.isNotNull("d", EnrollmentDirection.L_COMPETITION_GROUP));
        for (EnrollmentDirection dir : builder.<EnrollmentDirection>getResultList(_session))
        {
            List<EnrollmentDirection> enrollmentDirections = competitionGroup2EnrollmentDirections.get(dir.getCompetitionGroup());
            if (enrollmentDirections == null)
                competitionGroup2EnrollmentDirections.put(dir.getCompetitionGroup(), enrollmentDirections = new ArrayList<>());
            enrollmentDirections.add(dir);
        }
        return competitionGroup2EnrollmentDirections;
    }

    public Object[] getEnteredDocumentsData()
    {
        Map<CompetitionGroup, Set<Long>> competitionGroup2BudgetEnteredRequests = new HashMap<>();
        Map<CompetitionGroup, Set<Long>> competitionGroup2ContractEnteredRequests = new HashMap<>();

        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "rd", new String[]{
                RequestedEnrollmentDirection.L_ENTRANT_REQUEST + ".id",
                RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_COMPETITION_GROUP,
                RequestedEnrollmentDirection.L_COMPENSATION_TYPE + "." + CompensationType.P_CODE});
        builder.add(MQExpression.eq("rd", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, _model.getReport().getEnrollmentCampaign()));
        builder.add(UniMQExpression.betweenDate("rd", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));
        builder.add(MQExpression.isNotNull("rd", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_COMPETITION_GROUP));
        builder.add(MQExpression.eq("rd", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, false));
        builder.add(MQExpression.notEq("rd", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        for (Object[] obj : builder.<Object[]>getResultList(_session))
        {
            Long requestId = (Long) obj[0];
            CompetitionGroup cg = (CompetitionGroup) obj[1];
            String compenstypeCode = (String) obj[2];
            if (compenstypeCode.equals(UniDefines.COMPENSATION_TYPE_BUDGET))
            {
                Set<Long> budgetEnteredRequests = competitionGroup2BudgetEnteredRequests.get(cg);
                if (budgetEnteredRequests == null)
                    competitionGroup2BudgetEnteredRequests.put(cg, budgetEnteredRequests = new HashSet<>());
                budgetEnteredRequests.add(requestId);
            } else
            {
                Set<Long> contractEnteredRequests = competitionGroup2ContractEnteredRequests.get(cg);
                if (contractEnteredRequests == null)
                    competitionGroup2ContractEnteredRequests.put(cg, contractEnteredRequests = new HashSet<>());
                contractEnteredRequests.add(requestId);
            }
        }
        return new Object[]{competitionGroup2BudgetEnteredRequests, competitionGroup2ContractEnteredRequests};
    }

    // у PreliminaryEnrollmentStudent берем : id, compensationTypeCode, educationOrgUnitId,  educationLevelHighSchoolId, studentCategoryCode, parallel
    public List<Object[]> getPreliminaryEnrollmentStudents()
    {
        MQBuilder builder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "p", new String[]{
                "id",
                PreliminaryEnrollmentStudent.L_COMPENSATION_TYPE + "." + CompensationType.P_CODE,
                PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT + ".id",
                PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL + ".id",
                PreliminaryEnrollmentStudent.L_STUDENT_CATEGORY + "." + StudentCategory.P_CODE,
                PreliminaryEnrollmentStudent.P_PARALLEL});
        builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION + "." + RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, _model.getReport().getEnrollmentCampaign()));
        builder.add(MQExpression.isNotNull("p", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION + "." + RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_COMPETITION_GROUP));
        builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION + "." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, false));
        builder.add(MQExpression.notEq("p", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION + "." + RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        return builder.getResultList(_session);
    }

    @SuppressWarnings({"unchecked", "unused"})
    private static class ReportRow implements Comparable
    {
        private String _title;
        private String _developFormTitle;
        private String _developFormCode;
        private Integer _budgetPlan;         //план
        private Integer _budgetEntered = 0;      //подано
        private Integer _budgetEnrolled = 0;     //зачислено
        private Integer _contractPlan;         //план
        private Integer _contractEntered = 0;      //подано
        private Integer _contractEnrolled = 0;     //зачислено
        private Integer _contractEnrolledListener = 0;     //зачислено слуш

        private boolean _parallel;

        private ReportRow(String title, String developFormTitle, String developFormCode, Integer budgetPlan, Integer contractPlan)
        {
            _title = title;
            _developFormTitle = developFormTitle;
            _developFormCode = developFormCode;
            _budgetPlan = budgetPlan;
            _contractPlan = contractPlan;
        }

        private ReportRow(String title, String developFormTitle, String developFormCode)
        {
            _title = title;
            _developFormTitle = developFormTitle;
            _developFormCode = developFormCode;
        }

        private ReportRow(String title, String developFormTitle, boolean parallel)
        {
            _title = title;
            _developFormTitle = developFormTitle;
            _parallel = parallel;
        }

        private ReportRow(String title, boolean parallel)
        {
            _title = title;
            _parallel = parallel;
        }

        private ReportRow(String title)
        {
            _title = title;
        }

        public Integer getBudgetEnrolled()
        {
            if (isParallel())
                return null;
            return _budgetEnrolled;
        }

        public Integer getBudgetEntered()
        {
            if (isParallel())
                return null;
            return _budgetEntered;
        }

        public Integer getBudgetPlan()
        {
            if (isParallel())
                return null;
            return _budgetPlan;
        }

        public Integer getContractEnrolled()
        {
            return _contractEnrolled;
        }

        public Integer getContractEntered()
        {
            return _contractEntered;
        }

        public Integer getContractPlan()
        {
            return _contractPlan;
        }

        public String getDevelopFormTitle()
        {
            return _developFormTitle;
        }

        public String getTitle()
        {
            return _title;
        }

        public void setBudgetEnrolled(Integer budgetEnrolled)
        {
            _budgetEnrolled = budgetEnrolled;
        }

        public void setBudgetEntered(Integer budgetEntered)
        {
            _budgetEntered = budgetEntered;
        }

        public void setBudgetPlan(Integer budgetPlan)
        {
            _budgetPlan = budgetPlan;
        }

        public void setContractEnrolled(Integer contractEnrolled)
        {
            _contractEnrolled = contractEnrolled;
        }

        public void setContractEntered(Integer contractEntered)
        {
            _contractEntered = contractEntered;
        }

        public void setContractPlan(Integer contractPlan)
        {
            _contractPlan = contractPlan;
        }

        public boolean isParallel()
        {
            return _parallel;
        }

        public Integer getContractEnrolledListener()
        {
            return _contractEnrolledListener;
        }

        public void setContractEnrolledListener(Integer contractEnrolledListener)
        {
            _contractEnrolledListener = contractEnrolledListener;
        }

        public void setDevelopFormTitle(String developFormTitle)
        {
            _developFormTitle = developFormTitle;
        }

        public String getDevelopFormCode()
        {
            return _developFormCode;
        }

        public void setDevelopFormCode(String developFormCode)
        {
            _developFormCode = developFormCode;
        }

        public void setParallel(boolean parallel)
        {
            _parallel = parallel;
        }

        public String getBudgetCompetition(Integer plan)
        {
            return plan == null || plan == 0 ? "" : DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS.format(getBudgetEntered() / plan.doubleValue());
        }

        public String getContractCompetition(Integer plan)
        {
            return plan == null || plan == 0 ? "" : DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS.format(getContractEntered() / plan.doubleValue());
        }

        public String getContractEnrolledStr()
        {
            if (getContractEnrolled() == null)
                return null;
            return getContractEnrolled() + (getContractEnrolledListener() > 0 ? " (" + getContractEnrolledListener() + " сл)" : "");
        }

        @Override
        public int compareTo(Object o)
        {
            return getTitle().compareTo(((ReportRow) o).getTitle());
        }
    }

    private static Map<String, String> getDevelopFormCode2Title()
    {
        Map<String, String> developFormCode2Title = new HashMap<>();
        for (DevelopForm df : UniDaoFacade.getCoreDao().getCatalogItemList(DevelopForm.class))
        {
            developFormCode2Title.put(df.getCode(), StringUtils.uncapitalize(df.getTitle()));
        }
        return developFormCode2Title;
    }
}
