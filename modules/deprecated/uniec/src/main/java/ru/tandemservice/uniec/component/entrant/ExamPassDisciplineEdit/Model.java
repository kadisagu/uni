/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.ExamPassDisciplineEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;

import ru.tandemservice.uniec.entity.entrant.EntranceExamPhase;
import ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline;

import java.util.List;

/**
 * @author vip_delete
 * @since 23.03.2009
 */
@Input({
        @Bind(key = "examPassDisciplineId", binding = "examPassDiscipline.id", required = true)
})
public class Model
{
    private ExamPassDiscipline _examPassDiscipline = new ExamPassDiscipline();
    private List<EntranceExamPhase> _examPhaseList;
    private EntranceExamPhase _examPhase;


    public ExamPassDiscipline getExamPassDiscipline()
    {
        return _examPassDiscipline;
    }

    public void setExamPassDiscipline(ExamPassDiscipline examPassDiscipline)
    {
        _examPassDiscipline = examPassDiscipline;
    }


    public List<EntranceExamPhase> getExamPhaseList()
    {
        return _examPhaseList;
    }

    public void setExamPhaseList(List<EntranceExamPhase> examPhaseList)
    {
        _examPhaseList = examPhaseList;
    }

    public EntranceExamPhase getExamPhase()
    {
        return _examPhase;
    }

    public void setExamPhase(EntranceExamPhase examPhase)
    {
        _examPhase = examPhase;
    }

}
