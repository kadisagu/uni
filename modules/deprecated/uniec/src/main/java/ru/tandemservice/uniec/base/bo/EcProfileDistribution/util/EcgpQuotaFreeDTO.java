/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec.base.bo.EcProfileDistribution.util;

import java.util.Map;

/**
 * @author Vasily Zhukov
 * @since 10.05.2012
 */
public class EcgpQuotaFreeDTO implements IEcgpQuotaFreeDTO
{
    private IEcgpQuotaDTO _quotaDTO;

    private IEcgpQuotaUsedDTO _quotaUsedDTO;

    private int _totalFree;

    private Map<Long, Integer> _freeMap;

    public EcgpQuotaFreeDTO(IEcgpQuotaDTO quotaDTO, IEcgpQuotaUsedDTO quotaUsedDTO, int totalFree, Map<Long, Integer> freeMap)
    {
        _quotaDTO = quotaDTO;
        _quotaUsedDTO = quotaUsedDTO;
        _totalFree = totalFree;
        _freeMap = freeMap;
    }

    // Getters

    public IEcgpQuotaDTO getQuotaDTO()
    {
        return _quotaDTO;
    }

    public IEcgpQuotaUsedDTO getQuotaUsedDTO()
    {
        return _quotaUsedDTO;
    }

    public int getTotalFree()
    {
        return _totalFree;
    }

    public Map<Long, Integer> getFreeMap()
    {
        return _freeMap;
    }
}
