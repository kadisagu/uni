/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.util;

import java.util.Map;

/**
 * Свободные места по направлениям и видам целевого приема
 *
 * @author Vasily Zhukov
 * @since 11.07.2011
 */
public class EcgQuotaFreeDTO implements IEcgQuotaFreeDTO
{
    /**
     * планы по направлениям и видам цп
     */
    private IEcgQuotaDTO _quotaDTO;

    /**
     * количество занятых места по направлениям и видам цп
     */
    private IEcgQuotaUsedDTO _quotaUsedDTO;

    /**
     * направление приема -> свободно мест
     * свободно мест всегда не null, может быть отрицательно
     */
    private Map<Long, Integer> _freeMap;

    /**
     * направление приема -> вид целевого приема -> свободно мест
     * свободно мест всегда не null, может быть отрицательно
     */
    private Map<Long, Map<Long, Integer>> _taFreeMap;

    public EcgQuotaFreeDTO(IEcgQuotaDTO quotaDTO, IEcgQuotaUsedDTO quotaUsedDTO, Map<Long, Integer> freeMap, Map<Long, Map<Long, Integer>> taFreeMap)
    {
        _quotaDTO = quotaDTO;
        _quotaUsedDTO = quotaUsedDTO;
        _freeMap = freeMap;
        _taFreeMap = taFreeMap;
    }

    // Getters

    @Override
    public IEcgQuotaDTO getQuotaDTO()
    {
        return _quotaDTO;
    }

    @Override
    public IEcgQuotaUsedDTO getQuotaUsedDTO()
    {
        return _quotaUsedDTO;
    }

    @Override
    public Map<Long, Integer> getFreeMap()
    {
        return _freeMap;
    }

    @Override
    public Map<Long, Map<Long, Integer>> getTaFreeMap()
    {
        return _taFreeMap;
    }
}
