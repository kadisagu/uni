/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.dao.examgroup;

import org.hibernate.Session;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * Реализация стандартного алгоритма печати экзаменационной ведомости на экзаменационную группу
 *
 * @author vip_delete
 * @since 08.07.2009
 */
public abstract class AbstractExamSheetPrintBuilder implements IExamGroupPrint
{
    private Session _session;

    @Override
    public void init(Session session)
    {
        _session = session;
    }

    protected Session getSession()
    {
        return _session;
    }

    @Override
    public final RtfDocument createPrintForm(List<Long> ids)
    {
        if (ids.isEmpty())
            throw new ApplicationException("Нет данных для печати.");

        // загружаем настройку дат сдачи дисциплин
        String phAlias = "phase";
        Map<PairKey<Discipline2RealizationWayRelation, SubjectPassForm>, List<Date>> passDateByDiscipline = new DQLSelectBuilder()
                .fromEntity(EntranceExamPhase.class, phAlias)
                .column(property(phAlias))
                .where(eq(property(phAlias, EntranceExamPhase.setting().discipline().enrollmentCampaign()),
                          value(UniDaoFacade.getCoreDao().getNotNull(ExamGroup.class, ids.get(0)).getExamGroupSet().getEnrollmentCampaign().getId())))
                .order(property(phAlias, EntranceExamPhase.passDate()))
                .createStatement(getSession()).<EntranceExamPhase>list()
                .stream()
                .collect(Collectors.groupingBy(phase -> new PairKey<>(phase.getSetting().getDiscipline(), phase.getSetting().getSubjectPassForm()),
                                               Collectors.mapping(EntranceExamPhase::getPassDate, Collectors.toList())));

        // загружаем сокращенное название вуза
        String vuzShortTitle = TopOrgUnit.getInstance().getShortTitle();

        // загружаем шаблон
        RtfDocument template = getTemplate();

        // создаем результирующий документ, наследуя настройки из шаблона
        RtfDocument result = RtfBean.getElementFactory().createRtfDocument();
        result.setHeader(template.getHeader());
        result.setSettings(template.getSettings());

        for (Long id : ids)
        {
            // загружаем экзаменационную группу
            ExamGroup group = (ExamGroup) _session.get(ExamGroup.class, id);

            // нет группы, нечего печатать
            if (group == null) continue;

            Map<PairKey<Discipline2RealizationWayRelation, SubjectPassForm>, Set<RequestedEnrollmentDirection>> map = UniecDAOFacade.getExamGroupSetDao().getExamPassDisciplineList(group);

            for (Map.Entry<PairKey<Discipline2RealizationWayRelation, SubjectPassForm>, Set<RequestedEnrollmentDirection>> entry : map.entrySet())
            {
                PairKey<Discipline2RealizationWayRelation, SubjectPassForm> key = entry.getKey();

                // дисциплина
                Discipline2RealizationWayRelation discipline = key.getFirst();

                // форма сдачи
                SubjectPassForm subjectPassForm = key.getSecond();

                // не портим шаблон
                RtfDocument document = template.getClone();

                // дата сдачи
                List<Date> passDates = passDateByDiscipline.get(key);

                // модификатор документа
                RtfInjectModifier injectModifier = getRtfInjectModifier(group, discipline, subjectPassForm, passDates);

                // подставляем сокращенное название вуза
                injectModifier.put("vuzShortTitle", vuzShortTitle);

                // заменяем метки в документе
                injectModifier.modify(document);

                // подставляем данные в таблицу документа
                getRtfTableModifier(entry.getValue(), subjectPassForm).modify(document);

                // добавляем получившийся документ в результирующий
                result.getElementList().addAll(document.getElementList());
            }
        }

        if (result.getElementList().isEmpty())
            throw new ApplicationException("Нет данных для печати.");

        return result;
    }

    protected RtfDocument getTemplate()
    {
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, getTemplateCode());

        return new RtfReader().read(templateDocument.getCurrentTemplate());
    }

    protected RtfInjectModifier getRtfInjectModifier(ExamGroup group, Discipline2RealizationWayRelation discipline, SubjectPassForm subjectPassForm, List<Date> passDates)
    {
        String dates = passDates == null ? "" :
                passDates.stream().distinct().map(DateFormatter.DEFAULT_DATE_FORMATTER::format).collect(Collectors.joining(", "));

        return new RtfInjectModifier()
                .put("groupTitle", group.getShortTitle())
                .put("examPassDisciplineTitle", discipline.getTitle() + " (" + subjectPassForm.getTitle() + ")")
                .put("passDate", dates);
    }

    protected RtfTableModifier getRtfTableModifier(Set<RequestedEnrollmentDirection> directionSet, SubjectPassForm subjectPassForm)
    {
        // данные для таблицы
        String[][] tableData = getTableData(directionSet, subjectPassForm);

        return new RtfTableModifier().put("T", tableData);
    }

    protected String[][] getTableData(Set<RequestedEnrollmentDirection> directionSet, SubjectPassForm subjectPassForm)
    {
        // получаем список строк
        Set<EntrantRequest> requestSet = new HashSet<>();
        for (RequestedEnrollmentDirection direction : directionSet) requestSet.add(direction.getEntrantRequest());

        // сортируем его
        List<EntrantRequest> list = new ArrayList<>(requestSet);
        list.sort(new EntityComparator<>(new EntityOrder(new String[]{EntrantRequest.L_ENTRANT, Entrant.L_PERSON, Person.P_FULLFIO})));

        String[][] result = new String[list.size() + 3][];

        for (int i = 0; i < list.size(); i++)
            result[i] = getTableRow(i, list.get(i), subjectPassForm);

        return result;
    }

    protected String[] getTableRow(int rowNumber, EntrantRequest entrantRequest, SubjectPassForm subjectPassForm)
    {
        String number = Integer.toString(rowNumber + 1);                   // номер по порядку
        String stringNumber = entrantRequest.getStringNumber();            // номер заявления
        String fio = entrantRequest.getEntrant().getPerson().getFullFio(); // фамилия

        return new String[]{
                /*0*/ number,
                /*1*/ stringNumber,
                /*2*/ fio,
                /*3*/ UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM_INTERNAL.equals(subjectPassForm.getCode()) ? subjectPassForm.getShortTitle() : null,
                /*4*/ null,
                /*5*/ null,
                /*6*/ null,
                /*7*/ number,
                /*8*/ stringNumber,
                /*9*/ fio
        };
    }

    protected abstract String getTemplateCode();
}
