/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcOrder.ui.RevertParAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniec.base.bo.EcOrder.logic.EcEnrollmentOrderTypeHandler;
import ru.tandemservice.uniec.base.bo.EcOrder.logic.EcOrderEnrollmentExtractDSHandler;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.unimove.UnimoveDefines;

/**
 * @author Nikolay Fedorovskih
 * @since 05.08.2013
 */
@Configuration
public class EcOrderRevertParAddEdit extends BusinessComponentManager
{
    public static final String ORDER_TYPE_DS = "orderTypeDS";
    public static final String ORDER_DS = "orderDS";
    public static final String EXTRACT_DS = "extractDS";

    public static final String ENROLLMENT_CAMPAIGN_PARAM = "enrollmentCampaign";
    public static final String ORDER_TYPE_PARAM = "orderType";
    public static final String ORDER_PARAM = EcOrderEnrollmentExtractDSHandler.ENROLLMENT_ORDER_PARAM;

    public static final String CHECKBOX_COLUMN = "checkbox";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ORDER_TYPE_DS, orderTypeDSHandler()))
                .addDataSource(selectDS(ORDER_DS, orderDSHandler()))
                .addDataSource(searchListDS(EXTRACT_DS, getExtractDS(), enrollmentExtractDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint getExtractDS()
    {
        return columnListExtPointBuilder(EXTRACT_DS)
                .addColumn(checkboxColumn(CHECKBOX_COLUMN))
                .addColumn(textColumn("fio", EnrollmentExtract.entity().requestedEnrollmentDirection().entrantRequest().entrant().person().identityCard().fullFio()))
                .addColumn(textColumn("sex", EnrollmentExtract.entity().requestedEnrollmentDirection().entrantRequest().entrant().person().identityCard().sex().shortTitle()))
                .addColumn(textColumn("passport", EnrollmentExtract.entity().requestedEnrollmentDirection().entrantRequest().entrant().person().identityCard().fullNumber()))
                .addColumn(textColumn("direction", EnrollmentExtract.entity().educationOrgUnit().educationLevelHighSchool().title()))
                .addColumn(textColumn("conditions", EnrollmentExtract.entity().enrollmentConditions()))
                .addColumn(textColumn("eduInstitution", EcOrderEnrollmentExtractDSHandler.EDU_INSTITUTION))
                .addColumn(textColumn("formativeOrgUnit", EnrollmentExtract.entity().educationOrgUnit().formativeOrgUnit().title()))
                .addColumn(textColumn("territorialOrgUnit", EnrollmentExtract.entity().educationOrgUnit().territorialOrgUnit().title()))
                .addColumn(textColumn("developForm", EnrollmentExtract.entity().educationOrgUnit().developForm().title()))
                .addColumn(textColumn("developCondition", EnrollmentExtract.entity().educationOrgUnit().developCondition().title()))
                .addColumn(textColumn("developTech", EnrollmentExtract.entity().educationOrgUnit().developTech().title()))
                .addColumn(textColumn("developPeriod", EnrollmentExtract.entity().educationOrgUnit().developPeriod().title()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> orderTypeDSHandler()
    {
        return new EcEnrollmentOrderTypeHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> orderDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrollmentOrder.class)
                .where(EnrollmentOrder.enrollmentCampaign(), ENROLLMENT_CAMPAIGN_PARAM)
                .where(EnrollmentOrder.type(), ORDER_TYPE_PARAM)
                .where(EnrollmentOrder.state().code(), (Object) UnimoveDefines.CATALOG_ORDER_STATE_FINISHED)
                .order(EnrollmentOrder.number());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> enrollmentExtractDSHandler()
    {
        return new EcOrderEnrollmentExtractDSHandler(getName());
    }

}