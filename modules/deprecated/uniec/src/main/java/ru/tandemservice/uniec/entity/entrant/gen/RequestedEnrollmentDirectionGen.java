package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выбранное направление приема
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RequestedEnrollmentDirectionGen extends VersionedEntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection";
    public static final String ENTITY_NAME = "requestedEnrollmentDirection";
    public static final int VERSION_HASH = 1094115230;
    private static IEntityMeta ENTITY_META;

    public static final String P_VERSION = "version";
    public static final String L_ENTRANT_REQUEST = "entrantRequest";
    public static final String L_ENROLLMENT_DIRECTION = "enrollmentDirection";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String L_COMPETITION_KIND = "competitionKind";
    public static final String L_STATE = "state";
    public static final String P_REG_DATE = "regDate";
    public static final String P_NUMBER = "number";
    public static final String P_PRIORITY = "priority";
    public static final String P_PRIORITY_PER_ENTRANT = "priorityPerEntrant";
    public static final String P_ORIGINAL_DOCUMENT_HANDED_IN = "originalDocumentHandedIn";
    public static final String P_TARGET_ADMISSION = "targetAdmission";
    public static final String L_TARGET_ADMISSION_KIND = "targetAdmissionKind";
    public static final String L_STUDENT_CATEGORY = "studentCategory";
    public static final String L_PROFILE_CHOSEN_ENTRANCE_DISCIPLINE = "profileChosenEntranceDiscipline";
    public static final String P_GROUP = "group";
    public static final String P_PROFILE_WORK_EXPERIENCE_YEARS = "profileWorkExperienceYears";
    public static final String P_PROFILE_WORK_EXPERIENCE_MONTHS = "profileWorkExperienceMonths";
    public static final String P_PROFILE_WORK_EXPERIENCE_DAYS = "profileWorkExperienceDays";
    public static final String P_PROFILE_POST = "profilePost";
    public static final String P_STUDIED_O_P_P = "studiedOPP";
    public static final String P_GRADUATED_PROFILE_EDU_INSTITUTION = "graduatedProfileEduInstitution";
    public static final String P_GRADUATED_PROFILE_CLASS = "graduatedProfileClass";
    public static final String P_COMMENT = "comment";
    public static final String P_AGREE4_ENROLLMENT = "agree4Enrollment";
    public static final String L_EXTERNAL_ORG_UNIT = "externalOrgUnit";

    private int _version; 
    private EntrantRequest _entrantRequest;     // Заявление абитуриента
    private EnrollmentDirection _enrollmentDirection;     // Направление подготовки (специальность) приема
    private CompensationType _compensationType;     // Вид возмещения затрат
    private CompetitionKind _competitionKind;     // Вид конкурса
    private EntrantState _state;     // Состояние абитуриента
    private Date _regDate;     // Дата добавления
    private int _number;     // Номер
    private int _priority;     // Приоритет
    private int _priorityPerEntrant;     // Приоритет в рамках абитуриента в разрезе вида затрат
    private boolean _originalDocumentHandedIn;     // Сданы оригиналы документов
    private boolean _targetAdmission;     // Целевой прием
    private TargetAdmissionKind _targetAdmissionKind;     // Вид целевого приема
    private StudentCategory _studentCategory;     // Категория обучаемого
    private ChosenEntranceDiscipline _profileChosenEntranceDiscipline;     // Профильное выбранное вступительное испытание
    private String _group;     // Группа
    private Integer _profileWorkExperienceYears;     // Стаж работы по профилю (лет)
    private Integer _profileWorkExperienceMonths;     // Стаж работы по профилю (месяцев)
    private Integer _profileWorkExperienceDays;     // Стаж работы по профилю (дней)
    private String _profilePost;     // Профильная должность
    private boolean _studiedOPP;     // Учился в ОПП
    private boolean _graduatedProfileEduInstitution;     // Закончил профильное учебное заведение
    private boolean _graduatedProfileClass;     // Закончил профильный класс
    private String _comment;     // Комментарий
    private boolean _agree4Enrollment = false;     // Согласие на зачисление
    private ExternalOrgUnit _externalOrgUnit;     // Организация

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public int getVersion()
    {
        return _version;
    }

    /**
     * @param version  Свойство не может быть null.
     */
    public void setVersion(int version)
    {
        dirty(_version, version);
        _version = version;
    }

    /**
     * @return Заявление абитуриента. Свойство не может быть null.
     */
    @NotNull
    public EntrantRequest getEntrantRequest()
    {
        return _entrantRequest;
    }

    /**
     * @param entrantRequest Заявление абитуриента. Свойство не может быть null.
     */
    public void setEntrantRequest(EntrantRequest entrantRequest)
    {
        dirty(_entrantRequest, entrantRequest);
        _entrantRequest = entrantRequest;
    }

    /**
     * @return Направление подготовки (специальность) приема. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentDirection getEnrollmentDirection()
    {
        return _enrollmentDirection;
    }

    /**
     * @param enrollmentDirection Направление подготовки (специальность) приема. Свойство не может быть null.
     */
    public void setEnrollmentDirection(EnrollmentDirection enrollmentDirection)
    {
        dirty(_enrollmentDirection, enrollmentDirection);
        _enrollmentDirection = enrollmentDirection;
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Вид конкурса. Свойство не может быть null.
     */
    @NotNull
    public CompetitionKind getCompetitionKind()
    {
        return _competitionKind;
    }

    /**
     * @param competitionKind Вид конкурса. Свойство не может быть null.
     */
    public void setCompetitionKind(CompetitionKind competitionKind)
    {
        dirty(_competitionKind, competitionKind);
        _competitionKind = competitionKind;
    }

    /**
     * @return Состояние абитуриента. Свойство не может быть null.
     */
    @NotNull
    public EntrantState getState()
    {
        return _state;
    }

    /**
     * @param state Состояние абитуриента. Свойство не может быть null.
     */
    public void setState(EntrantState state)
    {
        dirty(_state, state);
        _state = state;
    }

    /**
     * @return Дата добавления. Свойство не может быть null.
     */
    @NotNull
    public Date getRegDate()
    {
        return _regDate;
    }

    /**
     * @param regDate Дата добавления. Свойство не может быть null.
     */
    public void setRegDate(Date regDate)
    {
        dirty(_regDate, regDate);
        _regDate = regDate;
    }

    /**
     * @return Номер. Свойство не может быть null.
     */
    @NotNull
    public int getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер. Свойство не может быть null.
     */
    public void setNumber(int number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Приоритет. Свойство не может быть null.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    /**
     * @return Приоритет в рамках абитуриента в разрезе вида затрат. Свойство не может быть null.
     */
    @NotNull
    public int getPriorityPerEntrant()
    {
        return _priorityPerEntrant;
    }

    /**
     * @param priorityPerEntrant Приоритет в рамках абитуриента в разрезе вида затрат. Свойство не может быть null.
     */
    public void setPriorityPerEntrant(int priorityPerEntrant)
    {
        dirty(_priorityPerEntrant, priorityPerEntrant);
        _priorityPerEntrant = priorityPerEntrant;
    }

    /**
     * @return Сданы оригиналы документов. Свойство не может быть null.
     */
    @NotNull
    public boolean isOriginalDocumentHandedIn()
    {
        return _originalDocumentHandedIn;
    }

    /**
     * @param originalDocumentHandedIn Сданы оригиналы документов. Свойство не может быть null.
     */
    public void setOriginalDocumentHandedIn(boolean originalDocumentHandedIn)
    {
        dirty(_originalDocumentHandedIn, originalDocumentHandedIn);
        _originalDocumentHandedIn = originalDocumentHandedIn;
    }

    /**
     * @return Целевой прием. Свойство не может быть null.
     */
    @NotNull
    public boolean isTargetAdmission()
    {
        return _targetAdmission;
    }

    /**
     * @param targetAdmission Целевой прием. Свойство не может быть null.
     */
    public void setTargetAdmission(boolean targetAdmission)
    {
        dirty(_targetAdmission, targetAdmission);
        _targetAdmission = targetAdmission;
    }

    /**
     * @return Вид целевого приема.
     */
    public TargetAdmissionKind getTargetAdmissionKind()
    {
        return _targetAdmissionKind;
    }

    /**
     * @param targetAdmissionKind Вид целевого приема.
     */
    public void setTargetAdmissionKind(TargetAdmissionKind targetAdmissionKind)
    {
        dirty(_targetAdmissionKind, targetAdmissionKind);
        _targetAdmissionKind = targetAdmissionKind;
    }

    /**
     * @return Категория обучаемого. Свойство не может быть null.
     */
    @NotNull
    public StudentCategory getStudentCategory()
    {
        return _studentCategory;
    }

    /**
     * @param studentCategory Категория обучаемого. Свойство не может быть null.
     */
    public void setStudentCategory(StudentCategory studentCategory)
    {
        dirty(_studentCategory, studentCategory);
        _studentCategory = studentCategory;
    }

    /**
     * @return Профильное выбранное вступительное испытание.
     */
    public ChosenEntranceDiscipline getProfileChosenEntranceDiscipline()
    {
        return _profileChosenEntranceDiscipline;
    }

    /**
     * @param profileChosenEntranceDiscipline Профильное выбранное вступительное испытание.
     */
    public void setProfileChosenEntranceDiscipline(ChosenEntranceDiscipline profileChosenEntranceDiscipline)
    {
        dirty(_profileChosenEntranceDiscipline, profileChosenEntranceDiscipline);
        _profileChosenEntranceDiscipline = profileChosenEntranceDiscipline;
    }

    /**
     * @return Группа.
     */
    @Length(max=255)
    public String getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа.
     */
    public void setGroup(String group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Стаж работы по профилю (лет).
     */
    public Integer getProfileWorkExperienceYears()
    {
        return _profileWorkExperienceYears;
    }

    /**
     * @param profileWorkExperienceYears Стаж работы по профилю (лет).
     */
    public void setProfileWorkExperienceYears(Integer profileWorkExperienceYears)
    {
        dirty(_profileWorkExperienceYears, profileWorkExperienceYears);
        _profileWorkExperienceYears = profileWorkExperienceYears;
    }

    /**
     * @return Стаж работы по профилю (месяцев).
     */
    public Integer getProfileWorkExperienceMonths()
    {
        return _profileWorkExperienceMonths;
    }

    /**
     * @param profileWorkExperienceMonths Стаж работы по профилю (месяцев).
     */
    public void setProfileWorkExperienceMonths(Integer profileWorkExperienceMonths)
    {
        dirty(_profileWorkExperienceMonths, profileWorkExperienceMonths);
        _profileWorkExperienceMonths = profileWorkExperienceMonths;
    }

    /**
     * @return Стаж работы по профилю (дней).
     */
    public Integer getProfileWorkExperienceDays()
    {
        return _profileWorkExperienceDays;
    }

    /**
     * @param profileWorkExperienceDays Стаж работы по профилю (дней).
     */
    public void setProfileWorkExperienceDays(Integer profileWorkExperienceDays)
    {
        dirty(_profileWorkExperienceDays, profileWorkExperienceDays);
        _profileWorkExperienceDays = profileWorkExperienceDays;
    }

    /**
     * @return Профильная должность.
     */
    @Length(max=255)
    public String getProfilePost()
    {
        return _profilePost;
    }

    /**
     * @param profilePost Профильная должность.
     */
    public void setProfilePost(String profilePost)
    {
        dirty(_profilePost, profilePost);
        _profilePost = profilePost;
    }

    /**
     * @return Учился в ОПП. Свойство не может быть null.
     */
    @NotNull
    public boolean isStudiedOPP()
    {
        return _studiedOPP;
    }

    /**
     * @param studiedOPP Учился в ОПП. Свойство не может быть null.
     */
    public void setStudiedOPP(boolean studiedOPP)
    {
        dirty(_studiedOPP, studiedOPP);
        _studiedOPP = studiedOPP;
    }

    /**
     * @return Закончил профильное учебное заведение. Свойство не может быть null.
     */
    @NotNull
    public boolean isGraduatedProfileEduInstitution()
    {
        return _graduatedProfileEduInstitution;
    }

    /**
     * @param graduatedProfileEduInstitution Закончил профильное учебное заведение. Свойство не может быть null.
     */
    public void setGraduatedProfileEduInstitution(boolean graduatedProfileEduInstitution)
    {
        dirty(_graduatedProfileEduInstitution, graduatedProfileEduInstitution);
        _graduatedProfileEduInstitution = graduatedProfileEduInstitution;
    }

    /**
     * @return Закончил профильный класс. Свойство не может быть null.
     */
    @NotNull
    public boolean isGraduatedProfileClass()
    {
        return _graduatedProfileClass;
    }

    /**
     * @param graduatedProfileClass Закончил профильный класс. Свойство не может быть null.
     */
    public void setGraduatedProfileClass(boolean graduatedProfileClass)
    {
        dirty(_graduatedProfileClass, graduatedProfileClass);
        _graduatedProfileClass = graduatedProfileClass;
    }

    /**
     * @return Комментарий.
     */
    @Length(max=2000)
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * @return Согласие на зачисление. Свойство не может быть null.
     */
    @NotNull
    public boolean isAgree4Enrollment()
    {
        return _agree4Enrollment;
    }

    /**
     * @param agree4Enrollment Согласие на зачисление. Свойство не может быть null.
     */
    public void setAgree4Enrollment(boolean agree4Enrollment)
    {
        dirty(_agree4Enrollment, agree4Enrollment);
        _agree4Enrollment = agree4Enrollment;
    }

    /**
     * @return Организация.
     */
    public ExternalOrgUnit getExternalOrgUnit()
    {
        return _externalOrgUnit;
    }

    /**
     * @param externalOrgUnit Организация.
     */
    public void setExternalOrgUnit(ExternalOrgUnit externalOrgUnit)
    {
        dirty(_externalOrgUnit, externalOrgUnit);
        _externalOrgUnit = externalOrgUnit;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof RequestedEnrollmentDirectionGen)
        {
            setVersion(((RequestedEnrollmentDirection)another).getVersion());
            setEntrantRequest(((RequestedEnrollmentDirection)another).getEntrantRequest());
            setEnrollmentDirection(((RequestedEnrollmentDirection)another).getEnrollmentDirection());
            setCompensationType(((RequestedEnrollmentDirection)another).getCompensationType());
            setCompetitionKind(((RequestedEnrollmentDirection)another).getCompetitionKind());
            setState(((RequestedEnrollmentDirection)another).getState());
            setRegDate(((RequestedEnrollmentDirection)another).getRegDate());
            setNumber(((RequestedEnrollmentDirection)another).getNumber());
            setPriority(((RequestedEnrollmentDirection)another).getPriority());
            setPriorityPerEntrant(((RequestedEnrollmentDirection)another).getPriorityPerEntrant());
            setOriginalDocumentHandedIn(((RequestedEnrollmentDirection)another).isOriginalDocumentHandedIn());
            setTargetAdmission(((RequestedEnrollmentDirection)another).isTargetAdmission());
            setTargetAdmissionKind(((RequestedEnrollmentDirection)another).getTargetAdmissionKind());
            setStudentCategory(((RequestedEnrollmentDirection)another).getStudentCategory());
            setProfileChosenEntranceDiscipline(((RequestedEnrollmentDirection)another).getProfileChosenEntranceDiscipline());
            setGroup(((RequestedEnrollmentDirection)another).getGroup());
            setProfileWorkExperienceYears(((RequestedEnrollmentDirection)another).getProfileWorkExperienceYears());
            setProfileWorkExperienceMonths(((RequestedEnrollmentDirection)another).getProfileWorkExperienceMonths());
            setProfileWorkExperienceDays(((RequestedEnrollmentDirection)another).getProfileWorkExperienceDays());
            setProfilePost(((RequestedEnrollmentDirection)another).getProfilePost());
            setStudiedOPP(((RequestedEnrollmentDirection)another).isStudiedOPP());
            setGraduatedProfileEduInstitution(((RequestedEnrollmentDirection)another).isGraduatedProfileEduInstitution());
            setGraduatedProfileClass(((RequestedEnrollmentDirection)another).isGraduatedProfileClass());
            setComment(((RequestedEnrollmentDirection)another).getComment());
            setAgree4Enrollment(((RequestedEnrollmentDirection)another).isAgree4Enrollment());
            setExternalOrgUnit(((RequestedEnrollmentDirection)another).getExternalOrgUnit());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RequestedEnrollmentDirectionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RequestedEnrollmentDirection.class;
        }

        public T newInstance()
        {
            return (T) new RequestedEnrollmentDirection();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "version":
                    return obj.getVersion();
                case "entrantRequest":
                    return obj.getEntrantRequest();
                case "enrollmentDirection":
                    return obj.getEnrollmentDirection();
                case "compensationType":
                    return obj.getCompensationType();
                case "competitionKind":
                    return obj.getCompetitionKind();
                case "state":
                    return obj.getState();
                case "regDate":
                    return obj.getRegDate();
                case "number":
                    return obj.getNumber();
                case "priority":
                    return obj.getPriority();
                case "priorityPerEntrant":
                    return obj.getPriorityPerEntrant();
                case "originalDocumentHandedIn":
                    return obj.isOriginalDocumentHandedIn();
                case "targetAdmission":
                    return obj.isTargetAdmission();
                case "targetAdmissionKind":
                    return obj.getTargetAdmissionKind();
                case "studentCategory":
                    return obj.getStudentCategory();
                case "profileChosenEntranceDiscipline":
                    return obj.getProfileChosenEntranceDiscipline();
                case "group":
                    return obj.getGroup();
                case "profileWorkExperienceYears":
                    return obj.getProfileWorkExperienceYears();
                case "profileWorkExperienceMonths":
                    return obj.getProfileWorkExperienceMonths();
                case "profileWorkExperienceDays":
                    return obj.getProfileWorkExperienceDays();
                case "profilePost":
                    return obj.getProfilePost();
                case "studiedOPP":
                    return obj.isStudiedOPP();
                case "graduatedProfileEduInstitution":
                    return obj.isGraduatedProfileEduInstitution();
                case "graduatedProfileClass":
                    return obj.isGraduatedProfileClass();
                case "comment":
                    return obj.getComment();
                case "agree4Enrollment":
                    return obj.isAgree4Enrollment();
                case "externalOrgUnit":
                    return obj.getExternalOrgUnit();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "version":
                    obj.setVersion((Integer) value);
                    return;
                case "entrantRequest":
                    obj.setEntrantRequest((EntrantRequest) value);
                    return;
                case "enrollmentDirection":
                    obj.setEnrollmentDirection((EnrollmentDirection) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "competitionKind":
                    obj.setCompetitionKind((CompetitionKind) value);
                    return;
                case "state":
                    obj.setState((EntrantState) value);
                    return;
                case "regDate":
                    obj.setRegDate((Date) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
                case "priorityPerEntrant":
                    obj.setPriorityPerEntrant((Integer) value);
                    return;
                case "originalDocumentHandedIn":
                    obj.setOriginalDocumentHandedIn((Boolean) value);
                    return;
                case "targetAdmission":
                    obj.setTargetAdmission((Boolean) value);
                    return;
                case "targetAdmissionKind":
                    obj.setTargetAdmissionKind((TargetAdmissionKind) value);
                    return;
                case "studentCategory":
                    obj.setStudentCategory((StudentCategory) value);
                    return;
                case "profileChosenEntranceDiscipline":
                    obj.setProfileChosenEntranceDiscipline((ChosenEntranceDiscipline) value);
                    return;
                case "group":
                    obj.setGroup((String) value);
                    return;
                case "profileWorkExperienceYears":
                    obj.setProfileWorkExperienceYears((Integer) value);
                    return;
                case "profileWorkExperienceMonths":
                    obj.setProfileWorkExperienceMonths((Integer) value);
                    return;
                case "profileWorkExperienceDays":
                    obj.setProfileWorkExperienceDays((Integer) value);
                    return;
                case "profilePost":
                    obj.setProfilePost((String) value);
                    return;
                case "studiedOPP":
                    obj.setStudiedOPP((Boolean) value);
                    return;
                case "graduatedProfileEduInstitution":
                    obj.setGraduatedProfileEduInstitution((Boolean) value);
                    return;
                case "graduatedProfileClass":
                    obj.setGraduatedProfileClass((Boolean) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "agree4Enrollment":
                    obj.setAgree4Enrollment((Boolean) value);
                    return;
                case "externalOrgUnit":
                    obj.setExternalOrgUnit((ExternalOrgUnit) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "version":
                        return true;
                case "entrantRequest":
                        return true;
                case "enrollmentDirection":
                        return true;
                case "compensationType":
                        return true;
                case "competitionKind":
                        return true;
                case "state":
                        return true;
                case "regDate":
                        return true;
                case "number":
                        return true;
                case "priority":
                        return true;
                case "priorityPerEntrant":
                        return true;
                case "originalDocumentHandedIn":
                        return true;
                case "targetAdmission":
                        return true;
                case "targetAdmissionKind":
                        return true;
                case "studentCategory":
                        return true;
                case "profileChosenEntranceDiscipline":
                        return true;
                case "group":
                        return true;
                case "profileWorkExperienceYears":
                        return true;
                case "profileWorkExperienceMonths":
                        return true;
                case "profileWorkExperienceDays":
                        return true;
                case "profilePost":
                        return true;
                case "studiedOPP":
                        return true;
                case "graduatedProfileEduInstitution":
                        return true;
                case "graduatedProfileClass":
                        return true;
                case "comment":
                        return true;
                case "agree4Enrollment":
                        return true;
                case "externalOrgUnit":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "version":
                    return true;
                case "entrantRequest":
                    return true;
                case "enrollmentDirection":
                    return true;
                case "compensationType":
                    return true;
                case "competitionKind":
                    return true;
                case "state":
                    return true;
                case "regDate":
                    return true;
                case "number":
                    return true;
                case "priority":
                    return true;
                case "priorityPerEntrant":
                    return true;
                case "originalDocumentHandedIn":
                    return true;
                case "targetAdmission":
                    return true;
                case "targetAdmissionKind":
                    return true;
                case "studentCategory":
                    return true;
                case "profileChosenEntranceDiscipline":
                    return true;
                case "group":
                    return true;
                case "profileWorkExperienceYears":
                    return true;
                case "profileWorkExperienceMonths":
                    return true;
                case "profileWorkExperienceDays":
                    return true;
                case "profilePost":
                    return true;
                case "studiedOPP":
                    return true;
                case "graduatedProfileEduInstitution":
                    return true;
                case "graduatedProfileClass":
                    return true;
                case "comment":
                    return true;
                case "agree4Enrollment":
                    return true;
                case "externalOrgUnit":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "version":
                    return Integer.class;
                case "entrantRequest":
                    return EntrantRequest.class;
                case "enrollmentDirection":
                    return EnrollmentDirection.class;
                case "compensationType":
                    return CompensationType.class;
                case "competitionKind":
                    return CompetitionKind.class;
                case "state":
                    return EntrantState.class;
                case "regDate":
                    return Date.class;
                case "number":
                    return Integer.class;
                case "priority":
                    return Integer.class;
                case "priorityPerEntrant":
                    return Integer.class;
                case "originalDocumentHandedIn":
                    return Boolean.class;
                case "targetAdmission":
                    return Boolean.class;
                case "targetAdmissionKind":
                    return TargetAdmissionKind.class;
                case "studentCategory":
                    return StudentCategory.class;
                case "profileChosenEntranceDiscipline":
                    return ChosenEntranceDiscipline.class;
                case "group":
                    return String.class;
                case "profileWorkExperienceYears":
                    return Integer.class;
                case "profileWorkExperienceMonths":
                    return Integer.class;
                case "profileWorkExperienceDays":
                    return Integer.class;
                case "profilePost":
                    return String.class;
                case "studiedOPP":
                    return Boolean.class;
                case "graduatedProfileEduInstitution":
                    return Boolean.class;
                case "graduatedProfileClass":
                    return Boolean.class;
                case "comment":
                    return String.class;
                case "agree4Enrollment":
                    return Boolean.class;
                case "externalOrgUnit":
                    return ExternalOrgUnit.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RequestedEnrollmentDirection> _dslPath = new Path<RequestedEnrollmentDirection>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RequestedEnrollmentDirection");
    }
            

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getVersion()
     */
    public static PropertyPath<Integer> version()
    {
        return _dslPath.version();
    }

    /**
     * @return Заявление абитуриента. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getEntrantRequest()
     */
    public static EntrantRequest.Path<EntrantRequest> entrantRequest()
    {
        return _dslPath.entrantRequest();
    }

    /**
     * @return Направление подготовки (специальность) приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getEnrollmentDirection()
     */
    public static EnrollmentDirection.Path<EnrollmentDirection> enrollmentDirection()
    {
        return _dslPath.enrollmentDirection();
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Вид конкурса. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getCompetitionKind()
     */
    public static CompetitionKind.Path<CompetitionKind> competitionKind()
    {
        return _dslPath.competitionKind();
    }

    /**
     * @return Состояние абитуриента. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getState()
     */
    public static EntrantState.Path<EntrantState> state()
    {
        return _dslPath.state();
    }

    /**
     * @return Дата добавления. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getRegDate()
     */
    public static PropertyPath<Date> regDate()
    {
        return _dslPath.regDate();
    }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    /**
     * @return Приоритет в рамках абитуриента в разрезе вида затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getPriorityPerEntrant()
     */
    public static PropertyPath<Integer> priorityPerEntrant()
    {
        return _dslPath.priorityPerEntrant();
    }

    /**
     * @return Сданы оригиналы документов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#isOriginalDocumentHandedIn()
     */
    public static PropertyPath<Boolean> originalDocumentHandedIn()
    {
        return _dslPath.originalDocumentHandedIn();
    }

    /**
     * @return Целевой прием. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#isTargetAdmission()
     */
    public static PropertyPath<Boolean> targetAdmission()
    {
        return _dslPath.targetAdmission();
    }

    /**
     * @return Вид целевого приема.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getTargetAdmissionKind()
     */
    public static TargetAdmissionKind.Path<TargetAdmissionKind> targetAdmissionKind()
    {
        return _dslPath.targetAdmissionKind();
    }

    /**
     * @return Категория обучаемого. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getStudentCategory()
     */
    public static StudentCategory.Path<StudentCategory> studentCategory()
    {
        return _dslPath.studentCategory();
    }

    /**
     * @return Профильное выбранное вступительное испытание.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getProfileChosenEntranceDiscipline()
     */
    public static ChosenEntranceDiscipline.Path<ChosenEntranceDiscipline> profileChosenEntranceDiscipline()
    {
        return _dslPath.profileChosenEntranceDiscipline();
    }

    /**
     * @return Группа.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getGroup()
     */
    public static PropertyPath<String> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Стаж работы по профилю (лет).
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getProfileWorkExperienceYears()
     */
    public static PropertyPath<Integer> profileWorkExperienceYears()
    {
        return _dslPath.profileWorkExperienceYears();
    }

    /**
     * @return Стаж работы по профилю (месяцев).
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getProfileWorkExperienceMonths()
     */
    public static PropertyPath<Integer> profileWorkExperienceMonths()
    {
        return _dslPath.profileWorkExperienceMonths();
    }

    /**
     * @return Стаж работы по профилю (дней).
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getProfileWorkExperienceDays()
     */
    public static PropertyPath<Integer> profileWorkExperienceDays()
    {
        return _dslPath.profileWorkExperienceDays();
    }

    /**
     * @return Профильная должность.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getProfilePost()
     */
    public static PropertyPath<String> profilePost()
    {
        return _dslPath.profilePost();
    }

    /**
     * @return Учился в ОПП. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#isStudiedOPP()
     */
    public static PropertyPath<Boolean> studiedOPP()
    {
        return _dslPath.studiedOPP();
    }

    /**
     * @return Закончил профильное учебное заведение. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#isGraduatedProfileEduInstitution()
     */
    public static PropertyPath<Boolean> graduatedProfileEduInstitution()
    {
        return _dslPath.graduatedProfileEduInstitution();
    }

    /**
     * @return Закончил профильный класс. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#isGraduatedProfileClass()
     */
    public static PropertyPath<Boolean> graduatedProfileClass()
    {
        return _dslPath.graduatedProfileClass();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @return Согласие на зачисление. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#isAgree4Enrollment()
     */
    public static PropertyPath<Boolean> agree4Enrollment()
    {
        return _dslPath.agree4Enrollment();
    }

    /**
     * @return Организация.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getExternalOrgUnit()
     */
    public static ExternalOrgUnit.Path<ExternalOrgUnit> externalOrgUnit()
    {
        return _dslPath.externalOrgUnit();
    }

    public static class Path<E extends RequestedEnrollmentDirection> extends EntityPath<E>
    {
        private PropertyPath<Integer> _version;
        private EntrantRequest.Path<EntrantRequest> _entrantRequest;
        private EnrollmentDirection.Path<EnrollmentDirection> _enrollmentDirection;
        private CompensationType.Path<CompensationType> _compensationType;
        private CompetitionKind.Path<CompetitionKind> _competitionKind;
        private EntrantState.Path<EntrantState> _state;
        private PropertyPath<Date> _regDate;
        private PropertyPath<Integer> _number;
        private PropertyPath<Integer> _priority;
        private PropertyPath<Integer> _priorityPerEntrant;
        private PropertyPath<Boolean> _originalDocumentHandedIn;
        private PropertyPath<Boolean> _targetAdmission;
        private TargetAdmissionKind.Path<TargetAdmissionKind> _targetAdmissionKind;
        private StudentCategory.Path<StudentCategory> _studentCategory;
        private ChosenEntranceDiscipline.Path<ChosenEntranceDiscipline> _profileChosenEntranceDiscipline;
        private PropertyPath<String> _group;
        private PropertyPath<Integer> _profileWorkExperienceYears;
        private PropertyPath<Integer> _profileWorkExperienceMonths;
        private PropertyPath<Integer> _profileWorkExperienceDays;
        private PropertyPath<String> _profilePost;
        private PropertyPath<Boolean> _studiedOPP;
        private PropertyPath<Boolean> _graduatedProfileEduInstitution;
        private PropertyPath<Boolean> _graduatedProfileClass;
        private PropertyPath<String> _comment;
        private PropertyPath<Boolean> _agree4Enrollment;
        private ExternalOrgUnit.Path<ExternalOrgUnit> _externalOrgUnit;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getVersion()
     */
        public PropertyPath<Integer> version()
        {
            if(_version == null )
                _version = new PropertyPath<Integer>(RequestedEnrollmentDirectionGen.P_VERSION, this);
            return _version;
        }

    /**
     * @return Заявление абитуриента. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getEntrantRequest()
     */
        public EntrantRequest.Path<EntrantRequest> entrantRequest()
        {
            if(_entrantRequest == null )
                _entrantRequest = new EntrantRequest.Path<EntrantRequest>(L_ENTRANT_REQUEST, this);
            return _entrantRequest;
        }

    /**
     * @return Направление подготовки (специальность) приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getEnrollmentDirection()
     */
        public EnrollmentDirection.Path<EnrollmentDirection> enrollmentDirection()
        {
            if(_enrollmentDirection == null )
                _enrollmentDirection = new EnrollmentDirection.Path<EnrollmentDirection>(L_ENROLLMENT_DIRECTION, this);
            return _enrollmentDirection;
        }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Вид конкурса. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getCompetitionKind()
     */
        public CompetitionKind.Path<CompetitionKind> competitionKind()
        {
            if(_competitionKind == null )
                _competitionKind = new CompetitionKind.Path<CompetitionKind>(L_COMPETITION_KIND, this);
            return _competitionKind;
        }

    /**
     * @return Состояние абитуриента. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getState()
     */
        public EntrantState.Path<EntrantState> state()
        {
            if(_state == null )
                _state = new EntrantState.Path<EntrantState>(L_STATE, this);
            return _state;
        }

    /**
     * @return Дата добавления. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getRegDate()
     */
        public PropertyPath<Date> regDate()
        {
            if(_regDate == null )
                _regDate = new PropertyPath<Date>(RequestedEnrollmentDirectionGen.P_REG_DATE, this);
            return _regDate;
        }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(RequestedEnrollmentDirectionGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(RequestedEnrollmentDirectionGen.P_PRIORITY, this);
            return _priority;
        }

    /**
     * @return Приоритет в рамках абитуриента в разрезе вида затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getPriorityPerEntrant()
     */
        public PropertyPath<Integer> priorityPerEntrant()
        {
            if(_priorityPerEntrant == null )
                _priorityPerEntrant = new PropertyPath<Integer>(RequestedEnrollmentDirectionGen.P_PRIORITY_PER_ENTRANT, this);
            return _priorityPerEntrant;
        }

    /**
     * @return Сданы оригиналы документов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#isOriginalDocumentHandedIn()
     */
        public PropertyPath<Boolean> originalDocumentHandedIn()
        {
            if(_originalDocumentHandedIn == null )
                _originalDocumentHandedIn = new PropertyPath<Boolean>(RequestedEnrollmentDirectionGen.P_ORIGINAL_DOCUMENT_HANDED_IN, this);
            return _originalDocumentHandedIn;
        }

    /**
     * @return Целевой прием. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#isTargetAdmission()
     */
        public PropertyPath<Boolean> targetAdmission()
        {
            if(_targetAdmission == null )
                _targetAdmission = new PropertyPath<Boolean>(RequestedEnrollmentDirectionGen.P_TARGET_ADMISSION, this);
            return _targetAdmission;
        }

    /**
     * @return Вид целевого приема.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getTargetAdmissionKind()
     */
        public TargetAdmissionKind.Path<TargetAdmissionKind> targetAdmissionKind()
        {
            if(_targetAdmissionKind == null )
                _targetAdmissionKind = new TargetAdmissionKind.Path<TargetAdmissionKind>(L_TARGET_ADMISSION_KIND, this);
            return _targetAdmissionKind;
        }

    /**
     * @return Категория обучаемого. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getStudentCategory()
     */
        public StudentCategory.Path<StudentCategory> studentCategory()
        {
            if(_studentCategory == null )
                _studentCategory = new StudentCategory.Path<StudentCategory>(L_STUDENT_CATEGORY, this);
            return _studentCategory;
        }

    /**
     * @return Профильное выбранное вступительное испытание.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getProfileChosenEntranceDiscipline()
     */
        public ChosenEntranceDiscipline.Path<ChosenEntranceDiscipline> profileChosenEntranceDiscipline()
        {
            if(_profileChosenEntranceDiscipline == null )
                _profileChosenEntranceDiscipline = new ChosenEntranceDiscipline.Path<ChosenEntranceDiscipline>(L_PROFILE_CHOSEN_ENTRANCE_DISCIPLINE, this);
            return _profileChosenEntranceDiscipline;
        }

    /**
     * @return Группа.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getGroup()
     */
        public PropertyPath<String> group()
        {
            if(_group == null )
                _group = new PropertyPath<String>(RequestedEnrollmentDirectionGen.P_GROUP, this);
            return _group;
        }

    /**
     * @return Стаж работы по профилю (лет).
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getProfileWorkExperienceYears()
     */
        public PropertyPath<Integer> profileWorkExperienceYears()
        {
            if(_profileWorkExperienceYears == null )
                _profileWorkExperienceYears = new PropertyPath<Integer>(RequestedEnrollmentDirectionGen.P_PROFILE_WORK_EXPERIENCE_YEARS, this);
            return _profileWorkExperienceYears;
        }

    /**
     * @return Стаж работы по профилю (месяцев).
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getProfileWorkExperienceMonths()
     */
        public PropertyPath<Integer> profileWorkExperienceMonths()
        {
            if(_profileWorkExperienceMonths == null )
                _profileWorkExperienceMonths = new PropertyPath<Integer>(RequestedEnrollmentDirectionGen.P_PROFILE_WORK_EXPERIENCE_MONTHS, this);
            return _profileWorkExperienceMonths;
        }

    /**
     * @return Стаж работы по профилю (дней).
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getProfileWorkExperienceDays()
     */
        public PropertyPath<Integer> profileWorkExperienceDays()
        {
            if(_profileWorkExperienceDays == null )
                _profileWorkExperienceDays = new PropertyPath<Integer>(RequestedEnrollmentDirectionGen.P_PROFILE_WORK_EXPERIENCE_DAYS, this);
            return _profileWorkExperienceDays;
        }

    /**
     * @return Профильная должность.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getProfilePost()
     */
        public PropertyPath<String> profilePost()
        {
            if(_profilePost == null )
                _profilePost = new PropertyPath<String>(RequestedEnrollmentDirectionGen.P_PROFILE_POST, this);
            return _profilePost;
        }

    /**
     * @return Учился в ОПП. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#isStudiedOPP()
     */
        public PropertyPath<Boolean> studiedOPP()
        {
            if(_studiedOPP == null )
                _studiedOPP = new PropertyPath<Boolean>(RequestedEnrollmentDirectionGen.P_STUDIED_O_P_P, this);
            return _studiedOPP;
        }

    /**
     * @return Закончил профильное учебное заведение. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#isGraduatedProfileEduInstitution()
     */
        public PropertyPath<Boolean> graduatedProfileEduInstitution()
        {
            if(_graduatedProfileEduInstitution == null )
                _graduatedProfileEduInstitution = new PropertyPath<Boolean>(RequestedEnrollmentDirectionGen.P_GRADUATED_PROFILE_EDU_INSTITUTION, this);
            return _graduatedProfileEduInstitution;
        }

    /**
     * @return Закончил профильный класс. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#isGraduatedProfileClass()
     */
        public PropertyPath<Boolean> graduatedProfileClass()
        {
            if(_graduatedProfileClass == null )
                _graduatedProfileClass = new PropertyPath<Boolean>(RequestedEnrollmentDirectionGen.P_GRADUATED_PROFILE_CLASS, this);
            return _graduatedProfileClass;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(RequestedEnrollmentDirectionGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @return Согласие на зачисление. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#isAgree4Enrollment()
     */
        public PropertyPath<Boolean> agree4Enrollment()
        {
            if(_agree4Enrollment == null )
                _agree4Enrollment = new PropertyPath<Boolean>(RequestedEnrollmentDirectionGen.P_AGREE4_ENROLLMENT, this);
            return _agree4Enrollment;
        }

    /**
     * @return Организация.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection#getExternalOrgUnit()
     */
        public ExternalOrgUnit.Path<ExternalOrgUnit> externalOrgUnit()
        {
            if(_externalOrgUnit == null )
                _externalOrgUnit = new ExternalOrgUnit.Path<ExternalOrgUnit>(L_EXTERNAL_ORG_UNIT, this);
            return _externalOrgUnit;
        }

        public Class getEntityClass()
        {
            return RequestedEnrollmentDirection.class;
        }

        public String getEntityName()
        {
            return "requestedEnrollmentDirection";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
