/**
 *$Id$
 */
package ru.tandemservice.uniec.base.bo.EcReport.ui.EnrollmentDataCollectList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uniec.base.bo.EcReport.logic.EnrollmentDataCollect.EnrollmentDataCollectReportSearchDSHandler;
import ru.tandemservice.uniec.base.bo.EcReport.ui.EnrollmentDataCollectAdd.EcReportEnrollmentDataCollectAdd;

/**
 * @author Alexander Shaburov
 * @since 14.08.13
 */
public class EcReportEnrollmentDataCollectListUI extends UIPresenter
{
    public void onClickAddReport()
    {
        getActivationBuilder().asRegion(EcReportEnrollmentDataCollectAdd.class)
                .activate();
    }

    public void onClickDelete()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onPrintReport()
    {
        getActivationBuilder().asRegion(IUniComponents.DOWNLOAD_STORABLE_REPORT)
                .parameter("reportId", getListenerParameterAsLong())
                .parameter("extension", "xls")
                .activate();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrollmentDataCollectReportSearchDSHandler.BIND_ENR_CAMP, getSettings().get("enrollmentCampaign"));
    }
}
