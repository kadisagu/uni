/* $Id$ */
package ru.tandemservice.uniec.component.report.SummaryQuotasMarksResults.Pub;

import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;

import ru.tandemservice.uniec.entity.report.SummaryQuotasMarksResultsReport;

/**
 * @author Vasily Zhukov
 * @since 18.08.2011
 */
@State(keys = PublisherActivator.PUBLISHER_ID_KEY, bindings = "report.id")
public class Model
{
    private SummaryQuotasMarksResultsReport _report = new SummaryQuotasMarksResultsReport();

    public SummaryQuotasMarksResultsReport getReport()
    {
        return _report;
    }

    public void setReport(SummaryQuotasMarksResultsReport report)
    {
        _report = report;
    }
}
