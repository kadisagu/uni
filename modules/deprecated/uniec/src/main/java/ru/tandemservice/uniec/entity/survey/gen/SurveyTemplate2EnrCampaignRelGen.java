package ru.tandemservice.uniec.entity.survey.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.survey.base.entity.SurveyTemplate;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.survey.SurveyTemplate2EnrCampaignRel;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь шаблона анкеты с Приемной кампанией
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SurveyTemplate2EnrCampaignRelGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.survey.SurveyTemplate2EnrCampaignRel";
    public static final String ENTITY_NAME = "surveyTemplate2EnrCampaignRel";
    public static final int VERSION_HASH = 1650311955;
    private static IEntityMeta ENTITY_META;

    public static final String L_TEMPLATE = "template";
    public static final String L_CAMPAIGN = "campaign";

    private SurveyTemplate _template;     // Шаблон анкеты
    private EnrollmentCampaign _campaign;     // Приемная кампания

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Шаблон анкеты. Свойство не может быть null.
     */
    @NotNull
    public SurveyTemplate getTemplate()
    {
        return _template;
    }

    /**
     * @param template Шаблон анкеты. Свойство не может быть null.
     */
    public void setTemplate(SurveyTemplate template)
    {
        dirty(_template, template);
        _template = template;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EnrollmentCampaign getCampaign()
    {
        return _campaign;
    }

    /**
     * @param campaign Приемная кампания. Свойство не может быть null и должно быть уникальным.
     */
    public void setCampaign(EnrollmentCampaign campaign)
    {
        dirty(_campaign, campaign);
        _campaign = campaign;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SurveyTemplate2EnrCampaignRelGen)
        {
            setTemplate(((SurveyTemplate2EnrCampaignRel)another).getTemplate());
            setCampaign(((SurveyTemplate2EnrCampaignRel)another).getCampaign());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SurveyTemplate2EnrCampaignRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SurveyTemplate2EnrCampaignRel.class;
        }

        public T newInstance()
        {
            return (T) new SurveyTemplate2EnrCampaignRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "template":
                    return obj.getTemplate();
                case "campaign":
                    return obj.getCampaign();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "template":
                    obj.setTemplate((SurveyTemplate) value);
                    return;
                case "campaign":
                    obj.setCampaign((EnrollmentCampaign) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "template":
                        return true;
                case "campaign":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "template":
                    return true;
                case "campaign":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "template":
                    return SurveyTemplate.class;
                case "campaign":
                    return EnrollmentCampaign.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SurveyTemplate2EnrCampaignRel> _dslPath = new Path<SurveyTemplate2EnrCampaignRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SurveyTemplate2EnrCampaignRel");
    }
            

    /**
     * @return Шаблон анкеты. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.survey.SurveyTemplate2EnrCampaignRel#getTemplate()
     */
    public static SurveyTemplate.Path<SurveyTemplate> template()
    {
        return _dslPath.template();
    }

    /**
     * @return Приемная кампания. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.survey.SurveyTemplate2EnrCampaignRel#getCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> campaign()
    {
        return _dslPath.campaign();
    }

    public static class Path<E extends SurveyTemplate2EnrCampaignRel> extends EntityPath<E>
    {
        private SurveyTemplate.Path<SurveyTemplate> _template;
        private EnrollmentCampaign.Path<EnrollmentCampaign> _campaign;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Шаблон анкеты. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.survey.SurveyTemplate2EnrCampaignRel#getTemplate()
     */
        public SurveyTemplate.Path<SurveyTemplate> template()
        {
            if(_template == null )
                _template = new SurveyTemplate.Path<SurveyTemplate>(L_TEMPLATE, this);
            return _template;
        }

    /**
     * @return Приемная кампания. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.survey.SurveyTemplate2EnrCampaignRel#getCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> campaign()
        {
            if(_campaign == null )
                _campaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_CAMPAIGN, this);
            return _campaign;
        }

        public Class getEntityClass()
        {
            return SurveyTemplate2EnrCampaignRel.class;
        }

        public String getEntityName()
        {
            return "surveyTemplate2EnrCampaignRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
