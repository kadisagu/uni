/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.util;

import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;

/**
 * @author Vasily Zhukov
 * @since 18.07.2011
 */
public interface IEcgRecommendedPreStudentDTO extends IEcgRecommendedDTO
{
    public static final String ENROLLMENT_CONDITIONS = "enrollmentConditions";
    public static final String ORDER_NUMBER = "orderNumber";
    public static final String PRE_ENROLL_VISIBLE = "preEnrollVisible";
    public static final String CHANGE_ORDER_DISABLED = "changeOrderDisabled";
    public static final String CHANGE_ENROLLMENT_CONDITION_DISABLED = "changeEnrollmentConditionDisabled";

    /**
     * @return студент предзачисления
     */
    PreliminaryEnrollmentStudent getPreStudent();

    /**
     * @return условия зачисления
     *         null, если нет предзачисления
     */
    String getEnrollmentConditions();

    /**
     * @return номер приказа
     *         пустая строка, если номер приказа не задан
     *         null, если приказа нет
     */
    String getOrderNumber();

    /**
     * @return true, если можно выполнить предзачисление и еще нет студента предзачисления
     */
    boolean isNoPreStudent();

    /**
     * @return true, если можно выполнить предзачисление (сданы оригиналы)
     */
    boolean isPreEnrollVisible();

    /**
     * @return true, если нельзя изменять тип приказа о зачислении
     */
    boolean isChangeOrderDisabled();

    /**
     * @return true, если нельзя изменять условия предзачисления
     */
    boolean isChangeEnrollmentConditionDisabled();
}
