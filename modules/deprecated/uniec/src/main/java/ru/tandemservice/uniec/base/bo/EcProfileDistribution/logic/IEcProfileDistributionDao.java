/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec.base.bo.EcProfileDistribution.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgEntrantRateRowDTO;
import ru.tandemservice.uniec.base.bo.EcProfileDistribution.util.*;
import ru.tandemservice.uniec.base.entity.ecgp.EcgpDistribution;
import ru.tandemservice.uniec.base.entity.ecgp.EcgpDistributionConfig;
import ru.tandemservice.uniec.base.entity.ecgp.EcgpEntrantRecommended;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.entrant.ProfileEducationOrgUnit;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Vasily Zhukov
 * @since 10.05.2012
 */
public interface IEcProfileDistributionDao extends INeedPersistenceSupport
{
    /**
     * Получает планы приема для указанного распределения
     *
     * @param configDTO конфигурация распределения
     * @return планы приема
     */
    IEcgpQuotaDTO getCurrentQuotaDTO(EcgpConfigDTO configDTO);

    /**
     * Получает кол-во занятых мест для указанного распределения
     *
     * @param distribution распределение
     * @return кол-во занятых мест
     */
    IEcgpQuotaUsedDTO getUsedQuotaDTO(EcgpDistribution distribution);

    /**
     * Получает кол-во свободных мест для указанного распределения
     *
     * @param distribution распределение
     * @return кол-во свободных мест
     */
    IEcgpQuotaFreeDTO getFreeQuotaDTO(EcgpDistribution distribution);

    /**
     * Получает список отсортированных строк-рейтингов абитуриентов
     *
     * @param distribution               распределение
     * @param usedTaKindList             используемые виды целевого приема
     * @param competitionKindPriorityMap приоритеты видов конкурса
     * @param rateRule                   правило выборки строк для рейтинга
     * @return рейтинг абитуриентов в распределении
     */
    List<? extends IEcgEntrantRateRowDTO> getEntrantRateRowList(EcgpDistribution distribution, List<TargetAdmissionKind> usedTaKindList, Map<CompetitionKind, Integer> competitionKindPriorityMap, IEcgEntrantRateRowDTO.Rule rateRule);

    /**
     * Добавляет рекомендованных абитуриентов в распределение
     *
     * @param distribution       распределение
     * @param chosenDirectionIds выбранные профили, по которым нужно рекомендовать абитуриентов к зачислению
     */
    void saveEntrantRecommendedList(EcgpDistribution distribution, Collection<Long> chosenDirectionIds);

    /**
     * Получает список отсортированных рекомендованных к зачислению по профилям в распределении
     *
     * @param distribution распределение
     * @return рекомендованные к зачислению по профилям из распределения
     */
    List<IEcgpRecommendedDTO> getEntrantRecommendedRowList(EcgpDistribution distribution);

    /**
     * Получает описание свободных мест / планов в html формате
     *
     * @param freeDTO информация о местах (можно получить с помощью getFreeQuotaDTO)
     * @return планы по профилям:
     *         id профиля -> план в формате html
     *         0 -> план по направлению приема
     */
    Map<Long, String> getQuotaHTMLDescription(IEcgpQuotaFreeDTO freeDTO);

    /**
     * Проверяет, разрешено ли студенту пред. зачисления находиться в распределении с указанной конфигурацией
     * Должны совпадать виды затрат и категория поступающих в условиях зачисления с видом затрат и категорией поступающего из распределения
     *
     * @param config     конфигурация распределения
     * @param preStudent студент предзачисления
     * @return true, если выбранное направление приема может находиться в этом распределении
     */
    boolean isConfigAllowDirection(EcgpDistributionConfig config, PreliminaryEnrollmentStudent preStudent);

    /**
     * Заполняет основное распределение в автоматическом режиме
     *
     * @param distribution основное распределение, которое требуется заполнить
     */
    void doDistributionFill(EcgpDistribution distribution);

    /**
     * Массово создает распределения по профилям по указанным конфигурациям
     * Для тех распределений где план нулевой, распределение не создается
     *
     * @param configSet конфигурации
     */
    void doBulkMakeDistribution(Set<EcgpConfigDTO> configSet);

    /**
     * Массово заполняет распределения по профилям по указанным конфигурациям
     *
     * @param configSet конфигурации
     */
    void doBulkFillDistribution(Set<EcgpConfigDTO> configSet);

    /**
     * Массово удаляет распределения по профилям по указанным конфигурациям
     *
     * @param configSet конфигурации
     */
    void doBulkDeleteDistribution(Set<EcgpConfigDTO> configSet);

    /**
     * Получает список строк на странице распределений
     *
     * @param enrollmentCampaign           приемная кампания
     * @param compensationType             вид затрат
     * @param secondHighAdmission          true, если распределения на второе высшее
     * @param formativeOrgUnitList         формирующие подр. (фильтр)
     * @param territorialOrgUnitList       территориальные подр. (фильтр)
     * @param educationLevelHighSchoolList направления подготовки (специальности) вуза (фильтр)
     * @param developFormList              формы освоения (фильтр)
     * @param developConditionList         условия освоения (фильтр)
     * @param developTechList              технологии освоения (фильтр)
     * @param developPeriodList            сроки освоения (фильтр)
     * @return DTO распределений
     */
    List<EcgpDistributionDTO> getDistributionDTOList(EnrollmentCampaign enrollmentCampaign,
                                                     CompensationType compensationType,
                                                     boolean secondHighAdmission,
                                                     List<OrgUnit> formativeOrgUnitList,
                                                     List<OrgUnit> territorialOrgUnitList,
                                                     List<EducationLevelsHighSchool> educationLevelHighSchoolList,
                                                     List<DevelopForm> developFormList,
                                                     List<DevelopCondition> developConditionList,
                                                     List<DevelopTech> developTechList,
                                                     List<DevelopPeriod> developPeriodList);

    /**
     * @param configDTO конфигурация распределения
     * @return сохраненное распределение
     */
    EcgpDistribution saveDistribution(EcgpConfigDTO configDTO);

    /**
     * Удаляет распределение по профилям
     *
     * @param distributionId идентификатор основного распределения
     */
    void deleteDistribution(Long distributionId);

    /**
     * Аннулирует рекомендацию к зачислению
     *
     * @param entrantRecommended рекомендованный к зачислению
     */
    void deleteEntrantRecommended(EcgpEntrantRecommended entrantRecommended);

    /**
     * Получает отсортированный список профилей направлений приема
     * <p/>
     *
     * @param configDTO конфигурация распределения
     * @return список профилей направлений приема
     */
    List<ProfileEducationOrgUnit> getDistributionProfileList(EcgpConfigDTO configDTO);
}
