/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantRequestAddEdit;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.tapsupport.component.matrix.CellCoordinates;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import org.tandemframework.tapsupport.component.selection.StaticSelectModel;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.base.bo.EcEntrant.EcEntrantManager;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.MethodDeliveryNReturnDocs;
import ru.tandemservice.uniec.entity.catalog.ProfileKnowledge;
import ru.tandemservice.uniec.entity.catalog.codes.MethodDeliveryNReturnDocsCodes;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.examset.ExamSet;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.entity.settings.EnrollmentCompetitionKind;
import ru.tandemservice.uniec.entity.settings.Qualification2CompetitionKindRelation;
import ru.tandemservice.uniec.entity.settings.UseExternalOrgUnitForTA;
import ru.tandemservice.uniec.ui.EnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantRequestAddEditUtil;
import ru.tandemservice.uniec.util.ExamSetUtil;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Боба
 * @since 08.08.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    @SuppressWarnings("unchecked")
    public void prepare(final Model model)
    {
        if (model.getEntrantRequest().getId() != null)
        {
            model.setAddForm(false);
            model.setEntrantRequest(getNotNull(EntrantRequest.class, model.getEntrantRequest().getId()));
            List<RequestedEnrollmentDirection> list = getList(RequestedEnrollmentDirection.class, RequestedEnrollmentDirection.L_ENTRANT_REQUEST, model.getEntrantRequest(), RequestedEnrollmentDirection.P_PRIORITY);
            model.setSelectedRequestedEnrollmentDirectionList(list);
            model.setForDelete(new ArrayList<RequestedEnrollmentDirection>());
            model.setExistedRequestedEnrollmentDirectionIds(new HashSet<Long>());
            for (RequestedEnrollmentDirection item : list)
                model.getExistedRequestedEnrollmentDirectionIds().add(item.getId());
            model.setRegNumber(model.getEntrantRequest().getRegNumber());
            if (model.getEnrollmentCampaign().isNumberOnDirectionManual())
            {
                model.setDirectionNumber(list.get(0).getNumber());
                model.setDirectionNumberVisible(true);
            }
        } else
        {
            model.setAddForm(true);
            model.getEntrantRequest().setRegDate(new Date());
            model.getEntrantRequest().setEntrant(getNotNull(Entrant.class, model.getEntrantRequest().getEntrant().getId()));
            if (model.getEnrollmentCampaign().isNumberOnDirectionManual())
                model.setDirectionNumberVisible(true);
            model.setSelectedRequestedEnrollmentDirectionList(new ArrayList<RequestedEnrollmentDirection>());
            model.setForDelete(new ArrayList<RequestedEnrollmentDirection>());
            model.setExistedRequestedEnrollmentDirectionIds(new HashSet<Long>());
            MethodDeliveryNReturnDocs method = getCatalogItem(MethodDeliveryNReturnDocs.class, MethodDeliveryNReturnDocsCodes.PERSONALLY);
            model.getEntrantRequest().setMethodReturnDocs(method);
            model.getEntrantRequest().setMethodDeliveryDocs(method);
        }

        EnrollmentCampaign enrollmentCampaign = model.getEnrollmentCampaign();

        model.setMethodDeliveryNReturnDocsSelectModel(new StaticSelectModel("id", "title", getCatalogItemList(MethodDeliveryNReturnDocs.class)));

        model.setEditForm(!model.isAddForm());
        model.setStateExamRestriction(enrollmentCampaign.isStateExamRestriction());
        model.setOneDirectionPerRequest(false); // сейчас такого ограничения не существует
        model.setTerritorialOrgUnit(TopOrgUnit.getInstance());

        model.setFormativeOrgUnitModel(EnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(model));
        model.setTerritorialOrgUnitModel(EnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(model));
        model.setEducationLevelsHighSchoolModel(EnrollmentDirectionUtil.createEducationLevelsHighSchoolModel(model));
        model.setDevelopFormModel(EnrollmentDirectionUtil.createDevelopFormModel(model));
        model.setDevelopConditionModel(EnrollmentDirectionUtil.createDevelopConditionModel(model));
        model.setDevelopTechModel(EnrollmentDirectionUtil.createDevelopTechModel(model));
        model.setDevelopPeriodModel(EnrollmentDirectionUtil.createDevelopPeriodModel(model));

        model.setCompensationTypeModel(EnrollmentDirectionUtil.createCompensationTypeModel(model));
        model.setUseExternalOrgUnitForTA(get(UseExternalOrgUnitForTA.class, UseExternalOrgUnitForTA.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
        model.setExternalOrgUnitModel(new LazySimpleSelectModel<>(ExternalOrgUnit.class, ExternalOrgUnit.P_TITLE_WITH_LEGAL_FORM));
        model.setTargetAdmissionKindList(UniecDAOFacade.getTargetAdmissionDao().getTargetAdmissionKindOptionList(model.getEnrollmentCampaign()));

        model.setProfileKnowledgeListModel(new LazySimpleSelectModel<>(ProfileKnowledge.class));
        List<StudentCategory> studentCategories = getCatalogItemListOrderByCode(StudentCategory.class);
        model.setStudentCategoryList(studentCategories);
        if(!studentCategories.isEmpty())
            model.getRequestedEnrollmentDirection().setStudentCategory(studentCategories.get(0));
        model.setCompetitionKindModel(getCompetitionKindModel(model));

        // надпись на форме про сданные оригиналы документов
        {
            Criteria c = getSession().createCriteria(RequestedEnrollmentDirection.class, "r");
            c.createAlias("r." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "req");
            c.add(Restrictions.eq("req." + EntrantRequest.L_ENTRANT, model.getEntrantRequest().getEntrant()));
            c.add(Restrictions.eq("r." + RequestedEnrollmentDirection.P_ORIGINAL_DOCUMENT_HANDED_IN, Boolean.TRUE));
            List<RequestedEnrollmentDirection> list = c.list();
            for (RequestedEnrollmentDirection oldDirection : list)
            {
                // TODO: нужно использовать EntrantOriginalDocumentRelation
                model.setOriginalHandedIn(true);
                StructureEducationLevels levelType = oldDirection.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType();
                model.setOriginalPlace(levelType.getDativeCaseShortTitle() + " " + oldDirection.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getDisplayableTitle() + " (заявление №" + oldDirection.getEntrantRequest().getRegNumber() + ")");
            }
        }
    }

    @Override
    public void prepareSelectedEnrollmentDirectionList(Model model, ErrorCollector errorCollector)
    {
        EntrantRequest entrantRequest = model.getEntrantRequest();

        // получаем список направлений приема, которые надо добавить в заявление.
        // выходим, если есть ошибка
        List<MultiKey> enrollmentDirectionList = getAddingEnrollmentDirections(model, errorCollector);
        if (errorCollector.hasErrors())
            return;

        List<RequestedEnrollmentDirection> newDirections = new ArrayList<>();

        // вычисляем минимальный id (у вновь добавленных будут отрицательные id)
        long id = 0L;
        List<RequestedEnrollmentDirection> alreadySelectedDirections = model.getSelectedRequestedEnrollmentDirectionList();
        for (RequestedEnrollmentDirection item : alreadySelectedDirections)
        {
            id = Math.min(id, item.getId());
        }

        for (MultiKey key : enrollmentDirectionList)
        {
            id--;

            // заполняем объект значениями по умолчанию
            final RequestedEnrollmentDirection newDirection = new RequestedEnrollmentDirection();
            newDirection.update(model.getRequestedEnrollmentDirection());

            newDirection.setEnrollmentDirection((EnrollmentDirection) key.getKey(0));
            newDirection.setCompensationType((CompensationType) key.getKey(1));
            newDirection.setEntrantRequest(entrantRequest);
            newDirection.setRegDate(new Date());
            newDirection.setOriginalDocumentHandedIn(false);

            // сохраняем в модели оценки по профильным дисциплинам
            List<ProfileExaminationMark> profileMarkList = new ArrayList<>();
            for (Map.Entry<CellCoordinates, Integer> entry : model.getMatrixData().entrySet())
            {
                if (entry.getValue() != null)
                {
                    Discipline2RealizationWayRelation discipline = getNotNull(Discipline2RealizationWayRelation.class, entry.getKey().getRowId());
                    ProfileExaminationMark profileMark = new ProfileExaminationMark();
                    profileMark.setMark(entry.getValue());
                    profileMark.setDiscipline(discipline);
                    profileMark.setEntrant(entrantRequest.getEntrant());
                    profileMarkList.add(profileMark);
                }
            }

            // этот id нужен, чтобы уметь отображать объект в SearchList
            newDirection.setId(id);

            model.getProfileMarkMap().put(id, profileMarkList);
            if (model.getProfileKnowledgeList() != null)
                model.getProfileKnowledgeMap().put(id, model.getProfileKnowledgeList());

            newDirections.add(newDirection);
        }

        // надпись на форме про сданные оригиналы документов
        if (model.getRequestedEnrollmentDirection().isOriginalDocumentHandedIn())
        {
            // оригиналы сданы
            model.setOriginalHandedIn(true);

            // снимаем оригиналы у всех уже выбранных направлений
            for (RequestedEnrollmentDirection selectedDirection : alreadySelectedDirections)
                selectedDirection.setOriginalDocumentHandedIn(false);

            // будет считаться, что оригиналы сданы по этому направлению
            RequestedEnrollmentDirection direction;

            // распределяем направления по видам затрат
            Map<String, List<RequestedEnrollmentDirection>> map = new HashMap<>();
            for (RequestedEnrollmentDirection item : newDirections)
            {
                List<RequestedEnrollmentDirection> list = map.get(item.getCompensationType().getCode());
                if (list == null)
                    map.put(item.getCompensationType().getCode(), list = new ArrayList<>());
                list.add(item);
            }

            // Если
            // 1. оригиналы документов сдаются на направление подготовки (специальность)
            // 2. среди выбранных есть направления на разные виды затрат
            // то оригиналы должны сдаваться только по направлениям на бюджет

            if (model.getEnrollmentCampaign().isOriginalDocumentPerDirection() && map.size() > 1)
            {
                List<RequestedEnrollmentDirection> list = map.get(UniDefines.COMPENSATION_TYPE_BUDGET);
                for (RequestedEnrollmentDirection item : list)
                    item.setOriginalDocumentHandedIn(true);

                direction = list.get(list.size() - 1);
            } else
            {
                for (RequestedEnrollmentDirection item : newDirections)
                    item.setOriginalDocumentHandedIn(true);

                direction = newDirections.get(newDirections.size() - 1);
            }

            StructureEducationLevels levelType = direction.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType();
            model.setOriginalPlace(levelType.getDativeCaseShortTitle() + " " + direction.getEnrollmentDirection().getShortTitle());
        }

        // добавляем в уже выбранные
        List<RequestedEnrollmentDirection> mergedDirections = new ArrayList<>(alreadySelectedDirections);
        EntrantRequestAddEditUtil.mergeSelectedDirections(mergedDirections, newDirections);

        // проверяем, можно ли добавлять такие направления
        if (!EntrantRequestAddEditUtil.validateRequestedDirections(getSession(), mergedDirections, model.getForDelete(), model.getEntrantRequest()))
            return;

        // если проверка успешная, то обновляем список выбранных
        model.setSelectedRequestedEnrollmentDirectionList(mergedDirections);

        // не сбрасываем выбранные на форме параметры только если мы на форме добавления и можно выбрать только одно направление
        if (!(model.isAddForm() && model.isOneDirectionPerRequest()))
            model.setRequestedEnrollmentDirection(new RequestedEnrollmentDirection());
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean prepareProfileDisciplineList(Model model)
    {
        // если нельзя установить направление приема по параметрам формы, то не показываем список профильных дисциплин
        EnrollmentDirection enrollmentDirection = EnrollmentDirectionUtil.getEnrollmentDirection(model, getSession());
        if (enrollmentDirection == null) return false;
        StudentCategory studentCategory = model.getRequestedEnrollmentDirection().getStudentCategory();
        if (studentCategory == null) return false;
        ExamSet examSet = ExamSetUtil.getExamSet(enrollmentDirection, studentCategory);
        if (examSet == null) return false;

        List<Discipline2RealizationWayRelation> profileDisciplineList = examSet.getProfileDisciplineList();
        model.setProfileDisciplineList(profileDisciplineList);

        // заполняем в матрице оценки по профильным предметам, если они уже есть
        Criteria c = getSession().createCriteria(ProfileExaminationMark.class);
        c.add(Restrictions.eq(ProfileExaminationMark.L_ENTRANT, model.getEntrantRequest().getEntrant()));

        Map<CellCoordinates, Integer> matrixData = model.getMatrixData();
        for (ProfileExaminationMark mark : (List<ProfileExaminationMark>) c.list())
            if (profileDisciplineList.contains(mark.getDiscipline()))
                matrixData.put(new CellCoordinates(mark.getDiscipline().getId(), Model.MARK_COLUMN_ID), mark.getMark());

        return !profileDisciplineList.isEmpty();
    }

    @Override
    public void update(Model model)
    {
        EcEntrantManager.instance().dao().saveOrUpdateEntrantRequest(
                model.getSelectedRequestedEnrollmentDirectionList(),
                model.getForDelete(),
                model.isAddForm(),
                model.getEntrantRequest(),
                model.getRegNumber(),
                model.getDirectionNumber(),
                model.getProfileKnowledgeMap(),
                model.getProfileMarkMap());
    }

    @Override
    public int getMaxRequestedEnrollmentDirectionNumber(Model model)
    {
        EnrollmentDirection enrollmentDirection = EnrollmentDirectionUtil.getEnrollmentDirection(model, getSession());
        return ru.tandemservice.uniec.component.entrant.RequestedEnrollmentDirectionAddEdit.DAO.getMaxRegNumber(enrollmentDirection, getSession()) + 1;
    }

    @Override
    public void reset(Model model)
    {
        model.setFormativeOrgUnit(null);
        model.setTerritorialOrgUnit(TopOrgUnit.getInstance());
        model.setEducationLevelsHighSchool(null);
        model.setDevelopForm(null);
        model.setDevelopTech(null);
        model.setDevelopCondition(null);
        model.setDevelopPeriod(null);
        model.setMatrixData(new HashMap<CellCoordinates, Integer>());
        model.setProfileKnowledgeList(null);

        model.setRequestedEnrollmentDirection(new RequestedEnrollmentDirection());
    }

    private List<MultiKey> getAddingEnrollmentDirections(Model model, ErrorCollector errorCollector)
    {
        EnrollmentDirection direction = EnrollmentDirectionUtil.getEnrollmentDirection(model, getSession());
        List<EnrollmentDirection> result;

        // в силу кода выбора параметров на форме с помощью EnrollmentDirectionUtil (см метод prepare), такой ошибки быть не должно
        // однако надо проверить, чтобы не было NPE
        if (direction == null)
        {
            errorCollector.add("Направление приема с указанными параметрами не найдено.");
            return null;
        }

        // если
        // 1. используются конкурсные группы
        // 2. выбранное направление приема находится в одной из них
        // 3. выбрана категория обучающегося "Студент" или "Слушатель"
        // 4. выбрано условие освоения "Полный срок",
        //    то добавляем все направления приема с полным сроком из этой конкурсной группы
        String studentCategoryCode = model.getRequestedEnrollmentDirection().getStudentCategory().getCode();
        boolean studentOrListener = UniDefines.STUDENT_CATEGORY_STUDENT.equals(studentCategoryCode) || UniDefines.STUDENT_CATEGORY_LISTENER.equals(studentCategoryCode);
        boolean fullTime = UniDefines.DEVELOP_CONDITION_FULL_TIME.equals(direction.getEducationOrgUnit().getDevelopCondition().getCode());

        if (model.getEnrollmentCampaign().isUseCompetitionGroup() && direction.getCompetitionGroup() != null && studentOrListener && fullTime)
        {
            result = new MQBuilder(EnrollmentDirection.ENTITY_CLASS, "d")
                    .add(MQExpression.eq("d", EnrollmentDirection.L_COMPETITION_GROUP, direction.getCompetitionGroup()))
                    .add(MQExpression.eq("d", EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_CONDITION + "." + DevelopCondition.P_CODE, UniDefines.DEVELOP_CONDITION_FULL_TIME))
                    .getResultList(getSession());
            Collections.sort(result, ITitled.TITLED_COMPARATOR);
        } else
        {
            result = Arrays.asList(direction);
        }

        List<MultiKey> selectableDirections = EntrantRequestAddEditUtil.getAddingEnrollmentDirections(getSession(),
                result,
                model.isContractAutoCompetition() ? null : model.getRequestedEnrollmentDirection().getCompensationType(),
                model.getSelectedRequestedEnrollmentDirectionList(),
                model.getEnrollmentCampaign().isDirectionPerCompTypeDiff(),
                model.isStateExamRestriction(),
                model.getEntrantRequest().getEntrant(),
                model.getForDelete()
        );

        if (selectableDirections.isEmpty())
        {
            if (model.isStateExamRestriction())
                errorCollector.add("Нельзя добавить направление подготовки (специальность) абитуриенту, возможно оно уже выбрано или не покрывается свидетельством ЕГЭ.");
            else
                errorCollector.add("Нельзя добавить направление подготовки (специальность) абитуриенту, возможно оно уже выбрано.");
            return null;
        }

        return selectableDirections;
    }

    protected ISelectModel getCompetitionKindModel(Model model)
    {
        return new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                EnrollmentCampaign campaign = model.getEntrantRequest().getEntrant().getEnrollmentCampaign();
                Qualifications qualification = model.getEducationLevelsHighSchool() != null ?
                        model.getEducationLevelsHighSchool().getEducationLevel().getQualification() : null;
                if (qualification == null)
                    qualification = campaign.getDefaultQualification();
                StudentCategory studentCategory = model.getRequestedEnrollmentDirection().getStudentCategory();

                if (qualification == null || studentCategory == null)
                    return ListResult.getEmpty();

                String q2cAlias = "q2c";
                String eckAlias = "eck";
                String ckAlias = "ck";
                List<CompetitionKind> competitionKinds = getList(
                        new DQLSelectBuilder()
                                .fromEntity(CompetitionKind.class, ckAlias)
                                .column(property(ckAlias))

                                .where(existsByExpr(Qualification2CompetitionKindRelation.class, q2cAlias, and(
                                        eq(property(q2cAlias, Qualification2CompetitionKindRelation.competitionKind()), property(ckAlias)),
                                        eq(property(q2cAlias, Qualification2CompetitionKindRelation.enrollmentCampaign()), value(campaign)),
                                        eq(property(q2cAlias, Qualification2CompetitionKindRelation.qualification()), value(qualification)),
                                        eq(property(q2cAlias, Qualification2CompetitionKindRelation.studentCategory()),
                                           value(studentCategory))
                                )))

                                .joinEntity(ckAlias, DQLJoinType.inner, EnrollmentCompetitionKind.class, eckAlias,
                                            and(eq(property(eckAlias, EnrollmentCompetitionKind.competitionKind()), property(ckAlias)),
                                                eq(property(eckAlias, EnrollmentCompetitionKind.enrollmentCampaign()), value(campaign))))
                                .order(property(eckAlias, EnrollmentCompetitionKind.priority()))
                );

                EnrollmentDirection enrollmentDirection = EnrollmentDirectionUtil.getEnrollmentDirection(model, getSession());
                if (enrollmentDirection != null && !enrollmentDirection.isInterview())
                    competitionKinds.remove(getCatalogItem(CompetitionKind.class, UniecDefines.COMPETITION_KIND_INTERVIEW));

                return new ListResult<>(competitionKinds, competitionKinds.size());
            }
        };
    }
}
