/* $Id: ReportSubRow.java 9173 2009-07-15 16:29:03Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.stateExamEnrollmentResults2010.StateExamEnrollmentResultsAdd;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.ITitled;

/**
 * @author vip_delete
 * @since 28.04.2009
 */
class ReportSubRow implements ITitled
{
    private String _title;
    private String _titleWithoutCode;
    private String _code;
    private List<ReportSubSubRow> _subSubRowList;

    public ReportSubRow(String titleWithCode, String code, List<ReportSubSubRow> subSubRowList)
    {
        _title = titleWithCode;
        _titleWithoutCode = StringUtils.substringAfter(titleWithCode, code + " ");
        _code = code;
        _subSubRowList = subSubRowList;
    }

    // Getters

    @Override
    public String getTitle()
    {
        return _title;
    }

    public String getCode()
    {
        return _code;
    }

    public String getTitleWithoutCode()
    {
        return _titleWithoutCode;
    }

    public List<ReportSubSubRow> getSubSubRowList()
    {
        return _subSubRowList;
    }
}
