package ru.tandemservice.uniec.entity.orders.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.orders.AbstractEntrantParagraph;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentParagraph;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Параграф приказа о зачислении абитуриентов
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrollmentParagraphGen extends AbstractEntrantParagraph
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.orders.EnrollmentParagraph";
    public static final String ENTITY_NAME = "enrollmentParagraph";
    public static final int VERSION_HASH = -1932229430;
    private static IEntityMeta ENTITY_META;

    public static final String L_GROUP_MANAGER = "groupManager";
    public static final String P_ENROLL_PARAGRAPH_PER_EDU_ORG_UNIT = "enrollParagraphPerEduOrgUnit";

    private EnrollmentExtract _groupManager;     // Староста
    private boolean _enrollParagraphPerEduOrgUnit;     // Параграф приказа о зачислении формируется на направление подготовки (специальность)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Староста.
     */
    public EnrollmentExtract getGroupManager()
    {
        return _groupManager;
    }

    /**
     * @param groupManager Староста.
     */
    public void setGroupManager(EnrollmentExtract groupManager)
    {
        dirty(_groupManager, groupManager);
        _groupManager = groupManager;
    }

    /**
     * @return Параграф приказа о зачислении формируется на направление подготовки (специальность). Свойство не может быть null.
     */
    @NotNull
    public boolean isEnrollParagraphPerEduOrgUnit()
    {
        return _enrollParagraphPerEduOrgUnit;
    }

    /**
     * @param enrollParagraphPerEduOrgUnit Параграф приказа о зачислении формируется на направление подготовки (специальность). Свойство не может быть null.
     */
    public void setEnrollParagraphPerEduOrgUnit(boolean enrollParagraphPerEduOrgUnit)
    {
        dirty(_enrollParagraphPerEduOrgUnit, enrollParagraphPerEduOrgUnit);
        _enrollParagraphPerEduOrgUnit = enrollParagraphPerEduOrgUnit;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrollmentParagraphGen)
        {
            setGroupManager(((EnrollmentParagraph)another).getGroupManager());
            setEnrollParagraphPerEduOrgUnit(((EnrollmentParagraph)another).isEnrollParagraphPerEduOrgUnit());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrollmentParagraphGen> extends AbstractEntrantParagraph.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrollmentParagraph.class;
        }

        public T newInstance()
        {
            return (T) new EnrollmentParagraph();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "groupManager":
                    return obj.getGroupManager();
                case "enrollParagraphPerEduOrgUnit":
                    return obj.isEnrollParagraphPerEduOrgUnit();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "groupManager":
                    obj.setGroupManager((EnrollmentExtract) value);
                    return;
                case "enrollParagraphPerEduOrgUnit":
                    obj.setEnrollParagraphPerEduOrgUnit((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "groupManager":
                        return true;
                case "enrollParagraphPerEduOrgUnit":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "groupManager":
                    return true;
                case "enrollParagraphPerEduOrgUnit":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "groupManager":
                    return EnrollmentExtract.class;
                case "enrollParagraphPerEduOrgUnit":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrollmentParagraph> _dslPath = new Path<EnrollmentParagraph>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrollmentParagraph");
    }
            

    /**
     * @return Староста.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentParagraph#getGroupManager()
     */
    public static EnrollmentExtract.Path<EnrollmentExtract> groupManager()
    {
        return _dslPath.groupManager();
    }

    /**
     * @return Параграф приказа о зачислении формируется на направление подготовки (специальность). Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentParagraph#isEnrollParagraphPerEduOrgUnit()
     */
    public static PropertyPath<Boolean> enrollParagraphPerEduOrgUnit()
    {
        return _dslPath.enrollParagraphPerEduOrgUnit();
    }

    public static class Path<E extends EnrollmentParagraph> extends AbstractEntrantParagraph.Path<E>
    {
        private EnrollmentExtract.Path<EnrollmentExtract> _groupManager;
        private PropertyPath<Boolean> _enrollParagraphPerEduOrgUnit;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Староста.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentParagraph#getGroupManager()
     */
        public EnrollmentExtract.Path<EnrollmentExtract> groupManager()
        {
            if(_groupManager == null )
                _groupManager = new EnrollmentExtract.Path<EnrollmentExtract>(L_GROUP_MANAGER, this);
            return _groupManager;
        }

    /**
     * @return Параграф приказа о зачислении формируется на направление подготовки (специальность). Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentParagraph#isEnrollParagraphPerEduOrgUnit()
     */
        public PropertyPath<Boolean> enrollParagraphPerEduOrgUnit()
        {
            if(_enrollParagraphPerEduOrgUnit == null )
                _enrollParagraphPerEduOrgUnit = new PropertyPath<Boolean>(EnrollmentParagraphGen.P_ENROLL_PARAGRAPH_PER_EDU_ORG_UNIT, this);
            return _enrollParagraphPerEduOrgUnit;
        }

        public Class getEntityClass()
        {
            return EnrollmentParagraph.class;
        }

        public String getEntityName()
        {
            return "enrollmentParagraph";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
