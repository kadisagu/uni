package ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.organization.base.entity.gen.OrgUnitGen;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.education.gen.EducationOrgUnitGen;
import ru.tandemservice.uniec.dao.ecg.IEnrollmentCompetitionGroupDAO;
import ru.tandemservice.uniec.entity.ecg.EcgDistribObject;
import ru.tandemservice.uniec.entity.ecg.EcgDistribQuota;
import ru.tandemservice.uniec.entity.ecg.gen.EcgDistribQuotaGen;
import ru.tandemservice.uniec.entity.entrant.gen.EnrollmentDirectionGen;

import java.util.List;
import java.util.Map;

/**
 * @author vdanilov
 */
public class DAO extends UniBaseDao implements IDAO {

	@Override
	public void prepare(final Model model) {
		model.setDistrib(this.get(EcgDistribObject.class, model.getDistrib().getId()));
	}

	@Override
	public void doApprove(final Model model) {
		IEnrollmentCompetitionGroupDAO.INSTANCE.get().doApproveDistribution(model.getDistrib());
	}

	@SuppressWarnings("unchecked")
	@Override
	public void prepareQuotaListDataSource(final Model model) {
		final MQBuilder builder = new MQBuilder(EcgDistribQuotaGen.ENTITY_CLASS, "q");
		builder.add(MQExpression.eq("q", EcgDistribQuotaGen.L_DISTRIBUTION, model.getDistrib()));
		builder.addOrder("q", EcgDistribQuotaGen.L_DIRECTION+"."+EnrollmentDirectionGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnitGen.L_EDUCATION_LEVEL_HIGH_SCHOOL+".title");
		builder.addOrder("q", EcgDistribQuotaGen.L_DIRECTION+"."+EnrollmentDirectionGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnitGen.L_EDUCATION_LEVEL_HIGH_SCHOOL+".id");
		builder.addOrder("q", EcgDistribQuotaGen.L_DIRECTION+"."+EnrollmentDirectionGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnitGen.L_FORMATIVE_ORG_UNIT+"."+ OrgUnitGen.P_SHORT_TITLE);
		builder.addOrder("q", EcgDistribQuotaGen.L_DIRECTION+"."+EnrollmentDirectionGen.L_EDUCATION_ORG_UNIT+".id");

		final Map<Long, Integer> quotaMap = IEnrollmentCompetitionGroupDAO.INSTANCE.get().getDir2QuotaMap(model.getDistrib());
		final List list = builder.getResultList(this.getSession());
		CollectionUtils.transform(list, input -> {
            final EcgDistribQuota quota = (EcgDistribQuota)input;
            return new Model.Wrapper(quota, quotaMap.get(quota.getDirection().getId()));
        });

		/* итог */ {
			int r = 0, q = 0;
			for (final Model.Wrapper w: (List<Model.Wrapper>)list) {
				r += Math.max(0, w.getRealQuota());
				q += Math.max(0, w.getQuota());
			}

			list.add(new Model.Wrapper(r, q));
		}

		final DynamicListDataSource<Model.Wrapper> ds = model.getQuotaDataSource();
		ds.setCountRow(list.size());
		ds.setTotalSize(list.size());
		ds.createPage(list);
	}

}
