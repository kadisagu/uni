/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.wizard.IdentityCardStep;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;

import org.tandemframework.shared.person.base.entity.IdentityCard;

/**
 * @author vip_delete
 * @since 14.06.2009
 */
@Input( { 
    @Bind(key = "basisPersonId", binding = "entrantModel.basisPersonId"), 
    @Bind(key = "onlineEntrantId", binding = "entrantModel.onlineEntrant.id"),
    @Bind(key = "enrollmentCampaignId", binding = "entrantModel.enrollmentCampaignId"),
    @Bind(key = "entrantMasterPermKey", binding = "entrantMasterPermKey"),
    @Bind(key = "identityCard")
})
public class Model
{
    private String _entrantMasterPermKey;
    private ru.tandemservice.uniec.component.entrant.EntrantAdd.Model _entrantModel = new ru.tandemservice.uniec.component.entrant.EntrantAdd.Model();
    private org.tandemframework.shared.person.base.bo.Person.ui.AddressEdit.Model _addressModel = new org.tandemframework.shared.person.base.bo.Person.ui.AddressEdit.Model();

    private IdentityCard _identityCard;

    public ru.tandemservice.uniec.component.entrant.EntrantAdd.Model getEntrantModel()
    {
        return _entrantModel;
    }

    public void setEntrantModel(ru.tandemservice.uniec.component.entrant.EntrantAdd.Model entrantModel)
    {
        _entrantModel = entrantModel;
    }

    public org.tandemframework.shared.person.base.bo.Person.ui.AddressEdit.Model getAddressModel()
    {
        return _addressModel;
    }

    public void setAddressModel(org.tandemframework.shared.person.base.bo.Person.ui.AddressEdit.Model addressModel)
    {
        _addressModel = addressModel;
    }

    public IdentityCard getIdentityCard()
    {
        return _identityCard;
    }

    public void setIdentityCard(IdentityCard identityCard)
    {
        _identityCard = identityCard;
    }

    public String getEntrantMasterPermKey()
    {
        if(StringUtils.isEmpty(_entrantMasterPermKey)) return "addEntrantMaster";
        return _entrantMasterPermKey;
    }

    public void setEntrantMasterPermKey(String entrantMasterPermKey)
    {
        _entrantMasterPermKey = entrantMasterPermKey;
    }
}
