/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.settings.PassDisciplineDates;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.entrant.EntranceExamPhase;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author vip_delete
 * @since 08.06.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.setSettings(component.getSettings());

        prepareListDataSource(component);

        getDao().prepare(model);
    }

    @SuppressWarnings({"unchecked"})
    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Название", Discipline2RealizationWayRelation.P_TITLE).setOrderable(false).setClickable(false));
        dataSource.addColumn(new BlockColumn("passDates", "Дата сдачи и ФИО экзаменаторов"));

        BlockColumn action = new BlockColumn("action", "");
        action.setBlockName("actionBlock");
        action.setWidth("45px");
        dataSource.addColumn(action);
        model.setDataSource(dataSource);
    }

    public void onClickApply(IBusinessComponent component)
    {
        getDao().update(getModel(component));
    }

    public void onClickSave(IBusinessComponent component)
    {
        onClickApply(component);
        deactivate(component);
    }


    public void onChangeParameters(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getSubjectPassForm() != null && model.getCompensationType() != null && model.getDevelopForm() != null)
        {
            getDao().prepareDates(model);
            component.saveSettings();
        }
    }


    public void onClickEditDates(IBusinessComponent component)
    {
        Long id = component.getListenerParameter();
        Model model = component.getModel();
        model.setInEditMode(id);
        List<EntranceExamPhase> phases = model.getPhaseByDiscipline().computeIfAbsent(id, k -> new ArrayList<>());
        if (phases.isEmpty()) phases.add(new EntranceExamPhase());
    }

    public void onClickSaveDates(IBusinessComponent component)
    {
        Long id = component.getListenerParameter();
        Model model = component.getModel();
        getDao().update(id, model);
        model.setInEditMode(null);
    }

    public void onClickCancelEditDates(IBusinessComponent component)
    {
        Long id = component.getListenerParameter();
        Model model = component.getModel();
        getDao().refreshPhases(id, model);
        model.setInEditMode(null);
    }

    public void onClickAddPassDate(IBusinessComponent component)
    {
        Long id = component.getListenerParameter();
        Model model = component.getModel();
        List<EntranceExamPhase> phases = model.getPhaseByDiscipline().computeIfAbsent(id, k -> new ArrayList<>());
        phases.add(new EntranceExamPhase());
    }

    public void onClickDeleteDate(IBusinessComponent component)
    {
        CoreCollectionUtils.Pair<Long, EntranceExamPhase> parameters = component.getListenerParameter();
        Long discipline = parameters.getX();
        EntranceExamPhase phase = parameters.getY();

        Model model = component.getModel();
        Map<Long, List<EntranceExamPhase>> phaseByDiscipline = model.getPhaseByDiscipline();
        List<EntranceExamPhase> phases = phaseByDiscipline.get(discipline);
        if (CollectionUtils.isEmpty(phases)) return;

        phases.remove(phase);

        if (phase.getId() != null) model.addPhase4Delete(phase);
    }

    @Override
    public void deactivate(IBusinessComponent component)
    {
        component.saveSettings();
        super.deactivate(component);
    }
}
