/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec.component.entrant.ProfileEducationOrgUnitEdit;

import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.PriorityProfileEduOu;
import ru.tandemservice.uniec.entity.entrant.ProfileEducationOrgUnit;

import java.util.*;

/**
 * @author Vasily Zhukov
 * @since 25.04.2012
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        EnrollmentDirection enrollmentDirection = getNotNull(EnrollmentDirection.class, model.getEnrollmentDirectionId());

        // быстро загружаем весь граф НПМин
        List<Object[]> list = new DQLSelectBuilder().fromEntity(EducationLevels.class, "e")
                .column(DQLExpressions.property(EducationLevels.id().fromAlias("e")))
                .column(DQLExpressions.property(EducationLevels.parentLevel().id().fromAlias("e")))
                .where(DQLExpressions.isNotNull(DQLExpressions.property(EducationLevels.parentLevel().fromAlias("e"))))
                .createStatement(getSession()).list();
        Map<Long, List<Long>> id2childIds = new HashMap<Long, List<Long>>(list.size() * 2);
        for (Object[] eduLevel : list)
        {
            Long id = (Long) eduLevel[0];
            Long parentId = (Long) eduLevel[1];
            List<Long> childIds = id2childIds.get(parentId);
            if (childIds == null)
                id2childIds.put(parentId, childIds = new ArrayList<Long>());
            childIds.add(id);
        }

        // берем текущее НПМин находим для него все НПМин, которые могут быть профилями к нему (поиск в ширину)
        EducationLevels eduLevel = enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();

        // НПМин, которые могут быть профилями 
        List<Long> possibleEduLevelList = new ArrayList<Long>();
        possibleEduLevelList.add(eduLevel.getId());

        // запускаем поиск в ширину на графе
        Queue<Long> queue = new ArrayDeque<Long>();
        queue.add(eduLevel.getId());
        while (!queue.isEmpty())
        {
            List<Long> childs = id2childIds.get(queue.poll());
            if (childs != null)
            {
                possibleEduLevelList.addAll(childs);
                queue.addAll(childs);
            }
        }

        // получаем возможные НПП: НПМин которых из возможных и на НПП не создано НП
        Set<EducationOrgUnit> possibleEduOrgUnitSet = new HashSet<EducationOrgUnit>(
                new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "e")
                        .where(DQLExpressions.in(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().educationLevel().id().fromAlias("e")), possibleEduLevelList))
                        .where(DQLExpressions.notIn(DQLExpressions.property(EducationOrgUnit.id().fromAlias("e")), new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, "d")
                                .column(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().id().fromAlias("d")))
                                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.enrollmentCampaign().fromAlias("d")), DQLExpressions.value(enrollmentDirection.getEnrollmentCampaign())))
                                .buildQuery()))
                        .createStatement(getSession()).<EducationOrgUnit>list()
        );

        // загружаем список существующих НПП
        List<ProfileEducationOrgUnit> currentList = new DQLSelectBuilder().fromEntity(ProfileEducationOrgUnit.class, "e")
                .where(DQLExpressions.eq(DQLExpressions.property(ProfileEducationOrgUnit.enrollmentDirection().fromAlias("e")), DQLExpressions.value(enrollmentDirection)))
                .createStatement(getSession()).list();

        // формируем общий список
        List<ProfileEducationOrgUnit> dtoList = new ArrayList<ProfileEducationOrgUnit>();
        Map<Long, Boolean> valueMap = new HashMap<Long, Boolean>();
        model.setValueMap(valueMap);
        for (ProfileEducationOrgUnit item : currentList)
        {
            ProfileEducationOrgUnit dto = new ProfileEducationOrgUnit();
            dto.update(item);
            // удобно в качестве DTO.id использовать ou.id
            // уникальность не нарушится, так как есть уникальный ключ profileEduOrgUnitKey
            // фактически в этом списке мы выбираем именно ou
            dto.setId(item.getEducationOrgUnit().getId());
            valueMap.put(item.getEducationOrgUnit().getId(), Boolean.TRUE);
            possibleEduOrgUnitSet.remove(item.getEducationOrgUnit());
            dtoList.add(dto);
        }

        for (EducationOrgUnit ou : possibleEduOrgUnitSet)
        {
            ProfileEducationOrgUnit dto = new ProfileEducationOrgUnit();
            dto.setEnrollmentDirection(enrollmentDirection);
            dto.setEducationOrgUnit(ou);
            dto.setId(ou.getId());
            dtoList.add(dto);
        }

        Comparator<ProfileEducationOrgUnit> comparator = new EntityComparator<ProfileEducationOrgUnit>(
                new EntityOrder(ProfileEducationOrgUnit.educationOrgUnit().educationLevelHighSchool().fullTitle().s()),
                new EntityOrder(ProfileEducationOrgUnit.educationOrgUnit().developForm().code().s()),
                new EntityOrder(ProfileEducationOrgUnit.educationOrgUnit().developCondition().code().s()),
                new EntityOrder(ProfileEducationOrgUnit.educationOrgUnit().developTech().code().s()),
                new EntityOrder(ProfileEducationOrgUnit.educationOrgUnit().developPeriod().code().s()),
                new EntityOrder(ProfileEducationOrgUnit.educationOrgUnit().id())
        );

        Collections.sort(dtoList, comparator);

        model.setDtoList(dtoList);

        // скроем те dto, которые нельзя выбирать, а это:
        // 1. НПП, на которые есть направления приема в этой приемной кампании
        // 2. НПП, которые уже выбраны в качестве профильных в этой приемной кампании
        List<Long> hiddenIds = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "e")
                .column(DQLExpressions.property(EducationOrgUnit.id().fromAlias("e")))
                .where(DQLExpressions.or(
                        // условие 1
                        DQLExpressions.in(DQLExpressions.property(EducationOrgUnit.id().fromAlias("e")), new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, "d")
                                .column(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().id().fromAlias("d")))
                                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.enrollmentCampaign().fromAlias("d")), DQLExpressions.value(enrollmentDirection.getEnrollmentCampaign())))
                                .buildQuery()
                        ),
                        // условие 2
                        DQLExpressions.in(DQLExpressions.property(EducationOrgUnit.id().fromAlias("e")), new DQLSelectBuilder().fromEntity(ProfileEducationOrgUnit.class, "p")
                                .column(DQLExpressions.property(ProfileEducationOrgUnit.educationOrgUnit().id().fromAlias("p")))
                                .where(DQLExpressions.eq(DQLExpressions.property(ProfileEducationOrgUnit.enrollmentDirection().enrollmentCampaign().fromAlias("p")), DQLExpressions.value(enrollmentDirection.getEnrollmentCampaign())))
                                .buildQuery()
                        )
                ))
                .createStatement(getSession()).list();
        hiddenIds.removeAll(model.getValueMap().keySet()); // конечно, нужно исключить НПП по профилям текущего направления приема

        // задизаблим те dto, по которым абитуриент уже поставил приоритеты
        List<Long> disabledIds = new DQLSelectBuilder().fromEntity(PriorityProfileEduOu.class, "e")
                .column(DQLExpressions.property(PriorityProfileEduOu.profileEducationOrgUnit().educationOrgUnit().id().fromAlias("e")))
                .where(DQLExpressions.eq(DQLExpressions.property(PriorityProfileEduOu.profileEducationOrgUnit().enrollmentDirection().fromAlias("e")), DQLExpressions.value(enrollmentDirection)))
                .createStatement(getSession()).list();

        model.setHiddenIds(new HashSet<Long>(hiddenIds));
        model.setDisabledIds(new HashSet<Long>(disabledIds));
    }

    @Override
    public void update(Model model)
    {
        // загружаем список существующих НПП
        Map<Long, ProfileEducationOrgUnit> curretMap = new HashMap<Long, ProfileEducationOrgUnit>();
        List<ProfileEducationOrgUnit> currentList = new DQLSelectBuilder().fromEntity(ProfileEducationOrgUnit.class, "e")
                .where(DQLExpressions.eq(DQLExpressions.property(ProfileEducationOrgUnit.enrollmentDirection().id().fromAlias("e")), DQLExpressions.value(model.getEnrollmentDirectionId())))
                .createStatement(getSession()).list();
        for (ProfileEducationOrgUnit item : currentList)
            curretMap.put(item.getEducationOrgUnit().getId(), item);

        Map<Long, ProfileEducationOrgUnit> id2dto = new HashMap<Long, ProfileEducationOrgUnit>();
        for (ProfileEducationOrgUnit dto : model.getDtoList())
            id2dto.put(dto.getId(), dto);

        Map<Long, Boolean> valueMap = ((BlockColumn<Boolean>) model.getDataSource().getColumn("checkbox")).getValueMap();
        List<Long> selectedIds = new ArrayList<Long>();
        for (Map.Entry<Long, Boolean> entry : valueMap.entrySet())
            if (entry.getValue())
                selectedIds.add(entry.getKey());

        for (Long ouId : selectedIds)
        {
            ProfileEducationOrgUnit dto = id2dto.get(ouId);
            if (dto == null)
                throw new RuntimeException("dto with id = " + ouId + " not found!"); // never happens

            // если такой id уже есть в базе, то обновляем значения планов
            ProfileEducationOrgUnit current = curretMap.remove(ouId);
            if (current != null)
            {
                current.update(dto);
                update(current);
            } else
            {
                // иначе создаем новый
                ProfileEducationOrgUnit item = new ProfileEducationOrgUnit();
                item.update(dto);
                save(item);
            }
        }

        // удаляем элементы из базы не выбранные на форме
        for (ProfileEducationOrgUnit item : curretMap.values())
            if (!model.getDisabledIds().contains(item.getEducationOrgUnit().getId()))
                delete(item);
    }
}
