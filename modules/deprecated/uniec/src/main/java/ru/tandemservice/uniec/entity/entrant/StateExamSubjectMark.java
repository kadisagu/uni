package ru.tandemservice.uniec.entity.entrant;

import ru.tandemservice.uniec.entity.entrant.gen.StateExamSubjectMarkGen;

/**
 * Балл по предмету ЕГЭ
 */
public class StateExamSubjectMark extends StateExamSubjectMarkGen
{


    /**
     * Возвращает Номер сертификата в формате '99-999999999-99'. Свой если есть или из Сертификата ЕГЭ
     *
     * @param ownOnly только свой Номер сертификата
     * @return Номер сертификата в формате '99-999999999-99' или пустую строку
     */
    public String getFormatedCertificateNumber(boolean ownOnly)
    {
        String number = getCertificateNumber(ownOnly);
        if (number == null) return "";

        return new StringBuilder(number).insert(2, "-").insert(12, "-").toString();
    }

    /**
     * Возвращает Номер сертификата в формате '99-999999999-99'.
     *
     * @return Номер сертификата в формате '99-999999999-99' или пустую строку
     */
    public String getFormatedCertificateNumber()
    {
        return getFormatedCertificateNumber(true);
    }

    /**
     * Возвращает Номер сертификата в формате '99-999999999-99'.
     * Номер должен иметь 13 символов
     *
     * @return Номер сертификата в формате '99-999999999-99' или пустую строку
     */
    public static String getFormatedCertificateNumber(String number)
    {
        if (number == null || number.length() != 13)
            throw new IllegalArgumentException("Certificate Number must have 13 characters");

        return new StringBuilder(number).insert(2, "-").insert(12, "-").toString();
    }

    /**
     * Возвращает Номер сертификата. Свой если есть или из Сертификата ЕГЭ
     *
     * @param ownOnly только свой Номер сертификата
     * @return Номер сертификата
     */
    public String getCertificateNumber(boolean ownOnly)
    {
        if (getCertificateNumber() == null)
            if (ownOnly) return null;
            else return getCertificate().getNumber();

        return getCertificateNumber();
    }

    /**
     * Деформатирует номер в одни цифры
     *
     * @param number Форматированный Номер сертификата
     */
    public void setFormatedCertificateNumber(String number)
    {
        setCertificateNumber(number != null ? number.replace("-", "") : null);
    }
}