package ru.tandemservice.uniec.entity.orders.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.orders.AbstractEntrantParagraph;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniec.entity.orders.EnrollmentRevertParagraph;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Параграф приказа об изменении приказа о зачислении
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrollmentRevertParagraphGen extends AbstractEntrantParagraph
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.orders.EnrollmentRevertParagraph";
    public static final String ENTITY_NAME = "enrollmentRevertParagraph";
    public static final int VERSION_HASH = -2119507068;
    private static IEntityMeta ENTITY_META;

    public static final String L_CANCELED_ORDER = "canceledOrder";
    public static final String P_TITLE = "title";

    private EnrollmentOrder _canceledOrder;     // Отменяемый приказ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Отменяемый приказ. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentOrder getCanceledOrder()
    {
        return _canceledOrder;
    }

    /**
     * @param canceledOrder Отменяемый приказ. Свойство не может быть null.
     */
    public void setCanceledOrder(EnrollmentOrder canceledOrder)
    {
        dirty(_canceledOrder, canceledOrder);
        _canceledOrder = canceledOrder;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrollmentRevertParagraphGen)
        {
            setCanceledOrder(((EnrollmentRevertParagraph)another).getCanceledOrder());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrollmentRevertParagraphGen> extends AbstractEntrantParagraph.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrollmentRevertParagraph.class;
        }

        public T newInstance()
        {
            return (T) new EnrollmentRevertParagraph();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "canceledOrder":
                    return obj.getCanceledOrder();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "canceledOrder":
                    obj.setCanceledOrder((EnrollmentOrder) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "canceledOrder":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "canceledOrder":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "canceledOrder":
                    return EnrollmentOrder.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrollmentRevertParagraph> _dslPath = new Path<EnrollmentRevertParagraph>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrollmentRevertParagraph");
    }
            

    /**
     * @return Отменяемый приказ. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentRevertParagraph#getCanceledOrder()
     */
    public static EnrollmentOrder.Path<EnrollmentOrder> canceledOrder()
    {
        return _dslPath.canceledOrder();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentRevertParagraph#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EnrollmentRevertParagraph> extends AbstractEntrantParagraph.Path<E>
    {
        private EnrollmentOrder.Path<EnrollmentOrder> _canceledOrder;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Отменяемый приказ. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentRevertParagraph#getCanceledOrder()
     */
        public EnrollmentOrder.Path<EnrollmentOrder> canceledOrder()
        {
            if(_canceledOrder == null )
                _canceledOrder = new EnrollmentOrder.Path<EnrollmentOrder>(L_CANCELED_ORDER, this);
            return _canceledOrder;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentRevertParagraph#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(EnrollmentRevertParagraphGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EnrollmentRevertParagraph.class;
        }

        public String getEntityName()
        {
            return "enrollmentRevertParagraph";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTitle();
}
