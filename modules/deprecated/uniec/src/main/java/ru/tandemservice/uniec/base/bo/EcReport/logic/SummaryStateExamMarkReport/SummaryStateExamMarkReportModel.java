/**
 *$Id:$
 */
package ru.tandemservice.uniec.base.bo.EcReport.logic.SummaryStateExamMarkReport;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;

import java.util.Date;
import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 30.07.12
 */
public class SummaryStateExamMarkReportModel implements MultiEnrollmentDirectionUtil.Model
{
    private MultiEnrollmentDirectionUtil.Parameters _parameters;

    // поля активности фильтров отчета
    private boolean _compensTypeActive;
    private boolean _studentCategoryActive;
    private boolean _qualificationActive;
    private boolean _formativeOrgUnitActive;
    private boolean _territorialOrgUnitActive;
    private boolean _developFormActive;
    private boolean _developConditionActive;
    private boolean _developTechActive;
    private boolean _developPeriodActive;

    // поля выбранных элементов в фильтрах отчета
    private EnrollmentCampaign _enrollmentCampaign;
    private Date _requestFrom;
    private Date _requestTo;
    private CompensationType _compensationType;
    private List<StudentCategory> _studentCategoryList;
    private List<Qualifications> _qualificationList;
    private List<OrgUnit> _formativeOrgUnitList;
    private List<OrgUnit> _territorialOrgUnitList;
    private List<DevelopForm> _developFormList;
    private List<DevelopCondition> _developConditionList;
    private List<DevelopTech> _developTechList;
    private List<DevelopPeriod> _developPeriodList;

    // модели фильтров отчета
    private ISingleSelectModel _enrollmentCampaignModel;
    private ISingleSelectModel _compensationTypeModel;
    private IMultiSelectModel _studentCategoryModel;
    private IMultiSelectModel _qualificationModel;
    private IMultiSelectModel _formativeOrgUnitModel;
    private IMultiSelectModel _territorialOrgUnitModel;
    private IMultiSelectModel _developFormModel;
    private IMultiSelectModel _developConditionModel;
    private IMultiSelectModel _developTechModel;
    private IMultiSelectModel _developPeriodModel;

    // поля для печати отчета
    List<Row> _rowList;

    // Getters && Setters
    @Override
    public List<EnrollmentCampaign> getSelectedEnrollmentCampaignList()
    {
        return MultiEnrollmentDirectionUtil.getSingletonList(_enrollmentCampaign);
    }

    @Override
    public List<OrgUnit> getSelectedFormativeOrgUnitList()
    {
        return isFormativeOrgUnitActive() ? getFormativeOrgUnitList() : null;
    }

    @Override
    public List<OrgUnit> getSelectedTerritorialOrgUnitList()
    {
        return isTerritorialOrgUnitActive() ? getTerritorialOrgUnitList() : null;
    }

    @Override
    public List<EducationLevelsHighSchool> getSelectedEducationLevelHighSchoolList()
    {
        return null;
    }

    @Override
    public List<DevelopForm> getSelectedDevelopFormList()
    {
        return isDevelopFormActive() ? getDevelopFormList() : null;
    }

    @Override
    public List<DevelopCondition> getSelectedDevelopConditionList()
    {
        return isDevelopConditionActive() ? getDevelopConditionList() : null;
    }

    @Override
    public List<DevelopTech> getSelectedDevelopTechList()
    {
        return isDevelopTechActive() ? getDevelopTechList() : null;
    }

    @Override
    public List<DevelopPeriod> getSelectedDevelopPeriodList()
    {
        return isDevelopPeriodActive() ? getDevelopPeriodList() : null;
    }

    public MultiEnrollmentDirectionUtil.Parameters getParameters()
    {
        return _parameters;
    }

    public void setParameters(MultiEnrollmentDirectionUtil.Parameters parameters)
    {
        _parameters = parameters;
    }

    public boolean isCompensTypeActive()
    {
        return _compensTypeActive;
    }

    public void setCompensTypeActive(boolean compensTypeActive)
    {
        _compensTypeActive = compensTypeActive;
    }

    public boolean isStudentCategoryActive()
    {
        return _studentCategoryActive;
    }

    public void setStudentCategoryActive(boolean studentCategoryActive)
    {
        _studentCategoryActive = studentCategoryActive;
    }

    public boolean isQualificationActive()
    {
        return _qualificationActive;
    }

    public void setQualificationActive(boolean qualificationActive)
    {
        _qualificationActive = qualificationActive;
    }

    public boolean isFormativeOrgUnitActive()
    {
        return _formativeOrgUnitActive;
    }

    public void setFormativeOrgUnitActive(boolean formativeOrgUnitActive)
    {
        _formativeOrgUnitActive = formativeOrgUnitActive;
    }

    public boolean isTerritorialOrgUnitActive()
    {
        return _territorialOrgUnitActive;
    }

    public void setTerritorialOrgUnitActive(boolean territorialOrgUnitActive)
    {
        _territorialOrgUnitActive = territorialOrgUnitActive;
    }

    public boolean isDevelopFormActive()
    {
        return _developFormActive;
    }

    public void setDevelopFormActive(boolean developFormActive)
    {
        _developFormActive = developFormActive;
    }

    public boolean isDevelopConditionActive()
    {
        return _developConditionActive;
    }

    public void setDevelopConditionActive(boolean developConditionActive)
    {
        _developConditionActive = developConditionActive;
    }

    public boolean isDevelopTechActive()
    {
        return _developTechActive;
    }

    public void setDevelopTechActive(boolean developTechActive)
    {
        _developTechActive = developTechActive;
    }

    public boolean isDevelopPeriodActive()
    {
        return _developPeriodActive;
    }

    public void setDevelopPeriodActive(boolean developPeriodActive)
    {
        _developPeriodActive = developPeriodActive;
    }

    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public Date getRequestFrom()
    {
        return _requestFrom;
    }

    public void setRequestFrom(Date requestFrom)
    {
        _requestFrom = requestFrom;
    }

    public Date getRequestTo()
    {
        return _requestTo;
    }

    public void setRequestTo(Date requestTo)
    {
        _requestTo = requestTo;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

    public List<StudentCategory> getStudentCategoryList()
    {
        return _studentCategoryList;
    }

    public void setStudentCategoryList(List<StudentCategory> studentCategoryList)
    {
        _studentCategoryList = studentCategoryList;
    }

    public List<Qualifications> getQualificationList()
    {
        return _qualificationList;
    }

    public void setQualificationList(List<Qualifications> qualificationList)
    {
        _qualificationList = qualificationList;
    }

    public List<OrgUnit> getFormativeOrgUnitList()
    {
        return _formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(List<OrgUnit> formativeOrgUnitList)
    {
        _formativeOrgUnitList = formativeOrgUnitList;
    }

    public List<OrgUnit> getTerritorialOrgUnitList()
    {
        return _territorialOrgUnitList;
    }

    public void setTerritorialOrgUnitList(List<OrgUnit> territorialOrgUnitList)
    {
        _territorialOrgUnitList = territorialOrgUnitList;
    }

    public List<DevelopForm> getDevelopFormList()
    {
        return _developFormList;
    }

    public void setDevelopFormList(List<DevelopForm> developFormList)
    {
        _developFormList = developFormList;
    }

    public List<DevelopCondition> getDevelopConditionList()
    {
        return _developConditionList;
    }

    public void setDevelopConditionList(List<DevelopCondition> developConditionList)
    {
        _developConditionList = developConditionList;
    }

    public List<DevelopTech> getDevelopTechList()
    {
        return _developTechList;
    }

    public void setDevelopTechList(List<DevelopTech> developTechList)
    {
        _developTechList = developTechList;
    }

    public List<DevelopPeriod> getDevelopPeriodList()
    {
        return _developPeriodList;
    }

    public void setDevelopPeriodList(List<DevelopPeriod> developPeriodList)
    {
        _developPeriodList = developPeriodList;
    }

    public ISingleSelectModel getEnrollmentCampaignModel()
    {
        return _enrollmentCampaignModel;
    }

    public void setEnrollmentCampaignModel(ISingleSelectModel enrollmentCampaignModel)
    {
        _enrollmentCampaignModel = enrollmentCampaignModel;
    }

    public ISingleSelectModel getCompensationTypeModel()
    {
        return _compensationTypeModel;
    }

    public void setCompensationTypeModel(ISingleSelectModel compensationTypeModel)
    {
        _compensationTypeModel = compensationTypeModel;
    }

    public IMultiSelectModel getStudentCategoryModel()
    {
        return _studentCategoryModel;
    }

    public void setStudentCategoryModel(IMultiSelectModel studentCategoryModel)
    {
        _studentCategoryModel = studentCategoryModel;
    }

    public IMultiSelectModel getQualificationModel()
    {
        return _qualificationModel;
    }

    public void setQualificationModel(IMultiSelectModel qualificationModel)
    {
        _qualificationModel = qualificationModel;
    }

    public IMultiSelectModel getFormativeOrgUnitModel()
    {
        return _formativeOrgUnitModel;
    }

    public void setFormativeOrgUnitModel(IMultiSelectModel formativeOrgUnitModel)
    {
        _formativeOrgUnitModel = formativeOrgUnitModel;
    }

    public IMultiSelectModel getTerritorialOrgUnitModel()
    {
        return _territorialOrgUnitModel;
    }

    public void setTerritorialOrgUnitModel(IMultiSelectModel territorialOrgUnitModel)
    {
        _territorialOrgUnitModel = territorialOrgUnitModel;
    }

    public IMultiSelectModel getDevelopFormModel()
    {
        return _developFormModel;
    }

    public void setDevelopFormModel(IMultiSelectModel developFormModel)
    {
        _developFormModel = developFormModel;
    }

    public IMultiSelectModel getDevelopConditionModel()
    {
        return _developConditionModel;
    }

    public void setDevelopConditionModel(IMultiSelectModel developConditionModel)
    {
        _developConditionModel = developConditionModel;
    }

    public IMultiSelectModel getDevelopTechModel()
    {
        return _developTechModel;
    }

    public void setDevelopTechModel(IMultiSelectModel developTechModel)
    {
        _developTechModel = developTechModel;
    }

    public IMultiSelectModel getDevelopPeriodModel()
    {
        return _developPeriodModel;
    }

    public void setDevelopPeriodModel(IMultiSelectModel developPeriodModel)
    {
        _developPeriodModel = developPeriodModel;
    }

    public List<Row> getRowList()
    {
        return _rowList;
    }

    public void setRowList(List<Row> rowList)
    {
        _rowList = rowList;
    }
}
