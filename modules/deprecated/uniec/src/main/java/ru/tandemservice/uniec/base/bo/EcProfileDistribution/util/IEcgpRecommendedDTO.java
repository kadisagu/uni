/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec.base.bo.EcProfileDistribution.util;

import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgCortegeDTO;
import ru.tandemservice.uniec.base.entity.ecgp.EcgpEntrantRecommended;

/**
 * @author Vasily Zhukov
 * @since 15.05.2012
 */
public interface IEcgpRecommendedDTO extends IEntity, IEcgCortegeDTO
{
    final String ENTRANT_RECOMMENDED = "entrantRecommended";
    final String FIO = "fio";
    final String COMPETITION_KIND_TITLE = "competitionKindTitle";
    final String TARGET_ADMISSION_KIND_TITLE = "targetAdmissionKindTitle";
    final String FINAL_MARK = "finalMark";
    final String PROFILE_MARK = "profileMark";
    final String CERTIFICATE_AVERAGE_MARK = "certificateAverageMark";
    final String GRADUATED_PROFILE_EDU_INSTITUTION_TITLE = "graduatedProfileEduInstitutionTitle";
    final String PRIORITIES_SHORT = "prioritiesShort";
    final String PRIORITIES = "priorities";

    /**
     * @return рекомендованный
     */
    EcgpEntrantRecommended getEntrantRecommended();

    /**
     * @return название вид конкурса
     */
    String getCompetitionKindTitle();

    /**
     * @return название вида цп
     */
    String getTargetAdmissionKindTitle();

    /**
     * @return "Да", если закончил профильное учебное учреждение по первому направлению из directionList
     *         "", в другом случае
     */
    String getGraduatedProfileEduInstitutionTitle();

    /**
     * @return сокр. названия всех НПВ из directionList
     */
    String getPriorities();

    /**
     * @return сокр. название всех НПВ без ФУТС
     */
    String getPrioritiesShort();
}
