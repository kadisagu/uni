/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.RequestedEnrollmentDirectionAddEdit;

import org.tandemframework.core.info.ErrorCollector;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author oleyba
 * @since 13.03.2009
 */
public interface IDAO extends IUniDao<Model>
{
    /**
     * Подготавливает список профильных предметов
     *
     * @param model модель
     * @return true, если есть профильные предметы
     */
    boolean prepareProfileDisciplineList(Model model);

    /**
     * Добавляет направления приема в уже созданное заявление.
     * При редактировании кол-во направлений приема не изменяется
     *
     * @param model модель
     * @param errorCollector сборщик ошибок.
     */
    public void update(Model model, ErrorCollector errorCollector);
}
