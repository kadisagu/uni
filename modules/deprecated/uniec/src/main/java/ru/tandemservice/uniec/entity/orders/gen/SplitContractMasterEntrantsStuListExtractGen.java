package ru.tandemservice.uniec.entity.orders.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.orders.SplitContractMasterEntrantsStuListExtract;
import ru.tandemservice.uniec.entity.orders.SplitEntrantsStuListExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «О распределении зачисленных на контракт абитуриентов по профилям (магистратура)»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SplitContractMasterEntrantsStuListExtractGen extends SplitEntrantsStuListExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.orders.SplitContractMasterEntrantsStuListExtract";
    public static final String ENTITY_NAME = "splitContractMasterEntrantsStuListExtract";
    public static final int VERSION_HASH = -1838992984;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SplitContractMasterEntrantsStuListExtractGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SplitContractMasterEntrantsStuListExtractGen> extends SplitEntrantsStuListExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SplitContractMasterEntrantsStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new SplitContractMasterEntrantsStuListExtract();
        }
    }
    private static final Path<SplitContractMasterEntrantsStuListExtract> _dslPath = new Path<SplitContractMasterEntrantsStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SplitContractMasterEntrantsStuListExtract");
    }
            

    public static class Path<E extends SplitContractMasterEntrantsStuListExtract> extends SplitEntrantsStuListExtract.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return SplitContractMasterEntrantsStuListExtract.class;
        }

        public String getEntityName()
        {
            return "splitContractMasterEntrantsStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
