/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.util;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.entity.settings.Group2DisciplineRelation;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author vip_delete
 * @since 14.06.2009
 */
public class EntrantUtil
{
    /**
     * Генерирует уникальный номер абитуриента в году по шаблону [xx][yyyyy],
     * где [хх] две последние цифры года приемной кампании, а [yyyyy] инкриментируемый счетчик абитуриентов в этом году
     *
     * @param session            hibernate session
     * @param enrollmentCampaign приемная кампания
     * @return уникальный номер абитуриента
     */
    public static String generateUniqueEntrantNumber(Session session, EnrollmentCampaign enrollmentCampaign)
    {
        int year = enrollmentCampaign.getEducationYear().getIntValue();
        String xx = String.format("%02d", year % 100);

        Criteria criteria = session.createCriteria(Entrant.class);
        criteria.add(Restrictions.like(Entrant.P_PERSONAL_NUMBER, xx + "%"));
        criteria.setProjection(Projections.max(Entrant.P_PERSONAL_NUMBER));

        String max = (String) criteria.uniqueResult();
        return (max != null) ? String.format("%07d", Integer.parseInt(max) + 1) : xx + "00001";
    }

    public static Map<Long, Set<Discipline2RealizationWayRelation>> getDisciplinesGroupMap(Session session, EnrollmentCampaign enrollmentCampaign)
    {
        MQBuilder groupBuilder = new MQBuilder(Group2DisciplineRelation.ENTITY_CLASS, "r");
        groupBuilder.addJoinFetch("r", Group2DisciplineRelation.discipline().s(), "discipline");
        groupBuilder.add(MQExpression.eq("r", Group2DisciplineRelation.L_DISCIPLINE + "." + Discipline2RealizationWayRelation.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign));
        Map<Long, Set<Discipline2RealizationWayRelation>> group2discipline = new HashMap<>();
        for (Group2DisciplineRelation groupRelation : groupBuilder.<Group2DisciplineRelation>getResultList(session))
        {
            Set<Discipline2RealizationWayRelation> set = group2discipline.get(groupRelation.getGroup().getId());
            if (set == null)
                group2discipline.put(groupRelation.getGroup().getId(), set = new HashSet<>());
            set.add(groupRelation.getDiscipline());
        }
        return group2discipline;
    }

    /**
     * При добавлении/редактировании заявления абитуриента и при добавлении/изменении направления или направлений из
     * заявления, API функция должна захватить лок NAME-enrollmentCampaignId до конца транзакции.
     * Это обеспечит уровень изоляции SERIALIZABLE для EntrantRequest и RequestedEnrollmentDirection в рамках приемной
     * кампании, на котором основаны все ограничения приемной кампании.
     * Hint: проверки на эти ограничения требуют отсутствия фантомных вставок
     *
     * @param session сессия
     * @param entrant абитуриент
     */
    public static void lockEnrollmentCampaign(Session session, Entrant entrant)
    {
        // TODO не ясно, зачем нужен лок ПК. Пока его закомментируем, т.к. он падает. Если что-то отвалится, будем смотреть по месту.
        //NamedSyncInTransactionCheckLocker.register(session, "enrollmentCampaignLock-" + entrant.getEnrollmentCampaign().getId());
        lockEntrant(session, entrant);
    }

    /**
     * При изменении данных абитуриента, для которых не требуется лок на всю приемную кампанию
     *
     * @param session сессия
     * @param entrant абитуриент
     */
    public static void lockEntrant(Session session, Entrant entrant)
    {
        NamedSyncInTransactionCheckLocker.register(session, "entrantLock-" + entrant.getId());
    }
}
