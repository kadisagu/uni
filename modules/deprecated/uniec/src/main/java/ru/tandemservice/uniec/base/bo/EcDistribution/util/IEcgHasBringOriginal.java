/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.util;

/**
 * @author Vasily Zhukov
 * @since 25.07.2011
 */
public interface IEcgHasBringOriginal
{
    IEcgDistribution getDistribution();

    Boolean getBringOriginal();
    
    Boolean getHasPreStudent();
}
