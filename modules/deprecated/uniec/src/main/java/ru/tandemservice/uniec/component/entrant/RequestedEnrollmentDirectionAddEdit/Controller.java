/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.RequestedEnrollmentDirectionAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.component.impl.BusinessComponent;
import org.tandemframework.core.component.impl.ComponentRegion;
import org.tandemframework.core.info.ErrorCollector;

/**
 * @author oleyba
 * @since 13.03.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickApply(IBusinessComponent component)
    {
        ErrorCollector errors = component.getUserContext().getErrorCollector();

        getDao().update(getModel(component), errors);

        if (!errors.hasErrors())
        {
            ((BusinessComponent) ((ComponentRegion) component.getParentRegion()).getOwner()).refresh();

            deactivate(component);
        }
    }

    public boolean isExistProfileDisciplineList(Model model)
    {
        return getDao().prepareProfileDisciplineList(model);
    }
}

