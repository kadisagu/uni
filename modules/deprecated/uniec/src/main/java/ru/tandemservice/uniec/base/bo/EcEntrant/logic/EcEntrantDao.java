/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec.base.bo.EcEntrant.logic;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;

import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.ProfileKnowledge;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantOriginalDocumentRelation;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.ProfileExaminationMark;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.RequestedProfileKnowledge;
import ru.tandemservice.uniec.util.EntrantRequestAddEditUtil;
import ru.tandemservice.uniec.util.EntrantUtil;

/**
 * @author Vasily Zhukov
 * @since 26.04.2012
 */
public class EcEntrantDao extends CommonDAO implements IEcEntrantDao
{
    @Override
    @SuppressWarnings("unchecked")
    public void saveOrUpdateEntrantRequest(
                                           List<RequestedEnrollmentDirection> selectedList,
                                           List<RequestedEnrollmentDirection> forDeleteList,
                                           boolean addForm,
                                           EntrantRequest entrantRequest,
                                           Integer entrantRequestNumber,
                                           Integer entrantDirectionNumber,
                                           Map<Long, List<ProfileKnowledge>> profileKnowledgeMap,
                                           Map<Long, List<ProfileExaminationMark>> profileMarkMap)
    {
        if (selectedList.isEmpty())
            throw new ApplicationException("Не выбраны направления подготовки (специальности).");

        // загружаем актуальные настройки приемной кампании на текущий момент
        Session session = getSession();
        EnrollmentCampaign enrollmentCampaign = entrantRequest.getEntrant().getEnrollmentCampaign();
        session.refresh(enrollmentCampaign);
        ErrorCollector errorCollector = ContextLocal.getUserContext().getErrorCollector();

        // захватываем лок
        EntrantUtil.lockEnrollmentCampaign(session, entrantRequest.getEntrant());

        // V A L I D A T E

        // номер заявления
        if (addForm)
        {
            // номер заявления мог быть указан на форме добавления, если это так, то достаточно проверить его на уникальность
            if (entrantRequestNumber != null)
            {
                Criteria criteria = session.createCriteria(EntrantRequest.class, "request");
                criteria.createAlias("request." + EntrantRequest.L_ENTRANT, "entrant");
                criteria.add(Restrictions.eq("entrant." + Entrant.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign));
                criteria.add(Restrictions.eq("request." + EntrantRequest.P_REG_NUMBER, entrantRequestNumber));
                criteria.setProjection(Projections.rowCount());

                Number countNumber = (Number) criteria.uniqueResult();
                if (countNumber != null && countNumber.intValue() > 0)
                    errorCollector.add("Номер заявления должен быть уникальным в рамках приемной кампании.", "regNumber");
            } else
            {
                // вычисляем новый номер заявления.
                // проверка его на уникальность в рамках приемной кампании
                Criteria criteria = session.createCriteria(EntrantRequest.class, "request");
                criteria.createAlias("request." + EntrantRequest.L_ENTRANT, "entrant");
                criteria.add(Restrictions.eq("entrant." + Entrant.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign));
                criteria.setProjection(Projections.max(EntrantRequest.P_REG_NUMBER));

                Number maxNumber = (Number) criteria.uniqueResult();
                entrantRequestNumber = (maxNumber == null ? 1 : maxNumber.intValue() + 1);
            }
            entrantRequest.setRegNumber(entrantRequestNumber);
        } else
        {
            if (entrantRequestNumber == null)
                throw new ApplicationException("Номер заявления должен быть указан.");

            // на форме редактирования можно указать новый номер заявления.
            // проверка его на уникальность в рамках приемной кампании
            Criteria criteria = session.createCriteria(EntrantRequest.class, "request");
            criteria.createAlias("request." + EntrantRequest.L_ENTRANT, "entrant");
            criteria.add(Restrictions.eq("entrant." + Entrant.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign));
            criteria.add(Restrictions.eq("request." + EntrantRequest.P_REG_NUMBER, entrantRequestNumber));
            criteria.add(Restrictions.ne("request." + EntrantRequest.P_ID, entrantRequest.getId()));
            criteria.setProjection(Projections.rowCount());

            Number countNumber = (Number) criteria.uniqueResult();
            if (countNumber != null && countNumber.intValue() > 0)
                errorCollector.add("Номер заявления должен быть уникальным в рамках приемной кампании.", "regNumber");

            entrantRequest.setRegNumber(entrantRequestNumber);
        }

        // если нет флага ручного задания номера или выбрано несколько направлений, то нельзя указывать номер направления
        if (!enrollmentCampaign.isNumberOnDirectionManual() || selectedList.size() != 1)
            entrantDirectionNumber = null;

        for (RequestedEnrollmentDirection item : selectedList)
        {
            // если целевой прием не указан, то и не должно быть организации и типа целевого приема
            if (!item.isTargetAdmission())
            {
                item.setExternalOrgUnit(null);
                item.setTargetAdmissionKind(null);
            }

            if (entrantDirectionNumber != null && (item.getId() < 0 || item.getNumber() != entrantDirectionNumber))
            {
                Criteria c = session.createCriteria(RequestedEnrollmentDirection.class);
                c.add(Restrictions.eq(RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, item.getEnrollmentDirection()));
                c.add(Restrictions.eq(RequestedEnrollmentDirection.P_NUMBER, entrantDirectionNumber));
                List<RequestedEnrollmentDirection> list = c.list();

                if (list.isEmpty())
                    item.setNumber(entrantDirectionNumber);
                else
                    errorCollector.add("№ на направлении/специальности «" + entrantDirectionNumber + "» уже занят абитуриентом «" + list.get(0).getEntrantRequest().getEntrant().getPerson().getFullFio() + "».", "directionNumber");
            }
        }

        EntrantRequestAddEditUtil.validateRequestedDirections(session, selectedList, forDeleteList, entrantRequest);
        if (errorCollector.hasErrors())
            return;

        // U P D A T E   D A T A

        // в этом блоке нельзя бросать ApplicationException или добавлять ошибки в ErrorCollector

        session.saveOrUpdate(entrantRequest);

        // находим максимальный приоритет среди всех существующих в базе ВНП среди всех заявлений этого абитуриента в рамках вида возмещения затрат
        // все новым ВНП будут добавляться в конец списка приоритетов ВНП среди всех заявлений
        int budgetMaxPriority = Integer.MIN_VALUE;
        int contractMaxPriority = Integer.MIN_VALUE;
        for (RequestedEnrollmentDirection direction : getList(RequestedEnrollmentDirection.class, RequestedEnrollmentDirection.entrantRequest().entrant(), entrantRequest.getEntrant()))
        {
            int priority = direction.getPriorityPerEntrant();
            if (direction.getCompensationType().isBudget())
            {
                if (priority > budgetMaxPriority)
                    budgetMaxPriority = priority;
            } else
            {
                if (priority > contractMaxPriority)
                    contractMaxPriority = priority;
            }
        }
        budgetMaxPriority++;
        contractMaxPriority++;

        for (RequestedEnrollmentDirection forDelete : forDeleteList)
        {
            forDelete.setProfileChosenEntranceDiscipline(null);  // сбрасываем профильное, потому что если оно есть, то ссылается на это направление, и, удаляясь каскадом, будет ошибка.
            session.update(forDelete);
        }

        session.flush();

        // удаляем исключенные с формы направления приема
        for (RequestedEnrollmentDirection forDelete : forDeleteList)
            session.delete(forDelete);

        // чтобы не падало на priorityUniqueKey, при одновременном удалении и изменении приоритета
        // см. класс ActionQueue.executeActions: запросы update выполняется раньше запросов delete
        session.flush();

        // получаем направления приема, которые находятся в базе в этом заявлении
        List<RequestedEnrollmentDirection> existedList = addForm ? Collections.<RequestedEnrollmentDirection>emptyList() : getList(RequestedEnrollmentDirection.class, RequestedEnrollmentDirection.L_ENTRANT_REQUEST, entrantRequest, RequestedEnrollmentDirection.P_PRIORITY);

        // требуется изменять приоритеты только если были изменения в списке направлений
        boolean needChangePriorities = addForm || !forDeleteList.isEmpty() || !Arrays.equals(selectedList.toArray(), existedList.toArray());

        if (needChangePriorities)
        {
            // всем направлениям приема, которые сохранены в базе присваиваем уникальный приоритет
            // так как потом будет перенумерация их приоритета (это уникальное поле)
            int priority = -1;
            for (RequestedEnrollmentDirection item : existedList)
            {
                item.setPriority(priority);
                session.update(item);
                priority--;
            }

            // надо сохранить все существующие в базе выбранные направления приема с новыми отрицательными приоритетами
            // если не делать flush, то в результирующие update запросы не попадут такие изменения, а попадут только update
            // в которых будет нормальный приоритета - 0, 1, 2. тут то с свалится на priorityUniqueKey
            session.flush();
        }

        // бежим по всем выбранным направлениям
        int priority = 1;
        EntrantState activeState = (EntrantState) session.createCriteria(EntrantState.class).add(Restrictions.eq(EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_ACTIVE_CODE)).uniqueResult();
        for (RequestedEnrollmentDirection item : selectedList)
        {
            // получаем заранее список профильных знаний так как Id может измениться
            List<ProfileKnowledge> profileKnowledgeList = profileKnowledgeMap == null ? null : profileKnowledgeMap.get(item.getId());

            // сохраняем оценки по профильным дисциплинам, если они есть
            List<ProfileExaminationMark> profileMarkList = profileMarkMap == null ? null : profileMarkMap.get(item.getId());

            if (profileMarkList != null)
            {
                for (ProfileExaminationMark profileMark : profileMarkList)
                {
                    Criteria c = session.createCriteria(ProfileExaminationMark.class);
                    c.add(Restrictions.eq(ProfileExaminationMark.L_ENTRANT, profileMark.getEntrant()));
                    c.add(Restrictions.eq(ProfileExaminationMark.L_DISCIPLINE, profileMark.getDiscipline()));
                    ProfileExaminationMark savedProfileMark = (ProfileExaminationMark) c.uniqueResult();
                    if (savedProfileMark == null)
                    {
                        session.save(profileMark);
                    } else
                    {
                        savedProfileMark.setMark(profileMark.getMark());
                        session.update(savedProfileMark);
                    }
                }
            }

            if (item.getId() < 0)
            {
                // 1. если текущего напраления нет в базе, то присваиваем ему пустой id.
                //    ранее (для отображения в SearchList ему был присвоен липовый id)
                item.setId(null);
                item.setPriorityPerEntrant(item.getCompensationType().isBudget() ? budgetMaxPriority++ : contractMaxPriority++);
                if (entrantDirectionNumber == null)
                {
                    Criteria c = session.createCriteria(RequestedEnrollmentDirection.class);
                    c.add(Restrictions.eq(RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, item.getEnrollmentDirection()));
                    c.setProjection(Projections.max(RequestedEnrollmentDirection.P_NUMBER));
                    Number maxNumber = (Number) c.uniqueResult();

                    // 2. присваиваем порядковый номер выбранного направления приема
                    //    (каким по счету абитуриент выбрал это направление приема)
                    item.setNumber(maxNumber == null ? 1 : maxNumber.intValue() + 1);
                }
            }

            if (needChangePriorities)
            {
                // присваиваем следующий по порядку приоритет
                item.setPriority(priority++);
            }

            if (item.getState() == null)
                item.setState(activeState);
            session.saveOrUpdate(item);

            if (item.isOriginalDocumentHandedIn())
                updateOriginalDocuments(item, true);

            // сохраняем профильные знания, если они есть
            if (profileKnowledgeList != null)
            {
                for (ProfileKnowledge profileKnowledge : profileKnowledgeList)
                {
                    Criteria c = session.createCriteria(RequestedProfileKnowledge.class);
                    c.add(Restrictions.eq(RequestedProfileKnowledge.L_PROFILE_KNOWLEDGE, profileKnowledge));
                    c.add(Restrictions.eq(RequestedProfileKnowledge.L_REQUESTED_ENROLLMENT_DIRECTION, item));
                    c.setProjection(Projections.rowCount());
                    Number number = (Number) c.uniqueResult();
                    int count = number == null ? 0 : number.intValue();
                    if (count == 0)
                    {
                        RequestedProfileKnowledge requestedProfileKnowledge = new RequestedProfileKnowledge();
                        requestedProfileKnowledge.setProfileKnowledge(profileKnowledge);
                        requestedProfileKnowledge.setRequestedEnrollmentDirection(item);
                        session.save(requestedProfileKnowledge);
                    }
                }
            }
        }

        doNormalizePriorityPerEntrant(entrantRequest.getEntrant());
    }

    @Override
    public void updateOriginalDocuments(RequestedEnrollmentDirection direction, boolean save)
    {
        Session session = getSession();
        EntrantUtil.lockEntrant(session, direction.getEntrantRequest().getEntrant());

        EntrantOriginalDocumentRelation relation = get(EntrantOriginalDocumentRelation.class, EntrantOriginalDocumentRelation.entrant(), direction.getEntrantRequest().getEntrant());
        if (save) {
            if (relation == null) {
                relation = new EntrantOriginalDocumentRelation();
                relation.setEntrant(direction.getEntrantRequest().getEntrant());
            }

            // если кому-то флаг устанавливается => у остальных он будет спброшен
            relation.setRequestedEnrollmentDirection(direction);

            if (relation.getId() == null)
                session.save(relation);
            else
                session.update(relation);
        } else {

            // если я сбрасываю флаг - то только тогда, когда он на мне
            if (relation != null) {
                if (direction.equals(relation.getRequestedEnrollmentDirection())) {
                    session.delete(relation);
                }
            }
        }
    }

    @Override
    public void deleteRequestedEnrollmentDirection(Long directionId)
    {
        Session session = getSession();
        RequestedEnrollmentDirection direction = getNotNull(RequestedEnrollmentDirection.class, directionId);
        EntrantUtil.lockEnrollmentCampaign(session, direction.getEntrantRequest().getEntrant());

        final MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "d");
        builder.add(MQExpression.eq("d", RequestedEnrollmentDirection.L_ENTRANT_REQUEST, direction.getEntrantRequest()));
        if (builder.getResultCount(session) <= 1)
            throw new ApplicationException("В заявлении должно оставаться хотя бы одно направление.");

        // сбрасываем профильное, потому что если оно есть, то ссылается на само это направление и удаляется каскадом, будет ошибка
        if (direction.getProfileChosenEntranceDiscipline() != null)
        {
            direction.setProfileChosenEntranceDiscipline(null);
            session.update(direction);
            session.flush();
        }
        session.delete(direction);

        // нормализуем приоритеты ВНП в заявлении
        CommonManager.instance().commonPriorityDao().doNormalizePriority(RequestedEnrollmentDirection.class,
            RequestedEnrollmentDirection.entrantRequest(), direction.getEntrantRequest()
        );

        // нормализуем приоритеты ВНП среди всех заявлений в разрезе вида возмещения затрат
        doNormalizePriorityPerEntrant(direction.getEntrantRequest().getEntrant());
    }

    @Override
    public void deleteEntrantRequest(Long entrantRequestId)
    {
        Session session = getSession();
        EntrantRequest entrantRequest = getNotNull(EntrantRequest.class, entrantRequestId);
        EntrantUtil.lockEnrollmentCampaign(session, entrantRequest.getEntrant());

        // сбрасываем профильное, потому что если оно есть, то ссылается на само это направление и удаляется каскадом, будет ошибка
        for (RequestedEnrollmentDirection direction : getList(RequestedEnrollmentDirection.class, RequestedEnrollmentDirection.entrantRequest(), entrantRequest))
        {
            direction.setProfileChosenEntranceDiscipline(null);
            session.update(direction);
        }
        session.flush();
        session.delete(entrantRequest); // все ВНП удалятся каскадом

        doNormalizePriorityPerEntrant(entrantRequest.getEntrant());
    }

    /**
     * При удалении элементов из списка ВНП требуется нормализация приоритетов ВНП среди всех заявлений в разрезе вида возмещения затрат
     * ВНП удаляются только при удалении ВНП и при удалении заявления, поэтому большого смысла писать листенер нет,
     * легче вызвать этот метод из дву api-функций: deleteRequestedEnrollmentDirection, deleteEntrantRequest
     * Hint: при добавлении ВНП из нужно добавлять в конец списка
     *
     * @param entrant абитуриент
     */
    @Override
    public void doNormalizePriorityPerEntrant(Entrant entrant)
    {
        for (CompensationType compensationType : getList(CompensationType.class))
            CommonManager.instance().commonPriorityDao().doNormalizePriority(RequestedEnrollmentDirection.class, RequestedEnrollmentDirection.P_PRIORITY_PER_ENTRANT,
                RequestedEnrollmentDirection.entrantRequest().entrant(), entrant,
                RequestedEnrollmentDirection.compensationType(), compensationType
            );
    }
}
