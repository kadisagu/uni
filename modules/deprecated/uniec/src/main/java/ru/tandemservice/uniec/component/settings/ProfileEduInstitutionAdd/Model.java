/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.settings.ProfileEduInstitutionAdd;

import org.tandemframework.shared.fias.base.bo.util.IKladrModel;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.person.base.bo.PersonSetting.util.IEduInstitutionTypeModel;
import org.tandemframework.shared.person.catalog.entity.EduInstitution;
import org.tandemframework.shared.person.catalog.entity.EducationalInstitutionTypeKind;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

/**
 * @author vip_delete
 * @since 04.06.2009
 */
public class Model implements IKladrModel, IEduInstitutionTypeModel
{
    private ISelectModel _countries;
    private ISelectModel _settlements;
    private ISelectModel _personEduInstitutionTypeList;
    private ISelectModel _personEduInstitutionKindList;
    private ISelectModel _personEduInstitutionList;

    private AddressCountry _addressCountry;
    private AddressItem _addressItem;
    private EducationalInstitutionTypeKind _eduInstitutionType;
    private EducationalInstitutionTypeKind _eduInstitutionKind;
    private EduInstitution _personEduInstitution;

    // Getters & Setters

    public ISelectModel getCountries()
    {
        return _countries;
    }

    public void setCountries(ISelectModel countries)
    {
        _countries = countries;
    }

    public ISelectModel getSettlements()
    {
        return _settlements;
    }

    public void setSettlements(ISelectModel settlements)
    {
        _settlements = settlements;
    }

    public ISelectModel getPersonEduInstitutionTypeList()
    {
        return _personEduInstitutionTypeList;
    }

    public void setPersonEduInstitutionTypeList(ISelectModel personEduInstitutionTypeList)
    {
        _personEduInstitutionTypeList = personEduInstitutionTypeList;
    }

    public ISelectModel getPersonEduInstitutionKindList()
    {
        return _personEduInstitutionKindList;
    }

    public void setPersonEduInstitutionKindList(ISelectModel personEduInstitutionKindList)
    {
        _personEduInstitutionKindList = personEduInstitutionKindList;
    }

    public ISelectModel getPersonEduInstitutionList()
    {
        return _personEduInstitutionList;
    }

    public void setPersonEduInstitutionList(ISelectModel personEduInstitutionList)
    {
        _personEduInstitutionList = personEduInstitutionList;
    }

    @Override
    public AddressCountry getAddressCountry()
    {
        return _addressCountry;
    }

    public void setAddressCountry(AddressCountry addressCountry)
    {
        _addressCountry = addressCountry;
    }

    @Override
    public AddressItem getAddressItem()
    {
        return _addressItem;
    }

    public void setAddressItem(AddressItem addressItem)
    {
        _addressItem = addressItem;
    }

    @Override
    public EducationalInstitutionTypeKind getEduInstitutionType()
    {
        return _eduInstitutionType;
    }

    public void setEduInstitutionType(EducationalInstitutionTypeKind eduInstitutionType)
    {
        _eduInstitutionType = eduInstitutionType;
    }

    @Override
    public EducationalInstitutionTypeKind getEduInstitutionKind()
    {
        return _eduInstitutionKind;
    }

    public void setEduInstitutionKind(EducationalInstitutionTypeKind eduInstitutionKind)
    {
        _eduInstitutionKind = eduInstitutionKind;
    }

    public EduInstitution getPersonEduInstitution()
    {
        return _personEduInstitution;
    }

    public void setPersonEduInstitution(EduInstitution personEduInstitution)
    {
        _personEduInstitution = personEduInstitution;
    }
}
