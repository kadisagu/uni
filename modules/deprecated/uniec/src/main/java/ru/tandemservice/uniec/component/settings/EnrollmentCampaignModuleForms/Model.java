/* $Id$ */
package ru.tandemservice.uniec.component.settings.EnrollmentCampaignModuleForms;

import org.tandemframework.core.CoreCollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: azhebko
 * Date: 12.07.12
 * Time: 11:48
 * To change this template use File | Settings | File Templates.
 */
public class Model
{
    private Map<String, CoreCollectionUtils.Pair<Boolean, String>> _map;
    private String _key;

    public Map<String, CoreCollectionUtils.Pair<Boolean, String>> getMap()
    {
        return _map;
    }

    public void setMap(Map<String, CoreCollectionUtils.Pair<Boolean, String>> map)
    {
        _map = map;
    }

    public String getKey()
    {
        return _key;
    }

    public void setKey(String key)
    {
        _key = key;
    }


    public List<String> getList()
    {
        return new ArrayList<String>(getMap().keySet());
    }

    public String getTitle()
    {
        return getMap().get(getKey()).getY();
    }

    public Boolean getValue()
    {
        return getMap().get(getKey()).getX();
    }

    public void setValue(Boolean value)
    {
        getMap().get(getKey()).setX(value);
    }
}
