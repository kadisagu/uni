/* $Id$ */
package ru.tandemservice.uniec.dao;

import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.uni.services.ContractAttributes;
import ru.tandemservice.uni.services.IEntrantEduContractDataService;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.orders.SplitEntrantsStuListExtract;

import java.util.*;

/**
 * @author Dmitry Seleznev
 * @since 16.08.2012
 */
public class UniecOrderDAO extends HibernateDaoSupport implements IUniecOrderDAO
{
    public RtfDocument getOrderPrintForm(byte[] template, StudentListOrder order, String scriptCode)
    {
        List<CoreCollectionUtils.Pair<Long, Long>> idPairsList = new ArrayList<>();
        for (AbstractStudentExtract extract : MoveStudentDaoFacade.getMoveStudentDao().getOrderExtractList(order, true))
        {
            if (extract instanceof SplitEntrantsStuListExtract)
            {
                SplitEntrantsStuListExtract splitExtract = (SplitEntrantsStuListExtract) extract;
                CoreCollectionUtils.Pair<Long, Long> pair = new CoreCollectionUtils.Pair<>(splitExtract.getEcgpEntrantRecommended().getId(), extract.getEntity().getId());
                idPairsList.add(pair);
            }
        }

        Map<Long, ContractAttributes> contractsMap = new HashMap<>();
        IEntrantEduContractDataService contractDataService = getEntrantEduContractDataService();
        if (null != contractDataService)
        {
            contractsMap = contractDataService.getEntrantContractAttributes(idPairsList);
        }

        Map<String, Object> map = CommonManager.instance().scriptDao().getScriptResult(
                DataAccessServices.dao().getByCode(UniecScriptItem.class, scriptCode),
                "session", getSession(),
                "template", template,
                "order", order,
                "contractsMap", contractsMap
        );

        return (RtfDocument) map.get("document");
    }

    protected IEntrantEduContractDataService getEntrantEduContractDataService()
    {
        IEntrantEduContractDataService bean;
        try
        {
            bean = (IEntrantEduContractDataService) ApplicationRuntime.getBean(IEntrantEduContractDataService.BEAN_NAME);
        }
        catch (NoSuchBeanDefinitionException e)
        {
            // бин не определен - падать не надо - надо вернуть null
            return null;
        }

        return bean;
    }
}