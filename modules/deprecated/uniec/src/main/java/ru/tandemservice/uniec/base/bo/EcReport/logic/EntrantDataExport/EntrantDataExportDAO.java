/**
 *$Id:$
 */
package ru.tandemservice.uniec.base.bo.EcReport.logic.EntrantDataExport;

import org.tandemframework.caf.logic.dao.BaseModifyAggregateDAO;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.CommonMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.CommonSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaignPeriod;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;

import java.util.*;

/**
 * @author Alexander Shaburov
 * @since 02.08.12
 */
public class EntrantDataExportDAO extends BaseModifyAggregateDAO implements IEntrantDataExportDAO
{
    @Override
    public <M extends EntrantDataExportModel> M prepareModel(M model)
    {
        // фильтр Приемная компания
        List<EnrollmentCampaign> ecList = DataAccessServices.dao()
                .getList(new DQLSelectBuilder().fromEntity(EnrollmentCampaign.class, "b").column("b")
                        .order(DQLExpressions.property(EnrollmentCampaign.id().fromAlias("b")), OrderDirection.desc));
        model.setEnrollmentCampaign(ecList.isEmpty() ? null : ecList.get(0));

        // фильтры Заявления с - по
        EnrollmentCampaign enrollmentCampaign = model.getEnrollmentCampaign();
        List<EnrollmentCampaignPeriod> periodList = enrollmentCampaign != null ? enrollmentCampaign.getPeriodList() : new LinkedList<EnrollmentCampaignPeriod>();
        model.setRequestFrom(periodList.isEmpty() ? null : periodList.get(0).getDateFrom());
        model.setRequestTo(new Date());

        // заполняем модели селектов
        model.setEnrollmentCampaignModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrollmentCampaign.class, "b").column("b");
                builder.where(DQLExpressions.likeUpper(DQLExpressions.property(EnrollmentCampaign.title().fromAlias("b")), DQLExpressions.value(CoreStringUtils.escapeLike(filter))));
                if (o != null)
                    builder.where(DQLExpressions.eqValue(DQLExpressions.property(EnrollmentCampaign.id().fromAlias("b")), o));
                builder.order(DQLExpressions.property(EnrollmentCampaign.id().fromAlias("b")), OrderDirection.desc);

                return new DQLListResultBuilder(builder);
            }
        });
        model.setCompensationTypeModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(CompensationType.class, "b").column("b");
                builder.where(DQLExpressions.likeUpper(DQLExpressions.property(CompensationType.title().fromAlias("b")), DQLExpressions.value(CoreStringUtils.escapeLike(filter))));
                if (o != null)
                    builder.where(DQLExpressions.eqValue(DQLExpressions.property(CompensationType.id().fromAlias("b")), o));
                builder.order(DQLExpressions.property(CompensationType.code().fromAlias("b")));

                return new DQLListResultBuilder(builder);
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((CompensationType) value).getShortTitle();
            }
        });
        model.setStatusModel(new CommonMultiSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EntrantState.class, "b").column("b");
                builder.where(DQLExpressions.likeUpper(DQLExpressions.property(EntrantState.title().fromAlias("b")), DQLExpressions.value(CoreStringUtils.escapeLike(filter))));
                if (set != null)
                    builder.where(DQLExpressions.in(DQLExpressions.property(EntrantState.id().fromAlias("b")), set));
                builder.order(DQLExpressions.property(EntrantState.title().fromAlias("b")));

                return new DQLListResultBuilder(builder);
            }
        });

        // заполняем модели селекта с помощью утили
        model.setFormativeOrgUnitModel((IMultiSelectModel) MultiEnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(null, model));
        model.setTerritorialOrgUnitModel((IMultiSelectModel) MultiEnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(null, model));
        model.setEducationLevelHighSchoolModel((IMultiSelectModel) MultiEnrollmentDirectionUtil.createEducationLevelsHighSchoolModel(model));
        model.setDevelopFormModel((IMultiSelectModel) MultiEnrollmentDirectionUtil.createDevelopFormModel(model));
        model.setDevelopConditionModel((IMultiSelectModel) MultiEnrollmentDirectionUtil.createDevelopConditionModel(model));
        model.setDevelopTechModel((IMultiSelectModel) MultiEnrollmentDirectionUtil.createDevelopTechModel(model));
        model.setDevelopPeriodModel((IMultiSelectModel) MultiEnrollmentDirectionUtil.createDevelopPeriodModel(model));


        // заполняем по умолчанию пустые селекты
        model.setCompensationType(null);
        model.setFormativeOrgUnitList(new ArrayList<OrgUnit>());
        model.setTerritorialOrgUnitList(new ArrayList<OrgUnit>());
        model.setEducationLevelHighSchoolList(new LinkedList<EducationLevelsHighSchool>());
        model.setDevelopFormList(new ArrayList<DevelopForm>());
        model.setDevelopConditionList(new ArrayList<DevelopCondition>());
        model.setDevelopTechList(new ArrayList<DevelopTech>());
        model.setDevelopPeriodList(new ArrayList<DevelopPeriod>());

        // запллняем активность полей по умолчанию
        model.setCompensationTypeActive(false);
        model.setFormativeOrgUnitActive(false);
        model.setEducationLevelHighSchoolActive(false);
        model.setTerritorialOrgUnitActive(false);
        model.setDevelopFormActive(false);
        model.setDevelopConditionActive(false);
        model.setDevelopTechActive(false);
        model.setDevelopPeriodActive(false);

        // определяем обязательность полей для модели утили
        model.setParameters(new MultiEnrollmentDirectionUtil.Parameters());
        model.getParameters().setRequired(false, MultiEnrollmentDirectionUtil.Parameters.Filter.values());

        return model;
    }

    @Override
    public <M extends EntrantDataExportModel> M onChangeEnrollmentCampaign(M model)
    {
        // подставляем даты выбранной ПК
        EnrollmentCampaign enrollmentCampaign = model.getEnrollmentCampaign();
        List<EnrollmentCampaignPeriod> periodList = enrollmentCampaign != null ? enrollmentCampaign.getPeriodList() : new LinkedList<EnrollmentCampaignPeriod>();
        model.setRequestFrom(periodList.isEmpty() ? null : periodList.get(0).getDateFrom());
        model.setRequestTo(periodList.isEmpty() ? null : periodList.get(0).getDateTo());

        return model;
    }
}
