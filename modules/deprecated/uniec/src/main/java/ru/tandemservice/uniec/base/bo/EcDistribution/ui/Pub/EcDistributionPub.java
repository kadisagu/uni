/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import ru.tandemservice.uniec.base.bo.EcDistribution.logic.EcSimpleDSHandler;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgRecommendedDTO;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgRecommendedPreStudentDTO;
import ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommended;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

/**
 * @author Vasily Zhukov
 * @since 07.07.2011
 */
@Configuration
public class EcDistributionPub extends BusinessComponentManager
{
    public static final String TAB_PANEL = "tabPanel";
    public static final String TAB_PANEL_PUB = "tabPanelPub";
    public static final String TAB_PANEL_RECOMMENDATION = "tabPanelRecommendation";

    public static final String DIRECTION_DS = "directionDS";
    public static final String RECOMMENDED_DS = "recommendedDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(DIRECTION_DS, directionDS(), directionDSHandler()))
                .addDataSource(searchListDS(RECOMMENDED_DS, recommendedDS(), recommendedDSHandler()))
                .create();
    }

    @Bean
    public TabPanelExtPoint tabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(TAB_PANEL)
                .addTab(htmlTab(TAB_PANEL_PUB, "Pub").create())
                .addTab(htmlTab(TAB_PANEL_RECOMMENDATION, "Recommendation").create())
                .create();
    }

    @Bean
    public ColumnListExtPoint directionDS()
    {
        return columnListExtPointBuilder(DIRECTION_DS)
                .addColumn(textColumn("code", EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().shortTitle()))
                .addColumn(textColumn("title", EnrollmentDirection.title()))
                .addColumn(textColumn("formativeOrgUnit", EnrollmentDirection.educationOrgUnit().formativeOrgUnit().title()))
                .addColumn(textColumn("territorialOrgUnit", EnrollmentDirection.educationOrgUnit().territorialOrgUnit().territorialShortTitle()))
                .addColumn(textColumn("developForm", EnrollmentDirection.educationOrgUnit().developForm().title()))
                .addColumn(textColumn("developCondition", EnrollmentDirection.educationOrgUnit().developCondition().title()))
                .addColumn(textColumn("developTech", EnrollmentDirection.educationOrgUnit().developTech().title()))
                .addColumn(textColumn("developPeriod", EnrollmentDirection.educationOrgUnit().developPeriod().title()))
                .addColumn(blockColumn("quota", "quotaBlockColumn").width("10").create())
                .addColumn(blockColumn("taQuota", "taQuotaBlockColumn").width("10").create())
                .create();
    }

    @Bean
    public ColumnListExtPoint recommendedDS()
    {
        return columnListExtPointBuilder(RECOMMENDED_DS)
                .addColumn(blockColumn("checkbox", "checkboxBlockColumn").hasBlockHeader(true).visible("ui:enrollmentVisible").required(true).create())
                .addColumn(textColumn("competitionKind", IEcgRecommendedDTO.COMPETITION_KIND_TITLE).create())
                .addColumn(textColumn("targetAdmissionKind", IEcgRecommendedDTO.TARGET_ADMISSION_KIND_TITLE).create())
                .addColumn(textColumn("finalMark", IEcgRecommendedDTO.FINAL_MARK).formatter(DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS).create())
                .addColumn(textColumn("profileMark", IEcgRecommendedDTO.PROFILE_MARK).formatter(DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS).create())
                .addColumn(textColumn("graduatedProfileEduInstitution", IEcgRecommendedDTO.GRADUATED_PROFILE_EDU_INSTITUTION_TITLE).create())
                .addColumn(textColumn("certificateAverageMark", IEcgRecommendedDTO.CERTIFICATE_AVERAGE_MARK).formatter(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).create())
                .addColumn(textColumn("fio", IEcgRecommendedDTO.ENTRANT_RECOMMENDED + "." + EcgEntrantRecommended.direction().entrantRequest().entrant().person().identityCard().fullFio().s()))
                .addColumn(textColumn("direction", IEcgRecommendedDTO.ENTRANT_RECOMMENDED + "." + EcgEntrantRecommended.direction().enrollmentDirection().title().s()))
                .addColumn(textColumn("developForm", IEcgRecommendedDTO.ENTRANT_RECOMMENDED + "." + EcgEntrantRecommended.direction().enrollmentDirection().educationOrgUnit().developForm().title().s()).create())
                .addColumn(textColumn("developCondition", IEcgRecommendedDTO.ENTRANT_RECOMMENDED + "." + EcgEntrantRecommended.direction().enrollmentDirection().educationOrgUnit().developCondition().title().s()).create())
                .addColumn(textColumn("developTech", IEcgRecommendedDTO.ENTRANT_RECOMMENDED + "." + EcgEntrantRecommended.direction().enrollmentDirection().educationOrgUnit().developTech().title().s()).create())
                .addColumn(textColumn("developPeriod", IEcgRecommendedDTO.ENTRANT_RECOMMENDED + "." + EcgEntrantRecommended.direction().enrollmentDirection().educationOrgUnit().developPeriod().title().s()).create())
                .addColumn(textColumn("priorities", IEcgRecommendedDTO.PRIORITIES).create())
                .addColumn(textColumn("originals", IEcgRecommendedDTO.ORIGINAL_DOCUMENT_HANDED_IN_TITLE).create())
                .addColumn(blockColumn("orderType", "orderTypeBlockColumn").visible("ui:enrollmentVisible").required(true).create())
                .addColumn(textColumn("enrollmentConditions", IEcgRecommendedPreStudentDTO.ENROLLMENT_CONDITIONS).visible("ui:enrollmentVisible").required(true).create())
                .addColumn(indicatorColumn("edit").path(IEcgRecommendedPreStudentDTO.PRE_ENROLL_VISIBLE)
                        .addIndicator(true, new IndicatorColumn.Item(CommonDefines.ICON_EDIT.getName(), CommonDefines.ICON_EDIT.getLabel(), EDIT_LISTENER)).clickable(true)
                        .disabled(IEcgRecommendedPreStudentDTO.CHANGE_ENROLLMENT_CONDITION_DISABLED).required(true).visible("ui:enrollmentVisible").permissionKey("distributionSectEdit").create())
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER, alert("recommendedDS.delete.alert", IEcgRecommendedDTO.ENTRANT_RECOMMENDED + "." + EcgEntrantRecommended.direction().entrantRequest().entrant().person().identityCard().fullFio().s())).disabled("ui:removeRecommendedDisabled").permissionKey("distributionSectEdit").create())
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> directionDSHandler()
    {
        return new EcSimpleDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> recommendedDSHandler()
    {
        return new EcSimpleDSHandler(getName());
    }
}
