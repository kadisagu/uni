/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcPreEnroll.ui.List;

import com.google.common.collect.ImmutableList;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.IUniecComponents;
import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager;
import ru.tandemservice.uniec.base.bo.EcPreEnroll.EcPreEnrollManager;
import ru.tandemservice.uniec.base.bo.EcPreEnroll.logic.EcPreEnrollParam;
import ru.tandemservice.uniec.base.bo.EcPreEnroll.logic.PreEntrantDSHandler;
import ru.tandemservice.uniec.base.bo.EcPreEnroll.util.PreEntrantDTO;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignModel;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.ui.EnrollmentDirectionUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Vasily Zhukov
 * @since 26.05.2011
 */
public class EcPreEnrollListUI extends UIPresenter implements IEnrollmentCampaignModel
{
    private static final String ENROLLMENT_CAMPAIGN_SETTING = "enrollmentCampaign";
    private static final String FORMATIVE_ORG_UNIT_SETTING = "formativeOrgUnit";
    private static final String TERRITORIAL_ORG_UNIT_SETTING = "territorialOrgUnit";
    private static final String EDUCATION_LEVELS_HIGH_SCHOOL_SETTING = "educationLevelsHighSchool";
    private static final String DEVELOP_FORM_SETTING = "developForm";
    private static final String DEVELOP_CONDITION_SETTING = "developCondition";
    private static final String DEVELOP_TECH_SETTING = "developTech";
    private static final String DEVELOP_PERIOD_SETTING = "developPeriod";
    private static final String AGREE4_ENROLLMENT_SETTING = "agree4Enrollment";
    public static final String ENTRANT_CUSTOM_STATE_EXIST_SETTING = "entrantCustomStateExist";
    public static final String ENTRANT_CUSTOM_STATE_LIST_SETTING = "entrantCustomStateList";

    private ISelectModel _compensationTypeModel;
    private ISelectModel _studentCategoryModel;
    private ISelectModel _orderTypeModel;
    private ISelectModel _formativeOrgUnitModel;
    private ISelectModel _territorialOrgUnitModel;
    private ISelectModel _educationLevelsHighSchoolModel;
    private ISelectModel _developFormModel;
    private ISelectModel _developTechModel;
    private ISelectModel _developConditionModel;
    private ISelectModel _developPeriodModel;

    private EnrollmentDirection _enrollmentDirection;
    private int _preStudentCount;
    private boolean _hasAdmissionKind;

    @Override
    public void onComponentRefresh()
    {
        ICommonDAO dao = DataAccessServices.dao();

        _compensationTypeModel = new LazySimpleSelectModel<>(CompensationType.class, CompensationType.P_SHORT_TITLE).setSortProperty(CompensationType.P_CODE).setSearchProperty(CompensationType.P_SHORT_TITLE);
        _studentCategoryModel = new LazySimpleSelectModel<>(dao.getList(StudentCategory.class, StudentCategory.P_CODE));
        _orderTypeModel = new LazySimpleSelectModel<>(EcOrderManager.instance().dao().getOrderTypeList(getEnrollmentCampaign()), EntrantEnrollmentOrderType.P_SHORT_TITLE);
        _formativeOrgUnitModel = EnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(null, this);
        _territorialOrgUnitModel = EnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(null, this);
        _educationLevelsHighSchoolModel = EnrollmentDirectionUtil.createEducationLevelsHighSchoolModel(this);
        _developFormModel = EnrollmentDirectionUtil.createDevelopFormModel(this);
        _developTechModel = EnrollmentDirectionUtil.createDevelopTechModel(this);
        _developPeriodModel = EnrollmentDirectionUtil.createDevelopPeriodModel(this);
        _developConditionModel = EnrollmentDirectionUtil.createDevelopConditionModel(this);

        _hasAdmissionKind = ISharedBaseDao.instance.get().existsEntity(TargetAdmissionKind.class);

        // init default values
        initDefaults();
    }

    private void initDefaults()
    {
        _enrollmentDirection = EnrollmentDirectionUtil.getEnrollmentDirection(this, DataAccessServices.dao().getComponentSession());

        // если приемная кампания не выбрана, то устанавливаем последнюю приемную кампанию, если она есть
        if (getEnrollmentCampaign() == null)
        {
            List<EnrollmentCampaign> list = DataAccessServices.dao().getList(EnrollmentCampaign.class, EnrollmentCampaign.P_ID);
            if (list != null && !list.isEmpty())
                _uiSettings.set(ENROLLMENT_CAMPAIGN_SETTING, list.get(list.size() - 1));
        }

        // если территориальное подр. не выбрано, то устанавливаем академию
        if (getTerritorialOrgUnit() == null)
            _uiSettings.set(TERRITORIAL_ORG_UNIT_SETTING, TopOrgUnit.getInstance());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (EcPreEnrollList.PRE_ENTRANT_DS.equals(dataSource.getName()))
        {
            _enrollmentDirection = EnrollmentDirectionUtil.getEnrollmentDirection(this, DataAccessServices.dao().getComponentSession());

            EcPreEnrollParam params = new EcPreEnrollParam();
            params.setEnrollmentDirection(_enrollmentDirection);
            params.setCompensationType(_uiSettings.get("compensationType"));
            params.setStudentCategoryList( _uiSettings.get("studentCategoryList"));
            params.setOriginalDocumentHandledIn(TwinComboDataSourceHandler.getSelectedValue(_uiSettings.get("originalDocument")));
            params.setAgree4Enrollment(TwinComboDataSourceHandler.getSelectedValue(_uiSettings.get(AGREE4_ENROLLMENT_SETTING)));
            params.setEntrantCustomStateExist(TwinComboDataSourceHandler.getSelectedValue(_uiSettings.get(ENTRANT_CUSTOM_STATE_EXIST_SETTING)));
            params.setEntrantCustomStateList(_uiSettings.get(ENTRANT_CUSTOM_STATE_LIST_SETTING));

            dataSource.put(PreEntrantDSHandler.PARAM, params);
        }
    }

    @Override
    public void onAfterDataSourceFetch(IUIDataSource dataSource)
    {
        if (EcPreEnrollList.PRE_ENTRANT_DS.equals(dataSource.getName()))
        {
            List<PreEntrantDTO> list = dataSource.getRecords();

            _preStudentCount = list.isEmpty() ? 0 : list.get(0).getPreStudentCount();

            Map<Long, EntrantEnrollmentOrderType> valueMap = new HashMap<>();
            list.stream()
                    .filter(item -> item.getPreStudent() != null)
                    .forEach(item -> valueMap.put(item.getId(), item.getPreStudent().getEntrantEnrollmentOrderType()));

            @SuppressWarnings("unchecked")
            BlockColumn<EntrantEnrollmentOrderType> blockColumn = (BlockColumn<EntrantEnrollmentOrderType>) ((PageableSearchListDataSource) dataSource).getLegacyDataSource().getColumn("orderType");
            blockColumn.setValueMap(valueMap);
        }
    }

    public String getPlanCaption()
    {
        StringBuilder caption = new StringBuilder("План приема по ");
        if (null != getEducationLevelsHighSchool())
            caption.append(getEducationLevelsHighSchool().isSpeciality() ? "специальности " : "направлению подготовки ").append(getEducationLevelsHighSchool().getDisplayableTitle());
        if (null != getTerritorialOrgUnit())
            caption.append(" (").append(getTerritorialOrgUnit().getTerritorialTitle()).append(")");
        return caption.toString();
    }

    public String getListTitle()
    {
        return "Список абитуриентов, успешно прошедших вступительные испытания (из них предварительно зачислено " + _preStudentCount + ")";
    }

    // IEnrollmentCampaignModel

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _uiSettings.get(ENROLLMENT_CAMPAIGN_SETTING);
    }

    @Override
    public OrgUnit getFormativeOrgUnit()
    {
        return _uiSettings.get(FORMATIVE_ORG_UNIT_SETTING);
    }

    @Override
    public OrgUnit getTerritorialOrgUnit()
    {
        return _uiSettings.get(TERRITORIAL_ORG_UNIT_SETTING);
    }

    @Override
    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _uiSettings.get(EDUCATION_LEVELS_HIGH_SCHOOL_SETTING);
    }

    @Override
    public DevelopForm getDevelopForm()
    {
        return _uiSettings.get(DEVELOP_FORM_SETTING);
    }

    @Override
    public DevelopCondition getDevelopCondition()
    {
        return _uiSettings.get(DEVELOP_CONDITION_SETTING);
    }

    @Override
    public DevelopTech getDevelopTech()
    {
        return _uiSettings.get(DEVELOP_TECH_SETTING);
    }

    @Override
    public DevelopPeriod getDevelopPeriod()
    {
        return _uiSettings.get(DEVELOP_PERIOD_SETTING);
    }

    // Getters & Setters

    public ISelectModel getCompensationTypeModel()
    {
        return _compensationTypeModel;
    }

    public void setCompensationTypeModel(ISelectModel compensationTypeModel)
    {
        _compensationTypeModel = compensationTypeModel;
    }

    public ISelectModel getStudentCategoryModel()
    {
        return _studentCategoryModel;
    }

    public void setStudentCategoryModel(ISelectModel studentCategoryModel)
    {
        _studentCategoryModel = studentCategoryModel;
    }

    public ISelectModel getOrderTypeModel()
    {
        return _orderTypeModel;
    }

    public void setOrderTypeModel(ISelectModel orderTypeModel)
    {
        _orderTypeModel = orderTypeModel;
    }

    public ISelectModel getFormativeOrgUnitModel()
    {
        return _formativeOrgUnitModel;
    }

    public void setFormativeOrgUnitModel(ISelectModel formativeOrgUnitModel)
    {
        _formativeOrgUnitModel = formativeOrgUnitModel;
    }

    public ISelectModel getTerritorialOrgUnitModel()
    {
        return _territorialOrgUnitModel;
    }

    public void setTerritorialOrgUnitModel(ISelectModel territorialOrgUnitModel)
    {
        _territorialOrgUnitModel = territorialOrgUnitModel;
    }

    public ISelectModel getEducationLevelsHighSchoolModel()
    {
        return _educationLevelsHighSchoolModel;
    }

    public void setEducationLevelsHighSchoolModel(ISelectModel educationLevelsHighSchoolModel)
    {
        _educationLevelsHighSchoolModel = educationLevelsHighSchoolModel;
    }

    public ISelectModel getDevelopFormModel()
    {
        return _developFormModel;
    }

    public void setDevelopFormModel(ISelectModel developFormModel)
    {
        _developFormModel = developFormModel;
    }

    public ISelectModel getDevelopTechModel()
    {
        return _developTechModel;
    }

    public void setDevelopTechModel(ISelectModel developTechModel)
    {
        _developTechModel = developTechModel;
    }

    public ISelectModel getDevelopConditionModel()
    {
        return _developConditionModel;
    }

    public void setDevelopConditionModel(ISelectModel developConditionModel)
    {
        _developConditionModel = developConditionModel;
    }

    public ISelectModel getDevelopPeriodModel()
    {
        return _developPeriodModel;
    }

    public void setDevelopPeriodModel(ISelectModel developPeriodModel)
    {
        _developPeriodModel = developPeriodModel;
    }

    public EnrollmentDirection getEnrollmentDirection()
    {
        return _enrollmentDirection;
    }

    public void setEnrollmentDirection(EnrollmentDirection enrollmentDirection)
    {
        _enrollmentDirection = enrollmentDirection;
    }

    public int getPreStudentCount()
    {
        return _preStudentCount;
    }

    public void setPreStudentCount(int preStudentCount)
    {
        _preStudentCount = preStudentCount;
    }

    public boolean isHasAdmissionKind()
    {
        return _hasAdmissionKind;
    }

    public void setHasAdmissionKind(boolean hasAdmissionKind)
    {
        _hasAdmissionKind = hasAdmissionKind;
    }

    public boolean isEntrantCustomStateListDisabled()
    {
        Boolean value = TwinComboDataSourceHandler.getSelectedValue(_uiSettings.get(ENTRANT_CUSTOM_STATE_EXIST_SETTING));

        return value == null;
    }


    // Listeners

    public void onClickView()
    {
        Entrant entrant = DataAccessServices.dao().<RequestedEnrollmentDirection>getNotNull(getListenerParameterAsLong()).getEntrantRequest().getEntrant();
        ContextLocal.createDesktop(new PublisherActivator(entrant, new ParametersMap()
                .add("selectedTab", "entrantTab")
        ), null);
    }

    public void onClickShow()
    {
        saveSettings();
        _orderTypeModel = new LazySimpleSelectModel<>(EcOrderManager.instance().dao().getOrderTypeList(getEnrollmentCampaign()), EntrantEnrollmentOrderType.P_SHORT_TITLE);
        initDefaults();
    }

    public void onClickClear()
    {
        clearSettings();
        _enrollmentDirection = null;
        _preStudentCount = 0;
        initDefaults();
        saveSettings();
    }

    public void onChangeOrderType()
    {
        Long requestedEnrollmentDirectionId = getListenerParameterAsLong();

        PageableSearchListDataSource dataSource = _uiConfig.getDataSource(EcPreEnrollList.PRE_ENTRANT_DS);
        @SuppressWarnings("unchecked") BlockColumn<EntrantEnrollmentOrderType> typeBlockColumn = (BlockColumn<EntrantEnrollmentOrderType>) dataSource.getLegacyDataSource().getColumn("orderType");
        EntrantEnrollmentOrderType orderType = typeBlockColumn.getValueMap().get(requestedEnrollmentDirectionId);

        EducationOrgUnit educationOrgUnit = _enrollmentDirection.getEducationOrgUnit();

        EcPreEnrollManager.instance().dao().doChangeOrderType(requestedEnrollmentDirectionId, educationOrgUnit, orderType);
    }

    public void onClickTransfer()
    {
        if (_enrollmentDirection == null)
            throw new ApplicationException("Не выбраны направления приема.");

        List<Long> ids = ImmutableList.of(_enrollmentDirection.getId());
        getConfig().getBusinessComponent().createDefaultChildRegion(new ComponentActivator(IUniecComponents.ENTRANT_TRANSFER, new ParametersMap().add("enrollmentDirectionIds", ids)));
    }

    public void onEditEntityFromList()
    {
        Long requestedEnrollmentDirectionId = getListenerParameterAsLong();

        RequestedEnrollmentDirection direction = DataAccessServices.dao().getNotNull(requestedEnrollmentDirectionId);

        PreliminaryEnrollmentStudent preStudent = DataAccessServices.dao().get(PreliminaryEnrollmentStudent.class, PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, direction);

        if (preStudent == null) return;

        getConfig().getBusinessComponent().createDefaultChildRegion(new ComponentActivator(IUniecComponents.ENTRANT_ENROLLMENT_CONDITITONS_EDIT, new ParametersMap().add("preliminaryEnrollmentStudentId", preStudent.getId())));
    }

    public void onChangeEntrantCustomStateExist()
    {
        if(_uiSettings.get(ENTRANT_CUSTOM_STATE_EXIST_SETTING) == null) _uiSettings.set(ENTRANT_CUSTOM_STATE_LIST_SETTING, null);
    }
}
