package ru.tandemservice.uniec.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.translator.MSSQLTranslator;
import org.tandemframework.dbsupport.sql.translator.OracleTranslator;
import org.tandemframework.dbsupport.sql.translator.PostgresTranslator;

import java.util.List;

import static org.tandemframework.shared.commonbase.utils.MigrationUtils.processor;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniec_2x10x2_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность onlineEntrant

        // создано обязательное свойство registrationYear
        {
            // создать колонку
            tool.createColumn("onlineentrant_t", new DBColumn("registrationyear_p", DBType.INTEGER));

            // задать значение по умолчанию
            ISQLTranslator translator = tool.getDialect().getSQLTranslator();
            if (translator instanceof MSSQLTranslator)
                tool.executeUpdate("UPDATE onlineentrant_t SET registrationyear_p=YEAR(registrationdate_p) WHERE registrationyear_p IS null");
            else if (translator instanceof OracleTranslator)
                tool.executeUpdate("UPDATE onlineentrant_t SET registrationyear_p=EXTRACT(YEAR FROM registrationdate_p) WHERE registrationyear_p IS null");
            else if (translator instanceof PostgresTranslator)
                tool.executeUpdate("UPDATE onlineentrant_t SET registrationyear_p=EXTRACT(YEAR FROM registrationdate_p) WHERE registrationyear_p IS null");


            recalculatePersonalNumbers(tool);

            // сделать колонку NOT NULL
            tool.setColumnNullable("onlineentrant_t", "registrationyear_p", false);
        }

        // создано свойство methodDeliveryNReturnDocs
        {
            // создать колонку
            tool.createColumn("onlineentrant_t", new DBColumn("methoddeliverynreturndocs_id", DBType.LONG));
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность onlineEntrantCertificateMark

        // создано свойство year
        {
            // создать колонку
            tool.createColumn("onlineentrantcertificatemark_t", new DBColumn("year_p", DBType.INTEGER));
        }
    }

    private void recalculatePersonalNumbers(DBTool tool) throws Exception
    {
        //года в которых есть абитуриенты с дублирующимися омерами
        final List<Object[]> years = tool.executeQuery(
                processor(Integer.class),
                "SELECT t1.registrationyear_p" +
                        " FROM (select" +
                        " personalnumber_p as pNumber, registrationyear_p as pYear" +
                        " FROM onlineentrant_t GROUP BY personalnumber_p, registrationyear_p having count(*)>1" +
                        " ) t2" +
                        " JOIN onlineentrant_t t1 ON t1.personalnumber_p = t2.pNumber AND t1.registrationyear_p = t2.pYear" +
                        " GROUP BY t1.registrationyear_p"
        );

        if (!years.isEmpty())
            tool.table("onlineentrant_t").uniqueKeys().clear();

        for (Object[] o : years)
        {
            Integer year = (Integer) o[0];

            tool.executeUpdate(";WITH t AS (SELECT personalnumber_p," +
                                       " registrationyear_p," +
                                       " Row_number() OVER (ORDER BY registrationdate_p ) AS pNumber" +
                                       " FROM onlineentrant_t WHERE registrationyear_p = ?)" +

                                       " UPDATE t SET personalnumber_p = pNumber",
                               year);

            tool.debug("Recalculated personalNumbers of onlineEntrant for year " + String.valueOf(year));
        }

    }
}