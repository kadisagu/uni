/**
 *$Id$
 */
package ru.tandemservice.uniec.base.bo.EcReport.logic.EnrollmentDataCollect;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.report.EnrollmentDataCollectReport;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 15.08.13
 */
public class EnrollmentDataCollectReportSearchDSHandler extends DefaultSearchDataSourceHandler
{
    public EnrollmentDataCollectReportSearchDSHandler(String ownerId)
    {
        super(ownerId);
    }

    public static final String BIND_ENR_CAMP = "bindEnrCamp";

    public static final String V_PROP_FROM_TO = "vPropFromTo";

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrollmentDataCollectReport.class, "r").column("r");
        filter(dql, "r", input, context);
        final DSOutput output = DQLSelectOutputBuilder.get(input, dql, context.getSession()).order().pageable(true).build();
        return wrap(output, input, context);
    }

    protected DSOutput wrap(DSOutput output, DSInput input, ExecutionContext context)
    {
        for (DataWrapper wrapper : DataWrapper.wrap(output))
        {
            final EnrollmentDataCollectReport report = wrapper.getWrapped();

            wrapper.setProperty(V_PROP_FROM_TO, DateFormatter.DEFAULT_DATE_FORMATTER.format(report.getRequestDateFrom()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(report.getRequestDateTo()));
        }

        return output;
    }

    /**
     * @param dql builder from EnrollmentDataCollectReport 
     */
    protected void filter(DQLSelectBuilder dql, String alias, DSInput input, ExecutionContext context)
    {
        final EnrollmentCampaign enrCamp = context.get(BIND_ENR_CAMP);

        if (enrCamp != null)
            dql.where(eq(property(EnrollmentDataCollectReport.enrollmentCampaign().fromAlias(alias)), value(enrCamp)));
    }
}
