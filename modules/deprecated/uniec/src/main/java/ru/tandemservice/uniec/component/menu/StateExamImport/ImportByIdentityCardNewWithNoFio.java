package ru.tandemservice.uniec.component.menu.StateExamImport;

import jxl.Workbook;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.*;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.person.base.entity.gen.IdentityCardGen;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec.entity.catalog.StateExamType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate;
import ru.tandemservice.uniec.entity.entrant.StateExamSubjectMark;
import ru.tandemservice.uniec.ui.EntrantStateExamCertificateNumberFormatter;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author vdanilov
 */
public class ImportByIdentityCardNewWithNoFio extends UniBaseDao implements IImportByIdentityCard {

    public static final SpringBeanCache<IImportByIdentityCard> instance = new SpringBeanCache<IImportByIdentityCard>(ImportByIdentityCardNewWithNoFio.class.getSimpleName());

    private static final String SEPARATOR = "%";
    private static final String FILE_CHARSET = "Windows-1251";

    private static final String COMMENT_ROW_PREFIX = "Комментарий:";

    private static final Pattern VALUE_ERROR_PATTERN = Pattern.compile("ОШИБКА([0-9,. ]*)[(]([0-9,. ]*)[)]");

    private static final String STATUS_NOT_FOUND = "НЕ НАЙДЕНО";
    private static final String STATUS_GOOD = "Действительно";
    private static final String STATUS_NOT_CERT = "Нет свидетельства";

    private static final String REPLY_ENTRANT_NOT_FOUND = "АБИТУРИЕНТА НЕТ В СИСТЕМЕ";
    private static final String REPLY_ENTRANT_DOUBLED = "ДУБЛЬ ПЕРСОНЫ";
    private static final String REPLY_FALSE = "ЛОЖНЫЕ СВИДЕТЕЛЬСТВА";
    private static final String REPLY_CERT_ADDED = "СОЗДАНО НОВОЕ";
    private static final String REPLY_MARKS_ADDED = "ДОБАВЛЕНЫ БАЛЛЫ";
    private static final String REPLY_MARKS_CHANGED = "ИЗМЕНЕНЫ БАЛЛЫ";
    private static final String REPLY_MARKS_DELETED = "УДАЛЕНЫ БАЛЛЫ";
    private static final String REPLY_APPROVED = "ПРОВЕРЕНО";
    private static final String REPLY_QUESTIONED = "ПОД ВОПРОСОМ";
    private static final String REPLY_DELETED = "УДАЛЕНО";
    private static final String REPLY_LOSER = "ДВОЕЧНИК";

    private static final int CERT_NUM_IDX = 0;
    private static final int ID_CARD_SERIA_IDX = 2;
    private static final int ID_CARD_NUMBER_IDX = 3;
    private static final int STATUS_IDX = 6;
    private static final int MARK_SECTION_IDX = 5;

    private static final String ZERO_NUMBER = "000000000";

    @Override
    public String getTitle() {
        return "По данным удостоверения личности (без ФИО) (ФБС)";
    }

    @Override
    public byte[] doImport(final EnrollmentCampaign enrollmentCampaign, final boolean deleteNotFound, byte[] importedContent) {
        return doExecuteImportByIdentityCard(enrollmentCampaign, deleteNotFound, importedContent);
    }

    public byte[] doExecuteImportByIdentityCard(final EnrollmentCampaign enrollmentCampaign, final boolean deleteNotFound, byte[] importedContent)
    {
        try {

            final Session session = getSession();

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            WritableWorkbook workbook = Workbook.createWorkbook(out);

            WritableSheet sheet = workbook.createSheet("Результат импорта", 0);
            sheet.setColumnView(CERT_NUM_IDX, 16);
            sheet.setColumnView(STATUS_IDX, 16);

            WritableCellFormat FORMAT_RED = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 10, WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.RED));

            // подготовим данные о свидетельствах в базе
            StateExamType firstWave = get(StateExamType.class, StateExamType.code().s(), UniecDefines.STATE_EXAM_TYPE_FIRST);
            final List<StateExamSubject> subjects = getCatalogItemListOrderByCode(StateExamSubject.class);
            final Map<MultiKey, Long> entrantMap = new HashMap<MultiKey, Long>();
            final Map<Long, String[]> entrantDataMap = new HashMap<Long, String[]>();
            final Map<Long, Map<String, Set<Long>>> certificateMap = new HashMap<Long, Map<String, Set<Long>>>();
            final Map<MultiKey, PairKey<Long, Integer>> markMap = new HashMap<MultiKey, PairKey<Long, Integer>>();
            final HashSet<MultiKey> doubledPersons = new HashSet<MultiKey>();
            prepareCertificateData(enrollmentCampaign, entrantMap, entrantDataMap, certificateMap, markMap, doubledPersons);

            final Set<Long> checkedEntrantIds = new HashSet<Long>();
            final Set<Long> statusNotFoundEntrantIds = new HashSet<Long>();
            final Set<Long> approvedCertificateIds = new HashSet<Long>();
            final Set<String> uniqCertNumberSet = new HashSet<>();

            int rownum = 1;

            // сверяем по файлу
            try
            {
                BufferedReader in = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(importedContent), FILE_CHARSET));
                String header = in.readLine();
                String[] headerItems = header.split(SEPARATOR);

                fillRow(sheet, headerItems, 0, null);

                while (true)
                {
                    String line = in.readLine();
                    if (line == null) break;

                    if (line.startsWith(COMMENT_ROW_PREFIX))
                    {
                        sheet.addCell(new Label(0, rownum++, line));
                        continue;
                    }
                    if (line.length() == 0)
                    {
                        continue;
                    }
                    String[] row = line.split(SEPARATOR);

                    // найдем такого абитуриента в данных из базы
                    String idcSeria = row[ID_CARD_SERIA_IDX];
                    if (!StringUtils.isEmpty(idcSeria))
                        idcSeria = String.format("%4s", idcSeria).replaceAll(" ", "0");
                    String idcNumber = row[ID_CARD_NUMBER_IDX];
                    if (!StringUtils.isEmpty(idcNumber))
                        idcNumber = String.format("%6s", idcNumber).replaceAll(" ", "0");
                    MultiKey entrantKey = new MultiKey(idcSeria, idcNumber);

                    // если персона дублирована - отмечаем
                    if (doubledPersons.contains(entrantKey))
                    {
                        fillRow(sheet, row, rownum, FORMAT_RED);
                        ((Label) sheet.getCell(STATUS_IDX, rownum++)).setString(REPLY_ENTRANT_DOUBLED);
                        continue;
                    }

                    Long entrantId = entrantMap.get(entrantKey);

                    // не нашли - помечаем строку
                    if (null == entrantId)
                    {
                        fillRow(sheet, row, rownum, FORMAT_RED);
                        ((Label) sheet.getCell(STATUS_IDX, rownum++)).setString(REPLY_ENTRANT_NOT_FOUND);
                        continue;
                    }
                    checkedEntrantIds.add(entrantId);

                    // нашли абитуриента, смотрим статус строки в файле ФБС
                    String status = row[STATUS_IDX];

                    // в ФБС свидетельств для абитуриента нет
                    if (STATUS_NOT_FOUND.equalsIgnoreCase(status))
                    {
                        // а у нас в базе есть - печатаем строки для этих свидетельств
                        if (certificateMap.containsKey(entrantId))
                        {
                            statusNotFoundEntrantIds.add(entrantId);
                            Map<String, Set<Long>> entrantCertificateMap = certificateMap.get(entrantId);
                            String[] entrantData = entrantDataMap.get(entrantId);
                            if (null != entrantCertificateMap && !entrantCertificateMap.isEmpty())
                            {
                                List<String> certificateNumbers = new ArrayList<String>(entrantCertificateMap.keySet());
                                Collections.sort(certificateNumbers);
                                for (String certNumber : certificateNumbers)
                                {
                                    for (Long certificateId : entrantCertificateMap.get(certNumber))
                                    {
                                        printCertificateRow(sheet, rownum, certificateId, certNumber, subjects, null, entrantData, REPLY_FALSE);
                                        for (int i = 0; i <= STATUS_IDX; i++)
                                            if (Label.class.isInstance(sheet.getCell(i, rownum)))
                                                ((Label) sheet.getCell(i, rownum)).setCellFormat(FORMAT_RED);
                                        rownum++;
                                    }
                                }
                            }
                        }
                        // и у нас в базе нет - печатаем строку без изменений
                        else
                            fillRow(sheet, row, rownum++, null);
                        continue;
                    }

                    String certNumber = row[CERT_NUM_IDX].replaceAll("-", "");

                    // не знаем такого варианта ответа ФБС - печатаем строку без изменений
                    if (!STATUS_GOOD.equalsIgnoreCase(status))
                    {
                        fillRow(sheet, row, rownum++, null);
                        continue;
                    }

                    // в строке есть "Нет свидетельства", такие строки при импорте пропускаются
                    if (StringUtils.containsIgnoreCase(line, STATUS_NOT_CERT))
                    {
                        fillRow(sheet, row, rownum, FORMAT_RED);
                        ((Label) sheet.getCell(STATUS_IDX, rownum++)).setString(REPLY_LOSER);
                        continue;
                    }

                    // проверяем файл на дубли сертификатов
                    if (!uniqCertNumberSet.add(certNumber))
                        throw new ApplicationException("Невозможно импортировать файл, т.к. свидетельство с номером " + new EntrantStateExamCertificateNumberFormatter().format(certNumber) + " дублируется.");

                    // статус строки "Действительно", ищем такое свидетельство в данных из базы
                    Set<Long> certIds = certificateMap.containsKey(entrantId) ? certificateMap.get(entrantId).get(certNumber) : null;

                    // нашли - ставим флаги и исправляем оценки в найденном свидетельстве
                    if (null != certIds)
                    {
                        certificateMap.get(entrantId).remove(certNumber);
                        boolean markChanged = false;
                        boolean markDeleted = false;
                        boolean markAdded = false;
                        if (certIds.size() != 1)
                            throw new ApplicationException("Невозможно импортировать файл, т.к. у абитуриента " + get(Entrant.class, entrantId).getPerson().getFullFio() + " свидетельство с номером " + get(EntrantStateExamCertificate.class, certIds.iterator().next()).getTitle() + " дублируется в базе данных. Удалите дубликаты и повторите процедуру импорта.");

                        for (Long certId : certIds) // на самом деле оно тут должно быть одно, потому что заданный номер свидетельства (не нули) уникален
                        {
                            approvedCertificateIds.add(certId);
                            for (StateExamSubject subject : subjects)
                            {
                                MultiKey markKey = new MultiKey(certId, subject.getCode());
                                PairKey<Long, Integer> markData = markMap.get(markKey);
                                int actualMarkValue = getMark(row, subject);
                                // у нас оценки нет, а в файле есть - добавляем
                                if (markData == null && actualMarkValue != 0)
                                {
                                    StateExamSubjectMark mark = new StateExamSubjectMark();
                                    mark.setCertificate((EntrantStateExamCertificate) session.load(EntrantStateExamCertificate.class, certId));
                                    mark.setSubject(subject);
                                    mark.setMark(actualMarkValue);
                                    session.save(mark);
                                    markAdded = true;
                                }
                                // у нас есть оценка, а в файле нет - удаляем
                                else if (markData != null && actualMarkValue == 0)
                                {
                                    StateExamSubjectMark mark = get(StateExamSubjectMark.class, markData.getFirst());
                                    session.delete(mark);
                                    markDeleted = true;
                                }
                                // у нас есть оценка, и в файле другая - исправляем
                                else if (markData != null && markData.getSecond() != actualMarkValue)
                                {
                                    StateExamSubjectMark mark = get(StateExamSubjectMark.class, markData.getFirst());
                                    mark.setMark(actualMarkValue);
                                    session.update(mark);
                                    markChanged = true;
                                }
                            }
                        }
                        // печатаем строку с нужной пометкой
                        fillRow(sheet, row, rownum, null);

                        Collection<String> replies = new ArrayList<String>(4);
                        if (markAdded) { replies.add(REPLY_MARKS_ADDED); }
                        if (markChanged) { replies.add(REPLY_MARKS_CHANGED); }
                        if (markDeleted) { replies.add(REPLY_MARKS_DELETED); }
                        ((Label) sheet.getCell(STATUS_IDX, rownum++)).setString(replies.isEmpty() ? REPLY_APPROVED : StringUtils.join(replies, ", "));
                    }
                    // не нашли свидетельство - создаем новое
                    else
                    {
                        EntrantStateExamCertificate certificate = new EntrantStateExamCertificate();
                        certificate.setEntrant(get(Entrant.class, entrantId));
                        certificate.setNumber(certNumber);
                        certificate.setAccepted(true);
                        certificate.setSent(true);
                        certificate.setRegistrationDate(new Date());
                        certificate.setStateExamType(firstWave);
                        session.save(certificate);
                        for (StateExamSubject subject : subjects)
                        {
                            int actualMarkValue = getMark(row, subject);
                            if (actualMarkValue != 0)
                            {
                                StateExamSubjectMark mark = new StateExamSubjectMark();
                                mark.setCertificate(certificate);
                                mark.setSubject(subject);
                                mark.setMark(actualMarkValue);
                                session.save(mark);
                            }
                        }
                        // печатаем строку с нужной пометкой
                        fillRow(sheet, row, rownum, null);
                        ((Label) sheet.getCell(STATUS_IDX, rownum++)).setString(REPLY_CERT_ADDED);
                    }
                }
            }
            catch (ApplicationException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                logger.error(e.getMessage(), e);
                if (Debug.isDisplay()) { throw e; }
                throw new ApplicationException("Формат файла некорректен, загрузка данных невозможна.");
            }

            // подтверждаем все свидетельства, которые надо подтвердить
            BatchUtils.execute(approvedCertificateIds, 500, new BatchUtils.Action<Long>() {
                @Override public void execute(Collection<Long> elements) {
                    MQBuilder builder = new MQBuilder(EntrantStateExamCertificate.ENTITY_CLASS, "cert");
                    builder.add(MQExpression.in("cert", EntrantStateExamCertificate.id().s(), elements));
                    for (EntrantStateExamCertificate certificate : builder.<EntrantStateExamCertificate>getResultList(session))
                    {
                        certificate.setAccepted(true);
                        certificate.setSent(true);
                        session.update(certificate);
                    }
                }
            });


            // для абитуриентов, которых встретили в файле, собираем не встретившиеся в файле свидетельства
            Set<Long> notApprovedCertificateIds = new HashSet<Long>();
            List<Long> sortedEntrantIds = new ArrayList<Long>();
            for (Long entrantId : checkedEntrantIds)
            {
                Map<String, Set<Long>> entrantCertificateMap = certificateMap.get(entrantId);
                if (null != entrantCertificateMap && !entrantCertificateMap.isEmpty())
                {
                    for (String certNumber : entrantCertificateMap.keySet()) {
                        notApprovedCertificateIds.addAll(entrantCertificateMap.get(certNumber));
                    }
                    sortedEntrantIds.add(entrantId);
                }
            }
            // снимаем флаги подтверждения с этих свидетельств, или удаляем
            BatchUtils.execute(notApprovedCertificateIds, 500, new BatchUtils.Action<Long>() {
                @Override public void execute(Collection<Long> elements) {
                    MQBuilder builder = new MQBuilder(EntrantStateExamCertificate.ENTITY_CLASS, "cert");
                    builder.add(MQExpression.in("cert", EntrantStateExamCertificate.id().s(), elements));
                    for (EntrantStateExamCertificate certificate : builder.<EntrantStateExamCertificate>getResultList(session))
                    {
                        if (doDeleteCertificate(deleteNotFound, certificate.getNumber(), certificate.getEntrant().getId(), statusNotFoundEntrantIds)) {
                            session.delete(certificate);
                        } else {
                            certificate.setAccepted(false);
                            certificate.setSent(false);
                            session.update(certificate);
                        }
                    }
                }
            });
            // теперь печатаем строки в файл по этим свидетельствам
            Collections.sort(sortedEntrantIds, new Comparator<Long>() {
                @Override public int compare(Long o1, Long o2) {
                    return entrantDataMap.get(o1)[0].compareTo(entrantDataMap.get(o2)[0]);
                }
            });
            // сначала печатаем ненулевые
            for (Long entrantId : sortedEntrantIds)
            {
                Map<String, Set<Long>> entrantCertificateMap = certificateMap.get(entrantId);
                String[] entrantData = entrantDataMap.get(entrantId);
                if (null != entrantCertificateMap && !entrantCertificateMap.isEmpty())
                {
                    List<String> certificateNumbers = new ArrayList<String>(entrantCertificateMap.keySet());
                    Collections.sort(certificateNumbers);
                    for (String certNumber : certificateNumbers)
                    {
                        if (getActualNumber(certNumber).equals(ZERO_NUMBER)) continue;
                        for (Long certificateId : entrantCertificateMap.get(certNumber))
                            printCertificateRow(sheet, rownum++, certificateId, certNumber, subjects, markMap, entrantData, REPLY_QUESTIONED);
                    }
                }
            }
            // а потом нулевые
            for (Long entrantId : sortedEntrantIds)
            {
                Map<String, Set<Long>> entrantCertificateMap = certificateMap.get(entrantId);
                String[] entrantData = entrantDataMap.get(entrantId);
                if (null != entrantCertificateMap && !entrantCertificateMap.isEmpty())
                    for (String certNumber : entrantCertificateMap.keySet())
                    {
                        if (!getActualNumber(certNumber).equals(ZERO_NUMBER)) continue;
                        for (Long certificateId : entrantCertificateMap.get(certNumber))
                            printCertificateRow(sheet, rownum++, certificateId, certNumber, subjects, markMap, entrantData, doDeleteCertificate(deleteNotFound, certNumber, entrantId, statusNotFoundEntrantIds) ? REPLY_DELETED : REPLY_QUESTIONED);
                    }
            }

            workbook.write();
            workbook.close();

            return out.toByteArray();
        } catch (Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    private int getIntValue(String mark)
    {
        return mark.isEmpty() ? 0 : Double.valueOf(mark.replace(",", ".")).intValue();
    }

    private int getMark(String[] row, StateExamSubject subject)
    {
        int index = Integer.valueOf(subject.getCode()) * 2 + MARK_SECTION_IDX;
        String value = row[index].replace("!", "").toUpperCase();

        // обрабатываем ошибки (несовпадение того, что мы передавали, и того, что у них есть - в любом случае берем то, что у них есть)
        final Matcher m = VALUE_ERROR_PATTERN.matcher(value);
        if (m.matches()) {
            return getIntValue(m.group(2) /* то, что в скобках */);
        }
        // если ошибок нет - берем то, что есть
        return getIntValue(value);
    }

    private void printCertificateRow(WritableSheet sheet, int rownum, Long certificateId, String certNumber, List<StateExamSubject> subjects, Map<MultiKey, PairKey<Long, Integer>> markMap, String[] entrantData, String status)
    throws WriteException
    {
        sheet.addCell(new Label(CERT_NUM_IDX, rownum, StringUtils.join(new String[]{certNumber.substring(0, 2), certNumber.substring(2, 11), certNumber.substring(11)}, "-")));
        for (int i = 0; i < 2; i++)
            sheet.addCell(new Label(i + ID_CARD_SERIA_IDX, rownum, entrantData[i]));
        sheet.addCell(new Label(STATUS_IDX, rownum, status));
        if (null != markMap)
            for (StateExamSubject subject : subjects)
            {
                int index = Integer.valueOf(subject.getCode()) * 2 + MARK_SECTION_IDX;
                MultiKey markKey = new MultiKey(certificateId, subject.getCode());
                PairKey<Long, Integer> markData = markMap.get(markKey);
                sheet.addCell(new Label(index, rownum, markData == null ? "" : String.valueOf(markData.getSecond())));
                sheet.addCell(new Label(index + 1, rownum, "0"));
            }
    }

    private boolean doDeleteCertificate(boolean deleteNotFound, String certificateNumber, Long entrantId, Set<Long> statusNotFoundEntrantIds)
    {
        return deleteNotFound && getActualNumber(certificateNumber).equalsIgnoreCase(ZERO_NUMBER) && !statusNotFoundEntrantIds.contains(entrantId);
    }

    private String getActualNumber(String number)
    {
        return number.substring(2, 11);
    }

    private static void fillRow(WritableSheet sheet, String[] row, int rownum, WritableCellFormat format) throws Exception
    {
        for (int i = 0; i < row.length; i++)
        {
            if (format != null)
                sheet.addCell(new Label(i, rownum, row[i], format));
            else
                sheet.addCell(new Label(i, rownum, row[i]));
        }
    }

    private void prepareCertificateData(EnrollmentCampaign enrollmentCampaign,
                                        Map<MultiKey, Long> entrantMap,
                                        Map<Long, String[]> entrantDataMap,
                                        Map<Long, Map<String, Set<Long>>> certificateMap,
                                        Map<MultiKey, PairKey<Long, Integer>> markMap,
                                        Set<MultiKey> doubledPersons)
    {
        // оценки, сертификаты, абитуриенты
        {
            MQBuilder builder = new MQBuilder(StateExamSubjectMark.ENTITY_CLASS, "mark", new String[]{
                StateExamSubjectMark.id().s(),
                StateExamSubjectMark.subject().code().s(),
                StateExamSubjectMark.mark().s()});
            builder.addJoin("mark", StateExamSubjectMark.certificate().s(), "cert");
            builder.addSelect("cert", new Object[]{
                EntrantStateExamCertificate.id().s(),
                EntrantStateExamCertificate.number().s()});
            builder.addJoin("cert", EntrantStateExamCertificate.entrant().s(), "entrant");
            builder.add(MQExpression.eq("entrant", Entrant.enrollmentCampaign().s(), enrollmentCampaign));
            builder.addSelect("entrant", new Object[]{
                Entrant.id().s()});
            builder.addJoin("entrant", Entrant.person().identityCard().s(), "idCard");
            builder.addSelect("idCard", new String[]{
                IdentityCardGen.P_SERIA,
                IdentityCardGen.P_NUMBER});

            for (Object[] row : builder.<Object[]>getResultList(getSession()))
            {
                Long markId = (Long) row[0];
                String subjectCode = (String) row[1];
                Integer mark = (Integer) row[2];
                Long certId = (Long) row[3];
                String certNumber = (String) row[4];
                Long entrantId = (Long) row[5];
                String idcSeria = (String) row[6];
                String idcNumber = (String) row[7];

                MultiKey entrantKey = new MultiKey(idcSeria, idcNumber);
                MultiKey markKey = new MultiKey(certId, subjectCode);

                Long existing = entrantMap.get(entrantKey);
                if (null != existing && !existing.equals(entrantId))
                    doubledPersons.add(entrantKey);

                entrantMap.put(entrantKey, entrantId);
                entrantDataMap.put(entrantId, new String[]{ idcSeria, idcNumber});
                Map<String, Set<Long>> entrantCertMap = certificateMap.get(entrantId);
                if (null == entrantCertMap)
                    certificateMap.put(entrantId, entrantCertMap = new HashMap<String, Set<Long>>());
                Set<Long> certIdList = entrantCertMap.get(certNumber);
                if (null == certIdList)
                    entrantCertMap.put(certNumber, certIdList = new HashSet<Long>());
                certIdList.add(certId);
                markMap.put(markKey, new PairKey<Long, Integer>(markId, mark));
            }
        }

        // сертификаты без оценок ( О.о ), абитуриенты
        {
            MQBuilder builder = new MQBuilder(EntrantStateExamCertificate.ENTITY_CLASS, "cert", new String[]{
                EntrantStateExamCertificate.id().s(),
                EntrantStateExamCertificate.number().s()});
            builder.addJoin("cert", EntrantStateExamCertificate.entrant().s(), "entrant");
            builder.add(MQExpression.eq("entrant", Entrant.enrollmentCampaign().s(), enrollmentCampaign));
            builder.addSelect("entrant", new Object[]{
                Entrant.id().s()});
            builder.addJoin("entrant", Entrant.person().identityCard().s(), "idCard");
            builder.addSelect("idCard", new String[]{
                IdentityCardGen.P_SERIA,
                IdentityCardGen.P_NUMBER});

            for (Object[] row : builder.<Object[]>getResultList(getSession()))
            {
                Long certId = (Long) row[0];
                String certNumber = (String) row[1];
                Long entrantId = (Long) row[2];
                String idcSeria = (String) row[3];
                String idcNumber = (String) row[4];

                MultiKey entrantKey = new MultiKey(idcSeria, idcNumber);

                Long existing = entrantMap.get(entrantKey);
                if (null != existing && !existing.equals(entrantId))
                    doubledPersons.add(entrantKey);

                entrantMap.put(entrantKey, entrantId);
                entrantDataMap.put(entrantId, new String[]{idcSeria, idcNumber});
                Map<String, Set<Long>> entrantCertMap = certificateMap.get(entrantId);
                if (null == entrantCertMap)
                    certificateMap.put(entrantId, entrantCertMap = new HashMap<String, Set<Long>>());
                Set<Long> certIdList = entrantCertMap.get(certNumber);
                if (null == certIdList)
                    entrantCertMap.put(certNumber, certIdList = new HashSet<Long>());
                certIdList.add(certId);
            }
        }

        // абитуриенты без сертификатов
        {
            MQBuilder builder = new MQBuilder(Entrant.ENTITY_CLASS, "entrant", new String[]{
                Entrant.id().s()});
            builder.add(MQExpression.eq("entrant", Entrant.enrollmentCampaign().s(), enrollmentCampaign));
            builder.addJoin("entrant", Entrant.person().identityCard().s(), "idCard");
            builder.addSelect("idCard", new String[]{
                IdentityCardGen.P_SERIA,
                IdentityCardGen.P_NUMBER});

            for (Object[] row : builder.<Object[]>getResultList(getSession()))
            {
                Long entrantId = (Long) row[0];
                String idcSeria = (String) row[1];
                String idcNumber = (String) row[2];

                MultiKey entrantKey = new MultiKey(idcSeria, idcNumber);

                Long existing = entrantMap.get(entrantKey);
                if (null != existing && !existing.equals(entrantId))
                    doubledPersons.add(entrantKey);

                entrantMap.put(entrantKey, entrantId);
            }
        }
    }

}
