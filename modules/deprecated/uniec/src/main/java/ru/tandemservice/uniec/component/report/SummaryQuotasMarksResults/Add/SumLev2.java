/* $Id$ */
package ru.tandemservice.uniec.component.report.SummaryQuotasMarksResults.Add;

import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uniec.util.EntrantDataUtil;

/**
 * @author Vasily Zhukov
 * @since 19.08.2011
 */
class SumLev2 extends SumLevBase
{
    SumLev2(EntrantDataUtil dataUtil, EducationLevels educationLevel)
    {
        super(educationLevel.getId(), dataUtil, educationLevel.getDisplayableTitle(), null);
    }

    @Override
    public int getLevel()
    {
        return 1;
    }
}
