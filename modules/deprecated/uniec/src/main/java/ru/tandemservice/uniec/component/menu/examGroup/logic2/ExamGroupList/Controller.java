/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.menu.examGroup.logic2.ExamGroupList;

import org.tandemframework.core.component.IBusinessComponent;

import ru.tandemservice.uniec.component.menu.examGroup.abstractExamGroupList.ExamGroupListController;

/**
 * @author Vasily Zhukov
 * @since 28.05.2010
 */
public class Controller extends ExamGroupListController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getModel(component).setPrincipalContext(component.getUserContext().getPrincipalContext());
        super.onRefreshComponent(component);
    }
}