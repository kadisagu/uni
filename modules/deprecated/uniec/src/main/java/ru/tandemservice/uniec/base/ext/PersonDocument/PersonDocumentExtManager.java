/* $Id:$ */
package ru.tandemservice.uniec.base.ext.PersonDocument;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.bo.PersonDocument.PersonDocumentManager;
import org.tandemframework.shared.person.base.bo.PersonDocument.logic.PersonDocumentContext;
import org.tandemframework.shared.person.base.util.PersonDocumentUtil;
import ru.tandemservice.uniec.entity.entrant.Entrant;

/**
 * @author nvankov
 * @since 30.11.2016
 */
@Configuration
public class PersonDocumentExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private PersonDocumentManager _personDocumentManager;

    @Bean
    public ItemListExtension<PersonDocumentContext> documentContext()
    {
        return this.itemListExtension(this._personDocumentManager.documentContext())
                .add(PersonDocumentUtil.key(Entrant.class), new PersonDocumentContext(PersonDocumentUtil.key(Entrant.class), "Абитуриент (старый)")).create();
    }
}
