/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec;


/**
 * @author Боба
 * @since 05.08.2008
 */
public interface UniecDefines
{
    // EntrantState
    String ENTRANT_STATE_ACTIVE_CODE = "1";
    String ENTRANT_STATE_OUT_OF_COMPETITION_CODE = "2";
    String ENTRANT_STATE_ENROLED_CODE = "3";
    String ENTRANT_STATE_TOBE_ENROLED_CODE = "4";
    String ENTRANT_STATE_PRELIMENARY_ENROLLED_CODE = "5";
    String ENTRANT_STATE_IN_ORDER = "6";
    String ENTRANT_STATE_TAKE_DOCUMENTS_AWAY = "7";

    // CompetitionKind
    String COMPETITION_KIND = "CompetitionKind";
    String COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES = "1";
    String COMPETITION_KIND_INTERVIEW = "2";
    String COMPETITION_KIND_BEYOND_COMPETITION = "3";
    String COMPETITION_KIND_COMPETITIVE_ADMISSION = "5";

    // StateExamTypes
    String STATE_EXAM_TYPE = "StateExamTypes";
    String STATE_EXAM_TYPE_FIRST = "1";
    String STATE_EXAM_TYPE_SECOND = "2";

    // EntrantExamListsFormingFeatures
    String EXAMLIST_FORMING_FEATURE_FOR_ENTRANT = "1";
    String EXAMLIST_FORMING_FEATURE_FOR_REQUEST = "2";

    // SubjectPassForms
    String SUBJECT_PASS_FORM_STATE_EXAM = "1";
    String SUBJECT_PASS_FORM_TEST = "2";
    String SUBJECT_PASS_FORM_EXAM = "3";
    String SUBJECT_PASS_FORM_INTERVIEW = "4";
    String SUBJECT_PASS_FORM_STATE_EXAM_INTERNAL = "5";

    // SubjectPassWays
    String SUBJECT_PASS_WAY_COMMON = "1";
    String SUBJECT_PASS_WAY_VERBAL = "5";
    String SUBJECT_PASS_WAY_WRITTEN = "2";


    // EntrantAbsenceNotes
    String ENTRANT_ABSENCE_NOTE_WAS_NOT = "1";

    // OlympiadDiplomaType
    String DIPLOMA_TYPE_OTHER = "2";

    // EntranceDisciplineKind
    String ENTRANCE_DISCIPLINE_KIND = "EntranceDisciplineKinds";
    String ENTRANCE_DISCIPLINE_KIND_REQUIRED = "1";
    String ENTRANCE_DISCIPLINE_KIND_PROFILE = "2";
    String ENTRANCE_DISCIPLINE_KIND_HIGH_SCHOOL = "3";

    // FinalMarkSource
    int FINAL_MARK_OLYMPIAD = 1;   // по диплому олимпиады
    int FINAL_MARK_DISCIPLINE_EXAM = 2;      // по дисциплине (экзамен)
    int FINAL_MARK_APPEAL_EXAM = 3;          // по апелляции (экзамен)
    int FINAL_MARK_DISCIPLINE_TEST = 4;      // по дисциплине (тестирование)
    int FINAL_MARK_APPEAL_TEST = 5;          // по апелляции (тестирование)
    int FINAL_MARK_DISCIPLINE_INTERVIEW = 6; // по дисциплине (собеседование)
    int FINAL_MARK_APPEAL_INTERVIEW = 7;     // по апелляции (собеседование)
    int FINAL_MARK_STATE_EXAM_INTERNAL = 8;  // по дисциплине (ЕГЭ (вуз))
    int FINAL_MARK_STATE_EXAM_INTERNAL_APPEAL = 9;  // по апелляции (ЕГЭ (вуз))
    int FINAL_MARK_STATE_EXAM_WAVE1 = 10; // финальная оценка вычислена по ЕГЭ (1-ая волна)
    int FINAL_MARK_STATE_EXAM_WAVE2 = 11; // финальная оценка вычислена по ЕГЭ (2-ая волна)

    // StateExamSubjects Certificate Export Types
    int EXPORT_TYPE_BY_CERTIFICATE_NUMBER_WITHOUT_FIO = 1;
    int EXPORT_TYPE_BY_IDENTITY_CARD_WITHOUT_FIO = 2;
    int EXPORT_TYPE_BY_CERTIFICATE_NUMBER = 3;
    int EXPORT_TYPE_BY_IDENTITY_CARD = 4;
    int EXPORT_TYPE_BY_CERTIFICATE_NUMBER_WITH_FIO = 5;
    int EXPORT_TYPE_BY_FIO_WITH_DOC = 7;

    // UniecScriptItem
    String TEMPLATE_ACCESS_COURSES = "accessCourses";
    String TEMPLATE_CONTEST_REPORT = "contestReport";
    String TEMPLATE_AUTH_CARD = "authCard";
    String TEMPLATE_ENROLLMENT_DIRECTION_ADMISSION_PLAN = "enrollmentDirectionAdmissionPlan";
    String TEMPLATE_ENROLLMENT_RESULT_BY_CG = "enrollmentResultByCG";
    String TEMPLATE_ENROLLMENT_RESULT_BY_ORGUNIT = "enrollmentResultByOrgUnit";
    String TEMPLATE_ENROLLMENT_RESULT_BY_ORGUNITS = "enrollmentResultByOrgUnits";
    String TEMPLATE_ENROLLMENT_RESULT_BY_SEL = "enrollmentResultBySEL";
    String TEMPLATE_ENROLLMENT_RESULT_BY_STAGES = "enrollmentResultByStage";
    String TEMPLATE_ENROLLMENT_RESULT_HORIZONTAL = "enrollmentResultHorizontal";
    String TEMPLATE_ENROLLMENT_RESULT_HORIZONTAL2 = "enrollmentResultHorizontal2";
    String TEMPLATE_ENROLLMENT_RESULT_MINZDRAV_2009 = "enrollmentResultMinzdrav2009";
    String TEMPLATE_ENROLLMENT_RESULTS_3NK = "enrollmentResults3NKReport";
    String TEMPLATE_ENTRANCE_EXAM_MEETING_BY_CK1 = "entranceExamMeetingByCK1";
    String TEMPLATE_ENTRANCE_EXAM_MEETING_BY_CK2 = "entranceExamMeetingByCK2";
    String TEMPLATE_ENTRANCE_EXAM_MEETING_BY_CK3 = "entranceExamMeetingByCK3";
    String TEMPLATE_ENTRANT_AGE_DISTRIB = "entrantAgeDistrib";
    String TEMPLATE_ENTRANT_BENEFIT_DISTRIB = "entrantBenefitDistrib";
    String TEMPLATE_ENTRANT_DAILY_RATING_BY_ED = "entrantDailyRatingByED";
    String TEMPLATE_ENTRANT_DAILY_RATING_BY_ED2 = "entrantDailyRatingByED2";
    String TEMPLATE_ENTRANT_DAILY_RATING_BY_TA = "entrantDailyRatingByTA";
    String TEMPLATE_ENTRANT_RATING_SUMMARY = "entrantRatingSummary";
    String TEMPLATE_ENTRANT_REGISTRY_FORM_1 = "entrantRegistryForm1";
    String TEMPLATE_ENTRANT_REGISTRY_FORM_2 = "entrantRegistryForm2";
    String TEMPLATE_ENTRANT_REGISTRY_FOR_TC = "entrantRegistryForTC";
    String TEMPLATE_ENTRANT_LIST_TAKE_PASS_ENTRANCE = "entrantListTakePassEntrance";
    String TEMPLATE_ENTRANT_REQUEST_APPLICATION = "entrantRequestApplication";
    String TEMPLATE_ENTRANT_REQUEST_DAILY_REPORT = "entrantRequestDailyReport";
    String TEMPLATE_ENTRANT_REQUEST_BY_CG_DAILY_REPORT = "entrantRequestByCGDailyReport";
    String TEMPLATE_ENTRANT_RESIDENCE_DISTRIB = "entrantResidenceDistrib";
    String TEMPLATE_ENTRANT_STUDENT_CARD = "entrantStudentCard";
    String TEMPLATE_ENTRANT_SUMMARY_BULLETIN = "entrantSummaryBulletin";
    String TEMPLATE_ENTRANTS_FOREIGN_LANGS = "entrantsForeignLangs";
    String TEMPLATE_ENTRANTS_RATING_CG = "entrantsRatingCG";
    String TEMPLATE_ENTRANTS_RATING_CG_RES = "entrantsRatingCGRes";
    String TEMPLATE_EXAM_ADDMISSION_BY_COMPETITION_GROUP = "examAdmissionByCompetitionGroup";
    String TEMPLATE_ENTRANTS_RATING = "entrantsRating";
    String TEMPLATE_FORM_3NK_DECRIPTION_2008 = "form3NKDecription2008Report";
    String TEMPLATE_FORM_76KD_2008 = "form76KD2008Report";
    String TEMPLATE_FORM_76KD_2009 = "form76KD2009Report";
    String TEMPLATE_FORM_VPO1_2009 = "formVPO1in2009Report";
    String TEMPLATE_INFO_EDU_PROFILE_REPORT = "infoEduProfileReport";
    String TEMPLATE_INFO_SOURCES = "infoSources";
    String TEMPLATE_ENTRANT_EDU_INST_DISTIB = "entrantEduInstDistrib";
    String TEMPLATE_ENROLLMENT_ACCESS_COURSES = "enrollmentAccessCourses";
    String TEMPLATE_COMPETITION_GROUP_ENTRANTS_BY_ED = "competitionGroupEntrantsByED";
    String TEMPLATE_ENTRANT_REGISTRY_INTERNAL_STATE_EXAM = "entrantRegistryInternalStateExam";
    String TEMPLATE_ECG_DISTRIB_LIST = "ecgDistribList";
    String TEMPLATE_ECG_DISTRIBUTION_PER_DIRECTION = "ecgDistributionPerDirection";
    String TEMPLATE_ECG_DISTRIBUTION_PER_COMPETITION_GROUP = "ecgDistributionPerCompetitionGroup";
    String TEMPLATE_ENROLLMENT_RESULT_INDEX_NUMBERS = "enrollmentResultIndexNumbers";
    String TEMPLATE_ENROLLMENT_RESULT_BY_EGE = "enrollmentResultByEGE";
    String TEMPLATE_DAILY_SUMMARY_WITH_CM = "dailySummaryWithCM";
    String SCRIPT_ENROLLMENT_EXAM_SHEET = "enrollmentExamSheet";
    String SCRIPT_ENTRANT_LETTERBOX = "entrantLetterbox";
    String SCRIPT_ENROLLMENT_EXTRACT = "enrollmentExtract";
    String SCRIPT_ENROLLMENT_REVERT_EXTRACT = "revert.extract";
    String SCRIPT_ENROLLMENT_ORDER = "enrollmentOrder";
    String SCRIPT_ENROLLMENT_REVERT_ORDER = "revert.order";
    String SCRIPT_ENTRANT_LIST = "entrantList";
    String SCRIPT_ENTRANT_REQUEST = "entrantRequest";
    String SCRIPT_ONLINE_ENTRANT_REQUEST = "onlineEntrantRequest";
    String SCRIPT_DOCUMENT_LIST_AND_RECEIPT = "documentListAndReceipt";
    String SOGLASIE_NA_OBRABOTKU_P_DN = "printPersonalDataConsent";
    String SCRIPT_ENROLLMENT_EXAM_LIST = "enrollmentExamList";
    String SCRIPT_EXAM_GROUP_PREFIX = "examGroupList";
    String SCRIPT_EXAM_GROUP_SHEET_PREFIX = "enrollmentPassDisciplineSheet";
    String SCRIPT_EXAM_GROUP_MARKS_PREFIX = "examGroupMarks";
    String SUMMARY_SHEET_EXAM_REPORT = "summarySheetExamReport";
    String SCRIPT_ADMITTED_ENTRANTS_DATA_EXPORT = "admittedEntrantsDataExport";
    String ENTRANT_DATA_EXPORT = "entrantDataExport";
    String SCRIPT_ENTRANT_STUDENT_FORM = "entrantStudentForm";
    String SCRIPT_SPLIT_BACHELOR_ENTRANTS_LIST_ORDER = "splitBachelorEntrantsListOrder";
    String SCRIPT_SPLIT_MASTER_ENTRANTS_LIST_ORDER = "splitMasterEntrantsListOrder";
    String SCRIPT_ENROLLED_DEAL_LIST = "enrolledDealList";

    // EntrantEnrollmentOrderTypes
    String ORDER_TYPE_STUDENT_BUDGET = "1";
    String ORDER_TYPE_STUDENT_CONTRACT = "2";
    String ORDER_TYPE_MASTER_BUDGET = "3";
    String ORDER_TYPE_MASTER_CONTRACT = "4";
    String ORDER_TYPE_LISTENER_CONTRACT = "5";
    String ORDER_TYPE_TARGET_BUDGET = "6";
    String ORDER_TYPE_TARGET_CONTRACT = "7";
    String ORDER_TYPE_STUDENT_BUDGET_SHORT = "8";
    String ORDER_TYPE_STUDENT_CONTRACT_SHORT = "9";
    String ORDER_TYPE_SECOND_HIGH = "10";
    String ORDER_TYPE_LISTENER_PARALLEL = "11";
    String ORDER_TYPE_STUDENT_CONTRACT_OPP = "12";
    String ORDER_TYPE_STUDENT = "13";
    String ORDER_TYPE_TARGET = "14";

    //владелец настроек модуля uniec
    String EC_MODULE_OWNER = "ecModuleOwner";

    //имя настройки в которой хранится признак используемости отчета
    String IS_REPORT_USED = "isReportUsed";

    // StateExamSubjects
    String STATE_EXAM_SUBJECTS = "StateExamSubjects";

    String CATALOG_EXAM_GROUP_TYPE_SPEC = "1";
    String CATALOG_EXAM_GROUP_TYPE_MASTER = "2";
    String CATALOG_EXAM_GROUP_TYPE_SHORT = "3";

    String EXAM_GROUP_LIST_ALG1 = "examGroupList_code1"; // шаблон для списка экзаменационных групп (механизм формирования 1)
    String EXAM_GROUP_LIST_ALG2 = "examGroupList_code2"; // шаблон для списка экзаменационных групп (механизм формирования 2)
    String ENROLLMENT_PASS_DISCIPLINE_SHEET_ALG1 = "enrollmentPassDisciplineSheet_code1"; // шаблон для ввода оценок по экзам. группам (механизм формирования 1)
    String ENROLLMENT_PASS_DISCIPLINE_SHEET_ALG2 = "enrollmentPassDisciplineSheet_code2"; // шаблон для ввода оценок по экзам. группам (механизм формирования 2)

    String EXAM_GROUP_NAME_MECH1 = "1";
}
