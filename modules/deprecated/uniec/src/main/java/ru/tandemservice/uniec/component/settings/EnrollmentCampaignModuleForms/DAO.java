/* $Id$ */
package ru.tandemservice.uniec.component.settings.EnrollmentCampaignModuleForms;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.shared.commonbase.tapestry.validator.ParamRequiredValidator;
import ru.tandemservice.uni.dao.UniDao;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: azhebko
 * Date: 12.07.12
 * Time: 11:57
 * To change this template use File | Settings | File Templates.
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        Map<String, CoreCollectionUtils.Pair<Boolean, String>> map = new HashMap<>();

        map.put("entrant.wizard.field.issuancePlace", new CoreCollectionUtils.Pair<>(ParamRequiredValidator.isRequired("entrant.wizard.field.issuancePlace"),"Кем выдано удостоверение"));
        map.put("entrant.wizard.field.issuanceCode", new CoreCollectionUtils.Pair<>(ParamRequiredValidator.isRequired("entrant.wizard.field.issuanceCode"), "Код подразделения"));
        map.put("entrant.wizard.field.issuanceDate", new CoreCollectionUtils.Pair<>(ParamRequiredValidator.isRequired("entrant.wizard.field.issuanceDate"), "Дата выдачи"));
        map.put("entrant.wizard.field.birthDate", new CoreCollectionUtils.Pair<>(ParamRequiredValidator.isRequired("entrant.wizard.field.birthDate"), "Дата рождения"));
        map.put("entrant.wizard.field.birthPlace", new CoreCollectionUtils.Pair<>(ParamRequiredValidator.isRequired("entrant.wizard.field.birthPlace"), "Место рождения"));

        model.setMap(map);
    }

    public void update(Model model)
    {
        for (String key: model.getList())
        {
            ParamRequiredValidator.setRequired(key, model.getMap().get(key).getX());
        }
    }
}
