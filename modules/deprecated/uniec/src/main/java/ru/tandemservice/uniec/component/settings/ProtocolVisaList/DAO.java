/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.settings.ProtocolVisaList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.report.OrgUnitCommissionChairman;
import ru.tandemservice.uniec.entity.report.OrgUnitExecutiveSecretary;
import ru.tandemservice.uniec.entity.report.OrgUnitSelectionSecretary;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

/**
 * @author vip_delete
 * @since 21.07.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));
        model.setOrgUnitListModel(new FullCheckSelectModel(OrgUnit.P_FULL_TITLE)
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (model.getEnrollmentCampaign() == null) return ListResult.getEmpty();

                MQBuilder builder = new MQBuilder(EnrollmentDirection.ENTITY_CLASS, "d", new String[]{EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT});
                builder.add(MQExpression.eq("d", EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
                if (filter != null)
                    builder.add(MQExpression.like("d", EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT + "." + OrgUnit.P_TITLE, "%" + filter));
                List<OrgUnit> result = new ArrayList<OrgUnit>(new HashSet<OrgUnit>(builder.<OrgUnit>getResultList(getSession())));

                Collections.sort(result, new EntityComparator<OrgUnit>(new EntityOrder(OrgUnit.P_FULL_TITLE)));

                return new ListResult<OrgUnit>(result);
            }
        });

        prepareData(model);
    }

    @Override
    public void prepareData(Model model)
    {
        final Session session = getSession();

        EnrollmentCampaign enrollmentCampaign = model.getEnrollmentCampaign();
        if (enrollmentCampaign == null) return;
        OrgUnit orgUnit = (OrgUnit) model.getSettings().get("orgUnit");
        if (orgUnit == null) return;
        CompensationType compensationType = (CompensationType) model.getSettings().get("compensationType");
        if (compensationType == null) return;

        model.setCommissionChairman(getFio(session, OrgUnitCommissionChairman.class, enrollmentCampaign, compensationType, orgUnit));
        model.setExecutiveSecretary(getFio(session, OrgUnitExecutiveSecretary.class, enrollmentCampaign, compensationType, orgUnit));
        model.setSelectionSecretary(getFio(session, OrgUnitSelectionSecretary.class, enrollmentCampaign, compensationType, orgUnit));
    }

    @Override
    @SuppressWarnings("unchecked")
    public void update(Model model)
    {
        final Session session = getSession();

        EnrollmentCampaign enrollmentCampaign = model.getEnrollmentCampaign();
        OrgUnit orgUnit = (OrgUnit) model.getSettings().get("orgUnit");
        CompensationType compensationType = (CompensationType) model.getSettings().get("compensationType");

        if (model.isCommissionChairmanAll())
        {
            for (OrgUnit item : (List<OrgUnit>) model.getOrgUnitListModel().findValues(null).getObjects())
                saveOrUpdate(session, OrgUnitCommissionChairman.class, enrollmentCampaign, item, compensationType, model.getCommissionChairman());
        } else
            saveOrUpdate(session, OrgUnitCommissionChairman.class, enrollmentCampaign, orgUnit, compensationType, model.getCommissionChairman());

        if (model.isExecutiveSecretaryAll())
        {
            for (OrgUnit item : (List<OrgUnit>) model.getOrgUnitListModel().findValues(null).getObjects())
                saveOrUpdate(session, OrgUnitExecutiveSecretary.class, enrollmentCampaign, item, compensationType, model.getExecutiveSecretary());
        } else
            saveOrUpdate(session, OrgUnitExecutiveSecretary.class, enrollmentCampaign, orgUnit, compensationType, model.getExecutiveSecretary());

        saveOrUpdate(session, OrgUnitSelectionSecretary.class, enrollmentCampaign, orgUnit, compensationType, model.getSelectionSecretary());
    }

    private static void saveOrUpdate(Session session, Class<? extends IProtocolEntity> clazz, EnrollmentCampaign enrollmentCampaign, OrgUnit orgUnit, CompensationType compensationType, String fio)
    {
        fio = StringUtils.trimToNull(fio);
        IProtocolEntity entity = getRelation(session, clazz, enrollmentCampaign, compensationType, orgUnit);
        if (entity == null)
        {
            if (fio == null)
                return;
            try
            {
                entity = clazz.newInstance();
            } catch (Exception e)
            {
                throw new RuntimeException(e.getMessage(), e);
            }
            entity.setEnrollmentCampaign(enrollmentCampaign);
            entity.setOrgUnit(orgUnit);
            entity.setCompensationType(compensationType);
        } else
        {
            if (fio == null)
                session.delete(entity);
        }

        entity.setFio(fio);
        session.saveOrUpdate(entity);
    }

    private static String getFio(Session session, Class<? extends IProtocolEntity> clazz, EnrollmentCampaign enrollmentCampaign, CompensationType compensationType, OrgUnit orgUnit)
    {
        IProtocolEntity entity = getRelation(session, clazz, enrollmentCampaign, compensationType, orgUnit);
        return entity == null ? null : entity.getFio();
    }

    @SuppressWarnings("unchecked")
    private static <T extends IProtocolEntity> T getRelation(Session session, Class<T> clazz, EnrollmentCampaign enrollmentCampaign, CompensationType compensationType, OrgUnit orgUnit)
    {
        return (T) new MQBuilder(clazz.getName(), "r")
        .add(MQExpression.eq("r", IProtocolEntity.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign))
        .add(MQExpression.eq("r", IProtocolEntity.L_ORG_UNIT, orgUnit))
        .add(MQExpression.eq("r", IProtocolEntity.L_COMPENSATION_TYPE, compensationType))
        .uniqueResult(session);
    }
}
