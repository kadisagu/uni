/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.wizard.NextOfKinStep;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInline;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInlineConfig;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInlineUI;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.person.base.entity.PersonNextOfKin;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.component.wizard.IEntrantWizardComponents;

/**
 * @author vip_delete
 * @since 15.06.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    public static final String REGION_ADDRESS = "addressRegion";

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        prepareListDataSource(component);

        AddressBaseEditInlineConfig addressConfig = new AddressBaseEditInlineConfig();
        addressConfig.setEntityId(model.getKinModel().getNextOfKin().getId() == null ? null : model.getKinModel().getNextOfKin().getId());
        addressConfig.setDetailLevel(4);
        addressConfig.setDetailedOnly(true);
        addressConfig.setInline(true);
        addressConfig.setFieldSetTitle("Адресные данные");
        addressConfig.setAddressProperty(PersonNextOfKin.L_ADDRESS);
        component.createChildRegion(REGION_ADDRESS, new ComponentActivator(AddressBaseEditInline.class.getSimpleName(), new ParametersMap().add(AddressBaseEditInlineUI.BIND_CONFIG, addressConfig)));

    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<PersonNextOfKin> dataSource = new DynamicListDataSource<>(component, component1 -> {
            model.getDataSource().setCountRow(model.getPersonNextOfKinList().size());
            UniBaseUtils.createPage(model.getDataSource(), model.getPersonNextOfKinList());
        });

        dataSource.addColumn(new SimpleColumn("ФИО", PersonNextOfKin.P_FULLFIO).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Степень родства", PersonNextOfKin.relationDegree().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дата рождения", PersonNextOfKin.P_BIRTH_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Контактный телефон", PersonNextOfKin.P_PHONES).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Место работы", PersonNextOfKin.P_EMPLOYMENT_PLACE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Должность", PersonNextOfKin.P_POST).setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete"));

        model.setDataSource(dataSource);
    }

    public void onClickSelect(IBusinessComponent component)
    {
        Model model = getModel(component);
        AddressBaseEditInlineUI addressBaseEditInlineUI = (AddressBaseEditInlineUI) component.getChildRegion(REGION_ADDRESS).getActiveComponent().getPresenter();
        if(addressBaseEditInlineUI.getResult() == null) throw new RuntimeException("Адрес обязателен");
        AddressBase address = addressBaseEditInlineUI.getResult();
        model.getKinModel().setAddress(address);

        getDao().prepareSelectNextOfKin(getModel(component));
    }

    public void onClickDelete(IBusinessComponent component)
    {
        getDao().deleteNextOfKin(getModel(component), (Long) component.getListenerParameter());
    }

    // Wizard Actions

    public void onClickNext(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getPersonNextOfKinList().isEmpty())
            throw new ApplicationException("Не выбраны ближайшие родственники.");

        ErrorCollector errors = component.getUserContext().getErrorCollector();
        getDao().validate(model, errors);
        if (errors.hasErrors())
            return;

        getDao().update(model);

        deactivate(component);

        component.getParentRegion().getOwner().createDefaultChildRegion(new ComponentActivator(IEntrantWizardComponents.PERSONAL_DATA_STEP, new ParametersMap()
                .add("entrantId", model.getEntrant().getId())
                .add("onlineEntrantId", model.getOnlineEntrant().getId())
                .add("entrantMasterPermKey", model.getEntrantMasterPermKey())
        ));
    }

    public void onClickSkip(IBusinessComponent component)
    {
        Model model = getModel(component);

        deactivate(component);

        component.getParentRegion().getOwner().createDefaultChildRegion(new ComponentActivator(IEntrantWizardComponents.PERSONAL_DATA_STEP, new ParametersMap()
                .add("entrantId", model.getEntrant().getId())
                .add("onlineEntrantId", model.getOnlineEntrant().getId())
                .add("entrantMasterPermKey", model.getEntrantMasterPermKey())
        ));
    }

    public void onClickStop(IBusinessComponent component)
    {
        getDao().update(getModel(component));

        deactivate(component);
    }
}
