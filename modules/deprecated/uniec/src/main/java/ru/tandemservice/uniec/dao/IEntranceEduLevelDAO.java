/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.dao;

import java.util.Collection;
import java.util.List;

import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;

import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

/**
 * @author Боба
 * @since 11.08.2008
 *
 * скорее всего вам нужно использовать EnrollmentDirectionUtil
 */
public interface IEntranceEduLevelDAO
{
    List<EducationLevelsHighSchool> getEducationLevelsHighSchoolList(IEntranceEduLevelModel model);

    List<DevelopForm> getDevelopFormList(IEntranceEduLevelModel model);

    List<DevelopCondition> getDevelopConditionList(IEntranceEduLevelModel model);

    List<DevelopTech> getDevelopTechList(IEntranceEduLevelModel model);

    List<DevelopPeriod> getDevelopPeriodList(IEntranceEduLevelModel model);

    EnrollmentDirection getEnrollmentDirection(IEntranceEduLevelModel model);

    ListResult<OrgUnit> getKindOrgUnitsList(String filter, String kindCode, String eduOrgUnitPropertyName, String eduOrgUnitFilterPropertyName, OrgUnit filterOrgUnit, Collection<Long> filterEnrollmentDirectionIds, IPrincipalContext context, EnrollmentCampaign enrollmentCampaign, int maxResult);
}
