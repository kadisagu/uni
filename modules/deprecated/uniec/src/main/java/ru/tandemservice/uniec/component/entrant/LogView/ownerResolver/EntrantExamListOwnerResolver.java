/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.LogView.ownerResolver;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.event.meta.IEntityOwnerResolver;

import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantExamList;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;

/**
 * @author ekachanova
 */
public class EntrantExamListOwnerResolver implements IEntityOwnerResolver
{
    @Override
    public Long getEntityOwnerId(IEntity entity)
    {
        if(entity instanceof EntrantExamList)
        {
            return getEntityOwnerId((EntrantExamList)entity);
        }
        return null;
    }

    protected Long getEntityOwnerId(EntrantExamList entrantExamList)
    {
        return entrantExamList.getEntrant() instanceof Entrant ? ((Entrant)entrantExamList.getEntrant()).getId()
                : entrantExamList.getEntrant() instanceof EntrantRequest ? (((EntrantRequest)entrantExamList.getEntrant()).getEntrant().getId()) : null;
    }
}
