/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.settings.EnrollmentOrderVisaAddEdit;

import org.apache.hivemind.Location;
import org.apache.hivemind.Resource;
import org.apache.tapestry.IAsset;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.settings.EnrollmentOrderVisaItem;
import ru.tandemservice.unimv.IPossibleVisa;
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising;

import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

/**
 * @author vip_delete
 * @since 28.07.2009
 */
@Input({
        @Bind(key = "title", binding = "title"),
        @Bind(key = "enrollmentCampaignId", binding = "enrollmentCampaignId")
})
public class Model implements IEnrollmentCampaignSelectModel
{
    private String _title;
    private Long _enrollmentCampaignId;
    private List<EnrollmentOrderVisaItem> _visaItemList;    // список EnrollmentOrderVisaItem входящих в группу в момент редактирования

    private List<EnrollmentCampaign> _enrollmentCampaignList;
    private IDataSettings _settings;
    private ISelectModel _possibleVisaListModel;
    private List<IPossibleVisa> _possibleVisaList;

    private List<Row> _rowList = new LinkedList<Row>();
    private ISingleSelectModel _groupsMemberModel;
    private GroupsMemberVising _groupsMemberVising;
    private DynamicListDataSource<Row> _rowDataSource;
    private Boolean _disabledAddButton;
    private List<GroupsMemberVising> _excludeGroupsMemberVising = new LinkedList<GroupsMemberVising>();

    private IAsset _asset = new IAsset()
    {
        @Override
        public String buildURL()
        {
            return "img/general/add.png";
        }

        @Override
        public InputStream getResourceAsStream()
        {
            return null;
        }

        @Override
        public Resource getResourceLocation()
        {
            return null;
        }

        @Override
        public Location getLocation()
        {
            return null;
        }
    };

    public boolean isEditForm()
    {
        return _enrollmentCampaignId != null;
    }

    // IEnrollmentCampaignSelectModel

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return (EnrollmentCampaign) getSettings().get(ENROLLMENT_CAMPAIGN_FILTER_NAME);
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        getSettings().set(ENROLLMENT_CAMPAIGN_FILTER_NAME, enrollmentCampaign);
    }

    // Getters & Setters

    public List<GroupsMemberVising> getExcludeGroupsMemberVising()
    {
        return _excludeGroupsMemberVising;
    }

    public void setExcludeGroupsMemberVising(List<GroupsMemberVising> excludeGroupsMemberVising)
    {
        _excludeGroupsMemberVising = excludeGroupsMemberVising;
    }

    public GroupsMemberVising getGroupsMemberVising()
    {
        return _groupsMemberVising;
    }

    public void setGroupsMemberVising(GroupsMemberVising groupsMemberVising)
    {
        _groupsMemberVising = groupsMemberVising;
    }

    public IAsset getAsset()
    {
        return _asset;
    }

    public void setAsset(IAsset asset)
    {
        _asset = asset;
    }

    public Boolean getDisabledAddButton()
    {
        return _disabledAddButton;
    }

    public void setDisabledAddButton(Boolean disabledAddButton)
    {
        _disabledAddButton = disabledAddButton;
    }

    public List<Row> getRowList()
    {
        return _rowList;
    }

    public void setRowList(List<Row> rowList)
    {
        _rowList = rowList;
    }

    public ISingleSelectModel getGroupsMemberModel()
    {
        return _groupsMemberModel;
    }

    public void setGroupsMemberModel(ISingleSelectModel groupsMemberModel)
    {
        _groupsMemberModel = groupsMemberModel;
    }

    public DynamicListDataSource<Row> getRowDataSource()
    {
        return _rowDataSource;
    }

    public void setRowDataSource(DynamicListDataSource<Row> rowDataSource)
    {
        _rowDataSource = rowDataSource;
    }

    public String getTitle()
    {
        return _title;
    }

    public void setTitle(String title)
    {
        _title = title;
    }

    public Long getEnrollmentCampaignId()
    {
        return _enrollmentCampaignId;
    }

    public void setEnrollmentCampaignId(Long enrollmentCampaignId)
    {
        _enrollmentCampaignId = enrollmentCampaignId;
    }

    public List<EnrollmentOrderVisaItem> getVisaItemList()
    {
        return _visaItemList;
    }

    public void setVisaItemList(List<EnrollmentOrderVisaItem> visaItemList)
    {
        _visaItemList = visaItemList;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    @Override
    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public ISelectModel getPossibleVisaListModel()
    {
        return _possibleVisaListModel;
    }

    public void setPossibleVisaListModel(ISelectModel possibleVisaListModel)
    {
        _possibleVisaListModel = possibleVisaListModel;
    }

    public List<IPossibleVisa> getPossibleVisaList()
    {
        return _possibleVisaList;
    }

    public void setPossibleVisaList(List<IPossibleVisa> possibleVisaList)
    {
        _possibleVisaList = possibleVisaList;
    }
}
