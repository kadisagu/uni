// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub;

import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.MergeType;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfElementFactory;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.ecg.EcgDistribObject;
import ru.tandemservice.uniec.entity.ecg.EcgDistribQuota;
import ru.tandemservice.uniec.entity.ecg.EcgDistribRelation;
import ru.tandemservice.uniec.entity.ecg.EcgReserveRelation;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.settings.EnrollmentCompetitionKind;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.report.IReportRow;
import ru.tandemservice.uniec.util.report.RequestedEnrollmentDirectionComparator;

import java.util.*;

/**
 * @author oleyba
 * @since 22.07.2010
 */
public class CompetitionGroupDistribListPrintFormCreator extends UniBaseDao implements IPrintFormCreator<Long>
{
    // competitionGroupDistribListPrint

    private static enum State
    {
        DOCS_AWAY, ORIG, COPY
    }

    private static final Map<State, String> stateMap = ImmutableMap.of(State.DOCS_AWAY, "Забрал документы",
                                                                       State.COPY, "Копии",
                                                                       State.ORIG, "Оригинал");
    private static final Map<String, String> competitionKindHeaderFirstBlock = ImmutableMap.of(
            UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES, "Абитуриенты, имеющие право на приём без вступительных испытаний",
            UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION, "Абитуриенты, имеющие право на приём вне конкурса",
            UniecDefines.COMPETITION_KIND_INTERVIEW, "Абитуриенты, поступающие по собеседованию, успешно прошедшие вступительные испытания и рекомендованные к зачислению",
            EnrollmentCompetitionKind.TARGET_ADMISSION, "Абитуриенты, поступающие по целевому приему, успешно прошедшие вступительные испытания и рекомендованные к зачислению",
            UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION, "Абитуриенты, успешно прошедшие вступительные испытания и рекомендованные к зачислению");
    private static final Map<String, String> competitionKindHeaderSecondBlock = ImmutableMap.of(
            UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES, "Абитуриенты, имеющие право на приём без вступительных испытаний",
            UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION, "Абитуриенты, имеющие право на приём вне конкурса",
            UniecDefines.COMPETITION_KIND_INTERVIEW, "Абитуриенты, поступающие по собеседованию, успешно прошедшие вступительные испытания",
            EnrollmentCompetitionKind.TARGET_ADMISSION, "Абитуриенты, поступающие по целевому приему, успешно прошедшие вступительные испытания",
            UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION, "Абитуриенты, успешно прошедшие вступительные испытания");

    @Override
    public RtfDocument createPrintForm(byte[] template, Long distribId)
    {
        RtfDocument document = new RtfReader().read(template);

        EcgDistribObject distrib = get(EcgDistribObject.class, distribId);

        List<EnrollmentDirection> directions = getList(EnrollmentDirection.class, EnrollmentDirection.competitionGroup().s(), distrib.getCompetitionGroup());
        Collections.sort(directions, new Comparator<EnrollmentDirection>(){ @Override public int compare(EnrollmentDirection o1, EnrollmentDirection o2){return o1.getEducationOrgUnit().getEducationLevelHighSchool().getTitle().compareTo(o2.getEducationOrgUnit().getEducationLevelHighSchool().getTitle());}});

        injectHeader(document, distrib, directions);

        List<EnrollmentCompetitionKind> competitionKinds = getList(EnrollmentCompetitionKind.class, EnrollmentCompetitionKind.enrollmentCampaign().s(), distrib.getCompetitionGroup().getEnrollmentCampaign(), EnrollmentCompetitionKind.priority().s());
        Map<String, EnrollmentCompetitionKind> competitionKindMap = new HashMap<>();
        EnrollmentCompetitionKind targetAdmission = prepareCompetitionKinds(competitionKinds, competitionKindMap);

        Map<EnrollmentDirection, Map<EnrollmentCompetitionKind, List<ReportRow>>> directionsRowMap = new HashMap<>();
        Map<EnrollmentCompetitionKind, List<ReportRow>> rowMap = new HashMap<>();
        Map<EnrollmentCompetitionKind, List<ReportRow>> reserveRowMap = new HashMap<>();

        prepareRowMaps(distrib, competitionKindMap, targetAdmission, directionsRowMap, rowMap, reserveRowMap);

        RequestedEnrollmentDirectionComparator rowComparator = new RequestedEnrollmentDirectionComparator(UniecDAOFacade.getSettingsDAO().getCompetitionKindPriorities(distrib.getCompetitionGroup().getEnrollmentCampaign()));

        List<String[]> tableContent = new ArrayList<>();
        final Set<Integer> headerRows = new HashSet<>();
        final Map<Integer, State> rowStateMap = new HashMap<>();
        int rowIndex = 0; int numberIter = 1;
        for (EnrollmentCompetitionKind competitionKind : competitionKinds)
        {
            if (!rowMap.containsKey(competitionKind)) continue;
            tableContent.add(new String[] {competitionKindHeaderFirstBlock.get(competitionKind.getCompetitionKind() == null ? EnrollmentCompetitionKind.TARGET_ADMISSION : competitionKind.getCompetitionKind().getCode()), "", "", "", "", ""});
            headerRows.add(rowIndex++);
            List<ReportRow> rows = rowMap.get(competitionKind);
            Collections.sort(rows, rowComparator);
            for (ReportRow row : rows)
            {
                row.content[0] = String.valueOf(numberIter++);
                tableContent.add(row.content);
                rowStateMap.put(rowIndex++, row.state);
            }
        }
        final int reserveStart = rowIndex;
        for (EnrollmentCompetitionKind competitionKind : competitionKinds)
        {
            if (!reserveRowMap.containsKey(competitionKind)) continue;
            tableContent.add(new String[] {competitionKindHeaderSecondBlock.get(competitionKind.getCompetitionKind() == null ? EnrollmentCompetitionKind.TARGET_ADMISSION : competitionKind.getCompetitionKind().getCode()), "", "", "", "", ""});
            headerRows.add(rowIndex++);
            List<ReportRow> rows = reserveRowMap.get(competitionKind);
            Collections.sort(rows, rowComparator);
            for (ReportRow row : rows)
            {
                row.content[0] = String.valueOf(numberIter++);
                tableContent.add(row.content);
                rowStateMap.put(rowIndex++, row.state);
            }
        }

        document.getHeader().getColorTable().getColorList();
        final int numberColorIndex = document.getHeader().getColorTable().addColor(204, 255, 255);
        final int reserveNumberColorIndex = document.getHeader().getColorTable().addColor(255, 255, 153);
        final int headerColorIndex = document.getHeader().getColorTable().addColor(217, 217, 217);
        final Map<State, Integer> stateColorMap = new HashMap<>();
        stateColorMap.put(State.DOCS_AWAY, document.getHeader().getColorTable().addColor(255, 153, 204));
        stateColorMap.put(State.COPY, document.getHeader().getColorTable().addColor(255, 255, 0));
        stateColorMap.put(State.ORIG, document.getHeader().getColorTable().addColor(0, 255, 0));

        RtfTableModifier tableModifier = new RtfTableModifier().put("T", tableContent.toArray(new String[tableContent.size()][]));
        tableModifier.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                if (headerRows.contains(rowIndex))
                {
                    cell.setBackgroundColorIndex(headerColorIndex);
                    cell.setMergeType(colIndex == 0 ? MergeType.HORIZONTAL_MERGED_FIRST : MergeType.HORIZONTAL_MERGED_NEXT);
                    if (colIndex == 0)
                        return new RtfString().boldBegin().append(value).boldEnd().toList();
                } else
                {
                    if (colIndex == 0)
                        cell.setBackgroundColorIndex(rowIndex > reserveStart ? reserveNumberColorIndex : numberColorIndex);
                    if (colIndex == 5)
                        cell.setBackgroundColorIndex(stateColorMap.get(rowStateMap.get(rowIndex)));
                }
                return null;
            }
        });
        tableModifier.modify(document);

        final RtfTable directionHeaderSample = (RtfTable) UniRtfUtil.findElement(document.getElementList(), "direction").getClone();

        final IRtfElementFactory elementFactory = RtfBean.getElementFactory();
        IRtfText lineBreak = elementFactory.createRtfText("\\par\\pard\n\n");
        lineBreak.setRaw(true);
        IRtfText pageBreak = elementFactory.createRtfText("\n\n\\page\\pard\n\n");
        pageBreak.setRaw(true);

        boolean first = true;
        for (EnrollmentDirection direction : directions)
        {
            Map<EnrollmentCompetitionKind, List<ReportRow>> directionRowMap = directionsRowMap.get(direction);
            if (null == directionRowMap) continue;

            if (!first)
            {
                RtfTable directionHeader = directionHeaderSample.getClone();
                final IRtfGroup group = elementFactory.createRtfGroup();
                group.getElementList().add(directionHeader);
                document.getElementList().add(lineBreak);
                document.getElementList().add(lineBreak);
                document.getElementList().add(pageBreak);
                document.getElementList().add(lineBreak);
                document.getElementList().add(group);
            }
            first = false;

            new RtfInjectModifier().put("direction", direction.getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitle()).modify(document);

            tableContent.clear();
            headerRows.clear();
            rowStateMap.clear();
            rowIndex = 0; numberIter = 1;
            for (EnrollmentCompetitionKind competitionKind : competitionKinds)
            {
                if (!directionRowMap.containsKey(competitionKind)) continue;
                tableContent.add(new String[] {competitionKindHeaderFirstBlock.get(competitionKind.getCompetitionKind() == null ? EnrollmentCompetitionKind.TARGET_ADMISSION : competitionKind.getCompetitionKind().getCode()), "", "", "", "", ""});
                headerRows.add(rowIndex++);
                List<ReportRow> rows = directionRowMap.get(competitionKind);
                Collections.sort(rows, rowComparator);
                for (ReportRow row : rows)
                {
                    row.content[0] = String.valueOf(numberIter++);
                    row.content[3] = row.content[4]; row.content[4] = row.content[5]; row.content[5] = "";
                    tableContent.add(row.content);
                    rowStateMap.put(rowIndex++, row.state);
                }
            }

            tableModifier = new RtfTableModifier().put("TD", tableContent.toArray(new String[tableContent.size()][]));
            tableModifier.put("TD", new RtfRowIntercepterBase()
            {
                @Override
                public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
                {
                    if (headerRows.contains(rowIndex))
                    {
                        cell.setBackgroundColorIndex(headerColorIndex);
                        cell.setMergeType(colIndex == 0 ? MergeType.HORIZONTAL_MERGED_FIRST : MergeType.HORIZONTAL_MERGED_NEXT);
                        if (colIndex == 0)
                            return new RtfString().boldBegin().append(value).boldEnd().toList();
                    } else
                    {
                        if (colIndex == 4)
                            cell.setBackgroundColorIndex(stateColorMap.get(rowStateMap.get(rowIndex)));
                    }
                    return null;
                }
            });
            tableModifier.modify(document);
        }

        getSession().clear();

        return document;
    }

    private EnrollmentCompetitionKind prepareCompetitionKinds(List<EnrollmentCompetitionKind> competitionKinds, Map<String, EnrollmentCompetitionKind> competitionKindMap)
    {
        EnrollmentCompetitionKind targetAdmission = null;
        for (EnrollmentCompetitionKind competitionKind : competitionKinds)
            if (competitionKind.getCompetitionKind() != null)
                competitionKindMap.put(competitionKind.getCompetitionKind().getCode(), competitionKind);
            else
                targetAdmission = competitionKind;
        return targetAdmission;
    }

    private void prepareRowMaps(EcgDistribObject distrib, Map<String, EnrollmentCompetitionKind> competitionKindMap, EnrollmentCompetitionKind targetAdmission, Map<EnrollmentDirection, Map<EnrollmentCompetitionKind, List<ReportRow>>> directionsRowMap, Map<EnrollmentCompetitionKind, List<ReportRow>> rowMap, Map<EnrollmentCompetitionKind, List<ReportRow>> reserveRowMap)
    {
        EntrantDataUtil dataUtil = prepareDataUtil(distrib);
        Map<Entrant, String> priorities = preparePriorities(distrib);
        DoubleFormatter markFormatter = DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS;
        getList(EntrantState.class);

        MQBuilder relations = new MQBuilder(EcgDistribRelation.ENTITY_CLASS, "rel");
        relations.add(MQExpression.eq("rel", EcgDistribRelation.quota().distribution().s(), distrib));
        relations.addJoinFetch("rel", EcgDistribRelation.quota().s(), "q");
        relations.addJoinFetch("q", EcgDistribQuota.direction().s(), "dir");
        relations.addJoinFetch("rel", EcgDistribRelation.entrantDirection().s(), "red");
        addFetch(relations);
        for (EcgDistribRelation rel : relations.<EcgDistribRelation>getResultList(getSession()))
        {
            EnrollmentDirection direction = rel.getQuota().getDirection();
            RequestedEnrollmentDirection requestedDirection = rel.getEntrantDirection();
            EnrollmentCompetitionKind competitionKind = requestedDirection.isTargetAdmission() ? targetAdmission : competitionKindMap.get(requestedDirection.getCompetitionKind().getCode());

            ReportRow row = new ReportRow(requestedDirection, dataUtil);
            row.state = requestedDirection.getState().getCode().equals(UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY) ? State.DOCS_AWAY : (requestedDirection.isOriginalDocumentHandedIn() ? State.ORIG : State.COPY);
            row.content = new String[] {
                    "",
                    row.fio,
                    markFormatter.format(row.finalMark),
                    direction.getEducationOrgUnit().getEducationLevelHighSchool().getShortTitle(),
                    StringUtils.trimToEmpty(priorities.get(requestedDirection.getEntrantRequest().getEntrant())),
                    stateMap.get(row.state)};
            {
                Map<EnrollmentCompetitionKind, List<ReportRow>> byDirection = directionsRowMap.get(direction);
                if (null == byDirection)
                    directionsRowMap.put(direction, byDirection = new HashMap<>());
                List<ReportRow> rowList = byDirection.get(competitionKind);
                if (null == rowList)
                    byDirection.put(competitionKind, rowList = new ArrayList<>());
                rowList.add(row);
            }
            List<ReportRow> rowList = rowMap.get(competitionKind);
            if (null == rowList)
                rowMap.put(competitionKind, rowList = new ArrayList<>());
            rowList.add(row);
        }

        dataUtil = prepareReserveDataUtil(distrib);

        Set<Entrant> reserveEntrants = new HashSet<>();
        for (RequestedEnrollmentDirection requestedDirection : getReserveDirectionsBuilder(distrib).<RequestedEnrollmentDirection>getResultList(getSession()))
        {
            if (reserveEntrants.contains(requestedDirection.getEntrantRequest().getEntrant())) continue;
            reserveEntrants.add(requestedDirection.getEntrantRequest().getEntrant());
            EnrollmentCompetitionKind competitionKind = requestedDirection.isTargetAdmission() ? targetAdmission : competitionKindMap.get(requestedDirection.getCompetitionKind().getCode());

            ReportRow row = new ReportRow(requestedDirection, dataUtil);
            row.state = requestedDirection.getState().getCode().equals(UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY) ? State.DOCS_AWAY : (requestedDirection.isOriginalDocumentHandedIn() ? State.ORIG : State.COPY);
            row.content = new String[] {
                    "",
                    row.fio,
                    markFormatter.format(row.finalMark),
                    "",
                    StringUtils.trimToEmpty(priorities.get(requestedDirection.getEntrantRequest().getEntrant())),
                    stateMap.get(row.state)};
            List<ReportRow> rowList = reserveRowMap.get(competitionKind);
            if (null == rowList)
                reserveRowMap.put(competitionKind, rowList = new ArrayList<>());
            rowList.add(row);
        }
        // добавляем тех, кто есть в резерве, а направление уже не сохранилось
        MQBuilder builder = new MQBuilder(EcgReserveRelation.ENTITY_CLASS, "rel");
        builder.add(MQExpression.eq("rel", EcgReserveRelation.distribution().s(), distrib));
        builder.addJoinFetch("rel", EcgReserveRelation.entrant().s(), "entr");
        builder.addJoinFetch("entr", Entrant.person().s(), "person");
        builder.addJoinFetch("person", Person.identityCard().s(), "idc");
        List<EcgReserveRelation> ecgReserveRelations = getList(EcgReserveRelation.class, EcgReserveRelation.distribution().s(), distrib);
        for (EcgReserveRelation rel : ecgReserveRelations)
        {
            if (reserveEntrants.contains(rel.getEntrant())) continue;
            reserveEntrants.add(rel.getEntrant());

            EnrollmentCompetitionKind competitionKind = competitionKindMap.get(UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION);
            ReportRow row = new ReportRow(rel.getEntrant().getPerson().getFullFio(), null);
            row.state = State.DOCS_AWAY;
            row.content = new String[] {
                    "",
                    row.fio,
                    markFormatter.format(row.finalMark),
                    "",
                    StringUtils.trimToEmpty(priorities.get(rel.getEntrant())),
                    stateMap.get(row.state)};
            List<ReportRow> rowList = reserveRowMap.get(competitionKind);
            if (null == rowList)
                reserveRowMap.put(competitionKind, rowList = new ArrayList<>());
            rowList.add(row);
        }
    }

    private MQBuilder getReserveDirectionsBuilder(EcgDistribObject distrib)
    {
        MQBuilder reserve = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "red");
        reserve.add(MQExpression.eq("red", RequestedEnrollmentDirection.enrollmentDirection().competitionGroup().s(), distrib.getCompetitionGroup()));
        reserve.add(MQExpression.eq("red", RequestedEnrollmentDirection.compensationType().s(), distrib.getCompensationType()));
        if (distrib.isSecondHighAdmission())
            reserve.add(MQExpression.eq("red", RequestedEnrollmentDirection.studentCategory().code().s(), UniDefines.STUDENT_CATEGORY_SECOND_HIGH));
        else
            reserve.add(MQExpression.notEq("red", RequestedEnrollmentDirection.studentCategory().code().s(), UniDefines.STUDENT_CATEGORY_SECOND_HIGH));
        MQBuilder reserveSub = new MQBuilder(EcgReserveRelation.ENTITY_CLASS, "rel");
        reserveSub.add(MQExpression.eq("rel", EcgReserveRelation.distribution().s(), distrib));
        reserveSub.add(MQExpression.eqProperty("rel", EcgReserveRelation.entrant().s(), "red", RequestedEnrollmentDirection.entrantRequest().entrant().s()));
        reserve.add(MQExpression.exists(reserveSub));
        addFetch(reserve);
        return reserve;
    }

    private void addFetch(MQBuilder builder)
    {
        builder.addJoinFetch("red", RequestedEnrollmentDirection.enrollmentDirection().s(), "enr_dir");
        builder.addLeftJoinFetch("red", RequestedEnrollmentDirection.profileChosenEntranceDiscipline().s(), "profile");
        builder.addJoinFetch("enr_dir", EnrollmentDirection.educationOrgUnit().s(), "edu_ou");
        builder.addJoinFetch("edu_ou", EducationOrgUnit.educationLevelHighSchool().s(), "edu_hs");
        builder.addJoinFetch("edu_hs", EducationLevelsHighSchool.educationLevel().s(), "edu_lev");
        builder.addJoinFetch("red", RequestedEnrollmentDirection.entrantRequest().s(), "req");
        builder.addJoinFetch("req", EntrantRequest.entrant().s(), "entr");
        builder.addJoinFetch("entr", Entrant.person().s(), "person");
        builder.addJoinFetch("person", Person.identityCard().s(), "idc");
    }

    private Map<Entrant, String> preparePriorities(EcgDistribObject distrib)
    {
        Map<Entrant, String> priorities = new HashMap<>();
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "red");
        builder.add(MQExpression.eq("red", RequestedEnrollmentDirection.enrollmentDirection().competitionGroup().s(), distrib.getCompetitionGroup()));
        builder.add(MQExpression.eq("red", RequestedEnrollmentDirection.compensationType().s(), distrib.getCompensationType()));
        if (distrib.isSecondHighAdmission())
            builder.add(MQExpression.eq("red", RequestedEnrollmentDirection.studentCategory().code().s(), UniDefines.STUDENT_CATEGORY_SECOND_HIGH));
        else
            builder.add(MQExpression.notEq("red", RequestedEnrollmentDirection.studentCategory().code().s(), UniDefines.STUDENT_CATEGORY_SECOND_HIGH));
        builder.addOrder("red", RequestedEnrollmentDirection.entrantRequest().regDate().s());
        builder.addOrder("red", RequestedEnrollmentDirection.priority().s());
        for (RequestedEnrollmentDirection direction : builder.<RequestedEnrollmentDirection>getResultList(getSession()))
        {
            String title = direction.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getShortTitle();
            Entrant entrant = direction.getEntrantRequest().getEntrant();
            String result = priorities.get(entrant);
            if (null == result)
                priorities.put(entrant, title);
            else
                priorities.put(entrant, result + ", " + title);
        }
        return priorities;
    }

    @SuppressWarnings("deprecation")
    private void injectHeader(RtfDocument document, EcgDistribObject distrib, List<EnrollmentDirection> directions)
    {
        List<EnrollmentDirection> actualDirections = new ArrayList<>();
        List<EnrollmentDirection> specialities = new ArrayList<>();
        for (EnrollmentDirection direction : directions)
        {
            if (distrib.getCompensationType().isBudget() && !direction.isBudget()) continue;
            if (!distrib.getCompensationType().isBudget() && !direction.isContract()) continue;
            if (direction.getEducationOrgUnit().getEducationLevelHighSchool().isSpeciality() || direction.getEducationOrgUnit().getEducationLevelHighSchool().isSpecialization())
                specialities.add(direction);
            else
                actualDirections.add(direction);
        }
        String directionsStr = "";
        if (!actualDirections.isEmpty())
            directionsStr = "Направления: " + UniStringUtils.join(actualDirections, "title", ", ");
        if (!specialities.isEmpty())
        {
            if (!directionsStr.isEmpty())
                directionsStr = directionsStr + "\\par";
            directionsStr = directionsStr + "Специальности: " + UniStringUtils.join(specialities, "title", ", ");
        }

        IRtfText text = RtfBean.getElementFactory().createRtfText(directionsStr);
        text.setRaw(true);
        new RtfInjectModifier()
                .put("ecgTitle", distrib.getCompetitionGroup().getTitle())
                .put("directions", Arrays.<IRtfElement>asList(text))
                .modify(document);
    }

    private EntrantDataUtil prepareDataUtil(EcgDistribObject distrib)
    {
        MQBuilder relationsSub = new MQBuilder(EcgDistribRelation.ENTITY_CLASS, "rel")
                .add(MQExpression.eq("rel", EcgDistribRelation.quota().distribution().s(), distrib))
                .add(MQExpression.eqProperty("rel", EcgDistribRelation.entrantDirection().id().s(), "red", "id"));
        MQBuilder requestedDirections = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "red")
                .add(MQExpression.exists(relationsSub));
        return new EntrantDataUtil(getSession(), distrib.getCompetitionGroup().getEnrollmentCampaign(), requestedDirections);
    }

    private EntrantDataUtil prepareReserveDataUtil(EcgDistribObject distrib)
    {
        MQBuilder relationsSub = new MQBuilder(EcgReserveRelation.ENTITY_CLASS, "rel")
                .add(MQExpression.eq("rel", EcgReserveRelation.distribution().s(), distrib))
                .add(MQExpression.eqProperty("rel", EcgReserveRelation.entrant().id().s(), "red", RequestedEnrollmentDirection.entrantRequest().entrant().id().s()));
        MQBuilder requestedDirections = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "red")
                .add(MQExpression.exists(relationsSub))
                .add(MQExpression.eq("red", RequestedEnrollmentDirection.enrollmentDirection().competitionGroup().s(), distrib.getCompetitionGroup()));
        return new EntrantDataUtil(getSession(), distrib.getCompetitionGroup().getEnrollmentCampaign(), requestedDirections);
    }

    private static class ReportRow implements IReportRow
    {
        RequestedEnrollmentDirection direction;

        String fio;
        Double finalMark;
        Double averageEduInstMark;
        String[] content;
        State state;

        private ReportRow(String fio, Double finalMark)
        {
            this.fio = fio;
            this.finalMark = finalMark;
        }

        private ReportRow(RequestedEnrollmentDirection direction, EntrantDataUtil dataUtil)
        {
            this.direction = direction;
            this.fio = direction.getEntrantRequest().getEntrant().getPerson().getFullFio();
            this.finalMark = dataUtil.getFinalMark(direction);
            this.averageEduInstMark = dataUtil.getAverageEduInstMark(direction.getEntrantRequest().getEntrant().getId());
        }

        @Override public RequestedEnrollmentDirection getRequestedEnrollmentDirection(){return direction;}
        @Override public Double getFinalMark(){return finalMark;}
        @Override public Double getProfileMark(){return (Double) FastBeanUtils.getValue(direction, RequestedEnrollmentDirection.profileChosenEntranceDiscipline().finalMark().s());}
        @Override public String getFio(){return fio;}
        @Override public Boolean isGraduatedProfileEduInstitution(){return Boolean.FALSE;}
        @Override public Double getCertificateAverageMark(){return averageEduInstMark;}
    }
}