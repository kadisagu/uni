/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec.component.entrant.ProfileEducationOrgUnitEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uniec.entity.entrant.ProfileEducationOrgUnit;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Vasily Zhukov
 * @since 25.04.2012
 */
@Input({
        @Bind(key = "enrollmentDirectionId", binding = "enrollmentDirectionId")
})
public class Model
{
    private Long _enrollmentDirectionId;
    private Map<Long, Boolean> _valueMap;
    private Set<Long> _hiddenIds;
    private Set<Long> _disabledIds;
    private List<ProfileEducationOrgUnit> _dtoList;
    private StaticListDataSource<ProfileEducationOrgUnit> _dataSource;

    public boolean isCheckboxVisible()
    {
        return !_hiddenIds.contains(_dataSource.getCurrentEntity().getId());
    }
    
    public boolean isCheckboxDisabled()
    {
        return _disabledIds.contains(_dataSource.getCurrentEntity().getId());
    }

    // Getters & Setters

    public Long getEnrollmentDirectionId()
    {
        return _enrollmentDirectionId;
    }

    public void setEnrollmentDirectionId(Long enrollmentDirectionId)
    {
        _enrollmentDirectionId = enrollmentDirectionId;
    }

    public Map<Long, Boolean> getValueMap()
    {
        return _valueMap;
    }

    public void setValueMap(Map<Long, Boolean> valueMap)
    {
        _valueMap = valueMap;
    }

    public Set<Long> getHiddenIds()
    {
        return _hiddenIds;
    }

    public void setHiddenIds(Set<Long> hiddenIds)
    {
        _hiddenIds = hiddenIds;
    }

    public Set<Long> getDisabledIds()
    {
        return _disabledIds;
    }

    public void setDisabledIds(Set<Long> disabledIds)
    {
        _disabledIds = disabledIds;
    }

    public List<ProfileEducationOrgUnit> getDtoList()
    {
        return _dtoList;
    }

    public void setDtoList(List<ProfileEducationOrgUnit> dtoList)
    {
        _dtoList = dtoList;
    }

    public StaticListDataSource<ProfileEducationOrgUnit> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(StaticListDataSource<ProfileEducationOrgUnit> dataSource)
    {
        _dataSource = dataSource;
    }
}
