package ru.tandemservice.uniec.entity.entrant;

import org.tandemframework.core.common.ITitled;

import ru.tandemservice.uniec.entity.entrant.gen.EntrantStateExamCertificateGen;
import ru.tandemservice.uniec.ui.EntrantStateExamCertificateNumberFormatter;

/**
 * Сертификат ЕГЭ
 */
public class EntrantStateExamCertificate extends EntrantStateExamCertificateGen implements ITitled
{
    public static final String P_TITLE = "title";

    @Override
    public String getTitle()
    {
        return EntrantStateExamCertificateNumberFormatter.formatStatic(getNumber());
    }
}