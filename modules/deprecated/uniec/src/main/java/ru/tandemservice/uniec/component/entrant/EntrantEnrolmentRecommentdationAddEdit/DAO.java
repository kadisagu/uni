/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantEnrolmentRecommentdationAddEdit;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.info.ErrorCollector;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.catalog.EnrollmentRecommendation;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantEnrolmentRecommendation;

/**
 * @author Боба
 * @since 08.08.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{

    @SuppressWarnings("unchecked")
    @Override
    public void prepare(Model model)
    {
        model.setEntrant(get(Entrant.class, model.getEntrant().getId()));

        Criteria criteria = getSession().createCriteria(EntrantEnrolmentRecommendation.class);
        criteria.createAlias(EntrantEnrolmentRecommendation.L_RECOMMENDATION, "recommendation");
        criteria.add(Restrictions.eq(EntrantEnrolmentRecommendation.L_ENTRANT, model.getEntrant()));
        if (model.getEntrantEnrolmentRecommendation().getId() != null)
        {
            model.setEntrantEnrolmentRecommendation(get(EntrantEnrolmentRecommendation.class, model.getEntrantEnrolmentRecommendation().getId()));
            criteria.add(Restrictions.not(Restrictions.eq(EntrantEnrolmentRecommendation.P_ID, model.getEntrantEnrolmentRecommendation().getId())));
        }
        criteria.setProjection(Projections.property("recommendation." + EnrollmentRecommendation.P_ID));
        List<Long> occupiedIds = criteria.list();

        criteria = getSession().createCriteria(EnrollmentRecommendation.class);
        if (occupiedIds.size() > 0)
        {
            criteria.add(Restrictions.not(Restrictions.in(EnrollmentRecommendation.P_ID, occupiedIds)));
        }

        model.setEnrolmentRecommendationsList(criteria.list());
    }

    @Override
    public void update(Model model)
    {
        if (model.getEntrantEnrolmentRecommendation().getId() == null)
        {
            model.getEntrantEnrolmentRecommendation().setEntrant(model.getEntrant());
        }
        getSession().saveOrUpdate(model.getEntrantEnrolmentRecommendation());
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        Criteria criteria = getSession().createCriteria(EntrantEnrolmentRecommendation.class);
        criteria.add(Restrictions.eq(EntrantEnrolmentRecommendation.L_ENTRANT, model.getEntrant()));
        criteria.add(Restrictions.eq(EntrantEnrolmentRecommendation.L_RECOMMENDATION, model.getEntrantEnrolmentRecommendation().getRecommendation()));
        if (model.getEntrantEnrolmentRecommendation().getId() != null)
        {
            criteria.add(Restrictions.not(Restrictions.eq(EntrantEnrolmentRecommendation.P_ID, model.getEntrantEnrolmentRecommendation().getId())));
        }
        criteria.setProjection(Projections.rowCount());
        Number count = (Number) criteria.uniqueResult();
        if (count.intValue() > 0)
        {
            model.getEntrantEnrolmentRecommendation().setId(null);
            errors.add("Рекомендация «" + model.getEntrantEnrolmentRecommendation().getRecommendation().getTitle() + "» уже указана для текущего абитуриента.", "recomendation");
        }
    }

}
