package ru.tandemservice.uniec.base.entity.ecg;

import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgEntrantRecommended;
import ru.tandemservice.uniec.base.entity.ecg.gen.EcgEntrantRecommendedGen;

/**
 * Рекомендованный абитуриент распределения
 */
public class EcgEntrantRecommended extends EcgEntrantRecommendedGen implements IEcgEntrantRecommended
{
}