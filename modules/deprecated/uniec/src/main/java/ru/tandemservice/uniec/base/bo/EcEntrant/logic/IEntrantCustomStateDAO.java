/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcEntrant.logic;

import ru.tandemservice.uniec.entity.entrant.EntrantCustomState;

import java.util.Date;
import java.util.List;

/**
 * @author nvankov
 * @since 4/3/13
 */
public interface IEntrantCustomStateDAO
{
    void saveOrUpdate(EntrantCustomState entrantCustomState);

    List<EntrantCustomState> getActiveStatesList(Long entrantId, Date date);
}
