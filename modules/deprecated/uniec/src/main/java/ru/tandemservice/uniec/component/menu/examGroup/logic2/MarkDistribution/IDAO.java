/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.menu.examGroup.logic2.MarkDistribution;

import ru.tandemservice.uniec.component.menu.examGroup.abstractMarkDistribution.IMarkDistributionDAO;

/**
 * @author Vasily Zhukov
 * @since 31.05.2010
 */
public interface IDAO extends IMarkDistributionDAO<Model>
{
}
