package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.entity.catalog.ContractAdmissionKind;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Студент предварительного зачисления
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PreliminaryEnrollmentStudentGen extends VersionedEntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent";
    public static final String ENTITY_NAME = "preliminaryEnrollmentStudent";
    public static final int VERSION_HASH = -81270377;
    private static IEntityMeta ENTITY_META;

    public static final String P_VERSION = "version";
    public static final String L_REQUESTED_ENROLLMENT_DIRECTION = "requestedEnrollmentDirection";
    public static final String L_ENTRANT_ENROLLMENT_ORDER_TYPE = "entrantEnrollmentOrderType";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String L_EDUCATION_ORG_UNIT = "educationOrgUnit";
    public static final String L_STUDENT_CATEGORY = "studentCategory";
    public static final String L_CONTRACT_ADMISSION_KIND = "contractAdmissionKind";
    public static final String P_TARGET_ADMISSION = "targetAdmission";
    public static final String P_STUDIED_O_P_P = "studiedOPP";
    public static final String P_PARALLEL = "parallel";
    public static final String P_PARALLEL_LOCKED = "parallelLocked";
    public static final String P_ENROLLMENT_CONDITIONS = "enrollmentConditions";

    private int _version; 
    private RequestedEnrollmentDirection _requestedEnrollmentDirection;     // Выбранное направление приема
    private EntrantEnrollmentOrderType _entrantEnrollmentOrderType;     // Тип приказа на зачисление абитуриента в личный состав студентов
    private CompensationType _compensationType;     // Вид возмещения затрат
    private EducationOrgUnit _educationOrgUnit;     // Параметры обучения студентов по направлению подготовки (НПП)
    private StudentCategory _studentCategory;     // Категория обучаемого
    private ContractAdmissionKind _contractAdmissionKind;     // Виды сверхпланового приема
    private boolean _targetAdmission;     // Целевой прием
    private boolean _studiedOPP;     // Учился в ОПП
    private boolean _parallel;     // Параллельное освоение
    private boolean _parallelLocked;     // Признак 'Параллельное освоение' менять нельзя

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public int getVersion()
    {
        return _version;
    }

    /**
     * @param version  Свойство не может быть null.
     */
    public void setVersion(int version)
    {
        dirty(_version, version);
        _version = version;
    }

    /**
     * @return Выбранное направление приема. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public RequestedEnrollmentDirection getRequestedEnrollmentDirection()
    {
        return _requestedEnrollmentDirection;
    }

    /**
     * @param requestedEnrollmentDirection Выбранное направление приема. Свойство не может быть null и должно быть уникальным.
     */
    public void setRequestedEnrollmentDirection(RequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        dirty(_requestedEnrollmentDirection, requestedEnrollmentDirection);
        _requestedEnrollmentDirection = requestedEnrollmentDirection;
    }

    /**
     * @return Тип приказа на зачисление абитуриента в личный состав студентов. Свойство не может быть null.
     */
    @NotNull
    public EntrantEnrollmentOrderType getEntrantEnrollmentOrderType()
    {
        return _entrantEnrollmentOrderType;
    }

    /**
     * @param entrantEnrollmentOrderType Тип приказа на зачисление абитуриента в личный состав студентов. Свойство не может быть null.
     */
    public void setEntrantEnrollmentOrderType(EntrantEnrollmentOrderType entrantEnrollmentOrderType)
    {
        dirty(_entrantEnrollmentOrderType, entrantEnrollmentOrderType);
        _entrantEnrollmentOrderType = entrantEnrollmentOrderType;
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     */
    @NotNull
    public EducationOrgUnit getEducationOrgUnit()
    {
        return _educationOrgUnit;
    }

    /**
     * @param educationOrgUnit Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     */
    public void setEducationOrgUnit(EducationOrgUnit educationOrgUnit)
    {
        dirty(_educationOrgUnit, educationOrgUnit);
        _educationOrgUnit = educationOrgUnit;
    }

    /**
     * @return Категория обучаемого. Свойство не может быть null.
     */
    @NotNull
    public StudentCategory getStudentCategory()
    {
        return _studentCategory;
    }

    /**
     * @param studentCategory Категория обучаемого. Свойство не может быть null.
     */
    public void setStudentCategory(StudentCategory studentCategory)
    {
        dirty(_studentCategory, studentCategory);
        _studentCategory = studentCategory;
    }

    /**
     * @return Виды сверхпланового приема.
     */
    public ContractAdmissionKind getContractAdmissionKind()
    {
        return _contractAdmissionKind;
    }

    /**
     * @param contractAdmissionKind Виды сверхпланового приема.
     */
    public void setContractAdmissionKind(ContractAdmissionKind contractAdmissionKind)
    {
        dirty(_contractAdmissionKind, contractAdmissionKind);
        _contractAdmissionKind = contractAdmissionKind;
    }

    /**
     * @return Целевой прием. Свойство не может быть null.
     */
    @NotNull
    public boolean isTargetAdmission()
    {
        return _targetAdmission;
    }

    /**
     * @param targetAdmission Целевой прием. Свойство не может быть null.
     */
    public void setTargetAdmission(boolean targetAdmission)
    {
        dirty(_targetAdmission, targetAdmission);
        _targetAdmission = targetAdmission;
    }

    /**
     * @return Учился в ОПП. Свойство не может быть null.
     */
    @NotNull
    public boolean isStudiedOPP()
    {
        return _studiedOPP;
    }

    /**
     * @param studiedOPP Учился в ОПП. Свойство не может быть null.
     */
    public void setStudiedOPP(boolean studiedOPP)
    {
        dirty(_studiedOPP, studiedOPP);
        _studiedOPP = studiedOPP;
    }

    /**
     * @return Параллельное освоение. Свойство не может быть null.
     */
    @NotNull
    public boolean isParallel()
    {
        return _parallel;
    }

    /**
     * @param parallel Параллельное освоение. Свойство не может быть null.
     */
    public void setParallel(boolean parallel)
    {
        dirty(_parallel, parallel);
        _parallel = parallel;
    }

    /**
     * @return Признак 'Параллельное освоение' менять нельзя. Свойство не может быть null.
     */
    @NotNull
    public boolean isParallelLocked()
    {
        return _parallelLocked;
    }

    /**
     * @param parallelLocked Признак 'Параллельное освоение' менять нельзя. Свойство не может быть null.
     */
    public void setParallelLocked(boolean parallelLocked)
    {
        dirty(_parallelLocked, parallelLocked);
        _parallelLocked = parallelLocked;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PreliminaryEnrollmentStudentGen)
        {
            setVersion(((PreliminaryEnrollmentStudent)another).getVersion());
            setRequestedEnrollmentDirection(((PreliminaryEnrollmentStudent)another).getRequestedEnrollmentDirection());
            setEntrantEnrollmentOrderType(((PreliminaryEnrollmentStudent)another).getEntrantEnrollmentOrderType());
            setCompensationType(((PreliminaryEnrollmentStudent)another).getCompensationType());
            setEducationOrgUnit(((PreliminaryEnrollmentStudent)another).getEducationOrgUnit());
            setStudentCategory(((PreliminaryEnrollmentStudent)another).getStudentCategory());
            setContractAdmissionKind(((PreliminaryEnrollmentStudent)another).getContractAdmissionKind());
            setTargetAdmission(((PreliminaryEnrollmentStudent)another).isTargetAdmission());
            setStudiedOPP(((PreliminaryEnrollmentStudent)another).isStudiedOPP());
            setParallel(((PreliminaryEnrollmentStudent)another).isParallel());
            setParallelLocked(((PreliminaryEnrollmentStudent)another).isParallelLocked());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PreliminaryEnrollmentStudentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PreliminaryEnrollmentStudent.class;
        }

        public T newInstance()
        {
            return (T) new PreliminaryEnrollmentStudent();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "version":
                    return obj.getVersion();
                case "requestedEnrollmentDirection":
                    return obj.getRequestedEnrollmentDirection();
                case "entrantEnrollmentOrderType":
                    return obj.getEntrantEnrollmentOrderType();
                case "compensationType":
                    return obj.getCompensationType();
                case "educationOrgUnit":
                    return obj.getEducationOrgUnit();
                case "studentCategory":
                    return obj.getStudentCategory();
                case "contractAdmissionKind":
                    return obj.getContractAdmissionKind();
                case "targetAdmission":
                    return obj.isTargetAdmission();
                case "studiedOPP":
                    return obj.isStudiedOPP();
                case "parallel":
                    return obj.isParallel();
                case "parallelLocked":
                    return obj.isParallelLocked();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "version":
                    obj.setVersion((Integer) value);
                    return;
                case "requestedEnrollmentDirection":
                    obj.setRequestedEnrollmentDirection((RequestedEnrollmentDirection) value);
                    return;
                case "entrantEnrollmentOrderType":
                    obj.setEntrantEnrollmentOrderType((EntrantEnrollmentOrderType) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "educationOrgUnit":
                    obj.setEducationOrgUnit((EducationOrgUnit) value);
                    return;
                case "studentCategory":
                    obj.setStudentCategory((StudentCategory) value);
                    return;
                case "contractAdmissionKind":
                    obj.setContractAdmissionKind((ContractAdmissionKind) value);
                    return;
                case "targetAdmission":
                    obj.setTargetAdmission((Boolean) value);
                    return;
                case "studiedOPP":
                    obj.setStudiedOPP((Boolean) value);
                    return;
                case "parallel":
                    obj.setParallel((Boolean) value);
                    return;
                case "parallelLocked":
                    obj.setParallelLocked((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "version":
                        return true;
                case "requestedEnrollmentDirection":
                        return true;
                case "entrantEnrollmentOrderType":
                        return true;
                case "compensationType":
                        return true;
                case "educationOrgUnit":
                        return true;
                case "studentCategory":
                        return true;
                case "contractAdmissionKind":
                        return true;
                case "targetAdmission":
                        return true;
                case "studiedOPP":
                        return true;
                case "parallel":
                        return true;
                case "parallelLocked":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "version":
                    return true;
                case "requestedEnrollmentDirection":
                    return true;
                case "entrantEnrollmentOrderType":
                    return true;
                case "compensationType":
                    return true;
                case "educationOrgUnit":
                    return true;
                case "studentCategory":
                    return true;
                case "contractAdmissionKind":
                    return true;
                case "targetAdmission":
                    return true;
                case "studiedOPP":
                    return true;
                case "parallel":
                    return true;
                case "parallelLocked":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "version":
                    return Integer.class;
                case "requestedEnrollmentDirection":
                    return RequestedEnrollmentDirection.class;
                case "entrantEnrollmentOrderType":
                    return EntrantEnrollmentOrderType.class;
                case "compensationType":
                    return CompensationType.class;
                case "educationOrgUnit":
                    return EducationOrgUnit.class;
                case "studentCategory":
                    return StudentCategory.class;
                case "contractAdmissionKind":
                    return ContractAdmissionKind.class;
                case "targetAdmission":
                    return Boolean.class;
                case "studiedOPP":
                    return Boolean.class;
                case "parallel":
                    return Boolean.class;
                case "parallelLocked":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PreliminaryEnrollmentStudent> _dslPath = new Path<PreliminaryEnrollmentStudent>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PreliminaryEnrollmentStudent");
    }
            

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent#getVersion()
     */
    public static PropertyPath<Integer> version()
    {
        return _dslPath.version();
    }

    /**
     * @return Выбранное направление приема. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent#getRequestedEnrollmentDirection()
     */
    public static RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> requestedEnrollmentDirection()
    {
        return _dslPath.requestedEnrollmentDirection();
    }

    /**
     * @return Тип приказа на зачисление абитуриента в личный состав студентов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent#getEntrantEnrollmentOrderType()
     */
    public static EntrantEnrollmentOrderType.Path<EntrantEnrollmentOrderType> entrantEnrollmentOrderType()
    {
        return _dslPath.entrantEnrollmentOrderType();
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent#getEducationOrgUnit()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
    {
        return _dslPath.educationOrgUnit();
    }

    /**
     * @return Категория обучаемого. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent#getStudentCategory()
     */
    public static StudentCategory.Path<StudentCategory> studentCategory()
    {
        return _dslPath.studentCategory();
    }

    /**
     * @return Виды сверхпланового приема.
     * @see ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent#getContractAdmissionKind()
     */
    public static ContractAdmissionKind.Path<ContractAdmissionKind> contractAdmissionKind()
    {
        return _dslPath.contractAdmissionKind();
    }

    /**
     * @return Целевой прием. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent#isTargetAdmission()
     */
    public static PropertyPath<Boolean> targetAdmission()
    {
        return _dslPath.targetAdmission();
    }

    /**
     * @return Учился в ОПП. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent#isStudiedOPP()
     */
    public static PropertyPath<Boolean> studiedOPP()
    {
        return _dslPath.studiedOPP();
    }

    /**
     * @return Параллельное освоение. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent#isParallel()
     */
    public static PropertyPath<Boolean> parallel()
    {
        return _dslPath.parallel();
    }

    /**
     * @return Признак 'Параллельное освоение' менять нельзя. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent#isParallelLocked()
     */
    public static PropertyPath<Boolean> parallelLocked()
    {
        return _dslPath.parallelLocked();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent#getEnrollmentConditions()
     */
    public static SupportedPropertyPath<String> enrollmentConditions()
    {
        return _dslPath.enrollmentConditions();
    }

    public static class Path<E extends PreliminaryEnrollmentStudent> extends EntityPath<E>
    {
        private PropertyPath<Integer> _version;
        private RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> _requestedEnrollmentDirection;
        private EntrantEnrollmentOrderType.Path<EntrantEnrollmentOrderType> _entrantEnrollmentOrderType;
        private CompensationType.Path<CompensationType> _compensationType;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnit;
        private StudentCategory.Path<StudentCategory> _studentCategory;
        private ContractAdmissionKind.Path<ContractAdmissionKind> _contractAdmissionKind;
        private PropertyPath<Boolean> _targetAdmission;
        private PropertyPath<Boolean> _studiedOPP;
        private PropertyPath<Boolean> _parallel;
        private PropertyPath<Boolean> _parallelLocked;
        private SupportedPropertyPath<String> _enrollmentConditions;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent#getVersion()
     */
        public PropertyPath<Integer> version()
        {
            if(_version == null )
                _version = new PropertyPath<Integer>(PreliminaryEnrollmentStudentGen.P_VERSION, this);
            return _version;
        }

    /**
     * @return Выбранное направление приема. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent#getRequestedEnrollmentDirection()
     */
        public RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> requestedEnrollmentDirection()
        {
            if(_requestedEnrollmentDirection == null )
                _requestedEnrollmentDirection = new RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection>(L_REQUESTED_ENROLLMENT_DIRECTION, this);
            return _requestedEnrollmentDirection;
        }

    /**
     * @return Тип приказа на зачисление абитуриента в личный состав студентов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent#getEntrantEnrollmentOrderType()
     */
        public EntrantEnrollmentOrderType.Path<EntrantEnrollmentOrderType> entrantEnrollmentOrderType()
        {
            if(_entrantEnrollmentOrderType == null )
                _entrantEnrollmentOrderType = new EntrantEnrollmentOrderType.Path<EntrantEnrollmentOrderType>(L_ENTRANT_ENROLLMENT_ORDER_TYPE, this);
            return _entrantEnrollmentOrderType;
        }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent#getEducationOrgUnit()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
        {
            if(_educationOrgUnit == null )
                _educationOrgUnit = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT, this);
            return _educationOrgUnit;
        }

    /**
     * @return Категория обучаемого. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent#getStudentCategory()
     */
        public StudentCategory.Path<StudentCategory> studentCategory()
        {
            if(_studentCategory == null )
                _studentCategory = new StudentCategory.Path<StudentCategory>(L_STUDENT_CATEGORY, this);
            return _studentCategory;
        }

    /**
     * @return Виды сверхпланового приема.
     * @see ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent#getContractAdmissionKind()
     */
        public ContractAdmissionKind.Path<ContractAdmissionKind> contractAdmissionKind()
        {
            if(_contractAdmissionKind == null )
                _contractAdmissionKind = new ContractAdmissionKind.Path<ContractAdmissionKind>(L_CONTRACT_ADMISSION_KIND, this);
            return _contractAdmissionKind;
        }

    /**
     * @return Целевой прием. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent#isTargetAdmission()
     */
        public PropertyPath<Boolean> targetAdmission()
        {
            if(_targetAdmission == null )
                _targetAdmission = new PropertyPath<Boolean>(PreliminaryEnrollmentStudentGen.P_TARGET_ADMISSION, this);
            return _targetAdmission;
        }

    /**
     * @return Учился в ОПП. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent#isStudiedOPP()
     */
        public PropertyPath<Boolean> studiedOPP()
        {
            if(_studiedOPP == null )
                _studiedOPP = new PropertyPath<Boolean>(PreliminaryEnrollmentStudentGen.P_STUDIED_O_P_P, this);
            return _studiedOPP;
        }

    /**
     * @return Параллельное освоение. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent#isParallel()
     */
        public PropertyPath<Boolean> parallel()
        {
            if(_parallel == null )
                _parallel = new PropertyPath<Boolean>(PreliminaryEnrollmentStudentGen.P_PARALLEL, this);
            return _parallel;
        }

    /**
     * @return Признак 'Параллельное освоение' менять нельзя. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent#isParallelLocked()
     */
        public PropertyPath<Boolean> parallelLocked()
        {
            if(_parallelLocked == null )
                _parallelLocked = new PropertyPath<Boolean>(PreliminaryEnrollmentStudentGen.P_PARALLEL_LOCKED, this);
            return _parallelLocked;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent#getEnrollmentConditions()
     */
        public SupportedPropertyPath<String> enrollmentConditions()
        {
            if(_enrollmentConditions == null )
                _enrollmentConditions = new SupportedPropertyPath<String>(PreliminaryEnrollmentStudentGen.P_ENROLLMENT_CONDITIONS, this);
            return _enrollmentConditions;
        }

        public Class getEntityClass()
        {
            return PreliminaryEnrollmentStudent.class;
        }

        public String getEntityName()
        {
            return "preliminaryEnrollmentStudent";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getEnrollmentConditions();
}
