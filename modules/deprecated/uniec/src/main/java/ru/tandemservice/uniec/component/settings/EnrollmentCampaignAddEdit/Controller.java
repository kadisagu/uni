/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.settings.EnrollmentCampaignAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uniec.entity.settings.CompetitionGroupRestriction;
import ru.tandemservice.uniec.entity.settings.EnrollmentDirectionRestriction;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.ArrayList;
import java.util.List;

/**
 * @author vip_delete
 * @since 11.02.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        // на форме добавления устанавливаем значение по умолчанию
        if (model.getEnrollmentCampaign().getId() == null)
        {
            // Прием на бюджетные места и по договору, по умолчанию "Не отличается"
            model.setEnrollmentPerCompensationType(model.getEnrollmentPerCompensationTypeList().get(1));

            // Конкурсные группы, по умолчанию "Не использовать"
            model.setCompetitionGroup(model.getCompetitionGroupList().get(0));

            // Зачтение диплома олимпиады, по умолчанию "За дисциплины по одному выбранному направлению приема"
            model.setOlympiadDiplomaRule(model.getOlympiadDiplomaRuleList().get(0));

            //учитывать приоритеты вступительных испытаний, по умолчанию - нет
            model.setPrioritiesAdmissionTestRule(model.getPrioritiesAdmissionTestRuleList().get(1));

            onChangeCompetitionGroup(component);
        }
    }

    // Listeners

    public void onChangeEducationYear(IBusinessComponent component)
    {
        Model model = getModel(component);
        EducationYear educationYear = model.getEnrollmentCampaign().getEducationYear();
        if (educationYear != null)
            model.getEnrollmentCampaign().setTitle(educationYear.getTitle());
    }

    public void onChangeCompetitionGroup(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getCompetitionGroup() == null) return;
        if (Model.COMPETITION_GROUP_NO_USE == model.getCompetitionGroup().getId())
        {
            List<EnrollmentDirectionRestriction> list = model.getDirectionRestrictionList();
            if (list == null)
                model.setDirectionRestrictionList(list = new ArrayList<EnrollmentDirectionRestriction>());
            if (list.isEmpty())
            {
                EnrollmentDirectionRestriction dto = new EnrollmentDirectionRestriction();
                dto.setDevelopForm(DataAccessServices.dao().getByCode(DevelopForm.class, DevelopFormCodes.FULL_TIME_FORM));
                dto.setCompensationType(DataAccessServices.dao().getByCode(CompensationType.class, UniDefines.COMPENSATION_TYPE_BUDGET));
                dto.setMaxEnrollmentDirection(3);
                dto.setMaxMinisterialDirection(null);
                dto.setMaxFormativeOrgUnit(null);
                list.add(dto);
            }
        } else
        {
            List<CompetitionGroupRestriction> list = model.getCompetitionGroupRestrictionList();
            if (list == null)
                model.setCompetitionGroupRestrictionList(list = new ArrayList<CompetitionGroupRestriction>());
            if (list.isEmpty())
            {
                CompetitionGroupRestriction dto = new CompetitionGroupRestriction();
                dto.setDevelopForm(DataAccessServices.dao().getByCode(DevelopForm.class, DevelopFormCodes.FULL_TIME_FORM));
                dto.setCompensationType(DataAccessServices.dao().getByCode(CompensationType.class, UniDefines.COMPENSATION_TYPE_BUDGET));
                dto.setMaxCompetitionGroup(3);
                list.add(dto);
            }
        }
    }

    public void onClickDeleteDirRestriction(IBusinessComponent component)
    {
        getModel(component).getDirectionRestrictionList().remove(component.<Integer>getListenerParameter() - 1);
    }

    public void onClickAddDirRestriction(IBusinessComponent component)
    {
        getModel(component).getDirectionRestrictionList().add(new EnrollmentDirectionRestriction());
    }

    public void onClickDeleteCGRestriction(IBusinessComponent component)
    {
        getModel(component).getCompetitionGroupRestrictionList().remove(component.<Integer>getListenerParameter() - 1);
    }

    public void onClickAddCGRestriction(IBusinessComponent component)
    {
        getModel(component).getCompetitionGroupRestrictionList().add(new CompetitionGroupRestriction());
    }

    public void onClickApply(IBusinessComponent component)
    {
        getDao().update(getModel(component));

        deactivate(component);
    }
}
