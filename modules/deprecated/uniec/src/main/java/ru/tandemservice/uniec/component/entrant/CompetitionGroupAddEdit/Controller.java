/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.CompetitionGroupAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import java.util.Collection;

/**
 * @author vip_delete
 * @since 27.05.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);
        model.setMessageSource(getMessageSource());

        prepareEnrollmentDirectionDataSource(component);
        prepareChoosedEnrollmentDirectionDataSource(component);
    }

    @SuppressWarnings({"unchecked", "deprecation"})
    private void prepareEnrollmentDirectionDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<EnrollmentDirection> dataSource = new DynamicListDataSource(component, component1 -> {
            getDao().prepareEnrollmentDirectionDataSource(model);
        });
        BlockColumn<Boolean> blockColumn = new BlockColumn<>(Model.CHECKBOX_COLUMN, "");
        blockColumn.setStyle("width:16px");
        blockColumn.setHeaderStyle("width:16px");
        dataSource.addColumn(blockColumn);
        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", EnrollmentDirection.P_TITLE));
        dataSource.addColumn(new SimpleColumn("Форма освоения", EnrollmentDirection.educationOrgUnit().developForm().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Условие освоения", EnrollmentDirection.educationOrgUnit().developCondition().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Технология освоения", EnrollmentDirection.educationOrgUnit().developTech().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Срок освоения", EnrollmentDirection.educationOrgUnit().developPeriod().title().s()).setClickable(false).setOrderable(false));
        model.setDataSource(dataSource);
    }

    private void prepareChoosedEnrollmentDirectionDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getChoosenDataSource() != null) return;

        DynamicListDataSource<EnrollmentDirection> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareChoosenEnrollmentDirectionDataSource(model);
        });
        dataSource.addColumn(new CheckboxColumn(Model.CHECKBOX_SELECTED_COLUMN));
        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", EnrollmentDirection.P_TITLE));
        dataSource.addColumn(new SimpleColumn("Формирующее подр.", EnrollmentDirection.FORMATIVE_ORG_UNIT_SHORT_TITLE_KEY).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Территориальное подр.", EnrollmentDirection.TERRITORIAL_ORG_UNIT_SHORT_TITLE_KEY).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Форма освоения", EnrollmentDirection.educationOrgUnit().developForm().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Условие освоения", EnrollmentDirection.educationOrgUnit().developCondition().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Технология освоения", EnrollmentDirection.educationOrgUnit().developTech().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Срок освоения", EnrollmentDirection.educationOrgUnit().developPeriod().title().s()).setClickable(false).setOrderable(false));
        model.setChoosenDataSource(dataSource);
    }

    // listeners

    public void onClickMove(IBusinessComponent component)
    {
        getDao().selectEnrollmentDirection(getModel(component));
    }

    public void onClickDeleteSelected(IBusinessComponent component)
    {
        Model model = getModel(component);
        Collection<IEntity> selected = ((CheckboxColumn) model.getChoosenDataSource().getColumn(Model.CHECKBOX_SELECTED_COLUMN)).getSelectedObjects();
        model.getSelectedEnrollmentDirectionSet().removeAll(UniBaseDao.ids(selected));
    }

    public void onClickApply(IBusinessComponent component)
    {
        getDao().update(getModel(component));
        deactivate(component);
    }
}
