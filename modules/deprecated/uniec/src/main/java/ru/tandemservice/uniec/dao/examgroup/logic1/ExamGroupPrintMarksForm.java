/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.dao.examgroup.logic1;

import org.tandemframework.rtf.modifiers.RtfInjectModifier;

import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.examgroup.AbstractExamGroupPrintBuilder;
import ru.tandemservice.uniec.entity.entrant.ExamGroup;

/**
 * @author Vasily Zhukov
 * @since 31.05.2010
 */
class ExamGroupPrintMarksForm extends AbstractExamGroupPrintBuilder
{
    @Override
    protected boolean isWithResult()
    {
        return true;
    }

    @Override
    protected String getTemplateCode()
    {
        return UniecDefines.EXAM_GROUP_LIST_ALG1;
    }

    @Override
    protected RtfInjectModifier getRtfInjectModifier(ExamGroup group)
    {
        // декодируем ключ группы
        ExamGroupLogicSample1.ExamGroupKey key = ExamGroupLogicSample1.parseKey(getSession(), group.getKey());

        return super.getRtfInjectModifier(group)
                .put("examGroupTypeTitle", key.getExamGroupType().getTitle())
                .put("developForm", key.getDevelopForm().getTitle())
                .put("compensationTypeStr", key.getCompensationType().isBudget() ? "За счет средств федерального бюджета" : "На места с оплатой стоимости обучения");
    }
}
