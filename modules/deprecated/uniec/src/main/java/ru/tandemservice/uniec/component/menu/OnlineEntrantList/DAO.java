/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.menu.OnlineEntrantList;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineRequestedEnrollmentDirection;
import ru.tandemservice.uniec.ui.EnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.regex.Pattern;

/**
 * @author agolubenko
 * @since May 11, 2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("onlineEntrant");
    static
    {
        _orderSettings.setOrders(OnlineEntrant.P_FULL_FIO,
                                 new OrderDescription(OnlineEntrant.lastName().s()),
                                 new OrderDescription(OnlineEntrant.firstName().s()),
                                 new OrderDescription(OnlineEntrant.middleName().s()));
    }

    @Override
    public void prepare(Model model)
    {
        Session session = getSession();

        EntrantFilterUtil.prepareEnrollmentCampaignFilterWithSecurity(model, session);
        model.setFormativeOrgUnitModel(EnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(model));
        model.setTerritorialOrgUnitModel(EnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(model));
        model.setEducationLevelsHighSchoolModel(EnrollmentDirectionUtil.createEducationLevelsHighSchoolModel(model));
        model.setDevelopFormList(EducationCatalogsManager.getDevelopFormSelectModel());
    }

    protected MQBuilder createDataSourceBuilder(Model model)
    {
        IDataSettings settings = model.getSettings();

        String lastName = settings.get("lastName");
        String firstName = settings.get("firstName");
        String middleName = settings.get("middleName");
        String personalNumber = settings.get("personalNumber");
        String identityCardSeria = settings.get("identityCardSeria");
        String identityCardNumber = settings.get("identityCardNumber");

        OrgUnit formativeOrgUnit = model.getFormativeOrgUnit();
        OrgUnit territorialOrgUnit = model.getTerritorialOrgUnit();
        EducationLevelsHighSchool educationLevelsHighSchool = model.getEducationLevelsHighSchool();
        DevelopForm developForm = model.getDevelopForm();

        MQBuilder builder = new MQBuilder(OnlineEntrant.ENTITY_CLASS, "onlineEntrant");
        builder.add(MQExpression.eq("onlineEntrant", OnlineEntrant.enrollmentCampaign().s(), model.getEnrollmentCampaign()));
        if (StringUtils.isNotEmpty(lastName))
        {
            builder.add(MQExpression.like("onlineEntrant", OnlineEntrant.lastName().s(), CoreStringUtils.escapeLike(lastName)));
        }
        if (StringUtils.isNotEmpty(firstName))
        {
            builder.add(MQExpression.like("onlineEntrant", OnlineEntrant.firstName().s(), CoreStringUtils.escapeLike(firstName)));
        }
        if (StringUtils.isNotEmpty(middleName))
        {
            builder.add(MQExpression.like("onlineEntrant", OnlineEntrant.middleName().s(), CoreStringUtils.escapeLike(middleName)));
        }
        if (StringUtils.isNotEmpty(personalNumber))
        {
            if(Pattern.compile("\\d{4}-\\d+").matcher(personalNumber).matches())
            {
                int year = Integer.parseInt(personalNumber.substring(0, 4));
                builder.add(MQExpression.eq("onlineEntrant", OnlineEntrant.registrationYear().s(), year));
                int number = Integer.parseInt(personalNumber.substring(5));
                builder.add(MQExpression.eq("onlineEntrant", OnlineEntrant.personalNumber().s(), number));
            }
            else if(StringUtils.isNumeric(personalNumber))
            {
                int number = Integer.parseInt(personalNumber);
                builder.add(MQExpression.eq("onlineEntrant", OnlineEntrant.personalNumber().s(), number));
            }
        }
        if (StringUtils.isNotEmpty(identityCardSeria))
        {
            builder.add(MQExpression.eq("onlineEntrant", OnlineEntrant.passportSeria().s(), identityCardSeria));
        }
        if (StringUtils.isNotEmpty(identityCardNumber))
        {
            builder.add(MQExpression.eq("onlineEntrant", OnlineEntrant.passportNumber().s(), identityCardNumber));
        }
        if (formativeOrgUnit != null || territorialOrgUnit != null || educationLevelsHighSchool != null || developForm != null)
        {
            MQBuilder directionsBuilder = new MQBuilder(OnlineRequestedEnrollmentDirection.ENTITY_CLASS, "onlineRequestedEnrollmentDirection");
            directionsBuilder.addJoin("onlineRequestedEnrollmentDirection", OnlineRequestedEnrollmentDirection.entrant().s(), "entrant");
            directionsBuilder.getSelectAliasList().clear();
            directionsBuilder.addSelect("entrant", new Object[] { OnlineEntrant.id().s() });
            directionsBuilder.setNeedDistinct(true);

            directionsBuilder.addJoin("onlineRequestedEnrollmentDirection", OnlineRequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().s(), "educationOrgUnit");
            directionsBuilder.add(MQExpression.eqProperty("onlineRequestedEnrollmentDirection", OnlineRequestedEnrollmentDirection.entrant().id().s(), "onlineEntrant", OnlineEntrant.id().s()));

            if (formativeOrgUnit != null)
            {
                directionsBuilder.add(MQExpression.eq("educationOrgUnit", EducationOrgUnit.formativeOrgUnit().s(), formativeOrgUnit));
            }
            if (territorialOrgUnit != null)
            {
                directionsBuilder.add(MQExpression.eq("educationOrgUnit", EducationOrgUnit.territorialOrgUnit().s(), territorialOrgUnit));
            }
            if (educationLevelsHighSchool != null)
            {
                directionsBuilder.add(MQExpression.eq("educationOrgUnit", EducationOrgUnit.educationLevelHighSchool().s(), educationLevelsHighSchool));
            }
            if (developForm != null)
            {
                directionsBuilder.add(MQExpression.eq("educationOrgUnit", EducationOrgUnit.developForm().s(), developForm));
            }

            builder.add(MQExpression.in("onlineEntrant", OnlineEntrant.id().s(), directionsBuilder));
        }

        return builder;
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = createDataSourceBuilder(model);
        _orderSettings.applyOrder(builder, model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }

    @Override
    public byte[] getPrintOnlineEntrantRequest(Long onlineEntrantId) throws Exception
    {
        return UniecDAOFacade.getOnlineEntrantRegistrationDAO().getPrintOnlineEntrantRequest(getNotNull(OnlineEntrant.class, onlineEntrantId).getUserId());
    }
}
