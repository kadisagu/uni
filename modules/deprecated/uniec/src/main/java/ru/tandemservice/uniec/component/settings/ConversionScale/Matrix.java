/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.settings.ConversionScale;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.tandemservice.uniec.entity.settings.ConversionScale;

/**
 * Класс представляющий собой матрицу для заполнения данных шкал перевода
 * @author agolubenko
 * @since 25.02.2009
 */
class Matrix
{
    private String _id; // уникальный идентификатор матрицы на странице
    private List<ScaleColumn> _horizontalObjects = new ArrayList<ScaleColumn>(); // балл по внутренней шкале
    private List<ConversionScale> _verticalObjects = new ArrayList<ConversionScale>(); // шкалы перевода

    private Map<Long, ConversionScale> _id2ConversionScale = new HashMap<Long, ConversionScale>();

    public Matrix(int minMark, int maxMark, double step)
    {
        _id = minMark + ":" + maxMark + ":" + step;
        
        for (double mark = minMark; mark <= maxMark; mark += step) // создание колонок с заданным шагом от минимального до максимального баллов 
        {
            _horizontalObjects.add(new ScaleColumn(mark));
        }
    }

    public void addConversionScale(ConversionScale conversionScale)
    {
        _verticalObjects.add(conversionScale);
        _id2ConversionScale.put(conversionScale.getId(), conversionScale);
    }

    public ConversionScale getConversionScale(Long id)
    {
        return _id2ConversionScale.get(id);
    }
    
    public String getId()
    {
        return _id;
    }

    public List<ScaleColumn> getHorizontalObjects()
    {
        return _horizontalObjects;
    }

    public List<ConversionScale> getVerticalObjects()
    {
        return _verticalObjects;
    }
}
