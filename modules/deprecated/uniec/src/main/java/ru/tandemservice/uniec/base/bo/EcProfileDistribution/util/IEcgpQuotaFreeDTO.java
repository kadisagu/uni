/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec.base.bo.EcProfileDistribution.util;

import java.util.Map;

/**
 * Свободные места по профилям
 *
 * @author Vasily Zhukov
 * @since 10.05.2012
 */
public interface IEcgpQuotaFreeDTO
{
    /**
     * @return планы приема по профилям
     */
    IEcgpQuotaDTO getQuotaDTO();

    /**
     * @return занятые места по профилям
     */
    IEcgpQuotaUsedDTO getQuotaUsedDTO();

    /**
     * @return суммарное количество свободных мест
     */
    int getTotalFree();

    /**
     * @return профиль -> свободно мест
     *         если свободно мест равно null, то значит план по профилю не ограничен (свободные места всегда есть)
     */
    Map<Long, Integer> getFreeMap();
}
