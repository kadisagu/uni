/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.wizard.PersonalDataStep;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import org.tandemframework.shared.person.base.entity.PersonDormitoryBenefit;
import org.tandemframework.shared.person.base.entity.PersonForeignLanguage;
import org.tandemframework.shared.person.base.entity.PersonSportAchievement;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.component.wizard.IEntrantWizardComponents;

import java.util.Date;
import java.util.List;

/**
 * @author vip_delete
 * @since 15.06.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        prepareBenefitListDataSource(component);
        prepareLanguageListDataSource(component);
        prepareSportListDataSource(component);
        prepareDormitoryBenefitDataSource(component);
    }

    // Prepare Data Source

    private void prepareBenefitListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getBenefitDataSource() != null) return;

        DynamicListDataSource<PersonBenefit> dataSource = new DynamicListDataSource<>(component, component1 -> {
            model.getBenefitDataSource().setCountRow(model.getSelectedBenefitList().size());
            UniBaseUtils.createPage(model.getBenefitDataSource(), model.getSelectedBenefitList());
        });

        dataSource.addColumn(new SimpleColumn("Название льготы", PersonBenefit.benefit().title()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Документ", PersonBenefit.document()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дата выдачи", PersonBenefit.P_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Основание", PersonBenefit.P_BASIC).setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteBenefit"));
        model.setBenefitDataSource(dataSource);
    }

    private void prepareLanguageListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getLanguageDataSource() != null) return;

        DynamicListDataSource<PersonForeignLanguage> dataSource = new DynamicListDataSource<>(component, component1 -> {
            model.getLanguageDataSource().setCountRow(model.getSelectedLanguageList().size());
            UniBaseUtils.createPage(model.getLanguageDataSource(), model.getSelectedLanguageList());
        });
        dataSource.addColumn(new SimpleColumn("Язык", PersonForeignLanguage.language().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Степень владения", PersonForeignLanguage.skill().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Основной", PersonForeignLanguage.P_MAIN, YesNoFormatter.IGNORE_NUMBERS).setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteLanguage"));
        model.setLanguageDataSource(dataSource);
    }

    private void prepareSportListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getSportDataSource() != null) return;

        DynamicListDataSource<PersonSportAchievement> dataSource = new DynamicListDataSource<>(component, component1 -> {
            model.getSportDataSource().setCountRow(model.getSelectedSportList().size());
            UniBaseUtils.createPage(model.getSportDataSource(), model.getSelectedSportList());
        });

        dataSource.addColumn(new SimpleColumn("Вид спорта", PersonSportAchievement.sportKind().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Разряд/звание", PersonSportAchievement.sportRank().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Наивысшее достижение", PersonSportAchievement.P_BEST_ACHIEVEMENT).setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteSport"));

        model.setSportDataSource(dataSource);
    }

    private void prepareDormitoryBenefitDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDormitoryBenefitDataSource() != null)
        {
            return;
        }

        DynamicListDataSource<PersonDormitoryBenefit> dataSource = new DynamicListDataSource<>(component, component1 -> {
            List<PersonDormitoryBenefit> list = model.getPersonDormitoryBenefitList();
            model.getDormitoryBenefitDataSource().setCountRow(list.size());
            UniBaseUtils.createPage(model.getDormitoryBenefitDataSource(), list);
        });

        dataSource.addColumn(new SimpleColumn("Льгота для поселения", PersonDormitoryBenefit.benefit().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Название и номер документа", PersonDormitoryBenefit.documentTitleAndNumber().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дата выдачи", PersonDormitoryBenefit.issuanceDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteDormitoryBenefit"));

        model.setDormitoryBenefitDataSource(dataSource);
    }


    // Event Listeners

    public void onClickSelectBenefit(IBusinessComponent component)
    {
        ErrorCollector errors = component.getUserContext().getErrorCollector();
        final Model model = getModel(component);

        if (model.getPersonBenefit().getBenefit() == null)
            errors.add("Поле \"Льгота\" обязательно для заполнения", "benefit");
        else
        {
            for (int i = 0; i < model.getSelectedBenefitList().size(); i++)
            {
                String str = model.getSelectedBenefitList().get(i).getBenefit().getCode();
                if (str.equalsIgnoreCase(model.getPersonBenefit().getBenefit().getCode()))
                {
                    errors.add("Нельзя выбрать одну и ту же льготу.", "benefit");
                }
            }
        }

        if (model.getPersonBenefit().getDate() != null && model.getPersonBenefit().getDate().after(new Date()))
            errors.add("Дата получения льготы должна быть меньше текущей.", "personBenefitDateId");

        if (errors.hasErrors()) return;


        getDao().prepareSelectBenefit(getModel(component));
    }

    public void onClickDeleteBenefit(IBusinessComponent component)
    {
        getDao().deleteBenefit(getModel(component), (Long) component.getListenerParameter());
    }

    public void onClickSelectLanguage(IBusinessComponent component)
    {
        final ErrorCollector errors = component.getUserContext().getErrorCollector();
        final Model model = getModel(component);

        if (model.getForeignLanguage().getLanguage() == null)
            errors.add("Поле \"Язык\" обязательно для заполнения", "language");
        else
        {
            for (int i = 0; i < model.getSelectedLanguageList().size(); i++)
            {
                String str = model.getSelectedLanguageList().get(i).getLanguage().getCode();
                if (str.equalsIgnoreCase(model.getForeignLanguage().getLanguage().getCode()))
                {
                    errors.add("Нельзя выбрать один и тот же язык.", "language");
                }
            }
        }
        if (errors.hasErrors()) return;

        getDao().prepareSelectLanguage(getModel(component));
    }

    public void onClickDeleteLanguage(IBusinessComponent component)
    {
        getDao().deleteLanguage(getModel(component), (Long) component.getListenerParameter());
    }

    public void onClickSelectSport(IBusinessComponent component)
    {
        final ErrorCollector errors = component.getUserContext().getErrorCollector();
        final Model model = getModel(component);

        if (model.getAchievement().getSportKind() == null)
            errors.add("Поле \"Вид спорта\" обязательно для заполнения", "sportkind");
        else
        {

            for (int i = 0; i < model.getSelectedSportList().size(); i++)
            {
                String str = model.getSelectedSportList().get(i).getSportKind().getCode();
                if (str.equalsIgnoreCase(model.getAchievement().getSportKind().getCode()))
                {
                    errors.add("Нельзя выбрать один и тот же вид спорта.", "sportkind");
                }
            }
        }
        if (model.getAchievement().getSportRank() == null)
            errors.add("Поле \"Разряд/звание\" обязательно для заполнения", "sportRank");

        if (errors.hasErrors()) return;

        getDao().prepareSelectSport(getModel(component));
    }

    public void onClickDeleteSport(IBusinessComponent component)
    {
        getDao().deleteSport(getModel(component), (Long) component.getListenerParameter());
    }

    public void onChangeNeedDormitory(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.setHasDormitoryBenefits(false);
        model.getPersonDormitoryBenefitList().clear();
    }

    public void onClickSelectDormitoryBenefit(IBusinessComponent component)
    {
        getDao().prepareSelectDormitoryBenefit(getModel(component));
    }

    public void onClickDeleteDormitoryBenefit(IBusinessComponent component)
    {
        getDao().deleteDormitoryBenefit(getModel(component), (Long) component.getListenerParameter());
    }

    // Wizard Actions

    public void onClickNext(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().update(model);

        deactivate(component);

        component.getParentRegion().getOwner().createDefaultChildRegion(new ComponentActivator(IEntrantWizardComponents.CONTACT_DATA_STEP, new ParametersMap()
                .add("entrantId", model.getEntrant().getId())
                .add("onlineEntrantId", model.getOnlineEntrant().getId())
                .add("entrantMasterPermKey", model.getEntrantMasterPermKey())
        ));
    }

    public void onClickSkip(IBusinessComponent component)
    {
        Model model = getModel(component);

        deactivate(component);

        component.getParentRegion().getOwner().createDefaultChildRegion(new ComponentActivator(IEntrantWizardComponents.CONTACT_DATA_STEP, new ParametersMap()
                .add("entrantId", model.getEntrant().getId())
                .add("onlineEntrantId", model.getOnlineEntrant().getId())
                .add("entrantMasterPermKey", model.getEntrantMasterPermKey())
        ));
    }

    public void onClickStop(IBusinessComponent component)
    {
        getDao().update(getModel(component));

        deactivate(component);
    }
}
