/* $Id$ */
package ru.tandemservice.uniec.component.listextract.ent2.e1;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.DegreeToExtractRelation;
import ru.tandemservice.movestudent.entity.SetDiplomaWorkTopicAndScientificAdviserStuListExtract;
import ru.tandemservice.movestudent.entity.StatusToExtractRelation;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.pps.PpsEntryByEmployeePost;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.uniec.entity.orders.SplitBudgetMasterEntrantsStuListExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 21.08.2012
 */
public class SplitBudgetMasterEntrantsStuListExtractPrint implements IPrintFormCreator<SplitBudgetMasterEntrantsStuListExtract>, IListParagraphPrintFormCreator<SplitBudgetMasterEntrantsStuListExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, final SplitBudgetMasterEntrantsStuListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);
        modifier.modify(document);
        return document;
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, SplitBudgetMasterEntrantsStuListExtract firstExtract)
    {
        final EducationOrgUnit eo = firstExtract.getEntity().getEducationOrgUnit();

        EducationLevels eduLevel = EducationOrgUnitUtil.getParentLevel(firstExtract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool());
        return CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract)
                .put("developCondition_G", CommonExtractPrint.getDevelopConditionStr2_G(eo.getDevelopCondition()))
                .put("developForm_G", CommonExtractPrint.getDevelopFormStr_G(eo.getDevelopForm()))
                .put("compensationType_G", CommonExtractPrint.getCompensationTypeStr_G(firstExtract.getEntity().getCompensationType()))
                .put("educationLevel", eo.getEducationLevelHighSchool().getEducationLevel().getLevelType().getTitle())
                .put("speciality", eduLevel != null ? eduLevel.getTitle() : "")
                .put("course", firstExtract.getEntity().getCourse().getTitle())
                .put("orgUnit_G", firstExtract.getEntity().getEducationOrgUnit().getFormativeOrgUnit().getGenitiveCaseTitle());
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, SplitBudgetMasterEntrantsStuListExtract firstExtract)
    {
        RtfTableModifier tableModifier = new RtfTableModifier();

        int cnt = 1;
        List<String[]> paragraphDataLines = new ArrayList<>();
        for (Object ext : paragraph.getExtractList())
        {
            final SetDiplomaWorkTopicAndScientificAdviserStuListExtract extract = (SetDiplomaWorkTopicAndScientificAdviserStuListExtract) ext;
            String statusAndPost = "";

            List<DegreeToExtractRelation> degreeToExtractRelationList = UniDaoFacade.getCoreDao().getCalculatedValue(session -> {
                final DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(DegreeToExtractRelation.class, "y").column(DQLExpressions.property("y"));
                dql.where(DQLExpressions.eq(DQLExpressions.property(DegreeToExtractRelation.extract().fromAlias("y")), DQLExpressions.value(extract.getId())));
                return dql.createStatement(session).list();
            });


            for (DegreeToExtractRelation degreeToExtractRelation : degreeToExtractRelationList)
                statusAndPost += degreeToExtractRelation.getDegree().getShortTitle() + ", ";

            List<StatusToExtractRelation> statusToExtractRelationList = UniDaoFacade.getCoreDao().getCalculatedValue(session -> {
                final DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(StatusToExtractRelation.class, "y").column(DQLExpressions.property("y"));
                dql.where(DQLExpressions.eq(DQLExpressions.property(DegreeToExtractRelation.extract().fromAlias("y")), DQLExpressions.value(extract.getId())));
                return dql.createStatement(session).list();
            });


            for (StatusToExtractRelation statusToExtractRelation : statusToExtractRelationList)
                statusAndPost += statusToExtractRelation.getStatus().getShortTitle() + ", ";

            if (extract.getScientificAdviserName() instanceof PpsEntryByEmployeePost)
                statusAndPost += ((PpsEntryByEmployeePost) extract.getScientificAdviserName()).getPost().getTitle() + " ";
            statusAndPost += extract.getScientificAdviserName().getOrgUnit().getGenitiveCaseTitle() != null ? extract.getScientificAdviserName().getOrgUnit().getGenitiveCaseTitle() : extract.getScientificAdviserName().getOrgUnit().getTitle();

            paragraphDataLines.add(new String[]{String.valueOf(cnt++) + ".", extract.getEntity().getPerson().getFullFio(), extract.getDiplomaWorkTopicNew(), extract.getScientificAdviserName().getPerson().getFullFio(), statusAndPost});
        }

        tableModifier.put("T", paragraphDataLines.toArray(new String[][]{}));

        return tableModifier;
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, SplitBudgetMasterEntrantsStuListExtract firstExtract)
    {
    }
}