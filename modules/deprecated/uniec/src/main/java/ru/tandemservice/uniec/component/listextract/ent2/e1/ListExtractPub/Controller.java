/* $Id$ */
package ru.tandemservice.uniec.component.listextract.ent2.e1.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.AbstractListExtractPubController;
import ru.tandemservice.uniec.entity.orders.SplitBudgetMasterEntrantsStuListExtract;

/**
 * @author Dmitry Seleznev
 * @since 21.08.2012
 */
public class Controller extends AbstractListExtractPubController<SplitBudgetMasterEntrantsStuListExtract, Model, IDAO>
{
}