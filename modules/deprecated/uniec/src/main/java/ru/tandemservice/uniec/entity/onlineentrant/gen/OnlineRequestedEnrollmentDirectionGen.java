package ru.tandemservice.uniec.entity.onlineentrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineRequestedEnrollmentDirection;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выбранное онлайн направление приема
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OnlineRequestedEnrollmentDirectionGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.onlineentrant.OnlineRequestedEnrollmentDirection";
    public static final String ENTITY_NAME = "onlineRequestedEnrollmentDirection";
    public static final int VERSION_HASH = 2090303321;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT = "entrant";
    public static final String L_ENROLLMENT_DIRECTION = "enrollmentDirection";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String L_STUDENT_CATEGORY = "studentCategory";
    public static final String L_COMPETITION_KIND = "competitionKind";
    public static final String P_TARGET_ADMISSION = "targetAdmission";
    public static final String P_PROFILE_WORK_EXPERIENCE_YEARS = "profileWorkExperienceYears";
    public static final String P_PROFILE_WORK_EXPERIENCE_MONTHS = "profileWorkExperienceMonths";
    public static final String P_PROFILE_WORK_EXPERIENCE_DAYS = "profileWorkExperienceDays";

    private OnlineEntrant _entrant;     // Онлайн-абитуриент
    private EnrollmentDirection _enrollmentDirection;     // Направление подготовки (специальность) приема
    private CompensationType _compensationType;     // Вид возмещения затрат
    private StudentCategory _studentCategory;     // Категория обучаемого
    private CompetitionKind _competitionKind;     // Вид конкурса
    private boolean _targetAdmission;     // Целевой прием
    private Integer _profileWorkExperienceYears;     // Стаж работы по профилю направления подготовки (Лет)
    private Integer _profileWorkExperienceMonths;     // Стаж работы по профилю направления подготовки (Месяцев)
    private Integer _profileWorkExperienceDays;     // Стаж работы по профилю направления подготовки (Дней)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Онлайн-абитуриент. Свойство не может быть null.
     */
    @NotNull
    public OnlineEntrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant Онлайн-абитуриент. Свойство не может быть null.
     */
    public void setEntrant(OnlineEntrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Направление подготовки (специальность) приема. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentDirection getEnrollmentDirection()
    {
        return _enrollmentDirection;
    }

    /**
     * @param enrollmentDirection Направление подготовки (специальность) приема. Свойство не может быть null.
     */
    public void setEnrollmentDirection(EnrollmentDirection enrollmentDirection)
    {
        dirty(_enrollmentDirection, enrollmentDirection);
        _enrollmentDirection = enrollmentDirection;
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Категория обучаемого. Свойство не может быть null.
     */
    @NotNull
    public StudentCategory getStudentCategory()
    {
        return _studentCategory;
    }

    /**
     * @param studentCategory Категория обучаемого. Свойство не может быть null.
     */
    public void setStudentCategory(StudentCategory studentCategory)
    {
        dirty(_studentCategory, studentCategory);
        _studentCategory = studentCategory;
    }

    /**
     * @return Вид конкурса. Свойство не может быть null.
     */
    @NotNull
    public CompetitionKind getCompetitionKind()
    {
        return _competitionKind;
    }

    /**
     * @param competitionKind Вид конкурса. Свойство не может быть null.
     */
    public void setCompetitionKind(CompetitionKind competitionKind)
    {
        dirty(_competitionKind, competitionKind);
        _competitionKind = competitionKind;
    }

    /**
     * @return Целевой прием. Свойство не может быть null.
     */
    @NotNull
    public boolean isTargetAdmission()
    {
        return _targetAdmission;
    }

    /**
     * @param targetAdmission Целевой прием. Свойство не может быть null.
     */
    public void setTargetAdmission(boolean targetAdmission)
    {
        dirty(_targetAdmission, targetAdmission);
        _targetAdmission = targetAdmission;
    }

    /**
     * @return Стаж работы по профилю направления подготовки (Лет).
     */
    public Integer getProfileWorkExperienceYears()
    {
        return _profileWorkExperienceYears;
    }

    /**
     * @param profileWorkExperienceYears Стаж работы по профилю направления подготовки (Лет).
     */
    public void setProfileWorkExperienceYears(Integer profileWorkExperienceYears)
    {
        dirty(_profileWorkExperienceYears, profileWorkExperienceYears);
        _profileWorkExperienceYears = profileWorkExperienceYears;
    }

    /**
     * @return Стаж работы по профилю направления подготовки (Месяцев).
     */
    public Integer getProfileWorkExperienceMonths()
    {
        return _profileWorkExperienceMonths;
    }

    /**
     * @param profileWorkExperienceMonths Стаж работы по профилю направления подготовки (Месяцев).
     */
    public void setProfileWorkExperienceMonths(Integer profileWorkExperienceMonths)
    {
        dirty(_profileWorkExperienceMonths, profileWorkExperienceMonths);
        _profileWorkExperienceMonths = profileWorkExperienceMonths;
    }

    /**
     * @return Стаж работы по профилю направления подготовки (Дней).
     */
    public Integer getProfileWorkExperienceDays()
    {
        return _profileWorkExperienceDays;
    }

    /**
     * @param profileWorkExperienceDays Стаж работы по профилю направления подготовки (Дней).
     */
    public void setProfileWorkExperienceDays(Integer profileWorkExperienceDays)
    {
        dirty(_profileWorkExperienceDays, profileWorkExperienceDays);
        _profileWorkExperienceDays = profileWorkExperienceDays;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof OnlineRequestedEnrollmentDirectionGen)
        {
            setEntrant(((OnlineRequestedEnrollmentDirection)another).getEntrant());
            setEnrollmentDirection(((OnlineRequestedEnrollmentDirection)another).getEnrollmentDirection());
            setCompensationType(((OnlineRequestedEnrollmentDirection)another).getCompensationType());
            setStudentCategory(((OnlineRequestedEnrollmentDirection)another).getStudentCategory());
            setCompetitionKind(((OnlineRequestedEnrollmentDirection)another).getCompetitionKind());
            setTargetAdmission(((OnlineRequestedEnrollmentDirection)another).isTargetAdmission());
            setProfileWorkExperienceYears(((OnlineRequestedEnrollmentDirection)another).getProfileWorkExperienceYears());
            setProfileWorkExperienceMonths(((OnlineRequestedEnrollmentDirection)another).getProfileWorkExperienceMonths());
            setProfileWorkExperienceDays(((OnlineRequestedEnrollmentDirection)another).getProfileWorkExperienceDays());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OnlineRequestedEnrollmentDirectionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OnlineRequestedEnrollmentDirection.class;
        }

        public T newInstance()
        {
            return (T) new OnlineRequestedEnrollmentDirection();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entrant":
                    return obj.getEntrant();
                case "enrollmentDirection":
                    return obj.getEnrollmentDirection();
                case "compensationType":
                    return obj.getCompensationType();
                case "studentCategory":
                    return obj.getStudentCategory();
                case "competitionKind":
                    return obj.getCompetitionKind();
                case "targetAdmission":
                    return obj.isTargetAdmission();
                case "profileWorkExperienceYears":
                    return obj.getProfileWorkExperienceYears();
                case "profileWorkExperienceMonths":
                    return obj.getProfileWorkExperienceMonths();
                case "profileWorkExperienceDays":
                    return obj.getProfileWorkExperienceDays();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entrant":
                    obj.setEntrant((OnlineEntrant) value);
                    return;
                case "enrollmentDirection":
                    obj.setEnrollmentDirection((EnrollmentDirection) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "studentCategory":
                    obj.setStudentCategory((StudentCategory) value);
                    return;
                case "competitionKind":
                    obj.setCompetitionKind((CompetitionKind) value);
                    return;
                case "targetAdmission":
                    obj.setTargetAdmission((Boolean) value);
                    return;
                case "profileWorkExperienceYears":
                    obj.setProfileWorkExperienceYears((Integer) value);
                    return;
                case "profileWorkExperienceMonths":
                    obj.setProfileWorkExperienceMonths((Integer) value);
                    return;
                case "profileWorkExperienceDays":
                    obj.setProfileWorkExperienceDays((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entrant":
                        return true;
                case "enrollmentDirection":
                        return true;
                case "compensationType":
                        return true;
                case "studentCategory":
                        return true;
                case "competitionKind":
                        return true;
                case "targetAdmission":
                        return true;
                case "profileWorkExperienceYears":
                        return true;
                case "profileWorkExperienceMonths":
                        return true;
                case "profileWorkExperienceDays":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entrant":
                    return true;
                case "enrollmentDirection":
                    return true;
                case "compensationType":
                    return true;
                case "studentCategory":
                    return true;
                case "competitionKind":
                    return true;
                case "targetAdmission":
                    return true;
                case "profileWorkExperienceYears":
                    return true;
                case "profileWorkExperienceMonths":
                    return true;
                case "profileWorkExperienceDays":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entrant":
                    return OnlineEntrant.class;
                case "enrollmentDirection":
                    return EnrollmentDirection.class;
                case "compensationType":
                    return CompensationType.class;
                case "studentCategory":
                    return StudentCategory.class;
                case "competitionKind":
                    return CompetitionKind.class;
                case "targetAdmission":
                    return Boolean.class;
                case "profileWorkExperienceYears":
                    return Integer.class;
                case "profileWorkExperienceMonths":
                    return Integer.class;
                case "profileWorkExperienceDays":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OnlineRequestedEnrollmentDirection> _dslPath = new Path<OnlineRequestedEnrollmentDirection>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OnlineRequestedEnrollmentDirection");
    }
            

    /**
     * @return Онлайн-абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineRequestedEnrollmentDirection#getEntrant()
     */
    public static OnlineEntrant.Path<OnlineEntrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Направление подготовки (специальность) приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineRequestedEnrollmentDirection#getEnrollmentDirection()
     */
    public static EnrollmentDirection.Path<EnrollmentDirection> enrollmentDirection()
    {
        return _dslPath.enrollmentDirection();
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineRequestedEnrollmentDirection#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Категория обучаемого. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineRequestedEnrollmentDirection#getStudentCategory()
     */
    public static StudentCategory.Path<StudentCategory> studentCategory()
    {
        return _dslPath.studentCategory();
    }

    /**
     * @return Вид конкурса. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineRequestedEnrollmentDirection#getCompetitionKind()
     */
    public static CompetitionKind.Path<CompetitionKind> competitionKind()
    {
        return _dslPath.competitionKind();
    }

    /**
     * @return Целевой прием. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineRequestedEnrollmentDirection#isTargetAdmission()
     */
    public static PropertyPath<Boolean> targetAdmission()
    {
        return _dslPath.targetAdmission();
    }

    /**
     * @return Стаж работы по профилю направления подготовки (Лет).
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineRequestedEnrollmentDirection#getProfileWorkExperienceYears()
     */
    public static PropertyPath<Integer> profileWorkExperienceYears()
    {
        return _dslPath.profileWorkExperienceYears();
    }

    /**
     * @return Стаж работы по профилю направления подготовки (Месяцев).
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineRequestedEnrollmentDirection#getProfileWorkExperienceMonths()
     */
    public static PropertyPath<Integer> profileWorkExperienceMonths()
    {
        return _dslPath.profileWorkExperienceMonths();
    }

    /**
     * @return Стаж работы по профилю направления подготовки (Дней).
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineRequestedEnrollmentDirection#getProfileWorkExperienceDays()
     */
    public static PropertyPath<Integer> profileWorkExperienceDays()
    {
        return _dslPath.profileWorkExperienceDays();
    }

    public static class Path<E extends OnlineRequestedEnrollmentDirection> extends EntityPath<E>
    {
        private OnlineEntrant.Path<OnlineEntrant> _entrant;
        private EnrollmentDirection.Path<EnrollmentDirection> _enrollmentDirection;
        private CompensationType.Path<CompensationType> _compensationType;
        private StudentCategory.Path<StudentCategory> _studentCategory;
        private CompetitionKind.Path<CompetitionKind> _competitionKind;
        private PropertyPath<Boolean> _targetAdmission;
        private PropertyPath<Integer> _profileWorkExperienceYears;
        private PropertyPath<Integer> _profileWorkExperienceMonths;
        private PropertyPath<Integer> _profileWorkExperienceDays;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Онлайн-абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineRequestedEnrollmentDirection#getEntrant()
     */
        public OnlineEntrant.Path<OnlineEntrant> entrant()
        {
            if(_entrant == null )
                _entrant = new OnlineEntrant.Path<OnlineEntrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Направление подготовки (специальность) приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineRequestedEnrollmentDirection#getEnrollmentDirection()
     */
        public EnrollmentDirection.Path<EnrollmentDirection> enrollmentDirection()
        {
            if(_enrollmentDirection == null )
                _enrollmentDirection = new EnrollmentDirection.Path<EnrollmentDirection>(L_ENROLLMENT_DIRECTION, this);
            return _enrollmentDirection;
        }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineRequestedEnrollmentDirection#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Категория обучаемого. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineRequestedEnrollmentDirection#getStudentCategory()
     */
        public StudentCategory.Path<StudentCategory> studentCategory()
        {
            if(_studentCategory == null )
                _studentCategory = new StudentCategory.Path<StudentCategory>(L_STUDENT_CATEGORY, this);
            return _studentCategory;
        }

    /**
     * @return Вид конкурса. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineRequestedEnrollmentDirection#getCompetitionKind()
     */
        public CompetitionKind.Path<CompetitionKind> competitionKind()
        {
            if(_competitionKind == null )
                _competitionKind = new CompetitionKind.Path<CompetitionKind>(L_COMPETITION_KIND, this);
            return _competitionKind;
        }

    /**
     * @return Целевой прием. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineRequestedEnrollmentDirection#isTargetAdmission()
     */
        public PropertyPath<Boolean> targetAdmission()
        {
            if(_targetAdmission == null )
                _targetAdmission = new PropertyPath<Boolean>(OnlineRequestedEnrollmentDirectionGen.P_TARGET_ADMISSION, this);
            return _targetAdmission;
        }

    /**
     * @return Стаж работы по профилю направления подготовки (Лет).
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineRequestedEnrollmentDirection#getProfileWorkExperienceYears()
     */
        public PropertyPath<Integer> profileWorkExperienceYears()
        {
            if(_profileWorkExperienceYears == null )
                _profileWorkExperienceYears = new PropertyPath<Integer>(OnlineRequestedEnrollmentDirectionGen.P_PROFILE_WORK_EXPERIENCE_YEARS, this);
            return _profileWorkExperienceYears;
        }

    /**
     * @return Стаж работы по профилю направления подготовки (Месяцев).
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineRequestedEnrollmentDirection#getProfileWorkExperienceMonths()
     */
        public PropertyPath<Integer> profileWorkExperienceMonths()
        {
            if(_profileWorkExperienceMonths == null )
                _profileWorkExperienceMonths = new PropertyPath<Integer>(OnlineRequestedEnrollmentDirectionGen.P_PROFILE_WORK_EXPERIENCE_MONTHS, this);
            return _profileWorkExperienceMonths;
        }

    /**
     * @return Стаж работы по профилю направления подготовки (Дней).
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineRequestedEnrollmentDirection#getProfileWorkExperienceDays()
     */
        public PropertyPath<Integer> profileWorkExperienceDays()
        {
            if(_profileWorkExperienceDays == null )
                _profileWorkExperienceDays = new PropertyPath<Integer>(OnlineRequestedEnrollmentDirectionGen.P_PROFILE_WORK_EXPERIENCE_DAYS, this);
            return _profileWorkExperienceDays;
        }

        public Class getEntityClass()
        {
            return OnlineRequestedEnrollmentDirection.class;
        }

        public String getEntityName()
        {
            return "onlineRequestedEnrollmentDirection";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
