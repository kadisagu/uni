/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.dao.examgroup.logic2;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniec.dao.examgroup.AbstractExamGroupLogic;
import ru.tandemservice.uniec.dao.examgroup.ExamGroupInfo;
import ru.tandemservice.uniec.dao.examgroup.IExamGroupInfo;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Логика работы экзаменационной группы, пример 2 (используется в УГМА)
 * Уникальный ключ: enrollmentDirection, compensationType
 *
 * @author Vasily Zhukov
 * @since 31.05.2010
 */
public class ExamGroupLogicSample2 extends AbstractExamGroupLogic
{
    @Override
    public List<IExamGroupInfo> getExamGroupInfo(EntrantRequest entrantRequest)
    {
        Session session = getSession();

        // получаем первое направление приема из заявления
        List<RequestedEnrollmentDirection> list = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "r")
                .add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST, entrantRequest))
                .addOrder("r", RequestedEnrollmentDirection.P_PRIORITY)
                .getResultList(session, 0, 1);

        if (list.size() == 0) return Collections.emptyList(); // пустое заявление. ключ нельзя создать

        RequestedEnrollmentDirection first = list.get(0);
        EnrollmentDirection enrollmentDirection = first.getEnrollmentDirection();
        EducationLevelsHighSchool highSchool = enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool();

        // сокращенное название направления подготовки (специальности) вуза
        String highSchoolShortTitle = highSchool.getShortTitle();

        // если есть это сокр.название, то его следует отделять черточкой от индекса группы
        String index = StringUtils.isEmpty(highSchoolShortTitle) ? "${index}" : highSchoolShortTitle + "-${index}";

        // часть формы освоения и вида возмещения затрат
        String postfix = first.getEnrollmentDirection().getEducationOrgUnit().getDevelopForm().getGroup() + (first.getCompensationType().isBudget() ? "" : "д");

        // сокращенное название
        String shortTitle = index + (StringUtils.isEmpty(postfix) ? "" : "-" + postfix);

        // название группы
        String title = "Группа №" + shortTitle;

        String key = createKey(enrollmentDirection, first.getCompensationType());

        List<IExamGroupInfo> resultList = new ArrayList<IExamGroupInfo>();
        resultList.add(new ExamGroupInfo(key, title, shortTitle));
        return resultList;
    }

    @Override
    public String getExamGroupListBusinessComponent()
    {
        return "ru.tandemservice.uniec.component.menu.examGroup.logic2.ExamGroupList";
    }

    @Override
    public String getMarkDistributionBusinessComponent()
    {
        return "ru.tandemservice.uniec.component.menu.examGroup.logic2.MarkDistribution";
    }

    public static String createKey(EnrollmentDirection enrollmentDirection, CompensationType compensationType)
    {
        return enrollmentDirection.getId() + ";" + compensationType.getId();
    }

    public static ExamGroupKey parseKey(Session session, String key)
    {
        String[] part = StringUtils.split(key, ';');
        EnrollmentDirection enrollmentDirection = (EnrollmentDirection) session.get(EnrollmentDirection.class, Long.parseLong(part[0]));
        CompensationType compensationType = (CompensationType) session.get(CompensationType.class, Long.parseLong(part[1]));
        return new ExamGroupKey(enrollmentDirection, compensationType);
    }

    public static final class ExamGroupKey
    {
        private EnrollmentDirection _enrollmentDirection;
        private CompensationType _compensationType;

        public ExamGroupKey(EnrollmentDirection enrollmentDirection, CompensationType compensationType)
        {
            _enrollmentDirection = enrollmentDirection;
            _compensationType = compensationType;
        }

        public EnrollmentDirection getEnrollmentDirection()
        {
            return _enrollmentDirection;
        }

        public CompensationType getCompensationType()
        {
            return _compensationType;
        }
    }
}
