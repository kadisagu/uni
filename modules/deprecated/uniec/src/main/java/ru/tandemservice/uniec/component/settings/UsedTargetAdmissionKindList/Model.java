// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.settings.UsedTargetAdmissionKindList;

import java.util.List;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

/**
 * @author oleyba
 * @since 07.07.2010
 */
public class Model  implements IEnrollmentCampaignSelectModel
{
    public static final String P_IN_USE = "inUse";
    public static final Long USE_EXTERNAL_ORG_UNIT_YES = 0L;
    public static final Long USE_EXTERNAL_ORG_UNIT_NO = 1L;

    private List<EnrollmentCampaign> _enrollmentCampaignList;
    private IDataSettings _settings;
    private DynamicListDataSource _dataSource;
    private List<IdentifiableWrapper> _externalOrgUnitChooseList;
    private IdentifiableWrapper _externalOrgUnitChoose;

    // IEnrollmentCampaignSelectModel

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return (EnrollmentCampaign) getSettings().get(ENROLLMENT_CAMPAIGN_FILTER_NAME);
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        getSettings().set(ENROLLMENT_CAMPAIGN_FILTER_NAME, enrollmentCampaign);
    }

    // Getters & Setters

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    @Override
    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public DynamicListDataSource getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource dataSource)
    {
        _dataSource = dataSource;
    }

    public List<IdentifiableWrapper> getExternalOrgUnitChooseList()
    {
        return _externalOrgUnitChooseList;
    }

    public void setExternalOrgUnitChooseList(List<IdentifiableWrapper> externalOrgUnitChooseList)
    {
        _externalOrgUnitChooseList = externalOrgUnitChooseList;
    }

    public IdentifiableWrapper getExternalOrgUnitChoose()
    {
        return _externalOrgUnitChoose;
    }

    public void setExternalOrgUnitChoose(IdentifiableWrapper externalOrgUnitChoose)
    {
        _externalOrgUnitChoose = externalOrgUnitChoose;
    }
}
