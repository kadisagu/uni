/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcOrder.ui.RevertPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager;
import ru.tandemservice.uniec.base.bo.EcOrder.ui.RevertAddEdit.EcOrderRevertAddEdit;
import ru.tandemservice.uniec.base.bo.EcOrder.ui.RevertParAddEdit.EcOrderRevertParAddEdit;
import ru.tandemservice.uniec.base.bo.EcOrder.ui.RevertParAddEdit.EcOrderRevertParAddEditUI;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.unimove.dao.MoveDaoFacade;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;

/**
 * @author Nikolay Fedorovskih
 * @since 06.08.2013
 */
@State({
               @Bind(key = UIPresenter.PUBLISHER_ID, binding = "orderId", required = true)
       })
public class EcOrderRevertPubUI extends UIPresenter
{
    private Long orderId;
    private EnrollmentOrder order;
    private CommonPostfixPermissionModel secModel;
    private Long visingStatus;

    @Override
    public void onComponentRefresh()
    {
        order = DataAccessServices.dao().getNotNull(EnrollmentOrder.class, getOrderId());
        secModel = new CommonPostfixPermissionModel("enrollmentOrder");
        visingStatus = UnimvDaoFacade.getVisaDao().getVisingStatus(order);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EcOrderRevertPub.ORDER_PARAM, order);
    }

    // Listeners

    public void onClickEdit()
    {
        _uiActivation.asDesktopRoot(EcOrderRevertAddEdit.class)
                .parameter(PublisherActivator.PUBLISHER_ID_KEY, order.getId())
                .activate();
    }

    public void onClickAddParagraph()
    {
        _uiActivation.asDesktopRoot(EcOrderRevertParAddEdit.class)
                .parameter(EcOrderRevertParAddEditUI.PARAMETER_ORDER_ID, order.getId())
                .activate();
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asDesktopRoot(EcOrderRevertParAddEdit.class)
                .parameter(EcOrderRevertParAddEditUI.PARAMETER_PARAGRAPH_ID, getListenerParameterAsLong())
                .activate();
    }

    public void onDeleteEntityFromList()
    {
        EcOrderManager.instance().dao().deleteRevertParagraph(getListenerParameterAsLong());
    }

    public void onClickDeleteOrder()
    {
        MoveDaoFacade.getMoveDao().deleteOrderWithExtracts(order);
        deactivate();
    }

    public void onClickReject()
    {
        EcOrderManager.instance().dao().doReject(order);
    }

    public void onClickCommit()
    {
        EcOrderManager.instance().dao().doCommit(order);
    }

    public void onClickSendToFormative()
    {
        EcOrderManager.instance().dao().doSendToFormative(order);
    }

    public void onClickSendToCoordination()
    {
        EcOrderManager.instance().dao().doSendToCoordination(order, getUserContext().getPrincipalContext());
    }

    public void onClickPrintOrder()
    {
        EcOrderManager.instance().dao().getDownloadPrintForm(order.getId(), false);
    }

    // Getters & Setters

    public EnrollmentOrder getOrder()
    {
        return order;
    }

    public String getPageTitle()
    {
        return "Приказ «" + order.getType().getTitle() + "» " + EcOrderRevertPub.orderTitleFormatter().format(order);
    }

    public Long getOrderId()
    {
        return orderId;
    }

    public void setOrderId(Long orderId)
    {
        this.orderId = orderId;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return secModel;
    }

    public Long getVisingStatus()
    {
        return visingStatus;
    }
}