/**
 *$Id$
 */
package ru.tandemservice.uniec.events.dset;

import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.transaction.sync.ParamTransactionCompleteListener;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;

import java.util.Collection;

/**
 * @author Alexander Zhebko
 * @since 13.03.2014
 */
public class EntrantRequestSaveListener extends ParamTransactionCompleteListener<Boolean>
{
    public void init()
    {
        Boolean ecEnabled = Boolean.valueOf(ApplicationRuntime.getProperty("uniec.enabled"));
        if (!ecEnabled)
            DSetEventManager.getInstance().registerListener(DSetEventType.beforeInsert, EntrantRequest.class, this);
    }

    @Override
    public Boolean beforeCompletion(final Session session, final Collection<Long> params)
    {
        throw new ApplicationException("Внесение данных запрещено в связи с вводом в действие новой версии модуля.");
    }
}