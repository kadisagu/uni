/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.ChooseEntranceEducationOrgUnit;

import java.util.Map;

import org.tandemframework.core.component.State;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.examset.ExamSet;

/**
 * @author Боба
 * @since 22.08.2008
 */
@State(keys = {"examSetId"}, bindings = {"examSetId"})
public class ChooseEntranceEducationOrgUnitModel
{
    public static final String EXAM_SET_ID = "examSetId";

    private String _examSetId;
    private ExamSet _examSet;
    private OrgUnit _formativeOrgUnit;
    private OrgUnit _territorialOrgUnit;
    private DynamicListDataSource<EnrollmentDirection> _dataSource;
    private ISelectModel _formativeOrgUnitModel;
    private ISelectModel _territorialOrgUnitModel;
    private IPrincipalContext _principalContext;
    private Map<Long, String> _id2title;// entranceEducationOrgUnitId -> examSet.title

    // Getters & Setters

    public String getExamSetId()
    {
        return _examSetId;
    }

    public void setExamSetId(String examSetId)
    {
        _examSetId = examSetId;
    }

    public ExamSet getExamSet()
    {
        return _examSet;
    }

    public void setExamSet(ExamSet examSet)
    {
        _examSet = examSet;
    }

    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        _formativeOrgUnit = formativeOrgUnit;
    }

    public OrgUnit getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    public void setTerritorialOrgUnit(OrgUnit territorialOrgUnit)
    {
        _territorialOrgUnit = territorialOrgUnit;
    }

    public DynamicListDataSource<EnrollmentDirection> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<EnrollmentDirection> dataSource)
    {
        _dataSource = dataSource;
    }

    public ISelectModel getFormativeOrgUnitModel()
    {
        return _formativeOrgUnitModel;
    }

    public void setFormativeOrgUnitModel(ISelectModel formativeOrgUnitModel)
    {
        _formativeOrgUnitModel = formativeOrgUnitModel;
    }

    public ISelectModel getTerritorialOrgUnitModel()
    {
        return _territorialOrgUnitModel;
    }

    public void setTerritorialOrgUnitModel(ISelectModel territorialOrgUnitModel)
    {
        _territorialOrgUnitModel = territorialOrgUnitModel;
    }

    public IPrincipalContext getPrincipalContext()
    {
        return _principalContext;
    }

    public void setPrincipalContext(IPrincipalContext principalContext)
    {
        _principalContext = principalContext;
    }

    public Map<Long, String> getId2title()
    {
        return _id2title;
    }

    public void setId2title(Map<Long, String> id2title)
    {
        _id2title = id2title;
    }
}
