/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec.component.entrant.PriorityProfileEduOuEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniec.entity.entrant.PriorityProfileEduOu;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

/**
 * @author Vasily Zhukov
 * @since 27.04.2012
 */
@Input({
        @Bind(key = "requestedEnrollmentDirectionId", binding = "requestedEnrollmentDirection.id", required = true)
})
public class Model
{
    private RequestedEnrollmentDirection _requestedEnrollmentDirection = new RequestedEnrollmentDirection();
    private DynamicListDataSource<PriorityProfileEduOuDTO> _dataSource;

    // Getters & Setters

    public RequestedEnrollmentDirection getRequestedEnrollmentDirection()
    {
        return _requestedEnrollmentDirection;
    }

    public void setRequestedEnrollmentDirection(RequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        _requestedEnrollmentDirection = requestedEnrollmentDirection;
    }

    public DynamicListDataSource<PriorityProfileEduOuDTO> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<PriorityProfileEduOuDTO> dataSource)
    {
        _dataSource = dataSource;
    }
}
