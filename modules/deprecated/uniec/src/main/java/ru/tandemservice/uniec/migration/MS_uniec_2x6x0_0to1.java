/* $Id:$ */
package ru.tandemservice.uniec.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Date;

/**
 * @author Denis Perminov
 * @since 23.05.2014
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniec_2x6x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.15"),
                        new ScriptDependency("org.tandemframework.shared", "1.6.0"),
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // DEV-4960
        Statement selStmt = tool.getConnection().createStatement();
        selStmt.execute("select " +
                "ord.ID," +
                "ord.EXCLUDEORDERDATE_P," +
                "ord.EXCLUDEORDERNUMBER_P," +
                "ord.SPLTTSPCLZTNORDRDT_P," +
                "ord.SPLTTSPCLZTNORDRNMBR_P," +
                "ase.ID," +
                "ase.PREVORDERDATE_P," +
                "ase.PREVORDERNUMBER_P " +
                "from ORDERDATA_T as ord " +
                "left join STUDENT_T as std on std.ID = ord.STUDENT_ID " +
                "left join ABSTRACTSTUDENTEXTRACT_T as ase on ase.ENTITY_ID = std.ID " +
                "left join STUDENTEXTRACTTYPE_T as sxt on sxt.ID = ase.TYPE_ID " +
                "left join ABSTRACTSTUDENTPARAGRAPH_T as asp on asp.ID = ase.PARAGRAPH_ID " +
                "left join ABSTRACTSTUDENTORDER_T as aso on aso.ID = asp.ORDER_ID " +
                "where ord.EXCLUDEORDERDATE_P is not null and ord.EXCLUDEORDERNUMBER_P is not null " +
                "and aso.COMMITDATE_P = ord.EXCLUDEORDERDATE_P and aso.NUMBER_P = ord.EXCLUDEORDERNUMBER_P " +
                "and sxt.CODE_P like '2.ent%'" +
                "order by std.ID");
        ResultSet selRslt = selStmt.getResultSet();

        PreparedStatement ordUpdate = tool.prepareStatement("update ORDERDATA_T set SPLTTSPCLZTNORDRDT_P=?, SPLTTSPCLZTNORDRNMBR_P=?, EXCLUDEORDERDATE_P=?, EXCLUDEORDERNUMBER_P=? where ID=?");
        PreparedStatement extUpdate = tool.prepareStatement("update ABSTRACTSTUDENTEXTRACT_T set PREVORDERDATE_P=?, PREVORDERNUMBER_P=? where ID=?");

        int i = 0;
        while (selRslt.next())
        {
            Long orderId = selRslt.getLong(1);

            Date exclOrdDate = selRslt.getDate(2);
            String exclOrdNumber = selRslt.getString(3);
            Date spltOrdDate = selRslt.getDate(4);
            String spltOrdNumber = selRslt.getString(5);

            Long extractId = selRslt.getLong(6);

            Date prevDate = selRslt.getDate(7);
            String prevNumber = selRslt.getString(8);

            if (null == prevNumber && null == spltOrdNumber)
            {
                ordUpdate.setDate(1, exclOrdDate);
                ordUpdate.setString(2, exclOrdNumber);
                ordUpdate.setDate(3, null);
                ordUpdate.setString(4, null);
                ordUpdate.setLong(5, orderId);
                ordUpdate.addBatch();
            }
            if (null != prevNumber && null == spltOrdNumber)
            {
                ordUpdate.setDate(1, exclOrdDate);
                ordUpdate.setString(2, exclOrdNumber);
                ordUpdate.setDate(3, prevDate);
                ordUpdate.setString(4, prevNumber);
                ordUpdate.setLong(5, orderId);
                ordUpdate.addBatch();

                extUpdate.setDate(1, null);
                extUpdate.setString(2, null);
                extUpdate.setLong(3, extractId);
                extUpdate.addBatch();
            }
            if (null == prevNumber && null != spltOrdNumber)
            {
                if (spltOrdDate.after(exclOrdDate))
                {
                    ordUpdate.setDate(1, spltOrdDate);
                    ordUpdate.setString(2, spltOrdNumber);

                    extUpdate.setDate(1, exclOrdDate);
                    extUpdate.setString(2, exclOrdNumber);
                }
                else
                {
                    ordUpdate.setDate(1, exclOrdDate);
                    ordUpdate.setString(2, exclOrdNumber);

                    extUpdate.setDate(1, spltOrdDate);
                    extUpdate.setString(2, spltOrdNumber);
                }
                ordUpdate.setDate(3, null);
                ordUpdate.setString(4, null);
                ordUpdate.setLong(5, orderId);
                ordUpdate.addBatch();

                extUpdate.setLong(3, extractId);
                extUpdate.addBatch();
            }
            if (null != prevNumber && null != spltOrdNumber)
            {
                if (spltOrdDate.after(exclOrdDate))
                {
                    ordUpdate.setDate(1, spltOrdDate);
                    ordUpdate.setString(2, spltOrdNumber);
                    ordUpdate.setDate(3, prevDate);
                    ordUpdate.setString(4, prevNumber);
                    ordUpdate.setLong(5, orderId);

                    extUpdate.setDate(1, null);
                    extUpdate.setString(2, null);
                    extUpdate.setLong(3, extractId);
                }
                else
                {
                    ordUpdate.setDate(1, exclOrdDate);
                    ordUpdate.setString(2, exclOrdNumber);
                    ordUpdate.setDate(3, prevDate);
                    ordUpdate.setString(4, prevNumber);
                    ordUpdate.setLong(5, orderId);

                    extUpdate.setDate(1, null);
                    extUpdate.setString(2, null);
                    extUpdate.setLong(3, extractId);
                }
                ordUpdate.addBatch();
                extUpdate.addBatch();
            }

            if (++i % 256 == 0)
            {
                ordUpdate.executeBatch();
                extUpdate.executeBatch();
            }
        }
        ordUpdate.executeBatch();
        extUpdate.executeBatch();
    }
}