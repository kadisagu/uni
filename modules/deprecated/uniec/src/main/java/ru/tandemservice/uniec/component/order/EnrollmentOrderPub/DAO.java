/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.order.EnrollmentOrderPub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniec.entity.orders.EnrollmentParagraph;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;

import java.util.*;

/**
 * @author vip_delete
 * @since 08.04.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setOrder(getNotNull(EnrollmentOrder.class, model.getOrder().getId()));
        model.setState(model.getOrder().getState());
        model.setSecModel(new CommonPostfixPermissionModel(model.getSecPostfix()));
        model.setVisingStatus(UnimvDaoFacade.getVisaDao().getVisingStatus(model.getOrder()));
        model.setType(EcOrderManager.instance().dao().getEnrollmentOrderType(model.getOrder().getEnrollmentCampaign(), model.getOrder().getType()));
    }

    @SuppressWarnings("unchecked")
    @Override
    public void prepareListDataSource(Model model)
    {
        List<EnrollmentParagraph> paragraphList = new ArrayList<>();
        Map<EnrollmentParagraph, List<EnrollmentExtract>> map = new HashMap<>();

        MQBuilder builder = new MQBuilder(EnrollmentExtract.ENTITY_CLASS, "e");
        builder.addJoinFetch("e", IAbstractExtract.L_ENTITY, "p");
        builder.addJoinFetch("p", PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT, "ou");
        builder.addJoinFetch("ou", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, "formOrgUnit");
        builder.addLeftJoinFetch("ou", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, "terrOrgUnit");
        builder.addJoinFetch("ou", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "hs");
        builder.addJoinFetch("ou", EducationOrgUnit.L_DEVELOP_FORM, "developForm");
        builder.addJoinFetch("ou", EducationOrgUnit.L_DEVELOP_CONDITION, "developCondition");
        builder.addJoinFetch("ou", EducationOrgUnit.L_DEVELOP_TECH, "developTech");
        builder.addJoinFetch("ou", EducationOrgUnit.L_DEVELOP_PERIOD, "developPeriod");

        builder.add(MQExpression.eq("e", IAbstractExtract.L_PARAGRAPH + "." + IAbstractParagraph.L_ORDER, model.getOrder()));

        for (EnrollmentExtract extract : builder.<EnrollmentExtract>getResultList(getSession()))
        {
            EnrollmentParagraph paragraph = (EnrollmentParagraph) extract.getParagraph();
            List<EnrollmentExtract> extractList = map.get(paragraph);
            if (extractList == null)
            {
                paragraphList.add(paragraph);
                map.put(paragraph, extractList = new ArrayList<>());
            }
            extractList.add(extract);
        }

        Collections.sort(paragraphList, (o1, o2) -> Integer.compare(o1.getNumber(), o2.getNumber()));

        model.getDataSource().setCountRow(paragraphList.size());
        UniBaseUtils.createPage(model.getDataSource(), paragraphList);

        for (ViewWrapper viewWrapper : ViewWrapper.getPatchedList(model.getDataSource()))
        {
            EnrollmentParagraph paragraph = (EnrollmentParagraph) viewWrapper.getEntity();
            List<EnrollmentExtract> extractList = map.get(paragraph);
            EnrollmentExtract extract = extractList.get(0);
            EducationOrgUnit orgUnit = extract.getEntity().getEducationOrgUnit();

            Set<String> territorialOrgUnitTitles = new TreeSet<>();
            for (EnrollmentExtract e : extractList)
                if (e.getEntity().getEducationOrgUnit().getTerritorialOrgUnit() != null)
                    territorialOrgUnitTitles.add(e.getEntity().getEducationOrgUnit().getTerritorialOrgUnit().getTerritorialTitle());

            Set<String> developConditionTitles = new TreeSet<>();
            for (EnrollmentExtract e : extractList)
                developConditionTitles.add(e.getEntity().getEducationOrgUnit().getDevelopCondition().getTitle());

            Set<String> developTechTitles = new TreeSet<>();
            for (EnrollmentExtract e : extractList)
                developTechTitles.add(e.getEntity().getEducationOrgUnit().getDevelopTech().getTitle());

            Set<String> developPeriodTitles = new TreeSet<>();
            for (EnrollmentExtract e : extractList)
                developPeriodTitles.add(e.getEntity().getEducationOrgUnit().getDevelopPeriod().getTitle());

            viewWrapper.setViewProperty(Model.FORMATIVE_ORG_UNIT_TITLE, orgUnit.getFormativeOrgUnit().getTitle());
            viewWrapper.setViewProperty(Model.TERRITORIAL_ORG_UNIT_TITLE, StringUtils.join(territorialOrgUnitTitles, ", "));
            viewWrapper.setViewProperty(Model.EDUCATION_ORG_UNIT_TITLE, orgUnit.getEducationLevelHighSchool().getTitle());
            viewWrapper.setViewProperty(Model.DEVELOP_FORM_TITLE, orgUnit.getDevelopForm().getTitle());
            viewWrapper.setViewProperty(Model.DEVELOP_CONDITION_TITLE, StringUtils.join(developConditionTitles, ", "));
            viewWrapper.setViewProperty(Model.DEVELOP_TECH_TITLE, StringUtils.join(developTechTitles, ", "));
            viewWrapper.setViewProperty(Model.DEVELOP_PERIOD_TITLE, StringUtils.join(developPeriodTitles, ", "));
            viewWrapper.setViewProperty(Model.COURSE_TITLE, extract.getCourse().getTitle());
            viewWrapper.setViewProperty(Model.EXTRACT_COUNT, extractList.size());
        }
    }

    @Override
    public void doSendToCoordination(Model model, IPrincipalContext initiator)
    {
        EcOrderManager.instance().dao().doSendToCoordination(model.getOrder(), initiator);
    }

    @Override
    public void doSendToFormative(Model model)
    {
        EcOrderManager.instance().dao().doSendToFormative(model.getOrder());
    }

    @Override
    public void doReject(Model model)
    {
        EcOrderManager.instance().dao().doReject(model.getOrder());
    }

    @Override
    public void doCommit(Model model)
    {
        EcOrderManager.instance().dao().doCommit(model.getOrder());
    }

    @Override
    public void doRollback(Model model)
    {
        EcOrderManager.instance().dao().doRollback(model.getOrder());
    }

    @Override
    public void deleteParagraph(Model model, Long paragraphId)
    {
        EcOrderManager.instance().dao().deleteParagraph(getNotNull(EnrollmentParagraph.class, paragraphId));
    }

    @Override
    public void updatePriority(Long paragraphId, int direction)
    {
        EcOrderManager.instance().dao().updatePriority(paragraphId, direction);
    }
}
