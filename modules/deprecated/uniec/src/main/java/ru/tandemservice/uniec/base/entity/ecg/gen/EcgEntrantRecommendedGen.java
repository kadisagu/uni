package ru.tandemservice.uniec.base.entity.ecg.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistribution;
import ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommended;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Рекомендованный абитуриент основного распределения
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EcgEntrantRecommendedGen extends EntityBase
 implements INaturalIdentifiable<EcgEntrantRecommendedGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommended";
    public static final String ENTITY_NAME = "ecgEntrantRecommended";
    public static final int VERSION_HASH = -992100420;
    private static IEntityMeta ENTITY_META;

    public static final String L_DISTRIBUTION = "distribution";
    public static final String L_DIRECTION = "direction";
    public static final String L_TARGET_ADMISSION_KIND = "targetAdmissionKind";

    private EcgDistribution _distribution;     // Основное распределение абитуриентов
    private RequestedEnrollmentDirection _direction;     // Выбранное направление приема
    private TargetAdmissionKind _targetAdmissionKind;     // Вид целевого приема

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Основное распределение абитуриентов. Свойство не может быть null.
     */
    @NotNull
    public EcgDistribution getDistribution()
    {
        return _distribution;
    }

    /**
     * @param distribution Основное распределение абитуриентов. Свойство не может быть null.
     */
    public void setDistribution(EcgDistribution distribution)
    {
        dirty(_distribution, distribution);
        _distribution = distribution;
    }

    /**
     * @return Выбранное направление приема. Свойство не может быть null.
     */
    @NotNull
    public RequestedEnrollmentDirection getDirection()
    {
        return _direction;
    }

    /**
     * @param direction Выбранное направление приема. Свойство не может быть null.
     */
    public void setDirection(RequestedEnrollmentDirection direction)
    {
        dirty(_direction, direction);
        _direction = direction;
    }

    /**
     * @return Вид целевого приема.
     */
    public TargetAdmissionKind getTargetAdmissionKind()
    {
        return _targetAdmissionKind;
    }

    /**
     * @param targetAdmissionKind Вид целевого приема.
     */
    public void setTargetAdmissionKind(TargetAdmissionKind targetAdmissionKind)
    {
        dirty(_targetAdmissionKind, targetAdmissionKind);
        _targetAdmissionKind = targetAdmissionKind;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EcgEntrantRecommendedGen)
        {
            if (withNaturalIdProperties)
            {
                setDistribution(((EcgEntrantRecommended)another).getDistribution());
                setDirection(((EcgEntrantRecommended)another).getDirection());
            }
            setTargetAdmissionKind(((EcgEntrantRecommended)another).getTargetAdmissionKind());
        }
    }

    public INaturalId<EcgEntrantRecommendedGen> getNaturalId()
    {
        return new NaturalId(getDistribution(), getDirection());
    }

    public static class NaturalId extends NaturalIdBase<EcgEntrantRecommendedGen>
    {
        private static final String PROXY_NAME = "EcgEntrantRecommendedNaturalProxy";

        private Long _distribution;
        private Long _direction;

        public NaturalId()
        {}

        public NaturalId(EcgDistribution distribution, RequestedEnrollmentDirection direction)
        {
            _distribution = ((IEntity) distribution).getId();
            _direction = ((IEntity) direction).getId();
        }

        public Long getDistribution()
        {
            return _distribution;
        }

        public void setDistribution(Long distribution)
        {
            _distribution = distribution;
        }

        public Long getDirection()
        {
            return _direction;
        }

        public void setDirection(Long direction)
        {
            _direction = direction;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EcgEntrantRecommendedGen.NaturalId) ) return false;

            EcgEntrantRecommendedGen.NaturalId that = (NaturalId) o;

            if( !equals(getDistribution(), that.getDistribution()) ) return false;
            if( !equals(getDirection(), that.getDirection()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getDistribution());
            result = hashCode(result, getDirection());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getDistribution());
            sb.append("/");
            sb.append(getDirection());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EcgEntrantRecommendedGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EcgEntrantRecommended.class;
        }

        public T newInstance()
        {
            return (T) new EcgEntrantRecommended();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "distribution":
                    return obj.getDistribution();
                case "direction":
                    return obj.getDirection();
                case "targetAdmissionKind":
                    return obj.getTargetAdmissionKind();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "distribution":
                    obj.setDistribution((EcgDistribution) value);
                    return;
                case "direction":
                    obj.setDirection((RequestedEnrollmentDirection) value);
                    return;
                case "targetAdmissionKind":
                    obj.setTargetAdmissionKind((TargetAdmissionKind) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "distribution":
                        return true;
                case "direction":
                        return true;
                case "targetAdmissionKind":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "distribution":
                    return true;
                case "direction":
                    return true;
                case "targetAdmissionKind":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "distribution":
                    return EcgDistribution.class;
                case "direction":
                    return RequestedEnrollmentDirection.class;
                case "targetAdmissionKind":
                    return TargetAdmissionKind.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EcgEntrantRecommended> _dslPath = new Path<EcgEntrantRecommended>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EcgEntrantRecommended");
    }
            

    /**
     * @return Основное распределение абитуриентов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommended#getDistribution()
     */
    public static EcgDistribution.Path<EcgDistribution> distribution()
    {
        return _dslPath.distribution();
    }

    /**
     * @return Выбранное направление приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommended#getDirection()
     */
    public static RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> direction()
    {
        return _dslPath.direction();
    }

    /**
     * @return Вид целевого приема.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommended#getTargetAdmissionKind()
     */
    public static TargetAdmissionKind.Path<TargetAdmissionKind> targetAdmissionKind()
    {
        return _dslPath.targetAdmissionKind();
    }

    public static class Path<E extends EcgEntrantRecommended> extends EntityPath<E>
    {
        private EcgDistribution.Path<EcgDistribution> _distribution;
        private RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> _direction;
        private TargetAdmissionKind.Path<TargetAdmissionKind> _targetAdmissionKind;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Основное распределение абитуриентов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommended#getDistribution()
     */
        public EcgDistribution.Path<EcgDistribution> distribution()
        {
            if(_distribution == null )
                _distribution = new EcgDistribution.Path<EcgDistribution>(L_DISTRIBUTION, this);
            return _distribution;
        }

    /**
     * @return Выбранное направление приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommended#getDirection()
     */
        public RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> direction()
        {
            if(_direction == null )
                _direction = new RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection>(L_DIRECTION, this);
            return _direction;
        }

    /**
     * @return Вид целевого приема.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommended#getTargetAdmissionKind()
     */
        public TargetAdmissionKind.Path<TargetAdmissionKind> targetAdmissionKind()
        {
            if(_targetAdmissionKind == null )
                _targetAdmissionKind = new TargetAdmissionKind.Path<TargetAdmissionKind>(L_TARGET_ADMISSION_KIND, this);
            return _targetAdmissionKind;
        }

        public Class getEntityClass()
        {
            return EcgEntrantRecommended.class;
        }

        public String getEntityName()
        {
            return "ecgEntrantRecommended";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
