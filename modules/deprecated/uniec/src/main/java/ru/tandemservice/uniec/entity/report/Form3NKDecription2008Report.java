package ru.tandemservice.uniec.entity.report;

import ru.tandemservice.uni.IStorableReport;
import ru.tandemservice.uniec.entity.report.gen.Form3NKDecription2008ReportGen;

/**
 * Форма 3НК (расшифровка, по форме 2008 года)
 */
public class Form3NKDecription2008Report extends Form3NKDecription2008ReportGen implements IStorableReport
{
}