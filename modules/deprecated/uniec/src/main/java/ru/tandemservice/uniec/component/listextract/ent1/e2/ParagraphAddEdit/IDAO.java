/* $Id$ */
package ru.tandemservice.uniec.component.listextract.ent1.e2.ParagraphAddEdit;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.IAbstractListParagraphAddEditDAO;
import ru.tandemservice.uniec.entity.orders.SplitContractBachelorEntrantsStuListExtract;

/**
 * @author Dmitry Seleznev
 * @since 21.08.2012
 */
public interface IDAO extends IAbstractListParagraphAddEditDAO<SplitContractBachelorEntrantsStuListExtract, Model>
{
}