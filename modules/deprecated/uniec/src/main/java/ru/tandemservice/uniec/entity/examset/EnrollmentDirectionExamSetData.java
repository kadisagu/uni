/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.entity.examset;

import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.settings.SetDiscipline;

import java.util.*;

/**
 * Информация по наборам экзаменов для направления приема по всем категориям, а так же по их структурам
 *
 * @author Vasily Zhukov
 * @since 04.08.2010
 */
public class EnrollmentDirectionExamSetData
{
    private EnrollmentDirection _enrollmentDirection;

    private Map<StudentCategory, List<ExamSetItem>> _categoryExamSetMap;

    private Map<StudentCategory, ExamSetStructure> _categoryExamSetStructureMap;

    private Boolean _structuresEqual;

    public EnrollmentDirectionExamSetData(EnrollmentDirection enrollmentDirection, Map<StudentCategory, List<ExamSetItem>> categoryExamSetMap, Map<StudentCategory, ExamSetStructure> categoryExamSetStructureMap)
    {
        // проверяем входящие данные
        if (enrollmentDirection == null)
            throw new RuntimeException("EnrollmentDirection cannot be null!");

        if (!categoryExamSetMap.keySet().equals(categoryExamSetStructureMap.keySet()))
            throw new RuntimeException("CategoryExamSetMap and categoryExamSetStructureMap must have same keySet!");

        for (StudentCategory studentCategory : categoryExamSetMap.keySet())
            if (studentCategory == null)
                throw new RuntimeException("StudentCategory cannot be null!");

        for (List<ExamSetItem> list : categoryExamSetMap.values())
            if (list == null || list.contains(null))
                throw new RuntimeException("List<ExamSetItem> cannot be null!");

        for (ExamSetStructure examSetStructure : categoryExamSetStructureMap.values())
            if (examSetStructure == null)
                throw new RuntimeException("ExamSetStructure cannot be null!");

        if (!enrollmentDirection.getEnrollmentCampaign().isExamSetDiff())
        {
            if (categoryExamSetMap.size() > 1)
                throw new RuntimeException("CategoryExamSetMap must have zero or one key!");

            if (categoryExamSetMap.size() == 1 && !categoryExamSetMap.keySet().iterator().next().getCode().equals(UniDefines.STUDENT_CATEGORY_STUDENT))
                throw new RuntimeException("CategoryExamSetMap have one key and must have 'Student' as a key!");
        }

        // сохраняем в unmodifiable структурах

        _enrollmentDirection = enrollmentDirection;
        _categoryExamSetMap = Collections.unmodifiableMap(new HashMap<>(categoryExamSetMap));
        _categoryExamSetStructureMap = Collections.unmodifiableMap(new HashMap<>(categoryExamSetStructureMap));
    }

    public boolean isStructuresEqual()
    {
        if (_structuresEqual == null)
        {
            _structuresEqual = _categoryExamSetStructureMap.isEmpty() || new HashSet<>(_categoryExamSetStructureMap.values()).size() == 1;
        }
        return _structuresEqual;
    }

    public List<ExamSetItem> getExamSetItemList()
    {
        if (!isStructuresEqual())
            throw new RuntimeException("You cannot get ExamSetItemList: structures are not equals ");
        Iterator<List<ExamSetItem>> iter = _categoryExamSetMap.values().iterator();
        return iter.hasNext() ? iter.next() : null;
    }

    public List<String> getExamSetItemKindTitleList()
    {
        if (!isStructuresEqual())
            throw new RuntimeException("You cannot get ExamSetItemKindTitleList: structures are not equals ");

        if (_categoryExamSetStructureMap.isEmpty()) return null;

        List<String> result = new ArrayList<>();

        ExamSetStructure structure = _categoryExamSetStructureMap.values().iterator().next();

        for (ExamSetStructureItem item : structure.getItemList())
            result.add(item.getKind().getShortTitle());

        return result;
    }

    public List<String[]> getExamSetItemTitleList()
    {
        if (!isStructuresEqual())
            throw new RuntimeException("You cannot get ExamSetItemTitleList: structures are not equals ");

        int resultLength = 0;
        List<String[]> result = null;

        for (Map.Entry<StudentCategory, List<ExamSetItem>> entry : _categoryExamSetMap.entrySet())
        {
            List<String[]> titles = new ArrayList<>();

            for (ExamSetItem item : entry.getValue())
            {
                List<String> titleList = new ArrayList<>();
                titleList.add(item.getSubject().getShortTitle());
                for (SetDiscipline choice : item.getChoice()) titleList.add(choice.getShortTitle());
                Collections.sort(titleList);
                titles.add(titleList.toArray(new String[titleList.size()]));
            }

            if (result == null)
            {
                result = titles;
            } else
            {
                int size = 0;
                for (String[] titleList : titles)
                    for (String title : titleList)
                        size = title.length();
                if (size < resultLength)
                {
                    resultLength = size;
                    result = titles;
                }
            }
        }
        return result;
    }

    public ExamSetStructure getExamSetStructure()
    {
        if (!isStructuresEqual())
            throw new RuntimeException("You cannot get ExamSetStructure: structures are not equals ");
        Iterator<ExamSetStructure> iter = _categoryExamSetStructureMap.values().iterator();
        return iter.hasNext() ? iter.next() : null;
    }

    public EnrollmentDirection getEnrollmentDirection()
    {
        return _enrollmentDirection;
    }

    public List<ExamSetItem> getCategoryExamSet(StudentCategory studentCategory)
    {
        if (_categoryExamSetMap.isEmpty()) return null;

        if (_enrollmentDirection.getEnrollmentCampaign().isExamSetDiff())
            return _categoryExamSetMap.get(studentCategory);

        return _categoryExamSetMap.values().iterator().next();
    }

    public ExamSetStructure getCategoryExamSetStructure(StudentCategory studentCategory)
    {
        if (_categoryExamSetStructureMap.isEmpty()) return null;

        if (_enrollmentDirection.getEnrollmentCampaign().isExamSetDiff())
            return _categoryExamSetStructureMap.get(studentCategory);

        return _categoryExamSetStructureMap.values().iterator().next();
    }

    // use getCategoryExamSet
    @Deprecated
    public Map<StudentCategory, List<ExamSetItem>> getCategoryExamSetMap()
    {
        return _categoryExamSetMap;
    }

    // use getCategoryExamSetStructure
    @Deprecated
    public Map<StudentCategory, ExamSetStructure> getCategoryExamSetStructureMap()
    {
        return _categoryExamSetStructureMap;
    }
}
