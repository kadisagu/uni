package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.base.entity.ecg.IPersistentEcgItem;
import ru.tandemservice.uniec.entity.entrant.CompetitionGroup;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Направление подготовки (специальность) приема
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrollmentDirectionGen extends EntityBase
 implements IPersistentEcgItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.EnrollmentDirection";
    public static final String ENTITY_NAME = "enrollmentDirection";
    public static final int VERSION_HASH = 1264970478;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_EDUCATION_ORG_UNIT = "educationOrgUnit";
    public static final String L_COMPETITION_GROUP = "competitionGroup";
    public static final String P_MINISTERIAL_PLAN = "ministerialPlan";
    public static final String P_SPEC_RIGHTS_QUOTA = "specRightsQuota";
    public static final String P_CONTRACT_PLAN = "contractPlan";
    public static final String P_TARGET_ADMISSION_PLAN_BUDGET = "targetAdmissionPlanBudget";
    public static final String P_TARGET_ADMISSION_PLAN_CONTRACT = "targetAdmissionPlanContract";
    public static final String P_LISTENER_PLAN = "listenerPlan";
    public static final String P_SECOND_HIGH_EDUCATION_PLAN = "secondHighEducationPlan";
    public static final String P_CRIMEA_QUOTA = "crimeaQuota";
    public static final String P_INTERVIEW = "interview";
    public static final String P_BUDGET = "budget";
    public static final String P_CONTRACT = "contract";
    public static final String P_CRIMEA = "crimea";
    public static final String P_TITLE = "title";
    public static final String P_HAS_INCREASED_PASS_MARKS = "hasIncreasedPassMarks";
    public static final String P_PRINT_TITLE = "printTitle";
    public static final String P_SHORT_TITLE = "shortTitle";

    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private EducationOrgUnit _educationOrgUnit;     // Параметры обучения студентов по направлению подготовки (НПП)
    private CompetitionGroup _competitionGroup;     // Конкурсная группа
    private Integer _ministerialPlan;     // План приема на бюджет
    private Integer _specRightsQuota;     // Квота приема лиц, имеющих особое право
    private Integer _contractPlan;     // План приема по договору
    private Integer _targetAdmissionPlanBudget;     // План приема по целевому приему по бюджету
    private Integer _targetAdmissionPlanContract;     // План приема по целевому приему по договору
    private Integer _listenerPlan;     // План приема слушателей
    private Integer _secondHighEducationPlan;     // План приема по второму высшему
    private Integer _crimeaQuota;     // Квота приема граждан Крыма
    private boolean _interview;     // Собеседование
    private boolean _budget;     // Есть ли прием на бюджет
    private boolean _contract;     // Есть ли прием на контракт
    private boolean _crimea = false;     // Есть ли прием граждан Крыма
    private String _title;     // Название
    private boolean _hasIncreasedPassMarks = false;     // Входит в перечень направлений с повышенным зачетным баллом

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     */
    @NotNull
    public EducationOrgUnit getEducationOrgUnit()
    {
        return _educationOrgUnit;
    }

    /**
     * @param educationOrgUnit Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     */
    public void setEducationOrgUnit(EducationOrgUnit educationOrgUnit)
    {
        dirty(_educationOrgUnit, educationOrgUnit);
        _educationOrgUnit = educationOrgUnit;
    }

    /**
     * @return Конкурсная группа.
     */
    public CompetitionGroup getCompetitionGroup()
    {
        return _competitionGroup;
    }

    /**
     * @param competitionGroup Конкурсная группа.
     */
    public void setCompetitionGroup(CompetitionGroup competitionGroup)
    {
        dirty(_competitionGroup, competitionGroup);
        _competitionGroup = competitionGroup;
    }

    /**
     * @return План приема на бюджет.
     */
    public Integer getMinisterialPlan()
    {
        return _ministerialPlan;
    }

    /**
     * @param ministerialPlan План приема на бюджет.
     */
    public void setMinisterialPlan(Integer ministerialPlan)
    {
        dirty(_ministerialPlan, ministerialPlan);
        _ministerialPlan = ministerialPlan;
    }

    /**
     * @return Квота приема лиц, имеющих особое право.
     */
    public Integer getSpecRightsQuota()
    {
        return _specRightsQuota;
    }

    /**
     * @param specRightsQuota Квота приема лиц, имеющих особое право.
     */
    public void setSpecRightsQuota(Integer specRightsQuota)
    {
        dirty(_specRightsQuota, specRightsQuota);
        _specRightsQuota = specRightsQuota;
    }

    /**
     * @return План приема по договору.
     */
    public Integer getContractPlan()
    {
        return _contractPlan;
    }

    /**
     * @param contractPlan План приема по договору.
     */
    public void setContractPlan(Integer contractPlan)
    {
        dirty(_contractPlan, contractPlan);
        _contractPlan = contractPlan;
    }

    /**
     * @return План приема по целевому приему по бюджету.
     */
    public Integer getTargetAdmissionPlanBudget()
    {
        return _targetAdmissionPlanBudget;
    }

    /**
     * @param targetAdmissionPlanBudget План приема по целевому приему по бюджету.
     */
    public void setTargetAdmissionPlanBudget(Integer targetAdmissionPlanBudget)
    {
        dirty(_targetAdmissionPlanBudget, targetAdmissionPlanBudget);
        _targetAdmissionPlanBudget = targetAdmissionPlanBudget;
    }

    /**
     * @return План приема по целевому приему по договору.
     */
    public Integer getTargetAdmissionPlanContract()
    {
        return _targetAdmissionPlanContract;
    }

    /**
     * @param targetAdmissionPlanContract План приема по целевому приему по договору.
     */
    public void setTargetAdmissionPlanContract(Integer targetAdmissionPlanContract)
    {
        dirty(_targetAdmissionPlanContract, targetAdmissionPlanContract);
        _targetAdmissionPlanContract = targetAdmissionPlanContract;
    }

    /**
     * @return План приема слушателей.
     */
    public Integer getListenerPlan()
    {
        return _listenerPlan;
    }

    /**
     * @param listenerPlan План приема слушателей.
     */
    public void setListenerPlan(Integer listenerPlan)
    {
        dirty(_listenerPlan, listenerPlan);
        _listenerPlan = listenerPlan;
    }

    /**
     * @return План приема по второму высшему.
     */
    public Integer getSecondHighEducationPlan()
    {
        return _secondHighEducationPlan;
    }

    /**
     * @param secondHighEducationPlan План приема по второму высшему.
     */
    public void setSecondHighEducationPlan(Integer secondHighEducationPlan)
    {
        dirty(_secondHighEducationPlan, secondHighEducationPlan);
        _secondHighEducationPlan = secondHighEducationPlan;
    }

    /**
     * @return Квота приема граждан Крыма.
     */
    public Integer getCrimeaQuota()
    {
        return _crimeaQuota;
    }

    /**
     * @param crimeaQuota Квота приема граждан Крыма.
     */
    public void setCrimeaQuota(Integer crimeaQuota)
    {
        dirty(_crimeaQuota, crimeaQuota);
        _crimeaQuota = crimeaQuota;
    }

    /**
     * @return Собеседование. Свойство не может быть null.
     */
    @NotNull
    public boolean isInterview()
    {
        return _interview;
    }

    /**
     * @param interview Собеседование. Свойство не может быть null.
     */
    public void setInterview(boolean interview)
    {
        dirty(_interview, interview);
        _interview = interview;
    }

    /**
     * @return Есть ли прием на бюджет. Свойство не может быть null.
     */
    @NotNull
    public boolean isBudget()
    {
        return _budget;
    }

    /**
     * @param budget Есть ли прием на бюджет. Свойство не может быть null.
     */
    public void setBudget(boolean budget)
    {
        dirty(_budget, budget);
        _budget = budget;
    }

    /**
     * @return Есть ли прием на контракт. Свойство не может быть null.
     */
    @NotNull
    public boolean isContract()
    {
        return _contract;
    }

    /**
     * @param contract Есть ли прием на контракт. Свойство не может быть null.
     */
    public void setContract(boolean contract)
    {
        dirty(_contract, contract);
        _contract = contract;
    }

    /**
     * @return Есть ли прием граждан Крыма. Свойство не может быть null.
     */
    @NotNull
    public boolean isCrimea()
    {
        return _crimea;
    }

    /**
     * @param crimea Есть ли прием граждан Крыма. Свойство не может быть null.
     */
    public void setCrimea(boolean crimea)
    {
        dirty(_crimea, crimea);
        _crimea = crimea;
    }

    /**
     * @return Название. Свойство не может быть null.
     *
     * Это формула "educationOrgUnit.educationLevelHighSchool.displayableTitle".
     */
    // @NotNull
    // @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Входит в перечень направлений с повышенным зачетным баллом. Свойство не может быть null.
     */
    @NotNull
    public boolean isHasIncreasedPassMarks()
    {
        return _hasIncreasedPassMarks;
    }

    /**
     * @param hasIncreasedPassMarks Входит в перечень направлений с повышенным зачетным баллом. Свойство не может быть null.
     */
    public void setHasIncreasedPassMarks(boolean hasIncreasedPassMarks)
    {
        dirty(_hasIncreasedPassMarks, hasIncreasedPassMarks);
        _hasIncreasedPassMarks = hasIncreasedPassMarks;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrollmentDirectionGen)
        {
            setEnrollmentCampaign(((EnrollmentDirection)another).getEnrollmentCampaign());
            setEducationOrgUnit(((EnrollmentDirection)another).getEducationOrgUnit());
            setCompetitionGroup(((EnrollmentDirection)another).getCompetitionGroup());
            setMinisterialPlan(((EnrollmentDirection)another).getMinisterialPlan());
            setSpecRightsQuota(((EnrollmentDirection)another).getSpecRightsQuota());
            setContractPlan(((EnrollmentDirection)another).getContractPlan());
            setTargetAdmissionPlanBudget(((EnrollmentDirection)another).getTargetAdmissionPlanBudget());
            setTargetAdmissionPlanContract(((EnrollmentDirection)another).getTargetAdmissionPlanContract());
            setListenerPlan(((EnrollmentDirection)another).getListenerPlan());
            setSecondHighEducationPlan(((EnrollmentDirection)another).getSecondHighEducationPlan());
            setCrimeaQuota(((EnrollmentDirection)another).getCrimeaQuota());
            setInterview(((EnrollmentDirection)another).isInterview());
            setBudget(((EnrollmentDirection)another).isBudget());
            setContract(((EnrollmentDirection)another).isContract());
            setCrimea(((EnrollmentDirection)another).isCrimea());
            setTitle(((EnrollmentDirection)another).getTitle());
            setHasIncreasedPassMarks(((EnrollmentDirection)another).isHasIncreasedPassMarks());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrollmentDirectionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrollmentDirection.class;
        }

        public T newInstance()
        {
            return (T) new EnrollmentDirection();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "educationOrgUnit":
                    return obj.getEducationOrgUnit();
                case "competitionGroup":
                    return obj.getCompetitionGroup();
                case "ministerialPlan":
                    return obj.getMinisterialPlan();
                case "specRightsQuota":
                    return obj.getSpecRightsQuota();
                case "contractPlan":
                    return obj.getContractPlan();
                case "targetAdmissionPlanBudget":
                    return obj.getTargetAdmissionPlanBudget();
                case "targetAdmissionPlanContract":
                    return obj.getTargetAdmissionPlanContract();
                case "listenerPlan":
                    return obj.getListenerPlan();
                case "secondHighEducationPlan":
                    return obj.getSecondHighEducationPlan();
                case "crimeaQuota":
                    return obj.getCrimeaQuota();
                case "interview":
                    return obj.isInterview();
                case "budget":
                    return obj.isBudget();
                case "contract":
                    return obj.isContract();
                case "crimea":
                    return obj.isCrimea();
                case "title":
                    return obj.getTitle();
                case "hasIncreasedPassMarks":
                    return obj.isHasIncreasedPassMarks();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "educationOrgUnit":
                    obj.setEducationOrgUnit((EducationOrgUnit) value);
                    return;
                case "competitionGroup":
                    obj.setCompetitionGroup((CompetitionGroup) value);
                    return;
                case "ministerialPlan":
                    obj.setMinisterialPlan((Integer) value);
                    return;
                case "specRightsQuota":
                    obj.setSpecRightsQuota((Integer) value);
                    return;
                case "contractPlan":
                    obj.setContractPlan((Integer) value);
                    return;
                case "targetAdmissionPlanBudget":
                    obj.setTargetAdmissionPlanBudget((Integer) value);
                    return;
                case "targetAdmissionPlanContract":
                    obj.setTargetAdmissionPlanContract((Integer) value);
                    return;
                case "listenerPlan":
                    obj.setListenerPlan((Integer) value);
                    return;
                case "secondHighEducationPlan":
                    obj.setSecondHighEducationPlan((Integer) value);
                    return;
                case "crimeaQuota":
                    obj.setCrimeaQuota((Integer) value);
                    return;
                case "interview":
                    obj.setInterview((Boolean) value);
                    return;
                case "budget":
                    obj.setBudget((Boolean) value);
                    return;
                case "contract":
                    obj.setContract((Boolean) value);
                    return;
                case "crimea":
                    obj.setCrimea((Boolean) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "hasIncreasedPassMarks":
                    obj.setHasIncreasedPassMarks((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "educationOrgUnit":
                        return true;
                case "competitionGroup":
                        return true;
                case "ministerialPlan":
                        return true;
                case "specRightsQuota":
                        return true;
                case "contractPlan":
                        return true;
                case "targetAdmissionPlanBudget":
                        return true;
                case "targetAdmissionPlanContract":
                        return true;
                case "listenerPlan":
                        return true;
                case "secondHighEducationPlan":
                        return true;
                case "crimeaQuota":
                        return true;
                case "interview":
                        return true;
                case "budget":
                        return true;
                case "contract":
                        return true;
                case "crimea":
                        return true;
                case "title":
                        return true;
                case "hasIncreasedPassMarks":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "educationOrgUnit":
                    return true;
                case "competitionGroup":
                    return true;
                case "ministerialPlan":
                    return true;
                case "specRightsQuota":
                    return true;
                case "contractPlan":
                    return true;
                case "targetAdmissionPlanBudget":
                    return true;
                case "targetAdmissionPlanContract":
                    return true;
                case "listenerPlan":
                    return true;
                case "secondHighEducationPlan":
                    return true;
                case "crimeaQuota":
                    return true;
                case "interview":
                    return true;
                case "budget":
                    return true;
                case "contract":
                    return true;
                case "crimea":
                    return true;
                case "title":
                    return true;
                case "hasIncreasedPassMarks":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "educationOrgUnit":
                    return EducationOrgUnit.class;
                case "competitionGroup":
                    return CompetitionGroup.class;
                case "ministerialPlan":
                    return Integer.class;
                case "specRightsQuota":
                    return Integer.class;
                case "contractPlan":
                    return Integer.class;
                case "targetAdmissionPlanBudget":
                    return Integer.class;
                case "targetAdmissionPlanContract":
                    return Integer.class;
                case "listenerPlan":
                    return Integer.class;
                case "secondHighEducationPlan":
                    return Integer.class;
                case "crimeaQuota":
                    return Integer.class;
                case "interview":
                    return Boolean.class;
                case "budget":
                    return Boolean.class;
                case "contract":
                    return Boolean.class;
                case "crimea":
                    return Boolean.class;
                case "title":
                    return String.class;
                case "hasIncreasedPassMarks":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrollmentDirection> _dslPath = new Path<EnrollmentDirection>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrollmentDirection");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#getEducationOrgUnit()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
    {
        return _dslPath.educationOrgUnit();
    }

    /**
     * @return Конкурсная группа.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#getCompetitionGroup()
     */
    public static CompetitionGroup.Path<CompetitionGroup> competitionGroup()
    {
        return _dslPath.competitionGroup();
    }

    /**
     * @return План приема на бюджет.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#getMinisterialPlan()
     */
    public static PropertyPath<Integer> ministerialPlan()
    {
        return _dslPath.ministerialPlan();
    }

    /**
     * @return Квота приема лиц, имеющих особое право.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#getSpecRightsQuota()
     */
    public static PropertyPath<Integer> specRightsQuota()
    {
        return _dslPath.specRightsQuota();
    }

    /**
     * @return План приема по договору.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#getContractPlan()
     */
    public static PropertyPath<Integer> contractPlan()
    {
        return _dslPath.contractPlan();
    }

    /**
     * @return План приема по целевому приему по бюджету.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#getTargetAdmissionPlanBudget()
     */
    public static PropertyPath<Integer> targetAdmissionPlanBudget()
    {
        return _dslPath.targetAdmissionPlanBudget();
    }

    /**
     * @return План приема по целевому приему по договору.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#getTargetAdmissionPlanContract()
     */
    public static PropertyPath<Integer> targetAdmissionPlanContract()
    {
        return _dslPath.targetAdmissionPlanContract();
    }

    /**
     * @return План приема слушателей.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#getListenerPlan()
     */
    public static PropertyPath<Integer> listenerPlan()
    {
        return _dslPath.listenerPlan();
    }

    /**
     * @return План приема по второму высшему.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#getSecondHighEducationPlan()
     */
    public static PropertyPath<Integer> secondHighEducationPlan()
    {
        return _dslPath.secondHighEducationPlan();
    }

    /**
     * @return Квота приема граждан Крыма.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#getCrimeaQuota()
     */
    public static PropertyPath<Integer> crimeaQuota()
    {
        return _dslPath.crimeaQuota();
    }

    /**
     * @return Собеседование. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#isInterview()
     */
    public static PropertyPath<Boolean> interview()
    {
        return _dslPath.interview();
    }

    /**
     * @return Есть ли прием на бюджет. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#isBudget()
     */
    public static PropertyPath<Boolean> budget()
    {
        return _dslPath.budget();
    }

    /**
     * @return Есть ли прием на контракт. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#isContract()
     */
    public static PropertyPath<Boolean> contract()
    {
        return _dslPath.contract();
    }

    /**
     * @return Есть ли прием граждан Крыма. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#isCrimea()
     */
    public static PropertyPath<Boolean> crimea()
    {
        return _dslPath.crimea();
    }

    /**
     * @return Название. Свойство не может быть null.
     *
     * Это формула "educationOrgUnit.educationLevelHighSchool.displayableTitle".
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Входит в перечень направлений с повышенным зачетным баллом. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#isHasIncreasedPassMarks()
     */
    public static PropertyPath<Boolean> hasIncreasedPassMarks()
    {
        return _dslPath.hasIncreasedPassMarks();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#getPrintTitle()
     */
    public static SupportedPropertyPath<String> printTitle()
    {
        return _dslPath.printTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#getShortTitle()
     */
    public static SupportedPropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    public static class Path<E extends EnrollmentDirection> extends EntityPath<E>
    {
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnit;
        private CompetitionGroup.Path<CompetitionGroup> _competitionGroup;
        private PropertyPath<Integer> _ministerialPlan;
        private PropertyPath<Integer> _specRightsQuota;
        private PropertyPath<Integer> _contractPlan;
        private PropertyPath<Integer> _targetAdmissionPlanBudget;
        private PropertyPath<Integer> _targetAdmissionPlanContract;
        private PropertyPath<Integer> _listenerPlan;
        private PropertyPath<Integer> _secondHighEducationPlan;
        private PropertyPath<Integer> _crimeaQuota;
        private PropertyPath<Boolean> _interview;
        private PropertyPath<Boolean> _budget;
        private PropertyPath<Boolean> _contract;
        private PropertyPath<Boolean> _crimea;
        private PropertyPath<String> _title;
        private PropertyPath<Boolean> _hasIncreasedPassMarks;
        private SupportedPropertyPath<String> _printTitle;
        private SupportedPropertyPath<String> _shortTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#getEducationOrgUnit()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
        {
            if(_educationOrgUnit == null )
                _educationOrgUnit = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT, this);
            return _educationOrgUnit;
        }

    /**
     * @return Конкурсная группа.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#getCompetitionGroup()
     */
        public CompetitionGroup.Path<CompetitionGroup> competitionGroup()
        {
            if(_competitionGroup == null )
                _competitionGroup = new CompetitionGroup.Path<CompetitionGroup>(L_COMPETITION_GROUP, this);
            return _competitionGroup;
        }

    /**
     * @return План приема на бюджет.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#getMinisterialPlan()
     */
        public PropertyPath<Integer> ministerialPlan()
        {
            if(_ministerialPlan == null )
                _ministerialPlan = new PropertyPath<Integer>(EnrollmentDirectionGen.P_MINISTERIAL_PLAN, this);
            return _ministerialPlan;
        }

    /**
     * @return Квота приема лиц, имеющих особое право.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#getSpecRightsQuota()
     */
        public PropertyPath<Integer> specRightsQuota()
        {
            if(_specRightsQuota == null )
                _specRightsQuota = new PropertyPath<Integer>(EnrollmentDirectionGen.P_SPEC_RIGHTS_QUOTA, this);
            return _specRightsQuota;
        }

    /**
     * @return План приема по договору.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#getContractPlan()
     */
        public PropertyPath<Integer> contractPlan()
        {
            if(_contractPlan == null )
                _contractPlan = new PropertyPath<Integer>(EnrollmentDirectionGen.P_CONTRACT_PLAN, this);
            return _contractPlan;
        }

    /**
     * @return План приема по целевому приему по бюджету.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#getTargetAdmissionPlanBudget()
     */
        public PropertyPath<Integer> targetAdmissionPlanBudget()
        {
            if(_targetAdmissionPlanBudget == null )
                _targetAdmissionPlanBudget = new PropertyPath<Integer>(EnrollmentDirectionGen.P_TARGET_ADMISSION_PLAN_BUDGET, this);
            return _targetAdmissionPlanBudget;
        }

    /**
     * @return План приема по целевому приему по договору.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#getTargetAdmissionPlanContract()
     */
        public PropertyPath<Integer> targetAdmissionPlanContract()
        {
            if(_targetAdmissionPlanContract == null )
                _targetAdmissionPlanContract = new PropertyPath<Integer>(EnrollmentDirectionGen.P_TARGET_ADMISSION_PLAN_CONTRACT, this);
            return _targetAdmissionPlanContract;
        }

    /**
     * @return План приема слушателей.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#getListenerPlan()
     */
        public PropertyPath<Integer> listenerPlan()
        {
            if(_listenerPlan == null )
                _listenerPlan = new PropertyPath<Integer>(EnrollmentDirectionGen.P_LISTENER_PLAN, this);
            return _listenerPlan;
        }

    /**
     * @return План приема по второму высшему.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#getSecondHighEducationPlan()
     */
        public PropertyPath<Integer> secondHighEducationPlan()
        {
            if(_secondHighEducationPlan == null )
                _secondHighEducationPlan = new PropertyPath<Integer>(EnrollmentDirectionGen.P_SECOND_HIGH_EDUCATION_PLAN, this);
            return _secondHighEducationPlan;
        }

    /**
     * @return Квота приема граждан Крыма.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#getCrimeaQuota()
     */
        public PropertyPath<Integer> crimeaQuota()
        {
            if(_crimeaQuota == null )
                _crimeaQuota = new PropertyPath<Integer>(EnrollmentDirectionGen.P_CRIMEA_QUOTA, this);
            return _crimeaQuota;
        }

    /**
     * @return Собеседование. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#isInterview()
     */
        public PropertyPath<Boolean> interview()
        {
            if(_interview == null )
                _interview = new PropertyPath<Boolean>(EnrollmentDirectionGen.P_INTERVIEW, this);
            return _interview;
        }

    /**
     * @return Есть ли прием на бюджет. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#isBudget()
     */
        public PropertyPath<Boolean> budget()
        {
            if(_budget == null )
                _budget = new PropertyPath<Boolean>(EnrollmentDirectionGen.P_BUDGET, this);
            return _budget;
        }

    /**
     * @return Есть ли прием на контракт. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#isContract()
     */
        public PropertyPath<Boolean> contract()
        {
            if(_contract == null )
                _contract = new PropertyPath<Boolean>(EnrollmentDirectionGen.P_CONTRACT, this);
            return _contract;
        }

    /**
     * @return Есть ли прием граждан Крыма. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#isCrimea()
     */
        public PropertyPath<Boolean> crimea()
        {
            if(_crimea == null )
                _crimea = new PropertyPath<Boolean>(EnrollmentDirectionGen.P_CRIMEA, this);
            return _crimea;
        }

    /**
     * @return Название. Свойство не может быть null.
     *
     * Это формула "educationOrgUnit.educationLevelHighSchool.displayableTitle".
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EnrollmentDirectionGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Входит в перечень направлений с повышенным зачетным баллом. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#isHasIncreasedPassMarks()
     */
        public PropertyPath<Boolean> hasIncreasedPassMarks()
        {
            if(_hasIncreasedPassMarks == null )
                _hasIncreasedPassMarks = new PropertyPath<Boolean>(EnrollmentDirectionGen.P_HAS_INCREASED_PASS_MARKS, this);
            return _hasIncreasedPassMarks;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#getPrintTitle()
     */
        public SupportedPropertyPath<String> printTitle()
        {
            if(_printTitle == null )
                _printTitle = new SupportedPropertyPath<String>(EnrollmentDirectionGen.P_PRINT_TITLE, this);
            return _printTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentDirection#getShortTitle()
     */
        public SupportedPropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new SupportedPropertyPath<String>(EnrollmentDirectionGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

        public Class getEntityClass()
        {
            return EnrollmentDirection.class;
        }

        public String getEntityName()
        {
            return "enrollmentDirection";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getPrintTitle();

    public abstract String getShortTitle();
}
