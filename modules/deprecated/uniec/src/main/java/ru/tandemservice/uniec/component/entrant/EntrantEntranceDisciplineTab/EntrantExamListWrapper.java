/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantEntranceDisciplineTab;

import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uniec.entity.entrant.EntrantExamList;
import ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline;

/**
 * @author vip_delete
 * @since 19.03.2009
 */
public class EntrantExamListWrapper
{
    private EntrantExamList _entrantExamList;
    private DynamicListDataSource<ExamPassDiscipline> _examPassDisciplineDataSource;

    public EntrantExamListWrapper(final EntrantExamList entrantExamList)
    {
        _entrantExamList = entrantExamList;
    }

    public EntrantExamList getEntrantExamList()
    {
        return _entrantExamList;
    }

    public DynamicListDataSource<ExamPassDiscipline> getExamPassDisciplineDataSource()
    {
        return _examPassDisciplineDataSource;
    }

    public void setExamPassDisciplineDataSource(DynamicListDataSource<ExamPassDiscipline> examPassDisciplineDataSource)
    {
        _examPassDisciplineDataSource = examPassDisciplineDataSource;
    }
}
