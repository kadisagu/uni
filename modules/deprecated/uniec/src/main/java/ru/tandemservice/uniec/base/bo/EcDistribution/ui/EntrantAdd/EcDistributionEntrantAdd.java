/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.ui.EntrantAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.uniec.base.bo.EcDistribution.logic.EcSimpleDSHandler;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgEntrantRateRowDTO;

/**
 * @author Vasily Zhukov
 * @since 12.07.2011
 */
@Configuration
public class EcDistributionEntrantAdd extends BusinessComponentManager
{
    public static final String ENTRANT_DS = "entrantDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
        .addDataSource(searchListDS(ENTRANT_DS, entrantDS(), entrantDSHandler()))
        .create();
    }

    @Bean
    public ColumnListExtPoint entrantDS()
    {
        return columnListExtPointBuilder(ENTRANT_DS)
        .addColumn(textColumn("competitionKind", IEcgEntrantRateRowDTO.COMPETITION_KIND_TITLE).create())
        .addColumn(textColumn("targetAdmissionKind", IEcgEntrantRateRowDTO.TARGET_ADMISSION_KIND_TITLE).visible("ui:targetAdmission").create())
        .addColumn(textColumn("finalMark", IEcgEntrantRateRowDTO.FINAL_MARK).formatter(DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS).create())
        .addColumn(textColumn("profileMark", IEcgEntrantRateRowDTO.PROFILE_MARK).formatter(DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS).create())
        .addColumn(textColumn("graduatedProfileEduInstitution", IEcgEntrantRateRowDTO.GRADUATED_PROFILE_EDU_INSTITUTION_TITLE).width("1").create())
        .addColumn(textColumn("certificateAverageMark", IEcgEntrantRateRowDTO.CERTIFICATE_AVERAGE_MARK).width("5").formatter(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).create())
        .addColumn(textColumn("fio", IEcgEntrantRateRowDTO.FIO))
        .addColumn(blockColumn("checkbox", "checkboxBlock").create())
        .addColumn(blockColumn("distributionDirection", "distributionDirectionBlock").visible("ui:distributionPerDirection").width("5").create())
        .addColumn(blockColumn("distributionGroup", "distributionGroupBlock").visible("mvel:!presenter.distributionPerDirection").width("5").create())
        .addColumn(textColumn("priorities", IEcgEntrantRateRowDTO.PRIORITIES).create())
        .addColumn(textColumn("originals", IEcgEntrantRateRowDTO.ORIGINAL_DOCUMENT_HANDED_IN_TITLE).create())
        .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> entrantDSHandler()
    {
        return new EcSimpleDSHandler(getName());
    }
}
