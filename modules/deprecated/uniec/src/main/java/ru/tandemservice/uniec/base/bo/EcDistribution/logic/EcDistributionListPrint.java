/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.IRtfRowSplitInterceptor;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.base.bo.EcDistribution.EcDistributionManager;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgDistribution;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistribution;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.ws.distribution.EnrollmentDistributionEnvironmentNode;
import ru.tandemservice.uniec.ws.distribution.EnrollmentDistributionServiceDao;
import ru.tandemservice.uniec.ws.distribution.IEnrollmentDistributionServiceDao;

import java.util.*;

/**
 * @author Vasily Zhukov
 * @since 27.07.2011
 */
public class EcDistributionListPrint extends CommonDAO implements IEcDistributionListPrint
{
    @Override
    public RtfDocument getPrintFormForDistributionPerDirection(byte[] template, EcgDistribution distribution)
    {
        RtfDocument document = new RtfReader().read(template);

        EnrollmentDirection enrollmentDirection = (EnrollmentDirection) distribution.getConfig().getEcgItem();
        EducationOrgUnit educationOrgUnit = enrollmentDirection.getEducationOrgUnit();
        OrgUnit formativeOrgUnit = educationOrgUnit.getFormativeOrgUnit();
        String formativeOrgUnitTitle = StringUtils.isEmpty(formativeOrgUnit.getNominativeCaseTitle()) ? formativeOrgUnit.getTitle() : formativeOrgUnit.getNominativeCaseTitle();

        // вызываем веб сервис
        final EnrollmentDistributionEnvironmentNode env = IEnrollmentDistributionServiceDao.INSTANCE.get().getEnrollmentDistributionEnvironmentNode(Collections.<IEcgDistribution>singleton(distribution));
        final EnrollmentDistributionEnvironmentNode.DistributionNode.DistributionRow distributionRow = env.distribution.row.get(0);

        Map<String, EnrollmentDistributionEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow> targetAdmissionKindMap = new HashMap<>();
        Map<String, EnrollmentDistributionEnvironmentNode.CompetitionKindNode.CompetitionKindRow> competitionKindMap = new HashMap<>();
        Map<String, EnrollmentDistributionEnvironmentNode.BenefitNode.BenefitRow> benefitMap = new HashMap<>();
        Map<String, EnrollmentDistributionEnvironmentNode.Row> documentStateMap = new HashMap<>();
        Map<String, EnrollmentDistributionEnvironmentNode.Row> enrollmentStateMap = new HashMap<>();
        fillTitleMap(env,
            targetAdmissionKindMap,
            competitionKindMap,
            benefitMap,
            documentStateMap,
            enrollmentStateMap
        );

        // заполняем таблицу
        final List<String[]> tableRow = new ArrayList<>();
        final Set<Integer> unionRowIndexList = new HashSet<>();
        fillTableRowListForDistributionPerDirection(distributionRow, tableRow, unionRowIndexList,
            targetAdmissionKindMap,
            competitionKindMap,
            benefitMap,
            documentStateMap,
            enrollmentStateMap
        );

        RtfInjectModifier im = new RtfInjectModifier();
        im.put("title", formativeOrgUnitTitle);
        im.put("educationLevelTitle", enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitle());
        im.put("developForm", educationOrgUnit.getDevelopForm().getTitle().toLowerCase() + " форма");
        im.put("developCondition", educationOrgUnit.getDevelopCondition().getTitle().toLowerCase());
        im.put("developPeriod", educationOrgUnit.getDevelopPeriod().getTitle().toLowerCase());
        im.put("compensationType", distribution.getConfig().getCompensationType().getShortTitle().toLowerCase());

        EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow quotaRow = distributionRow.quota.row.get(0);
        StringBuilder quotaTitle = new StringBuilder();
        quotaTitle.append("Количество мест ").append(distribution.getConfig().getCompensationType().isBudget() ? "на бюджет: " : "по договору: ").append(quotaRow.countBegin);

        // считаем план по цп
        int taQuotaCountBegin = 0;
        for (EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow.TaQuotaNode.TaQuotaRow row : quotaRow.taQuota.row)
            taQuotaCountBegin += row.countBegin;

        // если есть суммарный план по цп, то нужно его печатать
        if (taQuotaCountBegin > 0)
        {
            quotaTitle.append(", Целевой прием: ");
            if (quotaRow.defaultTaKind)
            {
                // если нет разделения по видам цп, то пишем просто план
                quotaTitle.append(taQuotaCountBegin);
            } else
            {
                // если есть разделение по видам цп, то пишем план для каждого вида цп
                List<String> list = new ArrayList<>();
                for (EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow.TaQuotaNode.TaQuotaRow row : quotaRow.taQuota.row)
                    list.add(targetAdmissionKindMap.get(row.id).shortTitle + " – " + row.countBegin);
                quotaTitle.append(StringUtils.join(list, ", "));
            }
        }

        im.put("quota", quotaTitle.toString());
        im.modify(document);
        fillTableData(document, document.getHeader().getColorTable().addColor(217, 217, 217), 4, tableRow, unionRowIndexList, distributionRow);

        return document;
    }

    @Override
    public RtfDocument getPrintFormForDistributionPerCompetitionGroup(byte[] template, IEcgDistribution distribution)
    {
        RtfDocument templateDocument = new RtfReader().read(template);

        RtfDocument result = RtfBean.getElementFactory().createRtfDocument();
        result.setHeader(templateDocument.getHeader());
        result.setSettings(templateDocument.getSettings());
        final int grayColorIndex = result.getHeader().getColorTable().addColor(217, 217, 217);

        // вызываем веб сервис
        final EnrollmentDistributionEnvironmentNode env = IEnrollmentDistributionServiceDao.INSTANCE.get().getEnrollmentDistributionEnvironmentNode(Collections.singleton(distribution));
        final EnrollmentDistributionEnvironmentNode.DistributionNode.DistributionRow distributionRow = env.distribution.row.get(0);

        Map<String, EnrollmentDistributionEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow> targetAdmissionKindMap = new HashMap<>();
        Map<String, EnrollmentDistributionEnvironmentNode.CompetitionKindNode.CompetitionKindRow> competitionKindMap = new HashMap<>();
        Map<String, EnrollmentDistributionEnvironmentNode.BenefitNode.BenefitRow> benefitMap = new HashMap<>();
        Map<String, EnrollmentDistributionEnvironmentNode.Row> documentStateMap = new HashMap<>();
        Map<String, EnrollmentDistributionEnvironmentNode.Row> enrollmentStateMap = new HashMap<>();
        fillTitleMap(env,
            targetAdmissionKindMap,
            competitionKindMap,
            benefitMap,
            documentStateMap,
            enrollmentStateMap
        );

        Map<String, EnrollmentDirection> directionMap = new HashMap<>();
        for (EnrollmentDirection direction : EcDistributionManager.instance().dao().getDistributionDirectionList(distribution.getDistribution()))
            directionMap.put(Long.toString(direction.getId()), direction);

        String currentWave = Integer.toString(distributionRow.wave);
        String currentDate = DateFormatter.DEFAULT_DATE_FORMATTER.format(null != distribution.getApprovalDate() ? distribution.getApprovalDate() : new Date());
        String compensationType = distribution.getConfig().getCompensationType().isBudget() ? "на места, финансируемые из федерального бюджета" : "на места с оплатой стоимости обучения";
        String developForm = directionMap.get(distributionRow.quota.row.get(0).id).getEducationOrgUnit().getDevelopForm().getGenCaseTitle();

        RtfInjectModifier im = new RtfInjectModifier();
        im.put("wave", currentWave);
        im.put("date", currentDate);
        im.put("title", distributionRow.ecgItemTitle);
        im.put("compensationType", compensationType);
        im.put("developForm", developForm);

        // заполняем блок с НП из КГ
        List<String> listA = new ArrayList<>(); // специализации
        List<String> listB = new ArrayList<>(); // остальные
        for (EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow quotaRow : distributionRow.quota.row)
        {
            EnrollmentDirection direction = directionMap.get(quotaRow.id);

            (direction.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isSpecialityPrintTitleMode() ? listA : listB).add(
                direction.getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitle() + " (" + direction.getEducationOrgUnit().getEducationLevelHighSchool().getShortTitle() + ")"
            );
        }
        String listAtitle = listA.isEmpty() ? null : (listA.size() == 1 ? "по специальности: " : "по специальностям: ") + StringUtils.join(listA, ", ");
        String listBtitle = listB.isEmpty() ? null : (listB.size() == 1 ? "по направлению: " : "по направлениям: ") + StringUtils.join(listB, ", ");
        RtfString directionTitle = new RtfString();
        if (listAtitle == null)
            directionTitle.append(listBtitle);
        else if (listBtitle == null)
            directionTitle.append(listAtitle);
        else
            directionTitle.append(listBtitle).par().append(listAtitle);
        im.put("directionTitle", directionTitle);

        // заполняем блок с планами приема
        Map<String, String> id2planTitle = new HashMap<>();  // НП -> строка с планами
        int totalCountBegin = 0;
        int totalTaQuotaCountBegin = 0;
        Map<String, Integer> totalTaKindId2plan = new HashMap<>();
        for (EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow quotaRow : distributionRow.quota.row)
        {
            StringBuilder quotaTitle = new StringBuilder();
            quotaTitle.append("Количество мест ").append(distribution.getConfig().getCompensationType().isBudget() ? "на бюджет: " : "по договору: ").append(quotaRow.countBegin);

            totalCountBegin += quotaRow.countBegin;

            // считаем план по цп
            int taQuotaCountBegin = 0;
            for (EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow.TaQuotaNode.TaQuotaRow row : quotaRow.taQuota.row)
                taQuotaCountBegin += row.countBegin;

            totalTaQuotaCountBegin += taQuotaCountBegin;

            // если есть суммарный план по цп, то нужно его печатать
            if (taQuotaCountBegin > 0)
            {
                quotaTitle.append(", Целевой прием: ");
                if (quotaRow.defaultTaKind)
                {
                    // если нет разделения по видам цп, то пишем просто план
                    quotaTitle.append(taQuotaCountBegin);
                }
                else
                {
                    // если есть разделение по видам цп, то пишем план для каждого вида цп
                    List<String> list = new ArrayList<>();
                    for (EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow.TaQuotaNode.TaQuotaRow row : quotaRow.taQuota.row)
                    {
                        list.add(targetAdmissionKindMap.get(row.id).shortTitle + " – " + row.countBegin);

                        Integer plan = totalTaKindId2plan.get(row.id);
                        totalTaKindId2plan.put(row.id, (plan == null ? 0 : plan.intValue()) + row.countBegin);
                    }
                    quotaTitle.append(StringUtils.join(list, ", "));
                }
            }
            id2planTitle.put(quotaRow.id, quotaTitle.toString());
        }
        StringBuilder quotaTitle = new StringBuilder();
        quotaTitle.append("Количество мест ").append(distribution.getConfig().getCompensationType().isBudget() ? "на бюджет: " : "по договору: ").append(totalCountBegin);
        // если есть суммарный план по цп, то нужно его печатать
        if (totalTaQuotaCountBegin > 0)
        {
            quotaTitle.append(", Целевой прием: ");
            if (totalTaKindId2plan.isEmpty())
            {
                // если нет разделения по видам цп, то пишем просто план
                quotaTitle.append(totalTaQuotaCountBegin);
            }
            else
            {
                // если есть разделение по видам цп, то пишем план для каждого вида цп (в каждой строки одни и те же виды цп)
                List<String> list = new ArrayList<>();
                for (EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow.TaQuotaNode.TaQuotaRow row : distributionRow.quota.row.get(0).taQuota.row)
                {
                    Integer plan = totalTaKindId2plan.get(row.id);
                    list.add(targetAdmissionKindMap.get(row.id).shortTitle + " – " + (plan == null ? 0 : plan));
                }
                quotaTitle.append(StringUtils.join(list, ", "));
            }
        }
        im.put("planTitle", quotaTitle.toString());

        RtfDocument clone = templateDocument.getClone();
        im.modify(clone);

        // заполняем таблицу
        final List<String[]> tableRow = new ArrayList<>();
        final Set<Integer> unionRowIndexList = new HashSet<>();
        fillTableRowListForDistributionPerCompetitionGroup(distributionRow, null, tableRow, unionRowIndexList,
            targetAdmissionKindMap,
            competitionKindMap,
            benefitMap,
            documentStateMap,
            enrollmentStateMap,
            directionMap
        );
        fillTableData(clone, grayColorIndex, 3, tableRow, unionRowIndexList, distributionRow);

        result.getElementList().addAll(clone.getElementList());

        // добавляем блоки детализации по каждому НП
        for (EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow quotaRow : distributionRow.quota.row)
        {
            im = new RtfInjectModifier();
            im.put("wave", currentWave);
            im.put("date", currentDate);
            im.put("title", distributionRow.ecgItemTitle);
            im.put("compensationType", compensationType);
            im.put("developForm", developForm);

            EnrollmentDirection direction = directionMap.get(quotaRow.id);
            EducationLevelsHighSchool hs = direction.getEducationOrgUnit().getEducationLevelHighSchool();
            im.put("directionTitle", (hs.getEducationLevel().getLevelType().isSpecialityPrintTitleMode() ? "по специальности: " : "по направлению: ") + hs.getPrintTitle() + " (" + hs.getShortTitle() + ")");
            im.put("planTitle", id2planTitle.get(quotaRow.id));

            clone = templateDocument.getClone();
            im.modify(clone);

            // заполняем таблицу
            tableRow.clear();
            unionRowIndexList.clear();
            fillTableRowListForDistributionPerCompetitionGroup(distributionRow, quotaRow.id, tableRow, unionRowIndexList,
                targetAdmissionKindMap,
                competitionKindMap,
                benefitMap,
                documentStateMap,
                enrollmentStateMap,
                directionMap
            );
            fillTableData(clone, grayColorIndex, 3, tableRow, unionRowIndexList, distributionRow);

            result.getElementList().addAll(clone.getElementList());
        }

        return result;
    }

    private void fillTitleMap(EnrollmentDistributionEnvironmentNode env,
                              Map<String, EnrollmentDistributionEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow> targetAdmissionKindMap,
                              Map<String, EnrollmentDistributionEnvironmentNode.CompetitionKindNode.CompetitionKindRow> competitionKindMap,
                              Map<String, EnrollmentDistributionEnvironmentNode.BenefitNode.BenefitRow> benefitMap,
                              Map<String, EnrollmentDistributionEnvironmentNode.Row> documentStateMap,
                              Map<String, EnrollmentDistributionEnvironmentNode.Row> enrollmentStateMap)
    {
        for (EnrollmentDistributionEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow row : env.targetAdmissionKind.row)
            targetAdmissionKindMap.put(row.id, row);

        for (EnrollmentDistributionEnvironmentNode.CompetitionKindNode.CompetitionKindRow row : env.competitionKind.row)
            competitionKindMap.put(row.id, row);

        for (EnrollmentDistributionEnvironmentNode.BenefitNode.BenefitRow row : env.benefit.row)
            benefitMap.put(row.id, row);

        for (EnrollmentDistributionEnvironmentNode.Row row : env.documentState.row)
            documentStateMap.put(row.id, row);

        for (EnrollmentDistributionEnvironmentNode.Row row : env.enrollmentState.row)
            enrollmentStateMap.put(row.id, row);
    }

    protected void fillTableRowListForDistributionPerDirection(EnrollmentDistributionEnvironmentNode.DistributionNode.DistributionRow distributionRow,
                                                               List<String[]> tableRowList,
                                                               Set<Integer> unionRowIndexList,
                                                               Map<String, EnrollmentDistributionEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow> targetAdmissionKindMap,
                                                               Map<String, EnrollmentDistributionEnvironmentNode.CompetitionKindNode.CompetitionKindRow> competitionKindMap,
                                                               Map<String, EnrollmentDistributionEnvironmentNode.BenefitNode.BenefitRow> benefitMap,
                                                               Map<String, EnrollmentDistributionEnvironmentNode.Row> documentStateMap,
                                                               Map<String, EnrollmentDistributionEnvironmentNode.Row> enrollmentStateMap)
    {

        //boolean defaultTaKind = distributionRow.quota.row.get(0).defaultTaKind;

        int currentRowKey = -1; // unknown

        int rowIndex = 0;

        int number = 1;

        for (EnrollmentDistributionEnvironmentNode.EntrantNode.EntrantRow row : distributionRow.entrant.row)
        {
            // вычисляем ключ строки:
            // для цп - 0
            // не для цп - вид конкурса (если нет вида конкурса, то вид конкурса = на общих основаниях
            int rowKey = row.targetAdmissionKind == null ? Integer.parseInt(row.competitionKind == null ? UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION : row.competitionKind) : 0;

            if (currentRowKey != rowKey)
            {
                // встретился новый тип строки
                currentRowKey = rowKey;

                // помечаем строку для объединения
                unionRowIndexList.add(rowIndex);

                // вычисляем содержимое строки
                String title = row.targetAdmissionKind == null ?
                    competitionKindMap.get(row.competitionKind == null ? UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION : row.competitionKind).title :
                        "Целевой прием";
//                        (defaultTaKind ? "Целевой прием" : "Целевой прием – " + targetAdmissionKindMap.get(row.targetAdmissionKind).shortTitle);

                    tableRowList.add(new String[]{null, title});

                    rowIndex++;

                    number = 1;
            }

            List<String> rowTitleList = new ArrayList<>();

            rowTitleList.add(Integer.toString(number++)); // № п/п
            rowTitleList.add(row.regNumber);            // Рег. №
            rowTitleList.add(row.fio);                  // Фио
            rowTitleList.add(row.finalMark);            // Сумма баллов

            // оценки по дисциплинам
            if (distributionRow.discipline.row.size() == 0)
            {
                rowTitleList.add(null);
            } else
            {
                if (row.marks == null)
                {
                    // абитуриент не актуальный
                    for (int i = 0; i < distributionRow.discipline.row.size(); i++)
                        rowTitleList.add(null);
                } else
                {
                    for (String mark : row.marks)
                        rowTitleList.add(mark.equals("-") ? null : mark);
                }
            }

            // сданы оригиналы
            rowTitleList.add(row.documentState == null ? null : documentStateMap.get(row.documentState).title);

            // вид конкурса
            if (row.targetAdmissionKind != null)
            {
                rowTitleList.add(targetAdmissionKindMap.get(row.targetAdmissionKind).shortTitle);
            } else
            {
                if (row.competitionKind == null)
                {
                    rowTitleList.add(null);
                } else
                {
                    if (row.competitionKind.equals(UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION) && row.benefitList != null && !row.benefitList.isEmpty())
                    {
                        List<String> list = new ArrayList<>();
                        for (String benefit : row.benefitList)
                            list.add(benefitMap.get(benefit).shortTitle);
                        rowTitleList.add(competitionKindMap.get(row.competitionKind).shortTitle + ", " + StringUtils.join(list, ", "));
                    } else
                    {
                        rowTitleList.add(competitionKindMap.get(row.competitionKind).shortTitle);
                    }
                }
            }

            // примечание
            rowTitleList.add(row.enrollmentState == null ?
                null :
                    enrollmentStateMap.get(row.enrollmentState).title.toLowerCase() + (row.orderNumber == null ? "" : ", приказ №" + row.orderNumber)
            );

            tableRowList.add(rowTitleList.toArray(new String[rowTitleList.size()]));

            rowIndex++;
        }
    }

    protected void fillTableRowListForDistributionPerCompetitionGroup(EnrollmentDistributionEnvironmentNode.DistributionNode.DistributionRow distributionRow,
                                                                      String directionId,
                                                                      List<String[]> tableRowList,
                                                                      Set<Integer> unionRowIndexList,
                                                                      Map<String, EnrollmentDistributionEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow> targetAdmissionKindMap,
                                                                      Map<String, EnrollmentDistributionEnvironmentNode.CompetitionKindNode.CompetitionKindRow> competitionKindMap,
                                                                      Map<String, EnrollmentDistributionEnvironmentNode.BenefitNode.BenefitRow> benefitMap,
                                                                      Map<String, EnrollmentDistributionEnvironmentNode.Row> documentStateMap,
                                                                      Map<String, EnrollmentDistributionEnvironmentNode.Row> enrollmentStateMap,
                                                                      Map<String, EnrollmentDirection> directionMap)
    {

        //boolean defaultTaKind = distributionRow.quota.row.get(0).defaultTaKind;

        int currentRowKey = -1; // unknown

        int rowIndex = 0;

        int number = 1;

        for (EnrollmentDistributionEnvironmentNode.EntrantNode.EntrantRow row : distributionRow.entrant.row)
        {
            if (directionId != null && (row.enrollmentState == null || !directionId.equals(row.enrollmentDirection)))
                continue;

            // вычисляем ключ строки:
            // для цп - 0
            // не для цп - вид конкурса (если нет вида конкурса, то вид конкурса = на общих основаниях
            int rowKey = row.targetAdmissionKind == null ? Integer.parseInt(row.competitionKind == null ? UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION : row.competitionKind) : 0;

            if (currentRowKey != rowKey)
            {
                // встретился новый тип строки
                currentRowKey = rowKey;

                // помечаем строку для объединения
                unionRowIndexList.add(rowIndex);

                // вычисляем содержимое строки
                String title = row.targetAdmissionKind == null ?
                    competitionKindMap.get(row.competitionKind == null ? UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION : row.competitionKind).title :
                        "Целевой прием";
//                        (defaultTaKind ? "Целевой прием" : "Целевой прием – " + targetAdmissionKindMap.get(row.targetAdmissionKind).shortTitle);

                    tableRowList.add(new String[]{null, title});

                    rowIndex++;

                    number = 1;
            }

            List<String> rowTitleList = new ArrayList<>();

            rowTitleList.add(Integer.toString(number++)); // № п/п
            rowTitleList.add(row.fio);                    // Фио
            rowTitleList.add(row.finalMark);              // Сумма баллов

            // оценки по дисциплинам
            if (distributionRow.discipline.row.size() == 0)
            {
                rowTitleList.add(null);
            } else
            {
                if (row.marks == null)
                {
                    // абитуриент не актуальный
                    for (int i = 0; i < distributionRow.discipline.row.size(); i++)
                        rowTitleList.add(null);
                } else
                {
                    for (String mark : row.marks)
                        rowTitleList.add(mark.equals("-") ? null : mark);
                }
            }

            // сданы документы
            rowTitleList.add(row.documentState == null ? null : documentStateMap.get(row.documentState).title);

            // вид конкурса
            if (row.targetAdmissionKind != null)
            {
                rowTitleList.add(targetAdmissionKindMap.get(row.targetAdmissionKind).shortTitle);
            } else
            {
                if (row.competitionKind == null)
                {
                    rowTitleList.add(null);
                } else
                {
                    if (row.competitionKind.equals(UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION) && row.benefitList != null && !row.benefitList.isEmpty())
                    {
                        List<String> list = new ArrayList<>();
                        for (String benefit : row.benefitList)
                            list.add(benefitMap.get(benefit).shortTitle);
                        rowTitleList.add(competitionKindMap.get(row.competitionKind).shortTitle + ", " + StringUtils.join(list, ", "));
                    } else
                    {
                        rowTitleList.add(competitionKindMap.get(row.competitionKind).shortTitle);
                    }
                }
            }

            // приоритеты
            if (null != row.priorityIds) {
                List<String> priorityList = new ArrayList<>(row.priorityIds.size());
                for (Long id : row.priorityIds)
                    priorityList.add(directionMap.get(id.toString()).getEducationOrgUnit().getEducationLevelHighSchool().getShortTitle());
                rowTitleList.add(StringUtils.join(priorityList, ", "));
            } else {
                rowTitleList.add("");
            }

            // *
            rowTitleList.add(row.enrollmentState == null ? null : directionMap.get(row.enrollmentDirection).getEducationOrgUnit().getEducationLevelHighSchool().getShortTitle());

            // примечание
            rowTitleList.add(row.enrollmentState == null ?
                null :
                    enrollmentStateMap.get(row.enrollmentState).title.toLowerCase() + (row.enrollmentState.equals(EnrollmentDistributionServiceDao.ENROLLED) && row.preEnrollId != null ? " (" + directionMap.get(row.preEnrollId).getEducationOrgUnit().getEducationLevelHighSchool().getShortTitle() + ")" : "") + (row.orderNumber == null ? "" : ", приказ №" + row.orderNumber)
            );

            tableRowList.add(rowTitleList.toArray(new String[rowTitleList.size()]));

            rowIndex++;
        }
    }

    protected void fillTableData(RtfDocument document, final int grayColorIndex, final int disciplineColumn, List<String[]> tableData, final Set<Integer> unionRowIndexList, final EnrollmentDistributionEnvironmentNode.DistributionNode.DistributionRow distributionRow)
    {
        RtfTableModifier tm = new RtfTableModifier();

        tm.put("T", tableData.toArray(new String[tableData.size()][]));
        tm.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public void beforeModify(RtfTable table, int currentRowIndex)
            {
                // если ВИ нет, то и разбивать ячейки не надо
                if (distributionRow.discipline.row.isEmpty())
                    return;

                // берем предыдущую строку (заголовок таблицы)
                RtfRow headerRow = table.getRowList().get(currentRowIndex - 1);

                // берем текущую строку (заголовок таблицы)
                RtfRow currentRow = table.getRowList().get(currentRowIndex);

                final int[] parts = new int[distributionRow.discipline.row.size()];
                Arrays.fill(parts, 1);

                // разбиваем 4-ую ячейку по количеству дисциплин в равных пропорциях, записывая сокр. названия дисциплин
                RtfUtil.splitRow(headerRow, disciplineColumn, (newCell, index) -> newCell.setElementList(new RtfString().boldBegin().append(distributionRow.discipline.row.get(index).shortTitle).boldEnd().toList()), parts);

                RtfUtil.splitRow(currentRow, disciplineColumn, null, parts);
            }

            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                // в тех строках, где будет объединение ячеек нужно выделять болдом
                if (unionRowIndexList.contains(rowIndex))
                    return new RtfString().boldBegin().append(value).boldEnd().toList();
                return null;
            }

            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                // объединяем все строки с нужными индексами, расставляя серый фон
                for (Integer rowIndex : unionRowIndexList)
                {
                    RtfRow row = newRowList.get(rowIndex + startIndex);
                    RtfUtil.unitAllCell(row, 1);
                    row.getCellList().get(0).setBackgroundColorIndex(grayColorIndex);
                }
            }
        });

        tm.modify(document);
    }
}
