/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantList;

import java.util.List;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.base.bo.UniStudent.vo.AddressCountryVO;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;

/**
 * @author agolubenko
 * @since 15.05.2008
 */
public class Model implements MultiEnrollmentDirectionUtil.Model, IEnrollmentCampaignSelectModel
{
    public static final String REQUEST_NUMBER_COLUMN = "requestNumber";
    public static final String ORIGINAL_DOCUMENT_HANDLE_IN_COLUMN = "originalDocumentHandleIn";
    public static final String P_ENTRANT_ACTIVE_CUSTOME_STATES = "entrantActiveCustomStates";
    public static final String DEF_ENTRANT_MASTER_PERM_KEY = "addEntrantMaster";

    public static final Long SHOW_ARCHIVAL_CODE = 0L;
    public static final Long SHOW_NON_ARCHIVAL_CODE = 1L;

    private IDataSettings _settings;
    private List<EnrollmentCampaign> _enrollmentCampaignList;
    private ISelectModel _compensationTypeModel;
    private ISelectModel _studentCategoryModel;
    private ISelectModel _qualificationModel;

    private MultiEnrollmentDirectionUtil.Parameters _parameters;
    private ISelectModel _formativeOrgUnitListModel;
    private ISelectModel _territorialOrgUnitListModel;
    private ISelectModel _educationLevelHighSchoolListModel;
    private ISelectModel _developFormListModel;
    private ISelectModel _developConditionListModel;
    private ISelectModel _developTechListModel;
    private ISelectModel _developPeriodListModel;

    private ISelectModel _entrantStateModel;
    private ISelectModel _archivalModel;
    private IMultiSelectModel _entrantCustomStateCIModel;
    private ISelectModel citizenShipModel;
    private DynamicListDataSource<Entrant> _dataSource;

    private AddressCountryVO _citizenship;

    private ISelectModel _specialCondition4ExamModel;
    private DataWrapper specialCondition4Exam;
    private ISelectModel _base4ExamByDifferentSourcesModel;
    private ISelectModel _targetAdmissionModel = TwinComboDataSourceHandler.getYesNoDefaultSelectModel();
    private IMultiSelectModel _targetAdmissionKindModel;
    private IMultiSelectModel _benefitModel;
    private IMultiSelectModel _competitionKindModel;

    // IEnrollmentCampaignSelectModel

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return (EnrollmentCampaign) _settings.get(ENROLLMENT_CAMPAIGN_FILTER_NAME);
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _settings.set(ENROLLMENT_CAMPAIGN_FILTER_NAME, enrollmentCampaign);
    }

    // MultiEnrollmentDirectionUtil.Model

    @Override
    public List<EnrollmentCampaign> getSelectedEnrollmentCampaignList()
    {
        return MultiEnrollmentDirectionUtil.getSingletonList(getEnrollmentCampaign());
    }

    @Override
    public List<OrgUnit> getSelectedFormativeOrgUnitList()
    {
        @SuppressWarnings("unchecked")
        List<OrgUnit> list = (List<OrgUnit>)_settings.get("formativeOrgUnitList");
        return list == null || list.isEmpty() ? null : list;
    }

    @Override
    public List<OrgUnit> getSelectedTerritorialOrgUnitList()
    {
        @SuppressWarnings("unchecked")
        List<OrgUnit> list =  (List<OrgUnit>) _settings.get("territorialOrgUnitList");
        return list == null || list.isEmpty() ? null : list;
    }

    @Override
    public List<EducationLevelsHighSchool> getSelectedEducationLevelHighSchoolList()
    {
        @SuppressWarnings("unchecked")
        List<EducationLevelsHighSchool> list = (List<EducationLevelsHighSchool>) _settings.get("educationLevelHighSchoolList");
        return list == null || list.isEmpty() ? null : list;
    }

    @Override
    public List<DevelopForm> getSelectedDevelopFormList()
    {
        @SuppressWarnings("unchecked")
        List<DevelopForm> list = (List<DevelopForm>) _settings.get("developFormList");
        return list == null || list.isEmpty() ? null : list;
    }

    @Override
    public List<DevelopCondition> getSelectedDevelopConditionList()
    {
        @SuppressWarnings("unchecked")
        List<DevelopCondition> list = (List<DevelopCondition>) _settings.get("developConditionList");
        return list == null || list.isEmpty() ? null : list;
    }

    @Override
    public List<DevelopTech> getSelectedDevelopTechList()
    {
        @SuppressWarnings("unchecked")
        List<DevelopTech> list = (List<DevelopTech>) _settings.get("developTechList");
        return list == null || list.isEmpty() ? null : list;
    }

    @Override
    public List<DevelopPeriod> getSelectedDevelopPeriodList()
    {
        @SuppressWarnings("unchecked")
        List<DevelopPeriod> list = (List<DevelopPeriod>) _settings.get("developPeriodList");
        return list == null || list.isEmpty() ? null : list;
    }

    // Getters & Setters

    @Override
    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public ISelectModel getCompensationTypeModel()
    {
        return _compensationTypeModel;
    }

    public void setCompensationTypeModel(ISelectModel compensationTypeModel)
    {
        _compensationTypeModel = compensationTypeModel;
    }

    public ISelectModel getStudentCategoryModel()
    {
        return _studentCategoryModel;
    }

    public void setStudentCategoryModel(ISelectModel studentCategoryModel)
    {
        _studentCategoryModel = studentCategoryModel;
    }

    public ISelectModel getQualificationModel()
    {
        return _qualificationModel;
    }

    public void setQualificationModel(ISelectModel qualificationModel)
    {
        _qualificationModel = qualificationModel;
    }

    @Override
    public MultiEnrollmentDirectionUtil.Parameters getParameters()
    {
        return _parameters;
    }

    public void setParameters(MultiEnrollmentDirectionUtil.Parameters parameters)
    {
        _parameters = parameters;
    }

    public ISelectModel getFormativeOrgUnitListModel()
    {
        return _formativeOrgUnitListModel;
    }

    public void setFormativeOrgUnitListModel(ISelectModel formativeOrgUnitListModel)
    {
        _formativeOrgUnitListModel = formativeOrgUnitListModel;
    }

    public ISelectModel getTerritorialOrgUnitListModel()
    {
        return _territorialOrgUnitListModel;
    }

    public void setTerritorialOrgUnitListModel(ISelectModel territorialOrgUnitListModel)
    {
        _territorialOrgUnitListModel = territorialOrgUnitListModel;
    }

    public ISelectModel getEducationLevelHighSchoolListModel()
    {
        return _educationLevelHighSchoolListModel;
    }

    public void setEducationLevelHighSchoolListModel(ISelectModel educationLevelHighSchoolListModel)
    {
        _educationLevelHighSchoolListModel = educationLevelHighSchoolListModel;
    }

    public ISelectModel getDevelopFormListModel()
    {
        return _developFormListModel;
    }

    public void setDevelopFormListModel(ISelectModel developFormListModel)
    {
        _developFormListModel = developFormListModel;
    }

    public ISelectModel getDevelopConditionListModel()
    {
        return _developConditionListModel;
    }

    public void setDevelopConditionListModel(ISelectModel developConditionListModel)
    {
        _developConditionListModel = developConditionListModel;
    }

    public ISelectModel getDevelopTechListModel()
    {
        return _developTechListModel;
    }

    public void setDevelopTechListModel(ISelectModel developTechListModel)
    {
        _developTechListModel = developTechListModel;
    }

    public ISelectModel getDevelopPeriodListModel()
    {
        return _developPeriodListModel;
    }

    public void setDevelopPeriodListModel(ISelectModel developPeriodListModel)
    {
        _developPeriodListModel = developPeriodListModel;
    }

    public ISelectModel getEntrantStateModel()
    {
        return _entrantStateModel;
    }

    public void setEntrantStateModel(ISelectModel entrantStateModel)
    {
        _entrantStateModel = entrantStateModel;
    }

    public ISelectModel getArchivalModel()
    {
        return _archivalModel;
    }

    public void setArchivalModel(ISelectModel archivalModel)
    {
        _archivalModel = archivalModel;
    }

    public IMultiSelectModel getEntrantCustomStateCIModel()
    {
        return _entrantCustomStateCIModel;
    }

    public void setEntrantCustomStateCIModel(IMultiSelectModel entrantCustomStateCIModel)
    {
        _entrantCustomStateCIModel = entrantCustomStateCIModel;
    }

    public ISelectModel getCitizenShipModel()
    {
        return citizenShipModel;
    }

    public void setCitizenShipModel(ISelectModel citizenShipModel)
    {
        this.citizenShipModel = citizenShipModel;
    }

    public AddressCountryVO getCitizenship()
    {
        return _citizenship;
    }

    public void setCitizenship(AddressCountryVO citizenship)
    {
        _citizenship = citizenship;
    }

    public DynamicListDataSource<Entrant> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<Entrant> dataSource)
    {
        _dataSource = dataSource;
    }

    public ISelectModel getBase4ExamByDifferentSourcesModel()
    {
        return _base4ExamByDifferentSourcesModel;
    }

    public void setBase4ExamByDifferentSourcesModel(ISelectModel base4ExamByDifferentSourcesModel)
    {
        _base4ExamByDifferentSourcesModel = base4ExamByDifferentSourcesModel;
    }

    public ISelectModel getSpecialCondition4ExamModel()
    {
        return _specialCondition4ExamModel;
    }

    public void setSpecialCondition4ExamModel(ISelectModel specialCondition4ExamModel)
    {
        _specialCondition4ExamModel = specialCondition4ExamModel;
    }

    public DataWrapper getSpecialCondition4Exam()
    {
        return specialCondition4Exam;
    }

    public void setSpecialCondition4Exam(DataWrapper specialCondition4Exam)
    {
        this.specialCondition4Exam = specialCondition4Exam;
    }

    public ISelectModel getTargetAdmissionModel()
    {
        return _targetAdmissionModel;
    }

    public void setTargetAdmissionModel(ISelectModel specialPurposeRecruitListModel)
    {
        _targetAdmissionModel = specialPurposeRecruitListModel;
    }

    public boolean isTargetAdmissionDisabled()
    {
        Boolean targetAdmission = TwinComboDataSourceHandler.getSelectedValue(_settings.get(DAO.TARGET_ADMISSION_PARAM));
        return targetAdmission == null || !targetAdmission;
    }

    public IMultiSelectModel getTargetAdmissionKindModel()
    {
        return _targetAdmissionKindModel;
    }

    public void setTargetAdmissionKindModel(IMultiSelectModel targetAdmissionKindModel)
    {
        this._targetAdmissionKindModel = targetAdmissionKindModel;
    }

    public IMultiSelectModel getBenefitModel()
    {
        return _benefitModel;
    }

    public void setBenefitModel(IMultiSelectModel benefitModel)
    {
        this._benefitModel = benefitModel;
    }

    public IMultiSelectModel getCompetitionKindModel()
    {
        return _competitionKindModel;
    }

    public void setCompetitionKindModel(IMultiSelectModel competitionKindModel)
    {
        _competitionKindModel = competitionKindModel;
    }
}
