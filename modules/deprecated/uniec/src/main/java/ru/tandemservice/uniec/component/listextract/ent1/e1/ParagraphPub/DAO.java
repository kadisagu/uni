/* $Id$ */
package ru.tandemservice.uniec.component.listextract.ent1.e1.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.AbstractListParagraphPubDAO;
import ru.tandemservice.uniec.entity.orders.SplitBudgetBachelorEntrantsStuListExtract;

/**
 * @author Dmitry Seleznev
 * @since 13.07.2012
 */
public class DAO extends AbstractListParagraphPubDAO<SplitBudgetBachelorEntrantsStuListExtract, Model> implements IDAO
{
}