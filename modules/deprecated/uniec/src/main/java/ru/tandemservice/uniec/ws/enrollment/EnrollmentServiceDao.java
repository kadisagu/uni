package ru.tandemservice.uniec.ws.enrollment;

import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineKind;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline;
import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline2SetDisciplineRelation;
import ru.tandemservice.uniec.entity.settings.*;

import java.util.*;

/**
 * @author Vasily Zhukov
 * @since 15.02.2011
 */
public class EnrollmentServiceDao extends UniBaseDao implements IEnrollmentServiceDao
{
    @Override
    public EnrollmentEnvironmentNode getEnrollmentEnvironmentData(String enrollmentCampaignTitle)
    {
        EnrollmentCampaign enrollmentCampaign = get(EnrollmentCampaign.class, EnrollmentCampaign.P_TITLE, enrollmentCampaignTitle);

        if (enrollmentCampaign == null)
            throw new RuntimeException("EnrollmentCampaign with title '" + enrollmentCampaignTitle + "' not found!");

        EnrollmentEnvironmentNode envNode = new EnrollmentEnvironmentNode();

        envNode.enrollmentCampaignTitle = enrollmentCampaign.getTitle();
        envNode.educationYearTitle = enrollmentCampaign.getEducationYear().getTitle();

        // сохраняем нужные справочники

        for (CompensationType row : getList(CompensationType.class, CompensationType.P_CODE))
            envNode.compensationType.row.add(new EnrollmentEnvironmentNode.Row(row.getShortTitle(), row.getTitle(), row.getCode()));

        for (SubjectPassForm row : getList(SubjectPassForm.class, SubjectPassForm.P_CODE))
            envNode.subjectPassForm.row.add(new EnrollmentEnvironmentNode.Row(row.getShortTitle(), row.getTitle(), row.getCode()));

        for (StudentCategory row : getList(StudentCategory.class, StudentCategory.P_CODE))
            envNode.studentCategory.row.add(new EnrollmentEnvironmentNode.Row(row.getTitle(), row.getCode()));

        for (EntranceDisciplineKind row : getList(EntranceDisciplineKind.class, EntranceDisciplineKind.P_CODE))
            envNode.entranceDisciplineKind.row.add(new EnrollmentEnvironmentNode.Row(row.getShortTitle(), row.getTitle(), row.getCode()));

        for (DevelopForm row : getList(DevelopForm.class, DevelopForm.P_CODE))
            envNode.developForm.row.add(new EnrollmentEnvironmentNode.Row(row.getShortTitle(), row.getTitle(), row.getCode()));

        for (DevelopCondition row : getList(DevelopCondition.class, DevelopCondition.P_CODE))
            envNode.developCondition.row.add(new EnrollmentEnvironmentNode.Row(row.getShortTitle(), row.getTitle(), row.getCode()));

        for (DevelopTech row : getList(DevelopTech.class, DevelopTech.P_CODE))
            envNode.developTech.row.add(new EnrollmentEnvironmentNode.Row(row.getShortTitle(), row.getTitle(), row.getCode()));

        for (DevelopPeriod row : getList(DevelopPeriod.class, DevelopPeriod.P_CODE))
            envNode.developPeriod.row.add(new EnrollmentEnvironmentNode.Row(row.getTitle(), row.getCode()));

        for (Qualifications row : getList(Qualifications.class, Qualifications.P_CODE))
            envNode.qualification.row.add(new EnrollmentEnvironmentNode.Row(row.getShortTitle(), row.getTitle(), row.getCode()));

        for (StructureEducationLevels row : getList(StructureEducationLevels.class, StructureEducationLevels.P_CODE))
            envNode.structureEducationLevel.row.add(new EnrollmentEnvironmentNode.StructureEducationLevelNode.StructureEducationLevelRow(row.getTitle(), row.getCode(), row.getParent() == null ? null : row.getParent().getCode()));

        List<TargetAdmissionKind> targetAdmissionKindList = new DQLSelectBuilder().fromEntity(TargetAdmissionKind.class, "t").column("t")
        .where(DQLExpressions.notIn(DQLExpressions.property(TargetAdmissionKind.id().fromAlias("t")), new DQLSelectBuilder().fromEntity(NotUsedTargetAdmissionKind.class, "n")
            .column(DQLExpressions.property(NotUsedTargetAdmissionKind.targetAdmissionKind().id().fromAlias("n")))
            .where(DQLExpressions.eq(DQLExpressions.property(NotUsedTargetAdmissionKind.enrollmentCampaign().fromAlias("n")), DQLExpressions.value(enrollmentCampaign)))
            .buildQuery()
        ))
        .order(DQLExpressions.property(TargetAdmissionKind.code().fromAlias("t")))
        .createStatement(new DQLExecutionContext(getSession())).list();

        for (TargetAdmissionKind row : targetAdmissionKindList)
            envNode.targetAdmissionKind.row.add(new EnrollmentEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow(row.getShortTitle(), row.getTitle(), row.getCode(), row.getParent() == null ? null : row.getParent().getCode(), row.getPriority()));

        // сохраняем все способы сдачи

        List<Discipline2RealizationWayRelation> discipline2RealizationWayRelationList = new DQLSelectBuilder().fromEntity(Discipline2RealizationWayRelation.class, "d")
        .column("d")
        .where(DQLExpressions.eq(DQLExpressions.property(Discipline2RealizationWayRelation.enrollmentCampaign().fromAlias("d")), DQLExpressions.value(enrollmentCampaign)))
        .order(DQLExpressions.property(Discipline2RealizationWayRelation.id().fromAlias("d")))
        .createStatement(new DQLExecutionContext(getSession())).list();

        for (Discipline2RealizationWayRelation row : discipline2RealizationWayRelationList)
            envNode.disciplinePassWay.row.add(new EnrollmentEnvironmentNode.Row(row.getTitle(), row.getId().toString()));

        // сохраняем все формы сдачи Discipline2RealizationForRelation

        List<Discipline2RealizationFormRelation> discipline2RealizationFormRelationList = new DQLSelectBuilder().fromEntity(Discipline2RealizationFormRelation.class, "d")
        .column("d")
        .where(DQLExpressions.eq(DQLExpressions.property(Discipline2RealizationFormRelation.discipline().enrollmentCampaign().fromAlias("d")), DQLExpressions.value(enrollmentCampaign)))
        .order(DQLExpressions.property(Discipline2RealizationFormRelation.id().fromAlias("d")))
        .createStatement(new DQLExecutionContext(getSession())).list();

        for (Discipline2RealizationFormRelation discipline : discipline2RealizationFormRelationList)
            envNode.disciplinePassForm.row.add(new EnrollmentEnvironmentNode.DisciplinePassFormNode.FormRow(discipline.getDiscipline().getId().toString(), discipline.getSubjectPassForm().getCode(), discipline.getType().getCode()));

        // сохраняем все группы дисциплин

        List<Group2DisciplineRelation> disciplinesGroupRelationList = new DQLSelectBuilder().fromEntity(Group2DisciplineRelation.class, "r").column("r")
        .where(DQLExpressions.eq(DQLExpressions.property(Group2DisciplineRelation.discipline().enrollmentCampaign().fromAlias("r")), DQLExpressions.value(enrollmentCampaign)))
        .order(DQLExpressions.property(DisciplinesGroup.id().fromAlias("r")))
        .createStatement(new DQLExecutionContext(getSession())).list();

        Map<DisciplinesGroup, List<String>> group2disciplines = new HashMap<>();
        for (Group2DisciplineRelation row : disciplinesGroupRelationList)
        {
            List<String> list = group2disciplines.get(row.getGroup());
            if (list == null)
                group2disciplines.put(row.getGroup(), list = new ArrayList<>());
            list.add(row.getDiscipline().getId().toString());
        }

        for (Map.Entry<DisciplinesGroup, List<String>> entry : group2disciplines.entrySet())
        {
            DisciplinesGroup row = entry.getKey();
            envNode.disciplinePassWayGroup.row.add(new EnrollmentEnvironmentNode.DisciplinePassWayGroupNode.GroupRow(row.getShortTitle(), row.getTitle(), row.getId().toString(), entry.getValue()));
        }

        Collections.sort(envNode.disciplinePassWayGroup.row);

        // собираем информацию по всем вступительным испытаниям из указанной приемной кампании

        List<EntranceDiscipline> entranceDisciplineList = new DQLSelectBuilder().fromEntity(EntranceDiscipline.class, "d").column("d")
        .where(DQLExpressions.eq(DQLExpressions.property(EntranceDiscipline.enrollmentDirection().enrollmentCampaign().fromAlias("d")), DQLExpressions.value(enrollmentCampaign)))
        .order(DQLExpressions.property(EntranceDiscipline.id().fromAlias("d")))
        .createStatement(new DQLExecutionContext(getSession())).list();

        Map<EnrollmentDirection, List<EntranceDiscipline>> direction2disciplineListMap = new LinkedHashMap<>();
        for (EntranceDiscipline entranceDiscipline : entranceDisciplineList)
        {
            List<EntranceDiscipline> list = direction2disciplineListMap.get(entranceDiscipline.getEnrollmentDirection());
            if (list == null)
                direction2disciplineListMap.put(entranceDiscipline.getEnrollmentDirection(), list = new ArrayList<>());
            list.add(entranceDiscipline);
        }

        // собираем информацию по видам целевого приема из указанной приемной кампании

        List<TargetAdmissionPlanRelation> targetAdmissionPlanRelationList = new DQLSelectBuilder().fromEntity(TargetAdmissionPlanRelation.class, "d").column("d")
        .where(DQLExpressions.eq(DQLExpressions.property(TargetAdmissionPlanRelation.entranceEducationOrgUnit().enrollmentCampaign().fromAlias("d")), DQLExpressions.value(enrollmentCampaign)))
        .where(DQLExpressions.in(DQLExpressions.property(TargetAdmissionPlanRelation.targetAdmissionKind().fromAlias("d")), targetAdmissionKindList))
        .order(DQLExpressions.property(TargetAdmissionPlanRelation.id().fromAlias("d")))
        .createStatement(new DQLExecutionContext(getSession())).list();

        Map<EnrollmentDirection, List<TargetAdmissionPlanRelation>> direction2admissionListMap = new LinkedHashMap<>();
        for (TargetAdmissionPlanRelation targetAdmissionPlanRelation : targetAdmissionPlanRelationList)
        {
            List<TargetAdmissionPlanRelation> list = direction2admissionListMap.get(targetAdmissionPlanRelation.getEntranceEducationOrgUnit());
            if (list == null)
                direction2admissionListMap.put(targetAdmissionPlanRelation.getEntranceEducationOrgUnit(), list = new ArrayList<>());
            list.add(targetAdmissionPlanRelation);
        }

        // собираем информацию по вступительным испытаниям по выборку

        List<EntranceDiscipline2SetDisciplineRelation> entranceDiscipline2SetDisciplineRelationList = new DQLSelectBuilder().fromEntity(EntranceDiscipline2SetDisciplineRelation.class, "r").column("r")
        .where(DQLExpressions.eq(DQLExpressions.property(EntranceDiscipline2SetDisciplineRelation.entranceDiscipline().enrollmentDirection().enrollmentCampaign().fromAlias("r")), DQLExpressions.value(enrollmentCampaign)))
        .order(DQLExpressions.property(EntranceDiscipline2SetDisciplineRelation.id().fromAlias("r")))
        .createStatement(new DQLExecutionContext(getSession())).list();

        Map<EntranceDiscipline, List<String>> discipline2chosenMap = new HashMap<>();
        for (EntranceDiscipline2SetDisciplineRelation relation : entranceDiscipline2SetDisciplineRelationList)
        {
            List<String> list = discipline2chosenMap.get(relation.getEntranceDiscipline());
            if (list == null)
                discipline2chosenMap.put(relation.getEntranceDiscipline(), list = new ArrayList<>());
            list.add(relation.getSetDiscipline().getId().toString());
        }

        // сохраняем все направления приема

        List<EnrollmentDirection> directionList = new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, "d")
        .column("d")
        .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.enrollmentCampaign().fromAlias("d")), DQLExpressions.value(enrollmentCampaign)))
        .order(DQLExpressions.property(EnrollmentDirection.id().fromAlias("d")))
        .createStatement(new DQLExecutionContext(getSession()))
        .list();

        Set<Long> usedOrgUnitIds = new HashSet<>();
        Set<Long> usedEducationLevelHighSchoolIds = new HashSet<>();
        Set<OrgUnitType> usedOrgUnitTypes = new HashSet<>();

        for (EnrollmentDirection direction : directionList)
        {
            EducationOrgUnit ou = direction.getEducationOrgUnit();

            if (usedOrgUnitIds.add(ou.getFormativeOrgUnit().getId()))
            {
                OrgUnit orgUnit = ou.getFormativeOrgUnit();
                usedOrgUnitTypes.add(orgUnit.getOrgUnitType());
                String settlemenetTitle = null;
                if (orgUnit.getAddress() != null && orgUnit.getAddress().getSettlement() != null)
                    settlemenetTitle = orgUnit.getAddress().getSettlement().getTitle();
                envNode.orgUnit.row.add(new EnrollmentEnvironmentNode.OrgUnitNode.OrgUnitRow(orgUnit.getShortTitle(), orgUnit.getTitle(), orgUnit.getId().toString(), orgUnit.getOrgUnitType().getCode(), orgUnit.getNominativeCaseTitle(), settlemenetTitle, orgUnit.getTerritorialTitle(), orgUnit.getTerritorialShortTitle()));
            }

            if (usedOrgUnitIds.add(ou.getTerritorialOrgUnit().getId()))
            {
                OrgUnit orgUnit = ou.getTerritorialOrgUnit();
                usedOrgUnitTypes.add(orgUnit.getOrgUnitType());
                String settlemenetTitle = null;
                if (orgUnit.getAddress() != null && orgUnit.getAddress().getSettlement() != null)
                    settlemenetTitle = orgUnit.getAddress().getSettlement().getTitle();
                envNode.orgUnit.row.add(new EnrollmentEnvironmentNode.OrgUnitNode.OrgUnitRow(orgUnit.getShortTitle(), orgUnit.getTitle(), orgUnit.getId().toString(), orgUnit.getOrgUnitType().getCode(), orgUnit.getNominativeCaseTitle(), settlemenetTitle, orgUnit.getTerritorialTitle(), orgUnit.getTerritorialShortTitle()));
            }

            if (usedEducationLevelHighSchoolIds.add(ou.getEducationLevelHighSchool().getId()))
            {
                EducationLevelsHighSchool hs = ou.getEducationLevelHighSchool();
                envNode.educationLevelHighSchool.row.add(new EnrollmentEnvironmentNode.EducationLevelHighSchoolNode.EducationLevelHighSchoolRow(hs.getShortTitle(), hs.getTitle(), hs.getId().toString(), hs.getEducationLevel().getTitleCodePrefix(), hs.getEducationLevel().getQualification().getCode()));
            }

            EnrollmentEnvironmentNode.EnrollmentDirectionNode.DirectionRow row = new EnrollmentEnvironmentNode.EnrollmentDirectionNode.DirectionRow();

            row.id = direction.getId().toString();
            row.formativeOrgUnit = ou.getFormativeOrgUnit().getId().toString();
            if (ou.getTerritorialOrgUnit() != null)
                row.territorialOrgUnit = ou.getTerritorialOrgUnit().getId().toString();
            row.educationLevelHighSchool = ou.getEducationLevelHighSchool().getId().toString();
            row.educationOrgUnit = ou.getId().toString();
            row.structureEducationLevel = ou.getEducationLevelHighSchool().getEducationLevel().getLevelType().getCode();
            row.developForm = ou.getDevelopForm().getCode();
            row.developCondition = ou.getDevelopCondition().getCode();
            row.developTech = ou.getDevelopTech().getCode();
            row.developPeriod = ou.getDevelopPeriod().getCode();
            row.budget = direction.isBudget();
            row.contract = direction.isContract();
            row.interview = direction.isInterview();
            row.ministerialPlan = direction.getMinisterialPlan();
            row.contractPlan = direction.getContractPlan();
            row.targetAdmissionPlanBudget = direction.getTargetAdmissionPlanBudget();
            row.targetAdmissionPlanContract = direction.getTargetAdmissionPlanContract();
            row.listenerPlan = direction.getListenerPlan();
            row.secondHighEducationPlan = direction.getSecondHighEducationPlan();

            List<EntranceDiscipline> list = direction2disciplineListMap.get(direction);

            if (list != null)
            {
                for (EntranceDiscipline discipline : list)
                {
                    EnrollmentEnvironmentNode.EnrollmentDirectionNode.EntranceDisciplineRow disciplineRow = new EnrollmentEnvironmentNode.EnrollmentDirectionNode.EntranceDisciplineRow();
                    disciplineRow.studentCategory = discipline.getStudentCategory().getCode();
                    disciplineRow.entranceDisciplineKind = discipline.getKind().getCode();
                    disciplineRow.budget = discipline.isBudget();
                    disciplineRow.contract = discipline.isContract();
                    disciplineRow.discipline = discipline.getDiscipline().getId().toString();
                    disciplineRow.choiceDisciplines = discipline2chosenMap.get(discipline);
                    row.entranceDiscipline.row.add(disciplineRow);
                }
            }

            List<TargetAdmissionPlanRelation> admissionPlanRelationList = direction2admissionListMap.get(direction);

            if (admissionPlanRelationList != null)
            {
                for (TargetAdmissionPlanRelation relation : admissionPlanRelationList)
                {
                    EnrollmentEnvironmentNode.EnrollmentDirectionNode.TargetAdmissionPlanRow planRow = new EnrollmentEnvironmentNode.EnrollmentDirectionNode.TargetAdmissionPlanRow();
                    planRow.targetAdmissionKind = relation.getTargetAdmissionKind().getCode();
                    planRow.budget = relation.isBudget();
                    planRow.plan = relation.getPlanValue();
                    row.targetAdmissionPlan.row.add(planRow);
                }
            }

            envNode.enrollmentDirection.row.add(row);
        }

        List<OrgUnitType> orgUnitTypeList = new ArrayList<>(usedOrgUnitTypes);
        Collections.sort(orgUnitTypeList, CommonCatalogUtil.CATALOG_CODE_COMPARATOR);
        for (OrgUnitType type : orgUnitTypeList)
            envNode.orgUnitType.row.add(new EnrollmentEnvironmentNode.Row(type.getTitle(), type.getCode()));

        Collections.sort(envNode.orgUnit.row);
        Collections.sort(envNode.educationLevelHighSchool.row);

        return envNode;
    }
}
