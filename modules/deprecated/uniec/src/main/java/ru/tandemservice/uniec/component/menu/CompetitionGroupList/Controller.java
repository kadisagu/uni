/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.menu.CompetitionGroupList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IEntityHandler;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.IUniecComponents;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

/**
 * @author vip_delete
 * @since 27.05.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.setSettings(component.getSettings());

        getDao().prepare(getModel(component));

        getDao().deleteEmptyCompetitionGroups(getModel(component));

        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null)
            return;

        IMergeRowIdResolver mergeRowIdResolver = entity -> Long.toString(((EnrollmentDirection) entity).getCompetitionGroup().getId());

        DynamicListDataSource<EnrollmentDirection> dataSource = UniBaseUtils.createDataSource(component, getDao());

        dataSource.addColumn(new SimpleColumn("Название", EnrollmentDirection.competitionGroup().title().s()).setMergeRows(true).setMergeRowIdResolver(mergeRowIdResolver).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().displayableTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Формирующее подр.", EnrollmentDirection.FORMATIVE_ORG_UNIT_SHORT_TITLE_KEY).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Территориальное подр.", EnrollmentDirection.TERRITORIAL_ORG_UNIT_SHORT_TITLE_KEY).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Форма освоения", EnrollmentDirection.educationOrgUnit().developForm().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Условие освоения", EnrollmentDirection.educationOrgUnit().developCondition().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Технология освоения", EnrollmentDirection.educationOrgUnit().developTech().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Срок освоения", EnrollmentDirection.educationOrgUnit().developPeriod().title().s()).setClickable(false).setOrderable(false));

        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit").setMergeRows(true).setMergeRowIdResolver(mergeRowIdResolver).setPermissionKey("editCompetitionGroup"));

        final IEntityHandler deleteDisabler = entity -> model.getDistributedIds().contains(((EnrollmentDirection)entity).getCompetitionGroup().getId());
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", getMessage("competition.group.delete") + " «{0}»?", EnrollmentDirection.competitionGroup().title().s()).setMergeRows(true).setMergeRowIdResolver(mergeRowIdResolver).setPermissionKey("delCompetitionGroup").setDisableHandler(deleteDisabler));
        model.setDataSource(dataSource);
    }

    public void onClickSearch(IBusinessComponent component)
    {
        getModel(component).getDataSource().refresh();
        component.saveSettings();
    }

    public void onClickAdd(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniecComponents.COMPETITION_GROUP_ADD_EDIT, new ParametersMap().add("competitionGroupId", null)));
    }

    public void onClickEdit(IBusinessComponent component)
    {
        EnrollmentDirection direction = getDao().getNotNull(EnrollmentDirection.class, (Long) component.getListenerParameter());
        component.createDefaultChildRegion(new ComponentActivator(IUniecComponents.COMPETITION_GROUP_ADD_EDIT, new ParametersMap().add("competitionGroupId", direction.getCompetitionGroup().getId())));
    }

    public void onClickDelete(IBusinessComponent component)
    {
        getDao().delete((Long)component.getListenerParameter(), getMessageSource());
        getModel(component).getDataSource().refresh();
    }
}
