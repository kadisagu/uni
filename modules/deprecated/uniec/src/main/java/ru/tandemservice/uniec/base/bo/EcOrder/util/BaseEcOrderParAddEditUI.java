/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcOrder.util;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.RadioButtonColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager;
import ru.tandemservice.uniec.base.bo.EcOrder.logic.EcOrderPreStudentDSHandler;
import ru.tandemservice.uniec.base.bo.EcOrder.logic.IEnrollmentExtractFactory;
import ru.tandemservice.uniec.base.bo.EcOrder.ui.BlockParAddEdit.EcOrderBlockParAddEdit;
import ru.tandemservice.uniec.base.bo.EcOrder.ui.BlockParAddEdit.EcOrderBlockParAddEditUI;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrderType;
import ru.tandemservice.uniec.entity.orders.EnrollmentParagraph;
import ru.tandemservice.unimove.IAbstractExtract;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Vasily Zhukov
 * @since 24.05.2011
 */
@Input({
    @Bind(key = BaseEcOrderParAddEditUI.PARAMETER_ORDER_ID, binding = BaseEcOrderParAddEditUI.PARAMETER_ORDER_ID),
    @Bind(key = BaseEcOrderParAddEditUI.PARAMETER_PARAGRAPH_ID, binding = BaseEcOrderParAddEditUI.PARAMETER_PARAGRAPH_ID)
})
public abstract class BaseEcOrderParAddEditUI extends UIPresenter implements IEnrollmentExtractFactory
{
    public static final String PARAMETER_ORDER_ID = "orderId";
    public static final String PARAMETER_PARAGRAPH_ID = "paragraphId";
    public static final String EC_ORDER_BLOCK_PAR_ADD_EDIT_REGION = "ecOrderBlockParAddEditRegion";

    private Long _orderId;
    private Long _paragraphId;
    private EnrollmentOrder _order;
    private EnrollmentParagraph _paragraph;
    private String _formTitle;
    private boolean _editForm;

    private EnrollmentOrderType _type;
    private Set<Long> _selectedPreStudentIds;
    private Long _groupManagerPreStudentId;
    private List<Course> _courseList;
    private Course _course;
    private String _groupTitle;

    @Override
    public void onComponentRefresh()
    {
        if (_orderId != null)
        {
            // создание параграфа
            _editForm = false;
            _order = DataAccessServices.dao().getNotNull(_orderId);
        }
        else
        {
            // редактирование параграфа
            _editForm = true;
            _paragraph = DataAccessServices.dao().getNotNull(_paragraphId);
            _order = (EnrollmentOrder) _paragraph.getOrder();

            EnrollmentExtract firstExtract = _paragraph.getFirstExtract();
            _course = firstExtract.getCourse();
            _groupTitle = firstExtract.getGroupTitle();

            _selectedPreStudentIds = new HashSet<>();
            for (IAbstractExtract extract : _paragraph.getExtractList())
                _selectedPreStudentIds.add(((PreliminaryEnrollmentStudent) extract.getEntity()).getId());
            _groupManagerPreStudentId = _paragraph.getGroupManager() == null ? null : _paragraph.getGroupManager().getEntity().getId();
        }

        _courseList = DevelopGridDAO.getCourseList();

        _type = EcOrderManager.instance().dao().getEnrollmentOrderType(_order.getEnrollmentCampaign(), _order.getType());

        String formTitle = (StringUtils.isNotEmpty(_order.getNumber()) ? "№" + _order.getNumber() + " " : "") + (_order.getCommitDate() != null ? "от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(_order.getCommitDate()) : "");
        if (_editForm)
            _formTitle = "Редактирование параграфа из приказа о зачислении " + formTitle;
        else
            _formTitle = "Добавление параграфа приказа о зачислении " + formTitle;

        // отображает общую часть формы добавления/редактирования параграфа
        UIPresenter presenter = _uiSupport.getChildUI(EC_ORDER_BLOCK_PAR_ADD_EDIT_REGION);
        if (presenter == null)
            _uiActivation.asRegion(EcOrderBlockParAddEdit.class, EC_ORDER_BLOCK_PAR_ADD_EDIT_REGION)
            .parameter(EcOrderBlockParAddEditUI.PARAMETER_ORDER_ID, _orderId)
            .parameter(EcOrderBlockParAddEditUI.PARAMETER_PARAGRAPH_ID, _paragraphId)
            .activate();
        else
            presenter.onComponentRefresh();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (EcOrderManager.PRE_STUDENT_DS.equals(dataSource.getName()))
        {
            EcOrderPreStudentDSHandler.Param param = _editForm ? EcOrderPreStudentDSHandler.Param.createEditFormParam(_paragraph) : EcOrderPreStudentDSHandler.Param.createAddFormParam(_order);

            EcOrderBlockParAddEditUI child = _uiSupport.getChildUI(EC_ORDER_BLOCK_PAR_ADD_EDIT_REGION);

            if (child.isEnrollParagraphPerEduOrgUnit())
                param.addParams(child.getFormativeOrgUnit(), child.getTerritorialOrgUnit(), child.getEducationLevelsHighSchool(), child.getDevelopForm(), child.getDevelopCondition(), child.getDevelopTech(), child.getDevelopPeriod());
            else
                param.addParams(child.getFormativeOrgUnit(), child.getEducationLevelsHighSchool(), child.getDevelopForm());

            dataSource.put(EcOrderPreStudentDSHandler.EC_ORDER_PRE_STUDENT_DS_PARAM, param);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onAfterDataSourceFetch(IUIDataSource dataSource)
    {
        if (_editForm)
        {
            if (EcOrderManager.PRE_STUDENT_DS.equals(dataSource.getName()))
            {
                DynamicListDataSource ds = ((PageableSearchListDataSource) dataSource).getLegacyDataSource();

                List<IEntity> checkboxColumnSelected = new ArrayList<>();
                IEntity radioboxColumnSelected = null;

                for (DataWrapper record : (List<DataWrapper>) ds.getEntityList())
                {
                    if (_selectedPreStudentIds.contains(record.getId()))
                        checkboxColumnSelected.add(record);

                    if (record.getId().equals(_groupManagerPreStudentId))
                        radioboxColumnSelected = record;
                }

                CheckboxColumn checkboxColumn = ((CheckboxColumn) ds.getColumn(EcOrderManager.CHECKBOX_COLUMN));
                if (checkboxColumn != null)
                    checkboxColumn.setSelectedObjects(checkboxColumnSelected);

                RadioButtonColumn radioboxColumn = ((RadioButtonColumn) ds.getColumn(EcOrderManager.RADIOBOX_COLUMN));
                if (radioboxColumn != null)
                    radioboxColumn.setSelectedEntity(radioboxColumnSelected);
            }
        }
    }

    public boolean isEnrollParagraphPerEduOrgUnit()
    {
        return ((EcOrderBlockParAddEditUI) _uiSupport.getChildUI(EC_ORDER_BLOCK_PAR_ADD_EDIT_REGION)).isEnrollParagraphPerEduOrgUnit();
    }

    // Getters & Setters

    public Long getOrderId()
    {
        return _orderId;
    }

    public void setOrderId(Long orderId)
    {
        _orderId = orderId;
    }

    public Long getParagraphId()
    {
        return _paragraphId;
    }

    public void setParagraphId(Long paragraphId)
    {
        _paragraphId = paragraphId;
    }

    public EnrollmentOrder getOrder()
    {
        return _order;
    }

    public void setOrder(EnrollmentOrder order)
    {
        _order = order;
    }

    public EnrollmentParagraph getParagraph()
    {
        return _paragraph;
    }

    public void setParagraph(EnrollmentParagraph paragraph)
    {
        _paragraph = paragraph;
    }

    public String getFormTitle()
    {
        return _formTitle;
    }

    public void setFormTitle(String formTitle)
    {
        _formTitle = formTitle;
    }

    public boolean isEditForm()
    {
        return _editForm;
    }

    public void setEditForm(boolean editForm)
    {
        _editForm = editForm;
    }

    public EnrollmentOrderType getType()
    {
        return _type;
    }

    public void setType(EnrollmentOrderType type)
    {
        _type = type;
    }

    public Set<Long> getSelectedPreStudentIds()
    {
        return _selectedPreStudentIds;
    }

    public void setSelectedPreStudentIds(Set<Long> selectedPreStudentIds)
    {
        _selectedPreStudentIds = selectedPreStudentIds;
    }

    public Long getGroupManagerPreStudentId()
    {
        return _groupManagerPreStudentId;
    }

    public void setGroupManagerPreStudentId(Long groupManagerPreStudentId)
    {
        _groupManagerPreStudentId = groupManagerPreStudentId;
    }

    public List<Course> getCourseList()
    {
        return _courseList;
    }

    public void setCourseList(List<Course> courseList)
    {
        _courseList = courseList;
    }

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public String getGroupTitle()
    {
        return _groupTitle;
    }

    public void setGroupTitle(String groupTitle)
    {
        _groupTitle = groupTitle;
    }

    // Listeners
    public void onClickApply()
    {
        DynamicListDataSource ds = ((PageableSearchListDataSource) _uiConfig.getDataSource(EcOrderManager.PRE_STUDENT_DS)).getLegacyDataSource();

        CheckboxColumn checkboxColumn = ((CheckboxColumn) ds.getColumn(EcOrderManager.CHECKBOX_COLUMN));
        if (checkboxColumn == null)
            throw new ApplicationException("Параграф не может быть пустым.");
        List<PreliminaryEnrollmentStudent> preStudents = checkboxColumn.getSelectedObjects().stream()
                .map(entity -> (PreliminaryEnrollmentStudent) ((DataWrapper) entity).getWrapped())
                .collect(Collectors.toList());

        RadioButtonColumn radioboxColumn = ((RadioButtonColumn) ds.getColumn(EcOrderManager.RADIOBOX_COLUMN));
        PreliminaryEnrollmentStudent manager = null;
        if (_type.isSelectHeadman() && radioboxColumn != null && radioboxColumn.getSelectedEntity() != null)
            manager = ((DataWrapper) radioboxColumn.getSelectedEntity()).getWrapped();

        // загружаем внутренний компонент
        EcOrderBlockParAddEditUI child = _uiSupport.getChildUI(EC_ORDER_BLOCK_PAR_ADD_EDIT_REGION);

        // сохраняем или обновляем параграф о зачислении
        EcOrderManager.instance().dao().saveOrUpdateEnrollmentParagraph(_order,
                                                                        _paragraph,
                                                                        this,
                                                                        child.isEnrollParagraphPerEduOrgUnit(),
                                                                        preStudents,
                                                                        manager,
                                                                        child.getFormativeOrgUnit(),
                                                                        child.getEducationLevelsHighSchool(),
                                                                        child.getDevelopForm(),
                                                                        _course,
                                                                        _groupTitle);
        // закрываем форму
        deactivate();
    }
}
