package ru.tandemservice.uniec.entity.settings;

import org.tandemframework.core.common.ITitled;

import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.settings.gen.EnrollmentCompetitionKindGen;

public class EnrollmentCompetitionKind extends EnrollmentCompetitionKindGen implements ITitled, Comparable<EnrollmentCompetitionKind>
{
    public static final String TARGET_ADMISSION = "Целевой прием";
    public static final String P_TITLE = "title";

    @Override
    public String getTitle()
    {
        CompetitionKind competitionKind = getCompetitionKind();
        return (competitionKind != null) ? competitionKind.getTitle() : TARGET_ADMISSION;
    }

    @Override
    public int compareTo(EnrollmentCompetitionKind o)
    {
        return (o != null) ? getPriority() - o.getPriority() : -1;
    }
}