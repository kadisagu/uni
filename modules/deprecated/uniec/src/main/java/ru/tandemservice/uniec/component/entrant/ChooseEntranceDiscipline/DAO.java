/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.ChooseEntranceDiscipline;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationFormRelation;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

import java.util.*;

/**
 * @author vip_delete
 * @since 17.03.2009
 */
public class DAO extends UniDao<ChooseEntranceDisciplineModel> implements IDAO
{
    @Override
    public void prepare(ChooseEntranceDisciplineModel model)
    {
        if (model.getDirectionId() != null)
        {
            RequestedEnrollmentDirection direction = getNotNull(RequestedEnrollmentDirection.class, model.getDirectionId());
            model.setEntrantRequest(direction.getEntrantRequest());
            model.setEntrant(model.getEntrantRequest().getEntrant());
            model.setEnrollmentCampaign(model.getEntrant().getEnrollmentCampaign());
        } else if (model.getEntrantRequestId() != null)
        {
            model.setEntrantRequest(getNotNull(EntrantRequest.class, model.getEntrantRequestId()));
            model.setEntrant(model.getEntrantRequest().getEntrant());
            model.setEnrollmentCampaign(model.getEntrant().getEnrollmentCampaign());
        } else if (model.getEntrantId() != null)
        {
            model.setEntrant(getNotNull(Entrant.class, model.getEntrantId()));
            model.setEnrollmentCampaign(model.getEntrant().getEnrollmentCampaign());
        } else
            throw new RuntimeException("Wrong component usage!");
        model.setDisciplineFormRestriction(model.getEnrollmentCampaign().isDisciplineFormRestriction());
    }

    @Override
    public void prepareListDataSource(ChooseEntranceDisciplineModel model)
    {
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "d");
        if (model.getDirectionId() != null)
            builder.add(MQExpression.eq("d", RequestedEnrollmentDirection.P_ID, model.getDirectionId()));
        else if (model.getEntrantRequestId() != null)
            builder.add(MQExpression.eq("d", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.P_ID, model.getEntrantRequestId()));
        else if (model.getEntrantId() != null)
            builder.add(MQExpression.eq("d", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ID, model.getEntrantId()));
        else
            throw new RuntimeException("Wrong component usage!");

        List<RequestedEnrollmentDirectionWrapper> wrapperList = new ArrayList<>();
        for (RequestedEnrollmentDirection direction : builder.<RequestedEnrollmentDirection>getResultList(getSession()))
            wrapperList.add(new RequestedEnrollmentDirectionWrapper(direction));

        model.getDataSource().setCountRow(wrapperList.size());
        UniBaseUtils.createPage(model.getDataSource(), wrapperList);
    }

    @Override
    public void update(ChooseEntranceDisciplineModel model)
    {
        final Session session = getSession();

        // формируем экзаменационные листы
        IEntrant ientrant = UniecDefines.EXAMLIST_FORMING_FEATURE_FOR_ENTRANT.equals(model.getEnrollmentCampaign().getEntrantExamListsFormingFeature().getCode()) ? model.getEntrant() : model.getEntrantRequest();

        EntrantExamList entrantExamList = get(EntrantExamList.class, EntrantExamList.L_ENTRANT, ientrant);
        if (entrantExamList == null)
        {
            // создаем если его еще нет
            entrantExamList = new EntrantExamList();
            entrantExamList.setEntrant(ientrant);
            getSession().save(entrantExamList);
        }

        // загружаем все examPassDiscipline из найденного экзам листа
        // set of [дисциплина приемной кампании, форма сдачи]
        Set<MultiKey> examPassDisciplineSet = new HashSet<>();
        for (ExamPassDiscipline examPassDiscipline : new MQBuilder(ExamPassDiscipline.ENTITY_CLASS, "d")
                .add(MQExpression.eq("d", ExamPassDiscipline.L_ENTRANT_EXAM_LIST, entrantExamList))
                .<ExamPassDiscipline>getResultList(session))
            examPassDisciplineSet.add(new MultiKey(examPassDiscipline.getEnrollmentCampaignDiscipline(), examPassDiscipline.getSubjectPassForm()));

        // загружаем дисциплины, покрывающиеся сертификатами ЕГЭ этого года
        Set<Discipline2RealizationWayRelation> coveredDisciplineSet = UniecDAOFacade.getExamSetDAO().getCoveredDisciplineList(model.getEntrant());

        // загружаем все формы сдачи по дисциплине
        // дисциплина приемной кампании -> все ее формы сдачи в указанной приемной кампании
        Map<Discipline2RealizationWayRelation, Set<Discipline2RealizationFormRelation>> realizationFormMap = new HashMap<>();
        for (Discipline2RealizationFormRelation formRelation : new MQBuilder(Discipline2RealizationFormRelation.ENTITY_CLASS, "d")
                .add(MQExpression.eq("d", Discipline2RealizationFormRelation.L_DISCIPLINE + "." + Discipline2RealizationWayRelation.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()))
                .<Discipline2RealizationFormRelation>getResultList(session))
        {
            Set<Discipline2RealizationFormRelation> set = realizationFormMap.get(formRelation.getDiscipline());
            if (set == null)
                realizationFormMap.put(formRelation.getDiscipline(), set = new HashSet<>());
            set.add(formRelation);
        }

        // загружаем выбранные вступительные испытания по абитуриенту
        // выбранное направление приема -> мн-во выбранных вступительных испытаний
        Map<RequestedEnrollmentDirection, Set<ChosenEntranceDiscipline>> chosenDisciplineMap = new HashMap<>();
        for (ChosenEntranceDiscipline chosen : new MQBuilder(ChosenEntranceDiscipline.ENTITY_CLASS, "d")
                .add(MQExpression.eq("d", ChosenEntranceDiscipline.L_CHOSEN_ENROLLMENT_DIRECTION + "." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT, model.getEntrant()))
                .<ChosenEntranceDiscipline>getResultList(session))
        {
            Set<ChosenEntranceDiscipline> set = chosenDisciplineMap.get(chosen.getChosenEnrollmentDirection());
            if (set == null)
                chosenDisciplineMap.put(chosen.getChosenEnrollmentDirection(), set = new HashSet<>());
            set.add(chosen);
        }

        for (RequestedEnrollmentDirectionWrapper wrapper : model.getDataSource().getEntityList())
        {
            final RequestedEnrollmentDirection direction = wrapper.getDirection();
            final EntrantRequest entrantRequest = direction.getEntrantRequest();
            final Entrant entrant = entrantRequest.getEntrant();
            final List<Discipline2RealizationWayRelation> selectedDisciplineList = wrapper.getSelectedDisciplineList();

            if (selectedDisciplineList.size() != new HashSet<>(selectedDisciplineList).size())
                throw new ApplicationException("Нельзя выбирать две одинаковые дисциплины.");

            // сохраненные в базе выбранные дисциплины
            Set<ChosenEntranceDiscipline> set = chosenDisciplineMap.get(direction);
            List<ChosenEntranceDiscipline> savedInDatabase = new ArrayList<>();
            if (set != null) savedInDatabase.addAll(set);
            
            // выбранные дисциплины которые будут сохранены в базе
            List<ChosenEntranceDiscipline> newSavedInDataBase = new ArrayList<>();
            
            // исключим из этого списка все такие дициплины, которые выбраные на форме, а оставшиеся удалим
            for (Discipline2RealizationWayRelation discipline : selectedDisciplineList)
            {
                ChosenEntranceDiscipline chosenEntranceDiscipline;

                // проверяем, выбрана ли уже такая дисциплина
                int i = 0;
                while (i < savedInDatabase.size() && !(savedInDatabase.get(i).getChosenEnrollmentDirection().equals(direction) && savedInDatabase.get(i).getEnrollmentCampaignDiscipline().equals(discipline)))
                    i++;
                if (i < savedInDatabase.size())
                {
                    // такая дисциплина уже выбрана
                    chosenEntranceDiscipline = savedInDatabase.remove(i);
                } else
                {
                    // надо создавать новую дисциплину
                    chosenEntranceDiscipline = new ChosenEntranceDiscipline();
                    chosenEntranceDiscipline.setChosenEnrollmentDirection(direction);
                    chosenEntranceDiscipline.setEnrollmentCampaignDiscipline(discipline);
                    session.save(chosenEntranceDiscipline);
                }
                newSavedInDataBase.add(chosenEntranceDiscipline);
            }

            // удаляем все вновь не выбранные дисциплины
            for (ChosenEntranceDiscipline chosenEntranceDiscipline : savedInDatabase)
            {
                if (chosenEntranceDiscipline.equals(chosenEntranceDiscipline.getChosenEnrollmentDirection().getProfileChosenEntranceDiscipline()))
                    chosenEntranceDiscipline.getChosenEnrollmentDirection().setProfileChosenEntranceDiscipline(null);
                session.delete(chosenEntranceDiscipline);
            }

            // надо чтобы в дальнейдем удаленные дисциплины не участвовали в select'ах
            session.flush();

            // создаем дисциплины для сдачи на базе различных дисциплин приемной кампании
            for (ChosenEntranceDiscipline discipline : newSavedInDataBase)
            {
                // получаю все формы значи для указанной дисциплины
                Set<Discipline2RealizationFormRelation> relList = realizationFormMap.get(discipline.getEnrollmentCampaignDiscipline());
                if (relList != null)
                {
                    // тут надо исключить некоторые формы сдачи
                    // ...

                    // сохраняем дисциплины для сдачи по всем формам сдачи
                    // дисциплины для сдачи не выбранные на форме не удаляются
                    // учитываем признак бюджет/контракт для дисциплины по форме сдачи
                    // формируем дисциплины для сдачи только для внутренних форм сдачи
                    for (Discipline2RealizationFormRelation rel : relList)
                    {
                        // не создаем дисциплины для сдачи во внешних формах сдачи
                        if (!rel.getSubjectPassForm().isInternal())
                            continue;

                        // если отличия в приеме на бюджет и контракт и выбранная форма не по тому виду возмещения затрат
                        if (entrant.getEnrollmentCampaign().isEnrollmentPerCompTypeDiff() && !discipline.getChosenEnrollmentDirection().getCompensationType().equals(rel.getType()))
                            continue;

                        // есть ограничение в формировании дисциплин и дисциплина покрывается сертификатом ЕГЭ
                        if (model.isDisciplineFormRestriction() && coveredDisciplineSet.contains(discipline.getEnrollmentCampaignDiscipline()))
                            continue;

                        MultiKey key = new MultiKey(discipline.getEnrollmentCampaignDiscipline(), rel.getSubjectPassForm());
                        if (!examPassDisciplineSet.contains(key))
                        {
                            examPassDisciplineSet.add(key);

                            ExamPassDiscipline examPassDiscipline = new ExamPassDiscipline();
                            examPassDiscipline.setEnrollmentCampaignDiscipline(discipline.getEnrollmentCampaignDiscipline());
                            examPassDiscipline.setSubjectPassForm(rel.getSubjectPassForm());
                            examPassDiscipline.setEntrantExamList(entrantExamList);
                            session.save(examPassDiscipline);
                        }
                    }
                }
            }
        }

        // удаляем экзаменационный лист если он пустой
        if (!existsEntity(ExamPassDiscipline.class, ExamPassDiscipline.L_ENTRANT_EXAM_LIST, entrantExamList))
            session.delete(entrantExamList);
    }
}
