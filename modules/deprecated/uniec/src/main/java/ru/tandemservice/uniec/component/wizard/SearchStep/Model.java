/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.wizard.SearchStep;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.gen.EnrollmentCampaignGen;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;

/**
 * @author agolubenko
 * @since May 12, 2010
 */
@Input(@Bind(key = "entrantMasterPermKey", binding = "entrantMasterPermKey"))
public class Model implements IEnrollmentCampaignSelectModel
{
    private Integer _personalNumber;
    private OnlineEntrant _onlineEntrant;
    private ISelectModel _onlineEntrantModel;

    private SearchMethod _searchMethod = SearchMethod.ENTRANTS;
    private List<SearchMethod> _searchMethodList = Arrays.asList(SearchMethod.ENTRANTS, SearchMethod.ONLINE_ENTRANTS);
    private String _blockName = _searchMethod.getName();

    private IdentityCard _identityCard = new IdentityCard();
    private List<IdentityCardType> _identityCardTypeList;

    private EnrollmentCampaign _enrollmentCampaign;
    private List<EnrollmentCampaign> _enrollmentCampaignList;

    private String _entrantMasterPermKey;

    public String getEntrantMasterPermKey()
    {
        if(StringUtils.isEmpty(_entrantMasterPermKey)) return "addEntrantMaster";
        return _entrantMasterPermKey;
    }

    public void setEntrantMasterPermKey(String entrantMasterPermKey)
    {
        _entrantMasterPermKey = entrantMasterPermKey;
    }

    @Override
    public IDataSettings getSettings()
    {
        return null;
    }

    public Integer getPersonalNumber()
    {
        return _personalNumber;
    }

    public void setPersonalNumber(Integer personalNumber)
    {
        _personalNumber = personalNumber;
    }

    public IdentityCard getIdentityCard()
    {
        return _identityCard;
    }

    public void setIdentityCard(IdentityCard identityCard)
    {
        _identityCard = identityCard;
    }

    public OnlineEntrant getOnlineEntrant()
    {
        return _onlineEntrant;
    }

    public void setOnlineEntrant(OnlineEntrant onlineEntrant)
    {
        _onlineEntrant = onlineEntrant;
    }

    public SearchMethod getSearchMethod()
    {
        return _searchMethod;
    }

    public void setSearchMethod(SearchMethod searchMethod)
    {
        _searchMethod = searchMethod;
    }

    public List<SearchMethod> getSearchMethodList()
    {
        return _searchMethodList;
    }

    public void setSearchMethodList(List<SearchMethod> searchMethodList)
    {
        _searchMethodList = searchMethodList;
    }

    public List<IdentityCardType> getIdentityCardTypeList()
    {
        return _identityCardTypeList;
    }

    public void setIdentityCardTypeList(List<IdentityCardType> identityCardTypeList)
    {
        _identityCardTypeList = identityCardTypeList;
    }

    public ISelectModel getOnlineEntrantModel()
    {
        return _onlineEntrantModel;
    }

    public void setOnlineEntrantModel(ISelectModel onlineEntrantModel)
    {
        _onlineEntrantModel = onlineEntrantModel;
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public String getBlockName()
    {
        return _blockName;
    }

    public void setBlockName(String blockName)
    {
        _blockName = blockName;
    }

    public boolean isUseOnlineEntrantRegistration()
    {
        return _enrollmentCampaignList.stream().anyMatch(EnrollmentCampaignGen::isUseOnlineEntrantRegistration);
    }

    public static enum SearchMethod
    {
        ENTRANTS("entrants", "Добавление нового абитуриента"), ONLINE_ENTRANTS("onlineEntrants", "Добавление абитуриента после онлайн-регистрации");

        private String _name;
        private String _title;

        private SearchMethod(String name, String title)
        {
            _name = name;
            _title = title;
        }

        public String getName()
        {
            return _name;
        }

        public String getTitle()
        {
            return _title;
        }
    }
}
