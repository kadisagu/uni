/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcOrder.ui.ListenerParParAddEdit;

import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniec.base.bo.EcOrder.util.BaseEcOrderParAddEditUI;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EntrListenerParExtract;

/**
 * @author Vasily Zhukov
 * @since 24.05.2011
 */
public class EcOrderListenerParParAddEditUI extends BaseEcOrderParAddEditUI
{
    private ISelectModel _parallelListModel;

    private EducationLevelsHighSchool _parallel;

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();

        if (!isEditForm())
            setCourse(DevelopGridDAO.getCourseMap().get(1));
        else
            _parallel = ((EntrListenerParExtract) getParagraph().getFirstExtract()).getParallel();

        _parallelListModel = new LazySimpleSelectModel<>(EducationLevelsHighSchool.class, EducationLevelsHighSchool.P_DISPLAYABLE_TITLE);
    }

    @Override
    public EnrollmentExtract createEnrollmentExtract(PreliminaryEnrollmentStudent preStudent)
    {
        EntrListenerParExtract extract = new EntrListenerParExtract();
        extract.setParallel(_parallel);
        return extract;
    }

    // Getters & Setters

    public ISelectModel getParallelListModel()
    {
        return _parallelListModel;
    }

    public void setParallelListModel(ISelectModel parallelListModel)
    {
        _parallelListModel = parallelListModel;
    }

    public EducationLevelsHighSchool getParallel()
    {
        return _parallel;
    }

    public void setParallel(EducationLevelsHighSchool parallel)
    {
        _parallel = parallel;
    }
}
