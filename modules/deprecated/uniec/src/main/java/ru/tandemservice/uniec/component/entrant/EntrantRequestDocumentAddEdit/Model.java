/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.EntrantRequestDocumentAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uniec.entity.catalog.EnrollmentDocument;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;

/**
 * @author vip_delete
 * @since 06.06.2009
 */
@Input({
        @Bind(key = "entrantRequestId", binding = "entrantRequest.id"),
        @Bind(key = "addForm", binding = "addForm")
})
public class Model
{
    public static final String BLOCK_COLUMN_AMOUNT = "amount";
    public static final String BLOCK_COLUMN_COPY = "copy";

    private EntrantRequest _entrantRequest = new EntrantRequest();
    private boolean _addForm;
    private DynamicListDataSource<EnrollmentDocument> _dataSource;

    public EntrantRequest getEntrantRequest()
    {
        return _entrantRequest;
    }

    public void setEntrantRequest(EntrantRequest entrantRequest)
    {
        _entrantRequest = entrantRequest;
    }

    public boolean isAddForm()
    {
        return _addForm;
    }

    public void setAddForm(boolean addForm)
    {
        _addForm = addForm;
    }

    public DynamicListDataSource<EnrollmentDocument> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<EnrollmentDocument> dataSource)
    {
        _dataSource = dataSource;
    }
}
