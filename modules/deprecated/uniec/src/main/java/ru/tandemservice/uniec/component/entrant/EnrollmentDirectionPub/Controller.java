/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EnrollmentDirectionPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.SimpleFormatter;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.IUniecComponents;
import ru.tandemservice.uniec.component.entrant.EntranceDisciplineAddEdit.EntranceDisciplineAddEditModel;
import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline;
import ru.tandemservice.uniec.entity.entrant.ProfileEducationOrgUnit;
import ru.tandemservice.uniec.entity.settings.SetDiscipline;

import java.util.HashMap;

/**
 * @author Боба
 * @since 30.07.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    @SuppressWarnings({"unchecked"})
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        model.setDataSource(getDataSource(component, model));

        model.setDataSourceMap(new HashMap<>());

        for (StudentCategory studentCategory : model.getStudentCategoryList())
            model.getDataSourceMap().put(studentCategory, getDataSource(component, model));

        prepareProfileDataSource(component);
    }

    @SuppressWarnings({"unchecked"})
    private DynamicListDataSource getDataSource(IBusinessComponent component, Model model)
    {
        DynamicListDataSource<EntranceDiscipline> dataSource = UniBaseUtils.createDataSource(component, getDao());

        dataSource.addColumn(new SimpleColumn("Вступительное испытание", new String[]{EntranceDiscipline.L_DISCIPLINE, SetDiscipline.P_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Приоритет ВИ", EntranceDiscipline.priority().s(), SimpleFormatter.INSTANCE).setClickable(false).setOrderable(false));
        if (model.getEnrollmentDirection().getEnrollmentCampaign().isEnrollmentPerCompTypeDiff())
        {
            dataSource.addColumn(new SimpleColumn("Бюджет", EntranceDiscipline.P_BUDGET, YesNoFormatter.INSTANCE).setClickable(false).setOrderable(false));
            dataSource.addColumn(new SimpleColumn("Контракт", EntranceDiscipline.P_CONTRACT, YesNoFormatter.INSTANCE).setClickable(false).setOrderable(false));
        }
        dataSource.addColumn(new SimpleColumn("Тип вступительного испытания", EntranceDiscipline.type().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Вид вступительного испытания", EntranceDiscipline.kind().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Испытание по выбору", EntranceDiscipline.L_CHOICABLE_DISCIPLINES, CollectionFormatter.COLLECTION_FORMATTER).setClickable(false).setOrderable(false));
        if(model.getEnrollmentDirection().getEnrollmentCampaign().isEnrollmentPerCompTypeDiff())
        {
            dataSource.addColumn(new SimpleColumn("Зачетный балл на бюджет", EntranceDiscipline.passMark().s(), DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS).setClickable(false).setOrderable(false));
            dataSource.addColumn(new SimpleColumn("Зачетный балл на контракт", EntranceDiscipline.passMarkContract().s(), DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS).setClickable(false).setOrderable(false));
        }
        else
        {
            dataSource.addColumn(new SimpleColumn("Зачетный балл", EntranceDiscipline.passMark().s(), DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS).setClickable(false).setOrderable(false));
        }
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditEntranceDiscipline").setPermissionKey("editEntranceDiscipline"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteEntranceDiscipline", "Удалить вступительное мероприятие «{0}» из набора?", new Object[]{new String[]{EntranceDiscipline.L_DISCIPLINE, SetDiscipline.P_TITLE}}).setPermissionKey("deleteEntranceDiscipline"));

        return dataSource;
    }

    public void prepareProfileDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getProfileDataSource() != null) return;

        DynamicListDataSource<ProfileEducationOrgUnit> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareProfileDataSource(getModel(component1));
        });

        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", ProfileEducationOrgUnit.educationOrgUnit().educationLevelHighSchool().fullTitle()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Формирующее подр.", ProfileEducationOrgUnit.educationOrgUnit().formativeOrgUnit().shortTitle()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Территориальное подр.", ProfileEducationOrgUnit.educationOrgUnit().territorialOrgUnit().territorialShortTitle()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Форма освоения", ProfileEducationOrgUnit.educationOrgUnit().developForm().title()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Условие освоения", ProfileEducationOrgUnit.educationOrgUnit().developCondition().title()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Технология освоения", ProfileEducationOrgUnit.educationOrgUnit().developTech().title()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Срок", ProfileEducationOrgUnit.educationOrgUnit().developPeriod().title()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Б", ProfileEducationOrgUnit.budgetPlan()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("К", ProfileEducationOrgUnit.contractPlan()).setOrderable(false));

        model.setProfileDataSource(dataSource);
    }

    public void onClickEdit(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniecComponents.ENROLLMENT_DIRECTION_EDIT, new ParametersMap()
                .add("enrollmentDirectionId", getModel(component).getEnrollmentDirection().getId())
        ));
    }

    public void onClickAddEntranceDiscipline(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniecComponents.ENTRANCE_DISCIPLINE_ADD_EDIT, new ParametersMap()
                .add(EntranceDisciplineAddEditModel.ENTRANCE_DISCIPLINE_ID, null)
                .add(EntranceDisciplineAddEditModel.ENROLLMENT_DIRECTION_ID, getModel(component).getEnrollmentDirection().getId())
        ));
    }

    public void onClickDeleteEntranceDiscipline(IBusinessComponent component)
    {
        getDao().deleteRow(component);
    }

    public void onClickEditEntranceDiscipline(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniecComponents.ENTRANCE_DISCIPLINE_ADD_EDIT, new ParametersMap()
                .add(EntranceDisciplineAddEditModel.ENTRANCE_DISCIPLINE_ID, component.getListenerParameter())
                .add(EntranceDisciplineAddEditModel.ENROLLMENT_DIRECTION_ID, null)
        ));
    }

    public void onClickChangePrifileEducationOrgUnitList(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(ru.tandemservice.uniec.component.entrant.ProfileEducationOrgUnitEdit.Controller.class.getPackage().getName(), new ParametersMap()
                .add("profileEducationOrgUnitId", null)
                .add("enrollmentDirectionId", getModel(component).getEnrollmentDirection().getId())
        ));
    }
}
