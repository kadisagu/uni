/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.entrant.ExcludePassFormTab;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Vasily Zhukov
 * @since 26.07.2010
 */
public interface IDAO extends IUniDao<Model>
{
}
