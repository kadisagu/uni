/**
 *$Id$
 */
package ru.tandemservice.uniec.base.bo.EcExamGroup.ui.SpecificationTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniec.base.bo.EcExamGroup.EcExamGroupManager;
import ru.tandemservice.uniec.entity.entrant.ExamGroup;
import ru.tandemservice.uniec.entity.entrant.ExamGroupSpecification;

import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 09.07.12
 */
@Input({
        @Bind(key = EcExamGroupSpecificationTabUI.EXAM_GROUP_ID, binding = EcExamGroupSpecificationTabUI.EXAM_GROUP_ID)
})
public class EcExamGroupSpecificationTabUI extends UIPresenter
{
    final public static String EXAM_GROUP_ID = "examGroupId";

    // fields

    private long _examGroupId;
    private ExamGroup _examGroup;

    // Presenter listeners

    @Override
    public void onComponentRefresh()
    {
        _examGroup = DataAccessServices.dao().getNotNull(ExamGroup.class, _examGroupId);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(EcExamGroupSpecificationTab.EXAM_GROUP_SPEC_DS))
            dataSource.put(EXAM_GROUP_ID, _examGroupId);
    }

    // Listeners

    public void onClickApply()
    {
        List<ExamGroupSpecification> records = getConfig().getDataSource(EcExamGroupSpecificationTab.EXAM_GROUP_SPEC_DS).getRecords();

        ErrorCollector errorCollector = EcExamGroupManager.instance().examGroupSpecificationDAO().validate(records);

        if (errorCollector.hasErrors())
            return;

        EcExamGroupManager.instance().examGroupSpecificationDAO().saveOrUpdateEntity(records);
    }

    // Calculare getters

    public String getStartDisciplineDateId()
    {
        return "startDisciplineDateId_" + getConfig().getDataSource(EcExamGroupSpecificationTab.EXAM_GROUP_SPEC_DS).getCurrentId();
    }

    public String getFinishDisciplineDateId()
    {
        return "finishDisciplineDateId_" + getConfig().getDataSource(EcExamGroupSpecificationTab.EXAM_GROUP_SPEC_DS).getCurrentId();
    }

    public String getAddressId()
    {
        return "addressId_" + getConfig().getDataSource(EcExamGroupSpecificationTab.EXAM_GROUP_SPEC_DS).getCurrentId();
    }

    public String getClassroomId()
    {
        return "classroomId_" + getConfig().getDataSource(EcExamGroupSpecificationTab.EXAM_GROUP_SPEC_DS).getCurrentId();
    }

    public String getExamCommissionMembershipId()
    {
        return "examCommissionMembershipId_" + getConfig().getDataSource(EcExamGroupSpecificationTab.EXAM_GROUP_SPEC_DS).getCurrentId();
    }

    // Getters & Setters

    public long getExamGroupId()
    {
        return _examGroupId;
    }

    public void setExamGroupId(long examGroupId)
    {
        _examGroupId = examGroupId;
    }

    public ExamGroup getExamGroup()
    {
        return _examGroup;
    }

    public void setExamGroup(ExamGroup examGroup)
    {
        _examGroup = examGroup;
    }
}
