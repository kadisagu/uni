/* $Id$ */
package ru.tandemservice.uniec.catalog.ext.Catalog;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import ru.tandemservice.uniec.entity.catalog.IndividualProgress;

/**
 * @author Ekaterina Zvereva
 * @since 31.03.2015
 */
@Configuration
public class CatalogExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private CatalogManager _catalogManager;

    @Bean
    public ItemListExtension<IDynamicCatalogDesc> dynamicCatalogsExtension()
    {
        return itemListExtension(_catalogManager.dynamicCatalogsExtPoint())
                .add(StringUtils.uncapitalize(IndividualProgress.class.getSimpleName()), IndividualProgress.getUiDesc())
                .create();
    }
}