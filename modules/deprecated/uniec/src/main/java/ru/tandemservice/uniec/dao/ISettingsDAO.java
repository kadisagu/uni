/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.dao;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.EnrollmentOrderBasic;
import ru.tandemservice.uniec.entity.catalog.EnrollmentOrderReason;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.entity.settings.DisciplinesGroup;

/**
 * @author agolubenko
 * @since 20.06.2008
 */
public interface ISettingsDAO
{
    /**
     * @param group группа дисциплин
     * @return список дисциплин в данной группе
     */
    List<Discipline2RealizationWayRelation> getDisciplines(DisciplinesGroup group);

    /**
     * Возвращает список дисциплин для каждой группы
     * 
     * @param groups группы дисциплин
     * @return группа -> дисциплины
     */
    //Map<DisciplinesGroup, List<Discipline2RealizationWayRelation>> getGroups2Disciplines(Collection<DisciplinesGroup> groups);

    /**
     * @param enrollmentDirection направление приема
     * @return профильные дисциплины для данного направления
     */
    Set<Discipline2RealizationWayRelation> getProfileDisciplines(EnrollmentDirection enrollmentDirection);

    /**
     * @param enrollmentCampaign приемная кампания
     * @return приоритеты видов конкурса в рамках этой кампании
     */
    Map<CompetitionKind, Integer> getCompetitionKindPriorities(EnrollmentCampaign enrollmentCampaign);

    /**
     * @param enrollmentCampaign приемная кампания
     * @return приоритеты видов конкурса в рамках этой кампании
     */
    List<CompetitionKind> getCompetitionKindPrioritizedList(EnrollmentCampaign enrollmentCampaign);


    /**
     * @param orgUnit орг.юнит
     * @return true если виден таб "Абитуриенты", false в противном случае
     */
    boolean isEntrantListTabVisible(OrgUnit orgUnit);
    /**
     * @param enrollmentOrderReason причина для приказа о зачислении
     * @return список оснований для данной причины
     */
    List<EnrollmentOrderBasic> getReasonToBasicsList(EnrollmentOrderReason enrollmentOrderReason);
    /**
     * @param order приказ о зачислении
     * @return основание, если оно есть иначе null
     */
    public EnrollmentOrderBasic getBasic(EnrollmentOrder order);
}
