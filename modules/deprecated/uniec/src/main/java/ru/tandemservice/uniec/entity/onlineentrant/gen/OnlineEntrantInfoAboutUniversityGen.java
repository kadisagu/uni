package ru.tandemservice.uniec.entity.onlineentrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.catalog.SourceInfoAboutUniversity;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrantInfoAboutUniversity;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выбранный источник информации об ОУ (онлайн-регистрация)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OnlineEntrantInfoAboutUniversityGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrantInfoAboutUniversity";
    public static final String ENTITY_NAME = "onlineEntrantInfoAboutUniversity";
    public static final int VERSION_HASH = -1276230098;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT = "entrant";
    public static final String L_SOURCE_INFO = "sourceInfo";

    private OnlineEntrant _entrant;     // Онлайн-абитуриент
    private SourceInfoAboutUniversity _sourceInfo;     // Источники информации об ОУ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Онлайн-абитуриент. Свойство не может быть null.
     */
    @NotNull
    public OnlineEntrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant Онлайн-абитуриент. Свойство не может быть null.
     */
    public void setEntrant(OnlineEntrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Источники информации об ОУ. Свойство не может быть null.
     */
    @NotNull
    public SourceInfoAboutUniversity getSourceInfo()
    {
        return _sourceInfo;
    }

    /**
     * @param sourceInfo Источники информации об ОУ. Свойство не может быть null.
     */
    public void setSourceInfo(SourceInfoAboutUniversity sourceInfo)
    {
        dirty(_sourceInfo, sourceInfo);
        _sourceInfo = sourceInfo;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof OnlineEntrantInfoAboutUniversityGen)
        {
            setEntrant(((OnlineEntrantInfoAboutUniversity)another).getEntrant());
            setSourceInfo(((OnlineEntrantInfoAboutUniversity)another).getSourceInfo());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OnlineEntrantInfoAboutUniversityGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OnlineEntrantInfoAboutUniversity.class;
        }

        public T newInstance()
        {
            return (T) new OnlineEntrantInfoAboutUniversity();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entrant":
                    return obj.getEntrant();
                case "sourceInfo":
                    return obj.getSourceInfo();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entrant":
                    obj.setEntrant((OnlineEntrant) value);
                    return;
                case "sourceInfo":
                    obj.setSourceInfo((SourceInfoAboutUniversity) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entrant":
                        return true;
                case "sourceInfo":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entrant":
                    return true;
                case "sourceInfo":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entrant":
                    return OnlineEntrant.class;
                case "sourceInfo":
                    return SourceInfoAboutUniversity.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OnlineEntrantInfoAboutUniversity> _dslPath = new Path<OnlineEntrantInfoAboutUniversity>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OnlineEntrantInfoAboutUniversity");
    }
            

    /**
     * @return Онлайн-абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrantInfoAboutUniversity#getEntrant()
     */
    public static OnlineEntrant.Path<OnlineEntrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Источники информации об ОУ. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrantInfoAboutUniversity#getSourceInfo()
     */
    public static SourceInfoAboutUniversity.Path<SourceInfoAboutUniversity> sourceInfo()
    {
        return _dslPath.sourceInfo();
    }

    public static class Path<E extends OnlineEntrantInfoAboutUniversity> extends EntityPath<E>
    {
        private OnlineEntrant.Path<OnlineEntrant> _entrant;
        private SourceInfoAboutUniversity.Path<SourceInfoAboutUniversity> _sourceInfo;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Онлайн-абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrantInfoAboutUniversity#getEntrant()
     */
        public OnlineEntrant.Path<OnlineEntrant> entrant()
        {
            if(_entrant == null )
                _entrant = new OnlineEntrant.Path<OnlineEntrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Источники информации об ОУ. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrantInfoAboutUniversity#getSourceInfo()
     */
        public SourceInfoAboutUniversity.Path<SourceInfoAboutUniversity> sourceInfo()
        {
            if(_sourceInfo == null )
                _sourceInfo = new SourceInfoAboutUniversity.Path<SourceInfoAboutUniversity>(L_SOURCE_INFO, this);
            return _sourceInfo;
        }

        public Class getEntityClass()
        {
            return OnlineEntrantInfoAboutUniversity.class;
        }

        public String getEntityName()
        {
            return "onlineEntrantInfoAboutUniversity";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
