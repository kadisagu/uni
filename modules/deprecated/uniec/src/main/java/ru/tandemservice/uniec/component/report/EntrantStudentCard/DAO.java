package ru.tandemservice.uniec.component.report.EntrantStudentCard;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.utils.CommonYesNoUIObject;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.ArrayList;
import java.util.Date;

/**
 * @author oleyba
 * @since 04.08.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    @SuppressWarnings("deprecation")
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.setDateFrom(model.getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : model.getEnrollmentCampaign().getStartDate());
        model.setDateTo(CoreDateUtils.getDayLastTimeMoment(new Date()));

        model.setQualificationListModel(new QualificationModel(getSession()));
        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));
        model.setStudentCategoryListModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(StudentCategory.class)));

        model.setFormativeOrgUnitListModel(MultiEnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setTerritorialOrgUnitListModel(MultiEnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setEducationLevelHighSchoolListModel(MultiEnrollmentDirectionUtil.createEducationLevelsHighSchoolModel(model));
        model.setDevelopFormListModel(MultiEnrollmentDirectionUtil.createDevelopFormModel(model));
        model.setDevelopConditionListModel(MultiEnrollmentDirectionUtil.createDevelopConditionModel(model));
        model.setDevelopTechListModel(MultiEnrollmentDirectionUtil.createDevelopTechModel(model));
        model.setDevelopPeriodListModel(MultiEnrollmentDirectionUtil.createDevelopPeriodModel(model));

        model.setStudentCategoryList(new ArrayList<>());
        model.setQualificationList(new ArrayList<>());
        model.setFormativeOrgUnitList(new ArrayList<>());
        model.setTerritorialOrgUnitList(new ArrayList<>());
        model.setEducationLevelHighSchoolList(new ArrayList<>());
        model.setDevelopFormList(new ArrayList<>());
        model.setDevelopConditionList(new ArrayList<>());
        model.setDevelopTechList(new ArrayList<>());
        model.setDevelopPeriodList(new ArrayList<>());

        model.setParallelList(CommonYesNoUIObject.createYesNoList());

        model.setCustomStateRulesModel(TwinComboDataSourceHandler.getCustomSelectModel("Содержит", "Не содержит"));
        model.setEntrantCustomStateListModel(new LazySimpleSelectModel<>(EntrantCustomStateCI.class));

        model.setParameters(new MultiEnrollmentDirectionUtil.Parameters());
        model.getParameters().setRequired(false, MultiEnrollmentDirectionUtil.Parameters.Filter.values());
    }

    @Override
    public void createReport(Model model)
    {
        model.setContent(new EntrantStudentCardPrintFactory(getSession(), model.getEnrollmentCampaign(), getStudentBuilder(model), "s", "p", "d").getReportContent());
    }

    private MQBuilder getStudentBuilder(Model model)
    {
        MQBuilder builder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "s");
        builder.addJoinFetch("s", PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT, "ou");
        builder.addJoinFetch("s", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, "d");
        builder.addJoinFetch("d", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_PERSON, "p");
        // параметры отчета
        builder.add(MQExpression.eq("d", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
        builder.add(UniMQExpression.betweenDate("d", RequestedEnrollmentDirection.P_REG_DATE, model.getDateFrom(), model.getDateTo()));
        if (model.isCompensTypeActive())
            builder.add(MQExpression.eq("s", PreliminaryEnrollmentStudent.L_COMPENSATION_TYPE, model.getCompensationType()));
        if (model.isStudentCategoryActive())
            builder.add(MQExpression.in("d", PreliminaryEnrollmentStudent.L_STUDENT_CATEGORY, model.getStudentCategoryList()));
        if (model.isQualificationActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL + "." + EducationLevelsHighSchool.L_EDUCATION_LEVEL + "." + EducationLevels.L_QUALIFICATION, model.getQualificationList()));
        if (model.isDevelopFormActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_DEVELOP_FORM, model.getDevelopFormList()));
        if (model.isDevelopConditionActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_DEVELOP_CONDITION, model.getDevelopConditionList()));
        if (model.isDevelopTechActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_DEVELOP_TECH, model.getDevelopTechList()));
        if (model.isDevelopPeriodActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_DEVELOP_PERIOD, model.getDevelopPeriodList()));
        if (model.isFormativeOrgUnitActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnitList()));
        if (model.isTerritorialOrgUnitActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnitList()));
        if (model.isEducationLevelHighSchoolActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, model.getEducationLevelHighSchoolList()));
        if (model.isParallelActive() && model.getParallel().isTrue())
            builder.add(MQExpression.eq("s", PreliminaryEnrollmentStudent.P_PARALLEL, Boolean.FALSE));

        // не архивные
        builder.add(MQExpression.eq("d", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, Boolean.FALSE));

        if (model.getCustomStateRuleValue() != null)
        {
            AbstractExpression expression4EntrantCustomState = EntrantCustomState.getExistsExpression4EntrantCustomState(
                    "d", RequestedEnrollmentDirection.entrantRequest().entrant().s(),
                    model.getCustomStateRuleValue(), model.getEntrantCustomStateList(), new Date());
            if (expression4EntrantCustomState != null)
                builder.add(expression4EntrantCustomState);
        }

        // сортировка
        builder.addOrder("ou", EducationOrgUnit.L_FORMATIVE_ORG_UNIT + ".title");
        builder.addLeftJoin("ou", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, "terr");
        builder.addOrder("terr", "title");
        builder.addOrder("s", PreliminaryEnrollmentStudent.L_COMPENSATION_TYPE + ".title");
        builder.addOrder("ou", EducationOrgUnit.L_DEVELOP_FORM + ".title");
        builder.addOrder("p", Person.L_IDENTITY_CARD + "." + IdentityCard.P_LAST_NAME);
        builder.addOrder("p", Person.L_IDENTITY_CARD + "." + IdentityCard.P_FIRST_NAME);
        builder.addOrder("p", Person.L_IDENTITY_CARD + "." + IdentityCard.P_MIDDLE_NAME);
        return builder;
    }
}
