/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantRequestTab;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Session;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.base.bo.EcEntrant.EcEntrantManager;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.util.EntrantDataUtil;

import java.util.List;

/**
 * @author agolubenko
 * @since 16.05.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        Entrant entrant = getNotNull(Entrant.class, model.getEntrant().getId());

        model.setEntrant(entrant);
        model.setOneDirectionPerRequest(false); // сейчас такого ограничения не существует

        // список ВНП, у которых есть профили без приоритетов 
        Number number = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "r")
                .where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().entrant().id().fromAlias("r")), DQLExpressions.value(entrant.getId())))
                .where(DQLExpressions.exists(new DQLSelectBuilder().fromEntity(ProfileEducationOrgUnit.class, "p")
                        .where(DQLExpressions.eq(DQLExpressions.property(ProfileEducationOrgUnit.enrollmentDirection().fromAlias("p")), DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().fromAlias("r"))))
                        .where(DQLExpressions.notIn(DQLExpressions.property(ProfileEducationOrgUnit.id().fromAlias("p")), new DQLSelectBuilder().fromEntity(PriorityProfileEduOu.class, "pr")
                                .column(DQLExpressions.property(PriorityProfileEduOu.profileEducationOrgUnit().id().fromAlias("pr")))
                                .where(DQLExpressions.eq(DQLExpressions.property(PriorityProfileEduOu.requestedEnrollmentDirection().fromAlias("pr")), DQLExpressions.property(RequestedEnrollmentDirection.id().fromAlias("r"))))
                                .buildQuery()))
                        .buildQuery())).createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();

        model.setHasEmptyPriorityProfileEduOu(number != null && number.intValue() > 0);
    }

    protected List<EntrantRequest> getEntrantRequestList(Entrant entrant)
    {
        MQBuilder requestBuilder = new MQBuilder(EntrantRequest.ENTITY_CLASS, "r");
        requestBuilder.add(MQExpression.eq("r", EntrantRequest.L_ENTRANT, entrant));
        requestBuilder.addOrder("r", EntrantRequest.P_REG_DATE);
        return requestBuilder.getResultList(getSession());
    }

    @Override
    public void prepareEntrantRequestList(Model model)
    {
        final Session session = getSession();

        model.getOriginalHandedInByRequest().clear();

        List<EntrantRequest> entrantRequestList = getEntrantRequestList(model.getEntrant());
        model.setEntrantRequestList(entrantRequestList);
        for (EntrantRequest request : entrantRequestList)
            model.getOriginalHandedInByRequest().put(request, Boolean.FALSE);

        model.setUpdateOnChangeColumns(UniStringUtils.join(entrantRequestList, EntrantRequest.P_ID, ","));

        // заполняем пометки о сдаче оригиналов для заявлений
        MQBuilder directionBuilder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "d", new String[]{RequestedEnrollmentDirection.L_ENTRANT_REQUEST});
        directionBuilder.add(MQExpression.eq("d", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT, model.getEntrant()));
        directionBuilder.add(MQExpression.eq("d", RequestedEnrollmentDirection.P_ORIGINAL_DOCUMENT_HANDED_IN, Boolean.TRUE));
        directionBuilder.setNeedDistinct(true);
        List<EntrantRequest> list = directionBuilder.getResultList(session);
        for (EntrantRequest request : list)
            model.getOriginalHandedInByRequest().put(request, Boolean.TRUE);

        // заполняем не актуальные направления
        model.getNonActualDirectionSet().clear();
        directionBuilder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "d");
        directionBuilder.add(MQExpression.eq("d", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT, model.getEntrant()));
        directionBuilder.add(MQExpression.in("d", RequestedEnrollmentDirection.L_COMPETITION_KIND + "." + CompetitionKind.P_CODE, UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION, UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION));
        EntrantDataUtil dataUtil = new EntrantDataUtil(getSession(), model.getEntrant().getEnrollmentCampaign(), directionBuilder, EntrantDataUtil.DetailLevel.EXAM_PASS);
        for (RequestedEnrollmentDirection direction : dataUtil.getDirectionSet())
        {
            if (CollectionUtils.isEmpty(dataUtil.getChosenEntranceDisciplineSet(direction)))
            {
                model.getNonActualDirectionSet().add(direction);
            }
        }
    }

    @Override
    public void prepareRequestedEnrollmentDirectionDataSource(Model model, EntrantRequest request)
    {
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "d");
        builder.add(MQExpression.eq("d", RequestedEnrollmentDirection.L_ENTRANT_REQUEST, request));
        builder.addOrder("d", RequestedEnrollmentDirection.P_PRIORITY, OrderDirection.asc);
        List<RequestedEnrollmentDirection> list = builder.getResultList(getSession());
        DynamicListDataSource<RequestedEnrollmentDirection> dataSource = model.getRequestedEnrollmentDirectionDataSourceByRequest().get(request);
        dataSource.setCountRow(list.size());
        UniBaseUtils.createPage(dataSource, list);
    }

    @Override
    public void updateDocumentsTakeAway(Long requestId)
    {
        final Session session = getSession();

        EntrantRequest entrantRequest = getNotNull(EntrantRequest.class, requestId);
        MQBuilder builder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "p");
        builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION + "." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST, entrantRequest));
        if (builder.getResultCount(session) > 0)
            throw new ApplicationException("Невозможно установить состояние «Забрал документы», абитуриент " + entrantRequest.getEntrant().getPerson().getFullFio() + " уже предварительно зачислен.");

        entrantRequest.setTakeAwayDocument(Boolean.TRUE);
        session.update(entrantRequest);
    }

    @Override
    public void updateDocumentsReturn(Long requestId)
    {
        final Session session = getSession();
        EntrantRequest entrantRequest = getNotNull(EntrantRequest.class, requestId);
        entrantRequest.setTakeAwayDocument(Boolean.FALSE);
        session.update(entrantRequest);
    }

    @Override
    public void updateOriginalsTakeAway(Model model, Long directionId)
    {
        RequestedEnrollmentDirection direction = getNotNull(RequestedEnrollmentDirection.class, directionId);

        EcEntrantManager.instance().dao().updateOriginalDocuments(direction, false);
    }

    @Override
    public void updateOriginalsReturn(Model model, Long directionId)
    {
        RequestedEnrollmentDirection direction = getNotNull(RequestedEnrollmentDirection.class, directionId);

        EcEntrantManager.instance().dao().updateOriginalDocuments(direction, true);
    }

    @Override
    public void executeIncludeIntoExamGroup(Long entrantRequestId)
    {
        EntrantRequest entrantRequest = getNotNull(EntrantRequest.class, entrantRequestId);

        final Session session = getSession();

        List<ExamGroup> examGroupList = UniecDAOFacade.getExamGroupSetDao().getPossibleExamGroups(entrantRequest);

        if (examGroupList.isEmpty())
            throw new ApplicationException(entrantRequest.getTitle() + " уже нельзя включить ни в одну экзаменационную группу.");

        for (ExamGroup examGroup : examGroupList)
        {
            // экзам. группа еще не сохранена в базе
            if (examGroup.getId() == null)
                session.save(examGroup);

            // включаем заявление в уже сохраненную группу examGroup

            ExamGroupRow examGroupRow = new ExamGroupRow();
            examGroupRow.setEntrantRequest(entrantRequest);
            examGroupRow.setExamGroup(examGroup);
            examGroupRow.setIndex(getNextExamGroupRowIndex(session, examGroup));

            session.save(examGroupRow);

            List<Discipline2RealizationWayRelation> alreadyCreateDiscList = new DQLSelectBuilder().fromEntity(ExamGroupSpecification.class, "s").column(DQLExpressions.property(ExamGroupSpecification.discipline().fromAlias("s")))
                        .where(DQLExpressions.eqValue(DQLExpressions.property(ExamGroupSpecification.examGroup().fromAlias("s")), examGroup))
                    .createStatement(getSession()).list();

            List<Discipline2RealizationWayRelation> disciplineList = new DQLSelectBuilder().fromEntity(Discipline2RealizationWayRelation.class, "b").column("b")
                    .where(DQLExpressions.eqValue(DQLExpressions.property(Discipline2RealizationWayRelation.enrollmentCampaign().fromAlias("b")), entrantRequest.getEntrant().getEnrollmentCampaign()))
                    .createStatement(getSession()).list();

            for (Discipline2RealizationWayRelation discipline : disciplineList)
            {
                if (alreadyCreateDiscList.contains(discipline))
                    continue;

                ExamGroupSpecification specification = new ExamGroupSpecification();
                specification.setExamGroup(examGroup);
                specification.setDiscipline(discipline);

                save(specification);
            }
        }
    }

    @Override
    public boolean isAllowIncludeIntoExamGroup(EntrantRequest entrantRequest)
    {
        return !UniecDAOFacade.getExamGroupSetDao().getPossibleExamGroups(entrantRequest).isEmpty();
    }

    @Override
    public boolean isExamGroupInvalid(EntrantRequest entrantRequest)
    {
        List<ExamGroup> examGroupList = getEntrantRequestGroup(entrantRequest);
        if (examGroupList.isEmpty()) return false;

        MQBuilder builder = new MQBuilder(ExamGroupRow.ENTITY_CLASS, "r");
        builder.add(MQExpression.in("r", ExamGroupRow.L_EXAM_GROUP, examGroupList));
        builder.add(MQExpression.eq("r", ExamGroupRow.L_ENTRANT_REQUEST, entrantRequest));
        List<ExamGroupRow> rowList = builder.getResultList(getSession());

        int i = 0;
        while (i < rowList.size() && UniecDAOFacade.getExamGroupSetDao().isExamGroupRowValid(rowList.get(i))) i++;
        return i < rowList.size();
    }

    @Override
    public List<ExamGroup> getEntrantRequestGroup(EntrantRequest entrantRequest)
    {
        return UniecDAOFacade.getExamGroupSetDao().getEntrantRequestGroups(entrantRequest);
    }

    // util methods

    private static int getNextExamGroupRowIndex(Session session, ExamGroup examGroup)
    {
        MQBuilder builder = new MQBuilder(ExamGroupRow.ENTITY_CLASS, "r");
        builder.add(MQExpression.eq("r", ExamGroupRow.L_EXAM_GROUP, examGroup));
        builder.addOrder("r", ExamGroupRow.P_INDEX, OrderDirection.desc);
        List<ExamGroupRow> list = builder.getResultList(session, 0, 1);
        return list.size() == 0 ? 1 : list.get(0).getIndex() + 1;
    }

    @Override
    public void setAgree4Enrollment(Long requestedDirectionId, boolean value)
    {
        RequestedEnrollmentDirection direction = getNotNull(RequestedEnrollmentDirection.class, requestedDirectionId);
        direction.setAgree4Enrollment(value);
        update(direction);
    }
}