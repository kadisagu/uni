/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.report.TechnicCommissionReport.Add;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import org.tandemframework.shared.commonbase.utils.CommonYesNoUIObject;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.CompetitionGroup;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.report.TechnicCommissionReport;

/**
 * @author agolubenko
 * @since 13.07.2009
 */
public class Model implements IEnrollmentCampaignSelectModel
{
    public static String ENROLLMENT_CAMPAIGN_STAGE_DOCUMENTS = "по ходу приема документов";
    public static String ENROLLMENT_CAMPAIGN_STAGE_EXAMS = "по результатам сдачи вступительных испытаний";
    public static String ENROLLMENT_CAMPAIGN_STAGE_ENROLLMENT = "по результатам зачисления";

    private TechnicCommissionReport _report = new TechnicCommissionReport();

    private List<EnrollmentCampaign> _enrollmentCampaignList;
    private List<String> _enrollmentCampaignStageList = Arrays.asList(ENROLLMENT_CAMPAIGN_STAGE_DOCUMENTS, ENROLLMENT_CAMPAIGN_STAGE_EXAMS, ENROLLMENT_CAMPAIGN_STAGE_ENROLLMENT);
    private List<CompetitionGroup> _competitionGroupList;
    private List<CompensationType> _compensationTypeList;

    private boolean _studentCategoryActive;
    private StudentCategory _studentCategory;
    private ISelectModel _studentCategoryModel;
    private List<StudentCategory> _studentCategoryList = new ArrayList<StudentCategory>();

    private boolean _technicCommissionActive;
    private List<IdentifiableWrapper<?>> _technicCommissionList = new ArrayList<IdentifiableWrapper<?>>();
    private ISelectModel _technicCommissionModel;

    private boolean _dateFromActive;
    private boolean _dateToActive;

    private String _competitionGroupName;
    private CompetitionGroup _competitionGroup;

    private boolean _parallelActive;
    private List<CommonYesNoUIObject> _parallelList;
    private CommonYesNoUIObject _parallel;

    public TechnicCommissionReport getReport()
    {
        return _report;
    }

    public void setReport(TechnicCommissionReport report)
    {
        _report = report;
    }

    @Override
    public IDataSettings getSettings()
    {
        return null;
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return getReport().getEnrollmentCampaign();
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        getReport().setEnrollmentCampaign(enrollmentCampaign);
    }

    public List<StudentCategory> getSelectedStudentCategoryList()
    {
        return (getEnrollmentCampaign().isExamSetDiff()) ? Collections.singletonList(getStudentCategory()) : getStudentCategoryList();
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public List<String> getEnrollmentCampaignStageList()
    {
        return _enrollmentCampaignStageList;
    }

    public void setEnrollmentCampaignStageList(List<String> enrollmentCampaignStageList)
    {
        _enrollmentCampaignStageList = enrollmentCampaignStageList;
    }

    public List<CompetitionGroup> getCompetitionGroupList()
    {
        return _competitionGroupList;
    }

    public void setCompetitionGroupList(List<CompetitionGroup> competitionGroupList)
    {
        _competitionGroupList = competitionGroupList;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public StudentCategory getStudentCategory()
    {
        return _studentCategory;
    }

    public void setStudentCategory(StudentCategory studentCategory)
    {
        _studentCategory = studentCategory;
    }

    public ISelectModel getStudentCategoryModel()
    {
        return _studentCategoryModel;
    }

    public void setStudentCategoryModel(ISelectModel studentCategoryModel)
    {
        _studentCategoryModel = studentCategoryModel;
    }

    public List<StudentCategory> getStudentCategoryList()
    {
        return _studentCategoryList;
    }

    public void setStudentCategoryList(List<StudentCategory> studentCategoryList)
    {
        _studentCategoryList = studentCategoryList;
    }

    public List<IdentifiableWrapper<?>> getTechnicCommissionList()
    {
        return _technicCommissionList;
    }

    public ISelectModel getTechnicCommissionModel()
    {
        return _technicCommissionModel;
    }

    public void setTechnicCommissionList(List<IdentifiableWrapper<?>> technicCommissionList)
    {
        _technicCommissionList = technicCommissionList;
    }

    public void setTechnicCommissionModel(ISelectModel technicCommissionModel)
    {
        _technicCommissionModel = technicCommissionModel;
    }

    public boolean isDateFromActive()
    {
        return _dateFromActive;
    }

    public void setDateFromActive(boolean dateFromActive)
    {
        _dateFromActive = dateFromActive;
    }

    public boolean isDateToActive()
    {
        return _dateToActive;
    }

    public void setDateToActive(boolean dateToActive)
    {
        _dateToActive = dateToActive;
    }

    public String getCompetitionGroupName()
    {
        return _competitionGroupName;
    }

    public void setCompetitionGroupName(String competitionGroupName)
    {
        _competitionGroupName = competitionGroupName;
    }

    public boolean isStudentCategoryActive()
    {
        return _studentCategoryActive;
    }

    public void setStudentCategoryActive(boolean studentCategoryActive)
    {
        _studentCategoryActive = studentCategoryActive;
    }

    public boolean isTechnicCommissionActive()
    {
        return _technicCommissionActive;
    }

    public void setTechnicCommissionActive(boolean technicCommissionActive)
    {
        _technicCommissionActive = technicCommissionActive;
    }

    public boolean isParallelActive()
    {
        return _parallelActive;
    }

    public void setParallelActive(boolean parallelActive)
    {
        _parallelActive = parallelActive;
    }

    public List<CommonYesNoUIObject> getParallelList()
    {
        return _parallelList;
    }

    public void setParallelList(List<CommonYesNoUIObject> parallelList)
    {
        _parallelList = parallelList;
    }

    public CommonYesNoUIObject getParallel()
    {
        return _parallel;
    }

    public void setParallel(CommonYesNoUIObject parallel)
    {
        _parallel = parallel;
    }

    public CompetitionGroup getCompetitionGroup()
    {
        return _competitionGroup;
    }

    public void setCompetitionGroup(CompetitionGroup competitionGroup)
    {
        _competitionGroup = competitionGroup;
    }
}
