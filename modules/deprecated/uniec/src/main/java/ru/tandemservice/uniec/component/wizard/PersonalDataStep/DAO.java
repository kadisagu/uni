/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.wizard.PersonalDataStep;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.bo.util.CountryAutocompleteModel;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.person.base.bo.Person.util.WorkPlaceSelectModel;
import org.tandemframework.shared.person.base.bo.Person.util.WorkPostSelectModel;
import org.tandemframework.shared.person.base.entity.*;
import org.tandemframework.shared.person.catalog.entity.*;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;

import java.util.List;

/**
 * @author vip_delete
 * @since 15.06.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setEntrant(getNotNull(Entrant.class, model.getEntrant().getId()));
        Person person = model.getEntrant().getPerson();

        // льготы
        MQBuilder builder = new MQBuilder(Benefit.ENTITY_CLASS, "b");
        builder.add(MQExpression.notIn("b", "id",
                new MQBuilder(NotUsedBenefit.ENTITY_CLASS, "n", new String[]{
                        NotUsedBenefit.L_BENEFIT + ".id"})
                .add(MQExpression.eq("n", NotUsedBenefit.P_PERSON_ROLE_NAME, "Entrant"))));
        builder.addOrder("b", ICatalogItem.CATALOG_ITEM_TITLE);
        model.setBenefitList(builder.<Benefit>getResultList(getSession()));
        model.setSelectedBenefitList(getList(PersonBenefit.class, PersonBenefit.L_PERSON, person));
        model.setHasBenefit(!model.getSelectedBenefitList().isEmpty());

        // иностранные языки
        model.setLanguageModel(new LazySimpleSelectModel<>(ForeignLanguage.class));
        model.setSkillList(getCatalogItemList(ForeignLanguageSkill.class));
        model.setSelectedLanguageList(getList(PersonForeignLanguage.class, PersonForeignLanguage.L_PERSON, person));
        model.getForeignLanguage().setLanguage(getCatalogItem(ForeignLanguage.class, UniDefines.CATALOG_FOREIGN_LANGUAGE_ENGLISH));
        model.setHasLanguage(!model.getSelectedLanguageList().isEmpty());

        // спортивные достижения
        model.setSportRankList(getCatalogItemListOrderByCode(SportRank.class));
        model.setSportTypeModel(new LazySimpleSelectModel<>(getCatalogItemList(SportType.class)).setSearchFromStart(false));
        model.setSelectedSportList(getList(PersonSportAchievement.class, PersonSportAchievement.L_PERSON, person));
        model.setHasSport(!model.getSelectedSportList().isEmpty());

        // сведения о персоне
        model.setFamilyStatusList(getCatalogItemList(FamilyStatus.class));
        model.setPensionTypeList(getCatalogItemList(PensionType.class));
        model.setFlatPresenceList(getCatalogItemList(FlatPresence.class));
        model.setWorkPlaceModel(new WorkPlaceSelectModel());
        model.setWorkPostModel(new WorkPostSelectModel());
        
        // льготы для поселения в общежитие
        model.setDormitoryBenefitList(getCatalogItemList(DormitoryBenefit.class));
        List<PersonDormitoryBenefit> personDormitoryBenefits = getList(PersonDormitoryBenefit.class, PersonDormitoryBenefit.person().s(), person);
        if (!personDormitoryBenefits.isEmpty())
        {
            model.setHasDormitoryBenefits(true);
            model.setPersonDormitoryBenefitList(personDormitoryBenefits);
        }
        
        // сведения о воинском учете
        List<PersonMilitaryStatus> personMilitaryStatuses = getList(PersonMilitaryStatus.class, PersonMilitaryStatus.L_PERSON, model.getEntrant().getPerson());
        if (!personMilitaryStatuses.isEmpty())
        {
            model.setMilitaryStatus(personMilitaryStatuses.get(0));
        }
        model.setMilitaryRegDataListModel(new LazySimpleSelectModel<>(MilitaryRegData.class));
        model.setMilitaryAbilityStatusListModel(new LazySimpleSelectModel<>(MilitaryAbilityStatus.class));
        model.setCountry(get(AddressCountry.class, AddressCountry.code().s(), IKladrDefines.RUSSIA_COUNTRY_CODE));
        model.setCountryListModel(new CountryAutocompleteModel());
        model.setMilitaryOfficeListModel(new BaseSingleSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(MilitaryOffice.ENTITY_CLASS, "o");
                if (null != model.getCountry())
                    builder.add(MQExpression.eq("o", MilitaryOffice.settlement().country().s(), model.getCountry()));
                filter = StringUtils.trimToNull(filter);
                if (null != filter)
                    builder.add(MQExpression.like("o", MilitaryOffice.title().s(), "%" + filter + "%"));
                return new ListResult<>(builder.<MilitaryOffice>getResultList(getSession(), 0, 20), builder.getResultCount(getSession()));
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                MQBuilder builder = new MQBuilder(MilitaryOffice.ENTITY_CLASS, "o");
                if (null != model.getCountry())
                    builder.add(MQExpression.eq("o", MilitaryOffice.settlement().country().s(), model.getCountry()));
                builder.add(MQExpression.eq("o", MilitaryOffice.id().s(), primaryKey));
                return builder.uniqueResult(getSession());
            }
        });
        model.setMilitaryRankListModel(new LazySimpleSelectModel<>(MilitaryRank.class));
        
        // онлайн-абитуриент
        Long onlineEntrantId = model.getOnlineEntrant().getId();
        if (onlineEntrantId != null)
        {
            OnlineEntrant onlineEntrant = get(OnlineEntrant.class, onlineEntrantId);
            model.setOnlineEntrant(onlineEntrant);
            
            // персона
            person.setChildCount(onlineEntrant.getChildCount());
            person.setFamilyStatus(onlineEntrant.getFamilyStatus());
            person.setWorkPlace(onlineEntrant.getWorkPlace());
            person.setWorkPlacePosition(onlineEntrant.getWorkPost());

            // льгота
            Benefit benefit = onlineEntrant.getBenefit();
            if (benefit != null)
            {
                model.setHasBenefit(true);
                
                PersonBenefit personBenefit = model.getPersonBenefit();
                personBenefit.setBenefit(benefit);
                personBenefit.setDocument(onlineEntrant.getBenefitDocument());
                personBenefit.setBasic(onlineEntrant.getBenefitBasic());
                personBenefit.setDate(onlineEntrant.getBenefitDate());
                prepareSelectBenefit(model);
            }
            
            // иностранный язык
            ForeignLanguage foreignLanguage = onlineEntrant.getForeignLanguage();
            if (foreignLanguage != null)
            {
                model.setHasLanguage(true);
                
                PersonForeignLanguage personForeignLanguage = model.getForeignLanguage();
                personForeignLanguage.setLanguage(foreignLanguage);
                personForeignLanguage.setMain(true);
                prepareSelectLanguage(model);
            }
            
            // спортивное достижение
            SportType sportType = onlineEntrant.getSportType();
            if (sportType != null)
            {
                model.setHasSport(true);
                
                PersonSportAchievement personSportAchievement = model.getAchievement();
                personSportAchievement.setSportKind(sportType);
                personSportAchievement.setSportRank(onlineEntrant.getSportRank());
                prepareSelectSport(model);
            }

            // Нуждается в общежитии
            person.setNeedDormitory(onlineEntrant.isNeedHotel());
        }
    }

    @Override
    public void prepareSelectBenefit(Model model)
    {
        Long min = 0L;
        for (PersonBenefit item : model.getSelectedBenefitList()) min = Math.min(min, item.getId());

        model.getPersonBenefit().setId(min - 1);
        model.getPersonBenefit().setPerson(model.getEntrant().getPerson());
        model.getSelectedBenefitList().add(model.getPersonBenefit());
        model.setPersonBenefit(new PersonBenefit());
        model.getPersonBenefit().setPerson(model.getEntrant().getPerson());
    }

    @Override
    public void deleteBenefit(Model model, Long personBenefitId)
    {
        List<PersonBenefit> list = model.getSelectedBenefitList();
        int i = 0;
        while (i < list.size() && !list.get(i).getId().equals(personBenefitId)) i++;
        if (i == list.size()) return;
        list.remove(i);
        if (personBenefitId > 0)
        {
            PersonBenefit personBenefit = get(PersonBenefit.class, personBenefitId);
            if (personBenefit != null) getSession().delete(personBenefit);
        }
    }

    @Override
    public void prepareSelectLanguage(Model model)
    {
        Long min = 0L;
        for (PersonForeignLanguage item : model.getSelectedLanguageList()) min = Math.min(min, item.getId());
        model.getForeignLanguage().setId(min - 1);
        model.getForeignLanguage().setPerson(model.getEntrant().getPerson());
        model.getSelectedLanguageList().add(model.getForeignLanguage());
        model.setForeignLanguage(new PersonForeignLanguage());
        model.getForeignLanguage().setPerson(model.getEntrant().getPerson());
    }

    @Override
    public void deleteLanguage(Model model, Long personLanguageId)
    {
        List<PersonForeignLanguage> list = model.getSelectedLanguageList();
        int i = 0;
        while (i < list.size() && !list.get(i).getId().equals(personLanguageId)) i++;
        if (i == list.size()) return;
        list.remove(i);
        if (personLanguageId > 0)
        {
            PersonForeignLanguage foreignLanguage = get(PersonForeignLanguage.class, personLanguageId);
            if (foreignLanguage != null) getSession().delete(foreignLanguage);
        }
    }

    @Override
    public void prepareSelectSport(Model model)
    {
        Long min = 0L;
        for (PersonSportAchievement item : model.getSelectedSportList()) min = Math.min(min, item.getId());
        model.getAchievement().setId(min - 1);
        model.getAchievement().setPerson(model.getEntrant().getPerson());
        model.getSelectedSportList().add(model.getAchievement());
        model.setAchievement(new PersonSportAchievement());
        model.getAchievement().setPerson(model.getEntrant().getPerson());
    }

    @Override
    public void deleteSport(Model model, Long personSportId)
    {
        List<PersonSportAchievement> list = model.getSelectedSportList();
        int i = 0;
        while (i < list.size() && !list.get(i).getId().equals(personSportId)) i++;
        if (i == list.size()) return;
        list.remove(i);
        if (personSportId > 0)
        {
            PersonSportAchievement achievement = get(PersonSportAchievement.class, personSportId);
            if (achievement != null) getSession().delete(achievement);
        }
    }
    
    @Override
    public void prepareSelectDormitoryBenefit(Model model)
    {
        // находим минимальный id
        long minId = 0;
        for (PersonDormitoryBenefit personDormitoryBenefit : model.getPersonDormitoryBenefitList())
        {
            minId = Math.min(minId, personDormitoryBenefit.getId());
            
            if (personDormitoryBenefit.getBenefit().equals(model.getPersonDormitoryBenefit().getBenefit()))
            {
                throw new ApplicationException("У персоны уже есть льгота такого типа.");
            }
        }

        // добавляем в список
        PersonDormitoryBenefit personDormitoryBenefit = model.getPersonDormitoryBenefit();
        personDormitoryBenefit.setId(minId - 1);
        personDormitoryBenefit.setPerson(model.getEntrant().getPerson());
        model.getPersonDormitoryBenefitList().add(personDormitoryBenefit);

        // обнуляем шаблон
        model.setPersonDormitoryBenefit(new PersonDormitoryBenefit());
    }

    @Override
    public void deleteDormitoryBenefit(Model model, final Long personDormitoryBenefitId)
    {
        // удаляем из списка
        CollectionUtils.filter(model.getPersonDormitoryBenefitList(), object -> !((PersonDormitoryBenefit) object).getId().equals(personDormitoryBenefitId));

        // удаляем объект, если существует
        if (personDormitoryBenefitId > 0)
        {
            PersonDormitoryBenefit personDormitoryBenefit = get(PersonDormitoryBenefit.class, personDormitoryBenefitId);
            if (personDormitoryBenefit != null)
            {
                delete(personDormitoryBenefit);
            }
        }
    }

    @Override
    public void update(Model model)
    {
        ErrorCollector errors = ContextLocal.getErrorCollector(); 
        final Session session = getSession();

        if (model.isHasBenefit() && model.getSelectedBenefitList().isEmpty())
            errors.add("Если у абитуриента есть льготы, то они должны быть указаны.");

        if (model.isHasLanguage() && model.getSelectedLanguageList().isEmpty())
            errors.add("Если абитуриент изучал иностранные языки, то они должны быть указаны.");

        if (model.isHasSport() && model.getSelectedSportList().isEmpty())
            errors.add("Если у абитуриента есть спортивные достижения, то они должны быть указаны.");
        
        if (model.isHasDormitoryBenefits() && model.getPersonDormitoryBenefitList().isEmpty())
        {
            errors.add("Если у абитуриента есть льготы для поселения в общежитие, то они должны быть указаны.");
        }

        checkMilitaryStatus(model, errors);

        if (errors.hasErrors()) return;
        
        // льготы
        // сохраняем льготы с id < 0 (добавленные вручную с формы)
        if (model.isHasBenefit())
            for (PersonBenefit benefit : model.getSelectedBenefitList())
                if (benefit.getId() < 0 && !hasBenefit(session, benefit))
                    session.save(benefit);

        // иностранные языки
        // сохраняем языки с id < 0 (добавленные вручную с формы) и еще отсутствующие у персоны
        if (model.isHasLanguage())
            for (PersonForeignLanguage language : model.getSelectedLanguageList())
                if (language.getId() < 0 && !hasLanguage(session, language))
                    session.save(language);

        // спортивные достижения
        // сохраняем спортивные достижения с id < 0 (добавленные вручную с формы)
        if (model.isHasSport())
            for (PersonSportAchievement achievement : model.getSelectedSportList())
                if (achievement.getId() < 0 && !hasSport(session, achievement))
                    session.save(achievement);
        
        // льготы для поселения в общежитие
        // сохраняем льготы для поселения в общежитие с id < 0 (добавленные вручную с формы)
        if (model.isHasDormitoryBenefits())
            for (PersonDormitoryBenefit dormitoryBenefit : model.getPersonDormitoryBenefitList())
                if (dormitoryBenefit.getId() < 0 && !hasDormitoryBenefit(session, dormitoryBenefit))
                    session.save(dormitoryBenefit);
        
        // сведения о воинском учете
        model.getMilitaryStatus().setPerson(model.getEntrant().getPerson());
        session.saveOrUpdate(model.getMilitaryStatus());
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {

        /*MQBuilder builder = new MQBuilder(PersonBenefit.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", PersonBenefit.L_BENEFIT, model.getPersonBenefit().getBenefit()));
        builder.add(MQExpression.eq("e", PersonBenefit.L_PERSON, get(Person.class, model.getEntrant().getId())));
        if (model.getPersonBenefit().getId() != null)
            builder.add(MQExpression.notEq("e", PersonBenefit.P_ID, model.getPersonBenefit().getId()));
        if (builder.getResultCount(getSession()) > 0)
            errors.add("Такая льгота уже выбрана.", "personBenefit"); */
       // for(Benefit item : model.get)
    }

    protected void checkMilitaryStatus(Model model, ErrorCollector errors)
    {
    }

    private static boolean hasBenefit(Session session, PersonBenefit benefit)
    {
        return new MQBuilder(PersonBenefit.ENTITY_CLASS, "p")
                .add(MQExpression.eq("p", PersonBenefit.L_PERSON, benefit.getPerson()))
                .add(MQExpression.eq("p", PersonBenefit.L_BENEFIT, benefit.getBenefit()))
                .getResultCount(session) > 0;
    }

    private static boolean hasLanguage(Session session, PersonForeignLanguage language)
    {
        return new MQBuilder(PersonForeignLanguage.ENTITY_CLASS, "p")
                .add(MQExpression.eq("p", PersonForeignLanguage.L_PERSON, language.getPerson()))
                .add(MQExpression.eq("p", PersonForeignLanguage.L_LANGUAGE, language.getLanguage()))
                .getResultCount(session) > 0;
    }

    private static boolean hasSport(Session session, PersonSportAchievement achievement)
    {
        return new MQBuilder(PersonSportAchievement.ENTITY_CLASS, "p")
                .add(MQExpression.eq("p", PersonSportAchievement.L_PERSON, achievement.getPerson()))
                .add(MQExpression.eq("p", PersonSportAchievement.L_SPORT_KIND, achievement.getSportKind()))
                .getResultCount(session) > 0;
    }
    
    private static boolean hasDormitoryBenefit(Session session, PersonDormitoryBenefit dormitoryBenefit)
    {
        return new MQBuilder(PersonDormitoryBenefit.ENTITY_CLASS, "p")
                .add(MQExpression.eq("p", PersonDormitoryBenefit.L_PERSON, dormitoryBenefit.getPerson()))
                .add(MQExpression.eq("p", PersonDormitoryBenefit.L_BENEFIT, dormitoryBenefit.getBenefit()))
                .getResultCount(session) > 0;
    }
}
