/* $Id:$ */
package ru.tandemservice.uniec.component.entrant.EntrantDirectionPriorityEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.entity.entrant.Entrant;

/**
 * @author oleyba
 * @since 5/25/12
 */
@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "entrant.id", required = true),
    @Bind(key = "compensationType", binding = "compType.id")
})
public class Model
{
    private Entrant entrant = new Entrant();
    private CompensationType compType = new CompensationType();
    
    private StaticListDataSource<RequestedDirectionPriorityDTO> dataSource;

    public Entrant getEntrant()
    {
        return entrant;
    }

    public void setEntrant(Entrant entrant)
    {
        this.entrant = entrant;
    }

    public CompensationType getCompType()
    {
        return compType;
    }

    public void setCompType(CompensationType compType)
    {
        this.compType = compType;
    }

    public StaticListDataSource<RequestedDirectionPriorityDTO> getDataSource()
    {
        return dataSource;
    }

    public void setDataSource(StaticListDataSource<RequestedDirectionPriorityDTO> dataSource)
    {
        this.dataSource = dataSource;
    }
}
