/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.EntrantEdit;

import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uniec.entity.catalog.AccessCourse;
import ru.tandemservice.uniec.entity.catalog.AccessDepartment;
import ru.tandemservice.uniec.entity.catalog.EnrollmentRecommendation;
import ru.tandemservice.uniec.entity.catalog.SourceInfoAboutUniversity;
import ru.tandemservice.uniec.entity.entrant.Entrant;

/**
 * @author vip_delete
 * @since 05.03.2009
 */
@Input({
        @Bind(key = "entrantId", binding = "entrant.id")
})
public class Model
{
    private Entrant _entrant = new Entrant();
    private ISelectModel _recomendationListModel;
    private List<EnrollmentRecommendation> _recomendationList;
    private ISelectModel _accessCourseListModel;
    private List<AccessCourse> _accessCourseList;
    private ISelectModel _accessDepartmentListModel;
    private List<AccessDepartment> _accessDepartmentList;
    private ISelectModel _sourceInfoAboutUniversityListModel;
    private List<SourceInfoAboutUniversity> _sourceInfoAboutUniversityList;
    private ISelectModel _base4ExamByDifferentSourcesModel;

    public Entrant getEntrant()
    {
        return _entrant;
    }

    public void setEntrant(Entrant entrant)
    {
        _entrant = entrant;
    }

    public ISelectModel getRecomendationListModel()
    {
        return _recomendationListModel;
    }

    public void setRecomendationListModel(ISelectModel recomendationListModel)
    {
        _recomendationListModel = recomendationListModel;
    }

    public List<EnrollmentRecommendation> getRecomendationList()
    {
        return _recomendationList;
    }

    public void setRecomendationList(List<EnrollmentRecommendation> recomendationList)
    {
        _recomendationList = recomendationList;
    }

    public ISelectModel getAccessCourseListModel()
    {
        return _accessCourseListModel;
    }

    public void setAccessCourseListModel(ISelectModel accessCourseListModel)
    {
        _accessCourseListModel = accessCourseListModel;
    }

    public List<AccessCourse> getAccessCourseList()
    {
        return _accessCourseList;
    }

    public void setAccessCourseList(List<AccessCourse> accessCourseList)
    {
        _accessCourseList = accessCourseList;
    }

    public ISelectModel getAccessDepartmentListModel()
    {
        return _accessDepartmentListModel;
    }

    public void setAccessDepartmentListModel(ISelectModel accessDepartmentListModel)
    {
        _accessDepartmentListModel = accessDepartmentListModel;
    }

    public List<AccessDepartment> getAccessDepartmentList()
    {
        return _accessDepartmentList;
    }

    public void setAccessDepartmentList(List<AccessDepartment> accessDepartmentList)
    {
        this._accessDepartmentList = accessDepartmentList;
    }

    public ISelectModel getSourceInfoAboutUniversityListModel()
    {
        return _sourceInfoAboutUniversityListModel;
    }

    public void setSourceInfoAboutUniversityListModel(ISelectModel sourceInfoAboutUniversityListModel)
    {
        _sourceInfoAboutUniversityListModel = sourceInfoAboutUniversityListModel;
    }

    public List<SourceInfoAboutUniversity> getSourceInfoAboutUniversityList()
    {
        return _sourceInfoAboutUniversityList;
    }

    public void setSourceInfoAboutUniversityList(List<SourceInfoAboutUniversity> sourceInfoAboutUniversityList)
    {
        _sourceInfoAboutUniversityList = sourceInfoAboutUniversityList;
    }

    public ISelectModel getBase4ExamByDifferentSourcesModel()
    {
        return _base4ExamByDifferentSourcesModel;
    }

    public void setBase4ExamByDifferentSourcesModel(ISelectModel base4ExamByDifferentSourcesModel)
    {
        _base4ExamByDifferentSourcesModel = base4ExamByDifferentSourcesModel;
    }
}
