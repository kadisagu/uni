/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.wizard;

/**
 * @author vip_delete
 * @since 16.06.2009
 */
public interface IEntrantWizardComponents
{
    String SEARCH_STEP = "ru.tandemservice.uniec.component.wizard.SearchStep";
    String IDENTITY_CARD_STEP = "ru.tandemservice.uniec.component.wizard.IdentityCardStep";
    String PERSON_SIMILAR_STEP = "ru.tandemservice.uniec.component.wizard.PersonSimilarStep";
    String EDU_INSTITUTION_STEP = "ru.tandemservice.uniec.component.wizard.EduInstitutionStep";
    String NEXT_OF_KIN_STEP = "ru.tandemservice.uniec.component.wizard.NextOfKinStep";
    String PERSONAL_DATA_STEP = "ru.tandemservice.uniec.component.wizard.PersonalDataStep";
    String CONTACT_DATA_STEP = "ru.tandemservice.uniec.component.wizard.ContactDataStep";
    String ENTRANT_DATA_STEP = "ru.tandemservice.uniec.component.wizard.EntrantDataStep";
    String STATE_EXAM_DATA_STEP = "ru.tandemservice.uniec.component.wizard.StateExamDataStep";
    String ENTRANT_REQUEST_STEP = "ru.tandemservice.uniec.component.wizard.EntrantRequestStep";
    String ENTRANT_DOCUMENT_STEP = "ru.tandemservice.uniec.component.wizard.EntrantDocumentStep";
}
