package ru.tandemservice.uniec.entity.catalog;

import ru.tandemservice.uniec.entity.catalog.gen.UniecScriptItemGen;

/**
 * Скрипт модуля абитуриент
 */
public class UniecScriptItem extends UniecScriptItemGen
{
    public UniecScriptItem()
    {
        setCatalogCode("uniecScriptItem");
    }
}