/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantStateExamCertificateAddEdit;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec.entity.catalog.StateExamType;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate;
import ru.tandemservice.uniec.entity.entrant.StateExamSubjectMark;
import ru.tandemservice.uniec.entity.settings.StateExamSubjectPassScore;
import ru.tandemservice.uniec.entity.settings.gen.StateExamSubjectPassScoreGen;
import ru.tandemservice.uniec.ui.EntrantStateExamCertificateNumberFormatter;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static ru.tandemservice.uniec.component.entrant.EntrantStateExamCertificateAddEdit.Controller.MARK_COLUMN_NAME;
import static ru.tandemservice.uniec.component.entrant.EntrantStateExamCertificateAddEdit.Controller.YEAR_COLUMN_NAME;

/**
 * @author agolubenko
 * @since 02.03.2009
 */
@SuppressWarnings("unchecked")
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        List<StateExamSubject> stateExamSubjects = getCatalogItemList(StateExamSubject.class);
        stateExamSubjects.sort(CommonCatalogUtil.createCodeComparator(StateExamSubject.P_SUBJECT_C_T_M_O_CODE));
        model.setStateExamSubjects(stateExamSubjects);
        model.setStateExamTypeList(getCatalogItemList(StateExamType.class));

        Map<StateExamSubject, StateExamSubjectMark> markBySubjectMap =
                model.getStateExamSubjects().stream().collect(HashMap::new, (map,sub)->map.put(sub, null), HashMap::putAll);
        if (model.getCertificate().getId() == null)
        {
            // форма добавления, заполняем данные по умолчанию
            model.getCertificate().setEntrant(getNotNull(Entrant.class, model.getCertificate().getEntrant().getId()));
            model.getCertificate().setRegistrationDate(new Date());
            model.getCertificate().setStateExamType(getCatalogItem(StateExamType.class, UniecDefines.STATE_EXAM_TYPE_FIRST));
            model.getCertificate().setAccepted(model.getCertificate().getEntrant().getEnrollmentCampaign().isStateExamAutoCheck());

            model.setTerritoryCode(getRegionCode(model.getCertificate().getEntrant()));
            model.setYear(String.format("%02d", CoreDateUtils.getYear(new Date()) % 100));
        }
        else
        {
            // форма редактирования
            model.setCertificate(getNotNull(EntrantStateExamCertificate.class, model.getCertificate().getId()));

            // разбиваем полный номер свидетельства на части
            String[] numberParts = EntrantStateExamCertificateNumberFormatter.split(model.getCertificate().getNumber());
            model.setTerritoryCode(numberParts[0]);
            model.setNumber(numberParts[1]);
            model.setYear(numberParts[2]);

            getList(StateExamSubjectMark.class, StateExamSubjectMark.L_CERTIFICATE, model.getCertificate())
                    .forEach(mark->markBySubjectMap.put(mark.getSubject(), mark));
        }

        DynamicListDataSource<StateExamSubject> dataSource = model.getDataSource();

        Map<Long, Integer> marks = markBySubjectMap.entrySet().stream()
                .collect(HashMap::new,
                         (map, entry) -> map.put(entry.getKey().getId(), entry.getValue() != null ? entry.getValue().getMark() : null),
                         HashMap::putAll);
        ((BlockColumn) dataSource.getColumn(Controller.MARK_COLUMN_NAME)).setValueMap(marks);

        Map<Long, String> numbers = markBySubjectMap.entrySet().stream()
                .collect(HashMap::new,
                         (map, entry) -> map.put(entry.getKey().getId(), entry.getValue() != null ? entry.getValue().getFormatedCertificateNumber(true) : null),
                         HashMap::putAll);
        ((BlockColumn) dataSource.getColumn(Controller.NUMBER_COLUMN_NAME)).setValueMap(numbers);

        Map<Long, Integer> years = markBySubjectMap.entrySet().stream()
                .collect(HashMap::new,
                         (map, entry) -> map.put(entry.getKey().getId(), entry.getValue() != null ? entry.getValue().getYear() : null),
                         HashMap::putAll);
        ((BlockColumn) dataSource.getColumn(Controller.YEAR_COLUMN_NAME)).setValueMap(years);

        model.setMarkBySubjectMap(markBySubjectMap);
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        model.getDataSource().setCountRow(model.getStateExamSubjects().size());
        UniBaseUtils.createPage(model.getDataSource(), model.getStateExamSubjects());
    }

    @Override
    public void update(Model model)
    {
        ErrorCollector errors = ContextLocal.getErrorCollector();
        EntrantStateExamCertificate certificate = model.getCertificate();

        if (model.isOldStyle())
            oldStyleUpdate(model, errors);
        else
            certificate.setNumber(makeCertificateNumberPlaceHolder(
                    model.getTerritoryCode(), new SimpleDateFormat("yy").format(Calendar.getInstance().get(Calendar.YEAR))));

        validateData(model, errors);
        if (errors != null && errors.hasErrors()) return;

        DynamicListDataSource<StateExamSubject> dataSource = model.getDataSource();
        Map<Long, Integer> marks = ((BlockColumn<Integer>) dataSource.getColumn(Controller.MARK_COLUMN_NAME)).getValueMap();
        Map<Long, String> numbers = ((BlockColumn<String>) dataSource.getColumn(Controller.NUMBER_COLUMN_NAME)).getValueMap();
        Map<Long, Integer> years = ((BlockColumn<Integer>) dataSource.getColumn(Controller.YEAR_COLUMN_NAME)).getValueMap();

        // U P D A T E

        final Session session = getSession();
        session.saveOrUpdate(certificate);

        model.getMarkBySubjectMap().entrySet()
                .forEach(entry -> {
                    StateExamSubject stateExamSubject = entry.getKey();
                    StateExamSubjectMark stateExamSubjectMark = entry.getValue();

                    Integer mark = marks.get(stateExamSubject.getId());
                    if (mark == null)// оценка не указана
                    {
                        if (stateExamSubjectMark != null)
                            session.delete(stateExamSubjectMark);
                    }
                    else // оценка указана
                    {
                        if (stateExamSubjectMark == null)
                            stateExamSubjectMark = new StateExamSubjectMark();

                        stateExamSubjectMark.setSubject(stateExamSubject);
                        stateExamSubjectMark.setMark(mark);
                        stateExamSubjectMark.setCertificate(certificate);
                        stateExamSubjectMark.setFormatedCertificateNumber(numbers.get(stateExamSubject.getId()));
                        stateExamSubjectMark.setYear(years.get(stateExamSubject.getId()));
                        session.saveOrUpdate(stateExamSubjectMark);
                    }
                });
    }

    protected void oldStyleUpdate(Model model, ErrorCollector errors){
        EntrantStateExamCertificate certificate = model.getCertificate();
        String stateExamTypeCode = certificate.getStateExamType().getCode();

        // номер обязателен
        boolean numberRequired = model.getCertificate().getEntrant().getEnrollmentCampaign().isStateExamWave1NumberRequired();

        // номер указан на форме и не нулевой -> его надо валидировать
        boolean numberExists = StringUtils.isNotEmpty(model.getNumber()) && !"000000000".equals(model.getNumber());

        if (UniecDefines.STATE_EXAM_TYPE_FIRST.equals(stateExamTypeCode) && !numberExists && numberRequired)
            errors.add("Для свидетельства ЕГЭ первой волны номер обязателен.", "number");

        if (numberExists && model.getNumber().length() != 9)
            errors.add("Номер свидетельства ЕГЭ должен содержать 9 цифр.", "number");

        if (certificate.getIssuanceDate() != null && certificate.getIssuanceDate().after(new Date()))
            errors.add("Дата выдачи должна быть не позже сегодняшней", "issuanceDate");

        String certificateNumber;
        if (numberExists)
        {
            certificateNumber = model.getTerritoryCode() + model.getNumber() + model.getYear();
            MQBuilder builder = new MQBuilder(EntrantStateExamCertificate.ENTITY_CLASS, "c");
            builder.add(MQExpression.eq("c", EntrantStateExamCertificate.entrant().enrollmentCampaign().s(), certificate.getEntrant().getEnrollmentCampaign()));
            builder.add(MQExpression.eq("c", EntrantStateExamCertificate.P_NUMBER, certificateNumber));
            if (certificate.getId() != null)
                builder.add(MQExpression.notEq("c", EntrantStateExamCertificate.P_ID, certificate.getId()));
            if (builder.getResultCount(getSession()) > 0)
                errors.add("Номер свидетельства ЕГЭ должен быть уникальным в рамках приемной кампании.", "territoryCode", "number", "year");
        } else
        {
            certificateNumber = makeCertificateNumberPlaceHolder(model.getTerritoryCode(), model.getYear());
        }

        certificate.setNumber(certificateNumber);
    }

    protected void validateData(Model model, ErrorCollector errors)
    {
        EntrantStateExamCertificate certificate = model.getCertificate();
        if (certificate.getRegistrationDate() != null && certificate.getRegistrationDate().after(new Date()))
            errors.add("Дата добавления должна быть не позже сегодняшней.", "registrationDate");

        Map<Long, Integer> marks = ((BlockColumn<Integer>) model.getDataSource().getColumn(Controller.MARK_COLUMN_NAME)).getValueMap();
        Map<Long, Integer> years = ((BlockColumn<Integer>) model.getDataSource().getColumn(Controller.YEAR_COLUMN_NAME)).getValueMap();

        if (marks.values().stream().noneMatch(Objects::nonNull))
            errors.add("В свидетельстве ЕГЭ необходимо указать баллы хотя бы для одного из предметов.");

        Map<StateExamSubject, StateExamSubjectPassScore> scoreMap =
                getList(StateExamSubjectPassScore.class, StateExamSubjectPassScore.L_ENROLLMENT_CAMPAIGN, model.getCertificate().getEntrant().getEnrollmentCampaign())
                        .stream().collect(Collectors.toMap(StateExamSubjectPassScoreGen::getStateExamSubject, score -> score));

        final int[] rowIndex = {0};
        getCatalogItemList(StateExamSubject.class).stream()
                .sorted(CommonCatalogUtil.createCodeComparator(StateExamSubject.P_SUBJECT_C_T_M_O_CODE))
                .forEach(subject -> {
                    Integer mark = marks.get(subject.getId());
                    Integer year = years.get(subject.getId());
                    if(mark != null && year == null)
                        errors.add("Необходимо указать год сдачи предмета " + subject.getTitle() + ".",
                                   Model.getSearchListFieldId(YEAR_COLUMN_NAME, rowIndex[0]));

                    StateExamSubjectPassScore score = scoreMap.get(subject);
                    if (score != null)
                    {
                        if (mark != null && mark < score.getMark())
                        {
                            errors.add("Балл по предмету не может быть меньше установленного зачетного балла (" + String.valueOf(score.getMark()) + ").",
                                       Model.getSearchListFieldId(MARK_COLUMN_NAME, rowIndex[0]));
                        }
                    }

                    rowIndex[0]++;
                });
    }


    /**
     * @param entrant абитуриент
     * @return код региона для свидетельств данного абитуриента
     *         сначала ищем в каком-нибудь из учебных заведений абитуриента, затем в самом ОУ, иначе пустая строка
     */
    @SuppressWarnings("deprecation")
    private String getRegionCode(Entrant entrant)
    {
        PersonEduInstitution personEduInstitution = entrant.getPerson().getPersonEduInstitution();

        if (personEduInstitution != null)
            return personEduInstitution.getRegionCode();
        else
        {
            AddressDetailed academyAddress = TopOrgUnit.getInstance().getAddress();
            return (academyAddress != null) ? academyAddress.getSettlement().getInheritedRegionCode() : null;
        }
    }

    /**
     * prefix + postfix <= 13
     *
     * @return возврашает номер вида prefix+{0}+postfix добивая середину нулями до 13 символов
     */
    public static String makeCertificateNumberPlaceHolder(String prefix, String postfix)
    {
        if (prefix == null) prefix = "";
        if (postfix == null) postfix = "";

        int zeros = 13 - (prefix.length() + postfix.length());

        StringBuilder result = new StringBuilder(prefix);
        for (int i = 0; i < zeros; i++)
            result.append("0");

        result.append(postfix);

        return result.toString();
    }
}
