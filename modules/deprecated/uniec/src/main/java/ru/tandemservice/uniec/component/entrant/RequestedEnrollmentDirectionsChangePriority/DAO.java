/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.RequestedEnrollmentDirectionsChangePriority;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

/**
 * @author ekachanova
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    @SuppressWarnings({"unchecked"})
    public void prepare(Model model)
    {
        model.setEntrantRequest(getNotNull(EntrantRequest.class, model.getEntrantRequest().getId()));

        Map<Long, Object> valueMap = new HashMap<Long, Object>();

        int i = 1;
        for (RequestedEnrollmentDirection item : model.getDataSource().getEntityList())
            valueMap.put(item.getId(), i++);

        ((BlockColumn) model.getDataSource().getColumn(Model.PRIORITY_COLUMN)).setValueMap(valueMap);
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "d");
        builder.add(MQExpression.eq("d", RequestedEnrollmentDirection.L_ENTRANT_REQUEST, model.getEntrantRequest()));
        builder.addOrder("d", RequestedEnrollmentDirection.P_PRIORITY);
        List<RequestedEnrollmentDirection> list = builder.getResultList(getSession());

        model.getDataSource().setCountRow(list.size());
        UniBaseUtils.createPage(model.getDataSource(), list);
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public void update(Model model)
    {
        Map<Long, Object> valueMap = ((IValueMapHolder) model.getDataSource().getColumn(Model.PRIORITY_COLUMN)).getValueMap();

        for (Object value : valueMap.values())
        {
            if (value == null)
                throw new ApplicationException("Поле «Приоритет» должно быть заполнено.");
            int num = ((Number) value).intValue();
            if (num < 1 || num > valueMap.size())
                throw new ApplicationException("Приоритеты указаны неверно, необходимо вводить цифры от 1 до " + valueMap.size() + ".");
        }
        if (valueMap.size() != new HashSet<Object>(valueMap.values()).size())
            throw new ApplicationException("Приоритеты не могут совпадать.");

        final Session session = getSession();

        int fakePriority = -1;
        for (RequestedEnrollmentDirection requestedEnrollmentDirection : model.getDataSource().getEntityList())
        {
            requestedEnrollmentDirection.setPriority(fakePriority--);
            session.update(requestedEnrollmentDirection);
        }
        session.flush();
        for (Map.Entry entry : valueMap.entrySet())
        {
            RequestedEnrollmentDirection requestedEnrollmentDirection = (RequestedEnrollmentDirection) get((Long) entry.getKey());
            requestedEnrollmentDirection.setPriority((((Number) entry.getValue()).intValue()));
            session.update(requestedEnrollmentDirection);
        }
    }
}
