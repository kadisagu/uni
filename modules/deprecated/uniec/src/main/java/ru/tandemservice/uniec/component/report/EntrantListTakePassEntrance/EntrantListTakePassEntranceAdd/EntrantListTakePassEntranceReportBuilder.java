/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EntrantListTakePassEntrance.EntrantListTakePassEntranceAdd;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.examset.ExamSet;
import ru.tandemservice.uniec.entity.examset.ExamSetItem;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationFormRelation;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.entity.settings.Group2DisciplineRelation;
import ru.tandemservice.uniec.entity.settings.SetDiscipline;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.EntrantDataUtil.DetailLevel;

import java.util.*;
import java.util.Map.Entry;

/**
 * @author vip_delete
 * @since 03.07.2009
 */
public class EntrantListTakePassEntranceReportBuilder
{
    private Model _model;
    private Session _session;

    public EntrantListTakePassEntranceReportBuilder(Model model, Session session)
    {
        _model = model;
        _session = session;
    }

    public DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    private byte[] buildReport()
    {
        final List<ReportRow> reportRowList = getReportRowList(_model.getExamType().getId() == 1L);
        final List<Discipline2RealizationWayRelation> disciplineList = getDisciplineList(_model.getExamType().getId() == 1L);

        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ENTRANT_LIST_TAKE_PASS_ENTRANCE);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());

        RtfInjectModifier modifier = new RtfInjectModifier();
        modifier.put("highSchoolShortTitle", TopOrgUnit.getInstance().getShortTitle());
        modifier.put("compensationTypeStr", _model.getReport().getCompensationType().isBudget() ? "За счет средств федерального бюджета" : "На места с оплатой стоимости обучения");
        modifier.put("developForm", _model.isDevelopFormActive() ? StringUtils.join(CommonBaseUtil.getPropertiesList(_model.getDevelopFormList(), DevelopForm.P_TITLE), ", ") : "");
        modifier.put("examTypeTitle", _model.getExamType().getId() == 0L ? "экзаменов (по материалам ОУ)" : "ЕГЭ в вузе");
        modifier.put("createDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(_model.getReport().getFormingDate()));
        modifier.put("qualificationTitle", StringUtils.join(CommonBaseUtil.getPropertiesList(getQualifications(), Qualifications.P_TITLE), ", "));
        modifier.modify(document);

        RtfTableModifier tableModifier = new RtfTableModifier();

        String[][] data = new String[reportRowList.size()][3 + disciplineList.size()];
        for (int i = 0; i < reportRowList.size(); i++)
        {
            ReportRow row = reportRowList.get(i);
            data[i][0] = Integer.toString(i + 1);
            data[i][1] = row.getRegNumber();
            data[i][2] = row.getTitle();

            int j = 3;
            for (Discipline2RealizationWayRelation discipline : disciplineList)
            {
                CoreCollectionUtils.Pair<Double, String> mark = row.getMark(discipline.getId());
                data[i][j] = mark == null ? "x" : (mark.getX() == -1.0 ? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(mark.getX()) + " (" + mark.getY() + ")");
                j++;
            }
        }

        tableModifier.put("T", data);

        if (!disciplineList.isEmpty())
        {
            tableModifier.put("T", new RtfRowIntercepterBase()
            {
                @Override
                public void beforeModify(RtfTable table, int currentRowIndex)
                {
                    int[] parts = new int[disciplineList.size()];
                    Arrays.fill(parts, 1);
                    RtfUtil.splitRow(table.getRowList().get(currentRowIndex - 1), 3, (newCell, index) -> newCell.setElementList(Arrays.asList((IRtfElement) RtfBean.getElementFactory().createRtfText(disciplineList.get(index).getShortTitle()))), parts);
                    RtfUtil.splitRow(table.getRowList().get(currentRowIndex), 3, null, parts);
                }
            });
        }
        tableModifier.modify(document);

        return RtfUtil.toByteArray(document);
    }

    private List<Qualifications> getQualifications()
    {
        return (_model.isQualificationActive()) ? _model.getQualificationList() : UniDaoFacade.getCoreDao().getList(Qualifications.class, Qualifications.P_CODE);
    }

    @SuppressWarnings("unchecked")
    private List<ReportRow> getReportRowList(boolean onlyStateExamInternal)
    {
        // подготавливаем данные
        EntrantDataUtil util = new EntrantDataUtil(_session, _model.getReport().getEnrollmentCampaign(), getRequestedEnrollmentDirectionsBuilder(), DetailLevel.EXAM_PASS_WITH_MARK);

        // для каждого выбранного направления приема
        Map<Entrant, Object[]> map = new HashMap<>();
        for (RequestedEnrollmentDirection requestedEnrollmentDirection : util.getDirectionSet())
        {
            EntrantRequest entrantRequest = requestedEnrollmentDirection.getEntrantRequest();
            Entrant entrant = entrantRequest.getEntrant();

            // подготавливаем структуру, если необходимо
            Object[] data = map.get(entrant);
            if (data == null)
            {
                map.put(entrant, data = new Object[]{new HashSet<EntrantRequest>(), new HashMap<Long, CoreCollectionUtils.Pair<Double, String>>()});
            }

            Set<EntrantRequest> entrantRequests = (Set<EntrantRequest>) data[0];
            Map<Long, CoreCollectionUtils.Pair<Double, String>> disciplineMap = (Map<Long, CoreCollectionUtils.Pair<Double, String>>) data[1];

            // добавляем заявление в список
            entrantRequests.add(entrantRequest);

            // для каждого выбранного вступительного испытания
            for (ChosenEntranceDiscipline chosenEntranceDiscipline : util.getChosenEntranceDisciplineSet(requestedEnrollmentDirection))
            {
                // для каждой дисциплины для сдачи
                for (ExamPassDiscipline examPassDiscipline : util.getExamPassDisciplineSet(chosenEntranceDiscipline))
                {
                    // проверяем, что внутренняя
                    SubjectPassForm subjectPassForm = examPassDiscipline.getSubjectPassForm();
                    if (!subjectPassForm.isInternal())
                    {
                        continue;
                    }

                    // проверяем ЕГЭ(вуз)
                    if (onlyStateExamInternal ^ subjectPassForm.getCode().equals(UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM_INTERNAL))
                    {
                        continue;
                    }

                    Long disciplineId = examPassDiscipline.getEnrollmentCampaignDiscipline().getId();
                    String passFormTitle = subjectPassForm.getShortTitle();

                    // берем итоговую оценку
                    double mark = util.getFinalMark(examPassDiscipline);
                    CoreCollectionUtils.Pair<Double, String> markData = disciplineMap.get(disciplineId);

                    // обновляем данные, если необходимо
                    if (markData != null)
                    {
                        if (mark > markData.getX())
                        {
                            markData.setX(mark);
                            markData.setY(passFormTitle);
                        }
                    }
                    // иначе сохраняем новые
                    else
                    {
                        disciplineMap.put(disciplineId, new CoreCollectionUtils.Pair<>(mark, passFormTitle));
                    }
                }
            }
        }

        // удаляем из списка абитуриентов, у которых нет оценок
        for (Iterator<Entry<Entrant, Object[]>> iterator = map.entrySet().iterator(); iterator.hasNext(); )
        {
            if (((Map<Long, CoreCollectionUtils.Pair<Double, String>>) iterator.next().getValue()[1]).isEmpty())
            {
                iterator.remove();
            }
        }

        // создаем строки отчета
        List<ReportRow> result = new ArrayList<>();
        for (Map.Entry<Entrant, Object[]> entry : map.entrySet())
        {
            Entrant entrant = entry.getKey();
            List<EntrantRequest> entrantRequestList = new ArrayList((Set) entry.getValue()[0]);
            Map<Long, CoreCollectionUtils.Pair<Double, String>> markData = (Map) entry.getValue()[1];

            Collections.sort(entrantRequestList, (o1, o2) -> o1.getRegDate().compareTo(o2.getRegDate()));

            String regNumber = getRegNumber(entrantRequestList, entrant);
            String fullFio = entrant.getPerson().getFullFio();

            result.add(new ReportRow(regNumber, fullFio, markData));
        }

        Collections.sort(result, ITitled.TITLED_COMPARATOR);
        return result;
    }

    private MQBuilder getRequestedEnrollmentDirectionsBuilder()
    {
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "req");
        builder.addJoinFetch("req", RequestedEnrollmentDirection.enrollmentDirection().s(), "ed");
        builder.addJoinFetch("req", RequestedEnrollmentDirection.entrantRequest().s(), "request");
        builder.addJoinFetch("request", EntrantRequest.entrant().s(), "entrant");
        builder.addJoinFetch("entrant", Entrant.person().s(), "person");
        builder.addJoinFetch("person", Person.identityCard().s(), "card");
        builder.addJoin("ed", EnrollmentDirection.educationOrgUnit().s(), "ou");

        builder.add(MQExpression.eq("entrant", Entrant.archival().s(), Boolean.FALSE));
        builder.add(MQExpression.notEq("req", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        builder.add(MQExpression.eq("request", EntrantRequest.entrant().enrollmentCampaign().s(), _model.getReport().getEnrollmentCampaign()));
        builder.add(UniMQExpression.betweenDate("request", EntrantRequest.regDate().s(), _model.getReport().getDateFrom(), _model.getReport().getDateTo()));
        builder.add(MQExpression.eq("req", RequestedEnrollmentDirection.compensationType().s(), _model.getReport().getCompensationType()));

        if (_model.getReport().getEnrollmentCampaign().isExamSetDiff())
        {
            builder.add(MQExpression.eq("req", RequestedEnrollmentDirection.studentCategory().s(), _model.getStudentCategory()));
        } else if (_model.isStudentCategoryActive())
        {
            builder.add(MQExpression.in("req", RequestedEnrollmentDirection.studentCategory().s(), _model.getStudentCategoryList()));
        }

        if (_model.isQualificationActive())
        {
            builder.add(MQExpression.in("ou", EducationOrgUnit.educationLevelHighSchool().educationLevel().qualification().s(), _model.getQualificationList()));
        }

        if (_model.isFormativeOrgUnitActive())
        {
            builder.add(MQExpression.in("ou", EducationOrgUnit.formativeOrgUnit().s(), _model.getFormativeOrgUnitList()));
        }

        if (_model.isTerritorialOrgUnitActive())
        {
            builder.add(_model.getTerritorialOrgUnitList().isEmpty() ? MQExpression.isNull("ou", EducationOrgUnit.territorialOrgUnit().s()) : MQExpression.in("ou", EducationOrgUnit.territorialOrgUnit().s(), _model.getTerritorialOrgUnitList()));
        }

        if (_model.isEducationLevelHighSchoolActive())
        {
            builder.add(MQExpression.in("ou", EducationOrgUnit.educationLevelHighSchool().s(), _model.getEducationLevelHighSchoolList()));
        }

        if (_model.isDevelopFormActive())
        {
            builder.add(MQExpression.in("ou", EducationOrgUnit.developForm().s(), _model.getDevelopFormList()));
        }

        if (_model.isDevelopConditionActive())
        {
            builder.add(MQExpression.in("ou", EducationOrgUnit.developCondition().s(), _model.getDevelopConditionList()));
        }

        if (_model.isDevelopTechActive())
        {
            builder.add(MQExpression.in("ou", EducationOrgUnit.developTech().s(), _model.getDevelopTechList()));
        }

        if (_model.isDevelopPeriodActive())
        {
            builder.add(MQExpression.in("ou", EducationOrgUnit.developPeriod().s(), _model.getDevelopPeriodList()));
        }

        return builder;
    }

    private List<Discipline2RealizationWayRelation> getDisciplineList(boolean onlyStateExamInternal)
    {
        EnrollmentCampaign enrollmentCampaign = _model.getReport().getEnrollmentCampaign();

        List<Group2DisciplineRelation> relList = new MQBuilder(Group2DisciplineRelation.ENTITY_CLASS, "r").add(MQExpression.eq("r", Group2DisciplineRelation.L_DISCIPLINE + "." + Discipline2RealizationWayRelation.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign)).getResultList(_session);

        Map<Long, Set<Discipline2RealizationWayRelation>> groupId2disciplineSet = new HashMap<>();
        for (Group2DisciplineRelation rel : relList)
        {
            Set<Discipline2RealizationWayRelation> set = groupId2disciplineSet.get(rel.getGroup().getId());
            if (set == null)
                groupId2disciplineSet.put(rel.getGroup().getId(), set = new HashSet<>());
            set.add(rel.getDiscipline());
        }

        List<EnrollmentDirection> enrollmentDirectionList = MultiEnrollmentDirectionUtil.getEnrollmentDirections(_model, _session);

        StudentCategory studentCategory = enrollmentCampaign.isExamSetDiff() ? _model.getStudentCategory() : UniDaoFacade.getCoreDao().getCatalogItem(StudentCategory.class, UniDefines.STUDENT_CATEGORY_STUDENT);
        Map<String, ExamSet> map = UniecDAOFacade.getExamSetDAO().getExamSetMap(enrollmentCampaign);

        Set<Discipline2RealizationWayRelation> disciplineSet = new HashSet<>();

        for (EnrollmentDirection direction : enrollmentDirectionList)
        {
            for (ExamSet examSet : map.values())
            {
                if (studentCategory.equals(examSet.getStudentCategory()) && examSet.getList().contains(direction))
                {
                    for (ExamSetItem examSetItem : examSet.getSetItemList())
                    {
                        if (examSetItem.getSubject() instanceof Discipline2RealizationWayRelation)
                            disciplineSet.add((Discipline2RealizationWayRelation) examSetItem.getSubject());
                        else
                            disciplineSet.addAll(groupId2disciplineSet.get(examSetItem.getSubject().getId()));

                        for (SetDiscipline choseItem : examSetItem.getChoice())
                        {
                            if (choseItem instanceof Discipline2RealizationWayRelation)
                                disciplineSet.add((Discipline2RealizationWayRelation) choseItem);
                            else
                                disciplineSet.addAll(groupId2disciplineSet.get(choseItem.getId()));
                        }
                    }
                }
            }
        }

        SubjectPassForm stateExamInternal = UniDaoFacade.getCoreDao().getCatalogItem(SubjectPassForm.class, UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM_INTERNAL);
        CompensationType budget = UniDaoFacade.getCoreDao().getCatalogItem(CompensationType.class, UniDefines.COMPENSATION_TYPE_BUDGET);
        CompensationType contract = UniDaoFacade.getCoreDao().getCatalogItem(CompensationType.class, UniDefines.COMPENSATION_TYPE_CONTRACT);

        // аналог UniecDAOFacade.getEntrantDAO().getRealizationFormSet, только с разделением на формы сдачи
        boolean compensationTypeDiff = enrollmentCampaign.isEnrollmentPerCompTypeDiff();
        MQBuilder builder = new MQBuilder(Discipline2RealizationFormRelation.ENTITY_CLASS, "r");
        builder.add(MQExpression.eq("r", Discipline2RealizationFormRelation.L_DISCIPLINE + "." + Discipline2RealizationWayRelation.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign));
        builder.add(MQExpression.eq("r", Discipline2RealizationFormRelation.L_SUBJECT_PASS_FORM + "." + SubjectPassForm.P_INTERNAL, Boolean.TRUE));
        if (!compensationTypeDiff)
            builder.add(MQExpression.eq("r", Discipline2RealizationFormRelation.L_TYPE, budget));
        if (onlyStateExamInternal)
            builder.add(MQExpression.eq("r", Discipline2RealizationFormRelation.L_SUBJECT_PASS_FORM, stateExamInternal));
        else
            builder.add(MQExpression.notEq("r", Discipline2RealizationFormRelation.L_SUBJECT_PASS_FORM, stateExamInternal));
        List<Discipline2RealizationFormRelation> list = builder.getResultList(_session);

        Set<MultiKey> realisationFormSet = new HashSet<>();
        if (compensationTypeDiff)
        {
            for (Discipline2RealizationFormRelation item : list)
                realisationFormSet.add(new MultiKey(item.getDiscipline(), item.getType()));
        } else
        {
            for (Discipline2RealizationFormRelation item : list)
                realisationFormSet.add(new MultiKey(new Object[]{item.getDiscipline()}));
        }

        List<Discipline2RealizationWayRelation> result = new ArrayList<>();

        // берем только такие дисциплины, которые onlyStateExamInternal
        for (Discipline2RealizationWayRelation discipline : disciplineSet)
        {
            if (compensationTypeDiff)
            {
                MultiKey key1 = new MultiKey(discipline, budget);
                MultiKey key2 = new MultiKey(discipline, contract);
                if (realisationFormSet.contains(key1) || realisationFormSet.contains(key2))
                    result.add(discipline);
            } else
            {
                MultiKey key = new MultiKey(new Object[]{discipline});
                if (realisationFormSet.contains(key))
                    result.add(discipline);
            }
        }

        Collections.sort(result, ITitled.TITLED_COMPARATOR);
        return result;
    }

    /**
     * Для кастомизации в проектах
     * @return Рег. №
     */
    protected String getRegNumber(List<EntrantRequest> entrantRequestList, Entrant entrant) {
        return StringUtils.join(CommonBaseUtil.getPropertiesList(entrantRequestList, EntrantRequest.P_STRING_NUMBER), ", ");
    }

    private static class ReportRow implements ITitled
    {
        private String _regNumber;
        private String _fullFio;
        private Map<Long, CoreCollectionUtils.Pair<Double, String>> _markMap;

        private ReportRow(String reqNumber, String fullFio, Map<Long, CoreCollectionUtils.Pair<Double, String>> markMap)
        {
            _regNumber = reqNumber;
            _fullFio = fullFio;
            _markMap = markMap;
        }

        public String getRegNumber()
        {
            return _regNumber;
        }

        @Override
        public String getTitle()
        {
            return _fullFio;
        }

        public CoreCollectionUtils.Pair<Double, String> getMark(Long disciplineId)
        {
            return _markMap.get(disciplineId);
        }
    }
}
