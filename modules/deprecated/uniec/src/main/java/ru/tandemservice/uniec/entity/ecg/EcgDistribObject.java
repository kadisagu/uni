package ru.tandemservice.uniec.entity.ecg;

import ru.tandemservice.uniec.entity.ecg.gen.EcgDistribObjectGen;

/**
 * Распределение по конкурсной группе
 */
public class EcgDistribObject extends EcgDistribObjectGen
{
    public String getCategory()
    {
        return isSecondHighAdmission() ? "Второе высшее" : "Студент/Слушатель" ;
    }
}