package ru.tandemservice.uniec.entity.report;

import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.uniec.entity.report.gen.*;

/** @see ru.tandemservice.uniec.entity.report.gen.EntrantDailyRatingByED2ReportGen */
public class EntrantDailyRatingByED2Report extends EntrantDailyRatingByED2ReportGen
{
    public static final String P_PERIOD_TITLE = "periodTitle";

    public String getPeriodTitle()
    {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }
}