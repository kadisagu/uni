/* $Id$ */
package ru.tandemservice.uniec.component.report.EntrantDailyRatingByED2.EntrantDailyRatingByED2Add;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.*;
import ru.tandemservice.uniec.entity.catalog.codes.EntrantStateCodes;
import ru.tandemservice.uniec.entity.catalog.codes.SubjectPassFormCodes;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.entrant.gen.ChosenEntranceDisciplineGen;
import ru.tandemservice.uniec.entity.entrant.gen.ExamPassDisciplineGen;
import ru.tandemservice.uniec.entity.examset.EnrollmentDirectionExamSetData;
import ru.tandemservice.uniec.entity.examset.ExamSetItem;
import ru.tandemservice.uniec.entity.examset.ExamSetStructure;
import ru.tandemservice.uniec.entity.examset.ExamSetStructureItem;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.entity.settings.SetDiscipline;
import ru.tandemservice.uniec.ui.EnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.ExamSetUtil;
import ru.tandemservice.uniec.util.MarkDistributionUtil;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 16.06.2016
 */
public class EntrantDailyRatingByED2ReportBuilder
{
    protected enum TABLE_KIND
    {
        TABLE_KIND_QUOTA("на места в пределах особой квоты", 1),
        TABLE_KIND_WITHOUT_EXAM("без вступительных испытаний", 2),
        TABLE_KIND_TARGET_ADMISSION("на места в пределах целевой квоты", 3),
        TABLE_KIND_DEFAULT("на места в рамках контрольных цифр приема", 4);

        final String value;
        final int priority;

        TABLE_KIND(String value, int priority)
        {
            this.value = value;
            this.priority = priority;
        }
    }
    public static final String PASS_FORM_EGE = "Результаты ЕГЭ";
    public static final String PASS_FORM_INNER = "Результаты ВИ";


    protected Model _model;
    protected Session _session;
    protected RtfDocument template;
    //fetch
    /**
     * map(enrollmentDirectionId, list(RequestedEnrollmentDirection) )
     */
    protected Map<Long, List<RequestedEnrollmentDirection>> _requestedDirectionByDirections;
    /**
     * map(requestedEnrollmentDirectionId, list(ChosenEntranceDiscipline) )
     */
    protected Map<Long, List<ChosenEntranceDiscipline>> _chosenDisciplineByRequestedDirection;
    protected Set<Long> _personWithBenefitIds;
    protected Set<Long> _entrantWithRecommendationIds;
    protected Map<Long, Integer> _individualProgressMarkByRequestMap = new HashMap<>();
    protected Map<EnrollmentDirection, EnrollmentDirectionExamSetData> _directionExamSetDataMap;
    /**
     * map(EntrantId, map(Discipline2RealizationWayRelationId, list(SubjectPassForm) ) )
     */
    protected Map<Long, Map<Long, List<SubjectPassForm>>> _passFormByEntrantByDiscipline;
    /**
     * map(EnrollmentDirectionId, map(StudentCategoryId, map(SetDisciplineId, EntranceDiscipline) ) )
     */
    protected Map<Long, Map<Long, Map<Long, EntranceDiscipline>>> _disciplinesByStudentCategoryByDirectionMap;


    public EntrantDailyRatingByED2ReportBuilder(Model model, Session session)
    {
        _model = model;
        _session = session;
    }

    public DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    protected byte[] buildReport()
    {
        List<EnrollmentDirection> enrollmentDirectionList = getEnrollmentDirectionList();
        init(enrollmentDirectionList);

        _directionExamSetDataMap.values().forEach(datum -> {
            if (!datum.isStructuresEqual())
                throw new ApplicationException("Для направления приема '" + datum.getEnrollmentDirection().getTitle() + "' по категориям поступающих и виду затрат наборы экзаменов различные.");

        });

        RtfDocument resultDocument = RtfBean.getElementFactory().createRtfDocument();
        resultDocument.setHeader(template.getHeader());
        resultDocument.setSettings(template.getSettings());
        resultDocument.setElementList(
                enrollmentDirectionList.stream()
                        .filter(direction -> !(_model.getReport().isNotPrintSpesWithoutRequest()
                                && CollectionUtils.isEmpty(_requestedDirectionByDirections.get(direction.getId()))))
                        .map(this::getDirectionTables)
                        .flatMap(Collection::stream)
                        .collect(Collectors.toList())
        );

        return RtfUtil.toByteArray(resultDocument);
    }

    protected void init(List<EnrollmentDirection> enrollmentDirectionList)
    {
        IScriptItem scriptItem = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ENTRANT_DAILY_RATING_BY_ED2);
        template = new RtfReader().read(scriptItem.getCurrentTemplate());

        EnrollmentCampaign campaign = _model.getEnrollmentCampaign();

        DQLSelectBuilder requestedDirectionsBuilder = getRequestedEnrollmentDirectionsBuilder(enrollmentDirectionList);

        List<RequestedEnrollmentDirection> requestedDirections = requestedDirectionsBuilder.createStatement(_session).list();

        _requestedDirectionByDirections = requestedDirections.stream()
                .sorted(Comparator.comparing(r -> r.getEnrollmentDirection().getTitle()))
                .collect(Collectors.groupingBy(reqDir -> reqDir.getEnrollmentDirection().getId(), Collectors.mapping(reqDir -> reqDir, Collectors.toList())));

        _directionExamSetDataMap = ExamSetUtil.getDirectionExamSetDataMap(_session, campaign,
                                                                          _model.getStudentCategoryList(),
                                                                          _model.getReport().getCompensationType());

        _chosenDisciplineByRequestedDirection = new DQLSelectBuilder()
                .fromEntity(ChosenEntranceDiscipline.class, "cho")
                .column(property("cho"))
                .where(in(property("cho", ChosenEntranceDiscipline.chosenEnrollmentDirection()), requestedDirectionsBuilder.buildQuery()))
                .createStatement(_session).<ChosenEntranceDiscipline>list()

                .stream()
                .collect(Collectors.groupingBy(cho -> cho.getChosenEnrollmentDirection().getId(),
                                               Collectors.mapping(cho -> cho, Collectors.toList())));

        _disciplinesByStudentCategoryByDirectionMap = new DQLSelectBuilder()
                .fromEntity(EntranceDiscipline.class, "ed")
                .column(property("ed"))
                .where(in(property("ed", EntranceDiscipline.enrollmentDirection()), enrollmentDirectionList))

                .createStatement(_session).<EntranceDiscipline>list().stream()

                .filter(dis -> dis.getDiscipline() instanceof Discipline2RealizationWayRelation)
                .collect(Collectors.groupingBy(
                        dis -> dis.getEnrollmentDirection().getId(),
                        Collectors.mapping(
                                dis -> dis,
                                Collectors.groupingBy(dis -> dis.getStudentCategory().getId(),
                                                      Collectors.mapping(dis -> dis,
                                                                         Collectors.toMap(dis -> dis.getDiscipline().getId(), dis -> dis))))));

        _personWithBenefitIds = new HashSet<>(
                new DQLSelectBuilder()
                        .fromEntity(Person.class, "p")
                        .column(property("p", Person.id()))
                        .where(exists(PersonBenefit.class, PersonBenefit.person().s(), property("p")))
                        .where(existsByExpr(RequestedEnrollmentDirection.class, "req",
                                            and(eq(property("req", RequestedEnrollmentDirection.entrantRequest().entrant().person()), property("p")),
                                                in(property("req"), requestedDirectionsBuilder.buildQuery()))
                        ))
                        .createStatement(_session).<Long>list());

        _entrantWithRecommendationIds = new HashSet<>(
                new DQLSelectBuilder()
                        .fromEntity(Entrant.class, "e")
                        .column(property("e", Entrant.id()))
                        .where(exists(EntrantEnrolmentRecommendation.class, EntrantEnrolmentRecommendation.entrant().s(), property("e")))
                        .where(existsByExpr(RequestedEnrollmentDirection.class, "req",
                                            and(eq(property("req", RequestedEnrollmentDirection.entrantRequest().entrant()), property("e")),
                                                in(property("req"), requestedDirectionsBuilder.buildQuery()))
                        ))
                        .createStatement(_session).<Long>list());

        List<ExamPassDiscipline> examPassDisciplineList = new DQLSelectBuilder()
                .fromEntity(ExamPassDiscipline.class, "epd")
                .column(property("epd"))
                .where(existsByExpr(ChosenEntranceDiscipline.class, "ced",
                                    and(eq(property("epd", ExamPassDiscipline.enrollmentCampaignDiscipline()),
                                           property("ced", ChosenEntranceDiscipline.enrollmentCampaignDiscipline())),
                                        in(property("ced", ChosenEntranceDiscipline.chosenEnrollmentDirection()), requestedDirections))
                ))

                .createStatement(_session).list();
        _passFormByEntrantByDiscipline = examPassDisciplineList.stream()
                .filter(row -> row.getEntrantExamList().getEntrant() instanceof Entrant ||
                        row.getEntrantExamList().getEntrant() instanceof EntrantRequest)
                .collect(Collectors.groupingBy(
                        row -> {
                            IEntrant iEntrant = row.getEntrantExamList().getEntrant();
                            if (iEntrant instanceof Entrant)
                                return ((Entrant) iEntrant).getId();
                            else if (iEntrant instanceof EntrantRequest)
                                return ((EntrantRequest) iEntrant).getEntrant().getId();
                            return null;
                        },
                        Collectors.groupingBy(row -> row.getEnrollmentCampaignDiscipline().getId(),
                                              Collectors.mapping(ExamPassDisciplineGen::getSubjectPassForm, Collectors.toList()))
                ));

        if (campaign.isUseIndividualProgressInAmountMark())
        {
            DQLSelectBuilder requestBuilder = new DQLSelectBuilder()
                    .fromEntity(RequestedEnrollmentDirection.class, "red")
                    .column(property("red", RequestedEnrollmentDirection.entrantRequest()))
                    .where(in(property("red"), requestedDirectionsBuilder.buildQuery()));

            String eipAlias = "eip";
            String reqAlias = "req";
            List<Object[]> raws = new DQLSelectBuilder()
                    .fromEntity(EntrantIndividualProgress.class, eipAlias)
                    .column(DQLFunctions.sum(property(eipAlias, EntrantIndividualProgress.mark())))
                    .where(eq(property(eipAlias, EntrantIndividualProgress.entrant().enrollmentCampaign()), value(campaign)))

                    .joinEntity(eipAlias, DQLJoinType.inner, EntrantRequest.class, reqAlias,
                                and(in(property(reqAlias), requestBuilder.buildQuery()),
                                    eq(property(eipAlias, EntrantIndividualProgress.entrant()),
                                       property(reqAlias, EntrantRequest.entrant())),
                                    IndProg2Request4Exclude.getExpression4ExcludeEntrantIndividualProgress(property(reqAlias), property(eipAlias))
                                ))
                    .column(property(reqAlias, EntrantRequest.id()))
                    .group(property(reqAlias, EntrantRequest.id()))
                    .createStatement(_session).list();

            _individualProgressMarkByRequestMap = raws.stream().collect(Collectors.toMap(raw -> (Long) raw[1], raw -> ((Number) raw[0]).intValue()));
        }
    }

    protected DQLSelectBuilder getRequestedEnrollmentDirectionsBuilder(List<EnrollmentDirection> enrollmentDirectionList)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(RequestedEnrollmentDirection.ENTITY_CLASS, "r")
                .column(property("r"))

                .where(eq(property("r", RequestedEnrollmentDirection.compensationType()), value(_model.getReport().getCompensationType())))
                .where(betweenDays(RequestedEnrollmentDirection.regDate().fromAlias("r"), _model.getReport().getDateFrom(), _model.getReport().getDateTo()))
                .where(in(property("r", RequestedEnrollmentDirection.enrollmentDirection()), enrollmentDirectionList))

                .where(eq(property("r", RequestedEnrollmentDirection.entrantRequest().entrant().archival()), value(false)))
                .where(not(eq(property("r", RequestedEnrollmentDirection.state().code()), value(EntrantStateCodes.OUT_OF_COMPETITION))))
                .where(not(eq(property("r", RequestedEnrollmentDirection.state().code()), value(EntrantStateCodes.TAKE_DOCUMENTS_AWAY))));
        if (_model.getReport().isWithoutEnrolled())
            builder.where(ne(property("r", RequestedEnrollmentDirection.state().code()), value(EntrantStateCodes.ENROLED)));

        if (_model.getReport().isOnlyWithOriginals())
            builder.where(eq(property("r", RequestedEnrollmentDirection.originalDocumentHandedIn()), value(true)));

        if (_model.getCustomStateRuleValue() != null)
        {
            IDQLExpression expression4EntrantCustomState = EntrantCustomState.getExistsExpression4EntrantCustomState(
                    property("r", RequestedEnrollmentDirection.entrantRequest().entrant()),
                    _model.getCustomStateRuleValue(), _model.getEntrantCustomStateList(), new Date());
            if (expression4EntrantCustomState != null)
                builder.where(expression4EntrantCustomState);
        }

        FilterUtils.applySelectFilter(builder, "r", RequestedEnrollmentDirection.studentCategory().s(), _model.getStudentCategoryList());
        FilterUtils.applySelectFilter(builder, "r", RequestedEnrollmentDirection.competitionKind().s(), _model.getCompetitionKindList());

        return builder;
    }

    protected List<IRtfElement> getDirectionTables(EnrollmentDirection direction)
    {
        RtfDocument directionDocument = RtfBean.getElementFactory().createRtfDocument();
        if (_model.getReport().isHeaderWithoutParam())
            addDirectionTitleShort(directionDocument, direction, template.getSettings().getPageWidth());
        else addDirectionTitleFull(directionDocument, direction, template.getSettings().getPageWidth());

        List<RequestedEnrollmentDirection> requestedDirections = _requestedDirectionByDirections.get(direction.getId());
        if (CollectionUtils.isEmpty(requestedDirections))
            return directionDocument.getElementList();

        directionDocument.getElementList().addAll(
                requestedDirections.stream()
                        .filter(reqDirection -> !(reqDirection.isTargetAdmission() && _model.getReport().isWithoutTargetAdmission()))
                        .collect(Collectors.groupingBy(this::getTableKind, Collectors.mapping(row -> row, Collectors.toList())))
                        .entrySet().stream()
                        .sorted(Comparator.comparingInt(e -> e.getKey().priority))
                        .map(entry -> getEntrantTable(template.getClone(), direction, entry.getValue(), entry.getKey()))
                        .flatMap(Collection::stream)
                        .collect(Collectors.toList())
        );

        return directionDocument.getElementList();
    }

    protected void addDirectionTitleShort(RtfDocument directionDocument, EnrollmentDirection direction, int pageWidth)
    {
        EducationOrgUnit educationOrgUnit = direction.getEducationOrgUnit();
        EducationLevelsHighSchool educationLevelHighSchool = educationOrgUnit.getEducationLevelHighSchool();

        RtfString directionTitle = new RtfString()
                .font(0).fontSize(24)
                .append("Списки лиц, подавших документы на ")
                .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(_model.getReport().getFormingDate()))
                .append(IRtfData.PAGEBB)
                .append(IRtfData.PAR)
                .append(educationLevelHighSchool.getTitle());

        int allPlan = getPlan(direction.getMinisterialPlan(), direction.getContractPlan());
        int targetPlan = getPlan(direction.getTargetAdmissionPlanBudget(), direction.getTargetAdmissionPlanContract());
        int quotaPlan = direction.getSpecRightsQuota() != null ? direction.getSpecRightsQuota() : 0;
        directionTitle.append(IRtfData.PAR).append("Количество мест - ").append(String.valueOf(allPlan));
        if (quotaPlan > 0 && _model.getReport().getCompensationType().isBudget())
            directionTitle.append(", в т.ч. в пределах особой квоты - ").append(String.valueOf(quotaPlan));
        if (targetPlan > 0)
            directionTitle.append(", в т.ч. в пределах целевой квоты - ").append(String.valueOf(targetPlan));

        RtfTable titleTable = RtfBean.getRtfApi().createTable(1, 1, pageWidth, 0, 0, 0);
        RtfCell titleCell = titleTable.getRowList().get(0).getCellList().get(0);
        titleCell.getElementList().addAll(directionTitle.toList());

        directionDocument.addElement(titleTable);
        directionDocument.addElement(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
        directionDocument.addElement(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
    }

    protected void addDirectionTitleFull(RtfDocument directionDocument, EnrollmentDirection direction, int pageWidth)
    {
        EducationOrgUnit educationOrgUnit = direction.getEducationOrgUnit();
        EducationLevelsHighSchool educationLevelHighSchool = educationOrgUnit.getEducationLevelHighSchool();

        RtfString directionTitle = new RtfString()
                .font(0).fontSize(24)
                .append("Рейтинг абитуриентов на ")
                .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(_model.getReport().getFormingDate()))
                .append(IRtfData.PAGEBB)
                .append(IRtfData.PAR)
                .append(educationLevelHighSchool.getEducationLevel().getTitleCodePrefix())
                .append(" ")
                .append(educationLevelHighSchool.getTitleWithProfile());

        Qualifications qualifications = educationLevelHighSchool.getEducationLevel().getQualification();
        if (qualifications != null)
            directionTitle.append(" (").append(qualifications.getTitle().toLowerCase()).append(")");

        directionTitle.append(IRtfData.PAR)
                .append(educationOrgUnit.getDevelopForm().getTitle())
                .append(" форма обучения (")
                .append(educationOrgUnit.getDevelopCondition().getTitle())
                .append(", ")
                .append(educationOrgUnit.getDevelopPeriod().getTitle())
                .append(")                   ")
                .append((_model.getReport().getCompensationType().isBudget() ? "За счет средств федерального бюджета" : "На места с оплатой стоимости обучения"));

        OrgUnit territorialOrgUnit = educationOrgUnit.getTerritorialOrgUnit();
        if (territorialOrgUnit != null)
            directionTitle.append("          ")
                    .append(StringUtils.isEmpty(territorialOrgUnit.getNominativeCaseTitle()) ? territorialOrgUnit.getFullTitle() : territorialOrgUnit.getNominativeCaseTitle());

        int allPlan = getPlan(direction.getMinisterialPlan(), direction.getContractPlan());
        int targetPlan = getPlan(direction.getTargetAdmissionPlanBudget(), direction.getTargetAdmissionPlanContract());
        int quotaPlan = direction.getSpecRightsQuota() != null ? direction.getSpecRightsQuota() : 0;
        directionTitle.append(IRtfData.PAR).append("Количество мест - ").append(String.valueOf(allPlan));
        if (quotaPlan > 0 && _model.getReport().getCompensationType().isBudget())
            directionTitle.append(", в т.ч. в пределах особой квоты - ").append(String.valueOf(quotaPlan));
        if (targetPlan > 0)
            directionTitle.append(", в т.ч. в пределах целевой квоты - ").append(String.valueOf(targetPlan));

        RtfTable titleTable = RtfBean.getRtfApi().createTable(1, 1, pageWidth, 0, 0, 0);
        RtfCell titleCell = titleTable.getRowList().get(0).getCellList().get(0);
        titleCell.getElementList().addAll(directionTitle.toList());

        directionDocument.addElement(titleTable);
        directionDocument.addElement(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
        directionDocument.addElement(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
    }

    protected TABLE_KIND getTableKind(RequestedEnrollmentDirection direction)
    {
        if (direction.isTargetAdmission())
            return TABLE_KIND.TABLE_KIND_TARGET_ADMISSION;

        switch (direction.getCompetitionKind().getCode())
        {
            case "1":
                return TABLE_KIND.TABLE_KIND_WITHOUT_EXAM;
            case "3":
                return TABLE_KIND.TABLE_KIND_QUOTA;
        }
        return TABLE_KIND.TABLE_KIND_DEFAULT;

    }

    protected List<IRtfElement> getEntrantTable(RtfDocument template, EnrollmentDirection direction,
                                                List<RequestedEnrollmentDirection> directions, TABLE_KIND tableKind)
    {
        StringBuilder title = new StringBuilder("Списки лиц, поступающих ")
                .append(tableKind.value);
        switch (tableKind)
        {
            case TABLE_KIND_TARGET_ADMISSION:
                title.append(" – число мест: ").append(getPlan(direction.getTargetAdmissionPlanBudget(), direction.getTargetAdmissionPlanContract()));
                break;
            case TABLE_KIND_QUOTA:
                title.append(" – число мест: ").append(direction.getSpecRightsQuota() != null ? direction.getSpecRightsQuota() : 0);
                break;
            case TABLE_KIND_WITHOUT_EXAM:
                break;
            case TABLE_KIND_DEFAULT:
                title.append(" – число мест: ").append(getPlan(direction.getMinisterialPlan(), direction.getContractPlan()));
                break;
        }

        new RtfInjectModifier()
                .put("tableTitle", title.toString())
                .modify(template);

        final int[] index = {1};
        String[][] strings = directions.stream()
                .map(this::createReportRow)
                .sorted(ReportRow.getComparator(_model.getReport().isSortByOriginalDocHandedIn()))
                .map(row -> getTableRow(row, index[0]++))
                .toArray(String[][]::new);

        // run table modifier
        new RtfTableModifier()
                .put("T", strings)
                .put("T", getRowIntercepterBase(direction))
                .modify(template);

        return template.getElementList();
    }

    protected int getPlan(Integer budgetPlan, Integer contractPlan)
    {
        if (_model.getReport().getCompensationType().isBudget())
            return budgetPlan != null ? budgetPlan : 0;
        else
            return contractPlan != null ? contractPlan : 0;
    }

    protected ReportRow createReportRow(RequestedEnrollmentDirection direction)
    {
        ReportRow row = new ReportRow();

        row._direction = direction;
        EntrantRequest request = row._direction.getEntrantRequest();
        Entrant entrant = request.getEntrant();
        row._withBenefits = _personWithBenefitIds.contains(entrant.getPerson().getId());
        row._individualMark = _individualProgressMarkByRequestMap.get(request.getId());
        EnrollmentDirectionExamSetData examSetData = _directionExamSetDataMap.get(row._direction.getEnrollmentDirection());

        Map<Long, Map<Long, EntranceDiscipline>> disciplinesByStudentCategory = _disciplinesByStudentCategoryByDirectionMap.get(row._direction.getEnrollmentDirection().getId());
        Map<Long, EntranceDiscipline> disciplines = null;
        if (disciplinesByStudentCategory != null)
            disciplines = disciplinesByStudentCategory.get(row._direction.getStudentCategory().getId());

        // находим наилучшее распределение выбранных вступительных испытаний по строкам структуры набора
        List<ChosenEntranceDiscipline> chosenDisciplineList = _chosenDisciplineByRequestedDirection.get(row._direction.getId());
        if (chosenDisciplineList != null)
        {
            chosenDisciplineList = Arrays.asList(MarkDistributionUtil.getChosenDistribution(
                    new HashSet<>(chosenDisciplineList),
                    examSetData.getExamSetStructure().getItemList()
            ));
            chosenDisciplineList.sort(new ChosenEntranceDisciplineComparator(disciplines));
            row._chosenDisciplineSortedList = chosenDisciplineList;
            row._chosenDisciplinesByWay = chosenDisciplineList.stream()
                    .collect(Collectors.toMap(chosen -> chosen.getEnrollmentCampaignDiscipline().getId(),
                                              chosen -> chosen, (cho1, cho2) -> cho1));
        }
        else
        {
            row._chosenDisciplinesByWay = new HashMap<>();
        }

        row._withRecommendations = _entrantWithRecommendationIds.contains(entrant.getId());

        return row;
    }

    protected String[] getTableRow(ReportRow row, int index)
    {
        List<String> result = new ArrayList<>();

        RequestedEnrollmentDirection requestedEnrollmentDirection = row._direction;
        Entrant entrant = requestedEnrollmentDirection.getEntrantRequest().getEntrant();
        Map<Long, List<SubjectPassForm>> passFormsByDiscipline = _passFormByEntrantByDiscipline.get(entrant.getId());

        result.add(String.valueOf(index));
        result.add(getRegNumber(requestedEnrollmentDirection));
        result.add(entrant.getPerson().getFullFio());

        result.add(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(row.getSumMark()));

        final boolean[] passFormEge = {false};
        final boolean[] passFormInner = {false};
        EnrollmentDirectionExamSetData examSetData = _directionExamSetDataMap.get(requestedEnrollmentDirection.getEnrollmentDirection());
        ExamSetStructure examSetStructure = examSetData == null ? null : examSetData.getExamSetStructure();
        if (examSetStructure == null || examSetStructure.getItemList().isEmpty())
        {
            if (!_model.getReport().isWithoutDetailSumMark())
                result.add("");
        }
        else
        {
            List<ExamSetStructureItem> itemList = examSetStructure.getItemList();

            for (ExamSetStructureItem item : itemList)
            {
                ChosenEntranceDiscipline chosenDiscipline = item.getDisciplineSet().stream()
                        .map(way -> row._chosenDisciplinesByWay.get(way.getId()))
                        .filter(Objects::nonNull)
                        .findFirst().orElse(null);

                String markStr;
                if (chosenDiscipline == null)
                    markStr = "x";
                else
                {
                    Double mark = chosenDiscipline.getFinalMark();

                    Discipline2RealizationWayRelation discipline = chosenDiscipline.getEnrollmentCampaignDiscipline();
                    List<SubjectPassForm> passForms = passFormsByDiscipline == null ? null : passFormsByDiscipline.get(discipline.getId());
                    if (passForms == null)
                    {
                        if (mark != null && mark > 0)
                            passFormEge[0] = true;
                    }
                    else
                    {
                        passForms.forEach(passForm -> {
                            switch (passForm.getCode())
                            {
                                case SubjectPassFormCodes.STATE_EXAM:
                                case SubjectPassFormCodes.STATE_EXAM_INTERNAL:
                                    passFormEge[0] = true;
                                    break;
                                default:
                                    passFormInner[0] = true;
                            }
                        });
                    }
                    markStr = mark == null ? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(mark);

                }
                if (!_model.getReport().isWithoutDetailSumMark())
                    result.add(markStr);
            }
        }

        if (_model.getEnrollmentCampaign().isUseIndividualProgressInAmountMark())
            result.add(String.valueOf(row._individualMark == null ? 0 : row._individualMark));

        result.add(_entrantWithRecommendationIds.contains(entrant.getId()) ? "Да" : "Нет");

        if (!_model.getReport().isWithoutDocumentInfo())
            result.add(requestedEnrollmentDirection.isOriginalDocumentHandedIn() ? "Оригинал" : "Копия");
        if (!_model.getReport().isWithoutAgree4Enrollment())
            result.add(requestedEnrollmentDirection.isAgree4Enrollment() ? "Да" : "Нет");

        result.add((passFormInner[0] ? PASS_FORM_INNER : "") +
                           ((passFormEge[0] && passFormInner[0]) ? "/ " : "") +
                           (passFormEge[0] ? PASS_FORM_EGE : "")
        );

        if (_model.getReport().isShowRequestedDirectionPriority())
            result.add(String.valueOf(requestedEnrollmentDirection.getPriority()));


        return result.toArray(new String[result.size()]);
    }

    protected RtfRowIntercepterBase getRowIntercepterBase(EnrollmentDirection direction)
    {
        return new RtfRowIntercepterBase()
        {
            @Override
            public void beforeModify(RtfTable table, int currentRowIndex)
            {
                RtfRow headRow = table.getRowList().get(currentRowIndex - 1);
                List<RtfCell> headerCellList = headRow.getCellList();

                RtfRow firstRow = table.getRowList().get(currentRowIndex);
                List<RtfCell> cellList = firstRow.getCellList();

                List<RtfCell> forHeaderDelete = new ArrayList<>();
                List<RtfCell> forDelete = new ArrayList<>();

                if (_model.getReport().isWithoutDocumentInfo())
                {
                    forHeaderDelete.add(headerCellList.get(7));
                    forDelete.add(cellList.get(7));
                }

                if (_model.getReport().isWithoutAgree4Enrollment())
                {
                    forHeaderDelete.add(headerCellList.get(8));
                    forDelete.add(cellList.get(8));
                }

                if (!_model.getReport().isShowRequestedDirectionPriority())
                {
                    forHeaderDelete.add(headerCellList.get(10));
                    forDelete.add(cellList.get(10));
                }

                if (_model.getReport().isWithoutDetailSumMark())
                {
                    forHeaderDelete.add(headerCellList.get(4));
                    forDelete.add(cellList.get(4));
                }
                else
                {
                    EnrollmentDirectionExamSetData examSetData = _directionExamSetDataMap.get(direction);
                    ExamSetStructure examSetStructure = examSetData == null ? null : examSetData.getExamSetStructure();
                    if (examSetStructure != null && !examSetStructure.getItemList().isEmpty())
                    {
                        // разбиваем колонку по количеству вступительных испытаний в наборе
                        int[] parts = new int[examSetStructure.getItemList().size()];
                        Arrays.fill(parts, 1);

                        RtfUtil.splitRow(headRow, 4, (newCell, index) -> {
                                             ExamSetStructureItem structureItem = examSetStructure.getItemList().get(index);

                                             RtfString rtfString = new RtfString();
                                             if (_model.getReport().isShowDisciplineTitles())
                                             {
                                                 // сейчас хитро, надо выбрать по всем категориям самое короткое название
                                                 int length = Integer.MAX_VALUE;
                                                 List<String> columnTitleList = new ArrayList<>();
                                                 for (StudentCategory studentCategory : _model.getStudentCategoryList())
                                                 {
                                                     // тут не должно быть NPE по построению
                                                     ExamSetItem item = examSetData.getCategoryExamSet(studentCategory).get(index);
                                                     List<String> titleList = new ArrayList<>();
                                                     titleList.add(item.getSubject().getShortTitle());
                                                     titleList.addAll(item.getChoice().stream().map(SetDiscipline::getShortTitle).collect(Collectors.toList()));

                                                     String title = StringUtils.join(titleList, " ");
                                                     if (title.length() < length)
                                                     {
                                                         length = title.length();
                                                         columnTitleList = titleList;
                                                     }
                                                 }
                                                 Collections.sort(columnTitleList);
                                                 for (Iterator<String> iterator = columnTitleList.iterator(); iterator.hasNext(); )
                                                 {
                                                     rtfString.append(iterator.next());
                                                     if (iterator.hasNext()) rtfString.append("/").append(IRtfData.LINE);
                                                 }
                                             }
                                             else
                                                 rtfString.append(structureItem.getKind().getShortTitle());

                                             newCell.setElementList(rtfString.toList());
                                         },
                                         parts);

                        RtfUtil.splitRow(firstRow, 4, null, parts);
                    }
                }

                headerCellList.removeAll(forHeaderDelete);
                cellList.removeAll(forDelete);
            }
        };
    }

    protected String getRegNumber(RequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        return requestedEnrollmentDirection.getStringNumber();
    }


    protected List<EnrollmentDirection> getEnrollmentDirectionList()
    {
        if (!_model.isByAllEnrollmentDirections())
            return Collections.singletonList(EnrollmentDirectionUtil.getEnrollmentDirection(_model, _session));

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EnrollmentDirection.ENTITY_CLASS, "d")
                .column(property("d"))
                .where(eq(property("d", EnrollmentDirection.enrollmentCampaign()), value(_model.getEnrollmentCampaign())));

        FilterUtils.applySelectFilter(builder, "d", EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().s(), _model.getQualificationList());
        FilterUtils.applySelectFilter(builder, "d", EnrollmentDirection.educationOrgUnit().developForm().s(), _model.getDevelopFormList());
        FilterUtils.applySelectFilter(builder, "d", EnrollmentDirection.educationOrgUnit().developCondition().s(), _model.getDevelopConditionList());

        List<EnrollmentDirection> directions = builder.createStatement(_session).list();
        directions.sort(ITitled.TITLED_COMPARATOR);

        return directions;
    }


    protected static class ReportRow
    {
        protected RequestedEnrollmentDirection _direction;
        protected Integer _individualMark;
        protected boolean _withBenefits;
        protected Map<Long, ChosenEntranceDiscipline> _chosenDisciplinesByWay;
        private List<ChosenEntranceDiscipline> _chosenDisciplineSortedList;
        private Double _sumMark;
        private boolean _withRecommendations;

        protected String getFio()
        {
            return _direction.getEntrantRequest().getEntrant().getPerson().getFullFio();
        }

        protected double getSumMark()
        {
            if (_sumMark == null)
            {
                _sumMark = _chosenDisciplinesByWay.values().stream()
                        .filter(Objects::nonNull)
                        .filter(chosen -> chosen.getFinalMark() != null)
                        .mapToDouble(ChosenEntranceDisciplineGen::getFinalMark)
                        .sum();
                if (_direction.getEnrollmentDirection().getEnrollmentCampaign().isUseIndividualProgressInAmountMark() && _individualMark != null)
                    _sumMark += _individualMark;
            }
            return _sumMark;
        }

        protected static Comparator<ReportRow> getComparator(boolean originalsFirst)
        {
            return (o1, o2) -> {
                int result;

                if (originalsFirst)
                {
                    result = Boolean.compare(o2._direction.isOriginalDocumentHandedIn(), o1._direction.isOriginalDocumentHandedIn());
                    if (result != 0) return result;
                }
                result = Double.compare(o2.getSumMark(), o1.getSumMark());
                if (result != 0) return result;

                Collection<ChosenEntranceDiscipline> chosenSet1 = o1._chosenDisciplineSortedList;
                Collection<ChosenEntranceDiscipline> chosenSet2 = o2._chosenDisciplineSortedList;
                if (CollectionUtils.isEmpty(chosenSet1))
                {
                    if (CollectionUtils.isEmpty(chosenSet2)) return 0;
                    else return 1;
                }
                else
                {
                    if (CollectionUtils.isEmpty(chosenSet2)) return -1;
                    else
                    {
                        Iterator<ChosenEntranceDiscipline> i1 = chosenSet1.iterator();
                        Iterator<ChosenEntranceDiscipline> i2 = chosenSet2.iterator();

                        while (i1.hasNext())
                        {
                            if (!i2.hasNext()) return -1;

                            ChosenEntranceDiscipline d1 = i1.next();
                            ChosenEntranceDiscipline d2 = i2.next();
                            result = Double.compare(d2 == null ? 0 : d2.getFinalMark() == null ? 0 : d2.getFinalMark(),
                                                    d1 == null ? 0 : d1.getFinalMark() == null ? 0 : d1.getFinalMark());
                            if (result != 0) return result;
                        }

                        if (i2.hasNext()) return 1;
                    }
                }

                // преимущественное право
                result = Boolean.compare(o2._withRecommendations, o1._withRecommendations);
                if (result != 0) return result;

                // льготы
                result = Boolean.compare(o2._withBenefits, o1._withBenefits);
                if (result != 0) return result;

                return CommonCollator.RUSSIAN_COLLATOR.compare(o1.getFio(), o2.getFio());
            };
        }
    }

    class ChosenEntranceDisciplineComparator implements Comparator<ChosenEntranceDiscipline>
    {
        Map<Long, EntranceDiscipline> _disciplines;

        ChosenEntranceDisciplineComparator(Map<Long, EntranceDiscipline> disciplines)
        {
            _disciplines = disciplines;
        }

        @Override
        public int compare(ChosenEntranceDiscipline o1, ChosenEntranceDiscipline o2)
        {
            if (_disciplines == null)
                return 0;

            EntranceDiscipline d1 = o1 == null ? null : _disciplines.get(o1.getEnrollmentCampaignDiscipline().getId());
            Integer p1 = d1 == null ? null : d1.getPriority();
            EntranceDiscipline d2 = o1 == null ? null : _disciplines.get(o2.getEnrollmentCampaignDiscipline().getId());
            Integer p2 = d2 == null ? null : d2.getPriority();

            if (p1 == null)
            {
                if (p2 == null) return 0;
                else return -1;
            }
            else
            {
                if (p2 == null) return 1;
                else return Integer.compare(p1, p2);
            }

        }
    }
}
