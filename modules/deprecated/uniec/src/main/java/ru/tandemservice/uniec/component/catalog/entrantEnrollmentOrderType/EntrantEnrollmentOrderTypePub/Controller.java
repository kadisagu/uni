/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.catalog.entrantEnrollmentOrderType.EntrantEnrollmentOrderTypePub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;

/**
 * @author AutoGenerator
 * Created on 09.11.2009
 */
public class Controller extends DefaultCatalogPubController<EntrantEnrollmentOrderType, Model, IDAO>
{
    @Override
    @SuppressWarnings({"unchecked"})
    protected DynamicListDataSource createListDataSource(IBusinessComponent context)
    {
        Model model = getModel(context);

        DynamicListDataSource<EntrantEnrollmentOrderType> dataSource = new DynamicListDataSource<EntrantEnrollmentOrderType>(context, this);
        dataSource.addColumn(getCatalogItemLinkColumn(model, "Название", EntrantEnrollmentOrderType.P_TITLE));
        dataSource.addColumn(new SimpleColumn("Сокращенное название", EntrantEnrollmentOrderType.P_SHORT_TITLE));
        dataSource.addColumn(new SimpleColumn("Группа приказов", "group").setClickable(false).setOrderable(false));

        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));
        if (model.isUserCatalog())
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem", "Удалить элемент «{0}» из справочника?", EntrantEnrollmentOrderType.P_TITLE).setPermissionKey(model.getCatalogItemDelete()).setDisableHandler(model.getDisabledEntityHandler()));
        return dataSource;
    }
}
