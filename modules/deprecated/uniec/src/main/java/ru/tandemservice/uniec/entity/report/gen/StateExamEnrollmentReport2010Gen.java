package ru.tandemservice.uniec.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сводка по результатам экзаменов или по результатам в свидетельствах ЕГЭ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StateExamEnrollmentReport2010Gen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010";
    public static final String ENTITY_NAME = "stateExamEnrollmentReport2010";
    public static final int VERSION_HASH = 466014435;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONTENT = "content";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String P_DATE_FROM = "dateFrom";
    public static final String P_DATE_TO = "dateTo";
    public static final String P_PRINT_FORM = "printForm";
    public static final String P_ENROLLMENT_CAMP_STAGE = "enrollmentCampStage";
    public static final String P_REPORT_ALGORITHM = "reportAlgorithm";
    public static final String P_STATE_EXAM_PERIOD = "stateExamPeriod";
    public static final String P_DEVELOP_CONDITION_TITLE = "developConditionTitle";
    public static final String P_QUALIFICATION_TITLE = "qualificationTitle";
    public static final String P_FORMATIVE_ORG_UNIT_TITLE = "formativeOrgUnitTitle";
    public static final String P_TERRITORIAL_ORG_UNIT_TITLE = "territorialOrgUnitTitle";
    public static final String P_STUDENT_CATEGORY_TITLE = "studentCategoryTitle";
    public static final String P_SHOW_EDU_GROUPS = "showEduGroups";
    public static final String P_GROUP_BY_BRANCH_AND_REPRESENTATION = "groupByBranchAndRepresentation";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_DEVELOP_FORM = "developForm";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String P_EXCLUDE_PARALLEL = "excludeParallel";

    private DatabaseFile _content;     // Печатная форма
    private Date _formingDate;     // Дата формирования
    private Date _dateFrom;     // Заявления с
    private Date _dateTo;     // Заявления по
    private String _printForm;     // Печатная форма
    private String _enrollmentCampStage;     // Стадия приемной кампании
    private String _reportAlgorithm;     // По результатам
    private String _stateExamPeriod;     // Период сдачи ЕГЭ
    private String _developConditionTitle; 
    private String _qualificationTitle; 
    private String _formativeOrgUnitTitle; 
    private String _territorialOrgUnitTitle; 
    private String _studentCategoryTitle; 
    private Boolean _showEduGroups;     // Выделять укрупненные группы
    private Boolean _groupByBranchAndRepresentation;     // Группировать по филиалам и представительствам
    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private DevelopForm _developForm;     // Форма освоения
    private CompensationType _compensationType;     // Вид возмещения затрат
    private Boolean _excludeParallel;     // Исключая поступивших на параллельное освоение образовательных программ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     */
    @NotNull
    public Date getDateFrom()
    {
        return _dateFrom;
    }

    /**
     * @param dateFrom Заявления с. Свойство не может быть null.
     */
    public void setDateFrom(Date dateFrom)
    {
        dirty(_dateFrom, dateFrom);
        _dateFrom = dateFrom;
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     */
    @NotNull
    public Date getDateTo()
    {
        return _dateTo;
    }

    /**
     * @param dateTo Заявления по. Свойство не может быть null.
     */
    public void setDateTo(Date dateTo)
    {
        dirty(_dateTo, dateTo);
        _dateTo = dateTo;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPrintForm()
    {
        return _printForm;
    }

    /**
     * @param printForm Печатная форма. Свойство не может быть null.
     */
    public void setPrintForm(String printForm)
    {
        dirty(_printForm, printForm);
        _printForm = printForm;
    }

    /**
     * @return Стадия приемной кампании. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getEnrollmentCampStage()
    {
        return _enrollmentCampStage;
    }

    /**
     * @param enrollmentCampStage Стадия приемной кампании. Свойство не может быть null.
     */
    public void setEnrollmentCampStage(String enrollmentCampStage)
    {
        dirty(_enrollmentCampStage, enrollmentCampStage);
        _enrollmentCampStage = enrollmentCampStage;
    }

    /**
     * @return По результатам. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getReportAlgorithm()
    {
        return _reportAlgorithm;
    }

    /**
     * @param reportAlgorithm По результатам. Свойство не может быть null.
     */
    public void setReportAlgorithm(String reportAlgorithm)
    {
        dirty(_reportAlgorithm, reportAlgorithm);
        _reportAlgorithm = reportAlgorithm;
    }

    /**
     * @return Период сдачи ЕГЭ.
     */
    @Length(max=255)
    public String getStateExamPeriod()
    {
        return _stateExamPeriod;
    }

    /**
     * @param stateExamPeriod Период сдачи ЕГЭ.
     */
    public void setStateExamPeriod(String stateExamPeriod)
    {
        dirty(_stateExamPeriod, stateExamPeriod);
        _stateExamPeriod = stateExamPeriod;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getDevelopConditionTitle()
    {
        return _developConditionTitle;
    }

    /**
     * @param developConditionTitle 
     */
    public void setDevelopConditionTitle(String developConditionTitle)
    {
        dirty(_developConditionTitle, developConditionTitle);
        _developConditionTitle = developConditionTitle;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getQualificationTitle()
    {
        return _qualificationTitle;
    }

    /**
     * @param qualificationTitle 
     */
    public void setQualificationTitle(String qualificationTitle)
    {
        dirty(_qualificationTitle, qualificationTitle);
        _qualificationTitle = qualificationTitle;
    }

    /**
     * @return 
     */
    public String getFormativeOrgUnitTitle()
    {
        return _formativeOrgUnitTitle;
    }

    /**
     * @param formativeOrgUnitTitle 
     */
    public void setFormativeOrgUnitTitle(String formativeOrgUnitTitle)
    {
        dirty(_formativeOrgUnitTitle, formativeOrgUnitTitle);
        _formativeOrgUnitTitle = formativeOrgUnitTitle;
    }

    /**
     * @return 
     */
    public String getTerritorialOrgUnitTitle()
    {
        return _territorialOrgUnitTitle;
    }

    /**
     * @param territorialOrgUnitTitle 
     */
    public void setTerritorialOrgUnitTitle(String territorialOrgUnitTitle)
    {
        dirty(_territorialOrgUnitTitle, territorialOrgUnitTitle);
        _territorialOrgUnitTitle = territorialOrgUnitTitle;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getStudentCategoryTitle()
    {
        return _studentCategoryTitle;
    }

    /**
     * @param studentCategoryTitle 
     */
    public void setStudentCategoryTitle(String studentCategoryTitle)
    {
        dirty(_studentCategoryTitle, studentCategoryTitle);
        _studentCategoryTitle = studentCategoryTitle;
    }

    /**
     * @return Выделять укрупненные группы.
     */
    public Boolean getShowEduGroups()
    {
        return _showEduGroups;
    }

    /**
     * @param showEduGroups Выделять укрупненные группы.
     */
    public void setShowEduGroups(Boolean showEduGroups)
    {
        dirty(_showEduGroups, showEduGroups);
        _showEduGroups = showEduGroups;
    }

    /**
     * @return Группировать по филиалам и представительствам.
     */
    public Boolean getGroupByBranchAndRepresentation()
    {
        return _groupByBranchAndRepresentation;
    }

    /**
     * @param groupByBranchAndRepresentation Группировать по филиалам и представительствам.
     */
    public void setGroupByBranchAndRepresentation(Boolean groupByBranchAndRepresentation)
    {
        dirty(_groupByBranchAndRepresentation, groupByBranchAndRepresentation);
        _groupByBranchAndRepresentation = groupByBranchAndRepresentation;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения. Свойство не может быть null.
     */
    public void setDevelopForm(DevelopForm developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Вид возмещения затрат.
     */
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Исключая поступивших на параллельное освоение образовательных программ.
     */
    public Boolean getExcludeParallel()
    {
        return _excludeParallel;
    }

    /**
     * @param excludeParallel Исключая поступивших на параллельное освоение образовательных программ.
     */
    public void setExcludeParallel(Boolean excludeParallel)
    {
        dirty(_excludeParallel, excludeParallel);
        _excludeParallel = excludeParallel;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StateExamEnrollmentReport2010Gen)
        {
            setContent(((StateExamEnrollmentReport2010)another).getContent());
            setFormingDate(((StateExamEnrollmentReport2010)another).getFormingDate());
            setDateFrom(((StateExamEnrollmentReport2010)another).getDateFrom());
            setDateTo(((StateExamEnrollmentReport2010)another).getDateTo());
            setPrintForm(((StateExamEnrollmentReport2010)another).getPrintForm());
            setEnrollmentCampStage(((StateExamEnrollmentReport2010)another).getEnrollmentCampStage());
            setReportAlgorithm(((StateExamEnrollmentReport2010)another).getReportAlgorithm());
            setStateExamPeriod(((StateExamEnrollmentReport2010)another).getStateExamPeriod());
            setDevelopConditionTitle(((StateExamEnrollmentReport2010)another).getDevelopConditionTitle());
            setQualificationTitle(((StateExamEnrollmentReport2010)another).getQualificationTitle());
            setFormativeOrgUnitTitle(((StateExamEnrollmentReport2010)another).getFormativeOrgUnitTitle());
            setTerritorialOrgUnitTitle(((StateExamEnrollmentReport2010)another).getTerritorialOrgUnitTitle());
            setStudentCategoryTitle(((StateExamEnrollmentReport2010)another).getStudentCategoryTitle());
            setShowEduGroups(((StateExamEnrollmentReport2010)another).getShowEduGroups());
            setGroupByBranchAndRepresentation(((StateExamEnrollmentReport2010)another).getGroupByBranchAndRepresentation());
            setEnrollmentCampaign(((StateExamEnrollmentReport2010)another).getEnrollmentCampaign());
            setDevelopForm(((StateExamEnrollmentReport2010)another).getDevelopForm());
            setCompensationType(((StateExamEnrollmentReport2010)another).getCompensationType());
            setExcludeParallel(((StateExamEnrollmentReport2010)another).getExcludeParallel());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StateExamEnrollmentReport2010Gen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StateExamEnrollmentReport2010.class;
        }

        public T newInstance()
        {
            return (T) new StateExamEnrollmentReport2010();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "content":
                    return obj.getContent();
                case "formingDate":
                    return obj.getFormingDate();
                case "dateFrom":
                    return obj.getDateFrom();
                case "dateTo":
                    return obj.getDateTo();
                case "printForm":
                    return obj.getPrintForm();
                case "enrollmentCampStage":
                    return obj.getEnrollmentCampStage();
                case "reportAlgorithm":
                    return obj.getReportAlgorithm();
                case "stateExamPeriod":
                    return obj.getStateExamPeriod();
                case "developConditionTitle":
                    return obj.getDevelopConditionTitle();
                case "qualificationTitle":
                    return obj.getQualificationTitle();
                case "formativeOrgUnitTitle":
                    return obj.getFormativeOrgUnitTitle();
                case "territorialOrgUnitTitle":
                    return obj.getTerritorialOrgUnitTitle();
                case "studentCategoryTitle":
                    return obj.getStudentCategoryTitle();
                case "showEduGroups":
                    return obj.getShowEduGroups();
                case "groupByBranchAndRepresentation":
                    return obj.getGroupByBranchAndRepresentation();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "developForm":
                    return obj.getDevelopForm();
                case "compensationType":
                    return obj.getCompensationType();
                case "excludeParallel":
                    return obj.getExcludeParallel();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "dateFrom":
                    obj.setDateFrom((Date) value);
                    return;
                case "dateTo":
                    obj.setDateTo((Date) value);
                    return;
                case "printForm":
                    obj.setPrintForm((String) value);
                    return;
                case "enrollmentCampStage":
                    obj.setEnrollmentCampStage((String) value);
                    return;
                case "reportAlgorithm":
                    obj.setReportAlgorithm((String) value);
                    return;
                case "stateExamPeriod":
                    obj.setStateExamPeriod((String) value);
                    return;
                case "developConditionTitle":
                    obj.setDevelopConditionTitle((String) value);
                    return;
                case "qualificationTitle":
                    obj.setQualificationTitle((String) value);
                    return;
                case "formativeOrgUnitTitle":
                    obj.setFormativeOrgUnitTitle((String) value);
                    return;
                case "territorialOrgUnitTitle":
                    obj.setTerritorialOrgUnitTitle((String) value);
                    return;
                case "studentCategoryTitle":
                    obj.setStudentCategoryTitle((String) value);
                    return;
                case "showEduGroups":
                    obj.setShowEduGroups((Boolean) value);
                    return;
                case "groupByBranchAndRepresentation":
                    obj.setGroupByBranchAndRepresentation((Boolean) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((DevelopForm) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "excludeParallel":
                    obj.setExcludeParallel((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "content":
                        return true;
                case "formingDate":
                        return true;
                case "dateFrom":
                        return true;
                case "dateTo":
                        return true;
                case "printForm":
                        return true;
                case "enrollmentCampStage":
                        return true;
                case "reportAlgorithm":
                        return true;
                case "stateExamPeriod":
                        return true;
                case "developConditionTitle":
                        return true;
                case "qualificationTitle":
                        return true;
                case "formativeOrgUnitTitle":
                        return true;
                case "territorialOrgUnitTitle":
                        return true;
                case "studentCategoryTitle":
                        return true;
                case "showEduGroups":
                        return true;
                case "groupByBranchAndRepresentation":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "developForm":
                        return true;
                case "compensationType":
                        return true;
                case "excludeParallel":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "content":
                    return true;
                case "formingDate":
                    return true;
                case "dateFrom":
                    return true;
                case "dateTo":
                    return true;
                case "printForm":
                    return true;
                case "enrollmentCampStage":
                    return true;
                case "reportAlgorithm":
                    return true;
                case "stateExamPeriod":
                    return true;
                case "developConditionTitle":
                    return true;
                case "qualificationTitle":
                    return true;
                case "formativeOrgUnitTitle":
                    return true;
                case "territorialOrgUnitTitle":
                    return true;
                case "studentCategoryTitle":
                    return true;
                case "showEduGroups":
                    return true;
                case "groupByBranchAndRepresentation":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "developForm":
                    return true;
                case "compensationType":
                    return true;
                case "excludeParallel":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "content":
                    return DatabaseFile.class;
                case "formingDate":
                    return Date.class;
                case "dateFrom":
                    return Date.class;
                case "dateTo":
                    return Date.class;
                case "printForm":
                    return String.class;
                case "enrollmentCampStage":
                    return String.class;
                case "reportAlgorithm":
                    return String.class;
                case "stateExamPeriod":
                    return String.class;
                case "developConditionTitle":
                    return String.class;
                case "qualificationTitle":
                    return String.class;
                case "formativeOrgUnitTitle":
                    return String.class;
                case "territorialOrgUnitTitle":
                    return String.class;
                case "studentCategoryTitle":
                    return String.class;
                case "showEduGroups":
                    return Boolean.class;
                case "groupByBranchAndRepresentation":
                    return Boolean.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "developForm":
                    return DevelopForm.class;
                case "compensationType":
                    return CompensationType.class;
                case "excludeParallel":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StateExamEnrollmentReport2010> _dslPath = new Path<StateExamEnrollmentReport2010>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StateExamEnrollmentReport2010");
    }
            

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getDateFrom()
     */
    public static PropertyPath<Date> dateFrom()
    {
        return _dslPath.dateFrom();
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getDateTo()
     */
    public static PropertyPath<Date> dateTo()
    {
        return _dslPath.dateTo();
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getPrintForm()
     */
    public static PropertyPath<String> printForm()
    {
        return _dslPath.printForm();
    }

    /**
     * @return Стадия приемной кампании. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getEnrollmentCampStage()
     */
    public static PropertyPath<String> enrollmentCampStage()
    {
        return _dslPath.enrollmentCampStage();
    }

    /**
     * @return По результатам. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getReportAlgorithm()
     */
    public static PropertyPath<String> reportAlgorithm()
    {
        return _dslPath.reportAlgorithm();
    }

    /**
     * @return Период сдачи ЕГЭ.
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getStateExamPeriod()
     */
    public static PropertyPath<String> stateExamPeriod()
    {
        return _dslPath.stateExamPeriod();
    }

    /**
     * @return 
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getDevelopConditionTitle()
     */
    public static PropertyPath<String> developConditionTitle()
    {
        return _dslPath.developConditionTitle();
    }

    /**
     * @return 
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getQualificationTitle()
     */
    public static PropertyPath<String> qualificationTitle()
    {
        return _dslPath.qualificationTitle();
    }

    /**
     * @return 
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getFormativeOrgUnitTitle()
     */
    public static PropertyPath<String> formativeOrgUnitTitle()
    {
        return _dslPath.formativeOrgUnitTitle();
    }

    /**
     * @return 
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getTerritorialOrgUnitTitle()
     */
    public static PropertyPath<String> territorialOrgUnitTitle()
    {
        return _dslPath.territorialOrgUnitTitle();
    }

    /**
     * @return 
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getStudentCategoryTitle()
     */
    public static PropertyPath<String> studentCategoryTitle()
    {
        return _dslPath.studentCategoryTitle();
    }

    /**
     * @return Выделять укрупненные группы.
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getShowEduGroups()
     */
    public static PropertyPath<Boolean> showEduGroups()
    {
        return _dslPath.showEduGroups();
    }

    /**
     * @return Группировать по филиалам и представительствам.
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getGroupByBranchAndRepresentation()
     */
    public static PropertyPath<Boolean> groupByBranchAndRepresentation()
    {
        return _dslPath.groupByBranchAndRepresentation();
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getDevelopForm()
     */
    public static DevelopForm.Path<DevelopForm> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Исключая поступивших на параллельное освоение образовательных программ.
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getExcludeParallel()
     */
    public static PropertyPath<Boolean> excludeParallel()
    {
        return _dslPath.excludeParallel();
    }

    public static class Path<E extends StateExamEnrollmentReport2010> extends EntityPath<E>
    {
        private DatabaseFile.Path<DatabaseFile> _content;
        private PropertyPath<Date> _formingDate;
        private PropertyPath<Date> _dateFrom;
        private PropertyPath<Date> _dateTo;
        private PropertyPath<String> _printForm;
        private PropertyPath<String> _enrollmentCampStage;
        private PropertyPath<String> _reportAlgorithm;
        private PropertyPath<String> _stateExamPeriod;
        private PropertyPath<String> _developConditionTitle;
        private PropertyPath<String> _qualificationTitle;
        private PropertyPath<String> _formativeOrgUnitTitle;
        private PropertyPath<String> _territorialOrgUnitTitle;
        private PropertyPath<String> _studentCategoryTitle;
        private PropertyPath<Boolean> _showEduGroups;
        private PropertyPath<Boolean> _groupByBranchAndRepresentation;
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private DevelopForm.Path<DevelopForm> _developForm;
        private CompensationType.Path<CompensationType> _compensationType;
        private PropertyPath<Boolean> _excludeParallel;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(StateExamEnrollmentReport2010Gen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getDateFrom()
     */
        public PropertyPath<Date> dateFrom()
        {
            if(_dateFrom == null )
                _dateFrom = new PropertyPath<Date>(StateExamEnrollmentReport2010Gen.P_DATE_FROM, this);
            return _dateFrom;
        }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getDateTo()
     */
        public PropertyPath<Date> dateTo()
        {
            if(_dateTo == null )
                _dateTo = new PropertyPath<Date>(StateExamEnrollmentReport2010Gen.P_DATE_TO, this);
            return _dateTo;
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getPrintForm()
     */
        public PropertyPath<String> printForm()
        {
            if(_printForm == null )
                _printForm = new PropertyPath<String>(StateExamEnrollmentReport2010Gen.P_PRINT_FORM, this);
            return _printForm;
        }

    /**
     * @return Стадия приемной кампании. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getEnrollmentCampStage()
     */
        public PropertyPath<String> enrollmentCampStage()
        {
            if(_enrollmentCampStage == null )
                _enrollmentCampStage = new PropertyPath<String>(StateExamEnrollmentReport2010Gen.P_ENROLLMENT_CAMP_STAGE, this);
            return _enrollmentCampStage;
        }

    /**
     * @return По результатам. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getReportAlgorithm()
     */
        public PropertyPath<String> reportAlgorithm()
        {
            if(_reportAlgorithm == null )
                _reportAlgorithm = new PropertyPath<String>(StateExamEnrollmentReport2010Gen.P_REPORT_ALGORITHM, this);
            return _reportAlgorithm;
        }

    /**
     * @return Период сдачи ЕГЭ.
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getStateExamPeriod()
     */
        public PropertyPath<String> stateExamPeriod()
        {
            if(_stateExamPeriod == null )
                _stateExamPeriod = new PropertyPath<String>(StateExamEnrollmentReport2010Gen.P_STATE_EXAM_PERIOD, this);
            return _stateExamPeriod;
        }

    /**
     * @return 
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getDevelopConditionTitle()
     */
        public PropertyPath<String> developConditionTitle()
        {
            if(_developConditionTitle == null )
                _developConditionTitle = new PropertyPath<String>(StateExamEnrollmentReport2010Gen.P_DEVELOP_CONDITION_TITLE, this);
            return _developConditionTitle;
        }

    /**
     * @return 
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getQualificationTitle()
     */
        public PropertyPath<String> qualificationTitle()
        {
            if(_qualificationTitle == null )
                _qualificationTitle = new PropertyPath<String>(StateExamEnrollmentReport2010Gen.P_QUALIFICATION_TITLE, this);
            return _qualificationTitle;
        }

    /**
     * @return 
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getFormativeOrgUnitTitle()
     */
        public PropertyPath<String> formativeOrgUnitTitle()
        {
            if(_formativeOrgUnitTitle == null )
                _formativeOrgUnitTitle = new PropertyPath<String>(StateExamEnrollmentReport2010Gen.P_FORMATIVE_ORG_UNIT_TITLE, this);
            return _formativeOrgUnitTitle;
        }

    /**
     * @return 
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getTerritorialOrgUnitTitle()
     */
        public PropertyPath<String> territorialOrgUnitTitle()
        {
            if(_territorialOrgUnitTitle == null )
                _territorialOrgUnitTitle = new PropertyPath<String>(StateExamEnrollmentReport2010Gen.P_TERRITORIAL_ORG_UNIT_TITLE, this);
            return _territorialOrgUnitTitle;
        }

    /**
     * @return 
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getStudentCategoryTitle()
     */
        public PropertyPath<String> studentCategoryTitle()
        {
            if(_studentCategoryTitle == null )
                _studentCategoryTitle = new PropertyPath<String>(StateExamEnrollmentReport2010Gen.P_STUDENT_CATEGORY_TITLE, this);
            return _studentCategoryTitle;
        }

    /**
     * @return Выделять укрупненные группы.
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getShowEduGroups()
     */
        public PropertyPath<Boolean> showEduGroups()
        {
            if(_showEduGroups == null )
                _showEduGroups = new PropertyPath<Boolean>(StateExamEnrollmentReport2010Gen.P_SHOW_EDU_GROUPS, this);
            return _showEduGroups;
        }

    /**
     * @return Группировать по филиалам и представительствам.
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getGroupByBranchAndRepresentation()
     */
        public PropertyPath<Boolean> groupByBranchAndRepresentation()
        {
            if(_groupByBranchAndRepresentation == null )
                _groupByBranchAndRepresentation = new PropertyPath<Boolean>(StateExamEnrollmentReport2010Gen.P_GROUP_BY_BRANCH_AND_REPRESENTATION, this);
            return _groupByBranchAndRepresentation;
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getDevelopForm()
     */
        public DevelopForm.Path<DevelopForm> developForm()
        {
            if(_developForm == null )
                _developForm = new DevelopForm.Path<DevelopForm>(L_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Исключая поступивших на параллельное освоение образовательных программ.
     * @see ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010#getExcludeParallel()
     */
        public PropertyPath<Boolean> excludeParallel()
        {
            if(_excludeParallel == null )
                _excludeParallel = new PropertyPath<Boolean>(StateExamEnrollmentReport2010Gen.P_EXCLUDE_PARALLEL, this);
            return _excludeParallel;
        }

        public Class getEntityClass()
        {
            return StateExamEnrollmentReport2010.class;
        }

        public String getEntityName()
        {
            return "stateExamEnrollmentReport2010";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
