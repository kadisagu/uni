/* $Id$ */
package ru.tandemservice.uniec.component.listextract.ent1.e1;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniec.entity.orders.SplitBudgetBachelorEntrantsStuListExtract;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 13.07.2012
 */
public class SplitBudgetBachelorEntrantsStuListExtractDao extends UniBaseDao implements IExtractComponentDao<SplitBudgetBachelorEntrantsStuListExtract>
{
    @Override
    public void doCommit(SplitBudgetBachelorEntrantsStuListExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveListExtractText(extract, MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);

        extract.setEducationOrgUnitOld(extract.getEntity().getEducationOrgUnit());
        extract.getEntity().setEducationOrgUnit(extract.getEducationOrgUnitNew());
        update(extract.getEntity());

        // заполняем номер и дату приказа
        if (null == extract.getParagraph() || null == extract.getParagraph().getOrder())
            return;
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        } else
        {
        // DEV-4960
            extract.setPrevOrderDate(orderData.getSplitToSpecializationOrderDate());
            extract.setPrevOrderNumber(orderData.getSplitToSpecializationOrderNumber());
        }
        // DEV-4960
        orderData.setSplitToSpecializationOrderDate(extract.getParagraph().getOrder().getCommitDate());
        orderData.setSplitToSpecializationOrderNumber(extract.getParagraph().getOrder().getNumber());
        getSession().saveOrUpdate(orderData);
    }

    @Override
    public void doRollback(SplitBudgetBachelorEntrantsStuListExtract extract, Map parameters)
    {
        extract.getEntity().setEducationOrgUnit(extract.getEducationOrgUnitOld());

        // возвращаем предыдущие номер и дату приказа
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }
        // DEV-4960
        orderData.setSplitToSpecializationOrderDate(extract.getPrevOrderDate());
        orderData.setSplitToSpecializationOrderNumber(extract.getPrevOrderNumber());
        getSession().saveOrUpdate(orderData);
    }
}