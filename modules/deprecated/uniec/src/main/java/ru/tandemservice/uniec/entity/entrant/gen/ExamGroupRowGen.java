package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.ExamGroup;
import ru.tandemservice.uniec.entity.entrant.ExamGroupRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Строка экзаменационной группы
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ExamGroupRowGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.ExamGroupRow";
    public static final String ENTITY_NAME = "examGroupRow";
    public static final int VERSION_HASH = -876263544;
    private static IEntityMeta ENTITY_META;

    public static final String P_INDEX = "index";
    public static final String L_EXAM_GROUP = "examGroup";
    public static final String L_ENTRANT_REQUEST = "entrantRequest";

    private int _index;     // Номер строки
    private ExamGroup _examGroup;     // Экзаменационная группа
    private EntrantRequest _entrantRequest;     // Заявление абитуриента

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Номер строки. Свойство не может быть null.
     */
    @NotNull
    public int getIndex()
    {
        return _index;
    }

    /**
     * @param index Номер строки. Свойство не может быть null.
     */
    public void setIndex(int index)
    {
        dirty(_index, index);
        _index = index;
    }

    /**
     * @return Экзаменационная группа. Свойство не может быть null.
     */
    @NotNull
    public ExamGroup getExamGroup()
    {
        return _examGroup;
    }

    /**
     * @param examGroup Экзаменационная группа. Свойство не может быть null.
     */
    public void setExamGroup(ExamGroup examGroup)
    {
        dirty(_examGroup, examGroup);
        _examGroup = examGroup;
    }

    /**
     * @return Заявление абитуриента. Свойство не может быть null.
     */
    @NotNull
    public EntrantRequest getEntrantRequest()
    {
        return _entrantRequest;
    }

    /**
     * @param entrantRequest Заявление абитуриента. Свойство не может быть null.
     */
    public void setEntrantRequest(EntrantRequest entrantRequest)
    {
        dirty(_entrantRequest, entrantRequest);
        _entrantRequest = entrantRequest;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ExamGroupRowGen)
        {
            setIndex(((ExamGroupRow)another).getIndex());
            setExamGroup(((ExamGroupRow)another).getExamGroup());
            setEntrantRequest(((ExamGroupRow)another).getEntrantRequest());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ExamGroupRowGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ExamGroupRow.class;
        }

        public T newInstance()
        {
            return (T) new ExamGroupRow();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "index":
                    return obj.getIndex();
                case "examGroup":
                    return obj.getExamGroup();
                case "entrantRequest":
                    return obj.getEntrantRequest();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "index":
                    obj.setIndex((Integer) value);
                    return;
                case "examGroup":
                    obj.setExamGroup((ExamGroup) value);
                    return;
                case "entrantRequest":
                    obj.setEntrantRequest((EntrantRequest) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "index":
                        return true;
                case "examGroup":
                        return true;
                case "entrantRequest":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "index":
                    return true;
                case "examGroup":
                    return true;
                case "entrantRequest":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "index":
                    return Integer.class;
                case "examGroup":
                    return ExamGroup.class;
                case "entrantRequest":
                    return EntrantRequest.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ExamGroupRow> _dslPath = new Path<ExamGroupRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ExamGroupRow");
    }
            

    /**
     * @return Номер строки. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamGroupRow#getIndex()
     */
    public static PropertyPath<Integer> index()
    {
        return _dslPath.index();
    }

    /**
     * @return Экзаменационная группа. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamGroupRow#getExamGroup()
     */
    public static ExamGroup.Path<ExamGroup> examGroup()
    {
        return _dslPath.examGroup();
    }

    /**
     * @return Заявление абитуриента. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamGroupRow#getEntrantRequest()
     */
    public static EntrantRequest.Path<EntrantRequest> entrantRequest()
    {
        return _dslPath.entrantRequest();
    }

    public static class Path<E extends ExamGroupRow> extends EntityPath<E>
    {
        private PropertyPath<Integer> _index;
        private ExamGroup.Path<ExamGroup> _examGroup;
        private EntrantRequest.Path<EntrantRequest> _entrantRequest;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Номер строки. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamGroupRow#getIndex()
     */
        public PropertyPath<Integer> index()
        {
            if(_index == null )
                _index = new PropertyPath<Integer>(ExamGroupRowGen.P_INDEX, this);
            return _index;
        }

    /**
     * @return Экзаменационная группа. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamGroupRow#getExamGroup()
     */
        public ExamGroup.Path<ExamGroup> examGroup()
        {
            if(_examGroup == null )
                _examGroup = new ExamGroup.Path<ExamGroup>(L_EXAM_GROUP, this);
            return _examGroup;
        }

    /**
     * @return Заявление абитуриента. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamGroupRow#getEntrantRequest()
     */
        public EntrantRequest.Path<EntrantRequest> entrantRequest()
        {
            if(_entrantRequest == null )
                _entrantRequest = new EntrantRequest.Path<EntrantRequest>(L_ENTRANT_REQUEST, this);
            return _entrantRequest;
        }

        public Class getEntityClass()
        {
            return ExamGroupRow.class;
        }

        public String getEntityName()
        {
            return "examGroupRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
