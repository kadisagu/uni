package ru.tandemservice.uniec.component.report.EntrantStudentForm;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.utils.CommonYesNoUIObject;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.ArrayList;
import java.util.Date;

public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    @SuppressWarnings("deprecation")
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.setDateFrom(model.getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : model.getEnrollmentCampaign().getStartDate());
        model.setDateTo(CoreDateUtils.getDayLastTimeMoment(new Date()));

        model.setQualificationListModel(new QualificationModel(getSession()));
        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));
        model.setStudentCategoryListModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(StudentCategory.class)));

        model.setFormativeOrgUnitListModel(MultiEnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setTerritorialOrgUnitListModel(MultiEnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setEducationLevelHighSchoolListModel(MultiEnrollmentDirectionUtil.createEducationLevelsHighSchoolModel(model));
        model.setDevelopFormListModel(MultiEnrollmentDirectionUtil.createDevelopFormModel(model));
        model.setDevelopConditionListModel(MultiEnrollmentDirectionUtil.createDevelopConditionModel(model));
        model.setDevelopTechListModel(MultiEnrollmentDirectionUtil.createDevelopTechModel(model));
        model.setDevelopPeriodListModel(MultiEnrollmentDirectionUtil.createDevelopPeriodModel(model));

        model.setStudentCategoryList(new ArrayList<>());
        model.setQualificationList(new ArrayList<>());
        model.setFormativeOrgUnitList(new ArrayList<>());
        model.setTerritorialOrgUnitList(new ArrayList<>());
        model.setEducationLevelHighSchoolList(new ArrayList<>());
        model.setDevelopFormList(new ArrayList<>());
        model.setDevelopConditionList(new ArrayList<>());
        model.setDevelopTechList(new ArrayList<>());
        model.setDevelopPeriodList(new ArrayList<>());

        model.setParallelList(CommonYesNoUIObject.createYesNoList());

        model.setParameters(new MultiEnrollmentDirectionUtil.Parameters());
        model.getParameters().setRequired(false, MultiEnrollmentDirectionUtil.Parameters.Filter.values());
    }
}
