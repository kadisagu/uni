/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.ui.Pub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.IUniecComponents;
import ru.tandemservice.uniec.base.bo.EcDistribution.EcDistributionManager;
import ru.tandemservice.uniec.base.bo.EcDistribution.logic.EcSimpleDSHandler;
import ru.tandemservice.uniec.base.bo.EcDistribution.ui.AddEdit.EcDistributionAddEdit;
import ru.tandemservice.uniec.base.bo.EcDistribution.ui.AddEdit.EcDistributionAddEditUI;
import ru.tandemservice.uniec.base.bo.EcDistribution.ui.ApprovalDateEdit.EcDistributionApprovalDateEdit;
import ru.tandemservice.uniec.base.bo.EcDistribution.ui.EntrantAdd.EcDistributionEntrantAdd;
import ru.tandemservice.uniec.base.bo.EcDistribution.ui.EntrantAdd.EcDistributionEntrantAddUI;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.*;
import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager;
import ru.tandemservice.uniec.base.bo.EcPreEnroll.EcPreEnrollManager;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistribution;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.entrant.CompetitionGroup;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import java.util.*;

/**
 * @author Vasily Zhukov
 * @since 07.07.2011
 */
@State({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "distributionId", required = true),
    @Bind(key = "selectedTab", binding = "selectedTab")
})
public class EcDistributionPubUI extends UIPresenter
{
    private Long _distributionId;
    private IEcgDistribution _distribution;
    private String _typeTitle;
    private String _selectedTab;
    private String _ecgItemDisplayName;
    private String _distributionCategoryTitle;
    private boolean _enrollmentVisible;
    private Map<Long, String> _quotaMap;
    private Map<Long, String> _taQuotaMap;
    private ISelectModel _orderTypeModel;
    private boolean _preEnrollDivVisible;
    private boolean _headerCheckbox;
    private String _headerCheckboxScript;
    private EntrantEnrollmentOrderType _orderType;
    private Set<Long> _recommendedIds;

    @Override
    public void onComponentRefresh()
    {
        _distribution = DataAccessServices.dao().getNotNull(_distributionId);

        // основное или уточняющее распределение
        _typeTitle = getConfig().getProperty(_distribution instanceof EcgDistribution ? "ui.distributionTypeTitle" : "ui.distributionDetailTypeTitle");

        // по конкурсной группе или направлению приема
        _ecgItemDisplayName = getConfig().getProperty(_distribution.getConfig().getEcgItem() instanceof CompetitionGroup ? "ui.ecgItem.competitionGroup" : "ui.ecgItem.educationLevelHighSchool");

        // категория поступающего
        _distributionCategoryTitle = _distribution.getConfig().isSecondHighAdmission() ? EcDistributionManager.DISTRIBUTION_CATEGORY_SECOND_HIGH_ADMISSION.getTitle() : EcDistributionManager.DISTRIBUTION_CATEGORY_STUDENTS_AND_LISTENERS.getTitle();

        // нужно ли показывать колонки для предзачисления
        _enrollmentVisible = EcDistributionManager.instance().dao().isDistributionAllowEnrollment(_distribution);

        _quotaMap = new HashMap<>();
        _taQuotaMap = new HashMap<>();
        EcDistributionManager.instance().dao().getQuotaHTMLDescription(_distribution, _quotaMap, _taQuotaMap);

        // предзачисление
        if (_enrollmentVisible)
            _orderTypeModel = new LazySimpleSelectModel<>(EcOrderManager.instance().dao().getOrderTypeList(_distribution.getConfig().getEcgItem().getEnrollmentCampaign()), EntrantEnrollmentOrderType.P_SHORT_TITLE);

        _recommendedIds = new HashSet<>();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (EcDistributionPub.DIRECTION_DS.equals(dataSource.getName()))
        {
            List<EnrollmentDirection> list = EcDistributionManager.instance().dao().getDistributionDirectionList(_distribution.getDistribution());

            if (_distribution.getConfig().getEcgItem().getEnrollmentCampaign().isUseCompetitionGroup())
            {
                EnrollmentDirection emptyLine = new EnrollmentDirection();
                emptyLine.setId(0L);
                list.add(emptyLine);
            }

            dataSource.put(EcSimpleDSHandler.LIST, list);
        } else if (EcDistributionPub.RECOMMENDED_DS.equals(dataSource.getName()))
        {
            if (_enrollmentVisible)
            {
                List<Long> noPreStudentIds = new ArrayList<>();
                List<IEcgRecommendedPreStudentDTO> list = EcDistributionManager.instance().dao().getEntrantRecommendedPreStudentRowList(_distribution);
                for (IEcgRecommendedPreStudentDTO item : list)
                    if (item.isNoPreStudent())
                        noPreStudentIds.add(item.getId());

                if (noPreStudentIds.isEmpty())
                    _headerCheckboxScript = "";
                else
                    _headerCheckboxScript = "var arr = ['" + StringUtils.join(noPreStudentIds, "', '") + "']; for (var i = 0; i < arr.length; i++) { var item = document.getElementById('checkbox_' + arr[i] + '_in'); if (item) item.checked = this.checked; }";
                dataSource.put(EcSimpleDSHandler.LIST, list);
            } else
            {
                dataSource.put(EcSimpleDSHandler.LIST, EcDistributionManager.instance().dao().getEntrantRecommendedRowList(_distribution));
            }

            dataSource.put(EcSimpleDSHandler.PAGEABLE, true);
        }
    }

    @Override
    public void onAfterDataSourceFetch(IUIDataSource dataSource)
    {
        if (_enrollmentVisible && EcDistributionPub.RECOMMENDED_DS.equals(dataSource.getName()))
        {
            List<IEcgRecommendedPreStudentDTO> list = dataSource.getRecords();

            Map<Long, EntrantEnrollmentOrderType> valueMap = new HashMap<>();
            for (IEcgRecommendedPreStudentDTO item : list)
                if (item.getPreStudent() != null)
                    valueMap.put(item.getId(), item.getPreStudent().getEntrantEnrollmentOrderType());

            @SuppressWarnings("unchecked")
            BlockColumn<EntrantEnrollmentOrderType> blockColumn = (BlockColumn<EntrantEnrollmentOrderType>) ((PageableSearchListDataSource) dataSource).getLegacyDataSource().getColumn("orderType");
            blockColumn.setValueMap(valueMap);
        }
    }

    // quota

    public String getQuota()
    {
        Long id = ((EnrollmentDirection) getConfig().getDataSource(EcDistributionPub.DIRECTION_DS).getCurrent()).getId();
        return _quotaMap.get(id);
    }

    // taQuota

    public String getTaQuota()
    {
        Long id = ((EnrollmentDirection) getConfig().getDataSource(EcDistributionPub.DIRECTION_DS).getCurrent()).getId();
        return _taQuotaMap.get(id);
    }

    public boolean isLockedOrApproved()
    {
        return _distribution.getState().isLockedOrApproved();
    }

    public boolean isCanViewReApproveButton()
    {
        return EcDistributionManager.instance().dao().isDistributionLastApproved(_distribution);
    }

    public boolean isCanViewEditApprovalDateButton()
    {
        return _distribution instanceof EcgDistribution;
    }

    public boolean isCanViewLockButton()
    {
        return _distribution instanceof EcgDistribution && _distribution.getState().isFormative();
    }

    public boolean isCanViewEditButton()
    {
        return _distribution instanceof EcgDistribution;
    }

    public boolean isRemoveRecommendedDisabled()
    {
        return _distribution.getState().isLockedOrApproved();
    }

    public boolean isCanPrint()
    {
        return _distribution instanceof EcgDistribution;
    }

    // checkbox

    public boolean isCheckboxValue()
    {
        return _recommendedIds.contains(_uiConfig.<IUIDataSource>getDataSource(EcDistributionPub.RECOMMENDED_DS).<IEcgRecommendedPreStudentDTO>getCurrent().getId());
    }

    public void setCheckboxValue(boolean value)
    {
        Long recommendedId = _uiConfig.<IUIDataSource>getDataSource(EcDistributionPub.RECOMMENDED_DS).<IEcgRecommendedPreStudentDTO>getCurrent().getId();
        if (value)
            _recommendedIds.add(recommendedId);
        else
            _recommendedIds.remove(recommendedId);
    }

    // presenter

    public String getRecommendationTabCaption() {
        return getConfig().getProperty(_distribution instanceof EcgDistribution ? "tabPanel.tabPanelRecommendationMain" : "tabPanel.tabPanelRecommendationDetail");
    }


    // Getters & Setters

    public Long getDistributionId()
    {
        return _distributionId;
    }

    public void setDistributionId(Long distributionId)
    {
        _distributionId = distributionId;
    }

    public IEcgDistribution getDistribution()
    {
        return _distribution;
    }

    public void setDistribution(IEcgDistribution distribution)
    {
        _distribution = distribution;
    }

    public String getTypeTitle()
    {
        return _typeTitle;
    }

    public void setTypeTitle(String typeTitle)
    {
        _typeTitle = typeTitle;
    }

    public String getSelectedTab()
    {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        _selectedTab = selectedTab;
    }

    public String getEcgItemDisplayName()
    {
        return _ecgItemDisplayName;
    }

    public void setEcgItemDisplayName(String ecgItemDisplayName)
    {
        _ecgItemDisplayName = ecgItemDisplayName;
    }

    public String getDistributionCategoryTitle()
    {
        return _distributionCategoryTitle;
    }

    public void setDistributionCategoryTitle(String distributionCategoryTitle)
    {
        _distributionCategoryTitle = distributionCategoryTitle;
    }

    public boolean isEnrollmentVisible()
    {
        return _enrollmentVisible;
    }

    public void setEnrollmentVisible(boolean enrollmentVisible)
    {
        _enrollmentVisible = enrollmentVisible;
    }

    public Map<Long, String> getQuotaMap()
    {
        return _quotaMap;
    }

    public void setQuotaMap(Map<Long, String> quotaMap)
    {
        _quotaMap = quotaMap;
    }

    public Map<Long, String> getTaQuotaMap()
    {
        return _taQuotaMap;
    }

    public void setTaQuotaMap(Map<Long, String> taQuotaMap)
    {
        _taQuotaMap = taQuotaMap;
    }

    public ISelectModel getOrderTypeModel()
    {
        return _orderTypeModel;
    }

    public void setOrderTypeModel(ISelectModel orderTypeModel)
    {
        _orderTypeModel = orderTypeModel;
    }

    public boolean isPreEnrollDivVisible()
    {
        return _preEnrollDivVisible;
    }

    public void setPreEnrollDivVisible(boolean preEnrollDivVisible)
    {
        _preEnrollDivVisible = preEnrollDivVisible;
    }

    public boolean isHeaderCheckbox()
    {
        return _headerCheckbox;
    }

    public void setHeaderCheckbox(boolean headerCheckbox)
    {
        _headerCheckbox = headerCheckbox;
    }

    public String getHeaderCheckboxScript()
    {
        return _headerCheckboxScript;
    }

    public void setHeaderCheckboxScript(String headerCheckboxScript)
    {
        _headerCheckboxScript = headerCheckboxScript;
    }

    public EntrantEnrollmentOrderType getOrderType()
    {
        return _orderType;
    }

    public void setOrderType(EntrantEnrollmentOrderType orderType)
    {
        _orderType = orderType;
    }

    // Listeners

    public void onClickRtfPrint()
    {
        _uiSupport.doRefresh();

        try
        {
            byte[] data = EcDistributionManager.instance().dao().getRtfPrint(_distributionId);

            String fileName = EcDistributionManager.instance().dao().getDistributionFileName(_distribution);

            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName(fileName + ".rtf").document(data), false);
        } finally
        {
            onComponentRefresh();
        }
    }

    public void onClickXmlPrint()
    {
        _uiSupport.doRefresh();

        try
        {
            byte[] data = EcDistributionManager.instance().dao().getXmlPrint(_distributionId);

            String fileName = EcDistributionManager.instance().dao().getDistributionFileName(_distribution);

            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().xml().fileName(fileName + ".soap.xml").document(data), false);
        } finally
        {
            onComponentRefresh();
        }
    }

    public void onClickEdit()
    {
        _uiSupport.doRefresh();

        _uiActivation.asRegion(EcDistributionAddEdit.class).parameter(
            EcDistributionAddEditUI.DISTRIBUTION_DTO,
            new EcgDistributionDTO(0L, (EcgDistribution) _distribution, null)
        ).activate();
    }

    public void onClickEditApprovalDate()
    {
        _uiSupport.doRefresh();

        _uiActivation.asRegion(EcDistributionApprovalDateEdit.class).parameter(
            EcDistributionAddEditUI.DISTRIBUTION_DTO,
            new EcgDistributionDTO(0L, (EcgDistribution) _distribution, null)
        ).activate();
    }

    public void onClickLock()
    {
        _uiSupport.doRefresh();

        try
        {
            EcDistributionManager.instance().dao().doDistributionLock((EcgDistribution) _distribution);
        } finally
        {
            onComponentRefresh();
        }
    }

    public void onClickApprove()
    {
        _uiSupport.doRefresh();

        try
        {
            EcDistributionManager.instance().dao().doDistributionApprove(_distribution);
        } finally
        {
            onComponentRefresh();
        }
    }

    public void onClickReApprove()
    {
        _uiSupport.doRefresh();

        try
        {
            EcDistributionManager.instance().dao().doDistributionReApprove(_distribution);
        } finally
        {
            onComponentRefresh();
        }
    }



    public void onClickAddByTA()
    {
        _uiSupport.doRefresh();

        _uiActivation.asRegion(EcDistributionEntrantAdd.class)
        .parameter(EcDistributionEntrantAddUI.DISTRIBUTION, _distribution)
        .parameter(EcDistributionEntrantAddUI.RATE_RULE, IEcgEntrantRateRowDTO.Rule.RULE_TARGET_ADMISSION)
        .activate();
    }

    public void onClickAddByOutOfCompetition()
    {
        _uiSupport.doRefresh();

        _uiActivation.asRegion(EcDistributionEntrantAdd.class)
        .parameter(EcDistributionEntrantAddUI.DISTRIBUTION, _distribution)
        .parameter(EcDistributionEntrantAddUI.RATE_RULE, IEcgEntrantRateRowDTO.Rule.RULE_NON_COMPETITIVE)
        .activate();
    }

    public void onClickAddByCompetition()
    {
        _uiSupport.doRefresh();

        _uiActivation.asRegion(EcDistributionEntrantAdd.class)
        .parameter(EcDistributionEntrantAddUI.DISTRIBUTION, _distribution)
        .parameter(EcDistributionEntrantAddUI.RATE_RULE, IEcgEntrantRateRowDTO.Rule.RULE_COMPETITIVE)
        .activate();
    }

    public void onClickAddByCompetitionWithOriginal()
    {
        _uiSupport.doRefresh();

        _uiActivation.asRegion(EcDistributionEntrantAdd.class)
        .parameter(EcDistributionEntrantAddUI.DISTRIBUTION, _distribution)
        .parameter(EcDistributionEntrantAddUI.RATE_RULE, IEcgEntrantRateRowDTO.Rule.RULE_COMPETITIVE_WITH_ORIGINAL)
        .activate();
    }

    public void onDeleteEntityFromList()
    {
        _uiSupport.doRefresh();

        try
        {
            EcDistributionManager.instance().dao().deleteEntrantRecommended((IEcgEntrantRecommended) DataAccessServices.dao().getNotNull(getListenerParameterAsLong()));
        } finally
        {
            onComponentRefresh();
        }
    }

    public void onChangeOrderType()
    {
        // предзачисление не активировано
        if (!EcDistributionManager.instance().dao().isDistributionLastApproved(_distribution)) return;

        Long id = getListenerParameterAsLong();
        List list = getConfig().getDataSource(EcDistributionPub.RECOMMENDED_DS).getRecords();

        int i = 0;
        while (i < list.size() && !((IEcgRecommendedPreStudentDTO) list.get(i)).getId().equals(id)) i++;

        // неправильный идентификатор
        if (i == list.size()) return;
        IEcgRecommendedPreStudentDTO recommendedPreStudentDTO = (IEcgRecommendedPreStudentDTO) list.get(i);

        // изменение типа приказа не доступно
        if (recommendedPreStudentDTO.isChangeOrderDisabled()) return;

        @SuppressWarnings("unchecked")
        EntrantEnrollmentOrderType orderType = ((BlockColumn<EntrantEnrollmentOrderType>) ((PageableSearchListDataSource) _uiConfig.getDataSource(EcDistributionPub.RECOMMENDED_DS)).getLegacyDataSource().getColumn("orderType")).getValueMap().get(recommendedPreStudentDTO.getId());
        EducationOrgUnit educationOrgUnit = recommendedPreStudentDTO.getEntrantRecommended().getDirection().getEnrollmentDirection().getEducationOrgUnit();

        // выполняем предзачисление
        EcPreEnrollManager.instance().dao().doChangeOrderType(recommendedPreStudentDTO.getEntrantRecommended().getDirection().getId(), educationOrgUnit, orderType);
    }

    public void onEditEntityFromList()
    {
        // предзачисление не активировано
        if (!EcDistributionManager.instance().dao().isDistributionLastApproved(_distribution)) return;

        Long id = getListenerParameterAsLong();
        List list = getConfig().getDataSource(EcDistributionPub.RECOMMENDED_DS).getRecords();

        int i = 0;
        while (i < list.size() && !((IEcgRecommendedPreStudentDTO) list.get(i)).getId().equals(id)) i++;

        // неправильный идентификатор
        if (i == list.size()) return;
        IEcgRecommendedPreStudentDTO recommendedPreStudentDTO = (IEcgRecommendedPreStudentDTO) list.get(i);

        // изменение условий предзачисления не доступно
        if (recommendedPreStudentDTO.isChangeEnrollmentConditionDisabled()) return;

        // загружаем форму редактирования условий предзачисления
        getConfig().getBusinessComponent().createDefaultChildRegion(new ComponentActivator(IUniecComponents.ENTRANT_ENROLLMENT_CONDITITONS_EDIT, new ParametersMap().add("preliminaryEnrollmentStudentId", recommendedPreStudentDTO.getPreStudent().getId())));
    }

    public void onClickPreEnroll()
    {
        if (_recommendedIds.isEmpty())
            throw new ApplicationException("Необходимо выбрать хотя бы одного абитуриента.");

        EcDistributionManager.instance().dao().doBulkPreEnroll(_recommendedIds, _orderType);
    }
}
