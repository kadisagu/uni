package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.ProfileExaminationMark;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Оценка по профильному предмету
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ProfileExaminationMarkGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.ProfileExaminationMark";
    public static final String ENTITY_NAME = "profileExaminationMark";
    public static final int VERSION_HASH = -465326130;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT = "entrant";
    public static final String L_DISCIPLINE = "discipline";
    public static final String P_MARK = "mark";

    private Entrant _entrant;     // (Старый) Абитуриент
    private Discipline2RealizationWayRelation _discipline;     // Дисциплина набора вступительных испытаний
    private int _mark;     // Оценка

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return (Старый) Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public Entrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant (Старый) Абитуриент. Свойство не может быть null.
     */
    public void setEntrant(Entrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Дисциплина набора вступительных испытаний. Свойство не может быть null.
     */
    @NotNull
    public Discipline2RealizationWayRelation getDiscipline()
    {
        return _discipline;
    }

    /**
     * @param discipline Дисциплина набора вступительных испытаний. Свойство не может быть null.
     */
    public void setDiscipline(Discipline2RealizationWayRelation discipline)
    {
        dirty(_discipline, discipline);
        _discipline = discipline;
    }

    /**
     * @return Оценка. Свойство не может быть null.
     */
    @NotNull
    public int getMark()
    {
        return _mark;
    }

    /**
     * @param mark Оценка. Свойство не может быть null.
     */
    public void setMark(int mark)
    {
        dirty(_mark, mark);
        _mark = mark;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ProfileExaminationMarkGen)
        {
            setEntrant(((ProfileExaminationMark)another).getEntrant());
            setDiscipline(((ProfileExaminationMark)another).getDiscipline());
            setMark(((ProfileExaminationMark)another).getMark());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ProfileExaminationMarkGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ProfileExaminationMark.class;
        }

        public T newInstance()
        {
            return (T) new ProfileExaminationMark();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entrant":
                    return obj.getEntrant();
                case "discipline":
                    return obj.getDiscipline();
                case "mark":
                    return obj.getMark();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entrant":
                    obj.setEntrant((Entrant) value);
                    return;
                case "discipline":
                    obj.setDiscipline((Discipline2RealizationWayRelation) value);
                    return;
                case "mark":
                    obj.setMark((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entrant":
                        return true;
                case "discipline":
                        return true;
                case "mark":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entrant":
                    return true;
                case "discipline":
                    return true;
                case "mark":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entrant":
                    return Entrant.class;
                case "discipline":
                    return Discipline2RealizationWayRelation.class;
                case "mark":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ProfileExaminationMark> _dslPath = new Path<ProfileExaminationMark>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ProfileExaminationMark");
    }
            

    /**
     * @return (Старый) Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ProfileExaminationMark#getEntrant()
     */
    public static Entrant.Path<Entrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Дисциплина набора вступительных испытаний. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ProfileExaminationMark#getDiscipline()
     */
    public static Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation> discipline()
    {
        return _dslPath.discipline();
    }

    /**
     * @return Оценка. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ProfileExaminationMark#getMark()
     */
    public static PropertyPath<Integer> mark()
    {
        return _dslPath.mark();
    }

    public static class Path<E extends ProfileExaminationMark> extends EntityPath<E>
    {
        private Entrant.Path<Entrant> _entrant;
        private Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation> _discipline;
        private PropertyPath<Integer> _mark;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return (Старый) Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ProfileExaminationMark#getEntrant()
     */
        public Entrant.Path<Entrant> entrant()
        {
            if(_entrant == null )
                _entrant = new Entrant.Path<Entrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Дисциплина набора вступительных испытаний. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ProfileExaminationMark#getDiscipline()
     */
        public Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation> discipline()
        {
            if(_discipline == null )
                _discipline = new Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation>(L_DISCIPLINE, this);
            return _discipline;
        }

    /**
     * @return Оценка. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ProfileExaminationMark#getMark()
     */
        public PropertyPath<Integer> mark()
        {
            if(_mark == null )
                _mark = new PropertyPath<Integer>(ProfileExaminationMarkGen.P_MARK, this);
            return _mark;
        }

        public Class getEntityClass()
        {
            return ProfileExaminationMark.class;
        }

        public String getEntityName()
        {
            return "profileExaminationMark";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
