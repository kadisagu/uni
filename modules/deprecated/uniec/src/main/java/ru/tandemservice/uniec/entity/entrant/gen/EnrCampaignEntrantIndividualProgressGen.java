package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniec.entity.catalog.IndividualProgress;
import ru.tandemservice.uniec.entity.entrant.EnrCampaignEntrantIndividualProgress;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Вид индивидуального достижения абитуриента, учитываемый в рамках ПК
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrCampaignEntrantIndividualProgressGen extends EntityBase
 implements INaturalIdentifiable<EnrCampaignEntrantIndividualProgressGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.EnrCampaignEntrantIndividualProgress";
    public static final String ENTITY_NAME = "enrCampaignEntrantIndividualProgress";
    public static final int VERSION_HASH = 1121714778;
    private static IEntityMeta ENTITY_META;

    public static final String L_INDIVIDUAL_PROGRESS = "individualProgress";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";

    private IndividualProgress _individualProgress;     // Индивидуальное достижение
    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Индивидуальное достижение. Свойство не может быть null.
     */
    @NotNull
    public IndividualProgress getIndividualProgress()
    {
        return _individualProgress;
    }

    /**
     * @param individualProgress Индивидуальное достижение. Свойство не может быть null.
     */
    public void setIndividualProgress(IndividualProgress individualProgress)
    {
        dirty(_individualProgress, individualProgress);
        _individualProgress = individualProgress;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrCampaignEntrantIndividualProgressGen)
        {
            if (withNaturalIdProperties)
            {
                setIndividualProgress(((EnrCampaignEntrantIndividualProgress)another).getIndividualProgress());
                setEnrollmentCampaign(((EnrCampaignEntrantIndividualProgress)another).getEnrollmentCampaign());
            }
        }
    }

    public INaturalId<EnrCampaignEntrantIndividualProgressGen> getNaturalId()
    {
        return new NaturalId(getIndividualProgress(), getEnrollmentCampaign());
    }

    public static class NaturalId extends NaturalIdBase<EnrCampaignEntrantIndividualProgressGen>
    {
        private static final String PROXY_NAME = "EnrCampaignEntrantIndividualProgressNaturalProxy";

        private Long _individualProgress;
        private Long _enrollmentCampaign;

        public NaturalId()
        {}

        public NaturalId(IndividualProgress individualProgress, EnrollmentCampaign enrollmentCampaign)
        {
            _individualProgress = ((IEntity) individualProgress).getId();
            _enrollmentCampaign = ((IEntity) enrollmentCampaign).getId();
        }

        public Long getIndividualProgress()
        {
            return _individualProgress;
        }

        public void setIndividualProgress(Long individualProgress)
        {
            _individualProgress = individualProgress;
        }

        public Long getEnrollmentCampaign()
        {
            return _enrollmentCampaign;
        }

        public void setEnrollmentCampaign(Long enrollmentCampaign)
        {
            _enrollmentCampaign = enrollmentCampaign;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrCampaignEntrantIndividualProgressGen.NaturalId) ) return false;

            EnrCampaignEntrantIndividualProgressGen.NaturalId that = (NaturalId) o;

            if( !equals(getIndividualProgress(), that.getIndividualProgress()) ) return false;
            if( !equals(getEnrollmentCampaign(), that.getEnrollmentCampaign()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getIndividualProgress());
            result = hashCode(result, getEnrollmentCampaign());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getIndividualProgress());
            sb.append("/");
            sb.append(getEnrollmentCampaign());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrCampaignEntrantIndividualProgressGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrCampaignEntrantIndividualProgress.class;
        }

        public T newInstance()
        {
            return (T) new EnrCampaignEntrantIndividualProgress();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "individualProgress":
                    return obj.getIndividualProgress();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "individualProgress":
                    obj.setIndividualProgress((IndividualProgress) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "individualProgress":
                        return true;
                case "enrollmentCampaign":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "individualProgress":
                    return true;
                case "enrollmentCampaign":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "individualProgress":
                    return IndividualProgress.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrCampaignEntrantIndividualProgress> _dslPath = new Path<EnrCampaignEntrantIndividualProgress>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrCampaignEntrantIndividualProgress");
    }
            

    /**
     * @return Индивидуальное достижение. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrCampaignEntrantIndividualProgress#getIndividualProgress()
     */
    public static IndividualProgress.Path<IndividualProgress> individualProgress()
    {
        return _dslPath.individualProgress();
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrCampaignEntrantIndividualProgress#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    public static class Path<E extends EnrCampaignEntrantIndividualProgress> extends EntityPath<E>
    {
        private IndividualProgress.Path<IndividualProgress> _individualProgress;
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Индивидуальное достижение. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrCampaignEntrantIndividualProgress#getIndividualProgress()
     */
        public IndividualProgress.Path<IndividualProgress> individualProgress()
        {
            if(_individualProgress == null )
                _individualProgress = new IndividualProgress.Path<IndividualProgress>(L_INDIVIDUAL_PROGRESS, this);
            return _individualProgress;
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrCampaignEntrantIndividualProgress#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

        public Class getEntityClass()
        {
            return EnrCampaignEntrantIndividualProgress.class;
        }

        public String getEntityName()
        {
            return "enrCampaignEntrantIndividualProgress";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
