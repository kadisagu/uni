package ru.tandemservice.uniec.component.report.EntrantStudentCard;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author oleyba
 * @since 04.08.2009
 */
public interface IDAO extends IUniDao<Model>
{
    void createReport(Model model);
}
