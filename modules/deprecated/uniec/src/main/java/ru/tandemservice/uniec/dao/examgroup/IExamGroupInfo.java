/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.dao.examgroup;

/**
 * Информация о возможной экзаменационной группе
 *
 * @author Vasily Zhukov
 * @since 28.05.2010
 */
public interface IExamGroupInfo
{
    String getKey();

    String getTitle();

    String getShortTitle();
}
