package ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.PreEnroll;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.education.gen.EducationOrgUnitGen;
import org.tandemframework.shared.person.base.entity.gen.PersonRoleGen;
import org.tandemframework.shared.person.base.entity.gen.IdentityCardGen;
import org.tandemframework.shared.person.base.entity.gen.PersonGen;
import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.dao.ecg.IEnrollmentCompetitionGroupDAO;
import ru.tandemservice.uniec.entity.ecg.EcgDistribObject;
import ru.tandemservice.uniec.entity.ecg.gen.EcgDistribQuotaGen;
import ru.tandemservice.uniec.entity.ecg.gen.EcgDistribRelationGen;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.gen.EnrollmentDirectionGen;
import ru.tandemservice.uniec.entity.entrant.gen.EntrantRequestGen;
import ru.tandemservice.uniec.entity.entrant.gen.RequestedEnrollmentDirectionGen;

/**
 * @author vdanilov
 */
public class DAO extends UniBaseDao implements IDAO {

    @Override
    public void prepare(final Model model) {
        model.setDistrib(this.get(EcgDistribObject.class, model.getDistrib().getId()));
        model.setOrderTypes(EcOrderManager.instance().dao().getOrderTypeList(model.getDistrib().getCompetitionGroup().getEnrollmentCampaign()));
    }

    @Override
    public void prepareStudentDataSource(final Model model) {

        final MQBuilder b = new MQBuilder(RequestedEnrollmentDirectionGen.ENTITY_CLASS, "d");
        b.addDomain("rel", EcgDistribRelationGen.ENTITY_CLASS);
        b.add(MQExpression.eqProperty("d", "id", "rel", EcgDistribRelationGen.L_ENTRANT_DIRECTION+".id"));
        b.add(MQExpression.eq("rel", EcgDistribRelationGen.L_QUOTA+"."+EcgDistribQuotaGen.L_DISTRIBUTION, model.getDistrib()));

        b.addJoinFetch("d", RequestedEnrollmentDirectionGen.L_ENTRANT_REQUEST, "rq");
        b.addJoinFetch("d", RequestedEnrollmentDirectionGen.L_ENROLLMENT_DIRECTION, "ed");
        b.addJoinFetch("rq", EntrantRequestGen.L_ENTRANT, "e");
        b.addJoinFetch("e", PersonRoleGen.L_PERSON, "p");
        b.addJoinFetch("p", PersonGen.L_IDENTITY_CARD, "idc");

        b.addOrder("ed", EnrollmentDirectionGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnitGen.L_EDUCATION_LEVEL_HIGH_SCHOOL+".code");
        b.addOrder("ed", EnrollmentDirectionGen.L_EDUCATION_ORG_UNIT+".id");
        b.addOrder("idc", IdentityCardGen.P_LAST_NAME);
        b.addOrder("idc", IdentityCardGen.P_FIRST_NAME);
        b.addOrder("idc", IdentityCardGen.P_MIDDLE_NAME);


        final List<Model.Row> rows = new ArrayList<Model.Row>();
        final List<RequestedEnrollmentDirection> resultList = b.<RequestedEnrollmentDirection>getResultList(getSession());
        final Map<RequestedEnrollmentDirection, PreliminaryEnrollmentStudent> dir2student = IEnrollmentCompetitionGroupDAO.INSTANCE.get().getPreliminaryEnrollmentStudentFor(resultList);
        for (final RequestedEnrollmentDirection dir: resultList) {
            final PreliminaryEnrollmentStudent s = dir2student.get(dir);
            rows.add(new Model.Row(null == s.getId() ? dir.getId() : s.getId(), "") {
                private static final long serialVersionUID = 1L;
                @Override public RequestedEnrollmentDirection getDirection() { return dir; }
                @Override public PreliminaryEnrollmentStudent getStudent() { return s; }
            });
        }

        model.setRows(rows);
    }

    @Override
    public void doPreEnroll(final Model model, final ErrorCollector errorCollector) {
        if (null == model.getDistrib().getParent()) {
            throw new ApplicationException("Предварительное зачисление в автоматическом режиме доступно только для уточняющих распределений.");
        }

        for (final Model.Row row: model.getRows()) {
            if (null == row.getStudent().getId()) {
                if (null != row.getStudent().getEntrantEnrollmentOrderType()) {
                    row.getStudent().setCompensationType(row.getStudent().getEntrantEnrollmentOrderType().getCompensationType());
                    final org.tandemframework.core.CoreCollectionUtils.Pair<String, PreliminaryEnrollmentStudent> result = UniecDAOFacade.getEntrantDAO().saveOrUpdatePreliminaryStudent(row.getStudent());
                    final String error = result.getX();
                    if (null != error) {
                        errorCollector.add(error, "orderType_" + row.getId());
                    }
                }
            }
        }
    }

    @Override
    public void delete(final Model model, final Long id) {
        if (null == id) { return; }
        final PreliminaryEnrollmentStudent s = get(PreliminaryEnrollmentStudent.class, id);
        if (null != s) {
            // delete(s);
        }
    }


}
