/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EntrantDailyRatingByTA.EntrantDailyRatingByTAAdd;

import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.entity.report.EntrantDailyRatingByTAReport;
import ru.tandemservice.uniec.ui.EnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.Date;

/**
 * @author oleyba
 * @since 12.06.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    @SuppressWarnings("deprecation")
    public void prepare(final Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.getReport().setDateFrom(model.getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : model.getEnrollmentCampaign().getStartDate());
        model.getReport().setDateTo(CoreDateUtils.getDayLastTimeMoment(new Date()));
        model.setTerritorialOrgUnit(TopOrgUnit.getInstance());

        model.setStudentCategoryListModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(StudentCategory.class)));
        model.setDevelopFormListModel(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setDevelopConditionListModel(EducationCatalogsManager.getDevelopConditionSelectModel());
        model.setQualificationListModel(new QualificationModel(getSession()));
        model.setFormativeOrgUnitModel(EnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setTerritorialOrgUnitModel(EnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setEducationLevelsHighSchoolModel(EnrollmentDirectionUtil.createEducationLevelsHighSchoolModel(model));
        model.setDevelopFormModel(EnrollmentDirectionUtil.createDevelopFormModel(model));
        model.setDevelopConditionModel(EnrollmentDirectionUtil.createDevelopConditionModel(model));
        model.setDevelopTechModel(EnrollmentDirectionUtil.createDevelopTechModel(model));
        model.setDevelopPeriodModel(EnrollmentDirectionUtil.createDevelopPeriodModel(model));
        model.setCompensationTypes(getCatalogItemList(CompensationType.class));
    }

    @Override
    public void update(Model model)
    {
        Session session = getSession();

        EntrantDailyRatingByTAReport report = model.getReport();
        report.setFormingDate(new Date());
        if (!model.isByAllEnrollmentDirections())
        {
            report.setEnrollmentDirection(EnrollmentDirectionUtil.getEnrollmentDirection(model, session));
            model.setQualificationList(null);
            model.setDevelopFormList(null);
            model.setDevelopConditionList(null);
        } else
        {
            report.setDevelopFormTitle(UniStringUtils.join(model.getDevelopFormList(), "title", "; "));
            report.setDevelopConditionTitle(UniStringUtils.join(model.getDevelopConditionList(), "title", "; "));
            report.setQualificationTitle(UniStringUtils.join(model.getQualificationList(), "title", "; "));
        }
        report.setStudentCategoryTitle(UniStringUtils.join(model.getStudentCategoryList(), StudentCategory.P_TITLE, "; "));

        DatabaseFile databaseFile = getReportContent(model, session);
        session.save(databaseFile);

        report.setContent(databaseFile);
        session.save(report);
    }

    public DatabaseFile getReportContent(Model model, Session session) {
        return new EntrantDailyRatingByTAReportBuilder(model).getContent();
    }
}
