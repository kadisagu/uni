/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.menu.MarkDistribution;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline;
import ru.tandemservice.uniec.entity.entrant.ExamPassMark;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

import java.util.HashSet;
import java.util.Set;

/**
 * @author vip_delete
 * @since 31.03.2009
 */
class EntrantDisciplineMarkWrapper extends IdentifiableWrapper
{
    private static final long serialVersionUID = -849757269994497280L;
    public static final String P_ENTRANT = "entrant";
    public static final String P_EXAM_PASS_DISCIPLINE = "examPassDiscipline";

    private ExamPassDiscipline _examPassDiscipline;
    private Entrant _entrant;
    private Set<RequestedEnrollmentDirection> _requestedEnrollmentDirections = new HashSet<RequestedEnrollmentDirection>();
    private String _mark;
    private String _examiners;
    private String _paperCode;

    public EntrantDisciplineMarkWrapper(ExamPassDiscipline examPassDiscipline, RequestedEnrollmentDirection direction, ExamPassMark examPassMark)
    {
        super(examPassDiscipline.getId(), direction.getEntrantRequest().getEntrant().getPerson().getFullFio());
        _entrant = direction.getEntrantRequest().getEntrant();
        _examPassDiscipline = examPassDiscipline;
        _requestedEnrollmentDirections.add(direction);
        _mark = examPassMark == null ? "" : examPassMark.getEntrantAbsenceNote() != null ? "н" : DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS.format(examPassMark.getMark());
        _examiners = examPassMark == null ? examPassDiscipline.getExamCommissionMembership() : examPassMark.getExaminers();
        _paperCode = examPassMark == null ? "" : examPassMark.getPaperCode();
    }

    public String getEntrantFIO()
    {
        return getEntrant().getPerson().getFullFio();
    }

    public String getExamListNumber()
    {
        return getExamPassDiscipline().getEntrantExamList().getNumber();
    }

    // Getters & Setters

    public ExamPassDiscipline getExamPassDiscipline()
    {
        return _examPassDiscipline;
    }

    public void setExamPassDiscipline(ExamPassDiscipline examPassDiscipline)
    {
        _examPassDiscipline = examPassDiscipline;
    }

    public Entrant getEntrant()
    {
        return _entrant;
    }

    public void setEntrant(Entrant entrant)
    {
        _entrant = entrant;
    }

    public Set<RequestedEnrollmentDirection> getRequestedEnrollmentDirections()
    {
        return _requestedEnrollmentDirections;
    }

    public void setRequestedEnrollmentDirections(Set<RequestedEnrollmentDirection> requestedEnrollmentDirections)
    {
        _requestedEnrollmentDirections = requestedEnrollmentDirections;
    }

    public String getMark()
    {
        return _mark;
    }

    public void setMark(String mark)
    {
        _mark = mark;
    }

    public String getExaminers()
    {
        return _examiners;
    }

    public void setExaminers(String examiners)
    {
        _examiners = examiners;
    }

    public String getPaperCode()
    {
        return _paperCode;
    }

    public void setPaperCode(String paperCode)
    {
        _paperCode = paperCode;
    }
}
