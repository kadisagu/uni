package ru.tandemservice.uniec.entity.survey;

import com.google.common.collect.Maps;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.survey.gen.*;

import java.util.Collection;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @see ru.tandemservice.uniec.entity.survey.gen.QuestionaryEntrantGen
 */
public class QuestionaryEntrant extends QuestionaryEntrantGen
{
    public static final String ALIAS = "s";

    @Override
    public String getObjectName()
    {
        return "Абитуриент";
    }

    @Override
    public void setQuestionaryObject(Object object)
    {
        if (object instanceof Entrant)
            setEntrant((Entrant) object);
        else
            throw new ApplicationException("Объект анкетирования не является абитуриентом.");
    }

    @Override
    public Object getQuestionaryObject()
    {
        return getEntrant();
    }

    @Override
    public String getQuestionaryObjectTitle()

    {
        return getEntrant() != null ? getEntrant().getFullTitle() : null;
    }

    @Override
    public DQLSelectBuilder getDQLSearchObject()
    {
        return new DQLSelectBuilder()
                .fromEntity(Entrant.class, ALIAS)
                .column(property(ALIAS, Entrant.id()))
                .order(property(ALIAS, Entrant.id()));
    }

    @Override
    public Map<PairKey<String, Boolean>, String> getDataSourceColumns()
    {
        return Maps.newHashMap();
    }

    @Override
    public Object getAnswerValue(String questionCode)
    {
        return null;
    }

    @Override
    public DQLSelectBuilder addWhereExpression(DQLSelectBuilder builder, String questionCode)
    {
        return builder;
    }

    @Override
    public DQLSelectBuilder getObjectList(Collection<Long> ids)
    {
        return new DQLSelectBuilder()
                .fromEntity(Entrant.class, ALIAS)
                .where(in(property(ALIAS, Entrant.id()), ids));
    }
}