/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.menu.examGroup.logic1.ExamGroupList;

import ru.tandemservice.uniec.component.menu.examGroup.abstractExamGroupList.IExamGroupListDAO;

/**
 * @author Vasily Zhukov
 * @since 28.05.2010
 */
public interface IDAO extends IExamGroupListDAO<Model>
{
}
