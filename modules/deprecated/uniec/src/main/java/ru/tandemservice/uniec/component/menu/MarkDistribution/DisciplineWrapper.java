/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.menu.MarkDistribution;

import ru.tandemservice.uni.ui.ObjectWrapper;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

/**
 * @author vip_delete
 * @since 17.11.2009
 */
@SuppressWarnings("unchecked")
class DisciplineWrapper extends ObjectWrapper implements Comparable
{
    private String _title;
    private Discipline2RealizationWayRelation _discipline;
    private SubjectPassForm _subjectPassForm;

    DisciplineWrapper(Discipline2RealizationWayRelation discipline, SubjectPassForm subjectPassForm)
    {
        _title = discipline.getTitle() + " (" + subjectPassForm.getTitle() + ")";
        _discipline = discipline;
        _subjectPassForm = subjectPassForm;
    }

    @Override
    public String getId()
    {
        return _discipline.getId() + "_" + _subjectPassForm.getId();
    }

    public String getTitle()
    {
        return _title;
    }

    public Discipline2RealizationWayRelation getDiscipline()
    {
        return _discipline;
    }

    public SubjectPassForm getSubjectPassForm()
    {
        return _subjectPassForm;
    }

    @Override
    public int compareTo(Object o)
    {
        DisciplineWrapper other = (DisciplineWrapper) o;
        int r = getDiscipline().getTitle().compareTo(other.getDiscipline().getTitle());
        if (r != 0) return r;
        return getSubjectPassForm().getCode().compareTo(other.getSubjectPassForm().getCode());
    }
}
