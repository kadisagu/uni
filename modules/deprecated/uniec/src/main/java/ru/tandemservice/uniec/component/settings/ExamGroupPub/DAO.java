/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.settings.ExamGroupPub;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.ExamGroup;
import ru.tandemservice.uniec.entity.entrant.ExamGroupRow;
import ru.tandemservice.uniec.entity.entrant.ExamGroupSet;
import ru.tandemservice.uniec.entity.entrant.ExamGroupSetOpened;

/**
 * @author vip_delete
 * @since 12.06.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setExamGroup(getNotNull(ExamGroup.class, model.getExamGroup().getId()));

        model.setStatusTitle(isGroupOpened(model.getExamGroup()) ? "Формируется" : "Закрыта");
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        final Session session = getSession();

        MQBuilder builder = new MQBuilder(ExamGroupRow.ENTITY_CLASS, "r");
        builder.addJoinFetch("r", ExamGroupRow.L_ENTRANT_REQUEST, "request");
        builder.addJoinFetch("request", EntrantRequest.L_ENTRANT, "entrant");
        builder.addJoinFetch("entrant", Entrant.L_PERSON, "person");
        builder.addJoinFetch("person", Person.L_IDENTITY_CARD, "card");
        builder.add(MQExpression.eq("r", ExamGroupRow.L_EXAM_GROUP, model.getExamGroup()));
        builder.addOrder("r", ExamGroupRow.P_INDEX);

        List<ExamGroupRow> list = builder.getResultList(session);
        model.getDataSource().setCountRow(list.size());
        UniBaseUtils.createPage(model.getDataSource(), list);

        Set<ExamGroupRow> invalidRowSet = new HashSet<ExamGroupRow>();
        for (ExamGroupRow row : list)
            if (!UniecDAOFacade.getExamGroupSetDao().isExamGroupRowValid(row))
                invalidRowSet.add(row);
        model.setInvalidRowSet(invalidRowSet);
    }

    // util methods

    private boolean isGroupOpened(ExamGroup examGroup)
    {
        // получаем открытый набор
        ExamGroupSet openedSet = (ExamGroupSet) new MQBuilder(ExamGroupSetOpened.ENTITY_CLASS, "s", new String[]{ExamGroupSetOpened.L_EXAM_GROUP_SET})
                .add(MQExpression.eq("s", ExamGroupSetOpened.L_ENROLLMENT_CAMPAIGN, examGroup.getExamGroupSet().getEnrollmentCampaign()))
                .uniqueResult(getSession());

        // если нет открытого набора, группа закрыта
        if (openedSet == null) return false;

        // если группа не в открытом наборе, то группа закрыта
        if (!openedSet.equals(examGroup.getExamGroupSet())) return false;

        // если есть группа с бОльшим номером с таким же ключем, то группа закрыта
        List<ExamGroup> list = new MQBuilder(ExamGroup.ENTITY_CLASS, "g")
                .add(MQExpression.eq("g", ExamGroup.P_KEY, examGroup.getKey()))
                .add(MQExpression.eq("g", ExamGroup.L_EXAM_GROUP_SET, examGroup.getExamGroupSet()))
                .add(MQExpression.great("g", ExamGroup.P_INDEX, examGroup.getIndex()))
                .getResultList(getSession(), 0, 1);
        if (list.size() > 0) return false;

        // если группа не заполнена, то она открыта
        return getCount(ExamGroupRow.class, ExamGroupRow.L_EXAM_GROUP, examGroup) < openedSet.getSize();
    }
}
