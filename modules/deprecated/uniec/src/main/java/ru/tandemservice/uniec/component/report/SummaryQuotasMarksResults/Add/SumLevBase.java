/* $Id$ */
package ru.tandemservice.uniec.component.report.SummaryQuotasMarksResults.Add;

import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.StateExamSubjectMark;
import ru.tandemservice.uniec.entity.settings.ConversionScale;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.MarkStatistic;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Vasily Zhukov
 * @since 19.08.2011
 */
abstract class SumLevBase implements ISumLev
{
    protected EntrantDataUtil dataUtil;
    private String _title;
    private String _subTitle;
    private Long _id;

    // данные для колонок по планам и кол-ву заявлений
    private int _budgetPlan;
    private int _budgetCount;
    private int _targetPlan;
    private int _targetCount;
    private int _contractPlan;
    private int _contractCount;
    private Set<Long> usedBudgetIds = new HashSet<>();
    private Set<Long> usedTargetIds = new HashSet<>();
    private Set<Long> usedContractIds = new HashSet<>();

    // данные для колонки по баллам ЕГЭ
    private Set<PairKey<Long, Long>> usedStateExamSubjectKeyIds = new HashSet<>();
    private Map<StateExamSubject, double[]> stateExamSubjectDataMap = new HashMap<>();

    // данные для колонки по баллам по дисциплинам приемной кампании
    private Set<PairKey<Long, Long>> usedDisciplineKeyIds = new HashSet<>();
    private Map<Discipline2RealizationWayRelation, double[]> disciplineDataMap = new HashMap<>();

    // данные по зачисленным
    private int[] _enrollCount;

    @SuppressWarnings({"unchecked"})
    protected SumLevBase(Long id, EntrantDataUtil dataUtil, String title, String subTitle)
    {
        this.dataUtil = dataUtil;
        _title = title;
        _subTitle = subTitle;
        _id = id;
    }

    void add(RequestedEnrollmentDirection direction, Map<Discipline2RealizationWayRelation, ConversionScale> discipline2scaleMap)
    {
        Long key = direction.getEntrantRequest().getId();

        // три колонки: планы и кол-во заявлений по бюджету/цп/контракту
        if (direction.getCompensationType().isBudget() && usedBudgetIds.add(key)) _budgetCount++;
        if (!direction.getCompensationType().isBudget() && usedContractIds.add(key)) _contractCount++;
        if (direction.isTargetAdmission() && usedTargetIds.add(key)) _targetCount++;

        for (ChosenEntranceDiscipline chosen : dataUtil.getChosenEntranceDisciplineSet(direction))
        {
            // должна быть финальная оценка
            if (chosen.getFinalMark() != null && chosen.getFinalMarkSource() != null)
            {
                int source = chosen.getFinalMarkSource();

                if (source == UniecDefines.FINAL_MARK_STATE_EXAM_WAVE1 || source == UniecDefines.FINAL_MARK_STATE_EXAM_WAVE2)
                {
                    MarkStatistic statistic = dataUtil.getMarkStatistic(chosen);

                    // берем оценку по ЕГЭ
                    StateExamSubjectMark stateExamSubjectMark = statistic.getStateExamMark();

                    // оценка должна существовать в сертификате ЕГЭ
                    if (stateExamSubjectMark != null)
                    {
                        StateExamSubject stateExamSubject = stateExamSubjectMark.getSubject();

                        // заявление для этого предмета ЕГЭ должно учитывается только один раз
                        if (usedStateExamSubjectKeyIds.add(PairKey.create(key, stateExamSubject.getId())))
                        {
                            double[] sumAndCount = stateExamSubjectDataMap.get(stateExamSubject);
                            if (sumAndCount == null)
                                stateExamSubjectDataMap.put(stateExamSubject, sumAndCount = new double[2]);
                            sumAndCount[0] += statistic.getScaledStateExamMark(); // сумма всех оценок
                            sumAndCount[1] += 1;                                  // кол-во слагаемых
                        }
                    }
                } else if (source == UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL || source == UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL_APPEAL)
                {
                    // берем дисциплину
                    Discipline2RealizationWayRelation discipline = chosen.getEnrollmentCampaignDiscipline();

                    ConversionScale scale = discipline2scaleMap.get(discipline);

                    if (scale != null && scale.getSubject() != null)
                    {
                        StateExamSubject stateExamSubject = scale.getSubject();

                        // заявление для этого предмета ЕГЭ должно учитывается только один раз
                        if (usedStateExamSubjectKeyIds.add(PairKey.create(key, stateExamSubject.getId())))
                        {
                            double[] sumAndCount = stateExamSubjectDataMap.get(stateExamSubject);
                            if (sumAndCount == null)
                                stateExamSubjectDataMap.put(stateExamSubject, sumAndCount = new double[2]);
                            sumAndCount[0] += chosen.getFinalMark(); // сумма всех оценок
                            sumAndCount[1] += 1;                     // кол-во слагаемых
                        }
                    }
                } else
                {
                    // берем дисциплину
                    Discipline2RealizationWayRelation discipline = chosen.getEnrollmentCampaignDiscipline();

                    // заявление для этой дисциплины должно учитывается только один раз
                    if (usedDisciplineKeyIds.add(PairKey.create(key, discipline.getId())))
                    {
                        double[] sumAndCount = disciplineDataMap.get(discipline);
                        if (sumAndCount == null)
                            disciplineDataMap.put(discipline, sumAndCount = new double[2]);
                        sumAndCount[0] += chosen.getFinalMark(); // сумма всех оценок
                        sumAndCount[1] += 1;                     // кол-во слагаемых
                    }
                }
            }
        }
    }

    @Override
    public int hashCode()
    {
        return _id.hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        return _id.equals(((SumLevBase) obj)._id);
    }

    @Override
    public int compareTo(ISumLev o)
    {
        if (getLevel() != o.getLevel())
            throw new RuntimeException("invalid compare");

        if (equals(o))
            return 0;

        int r = getTitle().compareTo(o.getTitle());
        if (r != 0) return r;

        if (getSubTitle() != null && o.getSubTitle() != null)
        {
            r = getSubTitle().compareTo(o.getSubTitle());
            if (r != 0) return r;
        }

        return _id.compareTo(((SumLevBase) o)._id);
    }

    @Override
    public double getAvgStateExamMark(StateExamSubject stateExamSubject)
    {
        double[] data = stateExamSubjectDataMap.get(stateExamSubject);
        if (data == null) return 0.0;
        return data[0] / data[1];
    }

    @Override
    public double getAvgDisciplineMark(Discipline2RealizationWayRelation discipline)
    {
        double[] data = disciplineDataMap.get(discipline);
        if (data == null) return 0.0;
        return data[0] / data[1];
    }

    @Override
    public int getEnrollCount(int wave)
    {
        return _enrollCount[wave];
    }

    // Getters & Setters

    public EntrantDataUtil getDataUtil()
    {
        return dataUtil;
    }

    public void setDataUtil(EntrantDataUtil dataUtil)
    {
        this.dataUtil = dataUtil;
    }

    @Override
    public String getTitle()
    {
        return _title;
    }

    public void setTitle(String title)
    {
        _title = title;
    }

    @Override
    public String getSubTitle()
    {
        return _subTitle;
    }

    public void setSubTitle(String subTitle)
    {
        _subTitle = subTitle;
    }

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        _id = id;
    }

    @Override
    public int getBudgetPlan()
    {
        return _budgetPlan;
    }

    public void setBudgetPlan(int budgetPlan)
    {
        _budgetPlan = budgetPlan;
    }

    @Override
    public int getBudgetCount()
    {
        return _budgetCount;
    }

    public void setBudgetCount(int budgetCount)
    {
        _budgetCount = budgetCount;
    }

    @Override
    public int getTargetPlan()
    {
        return _targetPlan;
    }

    public void setTargetPlan(int targetPlan)
    {
        _targetPlan = targetPlan;
    }

    @Override
    public int getTargetCount()
    {
        return _targetCount;
    }

    public void setTargetCount(int targetCount)
    {
        _targetCount = targetCount;
    }

    @Override
    public int getContractPlan()
    {
        return _contractPlan;
    }

    public void setContractPlan(int contractPlan)
    {
        _contractPlan = contractPlan;
    }

    @Override
    public int getContractCount()
    {
        return _contractCount;
    }

    public void setContractCount(int contractCount)
    {
        _contractCount = contractCount;
    }

    public Set<Long> getUsedBudgetIds()
    {
        return usedBudgetIds;
    }

    public void setUsedBudgetIds(Set<Long> usedBudgetIds)
    {
        this.usedBudgetIds = usedBudgetIds;
    }

    public Set<Long> getUsedTargetIds()
    {
        return usedTargetIds;
    }

    public void setUsedTargetIds(Set<Long> usedTargetIds)
    {
        this.usedTargetIds = usedTargetIds;
    }

    public Set<Long> getUsedContractIds()
    {
        return usedContractIds;
    }

    public void setUsedContractIds(Set<Long> usedContractIds)
    {
        this.usedContractIds = usedContractIds;
    }

    public Set<PairKey<Long, Long>> getUsedStateExamSubjectKeyIds()
    {
        return usedStateExamSubjectKeyIds;
    }

    public void setUsedStateExamSubjectKeyIds(Set<PairKey<Long, Long>> usedStateExamSubjectKeyIds)
    {
        this.usedStateExamSubjectKeyIds = usedStateExamSubjectKeyIds;
    }

    public Map<StateExamSubject, double[]> getStateExamSubjectDataMap()
    {
        return stateExamSubjectDataMap;
    }

    public void setStateExamSubjectDataMap(Map<StateExamSubject, double[]> stateExamSubjectDataMap)
    {
        this.stateExamSubjectDataMap = stateExamSubjectDataMap;
    }

    public Set<PairKey<Long, Long>> getUsedDisciplineKeyIds()
    {
        return usedDisciplineKeyIds;
    }

    public void setUsedDisciplineKeyIds(Set<PairKey<Long, Long>> usedDisciplineKeyIds)
    {
        this.usedDisciplineKeyIds = usedDisciplineKeyIds;
    }

    public Map<Discipline2RealizationWayRelation, double[]> getDisciplineDataMap()
    {
        return disciplineDataMap;
    }

    public void setDisciplineDataMap(Map<Discipline2RealizationWayRelation, double[]> disciplineDataMap)
    {
        this.disciplineDataMap = disciplineDataMap;
    }

    public int[] getEnrollCount()
    {
        return _enrollCount;
    }

    public void setEnrollCount(int[] enrollCount)
    {
        this._enrollCount = enrollCount;
    }
}
