/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.dao.examgroup.logic1;

import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.examgroup.AbstractExamSheetPrintBuilder;

/**
 * @author Vasily Zhukov
 * @since 31.05.2010
 */
class ExamGroupPrintSheetForm extends AbstractExamSheetPrintBuilder
{
    @Override
    protected String getTemplateCode()
    {
        return UniecDefines.ENROLLMENT_PASS_DISCIPLINE_SHEET_ALG1;
    }
}
