package ru.tandemservice.uniec.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип приемной кампании"
 * Имя сущности : enrollmentCampaignType
 * Файл data.xml : uniec.catalogs.data.xml
 */
public interface EnrollmentCampaignTypeCodes
{
    /** Константа кода (code) элемента : Не определен (title) */
    String INDEFINED = "enr.cmpType.indefin";
    /** Константа кода (code) элемента : Для бакалавриата и специалитета (title) */
    String BACHELOR = "enr.cmpType.bach";
    /** Константа кода (code) элемента : Для магистратуры (title) */
    String MASTER = "enr.cmpType.master";
    /** Константа кода (code) элемента : Для среднего профессионального образования (title) */
    String SPO = "enr.cmpType.spo";
    /** Константа кода (code) элемента : Для кадров высшей квалификации (title) */
    String HIGHER_QUAL = "enr.cmpType.highQual";
    /** Константа кода (code) элемента : Для иностранцев по межправительственным соглашениям (title) */
    String FOREIGNER = "enr.cmpType.foreign";

    Set<String> CODES = ImmutableSet.of(INDEFINED, BACHELOR, MASTER, SPO, HIGHER_QUAL, FOREIGNER);
}
