/* $Id$ */
package ru.tandemservice.uniec.component.report.EnrollmentResultByEGE.Add;

import org.tandemframework.core.common.ITitled;

/**
 * @author Vasily Zhukov
 * @since 19.08.2011
 */
interface ISumLev extends ITitled, Comparable<ISumLev>
{
    int getLevel();

    @Override
    /**
     * @return Направления подготовки по УГС
     */
    String getTitle();

    /**
     * @return СРЕДНИЕ БАЛЛЫ ПО ЕГЭ, поступившие на общих основаниях
     */
    double getCommonAvg();

    /**
     * @return СРЕДНИЕ БАЛЛЫ ПО ЕГЭ, поступившие на льготных условиях
     */
    double getBenefitAvg();

    /**
     * @return СРЕДНИЕ БАЛЛЫ ПО ЕГЭ, победители и призеры олимпиад
     */
    double getOlympiadAvg();

    /**
     * @return СРЕДНИЕ БАЛЛЫ ПО ЕГЭ, поступившие по целевому приему
     */
    double getTargetAvg();

    /**
     * @return ЗАЧИСЛЕНО, Всего
     */
    int getCount();

    /**
     * @return ЗАЧИСЛЕНО, поступившие на льготных условиях
     */
    int getBenefitCount();

    /**
     * @return ЗАЧИСЛЕНО, победители и призеры олимпиад
     */
    int getOlympiadCount();

    /**
     * @return ЗАЧИСЛЕНО, поступившие по целевому приему
     */
    int getTargetCount();
}
