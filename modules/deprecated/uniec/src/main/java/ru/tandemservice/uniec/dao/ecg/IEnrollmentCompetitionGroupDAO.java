package ru.tandemservice.uniec.dao.ecg;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.exception.ApplicationException;

import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.ecg.EcgDistribObject;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

/**
 * Управление зачислением по конкурсным группам
 * (распределения, выборки)
 * 
 * @author vdanilov
 */
public interface IEnrollmentCompetitionGroupDAO {
    public static final SpringBeanCache<IEnrollmentCompetitionGroupDAO> INSTANCE = new SpringBeanCache<IEnrollmentCompetitionGroupDAO>(IEnrollmentCompetitionGroupDAO.class.getName());


    /** строка с рейтингом для абитуриента */
    public static interface IEntrantRateRow {

        /** @return абитуриент */
        Entrant getEntrant();

        /** @return вид ЦП, nullable (если не ЦП - то null) */
        TargetAdmissionKind getTargetAdmissionKind();

        /** @return вид конкурса (not-null) общий (или внутри ЦП) */
        CompetitionKind getCompetitionKind();

        /** @return приоритет направления с флагм "учился в профильных учреждениях" */
        int getGraduatedProfileEduInstitutionPriority();

        /** @return приоритет направления, по которому сданы оригиналы */
        int getOriginalDocumentHandedInPriority();

        /** @return балл по испытаниям */
        Double getTotalMark();

        /** @return балл по профильному испытанию */
        Double getProfileMark();

        /** @return балл по аттестату */
        Double getCertificateAverageMark();

        /** @return список направлений по приоритетам */
        List<RequestedEnrollmentDirection> getDirections();
    }


    static interface IEntrantRateRowsSelector {
    }

    /**
     * возвращает список строк, доступных для добавления в распределение,
     * отсортированных в порядке убывания вероятности зачисления (по рейтингу, например)
     **/
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    List<IEntrantRateRow> getEntrantRateRows4Add(EcgDistribObject distribution, IEntrantRateRowsSelector selector);

    /**
     * возвращает список строк, уже находяшихся в распределении,
     * отсортированных в порядке убывания вероятности зачисления (по рейтингу, например)
     **/
    List<IEntrantRateRow> getEntrantRateRows(EcgDistribObject distribution);

    /**
     * возвращает допустимый план приема по распределению
     * (с учетом уже занятых в рамках данного распределения мест)
     * @return { enrollmentDirection.Id -> quota }
     **/
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    Map<Long, Integer> getDir2QuotaMap(EcgDistribObject distrib);

    /** Добавляет в распределение абитуриентов */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doAppendEntrantDirections(EcgDistribObject distrib, Collection<RequestedEnrollmentDirection> values);


    /** Утверждает распределение */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doApproveDistribution(EcgDistribObject distrib);


    /** проверяет возможность редактирования для распределения */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    void checkEditable(EcgDistribObject distrib) throws ApplicationException;


    /** удаляет распределение (с соответсвующими проверками) */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doDelete(EcgDistribObject distrib);


    /** формирует студентов предзачисления по указанным направлениям приема */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    Map<RequestedEnrollmentDirection, PreliminaryEnrollmentStudent> getPreliminaryEnrollmentStudentFor(Collection<RequestedEnrollmentDirection> directions);



}
