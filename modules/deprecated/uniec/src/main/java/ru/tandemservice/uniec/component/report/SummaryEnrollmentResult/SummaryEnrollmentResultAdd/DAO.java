/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.report.SummaryEnrollmentResult.SummaryEnrollmentResultAdd;

import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import org.tandemframework.shared.commonbase.utils.CommonYesNoUIObject;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.entity.report.SummaryEnrollmentResultsReport;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.Arrays;
import java.util.Date;

/**
 * @author Vasily Zhukov
 * @since 29.07.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    @SuppressWarnings("deprecation")
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.setEnrollmentCampaignStageList(Arrays.asList(
                new IdentifiableWrapper((long) Model.ENROLLMENT_CAMP_STAGE_DOCUMENTS, "по ходу приема документов"),
                new IdentifiableWrapper((long) Model.ENROLLMENT_CAMP_STAGE_EXAMS, "по результатам сдачи вступительных испытаний"),
                new IdentifiableWrapper((long) Model.ENROLLMENT_CAMP_STAGE_ENROLLMENT, "по результатам зачисления")
        ));
        model.getReport().setDateFrom(model.getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : model.getEnrollmentCampaign().getStartDate());
        model.getReport().setDateTo(CoreDateUtils.getDayLastTimeMoment(new Date()));
        model.setDevelopFormList(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setDevelopConditionListModel(EducationCatalogsManager.getDevelopConditionSelectModel());
        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));
        model.setQualificationListModel(new QualificationModel(getSession()));
        model.setStudentCategoryListModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(StudentCategory.class)));
        model.setParallelList(CommonYesNoUIObject.createYesNoList());

        // загружаем местоположение ОУ
        AddressDetailed address = TopOrgUnit.getInstance().getAddress();
        AddressItem area = address == null ? null : address.getSettlement();
        if (area != null)
            while (area.getParent() != null)
                area = area.getParent();
        model.setColumnList(Arrays.asList("Конкурс", "Медаль", "Диплом с отл", "Внеконкурсный прием", "Целевой прием", "Стаж по профилю", "Профильные знания", "Среднее полное образ-е", "Нач. проф-ное", "СПО", "ВО", "непВО", "Слушатели", "Мужчин", (area == null ? "регион ОУ" : area.getTitleWithType()), "Др. р-ны России", "Др. гос-ва", "Иностр. граж-не", "ЕГЭ -I (МОУ)", "ЕГЭ -II (вуз)"));
    }

    @Override
    public void update(Model model)
    {
        Session session = getSession();

        SummaryEnrollmentResultsReport report = model.getReport();
        report.setFormingDate(new Date());
        report.setEnrollmentCampaignStage(model.getEnrollmentCampaignStage().getTitle());

        if (model.isDevelopConditionActive())
            report.setDevelopConditionTitle(UniStringUtils.join(model.getDevelopConditionList(), DevelopCondition.P_TITLE, "; "));
        if (model.isQualificationActive())
            report.setQualificationTitle(UniStringUtils.join(model.getQualificationList(), Qualifications.P_TITLE, "; "));
        if (model.isStudentCategoryActive())
            report.setStudentCategoryTitle(UniStringUtils.join(model.getStudentCategoryList(), StudentCategory.P_TITLE, "; "));
        if (model.isParallelActive())
            report.setExcludeParallel(model.getParallel().isTrue());

        DatabaseFile databaseFile = new SummaryEnrollmentResultsReportBuilder(model, session).getContent();
        session.save(databaseFile);

        report.setContent(databaseFile);
        session.save(report);
    }
}
