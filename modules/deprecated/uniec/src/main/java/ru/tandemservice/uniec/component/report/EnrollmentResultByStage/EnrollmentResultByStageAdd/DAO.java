// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EnrollmentResultByStage.EnrollmentResultByStageAdd;

import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.utils.CommonYesNoUIObject;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.entity.report.EnrollmentResultByStageReport;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.Calendar;
import java.util.Date;

/**
 * @author oleyba
 * @since 16.10.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    @SuppressWarnings("deprecation")
    public void prepare(final Model model)
    {
        model.setParameters(new MultiEnrollmentDirectionUtil.Parameters());
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.getReport().setDateFrom(model.getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : model.getEnrollmentCampaign().getStartDate());
        model.getReport().setDateTo(CoreDateUtils.getDayLastTimeMoment(new Date()));

        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        int year = c.get(Calendar.YEAR);
        c.set(year, 7, 3); model.getStageDateFrom().put(1, c.getTime());
        c.set(year, 7, 6); model.getStageDateTo().put(1, c.getTime());
        c.set(year, 7, 7); model.getStageDateFrom().put(2, c.getTime());
        c.set(year, 7, 10); model.getStageDateTo().put(2, c.getTime());
        c.set(year, 7, 11); model.getStageDateFrom().put(3, c.getTime());
        c.set(year, 7, 15); model.getStageDateTo().put(3, c.getTime());
        c.set(year, 7, 16); model.getStageDateFrom().put(4, c.getTime());
        c.set(year, 12, 30); model.getStageDateTo().put(4, c.getTime());

        model.setStudentCategoryListModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(StudentCategory.class)));
        model.setQualificationListModel(new QualificationModel(getSession()));

        model.setFormativeOrgUnitListModel(MultiEnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setTerritorialOrgUnitListModel(MultiEnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setEducationLevelHighSchoolListModel(MultiEnrollmentDirectionUtil.createEducationLevelsHighSchoolModel(model));
        model.setDevelopFormListModel(MultiEnrollmentDirectionUtil.createDevelopFormModel(model));
        model.setDevelopConditionListModel(MultiEnrollmentDirectionUtil.createDevelopConditionModel(model));
        model.setDevelopTechListModel(MultiEnrollmentDirectionUtil.createDevelopTechModel(model));
        model.setDevelopPeriodListModel(MultiEnrollmentDirectionUtil.createDevelopPeriodModel(model));
        model.setParallelList(CommonYesNoUIObject.createYesNoList());
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        for (int key=1; key <=4; key++)
            if (!model.getStageDateTo().get(key).after(model.getStageDateFrom().get(key)))
                errors.add("Дата начала этапа должна быть до даты окончания этапа.", "stageFrom" + key, "stageTo" + key);
        for (int key=2; key <=4; key++)
            if (!model.getStageDateFrom().get(key).after(model.getStageDateTo().get(key - 1)))
                errors.add("Этапы не должны пересекаться.", "stageFrom" + key, "stageTo" + (key - 1));                
    }

    @Override
    public void update(Model model)
    {
        final Session session = getSession();

        EnrollmentResultByStageReport report = model.getReport();
        report.setFormingDate(new Date());

        if (model.isStudentCategoryActive())
            report.setStudentCategoryTitle(UniStringUtils.join(model.getStudentCategoryList(), "title", "; "));
        if (model.isQualificationActive())
            report.setQualificationTitle(UniStringUtils.join(model.getQualificationList(), "title", "; "));
        if (model.isFormativeOrgUnitActive())
            report.setFormativeOrgUnit(UniStringUtils.join(model.getFormativeOrgUnitList(), OrgUnit.P_FULL_TITLE, "; "));
        if (model.isTerritorialOrgUnitActive())
            report.setTerritorialOrgUnit(UniStringUtils.join(model.getTerritorialOrgUnitList(), OrgUnit.P_TERRITORIAL_FULL_TITLE, "; "));
        if (model.isEducationLevelHighSchoolActive())
            report.setEducationLevelHighSchoolTitle(UniStringUtils.join(model.getEducationLevelHighSchoolList(), EducationLevelsHighSchool.P_DISPLAYABLE_TITLE, "; "));
        report.setDevelopForm(UniStringUtils.join(model.getDevelopFormList(), "title", "; "));
        if (model.isDevelopConditionActive())
            report.setDevelopCondition(UniStringUtils.join(model.getDevelopConditionList(), "title", "; "));
        if (model.isDevelopTechActive())
            report.setDevelopTech(UniStringUtils.join(model.getDevelopTechList(), "title", "; "));
        if (model.isDevelopPeriodActive())
            report.setDevelopPeriod(UniStringUtils.join(model.getDevelopPeriodList(), "title", "; "));
        if (model.isParallelActive())
            report.setExcludeParallel(model.getParallel().isTrue());
        String stages = "";
        for (int key=1; key<=4; key++)
            stages = stages + DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getStageDateFrom().get(key)) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getStageDateTo().get(key)) + (key == 4 ? "" : "; ");
        model.getReport().setStages(stages);       

        DatabaseFile databaseFile = new EnrollmentResultByStageReportBuilder(model, session).getContent();
        session.save(databaseFile);

        report.setContent(databaseFile);
        session.save(report);
    }

}
