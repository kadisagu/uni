/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantRequestTab;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.component.impl.BusinessComponent;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IEntityHandler;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDefine;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.IUniecComponents;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.base.bo.EcEntrant.EcEntrantManager;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.ExamGroup;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

import java.util.List;
import java.util.Map;

/**
 * @author agolubenko
 * @since 16.05.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        getDao().prepareEntrantRequestList(model);

        for (final EntrantRequest entrantRequest : model.getEntrantRequestList())
        {
            prepareEntrantRequestDataSource(component, entrantRequest);
        }
    }

    private void prepareEntrantRequestDataSource(final IBusinessComponent component, final EntrantRequest entrantRequest)
    {
        final Model model = getModel(component);
        //if (model.getRequestedEnrollmentDirectionDataSourceByRequest().get(entrantRequest) != null) return;

        DynamicListDataSource<RequestedEnrollmentDirection> dataSource = new DynamicListDataSource<>(component, context -> {
            getDao().prepareRequestedEnrollmentDirectionDataSource(model, entrantRequest);
        });

        String eduUnit = RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_EDUCATION_ORG_UNIT + ".";

        dataSource.addColumn(new SimpleColumn("Дата добавления", RequestedEnrollmentDirection.P_REG_DATE, DateFormatter.DATE_FORMATTER_WITH_TIME).setClickable(false).setOrderable(false).setRequired(true));
        dataSource.addColumn(new SimpleColumn("№ рег.", RequestedEnrollmentDirection.P_NUMBER).setClickable(false).setOrderable(false).setWidth(2));
        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().displayableTitle().s()).setOrderable(false).setWidth(15));
        dataSource.addColumn(new SimpleColumn("Формирующее подр.", eduUnit + EducationOrgUnit.L_FORMATIVE_ORG_UNIT + "." + OrgUnit.P_TITLE).setClickable(false).setOrderable(false).setWidth(10));
        dataSource.addColumn(new SimpleColumn("Территориальное подр.", eduUnit + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT + "." + OrgUnit.P_TERRITORIAL_TITLE).setClickable(false).setOrderable(false).setWidth(10));
        dataSource.addColumn(new SimpleColumn("Форма освоения", eduUnit + EducationOrgUnit.L_DEVELOP_FORM + "." + DevelopForm.P_TITLE).setClickable(false).setOrderable(false).setWidth(6));
        dataSource.addColumn(new SimpleColumn("ЦП", RequestedEnrollmentDirection.P_TARGET_ADMISSION, YesNoFormatter.INSTANCE).setClickable(false).setOrderable(false).setWidth(2));
        dataSource.addColumn(new SimpleColumn("Категория поступающего", RequestedEnrollmentDirection.L_STUDENT_CATEGORY + "." + StudentCategory.P_TITLE).setClickable(false).setOrderable(false).setWidth(8));
        dataSource.addColumn(new SimpleColumn("Вид конкурса", RequestedEnrollmentDirection.L_COMPETITION_KIND + "." + CompetitionKind.P_TITLE).setClickable(false).setOrderable(false).setWidth(9));
        dataSource.addColumn(new SimpleColumn("Контракт", RequestedEnrollmentDirection.L_COMPENSATION_TYPE + "." + CompensationType.P_SHORT_TITLE).setClickable(false).setOrderable(false).setWidth(5));
        dataSource.addColumn(new SimpleColumn("Группа", RequestedEnrollmentDirection.P_GROUP).setClickable(false).setOrderable(false).setWidth(5));
        dataSource.addColumn(new SimpleColumn("Состояние", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_TITLE).setClickable(false).setOrderable(false).setWidth(5));
        dataSource.addColumn(new SimpleColumn("Примечание", RequestedEnrollmentDirection.P_COMMENT).setClickable(false).setOrderable(false).setWidth(5));
        // для тестов. можно смотреть приоритет среди всех заявлений абитуриента в разрезе вида возмещения затрат
        //dataSource.addColumn(new SimpleColumn("Приоритет в рамках абитуриента", RequestedEnrollmentDirection.P_PRIORITY_PER_ENTRANT).setClickable(false).setOrderable(false).setWidth(5));

        ToggleColumn agree4EnrollmentColumn = new ToggleColumn("Согласие на зачисление", RequestedEnrollmentDirection.P_AGREE4_ENROLLMENT, "Да", "Нет")
                .toggleOffListener("onClickAgree4EnrollmentOff")
                .toggleOnListener("onClickAgree4EnrollmentOn");
        dataSource.addColumn(agree4EnrollmentColumn.setPermissionKey("moveEntrantOriginalDocuments"));

        IEntityHandler entityHandler = entity -> !model.isAccessible();

        ToggleColumn column = new ToggleColumn("Сданы оригиналы", RequestedEnrollmentDirection.P_ORIGINAL_DOCUMENT_HANDED_IN, "Выдать оригиналы документов", "Принять оригиналы документов")
                .toggleOffListener("onClickOriginalsTakeAway", "Выдать оригиналы документов?")
                .toggleOnListener("onClickOriginalsReturn", "Принять оригиналы документов?");
        dataSource.addColumn(column.setPermissionKey("moveEntrantOriginalDocuments").setDisableHandler(entityHandler));

        dataSource.addColumn(new ActionColumn("Повысить приоритет", CommonBaseDefine.ICO_UP, "onClickUp").setPermissionKey("requestedEnrollmentDirectionUpPriority").setOrderable(false).setDisableHandler(entityHandler));
        dataSource.addColumn(new ActionColumn("Понизить приоритет", CommonBaseDefine.ICO_DOWN, "onClickDown").setPermissionKey("requestedEnrollmentDirectionDownPriority").setOrderable(false).setDisableHandler(entityHandler));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditRequestedEnrollmentDirection").setPermissionKey("editRequestedEnrollmentDirection").setDisableHandler(entityHandler));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить направление подготовки {0}?", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.P_TITLE).setPermissionKey("deleteRequestedEnrollmentDirection").setDisableHandler(entityHandler));

        model.getRequestedEnrollmentDirectionDataSourceByRequest().put(entrantRequest, dataSource);
    }

    public String getCurrentEntrantRequestDeleteAlert(Model model)
    {
        List<ExamGroup> groupList = getDao().getEntrantRequestGroup(model.getCurrentEntrantRequest());
        if (groupList.isEmpty())
            return "Удалить заявление №" + model.getCurrentEntrantRequest().getRegNumber() + " абитуриента «" + model.getCurrentEntrantRequest().getEntrant().getPerson().getFullFio() + "»?";
        else
            return "Удалить заявление №" + model.getCurrentEntrantRequest().getRegNumber() + " абитуриента «" + model.getCurrentEntrantRequest().getEntrant().getPerson().getFullFio() + "», исключая из экзаменационной группы?";
    }

    public void onClickDeleteEntrantRequest(IBusinessComponent component)
    {
        EcEntrantManager.instance().dao().deleteEntrantRequest(component.<Long>getListenerParameter());
        getDao().prepareEntrantRequestList(getModel(component));
        //TODO: hack #54866
        ((BusinessComponent) component.getParentRegion().getOwner()).refresh();
    }

    public void onClickAddEntrantRequest(IBusinessComponent component)
    {
        activate(component, new ComponentActivator(IUniecComponents.ENTRANT_REQUEST_ADD_EDIT, new ParametersMap().add("entrantRequestId", null).add("entrantId", getModel(component).getEntrant().getId())));
    }

    public void onClickAddRequestedEnrollmentDirection(IBusinessComponent component)
    {
        activate(component, new ComponentActivator(IUniecComponents.REQUESTED_ENROLLMENT_DIRECTION_ADD_EDIT, new ParametersMap().add("entrantRequestId", component.getListenerParameter())));
    }

    public void onClickEditRequestedEnrollmentDirection(IBusinessComponent component)
    {
        activate(component, new ComponentActivator(IUniecComponents.REQUESTED_ENROLLMENT_DIRECTION_ADD_EDIT, new ParametersMap().add("requestedEnrollmentDirectionId", component.getListenerParameter())));
    }

    public void onClickEditEntrantRequest(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniecComponents.ENTRANT_REQUEST_ADD_EDIT, new ParametersMap().add("entrantRequestId", component.getListenerParameter()).add("entrantId", null)));
    }

    @SuppressWarnings("unchecked")
    public void onClickPrintEntrantRequest(IBusinessComponent component) throws Exception
    {
        UniecScriptItem template = DataAccessServices.dao().getByCode(UniecScriptItem.class, UniecDefines.SCRIPT_ENTRANT_REQUEST);

        CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(template, component.<Long>getListenerParameter());
    }

    @SuppressWarnings("unchecked")
    public void onClickPrintEnrollmentExamSheet(IBusinessComponent component)
    {
        CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(DataAccessServices.dao().getByCode(UniecScriptItem.class, UniecDefines.SCRIPT_ENROLLMENT_EXAM_SHEET), component.<Long>getListenerParameter());
    }

    @SuppressWarnings("unchecked")
    public void onClickPrintListAndReceipt(IBusinessComponent component)
    {
        CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(DataAccessServices.dao().getByCode(UniecScriptItem.class, UniecDefines.SCRIPT_DOCUMENT_LIST_AND_RECEIPT), component.<Long>getListenerParameter());
    }

    public void onClickPrintPersonalDataConsent(IBusinessComponent component)
    {
        UniecScriptItem scriptItem = DataAccessServices.dao().getByCode(UniecScriptItem.class, UniecDefines.SOGLASIE_NA_OBRABOTKU_P_DN);

        Map<String, Object> scriptResult = CommonManager.instance().scriptDao()
                .getScriptResult(scriptItem, "template", scriptItem.getTemplate(), "entrantRequestId", component.getListenerParameter());

        byte[] content = (byte[]) scriptResult.get(IScriptExecutor.DOCUMENT);
        String filename = (String) scriptResult.get(IScriptExecutor.FILE_NAME);

        if (content == null)
            throw new ApplicationException("Скрипт печати не вернул содержимое файла. Обратитесь к администратору системы.");

        if (filename == null || !filename.contains("."))
            throw new ApplicationException("Скрипт печати вернул некорректное имя файла. Обратитесь к администратору системы.");

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().fileName(filename).contentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF).document(content), false);
    }

    @SuppressWarnings({"unchecked", "deprecation"})
    public void onClickPrintAuthCard(IBusinessComponent component)
    {
        Model model = getModel(component);
        UniecScriptItem template = getDao().getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_AUTH_CARD);
        if (template == null) return;

        // создаем отчет (formCreator должен существовать при заданном шаблоне)
        IPrintFormCreator<Long> formCreator = (IPrintFormCreator<Long>) ApplicationRuntime.getBean("entrantAuthCardPrint");
        byte[] content = RtfUtil.toByteArray(formCreator.createPrintForm(template.getCurrentTemplate(), (Long) component.getListenerParameter()));
        Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(content, "Удостоверение абитуриента " + model.getEntrant().getPerson().getFullFio() + ".rtf");
        activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new ParametersMap().add("id", id).add("zip", false).add("extension", "rtf")));
    }

    public void onClickPrintEntrantLetterbox(IBusinessComponent component) throws Exception
    {
        UniecScriptItem template = DataAccessServices.dao().getByCode(UniecScriptItem.class, UniecDefines.SCRIPT_ENTRANT_LETTERBOX);
        CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(template, component.<Long>getListenerParameter());
    }

    public void onClickDelete(IBusinessComponent component)
    {
        Model model = getModel(component);

        // удаляем ВНП
        EcEntrantManager.instance().dao().deleteRequestedEnrollmentDirection(component.<Long>getListenerParameter());

        // актуализируем данные на странице
        getDao().prepareEntrantRequestList(model);

        // актуализируем список всех датасурсов
        for (final EntrantRequest entrantRequest : model.getEntrantRequestList())
            prepareEntrantRequestDataSource(component, entrantRequest);

        //TODO: hack #54866
        ((BusinessComponent) component.getParentRegion().getOwner()).refresh();
    }

    public void onClickUp(IBusinessComponent component)
    {
        getDao().prepareEntrantRequestList(getModel(component));
        RequestedEnrollmentDirection direction = DataAccessServices.dao().getNotNull(component.<Long>getListenerParameter());
        CommonManager.instance().commonPriorityDao().doChangePriorityUp(direction.getId(), RequestedEnrollmentDirection.entrantRequest(), direction.getEntrantRequest());
    }

    public void onClickDown(IBusinessComponent component)
    {
        getDao().prepareEntrantRequestList(getModel(component));
        RequestedEnrollmentDirection direction = DataAccessServices.dao().getNotNull(component.<Long>getListenerParameter());
        CommonManager.instance().commonPriorityDao().doChangePriorityDown(direction.getId(), RequestedEnrollmentDirection.entrantRequest(), direction.getEntrantRequest());
    }

    public void onClickDocumentsReturn(IBusinessComponent component)
    {
        getDao().prepareEntrantRequestList(getModel(component));
        getDao().updateDocumentsReturn((Long) component.getListenerParameter());
        //TODO: hack #54866
        ((BusinessComponent) component.getParentRegion().getOwner()).refresh();
    }

    public void onClickDocumentsTakeAway(IBusinessComponent component)
    {
        getDao().prepareEntrantRequestList(getModel(component));
        getDao().updateDocumentsTakeAway((Long) component.getListenerParameter());
        //TODO: hack #54866
        ((BusinessComponent) component.getParentRegion().getOwner()).refresh();
    }

    public void onClickOriginalsTakeAway(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().updateOriginalsTakeAway(model, (Long) component.getListenerParameter());
        //TODO: hack #54866
        ((BusinessComponent) component.getParentRegion().getOwner()).refresh();
    }

    public void onClickOriginalsReturn(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().updateOriginalsReturn(model, (Long) component.getListenerParameter());
        //TODO: hack #54866
        ((BusinessComponent) component.getParentRegion().getOwner()).refresh();
    }

    public void onClickChangePriority(IBusinessComponent component)
    {
        activate(component, new ComponentActivator(IUniecComponents.REQUESTED_ENROLLMENT_DIRECTIONS_CHANGE_PRIORITY,
                new ParametersMap().add(ru.tandemservice.uniec.component.entrant.RequestedEnrollmentDirectionsChangePriority.Model.ENTRANT_REQUEST_ID, component.getListenerParameter())));
    }

    public void onClickIncludeIntoExamGroup(IBusinessComponent component)
    {
        getDao().executeIncludeIntoExamGroup((Long) component.getListenerParameter());

        component.getUserContext().getRootRegion().getActiveComponent().refresh();
    }

    public boolean isAllowIncludeIntoExamGroup(Model model)
    {
        return getDao().isAllowIncludeIntoExamGroup(model.getCurrentEntrantRequest());
    }

    public String getCurrentEntrantRequestGroupTitle(Model model)
    {
        List<ExamGroup> groupList = getDao().getEntrantRequestGroup(model.getCurrentEntrantRequest());
        return model.getCurrentEntrantRequest().getTitle() + (groupList.isEmpty() ? "" : " (" + UniStringUtils.join(groupList, ExamGroup.P_TITLE, ", ") + ")");
    }

    public boolean isExamGroupInvalid(Model model)
    {
        return getDao().isExamGroupInvalid(model.getCurrentEntrantRequest());
    }

    public void onClickAgree4EnrollmentOff(IBusinessComponent component)
    {
        getDao().setAgree4Enrollment(component.getListenerParameter(), false);
    }

    public void onClickAgree4EnrollmentOn(IBusinessComponent component)
    {
        getDao().setAgree4Enrollment(component.getListenerParameter(), true);
    }
}
