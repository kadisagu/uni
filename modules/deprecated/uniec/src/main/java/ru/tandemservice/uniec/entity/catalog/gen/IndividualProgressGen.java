package ru.tandemservice.uniec.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniec.entity.catalog.IndividualProgress;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Индивидуальное достижение абитуриента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class IndividualProgressGen extends EntityBase
 implements INaturalIdentifiable<IndividualProgressGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.catalog.IndividualProgress";
    public static final String ENTITY_NAME = "individualProgress";
    public static final int VERSION_HASH = 1427521995;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_TITLE = "title";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_MAX_MARK = "maxMark";

    private String _code;     // Системный код
    private String _title;     // Наименование
    private String _shortTitle;     // Сокращенное наименование
    private int _maxMark;     // Максимальный балл

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Наименование. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Наименование. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Сокращенное наименование. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное наименование. Свойство не может быть null.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Максимальный балл. Свойство не может быть null.
     */
    @NotNull
    public int getMaxMark()
    {
        return _maxMark;
    }

    /**
     * @param maxMark Максимальный балл. Свойство не может быть null.
     */
    public void setMaxMark(int maxMark)
    {
        dirty(_maxMark, maxMark);
        _maxMark = maxMark;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof IndividualProgressGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((IndividualProgress)another).getCode());
            }
            setTitle(((IndividualProgress)another).getTitle());
            setShortTitle(((IndividualProgress)another).getShortTitle());
            setMaxMark(((IndividualProgress)another).getMaxMark());
        }
    }

    public INaturalId<IndividualProgressGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<IndividualProgressGen>
    {
        private static final String PROXY_NAME = "IndividualProgressNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof IndividualProgressGen.NaturalId) ) return false;

            IndividualProgressGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends IndividualProgressGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) IndividualProgress.class;
        }

        public T newInstance()
        {
            return (T) new IndividualProgress();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "title":
                    return obj.getTitle();
                case "shortTitle":
                    return obj.getShortTitle();
                case "maxMark":
                    return obj.getMaxMark();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "maxMark":
                    obj.setMaxMark((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "title":
                        return true;
                case "shortTitle":
                        return true;
                case "maxMark":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "title":
                    return true;
                case "shortTitle":
                    return true;
                case "maxMark":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "title":
                    return String.class;
                case "shortTitle":
                    return String.class;
                case "maxMark":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<IndividualProgress> _dslPath = new Path<IndividualProgress>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "IndividualProgress");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.catalog.IndividualProgress#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Наименование. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.catalog.IndividualProgress#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Сокращенное наименование. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.catalog.IndividualProgress#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Максимальный балл. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.catalog.IndividualProgress#getMaxMark()
     */
    public static PropertyPath<Integer> maxMark()
    {
        return _dslPath.maxMark();
    }

    public static class Path<E extends IndividualProgress> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _title;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<Integer> _maxMark;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.catalog.IndividualProgress#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(IndividualProgressGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Наименование. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.catalog.IndividualProgress#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(IndividualProgressGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Сокращенное наименование. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.catalog.IndividualProgress#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(IndividualProgressGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Максимальный балл. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.catalog.IndividualProgress#getMaxMark()
     */
        public PropertyPath<Integer> maxMark()
        {
            if(_maxMark == null )
                _maxMark = new PropertyPath<Integer>(IndividualProgressGen.P_MAX_MARK, this);
            return _maxMark;
        }

        public Class getEntityClass()
        {
            return IndividualProgress.class;
        }

        public String getEntityName()
        {
            return "individualProgress";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
