/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.menu.MarkDistribution;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.EntrantAbsenceNote;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.EntrantFilterUtil;
import ru.tandemservice.uniec.util.EntrantMarkUtil;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author vip_delete
 * @since 17.11.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    @SuppressWarnings("deprecation")
    public void prepare(final Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());

        // заполняем другие селекты
        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));
        model.setStudentCategoryModel(new LazySimpleSelectModel<>(StudentCategory.class));

        model.setFormativeOrgUnitModel(MultiEnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(null, model));
        model.setTerritorialOrgUnitModel(MultiEnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(null, model));
        model.setEducationLevelHighSchoolModel(MultiEnrollmentDirectionUtil.createEducationLevelsHighSchoolModel(model));
        model.setDevelopFormModel(MultiEnrollmentDirectionUtil.createDevelopFormModel(model));
        model.setDevelopConditionModel(MultiEnrollmentDirectionUtil.createDevelopConditionModel(model));
        model.setDevelopTechModel(MultiEnrollmentDirectionUtil.createDevelopTechModel(model));
        model.setDevelopPeriodModel(MultiEnrollmentDirectionUtil.createDevelopPeriodModel(model));
        model.setGroupModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                List<EnrollmentDirection> list = MultiEnrollmentDirectionUtil.getEnrollmentDirections(model, getSession());
                if (list.isEmpty() || model.getEnrollmentCampaign() == null)
                    return ListResult.getEmpty();
                EntrantDataUtil dataUtil = getDataUtil(model, list, null, getSession());

                Set<String> groupSet = new HashSet<>();
                for (RequestedEnrollmentDirection direction : dataUtil.getDirectionSet())
                    for (ChosenEntranceDiscipline chosen : dataUtil.getChosenEntranceDisciplineSet(direction))
                        for (ExamPassDiscipline examPass : dataUtil.getExamPassDisciplineSet(chosen))
                        {
                            // только внутренние формы сдачи
                            if (!examPass.getSubjectPassForm().isInternal()) continue;

                            // если абитуриента не надо показывать в списке - не учитываем его при заполнении моделей фильтров
                            if (!isNeedShow(direction.getEntrantRequest().getEntrant(), dataUtil.getExamPassMark(examPass) != null))
                                continue;

                            // не учитываем пустые группы
                            if (!StringUtils.isEmpty(direction.getGroup()))
                                groupSet.add(direction.getGroup());
                        }

                List<GroupWrapper> groupList = new ArrayList<>();
                for (String groupTitle : groupSet)
                    if (StringUtils.containsIgnoreCase(groupTitle, filter))
                        groupList.add(new GroupWrapper("id_" + groupTitle, groupTitle));
                Collections.sort(groupList, ITitled.TITLED_COMPARATOR);

                return new ListResult<>(groupList);
            }
        });
        model.setDisciplineModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                List<EnrollmentDirection> list = MultiEnrollmentDirectionUtil.getEnrollmentDirections(model, getSession());
                if (list.isEmpty() || model.getEnrollmentCampaign() == null)
                    return ListResult.getEmpty();

                EntrantDataUtil dataUtil = getDataUtil(model, list, model.getGroup() == null ? null : model.getGroup().getTitle(), getSession());

                Set<PairKey<Discipline2RealizationWayRelation, SubjectPassForm>> pairSet = new HashSet<>();
                for (RequestedEnrollmentDirection direction : dataUtil.getDirectionSet())
                    for (ChosenEntranceDiscipline chosen : dataUtil.getChosenEntranceDisciplineSet(direction))
                        for (ExamPassDiscipline examPass : dataUtil.getExamPassDisciplineSet(chosen))
                        {
                            // только внутренние формы сдачи
                            if (!examPass.getSubjectPassForm().isInternal()) continue;

                            // если абитуриента не надо показывать в списке - не учитываем его при заполнении моделей фильтров
                            if (!isNeedShow(direction.getEntrantRequest().getEntrant(), dataUtil.getExamPassMark(examPass) != null))
                                continue;

                            pairSet.add(PairKey.create(examPass.getEnrollmentCampaignDiscipline(), examPass.getSubjectPassForm()));
                        }

                List<DisciplineWrapper> disciplineList = new ArrayList<>();
                for (PairKey<Discipline2RealizationWayRelation, SubjectPassForm> pairKey : pairSet)
                {
                    DisciplineWrapper discipline = new DisciplineWrapper(pairKey.getFirst(), pairKey.getSecond());
                    if (StringUtils.isEmpty(filter) || StringUtils.containsIgnoreCase(discipline.getTitle(), filter))
                        disciplineList.add(discipline);
                }
                Collections.sort(disciplineList);

                return new ListResult<>(disciplineList);
            }
        });

        model.setPassDateModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                DisciplineWrapper discipline = model.getDiscipline();
                if(discipline == null) return ListResult.getEmpty();

                List<EntranceExamPhase> phases = getList(
                        new DQLSelectBuilder()
                                .fromEntity(EntranceExamPhase.class, "pha")
                                .column(property("pha"))
                                .where(eq(property("pha", EntranceExamPhase.setting().discipline()), value(discipline.getDiscipline())))
                                .where(eq(property("pha", EntranceExamPhase.setting().subjectPassForm()), value(discipline.getSubjectPassForm())))
                                .order(property("pha", EntranceExamPhase.passDate()))
                );

                Set<Date> uniq = new HashSet<>();
                List<DataWrapper> dates = phases.stream()
                        .map(phase -> new DataWrapper(phase.getId(), DateFormatter.DEFAULT_DATE_FORMATTER.format(phase.getPassDate()), phase.getPassDate()))
                        .filter(wrapper -> (filter == null) || wrapper.getTitle().contains(filter))
                        .filter(wrapper -> uniq.add(wrapper.getWrapped()))
                        .collect(Collectors.toList());

                return new ListResult<>(dates);
            }
        });


        reloadModelEntities(model);
        
        if (model.getSettings().get("without") == null)
            model.getSettings().set("without", true);

        if (model.getSettings().get("withoutMark") == null)
            model.getSettings().set("withoutMark", false);

        DataSettingsFacade.saveSettings(model.getSettings());
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        DynamicListDataSource<EntrantDisciplineMarkWrapper> dataSource = model.getDataSource();
        Map<Long, EntrantDisciplineMarkWrapper> wrapperMap = new HashMap<>();

        if (model.getDiscipline() != null)
        {
            List<EnrollmentDirection> list = MultiEnrollmentDirectionUtil.getEnrollmentDirections(model, getSession());
            EntrantDataUtil dataUtil = getDataUtil(model, list, model.getGroup() == null ? null : model.getGroup().getTitle(), getSession());

            for (RequestedEnrollmentDirection direction : dataUtil.getDirectionSet())
                for (ChosenEntranceDiscipline chosen : dataUtil.getChosenEntranceDisciplineSet(direction))
                    for (ExamPassDiscipline examPass : dataUtil.getExamPassDisciplineSet(chosen))
                    {
                        // только внутренние формы сдачи
                        if (!examPass.getSubjectPassForm().isInternal()) continue;

                        Entrant entrant = direction.getEntrantRequest().getEntrant();
                        PairKey<ExamPassMark, ExamPassMarkAppeal> markKey = dataUtil.getExamPassMark(examPass);

                        // если абитуриента не надо показывать в списке - не учитываем его при заполнении моделей фильтров
                        if (!isNeedShow(entrant, markKey != null))
                            continue;

                        // дисциплина не совпадает
                        if (!examPass.getEnrollmentCampaignDiscipline().equals(model.getDiscipline().getDiscipline()))
                            continue;

                        // форма сдачи не совпадает
                        if (!examPass.getSubjectPassForm().equals(model.getDiscipline().getSubjectPassForm()))
                            continue;

                        ExamPassMark examPassMark = markKey == null ? null : markKey.getFirst();

                        //выбран чекбокс Не показывать имеющих оценки
                        if (Boolean.TRUE.equals(model.getSettings().get("withoutMark")) && examPassMark != null && examPassMark.getMark() >0)
                            continue;

                        DataWrapper passDate = model.getPassDate();
                        if (passDate != null && (examPass.getPassDate() == null || !passDate.<Date>getWrapped().equals(examPass.getPassDate())))
                            continue;

                        EntrantDisciplineMarkWrapper wrapper = wrapperMap.get(examPass.getId());
                        if (wrapper == null)
                            wrapperMap.put(examPass.getId(), new EntrantDisciplineMarkWrapper(examPass, direction, examPassMark));
                        else
                            wrapper.getRequestedEnrollmentDirections().add(direction);
                    }
        }

        dataSource.setCountRow(wrapperMap.size());
        List<EntrantDisciplineMarkWrapper> result = new ArrayList<>(wrapperMap.values());
        result.sort(ITitled.TITLED_COMPARATOR);
        UniBaseUtils.createPage(dataSource, result);
    }

    @Override
    public void update(Model model)
    {
        final Session session = getSession();

        List<EntrantDisciplineMarkWrapper> wrapperList = model.getDataSource().getEntityList();
        List<EntrantAbsenceNote> entrantAbsenceNoteList = getCatalogItemList(EntrantAbsenceNote.class);
        for (EntrantDisciplineMarkWrapper wrapper : wrapperList)
        {
            String mark = StringUtils.trimToNull(wrapper.getMark());
            String examiners = StringUtils.trimToNull(wrapper.getExaminers());
            String paperCode = StringUtils.trimToNull(wrapper.getPaperCode());

            // получаем оценку по дисциплине для сдачи + апелляцию, если она есть
            ExamPassMark examPassMark = get(ExamPassMark.class, ExamPassMark.L_EXAM_PASS_DISCIPLINE, wrapper.getExamPassDiscipline());
            ExamPassMarkAppeal examPassMarkAppeal = null;
            if (examPassMark != null)
                examPassMarkAppeal = get(ExamPassMarkAppeal.class, ExamPassMarkAppeal.L_EXAM_PASS_MARK, examPassMark);

            // если на форме поставили пусто, то удаляем оценку для сдачи (если была апелляция по ней, то она тоже удалится)
            if (StringUtils.isEmpty(mark))
            {
                if (examPassMarkAppeal != null)
                    session.delete(examPassMarkAppeal);
                if (examPassMark != null)
                    session.delete(examPassMark);
            } else
            {
                if (examPassMark == null)
                {
                    examPassMark = new ExamPassMark();
                    examPassMark.setExamPassDiscipline(wrapper.getExamPassDiscipline());
                }
                EntrantMarkUtil.parseAndSetMark(examPassMark, entrantAbsenceNoteList, mark);
                examPassMark.setExaminers(examiners);
                examPassMark.setPaperCode(paperCode);
                session.saveOrUpdate(examPassMark);
            }
        }
    }

    /**
     * Определяет, показывать ли строку с абитуриентом в ведомости
     *
     * @param entrant   абитуриент
     * @param markExist есть ли у абитуриента оценка по дисциплине ведомости
     * @return да\нет
     */
    private static boolean isNeedShow(Entrant entrant, boolean markExist)
    {
        if (markExist)
            return true;
        if (entrant.isArchival())
            return false;
        //        if (UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE.equals(entrant.getState().getCode())
        //                || UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY.equals(entrant.getState().getCode()))
        //            return false;
        return true;
    }

    /**
     * Определяет, печатать ли строку с абитуриентом в ведомость
     *
     * @param wrapper строка
     * @return да\нет
     */
    public static boolean isNeedPrint(EntrantDisciplineMarkWrapper wrapper)
    {
        // абитуриент архивный, печатать не надо
        if (wrapper.getRequestedEnrollmentDirections().iterator().next().getEntrantRequest().getEntrant().isArchival())
            return false;

        // если есть выбранное направление с состоянием не забрал документы, то печатает такую строку
        for (RequestedEnrollmentDirection direction : wrapper.getRequestedEnrollmentDirections())
            if (!UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY.equals(direction.getState().getCode()))
                return true;

        // все выбранные направления соответствующие данной строке в состоянии забрал документы - строку печатать не надо
        return false;
    }

    /**
     * @param model модель
     * @return данные для массовой печати ведомостей:
     *         группа -> дисциплина с формой сдачи -> строки (сортировка на каждом уровне)
     */
//    private Map<String, Map<PairKey<Discipline2RealizationWayRelation, SubjectPassForm>, List<EntrantDisciplineMarkWrapper>>> getMassReportData(Model model)
//    {
//        Map<String, Map<PairKey<Discipline2RealizationWayRelation, SubjectPassForm>, List<EntrantDisciplineMarkWrapper>>> result = new TreeMap<>();
//
//        List<EnrollmentDirection> list = MultiEnrollmentDirectionUtil.getEnrollmentDirections(model, getSession());
//        if (list.isEmpty())
//            return result;
//        EntrantDataUtil dataUtil = getDataUtil(model, list, null, getSession());
//
//        for (RequestedEnrollmentDirection direction : dataUtil.getDirectionSet())
//            for (ChosenEntranceDiscipline chosen : dataUtil.getChosenEntranceDisciplineSet(direction))
//                for (ExamPassDiscipline examPass : dataUtil.getExamPassDisciplineSet(chosen))
//                {
//                    // только внутренние формы сдачи
//                    if (!examPass.getSubjectPassForm().isInternal()) continue;
//
//                    Entrant entrant = direction.getEntrantRequest().getEntrant();
//                    PairKey<ExamPassMark, ExamPassMarkAppeal> markKey = dataUtil.getExamPassMark(examPass);
//
//                    // если абитуриента не надо показывать в списке - не учитываем его при заполнении моделей фильтров
//                    if (!isNeedShow(entrant, markKey != null))
//                        continue;
//
//                    Map<PairKey<Discipline2RealizationWayRelation, SubjectPassForm>, List<EntrantDisciplineMarkWrapper>> map = result.get(direction.getGroup());
//                    if (map == null)
//                        result.put(direction.getGroup(), map = new TreeMap<>(new Comparator<PairKey<Discipline2RealizationWayRelation, SubjectPassForm>>()
//                                {
//                            @Override
//                            public int compare(PairKey<Discipline2RealizationWayRelation, SubjectPassForm> o1, PairKey<Discipline2RealizationWayRelation, SubjectPassForm> o2)
//                            {
//                                int r = o1.getFirst().getEducationSubject().getTitle().compareTo(o2.getFirst().getEducationSubject().getTitle());
//                                if (r == 0)
//                                    r = o1.getSecond().getTitle().compareTo(o2.getSecond().getTitle());
//                                return r;
//                            }
//                                }));
//
//                    PairKey<Discipline2RealizationWayRelation, SubjectPassForm> pairKey = PairKey.create(examPass.getEnrollmentCampaignDiscipline(), examPass.getSubjectPassForm());
//                    List<EntrantDisciplineMarkWrapper> rowList = map.get(pairKey);
//                    if (rowList == null)
//                        map.put(pairKey, rowList = new ArrayList<>());
//
//                    ExamPassMark examPassMark = markKey == null ? null : markKey.getFirst();
//                    rowList.add(new EntrantDisciplineMarkWrapper(examPass, direction, examPassMark));
//                }
//        return result;
//    }

    private static EntrantDataUtil getDataUtil(Model model, List<EnrollmentDirection> enrollmentDirectionList, String groupTitle, Session session)
    {
        MQBuilder directionBuilder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "d");
        // строим статистику только по указанным направлениям приема
        directionBuilder.add(MQExpression.in("d", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, enrollmentDirectionList));
        // применяем необязательные фильтры
        if (Boolean.TRUE.equals(model.getSettings().get("without")))
            directionBuilder.add(MQExpression.notEq("d", RequestedEnrollmentDirection.state().code().s(), UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        if (model.getCompensationType() != null)
            directionBuilder.add(MQExpression.eq("d", RequestedEnrollmentDirection.L_COMPENSATION_TYPE, model.getCompensationType()));
        if (model.getStudentCategoryList() != null && !model.getStudentCategoryList().isEmpty())
            directionBuilder.add(MQExpression.in("d", RequestedEnrollmentDirection.L_STUDENT_CATEGORY, model.getStudentCategoryList()));
        if (groupTitle != null)
            directionBuilder.add(MQExpression.eq("d", RequestedEnrollmentDirection.P_GROUP, groupTitle));
        return new EntrantDataUtil(session, model.getEnrollmentCampaign(), directionBuilder);
    }
}
