/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EntrantAgeDistrib.EntrantAgeDistribAdd;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.utils.CommonYesNoUIObject;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.entity.report.EntrantAgeDistribReport;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.ArrayList;
import java.util.Date;

/**
 * @author ekachanova
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    @SuppressWarnings("deprecation")
    public void prepare(final Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.getReport().setDateFrom(model.getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : model.getEnrollmentCampaign().getStartDate());
        model.getReport().setDateTo(CoreDateUtils.getDayLastTimeMoment(new Date()));

        model.setQualificationListModel(new QualificationModel(getSession()));
        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));
        model.setStudentCategoryListModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(StudentCategory.class)));

        model.setFormativeOrgUnitListModel(MultiEnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setTerritorialOrgUnitListModel(MultiEnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setEducationLevelHighSchoolListModel(MultiEnrollmentDirectionUtil.createEducationLevelsHighSchoolModel(model));
        model.setDevelopFormListModel(MultiEnrollmentDirectionUtil.createDevelopFormModel(model));
        model.setDevelopConditionListModel(MultiEnrollmentDirectionUtil.createDevelopConditionModel(model));
        model.setDevelopTechListModel(MultiEnrollmentDirectionUtil.createDevelopTechModel(model));
        model.setDevelopPeriodListModel(MultiEnrollmentDirectionUtil.createDevelopPeriodModel(model));

        model.setStudentCategoryList(new ArrayList<>());
        model.setQualificationList(new ArrayList<>());
        model.setFormativeOrgUnitList(new ArrayList<>());
        model.setTerritorialOrgUnitList(new ArrayList<>());
        model.setEducationLevelHighSchoolList(new ArrayList<>());
        model.setDevelopFormList(new ArrayList<>());
        model.setDevelopConditionList(new ArrayList<>());
        model.setDevelopTechList(new ArrayList<>());
        model.setDevelopPeriodList(new ArrayList<>());

        model.setParallelList(CommonYesNoUIObject.createYesNoList());

        model.setParameters(new MultiEnrollmentDirectionUtil.Parameters());
        model.getParameters().setRequired(false, MultiEnrollmentDirectionUtil.Parameters.Filter.values());
    }

    @Override
    public void update(Model model)
    {
        final Session session = getSession();

        EntrantAgeDistribReport report = model.getReport();
        report.setFormingDate(new Date());

        if (model.isStudentCategoryActive())
            report.setStudentCategoryTitle(UniStringUtils.join(model.getStudentCategoryList(), "title", "; "));
        if (model.isQualificationActive())
            report.setQualificationTitle(UniStringUtils.join(model.getQualificationList(), "title", "; "));
        if (model.isFormativeOrgUnitActive())
            report.setFormativeOrgUnitTitle(UniStringUtils.join(model.getFormativeOrgUnitList(), OrgUnit.P_FULL_TITLE, "; "));
        if (model.isTerritorialOrgUnitActive())
            report.setTerritorialOrgUnitTitle(StringUtils.join(CommonBaseUtil.getPropertiesList(model.getTerritorialOrgUnitList(), OrgUnit.P_TERRITORIAL_FULL_TITLE), "; "));
        if (model.isEducationLevelHighSchoolActive())
            report.setEducationLevelHighSchoolTitle(UniStringUtils.join(model.getEducationLevelHighSchoolList(), EducationLevelsHighSchool.P_DISPLAYABLE_TITLE, "; "));
        if (model.isDevelopFormActive())
            report.setDevelopFormTitle(UniStringUtils.join(model.getDevelopFormList(), "title", "; "));
        if (model.isDevelopConditionActive())
            report.setDevelopConditionTitle(UniStringUtils.join(model.getDevelopConditionList(), "title", "; "));
        if (model.isDevelopTechActive())
            report.setDevelopTechTitle(UniStringUtils.join(model.getDevelopTechList(), "title", "; "));
        if (model.isDevelopPeriodActive())
            report.setDevelopPeriodTitle(UniStringUtils.join(model.getDevelopPeriodList(), "title", "; "));
        if (model.isParallelActive())
            report.setExcludeParallel(model.getParallel().isTrue());

        // some fixes
        if (!model.isCompensTypeActive())
            model.getReport().setCompensationType(null);

        DatabaseFile databaseFile = new EntrantAgeDistribReportBuilder(model, session).getContent();
        session.save(databaseFile);

        report.setContent(databaseFile);
        session.save(report);
    }
}
