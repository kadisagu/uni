/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcEntrant.ui.CustomStateAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniec.base.bo.EcEntrant.EcEntrantManager;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantCustomState;

import java.util.Date;

/**
 * @author nvankov
 * @since 4/3/13
 */
@Input
        ({
                @Bind(key = UIPresenter.PUBLISHER_ID, binding = "entrantCustomStateId"),
                @Bind(key = "entrantId", binding = "entrantId")

        })
public class EcEntrantCustomStateAddEditUI extends UIPresenter
{
    private Long _entrantId;
    private Long _entrantCustomStateId;
    private EntrantCustomState _entrantCustomState;

    public Long getEntrantId()
    {
        return _entrantId;
    }

    public void setEntrantId(Long entrantId)
    {
        _entrantId = entrantId;
    }

    public Long getEntrantCustomStateId()
    {
        return _entrantCustomStateId;
    }

    public void setEntrantCustomStateId(Long entrantCustomStateId)
    {
        _entrantCustomStateId = entrantCustomStateId;
    }

    public EntrantCustomState getEntrantCustomState()
    {
        return _entrantCustomState;
    }

    public void setEntrantCustomState(EntrantCustomState entrantCustomState)
    {
        _entrantCustomState = entrantCustomState;
    }

    @Override
    public void onComponentRefresh()
    {
        if (isEditForm())
        {
            _entrantCustomState = DataAccessServices.dao().get(_entrantCustomStateId);
        }
        else
        {
            _entrantCustomState = new EntrantCustomState();
            _entrantCustomState.setEntrant(DataAccessServices.dao().<Entrant>get(_entrantId));
        }
    }

    public boolean isAddForm()
    {
        return null == _entrantCustomStateId;
    }

    public boolean isEditForm()
    {
        return !isAddForm();
    }

    public String getSticker()
    {
        return _uiConfig.getProperty(isAddForm() ? "ui.sticker.add" : "ui.sticker.edit");
    }

    public void onClickApply()
    {
        Date beginDate = _entrantCustomState.getBeginDate();
        Date endDate = _entrantCustomState.getEndDate();
        if(null != beginDate && null != endDate)
        {
            if(beginDate.after(endDate))
            {
                _uiSupport.error("«Дата начала действия статуса» должна быть не больше «Дата окончания действия статуса»", "beginDate", "endDate");
            }
        }
        if(!getUserContext().getErrorCollector().hasErrors())
        {
            EcEntrantManager.instance().entrantCustomStateDAO().saveOrUpdate(_entrantCustomState);
            deactivate();
        }
    }
}
