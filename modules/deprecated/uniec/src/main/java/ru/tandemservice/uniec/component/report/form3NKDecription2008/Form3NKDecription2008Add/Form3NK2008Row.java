/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.form3NKDecription2008.Form3NKDecription2008Add;

import java.util.List;

import org.tandemframework.core.common.ITitled;

/**
 * @author vip_delete
 * @since 19.05.2009
 */
class Form3NK2008Row implements ITitled
{
    private String _title;
    private List<Form3NK2008SubRow> _subRowList;

    public Form3NK2008Row(String title, List<Form3NK2008SubRow> subRowList)
    {
        _title = title;
        _subRowList = subRowList;
    }

    // Getters

    @Override
    public String getTitle()
    {
        return _title;
    }

    public List<Form3NK2008SubRow> getSubRowList()
    {
        return _subRowList;
    }
}
