/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec.base.bo.EcProfileDistribution.util;

import java.util.Map;

/**
 * @author Vasily Zhukov
 * @since 10.05.2012
 */
public class EcgpQuotaUsedDTO implements IEcgpQuotaUsedDTO
{
    private int _totalUsed;

    private Map<Long, Integer> _usedMap;

    public EcgpQuotaUsedDTO(int totalUsed, Map<Long, Integer> usedMap)
    {
        _totalUsed = totalUsed;
        _usedMap = usedMap;
    }

    // Getters

    public int getTotalUsed()
    {
        return _totalUsed;
    }

    public Map<Long, Integer> getUsedMap()
    {
        return _usedMap;
    }
}
