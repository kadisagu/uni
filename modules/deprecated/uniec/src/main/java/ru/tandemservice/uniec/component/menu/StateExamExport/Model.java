/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.menu.StateExamExport;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.Date;
import java.util.List;

/**
 * @author vip_delete
 * @since 06.04.2009
 */
public class Model implements IEnrollmentCampaignSelectModel
{
    public static final Long CERTIFICATE_ACCEPTED_ID = 0L;
    public static final Long CERTIFICATE_NOT_ACCEPTED_ID = 1L;

    public static final Long CERTIFICATE_SENT_ID = 0L;
    public static final Long CERTIFICATE_NOT_SENT_ID = 1L;

    public static final Long CERTIFICATE_HOLDING_ID = 0L;
    public static final Long CERTIFICATE_NOT_HOLDING_ID = 1L;

    // приемная кампания
    private List<EnrollmentCampaign> _enrollmentCampaignList;
    private EnrollmentCampaign _enrollmentCampaign;

    // тип экспорта
    private List<IdentifiableWrapper> _exportTypeList;
    private IdentifiableWrapper _exportType;

    // даты с и по
    private Date _dateFrom;
    private Date _dateTo;

    // зачтенные сертификаты
    private List<IdentifiableWrapper> _acceptedList;
    private IdentifiableWrapper _accepted;

    // проверенные сертификаты
    private List<IdentifiableWrapper> _sentList;
    private IdentifiableWrapper _sent;

    // Выгружать данные абитуриентов
    private List<IdentifiableWrapper> _certificateHoldingList;
    private IdentifiableWrapper _certificateHolding;

    // Включая граждан не РФ
    private boolean _includeNonRF;

    // Выгружать абитуриентов без баллов, по умолчанию вкл.
    private boolean _includeEntrantWithoutMark = true;

    public boolean isExportByCertificate()
    {
        return getExportType() != null &&
                (getExportType().getId() == UniecDefines.EXPORT_TYPE_BY_CERTIFICATE_NUMBER_WITHOUT_FIO ||
                        getExportType().getId() == UniecDefines.EXPORT_TYPE_BY_CERTIFICATE_NUMBER ||
                        getExportType().getId() == UniecDefines.EXPORT_TYPE_BY_CERTIFICATE_NUMBER_WITH_FIO);
    }

    public boolean isExportByIdentityCard()
    {
        return getExportType() != null &&
                (getExportType().getId() == UniecDefines.EXPORT_TYPE_BY_IDENTITY_CARD ||
                        getExportType().getId() == UniecDefines.EXPORT_TYPE_BY_FIO_WITH_DOC);
    }

    public boolean isExportByIdentityCardWithoutFio()
    {
        return getExportType() != null &&
                getExportType().getId() == UniecDefines.EXPORT_TYPE_BY_IDENTITY_CARD_WITHOUT_FIO;
    }
    
    // IEnrollmentCampaignSelectModel

    @Override
    public IDataSettings getSettings()
    {
        return null;
    }

    // Getters & Setters

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public List<IdentifiableWrapper> getExportTypeList()
    {
        return _exportTypeList;
    }

    public void setExportTypeList(List<IdentifiableWrapper> exportTypeList)
    {
        _exportTypeList = exportTypeList;
    }

    public IdentifiableWrapper getExportType()
    {
        return _exportType;
    }

    public void setExportType(IdentifiableWrapper exportType)
    {
        _exportType = exportType;
    }

    public Date getDateFrom()
    {
        return _dateFrom;
    }

    public void setDateFrom(Date dateFrom)
    {
        _dateFrom = dateFrom;
    }

    public Date getDateTo()
    {
        return _dateTo;
    }

    public void setDateTo(Date dateTo)
    {
        _dateTo = dateTo;
    }

    public List<IdentifiableWrapper> getAcceptedList()
    {
        return _acceptedList;
    }

    public void setAcceptedList(List<IdentifiableWrapper> acceptedList)
    {
        _acceptedList = acceptedList;
    }

    public IdentifiableWrapper getAccepted()
    {
        return _accepted;
    }

    public void setAccepted(IdentifiableWrapper accepted)
    {
        _accepted = accepted;
    }

    public List<IdentifiableWrapper> getSentList()
    {
        return _sentList;
    }

    public void setSentList(List<IdentifiableWrapper> sentList)
    {
        _sentList = sentList;
    }

    public IdentifiableWrapper getSent()
    {
        return _sent;
    }

    public void setSent(IdentifiableWrapper sent)
    {
        _sent = sent;
    }

    public List<IdentifiableWrapper> getCertificateHoldingList()
    {
        return _certificateHoldingList;
    }

    public void setCertificateHoldingList(List<IdentifiableWrapper> certificateHoldingList)
    {
        _certificateHoldingList = certificateHoldingList;
    }

    public IdentifiableWrapper getCertificateHolding()
    {
        return _certificateHolding;
    }

    public void setCertificateHolding(IdentifiableWrapper certificateHolding)
    {
        _certificateHolding = certificateHolding;
    }

    public boolean isIncludeNonRF()
    {
        return _includeNonRF;
    }

    public void setIncludeNonRF(boolean includeNonRF)
    {
        _includeNonRF = includeNonRF;
    }

    public boolean isIncludeEntrantWithoutMark()
    {
        return _includeEntrantWithoutMark;
    }

    public void setIncludeEntrantWithoutMark(boolean includeEntrantWithoutMark)
    {
        _includeEntrantWithoutMark = includeEntrantWithoutMark;
    }
}
