// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EnrollmentAccessCourses.EnrollmentAccessCoursesAdd;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.util.EntrantDataUtil;

import java.util.*;

/**
 * @author oleyba
 * @since 05.05.2010
 */
public class EnrollmentAccessCoursesReportBuilder
{
    private Model model;
    private Session session;

    public EnrollmentAccessCoursesReportBuilder(Model model, Session session)
    {
        this.model = model;
        this.session = session;
    }

    public DatabaseFile getContent()
    {
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ENROLLMENT_ACCESS_COURSES);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());
        TopOrgUnit academy = TopOrgUnit.getInstance();

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("academyTitle", academy.getNominativeCaseTitle() == null ? academy.getTitle() == null ? "" : academy.getTitle() : academy.getNominativeCaseTitle());
        injectModifier.put("createDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));
        injectModifier.put("qualification", UniStringUtils.join(model.getQualificationList(), "title", ", "));
        injectModifier.put("developForm", model.getDevelopForm().getTitle());
        injectModifier.put("compensationTypeStr", model.getCompensationType() != null ? model.getCompensationType().isBudget() ? "За счет средств Федерального бюджета" : "На места с оплатой стоимости обучения" : "" );
        injectModifier.put("ouFormTitle", getNominativeTitles(model.getFormativeOrgUnitList()));
        injectModifier.put("ouTerrTitle", getNominativeTitles(model.getTerritorialOrgUnitList()));
        injectModifier.modify(document);

        EntrantDataUtil util = new EntrantDataUtil(session, model.getReport().getEnrollmentCampaign(), getPreliminaryDirectionBuilder(true));
        Map<Entrant, Set<EntrantAccessCourse>> accessCourseMap = EntrantDataUtil.getAccessCourseMap(session, getPreliminaryDirectionBuilder(false));
        int num = 1;
        DoubleFormatter formatter = DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS;
        List<String[]> tableRows = new ArrayList<>();
        final List<String> marks = new ArrayList<>();
        for (PreliminaryEnrollmentStudent student : getPreliminaryStudents())
        {
            RequestedEnrollmentDirection direction = student.getRequestedEnrollmentDirection();
            Set<ChosenEntranceDiscipline> entranceDisciplines = util.getChosenEntranceDisciplineSet(direction);
            double sum = 0;
            List<String> currentMarks = new ArrayList<>();
            for (ChosenEntranceDiscipline discipline : entranceDisciplines)
            {
                if (discipline.getFinalMark() != null) sum += discipline.getFinalMark();
                currentMarks.add(discipline.getTitle() + " - " + formatter.format(discipline.getFinalMark()));
            }
            Collections.sort(currentMarks);
            marks.add(StringUtils.join(currentMarks, "\\par"));
            Entrant entrant = direction.getEntrantRequest().getEntrant();
            List<String> courses = CommonBaseEntityUtil.getPropertiesList(accessCourseMap.get(entrant), EntrantAccessCourse.course().shortTitle().s());
            Collections.sort(courses);            
            tableRows.add(new String[] {
                    String.valueOf(num++),
                    StringUtils.join(courses, ", "),
                    entrant.getPerson().getFullFio(),
                    direction.getEntrantRequest().getStringNumber(),
                    "",
                    String.valueOf(formatter.format(sum))});

        }

        final int rowCount = tableRows.size();
        RtfTableModifier tableModifier = new RtfTableModifier().put("T", tableRows.toArray(new String[rowCount][]));
        tableModifier.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                for (int i = 0; i < rowCount; i++)
                {
                    RtfRow row = newRowList.get(startIndex + i);
                    // вставляем оценки в ячейки простым текстом, чтобы не экранировались переводы строки
                    {
                        String content = marks.get(i);
                        IRtfText text = RtfBean.getElementFactory().createRtfText(content);
                        text.setRaw(true);
                        row.getCellList().get(4).getElementList().add(text);
                    }
                }
            }
        });
        tableModifier.modify(document);

        DatabaseFile file = new DatabaseFile();
        file.setContent(RtfUtil.toByteArray(document));
        return file;        
    }

    private List<PreliminaryEnrollmentStudent> getPreliminaryStudents()
    {
        MQBuilder builder = getPreliminaryBuilder(true);
        builder.addJoinFetch("r", RequestedEnrollmentDirection.entrantRequest().s(), "req");
        builder.addJoinFetch("req", EntrantRequest.entrant().person().identityCard().s(), "idc");
        builder.addOrder("idc", IdentityCard.lastName().s());
        builder.addOrder("idc", IdentityCard.firstName().s());
        builder.addOrder("idc", IdentityCard.middleName().s());
        return builder.getResultList(session);
    }

    private MQBuilder getPreliminaryDirectionBuilder(boolean withCourses)
    {
        MQBuilder builder = getPreliminaryBuilder(withCourses);
        builder.getSelectAliasList().clear();
        builder.addSelect("r");
        return builder;
    }

    private MQBuilder getPreliminaryBuilder(boolean withCourses)
    {
        MQBuilder builder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "p");
        if (model.isStudentCategoryActive())
            builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.studentCategory().s(), model.getStudentCategoryList()));
        if (model.isCompensTypeActive())
            builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.compensationType().s(), model.getCompensationType()));
        builder.addJoin("p", PreliminaryEnrollmentStudent.educationOrgUnit().s(), "edu");
        if (model.isQualificationActive())
            builder.add(MQExpression.in("edu", EducationOrgUnit.educationLevelHighSchool().educationLevel().qualification().s(), model.getQualificationList()));
        if (model.isFormativeOrgUnitActive())
            builder.add(MQExpression.in("edu", EducationOrgUnit.formativeOrgUnit().s(), model.getFormativeOrgUnitList()));
        if (model.isTerritorialOrgUnitActive())
            builder.add(MQExpression.in("edu", EducationOrgUnit.territorialOrgUnit().s(), model.getTerritorialOrgUnitList()));
        if (model.isEducationLevelHighSchoolActive())
            builder.add(MQExpression.in("edu", EducationOrgUnit.formativeOrgUnit().s(), model.getFormativeOrgUnitList()));
        builder.add(MQExpression.eq("edu", EducationOrgUnit.developForm().s(), model.getDevelopForm()));
        if (model.isDevelopConditionActive())
            builder.add(MQExpression.in("edu", EducationOrgUnit.developCondition().s(), model.getDevelopConditionList()));
        if (model.isDevelopTechActive())
            builder.add(MQExpression.in("edu", EducationOrgUnit.developTech().s(), model.getDevelopTechList()));
        if (model.isDevelopPeriodActive())
            builder.add(MQExpression.in("edu", EducationOrgUnit.developPeriod().s(), model.getDevelopPeriodList()));
        if (model.isParallelActive() && model.getParallel().isTrue())
            builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.P_PARALLEL, false));

        builder.addJoin("p", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, "r");
        builder.add(UniMQExpression.betweenDate("r", RequestedEnrollmentDirection.P_REG_DATE, model.getReport().getDateFrom(), model.getReport().getDateTo()));
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_ENROLLMENT_CAMPAIGN, model.getReport().getEnrollmentCampaign()));
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, Boolean.FALSE));

        if (withCourses)
        {
            MQBuilder coursesBuilder = new MQBuilder(EntrantAccessCourse.ENTITY_CLASS, "__accessCourse", new String[] {EntrantAccessCourse.entrant().id().s()});
            builder.add(MQExpression.in("r", RequestedEnrollmentDirection.entrantRequest().entrant().id().s(), coursesBuilder));
        }
        
        return builder;
    }

    private String getNominativeTitles(List<OrgUnit> formativeOrgUnitList)
    {
        if (null == formativeOrgUnitList || formativeOrgUnitList.isEmpty()) return "";
        List<String> titles = new ArrayList<>();
        for (OrgUnit orgUnit : formativeOrgUnitList)
            titles.add(null == orgUnit.getNominativeCaseTitle() ? orgUnit.getTitle() : orgUnit.getNominativeCaseTitle());
        return StringUtils.join(titles, "\\par");
    }
}
