/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcPreEnroll.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import ru.tandemservice.uniec.base.bo.EcCampaign.EcCampaignManager;
import ru.tandemservice.uniec.base.bo.EcPreEnroll.logic.PreEntrantDSHandler;
import ru.tandemservice.uniec.base.bo.EcPreEnroll.util.PreEntrantDTO;
import ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

/**
 * @author Vasily Zhukov
 * @since 26.05.2011
 */
@Configuration
public class EcPreEnrollList extends BusinessComponentManager
{
    public static final String ENROLLMENT_CAMPAIGN_DS = "enrollmentCampaignDS";
    public static final String PRE_ENTRANT_DS = "preEntrantDS";
    public static final String ENTRANT_CUSTOM_STATE_EXIST_DS = "entrantCustomStateExistDS";
    public static final String ENTRANT_CUSTOM_STATE_DS = "entrantCustomStateDS";
    public static final String ORIGINAL_DOCUMENT_DS = "originalDocumentDS";

    @Bean
    public ColumnListExtPoint preEntrantDS()
    {
        return columnListExtPointBuilder(PRE_ENTRANT_DS)
                .addColumn(textColumn("regNumber", PreEntrantDTO.REG_NUMBER))
                .addColumn(actionColumn("fio", RequestedEnrollmentDirection.entrantRequest().entrant().person().identityCard().fullFio(), "onClickView").create())
                .addColumn(textColumn("sex", RequestedEnrollmentDirection.entrantRequest().entrant().person().identityCard().sex().shortTitle()))
                .addColumn(textColumn("passport", RequestedEnrollmentDirection.entrantRequest().entrant().person().identityCard().fullNumber()))
                .addColumn(textColumn("competitionKind", PreEntrantDTO.COMPETITION_KIND_TITLE))
                .addColumn(textColumn("admissionKind", RequestedEnrollmentDirection.targetAdmissionKind().fullHierarhyTitle()).visible("ui:hasAdmissionKind").create())
                .addColumn(textColumn("contract", RequestedEnrollmentDirection.compensationType().shortTitle()))
                .addColumn(textColumn("studentCategory", RequestedEnrollmentDirection.studentCategory().title()))
                .addColumn(textColumn("state", RequestedEnrollmentDirection.state().title()).width("7").create())
                .addColumn(textColumn("finalMarkStr", PreEntrantDTO.FINAL_MARK_STR))
                .addColumn(textColumn("graduatedProfileEduInstitution", RequestedEnrollmentDirection.graduatedProfileEduInstitution()).formatter(source -> (Boolean) source ? "да" : null).create())
                .addColumn(textColumn("averageMark", PreEntrantDTO.AVERAGE_MARK).formatter(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).create())
                .addColumn(textColumn("totalProfileWorkExperienceMonths", RequestedEnrollmentDirection.P_TOTAL_PROFILE_WORK_EXPERIENCE_MONTHS))
                .addColumn(textColumn("originalDocument", RequestedEnrollmentDirection.P_ORIGINAL_DOCUMENT_HANDED_IN).formatter(source -> (Boolean) source ? "оригиналы" : "копии").create())
                .addColumn(blockColumn("orderType", "orderType"))
                .addColumn(textColumn("enrollmentConditions", PreEntrantDTO.ENROLLMENT_CONDITIONS))
                .addColumn(textColumn("orderNumber", PreEntrantDTO.ORDER_NUMBER))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).disabled(PreEntrantDTO.CHANGE_ENROLLMENT_CONDITION_DISABLED).permissionKey("editEntrantEnrollmentConditions").create())
                .create();
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ENROLLMENT_CAMPAIGN_DS, EcCampaignManager.instance().enrollmentCampaignComboDSHandler()))
                .addDataSource(searchListDS(PRE_ENTRANT_DS, preEntrantDS(), preEntrantDSHandler()))
                .addDataSource(CommonManager.instance().yesNoDSConfig())
                .addDataSource(selectDS(ENTRANT_CUSTOM_STATE_EXIST_DS, entrantCustomStateExistDSHandler()))
                .addDataSource(selectDS(ENTRANT_CUSTOM_STATE_DS, entrantCustomStateDSHandler()))
                .addDataSource(selectDS(ORIGINAL_DOCUMENT_DS, originalDocumentDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> preEntrantDSHandler()
    {
        return new PreEntrantDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> entrantCustomStateExistDSHandler()
    {
        TwinComboDataSourceHandler handler = new TwinComboDataSourceHandler(getName());
        handler.yesTitle("Содержит");
        handler.noTitle("Не содержит");
        return handler;
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> entrantCustomStateDSHandler()
    {
        return EntrantCustomStateCI.defaultSelectDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> originalDocumentDSHandler()
    {
        TwinComboDataSourceHandler handler = new TwinComboDataSourceHandler(getName());
        handler.yesTitle("оригиналы");
        handler.noTitle("копии");
        return handler;
    }
}
