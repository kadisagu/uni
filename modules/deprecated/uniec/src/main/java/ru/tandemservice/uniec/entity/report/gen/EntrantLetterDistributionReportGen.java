package ru.tandemservice.uniec.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.report.EntrantLetterDistributionReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Распределение абитуриентов по буквам алфавита
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntrantLetterDistributionReportGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.report.EntrantLetterDistributionReport";
    public static final String ENTITY_NAME = "entrantLetterDistributionReport";
    public static final int VERSION_HASH = -2018751210;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONTENT = "content";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_DATE_FROM = "dateFrom";
    public static final String P_DATE_TO = "dateTo";
    public static final String P_ENROLLMENT_CAMP_STAGE = "enrollmentCampStage";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String P_QUALIFICATION_TITLE = "qualificationTitle";
    public static final String P_STUDENT_CATEGORY_TITLE = "studentCategoryTitle";
    public static final String P_FORMATIVE_ORG_UNIT_TITLE = "formativeOrgUnitTitle";
    public static final String P_TERRITORIAL_ORG_UNIT_TITLE = "territorialOrgUnitTitle";
    public static final String P_DEVELOP_FORM_TITLE = "developFormTitle";
    public static final String P_DEVELOP_CONDITION_TITLE = "developConditionTitle";
    public static final String P_DEVELOP_TECH_TITLE = "developTechTitle";
    public static final String P_DEVELOP_PERIOD_TITLE = "developPeriodTitle";
    public static final String P_EXCLUDE_PARALLEL = "excludeParallel";

    private DatabaseFile _content;     // Печатная форма
    private Date _formingDate;     // Дата формирования
    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private Date _dateFrom;     // Заявления с
    private Date _dateTo;     // Заявления по
    private String _enrollmentCampStage;     // Стадия приемной кампании
    private CompensationType _compensationType;     // Вид возмещения затрат
    private String _qualificationTitle;     // Квалификация
    private String _studentCategoryTitle;     // Категория поступающего
    private String _formativeOrgUnitTitle;     // Формирующее подр.
    private String _territorialOrgUnitTitle;     // Территориальное подр.
    private String _developFormTitle;     // Форма освоения
    private String _developConditionTitle;     // Условие освоения
    private String _developTechTitle;     // Технология освоения
    private String _developPeriodTitle;     // Срок освоения
    private Boolean _excludeParallel;     // Исключая поступивших на параллельное освоение образовательных программ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     */
    @NotNull
    public Date getDateFrom()
    {
        return _dateFrom;
    }

    /**
     * @param dateFrom Заявления с. Свойство не может быть null.
     */
    public void setDateFrom(Date dateFrom)
    {
        dirty(_dateFrom, dateFrom);
        _dateFrom = dateFrom;
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     */
    @NotNull
    public Date getDateTo()
    {
        return _dateTo;
    }

    /**
     * @param dateTo Заявления по. Свойство не может быть null.
     */
    public void setDateTo(Date dateTo)
    {
        dirty(_dateTo, dateTo);
        _dateTo = dateTo;
    }

    /**
     * @return Стадия приемной кампании.
     */
    @Length(max=255)
    public String getEnrollmentCampStage()
    {
        return _enrollmentCampStage;
    }

    /**
     * @param enrollmentCampStage Стадия приемной кампании.
     */
    public void setEnrollmentCampStage(String enrollmentCampStage)
    {
        dirty(_enrollmentCampStage, enrollmentCampStage);
        _enrollmentCampStage = enrollmentCampStage;
    }

    /**
     * @return Вид возмещения затрат.
     */
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Квалификация.
     */
    @Length(max=255)
    public String getQualificationTitle()
    {
        return _qualificationTitle;
    }

    /**
     * @param qualificationTitle Квалификация.
     */
    public void setQualificationTitle(String qualificationTitle)
    {
        dirty(_qualificationTitle, qualificationTitle);
        _qualificationTitle = qualificationTitle;
    }

    /**
     * @return Категория поступающего.
     */
    @Length(max=255)
    public String getStudentCategoryTitle()
    {
        return _studentCategoryTitle;
    }

    /**
     * @param studentCategoryTitle Категория поступающего.
     */
    public void setStudentCategoryTitle(String studentCategoryTitle)
    {
        dirty(_studentCategoryTitle, studentCategoryTitle);
        _studentCategoryTitle = studentCategoryTitle;
    }

    /**
     * @return Формирующее подр..
     */
    public String getFormativeOrgUnitTitle()
    {
        return _formativeOrgUnitTitle;
    }

    /**
     * @param formativeOrgUnitTitle Формирующее подр..
     */
    public void setFormativeOrgUnitTitle(String formativeOrgUnitTitle)
    {
        dirty(_formativeOrgUnitTitle, formativeOrgUnitTitle);
        _formativeOrgUnitTitle = formativeOrgUnitTitle;
    }

    /**
     * @return Территориальное подр..
     */
    public String getTerritorialOrgUnitTitle()
    {
        return _territorialOrgUnitTitle;
    }

    /**
     * @param territorialOrgUnitTitle Территориальное подр..
     */
    public void setTerritorialOrgUnitTitle(String territorialOrgUnitTitle)
    {
        dirty(_territorialOrgUnitTitle, territorialOrgUnitTitle);
        _territorialOrgUnitTitle = territorialOrgUnitTitle;
    }

    /**
     * @return Форма освоения.
     */
    @Length(max=255)
    public String getDevelopFormTitle()
    {
        return _developFormTitle;
    }

    /**
     * @param developFormTitle Форма освоения.
     */
    public void setDevelopFormTitle(String developFormTitle)
    {
        dirty(_developFormTitle, developFormTitle);
        _developFormTitle = developFormTitle;
    }

    /**
     * @return Условие освоения.
     */
    @Length(max=255)
    public String getDevelopConditionTitle()
    {
        return _developConditionTitle;
    }

    /**
     * @param developConditionTitle Условие освоения.
     */
    public void setDevelopConditionTitle(String developConditionTitle)
    {
        dirty(_developConditionTitle, developConditionTitle);
        _developConditionTitle = developConditionTitle;
    }

    /**
     * @return Технология освоения.
     */
    @Length(max=255)
    public String getDevelopTechTitle()
    {
        return _developTechTitle;
    }

    /**
     * @param developTechTitle Технология освоения.
     */
    public void setDevelopTechTitle(String developTechTitle)
    {
        dirty(_developTechTitle, developTechTitle);
        _developTechTitle = developTechTitle;
    }

    /**
     * @return Срок освоения.
     */
    @Length(max=255)
    public String getDevelopPeriodTitle()
    {
        return _developPeriodTitle;
    }

    /**
     * @param developPeriodTitle Срок освоения.
     */
    public void setDevelopPeriodTitle(String developPeriodTitle)
    {
        dirty(_developPeriodTitle, developPeriodTitle);
        _developPeriodTitle = developPeriodTitle;
    }

    /**
     * @return Исключая поступивших на параллельное освоение образовательных программ.
     */
    public Boolean getExcludeParallel()
    {
        return _excludeParallel;
    }

    /**
     * @param excludeParallel Исключая поступивших на параллельное освоение образовательных программ.
     */
    public void setExcludeParallel(Boolean excludeParallel)
    {
        dirty(_excludeParallel, excludeParallel);
        _excludeParallel = excludeParallel;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntrantLetterDistributionReportGen)
        {
            setContent(((EntrantLetterDistributionReport)another).getContent());
            setFormingDate(((EntrantLetterDistributionReport)another).getFormingDate());
            setEnrollmentCampaign(((EntrantLetterDistributionReport)another).getEnrollmentCampaign());
            setDateFrom(((EntrantLetterDistributionReport)another).getDateFrom());
            setDateTo(((EntrantLetterDistributionReport)another).getDateTo());
            setEnrollmentCampStage(((EntrantLetterDistributionReport)another).getEnrollmentCampStage());
            setCompensationType(((EntrantLetterDistributionReport)another).getCompensationType());
            setQualificationTitle(((EntrantLetterDistributionReport)another).getQualificationTitle());
            setStudentCategoryTitle(((EntrantLetterDistributionReport)another).getStudentCategoryTitle());
            setFormativeOrgUnitTitle(((EntrantLetterDistributionReport)another).getFormativeOrgUnitTitle());
            setTerritorialOrgUnitTitle(((EntrantLetterDistributionReport)another).getTerritorialOrgUnitTitle());
            setDevelopFormTitle(((EntrantLetterDistributionReport)another).getDevelopFormTitle());
            setDevelopConditionTitle(((EntrantLetterDistributionReport)another).getDevelopConditionTitle());
            setDevelopTechTitle(((EntrantLetterDistributionReport)another).getDevelopTechTitle());
            setDevelopPeriodTitle(((EntrantLetterDistributionReport)another).getDevelopPeriodTitle());
            setExcludeParallel(((EntrantLetterDistributionReport)another).getExcludeParallel());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntrantLetterDistributionReportGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntrantLetterDistributionReport.class;
        }

        public T newInstance()
        {
            return (T) new EntrantLetterDistributionReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "content":
                    return obj.getContent();
                case "formingDate":
                    return obj.getFormingDate();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "dateFrom":
                    return obj.getDateFrom();
                case "dateTo":
                    return obj.getDateTo();
                case "enrollmentCampStage":
                    return obj.getEnrollmentCampStage();
                case "compensationType":
                    return obj.getCompensationType();
                case "qualificationTitle":
                    return obj.getQualificationTitle();
                case "studentCategoryTitle":
                    return obj.getStudentCategoryTitle();
                case "formativeOrgUnitTitle":
                    return obj.getFormativeOrgUnitTitle();
                case "territorialOrgUnitTitle":
                    return obj.getTerritorialOrgUnitTitle();
                case "developFormTitle":
                    return obj.getDevelopFormTitle();
                case "developConditionTitle":
                    return obj.getDevelopConditionTitle();
                case "developTechTitle":
                    return obj.getDevelopTechTitle();
                case "developPeriodTitle":
                    return obj.getDevelopPeriodTitle();
                case "excludeParallel":
                    return obj.getExcludeParallel();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "dateFrom":
                    obj.setDateFrom((Date) value);
                    return;
                case "dateTo":
                    obj.setDateTo((Date) value);
                    return;
                case "enrollmentCampStage":
                    obj.setEnrollmentCampStage((String) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "qualificationTitle":
                    obj.setQualificationTitle((String) value);
                    return;
                case "studentCategoryTitle":
                    obj.setStudentCategoryTitle((String) value);
                    return;
                case "formativeOrgUnitTitle":
                    obj.setFormativeOrgUnitTitle((String) value);
                    return;
                case "territorialOrgUnitTitle":
                    obj.setTerritorialOrgUnitTitle((String) value);
                    return;
                case "developFormTitle":
                    obj.setDevelopFormTitle((String) value);
                    return;
                case "developConditionTitle":
                    obj.setDevelopConditionTitle((String) value);
                    return;
                case "developTechTitle":
                    obj.setDevelopTechTitle((String) value);
                    return;
                case "developPeriodTitle":
                    obj.setDevelopPeriodTitle((String) value);
                    return;
                case "excludeParallel":
                    obj.setExcludeParallel((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "content":
                        return true;
                case "formingDate":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "dateFrom":
                        return true;
                case "dateTo":
                        return true;
                case "enrollmentCampStage":
                        return true;
                case "compensationType":
                        return true;
                case "qualificationTitle":
                        return true;
                case "studentCategoryTitle":
                        return true;
                case "formativeOrgUnitTitle":
                        return true;
                case "territorialOrgUnitTitle":
                        return true;
                case "developFormTitle":
                        return true;
                case "developConditionTitle":
                        return true;
                case "developTechTitle":
                        return true;
                case "developPeriodTitle":
                        return true;
                case "excludeParallel":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "content":
                    return true;
                case "formingDate":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "dateFrom":
                    return true;
                case "dateTo":
                    return true;
                case "enrollmentCampStage":
                    return true;
                case "compensationType":
                    return true;
                case "qualificationTitle":
                    return true;
                case "studentCategoryTitle":
                    return true;
                case "formativeOrgUnitTitle":
                    return true;
                case "territorialOrgUnitTitle":
                    return true;
                case "developFormTitle":
                    return true;
                case "developConditionTitle":
                    return true;
                case "developTechTitle":
                    return true;
                case "developPeriodTitle":
                    return true;
                case "excludeParallel":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "content":
                    return DatabaseFile.class;
                case "formingDate":
                    return Date.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "dateFrom":
                    return Date.class;
                case "dateTo":
                    return Date.class;
                case "enrollmentCampStage":
                    return String.class;
                case "compensationType":
                    return CompensationType.class;
                case "qualificationTitle":
                    return String.class;
                case "studentCategoryTitle":
                    return String.class;
                case "formativeOrgUnitTitle":
                    return String.class;
                case "territorialOrgUnitTitle":
                    return String.class;
                case "developFormTitle":
                    return String.class;
                case "developConditionTitle":
                    return String.class;
                case "developTechTitle":
                    return String.class;
                case "developPeriodTitle":
                    return String.class;
                case "excludeParallel":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntrantLetterDistributionReport> _dslPath = new Path<EntrantLetterDistributionReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntrantLetterDistributionReport");
    }
            

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantLetterDistributionReport#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantLetterDistributionReport#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantLetterDistributionReport#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantLetterDistributionReport#getDateFrom()
     */
    public static PropertyPath<Date> dateFrom()
    {
        return _dslPath.dateFrom();
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantLetterDistributionReport#getDateTo()
     */
    public static PropertyPath<Date> dateTo()
    {
        return _dslPath.dateTo();
    }

    /**
     * @return Стадия приемной кампании.
     * @see ru.tandemservice.uniec.entity.report.EntrantLetterDistributionReport#getEnrollmentCampStage()
     */
    public static PropertyPath<String> enrollmentCampStage()
    {
        return _dslPath.enrollmentCampStage();
    }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.uniec.entity.report.EntrantLetterDistributionReport#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.uniec.entity.report.EntrantLetterDistributionReport#getQualificationTitle()
     */
    public static PropertyPath<String> qualificationTitle()
    {
        return _dslPath.qualificationTitle();
    }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.uniec.entity.report.EntrantLetterDistributionReport#getStudentCategoryTitle()
     */
    public static PropertyPath<String> studentCategoryTitle()
    {
        return _dslPath.studentCategoryTitle();
    }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.uniec.entity.report.EntrantLetterDistributionReport#getFormativeOrgUnitTitle()
     */
    public static PropertyPath<String> formativeOrgUnitTitle()
    {
        return _dslPath.formativeOrgUnitTitle();
    }

    /**
     * @return Территориальное подр..
     * @see ru.tandemservice.uniec.entity.report.EntrantLetterDistributionReport#getTerritorialOrgUnitTitle()
     */
    public static PropertyPath<String> territorialOrgUnitTitle()
    {
        return _dslPath.territorialOrgUnitTitle();
    }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.uniec.entity.report.EntrantLetterDistributionReport#getDevelopFormTitle()
     */
    public static PropertyPath<String> developFormTitle()
    {
        return _dslPath.developFormTitle();
    }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.uniec.entity.report.EntrantLetterDistributionReport#getDevelopConditionTitle()
     */
    public static PropertyPath<String> developConditionTitle()
    {
        return _dslPath.developConditionTitle();
    }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.uniec.entity.report.EntrantLetterDistributionReport#getDevelopTechTitle()
     */
    public static PropertyPath<String> developTechTitle()
    {
        return _dslPath.developTechTitle();
    }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.uniec.entity.report.EntrantLetterDistributionReport#getDevelopPeriodTitle()
     */
    public static PropertyPath<String> developPeriodTitle()
    {
        return _dslPath.developPeriodTitle();
    }

    /**
     * @return Исключая поступивших на параллельное освоение образовательных программ.
     * @see ru.tandemservice.uniec.entity.report.EntrantLetterDistributionReport#getExcludeParallel()
     */
    public static PropertyPath<Boolean> excludeParallel()
    {
        return _dslPath.excludeParallel();
    }

    public static class Path<E extends EntrantLetterDistributionReport> extends EntityPath<E>
    {
        private DatabaseFile.Path<DatabaseFile> _content;
        private PropertyPath<Date> _formingDate;
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<Date> _dateFrom;
        private PropertyPath<Date> _dateTo;
        private PropertyPath<String> _enrollmentCampStage;
        private CompensationType.Path<CompensationType> _compensationType;
        private PropertyPath<String> _qualificationTitle;
        private PropertyPath<String> _studentCategoryTitle;
        private PropertyPath<String> _formativeOrgUnitTitle;
        private PropertyPath<String> _territorialOrgUnitTitle;
        private PropertyPath<String> _developFormTitle;
        private PropertyPath<String> _developConditionTitle;
        private PropertyPath<String> _developTechTitle;
        private PropertyPath<String> _developPeriodTitle;
        private PropertyPath<Boolean> _excludeParallel;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantLetterDistributionReport#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantLetterDistributionReport#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(EntrantLetterDistributionReportGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantLetterDistributionReport#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantLetterDistributionReport#getDateFrom()
     */
        public PropertyPath<Date> dateFrom()
        {
            if(_dateFrom == null )
                _dateFrom = new PropertyPath<Date>(EntrantLetterDistributionReportGen.P_DATE_FROM, this);
            return _dateFrom;
        }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantLetterDistributionReport#getDateTo()
     */
        public PropertyPath<Date> dateTo()
        {
            if(_dateTo == null )
                _dateTo = new PropertyPath<Date>(EntrantLetterDistributionReportGen.P_DATE_TO, this);
            return _dateTo;
        }

    /**
     * @return Стадия приемной кампании.
     * @see ru.tandemservice.uniec.entity.report.EntrantLetterDistributionReport#getEnrollmentCampStage()
     */
        public PropertyPath<String> enrollmentCampStage()
        {
            if(_enrollmentCampStage == null )
                _enrollmentCampStage = new PropertyPath<String>(EntrantLetterDistributionReportGen.P_ENROLLMENT_CAMP_STAGE, this);
            return _enrollmentCampStage;
        }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.uniec.entity.report.EntrantLetterDistributionReport#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.uniec.entity.report.EntrantLetterDistributionReport#getQualificationTitle()
     */
        public PropertyPath<String> qualificationTitle()
        {
            if(_qualificationTitle == null )
                _qualificationTitle = new PropertyPath<String>(EntrantLetterDistributionReportGen.P_QUALIFICATION_TITLE, this);
            return _qualificationTitle;
        }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.uniec.entity.report.EntrantLetterDistributionReport#getStudentCategoryTitle()
     */
        public PropertyPath<String> studentCategoryTitle()
        {
            if(_studentCategoryTitle == null )
                _studentCategoryTitle = new PropertyPath<String>(EntrantLetterDistributionReportGen.P_STUDENT_CATEGORY_TITLE, this);
            return _studentCategoryTitle;
        }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.uniec.entity.report.EntrantLetterDistributionReport#getFormativeOrgUnitTitle()
     */
        public PropertyPath<String> formativeOrgUnitTitle()
        {
            if(_formativeOrgUnitTitle == null )
                _formativeOrgUnitTitle = new PropertyPath<String>(EntrantLetterDistributionReportGen.P_FORMATIVE_ORG_UNIT_TITLE, this);
            return _formativeOrgUnitTitle;
        }

    /**
     * @return Территориальное подр..
     * @see ru.tandemservice.uniec.entity.report.EntrantLetterDistributionReport#getTerritorialOrgUnitTitle()
     */
        public PropertyPath<String> territorialOrgUnitTitle()
        {
            if(_territorialOrgUnitTitle == null )
                _territorialOrgUnitTitle = new PropertyPath<String>(EntrantLetterDistributionReportGen.P_TERRITORIAL_ORG_UNIT_TITLE, this);
            return _territorialOrgUnitTitle;
        }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.uniec.entity.report.EntrantLetterDistributionReport#getDevelopFormTitle()
     */
        public PropertyPath<String> developFormTitle()
        {
            if(_developFormTitle == null )
                _developFormTitle = new PropertyPath<String>(EntrantLetterDistributionReportGen.P_DEVELOP_FORM_TITLE, this);
            return _developFormTitle;
        }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.uniec.entity.report.EntrantLetterDistributionReport#getDevelopConditionTitle()
     */
        public PropertyPath<String> developConditionTitle()
        {
            if(_developConditionTitle == null )
                _developConditionTitle = new PropertyPath<String>(EntrantLetterDistributionReportGen.P_DEVELOP_CONDITION_TITLE, this);
            return _developConditionTitle;
        }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.uniec.entity.report.EntrantLetterDistributionReport#getDevelopTechTitle()
     */
        public PropertyPath<String> developTechTitle()
        {
            if(_developTechTitle == null )
                _developTechTitle = new PropertyPath<String>(EntrantLetterDistributionReportGen.P_DEVELOP_TECH_TITLE, this);
            return _developTechTitle;
        }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.uniec.entity.report.EntrantLetterDistributionReport#getDevelopPeriodTitle()
     */
        public PropertyPath<String> developPeriodTitle()
        {
            if(_developPeriodTitle == null )
                _developPeriodTitle = new PropertyPath<String>(EntrantLetterDistributionReportGen.P_DEVELOP_PERIOD_TITLE, this);
            return _developPeriodTitle;
        }

    /**
     * @return Исключая поступивших на параллельное освоение образовательных программ.
     * @see ru.tandemservice.uniec.entity.report.EntrantLetterDistributionReport#getExcludeParallel()
     */
        public PropertyPath<Boolean> excludeParallel()
        {
            if(_excludeParallel == null )
                _excludeParallel = new PropertyPath<Boolean>(EntrantLetterDistributionReportGen.P_EXCLUDE_PARALLEL, this);
            return _excludeParallel;
        }

        public Class getEntityClass()
        {
            return EntrantLetterDistributionReport.class;
        }

        public String getEntityName()
        {
            return "entrantLetterDistributionReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
