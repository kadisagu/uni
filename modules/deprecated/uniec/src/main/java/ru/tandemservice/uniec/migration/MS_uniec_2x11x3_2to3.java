package ru.tandemservice.uniec.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniec_2x11x3_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.11.3")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность entrantRequest

        SQLSelectQuery selectQuery = new SQLSelectQuery()
                .from(SQLFrom.table("entrantrequest_t", "req"))
                .column("req.id", "request")
                .column("req.methoddeliverynreturndocs_id", "method")
                .where("req.methoddeliverynreturndocs_id is not null");

        Statement selectStatement = tool.getConnection().createStatement();
        selectStatement.execute(tool.getDialect().getSQLTranslator().toSql(selectQuery));

        ResultSet selectResult = selectStatement.getResultSet();
        HashMap<Long, Long> methodsByRequest = new HashMap<>();
        while (selectResult.next())
            methodsByRequest.put(selectResult.getLong("request"), selectResult.getLong("method"));


        // удалено свойство methodDeliveryNReturnDocs
        {
            // удалить колонку
            tool.dropColumn("entrantrequest_t", "methoddeliverynreturndocs_id");
        }

        // создано свойство methodDeliveryDocs
        {
            // создать колонку
            tool.createColumn("entrantrequest_t", new DBColumn("methoddeliverydocs_id", DBType.LONG));
        }

        // создано свойство methodReturnDocs
        {
            // создать колонку
            tool.createColumn("entrantrequest_t", new DBColumn("methodreturndocs_id", DBType.LONG));
        }

        PreparedStatement update = tool.prepareStatement("update entrantrequest_t " +
                                                                 "set methoddeliverydocs_id = ?, methodreturndocs_id = ? " +
                                                                 "where id = ?");

        for (Map.Entry<Long, Long> entry : methodsByRequest.entrySet())
        {
            Long request = entry.getKey();
            Long method = entry.getValue();
            update.clearParameters();
            update.setLong(1, method);
            update.setLong(2, method);
            update.setLong(3, request);
            update.executeUpdate();
        }
    }
}