package ru.tandemservice.uniec.base.entity.ecg.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistribution;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistributionDetail;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistributionState;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Уточняющее распределение
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EcgDistributionDetailGen extends EntityBase
 implements INaturalIdentifiable<EcgDistributionDetailGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.base.entity.ecg.EcgDistributionDetail";
    public static final String ENTITY_NAME = "ecgDistributionDetail";
    public static final int VERSION_HASH = 850613919;
    private static IEntityMeta ENTITY_META;

    public static final String L_DISTRIBUTION = "distribution";
    public static final String L_STATE = "state";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String P_APPROVAL_DATE = "approvalDate";

    private EcgDistribution _distribution;     // Основное распределение абитуриентов
    private EcgDistributionState _state;     // Состояние распределения
    private Date _formingDate;     // Дата формирования
    private Date _approvalDate;     // Дата утверждения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Основное распределение абитуриентов. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EcgDistribution getDistribution()
    {
        return _distribution;
    }

    /**
     * @param distribution Основное распределение абитуриентов. Свойство не может быть null и должно быть уникальным.
     */
    public void setDistribution(EcgDistribution distribution)
    {
        dirty(_distribution, distribution);
        _distribution = distribution;
    }

    /**
     * @return Состояние распределения. Свойство не может быть null.
     */
    @NotNull
    public EcgDistributionState getState()
    {
        return _state;
    }

    /**
     * @param state Состояние распределения. Свойство не может быть null.
     */
    public void setState(EcgDistributionState state)
    {
        dirty(_state, state);
        _state = state;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Дата утверждения.
     */
    public Date getApprovalDate()
    {
        return _approvalDate;
    }

    /**
     * @param approvalDate Дата утверждения.
     */
    public void setApprovalDate(Date approvalDate)
    {
        dirty(_approvalDate, approvalDate);
        _approvalDate = approvalDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EcgDistributionDetailGen)
        {
            if (withNaturalIdProperties)
            {
                setDistribution(((EcgDistributionDetail)another).getDistribution());
            }
            setState(((EcgDistributionDetail)another).getState());
            setFormingDate(((EcgDistributionDetail)another).getFormingDate());
            setApprovalDate(((EcgDistributionDetail)another).getApprovalDate());
        }
    }

    public INaturalId<EcgDistributionDetailGen> getNaturalId()
    {
        return new NaturalId(getDistribution());
    }

    public static class NaturalId extends NaturalIdBase<EcgDistributionDetailGen>
    {
        private static final String PROXY_NAME = "EcgDistributionDetailNaturalProxy";

        private Long _distribution;

        public NaturalId()
        {}

        public NaturalId(EcgDistribution distribution)
        {
            _distribution = ((IEntity) distribution).getId();
        }

        public Long getDistribution()
        {
            return _distribution;
        }

        public void setDistribution(Long distribution)
        {
            _distribution = distribution;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EcgDistributionDetailGen.NaturalId) ) return false;

            EcgDistributionDetailGen.NaturalId that = (NaturalId) o;

            if( !equals(getDistribution(), that.getDistribution()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getDistribution());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getDistribution());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EcgDistributionDetailGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EcgDistributionDetail.class;
        }

        public T newInstance()
        {
            return (T) new EcgDistributionDetail();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "distribution":
                    return obj.getDistribution();
                case "state":
                    return obj.getState();
                case "formingDate":
                    return obj.getFormingDate();
                case "approvalDate":
                    return obj.getApprovalDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "distribution":
                    obj.setDistribution((EcgDistribution) value);
                    return;
                case "state":
                    obj.setState((EcgDistributionState) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "approvalDate":
                    obj.setApprovalDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "distribution":
                        return true;
                case "state":
                        return true;
                case "formingDate":
                        return true;
                case "approvalDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "distribution":
                    return true;
                case "state":
                    return true;
                case "formingDate":
                    return true;
                case "approvalDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "distribution":
                    return EcgDistribution.class;
                case "state":
                    return EcgDistributionState.class;
                case "formingDate":
                    return Date.class;
                case "approvalDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EcgDistributionDetail> _dslPath = new Path<EcgDistributionDetail>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EcgDistributionDetail");
    }
            

    /**
     * @return Основное распределение абитуриентов. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgDistributionDetail#getDistribution()
     */
    public static EcgDistribution.Path<EcgDistribution> distribution()
    {
        return _dslPath.distribution();
    }

    /**
     * @return Состояние распределения. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgDistributionDetail#getState()
     */
    public static EcgDistributionState.Path<EcgDistributionState> state()
    {
        return _dslPath.state();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgDistributionDetail#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Дата утверждения.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgDistributionDetail#getApprovalDate()
     */
    public static PropertyPath<Date> approvalDate()
    {
        return _dslPath.approvalDate();
    }

    public static class Path<E extends EcgDistributionDetail> extends EntityPath<E>
    {
        private EcgDistribution.Path<EcgDistribution> _distribution;
        private EcgDistributionState.Path<EcgDistributionState> _state;
        private PropertyPath<Date> _formingDate;
        private PropertyPath<Date> _approvalDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Основное распределение абитуриентов. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgDistributionDetail#getDistribution()
     */
        public EcgDistribution.Path<EcgDistribution> distribution()
        {
            if(_distribution == null )
                _distribution = new EcgDistribution.Path<EcgDistribution>(L_DISTRIBUTION, this);
            return _distribution;
        }

    /**
     * @return Состояние распределения. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgDistributionDetail#getState()
     */
        public EcgDistributionState.Path<EcgDistributionState> state()
        {
            if(_state == null )
                _state = new EcgDistributionState.Path<EcgDistributionState>(L_STATE, this);
            return _state;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgDistributionDetail#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(EcgDistributionDetailGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Дата утверждения.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgDistributionDetail#getApprovalDate()
     */
        public PropertyPath<Date> approvalDate()
        {
            if(_approvalDate == null )
                _approvalDate = new PropertyPath<Date>(EcgDistributionDetailGen.P_APPROVAL_DATE, this);
            return _approvalDate;
        }

        public Class getEntityClass()
        {
            return EcgDistributionDetail.class;
        }

        public String getEntityName()
        {
            return "ecgDistributionDetail";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
