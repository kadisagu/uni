/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.settings.EnrollmentOrderVisaList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.settings.EnrollmentOrderVisaItem;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.List;

/**
 * @author vip_delete
 * @since 28.07.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(EnrollmentOrderVisaItem.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", EnrollmentOrderVisaItem.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
        builder.addOrder("e", EnrollmentOrderVisaItem.P_TITLE);
        builder.addOrder("e", EnrollmentOrderVisaItem.groupsMemberVising().index().s());
        builder.addOrder("e", EnrollmentOrderVisaItem.P_PRIORITY);

        List<EnrollmentOrderVisaItem> itemList = builder.getResultList(getSession());

        model.getDataSource().setCountRow(itemList.size());
        UniBaseUtils.createPage(model.getDataSource(), itemList);
    }

    @Override
    @SuppressWarnings({"unchecked", "deprecation"})
    public void updatePriority(Long id, boolean up)
    {
        UniBaseUtils.changePriority(id, (List) getItemList(id), EnrollmentOrderVisaItem.P_PRIORITY, up, getSession());
    }

    @Override
    public void deleteRow(IBusinessComponent component)
    {
        for (EnrollmentOrderVisaItem item : getItemListToDel((Long) component.getListenerParameter()))
            getSession().delete(item);
    }

    private List<EnrollmentOrderVisaItem> getItemList(Long id)
    {
        EnrollmentOrderVisaItem item = getNotNull(EnrollmentOrderVisaItem.class, id);
        MQBuilder builder = new MQBuilder(EnrollmentOrderVisaItem.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", EnrollmentOrderVisaItem.L_ENROLLMENT_CAMPAIGN, item.getEnrollmentCampaign()));
        builder.add(MQExpression.eq("e", EnrollmentOrderVisaItem.P_TITLE, item.getTitle()));
        builder.add(MQExpression.eq("e", EnrollmentOrderVisaItem.L_GROUPS_MEMBER_VISING, item.getGroupsMemberVising()));
        builder.addOrder("e", EnrollmentOrderVisaItem.P_PRIORITY);
        return builder.getResultList(getSession());
    }

    private List<EnrollmentOrderVisaItem> getItemListToDel(Long id)
    {
        EnrollmentOrderVisaItem item = getNotNull(EnrollmentOrderVisaItem.class, id);
        MQBuilder builder = new MQBuilder(EnrollmentOrderVisaItem.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", EnrollmentOrderVisaItem.L_ENROLLMENT_CAMPAIGN, item.getEnrollmentCampaign()));
        builder.add(MQExpression.eq("e", EnrollmentOrderVisaItem.P_TITLE, item.getTitle()));
        builder.addOrder("e", EnrollmentOrderVisaItem.P_PRIORITY);
        return builder.getResultList(getSession());
    }
}
