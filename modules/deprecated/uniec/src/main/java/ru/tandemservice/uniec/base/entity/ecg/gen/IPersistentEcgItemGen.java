package ru.tandemservice.uniec.base.entity.ecg.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.base.entity.ecg.IPersistentEcgItem;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import ru.tandemservice.uniec.base.entity.ecg.IPersistentEcgItem;

/**
 * Целевой объект распределения абитуриентов
 *
 * Объект, на который создается основное распределение абитуриентов: направление приема или конкурсная группа
 */
@SuppressWarnings({"all", "unchecked", "unused"})
public abstract class IPersistentEcgItemGen extends InterfaceStubBase
 implements IPersistentEcgItem{
    public static final int VERSION_HASH = 564921214;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_TITLE = "title";

    private EnrollmentCampaign _enrollmentCampaign;
    private String _title;

    @NotNull

    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    @NotNull
    @Length(max=255)

    public String getTitle()
    {
        return _title;
    }

    public void setTitle(String title)
    {
        _title = title;
    }

    private static final Path<IPersistentEcgItem> _dslPath = new Path<IPersistentEcgItem>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ru.tandemservice.uniec.base.entity.ecg.IPersistentEcgItem");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.IPersistentEcgItem#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.IPersistentEcgItem#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends IPersistentEcgItem> extends EntityPath<E>
    {
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.IPersistentEcgItem#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.IPersistentEcgItem#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(IPersistentEcgItemGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return IPersistentEcgItem.class;
        }

        public String getEntityName()
        {
            return "ru.tandemservice.uniec.base.entity.ecg.IPersistentEcgItem";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
