/* $Id$ */
package ru.tandemservice.uniec.component.catalog.entrantCustomStateCI.EntrantCustomStateCIAddEdit;

import org.apache.cxf.common.util.StringUtils;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI;

/**
 * @author nvankov
 * @since 4/3/13
 */
public class Model extends DefaultCatalogAddEditModel<EntrantCustomStateCI>
{
    private String _htmlColor;

    public String getHtmlColor()
    {
        return _htmlColor;
    }

    public void setHtmlColor(String htmlColor)
    {
        _htmlColor = htmlColor;
    }

    public String getColorStyle()
    {
        String example;
        if (null == getCatalogItem().getTitle())
            example = "Пример";
        else
            example = getCatalogItem().getTitle();
        if(!StringUtils.isEmpty(_htmlColor))
        {
            if (_htmlColor.matches("^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$"))
                return "<span style=\"color:" + _htmlColor + ";\" >" + example + "</span>";
            else
                return example;
        }
        else
            return example;
    }
}
