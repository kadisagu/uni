/* $Id$ */
package ru.tandemservice.uniec.component.listextract.ent1.e2.ParagraphAddEdit;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.base.entity.ecgp.EcgpDistribution;
import ru.tandemservice.uniec.base.entity.ecgp.EcgpEntrantRecommended;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.SplitContractBachelorEntrantsStuListExtract;

import java.util.List;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 21.08.2012
 */
public class Model extends AbstractListParagraphAddEditModel<SplitContractBachelorEntrantsStuListExtract>
{
    public static final String P_ENTRANT_DIRECTION = "direction";
    public static final String P_ENROLLMENT_ORDER_NUMBER = "enrollmentOrderNumber";
    public static final String P_ENROLLMENT_ORDER_DATE = "enrollmentOrderDate";
    public static final String P_ENROLLMENT_EXTRACT = "enrollmentExtract";

    private EnrollmentCampaign _enrollmentCampaign;
    private EcgpDistribution _ecgpDistribution;
    private CompensationType _compensationType;
    private OrgUnit _formativeOrgUnit;
    private OrgUnit _territorialOrgUnit;

    private List<EnrollmentCampaign> _enrollmentCampaignList;
    private ISelectModel _ecgpDistributionsListModel;
    private ISelectModel _formativeOrgUnitsListModel;
    private ISelectModel _territorialOrgUnitsListModel;

    Map<Long, EnrollmentExtract> _enrExtractsMap;
    Map<Long, EcgpEntrantRecommended> _entrantRecommendedMap;

    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public EcgpDistribution getEcgpDistribution()
    {
        return _ecgpDistribution;
    }

    public void setEcgpDistribution(EcgpDistribution ecgpDistribution)
    {
        _ecgpDistribution = ecgpDistribution;
    }

    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public ISelectModel getEcgpDistributionsListModel()
    {
        return _ecgpDistributionsListModel;
    }

    public void setEcgpDistributionsListModel(ISelectModel ecgpDistributionsListModel)
    {
        _ecgpDistributionsListModel = ecgpDistributionsListModel;
    }

    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        _formativeOrgUnit = formativeOrgUnit;
    }

    public OrgUnit getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    public void setTerritorialOrgUnit(OrgUnit territorialOrgUnit)
    {
        _territorialOrgUnit = territorialOrgUnit;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

    public ISelectModel getFormativeOrgUnitsListModel()
    {
        return _formativeOrgUnitsListModel;
    }

    public void setFormativeOrgUnitsListModel(ISelectModel formativeOrgUnitsListModel)
    {
        _formativeOrgUnitsListModel = formativeOrgUnitsListModel;
    }

    public ISelectModel getTerritorialOrgUnitsListModel()
    {
        return _territorialOrgUnitsListModel;
    }

    public void setTerritorialOrgUnitsListModel(ISelectModel territorialOrgUnitsListModel)
    {
        _territorialOrgUnitsListModel = territorialOrgUnitsListModel;
    }

    public Map<Long, EnrollmentExtract> getEnrExtractsMap()
    {
        return _enrExtractsMap;
    }

    public void setEnrExtractsMap(Map<Long, EnrollmentExtract> enrExtractsMap)
    {
        _enrExtractsMap = enrExtractsMap;
    }

    public Map<Long, EcgpEntrantRecommended> getEntrantRecommendedMap()
    {
        return _entrantRecommendedMap;
    }

    public void setEntrantRecommendedMap(Map<Long, EcgpEntrantRecommended> entrantRecommendedMap)
    {
        _entrantRecommendedMap = entrantRecommendedMap;
    }
}