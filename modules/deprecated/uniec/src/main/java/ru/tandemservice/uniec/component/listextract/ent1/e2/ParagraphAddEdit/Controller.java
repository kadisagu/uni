/* $Id$ */
package ru.tandemservice.uniec.component.listextract.ent1.e2.ParagraphAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.core.view.formatter.SimpleFormatter;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditController;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniec.entity.orders.SplitContractBachelorEntrantsStuListExtract;

/**
 * @author Dmitry Seleznev
 * @since 21.08.2012
 */
public class Controller extends AbstractListParagraphAddEditController<SplitContractBachelorEntrantsStuListExtract, IDAO, Model>
{
    public void onChangeEnrollmentCampaign(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getParagraphId() == null)
            model.setTerritorialOrgUnit(TopOrgUnit.getInstance());
    }

    @Override
    protected void prepareListDataSource(IBusinessComponent component, DynamicListDataSource<Student> dataSource)
    {
        dataSource.addColumn(new SimpleColumn("ФИО студента", Student.FIO_KEY, NoWrapFormatter.INSTANCE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Пол", Student.person().identityCard().sex().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("status", "Состояние", Model.STATUS_KEY, SimpleFormatter.INSTANCE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Курс", Student.course().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Контракт", Student.compensationType().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Профиль (специцализация)", Model.P_ENTRANT_DIRECTION).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Номер приказа о зачислении", Model.P_ENROLLMENT_ORDER_NUMBER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дата приказа о зачислении", Model.P_ENROLLMENT_ORDER_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
        dataSource.setOrder("status", OrderDirection.asc);
    }
}