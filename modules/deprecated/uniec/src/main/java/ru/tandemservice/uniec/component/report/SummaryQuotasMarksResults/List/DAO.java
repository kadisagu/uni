/* $Id$ */
package ru.tandemservice.uniec.component.report.SummaryQuotasMarksResults.List;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.report.SummaryQuotasMarksResultsReport;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

/**
 * @author Vasily Zhukov
 * @since 18.08.2011
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareListDataSource(Model model)
    {
        EnrollmentCampaign enrollmentCampaign = (EnrollmentCampaign) model.getSettings().get(IEnrollmentCampaignSelectModel.ENROLLMENT_CAMPAIGN_FILTER_NAME);

        MQBuilder builder = new MQBuilder(SummaryQuotasMarksResultsReport.ENTITY_CLASS, "r");
        builder.add(MQExpression.eq("r", SummaryQuotasMarksResultsReport.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign));
        new OrderDescriptionRegistry("r").applyOrder(builder, model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }

    @Override
    public void deleteRow(IBusinessComponent component)
    {
        Long id = (Long) component.getListenerParameter();
        SummaryQuotasMarksResultsReport report = getNotNull(SummaryQuotasMarksResultsReport.class, id);
        delete(report);
        delete(report.getContent());
    }
}
