/**
 *$Id:$
 */
package ru.tandemservice.uniec.base.bo.EcReport.ui.SummaryStateExamMarkAdd;

import jxl.write.WriteException;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uniec.base.bo.EcReport.EcReportManager;
import ru.tandemservice.uniec.base.bo.EcReport.logic.SummaryStateExamMarkReport.SummaryStateExamMarkReportModel;
import ru.tandemservice.uniec.base.bo.EcReport.ui.SummaryStateExamMarkPub.EcReportSummaryStateExamMarkPub;
import ru.tandemservice.uniec.entity.report.SummaryStateExamMarkReport;

import java.io.IOException;

/**
 * @author Alexander Shaburov
 * @since 30.07.12
 */
public class EcReportSummaryStateExamMarkAddUI extends UIPresenter
{
    // fields

    private Object _model;

    // Presenter listeners

    @Override
    public void onComponentRefresh()
    {
        _model = new SummaryStateExamMarkReportModel();

        EcReportManager.instance().summaryStateExamMarkReportDAO().prepareModel((SummaryStateExamMarkReportModel) _model);
    }

    public void onChangeEnrollmentCampaign()
    {
        EcReportManager.instance().summaryStateExamMarkReportDAO().onChangeEnrollmentCampaign((SummaryStateExamMarkReportModel) _model);
    }

    // Listeners

    public void onClickApply()
    {
        DatabaseFile reportFile;
        try
        {
            reportFile = EcReportManager.instance().summaryStateExamMarkReportDAO().createPrintReportFile((SummaryStateExamMarkReportModel) _model);
        }
        catch (IOException | WriteException e)
        {
            throw CoreExceptionUtils.getRuntimeException(e);
        }

        SummaryStateExamMarkReport report = EcReportManager.instance().summaryStateExamMarkReportDAO().createReport((SummaryStateExamMarkReportModel) _model, reportFile);

        DataAccessServices.dao().save(reportFile);
        DataAccessServices.dao().save(report);

        deactivate();

        getActivationBuilder().asDesktopRoot(EcReportSummaryStateExamMarkPub.class)
                .parameter(UIPresenter.PUBLISHER_ID, report.getId())
                .activate();
    }

    // Getters & Setters

    public Object getModel()
    {
        return _model;
    }

    public void setModel(Object model)
    {
        _model = model;
    }
}
