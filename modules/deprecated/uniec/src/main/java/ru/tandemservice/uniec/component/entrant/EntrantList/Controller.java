/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantList;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonRole;
import org.tandemframework.shared.commonbase.utils.PublisherColumnBuilder;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.IUniecComponents;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.component.wizard.IEntrantWizardComponents;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.ui.EntrantCustomStateCollectionFormatter;

import java.util.List;

/**
 * @author agolubenko
 * @since 15.05.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.setSettings(component.getSettings());

        getDao().prepare(model);

        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<Entrant> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(IndicatorColumn.createIconColumn("entrant", "Абитуриент"));
        dataSource.addColumn(new SimpleColumn("Личный №", Entrant.P_PERSONAL_NUMBER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("№ заявления", Model.REQUEST_NUMBER_COLUMN).setClickable(false).setOrderable(false));

        PublisherLinkColumn publisherLinkColumn = new PublisherColumnBuilder("Абитуриент", Entrant.P_FULLFIO, "entrantTab").subTab("entrantRequestTab").build();
        dataSource.addColumn(publisherLinkColumn);
        dataSource.addColumn(new SimpleColumn("Дополнительный статус", Model.P_ENTRANT_ACTIVE_CUSTOME_STATES, new EntrantCustomStateCollectionFormatter()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Пол", PersonRole.person().identityCard().sex().shortTitle().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Паспорт", new String[]{PersonRole.L_PERSON, Person.P_FULL_IDCARD_NUMBER}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Гражданство", PersonRole.person().identityCard().citizenship().title().s()).setClickable(false).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Дата рождения", PersonRole.person().identityCard().birthDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Состояние", Entrant.state().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дата добавления", Entrant.P_REGISTRATION_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Место работы", Entrant.person().workPlace()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new BooleanColumn("Оригиналы", Model.ORIGINAL_DOCUMENT_HANDLE_IN_COLUMN));

        ToggleColumn statusColumn = new ToggleColumn("Статус", Entrant.P_ARCHIVAL);
        statusColumn.indicator(Boolean.FALSE, new IndicatorColumn.Item("toggled_on", "Абитуриент не в архиве", "onClickSwitchEntrantArchival", "Списать абитуриента «{0}» в архив?", new Object[]{Entrant.P_FULLFIO}).setPermissionKey("archiveEntrant"));
        statusColumn.indicator(Boolean.TRUE, new IndicatorColumn.Item("toggled_off", "Абитуриент в архиве", "onClickSwitchEntrantArchival", "Восстановить абитуриента «{0}» из архива?", new Object[]{Entrant.P_FULLFIO}).setPermissionKey("archiveEntrant"));
        dataSource.addColumn(statusColumn);
        dataSource.addColumn(new ActionColumn("Удаление", ActionColumn.DELETE, "onClickDeleteEntrant", "Удалить абитуриента «{0}»?", Entrant.P_FULLFIO).setPermissionKey("deleteEntrant"));
        dataSource.setOrder(publisherLinkColumn, OrderDirection.asc);

        model.setDataSource(dataSource);
    }

    public void onClickPrint(IBusinessComponent component)
    {
        component.saveSettings();
        Model model = getModel(component);
        CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(DataAccessServices.dao().getByCode(UniecScriptItem.class, UniecDefines.SCRIPT_ENTRANT_LIST),
                IScriptExecutor.OBJECT_VARIABLE, getDao().getEntrantIds(model),
                "headerTable", getDao().getHeaderTable(model)
        );
    }

    public void onClickSearch(IBusinessComponent component)
    {
        if(null != getModel(component).getCitizenship())
        {
            component.getSettings().set("citizenship", getModel(component).getCitizenship().getId());
        }
        else
        {
            component.getSettings().set("citizenship", null);
        }
        component.saveSettings();
        getModel(component).getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.setCitizenship(null);
        EnrollmentCampaign savedEnrollmentCampaign = model.getEnrollmentCampaign();
        model.getSettings().clear();
        if (savedEnrollmentCampaign != null) // не сбрасывать фильтр кампании
            model.setEnrollmentCampaign(savedEnrollmentCampaign);
        getDao().prepare(model);
        onClickSearch(component);
    }

    public void onClickAddEntrant(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniecComponents.ENTRANT_ADD));
    }

    public void onClickAddEntrantWizard(IBusinessComponent component)
    {

        List<EnrollmentCampaign> openedCampaigns = getDao().getList(EnrollmentCampaign.class, EnrollmentCampaign.closed().s(), false);
        if (CollectionUtils.isEmpty(openedCampaigns))
            return;

        boolean searchBeforeEntrantRegistration = openedCampaigns.stream().anyMatch(EnrollmentCampaign::isSearchBeforeEntrantRegistration);
        component.createDefaultChildRegion(new ComponentActivator(
                ((searchBeforeEntrantRegistration) ? IEntrantWizardComponents.SEARCH_STEP : IEntrantWizardComponents.IDENTITY_CARD_STEP),
                new ParametersMap().add("entrantMasterPermKey", Model.DEF_ENTRANT_MASTER_PERM_KEY)));
    }

    public void onClickDeleteEntrant(IBusinessComponent component)
    {
        getDao().deleteRow(component);
    }

    public void onClickSwitchEntrantArchival(IBusinessComponent component)
    {
        getDao().changeArchival((Long) component.getListenerParameter());
    }
}
