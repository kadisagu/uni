package ru.tandemservice.uniec.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniec.entity.catalog.ExamGroupLogic;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Алгоритм работы экзаменационных групп
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ExamGroupLogicGen extends EntityBase
 implements INaturalIdentifiable<ExamGroupLogicGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.catalog.ExamGroupLogic";
    public static final String ENTITY_NAME = "examGroupLogic";
    public static final int VERSION_HASH = 1483915814;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_DAO_NAME = "daoName";
    public static final String P_DESCRIPTION = "description";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _daoName;     // Имя
    private String _description;     // Описание
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Имя. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getDaoName()
    {
        return _daoName;
    }

    /**
     * @param daoName Имя. Свойство не может быть null и должно быть уникальным.
     */
    public void setDaoName(String daoName)
    {
        dirty(_daoName, daoName);
        _daoName = daoName;
    }

    /**
     * @return Описание. Свойство не может быть null.
     */
    @NotNull
    @Length(max=1200)
    public String getDescription()
    {
        return _description;
    }

    /**
     * @param description Описание. Свойство не может быть null.
     */
    public void setDescription(String description)
    {
        dirty(_description, description);
        _description = description;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ExamGroupLogicGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((ExamGroupLogic)another).getCode());
            }
            setDaoName(((ExamGroupLogic)another).getDaoName());
            setDescription(((ExamGroupLogic)another).getDescription());
            setTitle(((ExamGroupLogic)another).getTitle());
        }
    }

    public INaturalId<ExamGroupLogicGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<ExamGroupLogicGen>
    {
        private static final String PROXY_NAME = "ExamGroupLogicNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof ExamGroupLogicGen.NaturalId) ) return false;

            ExamGroupLogicGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ExamGroupLogicGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ExamGroupLogic.class;
        }

        public T newInstance()
        {
            return (T) new ExamGroupLogic();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "daoName":
                    return obj.getDaoName();
                case "description":
                    return obj.getDescription();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "daoName":
                    obj.setDaoName((String) value);
                    return;
                case "description":
                    obj.setDescription((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "daoName":
                        return true;
                case "description":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "daoName":
                    return true;
                case "description":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "daoName":
                    return String.class;
                case "description":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ExamGroupLogic> _dslPath = new Path<ExamGroupLogic>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ExamGroupLogic");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.catalog.ExamGroupLogic#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Имя. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.catalog.ExamGroupLogic#getDaoName()
     */
    public static PropertyPath<String> daoName()
    {
        return _dslPath.daoName();
    }

    /**
     * @return Описание. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.catalog.ExamGroupLogic#getDescription()
     */
    public static PropertyPath<String> description()
    {
        return _dslPath.description();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uniec.entity.catalog.ExamGroupLogic#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends ExamGroupLogic> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _daoName;
        private PropertyPath<String> _description;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.catalog.ExamGroupLogic#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(ExamGroupLogicGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Имя. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.catalog.ExamGroupLogic#getDaoName()
     */
        public PropertyPath<String> daoName()
        {
            if(_daoName == null )
                _daoName = new PropertyPath<String>(ExamGroupLogicGen.P_DAO_NAME, this);
            return _daoName;
        }

    /**
     * @return Описание. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.catalog.ExamGroupLogic#getDescription()
     */
        public PropertyPath<String> description()
        {
            if(_description == null )
                _description = new PropertyPath<String>(ExamGroupLogicGen.P_DESCRIPTION, this);
            return _description;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uniec.entity.catalog.ExamGroupLogic#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(ExamGroupLogicGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return ExamGroupLogic.class;
        }

        public String getEntityName()
        {
            return "examGroupLogic";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
