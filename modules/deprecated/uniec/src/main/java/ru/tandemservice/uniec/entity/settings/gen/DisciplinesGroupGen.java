package ru.tandemservice.uniec.entity.settings.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.settings.DisciplinesGroup;
import ru.tandemservice.uniec.entity.settings.SetDiscipline;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Группа дисциплин набора вступительных испытаний
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DisciplinesGroupGen extends SetDiscipline
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.settings.DisciplinesGroup";
    public static final String ENTITY_NAME = "disciplinesGroup";
    public static final int VERSION_HASH = 1512013682;
    private static IEntityMeta ENTITY_META;

    public static final String P_TITLE = "title";
    public static final String P_SHORT_TITLE = "shortTitle";

    private String _title;     // Название
    private String _shortTitle;     // Сокращенное название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название. Свойство не может быть null.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof DisciplinesGroupGen)
        {
            setTitle(((DisciplinesGroup)another).getTitle());
            setShortTitle(((DisciplinesGroup)another).getShortTitle());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DisciplinesGroupGen> extends SetDiscipline.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DisciplinesGroup.class;
        }

        public T newInstance()
        {
            return (T) new DisciplinesGroup();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "title":
                    return obj.getTitle();
                case "shortTitle":
                    return obj.getShortTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "title":
                        return true;
                case "shortTitle":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "title":
                    return true;
                case "shortTitle":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "title":
                    return String.class;
                case "shortTitle":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DisciplinesGroup> _dslPath = new Path<DisciplinesGroup>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DisciplinesGroup");
    }
            

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.DisciplinesGroup#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.DisciplinesGroup#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    public static class Path<E extends DisciplinesGroup> extends SetDiscipline.Path<E>
    {
        private PropertyPath<String> _title;
        private PropertyPath<String> _shortTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.DisciplinesGroup#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(DisciplinesGroupGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.DisciplinesGroup#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(DisciplinesGroupGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

        public Class getEntityClass()
        {
            return DisciplinesGroup.class;
        }

        public String getEntityName()
        {
            return "disciplinesGroup";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
