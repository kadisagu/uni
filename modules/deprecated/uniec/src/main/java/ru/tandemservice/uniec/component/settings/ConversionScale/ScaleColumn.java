/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.settings.ConversionScale;

import org.tandemframework.core.common.IIdentifiable;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.view.formatter.IFormatter;

/**
 * @author agolubenko
 * @since 25.06.2008
 */
class ScaleColumn implements IIdentifiable, ITitled
{
    private static final IFormatter<Double> MARK_FORMATTER = source -> (source != null) ? String.format((source % 1 == 0) ? "%.0f" : "%.1f", source) : null;

    private double _mark;

    public ScaleColumn(double mark)
    {
        _mark = mark;
    }

    @Override
    public Long getId()
    {
        return (long) (_mark * 10);
    }

    public static double getMark(Long id)
    {
        return id / 10.0;
    }

    public static long getId(double mark)
    {
        return (long) (mark * 10);
    }

    @Override
    public String getTitle()
    {
        return MARK_FORMATTER.format(_mark);
    }

    public double getMark()
    {
        return _mark;
    }
}
