package ru.tandemservice.uniec.entity.entrant;

import org.tandemframework.core.common.ITitled;

import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniec.entity.entrant.gen.ExamPassDisciplineGen;

/**
 * Дисциплина для сдачи
 */
public class ExamPassDiscipline extends ExamPassDisciplineGen implements ITitled
{
    public static final String P_FINAL_MARK = "finalMark";

    public Double getFinalMark()
    {
        ExamPassMark passMark = UniDaoFacade.getCoreDao().get(ExamPassMark.class, ExamPassMark.L_EXAM_PASS_DISCIPLINE, this);
        if (passMark == null) return null;
        ExamPassMarkAppeal appealMark = UniDaoFacade.getCoreDao().get(ExamPassMarkAppeal.class, ExamPassMarkAppeal.L_EXAM_PASS_MARK, passMark);
        if (appealMark == null) return passMark.getMark();
        return appealMark.getMark();
    }

    @Override
    public String getTitle()
    {
        return getEnrollmentCampaignDiscipline().getTitle() + " (" + getSubjectPassForm().getTitle() + ")";
    }
}