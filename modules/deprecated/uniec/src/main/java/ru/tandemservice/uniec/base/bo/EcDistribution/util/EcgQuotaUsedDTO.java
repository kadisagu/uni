/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.util;

import java.util.Map;

/**
 * Занятые места по направлениям и видам целевого приема
 *
 * @author Vasily Zhukov
 * @since 11.07.2011
 */
public class EcgQuotaUsedDTO implements IEcgQuotaUsedDTO
{
    /**
     * направление приема -> занято мест
     * занято мест всегда не null
     */
    private Map<Long, Integer> _usedMap;

    /**
     * направление приема -> вид целевого приема -> занято мест
     * занято мест всегда не null
     */
    private Map<Long, Map<Long, Integer>> _taUsedMap;

    public EcgQuotaUsedDTO(Map<Long, Integer> usedMap, Map<Long, Map<Long, Integer>> taUsedMap)
    {
        _usedMap = usedMap;
        _taUsedMap = taUsedMap;
    }

    // Getters

    @Override
    public Map<Long, Integer> getUsedMap()
    {
        return _usedMap;
    }

    @Override
    public Map<Long, Map<Long, Integer>> getTaUsedMap()
    {
        return _taUsedMap;
    }
}
