/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.SummaryEnrollmentResult.SummaryEnrollmentResultAdd;

import java.util.Set;

import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

/**
 * Простой java-bean
 *
 * @author vip_delete
 * @since 23.04.2009
 */
class SummaryRowItem implements ISummaryRowItem
{
    private EnrollmentDirection _enrollmentDirection;
    private RequestedEnrollmentDirection _direction;
    private EntrantRequest _entrantRequest;
    private Entrant _entrant;
    private PersonEduInstitution _lastPersonEduInstitution;
    private Set<ChosenEntranceDiscipline> _chosenEntranceDisciplineSet;
    private Set<String> _benefitSet;
    private boolean _hasOlympiadDiplomas;
    private boolean _hasStudents;
    private boolean _hasRecommendations;
    private boolean _hasProfileKnowledges;
    private boolean _targetAdmission;
    private String _targetAdmissionKind;
    private String _competitionKind;
    private String _studentCategory;
    private boolean _hasWorkExperience;

    // Getters & Setters

    @Override
    public EnrollmentDirection getEnrollmentDirection()
    {
        return _enrollmentDirection;
    }

    public void setEnrollmentDirection(EnrollmentDirection enrollmentDirection)
    {
        _enrollmentDirection = enrollmentDirection;
    }

    @Override
    public RequestedEnrollmentDirection getDirection()
    {
        return _direction;
    }

    public void setDirection(RequestedEnrollmentDirection direction)
    {
        _direction = direction;
    }

    @Override
    public EntrantRequest getEntrantRequest()
    {
        return _entrantRequest;
    }

    public void setEntrantRequest(EntrantRequest entrantRequest)
    {
        _entrantRequest = entrantRequest;
    }

    @Override
    public Entrant getEntrant()
    {
        return _entrant;
    }

    public void setEntrant(Entrant entrant)
    {
        _entrant = entrant;
    }

    @Override
    public PersonEduInstitution getLastPersonEduInstitution()
    {
        return _lastPersonEduInstitution;
    }

    public void setLastPersonEduInstitution(PersonEduInstitution lastPersonEduInstitution)
    {
        _lastPersonEduInstitution = lastPersonEduInstitution;
    }

    @Override
    public Set<ChosenEntranceDiscipline> getChosenEntranceDisciplineSet()
    {
        return _chosenEntranceDisciplineSet;
    }

    public void setChosenEntranceDisciplineSet(Set<ChosenEntranceDiscipline> chosenEntranceDisciplineSet)
    {
        _chosenEntranceDisciplineSet = chosenEntranceDisciplineSet;
    }

    @Override
    public Set<String> getBenefitSet()
    {
        return _benefitSet;
    }

    public void setBenefitSet(Set<String> benefitSet)
    {
        _benefitSet = benefitSet;
    }

    @Override
    public boolean isHasOlympiadDiplomas()
    {
        return _hasOlympiadDiplomas;
    }

    public void setHasOlympiadDiplomas(boolean hasOlympiadDiplomas)
    {
        _hasOlympiadDiplomas = hasOlympiadDiplomas;
    }

    @Override
    public boolean isHasStudents()
    {
        return _hasStudents;
    }

    public void setHasStudents(boolean hasStudents)
    {
        _hasStudents = hasStudents;
    }

    @Override
    public boolean isHasRecommendations()
    {
        return _hasRecommendations;
    }

    public void setHasRecommendations(boolean hasRecommendations)
    {
        _hasRecommendations = hasRecommendations;
    }

    @Override
    public boolean isHasProfileKnowledges()
    {
        return _hasProfileKnowledges;
    }

    public void setHasProfileKnowledges(boolean hasProfileKnowledges)
    {
        _hasProfileKnowledges = hasProfileKnowledges;
    }

    @Override
    public boolean isTargetAdmission()
    {
        return _targetAdmission;
    }

    public void setTargetAdmission(boolean targetAdmission)
    {
        _targetAdmission = targetAdmission;
    }

    @Override
    public String getTargetAdmissionKind()
    {
        return _targetAdmissionKind;
    }

    public void setTargetAdmissionKind(String targetAdmissionKind)
    {
        _targetAdmissionKind = targetAdmissionKind;
    }

    @Override
    public String getCompetitionKind()
    {
        return _competitionKind;
    }

    public void setCompetitionKind(String competitionKind)
    {
        _competitionKind = competitionKind;
    }

    @Override
    public String getStudentCategory()
    {
        return _studentCategory;
    }

    public void setStudentCategory(String studentCategory)
    {
        _studentCategory = studentCategory;
    }

    @Override
    public boolean isHasWorkExperience()
    {
        return _hasWorkExperience;
    }

    public void setHasWorkExperience(boolean hasWorkExperience)
    {
        _hasWorkExperience = hasWorkExperience;
    }
}
