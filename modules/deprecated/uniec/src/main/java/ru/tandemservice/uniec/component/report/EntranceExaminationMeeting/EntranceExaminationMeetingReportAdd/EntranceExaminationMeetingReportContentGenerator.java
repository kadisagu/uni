/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.report.EntranceExaminationMeeting.EntranceExaminationMeetingReportAdd;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.*;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.fias.base.entity.ICitizenship;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.entity.settings.EnrollmentCompetitionKind;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.EntrantDataUtil.DetailLevel;
import ru.tandemservice.uniec.util.ExamSetUtil;
import ru.tandemservice.uniec.util.MarkStatistic;

import java.io.ByteArrayOutputStream;
import java.util.*;
import java.util.Map.Entry;

/**
 * Класс для генерации протокола заседания приемной комиссии
 * Постановка: Отчеты модуля «Абитуриент» ГОУ ВПО «УрГПУ»
 * Пункт: 1.1
 *
 * @author agolubenko
 * @since 11.11.2008
 */
public class EntranceExaminationMeetingReportContentGenerator
{
    Session _session;

    protected EntranceExaminationMeetingReport _report;
    protected List<EnrollmentDirection> _enrollmentDirections;
    protected List<StudentCategory> _studentCategories;
    protected Map<String, WritableCellFormat> _formatMap;

    public EntranceExaminationMeetingReportContentGenerator(EntranceExaminationMeetingReport report, List<EnrollmentDirection> enrollmentDirections, List<StudentCategory> studentCategories, Session session)
    {
        _session = session;
        _report = report;
        _enrollmentDirections = enrollmentDirections;
        _studentCategories = studentCategories;
    }

    /**
     * Генерирует содержимое отчета по заданным параметрам
     *
     * @return содержимое отчета
     * @throws Exception ошибка
     */
    public byte[] generateReportContent() throws Exception
    {
        MQBuilder requestedEnrollmentDirectionsBuilder = getRequestedEnrollmentDirectionsBuilder();
        List<Discipline2RealizationWayRelation> examDisciplines = getExamDisciplines(_enrollmentDirections);
        List<String> examDisciplinesShortTitles = CommonBaseEntityUtil.getPropertiesList(examDisciplines, Discipline2RealizationWayRelation.P_SHORT_TITLE);
        int tableRowWidth = new FieldValues(examDisciplines.size()).getRowWidth();

        // создаем печатную форму
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        WritableWorkbook workbook = Workbook.createWorkbook(out, ws);

        // создаем лист
        WritableSheet sheet = workbook.createSheet("Отчет", 0);
        sheet.getSettings().setCopies(1);
        sheet.getSettings().setOrientation(PageOrientation.LANDSCAPE);

        // для удобства, если захотят поставить масштаб, то по умолчанию будет 100, а не как сейчас почему-то 225
        sheet.getSettings().setScaleFactor(81);
        sheet.getSettings().setOrientation(PageOrientation.LANDSCAPE);

        // вписать в 1 страницу по ширине и 1000 по высоте — перекрывает scaleFactor
        sheet.getSettings().setFitWidth(1);
        sheet.getSettings().setFitHeight(1000);

        double margin = 1 / 2.54; // поле в дюймах
        sheet.getSettings().setTopMargin(margin);
        sheet.getSettings().setBottomMargin(margin);
        sheet.getSettings().setLeftMargin(margin);
        sheet.getSettings().setRightMargin(margin);



        // создаем форматы данных
        createFormatMap();

        // рисуем шапку отчета
        final int TABLE_DATA_COLUMN = 0;
        final int TABLE_DATA_ROW = 6;
        writeReportHeader(sheet, TABLE_DATA_COLUMN, TABLE_DATA_ROW, examDisciplinesShortTitles, tableRowWidth);

        int row = TABLE_DATA_ROW + 2; // шапка занимает две строки

        Integer[] rowHandler = new Integer[]{row};

        // рисуем данные отчета
        writeReportData(sheet, TABLE_DATA_COLUMN, rowHandler, requestedEnrollmentDirectionsBuilder, examDisciplines, tableRowWidth);

        // пропускаем пустую строку
        rowHandler[0]++;

        // кастомизируем отчет до вывода детализации направлений
        customizeSheetBeforeDetailTable(sheet, rowHandler);

        // пропускаем пустую строку
        rowHandler[0]++;

        // выводим детализацию по направлениям
        writeEnrollmentDirectionDetailsTable(sheet, TABLE_DATA_COLUMN + 1, rowHandler);

        sheet.getSettings().setPrintArea(0, 0, tableRowWidth, rowHandler[0]);

        workbook.write();
        workbook.close();

        return out.toByteArray();
    }

    protected void customizeSheetBeforeDetailTable(WritableSheet sheet, Integer[] rowHandler) throws Exception
    {
    }

    private void createFormatMap() throws Exception
    {
        // создаем шрифты
        WritableFont arial8 = new WritableFont(WritableFont.ARIAL, 8);
        WritableFont arial8bold = new WritableFont(WritableFont.ARIAL, 8, WritableFont.BOLD);
        WritableFont arial8boldItalic = new WritableFont(WritableFont.ARIAL, 8, WritableFont.BOLD, true);

        _formatMap = new HashMap<>();

        // create cell formats
        WritableCellFormat reportTitleFormat = new WritableCellFormat(arial8bold);
        reportTitleFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        reportTitleFormat.setAlignment(Alignment.CENTRE);
        _formatMap.put("reportTitle", reportTitleFormat);

        WritableCellFormat headerFormat = new WritableCellFormat(arial8);
        headerFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        headerFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        headerFormat.setAlignment(Alignment.CENTRE);
        headerFormat.setWrap(true);
        _formatMap.put("headerFormat", headerFormat);

        WritableCellFormat paramFormat = new WritableCellFormat(arial8);
        _formatMap.put("paramFormat", paramFormat);

        WritableCellFormat rowFormat = new WritableCellFormat(arial8);
        rowFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        rowFormat.setVerticalAlignment(VerticalAlignment.TOP);
        rowFormat.setWrap(true);
        _formatMap.put("rowFormat", rowFormat);

        WritableCellFormat ga = new WritableCellFormat(arial8);
        ga.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        ga.setVerticalAlignment(VerticalAlignment.TOP);
        _formatMap.put("ga", ga);
        _formatMap.put("ta", ga);

        WritableCellFormat gaCompetitionKind = new WritableCellFormat(arial8boldItalic);
        gaCompetitionKind.setIndentation(1);
        gaCompetitionKind.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        gaCompetitionKind.setVerticalAlignment(VerticalAlignment.TOP);
        gaCompetitionKind.setAlignment(Alignment.LEFT);
        _formatMap.put("gaCompetitionKind", gaCompetitionKind);

        WritableCellFormat gaFIO = new WritableCellFormat(arial8);
        gaFIO.setIndentation(2);
        gaFIO.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        gaFIO.setVerticalAlignment(VerticalAlignment.TOP);
        gaFIO.setAlignment(Alignment.LEFT);
        gaFIO.setWrap(true);
        _formatMap.put("gaFIO", gaFIO);

        WritableCellFormat taKind = new WritableCellFormat(arial8);
        taKind.setIndentation(1);
        taKind.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        taKind.setVerticalAlignment(VerticalAlignment.TOP);
        taKind.setAlignment(Alignment.LEFT);
        _formatMap.put("taKind", taKind);

        WritableCellFormat taPlace = new WritableCellFormat(arial8bold);
        taPlace.setIndentation(2);
        taPlace.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        taPlace.setVerticalAlignment(VerticalAlignment.TOP);
        taPlace.setAlignment(Alignment.LEFT);
        _formatMap.put("taPlace", taPlace);

        WritableCellFormat taCompetitionKind = new WritableCellFormat(arial8boldItalic);
        taCompetitionKind.setIndentation(3);
        taCompetitionKind.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        taCompetitionKind.setVerticalAlignment(VerticalAlignment.TOP);
        taCompetitionKind.setAlignment(Alignment.LEFT);
        _formatMap.put("taCompetitionKind", taCompetitionKind);

        WritableCellFormat taFIO = new WritableCellFormat(arial8);
        taFIO.setIndentation(4);
        taFIO.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        taFIO.setVerticalAlignment(VerticalAlignment.TOP);
        taFIO.setAlignment(Alignment.LEFT);
        taFIO.setWrap(true);
        _formatMap.put("taFIO", taFIO);

        WritableCellFormat right = new WritableCellFormat(rowFormat);
        right.setAlignment(Alignment.RIGHT);
        _formatMap.put("right", right);

        WritableCellFormat center = new WritableCellFormat(rowFormat);
        center.setAlignment(Alignment.CENTRE);
        _formatMap.put("center", center);
    }

    private void writeReportHeader(WritableSheet sheet, int column, int row, List<String> examDisciplinesShortTitles, int tableRowWidth) throws Exception
    {
        WritableCellFormat reportTitleFormat = _formatMap.get("reportTitle");
        WritableCellFormat paramFormat = _formatMap.get("paramFormat");
        WritableCellFormat headerFormat = _formatMap.get("headerFormat");

        // рисуем шапку отчета

        sheet.addCell(new Label(0, 1, "Протокол заседания приемной комиссии", reportTitleFormat));
        sheet.addCell(new Label(0, 2, TopOrgUnit.getInstance().getTitle(), reportTitleFormat));
        sheet.addCell(new Label(0, 3, "__от  __.__.______ г", reportTitleFormat));
        sheet.mergeCells(0, 1, tableRowWidth, 1);
        sheet.mergeCells(0, 2, tableRowWidth, 2);
        sheet.mergeCells(0, 3, tableRowWidth, 3);

        // рисуем параметры отчета
        sheet.addCell(new Label(0, 5, getDetails(), paramFormat));
        sheet.mergeCells(0, 5, tableRowWidth, 5);

        sheet.addCell(new Label(column, row, "№ п/п", headerFormat));
        sheet.mergeCells(column, row, column, row + 1);
        sheet.setColumnView(column, 4);
        column++;

        sheet.addCell(new Label(column, row, "Фамилия имя отчество", headerFormat));
        sheet.mergeCells(column, row, column, row + 1);
        sheet.setColumnView(column, 30);
        column++;

        sheet.addCell(new Label(column, row, "№ экзам листа", headerFormat));
        sheet.mergeCells(column, row, column, row + 1);
        sheet.setColumnView(column, 8);
        column++;

        sheet.addCell(new Label(column, row, "Базовое образование", headerFormat));
        sheet.mergeCells(column, row, column, row + 1);
        sheet.setColumnView(column, 11);
        column++;

        sheet.addCell(new Label(column, row, "Место жительства", headerFormat));
        sheet.mergeCells(column, row, column, row + 1);
        sheet.setColumnView(column, 16);
        column++;

        sheet.addCell(new Label(column, row, "Иностр. гражданство", headerFormat));
        sheet.mergeCells(column, row, column, row + 1);
        sheet.setColumnView(column, 11);
        column++;

        sheet.addCell(new Label(column, row, "Вид внеконкурс. приема", headerFormat));
        sheet.mergeCells(column, row, column, row + 1);
        sheet.setColumnView(column, 13);
        column++;

        if (examDisciplinesShortTitles.isEmpty())
        {
            sheet.addCell(new Label(column, row, "Результаты ЕГЭ", headerFormat));
            sheet.mergeCells(column, row, column, row + 1);
            sheet.setColumnView(column, 18);
            column++;

            sheet.addCell(new Label(column, row, "Результаты экзаменов", headerFormat));
            sheet.mergeCells(column, row, column, row + 1);
            sheet.setColumnView(column, 18);
            column++;
        } else
        {
            int cellWidth;
            switch (examDisciplinesShortTitles.size())
            {
                case 1:
                    cellWidth = 18;
                    break;
                case 2:
                    cellWidth = 9;
                    break;
                default:
                    cellWidth = 6;
                    break;
            }
            sheet.addCell(new Label(column, row, "Результаты ЕГЭ", headerFormat));
            sheet.mergeCells(column, row, column + examDisciplinesShortTitles.size() - 1, row);
            for (int i = 0; i < examDisciplinesShortTitles.size(); i++)
            {
                sheet.addCell(new Label(column + i, row + 1, examDisciplinesShortTitles.get(i), headerFormat));
                sheet.setColumnView(column + i, cellWidth);
            }
            column += examDisciplinesShortTitles.size();

            sheet.addCell(new Label(column, row, "Результаты экзаменов", headerFormat));
            sheet.mergeCells(column, row, column + examDisciplinesShortTitles.size() - 1, row);
            for (int i = 0; i < examDisciplinesShortTitles.size(); i++)
            {
                sheet.addCell(new Label(column + i, row + 1, examDisciplinesShortTitles.get(i), headerFormat));
                sheet.setColumnView(column + i, cellWidth);
            }
            column += examDisciplinesShortTitles.size();
        }

        sheet.addCell(new Label(column, row, "Сумма баллов", headerFormat));
        sheet.mergeCells(column, row, column, row + 1);
        sheet.setColumnView(column, 6);
        column++;

        sheet.addCell(new Label(column, row, "Дополнительные преимущества", headerFormat));
        sheet.mergeCells(column, row, column, row + 1);
        sheet.setColumnView(column, 15);
        column++;

        sheet.addCell(new Label(column, row, "Решение приемной комиссии", headerFormat));
        sheet.mergeCells(column, row, column, row + 1);
        sheet.setColumnView(column, 15);
    }

    private void writeReportData(WritableSheet sheet, int column, Integer[] row, MQBuilder requestedEnrollmentDirectionsBuilder, List<Discipline2RealizationWayRelation> examDisciplines, int tableRowWidth) throws Exception
    {
        // загрузка видов целевого приема в кэш
        _session.createCriteria(TargetAdmissionKind.class).list();

        // получим настройки видов конкурса в приемной кампании
        Map<CompetitionKind, EnrollmentCompetitionKind> competitionKind2EnrollmentCompetitionKind = getCompetitionKind2EnrollmentCompetitionKind();

        // создаем общий и целевой прием
        Admission general = new GeneralAdmission(sheet, _formatMap, tableRowWidth, competitionKind2EnrollmentCompetitionKind);
        Admission target = new TargetAdmission(sheet, _formatMap, tableRowWidth, competitionKind2EnrollmentCompetitionKind);

        // рассортируем выбранные направления приема
        List<RequestedEnrollmentDirection> requestedEnrollmentDirections = requestedEnrollmentDirectionsBuilder.getResultList(_session);
        for (RequestedEnrollmentDirection direction : requestedEnrollmentDirections)
            ((direction.isTargetAdmission()) ? target : general).put(direction);

        // заполняем поля приемов
        general.direction2Fields = target.direction2Fields = getFields(requestedEnrollmentDirectionsBuilder, requestedEnrollmentDirections, examDisciplines);

        // теперь получаем список строк
        Integer[] studentHandler = new Integer[]{1};
        target.writeRows(column, row, studentHandler);
        general.writeRows(column, row, studentHandler);

        // проставляем номера абитуриентов
        //WritableCellFormat right = _formatMap.get("right");
        //for (int i = start; i < row[0]; i++)
        //    sheet.addCell(new jxl.write.Number(column, i, (double) i - start, right));
    }

    private void writeEnrollmentDirectionDetailsTable(WritableSheet sheet, int column, Integer[] row) throws Exception
    {
        WritableCellFormat defaultFormat = new WritableCellFormat(_formatMap.get("rowFormat"));
        defaultFormat.setBorder(Border.NONE, BorderLineStyle.NONE);
        defaultFormat.setWrap(false);

        WritableFont bold = new WritableFont(defaultFormat.getFont());
        bold.setBoldStyle(WritableFont.BOLD);

        sheet.addCell(new Label(column, row[0]++, "Отчет построен по направлениям подготовки (специальностям):", new WritableCellFormat(bold)));
        for (EnrollmentDirection enrollmentDirection : _enrollmentDirections)
        {
            EducationOrgUnit educationOrgUnit = enrollmentDirection.getEducationOrgUnit();

            StringBuilder title = new StringBuilder();
            title.append(educationOrgUnit.getEducationLevelHighSchool().getShortTitle()).append(" ");
            title.append("(").append(StringUtils.join(new String[]{educationOrgUnit.getDevelopForm().getTitle().toLowerCase(), educationOrgUnit.getDevelopCondition().getTitle().toLowerCase(), educationOrgUnit.getDevelopPeriod().getTitle()}, ", ")).append(") ");
            title.append(educationOrgUnit.getFormativeOrgUnit().getShortTitle()).append(" ");
            title.append("(").append(educationOrgUnit.getTerritorialOrgUnit().getTerritorialTitle()).append(")");
            sheet.addCell(new Label(column, row[0]++, title.toString(), defaultFormat));
        }
    }

    /**
     * Возвращает для каждого вида конкурса его настройку в приемной кампании
     *
     * @return вид конкурса -> вид конкурса приемной кампании
     */
    @SuppressWarnings("unchecked")
    private Map<CompetitionKind, EnrollmentCompetitionKind> getCompetitionKind2EnrollmentCompetitionKind()
    {
        Criteria criteria = _session.createCriteria(EnrollmentCompetitionKind.class);
        criteria.add(Restrictions.eq(EnrollmentCompetitionKind.L_ENROLLMENT_CAMPAIGN, _report.getEnrollmentCampaign()));
        criteria.add(Restrictions.eq(EnrollmentCompetitionKind.P_USED, true));

        Map<CompetitionKind, EnrollmentCompetitionKind> result = new HashMap<>();
        for (EnrollmentCompetitionKind enrollmentCompetitionKind : (List<EnrollmentCompetitionKind>) criteria.list())
        {
            result.put(enrollmentCompetitionKind.getCompetitionKind(), enrollmentCompetitionKind);
        }
        return result;
    }

    /**
     * @return детализация параметров отчета
     */
    private String getDetails()
    {
        String developCondition = _report.getDevelopCondition();
        boolean developConditionFilled = !StringUtils.isEmpty(developCondition);

        String developPeriod = _report.getDevelopPeriod();
        boolean developPeriodFilled = !StringUtils.isEmpty(developPeriod);

        // направление подготовки (специальности) ОУ
        EducationLevelsHighSchool educationLevelsHighSchool = _report.getEducationLevelsHighSchool();

        // если по всем направлениям
        if (educationLevelsHighSchool == null)
        {
            // то берем из них
            educationLevelsHighSchool = _enrollmentDirections.get(0).getEducationOrgUnit().getEducationLevelHighSchool();
        }

        StringBuilder result = new StringBuilder();
        result.append("Специальность (направление): ").append(educationLevelsHighSchool.getPrintTitle()).append(" ");
        result.append("Отделение: ").append(_report.getDevelopForm().getTitle()).append(" ");
        if (developConditionFilled || developPeriodFilled)
        {
            result.append("(");
            if (developConditionFilled)
            {
                result.append("Условие освоения: ").append(developCondition);
                if (developPeriodFilled)
                {
                    result.append("; ");
                }
            }
            if (developPeriodFilled)
            {
                result.append("Срок освоения: ").append(developPeriod);
            }
            result.append(") ");
        }
        result.append("Финансирование: ").append(_report.getCompensationType().getShortTitle()).append(" ");
        if (!StringUtils.isEmpty(_report.getStudentCategory()))
        {
            result.append("Категория поступающего: ").append(_report.getStudentCategory().toLowerCase());
        }
        return result.toString();
    }

    /**
     * @return билдер выбранных направлений приема, удовлетворяющих параметрам запроса
     */
    private MQBuilder getRequestedEnrollmentDirectionsBuilder()
    {
        MQBuilder builder;

        // в зависимости от стадии приемной кампании
        String enrollmentCampaignStage = _report.getEnrollmentCampaignStage();

        // если по ходу приема документов
        if (enrollmentCampaignStage.equals(Model.ENROLLMENT_CAMPAIGN_STAGE_DOCUMENTS))
        {
            builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "requestedEnrollmentDirection");
            createAliases(builder);
            builder.add(MQExpression.in("requestedEnrollmentDirection", RequestedEnrollmentDirection.enrollmentDirection().s(), _enrollmentDirections));
            builder.add(MQExpression.eq("requestedEnrollmentDirection", RequestedEnrollmentDirection.compensationType().s(), _report.getCompensationType()));
            builder.add(MQExpression.notEq("state", EntrantState.code().s(), UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
            builder.add(MQExpression.between("entrantRequest", EntrantRequest.regDate().s(), _report.getRequestsFromDate(), CoreDateUtils.add(_report.getRequestsToDate(), Calendar.DAY_OF_MONTH, 1)));
            builder.add(MQExpression.eq("entrant", Entrant.archival().s(), false));
            if (!_studentCategories.isEmpty())
            {
                builder.add(MQExpression.in("requestedEnrollmentDirection", RequestedEnrollmentDirection.studentCategory().s(), _studentCategories));
            }
        }
        // иначе если по результатам сдачи вступительных испытаний
        else if (enrollmentCampaignStage.equals(Model.ENROLLMENT_CAMPAIGN_STAGE_EXAMS))
        {
            builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "requestedEnrollmentDirection");
            createAliases(builder);
            builder.add(MQExpression.in("requestedEnrollmentDirection", RequestedEnrollmentDirection.enrollmentDirection().s(), _enrollmentDirections));
            builder.add(MQExpression.eq("requestedEnrollmentDirection", RequestedEnrollmentDirection.compensationType().s(), _report.getCompensationType()));
            builder.add(MQExpression.notIn("state", EntrantState.code().s(), UniecDefines.ENTRANT_STATE_ACTIVE_CODE, UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
            builder.add(MQExpression.between("entrantRequest", EntrantRequest.regDate().s(), _report.getRequestsFromDate(), CoreDateUtils.add(_report.getRequestsToDate(), Calendar.DAY_OF_MONTH, 1)));
            builder.add(MQExpression.eq("entrant", Entrant.archival().s(), false));
            if (!_studentCategories.isEmpty())
            {
                builder.add(MQExpression.in("requestedEnrollmentDirection", RequestedEnrollmentDirection.studentCategory().s(), _studentCategories));
            }
        }
        // иначе если по результатам зачисления
        else if (enrollmentCampaignStage.equals(Model.ENROLLMENT_CAMPAIGN_STAGE_ENROLLMENT))
        {
            List<EducationOrgUnit> educationOrgUnits = new ArrayList<>();
            for (EnrollmentDirection enrollmentDirection : _enrollmentDirections)
            {
                educationOrgUnits.add(enrollmentDirection.getEducationOrgUnit());
            }

            builder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "preliminaryEnrollmentStudent", new String[]{PreliminaryEnrollmentStudent.requestedEnrollmentDirection().s()});
            builder.addJoinFetch("preliminaryEnrollmentStudent", PreliminaryEnrollmentStudent.requestedEnrollmentDirection().s(), "requestedEnrollmentDirection");
            createAliases(builder);
            builder.add(MQExpression.eq("preliminaryEnrollmentStudent", PreliminaryEnrollmentStudent.compensationType().s(), _report.getCompensationType()));
            builder.add(MQExpression.in("preliminaryEnrollmentStudent", PreliminaryEnrollmentStudent.educationOrgUnit().s(), educationOrgUnits));
            builder.add(MQExpression.between("requestedEnrollmentDirection", RequestedEnrollmentDirection.regDate().s(), _report.getRequestsFromDate(), CoreDateUtils.add(_report.getRequestsToDate(), Calendar.DAY_OF_MONTH, 1)));
            builder.add(MQExpression.eq("entrant", Entrant.archival().s(), false));
            if (!_studentCategories.isEmpty())
            {
                builder.add(MQExpression.in("preliminaryEnrollmentStudent", PreliminaryEnrollmentStudent.studentCategory().s(), _studentCategories));
            }
        } else
        {
            throw new RuntimeException("Unknown enrollment campaign stage");
        }
        return builder;
    }

    private void createAliases(MQBuilder builder)
    {
        builder.addJoinFetch("requestedEnrollmentDirection", RequestedEnrollmentDirection.entrantRequest().s(), "entrantRequest");
        builder.addJoinFetch("entrantRequest", EntrantRequest.entrant().s(), "entrant");
        builder.addJoinFetch("entrant", Entrant.person().s(), "person");
        builder.addJoinFetch("person", Person.identityCard().s(), "identityCard");
        builder.addJoinFetch("requestedEnrollmentDirection", RequestedEnrollmentDirection.state().s(), "state");
    }

    /**
     * Возвращает для каждого выбранного направления подготовки список полей, представляющих его в таблице
     *
     * @param requestedEnrollmentDirectionsBuilder
     *                                      билдер выбранных направлений приема
     * @param requestedEnrollmentDirections выбранные направления приема
     * @param examDisciplines               дисциплины приемной кампании
     * @return выбранное направление приема -> список полей
     */
    @SuppressWarnings("deprecation")
    private Map<RequestedEnrollmentDirection, FieldValues> getFields(MQBuilder requestedEnrollmentDirectionsBuilder, List<RequestedEnrollmentDirection> requestedEnrollmentDirections, List<Discipline2RealizationWayRelation> examDisciplines)
    {
        if (requestedEnrollmentDirectionsBuilder.getResultCount(_session) == 0)
        {
            return new HashMap<>();
        }

        Map<RequestedEnrollmentDirection, FieldValues> direction2Fields = new HashMap<>();

        EntrantDataUtil entrantDataUtil = new EntrantDataUtil(_session, _report.getEnrollmentCampaign(), requestedEnrollmentDirectionsBuilder, DetailLevel.EXAM_PASS_WITH_MARK_STATISTICS);

        // профильное образование
        Map<RequestedEnrollmentDirection, Set<RequestedProfileKnowledge>> direction2ProfileKnowledges = EntrantDataUtil.getProfileKnowledgeMap(_session, requestedEnrollmentDirectionsBuilder);

        // льготы
        Map<Person, Set<PersonBenefit>> person2Benefits = EntrantDataUtil.getPersonBenefitMap(_session, requestedEnrollmentDirectionsBuilder);

        // дополнительные преимущества
        Map<Entrant, Set<EntrantEnrolmentRecommendation>> entrant2EnrollmentRecomendations = EntrantDataUtil.getEnrollmentRecomendationMap(_session, requestedEnrollmentDirectionsBuilder);

        // заполнить сразу все поля, какие возможно, для остальных сформировать вспомогательные структуры
        for (RequestedEnrollmentDirection direction : requestedEnrollmentDirections)
        {
            direction2Fields.put(direction, new FieldValues(examDisciplines.size()));

            EntrantRequest entrantRequest = direction.getEntrantRequest();
            Entrant entrant = entrantRequest.getEntrant();
            Person person = entrant.getPerson();

            FieldValues fieldValues = direction2Fields.get(direction);

            fieldValues.personFullFio = person.getFullFio(); // ФИО
            fieldValues.regNumber = Integer.parseInt(direction.getStringNumber()); // номер
            fieldValues.personBenefits = (direction.getCompetitionKind().getCode().equals(UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION)) ? "" : null; // отмечаем, что надо показать льготы


            // адрес регистрации
            fieldValues.personRegistrationAddress = (null != person.getAddressRegistration() && person.getAddressRegistration() instanceof AddressDetailed ) ? getAddressTitle(((AddressDetailed)person.getAddressRegistration()).getSettlement()) : null;

            // гражданство
            ICitizenship citizenship = person.getIdentityCard().getCitizenship();
            fieldValues.personCitizenship = (citizenship.getCode() != IKladrDefines.RUSSIA_COUNTRY_CODE) ? citizenship.getTitle() : null;

            // балл за профильное испытание
            fieldValues.profileMark = (Double) FastBeanUtils.getValue(direction, RequestedEnrollmentDirection.profileChosenEntranceDiscipline().finalMark().s());

            // учебное заведение и профильное образование
            Set<RequestedProfileKnowledge> profileKnowledges = direction2ProfileKnowledges.get(direction);
            PersonEduInstitution personEduInstitution = person.getPersonEduInstitution();
            fieldValues.personEducationLevelStage = (profileKnowledges != null) ? UniStringUtils.join(profileKnowledges, RequestedProfileKnowledge.profileKnowledge().shortTitle().s(), "; ") : (personEduInstitution != null) ? personEduInstitution.getEducationLevel().getShortTitle() : null;

            // льготы
            Set<PersonBenefit> personBenefits = person2Benefits.get(person);
            fieldValues.personBenefits = (fieldValues.personBenefits != null && personBenefits != null) ? UniStringUtils.join(personBenefits, PersonBenefit.benefit().shortTitle().s(), "; ") : null;

            // дополнительные преимущества
            Set<EntrantEnrolmentRecommendation> enrollmentDirections = entrant2EnrollmentRecomendations.get(entrant);
            fieldValues.entrantEnrolmentRecomendations = (enrollmentDirections != null) ? UniStringUtils.join(enrollmentDirections, EntrantEnrolmentRecommendation.recommendation().shortTitle().s(), "; ") : null;

            // средний балл аттестата
            fieldValues.certificateAverageMark = entrantDataUtil.getAverageEduInstMark(entrant.getId());
        }

        Map<CoreCollectionUtils.Pair<Entrant, Discipline2RealizationWayRelation>, MarkStatistic> entrantAndDiscipline2ExamMarks = new HashMap<>();

        // вступительные испытания
        for (RequestedEnrollmentDirection requestedEnrollmentDirection : requestedEnrollmentDirections)
        {
            Entrant entrant = requestedEnrollmentDirection.getEntrantRequest().getEntrant();
            for (ChosenEntranceDiscipline chosenEntranceDiscipline : entrantDataUtil.getChosenEntranceDisciplineSet(requestedEnrollmentDirection))
            {
                if (examDisciplines.contains(chosenEntranceDiscipline.getEnrollmentCampaignDiscipline()))
                {
                    CoreCollectionUtils.Pair<Entrant, Discipline2RealizationWayRelation> key = new CoreCollectionUtils.Pair<>(entrant, chosenEntranceDiscipline.getEnrollmentCampaignDiscipline());
                    MarkStatistic value = entrantDataUtil.getMarkStatistic(chosenEntranceDiscipline);

                    entrantAndDiscipline2ExamMarks.put(key, value);
                }
            }
        }

        // для каждой дисциплины
        for (int i = 0; i < examDisciplines.size(); i++)
        {
            // для каждого выбранного направления приема
            Discipline2RealizationWayRelation discipline = examDisciplines.get(i);
            for (Entry<RequestedEnrollmentDirection, FieldValues> entry : direction2Fields.entrySet())
            {
                // получаем оценки
                RequestedEnrollmentDirection requestedEnrollmentDirection = entry.getKey();
                FieldValues fieldValues = entry.getValue();
                MarkStatistic markStatistic = entrantAndDiscipline2ExamMarks.get(new CoreCollectionUtils.Pair<>(requestedEnrollmentDirection.getEntrantRequest().getEntrant(), discipline));

                if (markStatistic == null)
                {
                    continue;
                }

                // результаты ЕГЭ
                Double stateExamMark = markStatistic.getScaledStateExamMark();

                // результаты ЕГЭ в вузе
                for (Map.Entry<SubjectPassForm, PairKey<ExamPassMark, ExamPassMarkAppeal>> markEntry  : new HashMap<>(markStatistic.getInternalMarkMap()).entrySet())
                {
                    final PairKey<ExamPassMark, ExamPassMarkAppeal> marks = markEntry.getValue();
                    if (marks == null)
                        continue;

                    final SubjectPassForm passForm = markEntry.getKey();
                    if (!passForm.getCode().equals(UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM) && !passForm.getCode().equals(UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM_INTERNAL))
                        continue;

                    ExamPassMark mark = marks.getFirst();
                    ExamPassMarkAppeal appeal = marks.getSecond();

                    double finalMark = (appeal != null) ? appeal.getMark() : mark.getMark();
                    if (stateExamMark == null || finalMark > stateExamMark)
                        stateExamMark = finalMark;
                }

                fieldValues.stateExamMarks[i] = stateExamMark;

                // результаты экзаменов
                Double enrollmentExamMark = Double.MIN_VALUE;
                if (markStatistic.getOlympiad() != null)
                {
                    enrollmentExamMark = (double) markStatistic.getOlympiad();
                } else
                {
                    for (Map.Entry<SubjectPassForm, PairKey<ExamPassMark, ExamPassMarkAppeal>> markEntry  : new HashMap<>(markStatistic.getInternalMarkMap()).entrySet())
                    {
                        final PairKey<ExamPassMark, ExamPassMarkAppeal> marks = markEntry.getValue();

                        if (marks == null)
                            continue;

                        final SubjectPassForm passForm = markEntry.getKey();
                        if (passForm.getCode().equals(UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM) || passForm.getCode().equals(UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM_INTERNAL))
                            continue;

                        ExamPassMark mark = marks.getFirst();
                        ExamPassMarkAppeal appeal = marks.getSecond();

                        double finalMark = (appeal != null) ? appeal.getMark() : mark.getMark();
                        if (finalMark > enrollmentExamMark)
                        {
                            enrollmentExamMark = finalMark;
                        }
                    }
                }

                if (enrollmentExamMark != Double.MIN_VALUE)
                {
                    fieldValues.enrollmentExamMarks[i] = enrollmentExamMark;
                }
            }
        }

        return direction2Fields;
    }

    /**
     * @param settlement элемент иерархии адресов
     * @return строка адреса
     */
    private String getAddressTitle(AddressItem settlement)
    {
        if (settlement == null) return null;

        String regionCode = settlement.getInheritedRegionCode();
        AddressItem zone = getZone(settlement);

        StringBuilder result = new StringBuilder();
        if (!StringUtils.isEmpty(regionCode))
        {
            result.append("(").append(regionCode).append(") ");
        }
        result.append(settlement.getTitleWithType());
        if (zone != null)
        {
            result.append(" (").append(zone.getTitleWithType()).append(")");
        }
        return result.toString();
    }

    /**
     * @param settlement элемент иерархии адресов
     * @return район
     */
    private AddressItem getZone(AddressItem settlement)
    {
        AddressItem result = settlement;
        while ((result = result.getParent()) != null)
        {
            if (result.getAddressType().getAddressLevel().getCode() == 1)
            {
                return result;
            }
        }
        return null;
    }

    /**
     * @param enrollmentDirections направления приема
     * @return дисциплины приемной компании, которые надо сдавать в качестве выбранных вступительных испытаний для данных направлений приема
     */
    public List<Discipline2RealizationWayRelation> getExamDisciplines(List<EnrollmentDirection> enrollmentDirections)
    {
        if (enrollmentDirections.isEmpty())
        {
            return Collections.emptyList();
        }

        Map<EnrollmentDirection, List<Discipline2RealizationWayRelation>> map = ExamSetUtil.getDirection2DisciplineMap(_session, _report.getEnrollmentCampaign(), _studentCategories, _report.getCompensationType());

        Set<Discipline2RealizationWayRelation> disciplineSet = new HashSet<>();
        for (EnrollmentDirection direction : enrollmentDirections)
        {
            disciplineSet.addAll(map.get(direction));
        }

        List<Discipline2RealizationWayRelation> list = new ArrayList<>(disciplineSet);
        Collections.sort(list, ITitled.TITLED_COMPARATOR);

        return list;
    }

    /**
     * Базовый класс приема
     *
     * @author agolubenko
     * @since 03.12.2008
     */
    static abstract class Admission
    {
        /**
         * Сортирует абитуриентов внутри одного вида конкурса по итоговому баллу; если он одинаковый, то по ФИО
         */
        static final Comparator<FieldValues> COMPARATOR = (o1, o2) -> {
            int result = -UniBaseUtils.compare(o1.getTotalMark(), o2.getTotalMark(), true);
            if (result == 0) {
                result = -UniBaseUtils.compare(o1.profileMark, o2.profileMark, true);
            }
            if (result == 0) {
                result = -UniBaseUtils.compare(o1.certificateAverageMark, o2.certificateAverageMark, true);
            }
            if (result == 0) {
                result = CommonCollator.RUSSIAN_COLLATOR.compare(o1.personFullFio, o2.personFullFio);
            }
            return result;
        };

        Map<RequestedEnrollmentDirection, FieldValues> direction2Fields;
        WritableSheet sheet;
        Map<String, WritableCellFormat> formatMap;
        WritableCellFormat rowFormat;
        int tableRowWidth;

        protected Admission(WritableSheet sheet, Map<String, WritableCellFormat> formatMap, int tableRowWidth)
        {
            this.sheet = sheet;
            this.formatMap = formatMap;
            this.rowFormat = formatMap.get("rowFormat");
            this.tableRowWidth = tableRowWidth;
        }

        /**
         * Добавляет выбранное направление подготовки
         *
         * @param requestedEnrollmentDirection выбранное направление подготовки
         */
        abstract void put(RequestedEnrollmentDirection requestedEnrollmentDirection);

        /**
         * Возвращает список строк для данного приема
         */
        abstract void writeRows(int column, Integer[] row, Integer[] studentHandler) throws Exception;

        static String getTitle(Map.Entry<?, ?> entry)
        {
            return ((ITitled) entry.getKey()).getTitle();
        }

        /**
         * @param requestedEnrollmentDirections выбранные направления приема абитуриентов
         * @return отсортированный список полей данных направлений
         */
        List<FieldValues> getSortedFieldsList(List<RequestedEnrollmentDirection> requestedEnrollmentDirections)
        {
            List<FieldValues> result = new ArrayList<>();
            for (RequestedEnrollmentDirection direction : requestedEnrollmentDirections)
                result.add(direction2Fields.get(direction));
            Collections.sort(result, Admission.COMPARATOR);
            return result;
        }

        void writeTitleRow(String title, WritableCellFormat format, int column, int row) throws Exception
        {
            sheet.addCell(new Blank(column, row, rowFormat));
            sheet.addCell(new Label(column + 1, row, title, format));
            for (int i = 2; i <= tableRowWidth; i++)
                sheet.addCell(new Blank(column + i, row, rowFormat));
        }
    }

    /**
     * Общий прием
     *
     * @author agolubenko
     * @since 03.12.2008
     */
    static class GeneralAdmission extends Admission
    {
        /**
         * Структура общего приема:
         * <p/>
         * ОБЩИЙ ПРИЕМ
         * [Вид конкурса]
         * [Выбранное направление приема]
         */
        Map<EnrollmentCompetitionKind, List<RequestedEnrollmentDirection>> structure = new TreeMap<>();

        Map<CompetitionKind, EnrollmentCompetitionKind> competitionKind2EnrollmentCompetitionKind;

        GeneralAdmission(WritableSheet sheet, Map<String, WritableCellFormat> formatMap, int tableRowWidth, Map<CompetitionKind, EnrollmentCompetitionKind> competitionKind2EnrollmentCompetitionKind)
        {
            super(sheet, formatMap, tableRowWidth);
            this.competitionKind2EnrollmentCompetitionKind = competitionKind2EnrollmentCompetitionKind;
        }

        @Override
        void put(RequestedEnrollmentDirection requestedEnrollmentDirection)
        {
            EnrollmentCompetitionKind enrollmentCompetitionKind = competitionKind2EnrollmentCompetitionKind.get(requestedEnrollmentDirection.getCompetitionKind());
            List<RequestedEnrollmentDirection> list = structure.get(enrollmentCompetitionKind);
            if (list == null)
            {
                list = new ArrayList<>();
                structure.put(enrollmentCompetitionKind, list);
            }
            list.add(requestedEnrollmentDirection);
        }

        @Override
        void writeRows(int column, Integer[] row, Integer[] studentHandler) throws Exception
        {
            writeTitleRow("ОБЩИЙ ПРИЕМ", formatMap.get("ga"), column, row[0]++);
            for (Map.Entry<EnrollmentCompetitionKind, List<RequestedEnrollmentDirection>> entry : structure.entrySet())
            {
                writeTitleRow(getTitle(entry), formatMap.get("gaCompetitionKind"), column, row[0]++);

                for (FieldValues fieldValues : getSortedFieldsList(entry.getValue()))
                    fieldValues.writeRow(sheet, studentHandler[0]++, column, row[0]++, formatMap.get("gaFIO"), formatMap);
            }
        }
    }

    /**
     * Целевой прием
     *
     * @author agolubenko
     * @since 03.12.2008
     */
    @SuppressWarnings("unchecked")
    static class TargetAdmission extends Admission
    {
        /**
         * Структура целевого приема:
         * <p/>
         * ЦЕЛЕВОЙ ПРИЕМ
         * [Вид целевого приема первого уровня]
         * [Вид целевого приема второго уровня]
         * [Вид конкурса]
         * [Выбранное направление приема]
         * [Вид конкурса]
         * [Выбранное направление приема]
         */
        Map<TargetAdmissionKind, Map<?, ?>> _structure = new TreeMap<>(TargetAdmissionKind.PRIORITY_COMPARATOR);

        Map<CompetitionKind, EnrollmentCompetitionKind> competitionKind2EnrollmentCompetitionKind;

        TargetAdmission(WritableSheet sheet, Map<String, WritableCellFormat> formatMap, int tableRowWidth, Map<CompetitionKind, EnrollmentCompetitionKind> competitionKind2EnrollmentCompetitionKind)
        {
            super(sheet, formatMap, tableRowWidth);
            this.competitionKind2EnrollmentCompetitionKind = competitionKind2EnrollmentCompetitionKind;
        }

        @Override
        void put(RequestedEnrollmentDirection requestedEnrollmentDirection)
        {
            EnrollmentCompetitionKind enrollmentCompetitionKind = competitionKind2EnrollmentCompetitionKind.get(requestedEnrollmentDirection.getCompetitionKind());

            TargetAdmissionKind targetAdmissionKind = requestedEnrollmentDirection.getTargetAdmissionKind();
            TargetAdmissionKind parent = (targetAdmissionKind != null) ? targetAdmissionKind.getParent() : null;

            if (parent == null)
            {
                Map<EnrollmentCompetitionKind, List<RequestedEnrollmentDirection>> map = (Map<EnrollmentCompetitionKind, List<RequestedEnrollmentDirection>>) _structure.get(targetAdmissionKind);
                if (map == null)
                {
                    map = new TreeMap<>();
                    _structure.put(targetAdmissionKind, map);
                }
                List<RequestedEnrollmentDirection> list = map.get(enrollmentCompetitionKind);
                if (list == null)
                {
                    list = new ArrayList<>();
                    map.put(enrollmentCompetitionKind, list);
                }
                list.add(requestedEnrollmentDirection);
            } else
            {
                Map<TargetAdmissionKind, Map<EnrollmentCompetitionKind, List<RequestedEnrollmentDirection>>> map1 = (Map<TargetAdmissionKind, Map<EnrollmentCompetitionKind, List<RequestedEnrollmentDirection>>>) _structure.get(parent);
                if (map1 == null)
                {
                    map1 = new TreeMap<>(TargetAdmissionKind.PRIORITY_COMPARATOR);
                    _structure.put(parent, map1);
                }
                Map<EnrollmentCompetitionKind, List<RequestedEnrollmentDirection>> map2 = map1.get(targetAdmissionKind);
                if (map2 == null)
                {
                    map2 = new TreeMap<>();
                    map1.put(targetAdmissionKind, map2);
                }
                List<RequestedEnrollmentDirection> list = map2.get(enrollmentCompetitionKind);
                if (list == null)
                {
                    list = new ArrayList<>();
                    map2.put(enrollmentCompetitionKind, list);
                }
                list.add(requestedEnrollmentDirection);
            }
        }

        @Override
        void writeRows(int column, Integer[] row, Integer[] studentHandler) throws Exception
        {
            WritableCellFormat format1 = new WritableCellFormat(formatMap.get("taCompetitionKind"));
            format1.setIndentation(format1.getIndentation() - 1);

            WritableCellFormat format2 = new WritableCellFormat(formatMap.get("taFIO"));
            format1.setIndentation(format2.getIndentation() - 1);

            writeTitleRow("ЦЕЛЕВОЙ ПРИЕМ", formatMap.get("ta"), column, row[0]++);
            for (Map.Entry entry : _structure.entrySet())
            {
                if (entry.getKey() != null)
                {
                    writeTitleRow(getTitle(entry), formatMap.get("taKind"), column, row[0]++);
                }
                for (Map.Entry entry2 : (Set<Map.Entry>) ((Map) entry.getValue()).entrySet())
                {
                    if (entry2.getKey() instanceof EnrollmentCompetitionKind)
                    {
                        writeTitleRow(getTitle(entry2), format1, column, row[0]++);
                        for (FieldValues fieldValues : getSortedFieldsList((List<RequestedEnrollmentDirection>) entry2.getValue()))
                            fieldValues.writeRow(sheet, studentHandler[0]++, column, row[0]++, format2, formatMap);
                    } else
                    {
                        if (entry2.getKey() != null)
                            writeTitleRow(getTitle(entry2), formatMap.get("taPlace"), column, row[0]++);
                        for (Map.Entry entry3 : (Set<Map.Entry>) ((Map) entry2.getValue()).entrySet())
                        {
                            writeTitleRow(getTitle(entry3), formatMap.get("taCompetitionKind"), column, row[0]++);
                            for (FieldValues fieldValues : getSortedFieldsList((List<RequestedEnrollmentDirection>) entry3.getValue()))
                                fieldValues.writeRow(sheet, studentHandler[0]++, column, row[0]++, formatMap.get("taFIO"), formatMap);
                        }
                    }
                }
            }
        }
    }

    /**
     * Класс для хранения значений полей выбранного направления приема (массивом пользоваться неудобно - в индексах легко запутаться)
     *
     * @author agolubenko
     * @since 04.12.2008
     */
    static class FieldValues
    {
        static final Double NO_FINAL_MARK = -1.0;
        int examMarksNumber;
        Double profileMark;

        String personFullFio;
        int regNumber;
        String personEducationLevelStage;
        String personRegistrationAddress;
        String personCitizenship;
        String personBenefits;
        Double[] stateExamMarks;
        Double[] enrollmentExamMarks;
        Double totalMark;
        String entrantEnrolmentRecomendations;

        Double certificateAverageMark;

        Double getTotalMark()
        {
            // проверяем закешированное значение
            if (totalMark != null)
            {
                return totalMark;
            }

            Double result = null;

            // для всех оценок
            for (int i = 0; i < examMarksNumber; i++)
            {
                // находим максимум по ним между егэ и экзаменами, то есть по сути берем итоговый балл
                Double mark = max(stateExamMarks[i], enrollmentExamMarks[i]);

                // прибавляем к результату
                if (!mark.equals(NO_FINAL_MARK))
                {
                    if (result == null)
                    {
                        result = mark;
                    } else
                    {
                        result += mark;
                    }
                }
            }
            return (totalMark = result);
        }

        Double max(Double m1, Double m2)
        {
            if (m1 == null)
            {
                m1 = NO_FINAL_MARK;
            }
            if (m2 == null)
            {
                m2 = NO_FINAL_MARK;
            }
            return Math.max(m1, m2);
        }

        FieldValues(int examMarksNumber)
        {
            this.examMarksNumber = examMarksNumber;

            stateExamMarks = (examMarksNumber != 0) ? new Double[examMarksNumber] : new Double[1]; // колонка "Результаты ЕГЭ"
            enrollmentExamMarks = (examMarksNumber != 0) ? new Double[examMarksNumber] : new Double[1]; // колонка "Результаты экзаменов"
        }

        void writeRow(WritableSheet sheet, int studentNumber, int column, int row, WritableCellFormat fioFormat, Map<String, WritableCellFormat> formatMap) throws Exception
        {
            WritableCellFormat rowFormat = formatMap.get("rowFormat");

            sheet.addCell(new jxl.write.Number(column++, row, studentNumber, rowFormat));
            sheet.addCell(new Label(column++, row, personFullFio, fioFormat));
            sheet.addCell(new jxl.write.Number(column++, row, regNumber, formatMap.get("right")));

            for (String field : new String[]{personEducationLevelStage, personRegistrationAddress, personCitizenship, personBenefits})
            {
                sheet.addCell(new Label(column++, row, field, rowFormat));
            }

            for (Double mark : stateExamMarks)
            {
                sheet.addCell(mark != null && !mark.equals(NO_FINAL_MARK) ? new jxl.write.Number(column++, row, mark, formatMap.get("center")) : new Blank(column++, row, formatMap.get("center")));
            }

            for (Double mark : enrollmentExamMarks)
            {
                sheet.addCell(mark != null && !mark.equals(NO_FINAL_MARK) ? new jxl.write.Number(column++, row, mark, formatMap.get("center")) : new Blank(column++, row, formatMap.get("center")));
            }

            sheet.addCell(getTotalMark() != null ? new jxl.write.Number(column++, row, getTotalMark(), formatMap.get("center")) : new Blank(column++, row, formatMap.get("center")));

            sheet.addCell(new Label(column++, row, entrantEnrolmentRecomendations, rowFormat));
            sheet.addCell(new Blank(column, row, rowFormat));
        }

        /**
         * @return длина строки
         */
        int getRowWidth()
        {
            return 9 + stateExamMarks.length + enrollmentExamMarks.length;
        }
    }
}
