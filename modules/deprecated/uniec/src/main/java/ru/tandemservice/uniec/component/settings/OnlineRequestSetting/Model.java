/* $Id$ */
package ru.tandemservice.uniec.component.settings.OnlineRequestSetting;

import java.util.List;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;

import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

/**
 * @author Vasily Zhukov
 * @since 03.06.2011
 */
public class Model implements IEnrollmentCampaignSelectModel
{
    public static final Long NOT_ALLOW_ID = 0L;
    public static final Long ALLOW_ID = 1L;

    private List<IdentifiableWrapper> _yesNoList;
    private IdentifiableWrapper _allowUploadDocuments;
    private IdentifiableWrapper _allowPrintOnlineRequest;
    private IDataSettings _settings;
    private List<EnrollmentCampaign> _enrollmentCampaignList;

    // IEnrollmentCampaignSelectModel

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return (EnrollmentCampaign) _settings.get(ENROLLMENT_CAMPAIGN_FILTER_NAME);
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _settings.set(ENROLLMENT_CAMPAIGN_FILTER_NAME, enrollmentCampaign);
    }

    // Getters & Setters

    public List<IdentifiableWrapper> getYesNoList()
    {
        return _yesNoList;
    }

    public void setYesNoList(List<IdentifiableWrapper> yesNoList)
    {
        _yesNoList = yesNoList;
    }

    public IdentifiableWrapper getAllowUploadDocuments()
    {
        return _allowUploadDocuments;
    }

    public void setAllowUploadDocuments(IdentifiableWrapper allowUploadDocuments)
    {
        _allowUploadDocuments = allowUploadDocuments;
    }

    public IdentifiableWrapper getAllowPrintOnlineRequest()
    {
        return _allowPrintOnlineRequest;
    }

    public void setAllowPrintOnlineRequest(IdentifiableWrapper allowPrintOnlineRequest)
    {
        _allowPrintOnlineRequest = allowPrintOnlineRequest;
    }

    @Override
    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }
}
