/**
 *$Id$
 */
package ru.tandemservice.uniec.base.bo.EcExamGroup;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import ru.tandemservice.uniec.base.bo.EcExamGroup.logic.ExamGroupSpecDSHandler;
import ru.tandemservice.uniec.base.bo.EcExamGroup.logic.ExamGroupSpecificationDAO;
import ru.tandemservice.uniec.base.bo.EcExamGroup.logic.IExamGroupSpecificationDAO;

/**
 * @author Alexander Shaburov
 * @since 09.07.12
 */
@Configuration
public class EcExamGroupManager extends BusinessObjectManager
{
    public static EcExamGroupManager instance()
    {
        return instance(EcExamGroupManager.class);
    }

    @Bean
    public IExamGroupSpecificationDAO examGroupSpecificationDAO()
    {
        return new ExamGroupSpecificationDAO();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> examGroupSpecDSHandler()
    {
        return new ExamGroupSpecDSHandler(getName());
    }
}
