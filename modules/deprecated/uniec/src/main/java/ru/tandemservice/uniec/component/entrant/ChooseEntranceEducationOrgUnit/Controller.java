/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.ChooseEntranceEducationOrgUnit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.formatter.BaseRawFormatter;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

/**
 * @author Боба
 * @since 22.08.2008
 */
public class Controller extends AbstractBusinessController<IDAO, ChooseEntranceEducationOrgUnitModel>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getModel(component).setPrincipalContext(component.getUserContext().getPrincipalContext());
        getDao().prepare(getModel(component));
        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        final ChooseEntranceEducationOrgUnitModel model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<EnrollmentDirection> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new CheckboxColumn("checkbox").setSelectCaption("Выбор").setWidth(1));
        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().printTitle().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Формирующее подразделение", EnrollmentDirection.educationOrgUnit().formativeOrgUnit().shortTitle().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Территориальное подразделение", EnrollmentDirection.educationOrgUnit().territorialOrgUnit().shortTitle().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Форма освоения", EnrollmentDirection.educationOrgUnit().developForm().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Условие освоения", EnrollmentDirection.educationOrgUnit().developCondition().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Технология освоения", EnrollmentDirection.educationOrgUnit().developTech().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Срок освоения", EnrollmentDirection.educationOrgUnit().developPeriod().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Набор", "", new BaseRawFormatter<EnrollmentDirection>()
        {
            @Override
            public String format(EnrollmentDirection enrollmentDirection)
            {
                return model.getId2title().get(enrollmentDirection.getId()).replace("\n", "<br/>");
            }
        }).setClickable(false).setOrderable(false));
        model.setDataSource(dataSource);
    }

    public void onClickApply(IBusinessComponent context)
    {
        getDao().update(getModel(context));
        deactivate(context);
    }
}
