/* $Id$ */
package ru.tandemservice.uniec.component.report.SummaryQuotasMarksResults.Add;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.utils.CommonYesNoUIObject;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.report.SummaryQuotasMarksResultsReport;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Vasily Zhukov
 * @since 18.08.2011
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    @SuppressWarnings("deprecation")
    public void prepare(Model model)
    {
        model.setParameters(new MultiEnrollmentDirectionUtil.Parameters());

        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.setEnrollmentCampaignStageList(Arrays.asList(
                new IdentifiableWrapper((long) Model.ENROLLMENT_CAMP_STAGE_DOCUMENTS, "по ходу приема документов"),
                new IdentifiableWrapper((long) Model.ENROLLMENT_CAMP_STAGE_EXAMS, "по результатам сдачи вступительных испытаний"),
                new IdentifiableWrapper((long) Model.ENROLLMENT_CAMP_STAGE_ENROLLMENT, "по результатам зачисления")
        ));
        model.getReport().setDateFrom(model.getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : model.getEnrollmentCampaign().getStartDate());
        model.getReport().setDateTo(CoreDateUtils.getDayLastTimeMoment(new Date()));
        model.setStudentCategoryListModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(StudentCategory.class)));
        model.setQualificationListModel(new QualificationModel(getSession()));
        model.setCompensationTypeListModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(CompensationType.class), CompensationType.P_SHORT_TITLE));
        model.setFormativeOrgUnitListModel(MultiEnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setTerritorialOrgUnitListModel(MultiEnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setEducationLevelHighSchoolListModel(MultiEnrollmentDirectionUtil.createEducationLevelsHighSchoolModel(model));
        model.setDevelopFormListModel(MultiEnrollmentDirectionUtil.createDevelopFormModel(model));
        model.setDevelopConditionListModel(MultiEnrollmentDirectionUtil.createDevelopConditionModel(model));
        model.setDevelopTechListModel(MultiEnrollmentDirectionUtil.createDevelopTechModel(model));
        model.setDevelopPeriodListModel(MultiEnrollmentDirectionUtil.createDevelopPeriodModel(model));
        model.setParallelList(CommonYesNoUIObject.createYesNoList());

        EnrollmentCampaign enrollmentCampaign = model.getEnrollmentCampaign();
        if (enrollmentCampaign != null)
        {
            int year = CoreDateUtils.get(enrollmentCampaign.getStartDate(), Calendar.YEAR);
            model.setEnroll3007title("30.07." + year + " г.");
            model.setEnroll0508title("05.08." + year + " г.");
            model.setEnroll3108title("10.08." + year + " г.");
        }
    }

    @Override
    public void update(Model model)
    {
        final Session session = getSession();

        SummaryQuotasMarksResultsReport report = model.getReport();
        report.setFormingDate(new Date());
        report.setEnrollmentCampaignStage(model.getEnrollmentCampaignStage().getTitle());

        if (model.isStudentCategoryActive())
            report.setStudentCategoryTitle(UniStringUtils.join(model.getStudentCategoryList(), StudentCategory.P_TITLE, "; "));

        if (model.isQualificationActive())
            report.setQualificationTitle(StringUtils.join(CommonBaseUtil.getPropertiesList(model.getQualificationList(), Qualifications.P_TITLE), "; "));

        if (model.getCompensationType() != null)
            report.setCompensationTypeTitle(model.getCompensationType().getShortTitle());

        if (model.isFormativeOrgUnitActive())
            report.setFormativeOrgUnitTitle(StringUtils.join(CommonBaseUtil.getPropertiesList(model.getFormativeOrgUnitList(), OrgUnit.P_FULL_TITLE), "; "));

        if (model.isTerritorialOrgUnitActive())
            report.setTerritorialOrgUnitTitle(StringUtils.join(CommonBaseUtil.getPropertiesList(model.getTerritorialOrgUnitList(), OrgUnit.P_TERRITORIAL_FULL_TITLE), "; "));

        if (model.isEducationLevelHighSchoolActive())
            report.setEducationLevelHighSchoolTitle(StringUtils.join(CommonBaseUtil.getPropertiesList(model.getEducationLevelHighSchoolList(), EducationLevelsHighSchool.P_DISPLAYABLE_TITLE), "; "));

        if (model.isDevelopFormActive())
            report.setDevelopFormTitle(StringUtils.join(CommonBaseUtil.getPropertiesList(model.getDevelopFormList(), DevelopForm.P_TITLE), "; "));

        if (model.isDevelopConditionActive())
            report.setDevelopConditionTitle(StringUtils.join(CommonBaseUtil.getPropertiesList(model.getDevelopConditionList(), DevelopCondition.P_TITLE), "; "));

        if (model.isDevelopTechActive())
            report.setDevelopTechTitle(StringUtils.join(CommonBaseUtil.getPropertiesList(model.getDevelopTechList(), DevelopTech.P_TITLE), "; "));

        if (model.isDevelopPeriodActive())
            report.setDevelopPeriodTitle(StringUtils.join(CommonBaseUtil.getPropertiesList(model.getDevelopPeriodList(), DevelopPeriod.P_TITLE), "; "));

        if (model.isParallelActive())
            report.setExcludeParallel(model.getParallel().isTrue());

        DatabaseFile databaseFile = new SummaryQuotasMarksResultsReportBuilder(model, session).getContent();
        session.save(databaseFile);

        report.setContent(databaseFile);
        session.save(report);
    }
}
