/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcSettings.ui.IndividualProgressSetting;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uniec.base.bo.EcCampaign.EcCampaignManager;
import ru.tandemservice.uniec.base.bo.EcSettings.logic.IndividualProgressDSHandler;
import ru.tandemservice.uniec.entity.catalog.IndividualProgress;
import ru.tandemservice.uniec.entity.entrant.EnrCampaignEntrantIndividualProgress;

/**
 * @author Ekaterina Zvereva
 * @since 31.03.2015
 */
@Configuration
public class EcSettingsIndividualProgressSetting extends BusinessComponentManager
{

    public static final String ENROLLMENT_CAMPAIGN_DS = "enrCampaignDS";
    public static final String ENTRANT_INDIVIDUAL_PROGRESS_DS = "entrantIndividualProgressDS";


    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ENROLLMENT_CAMPAIGN_DS,  EcCampaignManager.instance().enrollmentCampaignComboDSHandler()))
                .addDataSource(searchListDS(ENTRANT_INDIVIDUAL_PROGRESS_DS, entrantIndividualProgressDSColumn(), entrantIndividualProgressSearchDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint entrantIndividualProgressDSColumn()
    {
        return columnListExtPointBuilder(ENTRANT_INDIVIDUAL_PROGRESS_DS)
                .addColumn(textColumn("title", IndividualProgress.title()))
                .addColumn(textColumn("shortTitle", IndividualProgress.shortTitle()))
                .addColumn(textColumn("maxMark", IndividualProgress.maxMark()))
                .addColumn(
                        toggleColumn("use", EnrCampaignEntrantIndividualProgress.INDIVID_ARCHIEVEMENT_USED)
                                .toggleOnListener("onToggleEntrantIndividualProgressUse").toggleOnLabel("entrantIndividualProgress.use")
                                .toggleOffListener("onToggleEntrantIndividualProgressUse").toggleOffLabel("entrantIndividualProgress.notUse")
                )
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> entrantIndividualProgressSearchDSHandler()
    {
        return new IndividualProgressDSHandler(getName());
    }

}