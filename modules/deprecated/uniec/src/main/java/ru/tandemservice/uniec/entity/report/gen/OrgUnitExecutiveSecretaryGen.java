package ru.tandemservice.uniec.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.report.OrgUnitExecutiveSecretary;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Ответственный секретарь
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OrgUnitExecutiveSecretaryGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.report.OrgUnitExecutiveSecretary";
    public static final String ENTITY_NAME = "orgUnitExecutiveSecretary";
    public static final int VERSION_HASH = -1946185011;
    private static IEntityMeta ENTITY_META;

    public static final String P_FIO = "fio";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_COMPENSATION_TYPE = "compensationType";

    private String _fio;     // ФИО
    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private OrgUnit _orgUnit;     // Подразделение
    private CompensationType _compensationType;     // Вид возмещения затрат

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ФИО. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFio()
    {
        return _fio;
    }

    /**
     * @param fio ФИО. Свойство не может быть null.
     */
    public void setFio(String fio)
    {
        dirty(_fio, fio);
        _fio = fio;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof OrgUnitExecutiveSecretaryGen)
        {
            setFio(((OrgUnitExecutiveSecretary)another).getFio());
            setEnrollmentCampaign(((OrgUnitExecutiveSecretary)another).getEnrollmentCampaign());
            setOrgUnit(((OrgUnitExecutiveSecretary)another).getOrgUnit());
            setCompensationType(((OrgUnitExecutiveSecretary)another).getCompensationType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OrgUnitExecutiveSecretaryGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OrgUnitExecutiveSecretary.class;
        }

        public T newInstance()
        {
            return (T) new OrgUnitExecutiveSecretary();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "fio":
                    return obj.getFio();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "compensationType":
                    return obj.getCompensationType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "fio":
                    obj.setFio((String) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "fio":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "orgUnit":
                        return true;
                case "compensationType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "fio":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "orgUnit":
                    return true;
                case "compensationType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "fio":
                    return String.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "compensationType":
                    return CompensationType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OrgUnitExecutiveSecretary> _dslPath = new Path<OrgUnitExecutiveSecretary>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OrgUnitExecutiveSecretary");
    }
            

    /**
     * @return ФИО. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.OrgUnitExecutiveSecretary#getFio()
     */
    public static PropertyPath<String> fio()
    {
        return _dslPath.fio();
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.OrgUnitExecutiveSecretary#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.OrgUnitExecutiveSecretary#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.OrgUnitExecutiveSecretary#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    public static class Path<E extends OrgUnitExecutiveSecretary> extends EntityPath<E>
    {
        private PropertyPath<String> _fio;
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private CompensationType.Path<CompensationType> _compensationType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ФИО. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.OrgUnitExecutiveSecretary#getFio()
     */
        public PropertyPath<String> fio()
        {
            if(_fio == null )
                _fio = new PropertyPath<String>(OrgUnitExecutiveSecretaryGen.P_FIO, this);
            return _fio;
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.OrgUnitExecutiveSecretary#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.OrgUnitExecutiveSecretary#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.OrgUnitExecutiveSecretary#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

        public Class getEntityClass()
        {
            return OrgUnitExecutiveSecretary.class;
        }

        public String getEntityName()
        {
            return "orgUnitExecutiveSecretary";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
