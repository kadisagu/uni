// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EntrantRegistryForm2.EntrantRegistryForm2Pub;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;

import ru.tandemservice.uniec.entity.report.EntrantRegistryForm2Report;

/**
 * @author oleyba
 * @since 09.06.2010
 */
@Input(keys = PublisherActivator.PUBLISHER_ID_KEY, bindings = "report.id")
public class Model
{
    private EntrantRegistryForm2Report report = new EntrantRegistryForm2Report();

    public EntrantRegistryForm2Report getReport()
    {
        return report;
    }

    public void setReport(EntrantRegistryForm2Report report)
    {
        this.report = report;
    }
}
