/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EnrollmentResultByOrgUnit.EnrollmentResultByOrgUnitPub;

import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;

import ru.tandemservice.uniec.entity.report.EnrollmentResultByOrgUnitReport;

/**
 * @author ekachanova
 */
@State(keys = PublisherActivator.PUBLISHER_ID_KEY, bindings = "report.id")
public class Model
{
    private EnrollmentResultByOrgUnitReport _report = new EnrollmentResultByOrgUnitReport();

    public EnrollmentResultByOrgUnitReport getReport()
    {
        return _report;
    }

    public void setReport(EnrollmentResultByOrgUnitReport report)
    {
        _report = report;
    }
}
