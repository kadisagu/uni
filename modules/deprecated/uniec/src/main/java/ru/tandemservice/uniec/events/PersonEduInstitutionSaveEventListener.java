/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.events;

import java.util.List;

import org.tandemframework.hibsupport.event.single.FilteredSingleEntityEventListener;
import org.tandemframework.hibsupport.event.single.listener.IHibernateInsertListener;
import org.tandemframework.hibsupport.event.single.type.HibernateSaveEvent;

import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniec.entity.entrant.Entrant;

/**
 * @author vip_delete
 * @since 04.06.2009
 */
public class PersonEduInstitutionSaveEventListener extends FilteredSingleEntityEventListener<HibernateSaveEvent> implements IHibernateInsertListener
{
    @Override
    public void onFilteredEvent(HibernateSaveEvent event)
    {
        PersonEduInstitution personEduInstitution = (PersonEduInstitution) event.getEntity();
        if (personEduInstitution.getEduInstitution() != null && personEduInstitution.getEduInstitution().isProfile())
        {
            List<Entrant> entrantList = UniDaoFacade.getCoreDao().getList(Entrant.class, Entrant.L_PERSON, personEduInstitution.getPerson());
            for (Entrant entrant : entrantList)
            {
                entrant.setPassProfileEducation(true);
                event.getSession().update(entrant);
            }
        }
    }
}
