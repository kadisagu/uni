/* $Id:$ */
package ru.tandemservice.uniec.component.entrant.EntrantDirectionPriorityEdit;

/**
 * @author oleyba
 * @since 5/25/12
 */
public interface IDAO
{
    void prepare(Model model);

    void update(Model model);
}
