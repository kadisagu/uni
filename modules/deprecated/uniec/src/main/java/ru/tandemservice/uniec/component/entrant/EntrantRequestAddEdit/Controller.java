/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantRequestAddEdit;

import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.component.impl.BusinessComponent;
import org.tandemframework.core.component.impl.ComponentRegion;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.IUniecComponents;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.util.EntrantRequestAddEditUtil;

import java.util.List;
import java.util.Set;

/**
 * @author Боба
 * @since 08.08.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        String prefix = model.isAddForm() ? "addForm" : "editForm";
        model.setRegDateVisible("show".equals(getMessage(prefix + ".regDate")));
        model.setTechnicCommissionVisible("show".equals(getMessage(prefix + ".technicCommission")));

        // показывать номер заявления на форме редактирования или при условии его ручного ввода
        model.setRegNumberVisible(model.isEditForm() || model.getEnrollmentCampaign().isNumberOnRequestManual());

        if (!model.isOneDirectionPerRequest())
        {
            prepareSelectedRequestedEnrollmentDirectionDataSource(component);
        }

        model.setRegDateDisabled(!CoreServices.securityService().check(model.getEntrantRequest().getEntrant(), component.getUserContext().getPrincipalContext(), "editRequestRegistrationDate"));
        
        onRefresh(component);
    }

    protected void validateFields(IBusinessComponent component)
    {
        Model model = getModel(component);
        ErrorCollector errors = component.getUserContext().getErrorCollector();
        if (model.isRegDateVisible() && model.getEntrantRequest().getRegDate() == null)
            errors.add("Поле «Дата добавления» обязательно для заполнения.", "regDate");
        if (model.isRegNumberVisible() && model.getRegNumber() == null)
            errors.add("Поле «Регистрационный номер» обязательно для заполнения.", "regNumber");
        if (model.isTechnicCommissionVisible() && model.getEntrantRequest().getTechnicCommission() == null)
            errors.add("Поле «Техническая комиссия» обязательно для заполнения.", "technicCommission");
    }

    public final void onClickNext(IBusinessComponent component)
    {
        IDAO dao = getDao();
        Model model = getModel(component);

        validateFields(component);
        if (component.getUserContext().getErrorCollector().hasErrors())
            return;

        // если мы на форме добавления и можно выбрать только одно направление в заявлении
        // то должны произойти те же действия, что и по кнопке "Выбрать" в случае, когда можно добавлять несколько
        boolean oneDirectionAtAddForm = model.isAddForm() && model.isOneDirectionPerRequest();

        if (oneDirectionAtAddForm)
        {
            ErrorCollector errors = component.getUserContext().getErrorCollector();
            dao.prepareSelectedEnrollmentDirectionList(model, errors);
            if (errors.hasErrors())
            {
                return;
            }
        }

        try
        {
            dao.update(model);
        }
        catch (ApplicationException e)
        {
            // если была ошибка, то надо очистить выбранные направления
            if (oneDirectionAtAddForm)
            {
                model.getSelectedRequestedEnrollmentDirectionList().clear();
            }
            throw e;
        }
        catch (Throwable e)
        {
            // если была ошибка, то надо очистить выбранные направления
            if (oneDirectionAtAddForm)
            {
                model.getSelectedRequestedEnrollmentDirectionList().clear();
            }
            throw new RuntimeException(e.getMessage(), e);
        }

        activateNextRegion(component);
    }

    protected void activateNextRegion(IBusinessComponent component)
    {
        Model model = getModel(component);

        if (model.isAddForm())
        {
            component.createDefaultChildRegion(new ComponentActivator(IUniecComponents.ENTRANT_REQUEST_DOCUMENT_ADD_EDIT, new ParametersMap().add("entrantRequestId", model.getEntrantRequest().getId()).add("addForm", Boolean.TRUE)));
        }
        else
        {
            deactivate(component);
            ((BusinessComponent) ((ComponentRegion) component.getParentRegion()).getOwner()).refresh();
        }
    }

    private void prepareSelectedRequestedEnrollmentDirectionDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getSelectedRequestedEnrollmentDirectionDataSource() != null)
        {
            return;
        }

        model.setSelectedRequestedEnrollmentDirectionDataSource(EntrantRequestAddEditUtil.getSelectedRequestedEnrollmentDirectionDataSource(component, component1 -> {
            model.getSelectedRequestedEnrollmentDirectionDataSource().setCountRow(model.getSelectedRequestedEnrollmentDirectionList().size());
            UniBaseUtils.createPage(model.getSelectedRequestedEnrollmentDirectionDataSource(), model.getSelectedRequestedEnrollmentDirectionList());
        }));
    }

    public boolean isExistProfileDisciplineList(Model model)
    {
        return getDao().prepareProfileDisciplineList(model);
    }

    // Event Listeners

    public void onClickSelect(IBusinessComponent component)
    {
        ErrorCollector errors = component.getUserContext().getErrorCollector();
        getDao().prepareSelectedEnrollmentDirectionList(getModel(component), errors);
        if (!errors.hasErrors())
        {
            onClickReset(component);
        }
    }

    public void onClickReset(IBusinessComponent component)
    {
        getDao().reset(getModel(component));
    }

    public void onClickUp(IBusinessComponent component)
    {
        Model model = getModel(component);
        Long id = (Long) component.getListenerParameter();
        EntrantRequestAddEditUtil.processClickUp(model.getSelectedRequestedEnrollmentDirectionList(), id);
    }

    public void onClickDown(IBusinessComponent component)
    {
        Model model = getModel(component);
        Long id = (Long) component.getListenerParameter();
        EntrantRequestAddEditUtil.processClickDown(model.getSelectedRequestedEnrollmentDirectionList(), id);
    }

    public void onClickDelete(IBusinessComponent component)
    {
        Model model = getModel(component);
        Long id = (Long) component.getListenerParameter();
        Set<Long> existedIds = model.getExistedRequestedEnrollmentDirectionIds();
        List<RequestedEnrollmentDirection> forDeleteList = model.getForDelete();
        RequestedEnrollmentDirection forDelete = EntrantRequestAddEditUtil.processClickDelete(model.getSelectedRequestedEnrollmentDirectionList(), id, existedIds, forDeleteList);
        if (forDelete != null)
        {
            model.getProfileMarkMap().remove(forDelete.getId());
        }
    }

    public void onRefresh(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.isAddForm() && model.isOneDirectionPerRequest())
        {
            model.setDirectionNumber(getDao().getMaxRequestedEnrollmentDirectionNumber(model));
        }

        List competitionKinds = model.getCompetitionKindModel().findValues(null).getObjects();
        if (!competitionKinds.isEmpty() &&
                (model.getRequestedEnrollmentDirection().getCompetitionKind() == null
                        || !competitionKinds.contains(model.getRequestedEnrollmentDirection().getCompetitionKind())))
            model.getRequestedEnrollmentDirection().setCompetitionKind((CompetitionKind)competitionKinds.get(0));
    }
}
