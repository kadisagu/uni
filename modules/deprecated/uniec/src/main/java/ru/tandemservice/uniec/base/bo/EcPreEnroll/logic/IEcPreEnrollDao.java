/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcPreEnroll.logic;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.base.bo.EcPreEnroll.util.PreEntrantDTO;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

/**
 * @author Vasily Zhukov
 * @since 26.05.2011
 */
public interface IEcPreEnrollDao extends INeedPersistenceSupport
{
    /**
     * Получает список DTO абитуриентов, которые могут быть или уже предзачислены
     *
     * @param parameters параметры
     * @return список DTO абитуриентов, которые могут или уже предзачислены
     */
    List<PreEntrantDTO> getPreEntrantDTOList(EcPreEnrollParam parameters);

    /**
     * Предзачисляет или меняет предзачисление у выбранного направления приема
     *
     * @param requestedEnrollmentDirectionId идентификатор выбранного направления приема
     * @param educationOrgUnit               направление подготовки подразделения, на которое осуществляется предзачисление
     * @param orderType                      тип приказа предзачисления, может быть null, если требуется аннулировать предзачисление
     * @return студент предзачисления, либо null, если
     *         1. было аннулирование предзачисления (orderType == null)
     *         2. тип приказа уже не разрешен по настройке используемых приказов о зачислении
     */
    PreliminaryEnrollmentStudent doChangeOrderType(Long requestedEnrollmentDirectionId, EducationOrgUnit educationOrgUnit, EntrantEnrollmentOrderType orderType);

    Map<Long, Integer> fillIndividualProgressMap(EnrollmentCampaign campaign, Collection<RequestedEnrollmentDirection> elements);
}
