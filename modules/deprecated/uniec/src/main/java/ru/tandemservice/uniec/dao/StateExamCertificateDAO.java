/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate;
import ru.tandemservice.uniec.entity.entrant.StateExamSubjectMark;

/**
 * @author agolubenko
 * @since 11.03.2009
 */
public class StateExamCertificateDAO extends UniBaseDao implements IStateExamCertificateDAO
{
    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public Map<EntrantStateExamCertificate, List<StateExamSubjectMark>> prepareCertificatesSubjectsMarks(List<EntrantStateExamCertificate> certificates)
    {
        Map<EntrantStateExamCertificate, List<StateExamSubjectMark>> result = new HashMap<EntrantStateExamCertificate, List<StateExamSubjectMark>>();
        if (!certificates.isEmpty())
        {
            for (EntrantStateExamCertificate certificate : certificates)
            {
                result.put(certificate, new ArrayList<StateExamSubjectMark>());
            }

            Criteria criteria = getSession().createCriteria(StateExamSubjectMark.class);
            criteria.add(Restrictions.in(StateExamSubjectMark.L_CERTIFICATE, certificates));
            criteria.createAlias(StateExamSubjectMark.L_SUBJECT, "subject");
            criteria.addOrder(Order.asc("subject." + StateExamSubject.P_TITLE));

            for (StateExamSubjectMark mark : (List<StateExamSubjectMark>) criteria.list())
            {
                result.get(mark.getCertificate()).add(mark);
            }
        }
        return result;
    }

    @Override
    public void changeCertificateOriginal(Long id)
    {
        EntrantStateExamCertificate certificate = getNotNull(EntrantStateExamCertificate.class, id);
        certificate.setOriginal(!certificate.isOriginal());
        update(certificate);
    }

    @Override
    public void changeCertificateChecked(Long id)
    {
        EntrantStateExamCertificate certificate = getNotNull(EntrantStateExamCertificate.class, id);
        certificate.setAccepted(!certificate.isAccepted());
        update(certificate);
    }

    @Override
    public void changeCertificateSent(Long id)
    {
        EntrantStateExamCertificate certificate = getNotNull(EntrantStateExamCertificate.class, id);
        certificate.setSent(!certificate.isSent());
        update(certificate);
    }
}
