package ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.block.BlockListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.block.egeData.EgeDataBlock;
import ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.block.entrantData.EntrantDataBlock;
import ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.block.entrantRequestData.EntrantRequestDataBlock;
import ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.block.passDisciplineData.PassDisciplineDataBlock;
import ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.block.preliminaryData.PreliminaryDataBlock;
import ru.tandemservice.uniec.entity.catalog.Base4ExamByDifferentSources;

/**
 * @author Vasily Zhukov
 * @since 08.02.2011
 */
@Configuration
public class EcReportPersonAdd extends BusinessComponentManager
{
    // sub tab panels
    public static final String ENTRANT_TAB_PANEL = "entrantTabPanel";

    // second level tabs
    public static final String ENTRANT_PRINT_TAB = "entrantPrintTab";
    public static final String ENTRANT_DATA_TAB = "entrantDataTab";
    public static final String ENTRANT_REQUEST_DATA_TAB = "entrantRequestDataTab";
    public static final String PRELIMINARY_DATA_TAB = "preliminaryDataTab";

    // tab block lists
    public static final String ENTRANT_DATA_BLOCK_LIST = "entrantDataBlockList";
    public static final String ENTRANT_REQUEST_DATA_BLOCK_LIST = "entrantRequestDataBlockList";
    public static final String PRELIMINARY_DATA_BLOCK_LIST = "preliminaryDataBlockList";

    // block names
    public static final String ENTRANT_DATA = "entrantData";
    public static final String EGE_DATA = "egeData";
    public static final String PASS_DISCIPLINE = "passDisciplineData";
    public static final String ENTRANT_REQUEST_DATA = "entrantRequestData";
    public static final String PRELIMINARY_DATA = "preliminaryData";

    // print tab block lists
    public static final String ENTRANT_SCHEET_BLOCK_LIST = "entrantScheetBlockListExtPoint";
    public static final String ENTRANT_REQUEST_SCHEET_BLOCK_LIST = "entrantRequestScheetBlockListExtPoint";
    public static final String PRE_ENROLL_BLOCK_LIST = "preEnrollScheetBlockListExtPoint";

    // prefetch object name
    public static final String PREFETCH_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String PREFETCH_ENTRANT_DATA_UTIL = "entrantDataUtil";
    public static final String PREFETCH_STATE_EXAM_SUBJECT_MARK_MAP = "stateExamSubjectMarkMap";
    public static final String PREFETCH_PROFILE_KNOWLEDGE_MAP = "profileKnowledgeMap";
    public static final String PREFETCH_PRE_STUDENT_MAP = "preStudentMap";
    public static final String PREFETCH_ENROLLMENT_EXTRACT_MAP = "enrollmentExtractMap";
    public static final String PREFETCH_ENROLLMENT_RECOMMENDATION_MAP = "enrollmentRecommendationMap";
    public static final String PREFETCH_DIRECTION_OLYMPIAD_MAP = "directionOlympiadMap";
    public static final String PREFETCH_ENTRANT_EXAMGROUP_MAP = "entrantExamGroupMap";
    public static final String PREFETCH_ENTRANT_CUSTOMSTATE_MAP = "entrantCustomStateMap";
    public static final String PREFETCH_REQUEST_INDIVIDUAL_PROGRESS_MAP = "requestIndividualProgressMap";


    @Bean
    public TabPanelExtPoint entrantTabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(ENTRANT_TAB_PANEL)
        .addTab(htmlTab(ENTRANT_DATA_TAB, "EntrantDataTab"))
        .addTab(htmlTab(ENTRANT_REQUEST_DATA_TAB, "EntrantRequestDataTab"))
        .addTab(htmlTab(PRELIMINARY_DATA_TAB, "PreliminaryDataTab"))
        .addTab(htmlTab(ENTRANT_PRINT_TAB, "EntrantPrintTab"))
        .create();
    }

    @Bean
    public BlockListExtPoint entrantDataBlockListExtPoint()
    {
        return blockListExtPointBuilder(ENTRANT_DATA_BLOCK_LIST)
        .addBlock(htmlBlock(ENTRANT_DATA, "block/entrantData/EntrantData"))
        .addBlock(htmlBlock(EGE_DATA, "block/egeData/EgeData"))
        .addBlock(htmlBlock(PASS_DISCIPLINE, "block/passDisciplineData/PassDisciplineData"))
        .create();
    }

    @Bean
    public BlockListExtPoint entrantRequestDataBlockListExtPoint()
    {
        return blockListExtPointBuilder(ENTRANT_REQUEST_DATA_BLOCK_LIST)
        .addBlock(htmlBlock(ENTRANT_REQUEST_DATA, "block/entrantRequestData/EntrantRequestData"))
        .create();
    }

    @Bean
    public BlockListExtPoint preliminaryDataBlockListExtPoint()
    {
        return blockListExtPointBuilder(PRELIMINARY_DATA_BLOCK_LIST)
        .addBlock(htmlBlock(PRELIMINARY_DATA, "block/preliminaryData/PreliminaryData"))
        .create();
    }

    @Bean
    public BlockListExtPoint entrantScheetBlockListExtPoint()
    {
        return blockListExtPointBuilder(ENTRANT_SCHEET_BLOCK_LIST)
        .addBlock(htmlBlock(ENTRANT_SCHEET_BLOCK_LIST, "print/entrant/Template"))
        .addBlock(htmlBlock(ENTRANT_REQUEST_SCHEET_BLOCK_LIST, "print/request/Template"))
        .addBlock(htmlBlock(PRE_ENROLL_BLOCK_LIST, "print/preenroll/Template"))
        .create();
    }

    // EntrantDataBlock

    @Bean
    public IDefaultComboDataSourceHandler entrantDSHandler()
    {
        return EntrantDataBlock.createEntrantDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler enrollmentCampaignDSHandler()
    {
        return EntrantDataBlock.createEnrollmentCampaignDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler hasOlympiadDiplomaDSHandler()
    {
        return EntrantDataBlock.createHasOlympiadDiplomaDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler recomendationsDSHandler()
    {
        return EntrantDataBlock.createRecomendationsDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler accessCoursesDSHandler()
    {
        return EntrantDataBlock.createAccessCoursesDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler passProfileEducationDSHandler()
    {
        return EntrantDataBlock.createPassProfileEducationDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler hasExamResultDSHandler()
    {
        return EntrantDataBlock.createHasExamResultDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler infoSourceDSHandler()
    {
        return EntrantDataBlock.createInfoSourceDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler archivalDSHandler()
    {
        return EntrantDataBlock.createEntrantArchivalDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler hasIndividualProgressHandler()
    {
        return EntrantDataBlock.createEntrantHasIndividualProgressDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler individualProgressHandler()
    {
        return EntrantDataBlock.createEntrantIndividualProgressDS(getName());
    }

    // EgeData

    @Bean
    public IDefaultComboDataSourceHandler stateExamInternalDSHandler()
    {
        return EgeDataBlock.createStateExamInternalDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler hasStateExamCertificateDSHandler()
    {
        return EgeDataBlock.createHasStateExamCertificateDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler stateExamTypeDSHandler()
    {
        return EgeDataBlock.createStateExamTypeDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler certificateIssuanceYearDSHandler()
    {
        return EgeDataBlock.createCertificateIssuanceYearDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler stateExamSubjectDSHandler()
    {
        return EgeDataBlock.createStateExamSubjectDS(getName());
    }

    // PassDisciplineData

    @Bean
    public IDefaultComboDataSourceHandler educationSubjectDSHandler()
    {
        return PassDisciplineDataBlock.createEducationSubjectDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler subjectPassFormDS()
    {
        return PassDisciplineDataBlock.createSubjectPassFormDS(getName());
    }

    // EntrantRequestData

    @Bean
    public IDefaultComboDataSourceHandler qualificationDSHandler()
    {
        return EntrantRequestDataBlock.createQualificationDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler formativeOrgUnitDSHandler()
    {
        return EntrantRequestDataBlock.createFormativeOrgUnitDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler territorialOrgUnitDSHandler()
    {
        return EntrantRequestDataBlock.createTerritorialOrgUnitDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler educationOrgUnitDSHandler()
    {
        return EntrantRequestDataBlock.createEducationOrgUnitDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler competitionKindDSHandler()
    {
        return EntrantRequestDataBlock.createCompetitionKindDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler compensationTypeDSHandler()
    {
        return EntrantRequestDataBlock.createCompensationTypeDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler studentCategoryDSHandler()
    {
        return EntrantRequestDataBlock.createStudentCategoryDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler targetAdmissionDSHandler()
    {
        return EntrantRequestDataBlock.createTargetAdmissionDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler targetAdmissionKindDSHandler()
    {
        return EntrantRequestDataBlock.createTargetAdmissionKindDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler entrantStateDSHandler()
    {
        return EntrantRequestDataBlock.createEntrantStateDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler originalDocumentsDSHandler()
    {
        return EntrantRequestDataBlock.createOriginalDocumentsDS(getName());
    }

    // PreliminaryData

    @Bean
    public IDefaultComboDataSourceHandler qualificationPreDSHandler()
    {
        return PreliminaryDataBlock.createQualificationPreDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler formativeOrgUnitPreDSHandler()
    {
        return PreliminaryDataBlock.createFormativeOrgUnitPreDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler territorialOrgUnitPreDSHandler()
    {
        return PreliminaryDataBlock.createTerritorialOrgUnitPreDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler educationOrgUnitPreDSHandler()
    {
        return PreliminaryDataBlock.createEducationOrgUnitPreDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler compensationTypePreDSHandler()
    {
        return PreliminaryDataBlock.createCompensationTypePreDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler studentCategoryPreDSHandler()
    {
        return PreliminaryDataBlock.createStudentCategoryPreDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler targetAdmissionPreDSHandler()
    {
        return PreliminaryDataBlock.createTargetAdmissionPreDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler parallelPreDSHandler()
    {
        return PreliminaryDataBlock.createParallelPreDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler entrantCustomStateExistDSHandler()
    {
        return EntrantDataBlock.createEntrantCustomStateExistDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler entrantCustomStateListDSHandler()
    {
        return EntrantDataBlock.createEntrantCustomStateListDS(getName());
    }


    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()

        .addDataSource(selectDS(EntrantDataBlock.ENTRANT_DS, entrantDSHandler()))
        .addDataSource(selectDS(EntrantDataBlock.ENROLLMENT_CAMPAIGN_DS, enrollmentCampaignDSHandler()))
        .addDataSource(selectDS(EntrantDataBlock.HAS_OLYMPIAD_DIPLOMA_DS, hasOlympiadDiplomaDSHandler()))
        .addDataSource(selectDS(EntrantDataBlock.RECOMENDATIONS_DS, recomendationsDSHandler()))
        .addDataSource(selectDS(EntrantDataBlock.ACCESS_COURSES_DS, accessCoursesDSHandler()))
        .addDataSource(selectDS(EntrantDataBlock.PASS_PROFILE_EDUCATION_DS, passProfileEducationDSHandler()))
        .addDataSource(selectDS(EntrantDataBlock.HAS_EXAM_RESULT_DS, hasExamResultDSHandler()))
        .addDataSource(selectDS(EntrantDataBlock.INFO_SOURCE_DS, infoSourceDSHandler()))
        .addDataSource(selectDS(EntrantDataBlock.ENTRANT_ARCHIVAL_DS, archivalDSHandler()))
                .addDataSource(selectDS(EntrantDataBlock.ENTRANT_CUSTOM_STATE_EXIST_DS, entrantCustomStateExistDSHandler()))
                .addDataSource(selectDS(EntrantDataBlock.ENTRANT_CUSTOM_STATE_LIST_DS, entrantCustomStateListDSHandler()))
                .addDataSource(selectDS(EntrantDataBlock.ENTRANT_HAS_INDIVIDUAL_PROGRESS_DS, hasIndividualProgressHandler()))
                .addDataSource(selectDS(EntrantDataBlock.ENTRANT_INDIVIDUAL_PROGRESS_DS, individualProgressHandler()))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(EntrantDataBlock.BASE_4_EXAM_BY_DIFFERENT_SOURCES_DS, getName(),
                                                                         Base4ExamByDifferentSources.defaultSelectDSHandler(getName())))

        .addDataSource(selectDS(EgeDataBlock.STATE_EXAM_INTERNAL_DS, stateExamInternalDSHandler()))
        .addDataSource(selectDS(EgeDataBlock.HAS_STATE_EXAM_CERTIFICATE_DS, hasStateExamCertificateDSHandler()))
        .addDataSource(selectDS(EgeDataBlock.STATE_EXAM_TYPE_DS, stateExamTypeDSHandler()))
        .addDataSource(selectDS(EgeDataBlock.CERTIFICATE_ISSUANCE_YEAR_DS, certificateIssuanceYearDSHandler()))
        .addDataSource(selectDS(EgeDataBlock.STATE_EXAM_SUBJECT_DS, stateExamSubjectDSHandler()))

        .addDataSource(selectDS(PassDisciplineDataBlock.EDUCATION_SUBJECT_DS, educationSubjectDSHandler()))
        .addDataSource(selectDS(PassDisciplineDataBlock.SUBJECT_PASS_FORM_DS, subjectPassFormDS()))

        .addDataSource(selectDS(EntrantRequestDataBlock.QUALIFICATION_DS, qualificationDSHandler()))
        .addDataSource(selectDS(EntrantRequestDataBlock.FORMATIVE_ORGUNIT_DS, formativeOrgUnitDSHandler()))
        .addDataSource(this.selectDS(EntrantRequestDataBlock.TERRITORIAL_ORGUNIT_DS, territorialOrgUnitDSHandler()).addColumn(OrgUnit.territorialTitle().s()))
        .addDataSource(this.selectDS(EntrantRequestDataBlock.EDUCATION_ORGUNIT_DS, educationOrgUnitDSHandler()).addColumn(EducationLevelsHighSchool.displayableTitle().s()))
        .addDataSource(EducationCatalogsManager.instance().developFormDSConfig())
        .addDataSource(EducationCatalogsManager.instance().developConditionDSConfig())
        .addDataSource(EducationCatalogsManager.instance().developTechDSConfig())
        .addDataSource(EducationCatalogsManager.instance().developPeriodDSConfig())
        .addDataSource(selectDS(EntrantRequestDataBlock.COMPETITION_KIND_DS, competitionKindDSHandler()))
        .addDataSource(this.selectDS(EntrantRequestDataBlock.COMPENSATION_TYPE_DS, compensationTypeDSHandler()).addColumn(CompensationType.shortTitle().s()))
        .addDataSource(selectDS(EntrantRequestDataBlock.STUDENT_CATEGORY_DS, studentCategoryDSHandler()))
        .addDataSource(selectDS(EntrantRequestDataBlock.TARGET_ADMISSION_DS, targetAdmissionDSHandler()))
        .addDataSource(selectDS(EntrantRequestDataBlock.TARGET_ADMISSION_KIND_DS, targetAdmissionKindDSHandler()))
        .addDataSource(selectDS(EntrantRequestDataBlock.ENTRANT_STATE_DS, entrantStateDSHandler()))
        .addDataSource(selectDS(EntrantRequestDataBlock.ORIGINAL_DOCUMENTS_DS, originalDocumentsDSHandler()))

        .addDataSource(selectDS(PreliminaryDataBlock.QUALIFICATION_PRE_DS, qualificationPreDSHandler()))
        .addDataSource(selectDS(PreliminaryDataBlock.FORMATIVE_ORGUNIT_PRE_DS, formativeOrgUnitPreDSHandler()))
        .addDataSource(this.selectDS(PreliminaryDataBlock.TERRITORIAL_ORGUNIT_PRE_DS, territorialOrgUnitPreDSHandler()).addColumn(OrgUnit.territorialTitle().s()))
        .addDataSource(this.selectDS(PreliminaryDataBlock.EDUCATION_ORGUNIT_PRE_DS, educationOrgUnitPreDSHandler()).addColumn(EducationLevelsHighSchool.displayableTitle().s()))
        .addDataSource(selectDS(PreliminaryDataBlock.DEVELOP_FORM_PRE_DS, EducationCatalogsManager.instance().developFormDSHandler()))
        .addDataSource(selectDS(PreliminaryDataBlock.DEVELOP_CONDITION_PRE_DS, EducationCatalogsManager.instance().developConditionDSHandler()))
        .addDataSource(selectDS(PreliminaryDataBlock.DEVELOP_TECH_PRE_DS, EducationCatalogsManager.instance().developTechDSHandler()))
        .addDataSource(selectDS(PreliminaryDataBlock.DEVELOP_PERIOD_PRE_DS, EducationCatalogsManager.instance().developPeriodDSHandler()))
        .addDataSource(this.selectDS(PreliminaryDataBlock.COMPENSATION_TYPE_PRE_DS, compensationTypePreDSHandler()).addColumn(CompensationType.shortTitle().s()))
        .addDataSource(selectDS(PreliminaryDataBlock.STUDENT_CATEGORY_PRE_DS, studentCategoryPreDSHandler()))
        .addDataSource(selectDS(PreliminaryDataBlock.TARGET_ADMISSION_PRE_DS, targetAdmissionPreDSHandler()))
        .addDataSource(selectDS(PreliminaryDataBlock.PARALLEL_PRE_DS, parallelPreDSHandler()))
                .addDataSource(CommonManager.instance().yesNoDSConfig())

        .create();
    }
}
