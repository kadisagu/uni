/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.settings.DisciplinesGroupList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.MultiValuesColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.IUniecComponents;
import ru.tandemservice.uniec.entity.settings.DisciplinesGroup;

/**
 * @author agolubenko
 * @since 17.06.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.setSettings(component.getSettings());

        getDao().prepare(model);

        prepareListDataSource(component);
    }

    @SuppressWarnings({"unchecked"})
    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null)
        {
            return;
        }

        DynamicListDataSource<DisciplinesGroup> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Название", DisciplinesGroup.P_TITLE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Сокращенное название", DisciplinesGroup.P_SHORT_TITLE).setClickable(false));
        dataSource.addColumn(new MultiValuesColumn("Дисциплины", DisciplinesGroup.DISCIPLINES).setFormatter(CollectionFormatter.COLLECTION_FORMATTER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Редактирование", ActionColumn.EDIT, "onClickEditDisciplineGroup"));
        dataSource.addColumn(new ActionColumn("Удаление", ActionColumn.DELETE, "onClickDeleteDisciplineGroup", "Удалить группу дисциплин «{0}»?", DisciplinesGroup.title().s()));
        model.setDataSource(dataSource);
    }

    public void onClickEditDisciplineGroup(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniecComponents.DISCIPLINES_GROUP_ADD_EDIT, new ParametersMap()
                .add("groupId", component.getListenerParameter())
                .add("enrollmentCampaignId", null)
        ));
    }

    public void onClickAddDisciplineGroup(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniecComponents.DISCIPLINES_GROUP_ADD_EDIT, new ParametersMap()
                .add("groupId", null)
                .add("enrollmentCampaignId", getModel(component).getEnrollmentCampaign().getId())
        ));
    }

    public void onClickDeleteDisciplineGroup(IBusinessComponent component)
    {
        getDao().deleteRow(component);
    }

    public void onRefresh(IBusinessComponent component)
    {
        component.saveSettings();
    }
}
