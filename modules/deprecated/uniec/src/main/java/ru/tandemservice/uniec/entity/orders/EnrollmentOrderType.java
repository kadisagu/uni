package ru.tandemservice.uniec.entity.orders;

import org.tandemframework.core.common.ITitled;
import ru.tandemservice.uniec.entity.orders.gen.EnrollmentOrderTypeGen;

/**
 * Тип приказа в приемной кампании
 */
public class EnrollmentOrderType extends EnrollmentOrderTypeGen implements ITitled
{
    @Override
    public String getTitle()
    {
        return getEntrantEnrollmentOrderType().getTitle();
    }
}