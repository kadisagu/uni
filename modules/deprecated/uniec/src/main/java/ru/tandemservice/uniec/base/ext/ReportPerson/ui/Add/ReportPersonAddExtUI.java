/* $Id$ */
package ru.tandemservice.uniec.base.ext.ReportPerson.ui.Add;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.person.base.bo.ReportPerson.ui.Add.ReportPersonAddUI;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportDQLModifierOwner;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportPrintBlock;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportPrintInfo;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.ReportDQL;
import ru.tandemservice.uniec.base.ext.ReportPerson.ui.Add.print.education.EducationPrintBlockExt;

/**
 * @author Andrey Andreev
 * @since 09.08.2016
 */
public class ReportPersonAddExtUI extends UIAddon  implements IReportDQLModifierOwner
{
    private IReportPrintBlock _educationPrintBlockExt = new EducationPrintBlockExt();

    public ReportPersonAddExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {
        ((ReportPersonAddUI) _presenter).validateReportParams(errorCollector);
    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        _educationPrintBlockExt.modify(dql, printInfo);
    }

    public IReportPrintBlock getEducationPrintBlockExt()
    {
        return _educationPrintBlockExt;
    }

    public void setEducationPrintBlockExt(IReportPrintBlock educationPrintBlockExt)
    {
        _educationPrintBlockExt = educationPrintBlockExt;
    }
}