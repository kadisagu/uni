/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.entity.examset;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.tool.IdGen;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineKind;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineType;
import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline;
import ru.tandemservice.uniec.entity.settings.SetDiscipline;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Элемент набора экзаменов
 * <p/>
 * Обертка над EntranceDiscipline
 * для удобного использования в бизнес логике
 *
 * @author vip_delete
 * @since 03.02.2009
 */
public class ExamSetItem extends IdentifiableWrapper implements IExamSetItem
{
    private static final long serialVersionUID = -1647299120181791464L;
    public static final String P_TITLE = "title";
    public static final String P_BUDGET = "budget";
    public static final String P_CONTRACT = "contract";
    public static final String P_KIND = "kind";
    public static final String P_TYPE = "type";
    public static final String P_CHOICE_TITLE = "choiceTitle";

    private String _examSetItemId; // идентификатор дисциплины из набора
    private boolean _budget; // есть ли этот предмет для бюджетников
    private boolean _contract; // есть ли этот предмет для контрактников
    private EntranceDisciplineKind _kind; // профильный предмет?
    private EntranceDisciplineType _type; // тип ВИ
    private SetDiscipline _subject; // основной предмет
    private List<SetDiscipline> _choice; // предметы по выбору
    private String _choiceTitle; // названия дисциплин по выбору
    private List<EntranceDiscipline> _list; // список вступительных испытаний соотв. указанному элементу набора

    @SuppressWarnings( { "unchecked" })
    public ExamSetItem(Long id, boolean budget, boolean contract, EntranceDisciplineKind kind, EntranceDisciplineType type, SetDiscipline subject, List<SetDiscipline> choice)
    {
        super(id, subject.getTitle());
        _budget = budget;
        _contract = contract;
        _kind = kind;
        _type = type;
        _subject = subject;
        _choice = choice;
        _choiceTitle = CollectionFormatter.COLLECTION_FORMATTER.format(_choice);
        _list = new ArrayList<EntranceDiscipline>();
        _examSetItemId = prepareExamSetItemId();
    }

    private String uid(SetDiscipline dsc) {
        return Long.toString(dsc.getId() >> IdGen.CODE_BITS, 32);
    }

    private String prepareExamSetItemId()
    {
        final String prefix = uid(_subject) + ";" + ((_budget ? "1" : "0") + (_contract ? "1" : "0")) + ";" + _kind.getCode();
        if (_choice.isEmpty()) { return prefix; }

        List<String> ids = new ArrayList<String>(_choice.size());
        for (SetDiscipline discipline : _choice) {
            ids.add(uid(discipline));
        }
        Collections.sort(ids); // важно их сортировать (строка должна быть одинаковая для одинакового набора)
        return (prefix + ";" + StringUtils.join(ids.iterator(), ';'));
    }

    public boolean isProfile()
    {
        return getKind().getCode().equals(UniecDefines.ENTRANCE_DISCIPLINE_KIND_PROFILE);
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof ExamSetItem)
        {
            return _examSetItemId.equals(((ExamSetItem) obj).getExamSetItemId());
        }
        return false;
    }

    @Override
    public int hashCode()
    {
        return _examSetItemId.hashCode();
    }

    // Getters & Setters

    public List<EntranceDiscipline> getList()
    {
        return _list;
    }

    public void setList(List<EntranceDiscipline> list)
    {
        _list = list;
    }

    // Getters

    public String getExamSetItemId()
    {
        return _examSetItemId;
    }

    public boolean isBudget()
    {
        return _budget;
    }

    public boolean isContract()
    {
        return _contract;
    }

    @Override
    public EntranceDisciplineKind getKind()
    {
        return _kind;
    }

    @Override
    public EntranceDisciplineType getType()
    {
        return _type;
    }

    public void setType(EntranceDisciplineType type)
    {
        _type = type;
    }

    public SetDiscipline getSubject()
    {
        return _subject;
    }

    public List<SetDiscipline> getChoice()
    {
        return _choice;
    }

    @Override
    public String getChoiceTitle()
    {
        return _choiceTitle;
    }
}
