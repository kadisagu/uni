/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantRequestTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

import java.util.*;

/**
 * @author agolubenko
 * @since 16.05.2008
 */
@State({@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entrant.id")})
public class Model
{
    private Entrant _entrant = new Entrant();
    private List<EntrantRequest> _entrantRequestList;
    private boolean _allowTakeAwayDocuments;
    private boolean _oneDirectionPerRequest;
    private boolean _hasEmptyPriorityProfileEduOu;
    private EntrantRequest _currentEntrantRequest;
    private Map<EntrantRequest, Boolean> _originalHandedInByRequest = new HashMap<>();
    private Map<EntrantRequest, DynamicListDataSource<RequestedEnrollmentDirection>> _requestedEnrollmentDirectionDataSourceByRequest = new HashMap<>();
    private Set<RequestedEnrollmentDirection> _nonActualDirectionSet = new HashSet<>();
    private String _updateOnChangeColumns;

    public String getRequestRegDate()
    {
        return DateFormatter.DATE_FORMATTER_WITH_TIME.format(_currentEntrantRequest.getRegDate());
    }

    public boolean isRequestEnrollmentDirectionAdditionDisabled()
    {
        return !isAccessible() || (isOneDirectionPerRequest() && !getRequestedEnrollmentDirectionDataSourceByRequest().get(getCurrentEntrantRequest()).getEntityList().isEmpty());
    }

    public boolean isAccessible()
    {
        return !getEntrant().isArchival();
    }

    public boolean isCurrentEntrantRequestHasOriginals()
    {
        return Boolean.TRUE.equals(_originalHandedInByRequest.get(_currentEntrantRequest));
    }

    public boolean isFirstEntrantRequest()
    {
        return _entrantRequestList != null && !_entrantRequestList.isEmpty() && _entrantRequestList.get(0).equals(_currentEntrantRequest);
    }
    
    // Getters & Setters

    public Entrant getEntrant()
    {
        return _entrant;
    }

    public void setEntrant(Entrant entrant)
    {
        _entrant = entrant;
    }

    public List<EntrantRequest> getEntrantRequestList()
    {
        return _entrantRequestList;
    }

    public void setEntrantRequestList(List<EntrantRequest> entrantRequestList)
    {
        _entrantRequestList = entrantRequestList;
    }

    public boolean isAllowTakeAwayDocuments()
    {
        return _allowTakeAwayDocuments;
    }

    public void setAllowTakeAwayDocuments(boolean allowTakeAwayDocuments)
    {
        _allowTakeAwayDocuments = allowTakeAwayDocuments;
    }

    public EntrantRequest getCurrentEntrantRequest()
    {
        return _currentEntrantRequest;
    }

    public void setCurrentEntrantRequest(EntrantRequest currentEntrantRequest)
    {
        _currentEntrantRequest = currentEntrantRequest;
    }

    public Map<EntrantRequest, Boolean> getOriginalHandedInByRequest()
    {
        return _originalHandedInByRequest;
    }

    public void setOriginalHandedInByRequest(Map<EntrantRequest, Boolean> originalHandedInByRequest)
    {
        _originalHandedInByRequest = originalHandedInByRequest;
    }

    public Map<EntrantRequest, DynamicListDataSource<RequestedEnrollmentDirection>> getRequestedEnrollmentDirectionDataSourceByRequest()
    {
        return _requestedEnrollmentDirectionDataSourceByRequest;
    }

    public void setRequestedEnrollmentDirectionDataSourceByRequest(Map<EntrantRequest, DynamicListDataSource<RequestedEnrollmentDirection>> requestedEnrollmentDirectionDataSourceByRequest)
    {
        _requestedEnrollmentDirectionDataSourceByRequest = requestedEnrollmentDirectionDataSourceByRequest;
    }

    public Set<RequestedEnrollmentDirection> getNonActualDirectionSet()
    {
        return _nonActualDirectionSet;
    }

    public void setNonActualDirectionSet(Set<RequestedEnrollmentDirection> nonActualDirectionSet)
    {
        _nonActualDirectionSet = nonActualDirectionSet;
    }

    public boolean isOneDirectionPerRequest()
    {
        return _oneDirectionPerRequest;
    }

    public void setOneDirectionPerRequest(boolean oneDirectionPerRequest)
    {
        _oneDirectionPerRequest = oneDirectionPerRequest;
    }

    public boolean isHasEmptyPriorityProfileEduOu()
    {
        return _hasEmptyPriorityProfileEduOu;
    }

    public void setHasEmptyPriorityProfileEduOu(boolean hasEmptyPriorityProfileEduOu)
    {
        _hasEmptyPriorityProfileEduOu = hasEmptyPriorityProfileEduOu;
    }

    public String getUpdateOnChangeColumns()
    {
        return _updateOnChangeColumns;
    }

    public void setUpdateOnChangeColumns(String updateOnChangeColumns)
    {
        _updateOnChangeColumns = updateOnChangeColumns;
    }
}