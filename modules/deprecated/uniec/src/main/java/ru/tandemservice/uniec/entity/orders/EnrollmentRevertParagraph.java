package ru.tandemservice.uniec.entity.orders;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniec.entity.orders.gen.*;
import ru.tandemservice.unimove.IAbstractExtract;

import java.util.List;

/**
 * Параграф приказа об изменении приказа о зачислении
 */
public class EnrollmentRevertParagraph extends EnrollmentRevertParagraphGen implements ITitled
{
    @Override
    @EntityDSLSupport
    public String getTitle()
    {
        return "Параграф №" + getNumber();
    }

    @Override
    public List<? extends IAbstractExtract> getExtractList()
    {
        return UniDaoFacade.getCoreDao().getList(EnrollmentRevertExtract.class, IAbstractExtract.L_PARAGRAPH, this, IAbstractExtract.P_NUMBER);
    }

    @Override
    public int getExtractCount()
    {
        return UniDaoFacade.getCoreDao().getCount(EnrollmentRevertExtract.class, IAbstractExtract.L_PARAGRAPH, this);
    }
}