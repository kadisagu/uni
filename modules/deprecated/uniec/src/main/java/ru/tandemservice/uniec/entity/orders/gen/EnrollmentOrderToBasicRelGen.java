package ru.tandemservice.uniec.entity.orders.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.catalog.EnrollmentOrderBasic;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrderToBasicRel;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь приказа о зачислении с основанием. В приказе о зачислении есть список оснований
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrollmentOrderToBasicRelGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.orders.EnrollmentOrderToBasicRel";
    public static final String ENTITY_NAME = "enrollmentOrderToBasicRel";
    public static final int VERSION_HASH = 430602862;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORDER = "order";
    public static final String L_BASIC = "basic";

    private EnrollmentOrder _order;     // Прика о зачислении
    private EnrollmentOrderBasic _basic;     // Основание приказа о зачислении

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Прика о зачислении.
     */
    public EnrollmentOrder getOrder()
    {
        return _order;
    }

    /**
     * @param order Прика о зачислении.
     */
    public void setOrder(EnrollmentOrder order)
    {
        dirty(_order, order);
        _order = order;
    }

    /**
     * @return Основание приказа о зачислении.
     */
    public EnrollmentOrderBasic getBasic()
    {
        return _basic;
    }

    /**
     * @param basic Основание приказа о зачислении.
     */
    public void setBasic(EnrollmentOrderBasic basic)
    {
        dirty(_basic, basic);
        _basic = basic;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrollmentOrderToBasicRelGen)
        {
            setOrder(((EnrollmentOrderToBasicRel)another).getOrder());
            setBasic(((EnrollmentOrderToBasicRel)another).getBasic());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrollmentOrderToBasicRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrollmentOrderToBasicRel.class;
        }

        public T newInstance()
        {
            return (T) new EnrollmentOrderToBasicRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "order":
                    return obj.getOrder();
                case "basic":
                    return obj.getBasic();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "order":
                    obj.setOrder((EnrollmentOrder) value);
                    return;
                case "basic":
                    obj.setBasic((EnrollmentOrderBasic) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "order":
                        return true;
                case "basic":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "order":
                    return true;
                case "basic":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "order":
                    return EnrollmentOrder.class;
                case "basic":
                    return EnrollmentOrderBasic.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrollmentOrderToBasicRel> _dslPath = new Path<EnrollmentOrderToBasicRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrollmentOrderToBasicRel");
    }
            

    /**
     * @return Прика о зачислении.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrderToBasicRel#getOrder()
     */
    public static EnrollmentOrder.Path<EnrollmentOrder> order()
    {
        return _dslPath.order();
    }

    /**
     * @return Основание приказа о зачислении.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrderToBasicRel#getBasic()
     */
    public static EnrollmentOrderBasic.Path<EnrollmentOrderBasic> basic()
    {
        return _dslPath.basic();
    }

    public static class Path<E extends EnrollmentOrderToBasicRel> extends EntityPath<E>
    {
        private EnrollmentOrder.Path<EnrollmentOrder> _order;
        private EnrollmentOrderBasic.Path<EnrollmentOrderBasic> _basic;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Прика о зачислении.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrderToBasicRel#getOrder()
     */
        public EnrollmentOrder.Path<EnrollmentOrder> order()
        {
            if(_order == null )
                _order = new EnrollmentOrder.Path<EnrollmentOrder>(L_ORDER, this);
            return _order;
        }

    /**
     * @return Основание приказа о зачислении.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrderToBasicRel#getBasic()
     */
        public EnrollmentOrderBasic.Path<EnrollmentOrderBasic> basic()
        {
            if(_basic == null )
                _basic = new EnrollmentOrderBasic.Path<EnrollmentOrderBasic>(L_BASIC, this);
            return _basic;
        }

        public Class getEntityClass()
        {
            return EnrollmentOrderToBasicRel.class;
        }

        public String getEntityName()
        {
            return "enrollmentOrderToBasicRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
