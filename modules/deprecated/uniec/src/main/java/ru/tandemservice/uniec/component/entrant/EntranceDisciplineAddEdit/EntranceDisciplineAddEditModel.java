/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntranceDisciplineAddEdit;

import org.springframework.context.MessageSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineKind;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline;
import ru.tandemservice.uniec.entity.settings.SetDiscipline;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Боба
 * @since 01.08.2008
 */
@Input( { @Bind(key = EntranceDisciplineAddEditModel.ENTRANCE_DISCIPLINE_ID, binding = "entranceDiscipline.id"), @Bind(key = EntranceDisciplineAddEditModel.ENROLLMENT_DIRECTION_ID, binding = "entranceDiscipline.enrollmentDirection.id") })
public class EntranceDisciplineAddEditModel
{
    public static final String ENTRANCE_DISCIPLINE_ID = "entranceDisciplineId";
    public static final String ENROLLMENT_DIRECTION_ID = "enrollmentDirectionId";

    private EntranceDiscipline _entranceDiscipline = new EntranceDiscipline();
    {
        _entranceDiscipline.setEnrollmentDirection(new EnrollmentDirection());
    }

    private List<SetDiscipline> _disciplinesList; // список дисциплин. выбранная дисциплина записывается сразу в entranceDiscipline
    private List<EntranceDisciplineKind> _kindList;
    private List<EntranceDisciplineType> _typeList;
    private IMultiSelectModel _disciplinesSelectModel;
    private List<SetDiscipline> _choiceDisciplinesList = new ArrayList<SetDiscipline>();
    private List<StudentCategory> _studentCategoryList;

    private MessageSource _messageSource;

    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return getEntranceDiscipline().getEnrollmentDirection().getEnrollmentCampaign();
    }

    // Getters & Setters

    public List<EntranceDisciplineType> getTypeList()
    {
        return _typeList;
    }

    public void setTypeList(List<EntranceDisciplineType> typeList)
    {
        _typeList = typeList;
    }

    public EntranceDiscipline getEntranceDiscipline()
    {
        return _entranceDiscipline;
    }

    public void setEntranceDiscipline(EntranceDiscipline entranceDiscipline)
    {
        _entranceDiscipline = entranceDiscipline;
    }

    public List<SetDiscipline> getDisciplinesList()
    {
        return _disciplinesList;
    }

    public void setDisciplinesList(List<SetDiscipline> disciplinesList)
    {
        _disciplinesList = disciplinesList;
    }

    public List<EntranceDisciplineKind> getKindList()
    {
        return _kindList;
    }

    public void setKindList(List<EntranceDisciplineKind> kindList)
    {
        _kindList = kindList;
    }

    public IMultiSelectModel getDisciplinesSelectModel()
    {
        return _disciplinesSelectModel;
    }

    public void setDisciplinesSelectModel(IMultiSelectModel disciplinesSelectModel)
    {
        _disciplinesSelectModel = disciplinesSelectModel;
    }

    public List<SetDiscipline> getChoiceDisciplinesList()
    {
        return _choiceDisciplinesList;
    }

    public void setChoiceDisciplinesList(List<SetDiscipline> choiceDisciplinesList)
    {
        _choiceDisciplinesList = choiceDisciplinesList;
    }

    public List<StudentCategory> getStudentCategoryList()
    {
        return _studentCategoryList;
    }

    public void setStudentCategoryList(List<StudentCategory> studentCategoryList)
    {
        _studentCategoryList = studentCategoryList;
    }

    public MessageSource getMessageSource()
    {
        return _messageSource;
    }

    public void setMessageSource(MessageSource messageSource)
    {
        _messageSource = messageSource;
    }
}