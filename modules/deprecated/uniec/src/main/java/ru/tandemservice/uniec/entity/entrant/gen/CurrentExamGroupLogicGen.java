package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.catalog.ExamGroupLogic;
import ru.tandemservice.uniec.entity.entrant.CurrentExamGroupLogic;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Текущий алгоритм работы экзаменационных групп
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class CurrentExamGroupLogicGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.CurrentExamGroupLogic";
    public static final String ENTITY_NAME = "currentExamGroupLogic";
    public static final int VERSION_HASH = -1835774610;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_EXAM_GROUP_LOGIC = "examGroupLogic";

    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private ExamGroupLogic _examGroupLogic;     // Алгоритм работы экзаменационных групп

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null и должно быть уникальным.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Алгоритм работы экзаменационных групп. Свойство не может быть null.
     */
    @NotNull
    public ExamGroupLogic getExamGroupLogic()
    {
        return _examGroupLogic;
    }

    /**
     * @param examGroupLogic Алгоритм работы экзаменационных групп. Свойство не может быть null.
     */
    public void setExamGroupLogic(ExamGroupLogic examGroupLogic)
    {
        dirty(_examGroupLogic, examGroupLogic);
        _examGroupLogic = examGroupLogic;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof CurrentExamGroupLogicGen)
        {
            setEnrollmentCampaign(((CurrentExamGroupLogic)another).getEnrollmentCampaign());
            setExamGroupLogic(((CurrentExamGroupLogic)another).getExamGroupLogic());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends CurrentExamGroupLogicGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) CurrentExamGroupLogic.class;
        }

        public T newInstance()
        {
            return (T) new CurrentExamGroupLogic();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "examGroupLogic":
                    return obj.getExamGroupLogic();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "examGroupLogic":
                    obj.setExamGroupLogic((ExamGroupLogic) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "examGroupLogic":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "examGroupLogic":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "examGroupLogic":
                    return ExamGroupLogic.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<CurrentExamGroupLogic> _dslPath = new Path<CurrentExamGroupLogic>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "CurrentExamGroupLogic");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.entrant.CurrentExamGroupLogic#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Алгоритм работы экзаменационных групп. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.CurrentExamGroupLogic#getExamGroupLogic()
     */
    public static ExamGroupLogic.Path<ExamGroupLogic> examGroupLogic()
    {
        return _dslPath.examGroupLogic();
    }

    public static class Path<E extends CurrentExamGroupLogic> extends EntityPath<E>
    {
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private ExamGroupLogic.Path<ExamGroupLogic> _examGroupLogic;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.entrant.CurrentExamGroupLogic#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Алгоритм работы экзаменационных групп. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.CurrentExamGroupLogic#getExamGroupLogic()
     */
        public ExamGroupLogic.Path<ExamGroupLogic> examGroupLogic()
        {
            if(_examGroupLogic == null )
                _examGroupLogic = new ExamGroupLogic.Path<ExamGroupLogic>(L_EXAM_GROUP_LOGIC, this);
            return _examGroupLogic;
        }

        public Class getEntityClass()
        {
            return CurrentExamGroupLogic.class;
        }

        public String getEntityName()
        {
            return "currentExamGroupLogic";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
