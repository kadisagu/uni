package ru.tandemservice.uniec.entity.entrant;

import org.tandemframework.core.common.ITitled;

import ru.tandemservice.uniec.entity.entrant.gen.EntrantAccessCourseGen;

public class EntrantAccessCourse extends EntrantAccessCourseGen implements ITitled
{
    public static final String P_TITLE = "title";

    @Override
    public String getTitle()
    {
        return getCourse().getTitle();
    }
}