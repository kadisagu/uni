/* $Id: $ */
package ru.tandemservice.uniec.base.ext.OrgUnitReport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.OrgUnitReportManager;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.logic.OrgUnitReportBlockDefinition;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.logic.OrgUnitReportDefinition;
import ru.tandemservice.uni.component.reports.FormativeAndTerritorialOrgUnitReportVisibleResolver;


/**
 * @author Andrey Andreev
 * @since 13.04.2017
 */
@Configuration
public class OrgUnitReportExtManager extends BusinessObjectExtensionManager
{


    public static final String UNIEC_ORGUNIT_REPORT_BLOCK = "uniecOrgUnitReportBlock";


    @Autowired
    private OrgUnitReportManager _orgUnitReportManager;


    @Bean
    public ItemListExtension<OrgUnitReportBlockDefinition> blockListExtension()
    {

        return itemListExtension(_orgUnitReportManager.blockListExtPoint())
                .add(UNIEC_ORGUNIT_REPORT_BLOCK, new OrgUnitReportBlockDefinition("Отчеты модуля «(Старый) Абитуриенты»", UNIEC_ORGUNIT_REPORT_BLOCK, new FormativeAndTerritorialOrgUnitReportVisibleResolver()))
                .create();
    }

    @Bean
    public ItemListExtension<OrgUnitReportDefinition> reportListExtension()
    {
        return itemListExtension(_orgUnitReportManager.reportListExtPoint())
                .add("viewEntranceExaminationMeetingReportList", new OrgUnitReportDefinition("Протокол заседания приемной комиссии (с выделением групп «Общий прием» и «Целевой прием»)", "viewEntranceExaminationMeetingReportList", UNIEC_ORGUNIT_REPORT_BLOCK, "ru.tandemservice.uniec.component.report.EntranceExaminationMeeting.EntranceExaminationMeetingReportList", "viewEntranceExaminationMeetingReportList"))
                .create();
    }
}
