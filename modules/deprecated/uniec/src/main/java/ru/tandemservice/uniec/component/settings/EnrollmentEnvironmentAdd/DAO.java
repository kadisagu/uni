package ru.tandemservice.uniec.component.settings.EnrollmentEnvironmentAdd;

import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

/**
 * @author Vasily Zhukov
 * @since 17.02.2011
 */
public class DAO extends UniDao<Model> implements IUniDao<Model>
{
    @Override
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
    }
}
