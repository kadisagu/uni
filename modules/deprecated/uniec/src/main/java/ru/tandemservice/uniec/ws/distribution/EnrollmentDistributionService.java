/* $Id$ */
package ru.tandemservice.uniec.ws.distribution;

import java.util.Collections;

import javax.jws.WebParam;
import javax.jws.WebService;

import org.tandemframework.hibsupport.DataAccessServices;

import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.gen.CompensationTypeGen;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgDistribution;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistribution;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistributionConfig;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistributionDetail;
import ru.tandemservice.uniec.base.entity.ecg.IPersistentEcgItem;
import ru.tandemservice.uniec.base.entity.ecg.gen.EcgDistributionConfigGen;
import ru.tandemservice.uniec.base.entity.ecg.gen.EcgDistributionDetailGen;
import ru.tandemservice.uniec.base.entity.ecg.gen.EcgDistributionGen;

/**
 * @author Vasily Zhukov
 * @since 24.07.2011
 */
@WebService
public class EnrollmentDistributionService
{
    /**
     * Получение информации о распределении
     *
     * @param ecgItemId           идентификатор направления приема или конкурсной группы
     * @param compensationTypeId  Код вида возмещения затрат
     * @param secondHighAdmission true, если распределение на второе высшее; false, если для студентов или слушателей
     * @param wave                номер волны (начинается с 1)
     * @param detailDistribution  true, если следует брать уточняющее распределение
     * @return распределение
     */
    public EnrollmentDistributionEnvironmentNode getEnrollmentDistributionEnvironmentNode(
            @WebParam(name = "ecgItemId") Long ecgItemId,
            @WebParam(name = "compensationTypeId") String compensationTypeId,
            @WebParam(name = "secondHighAdmission") Boolean secondHighAdmission,
            @WebParam(name = "wave") Integer wave,
            @WebParam(name = "detailDistribution") Boolean detailDistribution
    )
    {
        if (ecgItemId == null)
            throw new RuntimeException("WebParam 'ecgItemId' is not specified!");

        if (compensationTypeId == null)
            throw new RuntimeException("WebParam 'compensationTypeId' is not specified!");

        if (secondHighAdmission == null)
            throw new RuntimeException("WebParam 'secondHighAdmission' is not specified!");

        if (wave == null)
            throw new RuntimeException("WebParam 'wave' is not specified!");

        IPersistentEcgItem ecgItem = DataAccessServices.dao().getNotNull(ecgItemId);

        CompensationType compensationType = DataAccessServices.dao().getByNaturalId(new CompensationTypeGen.NaturalId(compensationTypeId));

        EcgDistributionConfig config = DataAccessServices.dao().getByNaturalId(new EcgDistributionConfigGen.NaturalId(ecgItem, compensationType, secondHighAdmission));

        if (config == null) return new EnrollmentDistributionEnvironmentNode();

        EcgDistribution distribution = DataAccessServices.dao().getByNaturalId(new EcgDistributionGen.NaturalId(config, wave));

        if (distribution == null) return new EnrollmentDistributionEnvironmentNode();

        IEcgDistribution result = detailDistribution ? DataAccessServices.dao().<EcgDistributionDetail>getByNaturalId(new EcgDistributionDetailGen.NaturalId(distribution)) : distribution;

        if (result == null) return new EnrollmentDistributionEnvironmentNode();

        return IEnrollmentDistributionServiceDao.INSTANCE.get().getEnrollmentDistributionEnvironmentNode(Collections.singleton(result));
    }
}
