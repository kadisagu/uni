package ru.tandemservice.uniec.entity.orders.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EntrListenerParExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * О зачислении в число слушателей (паралл. освоение)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntrListenerParExtractGen extends EnrollmentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.orders.EntrListenerParExtract";
    public static final String ENTITY_NAME = "entrListenerParExtract";
    public static final int VERSION_HASH = -208185292;
    private static IEntityMeta ENTITY_META;

    public static final String L_PARALLEL = "parallel";

    private EducationLevelsHighSchool _parallel;     // Параметры выпуска студентов по направлению подготовки (НПв)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Параметры выпуска студентов по направлению подготовки (НПв). Свойство не может быть null.
     */
    @NotNull
    public EducationLevelsHighSchool getParallel()
    {
        return _parallel;
    }

    /**
     * @param parallel Параметры выпуска студентов по направлению подготовки (НПв). Свойство не может быть null.
     */
    public void setParallel(EducationLevelsHighSchool parallel)
    {
        dirty(_parallel, parallel);
        _parallel = parallel;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EntrListenerParExtractGen)
        {
            setParallel(((EntrListenerParExtract)another).getParallel());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntrListenerParExtractGen> extends EnrollmentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntrListenerParExtract.class;
        }

        public T newInstance()
        {
            return (T) new EntrListenerParExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "parallel":
                    return obj.getParallel();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "parallel":
                    obj.setParallel((EducationLevelsHighSchool) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "parallel":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "parallel":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "parallel":
                    return EducationLevelsHighSchool.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntrListenerParExtract> _dslPath = new Path<EntrListenerParExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntrListenerParExtract");
    }
            

    /**
     * @return Параметры выпуска студентов по направлению подготовки (НПв). Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EntrListenerParExtract#getParallel()
     */
    public static EducationLevelsHighSchool.Path<EducationLevelsHighSchool> parallel()
    {
        return _dslPath.parallel();
    }

    public static class Path<E extends EntrListenerParExtract> extends EnrollmentExtract.Path<E>
    {
        private EducationLevelsHighSchool.Path<EducationLevelsHighSchool> _parallel;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Параметры выпуска студентов по направлению подготовки (НПв). Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EntrListenerParExtract#getParallel()
     */
        public EducationLevelsHighSchool.Path<EducationLevelsHighSchool> parallel()
        {
            if(_parallel == null )
                _parallel = new EducationLevelsHighSchool.Path<EducationLevelsHighSchool>(L_PARALLEL, this);
            return _parallel;
        }

        public Class getEntityClass()
        {
            return EntrListenerParExtract.class;
        }

        public String getEntityName()
        {
            return "entrListenerParExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
