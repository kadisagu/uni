/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntranceDisciplineAddEdit;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineKind;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline;
import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline2SetDisciplineRelation;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.entity.settings.SetDiscipline;

import java.util.*;

/**
 * @author Боба
 * @since 01.08.2008
 */
public class DAO extends UniDao<EntranceDisciplineAddEditModel> implements IDAO
{
    @Override
    @SuppressWarnings("unchecked")
    public void prepare(final EntranceDisciplineAddEditModel model)
    {
        if (model.getEntranceDiscipline().getId() == null)
        {
            // форма добавления
            EnrollmentDirection enrollmentDirection = getNotNull(EnrollmentDirection.class, model.getEntranceDiscipline().getEnrollmentDirection().getId());
            model.getEntranceDiscipline().setEnrollmentDirection(enrollmentDirection);
            model.getEntranceDiscipline().setBudget(true);
            model.getEntranceDiscipline().setContract(true);
        } else
        {
            // форма редактирования
            model.setEntranceDiscipline(getNotNull(EntranceDiscipline.class, model.getEntranceDiscipline().getId()));
            model.setChoiceDisciplinesList(model.getEntranceDiscipline().getChoicableDisciplines());
        }

        // получаем список возможных дисциплин
        Criteria c = getSession().createCriteria(SetDiscipline.class);
        c.add(Restrictions.eq(SetDiscipline.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
        List<SetDiscipline> result = c.list();
        Collections.sort(result, ITitled.TITLED_COMPARATOR);
        model.setDisciplinesList(result);

        // получаем список категорий поступающих
        model.setStudentCategoryList(getCatalogItemListOrderByCode(StudentCategory.class));
        model.setKindList(getCatalogItemListOrderByCode(EntranceDisciplineKind.class));
        model.setTypeList(getCatalogItemListOrderByCode(EntranceDisciplineType.class));

        // получаем список возможных дисциплин по выбору
        model.setDisciplinesSelectModel(new FullCheckSelectModel(Discipline2RealizationWayRelation.P_TITLE)
        {
            @Override
            public ListResult findValues(String filter)
            {
                Criteria c = getSession().createCriteria(SetDiscipline.class);
                c.add(Restrictions.eq(SetDiscipline.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
                List<SetDiscipline> list = c.list();
                Collections.sort(list, ITitled.TITLED_COMPARATOR);

                if (StringUtils.isEmpty(filter))
                    return new ListResult<>(list);

                List<SetDiscipline> result = new ArrayList<>();
                for (SetDiscipline item : list)
                    if (StringUtils.containsIgnoreCase(item.getTitle(), filter))
                        result.add(item);
                return new ListResult<>(result);
            }
        });
    }

    @Override
    public void validate(EntranceDisciplineAddEditModel model, ErrorCollector errors)
    {
        EntranceDiscipline entranceDiscipline = model.getEntranceDiscipline();

        if (!entranceDiscipline.isBudget() && !entranceDiscipline.isContract())
        {
            errors.add("Укажите бюджет или контракт для вступительного испытания");
        }

        if (model.getChoiceDisciplinesList().contains(entranceDiscipline.getDiscipline()))
        {
            errors.add("Нельзя основную дисциплину выбрать как дисциплину по выбору", "choiceDisciplines");
        }

        // если нет дисциплин по выбору, то проверяем, что точно такого еще нет
        if (model.getChoiceDisciplinesList().isEmpty())
        {
            if (!checkSubjectUniqueness(entranceDiscipline))
            {
                errors.add("Такое вступительное испытание уже есть в этом наборе.");
            }
        }

        // если профильное
        if (entranceDiscipline.isProfile())
        {
            // то проверяем, что одно в наборе
            if (!checkProfileUniqueness(entranceDiscipline))
            {
                errors.add("В наборе может быть только одно профильное вступительное испытание.");
            }

            // и проверяем, что нет по выбору
            if (!model.getChoiceDisciplinesList().isEmpty())
            {
                errors.add("Профильное вступительное испытание не может иметь испытаний по выбору.");
            }
        }
    }

    @Override
    public void update(EntranceDisciplineAddEditModel model)
    {
        Session session = getSession();

        EntranceDiscipline entranceDiscipline = model.getEntranceDiscipline();
        entranceDiscipline.setStudentCategory(getStudentCategory(entranceDiscipline));

        if (entranceDiscipline.getEnrollmentDirection().getEnrollmentCampaign().isEnrollmentPerCompTypeDiff())
        {
            if (!entranceDiscipline.isBudget()) entranceDiscipline.setPassMark(null);
            if (!entranceDiscipline.isContract()) entranceDiscipline.setPassMarkContract(null);
        } else
        {
            entranceDiscipline.setPassMarkContract(null);
        }

        session.saveOrUpdate(entranceDiscipline);

        List<EntranceDiscipline2SetDisciplineRelation> oldList = getList(EntranceDiscipline2SetDisciplineRelation.class, EntranceDiscipline2SetDisciplineRelation.L_ENTRANCE_DISCIPLINE, entranceDiscipline);
        List<SetDiscipline> newList = model.getChoiceDisciplinesList();

        // delete unused
        Set<SetDiscipline> used = new HashSet<>();
        for (EntranceDiscipline2SetDisciplineRelation rel : oldList)
        {
            used.add(rel.getSetDiscipline());
            if (!newList.contains(rel.getSetDiscipline()))
            {
                session.delete(rel);
            }
        }

        // add newest
        for (SetDiscipline discipline : newList)
        {
            if (!used.contains(discipline))
            {
                EntranceDiscipline2SetDisciplineRelation rel = new EntranceDiscipline2SetDisciplineRelation();
                rel.setEntranceDiscipline(entranceDiscipline);
                rel.setSetDiscipline(discipline);
                session.save(rel);
            }
        }
    }

    private boolean checkProfileUniqueness(EntranceDiscipline entranceDiscipline)
    {
        Criteria criteria = getSession().createCriteria(EntranceDiscipline.class);
        criteria.createAlias(EntranceDiscipline.kind().s(), "type");
        criteria.add(Restrictions.eq(EntranceDiscipline.enrollmentDirection().s(), entranceDiscipline.getEnrollmentDirection()));
        criteria.add(Restrictions.eq(EntranceDiscipline.studentCategory().s(), getStudentCategory(entranceDiscipline)));
        criteria.add(Restrictions.eq(EntranceDisciplineKind.code().fromAlias("type").s(), UniecDefines.ENTRANCE_DISCIPLINE_KIND_PROFILE));
        if (entranceDiscipline.getId() != null)
        {
            criteria.add(Restrictions.ne(EntranceDiscipline.id().s(), entranceDiscipline.getId()));
        }
        criteria.setProjection(Projections.rowCount());
        return (((Number) criteria.uniqueResult()).intValue() == 0);
    }

    private boolean checkSubjectUniqueness(EntranceDiscipline entranceDiscipline)
    {
        MQBuilder builder = new MQBuilder(EntranceDiscipline.ENTITY_CLASS, "entranceDiscipline");
        builder.add(MQExpression.eq("entranceDiscipline", EntranceDiscipline.enrollmentDirection().s(), entranceDiscipline.getEnrollmentDirection()));
        builder.add(MQExpression.eq("entranceDiscipline", EntranceDiscipline.studentCategory().s(), getStudentCategory(entranceDiscipline)));
        builder.add(MQExpression.eq("entranceDiscipline", EntranceDiscipline.discipline().s(), entranceDiscipline.getDiscipline()));
        if (entranceDiscipline.getId() != null)
        {
            builder.add(MQExpression.notEq("entranceDiscipline", EntranceDiscipline.id().s(), entranceDiscipline.getId()));
        }

        MQBuilder subBuilder = new MQBuilder(EntranceDiscipline2SetDisciplineRelation.ENTITY_CLASS, "relation");
        subBuilder.add(MQExpression.eqProperty("entranceDiscipline", EntranceDiscipline.id().s(), "relation", EntranceDiscipline2SetDisciplineRelation.entranceDiscipline().id().s()));
        builder.add(MQExpression.notExists(subBuilder));

        return (builder.getResultCount(getSession()) == 0);
    }

    private StudentCategory getStudentCategory(EntranceDiscipline entranceDiscipline)
    {
        return (!entranceDiscipline.getEnrollmentDirection().getEnrollmentCampaign().isExamSetDiff()) ? getCatalogItem(StudentCategory.class, UniDefines.STUDENT_CATEGORY_STUDENT) : entranceDiscipline.getStudentCategory();
    }
}