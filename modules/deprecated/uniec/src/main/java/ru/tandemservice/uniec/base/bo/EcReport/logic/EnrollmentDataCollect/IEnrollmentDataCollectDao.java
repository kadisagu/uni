/**
 *$Id$
 */
package ru.tandemservice.uniec.base.bo.EcReport.logic.EnrollmentDataCollect;

import jxl.write.WriteException;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uniec.entity.report.EnrollmentDataCollectReport;

import java.io.IOException;

/**
 * @author Alexander Shaburov
 * @since 15.08.13
 */
public interface IEnrollmentDataCollectDao extends INeedPersistenceSupport
{
    <T extends EnrollmentDataCollectReportModel> T createModel();

    /**
     * Создает отчет и заполняет его поля.
     * @param model модель
     * @param reportFile файл отчета
     * @return Отчет "Сбор сведений от образовательного учреждения ВПО о ходе приема (форма 2, от 29 апреля 2013 г. № 313 )"
     */
    <T extends EnrollmentDataCollectReportModel> EnrollmentDataCollectReport createReport(T model, DatabaseFile reportFile);

    /**
     * Создает печатную форму отчета "Сбор сведений от образовательного учреждения ВПО о ходе приема (форма 2, от 29 апреля 2013 г. № 313)".
     * @param model модель
     * @return Файл отчета хранимый в базе данных
     */
    <T extends EnrollmentDataCollectReportModel> DatabaseFile createPrintReportFile(T model) throws IOException, WriteException;
}
