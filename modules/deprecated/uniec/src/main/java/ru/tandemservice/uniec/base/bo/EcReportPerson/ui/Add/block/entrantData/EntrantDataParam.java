package ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.block.entrantData;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.IIdentifiableWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.hibsupport.dql.IDQLSelectableQuery;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.EcReportPersonAdd;
import ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.EcReportPersonAddUI;
import ru.tandemservice.uniec.entity.catalog.*;
import ru.tandemservice.uniec.entity.entrant.*;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Vasily Zhukov
 * @since 08.02.2011
 */
public class EntrantDataParam implements IReportDQLModifier
{
    private IReportParam<IIdentifiableWrapper> _entrant = new ReportParam<IIdentifiableWrapper>();
    private IReportParam<EnrollmentCampaign> _enrollmentCampaign = new ReportParam<EnrollmentCampaign>();
    private IReportParam<IIdentifiableWrapper> _hasOlympiadDiploma = new ReportParam<IIdentifiableWrapper>();
    private IReportParam<List<EnrollmentRecommendation>> _recomendations = new ReportParam<List<EnrollmentRecommendation>>();
    private IReportParam<List<AccessCourse>> _accessCourses = new ReportParam<List<AccessCourse>>();
    private IReportParam<IIdentifiableWrapper> _passProfileEducation = new ReportParam<IIdentifiableWrapper>();
    private IReportParam<IIdentifiableWrapper> _hasExamResult = new ReportParam<IIdentifiableWrapper>();
    private IReportParam<List<SourceInfoAboutUniversity>> _infoSource = new ReportParam<List<SourceInfoAboutUniversity>>();
    private IReportParam<IIdentifiableWrapper> _entrantArchival = new ReportParam<IIdentifiableWrapper>();
    private IReportParam<DataWrapper> _entrantCustomStateExist = new ReportParam<>();
    private IReportParam<List<EntrantCustomStateCI>> _entrantCustomStateList = new ReportParam<>();
    private IReportParam<IIdentifiableWrapper> _entrantHasIndividualProgress = new ReportParam<>();
    private IReportParam<List<IndividualProgress>> _entrantIndividualProgress = new ReportParam<>();
    private IReportParam<List<Base4ExamByDifferentSources>> _base4ExamByDifferentSources = new ReportParam<>();
    private IReportParam<DataWrapper> _specialCondition4Exam= new ReportParam<>();

    private String alias(ReportDQL dql)
    {
        // соединяем абитуриента
        String entrantAlias = dql.innerJoinEntity(Entrant.class, Entrant.person());

        // добавляем сортировку
        dql.order(entrantAlias, Entrant.enrollmentCampaign().id());

        return entrantAlias;
    }

    private String erAlias(ReportDQL dql)
    {
        // соединяем рекомендации к зачислению
        String eerAlias = dql.innerJoinEntity(alias(dql), EntrantEnrolmentRecommendation.class, EntrantEnrolmentRecommendation.entrant());

        return eerAlias;
    }

    private String acAlias(ReportDQL dql)
    {
        // соединяем подготовительный курс абитуриента
        String eacAlias = dql.innerJoinEntity(alias(dql), EntrantAccessCourse.class, EntrantAccessCourse.entrant());

        return eacAlias;
    }

    private String iauAlias(ReportDQL dql)
    {
        // соединяем выбранный источник информации об ОУ
        String iauAlias = dql.innerJoinEntity(alias(dql), EntrantInfoAboutUniversity.class, EntrantInfoAboutUniversity.entrant());

        return iauAlias;
    }

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {

    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (_entrant.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "entrantData.entrant", _entrant.getData().getTitle());

            if (_entrant.getData().getId().equals(EntrantDataBlock.ENTRANT_YES))
            {
                alias(dql);
            }
            else
            {
                String entrantAlias = dql.nextAlias();

                dql.builder.where(notIn(property(dql.getFromEntityAlias()),
                                        new DQLSelectBuilder().fromEntity(Entrant.class, entrantAlias).column(property(Entrant.person().fromAlias(entrantAlias))).buildQuery()
                ));
            }
        }

        if (_enrollmentCampaign.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "entrantData.enrollmentCampaign", _enrollmentCampaign.getData().getTitle());

            // запоминаем выбранную приемную кампанию
            printInfo.putSharedObject(EcReportPersonAdd.PREFETCH_ENROLLMENT_CAMPAIGN, _enrollmentCampaign.getData());

            dql.builder.where(eq(property(Entrant.enrollmentCampaign().fromAlias(alias(dql))), value(_enrollmentCampaign.getData())));
        }

        if (_hasOlympiadDiploma.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "entrantData.hasOlympiadDiploma", _hasOlympiadDiploma.getData().getTitle());

            String alias = dql.nextAlias();

            IDQLSelectableQuery query = new DQLSelectBuilder().fromEntity(OlympiadDiploma.class, alias).column(property(OlympiadDiploma.id().fromAlias(alias)))
                    .where(eq(property(OlympiadDiploma.entrant().fromAlias(alias)), property(alias(dql))))
                    .buildQuery();

            dql.builder.where(_hasOlympiadDiploma.getData().getId().equals(EntrantDataBlock.HAS_OLYMPIAD_DIPLOMA_YES) ? exists(query) : notExists(query));
        }

        if (_recomendations.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "entrantData.recomendations", CommonBaseStringUtil.join(_recomendations.getData(), EnrollmentRecommendation.P_TITLE, ", "));

            if (_recomendations.getData().isEmpty())
            {
                String nextAlias = dql.nextAlias();

                dql.builder.where(notExists(new DQLSelectBuilder().fromEntity(EntrantEnrolmentRecommendation.class, nextAlias)
                                                    .column(property(EntrantEnrolmentRecommendation.id().fromAlias(nextAlias)))
                                                    .where(eq(property(EntrantEnrolmentRecommendation.entrant().fromAlias(nextAlias)), property(alias(dql))))
                                                    .buildQuery()
                ));
            }
            else
                dql.builder.where(in(property(EntrantEnrolmentRecommendation.recommendation().fromAlias(erAlias(dql))), _recomendations.getData()));
        }

        if (_accessCourses.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "entrantData.accessCourses", CommonBaseStringUtil.join(_accessCourses.getData(), AccessCourse.P_TITLE, ", "));

            if (_accessCourses.getData().isEmpty())
            {
                String nextAlias = dql.nextAlias();

                dql.builder.where(notExists(new DQLSelectBuilder().fromEntity(EntrantAccessCourse.class, nextAlias)
                                                    .column(property(EntrantAccessCourse.id().fromAlias(nextAlias)))
                                                    .where(eq(property(EntrantAccessCourse.entrant().fromAlias(nextAlias)), property(alias(dql))))
                                                    .buildQuery()
                ));
            }
            else
                dql.builder.where(in(property(EntrantAccessCourse.course().fromAlias(acAlias(dql))), _accessCourses.getData()));
        }

        if (_passProfileEducation.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "entrantData.passProfileEducation", _passProfileEducation.getData().getTitle());

            dql.builder.where(eq(property(Entrant.passProfileEducation().fromAlias(alias(dql))), value(_passProfileEducation.getData().getId().equals(EntrantDataBlock.PASS_PROFILE_EDUCATION_YES))));
        }

        if (_hasExamResult.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "entrantData.hasExamResult", _hasExamResult.getData().getTitle());

            String alias = dql.nextAlias();
            String alias1 = dql.nextAlias();
            String alias2 = dql.nextAlias();
            String alias3 = dql.nextAlias();

            IDQLSelectableQuery query = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, alias)
                    .column(property(RequestedEnrollmentDirection.id().fromAlias(alias)))
                    .where(or(
                            // вид конкурса - без вступительных испытаний
                            eq(property(RequestedEnrollmentDirection.competitionKind().code().fromAlias(alias)), value(UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES)),

                            // есть результат собеседования
                            exists(new DQLSelectBuilder().fromEntity(InterviewResult.class, alias1)
                                           .column(property(InterviewResult.id().fromAlias(alias1)))
                                           .where(eq(property(InterviewResult.requestedEnrollmentDirection().id().fromAlias(alias1)), property(RequestedEnrollmentDirection.id().fromAlias(alias))))
                                           .buildQuery()
                            ),

                            and(
                                    // есть выбранные вступительные испытания 
                                    exists(new DQLSelectBuilder().fromEntity(ChosenEntranceDiscipline.class, alias2)
                                                   .column(property(ChosenEntranceDiscipline.id().fromAlias(alias2)))
                                                   .where(eq(property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias(alias2)), property(RequestedEnrollmentDirection.id().fromAlias(alias))))
                                                   .buildQuery()
                                    ),

                                    //  по всем выбранным вступительным испытаниям есть оценки
                                    notExists(new DQLSelectBuilder().fromEntity(ChosenEntranceDiscipline.class, alias3)
                                                      .column(property(ChosenEntranceDiscipline.id().fromAlias(alias3)))
                                                      .where(eq(property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias(alias3)), property(RequestedEnrollmentDirection.id().fromAlias(alias))))
                                                      .where(isNull(property(ChosenEntranceDiscipline.finalMark().fromAlias(alias3))))
                                                      .buildQuery()
                                    )
                            )
                    )).buildQuery();

            dql.builder.where(_hasExamResult.getData().getId().equals(EntrantDataBlock.HAS_EXAM_RESULT_YES) ? exists(query) : notExists(query));
        }

        if (_infoSource.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "entrantData.infoSource", CommonBaseStringUtil.join(_infoSource.getData(), SourceInfoAboutUniversity.P_TITLE, ", "));

            dql.builder.where(in(property(EntrantInfoAboutUniversity.sourceInfo().fromAlias(iauAlias(dql))), _infoSource.getData()));
        }

        if (_entrantArchival.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "entrantData.entrantArchival", _entrantArchival.getData().getTitle());

            dql.builder.where(eq(property(Entrant.archival().fromAlias(alias(dql))), value(_entrantArchival.getData().getId().equals(EntrantDataBlock.ENTRANT_ARCHIVAL_YES))));
        }

        Boolean customStateExist = TwinComboDataSourceHandler.getSelectedValue(_entrantCustomStateExist.getData());
        if (customStateExist != null)
        {
            List<EntrantCustomStateCI> customStateList = _entrantCustomStateList.getData();
            String str = _entrantCustomStateExist.getData().getTitle() +
                    (CollectionUtils.isNotEmpty(customStateList) ? " " + customStateList.stream().map(EntrantCustomStateCI::getTitle).collect(Collectors.joining(", ")) : "");
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "entrantData.entrantCustomStateCI", str);


            IDQLExpression customStateExp = EntrantCustomState.getExpression4EntrantCustomState(property(alias(dql)), "ecs", customStateList, new Date());
            if (customStateExist) dql.builder.where(existsByExpr(EntrantCustomState.class, "ecs", customStateExp));
            else dql.builder.where(notExistsByExpr(EntrantCustomState.class, "ecs", customStateExp));
        }

        if (_entrantHasIndividualProgress.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET,
                                    "requestBlock.entrantHasIndividualProgress",
                                    _entrantHasIndividualProgress.getData().getTitle());

            IDQLSelectableQuery subQuery = new DQLSelectBuilder()
                    .fromEntity(EntrantIndividualProgress.class, "ip1")
                    .where(eq(property("ip1", EntrantIndividualProgress.entrant()), property(alias(dql))))
                    .buildQuery();

            if (_entrantHasIndividualProgress.getData().getId().equals(EntrantDataBlock.ENTRANT_HAS_INDIVIDUAL_PROGRESS_YES))
                dql.builder.where(exists(subQuery));
            else
                dql.builder.where(notExists(subQuery));
        }

        if (_entrantIndividualProgress.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET,
                                    "requestBlock.entrantIndividualProgress",
                                    CommonBaseStringUtil.join(_entrantIndividualProgress.getData(), IndividualProgress.P_TITLE, ", "));

            dql.builder.where(exists(
                    new DQLSelectBuilder()
                            .fromEntity(EntrantIndividualProgress.class, "ip2")
                            .where(eq(property("ip2", EntrantIndividualProgress.entrant()), property(alias(dql))))
                            .where(in(property("ip2", EntrantIndividualProgress.individualProgressType()), _entrantIndividualProgress.getData()))
                            .buildQuery()
            ));
        }

        if (_base4ExamByDifferentSources.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "entrantData.base4ExamByDifferentSources",
                                    CommonBaseStringUtil.join(_base4ExamByDifferentSources.getData(), Base4ExamByDifferentSources.P_TITLE, ", "));
            FilterUtils.applySelectFilter(dql.builder, alias(dql), Entrant.base4ExamByDifferentSources().s(), _base4ExamByDifferentSources.getData());
        }

        if (_specialCondition4Exam.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "entrantData.specialCondition4Exam", _specialCondition4Exam.getData().getTitle());
            FilterUtils.applySelectFilter(dql.builder, alias(dql), Entrant.specialCondition4Exam().s(),
                                          TwinComboDataSourceHandler.getSelectedValue(_specialCondition4Exam.getData()));
        }
    }

    // Getters

    public IReportParam<IIdentifiableWrapper> getEntrant()
    {
        return _entrant;
    }

    public IReportParam<EnrollmentCampaign> getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public IReportParam<IIdentifiableWrapper> getHasOlympiadDiploma()
    {
        return _hasOlympiadDiploma;
    }

    public IReportParam<List<EnrollmentRecommendation>> getRecomendations()
    {
        return _recomendations;
    }

    public IReportParam<List<AccessCourse>> getAccessCourses()
    {
        return _accessCourses;
    }

    public IReportParam<IIdentifiableWrapper> getPassProfileEducation()
    {
        return _passProfileEducation;
    }

    public IReportParam<IIdentifiableWrapper> getHasExamResult()
    {
        return _hasExamResult;
    }

    public IReportParam<List<SourceInfoAboutUniversity>> getInfoSource()
    {
        return _infoSource;
    }

    public IReportParam<IIdentifiableWrapper> getEntrantArchival()
    {
        return _entrantArchival;
    }

    public IReportParam<DataWrapper> getEntrantCustomStateExist()
    {
        return _entrantCustomStateExist;
    }

    public void setEntrantCustomStateExist(IReportParam<DataWrapper> entrantCustomStateExist)
    {
        _entrantCustomStateExist = entrantCustomStateExist;
    }

    public IReportParam<List<EntrantCustomStateCI>> getEntrantCustomStateList()
    {
        return _entrantCustomStateList;
    }

    public void setEntrantCustomStateList(IReportParam<List<EntrantCustomStateCI>> entrantCustomStateCI)
    {
        _entrantCustomStateList = entrantCustomStateCI;
    }

    public IReportParam<IIdentifiableWrapper> getEntrantHasIndividualProgress()
    {
        return _entrantHasIndividualProgress;
    }

    public IReportParam<List<IndividualProgress>> getEntrantIndividualProgress()
    {
        return _entrantIndividualProgress;
    }

    public IReportParam<List<Base4ExamByDifferentSources>> getBase4ExamByDifferentSources()
    {
        return _base4ExamByDifferentSources;
    }

    public IReportParam<DataWrapper> getSpecialCondition4Exam()
    {
        return _specialCondition4Exam;
    }
}
