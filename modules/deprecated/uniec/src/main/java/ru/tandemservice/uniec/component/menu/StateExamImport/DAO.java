/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.menu.StateExamImport;

import org.apache.commons.io.IOUtils;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.StateExamImportedFile;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.io.InputStream;
import java.util.Date;

/**
 * @author ekachanova
 */
public class DAO extends UniDao<Model> implements IDAO
{


    @Override
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
    }

    @Override
    public void update(Model model)
    {
        try {
            ITitled imp = model.getImport();
            if (null == imp) { throw new IllegalStateException(); }

            final byte[] importedContent;
            try (InputStream stream = model.getUploadFile().getStream())
            {
                importedContent = IOUtils.toByteArray(stream);
            }

            final byte[] resultContent;

	        if (imp instanceof IImportByIdentityCard)
	        {
		        final EnrollmentCampaign enrollmentCampaign = model.getEnrollmentCampaign();
		        final boolean deleteNotFound = model.isDeleteNotFound();
		        resultContent = ((IImportByIdentityCard) imp).doImport(enrollmentCampaign, deleteNotFound, importedContent);
	        }
	        else if (imp instanceof IImportByCertificate)
	        {
		        final EnrollmentCampaign enrollmentCampaign = model.getEnrollmentCampaign();
		        resultContent = ((IImportByCertificate) imp).doImport(enrollmentCampaign, importedContent);
	        }
	        else
	        {
		        throw new IllegalStateException(imp.getClass().getName());
	        }

            StateExamImportedFile importResult = new StateExamImportedFile();

            importResult.setImportDate(new Date());
            importResult.setImportType(model.getImportType().getTitle());
            importResult.setEnrollmentCampaign(model.getEnrollmentCampaign());

            DatabaseFile importedFile = new DatabaseFile();
            importedFile.setContent(importedContent);
            getSession().save(importedFile);
            importResult.setImportedFile(importedFile);

            DatabaseFile resultFile = new DatabaseFile();
            resultFile.setContent(resultContent);
            getSession().save(resultFile);
            importResult.setResultFile(resultFile);

            getSession().save(importResult);

        } catch (Exception e) {
            throw CoreExceptionUtils.getRuntimeException(e);
        }
    }


}
