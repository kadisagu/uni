/* $Id$ */
package ru.tandemservice.uniec.component.listextract.ent2.e1.ParagraphPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.AbstractListParagraphPubController;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.utils.MoveStudentUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniec.entity.orders.SplitBudgetMasterEntrantsStuListExtract;
import ru.tandemservice.uniec.entity.orders.SplitEntrantsStuListExtract;
import ru.tandemservice.unimove.IAbstractExtract;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 21.08.2012
 */
public class Controller extends AbstractListParagraphPubController<SplitBudgetMasterEntrantsStuListExtract, IDAO, Model>
{
    @Override
    protected void prepareListDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<SplitBudgetMasterEntrantsStuListExtract> dataSource = UniBaseUtils.createDataSource(component, getDao());

        @SuppressWarnings("unchecked")
        AbstractColumn linkColumn = new PublisherLinkColumn("ФИО", new String[]{IAbstractExtract.L_ENTITY, Student.L_PERSON, Person.P_FULLFIO})
                .setResolver(new DefaultPublisherLinkResolver()
                {
                    @Override
                    public String getComponentName(IEntity entity)
                    {
                        return MoveStudentUtils.getListExtractPubComponent(((StudentExtractType) entity.getProperty(IAbstractExtract.L_TYPE)));
                    }
                }).setOrderable(false);
        dataSource.addColumn(linkColumn);
        dataSource.addColumn(new SimpleColumn("Пол", new String[]{IAbstractExtract.L_ENTITY, Student.L_PERSON, Person.L_IDENTITY_CARD, IdentityCard.L_SEX, Sex.P_SHORT_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Состояние", ListStudentExtract.P_STUDENT_STATUS_STR).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Курс", ListStudentExtract.P_COURSE_STR).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Контракт", ListStudentExtract.P_COMPENSATION_TYPE_STR).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Профиль", SplitEntrantsStuListExtract.ecgpEntrantRecommended().profile().educationOrgUnit().educationLevelHighSchool().displayableTitle().s()).setClickable(false).setOrderable(false));
        //TODO: профиль

        AbstractColumn orderLinkColumn = new PublisherLinkColumn("№ приказа о зачислении", SplitEntrantsStuListExtract.enrollmentExtract().paragraph().order().number().s())
                .setResolver(new DefaultPublisherLinkResolver()
                {
                    @SuppressWarnings("unchecked")
                    @Override
                    public Object getParameters(IEntity entity)
                    {
                        Map<String, Object> params = new HashMap<>();
                        params.put(PublisherActivator.PUBLISHER_ID_KEY, ((ViewWrapper<SplitEntrantsStuListExtract>) entity).getEntity().getEnrollmentExtract().getOrder().getId());
                        return params;
                    }

                    @Override
                    public String getComponentName(IEntity entity)
                    {
                        return null;
                    }
                }).setOrderable(false);
        dataSource.addColumn(orderLinkColumn);

        dataSource.addColumn(new SimpleColumn("Дата приказа о зачислении", SplitEntrantsStuListExtract.enrollmentExtract().paragraph().order().commitDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));

        /*IndicatorColumn indiColumn = (IndicatorColumn) MoveStudentColumns.getExtractPrintColumn(this, getModel(component).getSecModel().getPermission("printExtract")).setDisabledProperty(AbstractStudentExtract.P_PRINTING_DISABLED_VIEW_PROPERTY);
        indiColumn.setListener(component.getName() + ":onClickExtractPrint");
        dataSource.addColumn(indiColumn);*/

        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteExtract", "Исключить студента из параграфа «{0}»?", new Object[]{new String[]{IAbstractExtract.L_ENTITY, Student.L_PERSON, Person.P_FULLFIO}}).setPermissionKey(model.getSecModel().getPermission("deleteStudent")).setDisabledProperty(Model.NO_DELETE));

        model.setDataSource(dataSource);
    }
}