/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.RequestedEnrollmentDirectionsChangePriority;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

/**
 * @author ekachanova
 */
@Input({
	@Bind(key= Model.ENTRANT_REQUEST_ID, binding="entrantRequest.id")
})
public class Model
{
    public static final String ENTRANT_REQUEST_ID = "entrantRequestId";
    static final String PRIORITY_COLUMN = "priority"; 

    private EntrantRequest _entrantRequest = new EntrantRequest();
    private DynamicListDataSource<RequestedEnrollmentDirection> _dataSource;

    public EntrantRequest getEntrantRequest()
    {
        return _entrantRequest;
    }

    public void setEntrantRequest(EntrantRequest entrantRequest)
    {
        _entrantRequest = entrantRequest;
    }

    public DynamicListDataSource<RequestedEnrollmentDirection> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<RequestedEnrollmentDirection> dataSource)
    {
        _dataSource = dataSource;
    }

//    public Validator getPriorityValidator()
//    {
//        final Long currentEntryId = getDataSource().getCurrentEntity().getId();
//        return new BaseValidator()
//        {
//            public void validate(final IFormComponent field, final ValidationMessages messages, final Object object) throws ValidatorException
//            {
//                int max = getDataSource().getEntityList().size();
//                Map<Long, Object> valueMap = ((IValueMapHolder)getDataSource().getColumn(Model.PRIORITY_COLUMN)).getValueMap();
//                Number currentValue = (Number)valueMap.get(currentEntryId);
//                if(currentValue == null)
//                {
//                    throw new ValidatorException(messages.formatValidationMessage("Поле «Приоритет» должно быть заполнено", ValidationStrings.PATTERN_MISMATCH, new Object[]{}), ValidationConstraint.PATTERN_MISMATCH);
//                }
//                if(currentValue.intValue() < 1 || currentValue.intValue() > max)
//                {
//                    throw new ValidatorException(messages.formatValidationMessage("Приоритеты указаны не верно, необходимо вводить цифры от 1 до " + max, ValidationStrings.PATTERN_MISMATCH, new Object[]{}), ValidationConstraint.PATTERN_MISMATCH);
//                }
//                for(Map.Entry entry : valueMap.entrySet())
//                {
//                    Number value = (Number)entry.getValue();
//                    if(!currentEntryId.equals(entry.getKey()) && currentValue.equals(value))
//                    {
//                        throw new ValidatorException(messages.formatValidationMessage("Приоритеты не могут совпадать.", ValidationStrings.PATTERN_MISMATCH, new Object[]{}), ValidationConstraint.PATTERN_MISMATCH);
//                    }
//                }
//            }
//            @Override
//            public boolean getAcceptsNull() { return true; }
//        };
//    }
}
