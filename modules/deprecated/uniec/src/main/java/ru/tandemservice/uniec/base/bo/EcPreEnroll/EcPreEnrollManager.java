/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcPreEnroll;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

import ru.tandemservice.uniec.base.bo.EcPreEnroll.logic.EcPreEnrollDao;
import ru.tandemservice.uniec.base.bo.EcPreEnroll.logic.IEcPreEnrollDao;

/**
 * @author Vasily Zhukov
 * @since 26.05.2011
 */
@Configuration
public class EcPreEnrollManager extends BusinessObjectManager
{
    public static EcPreEnrollManager instance()
    {
        return instance(EcPreEnrollManager.class);
    }

    @Bean
    public IEcPreEnrollDao dao()
    {
        return new EcPreEnrollDao();
    }
}
