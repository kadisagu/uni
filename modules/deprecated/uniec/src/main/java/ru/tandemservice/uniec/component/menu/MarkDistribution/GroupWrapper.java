/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.menu.MarkDistribution;

import org.tandemframework.core.common.ITitled;

import ru.tandemservice.uni.ui.ObjectWrapper;

/**
 * @author vip_delete
 * @since 17.11.2009
 */
class GroupWrapper extends ObjectWrapper implements ITitled
{
    private String _id;
    private String _title;

    GroupWrapper(String id, String title)
    {
        _id = id;
        _title = title;
    }

    @Override
    public String getId()
    {
        return _id;
    }

    @Override
    public String getTitle()
    {
        return _title;
    }
}
