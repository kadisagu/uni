package ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.block.entrantRequestData;

import java.util.Arrays;
import java.util.List;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.dsl.EntityPath;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;

/**
 * @author Vasily Zhukov
 * @since 08.02.2011
 */
public class EntrantRequestDataBlock
{
    // names
    public static final String QUALIFICATION_DS = "qualificationDS";
    public static final String FORMATIVE_ORGUNIT_DS = "formativeOrgUnitDS";
    public static final String TERRITORIAL_ORGUNIT_DS = "territorialOrgUnitDS";
    public static final String EDUCATION_ORGUNIT_DS = "educationOrgUnitDS";
    public static final String COMPETITION_KIND_DS = "competitionKindDS";
    public static final String COMPENSATION_TYPE_DS = "compensationTypeDS";
    public static final String STUDENT_CATEGORY_DS = "studentCategoryDS";
    public static final String TARGET_ADMISSION_DS = "targetAdmissionDS";
    public static final String TARGET_ADMISSION_KIND_DS = "targetAdmissionKindDS";
    public static final String ENTRANT_STATE_DS = "entrantStateDS";
    public static final String ORIGINAL_DOCUMENTS_DS = "originalDocumentsDS";

    // datasource parameter names
    public static final String PARAM_FORMATIVE_ORG_UNIT = "formativeOrgUnitList";
    public static final String PARAM_TERRITORIAL_ORG_UNIT = "territorialOrgUnitList";

    // ids
    public static final Long TARGET_ADMISSION_YES = 0L;
    public static final Long TARGET_ADMISSION_NO = 1L;
    public static final Long ORIGINAL_DOCUMENT_YES = 0L;
    public static final Long ORIGINAL_DOCUMENT_NO = 1L;

    public static void onBeforeDataSourceFetch(IUIDataSource dataSource, EntrantRequestDataParam param)
    {
        String name = dataSource.getName();

        if (EDUCATION_ORGUNIT_DS.equals(name))
        {
            param.getFormativeOrgUnit().putParamIfActive(dataSource, PARAM_FORMATIVE_ORG_UNIT);
            param.getTerritorialOrgUnit().putParamIfActive(dataSource, PARAM_TERRITORIAL_ORG_UNIT);
        }
    }

    public static IDefaultComboDataSourceHandler createQualificationDS(String name)
    {
        return (IDefaultComboDataSourceHandler) createPathDS(name, Qualifications.class, EducationOrgUnit.educationLevelHighSchool().educationLevel().qualification()).setOrderByProperty(Qualifications.P_ID);
    }

    public static IDefaultComboDataSourceHandler createFormativeOrgUnitDS(String name)
    {
        return createPathDS(name, OrgUnit.class, EducationOrgUnit.formativeOrgUnit());
    }

    public static IDefaultComboDataSourceHandler createTerritorialOrgUnitDS(String name)
    {
        return createPathDS(name, OrgUnit.class, EducationOrgUnit.territorialOrgUnit());
    }

    public static IDefaultComboDataSourceHandler createEducationOrgUnitDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, EducationLevelsHighSchool.class)
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);

                List<OrgUnit> formativeOrgUnitList = ep.context.get(PARAM_FORMATIVE_ORG_UNIT);
                List<OrgUnit> territorialOrgUnitList = ep.context.get(PARAM_TERRITORIAL_ORG_UNIT);

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "ou")
                        .column(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().id().fromAlias("ou")));

                if (formativeOrgUnitList != null)
                    builder.where(DQLExpressions.in(DQLExpressions.property(EducationOrgUnit.formativeOrgUnit().fromAlias("ou")), formativeOrgUnitList));

                if (territorialOrgUnitList != null)
                    builder.where(DQLExpressions.in(DQLExpressions.property(EducationOrgUnit.territorialOrgUnit().fromAlias("ou")), territorialOrgUnitList));

                ep.dqlBuilder.where(DQLExpressions.in(DQLExpressions.property("e.id"),
                        builder.buildQuery()
                ));
            }
        };
    }

    public static IDefaultComboDataSourceHandler createDevelopFormDS(String name)
    {
        // use EducationCatalogsManager.instance().developFormDSConfig()
        return EducationCatalogsManager.instance().developFormDSHandler();
    }

    public static IDefaultComboDataSourceHandler createDevelopConditionDS(String name)
    {
        // use EducationCatalogsManager.instance().developConditionDSConfig()
        return EducationCatalogsManager.instance().developConditionDSHandler();
    }

    public static IDefaultComboDataSourceHandler createDevelopTechDS(String name)
    {
        // use EducationCatalogsManager.instance().developTechDSConfig()
        return EducationCatalogsManager.instance().developTechDSHandler();
    }

    public static IDefaultComboDataSourceHandler createDevelopPeriodDS(String name)
    {
        // use EducationCatalogsManager.instance().developPeriodDSConfig()
        return EducationCatalogsManager.instance().developPeriodDSHandler();
    }

    public static IDefaultComboDataSourceHandler createCompetitionKindDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, CompetitionKind.class);
    }

    public static IDefaultComboDataSourceHandler createCompensationTypeDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, CompensationType.class, CompensationType.shortTitle());
    }

    public static IDefaultComboDataSourceHandler createStudentCategoryDS(String name)
    {
        return (IDefaultComboDataSourceHandler) new DefaultComboDataSourceHandler(name, StudentCategory.class).setOrderByProperty(StudentCategory.code().s());
    }

    public static IDefaultComboDataSourceHandler createTargetAdmissionDS(String name)
    {
        return new SimpleTitledComboDataSourceHandler(name)
                .addAll(Arrays.asList(new IdentifiableWrapper(TARGET_ADMISSION_YES, "да"), new IdentifiableWrapper(TARGET_ADMISSION_NO, "нет")))
                .filtered(true);
    }

    public static IDefaultComboDataSourceHandler createTargetAdmissionKindDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, TargetAdmissionKind.class);
    }

    public static IDefaultComboDataSourceHandler createEntrantStateDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, EntrantState.class);
    }

    public static IDefaultComboDataSourceHandler createOriginalDocumentsDS(String name)
    {
        return new SimpleTitledComboDataSourceHandler(name)
                .addAll(Arrays.asList(new IdentifiableWrapper(ORIGINAL_DOCUMENT_YES, "да"), new IdentifiableWrapper(ORIGINAL_DOCUMENT_NO, "нет")))
                .filtered(true);
    }

    // private

    public static DefaultComboDataSourceHandler createPathDS(String name, Class<? extends IEntity> clazz, final EntityPath path)
    {
        return new DefaultComboDataSourceHandler(name, clazz)
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);

                ep.dqlBuilder.where(DQLExpressions.in(DQLExpressions.property("e.id"), new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "ou")
                        .column(DQLExpressions.property(path.id().fromAlias("ou")))
                        .buildQuery()
                ));
            }
        };
    }
}
