package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.uniec.entity.catalog.Base4ExamByDifferentSources;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.IEntrant;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * (Старый) Абитуриент
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntrantGen extends PersonRole
 implements IEntrant{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.Entrant";
    public static final String ENTITY_NAME = "entrant";
    public static final int VERSION_HASH = 752296724;
    private static IEntityMeta ENTITY_META;

    public static final String P_ENTRANT_ID = "entrantId";
    public static final String P_PERSONAL_NUMBER = "personalNumber";
    public static final String P_REGISTRATION_DATE = "registrationDate";
    public static final String P_PASS_ARMY = "passArmy";
    public static final String P_BEGIN_ARMY = "beginArmy";
    public static final String P_END_ARMY = "endArmy";
    public static final String P_END_ARMY_YEAR = "endArmyYear";
    public static final String P_PASS_PROFILE_EDUCATION = "passProfileEducation";
    public static final String L_STATE = "state";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_ARCHIVAL = "archival";
    public static final String P_ADDITIONAL_INFO = "additionalInfo";
    public static final String P_SPECIAL_CONDITION4_EXAM = "specialCondition4Exam";
    public static final String L_BASE4_EXAM_BY_DIFFERENT_SOURCES = "base4ExamByDifferentSources";
    public static final String P_FROM_CRIMEA = "fromCrimea";

    private long _entrantId; 
    private String _personalNumber;     // Личный номер
    private Date _registrationDate;     // Дата добавления
    private boolean _passArmy;     // Служил в армии
    private Date _beginArmy;     // Дата начала службы
    private Date _endArmy;     // Дата окончания службы
    private Integer _endArmyYear;     // Год увольнения в запас
    private boolean _passProfileEducation;     // Закончил профильное образовательное учреждение
    private EntrantState _state;     // Состояние абитуриента
    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private boolean _archival;     // Архивный
    private String _additionalInfo;     // Дополнительная информация
    private boolean _specialCondition4Exam = false;     // Специальные условия для сдачи ВИ
    private Base4ExamByDifferentSources _base4ExamByDifferentSources;     // Основание для сдачи ВИ по материалам ОУ
    private boolean _fromCrimea = false;     // Гражданин Крыма

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return  Свойство не может быть null.
     *
     * Это формула "id".
     */
    // @NotNull
    public long getEntrantId()
    {
        return _entrantId;
    }

    /**
     * @param entrantId  Свойство не может быть null.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setEntrantId(long entrantId)
    {
        dirty(_entrantId, entrantId);
        _entrantId = entrantId;
    }

    /**
     * @return Личный номер. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPersonalNumber()
    {
        return _personalNumber;
    }

    /**
     * @param personalNumber Личный номер. Свойство не может быть null.
     */
    public void setPersonalNumber(String personalNumber)
    {
        dirty(_personalNumber, personalNumber);
        _personalNumber = personalNumber;
    }

    /**
     * @return Дата добавления. Свойство не может быть null.
     */
    @NotNull
    public Date getRegistrationDate()
    {
        return _registrationDate;
    }

    /**
     * @param registrationDate Дата добавления. Свойство не может быть null.
     */
    public void setRegistrationDate(Date registrationDate)
    {
        dirty(_registrationDate, registrationDate);
        _registrationDate = registrationDate;
    }

    /**
     * @return Служил в армии. Свойство не может быть null.
     */
    @NotNull
    public boolean isPassArmy()
    {
        return _passArmy;
    }

    /**
     * @param passArmy Служил в армии. Свойство не может быть null.
     */
    public void setPassArmy(boolean passArmy)
    {
        dirty(_passArmy, passArmy);
        _passArmy = passArmy;
    }

    /**
     * @return Дата начала службы.
     */
    public Date getBeginArmy()
    {
        return _beginArmy;
    }

    /**
     * @param beginArmy Дата начала службы.
     */
    public void setBeginArmy(Date beginArmy)
    {
        dirty(_beginArmy, beginArmy);
        _beginArmy = beginArmy;
    }

    /**
     * @return Дата окончания службы.
     */
    public Date getEndArmy()
    {
        return _endArmy;
    }

    /**
     * @param endArmy Дата окончания службы.
     */
    public void setEndArmy(Date endArmy)
    {
        dirty(_endArmy, endArmy);
        _endArmy = endArmy;
    }

    /**
     * @return Год увольнения в запас.
     */
    public Integer getEndArmyYear()
    {
        return _endArmyYear;
    }

    /**
     * @param endArmyYear Год увольнения в запас.
     */
    public void setEndArmyYear(Integer endArmyYear)
    {
        dirty(_endArmyYear, endArmyYear);
        _endArmyYear = endArmyYear;
    }

    /**
     * @return Закончил профильное образовательное учреждение. Свойство не может быть null.
     */
    @NotNull
    public boolean isPassProfileEducation()
    {
        return _passProfileEducation;
    }

    /**
     * @param passProfileEducation Закончил профильное образовательное учреждение. Свойство не может быть null.
     */
    public void setPassProfileEducation(boolean passProfileEducation)
    {
        dirty(_passProfileEducation, passProfileEducation);
        _passProfileEducation = passProfileEducation;
    }

    /**
     * @return Состояние абитуриента.
     */
    public EntrantState getState()
    {
        return _state;
    }

    /**
     * @param state Состояние абитуриента.
     */
    public void setState(EntrantState state)
    {
        dirty(_state, state);
        _state = state;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Архивный. Свойство не может быть null.
     */
    @NotNull
    public boolean isArchival()
    {
        return _archival;
    }

    /**
     * @param archival Архивный. Свойство не может быть null.
     */
    public void setArchival(boolean archival)
    {
        dirty(_archival, archival);
        _archival = archival;
    }

    /**
     * @return Дополнительная информация.
     */
    @Length(max=255)
    public String getAdditionalInfo()
    {
        return _additionalInfo;
    }

    /**
     * @param additionalInfo Дополнительная информация.
     */
    public void setAdditionalInfo(String additionalInfo)
    {
        dirty(_additionalInfo, additionalInfo);
        _additionalInfo = additionalInfo;
    }

    /**
     * @return Специальные условия для сдачи ВИ. Свойство не может быть null.
     */
    @NotNull
    public boolean isSpecialCondition4Exam()
    {
        return _specialCondition4Exam;
    }

    /**
     * @param specialCondition4Exam Специальные условия для сдачи ВИ. Свойство не может быть null.
     */
    public void setSpecialCondition4Exam(boolean specialCondition4Exam)
    {
        dirty(_specialCondition4Exam, specialCondition4Exam);
        _specialCondition4Exam = specialCondition4Exam;
    }

    /**
     * @return Основание для сдачи ВИ по материалам ОУ.
     */
    public Base4ExamByDifferentSources getBase4ExamByDifferentSources()
    {
        return _base4ExamByDifferentSources;
    }

    /**
     * @param base4ExamByDifferentSources Основание для сдачи ВИ по материалам ОУ.
     */
    public void setBase4ExamByDifferentSources(Base4ExamByDifferentSources base4ExamByDifferentSources)
    {
        dirty(_base4ExamByDifferentSources, base4ExamByDifferentSources);
        _base4ExamByDifferentSources = base4ExamByDifferentSources;
    }

    /**
     * @return Гражданин Крыма. Свойство не может быть null.
     */
    @NotNull
    public boolean isFromCrimea()
    {
        return _fromCrimea;
    }

    /**
     * @param fromCrimea Гражданин Крыма. Свойство не может быть null.
     */
    public void setFromCrimea(boolean fromCrimea)
    {
        dirty(_fromCrimea, fromCrimea);
        _fromCrimea = fromCrimea;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EntrantGen)
        {
            setEntrantId(((Entrant)another).getEntrantId());
            setPersonalNumber(((Entrant)another).getPersonalNumber());
            setRegistrationDate(((Entrant)another).getRegistrationDate());
            setPassArmy(((Entrant)another).isPassArmy());
            setBeginArmy(((Entrant)another).getBeginArmy());
            setEndArmy(((Entrant)another).getEndArmy());
            setEndArmyYear(((Entrant)another).getEndArmyYear());
            setPassProfileEducation(((Entrant)another).isPassProfileEducation());
            setState(((Entrant)another).getState());
            setEnrollmentCampaign(((Entrant)another).getEnrollmentCampaign());
            setArchival(((Entrant)another).isArchival());
            setAdditionalInfo(((Entrant)another).getAdditionalInfo());
            setSpecialCondition4Exam(((Entrant)another).isSpecialCondition4Exam());
            setBase4ExamByDifferentSources(((Entrant)another).getBase4ExamByDifferentSources());
            setFromCrimea(((Entrant)another).isFromCrimea());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntrantGen> extends PersonRole.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) Entrant.class;
        }

        public T newInstance()
        {
            return (T) new Entrant();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "entrantId":
                    return obj.getEntrantId();
                case "personalNumber":
                    return obj.getPersonalNumber();
                case "registrationDate":
                    return obj.getRegistrationDate();
                case "passArmy":
                    return obj.isPassArmy();
                case "beginArmy":
                    return obj.getBeginArmy();
                case "endArmy":
                    return obj.getEndArmy();
                case "endArmyYear":
                    return obj.getEndArmyYear();
                case "passProfileEducation":
                    return obj.isPassProfileEducation();
                case "state":
                    return obj.getState();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "archival":
                    return obj.isArchival();
                case "additionalInfo":
                    return obj.getAdditionalInfo();
                case "specialCondition4Exam":
                    return obj.isSpecialCondition4Exam();
                case "base4ExamByDifferentSources":
                    return obj.getBase4ExamByDifferentSources();
                case "fromCrimea":
                    return obj.isFromCrimea();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "entrantId":
                    obj.setEntrantId((Long) value);
                    return;
                case "personalNumber":
                    obj.setPersonalNumber((String) value);
                    return;
                case "registrationDate":
                    obj.setRegistrationDate((Date) value);
                    return;
                case "passArmy":
                    obj.setPassArmy((Boolean) value);
                    return;
                case "beginArmy":
                    obj.setBeginArmy((Date) value);
                    return;
                case "endArmy":
                    obj.setEndArmy((Date) value);
                    return;
                case "endArmyYear":
                    obj.setEndArmyYear((Integer) value);
                    return;
                case "passProfileEducation":
                    obj.setPassProfileEducation((Boolean) value);
                    return;
                case "state":
                    obj.setState((EntrantState) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "archival":
                    obj.setArchival((Boolean) value);
                    return;
                case "additionalInfo":
                    obj.setAdditionalInfo((String) value);
                    return;
                case "specialCondition4Exam":
                    obj.setSpecialCondition4Exam((Boolean) value);
                    return;
                case "base4ExamByDifferentSources":
                    obj.setBase4ExamByDifferentSources((Base4ExamByDifferentSources) value);
                    return;
                case "fromCrimea":
                    obj.setFromCrimea((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "entrantId":
                        return true;
                case "personalNumber":
                        return true;
                case "registrationDate":
                        return true;
                case "passArmy":
                        return true;
                case "beginArmy":
                        return true;
                case "endArmy":
                        return true;
                case "endArmyYear":
                        return true;
                case "passProfileEducation":
                        return true;
                case "state":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "archival":
                        return true;
                case "additionalInfo":
                        return true;
                case "specialCondition4Exam":
                        return true;
                case "base4ExamByDifferentSources":
                        return true;
                case "fromCrimea":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "entrantId":
                    return true;
                case "personalNumber":
                    return true;
                case "registrationDate":
                    return true;
                case "passArmy":
                    return true;
                case "beginArmy":
                    return true;
                case "endArmy":
                    return true;
                case "endArmyYear":
                    return true;
                case "passProfileEducation":
                    return true;
                case "state":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "archival":
                    return true;
                case "additionalInfo":
                    return true;
                case "specialCondition4Exam":
                    return true;
                case "base4ExamByDifferentSources":
                    return true;
                case "fromCrimea":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "entrantId":
                    return Long.class;
                case "personalNumber":
                    return String.class;
                case "registrationDate":
                    return Date.class;
                case "passArmy":
                    return Boolean.class;
                case "beginArmy":
                    return Date.class;
                case "endArmy":
                    return Date.class;
                case "endArmyYear":
                    return Integer.class;
                case "passProfileEducation":
                    return Boolean.class;
                case "state":
                    return EntrantState.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "archival":
                    return Boolean.class;
                case "additionalInfo":
                    return String.class;
                case "specialCondition4Exam":
                    return Boolean.class;
                case "base4ExamByDifferentSources":
                    return Base4ExamByDifferentSources.class;
                case "fromCrimea":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<Entrant> _dslPath = new Path<Entrant>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "Entrant");
    }
            

    /**
     * @return  Свойство не может быть null.
     *
     * Это формула "id".
     * @see ru.tandemservice.uniec.entity.entrant.Entrant#getEntrantId()
     */
    public static PropertyPath<Long> entrantId()
    {
        return _dslPath.entrantId();
    }

    /**
     * @return Личный номер. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.Entrant#getPersonalNumber()
     */
    public static PropertyPath<String> personalNumber()
    {
        return _dslPath.personalNumber();
    }

    /**
     * @return Дата добавления. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.Entrant#getRegistrationDate()
     */
    public static PropertyPath<Date> registrationDate()
    {
        return _dslPath.registrationDate();
    }

    /**
     * @return Служил в армии. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.Entrant#isPassArmy()
     */
    public static PropertyPath<Boolean> passArmy()
    {
        return _dslPath.passArmy();
    }

    /**
     * @return Дата начала службы.
     * @see ru.tandemservice.uniec.entity.entrant.Entrant#getBeginArmy()
     */
    public static PropertyPath<Date> beginArmy()
    {
        return _dslPath.beginArmy();
    }

    /**
     * @return Дата окончания службы.
     * @see ru.tandemservice.uniec.entity.entrant.Entrant#getEndArmy()
     */
    public static PropertyPath<Date> endArmy()
    {
        return _dslPath.endArmy();
    }

    /**
     * @return Год увольнения в запас.
     * @see ru.tandemservice.uniec.entity.entrant.Entrant#getEndArmyYear()
     */
    public static PropertyPath<Integer> endArmyYear()
    {
        return _dslPath.endArmyYear();
    }

    /**
     * @return Закончил профильное образовательное учреждение. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.Entrant#isPassProfileEducation()
     */
    public static PropertyPath<Boolean> passProfileEducation()
    {
        return _dslPath.passProfileEducation();
    }

    /**
     * @return Состояние абитуриента.
     * @see ru.tandemservice.uniec.entity.entrant.Entrant#getState()
     */
    public static EntrantState.Path<EntrantState> state()
    {
        return _dslPath.state();
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.Entrant#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Архивный. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.Entrant#isArchival()
     */
    public static PropertyPath<Boolean> archival()
    {
        return _dslPath.archival();
    }

    /**
     * @return Дополнительная информация.
     * @see ru.tandemservice.uniec.entity.entrant.Entrant#getAdditionalInfo()
     */
    public static PropertyPath<String> additionalInfo()
    {
        return _dslPath.additionalInfo();
    }

    /**
     * @return Специальные условия для сдачи ВИ. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.Entrant#isSpecialCondition4Exam()
     */
    public static PropertyPath<Boolean> specialCondition4Exam()
    {
        return _dslPath.specialCondition4Exam();
    }

    /**
     * @return Основание для сдачи ВИ по материалам ОУ.
     * @see ru.tandemservice.uniec.entity.entrant.Entrant#getBase4ExamByDifferentSources()
     */
    public static Base4ExamByDifferentSources.Path<Base4ExamByDifferentSources> base4ExamByDifferentSources()
    {
        return _dslPath.base4ExamByDifferentSources();
    }

    /**
     * @return Гражданин Крыма. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.Entrant#isFromCrimea()
     */
    public static PropertyPath<Boolean> fromCrimea()
    {
        return _dslPath.fromCrimea();
    }

    public static class Path<E extends Entrant> extends PersonRole.Path<E>
    {
        private PropertyPath<Long> _entrantId;
        private PropertyPath<String> _personalNumber;
        private PropertyPath<Date> _registrationDate;
        private PropertyPath<Boolean> _passArmy;
        private PropertyPath<Date> _beginArmy;
        private PropertyPath<Date> _endArmy;
        private PropertyPath<Integer> _endArmyYear;
        private PropertyPath<Boolean> _passProfileEducation;
        private EntrantState.Path<EntrantState> _state;
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<Boolean> _archival;
        private PropertyPath<String> _additionalInfo;
        private PropertyPath<Boolean> _specialCondition4Exam;
        private Base4ExamByDifferentSources.Path<Base4ExamByDifferentSources> _base4ExamByDifferentSources;
        private PropertyPath<Boolean> _fromCrimea;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return  Свойство не может быть null.
     *
     * Это формула "id".
     * @see ru.tandemservice.uniec.entity.entrant.Entrant#getEntrantId()
     */
        public PropertyPath<Long> entrantId()
        {
            if(_entrantId == null )
                _entrantId = new PropertyPath<Long>(EntrantGen.P_ENTRANT_ID, this);
            return _entrantId;
        }

    /**
     * @return Личный номер. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.Entrant#getPersonalNumber()
     */
        public PropertyPath<String> personalNumber()
        {
            if(_personalNumber == null )
                _personalNumber = new PropertyPath<String>(EntrantGen.P_PERSONAL_NUMBER, this);
            return _personalNumber;
        }

    /**
     * @return Дата добавления. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.Entrant#getRegistrationDate()
     */
        public PropertyPath<Date> registrationDate()
        {
            if(_registrationDate == null )
                _registrationDate = new PropertyPath<Date>(EntrantGen.P_REGISTRATION_DATE, this);
            return _registrationDate;
        }

    /**
     * @return Служил в армии. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.Entrant#isPassArmy()
     */
        public PropertyPath<Boolean> passArmy()
        {
            if(_passArmy == null )
                _passArmy = new PropertyPath<Boolean>(EntrantGen.P_PASS_ARMY, this);
            return _passArmy;
        }

    /**
     * @return Дата начала службы.
     * @see ru.tandemservice.uniec.entity.entrant.Entrant#getBeginArmy()
     */
        public PropertyPath<Date> beginArmy()
        {
            if(_beginArmy == null )
                _beginArmy = new PropertyPath<Date>(EntrantGen.P_BEGIN_ARMY, this);
            return _beginArmy;
        }

    /**
     * @return Дата окончания службы.
     * @see ru.tandemservice.uniec.entity.entrant.Entrant#getEndArmy()
     */
        public PropertyPath<Date> endArmy()
        {
            if(_endArmy == null )
                _endArmy = new PropertyPath<Date>(EntrantGen.P_END_ARMY, this);
            return _endArmy;
        }

    /**
     * @return Год увольнения в запас.
     * @see ru.tandemservice.uniec.entity.entrant.Entrant#getEndArmyYear()
     */
        public PropertyPath<Integer> endArmyYear()
        {
            if(_endArmyYear == null )
                _endArmyYear = new PropertyPath<Integer>(EntrantGen.P_END_ARMY_YEAR, this);
            return _endArmyYear;
        }

    /**
     * @return Закончил профильное образовательное учреждение. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.Entrant#isPassProfileEducation()
     */
        public PropertyPath<Boolean> passProfileEducation()
        {
            if(_passProfileEducation == null )
                _passProfileEducation = new PropertyPath<Boolean>(EntrantGen.P_PASS_PROFILE_EDUCATION, this);
            return _passProfileEducation;
        }

    /**
     * @return Состояние абитуриента.
     * @see ru.tandemservice.uniec.entity.entrant.Entrant#getState()
     */
        public EntrantState.Path<EntrantState> state()
        {
            if(_state == null )
                _state = new EntrantState.Path<EntrantState>(L_STATE, this);
            return _state;
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.Entrant#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Архивный. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.Entrant#isArchival()
     */
        public PropertyPath<Boolean> archival()
        {
            if(_archival == null )
                _archival = new PropertyPath<Boolean>(EntrantGen.P_ARCHIVAL, this);
            return _archival;
        }

    /**
     * @return Дополнительная информация.
     * @see ru.tandemservice.uniec.entity.entrant.Entrant#getAdditionalInfo()
     */
        public PropertyPath<String> additionalInfo()
        {
            if(_additionalInfo == null )
                _additionalInfo = new PropertyPath<String>(EntrantGen.P_ADDITIONAL_INFO, this);
            return _additionalInfo;
        }

    /**
     * @return Специальные условия для сдачи ВИ. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.Entrant#isSpecialCondition4Exam()
     */
        public PropertyPath<Boolean> specialCondition4Exam()
        {
            if(_specialCondition4Exam == null )
                _specialCondition4Exam = new PropertyPath<Boolean>(EntrantGen.P_SPECIAL_CONDITION4_EXAM, this);
            return _specialCondition4Exam;
        }

    /**
     * @return Основание для сдачи ВИ по материалам ОУ.
     * @see ru.tandemservice.uniec.entity.entrant.Entrant#getBase4ExamByDifferentSources()
     */
        public Base4ExamByDifferentSources.Path<Base4ExamByDifferentSources> base4ExamByDifferentSources()
        {
            if(_base4ExamByDifferentSources == null )
                _base4ExamByDifferentSources = new Base4ExamByDifferentSources.Path<Base4ExamByDifferentSources>(L_BASE4_EXAM_BY_DIFFERENT_SOURCES, this);
            return _base4ExamByDifferentSources;
        }

    /**
     * @return Гражданин Крыма. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.Entrant#isFromCrimea()
     */
        public PropertyPath<Boolean> fromCrimea()
        {
            if(_fromCrimea == null )
                _fromCrimea = new PropertyPath<Boolean>(EntrantGen.P_FROM_CRIMEA, this);
            return _fromCrimea;
        }

        public Class getEntityClass()
        {
            return Entrant.class;
        }

        public String getEntityName()
        {
            return "entrant";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
