// Copyright 2006-2012 Tandem Service Software
// Proprietary Licence for Tandem University

package ru.tandemservice.uniec.ws.distribution;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Информация о распределении абитуриентов для зачисления
 *
 * @author Vasily Zhukov
 * @since 24.07.2011
 */
@XmlRootElement
public class EnrollmentDistributionEnvironmentNode
{
    /**
     * Название приемной кампании
     */
    @XmlAttribute(required = true)
    public String enrollmentCampaignTitle;

    /**
     * Название года обучения приемной кампании
     */
    @XmlAttribute(required = true)
    public String educationYearTitle;

    /**
     * Дата обращения к веб-сервису
     */
    @XmlAttribute(required = true)
    public Date currentDate;

    /**
     * Все виды целевого приема в указанной приемной кампании
     */
    public TargetAdmissionKindNode targetAdmissionKind = new TargetAdmissionKindNode();

    /**
     * Все виды конкурса
     */
    public CompetitionKindNode competitionKind = new CompetitionKindNode();

    /**
     * Все льготы
     */
    public BenefitNode benefit = new BenefitNode();

    /**
     * Все типы приказов о зачислении
     */
    public EnrollmentOrderTypeNode orderType = new EnrollmentOrderTypeNode();

    /**
     * Все состояния распределений
     */
    public DistributionStateNode distributionState = new DistributionStateNode();

    /**
     * Все состояния документов у абитуриентов из распределения
     */
    public DocumentStateNode documentState = new DocumentStateNode();

    /**
     * Все состояния зачисления у абитуриентов из распределения
     */
    public EnrollmentStateNode enrollmentState = new EnrollmentStateNode();

    /**
     * Все распределения
     */
    public DistributionNode distribution = new DistributionNode();

    /**
     * Все распределения
     */
    public static class DistributionNode
    {
        public static class DistributionRow
        {
            /**
             * Идентификатор объекта на который создается распределение
             * Это может быть направление приема или конкурсная группа
             */
            @XmlAttribute(required = true)
            public String ecgItemId;

            /**
             * Название объекта, на который создается распределение
             */
            @XmlAttribute(required = true)
            public String ecgItemTitle;

            /**
             * Код вида возмещения затрат
             */
            @XmlAttribute(required = true)
            public String compensationTypeId;

            /**
             * True, если распределение для категории поступающих на второе высшее
             * False, если распределение для категорий поступающих студент или слушатель
             */
            @XmlAttribute(required = true)
            public boolean secondHighAdmission;

            /**
             * Номер волны распределения (счет с единицы)
             */
            @XmlAttribute(required = true)
            public int wave;

            /**
             * True, если распределение является уточняющим
             * False, если распределение является основным
             */
            @XmlAttribute(required = true)
            public boolean distributionDetail;

            /**
             * Состояние распределения
             *
             * @see DistributionStateNode
             */
            @XmlAttribute(required = true)
            public String state;

            /**
             * Все дисциплины набора вступительных испытаний для распределения
             */
            public DisciplineNode discipline = new DisciplineNode();

            /**
             * Планы по направлениям приема из распределения
             */
            public QuotaNode quota = new QuotaNode();

            /**
             * Список абитуриентов из распределения
             */
            public EntrantNode entrant = new EntrantNode();
        }

        public List<DistributionRow> row = new ArrayList<DistributionRow>();
    }

    /**
     * План по направлению приема
     */
    public static class QuotaNode
    {
        public static class QuotaRow
        {
            public QuotaRow()
            {
            }

            public QuotaRow(String id, int count, int countBegin, boolean defaultTaKind, TaQuotaNode taQuota)
            {
                this.id = id;
                this.count = count;
                this.countBegin = countBegin;
                this.defaultTaKind = defaultTaKind;
                this.taQuota = taQuota;
            }

            /**
             * Идентификатор направления приема
             */
            @XmlAttribute(required = true)
            public String id;

            /**
             * Количество мест (план) в текущем распределении
             */
            @XmlAttribute(required = true)
            public int count;

            /**
             * Количество мест (план) из распределения первой волны
             */
            @XmlAttribute(required = true)
            public int countBegin;

            /**
             * True, если не используется разделение планов цп по видам,
             * (то есть используется только один вид цп и он вид цп по умолчанию)
             * False, если планы по видам цп разделяются по видам
             */
            @XmlAttribute(required = true)
            public boolean defaultTaKind;

            /**
             * План по виду цп в направлении приема
             */
            public static class TaQuotaNode
            {
                public static class TaQuotaRow
                {
                    public TaQuotaRow()
                    {
                    }

                    public TaQuotaRow(String id, int count, int countBegin)
                    {
                        this.id = id;
                        this.count = count;
                        this.countBegin = countBegin;
                    }

                    /**
                     * Идентификатор вида цп
                     */
                    @XmlAttribute(required = true)
                    public String id;

                    /**
                     * Количество мест (план по цп) в текущем распределении
                     */
                    @XmlAttribute(required = true)
                    public int count;

                    /**
                     * Количество мест (план по цп) из распределения первой волны
                     */
                    @XmlAttribute(required = true)
                    public int countBegin;
                }

                public List<TaQuotaRow> row = new ArrayList<TaQuotaRow>();
            }

            /**
             * Планы приема по видам цп по направлению приема
             */
            public TaQuotaNode taQuota = new TaQuotaNode();
        }

        public List<QuotaRow> row = new ArrayList<QuotaRow>();
    }

    /**
     * Абитуриент из распределения
     */
    public static class EntrantNode
    {
        public static class EntrantRow
        {
            /**
             * Приоритеты НП абитуриента из распределения
             */
            @XmlAttribute
            @XmlList
            public List<Long> priorityIds;

            /**
             * Идентификатор НП студента предзачисления
             */
            @XmlAttribute
            public String preEnrollId;

            /**
             * ФИО абитуриента
             */
            @XmlAttribute(required = true)
            public String fio;

            /**
             * Вид конкурса
             *
             * @see CompetitionKindNode
             */
            @XmlAttribute
            public String competitionKind;

            /**
             * Льготы персоны
             *
             * @see BenefitNode
             */
            @XmlAttribute
            @XmlList
            public List<String> benefitList;

            /**
             * Средний бал из основного законченного образовательного учреждения
             */
            @XmlAttribute
            public String averageEduInstitutionMark;

            /**
             * Балл по профильному вступительному испытанию
             */
            @XmlAttribute
            public String profileMark;

            /**
             * Сумма баллов (финальная оценка по выбранному направлению приема)
             */
            @XmlAttribute
            public String finalMark;

            /**
             * Оценки по дисциплинам вступительных испытаний
             * Каждая оценка может быть:
             * 1. x, если не должен сдавать
             * 2. -, если должен сдавать, но еще не сдавал
             * 3. число, если есть финальная оценка (если шкала перевода содержит шаг 0.5, то финальная оценка может быть дробной с двумя знаками после запятой)
             *
             * @see DisciplineNode
             */
            @XmlAttribute
            @XmlList
            public List<String> marks;

            /**
             * Вид цп, по которому рекомендован
             */
            @XmlAttribute
            public String targetAdmissionKind;

            /**
             * Состояние документов
             *
             * @see DocumentStateNode
             */
            @XmlAttribute
            public String documentState;

            /**
             * Состояние зачисления
             *
             * @see EnrollmentStateNode
             */
            @XmlAttribute
            public String enrollmentState;

            /**
             * Идентификатор приказа о зачислении
             */
            @XmlAttribute
            public String orderId;

            /**
             * Номер приказа о зачислении
             */
            @XmlAttribute
            public String orderNumber;

            /**
             * Тип приказа о зачислении
             *
             * @see EnrollmentOrderTypeNode
             */
            @XmlAttribute
            public String orderType;

            /**
             * Направление приема
             */
            @XmlAttribute
            public String enrollmentDirection;

            /**
             * Регистрационный номер заявления
             */
            @XmlAttribute
            public String requestNumber;

            /**
             * Регистрационный номер выбранного направления приема (специальности)
             */
            @XmlAttribute
            public String regNumber;

            /**
             * Идентификатор абитуриента
             */
            @XmlAttribute(required = true)
            public String entrantId;
        }

        public List<EntrantRow> row = new ArrayList<EntrantRow>();
    }

    /**
     * Простая строка таблицы
     */
    public static class Row implements Comparable<Row>
    {
        public Row()
        {
        }

        public Row(String title, String id)
        {
            this.title = title;
            this.id = id;
        }

        public Row(String shortTitle, String title, String id)
        {
            this.shortTitle = shortTitle;
            this.title = title;
            this.id = id;
        }

        @Override
        public int compareTo(Row o)
        {
            return o.id.compareTo(o.toString());
        }

        /**
         * Сокращенное название
         */
        @XmlAttribute
        public String shortTitle;

        /**
         * Название
         */
        @XmlAttribute(required = true)
        public String title;

        /**
         * Идентификатор строки
         */
        @XmlAttribute(required = true)
        public String id;
    }

    /**
     * Все виды целевого приема в указанной приемной кампании
     */
    public static class TargetAdmissionKindNode
    {
        public static class TargetAdmissionKindRow extends Row
        {
            public TargetAdmissionKindRow()
            {
            }

            public TargetAdmissionKindRow(String shortTitle, String title, String id, String parent, int priority, boolean used)
            {
                super(shortTitle, title, id);
                this.parent = parent;
                this.priority = priority;
                this.used = used;
            }

            /**
             * Родительский вид целевого приема
             *
             * @see TargetAdmissionKindNode
             */
            @XmlAttribute
            public String parent;

            /**
             * Приоритет
             */
            @XmlAttribute(required = true)
            public int priority;

            /**
             * Используется ли в текущей приемной кампании
             * true - используется; false - не используется
             */
            @XmlAttribute(required = true)
            public boolean used;
        }

        public List<TargetAdmissionKindRow> row = new ArrayList<TargetAdmissionKindRow>();
    }

    /**
     * Все виды конкурса
     */
    public static class CompetitionKindNode
    {
        public static class CompetitionKindRow extends Row
        {
            public CompetitionKindRow()
            {
            }

            public CompetitionKindRow(String shortTitle, String title, String id, boolean used)
            {
                super(shortTitle, title, id);
                this.used = used;
            }

            /**
             * Используется ли в текущей приемной кампании
             * true - используется; false - не используется
             */
            @XmlAttribute(required = true)
            public boolean used;
        }

        public List<CompetitionKindRow> row = new ArrayList<CompetitionKindRow>();
    }

    /**
     * Все льготы
     */
    public static class BenefitNode
    {
        public static class BenefitRow extends Row
        {
            public BenefitRow()
            {
            }

            public BenefitRow(String shortTitle, String title, String id, boolean used)
            {
                super(shortTitle, title, id);
                this.used = used;
            }

            /**
             * Используется ли в текущей приемной кампании
             * true - используется; false - не используется
             */
            @XmlAttribute(required = true)
            public boolean used;
        }

        public List<BenefitRow> row = new ArrayList<BenefitRow>();
    }

    public static class EnrollmentOrderTypeNode
    {
        public static class EnrollmentOrderTypeRow extends Row
        {
            public EnrollmentOrderTypeRow()
            {
            }

            public EnrollmentOrderTypeRow(String shortTitle, String title, String id, boolean used)
            {
                super(shortTitle, title, id);
                this.used = used;
            }

            /**
             * Используется ли в текущей приемной кампании
             * true - используется; false - не используется
             */
            @XmlAttribute(required = true)
            public boolean used;
        }

        public List<EnrollmentOrderTypeRow> row = new ArrayList<EnrollmentOrderTypeRow>();
    }

    public static class DistributionStateNode
    {
        public List<Row> row = new ArrayList<Row>();
    }

    public static class DocumentStateNode
    {
        public List<Row> row = new ArrayList<Row>();
    }

    public static class EnrollmentStateNode
    {
        public List<Row> row = new ArrayList<Row>();
    }

    /**
     * Все дисциплины набора вступительных испытаний
     */
    public static class DisciplineNode
    {
        /**
         * Дисциплина набора вступительного испытания
         */
        public static class DisciplineRow
        {
            public DisciplineRow()
            {
            }

            public DisciplineRow(String title, String shortTitle, String id)
            {
                this.title = title;
                this.shortTitle = shortTitle;
                this.id = id;
            }

            /**
             * Название дисциплины набора вступительного испытания
             *
             * @see ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation
             */
            @XmlAttribute
            public String title;

            /**
             * Сокращенное название дисциплины набора вступительного испытания
             *
             * @see ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation
             */
            @XmlAttribute
            public String shortTitle;

            /**
             * Идентификатор дисциплины набора вступительного испытания
             *
             * @see ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation
             */
            @XmlAttribute(required = true)
            public String id;
        }

        public List<DisciplineRow> row = new ArrayList<DisciplineRow>();
    }
}
