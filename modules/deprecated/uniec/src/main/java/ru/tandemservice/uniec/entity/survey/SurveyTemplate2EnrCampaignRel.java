package ru.tandemservice.uniec.entity.survey;

import org.tandemframework.shared.survey.base.entity.SurveyTemplate;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.survey.gen.*;

/** @see ru.tandemservice.uniec.entity.survey.gen.SurveyTemplate2EnrCampaignRelGen */
public class SurveyTemplate2EnrCampaignRel extends SurveyTemplate2EnrCampaignRelGen
{

    public SurveyTemplate2EnrCampaignRel()
    {
    }

    public SurveyTemplate2EnrCampaignRel(EnrollmentCampaign campaign, SurveyTemplate template)
    {
        setCampaign(campaign);
        setTemplate(template);
    }
}