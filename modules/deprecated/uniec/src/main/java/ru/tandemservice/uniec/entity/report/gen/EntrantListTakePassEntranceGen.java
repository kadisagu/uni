package ru.tandemservice.uniec.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.report.EntrantListTakePassEntrance;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Список абитуриентов, идущих на сдачу вступительных испытаний в ОУ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntrantListTakePassEntranceGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.report.EntrantListTakePassEntrance";
    public static final String ENTITY_NAME = "entrantListTakePassEntrance";
    public static final int VERSION_HASH = 1515599811;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONTENT = "content";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String P_DATE_FROM = "dateFrom";
    public static final String P_DATE_TO = "dateTo";
    public static final String P_EXAM_TITLE = "examTitle";
    public static final String P_QUALIFICATION_TITLE = "qualificationTitle";
    public static final String P_STUDENT_CATEGORY_TITLE = "studentCategoryTitle";
    public static final String P_FORMATIVE_ORG_UNIT_TITLE = "formativeOrgUnitTitle";
    public static final String P_TERRITORIAL_ORG_UNIT_TITLE = "territorialOrgUnitTitle";
    public static final String P_EDUCATION_LEVEL_HIGH_SCHOOL_TITLE = "educationLevelHighSchoolTitle";
    public static final String P_DEVELOP_FORM_TITLE = "developFormTitle";
    public static final String P_DEVELOP_CONDITION_TITLE = "developConditionTitle";
    public static final String P_DEVELOP_TECH_TITLE = "developTechTitle";
    public static final String P_DEVELOP_PERIOD_TITLE = "developPeriodTitle";

    private DatabaseFile _content;     // Печатная форма
    private Date _formingDate;     // Дата формирования
    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private CompensationType _compensationType;     // Вид возмещения затрат
    private Date _dateFrom;     // Заявления с
    private Date _dateTo;     // Заявления по
    private String _examTitle; 
    private String _qualificationTitle;     // Квалификация
    private String _studentCategoryTitle;     // Категория поступающего
    private String _formativeOrgUnitTitle;     // Формирующее подр.
    private String _territorialOrgUnitTitle;     // Территориальное подр.
    private String _educationLevelHighSchoolTitle;     // Уровень образования ОУ подразделение
    private String _developFormTitle;     // Форма освоения
    private String _developConditionTitle;     // Условие освоения
    private String _developTechTitle;     // Технология освоения
    private String _developPeriodTitle;     // Срок освоения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     */
    @NotNull
    public Date getDateFrom()
    {
        return _dateFrom;
    }

    /**
     * @param dateFrom Заявления с. Свойство не может быть null.
     */
    public void setDateFrom(Date dateFrom)
    {
        dirty(_dateFrom, dateFrom);
        _dateFrom = dateFrom;
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     */
    @NotNull
    public Date getDateTo()
    {
        return _dateTo;
    }

    /**
     * @param dateTo Заявления по. Свойство не может быть null.
     */
    public void setDateTo(Date dateTo)
    {
        dirty(_dateTo, dateTo);
        _dateTo = dateTo;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getExamTitle()
    {
        return _examTitle;
    }

    /**
     * @param examTitle  Свойство не может быть null.
     */
    public void setExamTitle(String examTitle)
    {
        dirty(_examTitle, examTitle);
        _examTitle = examTitle;
    }

    /**
     * @return Квалификация.
     */
    @Length(max=255)
    public String getQualificationTitle()
    {
        return _qualificationTitle;
    }

    /**
     * @param qualificationTitle Квалификация.
     */
    public void setQualificationTitle(String qualificationTitle)
    {
        dirty(_qualificationTitle, qualificationTitle);
        _qualificationTitle = qualificationTitle;
    }

    /**
     * @return Категория поступающего.
     */
    @Length(max=255)
    public String getStudentCategoryTitle()
    {
        return _studentCategoryTitle;
    }

    /**
     * @param studentCategoryTitle Категория поступающего.
     */
    public void setStudentCategoryTitle(String studentCategoryTitle)
    {
        dirty(_studentCategoryTitle, studentCategoryTitle);
        _studentCategoryTitle = studentCategoryTitle;
    }

    /**
     * @return Формирующее подр..
     */
    public String getFormativeOrgUnitTitle()
    {
        return _formativeOrgUnitTitle;
    }

    /**
     * @param formativeOrgUnitTitle Формирующее подр..
     */
    public void setFormativeOrgUnitTitle(String formativeOrgUnitTitle)
    {
        dirty(_formativeOrgUnitTitle, formativeOrgUnitTitle);
        _formativeOrgUnitTitle = formativeOrgUnitTitle;
    }

    /**
     * @return Территориальное подр..
     */
    public String getTerritorialOrgUnitTitle()
    {
        return _territorialOrgUnitTitle;
    }

    /**
     * @param territorialOrgUnitTitle Территориальное подр..
     */
    public void setTerritorialOrgUnitTitle(String territorialOrgUnitTitle)
    {
        dirty(_territorialOrgUnitTitle, territorialOrgUnitTitle);
        _territorialOrgUnitTitle = territorialOrgUnitTitle;
    }

    /**
     * @return Уровень образования ОУ подразделение.
     */
    public String getEducationLevelHighSchoolTitle()
    {
        return _educationLevelHighSchoolTitle;
    }

    /**
     * @param educationLevelHighSchoolTitle Уровень образования ОУ подразделение.
     */
    public void setEducationLevelHighSchoolTitle(String educationLevelHighSchoolTitle)
    {
        dirty(_educationLevelHighSchoolTitle, educationLevelHighSchoolTitle);
        _educationLevelHighSchoolTitle = educationLevelHighSchoolTitle;
    }

    /**
     * @return Форма освоения.
     */
    @Length(max=255)
    public String getDevelopFormTitle()
    {
        return _developFormTitle;
    }

    /**
     * @param developFormTitle Форма освоения.
     */
    public void setDevelopFormTitle(String developFormTitle)
    {
        dirty(_developFormTitle, developFormTitle);
        _developFormTitle = developFormTitle;
    }

    /**
     * @return Условие освоения.
     */
    @Length(max=255)
    public String getDevelopConditionTitle()
    {
        return _developConditionTitle;
    }

    /**
     * @param developConditionTitle Условие освоения.
     */
    public void setDevelopConditionTitle(String developConditionTitle)
    {
        dirty(_developConditionTitle, developConditionTitle);
        _developConditionTitle = developConditionTitle;
    }

    /**
     * @return Технология освоения.
     */
    @Length(max=255)
    public String getDevelopTechTitle()
    {
        return _developTechTitle;
    }

    /**
     * @param developTechTitle Технология освоения.
     */
    public void setDevelopTechTitle(String developTechTitle)
    {
        dirty(_developTechTitle, developTechTitle);
        _developTechTitle = developTechTitle;
    }

    /**
     * @return Срок освоения.
     */
    @Length(max=255)
    public String getDevelopPeriodTitle()
    {
        return _developPeriodTitle;
    }

    /**
     * @param developPeriodTitle Срок освоения.
     */
    public void setDevelopPeriodTitle(String developPeriodTitle)
    {
        dirty(_developPeriodTitle, developPeriodTitle);
        _developPeriodTitle = developPeriodTitle;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntrantListTakePassEntranceGen)
        {
            setContent(((EntrantListTakePassEntrance)another).getContent());
            setFormingDate(((EntrantListTakePassEntrance)another).getFormingDate());
            setEnrollmentCampaign(((EntrantListTakePassEntrance)another).getEnrollmentCampaign());
            setCompensationType(((EntrantListTakePassEntrance)another).getCompensationType());
            setDateFrom(((EntrantListTakePassEntrance)another).getDateFrom());
            setDateTo(((EntrantListTakePassEntrance)another).getDateTo());
            setExamTitle(((EntrantListTakePassEntrance)another).getExamTitle());
            setQualificationTitle(((EntrantListTakePassEntrance)another).getQualificationTitle());
            setStudentCategoryTitle(((EntrantListTakePassEntrance)another).getStudentCategoryTitle());
            setFormativeOrgUnitTitle(((EntrantListTakePassEntrance)another).getFormativeOrgUnitTitle());
            setTerritorialOrgUnitTitle(((EntrantListTakePassEntrance)another).getTerritorialOrgUnitTitle());
            setEducationLevelHighSchoolTitle(((EntrantListTakePassEntrance)another).getEducationLevelHighSchoolTitle());
            setDevelopFormTitle(((EntrantListTakePassEntrance)another).getDevelopFormTitle());
            setDevelopConditionTitle(((EntrantListTakePassEntrance)another).getDevelopConditionTitle());
            setDevelopTechTitle(((EntrantListTakePassEntrance)another).getDevelopTechTitle());
            setDevelopPeriodTitle(((EntrantListTakePassEntrance)another).getDevelopPeriodTitle());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntrantListTakePassEntranceGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntrantListTakePassEntrance.class;
        }

        public T newInstance()
        {
            return (T) new EntrantListTakePassEntrance();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "content":
                    return obj.getContent();
                case "formingDate":
                    return obj.getFormingDate();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "compensationType":
                    return obj.getCompensationType();
                case "dateFrom":
                    return obj.getDateFrom();
                case "dateTo":
                    return obj.getDateTo();
                case "examTitle":
                    return obj.getExamTitle();
                case "qualificationTitle":
                    return obj.getQualificationTitle();
                case "studentCategoryTitle":
                    return obj.getStudentCategoryTitle();
                case "formativeOrgUnitTitle":
                    return obj.getFormativeOrgUnitTitle();
                case "territorialOrgUnitTitle":
                    return obj.getTerritorialOrgUnitTitle();
                case "educationLevelHighSchoolTitle":
                    return obj.getEducationLevelHighSchoolTitle();
                case "developFormTitle":
                    return obj.getDevelopFormTitle();
                case "developConditionTitle":
                    return obj.getDevelopConditionTitle();
                case "developTechTitle":
                    return obj.getDevelopTechTitle();
                case "developPeriodTitle":
                    return obj.getDevelopPeriodTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "dateFrom":
                    obj.setDateFrom((Date) value);
                    return;
                case "dateTo":
                    obj.setDateTo((Date) value);
                    return;
                case "examTitle":
                    obj.setExamTitle((String) value);
                    return;
                case "qualificationTitle":
                    obj.setQualificationTitle((String) value);
                    return;
                case "studentCategoryTitle":
                    obj.setStudentCategoryTitle((String) value);
                    return;
                case "formativeOrgUnitTitle":
                    obj.setFormativeOrgUnitTitle((String) value);
                    return;
                case "territorialOrgUnitTitle":
                    obj.setTerritorialOrgUnitTitle((String) value);
                    return;
                case "educationLevelHighSchoolTitle":
                    obj.setEducationLevelHighSchoolTitle((String) value);
                    return;
                case "developFormTitle":
                    obj.setDevelopFormTitle((String) value);
                    return;
                case "developConditionTitle":
                    obj.setDevelopConditionTitle((String) value);
                    return;
                case "developTechTitle":
                    obj.setDevelopTechTitle((String) value);
                    return;
                case "developPeriodTitle":
                    obj.setDevelopPeriodTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "content":
                        return true;
                case "formingDate":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "compensationType":
                        return true;
                case "dateFrom":
                        return true;
                case "dateTo":
                        return true;
                case "examTitle":
                        return true;
                case "qualificationTitle":
                        return true;
                case "studentCategoryTitle":
                        return true;
                case "formativeOrgUnitTitle":
                        return true;
                case "territorialOrgUnitTitle":
                        return true;
                case "educationLevelHighSchoolTitle":
                        return true;
                case "developFormTitle":
                        return true;
                case "developConditionTitle":
                        return true;
                case "developTechTitle":
                        return true;
                case "developPeriodTitle":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "content":
                    return true;
                case "formingDate":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "compensationType":
                    return true;
                case "dateFrom":
                    return true;
                case "dateTo":
                    return true;
                case "examTitle":
                    return true;
                case "qualificationTitle":
                    return true;
                case "studentCategoryTitle":
                    return true;
                case "formativeOrgUnitTitle":
                    return true;
                case "territorialOrgUnitTitle":
                    return true;
                case "educationLevelHighSchoolTitle":
                    return true;
                case "developFormTitle":
                    return true;
                case "developConditionTitle":
                    return true;
                case "developTechTitle":
                    return true;
                case "developPeriodTitle":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "content":
                    return DatabaseFile.class;
                case "formingDate":
                    return Date.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "compensationType":
                    return CompensationType.class;
                case "dateFrom":
                    return Date.class;
                case "dateTo":
                    return Date.class;
                case "examTitle":
                    return String.class;
                case "qualificationTitle":
                    return String.class;
                case "studentCategoryTitle":
                    return String.class;
                case "formativeOrgUnitTitle":
                    return String.class;
                case "territorialOrgUnitTitle":
                    return String.class;
                case "educationLevelHighSchoolTitle":
                    return String.class;
                case "developFormTitle":
                    return String.class;
                case "developConditionTitle":
                    return String.class;
                case "developTechTitle":
                    return String.class;
                case "developPeriodTitle":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntrantListTakePassEntrance> _dslPath = new Path<EntrantListTakePassEntrance>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntrantListTakePassEntrance");
    }
            

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantListTakePassEntrance#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantListTakePassEntrance#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantListTakePassEntrance#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantListTakePassEntrance#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantListTakePassEntrance#getDateFrom()
     */
    public static PropertyPath<Date> dateFrom()
    {
        return _dslPath.dateFrom();
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantListTakePassEntrance#getDateTo()
     */
    public static PropertyPath<Date> dateTo()
    {
        return _dslPath.dateTo();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantListTakePassEntrance#getExamTitle()
     */
    public static PropertyPath<String> examTitle()
    {
        return _dslPath.examTitle();
    }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.uniec.entity.report.EntrantListTakePassEntrance#getQualificationTitle()
     */
    public static PropertyPath<String> qualificationTitle()
    {
        return _dslPath.qualificationTitle();
    }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.uniec.entity.report.EntrantListTakePassEntrance#getStudentCategoryTitle()
     */
    public static PropertyPath<String> studentCategoryTitle()
    {
        return _dslPath.studentCategoryTitle();
    }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.uniec.entity.report.EntrantListTakePassEntrance#getFormativeOrgUnitTitle()
     */
    public static PropertyPath<String> formativeOrgUnitTitle()
    {
        return _dslPath.formativeOrgUnitTitle();
    }

    /**
     * @return Территориальное подр..
     * @see ru.tandemservice.uniec.entity.report.EntrantListTakePassEntrance#getTerritorialOrgUnitTitle()
     */
    public static PropertyPath<String> territorialOrgUnitTitle()
    {
        return _dslPath.territorialOrgUnitTitle();
    }

    /**
     * @return Уровень образования ОУ подразделение.
     * @see ru.tandemservice.uniec.entity.report.EntrantListTakePassEntrance#getEducationLevelHighSchoolTitle()
     */
    public static PropertyPath<String> educationLevelHighSchoolTitle()
    {
        return _dslPath.educationLevelHighSchoolTitle();
    }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.uniec.entity.report.EntrantListTakePassEntrance#getDevelopFormTitle()
     */
    public static PropertyPath<String> developFormTitle()
    {
        return _dslPath.developFormTitle();
    }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.uniec.entity.report.EntrantListTakePassEntrance#getDevelopConditionTitle()
     */
    public static PropertyPath<String> developConditionTitle()
    {
        return _dslPath.developConditionTitle();
    }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.uniec.entity.report.EntrantListTakePassEntrance#getDevelopTechTitle()
     */
    public static PropertyPath<String> developTechTitle()
    {
        return _dslPath.developTechTitle();
    }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.uniec.entity.report.EntrantListTakePassEntrance#getDevelopPeriodTitle()
     */
    public static PropertyPath<String> developPeriodTitle()
    {
        return _dslPath.developPeriodTitle();
    }

    public static class Path<E extends EntrantListTakePassEntrance> extends EntityPath<E>
    {
        private DatabaseFile.Path<DatabaseFile> _content;
        private PropertyPath<Date> _formingDate;
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private CompensationType.Path<CompensationType> _compensationType;
        private PropertyPath<Date> _dateFrom;
        private PropertyPath<Date> _dateTo;
        private PropertyPath<String> _examTitle;
        private PropertyPath<String> _qualificationTitle;
        private PropertyPath<String> _studentCategoryTitle;
        private PropertyPath<String> _formativeOrgUnitTitle;
        private PropertyPath<String> _territorialOrgUnitTitle;
        private PropertyPath<String> _educationLevelHighSchoolTitle;
        private PropertyPath<String> _developFormTitle;
        private PropertyPath<String> _developConditionTitle;
        private PropertyPath<String> _developTechTitle;
        private PropertyPath<String> _developPeriodTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantListTakePassEntrance#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantListTakePassEntrance#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(EntrantListTakePassEntranceGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantListTakePassEntrance#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantListTakePassEntrance#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantListTakePassEntrance#getDateFrom()
     */
        public PropertyPath<Date> dateFrom()
        {
            if(_dateFrom == null )
                _dateFrom = new PropertyPath<Date>(EntrantListTakePassEntranceGen.P_DATE_FROM, this);
            return _dateFrom;
        }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantListTakePassEntrance#getDateTo()
     */
        public PropertyPath<Date> dateTo()
        {
            if(_dateTo == null )
                _dateTo = new PropertyPath<Date>(EntrantListTakePassEntranceGen.P_DATE_TO, this);
            return _dateTo;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantListTakePassEntrance#getExamTitle()
     */
        public PropertyPath<String> examTitle()
        {
            if(_examTitle == null )
                _examTitle = new PropertyPath<String>(EntrantListTakePassEntranceGen.P_EXAM_TITLE, this);
            return _examTitle;
        }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.uniec.entity.report.EntrantListTakePassEntrance#getQualificationTitle()
     */
        public PropertyPath<String> qualificationTitle()
        {
            if(_qualificationTitle == null )
                _qualificationTitle = new PropertyPath<String>(EntrantListTakePassEntranceGen.P_QUALIFICATION_TITLE, this);
            return _qualificationTitle;
        }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.uniec.entity.report.EntrantListTakePassEntrance#getStudentCategoryTitle()
     */
        public PropertyPath<String> studentCategoryTitle()
        {
            if(_studentCategoryTitle == null )
                _studentCategoryTitle = new PropertyPath<String>(EntrantListTakePassEntranceGen.P_STUDENT_CATEGORY_TITLE, this);
            return _studentCategoryTitle;
        }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.uniec.entity.report.EntrantListTakePassEntrance#getFormativeOrgUnitTitle()
     */
        public PropertyPath<String> formativeOrgUnitTitle()
        {
            if(_formativeOrgUnitTitle == null )
                _formativeOrgUnitTitle = new PropertyPath<String>(EntrantListTakePassEntranceGen.P_FORMATIVE_ORG_UNIT_TITLE, this);
            return _formativeOrgUnitTitle;
        }

    /**
     * @return Территориальное подр..
     * @see ru.tandemservice.uniec.entity.report.EntrantListTakePassEntrance#getTerritorialOrgUnitTitle()
     */
        public PropertyPath<String> territorialOrgUnitTitle()
        {
            if(_territorialOrgUnitTitle == null )
                _territorialOrgUnitTitle = new PropertyPath<String>(EntrantListTakePassEntranceGen.P_TERRITORIAL_ORG_UNIT_TITLE, this);
            return _territorialOrgUnitTitle;
        }

    /**
     * @return Уровень образования ОУ подразделение.
     * @see ru.tandemservice.uniec.entity.report.EntrantListTakePassEntrance#getEducationLevelHighSchoolTitle()
     */
        public PropertyPath<String> educationLevelHighSchoolTitle()
        {
            if(_educationLevelHighSchoolTitle == null )
                _educationLevelHighSchoolTitle = new PropertyPath<String>(EntrantListTakePassEntranceGen.P_EDUCATION_LEVEL_HIGH_SCHOOL_TITLE, this);
            return _educationLevelHighSchoolTitle;
        }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.uniec.entity.report.EntrantListTakePassEntrance#getDevelopFormTitle()
     */
        public PropertyPath<String> developFormTitle()
        {
            if(_developFormTitle == null )
                _developFormTitle = new PropertyPath<String>(EntrantListTakePassEntranceGen.P_DEVELOP_FORM_TITLE, this);
            return _developFormTitle;
        }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.uniec.entity.report.EntrantListTakePassEntrance#getDevelopConditionTitle()
     */
        public PropertyPath<String> developConditionTitle()
        {
            if(_developConditionTitle == null )
                _developConditionTitle = new PropertyPath<String>(EntrantListTakePassEntranceGen.P_DEVELOP_CONDITION_TITLE, this);
            return _developConditionTitle;
        }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.uniec.entity.report.EntrantListTakePassEntrance#getDevelopTechTitle()
     */
        public PropertyPath<String> developTechTitle()
        {
            if(_developTechTitle == null )
                _developTechTitle = new PropertyPath<String>(EntrantListTakePassEntranceGen.P_DEVELOP_TECH_TITLE, this);
            return _developTechTitle;
        }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.uniec.entity.report.EntrantListTakePassEntrance#getDevelopPeriodTitle()
     */
        public PropertyPath<String> developPeriodTitle()
        {
            if(_developPeriodTitle == null )
                _developPeriodTitle = new PropertyPath<String>(EntrantListTakePassEntranceGen.P_DEVELOP_PERIOD_TITLE, this);
            return _developPeriodTitle;
        }

        public Class getEntityClass()
        {
            return EntrantListTakePassEntrance.class;
        }

        public String getEntityName()
        {
            return "entrantListTakePassEntrance";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
