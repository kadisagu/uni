/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcOrder.ui.TargetContractParAddEdit;

import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uniec.base.bo.EcOrder.util.BaseEcOrderParAddEditUI;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EntrTargetContractExtract;

/**
 * @author Vasily Zhukov
 * @since 24.05.2011
 */
public class EcOrderTargetContractParAddEditUI extends BaseEcOrderParAddEditUI
{
    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();

        if (!isEditForm())
            setCourse(DevelopGridDAO.getCourseMap().get(1));
    }

    @Override
    public EnrollmentExtract createEnrollmentExtract(PreliminaryEnrollmentStudent preStudent)
    {
        return new EntrTargetContractExtract();
    }
}
