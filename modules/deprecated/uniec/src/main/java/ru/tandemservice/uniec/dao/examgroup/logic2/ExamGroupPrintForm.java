/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.dao.examgroup.logic2;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.examgroup.AbstractExamGroupPrintBuilder;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.ExamGroup;

/**
 * @author Vasily Zhukov
 * @since 31.05.2010
 */
class ExamGroupPrintForm extends AbstractExamGroupPrintBuilder
{
    @Override
    protected boolean isWithResult()
    {
        return false;
    }

    @Override
    protected String getTemplateCode()
    {
        return UniecDefines.EXAM_GROUP_LIST_ALG2;
    }

    @Override
    protected RtfInjectModifier getRtfInjectModifier(ExamGroup group)
    {
        // декодируем ключ группы
        ExamGroupLogicSample2.ExamGroupKey key = ExamGroupLogicSample2.parseKey(getSession(), group.getKey());

        EnrollmentDirection enrollmentDirection = key.getEnrollmentDirection();
        EducationOrgUnit educationOrgUnit = enrollmentDirection.getEducationOrgUnit();
        OrgUnit formativeOrgUnit = key.getEnrollmentDirection().getEducationOrgUnit().getFormativeOrgUnit();

        return super.getRtfInjectModifier(group)
                .put("enrollmentDirection", enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitle())
                .put("developForm", educationOrgUnit.getDevelopForm().getTitle())
                .put("formativeOrgUnit", StringUtils.isEmpty(formativeOrgUnit.getNominativeCaseTitle()) ? formativeOrgUnit.getTitle() : formativeOrgUnit.getNominativeCaseTitle())
                .put("compensationTypeStr", key.getCompensationType().isBudget() ? "За счет средств федерального бюджета" : "На места с оплатой стоимости обучения");
    }
}
