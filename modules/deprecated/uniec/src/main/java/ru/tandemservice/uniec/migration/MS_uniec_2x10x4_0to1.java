package ru.tandemservice.uniec.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniec_2x10x4_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.4")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность entrantDailyRatingByED2Report

		// создано обязательное свойство sortByOriginalDocHandedIn
		{
			// создать колонку
			tool.createColumn("enr_rating_by_ed2_report_t", new DBColumn("sortbyoriginaldochandedin_p", DBType.BOOLEAN));

			tool.executeUpdate("update enr_rating_by_ed2_report_t set sortbyoriginaldochandedin_p=? where sortbyoriginaldochandedin_p is null", Boolean.FALSE);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr_rating_by_ed2_report_t", "sortbyoriginaldochandedin_p", false);

		}


    }
}