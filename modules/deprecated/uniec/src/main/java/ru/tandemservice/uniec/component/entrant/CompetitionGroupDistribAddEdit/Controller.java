package ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribAddEdit;

import org.apache.tapestry.form.IFormComponent;
import org.apache.tapestry.form.ValidationMessages;
import org.apache.tapestry.form.validator.Required;
import org.apache.tapestry.form.validator.Validator;
import org.apache.tapestry.valid.ValidatorException;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.AbstractListDataSource;

import org.tandemframework.shared.organization.base.entity.gen.OrgUnitGen;
import ru.tandemservice.uni.entity.catalog.gen.EducationLevelsHighSchoolGen;
import ru.tandemservice.uni.entity.education.gen.EducationOrgUnitGen;
import ru.tandemservice.uniec.entity.ecg.EcgDistribQuota;
import ru.tandemservice.uniec.entity.ecg.gen.EcgDistribQuotaGen;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.gen.EnrollmentDirectionGen;

/**
 * @author vdanilov
 */
public class Controller extends AbstractBusinessController<IDAO, Model> {


	@Override
	@SuppressWarnings("unchecked")
	public void onRefreshComponent(final IBusinessComponent component) {
		final Model model = this.getModel(component);
		this.getDao().prepare(model);

		final AbstractListDataSource<EcgDistribQuota> ds = model.getQuotaDataSource();
		ds.getColumns().clear();
		ds.addColumn(new SimpleColumn("Шифр", EcgDistribQuotaGen.L_DIRECTION+"."+EnrollmentDirectionGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnitGen.L_EDUCATION_LEVEL_HIGH_SCHOOL+"."+EducationLevelsHighSchoolGen.P_SHORT_TITLE).setClickable(false).setOrderable(false));
		ds.addColumn(new SimpleColumn("Направление подготовки (специальность)", EcgDistribQuotaGen.L_DIRECTION+"."+EnrollmentDirection.P_TITLE).setClickable(false).setOrderable(false));
		ds.addColumn(new SimpleColumn("Формирующее подр.", EcgDistribQuotaGen.L_DIRECTION+"."+EnrollmentDirectionGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnitGen.L_FORMATIVE_ORG_UNIT+"."+ OrgUnitGen.P_SHORT_TITLE).setClickable(false).setOrderable(false));
		ds.addColumn(new SimpleColumn("Территориальное подр.", EcgDistribQuotaGen.L_DIRECTION+"."+EnrollmentDirectionGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnitGen.L_TERRITORIAL_ORG_UNIT+"."+OrgUnitGen.P_TERRITORIAL_SHORT_TITLE).setClickable(false).setOrderable(false));
		ds.addColumn(new SimpleColumn("Форма", EcgDistribQuotaGen.L_DIRECTION+"."+EnrollmentDirectionGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnitGen.L_DEVELOP_FORM+".title").setClickable(false).setOrderable(false));
		ds.addColumn(new SimpleColumn("Условие", EcgDistribQuotaGen.L_DIRECTION+"."+EnrollmentDirectionGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnitGen.L_DEVELOP_CONDITION+".title").setClickable(false).setOrderable(false));
		ds.addColumn(new SimpleColumn("Технология", EcgDistribQuotaGen.L_DIRECTION+"."+EnrollmentDirectionGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnitGen.L_DEVELOP_TECH+".title").setClickable(false).setOrderable(false));
		ds.addColumn(new SimpleColumn("Срок", EcgDistribQuotaGen.L_DIRECTION+"."+EnrollmentDirectionGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnitGen.L_DEVELOP_PERIOD+".title").setClickable(false).setOrderable(false));
		ds.addColumn(new BlockColumn("quota", "План приема"));
	}

	public void onChangeCompensationType(final IBusinessComponent component) {
		final Model model = this.getModel(component);
		if (null == model.getDistrib().getId()) {
			model.getDistrib().setTitle(null);
			model.getQuotaList().clear();
		}

		this.getDao().prepare(model);
	}


	public void onClickApply(final IBusinessComponent component) {
		final Model model = this.getModel(component);
		final boolean isNew = (null==model.getDistrib().getId());
		this.getDao().save(model);

		model.setResultId(isNew ? model.getDistrib().getId() : null);
		this.deactivate(component);
	}

	public Validator getValidator(final EcgDistribQuota quota) {
		return new Required() {
			@Override public void validate(final IFormComponent field, final ValidationMessages messages, final Object object) throws ValidatorException {
				super.validate(field, messages, object);
				final int value = ((Number)object).intValue();
				try {
					Controller.this.getDao().validate(quota, value);
				} catch (final ApplicationException e) {
					throw new ValidatorException(e.getMessage());
				}
			}
		};

	}

}
