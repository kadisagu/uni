/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.menu.examGroup.abstractMarkDistribution;

import org.tandemframework.core.entity.IdentifiableWrapper;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline;

/**
 * @author vip_delete
 * @since 31.03.2009
 */
public final class EntrantDisciplineMarkWrapper extends IdentifiableWrapper
{
    private static final long serialVersionUID = 2241208418452698596L;
    public static final String P_ENTRANT_REQUEST = "entrantRequest";
    public static final String P_EXAM_PASS_DISCIPLINE = "examPassDiscipline";

    private ExamPassDiscipline _examPassDiscipline;
    private EntrantRequest _entrantRequest;
    private String _mark;
    private String _paperCode;
    private String _examiners;

    public EntrantDisciplineMarkWrapper(ExamPassDiscipline examPassDiscipline, EntrantRequest entrantRequest, String mark, String paperCode, String examiners)
    {
        super(examPassDiscipline.getId(), entrantRequest.getEntrant().getPerson().getFullFio());
        _examPassDiscipline = examPassDiscipline;
        _entrantRequest = entrantRequest;
        _mark = mark;
        _paperCode = paperCode;
        _examiners = examiners;
    }

    // Getters

    public ExamPassDiscipline getExamPassDiscipline()
    {
        return _examPassDiscipline;
    }

    public EntrantRequest getEntrantRequest()
    {
        return _entrantRequest;
    }

    public String getMark()
    {
        return _mark;
    }

    public String getPaperCode()
    {
        return _paperCode;
    }

    public String getExaminers()
    {
        return _examiners;
    }

    // Setters

    public void setMark(String mark)
    {
        _mark = mark;
    }

    public void setPaperCode(String paperCode)
    {
        _paperCode = paperCode;
    }

    public void setExaminers(String examiners)
    {
        _examiners = examiners;
    }
}
