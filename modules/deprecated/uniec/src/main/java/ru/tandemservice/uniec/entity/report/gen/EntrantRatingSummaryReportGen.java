package ru.tandemservice.uniec.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.report.EntrantRatingSummaryReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сводный рейтинг абитуриентов
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntrantRatingSummaryReportGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.report.EntrantRatingSummaryReport";
    public static final String ENTITY_NAME = "entrantRatingSummaryReport";
    public static final int VERSION_HASH = 736997970;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONTENT = "content";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_DATE_FROM = "dateFrom";
    public static final String P_DATE_TO = "dateTo";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String P_STUDENT_CATEGORY_TITLE = "studentCategoryTitle";
    public static final String L_DEVELOP_FORM = "developForm";
    public static final String P_DEVELOP_CONDITION_TITLE = "developConditionTitle";
    public static final String P_QUALIFICATION_TITLE = "qualificationTitle";
    public static final String P_FORMATIVE_ORG_UNIT_TITLE = "formativeOrgUnitTitle";
    public static final String P_TERRITORIAL_ORG_UNIT_TITLE = "territorialOrgUnitTitle";
    public static final String P_ONLY_WITH_ORIGINALS = "onlyWithOriginals";
    public static final String P_BY_EXAM_SET = "byExamSet";
    public static final String P_ENTRANT_CUSTOM_STATE_TITLE = "entrantCustomStateTitle";

    private DatabaseFile _content;     // Печатная форма
    private Date _formingDate;     // Дата формирования
    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private Date _dateFrom;     // Заявления с
    private Date _dateTo;     // Заявления по
    private CompensationType _compensationType;     // Вид возмещения затрат
    private String _studentCategoryTitle;     // Категория поступающего
    private DevelopForm _developForm;     // Форма освоения
    private String _developConditionTitle;     // Условие освоения
    private String _qualificationTitle;     // Квалификация
    private String _formativeOrgUnitTitle;     // Формирующее подр.
    private String _territorialOrgUnitTitle;     // Территориальное подр.
    private boolean _onlyWithOriginals;     // Не включать абитуриентов без оригиналов документов
    private boolean _byExamSet;     // Группировать абитуриентов по наборам вступительных испытаний
    private String _entrantCustomStateTitle;     // Дополнительные статусы абитуриентов

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     */
    @NotNull
    public Date getDateFrom()
    {
        return _dateFrom;
    }

    /**
     * @param dateFrom Заявления с. Свойство не может быть null.
     */
    public void setDateFrom(Date dateFrom)
    {
        dirty(_dateFrom, dateFrom);
        _dateFrom = dateFrom;
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     */
    @NotNull
    public Date getDateTo()
    {
        return _dateTo;
    }

    /**
     * @param dateTo Заявления по. Свойство не может быть null.
     */
    public void setDateTo(Date dateTo)
    {
        dirty(_dateTo, dateTo);
        _dateTo = dateTo;
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Категория поступающего.
     */
    @Length(max=255)
    public String getStudentCategoryTitle()
    {
        return _studentCategoryTitle;
    }

    /**
     * @param studentCategoryTitle Категория поступающего.
     */
    public void setStudentCategoryTitle(String studentCategoryTitle)
    {
        dirty(_studentCategoryTitle, studentCategoryTitle);
        _studentCategoryTitle = studentCategoryTitle;
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения. Свойство не может быть null.
     */
    public void setDevelopForm(DevelopForm developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Условие освоения.
     */
    @Length(max=255)
    public String getDevelopConditionTitle()
    {
        return _developConditionTitle;
    }

    /**
     * @param developConditionTitle Условие освоения.
     */
    public void setDevelopConditionTitle(String developConditionTitle)
    {
        dirty(_developConditionTitle, developConditionTitle);
        _developConditionTitle = developConditionTitle;
    }

    /**
     * @return Квалификация.
     */
    @Length(max=255)
    public String getQualificationTitle()
    {
        return _qualificationTitle;
    }

    /**
     * @param qualificationTitle Квалификация.
     */
    public void setQualificationTitle(String qualificationTitle)
    {
        dirty(_qualificationTitle, qualificationTitle);
        _qualificationTitle = qualificationTitle;
    }

    /**
     * @return Формирующее подр..
     */
    public String getFormativeOrgUnitTitle()
    {
        return _formativeOrgUnitTitle;
    }

    /**
     * @param formativeOrgUnitTitle Формирующее подр..
     */
    public void setFormativeOrgUnitTitle(String formativeOrgUnitTitle)
    {
        dirty(_formativeOrgUnitTitle, formativeOrgUnitTitle);
        _formativeOrgUnitTitle = formativeOrgUnitTitle;
    }

    /**
     * @return Территориальное подр..
     */
    public String getTerritorialOrgUnitTitle()
    {
        return _territorialOrgUnitTitle;
    }

    /**
     * @param territorialOrgUnitTitle Территориальное подр..
     */
    public void setTerritorialOrgUnitTitle(String territorialOrgUnitTitle)
    {
        dirty(_territorialOrgUnitTitle, territorialOrgUnitTitle);
        _territorialOrgUnitTitle = territorialOrgUnitTitle;
    }

    /**
     * @return Не включать абитуриентов без оригиналов документов. Свойство не может быть null.
     */
    @NotNull
    public boolean isOnlyWithOriginals()
    {
        return _onlyWithOriginals;
    }

    /**
     * @param onlyWithOriginals Не включать абитуриентов без оригиналов документов. Свойство не может быть null.
     */
    public void setOnlyWithOriginals(boolean onlyWithOriginals)
    {
        dirty(_onlyWithOriginals, onlyWithOriginals);
        _onlyWithOriginals = onlyWithOriginals;
    }

    /**
     * @return Группировать абитуриентов по наборам вступительных испытаний. Свойство не может быть null.
     */
    @NotNull
    public boolean isByExamSet()
    {
        return _byExamSet;
    }

    /**
     * @param byExamSet Группировать абитуриентов по наборам вступительных испытаний. Свойство не может быть null.
     */
    public void setByExamSet(boolean byExamSet)
    {
        dirty(_byExamSet, byExamSet);
        _byExamSet = byExamSet;
    }

    /**
     * @return Дополнительные статусы абитуриентов.
     */
    @Length(max=255)
    public String getEntrantCustomStateTitle()
    {
        return _entrantCustomStateTitle;
    }

    /**
     * @param entrantCustomStateTitle Дополнительные статусы абитуриентов.
     */
    public void setEntrantCustomStateTitle(String entrantCustomStateTitle)
    {
        dirty(_entrantCustomStateTitle, entrantCustomStateTitle);
        _entrantCustomStateTitle = entrantCustomStateTitle;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntrantRatingSummaryReportGen)
        {
            setContent(((EntrantRatingSummaryReport)another).getContent());
            setFormingDate(((EntrantRatingSummaryReport)another).getFormingDate());
            setEnrollmentCampaign(((EntrantRatingSummaryReport)another).getEnrollmentCampaign());
            setDateFrom(((EntrantRatingSummaryReport)another).getDateFrom());
            setDateTo(((EntrantRatingSummaryReport)another).getDateTo());
            setCompensationType(((EntrantRatingSummaryReport)another).getCompensationType());
            setStudentCategoryTitle(((EntrantRatingSummaryReport)another).getStudentCategoryTitle());
            setDevelopForm(((EntrantRatingSummaryReport)another).getDevelopForm());
            setDevelopConditionTitle(((EntrantRatingSummaryReport)another).getDevelopConditionTitle());
            setQualificationTitle(((EntrantRatingSummaryReport)another).getQualificationTitle());
            setFormativeOrgUnitTitle(((EntrantRatingSummaryReport)another).getFormativeOrgUnitTitle());
            setTerritorialOrgUnitTitle(((EntrantRatingSummaryReport)another).getTerritorialOrgUnitTitle());
            setOnlyWithOriginals(((EntrantRatingSummaryReport)another).isOnlyWithOriginals());
            setByExamSet(((EntrantRatingSummaryReport)another).isByExamSet());
            setEntrantCustomStateTitle(((EntrantRatingSummaryReport)another).getEntrantCustomStateTitle());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntrantRatingSummaryReportGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntrantRatingSummaryReport.class;
        }

        public T newInstance()
        {
            return (T) new EntrantRatingSummaryReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "content":
                    return obj.getContent();
                case "formingDate":
                    return obj.getFormingDate();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "dateFrom":
                    return obj.getDateFrom();
                case "dateTo":
                    return obj.getDateTo();
                case "compensationType":
                    return obj.getCompensationType();
                case "studentCategoryTitle":
                    return obj.getStudentCategoryTitle();
                case "developForm":
                    return obj.getDevelopForm();
                case "developConditionTitle":
                    return obj.getDevelopConditionTitle();
                case "qualificationTitle":
                    return obj.getQualificationTitle();
                case "formativeOrgUnitTitle":
                    return obj.getFormativeOrgUnitTitle();
                case "territorialOrgUnitTitle":
                    return obj.getTerritorialOrgUnitTitle();
                case "onlyWithOriginals":
                    return obj.isOnlyWithOriginals();
                case "byExamSet":
                    return obj.isByExamSet();
                case "entrantCustomStateTitle":
                    return obj.getEntrantCustomStateTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "dateFrom":
                    obj.setDateFrom((Date) value);
                    return;
                case "dateTo":
                    obj.setDateTo((Date) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "studentCategoryTitle":
                    obj.setStudentCategoryTitle((String) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((DevelopForm) value);
                    return;
                case "developConditionTitle":
                    obj.setDevelopConditionTitle((String) value);
                    return;
                case "qualificationTitle":
                    obj.setQualificationTitle((String) value);
                    return;
                case "formativeOrgUnitTitle":
                    obj.setFormativeOrgUnitTitle((String) value);
                    return;
                case "territorialOrgUnitTitle":
                    obj.setTerritorialOrgUnitTitle((String) value);
                    return;
                case "onlyWithOriginals":
                    obj.setOnlyWithOriginals((Boolean) value);
                    return;
                case "byExamSet":
                    obj.setByExamSet((Boolean) value);
                    return;
                case "entrantCustomStateTitle":
                    obj.setEntrantCustomStateTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "content":
                        return true;
                case "formingDate":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "dateFrom":
                        return true;
                case "dateTo":
                        return true;
                case "compensationType":
                        return true;
                case "studentCategoryTitle":
                        return true;
                case "developForm":
                        return true;
                case "developConditionTitle":
                        return true;
                case "qualificationTitle":
                        return true;
                case "formativeOrgUnitTitle":
                        return true;
                case "territorialOrgUnitTitle":
                        return true;
                case "onlyWithOriginals":
                        return true;
                case "byExamSet":
                        return true;
                case "entrantCustomStateTitle":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "content":
                    return true;
                case "formingDate":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "dateFrom":
                    return true;
                case "dateTo":
                    return true;
                case "compensationType":
                    return true;
                case "studentCategoryTitle":
                    return true;
                case "developForm":
                    return true;
                case "developConditionTitle":
                    return true;
                case "qualificationTitle":
                    return true;
                case "formativeOrgUnitTitle":
                    return true;
                case "territorialOrgUnitTitle":
                    return true;
                case "onlyWithOriginals":
                    return true;
                case "byExamSet":
                    return true;
                case "entrantCustomStateTitle":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "content":
                    return DatabaseFile.class;
                case "formingDate":
                    return Date.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "dateFrom":
                    return Date.class;
                case "dateTo":
                    return Date.class;
                case "compensationType":
                    return CompensationType.class;
                case "studentCategoryTitle":
                    return String.class;
                case "developForm":
                    return DevelopForm.class;
                case "developConditionTitle":
                    return String.class;
                case "qualificationTitle":
                    return String.class;
                case "formativeOrgUnitTitle":
                    return String.class;
                case "territorialOrgUnitTitle":
                    return String.class;
                case "onlyWithOriginals":
                    return Boolean.class;
                case "byExamSet":
                    return Boolean.class;
                case "entrantCustomStateTitle":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntrantRatingSummaryReport> _dslPath = new Path<EntrantRatingSummaryReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntrantRatingSummaryReport");
    }
            

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantRatingSummaryReport#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantRatingSummaryReport#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantRatingSummaryReport#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantRatingSummaryReport#getDateFrom()
     */
    public static PropertyPath<Date> dateFrom()
    {
        return _dslPath.dateFrom();
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantRatingSummaryReport#getDateTo()
     */
    public static PropertyPath<Date> dateTo()
    {
        return _dslPath.dateTo();
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantRatingSummaryReport#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.uniec.entity.report.EntrantRatingSummaryReport#getStudentCategoryTitle()
     */
    public static PropertyPath<String> studentCategoryTitle()
    {
        return _dslPath.studentCategoryTitle();
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantRatingSummaryReport#getDevelopForm()
     */
    public static DevelopForm.Path<DevelopForm> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.uniec.entity.report.EntrantRatingSummaryReport#getDevelopConditionTitle()
     */
    public static PropertyPath<String> developConditionTitle()
    {
        return _dslPath.developConditionTitle();
    }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.uniec.entity.report.EntrantRatingSummaryReport#getQualificationTitle()
     */
    public static PropertyPath<String> qualificationTitle()
    {
        return _dslPath.qualificationTitle();
    }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.uniec.entity.report.EntrantRatingSummaryReport#getFormativeOrgUnitTitle()
     */
    public static PropertyPath<String> formativeOrgUnitTitle()
    {
        return _dslPath.formativeOrgUnitTitle();
    }

    /**
     * @return Территориальное подр..
     * @see ru.tandemservice.uniec.entity.report.EntrantRatingSummaryReport#getTerritorialOrgUnitTitle()
     */
    public static PropertyPath<String> territorialOrgUnitTitle()
    {
        return _dslPath.territorialOrgUnitTitle();
    }

    /**
     * @return Не включать абитуриентов без оригиналов документов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantRatingSummaryReport#isOnlyWithOriginals()
     */
    public static PropertyPath<Boolean> onlyWithOriginals()
    {
        return _dslPath.onlyWithOriginals();
    }

    /**
     * @return Группировать абитуриентов по наборам вступительных испытаний. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantRatingSummaryReport#isByExamSet()
     */
    public static PropertyPath<Boolean> byExamSet()
    {
        return _dslPath.byExamSet();
    }

    /**
     * @return Дополнительные статусы абитуриентов.
     * @see ru.tandemservice.uniec.entity.report.EntrantRatingSummaryReport#getEntrantCustomStateTitle()
     */
    public static PropertyPath<String> entrantCustomStateTitle()
    {
        return _dslPath.entrantCustomStateTitle();
    }

    public static class Path<E extends EntrantRatingSummaryReport> extends EntityPath<E>
    {
        private DatabaseFile.Path<DatabaseFile> _content;
        private PropertyPath<Date> _formingDate;
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<Date> _dateFrom;
        private PropertyPath<Date> _dateTo;
        private CompensationType.Path<CompensationType> _compensationType;
        private PropertyPath<String> _studentCategoryTitle;
        private DevelopForm.Path<DevelopForm> _developForm;
        private PropertyPath<String> _developConditionTitle;
        private PropertyPath<String> _qualificationTitle;
        private PropertyPath<String> _formativeOrgUnitTitle;
        private PropertyPath<String> _territorialOrgUnitTitle;
        private PropertyPath<Boolean> _onlyWithOriginals;
        private PropertyPath<Boolean> _byExamSet;
        private PropertyPath<String> _entrantCustomStateTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantRatingSummaryReport#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantRatingSummaryReport#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(EntrantRatingSummaryReportGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantRatingSummaryReport#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantRatingSummaryReport#getDateFrom()
     */
        public PropertyPath<Date> dateFrom()
        {
            if(_dateFrom == null )
                _dateFrom = new PropertyPath<Date>(EntrantRatingSummaryReportGen.P_DATE_FROM, this);
            return _dateFrom;
        }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantRatingSummaryReport#getDateTo()
     */
        public PropertyPath<Date> dateTo()
        {
            if(_dateTo == null )
                _dateTo = new PropertyPath<Date>(EntrantRatingSummaryReportGen.P_DATE_TO, this);
            return _dateTo;
        }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantRatingSummaryReport#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.uniec.entity.report.EntrantRatingSummaryReport#getStudentCategoryTitle()
     */
        public PropertyPath<String> studentCategoryTitle()
        {
            if(_studentCategoryTitle == null )
                _studentCategoryTitle = new PropertyPath<String>(EntrantRatingSummaryReportGen.P_STUDENT_CATEGORY_TITLE, this);
            return _studentCategoryTitle;
        }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantRatingSummaryReport#getDevelopForm()
     */
        public DevelopForm.Path<DevelopForm> developForm()
        {
            if(_developForm == null )
                _developForm = new DevelopForm.Path<DevelopForm>(L_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.uniec.entity.report.EntrantRatingSummaryReport#getDevelopConditionTitle()
     */
        public PropertyPath<String> developConditionTitle()
        {
            if(_developConditionTitle == null )
                _developConditionTitle = new PropertyPath<String>(EntrantRatingSummaryReportGen.P_DEVELOP_CONDITION_TITLE, this);
            return _developConditionTitle;
        }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.uniec.entity.report.EntrantRatingSummaryReport#getQualificationTitle()
     */
        public PropertyPath<String> qualificationTitle()
        {
            if(_qualificationTitle == null )
                _qualificationTitle = new PropertyPath<String>(EntrantRatingSummaryReportGen.P_QUALIFICATION_TITLE, this);
            return _qualificationTitle;
        }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.uniec.entity.report.EntrantRatingSummaryReport#getFormativeOrgUnitTitle()
     */
        public PropertyPath<String> formativeOrgUnitTitle()
        {
            if(_formativeOrgUnitTitle == null )
                _formativeOrgUnitTitle = new PropertyPath<String>(EntrantRatingSummaryReportGen.P_FORMATIVE_ORG_UNIT_TITLE, this);
            return _formativeOrgUnitTitle;
        }

    /**
     * @return Территориальное подр..
     * @see ru.tandemservice.uniec.entity.report.EntrantRatingSummaryReport#getTerritorialOrgUnitTitle()
     */
        public PropertyPath<String> territorialOrgUnitTitle()
        {
            if(_territorialOrgUnitTitle == null )
                _territorialOrgUnitTitle = new PropertyPath<String>(EntrantRatingSummaryReportGen.P_TERRITORIAL_ORG_UNIT_TITLE, this);
            return _territorialOrgUnitTitle;
        }

    /**
     * @return Не включать абитуриентов без оригиналов документов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantRatingSummaryReport#isOnlyWithOriginals()
     */
        public PropertyPath<Boolean> onlyWithOriginals()
        {
            if(_onlyWithOriginals == null )
                _onlyWithOriginals = new PropertyPath<Boolean>(EntrantRatingSummaryReportGen.P_ONLY_WITH_ORIGINALS, this);
            return _onlyWithOriginals;
        }

    /**
     * @return Группировать абитуриентов по наборам вступительных испытаний. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantRatingSummaryReport#isByExamSet()
     */
        public PropertyPath<Boolean> byExamSet()
        {
            if(_byExamSet == null )
                _byExamSet = new PropertyPath<Boolean>(EntrantRatingSummaryReportGen.P_BY_EXAM_SET, this);
            return _byExamSet;
        }

    /**
     * @return Дополнительные статусы абитуриентов.
     * @see ru.tandemservice.uniec.entity.report.EntrantRatingSummaryReport#getEntrantCustomStateTitle()
     */
        public PropertyPath<String> entrantCustomStateTitle()
        {
            if(_entrantCustomStateTitle == null )
                _entrantCustomStateTitle = new PropertyPath<String>(EntrantRatingSummaryReportGen.P_ENTRANT_CUSTOM_STATE_TITLE, this);
            return _entrantCustomStateTitle;
        }

        public Class getEntityClass()
        {
            return EntrantRatingSummaryReport.class;
        }

        public String getEntityName()
        {
            return "entrantRatingSummaryReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
