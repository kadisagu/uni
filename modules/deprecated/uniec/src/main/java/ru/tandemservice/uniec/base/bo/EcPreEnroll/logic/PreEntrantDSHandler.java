/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcPreEnroll.logic;

import java.util.List;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;

import ru.tandemservice.uniec.base.bo.EcPreEnroll.EcPreEnrollManager;
import ru.tandemservice.uniec.base.bo.EcPreEnroll.util.PreEntrantDTO;

/**
 * @author Vasily Zhukov
 * @since 26.05.2011
 */
public class PreEntrantDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String PARAM = "param";

    public PreEntrantDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        EcPreEnrollParam param = context.get(PARAM);

        List<PreEntrantDTO> preEntrantDTOList = EcPreEnrollManager.instance().dao().getPreEntrantDTOList(param);

        return ListOutputBuilder.get(input, preEntrantDTOList).pageable(true).build();
    }
}
