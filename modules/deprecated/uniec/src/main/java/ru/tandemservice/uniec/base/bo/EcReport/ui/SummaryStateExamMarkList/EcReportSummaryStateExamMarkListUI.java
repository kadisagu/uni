/**
 *$Id:$
 */
package ru.tandemservice.uniec.base.bo.EcReport.ui.SummaryStateExamMarkList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uniec.base.bo.EcReport.ui.SummaryStateExamMarkAdd.EcReportSummaryStateExamMarkAdd;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 30.07.12
 */
public class EcReportSummaryStateExamMarkListUI extends UIPresenter
{
    // property
    public static String ENROLLMENT_CAMPAIGN_PROPERTY = "enrollmentCampaign";

    @Override
    public void onComponentRefresh()
    {
        if (getSettings().get("enrollmentCampaign") == null)
        {
            List<EnrollmentCampaign> enrollmentCampaignList = DataAccessServices.dao().getList(EnrollmentCampaign.class, EnrollmentCampaign.id().s());
            getSettings().set("enrollmentCampaign", enrollmentCampaignList.isEmpty() ? null : enrollmentCampaignList.get(enrollmentCampaignList.size() - 1));
        }
    }

    // Listeners

    public void onClickAddReport()
    {
        getActivationBuilder().asRegion(EcReportSummaryStateExamMarkAdd.class).activate();
    }

    public void onPrintReport()
    {
        getActivationBuilder().asRegion(IUniComponents.DOWNLOAD_STORABLE_REPORT)
                .parameter("reportId", getListenerParameterAsLong())
                .parameter("extension", "xls")
                .activate();
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onClickShow()
    {
        saveSettings();
    }

    public void onClickClear()
    {
        clearSettings();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(EcReportSummaryStateExamMarkList.REPORT_LIST_DS))
        {
            dataSource.put(ENROLLMENT_CAMPAIGN_PROPERTY, getSettings().get("enrollmentCampaign"));
        }
    }
}
