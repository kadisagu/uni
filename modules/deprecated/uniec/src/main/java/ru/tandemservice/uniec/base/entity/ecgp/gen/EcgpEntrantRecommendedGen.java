package ru.tandemservice.uniec.base.entity.ecgp.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniec.base.entity.ecgp.EcgpDistribution;
import ru.tandemservice.uniec.base.entity.ecgp.EcgpEntrantRecommended;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.entrant.ProfileEducationOrgUnit;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Рекомендованный абитуриент распределения по профилям
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EcgpEntrantRecommendedGen extends EntityBase
 implements INaturalIdentifiable<EcgpEntrantRecommendedGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.base.entity.ecgp.EcgpEntrantRecommended";
    public static final String ENTITY_NAME = "ecgpEntrantRecommended";
    public static final int VERSION_HASH = -1701371411;
    private static IEntityMeta ENTITY_META;

    public static final String L_DISTRIBUTION = "distribution";
    public static final String L_PRE_STUDENT = "preStudent";
    public static final String L_PROFILE = "profile";

    private EcgpDistribution _distribution;     // Распределение абитуриентов по профилям
    private PreliminaryEnrollmentStudent _preStudent;     // Студент предварительного зачисления
    private ProfileEducationOrgUnit _profile;     // Профиль направления подготовки подразделения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Распределение абитуриентов по профилям. Свойство не может быть null.
     */
    @NotNull
    public EcgpDistribution getDistribution()
    {
        return _distribution;
    }

    /**
     * @param distribution Распределение абитуриентов по профилям. Свойство не может быть null.
     */
    public void setDistribution(EcgpDistribution distribution)
    {
        dirty(_distribution, distribution);
        _distribution = distribution;
    }

    /**
     * @return Студент предварительного зачисления. Свойство не может быть null.
     */
    @NotNull
    public PreliminaryEnrollmentStudent getPreStudent()
    {
        return _preStudent;
    }

    /**
     * @param preStudent Студент предварительного зачисления. Свойство не может быть null.
     */
    public void setPreStudent(PreliminaryEnrollmentStudent preStudent)
    {
        dirty(_preStudent, preStudent);
        _preStudent = preStudent;
    }

    /**
     * @return Профиль направления подготовки подразделения. Свойство не может быть null.
     */
    @NotNull
    public ProfileEducationOrgUnit getProfile()
    {
        return _profile;
    }

    /**
     * @param profile Профиль направления подготовки подразделения. Свойство не может быть null.
     */
    public void setProfile(ProfileEducationOrgUnit profile)
    {
        dirty(_profile, profile);
        _profile = profile;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EcgpEntrantRecommendedGen)
        {
            if (withNaturalIdProperties)
            {
                setDistribution(((EcgpEntrantRecommended)another).getDistribution());
                setPreStudent(((EcgpEntrantRecommended)another).getPreStudent());
            }
            setProfile(((EcgpEntrantRecommended)another).getProfile());
        }
    }

    public INaturalId<EcgpEntrantRecommendedGen> getNaturalId()
    {
        return new NaturalId(getDistribution(), getPreStudent());
    }

    public static class NaturalId extends NaturalIdBase<EcgpEntrantRecommendedGen>
    {
        private static final String PROXY_NAME = "EcgpEntrantRecommendedNaturalProxy";

        private Long _distribution;
        private Long _preStudent;

        public NaturalId()
        {}

        public NaturalId(EcgpDistribution distribution, PreliminaryEnrollmentStudent preStudent)
        {
            _distribution = ((IEntity) distribution).getId();
            _preStudent = ((IEntity) preStudent).getId();
        }

        public Long getDistribution()
        {
            return _distribution;
        }

        public void setDistribution(Long distribution)
        {
            _distribution = distribution;
        }

        public Long getPreStudent()
        {
            return _preStudent;
        }

        public void setPreStudent(Long preStudent)
        {
            _preStudent = preStudent;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EcgpEntrantRecommendedGen.NaturalId) ) return false;

            EcgpEntrantRecommendedGen.NaturalId that = (NaturalId) o;

            if( !equals(getDistribution(), that.getDistribution()) ) return false;
            if( !equals(getPreStudent(), that.getPreStudent()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getDistribution());
            result = hashCode(result, getPreStudent());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getDistribution());
            sb.append("/");
            sb.append(getPreStudent());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EcgpEntrantRecommendedGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EcgpEntrantRecommended.class;
        }

        public T newInstance()
        {
            return (T) new EcgpEntrantRecommended();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "distribution":
                    return obj.getDistribution();
                case "preStudent":
                    return obj.getPreStudent();
                case "profile":
                    return obj.getProfile();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "distribution":
                    obj.setDistribution((EcgpDistribution) value);
                    return;
                case "preStudent":
                    obj.setPreStudent((PreliminaryEnrollmentStudent) value);
                    return;
                case "profile":
                    obj.setProfile((ProfileEducationOrgUnit) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "distribution":
                        return true;
                case "preStudent":
                        return true;
                case "profile":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "distribution":
                    return true;
                case "preStudent":
                    return true;
                case "profile":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "distribution":
                    return EcgpDistribution.class;
                case "preStudent":
                    return PreliminaryEnrollmentStudent.class;
                case "profile":
                    return ProfileEducationOrgUnit.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EcgpEntrantRecommended> _dslPath = new Path<EcgpEntrantRecommended>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EcgpEntrantRecommended");
    }
            

    /**
     * @return Распределение абитуриентов по профилям. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecgp.EcgpEntrantRecommended#getDistribution()
     */
    public static EcgpDistribution.Path<EcgpDistribution> distribution()
    {
        return _dslPath.distribution();
    }

    /**
     * @return Студент предварительного зачисления. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecgp.EcgpEntrantRecommended#getPreStudent()
     */
    public static PreliminaryEnrollmentStudent.Path<PreliminaryEnrollmentStudent> preStudent()
    {
        return _dslPath.preStudent();
    }

    /**
     * @return Профиль направления подготовки подразделения. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecgp.EcgpEntrantRecommended#getProfile()
     */
    public static ProfileEducationOrgUnit.Path<ProfileEducationOrgUnit> profile()
    {
        return _dslPath.profile();
    }

    public static class Path<E extends EcgpEntrantRecommended> extends EntityPath<E>
    {
        private EcgpDistribution.Path<EcgpDistribution> _distribution;
        private PreliminaryEnrollmentStudent.Path<PreliminaryEnrollmentStudent> _preStudent;
        private ProfileEducationOrgUnit.Path<ProfileEducationOrgUnit> _profile;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Распределение абитуриентов по профилям. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecgp.EcgpEntrantRecommended#getDistribution()
     */
        public EcgpDistribution.Path<EcgpDistribution> distribution()
        {
            if(_distribution == null )
                _distribution = new EcgpDistribution.Path<EcgpDistribution>(L_DISTRIBUTION, this);
            return _distribution;
        }

    /**
     * @return Студент предварительного зачисления. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecgp.EcgpEntrantRecommended#getPreStudent()
     */
        public PreliminaryEnrollmentStudent.Path<PreliminaryEnrollmentStudent> preStudent()
        {
            if(_preStudent == null )
                _preStudent = new PreliminaryEnrollmentStudent.Path<PreliminaryEnrollmentStudent>(L_PRE_STUDENT, this);
            return _preStudent;
        }

    /**
     * @return Профиль направления подготовки подразделения. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecgp.EcgpEntrantRecommended#getProfile()
     */
        public ProfileEducationOrgUnit.Path<ProfileEducationOrgUnit> profile()
        {
            if(_profile == null )
                _profile = new ProfileEducationOrgUnit.Path<ProfileEducationOrgUnit>(L_PROFILE, this);
            return _profile;
        }

        public Class getEntityClass()
        {
            return EcgpEntrantRecommended.class;
        }

        public String getEntityName()
        {
            return "ecgpEntrantRecommended";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
