/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.dao;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.uniec.entity.entrant.*;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author Боба
 * @since 11.08.2008
 */
public class EntranceEduLevelDAO extends UniBaseDao implements IEntranceEduLevelDAO
{
    public static final Comparator<EducationLevelsHighSchool> EDULEVELSHISHSCHOOL_COMPARATOR = (o1, o2) -> o1.getDisplayableTitle().compareTo(o2.getDisplayableTitle());

    @Override
    public EnrollmentDirection getEnrollmentDirection(IEntranceEduLevelModel model)
    {
        if (model.getFormativeOrgUnit() == null || model.getDevelopForm() == null || model.getDevelopCondition() == null || model.getDevelopTech() == null || model.getDevelopPeriod() == null)
        {
            return null;
        }

        List<Long> filterEntranceEducationOrgUnitIds = getFilterEnrollmentDirectionIds(model);

        Criteria criteria = getSession().createCriteria(EnrollmentDirection.class, "ee");
        criteria.createAlias("ee." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "e");

        criteria.add(Restrictions.eq(EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
        criteria.add(Restrictions.eq("e." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnit()));
        criteria.add(Restrictions.eq("e." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, model.getEducationLevelsHighSchool()));
        criteria.add(model.getTerritorialOrgUnit() == null ? Restrictions.isNull("e." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT) : Restrictions.eq("e." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnit()));
        criteria.add(Restrictions.eq("e." + EducationOrgUnit.L_DEVELOP_FORM, model.getDevelopForm()));
        criteria.add(Restrictions.eq("e." + EducationOrgUnit.L_DEVELOP_CONDITION, model.getDevelopCondition()));
        criteria.add(Restrictions.eq("e." + EducationOrgUnit.L_DEVELOP_TECH, model.getDevelopTech()));
        criteria.add(Restrictions.eq("e." + EducationOrgUnit.L_DEVELOP_PERIOD, model.getDevelopPeriod()));
        if (filterEntranceEducationOrgUnitIds.size() > 0)
        {
            criteria.add(Restrictions.not(Restrictions.in("ee." + EnrollmentDirection.P_ID, filterEntranceEducationOrgUnitIds)));
        }
        return (EnrollmentDirection) criteria.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    private List<Long> getFilterEnrollmentDirectionIds(IEntranceEduLevelModel model)
    {
        Entrant entrant = model.getEntrant();
        EntrantRequest entrantRequest = model.getEntrantRequest();

        if (entrant == null || entrantRequest == null) return Collections.emptyList();
        Criteria criteria = getSession().createCriteria(RequestedEnrollmentDirection.class, "r");
        criteria.createAlias("r." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "request");
        criteria.add(Restrictions.eq("request." + EntrantRequest.L_ENTRANT, entrant));
        if (entrantRequest.getId() != null)
            criteria.add(Restrictions.ne("request." + EntrantRequest.P_ID, entrantRequest.getId()));

        criteria.setProjection(Projections.property("r." + RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.P_ID));
        return criteria.list();
    }

    @Override
    public List<EducationLevelsHighSchool> getEducationLevelsHighSchoolList(IEntranceEduLevelModel model)
    {
        if (model.getFormativeOrgUnit() == null)
        {
            return Collections.emptyList();
        }

        MQBuilder builder = new MQBuilder(EnrollmentDirection.ENTITY_CLASS, "ee");
        builder.addJoin("ee", EnrollmentDirection.L_EDUCATION_ORG_UNIT, "e");
        builder.addJoin("e", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "h");
        builder.add(MQExpression.eq("ee", EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
        builder.add(MQExpression.eq("e", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnit()));
        builder.add(model.getTerritorialOrgUnit() == null ? MQExpression.isNull("e", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT) : MQExpression.eq("e", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnit()));
        builder.setNeedDistinct(true);
        builder.getSelectAliasList().clear();
        builder.addSelect("h");

        List<EducationLevelsHighSchool> resultList = builder.getResultList(getSession());
        Collections.sort(resultList, EDULEVELSHISHSCHOOL_COMPARATOR);
        return resultList;
    }

    @Override
    public List<DevelopForm> getDevelopFormList(IEntranceEduLevelModel model)
    {
        if (model.getFormativeOrgUnit() == null || model.getEducationLevelsHighSchool() == null)
        {
            return Collections.emptyList();
        }

        MQBuilder builder = new MQBuilder(EnrollmentDirection.ENTITY_CLASS, "ee");
        builder.addJoin("ee", EnrollmentDirection.L_EDUCATION_ORG_UNIT, "e");
        builder.addJoin("e", EducationOrgUnit.L_DEVELOP_FORM, "f");
        builder.add(MQExpression.eq("ee", EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
        builder.add(MQExpression.eq("e", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnit()));
        builder.add(model.getTerritorialOrgUnit() == null ? MQExpression.isNull("e", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT) : MQExpression.eq("e", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnit()));
        builder.add(MQExpression.eq("e", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, model.getEducationLevelsHighSchool()));
        builder.getSelectAliasList().clear();
        builder.addSelect("f");
        builder.setNeedDistinct(true);
        builder.addOrder("f", DevelopForm.P_CODE);

        return builder.getResultList(getSession());
    }

    @Override
    public List<DevelopCondition> getDevelopConditionList(IEntranceEduLevelModel model)
    {
        if (model.getFormativeOrgUnit() == null || model.getEducationLevelsHighSchool() == null || model.getDevelopForm() == null)
        {
            return Collections.emptyList();
        }

        MQBuilder builder = new MQBuilder(EnrollmentDirection.ENTITY_CLASS, "ee");
        builder.addJoin("ee", EnrollmentDirection.L_EDUCATION_ORG_UNIT, "e");
        builder.addJoin("e", EducationOrgUnit.L_DEVELOP_CONDITION, "c");
        builder.add(MQExpression.eq("ee", EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
        builder.add(MQExpression.eq("e", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnit()));
        builder.add(model.getTerritorialOrgUnit() == null ? MQExpression.isNull("e", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT) : MQExpression.eq("e", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnit()));
        builder.add(MQExpression.eq("e", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, model.getEducationLevelsHighSchool()));
        builder.add(MQExpression.eq("e", EducationOrgUnit.L_DEVELOP_FORM, model.getDevelopForm()));
        builder.getSelectAliasList().clear();
        builder.addSelect("c");
        builder.setNeedDistinct(true);
        builder.addOrder("c", DevelopCondition.P_CODE);

        return builder.getResultList(getSession());
    }

    @Override
    public List<DevelopTech> getDevelopTechList(IEntranceEduLevelModel model)
    {
        if (model.getFormativeOrgUnit() == null || model.getEducationLevelsHighSchool() == null || model.getDevelopForm() == null || model.getDevelopCondition() == null)
        {
            return Collections.emptyList();
        }

        MQBuilder builder = new MQBuilder(EnrollmentDirection.ENTITY_CLASS, "ee");
        builder.addJoin("ee", EnrollmentDirection.L_EDUCATION_ORG_UNIT, "e");
        builder.addJoin("e", EducationOrgUnit.L_DEVELOP_TECH, "t");
        builder.add(MQExpression.eq("ee", EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
        builder.add(MQExpression.eq("e", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnit()));
        builder.add(model.getTerritorialOrgUnit() == null ? MQExpression.isNull("e", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT) : MQExpression.eq("e", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnit()));
        builder.add(MQExpression.eq("e", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, model.getEducationLevelsHighSchool()));
        builder.add(MQExpression.eq("e", EducationOrgUnit.L_DEVELOP_FORM, model.getDevelopForm()));
        builder.add(MQExpression.eq("e", EducationOrgUnit.L_DEVELOP_CONDITION, model.getDevelopCondition()));
        builder.getSelectAliasList().clear();
        builder.addSelect("t");
        builder.setNeedDistinct(true);
        builder.addOrder("t", DevelopTech.P_CODE);

        return builder.getResultList(getSession());
    }

    @Override
    public List<DevelopPeriod> getDevelopPeriodList(IEntranceEduLevelModel model)
    {
        if (model.getFormativeOrgUnit() == null || model.getEducationLevelsHighSchool() == null || model.getDevelopForm() == null || model.getDevelopCondition() == null || model.getDevelopTech() == null)
        {
            return Collections.emptyList();
        }

        MQBuilder builder = new MQBuilder(EnrollmentDirection.ENTITY_CLASS, "ee");
        builder.addJoin("ee", EnrollmentDirection.L_EDUCATION_ORG_UNIT, "e");
        builder.addJoin("e", EducationOrgUnit.L_DEVELOP_PERIOD, "p");
        builder.add(MQExpression.eq("ee", EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
        builder.add(MQExpression.eq("e", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnit()));
        builder.add(model.getTerritorialOrgUnit() == null ? MQExpression.isNull("e", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT) : MQExpression.eq("e", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnit()));
        builder.add(MQExpression.eq("e", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, model.getEducationLevelsHighSchool()));
        builder.add(MQExpression.eq("e", EducationOrgUnit.L_DEVELOP_FORM, model.getDevelopForm()));
        builder.add(MQExpression.eq("e", EducationOrgUnit.L_DEVELOP_CONDITION, model.getDevelopCondition()));
        builder.add(MQExpression.eq("e", EducationOrgUnit.L_DEVELOP_TECH, model.getDevelopTech()));
        builder.getSelectAliasList().clear();
        builder.addSelect("p");
        builder.setNeedDistinct(true);

        List<DevelopPeriod> resultList = builder.getResultList(getSession());
        Collections.sort(resultList, ITitled.TITLED_COMPARATOR);
        return resultList;
    }

    @Override
    public ListResult<OrgUnit> getKindOrgUnitsList(String filter, String kindCode, String eduOrgUnitPropertyName, String eduOrgUnitFilterPropertyName, OrgUnit filterOrgUnit, Collection<Long> filterEnrollmentDirectionIds, final IPrincipalContext context, EnrollmentCampaign enrollmentCampaign, int maxResult)
    {
        MQBuilder subBuilder = new MQBuilder(EnrollmentDirection.ENTITY_CLASS, "ee", new String[]{EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + eduOrgUnitPropertyName + "." + OrgUnit.P_ID});
        subBuilder.add(MQExpression.eq("ee", EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign));
        if (filterOrgUnit != null && eduOrgUnitFilterPropertyName != null)
        {
            subBuilder.addJoin("ee", EnrollmentDirection.L_EDUCATION_ORG_UNIT, "eduOrgUnit");
            subBuilder.add(MQExpression.eq("eduOrgUnit", eduOrgUnitFilterPropertyName, filterOrgUnit));
        }
        if (filterEnrollmentDirectionIds != null && filterEnrollmentDirectionIds.size() > 0)
        {
            subBuilder.add(MQExpression.notIn("ee", EnrollmentDirection.P_ID, filterEnrollmentDirectionIds));
        }

        MQBuilder builder = new MQBuilder(OrgUnitToKindRelation.ENTITY_CLASS, "r", new String[]{OrgUnitToKindRelation.L_ORG_UNIT});
        builder.add(MQExpression.eq("r", OrgUnitToKindRelation.L_ORG_UNIT_KIND + "." + OrgUnitKind.P_CODE, kindCode));
        if (filter != null)
            builder.add(MQExpression.like("r", OrgUnitToKindRelation.L_ORG_UNIT + "." + OrgUnit.P_TITLE, "%" + filter));
        if (subBuilder.getExpressionList().size() > 1)
        {
            builder.add(MQExpression.in("r", OrgUnitToKindRelation.L_ORG_UNIT + "." + OrgUnit.P_ID, subBuilder));
        }
        builder.addOrder("r", OrgUnitToKindRelation.L_ORG_UNIT + "." + OrgUnit.P_TITLE);
        List<OrgUnit> result = builder.getResultList(getSession(), 0, 100);
        long count = builder.getResultCount(getSession());

        if (context == null)
        {
            return new ListResult<>(result, count);
        }

        CollectionUtils.filter(result, object -> {
            OrgUnit orgUnit = (OrgUnit) object;
            return CoreServices.securityService().check(orgUnit, context, "orgUnit_viewPub_" + orgUnit.getOrgUnitType().getCode());
        });
        return new ListResult<>(result, count);
    }
}
