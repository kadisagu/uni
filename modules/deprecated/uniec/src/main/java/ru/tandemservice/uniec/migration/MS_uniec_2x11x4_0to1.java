package ru.tandemservice.uniec.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniec_2x11x4_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.11.4")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность entranceExamPhase

		// создано свойство examCommissionMembership
		{
			// создать колонку
			tool.createColumn("entranceexamphase_t", new DBColumn("examcommissionmembership_p", DBType.createVarchar(255)));
		}


        ////////////////////////////////////////////////////////////////////////////////
        // сущность examPassDiscipline

        // создано свойство examCommissionMembership
        {
            // создать колонку
            tool.createColumn("exampassdiscipline_t", new DBColumn("examcommissionmembership_p", DBType.createVarchar(255)));
        }
    }
}