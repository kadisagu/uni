/**
 *$Id$
 */
package ru.tandemservice.uniec.base.bo.EcExamGroup.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniec.base.bo.EcExamGroup.ui.SpecificationTab.EcExamGroupSpecificationTabUI;
import ru.tandemservice.uniec.entity.entrant.ExamGroupSpecification;

/**
 * @author Alexander Shaburov
 * @since 09.07.12
 */
public class ExamGroupSpecDSHandler extends DefaultSearchDataSourceHandler
{
    public ExamGroupSpecDSHandler(String ownerId)
    {
        super(ownerId, ExamGroupSpecification.class);
    }

    @Override
    protected DSOutput execute(DSInput dsInput, ExecutionContext context)
    {
        Long examGroupId = context.get(EcExamGroupSpecificationTabUI.EXAM_GROUP_ID);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(ExamGroupSpecification.class, "b").column("b")
                .where(DQLExpressions.eqValue(DQLExpressions.property(ExamGroupSpecification.examGroup().id().fromAlias("b")), examGroupId))
                .order(DQLExpressions.property(ExamGroupSpecification.discipline().educationSubject().title().fromAlias("b")));

        return DQLSelectOutputBuilder.get(dsInput, builder, context.getSession()).build();
    }
}
