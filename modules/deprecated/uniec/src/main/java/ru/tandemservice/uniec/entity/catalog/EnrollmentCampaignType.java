package ru.tandemservice.uniec.entity.catalog;

import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.BaseDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import ru.tandemservice.uniec.entity.catalog.gen.*;


/**
 * @see ru.tandemservice.uniec.entity.catalog.gen.EnrollmentCampaignTypeGen
 */
public class EnrollmentCampaignType extends EnrollmentCampaignTypeGen  implements IDynamicCatalogItem
{

    public static EntityComboDataSourceHandler defaultSelectDSHandler(String name)
    {
        return new EntityComboDataSourceHandler(name, EnrollmentCampaignType.class)
                .order(EnrollmentCampaignType.title())
                .filter(EnrollmentCampaignType.title());
    }
}