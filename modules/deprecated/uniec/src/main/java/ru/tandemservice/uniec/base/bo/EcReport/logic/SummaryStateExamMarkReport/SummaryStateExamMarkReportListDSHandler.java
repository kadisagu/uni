/**
 *$Id:$
 */
package ru.tandemservice.uniec.base.bo.EcReport.logic.SummaryStateExamMarkReport;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.datasource.output.IDQLSelectOutputBuilder;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniec.base.bo.EcReport.ui.SummaryStateExamMarkList.EcReportSummaryStateExamMarkListUI;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.report.SummaryStateExamMarkReport;

import java.util.Date;
import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 30.07.12
 */
public class SummaryStateExamMarkReportListDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static String FROM_TO_PROPERTY = "fromTo";

    public SummaryStateExamMarkReportListDSHandler(String ownerId)
    {
        super(ownerId, SummaryStateExamMarkReport.class);
    }

    @Override
    protected DSOutput execute(DSInput dsInput, ExecutionContext context)
    {
        EnrollmentCampaign ec = context.get(EcReportSummaryStateExamMarkListUI.ENROLLMENT_CAMPAIGN_PROPERTY);

        DQLOrderDescriptionRegistry registry = new DQLOrderDescriptionRegistry(SummaryStateExamMarkReport.class, "b");

        DQLSelectBuilder dqlBuilder = new DQLSelectBuilder().fromEntity(SummaryStateExamMarkReport.class, "b").column("b")
                .where(DQLExpressions.eqValue(DQLExpressions.property(SummaryStateExamMarkReport.enrollmentCampaign().fromAlias("b")), ec));

        IDQLSelectOutputBuilder builder = DQLSelectOutputBuilder.get(dsInput, dqlBuilder, context.getSession()).order(registry);

        List<DataWrapper> wrapperList = DataWrapper.wrap(builder.build());

        for (DataWrapper wrapper : wrapperList)
        {
            Date requestDateFrom = ((SummaryStateExamMarkReport) wrapper.getWrapped()).getRequestDateFrom();
            Date requestDateTo = ((SummaryStateExamMarkReport) wrapper.getWrapped()).getRequestDateTo();
            wrapper.setProperty(FROM_TO_PROPERTY, DateFormatter.DEFAULT_DATE_FORMATTER.format(requestDateFrom) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(requestDateTo));
        }

        return ListOutputBuilder.get(dsInput, wrapperList).pageable(true).build();
    }
}
