package ru.tandemservice.uniec.entity.entrant;

import ru.tandemservice.uniec.entity.catalog.IndividualProgress;
import ru.tandemservice.uniec.entity.entrant.gen.*;

/** @see ru.tandemservice.uniec.entity.entrant.gen.EnrCampaignEntrantIndividualProgressGen */
public class EnrCampaignEntrantIndividualProgress extends EnrCampaignEntrantIndividualProgressGen
{
    public static final String INDIVID_ARCHIEVEMENT_USED = "usedAchievement";
    public EnrCampaignEntrantIndividualProgress()
    {
    };

    public EnrCampaignEntrantIndividualProgress(EnrollmentCampaign campaign, IndividualProgress individualProgress)
    {
        setEnrollmentCampaign(campaign);
        setIndividualProgress(individualProgress);
    }
}