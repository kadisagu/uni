package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate;
import ru.tandemservice.uniec.entity.entrant.StateExamSubjectMark;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Балл по предмету ЕГЭ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StateExamSubjectMarkGen extends VersionedEntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.StateExamSubjectMark";
    public static final String ENTITY_NAME = "stateExamSubjectMark";
    public static final int VERSION_HASH = 1368203198;
    private static IEntityMeta ENTITY_META;

    public static final String P_VERSION = "version";
    public static final String L_SUBJECT = "subject";
    public static final String L_CERTIFICATE = "certificate";
    public static final String P_MARK = "mark";
    public static final String P_YEAR = "year";
    public static final String P_CERTIFICATE_NUMBER = "certificateNumber";

    private int _version; 
    private StateExamSubject _subject;     // Предмет ЕГЭ
    private EntrantStateExamCertificate _certificate;     // Сертификат ЕГЭ
    private int _mark;     // Балл
    private Integer _year;     // Год сдачи
    private String _certificateNumber;     // Номер свидетельства

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public int getVersion()
    {
        return _version;
    }

    /**
     * @param version  Свойство не может быть null.
     */
    public void setVersion(int version)
    {
        dirty(_version, version);
        _version = version;
    }

    /**
     * @return Предмет ЕГЭ. Свойство не может быть null.
     */
    @NotNull
    public StateExamSubject getSubject()
    {
        return _subject;
    }

    /**
     * @param subject Предмет ЕГЭ. Свойство не может быть null.
     */
    public void setSubject(StateExamSubject subject)
    {
        dirty(_subject, subject);
        _subject = subject;
    }

    /**
     * @return Сертификат ЕГЭ. Свойство не может быть null.
     */
    @NotNull
    public EntrantStateExamCertificate getCertificate()
    {
        return _certificate;
    }

    /**
     * @param certificate Сертификат ЕГЭ. Свойство не может быть null.
     */
    public void setCertificate(EntrantStateExamCertificate certificate)
    {
        dirty(_certificate, certificate);
        _certificate = certificate;
    }

    /**
     * @return Балл. Свойство не может быть null.
     */
    @NotNull
    public int getMark()
    {
        return _mark;
    }

    /**
     * @param mark Балл. Свойство не может быть null.
     */
    public void setMark(int mark)
    {
        dirty(_mark, mark);
        _mark = mark;
    }

    /**
     * @return Год сдачи.
     */
    public Integer getYear()
    {
        return _year;
    }

    /**
     * @param year Год сдачи.
     */
    public void setYear(Integer year)
    {
        dirty(_year, year);
        _year = year;
    }

    /**
     * @return Номер свидетельства.
     */
    @Length(max=13)
    public String getCertificateNumber()
    {
        return _certificateNumber;
    }

    /**
     * @param certificateNumber Номер свидетельства.
     */
    public void setCertificateNumber(String certificateNumber)
    {
        dirty(_certificateNumber, certificateNumber);
        _certificateNumber = certificateNumber;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StateExamSubjectMarkGen)
        {
            setVersion(((StateExamSubjectMark)another).getVersion());
            setSubject(((StateExamSubjectMark)another).getSubject());
            setCertificate(((StateExamSubjectMark)another).getCertificate());
            setMark(((StateExamSubjectMark)another).getMark());
            setYear(((StateExamSubjectMark)another).getYear());
            setCertificateNumber(((StateExamSubjectMark)another).getCertificateNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StateExamSubjectMarkGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StateExamSubjectMark.class;
        }

        public T newInstance()
        {
            return (T) new StateExamSubjectMark();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "version":
                    return obj.getVersion();
                case "subject":
                    return obj.getSubject();
                case "certificate":
                    return obj.getCertificate();
                case "mark":
                    return obj.getMark();
                case "year":
                    return obj.getYear();
                case "certificateNumber":
                    return obj.getCertificateNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "version":
                    obj.setVersion((Integer) value);
                    return;
                case "subject":
                    obj.setSubject((StateExamSubject) value);
                    return;
                case "certificate":
                    obj.setCertificate((EntrantStateExamCertificate) value);
                    return;
                case "mark":
                    obj.setMark((Integer) value);
                    return;
                case "year":
                    obj.setYear((Integer) value);
                    return;
                case "certificateNumber":
                    obj.setCertificateNumber((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "version":
                        return true;
                case "subject":
                        return true;
                case "certificate":
                        return true;
                case "mark":
                        return true;
                case "year":
                        return true;
                case "certificateNumber":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "version":
                    return true;
                case "subject":
                    return true;
                case "certificate":
                    return true;
                case "mark":
                    return true;
                case "year":
                    return true;
                case "certificateNumber":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "version":
                    return Integer.class;
                case "subject":
                    return StateExamSubject.class;
                case "certificate":
                    return EntrantStateExamCertificate.class;
                case "mark":
                    return Integer.class;
                case "year":
                    return Integer.class;
                case "certificateNumber":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StateExamSubjectMark> _dslPath = new Path<StateExamSubjectMark>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StateExamSubjectMark");
    }
            

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.StateExamSubjectMark#getVersion()
     */
    public static PropertyPath<Integer> version()
    {
        return _dslPath.version();
    }

    /**
     * @return Предмет ЕГЭ. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.StateExamSubjectMark#getSubject()
     */
    public static StateExamSubject.Path<StateExamSubject> subject()
    {
        return _dslPath.subject();
    }

    /**
     * @return Сертификат ЕГЭ. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.StateExamSubjectMark#getCertificate()
     */
    public static EntrantStateExamCertificate.Path<EntrantStateExamCertificate> certificate()
    {
        return _dslPath.certificate();
    }

    /**
     * @return Балл. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.StateExamSubjectMark#getMark()
     */
    public static PropertyPath<Integer> mark()
    {
        return _dslPath.mark();
    }

    /**
     * @return Год сдачи.
     * @see ru.tandemservice.uniec.entity.entrant.StateExamSubjectMark#getYear()
     */
    public static PropertyPath<Integer> year()
    {
        return _dslPath.year();
    }

    /**
     * @return Номер свидетельства.
     * @see ru.tandemservice.uniec.entity.entrant.StateExamSubjectMark#getCertificateNumber()
     */
    public static PropertyPath<String> certificateNumber()
    {
        return _dslPath.certificateNumber();
    }

    public static class Path<E extends StateExamSubjectMark> extends EntityPath<E>
    {
        private PropertyPath<Integer> _version;
        private StateExamSubject.Path<StateExamSubject> _subject;
        private EntrantStateExamCertificate.Path<EntrantStateExamCertificate> _certificate;
        private PropertyPath<Integer> _mark;
        private PropertyPath<Integer> _year;
        private PropertyPath<String> _certificateNumber;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.StateExamSubjectMark#getVersion()
     */
        public PropertyPath<Integer> version()
        {
            if(_version == null )
                _version = new PropertyPath<Integer>(StateExamSubjectMarkGen.P_VERSION, this);
            return _version;
        }

    /**
     * @return Предмет ЕГЭ. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.StateExamSubjectMark#getSubject()
     */
        public StateExamSubject.Path<StateExamSubject> subject()
        {
            if(_subject == null )
                _subject = new StateExamSubject.Path<StateExamSubject>(L_SUBJECT, this);
            return _subject;
        }

    /**
     * @return Сертификат ЕГЭ. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.StateExamSubjectMark#getCertificate()
     */
        public EntrantStateExamCertificate.Path<EntrantStateExamCertificate> certificate()
        {
            if(_certificate == null )
                _certificate = new EntrantStateExamCertificate.Path<EntrantStateExamCertificate>(L_CERTIFICATE, this);
            return _certificate;
        }

    /**
     * @return Балл. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.StateExamSubjectMark#getMark()
     */
        public PropertyPath<Integer> mark()
        {
            if(_mark == null )
                _mark = new PropertyPath<Integer>(StateExamSubjectMarkGen.P_MARK, this);
            return _mark;
        }

    /**
     * @return Год сдачи.
     * @see ru.tandemservice.uniec.entity.entrant.StateExamSubjectMark#getYear()
     */
        public PropertyPath<Integer> year()
        {
            if(_year == null )
                _year = new PropertyPath<Integer>(StateExamSubjectMarkGen.P_YEAR, this);
            return _year;
        }

    /**
     * @return Номер свидетельства.
     * @see ru.tandemservice.uniec.entity.entrant.StateExamSubjectMark#getCertificateNumber()
     */
        public PropertyPath<String> certificateNumber()
        {
            if(_certificateNumber == null )
                _certificateNumber = new PropertyPath<String>(StateExamSubjectMarkGen.P_CERTIFICATE_NUMBER, this);
            return _certificateNumber;
        }

        public Class getEntityClass()
        {
            return StateExamSubjectMark.class;
        }

        public String getEntityName()
        {
            return "stateExamSubjectMark";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
