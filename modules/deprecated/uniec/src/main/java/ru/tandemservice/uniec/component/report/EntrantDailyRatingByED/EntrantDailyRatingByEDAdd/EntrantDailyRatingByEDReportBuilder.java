/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.report.EntrantDailyRatingByED.EntrantDailyRatingByEDAdd;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineKind;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.examset.ExamSetItem;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.entity.settings.SetDiscipline;
import ru.tandemservice.uniec.ui.EnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.ExamSetUtil;
import ru.tandemservice.uniec.util.MarkDistributionUtil;
import ru.tandemservice.uniec.util.report.IReportRow;
import ru.tandemservice.uniec.util.report.RequestedEnrollmentDirectionComparator;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author vip_delete
 * @since 23.06.2009
 */
public class EntrantDailyRatingByEDReportBuilder
{
    private Model _model;
    private Session _session;

    public EntrantDailyRatingByEDReportBuilder(Model model, Session session)
    {
        _model = model;
        _session = session;
    }

    public DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    @SuppressWarnings("deprecation")
    private byte[] buildReport()
    {
        // создаем запрос по выбранным направления приема
        MQBuilder directionBuilder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "r");
        directionBuilder.addJoinFetch("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "request");
        directionBuilder.addJoinFetch("request", EntrantRequest.L_ENTRANT, "entrant");
        directionBuilder.addJoinFetch("entrant", Entrant.L_PERSON, "person");
        directionBuilder.addJoinFetch("person", Person.L_IDENTITY_CARD, "identityCard");
        directionBuilder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_COMPENSATION_TYPE, _model.getReport().getCompensationType()));
        directionBuilder.add(UniMQExpression.betweenDate("r", RequestedEnrollmentDirection.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));
        directionBuilder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, Boolean.FALSE));
        directionBuilder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE));
        directionBuilder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        if (_model.getReport().isOnlyWithOriginals())
            directionBuilder.add(MQExpression.eq("r", RequestedEnrollmentDirection.P_ORIGINAL_DOCUMENT_HANDED_IN, Boolean.TRUE));
        if (!_model.getStudentCategoryList().isEmpty())
            directionBuilder.add(MQExpression.in("r", RequestedEnrollmentDirection.L_STUDENT_CATEGORY, _model.getStudentCategoryList()));

        // получаем список направлений приема
        List<EnrollmentDirection> enrollmentDirectionList;
        if (_model.isByAllEnrollmentDirections())
        {
            MQBuilder builder = new MQBuilder(EnrollmentDirection.ENTITY_CLASS, "d");
            builder.add(MQExpression.eq("d", EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, _model.getEnrollmentCampaign()));
            if (!_model.getQualificationList().isEmpty())
                builder.add(MQExpression.in("d", EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL + "." + EducationLevelsHighSchool.L_EDUCATION_LEVEL + "." + EducationLevels.L_QUALIFICATION, _model.getQualificationList()));
            if (!_model.getDevelopFormList().isEmpty())
                builder.add(MQExpression.in("d", EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_FORM, _model.getDevelopFormList()));
            if (!_model.getDevelopConditionList().isEmpty())
                builder.add(MQExpression.in("d", EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_CONDITION, _model.getDevelopConditionList()));
            enrollmentDirectionList = builder.getResultList(_session);
            Collections.sort(enrollmentDirectionList, ITitled.TITLED_COMPARATOR);

            directionBuilder.add(MQExpression.in("r", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, builder));
        } else
        {
            EnrollmentDirection enrollmentDirection = EnrollmentDirectionUtil.getEnrollmentDirection(_model, _session);
            enrollmentDirectionList = Arrays.asList(enrollmentDirection);

            directionBuilder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, enrollmentDirection));
        }

        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ENTRANT_DAILY_RATING_BY_ED);
        RtfDocument template = new RtfReader().read(templateDocument.getCurrentTemplate());
        RtfDocument result = RtfBean.getElementFactory().createRtfDocument();
        result.setHeader(template.getHeader());
        result.setSettings(template.getSettings());

        // fetch data
        final Map<EnrollmentDirection, List<PairKey<EntranceDisciplineKind, Set<Discipline2RealizationWayRelation>>>> direction2disciplinesMap = ExamSetUtil.getDirection2disciplinesMap(_session, _model.getReport().getEnrollmentCampaign(), _model.getStudentCategoryList(), _model.getReport().getCompensationType());
        final Map<EnrollmentDirection, Map<StudentCategory, List<ExamSetItem>>> direction2examSetDetailMap = ExamSetUtil.getDirection2examSetDetailMap(_model.getReport().getEnrollmentCampaign(), _model.getStudentCategoryList(), _model.getReport().getCompensationType());

        EntrantDataUtil dataUtil = new EntrantDataUtil(_session, _model.getEnrollmentCampaign(), directionBuilder);
        Map<Person, Set<PersonBenefit>> benefitsByPerson = EntrantDataUtil.getPersonBenefitMap(_session, directionBuilder);

        for (EnrollmentDirection direction : enrollmentDirectionList)
            if (!direction2disciplinesMap.containsKey(direction))
                throw new ApplicationException("Для направления приема '" + direction.getTitle() + "' по категориям поступающих и виду затрат наборы экзаменов различные.");

        for (final EnrollmentDirection direction : enrollmentDirectionList)
        {
            Set<RequestedEnrollmentDirection> directionSet = dataUtil.getDirectionSet(direction);
            if (_model.getReport().isNotPrintSpesWithoutRequest() && CollectionUtils.isEmpty(directionSet))
                continue;

            // получаем структуру набора экзаменов текущего направления приема
            final List<PairKey<EntranceDisciplineKind, Set<Discipline2RealizationWayRelation>>> structure = direction2disciplinesMap.get(direction);

            // fill reportRow list
            final Map<CompetitionKind, Integer> priorities = UniecDAOFacade.getSettingsDAO().getCompetitionKindPriorities(_model.getEnrollmentCampaign());

            // build rtf-data
            final int[] index ={1};
            String[][] table = null;
            if (!CollectionUtils.isEmpty(directionSet))
                table = directionSet.stream()
                        .map(item -> new ReportRow(item, dataUtil))
                        .sorted(ReportRow.getComparator(priorities, _model.getReport().isOrderByOriginals()))
                        .map(row -> {
                            Set<PersonBenefit> personBenefits = benefitsByPerson.get(row.getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getPerson());
                            return getTableRow(row, index[0]++, personBenefits, structure, dataUtil);
                        })
                        .toArray(String[][]::new);

            OrgUnit territorialOrgUnit = direction.getEducationOrgUnit().getTerritorialOrgUnit();
            String orgUnitTitle = "";
            if (territorialOrgUnit != null)
                orgUnitTitle = "          " + (StringUtils.isEmpty(territorialOrgUnit.getNominativeCaseTitle()) ? territorialOrgUnit.getFullTitle() : territorialOrgUnit.getNominativeCaseTitle());

            // run inject modifier
            RtfDocument document = template.getClone();
            RtfString str = new RtfString();
            Qualifications qualifications = direction.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getQualification();
            str.append("Рейтинг абитуриентов на ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(_model.getReport().getFormingDate())).append(IRtfData.PAR).append(direction.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getTitleCodePrefix()).append(" ").append(direction.getEducationOrgUnit().getEducationLevelHighSchool().getTitleWithProfile()).append(" (")
            .append(null == qualifications ? "" : qualifications.getTitle().toLowerCase())
            .append(")").append(IRtfData.PAR).append(direction.getEducationOrgUnit().getDevelopForm().getTitle()).append(" форма обучения (").append(direction.getEducationOrgUnit().getDevelopCondition().getTitle()).append(", ").append(direction.getEducationOrgUnit().getDevelopPeriod().getTitle()).append(")                   ").append((_model.getReport().getCompensationType().isBudget() ? "За счет средств федерального бюджета" : "На места с оплатой стоимости обучения")).append(orgUnitTitle).append(IRtfData.PAR);
            if(!_model.getReport().isNotPrintNumInfo())
                str.append("Количество бюджетных мест ").append((null == direction.getMinisterialPlan() ? "0" : String.valueOf(direction.getMinisterialPlan()))).append(", в том числе целевых ").append((null == direction.getTargetAdmissionPlanBudget() ? "0" : String.valueOf(direction.getTargetAdmissionPlanBudget())));
            RtfInjectModifier injectModifier = new RtfInjectModifier();
            injectModifier.put("PARAM", str.toList());
            injectModifier.modify(document);

            // run table modifier
            RtfTableModifier modifier = new RtfTableModifier();
            modifier.put("T", table);

            modifier.put("T", getRowIntercepterBase(structure, direction2examSetDetailMap.get(direction)));

            modifier.modify(document);

            result.getElementList().addAll(document.getElementList());
        }

        return RtfUtil.toByteArray(result);
    }

    protected String[] getTableRow(ReportRow row, int index,
                                   Set<PersonBenefit> personBenefits,
                                   List<PairKey<EntranceDisciplineKind, Set<Discipline2RealizationWayRelation>>> structure,
                                   EntrantDataUtil dataUtil)
    {
        List<String> result = new ArrayList<>();

        RequestedEnrollmentDirection requestedEnrollmentDirection = row.getRequestedEnrollmentDirection();
        Person person = requestedEnrollmentDirection.getEntrantRequest().getEntrant().getPerson();

        result.add(String.valueOf(index));

        result.add(getRegNumber(requestedEnrollmentDirection));

        result.add(person.getFullFio());

        if (!_model.getReport().isWithoutDocumentInfo())
            result.add(requestedEnrollmentDirection.isOriginalDocumentHandedIn() ? "Оригинал" : "Копия");
        if (!_model.getReport().isWithoutAgree4Enrollment())
            result.add(requestedEnrollmentDirection.isAgree4Enrollment() ? "Да" : "Нет");
        result.add(getCategory(requestedEnrollmentDirection, personBenefits));
        if (_model.getReport().isShowRequestedDirectionPriority())
            result.add(String.valueOf(requestedEnrollmentDirection.getPriority()));

        if (!_model.getReport().isWithoutDetailSumMark())
        {
            if (structure.isEmpty())
            {
                result.add("");
            }
            else
            {
                Set<ChosenEntranceDiscipline> chosenSet = dataUtil.getChosenEntranceDisciplineSet(requestedEnrollmentDirection);
                ChosenEntranceDiscipline[] distribution = MarkDistributionUtil.getNearestDistribution(chosenSet, structure);

                for (int k = 0; k < structure.size(); k++)
                {
                    ChosenEntranceDiscipline disc = distribution[k];
                    Double mark = disc == null ? null : row.getMarkMap().get(disc.getEnrollmentCampaignDiscipline());

                    result.add(mark == null ? "x" : (mark == -1.0 ? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(mark)));
                }
            }
        }

        if (_model.getEnrollmentCampaign().isUseIndividualProgressInAmountMark())
            result.add(String.valueOf(row.getIndividualMark()));
        result.add(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(row.getSumMark()));

        return result.toArray(new String[result.size()]);
    }

    protected RtfRowIntercepterBase getRowIntercepterBase(List<PairKey<EntranceDisciplineKind, Set<Discipline2RealizationWayRelation>>> structure,
                                                          Map<StudentCategory, List<ExamSetItem>> examSetByStudCategory){
        return new RtfRowIntercepterBase()
        {
            @Override
            public void beforeModify(RtfTable table, int currentRowIndex)
            {
                RtfRow headRow = table.getRowList().get(currentRowIndex - 1);
                List<RtfCell> headerCellList = headRow.getCellList();

                RtfRow firstRow = table.getRowList().get(currentRowIndex);
                List<RtfCell> cellList = firstRow.getCellList();

                List<RtfCell> forHeaderDelete = new ArrayList<>();
                List<RtfCell> forDelete = new ArrayList<>();

                if (_model.getReport().isWithoutDocumentInfo())
                {
                    forHeaderDelete.add(headerCellList.get(3));
                    forDelete.add(cellList.get(3));
                }

                if (_model.getReport().isWithoutAgree4Enrollment())
                {
                    forHeaderDelete.add(headerCellList.get(4));
                    forDelete.add(cellList.get(4));
                }

                if (!_model.getReport().isShowRequestedDirectionPriority())
                {
                    forHeaderDelete.add(headerCellList.get(6));
                    forDelete.add(cellList.get(6));
                }
                //не учитываются инд. достижения
                if (!_model.getEnrollmentCampaign().isUseIndividualProgressInAmountMark())
                {
                    forHeaderDelete.add(headerCellList.get(8));
                    forDelete.add(cellList.get(8));
                }

                if (_model.getReport().isWithoutDetailSumMark())
                {
                    forHeaderDelete.add(headerCellList.get(7));
                    forDelete.add(cellList.get(7));
                }
                else if (!structure.isEmpty())
                {
                    // разбиваем колонку по количеству вступительных испытаний в наборе
                    int[] parts = new int[structure.size()];
                    Arrays.fill(parts, 1);

                    RtfUtil.splitRow(headRow, 7, (newCell, index) -> {
                                         PairKey<EntranceDisciplineKind, Set<Discipline2RealizationWayRelation>> key = structure.get(index);
                                         EntranceDisciplineKind entranceDisciplineKind = key.getFirst();
                                         key.getSecond();

                                         RtfString rtfString = new RtfString();
                                         if (_model.getReport().isShowDisciplineTitles())
                                         {
                                             // сейчас хитро, надо выбрать по всем категориям самое короткое название
                                             int length = Integer.MAX_VALUE;
                                             List<String> columnTitleList = new ArrayList<>();
                                             for (StudentCategory studentCategory : _model.getStudentCategoryList())
                                             {
                                                 // тут не должно быть NPE по построению
                                                 ExamSetItem item = examSetByStudCategory.get(studentCategory).get(index);
                                                 List<String> titleList = new ArrayList<>();
                                                 titleList.add(item.getSubject().getShortTitle());
                                                 titleList.addAll(item.getChoice().stream().map(SetDiscipline::getShortTitle).collect(Collectors.toList()));

                                                 String title = StringUtils.join(titleList, " ");
                                                 if (title.length() < length)
                                                 {
                                                     length = title.length();
                                                     columnTitleList = titleList;
                                                 }
                                             }
                                             Collections.sort(columnTitleList);
                                             for (Iterator<String> iterator = columnTitleList.iterator(); iterator.hasNext(); )
                                             {
                                                 rtfString.append(iterator.next());
                                                 if (iterator.hasNext()) rtfString.append("/").append(IRtfData.LINE);
                                             }
                                         }
                                         else
                                         {
                                             rtfString.append(entranceDisciplineKind.getShortTitle());
                                         }

                                         newCell.setElementList(rtfString.toList());
                                     },
                                     parts);

                    RtfUtil.splitRow(firstRow, 7, null, parts);
                }

                headerCellList.removeAll(forHeaderDelete);
                cellList.removeAll(forDelete);
            }
        };
    }

    protected String getCategory(RequestedEnrollmentDirection requestedEnrollmentDirection, Set<PersonBenefit> personBenefits)
    {
        String result;
        CompetitionKind competitionKind = requestedEnrollmentDirection.getCompetitionKind();
        if (requestedEnrollmentDirection.isTargetAdmission())
        {
            result = "Целевой прием";

            if (!competitionKind.getCode().equals(UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION))
            {
                result += ", " + competitionKind.getShortTitle();
            }
        } else
        {
            result = competitionKind.getTitle();
        }
        return result;
    }

    /**
     * Для кастомизации в проектах
     * @return Рег. №
     */
    protected String getRegNumber(RequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        return requestedEnrollmentDirection.getStringNumber();
    }

    private static class ReportRow implements IReportRow
    {
        private RequestedEnrollmentDirection _direction;
        private double _sumMark;
        private Map<Discipline2RealizationWayRelation, Double> _markMap;
        private Double averageEduInstMark;
        private int _individualMark; //Баллы за индивидуальные достижения

        private ReportRow(RequestedEnrollmentDirection direction, EntrantDataUtil dataUtil)
        {
            _direction = direction;
            _individualMark =  dataUtil.getIndividualProgressMarkByRequest(direction.getEntrantRequest().getId());
            _sumMark = dataUtil.getFinalMark(direction) + _individualMark;
            _markMap = dataUtil.getMarkMap(direction);
            averageEduInstMark = dataUtil.getAverageEduInstMark(direction.getEntrantRequest().getEntrant().getId());
        }

        // Getters & Setters

        @Override
        public RequestedEnrollmentDirection getRequestedEnrollmentDirection()
        {
            return _direction;
        }

        public double getSumMark()
        {
            return _sumMark;
        }

        public Map<Discipline2RealizationWayRelation, Double> getMarkMap()
        {
            return _markMap;
        }

        @Override
        public Double getProfileMark()
        {
            return (Double) FastBeanUtils.getValue(getRequestedEnrollmentDirection(), RequestedEnrollmentDirection.profileChosenEntranceDiscipline().finalMark().s());
        }

        @Override
        public Double getFinalMark()
        {
            return getSumMark();
        }

        @Override
        public String getFio()
        {
            return getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getPerson().getFullFio();
        }

        @Override
        public Boolean isGraduatedProfileEduInstitution()
        {
            return Boolean.FALSE; // мы не грузим эти данные, так что по ним не сравниваем
        }

        @Override
        public Double getCertificateAverageMark()
        {
            return averageEduInstMark;
        }

        public int getIndividualMark()
        {
            return _individualMark;
        }

        static RequestedEnrollmentDirectionComparator getComparator(Map<CompetitionKind, Integer> priorities, boolean isOrderByOriginals)
        {
            return new RequestedEnrollmentDirectionComparator(priorities)
            {
                @Override
                public int compare(IReportRow o1, IReportRow o2)
                {
                    int result = Boolean.compare(o2.getRequestedEnrollmentDirection().isOriginalDocumentHandedIn(),
                                                 o1.getRequestedEnrollmentDirection().isOriginalDocumentHandedIn());
                    if (result == 0 || !isOrderByOriginals)
                        return super.compare(o1, o2);
                    else return result;
                }
            };
        }
    }
}
