/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.menu.ExamSetList;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.examset.ExamSet;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Боба
 * @since 19.08.2008
 */
public class Model implements IEnrollmentCampaignSelectModel
{
    private IDataSettings _settings;
    private List<EnrollmentCampaign> _enrollmentCampaignList;
    private List<StudentCategory> _studentCategoryList;
    private DynamicListDataSource<ExamSet> _dataSource;
    private Map<Long, String> _id2examSetId = new HashMap<>();
    
    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return (EnrollmentCampaign) getSettings().get(ENROLLMENT_CAMPAIGN_FILTER_NAME);
    }
    
    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        getSettings().set(ENROLLMENT_CAMPAIGN_FILTER_NAME, enrollmentCampaign);
    }

    @Override
    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public List<StudentCategory> getStudentCategoryList()
    {
        return _studentCategoryList;
    }

    public void setStudentCategoryList(List<StudentCategory> studentCategoryList)
    {
        _studentCategoryList = studentCategoryList;
    }

    public DynamicListDataSource<ExamSet> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<ExamSet> dataSource)
    {
        _dataSource = dataSource;
    }

    public Map<Long, String> getId2examSetId()
    {
        return _id2examSetId;
    }

    public void setId2examSetId(Map<Long, String> id2examSetId)
    {
        _id2examSetId = id2examSetId;
    }
}
