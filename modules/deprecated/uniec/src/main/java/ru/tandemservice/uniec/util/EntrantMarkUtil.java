/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.util;

import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.uniec.entity.catalog.EntrantAbsenceNote;
import ru.tandemservice.uniec.entity.entrant.ExamPassMark;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author vip_delete
 * @since 02.04.2009
 */
public class EntrantMarkUtil
{
    public static void parseAndSetMark(ExamPassMark examPassMark, List<EntrantAbsenceNote> entrantAbsenceNote, String examMark)
    {
        Map<String, EntrantAbsenceNote> map = new HashMap<>();
        for (EntrantAbsenceNote note : entrantAbsenceNote)
            map.put(note.getShortTitle(), note);

        // здесь examMark не пустая строка и не null
        EntrantAbsenceNote note = map.get(examMark);
        if (note != null)
        {
            examPassMark.setEntrantAbsenceNote(note);
            examPassMark.setMark(0.0);
        } else
        {
            examPassMark.setEntrantAbsenceNote(null);
            try
            {
                examMark = examMark.replace(',', '.');
                double mark = Double.parseDouble(examMark);

                // учитываем только один знак после запятой
                int markInteger = (int) (mark * 10);

                examPassMark.setMark(markInteger / 10.0);
            } catch (NumberFormatException e)
            {
                throw new ApplicationException("Неправильный формат одной из оценок: " + examMark + ".");
            }
        }
    }
}
