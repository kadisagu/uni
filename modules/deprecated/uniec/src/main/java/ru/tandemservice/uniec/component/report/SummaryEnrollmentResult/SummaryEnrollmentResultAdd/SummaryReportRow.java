/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.SummaryEnrollmentResult.SummaryEnrollmentResultAdd;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.entity.*;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import org.tandemframework.shared.person.catalog.entity.EducationalInstitutionTypeKind;
import org.tandemframework.shared.person.catalog.entity.codes.EducationalInstitutionTypeKindCodes;
import org.tandemframework.shared.person.catalog.entity.codes.GraduationHonourCodes;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline;

import java.util.*;

/**
 * Строка отчета сводки
 * <p/>
 * с помощью метода appendRowItems в строке собираются все данные
 * с помощью метода buildRow идет подсчет статистики
 *
 * @author vip_delete
 * @since 21.04.2009
 */
class SummaryReportRow implements ITitled
{
    // 1. Название
    private String _title;

    // 1. Расширенное название
    private String _fullTitle;

    // надо ли использовать расширенное название
    private boolean _useFullTitle;

    // 2. План
    private int _plan;

    // 3. Всего
    private int _count;

    // 4. Конкурс 
    private String _contest;

    // 5. Медаль 
    private int _medal;

    // 6. Диплом с отличием
    private int _diploma;

    // 7. Внеконкурсный прием
    private int _countNoContest;
    private Map<String, Integer> _noContestStats;

    // 8. Целевой прием 
    private int _countTargetAdmission;
    private Map<String, Integer> _targetAdmissionStats;

    // 9. Стаж по профилю 
    private int _countServiceLength;

    // 11. Среднее полное образ-е
    private int _countTotalEducation;
    private int _currentTotalEducation;

    // 12. Нач. проф-ное
    private int _countBeginEducation;
    private int _currentBeginEducation;

    // 13. СПО
    private int _countProfileEducation;
    private int _countSPPOProfileEducation;
    private int _currentProfileEducation;
    private int _currentSPPOProfileEducation;

    // 14. ВО
    private int _countHighProfileEducation;
    private int _countHighProfileEducationBachelors;
    private int _currentHighProfileEducationBachelors;
    private int _countHighProfileEducationSpec;
    private int _currentHighProfileEducationSpec;
    private int _countHighProfileEducationMaster;
    private int _currentHighProfileEducationMaster;

    // 15. непВО
    private int _countHalfHighEducation;

    // 16. Слушатели
    private int _countListeners;
    private int _countStudListeners;

    // 17. Мужчин
    private int _countMans;
    private int _countSoldiers;

    // 18. Свердловская обл
    private int _countInHighSchoolArea;
    private int _countInHighSchoolCity;
    private int _countInHighSchoolVillage;

    // 19. Др. р-ны России
    private int _countInOtherArea;
    private int _countInOtherVillage;

    // 20. Др. гос-ва
    private int _countForeignArea;

    // 21. Иностр. граж-не
    private int _countForeignCitizenship;

    // 22. ЕГЭ-I (МОУ)
    private int _wave1countStateExam;
    private int[] _wave1stateExam;

    // 22. ЕГЭ-II (МОУ)
    private int _wave2countStateExam;
    private int[] _wave2stateExam;

    // Уч. Олимп (без. экз)
    private int _withoutExam;

    // Уч. Олимп (100 балл)
    private int _withDiplom;

    // веч
    private int _eveningSchool;

    // кор
    private int _stupidSchool;

    // Проф класс
    private int _withProfileClass;

    // Преимущественное право
    private int _withRecomendations;

    private Collection<ISummaryRowItem> _rowItems;

    public SummaryReportRow(String title, String fullTitle)
    {
        _title = title;
        _fullTitle = fullTitle;
        _rowItems = new ArrayList<>();
    }

    public void useFullTitle()
    {
        _useFullTitle = true;
    }

    public void appendRowItems(Collection<ISummaryRowItem> rowItems)
    {
        _rowItems.addAll(rowItems);
    }

    public void increasePlan(int plan)
    {
        _plan += plan;
    }

    public void buildRow(AddressItem highSchoolCity)
    {
        // 3. Всего
        _count = _rowItems.size();

        // 4. Конкурс
        _contest = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format((_count + 0.0) / (_plan + 0.0));

        // 5. Медаль + 6. Диплом с отличием
        {
            _medal = 0;
            _diploma = 0;
            for (ISummaryRowItem rowItem : _rowItems)
            {
                PersonEduInstitution personEduInstitution = rowItem.getLastPersonEduInstitution();
                if (personEduInstitution != null && personEduInstitution.getGraduationHonour() != null)
                {
                    String code = personEduInstitution.getGraduationHonour().getCode();
                    if (GraduationHonourCodes.GOLD_MEDAL.equals(code) || GraduationHonourCodes.SILVER_MEDAL.equals(code))
                        _medal++;
                    if (GraduationHonourCodes.RED_DIPLOMA.equals(code))
                        _diploma++;
                }
            }
        }

        // 7. Внеконкурсный прием
        {
            _countNoContest = 0;
            _noContestStats = new HashMap<>();
            for (ISummaryRowItem rowItem : _rowItems)
            {
                if (UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION.equals(rowItem.getCompetitionKind()))
                {
                    _countNoContest++;
                    Set<String> benefitSet = rowItem.getBenefitSet();
                    if (benefitSet != null)
                        for (String code : benefitSet)
                        {
                            Integer count = _noContestStats.get(code);
                            if (count == null) count = 0;
                            _noContestStats.put(code, count + 1);
                        }
                }
            }
        }

        // 8. Целевой прием
        {
            _countTargetAdmission = 0;
            _targetAdmissionStats = new HashMap<>();
            for (ISummaryRowItem rowItem : _rowItems)
            {
                if (rowItem.isTargetAdmission())
                {
                    _countTargetAdmission++;
                    if (rowItem.getTargetAdmissionKind() != null)
                    {
                        Integer count = _targetAdmissionStats.get(rowItem.getTargetAdmissionKind());
                        if (count == null) count = 0;
                        _targetAdmissionStats.put(rowItem.getTargetAdmissionKind(), count + 1);
                    }
                }
            }
        }

        // 9. Стаж по профилю
        {
            _countServiceLength = 0;
            for (ISummaryRowItem rowItem : _rowItems)
            {
                if (rowItem.isHasWorkExperience())
                    _countServiceLength++;
            }
        }

        // 11. Среднее полное образ-е + 12. Нач. проф-ное + 13. СПО + 14. ВО + 15. непВО
        {
            _countTotalEducation = 0;
            _currentTotalEducation = 0;
            _eveningSchool = 0;
            _stupidSchool = 0;

            _countBeginEducation = 0;
            _currentBeginEducation = 0;

            _countProfileEducation = 0;
            _currentProfileEducation = 0;

            _countHighProfileEducation = 0;

            _countHalfHighEducation = 0;
            for (ISummaryRowItem rowItem : _rowItems)
            {
                int currentYear = CoreDateUtils.getYear(rowItem.getEntrantRequest().getRegDate());

                PersonEduInstitution personEduInstitution = rowItem.getLastPersonEduInstitution();
                if (personEduInstitution != null)
                {
                    EducationalInstitutionTypeKind institutionTypeKind = personEduInstitution.getEduInstitutionKind();
                    String institutionTypeCode = institutionTypeKind.getParent() == null ? institutionTypeKind.getCode() : institutionTypeKind.getParent().getCode();
                    String institutionKindCode = institutionTypeKind.getCode();

                    String educationLevelStageCode = personEduInstitution.getEducationLevel() == null ? null : personEduInstitution.getEducationLevel().getCode();
                    String educationLevelStageParentCode = educationLevelStageCode == null || personEduInstitution.getEducationLevel().getParent() == null ? null : personEduInstitution.getEducationLevel().getParent().getCode();

                    if (educationLevelStageCode != null)
                    {
                        int year = personEduInstitution.getYearEnd();
                        if (UniDefines.EDUCATION_LEVEL_STAGE_FULL_EDUCATION.equals(educationLevelStageCode) || UniDefines.EDUCATION_LEVEL_STAGE_FULL_EDUCATION.equals(educationLevelStageParentCode))
                        {
                            _countTotalEducation++;
                            if (year == currentYear)
                            {
                                _currentTotalEducation++;
                                if (EducationalInstitutionTypeKindCodes.SCHOOL_EVENING.equals(institutionTypeCode))
                                    _eveningSchool++;
                                if (EducationalInstitutionTypeKindCodes.SCHOOL_STUPID_2.equals(institutionKindCode) || EducationalInstitutionTypeKindCodes.SCHOOL_STUPID_3.equals(institutionKindCode))
                                    _stupidSchool++;
                            }
                        } else if (UniDefines.EDUCATION_LEVEL_STAGE_BEGIN_PROFILE.equals(educationLevelStageCode) || UniDefines.EDUCATION_LEVEL_STAGE_BEGIN_PROFILE.equals(educationLevelStageParentCode))
                        {
                            _countBeginEducation++;
                            if (year == currentYear)
                                _currentBeginEducation++;
                        } else if (UniDefines.EDUCATION_LEVEL_STAGE_FULL_PROFILE.equals(educationLevelStageCode) || UniDefines.EDUCATION_LEVEL_STAGE_FULL_PROFILE.equals(educationLevelStageParentCode))
                        {
                            _countProfileEducation++;
                            if (rowItem.isHasProfileKnowledges())
                                _countSPPOProfileEducation++;
                            if (year == currentYear)
                            {
                                _currentProfileEducation++;
                                if (rowItem.isHasProfileKnowledges())
                                    _currentSPPOProfileEducation++;
                            }
                        } else if (UniDefines.EDUCATION_LEVEL_STAGE_HIGH_PROFILE.equals(educationLevelStageCode))
                        {
                            String code = personEduInstitution.getEducationLevelStage().getCode();
                            if (UniDefines.EDUCATION_LEVEL_STAGE_HALF_HIGH.equals(code))
                            {
                                _countHalfHighEducation++;
                            } else
                            {
                                _countHighProfileEducation++;
                                if (UniDefines.EDUCATION_LEVEL_STAGE_BACHELORS_DEGREE.equals(code))
                                {
                                    _countHighProfileEducationBachelors++;
                                    if (year == currentYear)
                                        _currentHighProfileEducationBachelors++;
                                } else if (UniDefines.EDUCATION_LEVEL_STAGE_CERTIFICATED_SPEICALISTS_TRAINING.equals(code))
                                {
                                    _countHighProfileEducationSpec++;
                                    if (year == currentYear)
                                        _currentHighProfileEducationSpec++;
                                } else if (UniDefines.EDUCATION_LEVEL_STAGE_MASTERS_DEGREE.equals(code))
                                {
                                    _countHighProfileEducationMaster++;
                                    if (year == currentYear)
                                        _currentHighProfileEducationMaster++;
                                }
                            }
                        }
                    }
                }
            }
        }

        // 16. Слушатели, Проф класс, Пр. право
        {
            _countListeners = 0;
            for (ISummaryRowItem rowItem : _rowItems)
            {
                if (UniDefines.STUDENT_CATEGORY_LISTENER.equals(rowItem.getStudentCategory()))
                {
                    _countListeners++;
                    if (rowItem.isHasStudents())
                        _countStudListeners++;
                }

                if (rowItem.getDirection().isGraduatedProfileClass())
                    _withProfileClass++;

                if (rowItem.isHasRecommendations())
                    _withRecomendations++;
            }
        }

        // 17. Мужчин
        {
            _countMans = 0;
            _countSoldiers = 0;
            for (ISummaryRowItem rowItem : _rowItems)
            {
                if (rowItem.getEntrant().getPerson().getIdentityCard().getSex().isMale())
                {
                    _countMans++;
                    if (rowItem.getEntrant().isPassArmy())
                        _countSoldiers++;
                }
            }
        }

        // 18. Свердловская обл + 19. Др. р-ны России
        {
            _countInHighSchoolArea = 0;
            _countInHighSchoolCity = 0;
            _countInHighSchoolVillage = 0;

            _countInOtherArea = 0;
            _countInOtherVillage = 0;

            if (highSchoolCity != null)
            {
                AddressItem highSchoolArea = highSchoolCity;
                while (highSchoolArea.getParent() != null) highSchoolArea = highSchoolArea.getParent();
                for (ISummaryRowItem rowItem : _rowItems)
                {
                    AddressBase address = rowItem.getEntrant().getPerson().getIdentityCard().getAddress();
                    if (address != null && address instanceof AddressDetailed)
                    {
                        AddressItem entrantCity = ((AddressDetailed)address).getSettlement();
                        if (entrantCity != null && entrantCity.getCountry().getCode() == IKladrDefines.RUSSIA_COUNTRY_CODE)
                        {
                            AddressItem entrantArea = entrantCity;
                            while (entrantArea.getParent() != null) entrantArea = entrantArea.getParent();

                            if (entrantArea.equals(highSchoolArea))
                            {
                                _countInHighSchoolArea++;
                                if (entrantCity.getAddressType().isCountryside())
                                    _countInHighSchoolVillage++;
                                if (entrantCity.equals(highSchoolCity))
                                    _countInHighSchoolCity++;
                            } else
                            {
                                _countInOtherArea++;
                                if (entrantCity.getAddressType().isCountryside())
                                    _countInOtherVillage++;
                            }
                        }
                    }
                }
            }
        }

        // 20. Др. гос-ва
        {
            _countForeignArea = 0;
            for (ISummaryRowItem rowItem : _rowItems)
            {
                PersonEduInstitution personEduInstitution = rowItem.getLastPersonEduInstitution();
                if (personEduInstitution != null)
                {
                    AddressCountry country = personEduInstitution.getAddressItem().getCountry();
                    if (country != null && country.getCode() != IKladrDefines.RUSSIA_COUNTRY_CODE)
                        _countForeignArea++;
                }
            }
        }

        // 21. Иностр. граж-не
        {
            _countForeignCitizenship = 0;
            for (ISummaryRowItem rowItem : _rowItems)
            {
                ICitizenship citizenship = rowItem.getEntrant().getPerson().getIdentityCard().getCitizenship();
                if (citizenship != null && citizenship.getCode() != IKladrDefines.RUSSIA_COUNTRY_CODE)
                    _countForeignCitizenship++;
            }
        }

        // 22. ЕГЭ-I (МОУ) + 22. ЕГЭ-II (МОУ)
        {
            final int MAX_DISCIPLINE_COUNT = 4;
            _wave1countStateExam = 0;
            _wave2countStateExam = 0;
            _wave1stateExam = new int[MAX_DISCIPLINE_COUNT];
            _wave2stateExam = new int[MAX_DISCIPLINE_COUNT];

            for (ISummaryRowItem rowItem : _rowItems)
            {
                Set<ChosenEntranceDiscipline> set = rowItem.getChosenEntranceDisciplineSet();
                if (set != null)
                {
                    int stateExamWave1 = 0;         // кол-во дисциплин, которые зачтены по ЕГЭ (1-ая волна)
                    int stateExamWave2 = 0;         // кол-во дисциплин, которые зачтены по ЕГЭ (2-ая волна) и по форме сдачи ЕГЭ (вуз)
                    for (ChosenEntranceDiscipline discipline : set)
                    {
                        if (discipline.getFinalMarkSource() != null)
                        {
                            switch (discipline.getFinalMarkSource())
                            {
                                case UniecDefines.FINAL_MARK_STATE_EXAM_WAVE1:
                                    stateExamWave1++;
                                    break;
                                case UniecDefines.FINAL_MARK_STATE_EXAM_WAVE2:
                                case UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL:
                                case UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL_APPEAL:
                                    stateExamWave2++;
                                    break;
                            }
                        }
                    }
                    if (stateExamWave1 > 0 && stateExamWave1 <= MAX_DISCIPLINE_COUNT)
                    {
                        _wave1countStateExam++;
                        _wave1stateExam[stateExamWave1 - 1]++;
                    }

                    if (stateExamWave2 > 0 && stateExamWave2 <= MAX_DISCIPLINE_COUNT)
                    {
                        _wave2countStateExam++;
                        _wave2stateExam[stateExamWave2 - 1]++;
                    }
                }
            }
        }

        // Учтение олимпиады
        {
            _withoutExam = 0;

            _withDiplom = 0;

            for (ISummaryRowItem rowItem : _rowItems)
            {
                // без экз. - число выбранных направлений приема с видом конкурса "без экзаменов" и у абитуриента (у которого выбрано данное направление приема) имеется в системе хотя бы один диплом участника олимпиады
                if (UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES.equals(rowItem.getCompetitionKind()) && rowItem.isHasOlympiadDiplomas())
                    _withoutExam++;

                // 100 балл - число выбранных направлений приема у которых по одному из вступительных испытаний источником итогового балла является форма сдача "диплом"
                boolean diplomAccepted = false;
                for (ChosenEntranceDiscipline chosen : rowItem.getChosenEntranceDisciplineSet())
                    diplomAccepted = diplomAccepted || (chosen.getFinalMarkSource() != null && chosen.getFinalMarkSource().equals(UniecDefines.FINAL_MARK_OLYMPIAD));

                if (diplomAccepted)
                    _withDiplom++;
            }
        }
    }

    // Getters

    @Override
    public String getTitle()
    {
        return _title;
    }

    public String getFullTitle()
    {
        return _fullTitle;
    }

    public boolean isUseFullTitle()
    {
        return _useFullTitle;
    }

    public int getPlan()
    {
        return _plan;
    }

    public int getCount()
    {
        return _count;
    }

    public String getContest()
    {
        return _contest;
    }

    public int getMedal()
    {
        return _medal;
    }

    public int getDiploma()
    {
        return _diploma;
    }

    public int getCountNoContest()
    {
        return _countNoContest;
    }

    public Map<String, Integer> getNoContestStats()
    {
        return _noContestStats;
    }

    public int getCountTargetAdmission()
    {
        return _countTargetAdmission;
    }

    public Map<String, Integer> getTargetAdmissionStats()
    {
        return _targetAdmissionStats;
    }

    public int getCountServiceLength()
    {
        return _countServiceLength;
    }

    public int getCountTotalEducation()
    {
        return _countTotalEducation;
    }

    public int getCurrentTotalEducation()
    {
        return _currentTotalEducation;
    }

    public int getCountBeginEducation()
    {
        return _countBeginEducation;
    }

    public int getCurrentBeginEducation()
    {
        return _currentBeginEducation;
    }

    public int getCountProfileEducation()
    {
        return _countProfileEducation;
    }

    public int getCountSPPOProfileEducation()
    {
        return _countSPPOProfileEducation;
    }

    public int getCurrentProfileEducation()
    {
        return _currentProfileEducation;
    }

    public int getCurrentSPPOProfileEducation()
    {
        return _currentSPPOProfileEducation;
    }

    public int getCountHighProfileEducation()
    {
        return _countHighProfileEducation;
    }

    public int getCountHighProfileEducationBachelors()
    {
        return _countHighProfileEducationBachelors;
    }

    public int getCurrentHighProfileEducationBachelors()
    {
        return _currentHighProfileEducationBachelors;
    }

    public int getCountHighProfileEducationSpec()
    {
        return _countHighProfileEducationSpec;
    }

    public int getCurrentHighProfileEducationSpec()
    {
        return _currentHighProfileEducationSpec;
    }

    public int getCountHighProfileEducationMaster()
    {
        return _countHighProfileEducationMaster;
    }

    public int getCurrentHighProfileEducationMaster()
    {
        return _currentHighProfileEducationMaster;
    }

    public int getCountHalfHighEducation()
    {
        return _countHalfHighEducation;
    }

    public int getCountListeners()
    {
        return _countListeners;
    }

    public int getCountStudListeners()
    {
        return _countStudListeners;
    }

    public int getCountMans()
    {
        return _countMans;
    }

    public int getCountSoldiers()
    {
        return _countSoldiers;
    }

    public int getCountInHighSchoolArea()
    {
        return _countInHighSchoolArea;
    }

    public int getCountInHighSchoolCity()
    {
        return _countInHighSchoolCity;
    }

    public int getCountInHighSchoolVillage()
    {
        return _countInHighSchoolVillage;
    }

    public int getCountInOtherArea()
    {
        return _countInOtherArea;
    }

    public int getCountInOtherVillage()
    {
        return _countInOtherVillage;
    }

    public int getCountForeignArea()
    {
        return _countForeignArea;
    }

    public int getCountForeignCitizenship()
    {
        return _countForeignCitizenship;
    }

    public int getWave1countStateExam()
    {
        return _wave1countStateExam;
    }

    public int[] getWave1stateExam()
    {
        return _wave1stateExam;
    }

    public int getWave2countStateExam()
    {
        return _wave2countStateExam;
    }

    public int[] getWave2stateExam()
    {
        return _wave2stateExam;
    }

    public int getWithoutExam()
    {
        return _withoutExam;
    }

    public int getWithDiplom()
    {
        return _withDiplom;
    }

    public int getEveningSchool()
    {
        return _eveningSchool;
    }

    public int getStupidSchool()
    {
        return _stupidSchool;
    }

    public int getWithProfileClass()
    {
        return _withProfileClass;
    }

    public int getWithRecomendations()
    {
        return _withRecomendations;
    }
}
