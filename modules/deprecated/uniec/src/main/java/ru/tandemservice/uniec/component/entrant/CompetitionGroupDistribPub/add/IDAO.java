package ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.add;

import ru.tandemservice.uni.dao.IPrepareable;

/**
 * @author vdanilov
 */
public interface IDAO extends IPrepareable<Model> {
	void save(Model model);
}
