/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantEnrollmentConditionsEdit;

import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;

/**
 * @author agolubenko
 * @since 07.04.2009
 */
@Input(@Bind(key = "preliminaryEnrollmentStudentId"))
public class Model
{
    public static final Long AUTO = 0L;
    public static final Long YES_LOCKED = 1L;
    private Long _preliminaryEnrollmentStudentId;
    private PreliminaryEnrollmentStudent _preliminaryEnrollmentStudent;
    private List<StudentCategory> _studentCategoryList;
    private List<CompensationType> _compensationTypeList;
    private List<IdentifiableWrapper> _parallelList;
    private IdentifiableWrapper _parallelValue;
    private boolean _existOrder;
    private ISelectModel _contractAdmissionKindModel;

    public Entrant getEntrant()
    {
        return _preliminaryEnrollmentStudent.getRequestedEnrollmentDirection().getEntrantRequest().getEntrant();
    }

    public boolean isListener()
    {
        return UniDefines.STUDENT_CATEGORY_LISTENER.equals(_preliminaryEnrollmentStudent.getStudentCategory().getCode());
    }

    // Getters & Setters

    public Long getPreliminaryEnrollmentStudentId()
    {
        return _preliminaryEnrollmentStudentId;
    }

    public void setPreliminaryEnrollmentStudentId(Long preliminaryEnrollmentStudentId)
    {
        _preliminaryEnrollmentStudentId = preliminaryEnrollmentStudentId;
    }

    public PreliminaryEnrollmentStudent getPreliminaryEnrollmentStudent()
    {
        return _preliminaryEnrollmentStudent;
    }

    public void setPreliminaryEnrollmentStudent(PreliminaryEnrollmentStudent preliminaryEnrollmentStudent)
    {
        _preliminaryEnrollmentStudent = preliminaryEnrollmentStudent;
    }

    public List<StudentCategory> getStudentCategoryList()
    {
        return _studentCategoryList;
    }

    public void setStudentCategoryList(List<StudentCategory> studentCategoryList)
    {
        _studentCategoryList = studentCategoryList;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public List<IdentifiableWrapper> getParallelList()
    {
        return _parallelList;
    }

    public void setParallelList(List<IdentifiableWrapper> parallelList)
    {
        _parallelList = parallelList;
    }

    public IdentifiableWrapper getParallelValue()
    {
        return _parallelValue;
    }

    public void setParallelValue(IdentifiableWrapper parallelValue)
    {
        _parallelValue = parallelValue;
    }

    public boolean isExistOrder()
    {
        return _existOrder;
    }

    public void setExistOrder(boolean existOrder)
    {
        _existOrder = existOrder;
    }

    public ISelectModel getContractAdmissionKindModel()
    {
        return _contractAdmissionKindModel;
    }

    public void setContractAdmissionKindModel(ISelectModel contractAdmissionKindModel)
    {
        _contractAdmissionKindModel = contractAdmissionKindModel;
    }
}
