/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.orgunit.OrgUnitEntrantList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.BooleanColumn;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.unibase.UniBaseUtils;
import org.tandemframework.shared.commonbase.utils.PublisherColumnBuilder;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.Entrant;

/**
 * @author agolubenko
 * @since 24.07.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.setSettings(component.getSettings());

        getDao().prepare(model);

        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null)
        {
            return;
        }

        DynamicListDataSource<Entrant> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new IndicatorColumn("Иконка", null).defaultIndicator(new IndicatorColumn.Item("entrant", "Абитуриент")).setOrderable(false).setClickable(false).setRequired(true));
        dataSource.addColumn(new SimpleColumn("Личный №", Entrant.P_PERSONAL_NUMBER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("№ заявления", Model.REQUEST_NUMBER).setClickable(false).setOrderable(false));
        PublisherLinkColumn column = new PublisherColumnBuilder("Абитуриент", Entrant.P_FULLFIO, null).subTab(null).build();
        dataSource.addColumn(column);
        dataSource.addColumn(new SimpleColumn("Пол", PersonRole.person().identityCard().sex().shortTitle().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Паспорт", new String[] { PersonRole.L_PERSON, Person.P_FULL_IDCARD_NUMBER }).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Гражданство", PersonRole.person().identityCard().citizenship().title().s()).setClickable(false).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Дата рождения", PersonRole.person().identityCard().birthDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Состояние", Entrant.state().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дата добавления", Entrant.P_REGISTRATION_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new BooleanColumn("Статус", Entrant.P_ARCHIVAL).setTrueLabel("Абитуриент не в архиве").setFalseLabel("Абитуриент в архиве"));

        dataSource.setOrder(column, OrderDirection.asc);

        model.setDataSource(dataSource);
    }

    public void onClickSearch(IBusinessComponent component)
    {
        if(null != getModel(component).getCitizenship())
        {
            component.getSettings().set("citizenship", getModel(component).getCitizenship().getId());
        }
        else
        {
            component.getSettings().set("citizenship", null);
        }
        component.saveSettings();
        getModel(component).getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.setCitizenship(null);
        model.getSettings().clear();
        getDao().prepare(model);
        onClickSearch(component);
    }

    public void onClickPrint(IBusinessComponent component)
    {
        component.saveSettings();
        Model model = getModel(component);
        CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(DataAccessServices.dao().getByCode(UniecScriptItem.class, UniecDefines.SCRIPT_ENTRANT_LIST),
                IScriptExecutor.OBJECT_VARIABLE, getDao().getEntrantIds(model),
                "headerTable", getDao().getHeaderTable(model)
        );
    }
}
