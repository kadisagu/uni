/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EnrollmentResultHorizontal.EnrollmentResultHorizontalAdd;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;

import java.util.*;
import java.util.Map.Entry;

/**
 * @author ekachanova
 */
class EnrollmentResultHorizontReport2Builder
{
    private static Map<String, String> _developFormTextByCode = new HashMap<>();

    static
    {
        _developFormTextByCode.put(DevelopFormCodes.FULL_TIME_FORM, "по очной форме обучения");
        _developFormTextByCode.put(DevelopFormCodes.CORESP_FORM, "по заочной форме обучения");
        _developFormTextByCode.put(DevelopFormCodes.PART_TIME_FORM, "по очной-заочной форме обучения");
        _developFormTextByCode.put(DevelopFormCodes.EXTERNAL_FORM, "по экстернату");
        _developFormTextByCode.put(DevelopFormCodes.APPLICANT_FORM, "по самостоятельному обучению и итоговой аттестации");
    }

    private static String BENEFIT_PARTY_OF_ARMED_CONFLICT_TITLE = "Военнослужащий, участник вооруженных конфликтов";
    private static String BENEFIT_HANDICAPPED_1GR_TITLE = "Инвалид 1 группы";
    private static String BENEFIT_HANDICAPPED_2GR_TITLE = "Инвалид 2 группы";
    private static String BENEFIT_ORPHAN_TITLE = "Сирота";
    private static String BENEFIT_CHERNOBYL_TITLE = "Чернобыль и приравненные";
    private static String BENEFIT_CHILDREN_HANDICAPPED_TITLE = "Дети-инвалиды";

    private static Set<String> _benefitTitlesHandicapped = new HashSet<>(Arrays.asList(
        BENEFIT_HANDICAPPED_1GR_TITLE,
        BENEFIT_HANDICAPPED_2GR_TITLE,
        BENEFIT_CHILDREN_HANDICAPPED_TITLE));
    private static Set<String> _benefitTitles = new HashSet<>(Arrays.asList(
        BENEFIT_ORPHAN_TITLE,
        BENEFIT_HANDICAPPED_1GR_TITLE,
        BENEFIT_HANDICAPPED_2GR_TITLE,
        BENEFIT_CHILDREN_HANDICAPPED_TITLE,
        BENEFIT_CHERNOBYL_TITLE,
        BENEFIT_PARTY_OF_ARMED_CONFLICT_TITLE));

    private Model _model;
    private Session _session;

    EnrollmentResultHorizontReport2Builder(Model model, Session session)
    {
        _model = model;
        _session = session;
    }

    DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    @SuppressWarnings("unchecked")
    private byte[] buildReport()
    {
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ENROLLMENT_RESULT_HORIZONTAL2);

        // получаем список направлений приема
        List<EnrollmentDirection> enrollmentDirectionList;
        if (_model.isCompetitionGroupActive())
        {
            MQBuilder builder = new MQBuilder(EnrollmentDirection.ENTITY_CLASS, "d");
            builder.add(MQExpression.eq("d", EnrollmentDirection.L_COMPETITION_GROUP, _model.getCompetitionGroup()));
            enrollmentDirectionList = builder.getResultList(_session);
        } else
        {
            enrollmentDirectionList = MultiEnrollmentDirectionUtil.getEnrollmentDirections(_model, _session);
        }

        TopOrgUnit academy = TopOrgUnit.getInstance();
        String academyTitle = academy.getShortTitle();

        AddressDetailed academyAddress = academy.getAddress();
        AddressItem academyCity = academyAddress == null ? null : academyAddress.getSettlement();
        AddressItem academyRegion = academyCity;
        if (academyRegion != null)
        {
            while (academyRegion.getParent() != null)
            {
                academyRegion = academyRegion.getParent();
            }
        }

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("academyTitle", academyTitle);
        injectModifier.put("year", _model.getReport().getEnrollmentCampaign().getEducationYear().getTitle().split("/")[0]);
        injectModifier.put("city", academyCity != null ? academyCity.getTitleWithType() : "");
        injectModifier.put("region", academyRegion != null ? academyRegion.getTitleWithType() : "");

        Map<Long, int[][]> dataByDirection = getData(enrollmentDirectionList, academyCity, academyRegion);

        RtfDocument document = null;

        if (_model.isDivideByEducationLevelHighSchool())
        {
            for (EnrollmentDirection direction : enrollmentDirectionList)
            {
                RtfDocument page = printDirections(templateDocument, injectModifier, dataByDirection, Collections.singletonList(direction));
                if (null == document)
                    document = page;
                else
                {
                    RtfUtil.modifySourceList(document.getHeader(), page.getHeader(), page.getElementList());
                    document.getElementList().addAll(page.getElementList());
                }
            }
        } else
            document = printDirections(templateDocument, injectModifier, dataByDirection, enrollmentDirectionList);

        if (null == document)
            throw new ApplicationException("Нет заявлений, подходящих под указанные параметры.");

        return RtfUtil.toByteArray(document);
    }

    private RtfDocument printDirections(IScriptItem templateDocument, RtfInjectModifier injectModifier, Map<Long, int[][]> dataByDirection, List<EnrollmentDirection> enrollmentDirectionList)
    {
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());

        Set<DevelopForm> developForms = CommonBaseEntityUtil.getPropertiesSet(enrollmentDirectionList, EnrollmentDirection.educationOrgUnit().developForm());
        if (developForms.size() == 1)
            injectModifier.put("developForm", _developFormTextByCode.get(developForms.iterator().next().getCode()).toUpperCase());
        else
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Collections.singletonList("developForm"), false, false);

        injectModifier.put("compensType", _model.getReport().getCompensationType().getCode().equals(UniDefines.COMPENSATION_TYPE_BUDGET) ? "НА МЕСТА, ФИНАНСИРУЕМЫЕ ИЗ ФЕДЕРАЛЬНОГО БЮДЖЕТА" : "НА МЕСТА, НЕ ФИНАНСИРУЕМЫЕ ИЗ ФЕДЕРАЛЬНОГО БЮДЖЕТА");

        if (_model.isDivideByEducationLevelHighSchool())
        {
            EducationOrgUnit eduOu = enrollmentDirectionList.get(0).getEducationOrgUnit();
            StringBuilder title = new StringBuilder();
            title.append(eduOu.getFormativeOrgUnit().getNominativeCaseTitle() == null ? eduOu.getFormativeOrgUnit().getTitle() : eduOu.getFormativeOrgUnit().getNominativeCaseTitle());
            if (null != eduOu.getTerritorialOrgUnit())
                title.append(" (").append(eduOu.getTerritorialOrgUnit().getNominativeCaseTitle() == null ? eduOu.getTerritorialOrgUnit().getTerritorialTitle() : eduOu.getTerritorialOrgUnit().getNominativeCaseTitle()).append(")");
            title.append("\\par ");
            title.append(eduOu.getEducationLevelHighSchool().getEducationLevel().getTitleCodePrefix()).append(" ").append(eduOu.getEducationLevelHighSchool().getTitle());
            title.append(" (").append(UniStringUtils.join(Arrays.<IEntity>asList(eduOu.getDevelopForm(), eduOu.getDevelopCondition(), eduOu.getDevelopTech(), eduOu.getDevelopPeriod()), "title", ", ").toLowerCase()).append(")");
            IRtfText text = RtfBean.getElementFactory().createRtfText(title.toString());
            text.setRaw(true);
            injectModifier.put("enrollmentDirection", Collections.singletonList(text));
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Collections.singletonList("competitionGroup"), false, false);
        } else
        {
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Collections.singletonList("enrollmentDirection"), false, false);
            if (_model.isCompetitionGroupActive())
                injectModifier.put("competitionGroup", _model.getCompetitionGroupName() + " №" + _model.getCompetitionGroup().getTitle());
            else
                SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Collections.singletonList("competitionGroup"), false, false);
        }

        injectModifier.modify(document);

        int[][] data = getSummaryData(dataByDirection, enrollmentDirectionList);

        String[][] dataStr = new String[4][21];
        for (int i = 0; i < 4; i++)
        {
            dataStr[i] = new String[21];
            for (int j = 0; j < 20; j++)
            {
                dataStr[i][j + 1] = String.valueOf(data[i][j]);
            }
        }
        dataStr[0][0] = "Подано заявлений";
        dataStr[1][0] = "Держали испытание";
        dataStr[2][0] = "Выдержали испытание";
        dataStr[3][0] = "Всего зачислено";

        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T", dataStr);
        tableModifier.modify(document);
        return document;
    }

    @SuppressWarnings("unchecked")
    private Map<Long, int[][]> getData(List<EnrollmentDirection> enrollmentDirectionList, AddressItem academyCity, AddressItem academyRegion)
    {
        Set<Long> processedEntrants = new HashSet<>();

        Map<Long, Set<Long>[][]> enrollmentDirectionId2Data = new HashMap<>();
        for (EnrollmentDirection enrollmentDirection : enrollmentDirectionList)
        {
            Set<Long>[][] data = new Set[4][20];
            for (int i = 0; i < 4; i++)
            {
                data[i] = new Set[20];
                for (int j = 0; j < 20; j++)
                {
                    data[i][j] = new HashSet<>();
                }
            }
            enrollmentDirectionId2Data.put(enrollmentDirection.getId(), data);
        }

        {
            MQBuilder baseDirectionBuilder = getBaseDirectionBuilder(enrollmentDirectionList);
            Map<Long, Set<String>> personBenefitMap = getPersonBenefitMap(_session, baseDirectionBuilder);
            List<Long> notEmptyStateExamDirectionIds = getNotEmptyStateExamDirectionIds(baseDirectionBuilder);
            List<Long> notEmptyAccessCourseEntrantIds = getNotEmptyAccessCourseEntrantIds(baseDirectionBuilder);

            // Подано заявлений
            baseDirectionBuilder.addSelect("r", new String[]{
                "id",
                RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + ".id",
                RequestedEnrollmentDirection.L_ENTRANT_REQUEST + ".id",
                RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + ".id",
                RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_PERSON,
                RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.P_REG_DATE,
                RequestedEnrollmentDirection.P_GRADUATED_PROFILE_EDU_INSTITUTION,
                RequestedEnrollmentDirection.P_STUDIED_O_P_P,
                RequestedEnrollmentDirection.L_COMPETITION_KIND + "." + CompetitionKind.P_CODE});
            for (Object[] obj : baseDirectionBuilder.<Object[]>getResultList(_session))
            {
                Long entrantId = (Long) obj[3];
                if (!_model.isDivideByEducationLevelHighSchool() && processedEntrants.contains(entrantId))
                    continue;
                processedEntrants.add(entrantId);
                processRow(enrollmentDirectionId2Data.get((Long) obj[1]), 0, (Long) obj[2], (Long) obj[0], entrantId, (Person) obj[4], (Date) obj[5], (Boolean) obj[6], (Boolean) obj[7], (String) obj[8],
                    academyCity, academyRegion, personBenefitMap, notEmptyStateExamDirectionIds, notEmptyAccessCourseEntrantIds);
            }
            processedEntrants.clear();

            // Держали испытание
            final MQBuilder examDirectionBuilder = getBaseDirectionBuilder(enrollmentDirectionList);

            MQBuilder interviewResultBuilder = new MQBuilder(InterviewResult.ENTITY_CLASS, "interviewResult", new String[]{"id"});
            interviewResultBuilder.add(MQExpression.eqProperty("interviewResult", InterviewResult.L_REQUESTED_ENROLLMENT_DIRECTION + ".id", "r", "id"));

            MQBuilder chosenBuilder = new MQBuilder(ChosenEntranceDiscipline.ENTITY_CLASS, "chosen", new String[]{"id"});
            chosenBuilder.add(MQExpression.eqProperty("chosen", ChosenEntranceDiscipline.L_CHOSEN_ENROLLMENT_DIRECTION + ".id", "r", "id"));
            chosenBuilder.add(MQExpression.isNotNull("chosen", ChosenEntranceDiscipline.P_FINAL_MARK));

            examDirectionBuilder.add(MQExpression.or(
                MQExpression.eq("r", RequestedEnrollmentDirection.L_COMPETITION_KIND + "." + CompetitionKind.P_CODE, UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES),
                MQExpression.exists(interviewResultBuilder),
                MQExpression.exists(chosenBuilder)));

            examDirectionBuilder.addSelect("r", new String[]{
                "id",
                RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + ".id",
                RequestedEnrollmentDirection.L_ENTRANT_REQUEST + ".id",
                RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + ".id",
                RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_PERSON,
                RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.P_REG_DATE,
                RequestedEnrollmentDirection.P_GRADUATED_PROFILE_EDU_INSTITUTION,
                RequestedEnrollmentDirection.P_STUDIED_O_P_P,
                RequestedEnrollmentDirection.L_COMPETITION_KIND + "." + CompetitionKind.P_CODE});
            for (Object[] obj : examDirectionBuilder.<Object[]>getResultList(_session))
            {
                Long entrantId = (Long) obj[3];
                if (!_model.isDivideByEducationLevelHighSchool() && processedEntrants.contains(entrantId))
                    continue;
                processedEntrants.add(entrantId);
                processRow(enrollmentDirectionId2Data.get((Long) obj[1]), 1, (Long) obj[2], (Long) obj[0], entrantId, (Person) obj[4], (Date) obj[5], (Boolean) obj[6], (Boolean) obj[7], (String) obj[8],
                    academyCity, academyRegion, personBenefitMap, notEmptyStateExamDirectionIds, notEmptyAccessCourseEntrantIds);
            }
            processedEntrants.clear();

            // Выдержали испытание
            final MQBuilder examPassDirectionBuilder = getBaseDirectionBuilder(enrollmentDirectionList);
            examPassDirectionBuilder.add(MQExpression.notIn("r", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY, UniecDefines.ENTRANT_STATE_ACTIVE_CODE));
            examPassDirectionBuilder.addSelect("r", new String[]{
                "id",
                RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + ".id",
                RequestedEnrollmentDirection.L_ENTRANT_REQUEST + ".id",
                RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + ".id",
                RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_PERSON,
                RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.P_REG_DATE,
                RequestedEnrollmentDirection.P_GRADUATED_PROFILE_EDU_INSTITUTION,
                RequestedEnrollmentDirection.P_STUDIED_O_P_P,
                RequestedEnrollmentDirection.L_COMPETITION_KIND + "." + CompetitionKind.P_CODE});
            for (Object[] obj : examPassDirectionBuilder.<Object[]>getResultList(_session))
            {
                Long entrantId = (Long) obj[3];
                if (!_model.isDivideByEducationLevelHighSchool() && processedEntrants.contains(entrantId))
                    continue;
                processedEntrants.add(entrantId);
                processRow(enrollmentDirectionId2Data.get((Long) obj[1]), 2, (Long) obj[2], (Long) obj[0], entrantId, (Person) obj[4], (Date) obj[5], (Boolean) obj[6], (Boolean) obj[7], (String) obj[8],
                    academyCity, academyRegion, personBenefitMap, notEmptyStateExamDirectionIds, notEmptyAccessCourseEntrantIds);
            }
        }

        {
            // Всего зачислено
            final MQBuilder preliminaryDirectionBuilder = getBasePreliminaryBuilder(enrollmentDirectionList);
            preliminaryDirectionBuilder.addSelect("p", new String[]{PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION});

            Map<Long, Set<String>> personBenefitMap = getPersonBenefitMap(_session, preliminaryDirectionBuilder);
            List<Long> notEmptyStateExamDirectionIds = getNotEmptyStateExamDirectionIds(preliminaryDirectionBuilder);
            List<Long> notEmptyAccessCourseEntrantIds = getNotEmptyAccessCourseEntrantIds(preliminaryDirectionBuilder);

            final MQBuilder preliminaryBuilder = getBasePreliminaryBuilder(enrollmentDirectionList);
            preliminaryBuilder.addSelect("p", new String[]{"id", PreliminaryEnrollmentStudent.P_STUDIED_O_P_P});
            preliminaryBuilder.addSelect("r", new String[]{
                "id",
                RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + ".id",
                RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + ".id",
                RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_PERSON,
                RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.P_REG_DATE,
                RequestedEnrollmentDirection.P_GRADUATED_PROFILE_EDU_INSTITUTION,
                RequestedEnrollmentDirection.L_COMPETITION_KIND + "." + CompetitionKind.P_CODE});
            for (Object[] obj : preliminaryBuilder.<Object[]>getResultList(_session))
            {
                processRow(enrollmentDirectionId2Data.get((Long) obj[3]), 3, (Long) obj[0], (Long) obj[2], (Long) obj[4], (Person) obj[5], (Date) obj[6], (Boolean) obj[7], (Boolean) obj[1], (String) obj[8],
                    academyCity, academyRegion, personBenefitMap, notEmptyStateExamDirectionIds, notEmptyAccessCourseEntrantIds);
            }
        }

        Map<Long, int[][]> result = new HashMap<>();
        for (Entry<Long, Set<Long>[][]> entry : enrollmentDirectionId2Data.entrySet())
        {
            int[][] data = new int[4][20];
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 20; j++)
                {
                    data[i][j] = entry.getValue()[i][j].size();
                }
            }
            result.put(entry.getKey(), data);
        }
        return result;
    }

    private int[][] getSummaryData(Map<Long, int[][]> enrollmentDirectionId2Data, List<EnrollmentDirection> enrollmentDirections)
    {
        int[][] result = new int[4][20];
        for (EnrollmentDirection direction : enrollmentDirections)
        {
            int[][] data = enrollmentDirectionId2Data.get(direction.getId());
            if (null != data)
                for (int row = 0; row < data.length; row++)
                {
                    for (int column = 0; column < data[row].length; column++)
                    {
                        result[row][column] += data[row][column];
                    }
                }
        }
        return result;
    }

    @SuppressWarnings("deprecation")
    private void processRow(Set<Long>[][] data, int row, Long id, Long directionId, Long entrantId, Person person,
                            Date requestRegDate, boolean graduatedProfileEduInstitution, boolean studiedOPP, String competitionKindCode,
                            AddressItem academyCity, AddressItem academyRegion,
                            Map<Long, Set<String>> personBenefitMap,
                            List<Long> notEmptyStateExamDirectionIds, List<Long> notEmptyAccessCourseEntrantIds)
    {
        int requestYear = CoreDateUtils.getYear(requestRegDate);
        data[row][0].add(id);
        if (person.getIdentityCard().getSex().isMale())
            data[row][1].add(id);
        else
            data[row][2].add(id);
        PersonEduInstitution eduInstitution = person.getPersonEduInstitution();
        if (eduInstitution != null)
        {
            String educationLevelCode = eduInstitution.getEducationLevelStage().getCode();
            int eduInstitutionEndYear = eduInstitution.getYearEnd();

            if (educationLevelCode.equals(UniDefines.EDUCATION_LEVEL_STAGE_BACHELORS_DEGREE))
            {
                data[row][3].add(id);
                if (eduInstitutionEndYear == requestYear)
                    data[row][4].add(id);
            } else if (educationLevelCode.equals(UniDefines.EDUCATION_LEVEL_STAGE_CERTIFICATED_SPEICALISTS_TRAINING))
            {
                data[row][5].add(id);
                if (eduInstitutionEndYear == requestYear)
                    data[row][6].add(id);
            }
        }

        AddressBase entrantAddress = person.getIdentityCard().getAddress();
        AddressItem entrantCity = (entrantAddress != null && entrantAddress instanceof AddressDetailed )? ((AddressDetailed)entrantAddress).getSettlement() : null;
        if (entrantCity != null && entrantCity.getCountry().getCode() == IKladrDefines.RUSSIA_COUNTRY_CODE)
        {
            if (entrantCity.equals(academyCity))
                data[row][7].add(id);
            else
            {
                AddressItem entrantRegion = entrantCity;
                while (entrantRegion.getParent() != null) entrantRegion = entrantRegion.getParent();
                if (entrantRegion.equals(academyRegion))
                    data[row][8].add(id);
                else
                    data[row][9].add(id);
            }
        } else
        {
            data[row][10].add(id);
        }

        if (notEmptyStateExamDirectionIds.contains(directionId))
            data[row][11].add(id);
        if (graduatedProfileEduInstitution)
            data[row][12].add(id);

        if (competitionKindCode.equals(UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION))
        {
            Set<String> personBenefitTitles = personBenefitMap.get(person.getId());
            if (CollectionUtils.isNotEmpty(personBenefitTitles))
            {
                if (personBenefitTitles.contains(BENEFIT_ORPHAN_TITLE))
                    data[row][13].add(id);
                if (CollectionUtils.containsAny(personBenefitTitles, _benefitTitlesHandicapped))
                    data[row][14].add(id);
                if (personBenefitTitles.contains(BENEFIT_CHERNOBYL_TITLE))
                    data[row][15].add(id);
                if (personBenefitTitles.contains(BENEFIT_PARTY_OF_ARMED_CONFLICT_TITLE))
                    data[row][16].add(id);
                if (!CollectionUtils.containsAny(personBenefitTitles, _benefitTitles))
                    data[row][17].add(id);
            }
        }
        if (notEmptyAccessCourseEntrantIds.contains(entrantId))
            data[row][18].add(id);
        if (studiedOPP)
            data[row][19].add(id);
    }

    private MQBuilder getBaseDirectionBuilder(List<EnrollmentDirection> enrollmentDirectionList)
    {
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "r");
        builder.add(UniMQExpression.betweenDate("r", RequestedEnrollmentDirection.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, Boolean.FALSE));
        builder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        if (_model.isQualificationActive())
            builder.add(MQExpression.in("r", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL + "." + EducationLevelsHighSchool.L_EDUCATION_LEVEL + "." + EducationLevels.L_QUALIFICATION, _model.getQualificationList()));
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_COMPENSATION_TYPE, _model.getReport().getCompensationType()));
        if (_model.isStudentCategoryActive())
            builder.add(MQExpression.in("r", RequestedEnrollmentDirection.L_STUDENT_CATEGORY, _model.getStudentCategoryList()));
        builder.add(MQExpression.in("r", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, enrollmentDirectionList));
        return builder;
    }

    private MQBuilder getBasePreliminaryBuilder(List<EnrollmentDirection> enrollmentDirectionList)
    {
        MQBuilder builder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "p");
        if (_model.isQualificationActive())
            builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL + "." + EducationLevelsHighSchool.L_EDUCATION_LEVEL + "." + EducationLevels.L_QUALIFICATION, _model.getQualificationList()));
        builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.L_COMPENSATION_TYPE, _model.getReport().getCompensationType()));
        if (_model.isStudentCategoryActive())
            builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.L_STUDENT_CATEGORY, _model.getStudentCategoryList()));
        if (_model.isParallelActive() && _model.getParallel().isTrue())
            builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.P_PARALLEL, Boolean.FALSE));

        builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT, CommonBaseEntityUtil.getPropertiesSet(enrollmentDirectionList, EnrollmentDirection.L_EDUCATION_ORG_UNIT)));
        builder.addJoin("p", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, "r");
        builder.add(UniMQExpression.betweenDate("r", RequestedEnrollmentDirection.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_ENROLLMENT_CAMPAIGN, _model.getReport().getEnrollmentCampaign()));
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, Boolean.FALSE));
        builder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        return builder;
    }

    private List<Long> getNotEmptyStateExamDirectionIds(MQBuilder directionBuilder)
    {
        MQBuilder builder = new MQBuilder(ChosenEntranceDiscipline.ENTITY_CLASS, "chosen", new String[]{
            ChosenEntranceDiscipline.L_CHOSEN_ENROLLMENT_DIRECTION + ".id"});
        builder.add(MQExpression.in("chosen", ChosenEntranceDiscipline.L_CHOSEN_ENROLLMENT_DIRECTION, directionBuilder));
        builder.add(MQExpression.isNotNull("chosen", ChosenEntranceDiscipline.P_FINAL_MARK));
        builder.add(MQExpression.in("chosen", ChosenEntranceDiscipline.P_FINAL_MARK_SOURCE,
            UniecDefines.FINAL_MARK_STATE_EXAM_WAVE1,
            UniecDefines.FINAL_MARK_STATE_EXAM_WAVE2,
            UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL,
            UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL_APPEAL));
        builder.setNeedDistinct(true);
        return builder.getResultList(_session);
    }

    public List<Long> getNotEmptyAccessCourseEntrantIds(MQBuilder directionBuilder)
    {
        MQBuilder builder = new MQBuilder(EntrantAccessCourse.ENTITY_CLASS, "accessCourse", new String[]{EntrantAccessCourse.L_ENTRANT + ".id"});
        builder.addDomain("r_accessCourse", RequestedEnrollmentDirection.ENTITY_CLASS);
        builder.add(MQExpression.eqProperty("accessCourse", EntrantAccessCourse.L_ENTRANT, "r_accessCourse", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT));
        builder.add(MQExpression.in("r_accessCourse", "id", directionBuilder));
        builder.setNeedDistinct(true);
        return builder.getResultList(_session);
    }

    private Map<Long, Set<String>> getPersonBenefitMap(Session session, MQBuilder directionBuilder)
    {
        Map<Long, Set<String>> personId2benefitTitles = new HashMap<>();

        MQBuilder builder = new MQBuilder(PersonBenefit.ENTITY_CLASS, "benefit");
        builder.addDomain("r_benefit", RequestedEnrollmentDirection.ENTITY_CLASS);
        builder.add(MQExpression.eqProperty("benefit", PersonBenefit.L_PERSON, "r_benefit", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_PERSON));
        builder.add(MQExpression.in("r_benefit", "id", directionBuilder));
        for (PersonBenefit personBenefit : builder.<PersonBenefit>getResultList(session))
        {
            Set<String> set = personId2benefitTitles.get(personBenefit.getPerson().getId());
            if (set == null)
                personId2benefitTitles.put(personBenefit.getPerson().getId(), set = new HashSet<>());
            set.add(personBenefit.getBenefit().getTitle());
        }
        return personId2benefitTitles;
    }
}
