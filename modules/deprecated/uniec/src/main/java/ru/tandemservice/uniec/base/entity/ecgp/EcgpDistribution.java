package ru.tandemservice.uniec.base.entity.ecgp;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.uniec.base.entity.ecgp.gen.*;

/**
 * Распределение абитуриентов по профилям
 */
public class EcgpDistribution extends EcgpDistributionGen implements ITitled
{
    // ITitled

    @Override
    public String getTitle()
    {
        return getConfig().getEcgItem().getTitle() + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getFormingDate()) + " (" + getConfig().getCompensationType().getShortTitle() + ")";
    }
}