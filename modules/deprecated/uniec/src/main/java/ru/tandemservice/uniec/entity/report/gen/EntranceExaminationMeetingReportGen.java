package ru.tandemservice.uniec.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.report.StorableReport;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Протокол заседания приемной комиссии
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntranceExaminationMeetingReportGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport";
    public static final String ENTITY_NAME = "entranceExaminationMeetingReport";
    public static final int VERSION_HASH = -1297665018;
    private static IEntityMeta ENTITY_META;

    public static final String P_REQUESTS_FROM_DATE = "requestsFromDate";
    public static final String P_REQUESTS_TO_DATE = "requestsToDate";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String P_TERRITORIAL_ORG_UNIT = "territorialOrgUnit";
    public static final String L_EDUCATION_LEVELS_HIGH_SCHOOL = "educationLevelsHighSchool";
    public static final String L_DEVELOP_FORM = "developForm";
    public static final String P_DEVELOP_CONDITION = "developCondition";
    public static final String P_DEVELOP_TECH = "developTech";
    public static final String P_DEVELOP_PERIOD = "developPeriod";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String P_STUDENT_CATEGORY = "studentCategory";
    public static final String P_ENROLLMENT_CAMPAIGN_STAGE = "enrollmentCampaignStage";

    private Date _requestsFromDate;     // Начальная дата подачи заявлений
    private Date _requestsToDate;     // Конечная дата подачи заявлений
    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private OrgUnit _orgUnit;     // Подразделение
    private OrgUnit _formativeOrgUnit;     // Формирующее подр.
    private String _territorialOrgUnit;     // Территориальное подр.
    private EducationLevelsHighSchool _educationLevelsHighSchool;     // Параметры выпуска студентов по направлению подготовки (НПв)
    private DevelopForm _developForm;     // Форма освоения
    private String _developCondition;     // Условие освоения
    private String _developTech;     // Технология освоения
    private String _developPeriod;     // Срок освоения
    private CompensationType _compensationType;     // Вид возмещения затрат
    private String _studentCategory;     // Категория поступающего
    private String _enrollmentCampaignStage;     // Стадия приемной кампании

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Начальная дата подачи заявлений. Свойство не может быть null.
     */
    @NotNull
    public Date getRequestsFromDate()
    {
        return _requestsFromDate;
    }

    /**
     * @param requestsFromDate Начальная дата подачи заявлений. Свойство не может быть null.
     */
    public void setRequestsFromDate(Date requestsFromDate)
    {
        dirty(_requestsFromDate, requestsFromDate);
        _requestsFromDate = requestsFromDate;
    }

    /**
     * @return Конечная дата подачи заявлений. Свойство не может быть null.
     */
    @NotNull
    public Date getRequestsToDate()
    {
        return _requestsToDate;
    }

    /**
     * @param requestsToDate Конечная дата подачи заявлений. Свойство не может быть null.
     */
    public void setRequestsToDate(Date requestsToDate)
    {
        dirty(_requestsToDate, requestsToDate);
        _requestsToDate = requestsToDate;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Подразделение.
     */
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Формирующее подр.. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    /**
     * @param formativeOrgUnit Формирующее подр.. Свойство не может быть null.
     */
    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        dirty(_formativeOrgUnit, formativeOrgUnit);
        _formativeOrgUnit = formativeOrgUnit;
    }

    /**
     * @return Территориальное подр..
     */
    @Length(max=255)
    public String getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    /**
     * @param territorialOrgUnit Территориальное подр..
     */
    public void setTerritorialOrgUnit(String territorialOrgUnit)
    {
        dirty(_territorialOrgUnit, territorialOrgUnit);
        _territorialOrgUnit = territorialOrgUnit;
    }

    /**
     * @return Параметры выпуска студентов по направлению подготовки (НПв).
     */
    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _educationLevelsHighSchool;
    }

    /**
     * @param educationLevelsHighSchool Параметры выпуска студентов по направлению подготовки (НПв).
     */
    public void setEducationLevelsHighSchool(EducationLevelsHighSchool educationLevelsHighSchool)
    {
        dirty(_educationLevelsHighSchool, educationLevelsHighSchool);
        _educationLevelsHighSchool = educationLevelsHighSchool;
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения. Свойство не может быть null.
     */
    public void setDevelopForm(DevelopForm developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Условие освоения.
     */
    @Length(max=255)
    public String getDevelopCondition()
    {
        return _developCondition;
    }

    /**
     * @param developCondition Условие освоения.
     */
    public void setDevelopCondition(String developCondition)
    {
        dirty(_developCondition, developCondition);
        _developCondition = developCondition;
    }

    /**
     * @return Технология освоения.
     */
    @Length(max=255)
    public String getDevelopTech()
    {
        return _developTech;
    }

    /**
     * @param developTech Технология освоения.
     */
    public void setDevelopTech(String developTech)
    {
        dirty(_developTech, developTech);
        _developTech = developTech;
    }

    /**
     * @return Срок освоения.
     */
    @Length(max=255)
    public String getDevelopPeriod()
    {
        return _developPeriod;
    }

    /**
     * @param developPeriod Срок освоения.
     */
    public void setDevelopPeriod(String developPeriod)
    {
        dirty(_developPeriod, developPeriod);
        _developPeriod = developPeriod;
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Категория поступающего.
     */
    @Length(max=255)
    public String getStudentCategory()
    {
        return _studentCategory;
    }

    /**
     * @param studentCategory Категория поступающего.
     */
    public void setStudentCategory(String studentCategory)
    {
        dirty(_studentCategory, studentCategory);
        _studentCategory = studentCategory;
    }

    /**
     * @return Стадия приемной кампании. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getEnrollmentCampaignStage()
    {
        return _enrollmentCampaignStage;
    }

    /**
     * @param enrollmentCampaignStage Стадия приемной кампании. Свойство не может быть null.
     */
    public void setEnrollmentCampaignStage(String enrollmentCampaignStage)
    {
        dirty(_enrollmentCampaignStage, enrollmentCampaignStage);
        _enrollmentCampaignStage = enrollmentCampaignStage;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EntranceExaminationMeetingReportGen)
        {
            setRequestsFromDate(((EntranceExaminationMeetingReport)another).getRequestsFromDate());
            setRequestsToDate(((EntranceExaminationMeetingReport)another).getRequestsToDate());
            setEnrollmentCampaign(((EntranceExaminationMeetingReport)another).getEnrollmentCampaign());
            setOrgUnit(((EntranceExaminationMeetingReport)another).getOrgUnit());
            setFormativeOrgUnit(((EntranceExaminationMeetingReport)another).getFormativeOrgUnit());
            setTerritorialOrgUnit(((EntranceExaminationMeetingReport)another).getTerritorialOrgUnit());
            setEducationLevelsHighSchool(((EntranceExaminationMeetingReport)another).getEducationLevelsHighSchool());
            setDevelopForm(((EntranceExaminationMeetingReport)another).getDevelopForm());
            setDevelopCondition(((EntranceExaminationMeetingReport)another).getDevelopCondition());
            setDevelopTech(((EntranceExaminationMeetingReport)another).getDevelopTech());
            setDevelopPeriod(((EntranceExaminationMeetingReport)another).getDevelopPeriod());
            setCompensationType(((EntranceExaminationMeetingReport)another).getCompensationType());
            setStudentCategory(((EntranceExaminationMeetingReport)another).getStudentCategory());
            setEnrollmentCampaignStage(((EntranceExaminationMeetingReport)another).getEnrollmentCampaignStage());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntranceExaminationMeetingReportGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntranceExaminationMeetingReport.class;
        }

        public T newInstance()
        {
            return (T) new EntranceExaminationMeetingReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "requestsFromDate":
                    return obj.getRequestsFromDate();
                case "requestsToDate":
                    return obj.getRequestsToDate();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "formativeOrgUnit":
                    return obj.getFormativeOrgUnit();
                case "territorialOrgUnit":
                    return obj.getTerritorialOrgUnit();
                case "educationLevelsHighSchool":
                    return obj.getEducationLevelsHighSchool();
                case "developForm":
                    return obj.getDevelopForm();
                case "developCondition":
                    return obj.getDevelopCondition();
                case "developTech":
                    return obj.getDevelopTech();
                case "developPeriod":
                    return obj.getDevelopPeriod();
                case "compensationType":
                    return obj.getCompensationType();
                case "studentCategory":
                    return obj.getStudentCategory();
                case "enrollmentCampaignStage":
                    return obj.getEnrollmentCampaignStage();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "requestsFromDate":
                    obj.setRequestsFromDate((Date) value);
                    return;
                case "requestsToDate":
                    obj.setRequestsToDate((Date) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "formativeOrgUnit":
                    obj.setFormativeOrgUnit((OrgUnit) value);
                    return;
                case "territorialOrgUnit":
                    obj.setTerritorialOrgUnit((String) value);
                    return;
                case "educationLevelsHighSchool":
                    obj.setEducationLevelsHighSchool((EducationLevelsHighSchool) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((DevelopForm) value);
                    return;
                case "developCondition":
                    obj.setDevelopCondition((String) value);
                    return;
                case "developTech":
                    obj.setDevelopTech((String) value);
                    return;
                case "developPeriod":
                    obj.setDevelopPeriod((String) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "studentCategory":
                    obj.setStudentCategory((String) value);
                    return;
                case "enrollmentCampaignStage":
                    obj.setEnrollmentCampaignStage((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "requestsFromDate":
                        return true;
                case "requestsToDate":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "orgUnit":
                        return true;
                case "formativeOrgUnit":
                        return true;
                case "territorialOrgUnit":
                        return true;
                case "educationLevelsHighSchool":
                        return true;
                case "developForm":
                        return true;
                case "developCondition":
                        return true;
                case "developTech":
                        return true;
                case "developPeriod":
                        return true;
                case "compensationType":
                        return true;
                case "studentCategory":
                        return true;
                case "enrollmentCampaignStage":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "requestsFromDate":
                    return true;
                case "requestsToDate":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "orgUnit":
                    return true;
                case "formativeOrgUnit":
                    return true;
                case "territorialOrgUnit":
                    return true;
                case "educationLevelsHighSchool":
                    return true;
                case "developForm":
                    return true;
                case "developCondition":
                    return true;
                case "developTech":
                    return true;
                case "developPeriod":
                    return true;
                case "compensationType":
                    return true;
                case "studentCategory":
                    return true;
                case "enrollmentCampaignStage":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "requestsFromDate":
                    return Date.class;
                case "requestsToDate":
                    return Date.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "formativeOrgUnit":
                    return OrgUnit.class;
                case "territorialOrgUnit":
                    return String.class;
                case "educationLevelsHighSchool":
                    return EducationLevelsHighSchool.class;
                case "developForm":
                    return DevelopForm.class;
                case "developCondition":
                    return String.class;
                case "developTech":
                    return String.class;
                case "developPeriod":
                    return String.class;
                case "compensationType":
                    return CompensationType.class;
                case "studentCategory":
                    return String.class;
                case "enrollmentCampaignStage":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntranceExaminationMeetingReport> _dslPath = new Path<EntranceExaminationMeetingReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntranceExaminationMeetingReport");
    }
            

    /**
     * @return Начальная дата подачи заявлений. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport#getRequestsFromDate()
     */
    public static PropertyPath<Date> requestsFromDate()
    {
        return _dslPath.requestsFromDate();
    }

    /**
     * @return Конечная дата подачи заявлений. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport#getRequestsToDate()
     */
    public static PropertyPath<Date> requestsToDate()
    {
        return _dslPath.requestsToDate();
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Формирующее подр.. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport#getFormativeOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    /**
     * @return Территориальное подр..
     * @see ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport#getTerritorialOrgUnit()
     */
    public static PropertyPath<String> territorialOrgUnit()
    {
        return _dslPath.territorialOrgUnit();
    }

    /**
     * @return Параметры выпуска студентов по направлению подготовки (НПв).
     * @see ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport#getEducationLevelsHighSchool()
     */
    public static EducationLevelsHighSchool.Path<EducationLevelsHighSchool> educationLevelsHighSchool()
    {
        return _dslPath.educationLevelsHighSchool();
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport#getDevelopForm()
     */
    public static DevelopForm.Path<DevelopForm> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport#getDevelopCondition()
     */
    public static PropertyPath<String> developCondition()
    {
        return _dslPath.developCondition();
    }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport#getDevelopTech()
     */
    public static PropertyPath<String> developTech()
    {
        return _dslPath.developTech();
    }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport#getDevelopPeriod()
     */
    public static PropertyPath<String> developPeriod()
    {
        return _dslPath.developPeriod();
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport#getStudentCategory()
     */
    public static PropertyPath<String> studentCategory()
    {
        return _dslPath.studentCategory();
    }

    /**
     * @return Стадия приемной кампании. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport#getEnrollmentCampaignStage()
     */
    public static PropertyPath<String> enrollmentCampaignStage()
    {
        return _dslPath.enrollmentCampaignStage();
    }

    public static class Path<E extends EntranceExaminationMeetingReport> extends StorableReport.Path<E>
    {
        private PropertyPath<Date> _requestsFromDate;
        private PropertyPath<Date> _requestsToDate;
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private OrgUnit.Path<OrgUnit> _formativeOrgUnit;
        private PropertyPath<String> _territorialOrgUnit;
        private EducationLevelsHighSchool.Path<EducationLevelsHighSchool> _educationLevelsHighSchool;
        private DevelopForm.Path<DevelopForm> _developForm;
        private PropertyPath<String> _developCondition;
        private PropertyPath<String> _developTech;
        private PropertyPath<String> _developPeriod;
        private CompensationType.Path<CompensationType> _compensationType;
        private PropertyPath<String> _studentCategory;
        private PropertyPath<String> _enrollmentCampaignStage;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Начальная дата подачи заявлений. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport#getRequestsFromDate()
     */
        public PropertyPath<Date> requestsFromDate()
        {
            if(_requestsFromDate == null )
                _requestsFromDate = new PropertyPath<Date>(EntranceExaminationMeetingReportGen.P_REQUESTS_FROM_DATE, this);
            return _requestsFromDate;
        }

    /**
     * @return Конечная дата подачи заявлений. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport#getRequestsToDate()
     */
        public PropertyPath<Date> requestsToDate()
        {
            if(_requestsToDate == null )
                _requestsToDate = new PropertyPath<Date>(EntranceExaminationMeetingReportGen.P_REQUESTS_TO_DATE, this);
            return _requestsToDate;
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Формирующее подр.. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport#getFormativeOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new OrgUnit.Path<OrgUnit>(L_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

    /**
     * @return Территориальное подр..
     * @see ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport#getTerritorialOrgUnit()
     */
        public PropertyPath<String> territorialOrgUnit()
        {
            if(_territorialOrgUnit == null )
                _territorialOrgUnit = new PropertyPath<String>(EntranceExaminationMeetingReportGen.P_TERRITORIAL_ORG_UNIT, this);
            return _territorialOrgUnit;
        }

    /**
     * @return Параметры выпуска студентов по направлению подготовки (НПв).
     * @see ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport#getEducationLevelsHighSchool()
     */
        public EducationLevelsHighSchool.Path<EducationLevelsHighSchool> educationLevelsHighSchool()
        {
            if(_educationLevelsHighSchool == null )
                _educationLevelsHighSchool = new EducationLevelsHighSchool.Path<EducationLevelsHighSchool>(L_EDUCATION_LEVELS_HIGH_SCHOOL, this);
            return _educationLevelsHighSchool;
        }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport#getDevelopForm()
     */
        public DevelopForm.Path<DevelopForm> developForm()
        {
            if(_developForm == null )
                _developForm = new DevelopForm.Path<DevelopForm>(L_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport#getDevelopCondition()
     */
        public PropertyPath<String> developCondition()
        {
            if(_developCondition == null )
                _developCondition = new PropertyPath<String>(EntranceExaminationMeetingReportGen.P_DEVELOP_CONDITION, this);
            return _developCondition;
        }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport#getDevelopTech()
     */
        public PropertyPath<String> developTech()
        {
            if(_developTech == null )
                _developTech = new PropertyPath<String>(EntranceExaminationMeetingReportGen.P_DEVELOP_TECH, this);
            return _developTech;
        }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport#getDevelopPeriod()
     */
        public PropertyPath<String> developPeriod()
        {
            if(_developPeriod == null )
                _developPeriod = new PropertyPath<String>(EntranceExaminationMeetingReportGen.P_DEVELOP_PERIOD, this);
            return _developPeriod;
        }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport#getStudentCategory()
     */
        public PropertyPath<String> studentCategory()
        {
            if(_studentCategory == null )
                _studentCategory = new PropertyPath<String>(EntranceExaminationMeetingReportGen.P_STUDENT_CATEGORY, this);
            return _studentCategory;
        }

    /**
     * @return Стадия приемной кампании. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport#getEnrollmentCampaignStage()
     */
        public PropertyPath<String> enrollmentCampaignStage()
        {
            if(_enrollmentCampaignStage == null )
                _enrollmentCampaignStage = new PropertyPath<String>(EntranceExaminationMeetingReportGen.P_ENROLLMENT_CAMPAIGN_STAGE, this);
            return _enrollmentCampaignStage;
        }

        public Class getEntityClass()
        {
            return EntranceExaminationMeetingReport.class;
        }

        public String getEntityName()
        {
            return "entranceExaminationMeetingReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
