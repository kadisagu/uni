/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.OlympiadDiplomaAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.fias.base.bo.util.IKladrModel;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignModel;
import ru.tandemservice.uniec.entity.catalog.OlympiadDiplomaDegree;
import ru.tandemservice.uniec.entity.catalog.OlympiadDiplomaType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.OlympiadDiploma;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

import java.util.List;

/**
 * @author oleyba
 * @since 04.03.2009
 */
@Input({
        @Bind(key = Model.DIPLOMA_ID_BINDING_KEY, binding = "diploma.id"),
        @Bind(key = Model.ENTRANT_ID_BINDING_KEY, binding = "entrantId")
})
public class Model implements IKladrModel, IEnrollmentCampaignModel
{
    public static final String DIPLOMA_ID_BINDING_KEY = "diplomaId";
    public static final String ENTRANT_ID_BINDING_KEY = "entrantId";

    private OlympiadDiploma _diploma = new OlympiadDiploma();
    private List<OlympiadDiplomaType> _diplomaTypeList;
    private Long _entrantId;
    private boolean _addForm;

    private List<OlympiadDiplomaDegree> _degreesList;
    private ISelectModel _countriesModel;
    private AddressCountry _country;
    private ISelectModel _settlementsModel;

    private ISelectModel _formativeOrgUnitModel;
    private ISelectModel _territorialOrgUnitModel;
    private ISelectModel _educationLevelsHighSchoolModel;
    private ISelectModel _developFormModel;
    private ISelectModel _developTechModel;
    private ISelectModel _developConditionModel;
    private ISelectModel _developPeriodModel;

    private OrgUnit _formativeOrgUnit;
    private OrgUnit _territorialOrgUnit;
    private DevelopForm _developForm;
    private DevelopTech _developTech;
    private DevelopCondition _developCondition;
    private DevelopPeriod _developPeriod;
    private EducationLevelsHighSchool _educationLevelsHighSchool;

    private ISelectModel _disciplinesModel;

    private List<CompensationType> _compensationTypeList;
    private CompensationType _compensationType;
    private Discipline2RealizationWayRelation _selectedDiscipline;
    private boolean _hasOlympiadDiplomaRelation;

    // IKladrModel

    @Override
    public AddressCountry getAddressCountry()
    {
        return getCountry();
    }

    @Override
    public AddressItem getAddressItem()
    {
        return getDiploma().getSettlement();
    }

    // IEnrollmentCampaignModel

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return getDiploma().getEntrant().getEnrollmentCampaign();
    }

    // Getters & Setters

    public OlympiadDiploma getDiploma()
    {
        return _diploma;
    }

    public void setDiploma(OlympiadDiploma diploma)
    {
        _diploma = diploma;
    }

    public List<OlympiadDiplomaType> getDiplomaTypeList()
    {
        return _diplomaTypeList;
    }

    public void setDiplomaTypeList(List<OlympiadDiplomaType> diplomaTypeList)
    {
        _diplomaTypeList = diplomaTypeList;
    }

    public Long getEntrantId()
    {
        return _entrantId;
    }

    public void setEntrantId(Long entrantId)
    {
        _entrantId = entrantId;
    }

    public boolean isAddForm()
    {
        return _addForm;
    }

    public void setAddForm(boolean addForm)
    {
        _addForm = addForm;
    }

    public List<OlympiadDiplomaDegree> getDegreesList()
    {
        return _degreesList;
    }

    public void setDegreesList(List<OlympiadDiplomaDegree> degreesList)
    {
        _degreesList = degreesList;
    }

    public ISelectModel getCountriesModel()
    {
        return _countriesModel;
    }

    public void setCountriesModel(ISelectModel countriesModel)
    {
        _countriesModel = countriesModel;
    }

    public AddressCountry getCountry()
    {
        return _country;
    }

    public void setCountry(AddressCountry country)
    {
        _country = country;
    }

    public ISelectModel getSettlementsModel()
    {
        return _settlementsModel;
    }

    public void setSettlementsModel(ISelectModel settlementsModel)
    {
        _settlementsModel = settlementsModel;
    }

    public ISelectModel getFormativeOrgUnitModel()
    {
        return _formativeOrgUnitModel;
    }

    public void setFormativeOrgUnitModel(ISelectModel formativeOrgUnitModel)
    {
        _formativeOrgUnitModel = formativeOrgUnitModel;
    }

    public ISelectModel getTerritorialOrgUnitModel()
    {
        return _territorialOrgUnitModel;
    }

    public void setTerritorialOrgUnitModel(ISelectModel territorialOrgUnitModel)
    {
        _territorialOrgUnitModel = territorialOrgUnitModel;
    }

    public ISelectModel getEducationLevelsHighSchoolModel()
    {
        return _educationLevelsHighSchoolModel;
    }

    public void setEducationLevelsHighSchoolModel(ISelectModel educationLevelsHighSchoolModel)
    {
        _educationLevelsHighSchoolModel = educationLevelsHighSchoolModel;
    }

    public ISelectModel getDevelopFormModel()
    {
        return _developFormModel;
    }

    public void setDevelopFormModel(ISelectModel developFormModel)
    {
        _developFormModel = developFormModel;
    }

    public ISelectModel getDevelopTechModel()
    {
        return _developTechModel;
    }

    public void setDevelopTechModel(ISelectModel developTechModel)
    {
        _developTechModel = developTechModel;
    }

    public ISelectModel getDevelopConditionModel()
    {
        return _developConditionModel;
    }

    public void setDevelopConditionModel(ISelectModel developConditionModel)
    {
        _developConditionModel = developConditionModel;
    }

    public ISelectModel getDevelopPeriodModel()
    {
        return _developPeriodModel;
    }

    public void setDevelopPeriodModel(ISelectModel developPeriodModel)
    {
        _developPeriodModel = developPeriodModel;
    }

    @Override
    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        _formativeOrgUnit = formativeOrgUnit;
    }

    @Override
    public OrgUnit getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    public void setTerritorialOrgUnit(OrgUnit territorialOrgUnit)
    {
        _territorialOrgUnit = territorialOrgUnit;
    }

    @Override
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public void setDevelopForm(DevelopForm developForm)
    {
        _developForm = developForm;
    }

    @Override
    public DevelopTech getDevelopTech()
    {
        return _developTech;
    }

    public void setDevelopTech(DevelopTech developTech)
    {
        _developTech = developTech;
    }

    @Override
    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    public void setDevelopCondition(DevelopCondition developCondition)
    {
        _developCondition = developCondition;
    }

    @Override
    public DevelopPeriod getDevelopPeriod()
    {
        return _developPeriod;
    }

    public void setDevelopPeriod(DevelopPeriod developPeriod)
    {
        _developPeriod = developPeriod;
    }

    @Override
    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _educationLevelsHighSchool;
    }

    public void setEducationLevelsHighSchool(EducationLevelsHighSchool educationLevelsHighSchool)
    {
        _educationLevelsHighSchool = educationLevelsHighSchool;
    }

    public ISelectModel getDisciplinesModel()
    {
        return _disciplinesModel;
    }

    public void setDisciplinesModel(ISelectModel disciplinesModel)
    {
        _disciplinesModel = disciplinesModel;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

    public boolean isHasOlympiadDiplomaRelation()
    {
        return _hasOlympiadDiplomaRelation;
    }

    public void setHasOlympiadDiplomaRelation(boolean hasOlympiadDiplomaRelation)
    {
        _hasOlympiadDiplomaRelation = hasOlympiadDiplomaRelation;
    }

    public Discipline2RealizationWayRelation getSelectedDiscipline() {
        return _selectedDiscipline;
    }

    public void setSelectedDiscipline(Discipline2RealizationWayRelation selectedDiscipline) {
        this._selectedDiscipline = selectedDiscipline;
    }

    // выбрана Всероссийская олимпиада
    public boolean isSeriaVisible()
    {
        if (getDiploma().getDiplomaType() != null) {
            return !getDiploma().getDiplomaType().getCode().equals(UniecDefines.DIPLOMA_TYPE_OTHER);
        }
        return false;
    }
}
