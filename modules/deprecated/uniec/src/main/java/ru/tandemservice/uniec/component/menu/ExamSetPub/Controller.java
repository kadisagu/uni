/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.menu.ExamSetPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.IUniecComponents;
import ru.tandemservice.uniec.component.entrant.ChooseEntranceEducationOrgUnit.ChooseEntranceEducationOrgUnitModel;
import ru.tandemservice.uniec.component.entrant.EntranceDisciplineAddEdit.EntranceDisciplineAddEditModel;
import ru.tandemservice.uniec.component.menu.ExamSetItemAddEdit.ExamSetItemAddEditModel;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineKind;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.examset.ExamSetItem;
import ru.tandemservice.uniec.util.ExamSetUtil;

import java.util.Collections;
import java.util.List;

/**
 * @author Боба
 * @since 19.08.2008
 */
public class Controller extends AbstractBusinessController<IDAO, ExamSetPubModel>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        ExamSetPubModel model = getModel(component);

        getDao().prepare(model);

        prepapreEntranceDisciplineDataSource(component);

        prepareEntranceEducationOrgUnitDataSource(component);
    }

    private void prepapreEntranceDisciplineDataSource(IBusinessComponent component)
    {
        final ExamSetPubModel model = getModel(component);

        if (model.getEntranceDisciplineDataSource() != null) return;

        DynamicListDataSource<ExamSetItem> dataSource = new DynamicListDataSource<>(component, component1 -> {
            List<ExamSetItem> list = model.getExamSet().getSetItemList();
            model.getEntranceDisciplineDataSource().setCountRow(list.size());
            UniBaseUtils.createPage(model.getEntranceDisciplineDataSource(), list);
        });

        dataSource.addColumn(new SimpleColumn("Вступительное испытание", ExamSetItem.P_TITLE).setClickable(false).setOrderable(false));
        if (model.getExamSet().getEnrollmentCampaign().isEnrollmentPerCompTypeDiff())
        {
            dataSource.addColumn(new SimpleColumn("Бюджет", ExamSetItem.P_BUDGET, YesNoFormatter.INSTANCE).setClickable(false).setOrderable(false));
            dataSource.addColumn(new SimpleColumn("Контракт", ExamSetItem.P_CONTRACT, YesNoFormatter.INSTANCE).setClickable(false).setOrderable(false));
        }
        dataSource.addColumn(new SimpleColumn("Тип вступительного испытания", new String[]{ExamSetItem.P_TYPE, EntranceDisciplineType.P_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Вид вступительного испытания", new String[]{ExamSetItem.P_KIND, EntranceDisciplineKind.P_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Испытания по выбору", ExamSetItem.P_CHOICE_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditExamSetItem").setPermissionKey("editExamSetItem"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteExamSetItem", "Удалить вступительное мероприятие «{0}» из набора?", new Object[]{new String[]{ExamSetItem.P_TITLE}}).setPermissionKey("deleteExamSetItem"));

        model.setEntranceDisciplineDataSource(dataSource);
    }

    private void prepareEntranceEducationOrgUnitDataSource(IBusinessComponent component)
    {
        final ExamSetPubModel model = getModel(component);

        if (model.getEntranceEducationOrgUnitDataSource() != null) return;

        DynamicListDataSource<EnrollmentDirection> dataSource = new DynamicListDataSource<>(component, component1 -> {
            List<EnrollmentDirection> result = model.getExamSet().getList();
            Collections.sort(result, new EntityComparator<>(model.getEntranceEducationOrgUnitDataSource().getEntityOrder()));
            model.getEntranceEducationOrgUnitDataSource().setCountRow(result.size());
            UniBaseUtils.createPage(model.getEntranceEducationOrgUnitDataSource(), result);
        });

        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", EnrollmentDirection.P_TITLE));
        dataSource.addColumn(new SimpleColumn("Формирующее подр.", EnrollmentDirection.FORMATIVE_ORG_UNIT_SHORT_TITLE_KEY).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Территориальное подр.", EnrollmentDirection.TERRITORIAL_ORG_UNIT_SHORT_TITLE_KEY).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Форма освоения", EnrollmentDirection.educationOrgUnit().developForm().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Условие освоения", EnrollmentDirection.educationOrgUnit().developCondition().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Срок", EnrollmentDirection.educationOrgUnit().developPeriod().title().s()).setClickable(false));

        model.setEntranceEducationOrgUnitDataSource(dataSource);
    }

    public void onClickEditExamSetItem(IBusinessComponent component)
    {
        ExamSetPubModel model = getModel(component);

        Long id = component.getListenerParameter();

        component.createDefaultChildRegion(new ComponentActivator(IUniecComponents.EXAM_SET_ITEM_ADD_EDIT, new ParametersMap()
                .add(ExamSetItemAddEditModel.EXAM_SET_ID, model.getExamSetId())
                .add(ExamSetItemAddEditModel.EXAM_SET_ITEM_ID, ExamSetUtil.getExamSetItemId(model.getExamSet(), id))
        ));
    }

    public void onClickDeleteExamSetItem(IBusinessComponent component)
    {
        final ExamSetPubModel model = getModel(component);

        getDao().deleteEntranceDiscipline(model, (Long) component.getListenerParameter());
    }

    public void onClickAddExamSetItem(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniecComponents.EXAM_SET_ITEM_ADD_EDIT, new ParametersMap()
                .add(ExamSetItemAddEditModel.EXAM_SET_ID, getModel(component).getExamSetId())
                .add(ExamSetItemAddEditModel.ENTRANCE_DISCIPLINE_ID, null)
                .add(EntranceDisciplineAddEditModel.ENROLLMENT_DIRECTION_ID, null)
        ));
    }

    public void onClickAddEntranceEducationOrgUnit(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniecComponents.CHOOSE_ENTRANCE_EDUCATION_ORG_UNIT, new ParametersMap()
                .add(ChooseEntranceEducationOrgUnitModel.EXAM_SET_ID, getModel(component).getExamSetId())
        ));
    }
}
