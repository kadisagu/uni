package ru.tandemservice.uniec.entity.catalog;

import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniec.entity.catalog.gen.EntrantEnrollmentOrderTypeGen;

/**
 * Тип приказа на зачисление абитуриента в личный состав студентов
 */
public class EntrantEnrollmentOrderType extends EntrantEnrollmentOrderTypeGen
{
    public static final String REVERT_ORDER_CODE = "revert";

    public static EntityComboDataSourceHandler defaultSelectDSHandler(String name) {
        return new EntityComboDataSourceHandler(name, EntrantEnrollmentOrderType.class)
                .filter(EntrantEnrollmentOrderType.title())
                .order(EntrantEnrollmentOrderType.title());
    }
}