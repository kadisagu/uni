/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.ContestReport.ContestReportAdd;

import java.util.ArrayList;
import java.util.List;

import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignModel;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.dao.IEntranceEduLevelModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.report.ContestReport;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil.Parameters;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil.Parameters.Filter;

/**
 * @author oleyba
 * @since 12.07.2009
 */
public class Model implements IEnrollmentCampaignModel, IEnrollmentCampaignSelectModel, IEntranceEduLevelModel, MultiEnrollmentDirectionUtil.Model
{
    public static final String FORMATIVE_ORGUNIT = "formativeOrgUnit";
    public static final String TERRITORIAL_ORGUNIT = "territorialOrgUnit";
    public static final String EDUCATION_LEVEL_HIGH_SCHOOL = "educationLevelsHighSchool";
    public static final String DEVELOP_FORM = "developForm";
    public static final String DEVELOP_CONDITION = "developCondition";
    public static final String DEVELOP_TECH = "developTech";
    public static final String DEVELOP_PERIOD = "developPeriod";
    public static final String GROUP = "group";

    private List<EnrollmentCampaign> _enrollmentCampaignList;
    private ISelectModel _compensationTypeModel;
    private ISelectModel _studentCategoryModel;
    private ISelectModel _enrollmentCampaignModel;
    private ISelectModel _formativeOrgUnitModel;
    private ISelectModel _territorialOrgUnitModel;
    private ISelectModel _educationLevelHighSchoolModel;
    private ISelectModel _developFormModel;
    private ISelectModel _developConditionModel;
    private ISelectModel _developTechModel;
    private ISelectModel _developPeriodModel;
    private ISelectModel _groupModel;
    private ISelectModel _disciplineModel;

    private boolean byAllEnrollmentDirections;

    private CompensationType compensationType;
    private List<StudentCategory> studentCategoryList = new ArrayList<StudentCategory>();
    private OrgUnit _formativeOrgUnit;
    private OrgUnit _territorialOrgUnit;
    private EducationLevelsHighSchool _educationLevelsHighSchool;
    private DevelopForm _developForm;
    private DevelopCondition _developCondition;
    private DevelopTech _developTech;
    private DevelopPeriod _developPeriod;
    private GroupWrapper _group;
    private DisciplineWrapper _discipline;

    private ContestReport _report = new ContestReport();
    private IPrincipalContext _principalContext;

    @Override
    public Entrant getEntrant()
    {
        return null;
    }

    @Override
    public EntrantRequest getEntrantRequest()
    {
        return null;
    }

    @Override
    public IDataSettings getSettings()
    {
        return null;
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return getReport().getEnrollmentCampaign();
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        getReport().setEnrollmentCampaign(enrollmentCampaign);
    }

    @Override
    public Parameters getParameters()
    {
        Parameters result = new Parameters();
        result.setRequired(true, Filter.values());
        return result;
    }

    @Override
    public List<EnrollmentCampaign> getSelectedEnrollmentCampaignList()
    {
        return MultiEnrollmentDirectionUtil.getSingletonList(getEnrollmentCampaign());
    }

    @Override
    public List<OrgUnit> getSelectedFormativeOrgUnitList()
    {
        return MultiEnrollmentDirectionUtil.getSingletonList(getFormativeOrgUnit());
    }

    @Override
    public List<OrgUnit> getSelectedTerritorialOrgUnitList()
    {
        return MultiEnrollmentDirectionUtil.getSingletonList(getTerritorialOrgUnit());
    }

    @Override
    public List<EducationLevelsHighSchool> getSelectedEducationLevelHighSchoolList()
    {
        return MultiEnrollmentDirectionUtil.getSingletonList(getEducationLevelsHighSchool());
    }

    @Override
    public List<DevelopCondition> getSelectedDevelopConditionList()
    {
        return MultiEnrollmentDirectionUtil.getSingletonList(getDevelopCondition());
    }

    @Override
    public List<DevelopForm> getSelectedDevelopFormList()
    {
        return MultiEnrollmentDirectionUtil.getSingletonList(getDevelopForm());
    }

    @Override
    public List<DevelopPeriod> getSelectedDevelopPeriodList()
    {
        return MultiEnrollmentDirectionUtil.getSingletonList(getDevelopPeriod());
    }

    @Override
    public List<DevelopTech> getSelectedDevelopTechList()
    {
        return MultiEnrollmentDirectionUtil.getSingletonList(getDevelopTech());
    }

    public ISelectModel getCompensationTypeModel()
    {
        return _compensationTypeModel;
    }

    public void setCompensationTypeModel(ISelectModel compensationTypeModel)
    {
        _compensationTypeModel = compensationTypeModel;
    }

    @Override
    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    public void setDevelopCondition(DevelopCondition developCondition)
    {
        _developCondition = developCondition;
    }

    public ISelectModel getDevelopConditionModel()
    {
        return _developConditionModel;
    }

    public void setDevelopConditionModel(ISelectModel developConditionModel)
    {
        _developConditionModel = developConditionModel;
    }

    @Override
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public void setDevelopForm(DevelopForm developForm)
    {
        _developForm = developForm;
    }

    public ISelectModel getDevelopFormModel()
    {
        return _developFormModel;
    }

    public void setDevelopFormModel(ISelectModel developFormModel)
    {
        _developFormModel = developFormModel;
    }

    @Override
    public DevelopPeriod getDevelopPeriod()
    {
        return _developPeriod;
    }

    public void setDevelopPeriod(DevelopPeriod developPeriod)
    {
        _developPeriod = developPeriod;
    }

    public ISelectModel getDevelopPeriodModel()
    {
        return _developPeriodModel;
    }

    public void setDevelopPeriodModel(ISelectModel developPeriodModel)
    {
        _developPeriodModel = developPeriodModel;
    }

    @Override
    public DevelopTech getDevelopTech()
    {
        return _developTech;
    }

    public void setDevelopTech(DevelopTech developTech)
    {
        _developTech = developTech;
    }

    public ISelectModel getDevelopTechModel()
    {
        return _developTechModel;
    }

    public void setDevelopTechModel(ISelectModel developTechModel)
    {
        _developTechModel = developTechModel;
    }

    public DisciplineWrapper getDiscipline()
    {
        return _discipline;
    }

    public void setDiscipline(DisciplineWrapper discipline)
    {
        _discipline = discipline;
    }

    public ISelectModel getDisciplineModel()
    {
        return _disciplineModel;
    }

    public void setDisciplineModel(ISelectModel disciplineModel)
    {
        _disciplineModel = disciplineModel;
    }

    public ISelectModel getEducationLevelHighSchoolModel()
    {
        return _educationLevelHighSchoolModel;
    }

    public void setEducationLevelHighSchoolModel(ISelectModel educationLevelHighSchoolModel)
    {
        _educationLevelHighSchoolModel = educationLevelHighSchoolModel;
    }

    @Override
    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _educationLevelsHighSchool;
    }

    public void setEducationLevelsHighSchool(EducationLevelsHighSchool educationLevelsHighSchool)
    {
        _educationLevelsHighSchool = educationLevelsHighSchool;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public ISelectModel getEnrollmentCampaignModel()
    {
        return _enrollmentCampaignModel;
    }

    public void setEnrollmentCampaignModel(ISelectModel enrollmentCampaignModel)
    {
        _enrollmentCampaignModel = enrollmentCampaignModel;
    }

    @Override
    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        _formativeOrgUnit = formativeOrgUnit;
    }

    public ISelectModel getFormativeOrgUnitModel()
    {
        return _formativeOrgUnitModel;
    }

    public void setFormativeOrgUnitModel(ISelectModel formativeOrgUnitModel)
    {
        _formativeOrgUnitModel = formativeOrgUnitModel;
    }

    public GroupWrapper getGroup()
    {
        return _group;
    }

    public void setGroup(GroupWrapper group)
    {
        _group = group;
    }

    public ISelectModel getGroupModel()
    {
        return _groupModel;
    }

    public void setGroupModel(ISelectModel groupModel)
    {
        _groupModel = groupModel;
    }

    public ContestReport getReport()
    {
        return _report;
    }

    public void setReport(ContestReport report)
    {
        _report = report;
    }

    public ISelectModel getStudentCategoryModel()
    {
        return _studentCategoryModel;
    }

    public void setStudentCategoryModel(ISelectModel studentCategoryModel)
    {
        _studentCategoryModel = studentCategoryModel;
    }

    @Override
    public OrgUnit getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    public void setTerritorialOrgUnit(OrgUnit territorialOrgUnit)
    {
        _territorialOrgUnit = territorialOrgUnit;
    }

    public ISelectModel getTerritorialOrgUnitModel()
    {
        return _territorialOrgUnitModel;
    }

    public void setTerritorialOrgUnitModel(ISelectModel territorialOrgUnitModel)
    {
        _territorialOrgUnitModel = territorialOrgUnitModel;
    }

    public CompensationType getCompensationType()
    {
        return compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        this.compensationType = compensationType;
    }

    public List<StudentCategory> getStudentCategoryList()
    {
        return studentCategoryList;
    }

    public void setStudentCategoryList(List<StudentCategory> studentCategoryList)
    {
        this.studentCategoryList = studentCategoryList;
    }

    public IPrincipalContext getPrincipalContext()
    {
        return _principalContext;
    }

    public void setPrincipalContext(IPrincipalContext principalContext)
    {
        _principalContext = principalContext;
    }

    public boolean isByAllEnrollmentDirections()
    {
        return byAllEnrollmentDirections;
    }

    public void setByAllEnrollmentDirections(boolean byAllEnrollmentDirections)
    {
        this.byAllEnrollmentDirections = byAllEnrollmentDirections;
    }
}
