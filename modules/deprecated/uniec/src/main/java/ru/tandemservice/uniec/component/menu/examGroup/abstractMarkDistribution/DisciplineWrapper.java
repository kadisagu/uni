/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.menu.examGroup.abstractMarkDistribution;

import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

/**
 * @author vip_delete
 * @since 03.04.2009
 */
public final class DisciplineWrapper
{
    private Discipline2RealizationWayRelation _discipline;
    private SubjectPassForm _subjectPassForm;

    public DisciplineWrapper(Discipline2RealizationWayRelation discipline, SubjectPassForm subjectPassForm)
    {
        _discipline = discipline;
        _subjectPassForm = subjectPassForm;
    }

    public Discipline2RealizationWayRelation getDiscipline()
    {
        return _discipline;
    }

    public SubjectPassForm getSubjectPassForm()
    {
        return _subjectPassForm;
    }

    public String getTitle()
    {
        return _discipline.getTitle() + " (" + _subjectPassForm.getTitle() + ")";
    }

    public String getId()
    {
        return _discipline.getId() + "_" + _subjectPassForm.getId();
    }

    @Override
    public int hashCode()
    {
        return getId().hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (!(obj instanceof DisciplineWrapper)) return false;
        DisciplineWrapper other = (DisciplineWrapper)obj;
        if (!other.getDiscipline().equals(getDiscipline())) return false;
        return other.getSubjectPassForm().equals(getSubjectPassForm());
    }
}
