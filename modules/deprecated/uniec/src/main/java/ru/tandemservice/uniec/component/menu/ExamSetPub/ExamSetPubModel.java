/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.menu.ExamSetPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uniec.component.entrant.EntranceDisciplineAddEdit.EntranceDisciplineAddEditModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.examset.ExamSet;
import ru.tandemservice.uniec.entity.examset.ExamSetItem;

/**
 * @author Боба
 * @since 19.08.2008
 */
@State({
        @Bind(key = ExamSetPubModel.EXAM_SET_ID_PARAMETER, binding = "examSetId"),
        @Bind(key = EntranceDisciplineAddEditModel.ENTRANCE_DISCIPLINE_ID, binding = "entranceDisciplineId")
})
public class ExamSetPubModel
{
    public static final String EXAM_SET_ID_PARAMETER = "examSetId";

    private String _examSetId; // id набора
    private ExamSet _examSet;  // securityObject
    private Long _entranceDisciplineId;
    private DynamicListDataSource<ExamSetItem> _entranceDisciplineDataSource;
    private DynamicListDataSource<EnrollmentDirection> _entranceEducationOrgUnitDataSource;

    // Getters & Setters

    public String getExamSetId()
    {
        return _examSetId;
    }

    public void setExamSetId(String examSetId)
    {
        _examSetId = examSetId;
    }

    public ExamSet getExamSet()
    {
        return _examSet;
    }

    public void setExamSet(ExamSet examSet)
    {
        _examSet = examSet;
    }

    public Long getEntranceDisciplineId()
    {
        return _entranceDisciplineId;
    }

    public void setEntranceDisciplineId(Long entranceDisciplineId)
    {
        _entranceDisciplineId = entranceDisciplineId;
    }

    public DynamicListDataSource<ExamSetItem> getEntranceDisciplineDataSource()
    {
        return _entranceDisciplineDataSource;
    }

    public void setEntranceDisciplineDataSource(DynamicListDataSource<ExamSetItem> entranceDisciplineDataSource)
    {
        _entranceDisciplineDataSource = entranceDisciplineDataSource;
    }

    public DynamicListDataSource<EnrollmentDirection> getEntranceEducationOrgUnitDataSource()
    {
        return _entranceEducationOrgUnitDataSource;
    }

    public void setEntranceEducationOrgUnitDataSource(DynamicListDataSource<EnrollmentDirection> entranceEducationOrgUnitDataSource)
    {
        _entranceEducationOrgUnitDataSource = entranceEducationOrgUnitDataSource;
    }
}
