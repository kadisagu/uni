/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.events;

import org.tandemframework.hibsupport.event.single.FilteredSingleEntityEventListener;
import org.tandemframework.hibsupport.event.single.type.HibernateSaveEvent;
import org.tandemframework.hibsupport.event.single.listener.IHibernateInsertListener;
import ru.tandemservice.uniec.entity.entrant.*;

import java.util.List;

/**
 * @author vip_delete
 * @since 08.06.2009
 */
public class ExamPassDisciplineSaveEventListener extends FilteredSingleEntityEventListener<HibernateSaveEvent> implements IHibernateInsertListener
{
    @Override
    public void onFilteredEvent(HibernateSaveEvent event)
    {
        ExamPassDiscipline passDiscipline = (ExamPassDiscipline) event.getEntity();

        List<EntranceExamPhase> phases = EntranceExamPhase.getSelectBuilder4ExamPassDiscipline(passDiscipline)
                .createStatement(event.getSession()).list();
        if (phases.size() == 1) passDiscipline.setPassDate(phases.get(0).getPassDate());
    }
}
