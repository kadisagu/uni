/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec.base.bo.EcDistribution.util;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Vasily Zhukov
 * @since 29.05.2012
 */
public class EcgPriorityInfo implements IEcgPriorityInfo
{
    private Long _entrantId;

    private List<Long> _priorityIdList;
    
    private List<String> _priorityList;
    
    public EcgPriorityInfo(Long entrantId)
    {
        _entrantId = entrantId;
        _priorityIdList = new ArrayList<Long>();
        _priorityList = new ArrayList<String>();
    }
    
    // Getters

    public Long getEntrantId()
    {
        return _entrantId;
    }

    public List<Long> getPriorityIdList()
    {
        return _priorityIdList;
    }

    public List<String> getPriorityList()
    {
        return _priorityList;
    }
}
