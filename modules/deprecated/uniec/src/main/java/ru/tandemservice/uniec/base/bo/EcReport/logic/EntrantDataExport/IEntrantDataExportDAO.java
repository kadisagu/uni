/**
 *$Id:$
 */
package ru.tandemservice.uniec.base.bo.EcReport.logic.EntrantDataExport;

/**
 * @author Alexander Shaburov
 * @since 02.08.12
 */
public interface IEntrantDataExportDAO
{
    /**
     * Подготавливает модель формы добавления отчета.<p/>
     * Заполняет модели селектов и значения по умолчанию.
     * @param model модель
     * @return подготовленную модель
     */
    <M extends EntrantDataExportModel> M prepareModel(M model);

    /**
     * Листенер на изменеие поля Приемная компания.
     * Сетит даты заявления.
     * @param model модель
     * @return модель
     */
    <M extends EntrantDataExportModel> M onChangeEnrollmentCampaign(M model);
}
