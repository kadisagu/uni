/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EntrantEduInstDistrib.EntrantEduInstDistribAdd;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.NotUsedEducationLevelStage;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import org.tandemframework.shared.person.catalog.entity.EduInstitution;
import org.tandemframework.shared.person.catalog.entity.EducationLevelStage;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;

import java.util.*;

/**
 * @author Vasily Zhukov
 */
class EntrantEduInstDistribReportBuilder
{
    private Model _model;
    private Session _session;
    private static Comparator<IEntity> COMPARATOR = new EntityComparator("title");

    EntrantEduInstDistribReportBuilder(Model model, Session session)
    {
        _model = model;
        _session = session;
    }

    DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    @SuppressWarnings("unchecked")
    private byte[] buildReport()
    {
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ENTRANT_EDU_INST_DISTIB);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());

        // сразу подставим название вуза
        RtfInjectModifier im = new RtfInjectModifier();
        im.put("academyTitle", TopOrgUnit.getInstance().getTitle());
        im.modify(document);

        final int grayIndex = document.getHeader().getColorTable().addColor(128, 128, 128);

        final IRtfControl left = RtfBean.getElementFactory().createRtfControl(IRtfData.QL);
        final IRtfControl center = RtfBean.getElementFactory().createRtfControl(IRtfData.QC);
        final IRtfControl pard = RtfBean.getElementFactory().createRtfControl(IRtfData.PARD);
        final IRtfControl par = RtfBean.getElementFactory().createRtfControl(IRtfData.PAR);

        List<EducationLevelStage> stageList = null;

        if (_model.isStageActive())
        {
            stageList = new DQLSelectBuilder().fromEntity(EducationLevelStage.class, "e")
                    .where(DQLExpressions.in(DQLExpressions.property(EducationLevelStage.parent().fromAlias("e")), _model.getStageList()))
                    .where(DQLExpressions.notIn(DQLExpressions.property(EducationLevelStage.id().fromAlias("e")), new DQLSelectBuilder().fromEntity(NotUsedEducationLevelStage.class, "n")
                            .column(DQLExpressions.property(NotUsedEducationLevelStage.educationLevelStage().id().fromAlias("n")))
                            .where(DQLExpressions.eq(DQLExpressions.property(NotUsedEducationLevelStage.personRoleName().fromAlias("n")), DQLExpressions.value(Entrant.class.getSimpleName())))
                            .buildQuery()
                    ))
                    .createStatement(_session).list();

            stageList.addAll(_model.getStageList());
        }

        // статистика
        List<RequestedEnrollmentDirection>[] data = new List[]{
                getDirectionList(Model.ENROLLMENT_CAMP_STAGE_DOCUMENTS, stageList),
                getDirectionList(Model.ENROLLMENT_CAMP_STAGE_EXAMS, stageList),
                getDirectionList(Model.ENROLLMENT_CAMP_STAGE_ENROLLMENT, stageList)
        };

        if (_model.getGroup().isTrue())
        {
            final Comparator<IEntity> ENROLLMENT_DIRECTION_COMPARATOR = new EntityComparator(EnrollmentDirection.printTitle().s());

            Map<OrgUnit, Map<EnrollmentDirection, List<RequestedEnrollmentDirection>[]>> stat = new TreeMap<>(COMPARATOR);
            for (int i = 0; i < data.length; i++)
            {
                for (RequestedEnrollmentDirection direction : data[i])
                {
                    EnrollmentDirection enrollmentDirection = direction.getEnrollmentDirection();
                    OrgUnit formativeOrgUnit = enrollmentDirection.getEducationOrgUnit().getFormativeOrgUnit();

                    Map<EnrollmentDirection, List<RequestedEnrollmentDirection>[]> map = stat.get(formativeOrgUnit);
                    if (map == null)
                        stat.put(formativeOrgUnit, map = new TreeMap<>(ENROLLMENT_DIRECTION_COMPARATOR));

                    List<RequestedEnrollmentDirection>[] listArray = map.get(enrollmentDirection);
                    if (listArray == null)
                    {
                        map.put(enrollmentDirection, listArray = new List[data.length]);
                        for (int j = 0; j < data.length; j++)
                            listArray[j] = new ArrayList<>();
                    }

                    listArray[i].add(direction);
                }
            }

            RtfTable table = (RtfTable) document.getElementList().get(4); // такой шаблон

            document.getElementList().remove(3); // убираем перенос перед таблицей
            document.getElementList().remove(3); // убираем таблицу

            for (Map.Entry<OrgUnit, Map<EnrollmentDirection, List<RequestedEnrollmentDirection>[]>> entry : stat.entrySet())
            {
                OrgUnit formativeOrgUnit = entry.getKey();

                // название группы
                document.getElementList().add(center);
                document.getElementList().add(par);
                IRtfGroup group = RtfBean.getElementFactory().createRtfGroup();
                group.setElementList(new RtfString().boldBegin().fontSize(22).append(StringUtils.isEmpty(formativeOrgUnit.getNominativeCaseTitle()) ? formativeOrgUnit.getTitle() : formativeOrgUnit.getNominativeCaseTitle()).boldEnd().toList());
                document.getElementList().add(group);
                document.getElementList().add(par);
                document.getElementList().add(par);

                for (Map.Entry<EnrollmentDirection, List<RequestedEnrollmentDirection>[]> subEntry : entry.getValue().entrySet())
                {
                    EnrollmentDirection enrollmentDirection = subEntry.getKey();

                    // название таблицы
                    document.getElementList().add(left);
                    IRtfGroup subGroup = RtfBean.getElementFactory().createRtfGroup();
                    subGroup.setElementList(new RtfString().boldBegin().append(enrollmentDirection.getPrintTitle()).boldEnd().toList());
                    document.getElementList().add(subGroup);
                    document.getElementList().add(par);  // после заголовка таблицы перенос
                    document.getElementList().add(pard);

                    // заполним таблицу
                    IRtfElement t = table.getClone();
                    modifyTable(Collections.singletonList(t), subEntry.getValue(), center, left, grayIndex);

                    // вставим в основной документ
                    document.getElementList().add(t);
                    document.getElementList().add(pard); // после каждой таблицы перенос
                    document.getElementList().add(par);
                }
            }
        } else
        {
            // заполним таблицу
            modifyTable(document.getElementList(), data, center, left, grayIndex);
        }
        return RtfUtil.toByteArray(document);
    }

    private List<RequestedEnrollmentDirection> getDirectionList(int stateId, List<EducationLevelStage> stageList)
    {
        MQBuilder builder;
        if (Model.ENROLLMENT_CAMP_STAGE_DOCUMENTS == stateId || Model.ENROLLMENT_CAMP_STAGE_EXAMS == stateId)
        {
            builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "r");
            builder.addJoin("r", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit(), "ou");
            builder.addJoin("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "request");
            builder.addJoin("request", EntrantRequest.L_ENTRANT, "entrant");
            builder.addJoin("entrant", Entrant.L_PERSON, "person");
            builder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
            if (_model.isStudentCategoryActive())
                builder.add(MQExpression.in("r", RequestedEnrollmentDirection.L_STUDENT_CATEGORY, _model.getStudentCategoryList()));
            if (_model.isCompensationTypeActive())
                builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_COMPENSATION_TYPE, _model.getCompensationType()));
            if (Model.ENROLLMENT_CAMP_STAGE_EXAMS == stateId)
                builder.add(MQExpression.notIn("r", RequestedEnrollmentDirection.state().code(), UniecDefines.ENTRANT_STATE_ACTIVE_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY, UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE));
        } else if (Model.ENROLLMENT_CAMP_STAGE_ENROLLMENT == stateId)
        {
            builder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "p", new String[]{PreliminaryEnrollmentStudent.requestedEnrollmentDirection().s()});
            builder.addJoin("p", PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT, "ou");
            builder.addJoin("p", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, "r");
            builder.addJoin("r", RequestedEnrollmentDirection.entrantRequest().s(), "request");
            builder.addJoin("request", EntrantRequest.entrant().s(), "entrant");
            builder.addJoin("entrant", Entrant.person().s(), "person");
            if (_model.isStudentCategoryActive())
                builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.L_STUDENT_CATEGORY, _model.getStudentCategoryList()));
            if (_model.isCompensationTypeActive())
                builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.L_COMPENSATION_TYPE, _model.getCompensationType()));
            if (_model.isParallelActive() && _model.getParallel().isTrue())
                builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.parallel().s(), Boolean.FALSE));
        } else
            throw new RuntimeException("Unknown EnrollmentCampaignStage: " + stateId);

        builder.add(MQExpression.eq("entrant", Entrant.P_ARCHIVAL, Boolean.FALSE));
        builder.add(MQExpression.eq("entrant", Entrant.L_ENROLLMENT_CAMPAIGN, _model.getEnrollmentCampaign()));
        builder.add(UniMQExpression.betweenDate("r", RequestedEnrollmentDirection.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));

        builder.add(MQExpression.isNotNull("person", Person.L_PERSON_EDU_INSTITUTION));

        if (_model.isStageActive())
            builder.add(MQExpression.in("person", Person.personEduInstitution().educationLevelStage(), stageList));
        if (_model.isQualificationActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.educationLevelHighSchool().educationLevel().qualification(), _model.getQualificationList()));
        if (_model.isFormativeOrgUnitActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, _model.getFormativeOrgUnitList()));
        if (_model.isTerritorialOrgUnitActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, _model.getTerritorialOrgUnitList()));
        if (_model.isEducationLevelHighSchoolActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, _model.getEducationLevelHighSchoolList()));
        if (_model.isDevelopFormActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_DEVELOP_FORM, _model.getDevelopFormList()));
        if (_model.isDevelopConditionActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_DEVELOP_CONDITION, _model.getDevelopConditionList()));
        if (_model.isDevelopTechActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_DEVELOP_TECH, _model.getDevelopTechList()));
        if (_model.isDevelopPeriodActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_DEVELOP_PERIOD, _model.getDevelopPeriodList()));

        return builder.getResultList(_session);
    }

    // регион россии либо null -> населенный пункт -> ОУ либо null -> число абитуриентов
    private static Map<AddressItem, Map<AddressItem, Map<EduInstitution, int[]>>> getStat(List<RequestedEnrollmentDirection>[] data)
    {
        Map<AddressItem, Map<AddressItem, Map<EduInstitution, int[]>>> stat = new TreeMap<>(COMPARATOR);

        for (int i = 0; i < data.length; i++)
        {
            Set<Entrant> usedIds = new HashSet<>();

            for (RequestedEnrollmentDirection direction : data[i])
            {
                Entrant entrant = direction.getEntrantRequest().getEntrant();

                if (usedIds.add(entrant))
                {
                    PersonEduInstitution personEduInstitution = entrant.getPerson().getPersonEduInstitution();

                    AddressItem item = personEduInstitution.getAddressItem();

                    boolean russia = item.getCountry().getCode() == IKladrDefines.RUSSIA_COUNTRY_CODE;

                    // detect region
                    AddressItem region;
                    if (russia)
                    {
                        region = item;
                        while (region.getParent() != null)
                            region = region.getParent();
                    } else
                        region = null;

                    EduInstitution eduInstitution = russia ? personEduInstitution.getEduInstitution() : null;

                    Map<AddressItem, Map<EduInstitution, int[]>> subStat = stat.get(region);

                    if (subStat == null)
                        stat.put(region, subStat = new TreeMap<>(COMPARATOR));

                    Map<EduInstitution, int[]> subSubStat = subStat.get(item);

                    if (subSubStat == null)
                        subStat.put(item, subSubStat = new TreeMap<>(COMPARATOR));

                    int[] mutableInt = subSubStat.get(eduInstitution);

                    if (mutableInt == null)
                        subSubStat.put(eduInstitution, mutableInt = new int[data.length]);

                    mutableInt[i]++;
                }
            }
        }

        return stat;
    }

    private static final int LEVEL_REGION = 0;
    private static final int LEVEL_CITY = 1;
    private static final int LEVEL_INST = 2;
    private static final int LEVEL_TOTAL = 3;

    private static void buildReportList(List<RequestedEnrollmentDirection> data[], List<String[]> rowList, List<Integer> rowTypeList)
    {
        Map<AddressItem, Map<AddressItem, Map<EduInstitution, int[]>>> stat = getStat(data);

        int len = data.length;

        int[] totalSum = new int[len];

        for (Map.Entry<AddressItem, Map<AddressItem, Map<EduInstitution, int[]>>> entry : stat.entrySet())
        {
            AddressItem region = entry.getKey();

            boolean russia = region != null;

            int regionIndex = rowList.size();
            int[] regionSum = new int[len];

            rowList.add(null);
            rowTypeList.add(LEVEL_REGION);

            for (Map.Entry<AddressItem, Map<EduInstitution, int[]>> subEntry : entry.getValue().entrySet())
            {
                AddressItem city = subEntry.getKey();
                int cityIndex = rowList.size();
                int[] citySum = new int[len];

                rowList.add(null);
                rowTypeList.add(LEVEL_CITY);

                for (Map.Entry<EduInstitution, int[]> subSubEntry : subEntry.getValue().entrySet())
                {
                    EduInstitution eduInstitution = subSubEntry.getKey();
                    int[] sum = subSubEntry.getValue();

                    if (russia)
                    {
                        rowList.add(getStringArray(eduInstitution == null ? "Не выбрано образовательное учреждение" : eduInstitution.getTitle(), sum));
                        rowTypeList.add(LEVEL_INST);
                    }

                    for (int i = 0; i < len; i++) citySum[i] += sum[i];
                }

                rowList.set(cityIndex, getStringArray(russia ? city.getTitleWithType() : city.getTitleWithType() + " (" + city.getCountry().getTitle() + ")", citySum));

                for (int i = 0; i < len; i++) regionSum[i] += citySum[i];
            }

            rowList.set(regionIndex, getStringArray(!russia ? "Получили образование в другом государстве" : region.getTitleWithType(), regionSum));

            for (int i = 0; i < len; i++) totalSum[i] += regionSum[i];
        }

        rowList.add(getStringArray("ИТОГО:", totalSum));
        rowTypeList.add(LEVEL_TOTAL);
    }

    private static String[] getStringArray(String title, int[] sum)
    {
        String[] data = new String[sum.length + 1];
        data[0] = title;
        for (int i = 0; i < sum.length; i++)
            data[i + 1] = Integer.toString(sum[i]);
        return data;
    }

    private static void modifyTable(List<IRtfElement> elementList, List<RequestedEnrollmentDirection>[] data, final IRtfControl center, final IRtfControl left, final int grayIndex)
    {
        final List<String[]> rowList = new ArrayList<>();
        final List<Integer> rowTypeList = new ArrayList<>();
        buildReportList(data, rowList, rowTypeList);

        RtfTableModifier tm = new RtfTableModifier();
        tm.put("T", rowList.toArray(new String[rowList.size()][]));
        tm.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                int rowType = rowTypeList.get(rowIndex);

                switch (rowType)
                {
                    case LEVEL_REGION:
                        cell.getElementList().add(center);
                        return new RtfString().boldBegin().color(grayIndex).append(value).boldEnd().toList();
                    case LEVEL_CITY:
                        cell.getElementList().add(center);
                        return new RtfString().color(grayIndex).append(value).toList();
                    case LEVEL_INST:
                        cell.getElementList().add(colIndex == 0 ? left : center);
                        return new RtfString().append(value).toList();
                    case LEVEL_TOTAL:
                        cell.getElementList().add(colIndex == 0 ? left : center);
                        return new RtfString().boldBegin().append(value).boldEnd().toList();
                    default:
                        throw new RuntimeException("Unknown rowType: " + rowType);
                }
            }
        });
        tm.modify(elementList);
    }

    public static class EntityComparator implements Comparator<IEntity>
    {
        private String _property;

        public EntityComparator(String property)
        {
            _property = property;
        }

        @Override
        public int compare(IEntity o1, IEntity o2)
        {
            if (o1 == null)
            {
                return o2 == null ? 0 : 1;
            } else
            {
                if (o2 == null) return -1;

                Comparable p1 = ((Comparable) o1.getProperty(_property));
                Comparable p2 = ((Comparable) o2.getProperty(_property));

                @SuppressWarnings("unchecked")
                int r = p1.compareTo(p2);

                if (r != 0) return r;

                return o1.getId().compareTo(o2.getId());
            }
        }
    }
}
