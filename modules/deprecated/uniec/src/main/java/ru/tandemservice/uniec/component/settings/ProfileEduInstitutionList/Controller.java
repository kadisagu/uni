/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.settings.ProfileEduInstitutionList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.catalog.entity.EduInstitution;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.IUniecComponents;

/**
 * @author vip_delete
 * @since 04.06.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.setSettings(component.getSettings());

        getDao().prepare(model);

        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<EduInstitution> dataSource = UniBaseUtils.createDataSource(component, getDao());

        dataSource.addColumn(new SimpleColumn("Название", EduInstitution.P_TITLE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Вид образовательного учреждения", EduInstitution.eduInstitutionKind().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Населенный пункт", EduInstitution.SETTLEMENT_TITLE_WITH_TYPE).setClickable(false));
        dataSource.addColumn(new ActionColumn("Исключить", ActionColumn.DELETE, "onClickDeleteItem", "Исключить «{0}» из профильных образовательных учреждений?", EduInstitution.P_TITLE));

        model.setDataSource(dataSource);
    }

    public void onClickAddItem(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniecComponents.PROFILE_EDU_INSTITUTION_ADD));
    }

    public void onClickDeleteItem(IBusinessComponent component)
    {
        getDao().deleteRow(component);
    }
}
