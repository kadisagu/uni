/**
 *$Id$
 */
package ru.tandemservice.uniec.base.bo.EcSystemAction;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniec.base.bo.EcSystemAction.logic.EcSystemActionDao;
import ru.tandemservice.uniec.base.bo.EcSystemAction.logic.IEcSystemActionDao;

/**
 * @author Alexander Shaburov
 * @since 13.02.13
 */
@Configuration
public class EcSystemActionManager extends BusinessObjectManager
{
    public static EcSystemActionManager instance()
    {
        return instance(EcSystemActionManager.class);
    }

    @Bean
    public IEcSystemActionDao dao()
    {
        return new EcSystemActionDao();
    }
}
