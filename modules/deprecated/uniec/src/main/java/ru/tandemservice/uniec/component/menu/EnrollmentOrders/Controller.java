/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.menu.EnrollmentOrders;

import org.apache.commons.io.IOUtils;
import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.sec.entity.Admin;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.IUniecComponents;
import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager;
import ru.tandemservice.uniec.base.bo.EcOrder.ui.RevertAddEdit.EcOrderRevertAddEdit;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniec.ws.enrollmentorder.EnrollmentOrderEnvironmentNode;
import ru.tandemservice.uniec.ws.enrollmentorder.IEnrollmentOrderServiceDao;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.entity.catalog.OrderStates;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author vip_delete
 * @since 07.04.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.setSettings(component.getSettings());

        getDao().prepare(model);

        prepareListDataSource(component);
    }

    @SuppressWarnings({"unchecked"})
    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource dataSource = UniBaseUtils.createDataSource(component, getDao());

        dataSource.addColumn(new CheckboxColumn("selected").setDisabledProperty("disabledStateChange"));
        dataSource.addColumn(new SimpleColumn(IAbstractOrder.P_CREATE_DATE, "Дата формирования", IAbstractOrder.P_CREATE_DATE, DateFormatter.DATE_FORMATTER_WITH_TIME));
        dataSource.addColumn(new SimpleColumn("Дата приказа", EnrollmentOrder.P_COMMIT_DATE).setFormatter(DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("№ приказа", IAbstractOrder.P_NUMBER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Тип приказа", EnrollmentOrder.type().shortTitle().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn(IAbstractOrder.P_COMMIT_DATE_SYSTEM, "Дата проведения", IAbstractOrder.P_COMMIT_DATE_SYSTEM, DateFormatter.DATE_FORMATTER_WITH_TIME).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Состояние", new String[]{IAbstractOrder.L_STATE, OrderStates.P_TITLE}).setClickable(false));

        dataSource.addColumn(CommonBaseUtil.getPrintColumn("onClickPrint", "Печать").setPermissionKey("printEnrollmentOrder").setDisabledProperty("disabledPrint"));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditEnrollmentOrder").setPermissionKey("editEnrollmentOrder").setDisabledProperty(EnrollmentOrder.P_READONLY));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteEnrollmentOrder", "Удалить «{0}»?", IAbstractOrder.P_TITLE).setPermissionKey("deleteEnrollmentOrder").setDisabledProperty(EnrollmentOrder.P_READONLY));

        dataSource.setOrder(IAbstractOrder.P_COMMIT_DATE_SYSTEM, OrderDirection.desc);
        model.setDataSource(dataSource);
    }

    public void onClickOrdersSearch(IBusinessComponent component)
    {
        component.saveSettings();
        getModel(component).getDataSource().refresh();
    }

    public void onClickOrdersClear(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.getSettings().clear();
        getDao().prepare(model);
        onClickOrdersSearch(component);
    }

    // listeners

    public void onClickAddEnrollmentOrder(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniecComponents.ENROLLMENT_ORDER_ADD_EDIT));
    }

    public void onClickAddEnrollmentRevertOrder(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(EcOrderRevertAddEdit.class.getSimpleName()));
    }

    public void onClickPrint(IBusinessComponent component)
    {
        EcOrderManager.instance().dao().getDownloadPrintForm(component.<Long>getListenerParameter(), false);
    }

    public void onClickEditEnrollmentOrder(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniecComponents.ENROLLMENT_ORDER_ADD_EDIT, new ParametersMap().add("orderId", component.getListenerParameter())));
    }

    public void onClickDeleteEnrollmentOrder(IBusinessComponent component)
    {
        EcOrderManager.instance().dao().deleteOrder(getDao().getNotNull(EnrollmentOrder.class, (Long) component.getListenerParameter()));
    }

    public void onClickSendCheckedToCoordination(IBusinessComponent component)
    {
        IPrincipalContext principalContext = component.getUserContext().getPrincipalContext();
        if (!(principalContext instanceof IPersistentPersonable) && !(principalContext instanceof Admin && Debug.isEnabled()))
            throw new ApplicationException(EntityRuntime.getMeta(principalContext.getId()).getTitle() + " не может отправлять документы на согласование.");
        if (principalContext instanceof Admin)
            getDao().doSendCheckedToCoordination(getModel(component), null);
        else
            getDao().doSendCheckedToCoordination(getModel(component), (IPersistentPersonable) principalContext);
    }

    public void onClickCommitChecked(IBusinessComponent component)
    {
        getDao().doCommit(getModel(component));
    }

    public void onClickEnrollmentOrderExport(IBusinessComponent component) throws Exception
    {
        Model model = getModel(component);

        @SuppressWarnings("unchecked")
        List<EnrollmentOrder> enrollmentOrderList = getDao().getEnrollmentOrderList(model);

        EnrollmentOrderEnvironmentNode node = IEnrollmentOrderServiceDao.INSTANCE.get().getEnrollmentOrderEnvironmentData(model.getEnrollmentCampaign(), enrollmentOrderList);

        if (node.enrollmentOrder.row.isEmpty())
            throw new ApplicationException("В списке нет приказов, соответствующим критериям выгрузки. Должен быть хотя бы один приказ в состоянии «Согласовано» или «Проведено».");

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(output);
        for (EnrollmentOrderEnvironmentNode.EnrollmentOrderNode.EnrollmentOrderRow row : node.enrollmentOrder.row)
        {
            long id = row.id;
            zip.putNextEntry(new ZipEntry(id + ".rtf"));
            IOUtils.write(row.text, zip);
            zip.closeEntry();
            row.text = null;
        }

        ByteArrayOutputStream result = new ByteArrayOutputStream();
        Marshaller marshaller = JAXBContext.newInstance(EnrollmentOrderEnvironmentNode.class).createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(node, result);

        zip.putNextEntry(new ZipEntry("data.xml"));
        IOUtils.write(result.toByteArray(), zip);
        zip.close();

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().fileName("EnrollmentOrderData.zip").document(output.toByteArray()).zip(), false);
    }
}
