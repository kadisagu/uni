/* $Id$ */
package ru.tandemservice.uniec.component.listextract.ent2.e2.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.AbstractListExtractPubDAO;
import ru.tandemservice.uniec.entity.orders.SplitContractMasterEntrantsStuListExtract;

/**
 * @author Dmitry Seleznev
 * @since 21.08.2012
 */
public class DAO extends AbstractListExtractPubDAO<SplitContractMasterEntrantsStuListExtract, Model> implements IDAO
{
}