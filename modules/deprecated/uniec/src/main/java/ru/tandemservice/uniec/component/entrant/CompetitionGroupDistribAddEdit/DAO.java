package ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribAddEdit;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.education.gen.EducationOrgUnitGen;
import ru.tandemservice.uniec.dao.ecg.IEnrollmentCompetitionGroupDAO;
import ru.tandemservice.uniec.entity.ecg.EcgDistribObject;
import ru.tandemservice.uniec.entity.ecg.EcgDistribQuota;
import ru.tandemservice.uniec.entity.ecg.gen.EcgDistribObjectGen;
import ru.tandemservice.uniec.entity.ecg.gen.EcgDistribQuotaGen;
import ru.tandemservice.uniec.entity.entrant.CompetitionGroup;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.gen.*;

import java.util.*;

/**
 * @author vdanilov
 */
public class DAO extends UniBaseDao implements IDAO
{


    @Override
    public void prepare(final Model model)
    {
        if (null == model.getDistrib().getId())
        {
            if (null == model.getDistrib().getCompetitionGroup())
            {
                throw new NullPointerException(); // wtf (есть объект, но нет id)
            }
        } else
        {
            final IEntity e = this.get(model.getDistrib().getId());
            if (e instanceof EcgDistribObject)
            {
                model.setDistrib((EcgDistribObject) e);

            } else if (e instanceof CompetitionGroup)
            {
                model.setDistrib(new EcgDistribObject());
                model.getDistrib().setParent(null == model.getParentId() ? null : get(EcgDistribObject.class, model.getParentId()));
                if (null != model.getDistrib().getParent())
                {
                    /* копируем из родительского распределения */
                    model.getDistrib().setCompetitionGroup(model.getDistrib().getParent().getCompetitionGroup());
                    model.getDistrib().setCompensationType(model.getDistrib().getParent().getCompensationType());

                } else
                {
                    /* делаем все руками */
                    model.getDistrib().setCompetitionGroup((CompetitionGroup) e);
                    model.getDistrib().setCompensationType(
                            (null == model.getCompensationTypeId()) ? null : this.get(CompensationType.class, model.getCompensationTypeId())
                    );
                }


            } else
            {
                if (null == e)
                {
                    throw new NullPointerException(String.valueOf(model.getDistrib().getId()));
                } // wtf (объекта по id нет)
                throw new IllegalStateException(String.valueOf(e.getClass())); // wtf (странный тпи объекта)
            }
        }

        IEnrollmentCompetitionGroupDAO.INSTANCE.get().checkEditable(model.getDistrib());


        if (StringUtils.isEmpty(model.getDistrib().getTitle()))
        {
            if (null != model.getDistrib().getParent())
            {
                model.getDistrib().setTitle(
                        model.getDistrib().getParent().getTitle() + " [" + DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()) + "]"
                );
            } else if (null != model.getDistrib().getCompensationType())
            {
                model.getDistrib().setTitle(
                        model.getDistrib().getCompetitionGroup().getTitle() +
                        " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()) +
                        " (" + model.getDistrib().getCompensationType().getShortTitle().toLowerCase() + ")"
                );
            }
        }

        model.setCompensationTypeList(this.getCatalogItemListOrderByCode(CompensationType.class));
        model.setCategoryList(Arrays.asList(new IdentifiableWrapper(0L, "Студент/Слушатель"), new IdentifiableWrapper(1L, "Второе высшее")));
        model.getDistrib().setSecondHighAdmission(model.isSecondHighAdmission());
        model.setCategory(model.getDistrib().isSecondHighAdmission() ? new IdentifiableWrapper(1L, "Второе высшее") : new IdentifiableWrapper(0L, "Студент/Слушатель"));
        this.refreshQuotaList(model);
    }

    @SuppressWarnings("unchecked")
    private List<EcgDistribQuota> refreshQuotaList(final Model model)
    {
        final Map<EnrollmentDirection, EcgDistribQuota> quotaMap = SafeMap.get(key -> {
            final EcgDistribQuota q = new EcgDistribQuota();
            q.setDirection(key);
            q.setDistribution(model.getDistrib());
            q.setQuota(-1);
            return q;
        });


        if (null != model.getDistrib().getId())
        {
            final MQBuilder qb = new MQBuilder(EcgDistribQuotaGen.ENTITY_CLASS, "q");
            qb.add(MQExpression.eq("q", EcgDistribQuotaGen.L_DISTRIBUTION, model.getDistrib()));
            final List<EcgDistribQuota> quotas = qb.getResultList(this.getSession());
            for (final EcgDistribQuota quota : quotas)
            {
                quotaMap.put(quota.getDirection(), quota);
            }
        }

        final MQBuilder builder = new MQBuilder(EnrollmentDirectionGen.ENTITY_CLASS, "d");
        builder.add(MQExpression.eq("d", EnrollmentDirectionGen.L_COMPETITION_GROUP, model.getDistrib().getCompetitionGroup()));
        builder.add(MQExpression.eq("d", EnrollmentDirectionGen.L_ENROLLMENT_CAMPAIGN, model.getDistrib().getCompetitionGroup().getEnrollmentCampaign())); // double check //
        builder.addOrder("d", EnrollmentDirectionGen.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnitGen.L_EDUCATION_LEVEL_HIGH_SCHOOL + "." + ICatalogItem.CATALOG_ITEM_TITLE);
        builder.addOrder("d", EnrollmentDirectionGen.L_EDUCATION_ORG_UNIT + ".id");
        final List<EnrollmentDirection> directions = builder.getResultList(this.getSession());
        for (final EnrollmentDirection direction : directions)
        {
            quotaMap.get(direction);
        }

        if (null == model.getDistrib().getParent())
        {
            /* стандартно */

            for (final EcgDistribQuota quota : model.getQuotaList())
            {
                final EcgDistribQuota q = quotaMap.get(quota.getDirection());
                q.setQuota(quota.getQuota()); // reassign value
            }


            boolean ok = false;
            for (final EcgDistribQuota q : quotaMap.values())
            {
                final Integer plan = this._plan(q);
                if (null != plan)
                {
                    if (plan > 0)
                    {
                        if (q.getQuota() < 0)
                        {
                            // значение еще не задано
                            q.setQuota(plan);

                        } else if (q.getQuota() > plan)
                        {
                            // превышает :(
                            q.setQuota(plan);

                        }
                    } else
                    {
                        q.setQuota(0);
                    }
                    ok |= (q.getQuota() > 0);
                } else
                {
                    // а нету здесь плана - считаем безлимитка
                    ok |= true;
                }
            }

            if (!ok)
            {
                throw new ApplicationException("По группе «" + model.getDistrib().getCompetitionGroup().getTitle() + "» по направлениям подготовки (специальностям) не осталось плановых мест с видом возмещения затрат «" + model.getDistrib().getCompensationType().getShortTitle() + "».");
            }
        } else
        {
            /* надо брать из базового распределения */

            final MQBuilder b = new MQBuilder(EcgDistribQuotaGen.ENTITY_CLASS, "q");
            b.add(MQExpression.eq("q", EcgDistribQuotaGen.L_DISTRIBUTION, model.getDistrib().getParent()));
            for (final EcgDistribQuota q : b.<EcgDistribQuota>getResultList(getSession()))
            {
                quotaMap.get(q.getDirection()).setQuota(q.getQuota());
            }

        }

        model.setQuotaList(new ArrayList<>(CollectionUtils.collect(directions, quotaMap::get)));

        return model.getQuotaList();
    }


    @Override
    @SuppressWarnings("unchecked")
    public void save(final Model model)
    {
        if (null == model.getDistrib().getRegistrationDate())
        {
            model.getDistrib().setRegistrationDate(new Date());
        }

        final Session session = this.getSession();

        final List<EcgDistribObject> similiar = session.createCriteria(EcgDistribObject.class)
        .add(Restrictions.eq(EcgDistribObjectGen.L_COMPETITION_GROUP, model.getDistrib().getCompetitionGroup()))
        .add(Restrictions.eq(EcgDistribObjectGen.L_COMPENSATION_TYPE, model.getDistrib().getCompensationType()))
        .add(Restrictions.eq(EcgDistribObjectGen.P_SECOND_HIGH_ADMISSION, model.getDistrib().isSecondHighAdmission()))
        .add(Restrictions.eq(EcgDistribObjectGen.P_TITLE, model.getDistrib().getTitle()))
        .list();
        similiar.remove(model.getDistrib());

        if (!similiar.isEmpty())
        {
            throw new ApplicationException("Указанное название уже используется в системе.");
        }

        session.saveOrUpdate(model.getDistrib());

        final List<EcgDistribQuota> list = this.refreshQuotaList(model);
        for (final EcgDistribQuota q : list)
        {
            this.validate(q, q.getQuota());
            session.saveOrUpdate(q);
        }
    }

    @Override
    public void validate(final EcgDistribQuota q, final int quota) throws ApplicationException
    {
        final Integer plan = this._plan(q);
        if (null == plan)
        {
            return; /* no limit */
        }

        final int p = Math.max(0, plan); /* ну... отрицательных распределений не бывает, как мы знаем */
        if (quota > p)
        {
            throw new ApplicationException("Значение плана приема по направлению (специальности) «" + q.getDirection().getEducationOrgUnit().getEducationLevelHighSchool().getDisplayableTitle() + "» не может превышать «" + p + "».");
        }
    }

    private Integer _plan(final EcgDistribQuota q)
    {
        if (null != q.getDistribution().getParent())
        {
            return null; /* нет распределений */
        }

        final EnrollmentDirection direction = q.getDirection();
        if (null == direction)
        {
            return null; /* no direction, wtf? */
        }

        final CompensationType compensationType = q.getDistribution().getCompensationType();
        if (null == compensationType)
        {
            return null; /* no compensation type yet */
        }

        final boolean budget = compensationType.isBudget();
        final Integer p = (budget ? direction.getMinisterialPlan() : direction.getContractPlan());
        if (null == p)
        {
            return null; /* no limit*/
        }

        int plan = p;
        final MQBuilder builder = new MQBuilder(PreliminaryEnrollmentStudentGen.ENTITY_CLASS, "ps", new String[]{
                PreliminaryEnrollmentStudentGen.L_REQUESTED_ENROLLMENT_DIRECTION + "." + RequestedEnrollmentDirectionGen.L_ENTRANT_REQUEST + "." + EntrantRequestGen.L_ENTRANT + ".id"
        });
        builder.setNeedDistinct(true);
        builder.add(MQExpression.eq("ps", PreliminaryEnrollmentStudentGen.L_REQUESTED_ENROLLMENT_DIRECTION + "." + RequestedEnrollmentDirectionGen.L_ENTRANT_REQUEST + "." + EntrantRequestGen.L_ENTRANT + "." + EntrantGen.P_ARCHIVAL, Boolean.FALSE));
        builder.add(MQExpression.eq("ps", PreliminaryEnrollmentStudentGen.L_REQUESTED_ENROLLMENT_DIRECTION + "." + RequestedEnrollmentDirectionGen.L_ENTRANT_REQUEST + "." + EntrantRequestGen.L_ENTRANT + "." + EntrantGen.L_ENROLLMENT_CAMPAIGN, q.getDistribution().getCompetitionGroup().getEnrollmentCampaign()));
        builder.add(MQExpression.eq("ps", PreliminaryEnrollmentStudentGen.L_STUDENT_CATEGORY + ".code", UniDefines.STUDENT_CATEGORY_STUDENT));
        builder.add(MQExpression.eq("ps", PreliminaryEnrollmentStudentGen.L_COMPENSATION_TYPE, compensationType));
        builder.add(MQExpression.eq("ps", PreliminaryEnrollmentStudentGen.L_EDUCATION_ORG_UNIT, q.getDirection().getEducationOrgUnit()));

        plan = plan - (int) builder.getResultCount(this.getSession());
        return plan;
    }

}
