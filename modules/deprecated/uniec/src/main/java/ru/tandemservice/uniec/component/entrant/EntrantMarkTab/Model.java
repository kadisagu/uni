/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.EntrantMarkTab;

import org.apache.tapestry.form.validator.Validator;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IIdentifiableWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

import java.util.*;

/**
 * @author vip_delete
 * @since 23.03.2009
 */
@State({@Bind(key = "entrantId", binding = "entrant.id")})
public class Model
{
    public static final String P_MARK_SUM = "markSum";
    public static final String P_EXAM_COUNT = "examCount";
    public static final String P_PASS_COUNT = "passCount";
    public static final String COLUMN_PASS_OR_NOT = "passOrNot";
    public static final long PASS_INTERVIEW_CODE = 0;
    public static final long FAIL_INTERVIEW_CODE = 1;

    private Entrant _entrant = new Entrant();
    private boolean _formingForEntrant;
    private List<EntrantRequest> _entrantRequestList;
    private EntrantRequest _entrantRequest;
    private List _passOrNotList;
    private Map<Long, IIdentifiableWrapper> _interviewBlockColumnMap = new HashMap<>();
    private boolean _hasUnusedOlympiadDiploma;

    // для случая: экзам лист создается на абитуриента
    private boolean _hasDirectionList;
    private boolean _hasDirectionInterviewList;
    private boolean _hasMarkList;
    private DynamicListDataSource<RequestedEnrollmentDirection> _requestedEnrollmentDirectionDataSource;
    private DynamicListDataSource<RequestedEnrollmentDirection> _requestedEnrollmentDirectionInterviewDataSource;
    private DynamicListDataSource<DisciplineMarkWrapper> _markResultDataSource;

    // для случая: экзам лист создается на каждое заявление
    private Set<EntrantRequest> _hasDirectionSet = new HashSet<>();
    private Set<EntrantRequest> _hasDirectionInterviewSet = new HashSet<>();
    private Set<EntrantRequest> _hasMarkSet = new HashSet<>();
    private Map<EntrantRequest, DynamicListDataSource<RequestedEnrollmentDirection>> _requestedEnrollmentDirectionDataSourceMap = new HashMap<>();
    private Map<EntrantRequest, DynamicListDataSource<RequestedEnrollmentDirection>> _requestedEnrollmentDirectionInterviewDataSourceMap = new HashMap<>();
    private Map<EntrantRequest, DynamicListDataSource<DisciplineMarkWrapper>> _markResultDataSourceMap = new HashMap<>();

    // метод очистки модели
    public void clear()
    {
        _interviewBlockColumnMap.clear();
        _hasDirectionSet.clear();
        _hasDirectionInterviewSet.clear();
        _hasMarkSet.clear();
        _requestedEnrollmentDirectionDataSourceMap.clear();
        _requestedEnrollmentDirectionInterviewDataSourceMap.clear();
        _markResultDataSourceMap.clear();
    }

    // методы получения текущего состояния
    public DynamicListDataSource<RequestedEnrollmentDirection> getDirectionDataSource()
    {
        if (_formingForEntrant)
            return _requestedEnrollmentDirectionDataSource;
        return _requestedEnrollmentDirectionDataSourceMap.get(_entrantRequest);
    }

    public void setDirectionDataSource(DynamicListDataSource<RequestedEnrollmentDirection> dataSource)
    {
        if (_formingForEntrant)
            _requestedEnrollmentDirectionDataSource = dataSource;
        else
            _requestedEnrollmentDirectionDataSourceMap.put(_entrantRequest, dataSource);
    }

    public DynamicListDataSource<RequestedEnrollmentDirection> getDirectionInterviewDataSource()
    {
        if (_formingForEntrant)
            return _requestedEnrollmentDirectionInterviewDataSource;
        return _requestedEnrollmentDirectionInterviewDataSourceMap.get(_entrantRequest);
    }

    public void setDirectionInterviewDataSource(DynamicListDataSource<RequestedEnrollmentDirection> dataSource)
    {
        if (_formingForEntrant)
            _requestedEnrollmentDirectionInterviewDataSource = dataSource;
        else
            _requestedEnrollmentDirectionInterviewDataSourceMap.put(_entrantRequest, dataSource);
    }

    public DynamicListDataSource<DisciplineMarkWrapper> getMarkDataSource()
    {
        if (_formingForEntrant)
            return _markResultDataSource;
        return _markResultDataSourceMap.get(_entrantRequest);
    }

    public void setMarkDataSource(DynamicListDataSource<DisciplineMarkWrapper> dataSource)
    {
        if (_formingForEntrant)
            _markResultDataSource = dataSource;
        else
            _markResultDataSourceMap.put(_entrantRequest, dataSource);
    }

    public boolean isHasDirectionBlock()
    {
        if (_formingForEntrant)
            return _hasDirectionList;
        return _hasDirectionSet.contains(_entrantRequest);
    }

    public boolean isHasDirectionInterviewBlock()
    {
        if (_formingForEntrant)
            return _hasDirectionInterviewList;
        return _hasDirectionInterviewSet.contains(_entrantRequest);
    }

    public boolean isHasMarkBlock()
    {
        if (_formingForEntrant)
            return _hasMarkList;
        return _hasMarkSet.contains(_entrantRequest);
    }

    // validators

    public Validator getMarkValidator()
    {
        DisciplineMarkWrapper currentItem = getMarkDataSource().getCurrentEntity();
        return new EntrantMarkValidator(false, currentItem.getMinMark(), currentItem.getMaxMark());
    }

    public Validator getMarkAppealValidator()
    {
        DisciplineMarkWrapper currentItem = getMarkDataSource().getCurrentEntity();
        return new EntrantMarkValidator(true, currentItem.getMinMark(), currentItem.getMaxMark());
    }

    public boolean isAccessible()
    {
        return !getEntrant().isArchival();
    }

    // Getters & Setters

    public Entrant getEntrant()
    {
        return _entrant;
    }

    public void setEntrant(Entrant entrant)
    {
        _entrant = entrant;
    }

    public boolean isFormingForEntrant()
    {
        return _formingForEntrant;
    }

    public void setFormingForEntrant(boolean formingForEntrant)
    {
        _formingForEntrant = formingForEntrant;
    }

    public List<EntrantRequest> getEntrantRequestList()
    {
        return _entrantRequestList;
    }

    public void setEntrantRequestList(List<EntrantRequest> entrantRequestList)
    {
        _entrantRequestList = entrantRequestList;
    }

    public EntrantRequest getEntrantRequest()
    {
        return _entrantRequest;
    }

    public void setEntrantRequest(EntrantRequest entrantRequest)
    {
        _entrantRequest = entrantRequest;
    }

    public List getPassOrNotList()
    {
        return _passOrNotList;
    }

    public void setPassOrNotList(List passOrNotList)
    {
        _passOrNotList = passOrNotList;
    }

    public Map<Long, IIdentifiableWrapper> getInterviewBlockColumnMap()
    {
        return _interviewBlockColumnMap;
    }

    public void setInterviewBlockColumnMap(Map<Long, IIdentifiableWrapper> interviewBlockColumnMap)
    {
        _interviewBlockColumnMap = interviewBlockColumnMap;
    }

    public boolean isHasUnusedOlympiadDiploma()
    {
        return _hasUnusedOlympiadDiploma;
    }

    public void setHasUnusedOlympiadDiploma(boolean hasUnusedOlympiadDiploma)
    {
        _hasUnusedOlympiadDiploma = hasUnusedOlympiadDiploma;
    }

    public boolean isHasDirectionList()
    {
        return _hasDirectionList;
    }

    public void setHasDirectionList(boolean hasDirectionList)
    {
        _hasDirectionList = hasDirectionList;
    }

    public boolean isHasDirectionInterviewList()
    {
        return _hasDirectionInterviewList;
    }

    public void setHasDirectionInterviewList(boolean hasDirectionInterviewList)
    {
        _hasDirectionInterviewList = hasDirectionInterviewList;
    }

    public boolean isHasMarkList()
    {
        return _hasMarkList;
    }

    public void setHasMarkList(boolean hasMarkList)
    {
        _hasMarkList = hasMarkList;
    }

    public DynamicListDataSource<RequestedEnrollmentDirection> getRequestedEnrollmentDirectionDataSource()
    {
        return _requestedEnrollmentDirectionDataSource;
    }

    public void setRequestedEnrollmentDirectionDataSource(DynamicListDataSource<RequestedEnrollmentDirection> requestedEnrollmentDirectionDataSource)
    {
        _requestedEnrollmentDirectionDataSource = requestedEnrollmentDirectionDataSource;
    }

    public DynamicListDataSource<RequestedEnrollmentDirection> getRequestedEnrollmentDirectionInterviewDataSource()
    {
        return _requestedEnrollmentDirectionInterviewDataSource;
    }

    public void setRequestedEnrollmentDirectionInterviewDataSource(DynamicListDataSource<RequestedEnrollmentDirection> requestedEnrollmentDirectionInterviewDataSource)
    {
        _requestedEnrollmentDirectionInterviewDataSource = requestedEnrollmentDirectionInterviewDataSource;
    }

    public DynamicListDataSource<DisciplineMarkWrapper> getMarkResultDataSource()
    {
        return _markResultDataSource;
    }

    public void setMarkResultDataSource(DynamicListDataSource<DisciplineMarkWrapper> markResultDataSource)
    {
        _markResultDataSource = markResultDataSource;
    }

    public Set<EntrantRequest> getHasDirectionSet()
    {
        return _hasDirectionSet;
    }

    public void setHasDirectionSet(Set<EntrantRequest> hasDirectionSet)
    {
        _hasDirectionSet = hasDirectionSet;
    }

    public Set<EntrantRequest> getHasDirectionInterviewSet()
    {
        return _hasDirectionInterviewSet;
    }

    public void setHasDirectionInterviewSet(Set<EntrantRequest> hasDirectionInterviewSet)
    {
        _hasDirectionInterviewSet = hasDirectionInterviewSet;
    }

    public Set<EntrantRequest> getHasMarkSet()
    {
        return _hasMarkSet;
    }

    public void setHasMarkSet(Set<EntrantRequest> hasMarkSet)
    {
        _hasMarkSet = hasMarkSet;
    }

    public Map<EntrantRequest, DynamicListDataSource<RequestedEnrollmentDirection>> getRequestedEnrollmentDirectionDataSourceMap()
    {
        return _requestedEnrollmentDirectionDataSourceMap;
    }

    public void setRequestedEnrollmentDirectionDataSourceMap(Map<EntrantRequest, DynamicListDataSource<RequestedEnrollmentDirection>> requestedEnrollmentDirectionDataSourceMap)
    {
        _requestedEnrollmentDirectionDataSourceMap = requestedEnrollmentDirectionDataSourceMap;
    }

    public Map<EntrantRequest, DynamicListDataSource<RequestedEnrollmentDirection>> getRequestedEnrollmentDirectionInterviewDataSourceMap()
    {
        return _requestedEnrollmentDirectionInterviewDataSourceMap;
    }

    public void setRequestedEnrollmentDirectionInterviewDataSourceMap(Map<EntrantRequest, DynamicListDataSource<RequestedEnrollmentDirection>> requestedEnrollmentDirectionInterviewDataSourceMap)
    {
        _requestedEnrollmentDirectionInterviewDataSourceMap = requestedEnrollmentDirectionInterviewDataSourceMap;
    }

    public Map<EntrantRequest, DynamicListDataSource<DisciplineMarkWrapper>> getMarkResultDataSourceMap()
    {
        return _markResultDataSourceMap;
    }

    public void setMarkResultDataSourceMap(Map<EntrantRequest, DynamicListDataSource<DisciplineMarkWrapper>> markResultDataSourceMap)
    {
        _markResultDataSourceMap = markResultDataSourceMap;
    }
}
