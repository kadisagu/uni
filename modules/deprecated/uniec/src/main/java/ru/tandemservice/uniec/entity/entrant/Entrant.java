package ru.tandemservice.uniec.entity.entrant;

import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uniec.entity.entrant.gen.EntrantGen;

public class Entrant extends EntrantGen
{
    public static final Object P_FULLFIO = new String[]{L_PERSON, Person.P_FULLFIO};

    @Override
    public String getTitle()
    {
        return getFullTitle();
    }

    @Override
    public String getFullTitle()
    {
        return getPerson().getIdentityCard().getFio() + ": " + getContextTitle();
    }

    @Override
    public String getContextTitle()
    {
        return "Абитуриент (прием " + getEnrollmentCampaign().getTitle() + ")";
    }

}