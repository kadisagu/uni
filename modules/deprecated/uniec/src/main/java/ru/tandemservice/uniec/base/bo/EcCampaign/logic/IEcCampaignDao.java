/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcCampaign.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

/**
 * @author Vasily Zhukov
 * @since 15.06.2011
 */
public interface IEcCampaignDao extends INeedPersistenceSupport
{
    /**
     * Генерирует уникальный номер абитуриента в году по шаблону [xx][yyyyy],
     * где [хх] две последние цифры года приемной кампании, а [yyyyy] инкриментируемый счетчик абитуриентов в этом году
     *
     * @param enrollmentCampaign приемная кампания
     * @return новый уникальный номер абитуриента в приемной кампании
     */
    String getUniqueEntrantNumber(EnrollmentCampaign enrollmentCampaign);
}
