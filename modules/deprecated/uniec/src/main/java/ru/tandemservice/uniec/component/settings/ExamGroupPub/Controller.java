/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.settings.ExamGroupPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.formatter.BaseRawFormatter;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.ExamGroupRow;

/**
 * @author vip_delete
 * @since 12.06.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<ExamGroupRow> dataSource = UniBaseUtils.createDataSource(component, getDao());

        final String[] fullFio = new String[]{ExamGroupRow.L_ENTRANT_REQUEST, EntrantRequest.L_ENTRANT, Entrant.L_PERSON, Person.P_FULLFIO};
        final String[] number = new String[]{ExamGroupRow.L_ENTRANT_REQUEST, EntrantRequest.P_REG_NUMBER};

        AbstractColumn<?> fioColumn = new SimpleColumn("ФИО", "", new BaseRawFormatter<ExamGroupRow>()
                {
            @Override
            public String format(ExamGroupRow source)
            {
                String title = (String) source.getProperty(fullFio);
                if (model.getInvalidRowSet().contains(source))
                    return "<span style='color:red'>" + title + "</span>";
                else
                    return title;
            }
                }).setClickable(false).setOrderable(false);
        dataSource.addColumn(fioColumn);
        dataSource.addColumn(new SimpleColumn("Номер заявления", number).setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Исключить абитуриента «{0}» по заявлению «{1}» из этой группы?", fullFio, number));
        model.setDataSource(dataSource);
    }

    public void onClickDelete(IBusinessComponent component)
    {
        getDao().deleteRow(component);

        // группа может стать открытой
        getDao().prepare(getModel(component));
    }
}
