package ru.tandemservice.uniec.base.entity.ecg;

import org.tandemframework.core.view.formatter.DateFormatter;

import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgDistribution;
import ru.tandemservice.uniec.base.entity.ecg.gen.EcgDistributionDetailGen;

/**
 * Уточняющее распределение
 */
public class EcgDistributionDetail extends EcgDistributionDetailGen implements IEcgDistribution
{
    // ITitled

    @Override
    public String getTitle()
    {
        return getDistribution().getTitle() + " [" + DateFormatter.DEFAULT_DATE_FORMATTER.format(getFormingDate()) + "]";
    }

    // IEcgDistribution

    @Override
    public EcgDistributionConfig getConfig()
    {
        return getDistribution().getConfig();
    }

    @Override
    public String getStateTitle()
    {
        if (getState().isApproved() && null != getApprovalDate())
            return getState().getTitle() + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getApprovalDate());

        return getState().getTitle();
    }

}