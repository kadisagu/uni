/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.entrant.ExcludePassFormTab;

import java.util.Collections;
import java.util.List;

import org.tandemframework.core.common.IGrouped;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IdentifiableWrapper;

import ru.tandemservice.uniec.entity.entrant.EntrantRequest;

/**
 * @author Vasily Zhukov
 * @since 26.07.2010
 */
class EntrantRequestGroup extends IdentifiableWrapper<EntrantRequest> implements IGrouped
{
    private static final long serialVersionUID = 1L;
    private List<EnrollmentDirectionGroup> _enrollmentDirectionGroups;

    EntrantRequestGroup(EntrantRequest entrantRequest, List<EnrollmentDirectionGroup> enrollmentDirectionGroups)
    {
        super(entrantRequest.getId(), entrantRequest.getTitle());
        _enrollmentDirectionGroups = enrollmentDirectionGroups;
    }

    @Override
    public List<? extends IGrouped> getGroupList()
    {
        return _enrollmentDirectionGroups;
    }

    @Override
    public List<? extends ITitled> getGroupItemList()
    {
        return Collections.emptyList();
    }

    @Override
    public String getName()
    {
        return getId() + "";
    }
}
