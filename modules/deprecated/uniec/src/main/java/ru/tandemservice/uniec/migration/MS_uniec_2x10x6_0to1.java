package ru.tandemservice.uniec.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniec_2x10x6_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.6")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность usedEnrollmentDocument

        // создано обязательное свойство printEduDocumentAttachmentInfo
        {
            // создать колонку
            tool.createColumn("usedenrollmentdocument_t", new DBColumn("prnteddcmntattchmntinf_p", DBType.BOOLEAN));

            // задать значение по умолчанию
            tool.executeUpdate("update usedenrollmentdocument_t set prnteddcmntattchmntinf_p=? where prnteddcmntattchmntinf_p is null", false);

            // сделать колонку NOT NULL
            tool.setColumnNullable("usedenrollmentdocument_t", "prnteddcmntattchmntinf_p", false);

        }


    }
}