package ru.tandemservice.uniec.entity.report;

import org.tandemframework.core.view.formatter.DateFormatter;

import ru.tandemservice.uni.IStorableReport;
import ru.tandemservice.uniec.entity.report.gen.EnrollmentResultByStageReportGen;

/**
 * Результаты приема по этапам
 */
public class EnrollmentResultByStageReport extends EnrollmentResultByStageReportGen implements IStorableReport
{
    public static final String P_PERIOD_TITLE = "periodTitle";

    public String getPeriodTitle()
    {
        return "с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }

}