package ru.tandemservice.uniec.entity.settings;

import ru.tandemservice.uniec.entity.settings.gen.NotUsedTargetAdmissionKindGen;

/**
 * Неиспользуемый вид целевого приема
 */
public class NotUsedTargetAdmissionKind extends NotUsedTargetAdmissionKindGen
{
}