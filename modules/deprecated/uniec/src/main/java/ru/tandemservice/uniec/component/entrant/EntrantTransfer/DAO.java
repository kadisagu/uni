/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantTransfer;

import java.util.ArrayList;
import java.util.List;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

/**
 * @author agolubenko
 * @since 02.04.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("entrant");
    {
        _orderSettings.setOrders(Entrant.P_FULLFIO, new OrderDescription("identityCard", IdentityCard.P_LAST_NAME), new OrderDescription("identityCard", IdentityCard.P_FIRST_NAME), new OrderDescription("identityCard", IdentityCard.P_MIDDLE_NAME));
    }

    @Override
    public void prepare(Model model)
    {
        model.setEnrollmentDirectionList(getList(EnrollmentDirection.class, model.getEnrollmentDirectionIds()));
        if (model.getEnrollmentDirection() != null && model.getEnrollmentDirection().getId() != null)
            model.setEnrollmentDirection(get(EnrollmentDirection.class, model.getEnrollmentDirection().getId()));
        else
            model.setEnrollmentDirection(null);
        model.setOrderTypeList(EcOrderManager.instance().dao().getOrderTypeList(model.getEnrollmentDirectionList().get(0).getEnrollmentCampaign()));
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "requestedEnrollmentDirection");

        builder.addJoin("requestedEnrollmentDirection", RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "request");
        builder.addJoin("request", EntrantRequest.L_ENTRANT, "entrant");
        builder.addJoin("entrant", Entrant.L_PERSON, "person");
        builder.addJoin("person", Person.L_IDENTITY_CARD, "identityCard");
        builder.addJoin("requestedEnrollmentDirection", RequestedEnrollmentDirection.L_STATE, "state");

        builder.add(MQExpression.eq("entrant", Entrant.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentDirectionList().get(0).getEnrollmentCampaign()));
        builder.add(MQExpression.notIn("state", EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_ACTIVE_CODE, UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));

        // фильтрация
        if (model.getLastNameFilter() != null)
        {
            builder.add(MQExpression.like("identityCard", IdentityCard.P_LAST_NAME, "%" + model.getLastNameFilter()));
        }

        if (model.getFirstNameFilter() != null)
        {
            builder.add(MQExpression.like("identityCard", IdentityCard.P_FIRST_NAME, "%" + model.getFirstNameFilter()));
        }

        if (model.getMiddleNameFilter() != null)
        {
            builder.add(MQExpression.like("identityCard", IdentityCard.P_MIDDLE_NAME, "%" + model.getMiddleNameFilter()));
        }
        builder.add(MQExpression.eq("entrant", Entrant.P_ARCHIVAL, false));

        // сделать уникальную выборку абитуриентов
        // проперти identityCard должны быть тут из-за distinct и order by, иначе ошибка синтаксиса
        builder.getSelectAliasList().clear();
        builder.addSelect("entrant");
        builder.addSelect("identityCard", new Object[] { IdentityCard.P_LAST_NAME, IdentityCard.P_FIRST_NAME, IdentityCard.P_MIDDLE_NAME });
        builder.setNeedDistinct(true);

        DynamicListDataSource<Entrant> dataSource = model.getDataSource();

        _orderSettings.applyOrder(builder, dataSource.getEntityOrder());
        dataSource.setTotalSize(builder.getResultCount(getSession()));

        int startRow = (int) dataSource.getStartRow();
        int countRow = (int) dataSource.getCountRow();

        List<Entrant> result = new ArrayList<Entrant>();
        List<Object[]> list = builder.getResultList(getSession(), startRow, countRow);
        for (Object[] objects : list)
        {
            result.add((Entrant) objects[0]); // достаем абитуриента, identityCard не нужна
        }
        dataSource.createPage(result);
    }
}
