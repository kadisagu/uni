/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.RequestedEnrollmentDirectionAddEdit;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tandemframework.core.common.IIdentifiable;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.matrix.CellCoordinates;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;

import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignModel;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.ProfileKnowledge;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.entity.settings.UseExternalOrgUnitForTA;

/**
 * @author oleyba
 * @since 13.03.2009
 */
@Input({
    @Bind(key = "entrantRequestId", binding = "entrantRequest.id"),
    @Bind(key = "requestedEnrollmentDirectionId", binding = "requestedEnrollmentDirection.id")
})
public class Model implements IEnrollmentCampaignModel
{
    public static final Long MARK_COLUMN_ID = 1L;

    private EntrantRequest _entrantRequest = new EntrantRequest();

    private RequestedEnrollmentDirection _requestedEnrollmentDirection = new RequestedEnrollmentDirection();

    private ISelectModel _formativeOrgUnitModel;
    private ISelectModel _territorialOrgUnitModel;
    private ISelectModel _educationLevelsHighSchoolModel;
    private ISelectModel _developFormModel;
    private ISelectModel _developTechModel;
    private ISelectModel _developConditionModel;
    private ISelectModel _developPeriodModel;

    private OrgUnit _formativeOrgUnit;
    private OrgUnit _territorialOrgUnit;
    private DevelopForm _developForm;
    private DevelopTech _developTech;
    private DevelopCondition _developCondition;
    private DevelopPeriod _developPeriod;
    private EducationLevelsHighSchool _educationLevelsHighSchool;
    private ISelectModel _profileKnowledgeListModel;
    private List<ProfileKnowledge> _profileKnowledgeList;
    private List<StudentCategory> _studentCategoryList;

    private ISelectModel _compensationTypeModel;
    private List<HSelectOption> _targetAdmissionKindList;
    private ISelectModel _competitionKindModel;
    private ISelectModel _externalOrgUnitModel;
    private UseExternalOrgUnitForTA _useExternalOrgUnitForTA;

    private List<Discipline2RealizationWayRelation> _profileDisciplineList;
    private IIdentifiable _markColumn = new IdentifiableWrapper(MARK_COLUMN_ID, "Балл");
    private Map<CellCoordinates, Integer> _matrixData = new HashMap<CellCoordinates, Integer>();

    private boolean _addForm;
    private boolean _editForm;
    private CompensationType _oldCompensationType;
    private CompetitionKind _oldCompetitionKind;
    private boolean _originalHandedIn = false;
    private String _originalPlace;
    private Integer _regNumber;
    private boolean _contractAutoCompetition = false;
    private boolean _stateExamRestriction = false;

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _entrantRequest.getEntrant().getEnrollmentCampaign();
    }

    public boolean isCompetitionKindDisabled()
    {
        return isEditForm() && !getRequestedEnrollmentDirection().getState().getCode().equals(UniecDefines.ENTRANT_STATE_ACTIVE_CODE);
    }

    public boolean isUseExternalOrgUnit()
    {
        return _requestedEnrollmentDirection.isTargetAdmission() &&  _useExternalOrgUnitForTA != null;
    }

    // Getters & Setters

    public EntrantRequest getEntrantRequest()
    {
        return _entrantRequest;
    }

    public void setEntrantRequest(EntrantRequest entrantRequest)
    {
        _entrantRequest = entrantRequest;
    }

    public RequestedEnrollmentDirection getRequestedEnrollmentDirection()
    {
        return _requestedEnrollmentDirection;
    }

    public void setRequestedEnrollmentDirection(RequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        _requestedEnrollmentDirection = requestedEnrollmentDirection;
    }

    public ISelectModel getFormativeOrgUnitModel()
    {
        return _formativeOrgUnitModel;
    }

    public void setFormativeOrgUnitModel(ISelectModel formativeOrgUnitModel)
    {
        _formativeOrgUnitModel = formativeOrgUnitModel;
    }

    public ISelectModel getTerritorialOrgUnitModel()
    {
        return _territorialOrgUnitModel;
    }

    public void setTerritorialOrgUnitModel(ISelectModel territorialOrgUnitModel)
    {
        _territorialOrgUnitModel = territorialOrgUnitModel;
    }

    public ISelectModel getEducationLevelsHighSchoolModel()
    {
        return _educationLevelsHighSchoolModel;
    }

    public void setEducationLevelsHighSchoolModel(ISelectModel educationLevelsHighSchoolModel)
    {
        _educationLevelsHighSchoolModel = educationLevelsHighSchoolModel;
    }

    public ISelectModel getDevelopFormModel()
    {
        return _developFormModel;
    }

    public void setDevelopFormModel(ISelectModel developFormModel)
    {
        _developFormModel = developFormModel;
    }

    public ISelectModel getDevelopTechModel()
    {
        return _developTechModel;
    }

    public void setDevelopTechModel(ISelectModel developTechModel)
    {
        _developTechModel = developTechModel;
    }

    public ISelectModel getDevelopConditionModel()
    {
        return _developConditionModel;
    }

    public void setDevelopConditionModel(ISelectModel developConditionModel)
    {
        _developConditionModel = developConditionModel;
    }

    public ISelectModel getDevelopPeriodModel()
    {
        return _developPeriodModel;
    }

    public void setDevelopPeriodModel(ISelectModel developPeriodModel)
    {
        _developPeriodModel = developPeriodModel;
    }

    @Override
    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        _formativeOrgUnit = formativeOrgUnit;
    }

    @Override
    public OrgUnit getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    public void setTerritorialOrgUnit(OrgUnit territorialOrgUnit)
    {
        _territorialOrgUnit = territorialOrgUnit;
    }

    @Override
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public void setDevelopForm(DevelopForm developForm)
    {
        _developForm = developForm;
    }

    @Override
    public DevelopTech getDevelopTech()
    {
        return _developTech;
    }

    public void setDevelopTech(DevelopTech developTech)
    {
        _developTech = developTech;
    }

    @Override
    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    public void setDevelopCondition(DevelopCondition developCondition)
    {
        _developCondition = developCondition;
    }

    @Override
    public DevelopPeriod getDevelopPeriod()
    {
        return _developPeriod;
    }

    public void setDevelopPeriod(DevelopPeriod developPeriod)
    {
        _developPeriod = developPeriod;
    }

    @Override
    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _educationLevelsHighSchool;
    }

    public void setEducationLevelsHighSchool(EducationLevelsHighSchool educationLevelsHighSchool)
    {
        _educationLevelsHighSchool = educationLevelsHighSchool;
    }

    public ISelectModel getProfileKnowledgeListModel()
    {
        return _profileKnowledgeListModel;
    }

    public void setProfileKnowledgeListModel(ISelectModel profileKnowledgeListModel)
    {
        _profileKnowledgeListModel = profileKnowledgeListModel;
    }

    public List<ProfileKnowledge> getProfileKnowledgeList()
    {
        return _profileKnowledgeList;
    }

    public void setProfileKnowledgeList(List<ProfileKnowledge> profileKnowledgeList)
    {
        _profileKnowledgeList = profileKnowledgeList;
    }

    public List<StudentCategory> getStudentCategoryList()
    {
        return _studentCategoryList;
    }

    public void setStudentCategoryList(List<StudentCategory> studentCategoryList)
    {
        _studentCategoryList = studentCategoryList;
    }

    public ISelectModel getCompensationTypeModel()
    {
        return _compensationTypeModel;
    }

    public void setCompensationTypeModel(ISelectModel compensationTypeModel)
    {
        _compensationTypeModel = compensationTypeModel;
    }

    public List<HSelectOption> getTargetAdmissionKindList()
    {
        return _targetAdmissionKindList;
    }

    public void setTargetAdmissionKindList(List<HSelectOption> targetAdmissionKindList)
    {
        _targetAdmissionKindList = targetAdmissionKindList;
    }

    public ISelectModel getCompetitionKindModel()
    {
        return _competitionKindModel;
    }

    public void setCompetitionKindModel(ISelectModel competitionKindModel)
    {
        _competitionKindModel = competitionKindModel;
    }

    public ISelectModel getExternalOrgUnitModel()
    {
        return _externalOrgUnitModel;
    }

    public void setExternalOrgUnitModel(ISelectModel externalOrgUnitModel)
    {
        _externalOrgUnitModel = externalOrgUnitModel;
    }

    public UseExternalOrgUnitForTA getUseExternalOrgUnitForTA()
    {
        return _useExternalOrgUnitForTA;
    }

    public void setUseExternalOrgUnitForTA(UseExternalOrgUnitForTA useExternalOrgUnitForTA)
    {
        _useExternalOrgUnitForTA = useExternalOrgUnitForTA;
    }

    public List<Discipline2RealizationWayRelation> getProfileDisciplineList()
    {
        return _profileDisciplineList;
    }

    public void setProfileDisciplineList(List<Discipline2RealizationWayRelation> profileDisciplineList)
    {
        _profileDisciplineList = profileDisciplineList;
    }

    public IIdentifiable getMarkColumn()
    {
        return _markColumn;
    }

    public void setMarkColumn(IIdentifiable markColumn)
    {
        _markColumn = markColumn;
    }

    public Map<CellCoordinates, Integer> getMatrixData()
    {
        return _matrixData;
    }

    public void setMatrixData(Map<CellCoordinates, Integer> matrixData)
    {
        _matrixData = matrixData;
    }

    public boolean isAddForm()
    {
        return _addForm;
    }

    public void setAddForm(boolean addForm)
    {
        _addForm = addForm;
    }

    public boolean isEditForm()
    {
        return _editForm;
    }

    public void setEditForm(boolean editForm)
    {
        _editForm = editForm;
    }

    public CompensationType getOldCompensationType()
    {
        return _oldCompensationType;
    }

    public void setOldCompensationType(CompensationType oldCompensationType)
    {
        _oldCompensationType = oldCompensationType;
    }

    public CompetitionKind getOldCompetitionKind()
    {
        return _oldCompetitionKind;
    }

    public void setOldCompetitionKind(CompetitionKind oldCompetitionKind)
    {
        _oldCompetitionKind = oldCompetitionKind;
    }

    public boolean isOriginalHandedIn()
    {
        return _originalHandedIn;
    }

    public void setOriginalHandedIn(boolean originalHandedIn)
    {
        _originalHandedIn = originalHandedIn;
    }

    public String getOriginalPlace()
    {
        return _originalPlace;
    }

    public void setOriginalPlace(String originalPlace)
    {
        _originalPlace = originalPlace;
    }

    public Integer getRegNumber()
    {
        return _regNumber;
    }

    public void setRegNumber(Integer regNumber)
    {
        _regNumber = regNumber;
    }

    public boolean isContractAutoCompetition()
    {
        return _contractAutoCompetition;
    }

    public void setContractAutoCompetition(boolean contractAutoCompetition)
    {
        _contractAutoCompetition = contractAutoCompetition;
    }

    public boolean isStateExamRestriction()
    {
        return _stateExamRestriction;
    }

    public void setStateExamRestriction(boolean stateExamRestriction)
    {
        _stateExamRestriction = stateExamRestriction;
    }
}
