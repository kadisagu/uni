package ru.tandemservice.uniec.entity.settings.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.settings.CompetitionGroupRestriction;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Ограничения по формам освоения и видам возмещения затрат для конкурсных групп
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class CompetitionGroupRestrictionGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.settings.CompetitionGroupRestriction";
    public static final String ENTITY_NAME = "competitionGroupRestriction";
    public static final int VERSION_HASH = 1311635157;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_DEVELOP_FORM = "developForm";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String P_MAX_COMPETITION_GROUP = "maxCompetitionGroup";

    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private DevelopForm _developForm;     // Форма освоения
    private CompensationType _compensationType;     // Вид возмещения затрат
    private Integer _maxCompetitionGroup;     // Максимальное число выбираемых конкурсных групп

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения. Свойство не может быть null.
     */
    public void setDevelopForm(DevelopForm developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Максимальное число выбираемых конкурсных групп.
     */
    public Integer getMaxCompetitionGroup()
    {
        return _maxCompetitionGroup;
    }

    /**
     * @param maxCompetitionGroup Максимальное число выбираемых конкурсных групп.
     */
    public void setMaxCompetitionGroup(Integer maxCompetitionGroup)
    {
        dirty(_maxCompetitionGroup, maxCompetitionGroup);
        _maxCompetitionGroup = maxCompetitionGroup;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof CompetitionGroupRestrictionGen)
        {
            setEnrollmentCampaign(((CompetitionGroupRestriction)another).getEnrollmentCampaign());
            setDevelopForm(((CompetitionGroupRestriction)another).getDevelopForm());
            setCompensationType(((CompetitionGroupRestriction)another).getCompensationType());
            setMaxCompetitionGroup(((CompetitionGroupRestriction)another).getMaxCompetitionGroup());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends CompetitionGroupRestrictionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) CompetitionGroupRestriction.class;
        }

        public T newInstance()
        {
            return (T) new CompetitionGroupRestriction();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "developForm":
                    return obj.getDevelopForm();
                case "compensationType":
                    return obj.getCompensationType();
                case "maxCompetitionGroup":
                    return obj.getMaxCompetitionGroup();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((DevelopForm) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "maxCompetitionGroup":
                    obj.setMaxCompetitionGroup((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "developForm":
                        return true;
                case "compensationType":
                        return true;
                case "maxCompetitionGroup":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "developForm":
                    return true;
                case "compensationType":
                    return true;
                case "maxCompetitionGroup":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "developForm":
                    return DevelopForm.class;
                case "compensationType":
                    return CompensationType.class;
                case "maxCompetitionGroup":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<CompetitionGroupRestriction> _dslPath = new Path<CompetitionGroupRestriction>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "CompetitionGroupRestriction");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.CompetitionGroupRestriction#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.CompetitionGroupRestriction#getDevelopForm()
     */
    public static DevelopForm.Path<DevelopForm> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.CompetitionGroupRestriction#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Максимальное число выбираемых конкурсных групп.
     * @see ru.tandemservice.uniec.entity.settings.CompetitionGroupRestriction#getMaxCompetitionGroup()
     */
    public static PropertyPath<Integer> maxCompetitionGroup()
    {
        return _dslPath.maxCompetitionGroup();
    }

    public static class Path<E extends CompetitionGroupRestriction> extends EntityPath<E>
    {
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private DevelopForm.Path<DevelopForm> _developForm;
        private CompensationType.Path<CompensationType> _compensationType;
        private PropertyPath<Integer> _maxCompetitionGroup;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.CompetitionGroupRestriction#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.CompetitionGroupRestriction#getDevelopForm()
     */
        public DevelopForm.Path<DevelopForm> developForm()
        {
            if(_developForm == null )
                _developForm = new DevelopForm.Path<DevelopForm>(L_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.CompetitionGroupRestriction#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Максимальное число выбираемых конкурсных групп.
     * @see ru.tandemservice.uniec.entity.settings.CompetitionGroupRestriction#getMaxCompetitionGroup()
     */
        public PropertyPath<Integer> maxCompetitionGroup()
        {
            if(_maxCompetitionGroup == null )
                _maxCompetitionGroup = new PropertyPath<Integer>(CompetitionGroupRestrictionGen.P_MAX_COMPETITION_GROUP, this);
            return _maxCompetitionGroup;
        }

        public Class getEntityClass()
        {
            return CompetitionGroupRestriction.class;
        }

        public String getEntityName()
        {
            return "competitionGroupRestriction";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
