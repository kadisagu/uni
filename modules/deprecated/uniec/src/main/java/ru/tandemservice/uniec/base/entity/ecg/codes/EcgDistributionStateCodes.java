package ru.tandemservice.uniec.base.entity.ecg.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Состояние распределения"
 * Имя сущности : ecgDistributionState
 * Файл data.xml : uniec.catalogs.data.xml
 */
public interface EcgDistributionStateCodes
{
    /** Константа кода (code) элемента : Формируется (title) */
    String FORMATIVE = "1";
    /** Константа кода (code) элемента : Зафиксировано (title) */
    String LOCKED = "2";
    /** Константа кода (code) элемента : Утверждено (title) */
    String APPROVED = "3";

    Set<String> CODES = ImmutableSet.of(FORMATIVE, LOCKED, APPROVED);
}
