/* $Id$ */
package ru.tandemservice.uniec.component.report.EnrollmentResultByEGE.Pub;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Vasily Zhukov
 * @since 18.08.2011
 */
public interface IDAO extends IUniDao<Model>
{
}
