/* $Id$ */
package ru.tandemservice.uniec.component.menu.StateExamImport;

import com.google.common.collect.Iterables;
import jxl.Workbook;
import jxl.format.*;
import jxl.write.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec.entity.catalog.StateExamType;
import ru.tandemservice.uniec.entity.catalog.gen.StateExamSubjectGen;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate;
import ru.tandemservice.uniec.entity.entrant.StateExamSubjectMark;

import java.io.*;
import java.lang.Boolean;
import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.*;

/**
 * Иимпорт по ФИО Удостоверению личности и номеру свидетельства
 *
 * @author Andrey Andreev
 * @since 11.07.2016
 */
public class ImportByCertificateWithIcFio2016 extends UniBaseDao implements IImportByCertificate
{
    private static final String SEPARATOR = "%";
    private static final String FILE_CHARSET = "Windows-1251";
    private static final String WITHOUT_CERTIFICATE_NUMBER = "0000000000000";
    private StateExamType firstWave;
    private WritableCellFormat redCell;

    public static final SpringBeanCache<IImportByCertificate> instance = new SpringBeanCache<>(ImportByCertificateWithIcFio2016.class.getSimpleName());

    @Override
    public String getTitle()
    {
        return "По данным удостоверения личности и ФИО (нов.)(ФБС)";
    }

    @Override
    public byte[] doImport(EnrollmentCampaign enrollmentCampaign, byte[] importedContent)
    {
        Map<String, StateExamSubject> subjectMap = getCatalogItemList(StateExamSubject.class).stream()
                .collect(Collectors.toMap(StateExamSubjectGen::getTitle, sbj -> sbj));
        redCell = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 10, WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE, jxl.format.Colour.RED));

        List<Row> raws = readFile(importedContent, subjectMap);
        Map<String, List<Row>> importDataByEntrant = raws.stream()
                .collect(Collectors.groupingBy(
                        row -> getUniqueEntrantKey(row.lastName, row.firstName, row.middleName, row.idCardSeria, row.idCardNumber),
                        Collectors.mapping(row -> row, Collectors.toList())));

        List<Entrant> existEntrants = getEntrants(enrollmentCampaign, importDataByEntrant.keySet());
        Map<String, List<Entrant>> entrantByUniqueKey = existEntrants.stream()
                .collect(Collectors.groupingBy(this::getUniqueEntrantKey, Collectors.mapping(entrant -> entrant, Collectors.toList())));

        List<StateExamMarkWrapper> existMarks = getExistMarks(existEntrants);
        Map<Long, Map<String, Map<String, StateExamMarkWrapper>>> groupedExistMarks = groupMarksBySubjectNumberEntrant(existMarks);
        Map<Long, List<StateExamMarkWrapper>> marksByCertificate = groupMarksByCertificate(existMarks);

        firstWave = get(StateExamType.class, StateExamType.code().s(), UniecDefines.STATE_EXAM_TYPE_FIRST);

        for (Map.Entry<String, List<Row>> entrantData : importDataByEntrant.entrySet())
        {
            String key = entrantData.getKey();
            List<Row> rows = entrantData.getValue();

            List<Entrant> entrants = entrantByUniqueKey.get(key);
            if (CollectionUtils.isEmpty(entrants))
            {
                rows.forEach(row -> row.addCommentWithFire("Абитуриент в базе не найден"));
                continue;
            }

            if (entrants.size() > 1)
                rows.forEach(row -> row.addCommentWithFire("Больше одного абитуриента у одной персоны"));

            entrants.forEach(entrant -> checkEntrantMarks(entrant, rows, groupedExistMarks, marksByCertificate));
        }

        markCheckedCertificates(marksByCertificate);

        return buildReport(raws);
    }

    private List<Row> readFile(byte[] importedContent, Map<String, StateExamSubject> subjectMap)
    {
        try
        {
            BufferedReader in = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(importedContent), FILE_CHARSET));
            return in.lines()
                    .filter(line -> line != null)
                    .sorted(CommonCollator.RUSSIAN_COLLATOR::compare)
                    .map(line -> line.split(SEPARATOR))
                    .map(line -> new Row(line, subjectMap))
                    .collect(Collectors.toList());
        }
        catch (IOException e)
        {
            logger.error(e.getMessage(), e);
            throw new ApplicationException("Формат файла некорректен, загрузка данных невозможна. ", e);
        }
    }

    /**
     * ищет по ключу {@link #getUniqueEntrantKey(String, String, String, String, String) getUniqueEntrantKey}
     *
     * @return список упомянутых в файле абитуриентов
     */
    private List<Entrant> getEntrants(EnrollmentCampaign enrollmentCampaign, Collection<String> uniqueStringKey)
    {
        List<Entrant> entrants = new ArrayList<>();
        for (final List keysPart : Iterables.partition(uniqueStringKey, DQL.MAX_VALUES_ROW_NUMBER))
        {
            DQLCaseExpressionBuilder seria = new DQLCaseExpressionBuilder()
                    .when(isNotNull(property("ic", IdentityCard.seria())), property("ic", IdentityCard.seria()))
                    .otherwise(value(""));

            DQLCaseExpressionBuilder number = new DQLCaseExpressionBuilder()
                    .when(isNotNull(property("ic", IdentityCard.number())), property("ic", IdentityCard.number()))
                    .otherwise(value(""));

            DQLCaseExpressionBuilder middleName = new DQLCaseExpressionBuilder()
                    .when(isNotNull(property("ic", IdentityCard.middleName())), property("ic", IdentityCard.middleName()))
                    .otherwise(value(""));

            DQLSelectBuilder builder = new DQLSelectBuilder()
                    .fromEntity(Entrant.class, "e")
                    .column(property("e"))
                    .joinPath(DQLJoinType.inner, Entrant.person().identityCard().fromAlias("e"), "ic")
                    .where(eq(property("e", Entrant.enrollmentCampaign()), value(enrollmentCampaign)))
                    .where(in(concat(property("ic", IdentityCard.lastName()), value(SEPARATOR),
                                     property("ic", IdentityCard.firstName()), value(SEPARATOR),
                                     middleName.build(), value(SEPARATOR),
                                     seria.build(), value(SEPARATOR), number.build()),
                              keysPart));

            entrants.addAll(this.<Entrant>getList(builder));
        }

        return entrants;
    }

    /**
     * @return список оценок ЕГЭ абитуриентов
     */
    private List<StateExamMarkWrapper> getExistMarks(List<Entrant> entrants)
    {
        List<StateExamSubjectMark> examSubjectMarks = new ArrayList<>();
        for (final List subList : Iterables.partition(entrants, DQL.MAX_VALUES_ROW_NUMBER))
        {
            examSubjectMarks.addAll(
                    new DQLSelectBuilder()
                            .fromEntity(StateExamSubjectMark.class, "sesm")
                            .column(property("sesm"))
                            .where(in(property("sesm", StateExamSubjectMark.certificate().entrant()), subList))
                            .createStatement(getSession()).<StateExamSubjectMark>list()
            );
        }

        return examSubjectMarks.stream().map(StateExamMarkWrapper::new).collect(Collectors.toList());
    }

    /**
     * сгруппированне по абитуриентам, номерам свидетельства и предметам оценки
     *
     * @return map(Entrant id, map[CertificateNumber, map[Subject, StateExamSubjectMark) ) )
     */
    private Map<Long, Map<String, Map<String, StateExamMarkWrapper>>> groupMarksBySubjectNumberEntrant(List<StateExamMarkWrapper> marks)
    {
        return marks.stream().collect(Collectors.groupingBy(
                mark -> mark.entity.getCertificate().getEntrant().getId(),
                Collectors.mapping(mark -> mark, Collectors.groupingBy(
                        mark -> mark.entity.getCertificateNumber() != null ? mark.entity.getCertificateNumber() : WITHOUT_CERTIFICATE_NUMBER,
                        Collectors.mapping(sesm -> sesm, Collectors.toMap(
                                mark -> mark.entity.getSubject().getTitle(), mark -> mark,
                                (mark1, mark2) -> {
                                    String fullFio = mark1.entity.getCertificate().getEntrant().getFullFio();
                                    String certificateNumber = mark1.entity.getCertificateNumber();
                                    String subject = mark1.entity.getSubject().getTitle();
                                    throw new ApplicationException("Невозможно импортировать файл, " +
                                                                           "т.к. у абитуриента " + fullFio +
                                                                           " в базе данных дублируется оценка по предмету «" + subject + "»" +
                                                                           ", номер сертификата " + certificateNumber +
                                                                           ". Удалите дубликаты и повторите процедуру импорта.");
                                }))
                ))
        ));
    }

    /**
     * сруппированные по id сертификата оценки
     *
     * @return map(Certificate id, list[StateExamSubjectMark) )
     */
    private Map<Long, List<StateExamMarkWrapper>> groupMarksByCertificate(List<StateExamMarkWrapper> marks)
    {
        return marks.stream().collect(Collectors.groupingBy(
                mark -> mark.entity.getCertificate().getId(),
                Collectors.mapping(mark -> mark, Collectors.toList())
        ));
    }

    private byte[] buildReport(Collection<Row> inDataByEntrant)
    {
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        WritableWorkbook workbook;
        try
        {
            workbook = Workbook.createWorkbook(out);
            final WritableSheet sheet = workbook.createSheet("Результат импорта", 0);

            WritableCellFormat headerFormat = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 10));
            headerFormat.setWrap(true);

            sheet.setRowView(0, 500);

            int colIndex = 0;
            sheet.setColumnView(colIndex, 16);
            sheet.addCell(new Label(colIndex++, 0, "Фамилия", headerFormat));
            sheet.setColumnView(colIndex, 16);
            sheet.addCell(new Label(colIndex++, 0, "Имя", headerFormat));
            sheet.setColumnView(colIndex, 16);
            sheet.addCell(new Label(colIndex++, 0, "Отчество", headerFormat));
            sheet.setColumnView(colIndex, 10);
            sheet.addCell(new Label(colIndex++, 0, "Серия УЛ", headerFormat));
            sheet.setColumnView(colIndex, 10);
            sheet.addCell(new Label(colIndex++, 0, "Номер УЛ", headerFormat));
            sheet.setColumnView(colIndex, 18);
            sheet.addCell(new Label(colIndex++, 0, "Номер сертификата", headerFormat));
            sheet.setColumnView(colIndex, 16);
            sheet.addCell(new Label(colIndex++, 0, "Предмет", headerFormat));
            sheet.setColumnView(colIndex, 8);
            sheet.addCell(new Label(colIndex++, 0, "Оценка", headerFormat));
            sheet.setColumnView(colIndex, 6);
            sheet.addCell(new Label(colIndex++, 0, "Год", headerFormat));
            sheet.setColumnView(colIndex, 20);
            sheet.addCell(new Label(colIndex++, 0, "Статус", headerFormat));
            sheet.setColumnView(colIndex, 13);
            sheet.addCell(new Label(colIndex++, 0, "Типографский номер", headerFormat));
            sheet.setColumnView(colIndex, 13);
            sheet.addCell(new Label(colIndex++, 0, "Аппеляция/ Перепроверка", headerFormat));
            sheet.setColumnView(colIndex, 7);
            sheet.addCell(new Label(colIndex++, 0, "Регион", headerFormat));
            sheet.setColumnView(colIndex, 20);
            sheet.addCell(new Label(colIndex, 0, "Комментарии", headerFormat));

            int[] rowIndex = {1};
            inDataByEntrant.forEach(row -> addRow(sheet, row, rowIndex[0]++));

            workbook.write();
            workbook.close();
        }
        catch (IOException | WriteException e)
        {
            throw CoreExceptionUtils.getRuntimeException(e);
        }


        return out.toByteArray();
    }

    private void addRow(WritableSheet sheet, Row row, int rowIndex)
    {
        int colIndex = 0;
        try
        {
            WritableCellFormat format;
            if (row.red) format = redCell;
            else format = WritableWorkbook.NORMAL_STYLE;

            String certificateNumber = WITHOUT_CERTIFICATE_NUMBER.equals(row.certificateNumber) ? "" : StateExamSubjectMark.getFormatedCertificateNumber(row.certificateNumber);
            sheet.addCell(new Label(colIndex++, rowIndex, row.lastName, format));
            sheet.addCell(new Label(colIndex++, rowIndex, row.firstName, format));
            sheet.addCell(new Label(colIndex++, rowIndex, row.middleName, format));
            sheet.addCell(new Label(colIndex++, rowIndex, row.idCardSeria, format));
            sheet.addCell(new Label(colIndex++, rowIndex, row.idCardNumber, format));
            sheet.addCell(new Label(colIndex++, rowIndex, certificateNumber, format));
            sheet.addCell(new Label(colIndex++, rowIndex, row.subject.getTitle(), format));
            sheet.addCell(new jxl.write.Number(colIndex++, rowIndex, row.mark, format));
            sheet.addCell(new Label(colIndex++, rowIndex, row.year != null ? String.valueOf(row.year) : "", format));
            sheet.addCell(new Label(colIndex++, rowIndex, row.status.title, format));
            sheet.addCell(new Label(colIndex++, rowIndex, row.printNumber, format));
            sheet.addCell(new Label(colIndex++, rowIndex, row.flag != null ? (row.flag ? "есть" : "нет") : "", format));
            sheet.addCell(new Label(colIndex++, rowIndex, row.region, format));
            sheet.addCell(new Label(colIndex, rowIndex, row.getComments(), format));
        }
        catch (WriteException e)
        {
            throw CoreExceptionUtils.getRuntimeException(e);
        }
    }

    private String getUniqueEntrantKey(String lastName, String firstName, String middleName, String icSeria, String icNumber)
    {
        return (lastName == null ? "" : lastName)
                + SEPARATOR
                + (firstName == null ? "" : firstName)
                + SEPARATOR
                + (middleName == null ? "" : middleName)
                + SEPARATOR
                + (icSeria == null ? "" : icSeria)
                + SEPARATOR
                + (icNumber == null ? "" : icNumber);
    }

    private String getUniqueEntrantKey(Entrant entrant)
    {
        String lastName = entrant.getPerson().getIdentityCard().getLastName();
        String firstName = entrant.getPerson().getIdentityCard().getFirstName();
        String middleName = entrant.getPerson().getIdentityCard().getMiddleName();
        String icSeria = entrant.getPerson().getIdentityCard().getSeria();
        String icNumber = entrant.getPerson().getIdentityCard().getNumber();

        return getUniqueEntrantKey(lastName, firstName, middleName, icSeria, icNumber);
    }

    private void checkEntrantMarks(Entrant entrant, List<Row> rows,
                                   Map<Long, Map<String, Map<String, StateExamMarkWrapper>>> groupedExistMarks,
                                   Map<Long, List<StateExamMarkWrapper>> marksByCertificate)
    {
        Map<String, Map<String, StateExamMarkWrapper>> entrantMarks = groupedExistMarks.get(entrant.getId());
        if (entrantMarks == null)
        {
            entrantMarks = new HashMap<>();
            groupedExistMarks.put(entrant.getId(), entrantMarks);
        }

        for (Row row : rows)
        {
            if (row.wrong) //строки с ошибками не нужны
                continue;

            Map<String, StateExamMarkWrapper> certificateMarks = entrantMarks.get(row.certificateNumber);

            if (!Row.STATUS.ACTIVE.equals(row.status))// недействительные удаляю если найду
            {
                if (certificateMarks != null)
                {
                    StateExamMarkWrapper mark = certificateMarks.get(row.subject.getTitle());
                    if (mark != null)
                    {
                        certificateMarks.remove(row.subject.getTitle());
                        marksByCertificate.get(mark.entity.getCertificate().getId()).remove(mark);
                        row.addComment("Удален");
                        delete(mark.entity);
                    }
                }
                continue;
            }


            if (certificateMarks == null)
            {
                certificateMarks = new HashMap<>();
                entrantMarks.put(row.certificateNumber, certificateMarks);
            }

            StateExamMarkWrapper mark = certificateMarks.get(row.subject.getTitle());
            if (mark == null)
            {
                EntrantStateExamCertificate certificate = certificateMarks.values().isEmpty()
                        ? createNewCert(entrant, row.certificateNumber, firstWave)
                        : certificateMarks.values().iterator().next().entity.getCertificate();
                mark = createNewMark(row, certificate);
                certificateMarks.put(row.subject.getTitle(), mark);
                SafeMap.safeGet(marksByCertificate, mark.entity.getCertificate().getId(), key -> new ArrayList<>()).add(mark);
            }
            else
            {
                if (mark.entity.getMark() != row.mark)
                {
                    int old = mark.entity.getMark();
                    mark.entity.setMark(row.mark);
                    row.addComment("Оценка изменена: " + String.valueOf(old) + "->" + String.valueOf(row.mark));
                }
                if (!Objects.equals(mark.entity.getYear(), row.year))
                {
                    String old = String.valueOf(mark.entity.getYear());
                    mark.entity.setYear(row.year);
                    row.addComment("Год изменен: " + String.valueOf(old) + "->" + String.valueOf(row.year));
                }
            }
            mark.checked = true;
        }
    }

    private EntrantStateExamCertificate createNewCert(Entrant entrant, String number, StateExamType firstWave)
    {
        EntrantStateExamCertificate certificate = new EntrantStateExamCertificate();
        certificate.setEntrant(entrant);
        certificate.setNumber(StringUtils.isEmpty(number) ? WITHOUT_CERTIFICATE_NUMBER : number);
        certificate.setAccepted(false);
        certificate.setSent(false);
        certificate.setRegistrationDate(new Date());
        certificate.setStateExamType(firstWave);
        save(certificate);
        return certificate;
    }

    public StateExamMarkWrapper createNewMark(Row row, EntrantStateExamCertificate certificate)
    {
        StateExamSubjectMark mark = new StateExamSubjectMark();
        mark.setSubject(row.subject);
        mark.setMark(row.mark);
        mark.setCertificateNumber(WITHOUT_CERTIFICATE_NUMBER.equals(row.certificateNumber) ? null : row.certificateNumber);
        mark.setYear(row.year);
        mark.setCertificate(certificate);
        save(mark);
        row.addComment("Добавлен новый балл ЕГЭ");
        return new StateExamMarkWrapper(mark);
    }

    private void markCheckedCertificates(Map<Long, List<StateExamMarkWrapper>> marksByCertificate)
    {
        marksByCertificate.entrySet().forEach(marks -> {
            if (marks.getValue().stream().filter(mark -> !mark.checked).count() == 0) //если непроверенных оценок нет, сертификат помечается как проверенный
            {
                marks.getValue().get(0).entity.getCertificate().setSent(true);
                marks.getValue().get(0).entity.getCertificate().setAccepted(true);
            }
        });
    }

    private static class Row
    {
        enum STATUS
        {
            ACTIVE("Действующий"),
            CANCELLED_WITH_RIGHT_RESIT("Аннулирован с правом пересдачи"),
            CANCELLED_WITHOUT_RIGHT_RESIT("Аннулирован без права пересдачи"),
            INVALID_VIOLATIONS("Недействительный результат по причине нарушения порядка проведения ГИА"),
            OUTDATED("Истек срок"),
            LESS_THAN_MINIMUM("Ниже минимума"),
            NOT_FOUND("Не найдено"),
            EMPTY("");

            final String title;

            STATUS(String title)
            {
                this.title = title;
            }

            public static STATUS getStatus(String status)
            {
                switch (status)
                {
                    case "Действующий":
                        return ACTIVE;
                    case "Аннулирован с правом пересдачи":
                        return CANCELLED_WITH_RIGHT_RESIT;
                    case "Аннулирован без права пересдачи":
                        return CANCELLED_WITHOUT_RIGHT_RESIT;
                    case "Недействительный результат по причине нарушения порядка проведения ГИА":
                        return INVALID_VIOLATIONS;
                    case "Истек срок":
                        return OUTDATED;
                    case "Ниже минимума":
                        return LESS_THAN_MINIMUM;
                    case "Не найдено":
                        return NOT_FOUND;
                    default:
                        return EMPTY;
                }
            }
        }

        String lastName;
        String firstName;
        String middleName;
        String idCardSeria;
        String idCardNumber;

        StateExamSubject subject;
        Integer mark;
        Integer year;

        String region;
        STATUS status;
        Boolean flag;               //признак наличия аппеляции/перепроверки
        String certificateNumber;  //номер свидетельства
        String printNumber;        //типографский номер

        private List<String> comments = new ArrayList<>();
        boolean red = false;
        boolean wrong = false;

        Row(String[] raw, Map<String, StateExamSubject> subjectMap)
        {
            lastName = raw[0];
            firstName = raw[1];
            middleName = raw[2];

            idCardSeria = raw[3];
            if (!StringUtils.isEmpty(idCardSeria))
                idCardSeria = String.format("%4s", idCardSeria).replaceAll(" ", "0");
            idCardNumber = raw[4];
            if (!StringUtils.isEmpty(idCardNumber))
                idCardNumber = String.format("%6s", idCardNumber).replaceAll(" ", "0");

            status = STATUS.getStatus(raw[9]);
            if (!STATUS.ACTIVE.equals(status))
                red = true;

            subject = StringUtils.isEmpty(raw[5]) ? null : subjectMap.get(raw[5]);
            if (subject == null)
                wrong = true;

            mark = StringUtils.isEmpty(raw[6]) ? null : Integer.parseInt(raw[6]);
            if (mark == null)
                wrong = true;


            year = StringUtils.isEmpty(raw[7]) ? null : Integer.parseInt(raw[7]);
            region = raw[8];

            flag = StringUtils.isEmpty(raw[10]) ? null : (raw[10].equals("0") ? Boolean.FALSE : (raw[10].equals("1") ? Boolean.TRUE : null));

            if (StringUtils.isEmpty(raw[11]))
            {
                certificateNumber = WITHOUT_CERTIFICATE_NUMBER;
                addCommentWithFire("Нет номера свидетельства");
            }
            else certificateNumber = raw[11].replace("-", "");

            printNumber = raw[12];
        }

        void addComment(String comment)
        {
            comments.add(comment);
        }

        void addCommentWithFire(String comment)
        {
            comments.add(comment);
            red = true;
        }

        String getComments()
        {
            return comments.stream().collect(Collectors.joining("; "));
        }
    }

    private static class StateExamMarkWrapper
    {
        final StateExamSubjectMark entity;
        boolean checked = false;

        public StateExamMarkWrapper(StateExamSubjectMark stateExamSubjectMark)
        {
            this.entity = stateExamSubjectMark;
        }
    }

}