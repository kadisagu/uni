/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.settings.StateExamSubjectPassScore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec.entity.settings.StateExamSubjectPassScore;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

/**
 * @author oleyba
 * @since 07.07.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        Map<StateExamSubject, StateExamSubjectPassScore> scoreMap = new HashMap<StateExamSubject, StateExamSubjectPassScore>();
        for (StateExamSubjectPassScore score : getList(StateExamSubjectPassScore.class, StateExamSubjectPassScore.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()))
            scoreMap.put(score.getStateExamSubject(), score);
        List<Model.SubjectWrapper> stateExamSubjectList = new ArrayList<Model.SubjectWrapper>();
        for (StateExamSubject subject : getCatalogItemList(StateExamSubject.class))
        {
            StateExamSubjectPassScore score = scoreMap.get(subject);
            stateExamSubjectList.add(new Model.SubjectWrapper(subject, score == null? 0 : score.getMark()));
        }
        DynamicListDataSource<Model.SubjectWrapper> dataSource = model.getDataSource();
        dataSource.setCountRow(stateExamSubjectList.size());
        UniBaseUtils.createPage(dataSource, stateExamSubjectList);
    }

    @Override
    public void update(Model model)
    {
        Map<StateExamSubject, StateExamSubjectPassScore> scoreMap = new HashMap<StateExamSubject, StateExamSubjectPassScore>();
        for (StateExamSubjectPassScore score : getList(StateExamSubjectPassScore.class, StateExamSubjectPassScore.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()))
            scoreMap.put(score.getStateExamSubject(), score);
        for (Model.SubjectWrapper wrapper : model.getDataSource().getEntityList())
        {
            StateExamSubjectPassScore existingScore = scoreMap.get(wrapper.getSubject());
            if (wrapper.getMark() == 0 && existingScore != null)
                delete(existingScore);
            if (wrapper.getMark() > 0 && existingScore != null)
            {
                existingScore.setMark(wrapper.getMark());
                update(existingScore);
            }
            if (wrapper.getMark() > 0 && existingScore == null)
            {
                StateExamSubjectPassScore score = new StateExamSubjectPassScore();
                score.setEnrollmentCampaign(model.getEnrollmentCampaign());
                score.setStateExamSubject(wrapper.getSubject());
                score.setMark(wrapper.getMark());
                save(score);
            }
        }
    }
}
