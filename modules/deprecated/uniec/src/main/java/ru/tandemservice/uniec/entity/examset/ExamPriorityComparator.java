/* $Id$ */
package ru.tandemservice.uniec.entity.examset;

import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline;

import java.util.Comparator;

/**
 * @author Ekaterina Zvereva
 * @since 28.07.2014
 */
public class ExamPriorityComparator extends ExamSetItemComparator
{
    @Override
    public int compare(IExamSetItem o1, IExamSetItem o2)
    {
        if (!(o1 instanceof EntranceDiscipline))
            return super.compare(o1, o2);

        Integer priority1 = ((EntranceDiscipline) o1).getPriority();
        Integer priority2 = ((EntranceDiscipline) o2).getPriority();

        if (priority1 == null && priority2 == null)
        {
            return super.compare(o1, o2);
        }

        if (priority1 != null && priority2 != null)
        {
            return priority1.compareTo(priority2);
        }

        return (priority1 != null ? -1 : 1);
    }
}