/* $Id:$ */
package ru.tandemservice.uniec.base.bo.EcSettings.ui.Template2CampaignRel;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.survey.base.entity.SurveyTemplate;
import ru.tandemservice.uniec.base.bo.EcSettings.logic.Template2CampaignRelDSHandler;
import ru.tandemservice.uniec.entity.catalog.IndividualProgress;
import ru.tandemservice.uniec.entity.survey.QuestionaryEntrant;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

/**
 * @author Andrey Andreev
 * @since 21.03.2017
 */
@Configuration
public class EcSettingsTemplate2CampaignRel extends BusinessComponentManager
{
    public static final String EDUCATION_YEAR_DS = "educationYearDS";
    public static final String SEARCH_LIST_DS = "searchListDS";
    public static final String SURVEY_TEMPLATE_DS = "surveyTemplateDS";

    public static final String TEMPLATE_PARAM = "surveyTemplate";


    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(EDUCATION_YEAR_DS, educationYearDSHandler()))
                .addDataSource(searchListDS(SEARCH_LIST_DS, template2CampaignRelDSColumn(), template2CampaignRelDSHandler()))
                .addDataSource(selectDS(SURVEY_TEMPLATE_DS, templateDSHandler()))
                .create();
    }


    @Bean
    public IBusinessHandler<DSInput, DSOutput> educationYearDSHandler()
    {
        return EducationYear.defaultSelectDSHandler(getName());
    }

    @Bean
    public ColumnListExtPoint template2CampaignRelDSColumn()
    {
        return columnListExtPointBuilder(SEARCH_LIST_DS)
                .addColumn(textColumn("campaign", IndividualProgress.title()))
                .addColumn(blockColumn("template"))
                .addColumn(blockColumn("action", "actionBlock").width("45px").selectCaption("").hasBlockHeader(true))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> template2CampaignRelDSHandler()
    {
        return new Template2CampaignRelDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> templateDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), SurveyTemplate.class)
                .where(SurveyTemplate.objectClass(), QuestionaryEntrant.class)
                .titleProperty(SurveyTemplate.title().s())
                .order(SurveyTemplate.title());
    }
}