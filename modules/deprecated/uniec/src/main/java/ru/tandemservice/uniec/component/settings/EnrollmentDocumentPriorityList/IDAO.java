/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.settings.EnrollmentDocumentPriorityList;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author vip_delete
 * @since 06.06.2009
 */
public interface IDAO extends IUniDao<Model>
{
    void updatePriority(Long documentId, int direction);

    void changeEducationDocumentInfo(Model model, Long documentId);

    void changeAttachmentInfo(Model model, Long documentId);

    void changePrintStateExamCertificateNumber (Model model, Long documentId);

    void changePrintOriginalityInfo(Model model, Long documentId);

    void changeUsed(Model model, Long documentId);
}
