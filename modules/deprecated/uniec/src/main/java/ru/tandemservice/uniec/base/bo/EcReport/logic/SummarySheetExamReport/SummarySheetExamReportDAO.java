/**
 *$Id$
 */
package ru.tandemservice.uniec.base.bo.EcReport.logic.SummarySheetExamReport;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.NumberAsStringComparator;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfElementFactory;
import org.tandemframework.rtf.util.IRtfRowSplitInterceptor;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.CommonMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.CommonSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.catalog.codes.EntrantStateCodes;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.report.SummarySheetExamReport;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantDataUtil;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author Alexander Shaburov
 * @since 18.07.12
 */
public class SummarySheetExamReportDAO extends CommonDAO implements ISummarySheetExamReportDAO
{
    /**
     * Первый ключ - Абитуриент.
     * Второй ключ - список Пар [Название предмета, Оценка].
     * Сортируем в первую очередь по ФИО Абитуриента.
     * Если ФИО равны и равны списки, то элементы равны, иначе сортируем по hashCode списка.
     * (вторая сортировка нужна, т.к. компаратор используется в TreeMap и если сортировать только по фио, то метод get может возвращать не то, что ожидаем)
     */
    @SuppressWarnings("unchecked")
    private Comparator<MultiKey> fioGroupComparator = (o1, o2) -> {
        Entrant entrant1 = (Entrant) o1.getKey(0);
        Entrant entrant2 = (Entrant) o2.getKey(0);
        List<CoreCollectionUtils.Pair<String, Double>> pairList1 = (List<CoreCollectionUtils.Pair<String, Double>>) o1.getKey(1);
        List<CoreCollectionUtils.Pair<String, Double>> pairList2 = (List<CoreCollectionUtils.Pair<String, Double>>) o2.getKey(1);
        int result = CommonCollator.RUSSIAN_COLLATOR.compare(entrant1.getFio(), entrant2.getFio());
        if (result == 0)
            result = pairList1.equals(pairList2) ? 0 : pairList1.hashCode() > pairList2.hashCode() ? 1 : -1;
        return result;
    };

    /**
     * 1. Дата регистрации
     * 2. Приоритет
     */
    private Comparator<RequestedEnrollmentDirection> directionComparator = (o1, o2) -> {
        int result = o1.getRegDate().compareTo(o2.getRegDate());
        if (result == 0)
            result = o1.getPriority() < o2.getPriority() ? -1 : (o1.getPriority() > o2.getPriority() ? 1 : 0);
        return result;
    };

    /**
     * Первый ключ - Направление подготовки (специальность) приема.
     * Второй ключ - Вид возмещения затрат.
     * Сортируем по Названию НПП.
     * Если Названия равны, то сортируем по Названию ВВЗ.
     * (вторая сортировка нужна, т.к. компаратор используется в TreeMap и если сортировать только по фио, то метод get может возвращать не то, что ожидаем)
     */
    private Comparator<MultiKey> titleDirectionComparator = (o1, o2) -> {
        EnrollmentDirection entrant1 = (EnrollmentDirection) o1.getKey(0);
        EnrollmentDirection entrant2 = (EnrollmentDirection) o2.getKey(0);
        CompensationType type1 = (CompensationType) o1.getKey(1);
        CompensationType type2 = (CompensationType) o2.getKey(1);
        int result = entrant1.getTitle().compareTo(entrant2.getTitle());
        if (result == 0)
            result = type1.getTitle().compareTo(type2.getTitle());
        return result;
    };

    /**
     * Сортируем по ФИО Абитуриента этого ВНП.
     */
    private final Comparator<RequestedEnrollmentDirection> fioDirectionComparator = CommonCollator.comparing(o -> o.getEntrantRequest().getEntrant().getFio());

    @Override
    public <M extends SummarySheetExamReportModel> M prepareModel(M model)
    {
        // фильтр Приемная компания
        List<EnrollmentCampaign> ecList = DataAccessServices.dao()
                .getList(new DQLSelectBuilder().fromEntity(EnrollmentCampaign.class, "b").column("b")
                        .order(DQLExpressions.property(EnrollmentCampaign.id().fromAlias("b")), OrderDirection.desc));
        model.setEnrollmentCampaign(ecList.isEmpty() ? null : ecList.get(0));

        // фильтры Заявления с - по
        EnrollmentCampaign enrollmentCampaign = model.getEnrollmentCampaign();
        List<EnrollmentCampaignPeriod> periodList = enrollmentCampaign != null ? enrollmentCampaign.getPeriodList() : new LinkedList<>();
        model.setRequestFrom(periodList.isEmpty() ? null : periodList.get(0).getDateFrom());
        model.setRequestTo(new Date());

        // заполняем модели селектов
        model.setEnrollmentCampaignModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrollmentCampaign.class, "b").column("b");
                builder.where(DQLExpressions.likeUpper(DQLExpressions.property(EnrollmentCampaign.title().fromAlias("b")), DQLExpressions.value(CoreStringUtils.escapeLike(filter))));
                if (o != null)
                    builder.where(DQLExpressions.eqValue(DQLExpressions.property(EnrollmentCampaign.id().fromAlias("b")), o));
                builder.order(DQLExpressions.property(EnrollmentCampaign.id().fromAlias("b")), OrderDirection.desc);

                return new DQLListResultBuilder(builder);
            }
        });
        model.setCompensationTypeModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(CompensationType.class, "b").column("b");
                builder.where(DQLExpressions.likeUpper(DQLExpressions.property(CompensationType.title().fromAlias("b")), DQLExpressions.value(CoreStringUtils.escapeLike(filter))));
                if (o != null)
                    builder.where(DQLExpressions.eqValue(DQLExpressions.property(CompensationType.id().fromAlias("b")), o));
                builder.order(DQLExpressions.property(CompensationType.code().fromAlias("b")));

                return new DQLListResultBuilder(builder);
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((CompensationType) value).getShortTitle();
            }
        });
        model.setStudentCategoryModel(new CommonMultiSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentCategory.class, "b").column("b");
                builder.where(DQLExpressions.likeUpper(DQLExpressions.property(StudentCategory.title().fromAlias("b")), DQLExpressions.value(CoreStringUtils.escapeLike(filter))));
                if (set != null)
                    builder.where(DQLExpressions.in(DQLExpressions.property(StudentCategory.id().fromAlias("b")), set));
                builder.order(DQLExpressions.property(StudentCategory.title().fromAlias("b")));

                return new DQLListResultBuilder(builder);
            }
        });
        model.setQualificationModel(new QualificationModel(getSession()));

        // заполняем модели селекта с помощью утили
        model.setFormativeOrgUnitModel((IMultiSelectModel) MultiEnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(null, model));
        model.setTerritorialOrgUnitModel((IMultiSelectModel) MultiEnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(null, model));
        model.setDevelopFormModel((IMultiSelectModel) MultiEnrollmentDirectionUtil.createDevelopFormModel(model));
        model.setDevelopConditionModel((IMultiSelectModel) MultiEnrollmentDirectionUtil.createDevelopConditionModel(model));
        model.setDevelopTechModel((IMultiSelectModel) MultiEnrollmentDirectionUtil.createDevelopTechModel(model));
        model.setDevelopPeriodModel((IMultiSelectModel) MultiEnrollmentDirectionUtil.createDevelopPeriodModel(model));

        // заполняем по умолчанию пустые селекты
        model.setCompensationType(null);
        model.setStudentCategoryList(new ArrayList<>());
        model.setQualificationList(new ArrayList<>());
        model.setFormativeOrgUnitList(new ArrayList<>());
        model.setTerritorialOrgUnitList(new ArrayList<>());
        model.setDevelopFormList(new ArrayList<>());
        model.setDevelopConditionList(new ArrayList<>());
        model.setDevelopTechList(new ArrayList<>());
        model.setDevelopPeriodList(new ArrayList<>());

        // запллняем активность полей по умолчанию
        model.setCompensTypeActive(false);
        model.setStudentCategoryActive(false);
        model.setQualificationActive(false);
        model.setFormativeOrgUnitActive(false);
        model.setTerritorialOrgUnitActive(false);
        model.setDevelopFormActive(false);
        model.setDevelopConditionActive(false);
        model.setDevelopTechActive(false);
        model.setDevelopPeriodActive(false);

        // определяем обязательность полей для модели утили
        model.setParameters(new MultiEnrollmentDirectionUtil.Parameters());
        model.getParameters().setRequired(false, MultiEnrollmentDirectionUtil.Parameters.Filter.values());

        return model;
    }

    @Override
    public <M extends SummarySheetExamReportModel> M onChangeEnrollmentCampaign(M model)
    {
        // подставляем даты выбранной ПК
        EnrollmentCampaign enrollmentCampaign = model.getEnrollmentCampaign();
        List<EnrollmentCampaignPeriod> periodList = enrollmentCampaign != null ? enrollmentCampaign.getPeriodList() : new LinkedList<>();
        model.setRequestFrom(periodList.isEmpty() ? null : periodList.get(0).getDateFrom());
        model.setRequestTo(periodList.isEmpty() ? null : periodList.get(0).getDateTo());

        return model;
    }

    @Override
    public <M extends SummarySheetExamReportModel> ErrorCollector validate(M model)
    {
        ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();

        if (model.getRequestFrom().getTime() > model.getRequestTo().getTime())
            errorCollector.add("Дата начала должна быть меньше даты окончания.", "requestFrom", "requestTo");

        if (!model.getEnrollmentCampaign().getEntrantExamListsFormingFeature().isFormingForEntrant())
            errorCollector.add("Нельзя сформировать отчет, т.к. он предполагает использование в приемной кампании механизма, при котором ЭЛ формируется для абитуриента один.");

        return errorCollector;
    }

    @Override
    public <T extends SummarySheetExamReportModel> SummarySheetExamReport createReport(T model, DatabaseFile reportFile)
    {
        SummarySheetExamReport report = new SummarySheetExamReport();
        report.setContent(reportFile);
        report.setFormingDate(new Date());
        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setRequestDateFrom(model.getRequestFrom());
        report.setRequestDateTo(model.getRequestTo());
        if (model.isCompensTypeActive())
            report.setCompensationTypeText(model.getCompensationType().getTitle());
        if (model.isStudentCategoryActive())
            report.setStudentCategoryText(UniStringUtils.join(model.getStudentCategoryList(), StudentCategory.title().s(), "; "));
        if (model.isQualificationActive())
            report.setQualificationText(UniStringUtils.join(model.getQualificationList(), Qualifications.title().s(), "; "));
        if (model.isFormativeOrgUnitActive())
            report.setFormativeOrgUnitText(UniStringUtils.join(model.getFormativeOrgUnitList(), OrgUnit.fullTitle().s(), "; "));
        if (model.isTerritorialOrgUnitActive())
            report.setTerritorialOrgUnitText(UniStringUtils.join(model.getTerritorialOrgUnitList(), OrgUnit.territorialFullTitle().s(), "; "));
        if (model.isDevelopFormActive())
            report.setDevelopFormText(UniStringUtils.join(model.getDevelopFormList(), DevelopForm.title().s(), "; "));
        if (model.isDevelopConditionActive())
            report.setDevelopConditionText(UniStringUtils.join(model.getDevelopConditionList(), DevelopCondition.title().s(), "; "));
        if (model.isDevelopTechActive())
            report.setDevelopTechText(UniStringUtils.join(model.getDevelopTechList(), DevelopTech.title().s(), "; "));
        if (model.isDevelopPeriodActive())
            report.setDevelopPeriodText(UniStringUtils.join(model.getDevelopPeriodList(), DevelopPeriod.title().s(), "; "));

        return report;
    }

    @Override
    public <T extends SummarySheetExamReportModel> DatabaseFile createPrintReportFile(final T model)
    {
        prepareReportModel(model);

        RtfDocument document = model.getDocument();
        EntrantDataUtil dataUtil = model.getDataUtil();
        // ищем таблицу в документе
        RtfSearchResult searchResult = UniRtfUtil.findRtfTableMark(document, "T");
        // сохраняем пустой "шаблон" таблицы
        RtfTable emptyTable = (RtfTable) searchResult.getElementList().get(searchResult.getIndex()).getClone();
        List<IRtfElement> elementList = document.getElementList();

        AtomicReference<RtfRowIntercepterBase> rowIntercepter = new AtomicReference<>(new RtfRowIntercepterBase()
        {
            @Override
            public void beforeModify(RtfTable table, int currentRowIndex)
            {
                // на число колонок дисциплин равно кол-ву выбранных ДНВИ во всех ВНП
                int[] parts = new int[model.getDisciplineSet().size()];
                Arrays.fill(parts, 1);
                final Iterator<Discipline2RealizationWayRelation> iterator = model.getDisciplineSet().iterator();

                for (RtfRow row : table.getRowList())
                {
                    if (table.getRowList().indexOf(row) < 2)
                        continue;

                    // интерцептор для именования колонок с дисциплинами
                    IRtfRowSplitInterceptor headerInterceptor = (newCell, index) -> {
                        Discipline2RealizationWayRelation next = iterator.next();
                        IRtfElement text = RtfBean.getElementFactory().createRtfText(next.getShortTitle());
                        newCell.getElementList().add(text);
                    };

                    // разбиваем колонку с дисциплинами
                    RtfUtil.splitRow(row, 3, table.getRowList().indexOf(row) == 2 ? headerInterceptor : null, parts);
                }
            }

            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                // там где это нужно делаем italic
                if (value.contains("_i_"))
                {
                    String s = value.replaceAll("_i_", "");
                    RtfString string = new RtfString();
                    string.append(IRtfData.I).append(s).append(IRtfData.I, 0);
                    return string.toList();
                }
                return null;
            }
        });

        new RtfInjectModifier().put("date", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getRequestTo())).modify(document);
        // если ВНП совсем не нашли, то все ровно нужно заполнить метку tableTitle
        if (model.getDirectionGroupMap().size() == 0 && model.getDirectionWithoutGroupMap().size() == 0)
            new RtfTableModifier().put("tableTitle", (String[][]) null).modify(document);

        // 1. РИСУЕМ ТАБЛИЦЫ С ВНП, У КОТОРЫХ УКАЗАННЫ ГРУППЫ
        Iterator<Map.Entry<String, Map<MultiKey, List<RequestedEnrollmentDirection>>>> dirGroupIterator = model.getDirectionGroupMap().entrySet().iterator();
        while (dirGroupIterator.hasNext())
        {
            Map.Entry<String, Map<MultiKey, List<RequestedEnrollmentDirection>>> tableEntry = dirGroupIterator.next();

            // заполняем номер группы у таблицы
            new RtfInjectModifier().put("tableTitle", tableEntry.getKey()).modify(document);

            List<String[]> table = new LinkedList<>();
            int rowNumber = 1;
            for (Map.Entry<MultiKey, List<RequestedEnrollmentDirection>> lineEntry : tableEntry.getValue().entrySet())
            {
                // достаем подготовленные необходимые данные
                Entrant entrant = (Entrant) lineEntry.getKey().getKey(0);
                RequestedEnrollmentDirection direction = lineEntry.getValue().get(0);
                Map<Discipline2RealizationWayRelation, Double> markMap = dataUtil.getMarkMap(direction);

                String[] line = new String[4 + model.getDisciplineSet().size()];

                line[0] = String.valueOf(rowNumber++);
                line[1] = entrant.getFio();
                line[2] = entrant.getPersonalNumber();
                int i = 1;
                // заполняем колонки дисциплин
                for (Discipline2RealizationWayRelation discipline : model.getDisciplineSet())
                {
                    // узнаем источник оценки
                    Integer markSource = model.getDirectionDicsiplineMarkSourceMap().get(direction).get(discipline);
                    // если это внутренняя форма сдачи
                    boolean italic = !Arrays.asList(UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL, UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL_APPEAL, UniecDefines.FINAL_MARK_STATE_EXAM_WAVE1, UniecDefines.FINAL_MARK_STATE_EXAM_WAVE2).contains(markSource);
                    // то italic
                    line[2 + i] = italic && markSource != null ? "_i_" : "";

                    // если в ВНП не выбрано такой дисциплины, то "x"
                    if (!markMap.containsKey(discipline))
                        line[2 + i++] = "x";
                    else
                    // если выбрано, но оценки нет, то "пусто",
                    // иначе собственно оценка
                        line[2 + i++] += markMap.get(discipline) == -1d ? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(markMap.get(discipline));


                }
                // если среди ВНП у абитуриента есть ВНП в состоянии "выбыл", то помечаем указывая это НПП
                boolean outOfCompetition = false;
                List<RequestedEnrollmentDirection> outOfCompetitionList = new LinkedList<>();
                for (RequestedEnrollmentDirection dir : lineEntry.getValue())
                {
                    if (dir.getState().getCode().equals(UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE))
                    {
                        outOfCompetition = true;
                        outOfCompetitionList.add(dir);
                    }
                }
                Collections.sort(outOfCompetitionList, new EntityComparator<>(new EntityOrder(RequestedEnrollmentDirection.enrollmentDirection().title().s())));
                List<String> outOfCompetitionStringList = new LinkedList<>();
                for (RequestedEnrollmentDirection dir : outOfCompetitionList)
                {
                    StringBuilder s = new StringBuilder(dir.getEnrollmentDirection().getEducationOrgUnit().getShortTitle());
                    if (!dir.getCompensationType().isBudget())
                        s.append(" (д)");
                    outOfCompetitionStringList.add(s.toString());
                }
                if (outOfCompetition)
                    line[2 + i] = "выбыл: " + StringUtils.join(outOfCompetitionStringList, ", ");
                else
                    line[2 + i] = "";

                table.add(line);
            }

            // заменяем метку и регистрируем интерцептор
            new RtfTableModifier().put("T", table.toArray(new String[][]{})).put("T", rowIntercepter.get()).modify(document);

            // если это не последняя таблица
            if (dirGroupIterator.hasNext())
            {
                // то новая страница и в ней новая таблица
                insertPageBreak(elementList);
                elementList.add(emptyTable.getClone());
            }
        }


        // 2. РИСУЕМ ТАБЛИЦЫ С ВНП, У КОТОРЫХ НЕ УКАЗАННЫ ГРУППЫ
        Iterator<Map.Entry<MultiKey, List<RequestedEnrollmentDirection>>> dirWithoutGroupIterator = model.getDirectionWithoutGroupMap().entrySet().iterator();

        // если были ВНП с группой и есть ВНП без группы, то нужно подготовим для них первую таблицу
        if (model.getDirectionGroupMap().size() > 0 && model.getDirectionWithoutGroupMap().size() > 0)
        {
            insertPageBreak(elementList);
            elementList.add(emptyTable.getClone());
        }

        while (dirWithoutGroupIterator.hasNext())
        {
            Map.Entry<MultiKey, List<RequestedEnrollmentDirection>> tableEntry = dirWithoutGroupIterator.next();

            // подготавливаем метку tableTitle у таблицы и заменяем ее
            EnrollmentDirection enrollmentDirection = (EnrollmentDirection) tableEntry.getKey().getKey(0);
            StringBuilder tableTitleBuilder = new StringBuilder(enrollmentDirection.getPrintTitle());
            if (model.isDevelopFormActive() || model.isDevelopConditionActive() || model.isDevelopTechActive() || model.isDevelopPeriodActive())
            {
                tableTitleBuilder.append(" (");
                tableTitleBuilder.append(UniStringUtils.join(model.getDevelopFormList(), DevelopForm.title().s(), ", "));
                if (model.isDevelopConditionActive() || model.isDevelopTechActive() || model.isDevelopPeriodActive())
                    tableTitleBuilder.append(", ");
                tableTitleBuilder.append(UniStringUtils.join(model.getDevelopConditionList(), DevelopForm.title().s(), ", "));
                if (!(model.getDevelopTechList().size() == 1 && model.getDevelopTechList().get(0).getCode().equals(UniDefines.DEVELOP_TECH_SIMPLE)) || model.isDevelopPeriodActive())
                    tableTitleBuilder.append(", ");
                if (!(model.getDevelopTechList().size() == 1 && model.getDevelopTechList().get(0).getCode().equals(UniDefines.DEVELOP_TECH_SIMPLE)))
                    tableTitleBuilder.append(UniStringUtils.join(model.getDevelopTechList(), DevelopForm.title().s(), ", "));
                if (model.isDevelopPeriodActive())
                    tableTitleBuilder.append(", ");
                tableTitleBuilder.append(UniStringUtils.join(model.getDevelopPeriodList(), DevelopForm.title().s(), ", "));
                tableTitleBuilder.append(")");
            }
            tableTitleBuilder.append(" ").append(((CompensationType) tableEntry.getKey().getKey(1)).getTitle());
            RtfString tableTitle = new RtfString();
            tableTitle.append(tableTitleBuilder.toString()).par();
            tableTitle.append(enrollmentDirection.getEducationOrgUnit().getFormativeOrgUnit().getNominativeCaseTitle() != null ? enrollmentDirection.getEducationOrgUnit().getFormativeOrgUnit().getNominativeCaseTitle() : enrollmentDirection.getEducationOrgUnit().getFormativeOrgUnit().getTitle());
            new RtfInjectModifier().put("tableTitle", tableTitle).modify(document);

            int rowNumber = 1;
            List<String[]> table = new LinkedList<>();
            for (RequestedEnrollmentDirection direction : tableEntry.getValue())
            {
                // достаем подготовленные необходимые данные
                Entrant entrant = direction.getEntrantRequest().getEntrant();
                Map<Discipline2RealizationWayRelation, Double> markMap = dataUtil.getMarkMap(direction);

                String[] line = new String[4 + model.getDisciplineSet().size()];

                line[0] = String.valueOf(rowNumber++);
                line[1] = entrant.getFio();
                line[2] = entrant.getPersonalNumber();
                int i = 1;
                // заполняем колонки дисциплин
                for (Discipline2RealizationWayRelation discipline : model.getDisciplineSet())
                {
                    // узнаем источник оценки
                    Integer markSource = model.getDirectionDicsiplineMarkSourceMap().get(direction).get(discipline);
                    // если это внутренняя форма сдачи
                    boolean italic = !Arrays.asList(UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL, UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL_APPEAL, UniecDefines.FINAL_MARK_STATE_EXAM_WAVE1, UniecDefines.FINAL_MARK_STATE_EXAM_WAVE2).contains(markSource);
                    // то italic
                    line[2 + i] = italic && markSource != null ? "_i_" : "";

                    // если в ВНП не выбранной такой дисциплины, то "х"
                    if (!markMap.containsKey(discipline))
                        line[2 + i++] += "x";
                    else
                    // если выбранна, но оценки нет, то "пусто"
                    // иначе оценка
                        line[2 + i++] += markMap.get(discipline) == -1d ? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(markMap.get(discipline));
                }
                // если ВНП в состоянии "выбыл", то помечаем это
                if (direction.getState().getCode().equals(UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE))
                    line[2 + i] = "выбыл";
                else
                    line[2 + i] = "";

                table.add(line);
            }

            // заменяем метку и регистрируем интерцептор
            new RtfTableModifier().put("T", table.toArray(new String[][]{})).put("T", rowIntercepter.get()).modify(document);

            // если это не последняя таблица
            if (dirWithoutGroupIterator.hasNext())
            {
                // то новый лист
                insertPageBreak(elementList);
                // и новая таблица
                elementList.add(emptyTable.getClone());
            }
        }

        DatabaseFile databaseFile = new DatabaseFile();
        databaseFile.setContent(RtfUtil.toByteArray(document));
        databaseFile.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_VND_MS_EXCEL);
        databaseFile.setFilename("Report_" + DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));
        return databaseFile;
    }

    /**
     * Подготавливает данные необходимые для печати отчета.
     * @param model модель
     * @return модель
     */
    protected <M extends SummarySheetExamReportModel> M prepareReportModel(M model)
    {
        // зануляем
        model.setDocument(null);
        model.setDirectionGroupMap(null);
        model.setDirectionWithoutGroupMap(null);
        model.setDataUtil(null);
        model.setDisciplineSet(new HashSet<>());
        model.setDirectionDicsiplineMarkSourceMap(null);

        // шаблон
        byte[] currentTemplate = get(UniecScriptItem.class, UniecScriptItem.CATALOG_ITEM_CODE, UniecDefines.SUMMARY_SHEET_EXAM_REPORT).getCurrentTemplate();
        model.setDocument(new RtfReader().read(currentTemplate));

        // поднимаем ВНП:
        // ВНП соответствует параметрам отчета: виду возмещения затрат, категории поступающего, квалификации, параметрам в утили
        // Состояние ВНП любое кроме «забрал документы»
        // ВНП в заявлении, которое зарегистрировано в промежутке двух указанных при построении отчета дат (включая эти даты)
        // у ВНП заполнено поле "Группа" ИЛИ по которым хотя бы один итоговый балл (в рейтинге) получен во внутренней форме сдачи (это все формы сдачи кроме ЕГЭ и ЕГЭ(вуз))
        final DQLSelectBuilder directionBuilder = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "rd").column("rd");
        directionBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().entrant().archival().fromAlias("rd")), false));
        directionBuilder.where(DQLExpressions.in(DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().fromAlias("rd")), MultiEnrollmentDirectionUtil.getEnrollmentDirections(model, getSession())));
        if (model.isCompensTypeActive())
            directionBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(RequestedEnrollmentDirection.compensationType().id().fromAlias("rd")), model.getCompensationType().getId()));
        if (model.isStudentCategoryActive())
            directionBuilder.where(DQLExpressions.in(DQLExpressions.property(RequestedEnrollmentDirection.studentCategory().id().fromAlias("rd")), ids(model.getStudentCategoryList())));
        if (model.isQualificationActive())
            directionBuilder.where(DQLExpressions.in(DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().id().fromAlias("rd")), ids(model.getQualificationList())));
        directionBuilder.where(DQLExpressions.ne(DQLExpressions.property(RequestedEnrollmentDirection.state().code().fromAlias("rd")), EntrantStateCodes.TAKE_DOCUMENTS_AWAY));
        directionBuilder.where(DQLExpressions.le(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().regDate().fromAlias("rd")), DQLExpressions.valueTimestamp(getRequestTo(model.getRequestTo()))));
        directionBuilder.where(DQLExpressions.ge(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().regDate().fromAlias("rd")), DQLExpressions.valueTimestamp(model.getRequestFrom())));
        IDQLExpression notNullGroup = DQLExpressions.isNotNull(DQLExpressions.property(RequestedEnrollmentDirection.group().fromAlias("rd")));
        IDQLExpression existsInnerMarkSource = DQLExpressions.exists(new DQLSelectBuilder().fromEntity(ChosenEntranceDiscipline.class, "cd").column("cd.id")
                .where(DQLExpressions.eq(DQLExpressions.property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("cd")), DQLExpressions.property(RequestedEnrollmentDirection.id().fromAlias("rd"))))
                .where(DQLExpressions.notIn(DQLExpressions.property(ChosenEntranceDiscipline.finalMarkSource().fromAlias("cd")), new Integer[]{UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL, UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL_APPEAL, UniecDefines.FINAL_MARK_STATE_EXAM_WAVE1, UniecDefines.FINAL_MARK_STATE_EXAM_WAVE2}))
                .buildQuery());
        directionBuilder.where(DQLExpressions.or(notNullGroup, existsInnerMarkSource));

        final List<RequestedEnrollmentDirection> directionList = directionBuilder.createStatement(getSession()).list();

        // подготавливам билдер для Утили
        final MQBuilder directionMqBuilder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "b");
        if (!directionList.isEmpty())
        {
            final List<AbstractExpression> expressionList = new LinkedList<>();
            BatchUtils.execute(directionList, 512, elements -> expressionList.add(MQExpression.in("b", RequestedEnrollmentDirection.id().s(), ids(elements))));
            directionMqBuilder.add(MQExpression.or((AbstractExpression[]) expressionList.toArray(new AbstractExpression[expressionList.size()])));
        }
        else
        {
            directionMqBuilder.add(MQExpression.in("b", RequestedEnrollmentDirection.id().s(), Arrays.asList()));
        }


        EntrantDataUtil dataUtil = new EntrantDataUtil(getSession(), model.getEnrollmentCampaign(), directionMqBuilder);
        model.setDataUtil(dataUtil);

        // список для отсева ВНП без групп
        List<RequestedEnrollmentDirection> directionWithoutGroupList = new LinkedList<>();
        // основные мапы группировки ВНП
        Map<String, Map<MultiKey, List<RequestedEnrollmentDirection>>> directionGroupMap = new TreeMap<>(NumberAsStringComparator.get());
        Map<MultiKey, List<RequestedEnrollmentDirection>> directionWithoutGroupMap = new TreeMap<>(titleDirectionComparator);
        // мапа с Источникам оценок дисциплин абитуриента
        Map<RequestedEnrollmentDirection, Map<Discipline2RealizationWayRelation, Integer>> directionDicsiplineMarkSourceMap = new HashMap<>();

        // группируем все ВНП по Группам, сортировка по названию Группы - это будут таблицы,
        // внутри Групп ВНП группируем по ключу, сортировка по ФИО - [Абитуриент, список ВВИ] - это будут строки в таблицах
        // в строке мб несколько ВНП, поэтому внутри ключа ВНП сортируем по 1. Дате регистрации, 2. Приоритету - первое ВНП будет ключевым в строке, по нему будут браться данные
        for (RequestedEnrollmentDirection direction : directionList)
        {
            String group = direction.getGroup();
            Set<ChosenEntranceDiscipline> disciplineSet = dataUtil.getChosenEntranceDisciplineSet(direction);
            List<ChosenEntranceDiscipline> disciplineList = Arrays.asList(disciplineSet.toArray(new ChosenEntranceDiscipline[disciplineSet.size()]));

            // заполняем мапу с данными по Источникам оценок дисциплин абитуриента
            Map<Discipline2RealizationWayRelation, Integer> disciplineMarkSourceMap = directionDicsiplineMarkSourceMap.get(direction);
            if (disciplineMarkSourceMap == null)
                directionDicsiplineMarkSourceMap.put(direction, disciplineMarkSourceMap = new HashMap<>());
            for (ChosenEntranceDiscipline discipline : disciplineList)
            {
                model.getDisciplineSet().add(discipline.getEnrollmentCampaignDiscipline());
                disciplineMarkSourceMap.put(discipline.getEnrollmentCampaignDiscipline(), discipline.getFinalMarkSource());
            }

            // если группа не указана, то отсеиваем ее, обработаем позже
            if (group == null)
            {
                directionWithoutGroupList.add(direction);
                continue;
            }

            Collections.sort(disciplineList, ITitled.TITLED_COMPARATOR);
            List<CoreCollectionUtils.Pair<String, Double>> pairList = new LinkedList<>();
            for (ChosenEntranceDiscipline discipline : disciplineList)
                pairList.add(new CoreCollectionUtils.Pair<>(discipline.getTitle(), discipline.getFinalMark()));
            Entrant entrant = direction.getEntrantRequest().getEntrant();// абитуриент
            MultiKey multiKey = new MultiKey(entrant, pairList);// ключ

            // группировка по группе
            Map<MultiKey, List<RequestedEnrollmentDirection>> groupDirectionMap = directionGroupMap.get(group);
            if (groupDirectionMap == null)
                directionGroupMap.put(group, groupDirectionMap = new TreeMap<>(fioGroupComparator));

            // по ключу
            List<RequestedEnrollmentDirection> list = groupDirectionMap.get(multiKey);
            if (list == null)
                groupDirectionMap.put(multiKey, list = new LinkedList<>());
            list.add(direction);
            // сортировка внутри ключа
            Collections.sort(list, directionComparator);
        }
        model.setDirectionGroupMap(directionGroupMap);
        model.setDirectionDicsiplineMarkSourceMap(directionDicsiplineMarkSourceMap);

        // группируем ВНП без группы, по ключу - [НПП, Вид возмещения затрат], сортировка по ФИО - это таблицы
        // ВНП в группе - это строки
        // в группе должны быть уникальные ВНП
        for (RequestedEnrollmentDirection direction : directionWithoutGroupList)
        {
            MultiKey multiKey = new MultiKey(direction.getEnrollmentDirection(), direction.getCompensationType());
            List<RequestedEnrollmentDirection> list = directionWithoutGroupMap.get(multiKey);
            if (list == null)
                directionWithoutGroupMap.put(multiKey, list = new LinkedList<>());
            list.add(direction);
            Collections.sort(list, fioDirectionComparator);
        }
        model.setDirectionWithoutGroupMap(directionWithoutGroupMap);

        return model;
    }

    private Date getRequestTo(Date date)
    {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTime();
    }

    /**
     * Вставляет разрыв страницы в документ.
     * @param elementList список элементов в документе
     */
    private void insertPageBreak(List<IRtfElement> elementList)
    {
        IRtfElementFactory elementFactory = RtfBean.getElementFactory();

        elementList.add(elementFactory.createRtfControl(IRtfData.PARD));
        elementList.add(elementFactory.createRtfControl(IRtfData.PAR));

        elementList.add(elementFactory.createRtfControl(IRtfData.PAGE));

        elementList.add(elementFactory.createRtfControl(IRtfData.PARD));
        elementList.add(elementFactory.createRtfControl(IRtfData.PAR));
    }
}
