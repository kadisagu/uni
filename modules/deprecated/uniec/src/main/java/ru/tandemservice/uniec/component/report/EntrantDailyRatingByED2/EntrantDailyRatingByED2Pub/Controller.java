/* $Id$ */
package ru.tandemservice.uniec.component.report.EntrantDailyRatingByED2.EntrantDailyRatingByED2Pub;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import ru.tandemservice.uni.base.bo.UniReport.util.UniReportUtils;

/**
 * @author Andrey Andreev
 * @since 16.06.2016
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickPrint(IBusinessComponent component)
    {
        BusinessComponentUtils.downloadDocument(UniReportUtils.createRenderer(getModel(component).getReport().getId()), true);
    }
}
