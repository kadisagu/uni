/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.util;

import java.util.Map;

/**
 * Занятые места по направлениям приема и видам цп
 *
 * @author Vasily Zhukov
 * @since 13.07.2011
 */
public interface IEcgQuotaUsedDTO
{
    /**
     * @return направление приема -> занято мест (всегда не null)
     */
    Map<Long, Integer> getUsedMap();

    /**
     * @return направление приема -> вид целевого приема -> занято мест (всегда не null)
     */
    Map<Long, Map<Long, Integer>> getTaUsedMap();
}
