/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.ExamAdmissionByCompetitionGroup.ExamAdmissionByCompetitionGroupAdd;

import java.util.List;

import org.tandemframework.core.settings.IDataSettings;

import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.CompetitionGroup;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.report.ExamAdmissionByCGReport;

/**
 * @author ekachanova
 */
public class Model implements IEnrollmentCampaignSelectModel
{
    private ExamAdmissionByCGReport _report = new ExamAdmissionByCGReport();

    private List<EnrollmentCampaign> _enrollmentCampaignList;
    private List<CompetitionGroup> _competitionGroupList;
    private List<CompensationType> _compensTypeList;
    private CompetitionGroup _competitionGroup;

    private String _competitionGroupFieldName;

    public ExamAdmissionByCGReport getReport()
    {
        return _report;
    }

    public void setReport(ExamAdmissionByCGReport report)
    {
        _report = report;
    }

    public List<CompensationType> getCompensTypeList()
    {
        return _compensTypeList;
    }

    public void setCompensTypeList(List<CompensationType> compensTypeList)
    {
        _compensTypeList = compensTypeList;
    }

    public List<CompetitionGroup> getCompetitionGroupList()
    {
        return _competitionGroupList;
    }

    public void setCompetitionGroupList(List<CompetitionGroup> competitionGroupList)
    {
        _competitionGroupList = competitionGroupList;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    @Override
    public IDataSettings getSettings()
    {
        return null;
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return getReport().getEnrollmentCampaign();
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        getReport().setEnrollmentCampaign(enrollmentCampaign);
    }

    public String getCompetitionGroupFieldName()
    {
        return _competitionGroupFieldName;
    }

    public void setCompetitionGroupFieldName(String competitionGroupFieldName)
    {
        _competitionGroupFieldName = competitionGroupFieldName;
    }

    public CompetitionGroup getCompetitionGroup()
    {
        return _competitionGroup;
    }

    public void setCompetitionGroup(CompetitionGroup competitionGroup)
    {
        _competitionGroup = competitionGroup;
    }
}
