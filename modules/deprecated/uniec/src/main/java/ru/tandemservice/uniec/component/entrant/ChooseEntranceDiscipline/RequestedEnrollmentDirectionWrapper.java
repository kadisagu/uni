/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.ChooseEntranceDiscipline;

import java.util.ArrayList;
import java.util.List;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;

import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.examset.ExamSet;
import ru.tandemservice.uniec.entity.examset.ExamSetItem;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.entity.settings.DisciplinesGroup;
import ru.tandemservice.uniec.entity.settings.SetDiscipline;
import ru.tandemservice.uniec.util.ExamSetUtil;

/**
 * @author vip_delete
 * @since 18.03.2009
 */
class RequestedEnrollmentDirectionWrapper extends IdentifiableWrapper
{
    private static final long serialVersionUID = 2086698352318528085L;
    private ExamSet _examSet;
    private RequestedEnrollmentDirection _direction;
    private StudentCategory _studentCategory;
    private List<ExamSetItemWrapper> _examSetItemWrapperList = new ArrayList<ExamSetItemWrapper>();
    private ExamSetItemWrapper _currentExamSetItemWrapper;

    RequestedEnrollmentDirectionWrapper(RequestedEnrollmentDirection direction)
    {
        super(direction.getId(), direction.getTitle());
        _direction = direction;
        _studentCategory = direction.getEnrollmentDirection().getEnrollmentCampaign().isExamSetDiff() ? direction.getStudentCategory() : UniDaoFacade.getCoreDao().getCatalogItem(StudentCategory.class, UniDefines.STUDENT_CATEGORY_STUDENT);
        _examSet = ExamSetUtil.getExamSet(direction.getEnrollmentDirection(), _studentCategory);
        if (!direction.isWithoutEntranceDisciplines())
            for (ExamSetItem examSetItem : _examSet.getSetItemList())
            {
                if (UniDefines.COMPENSATION_TYPE_BUDGET.equals(_direction.getCompensationType().getCode()) && examSetItem.isBudget() ||
                        UniDefines.COMPENSATION_TYPE_CONTRACT.equals(_direction.getCompensationType().getCode()) && examSetItem.isContract())
                    _examSetItemWrapperList.add(new ExamSetItemWrapper(examSetItem));
            }
    }

    public RequestedEnrollmentDirection getDirection()
    {
        return _direction;
    }

    public List<ExamSetItemWrapper> getExamSetItemWrapperList()
    {
        return _examSetItemWrapperList;
    }

    public List<Discipline2RealizationWayRelation> getSelectedDisciplineList()
    {
        ExamSet examSet = ExamSetUtil.getExamSet(_direction.getEnrollmentDirection(), _studentCategory);
        if (!examSet.getExamSetId().equals(_examSet.getExamSetId()))
            throw new ApplicationException("Набор экзаменов для выбранного направления приема был изменен.");

        List<Discipline2RealizationWayRelation> list = new ArrayList<Discipline2RealizationWayRelation>();
        for (ExamSetItemWrapper item : _examSetItemWrapperList)
        {
            if (item.getOptionList().size() == 1)
                list.add((Discipline2RealizationWayRelation) item.getOptionList().get(0).getObject());
            else
                list.add(item.getValue());
        }
        return list;
    }

    // Getters & Setters

    public ExamSetItemWrapper getCurrentExamSetItemWrapper()
    {
        return _currentExamSetItemWrapper;
    }

    public void setCurrentExamSetItemWrapper(ExamSetItemWrapper currentExamSetItemWrapper)
    {
        _currentExamSetItemWrapper = currentExamSetItemWrapper;
    }

    // util class

    static final class ExamSetItemWrapper
    {
        private List<HSelectOption> _optionList = new ArrayList<HSelectOption>();
        private Discipline2RealizationWayRelation _value;

        ExamSetItemWrapper(ExamSetItem examSetItem)
        {
            appendSetDiscipline(examSetItem.getSubject());
            for (SetDiscipline setDiscipline : examSetItem.getChoice())
                appendSetDiscipline(setDiscipline);
        }

        private void appendSetDiscipline(SetDiscipline setDiscipline)
        {
            if (setDiscipline instanceof DisciplinesGroup)
            {
                _optionList.add(new HSelectOption(setDiscipline, 0, false));
                for (Discipline2RealizationWayRelation discipline : setDiscipline.getDisciplines())
                    _optionList.add(new HSelectOption(discipline, 1, true));
            } else
                _optionList.add(new HSelectOption(setDiscipline, 0, true));
        }

        public List<HSelectOption> getOptionList()
        {
            return _optionList;
        }

        // Getters & Setters

        public Discipline2RealizationWayRelation getValue()
        {
            return _value;
        }

        public void setValue(Discipline2RealizationWayRelation value)
        {
            _value = value;
        }
    }
}
