package ru.tandemservice.uniec.component.report.EntrantStudentCard;

import java.util.Date;
import java.util.List;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import org.tandemframework.shared.commonbase.utils.CommonYesNoUIObject;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;

/**
 * @author oleyba
 * @since 04.08.2009
 */
public class Model implements MultiEnrollmentDirectionUtil.Model, IEnrollmentCampaignSelectModel
{
    private byte[] _content;

    private IPrincipalContext _principalContext;

    private MultiEnrollmentDirectionUtil.Parameters _parameters;

    private boolean _qualificationActive;
    private boolean _compensTypeActive;
    private boolean _studentCategoryActive;
    private boolean _formativeOrgUnitActive;
    private boolean _territorialOrgUnitActive;
    private boolean _educationLevelHighSchoolActive;
    private boolean _developFormActive;
    private boolean _developConditionActive;
    private boolean _developTechActive;
    private boolean _developPeriodActive;

    private List<EnrollmentCampaign> _enrollmentCampaignList;
    private ISelectModel _qualificationListModel;
    private List<CompensationType> _compensationTypeList;
    private ISelectModel _studentCategoryListModel;
    private ISelectModel _formativeOrgUnitListModel;
    private ISelectModel _territorialOrgUnitListModel;
    private ISelectModel _educationLevelHighSchoolListModel;
    private ISelectModel _developFormListModel;
    private ISelectModel _developConditionListModel;
    private ISelectModel _developTechListModel;
    private ISelectModel _developPeriodListModel;

    private Date dateFrom;
    private Date dateTo;
    private EnrollmentCampaign enrollmentCampaign;
    private CompensationType compensationType;
    private List<Qualifications> _qualificationList;
    private List<StudentCategory> _studentCategoryList;
    private List<OrgUnit> _formativeOrgUnitList;
    private List<OrgUnit> _territorialOrgUnitList;
    private List<EducationLevelsHighSchool> _educationLevelHighSchoolList;
    private List<DevelopForm> _developFormList;
    private List<DevelopCondition> _developConditionList;
    private List<DevelopTech> _developTechList;
    private List<DevelopPeriod> _developPeriodList;

    private boolean _parallelActive;
    private List<CommonYesNoUIObject> _parallelList;
    private CommonYesNoUIObject _parallel;

    private ISelectModel _customStateRulesModel;
    private DataWrapper _customStateRule;
    private ISelectModel _entrantCustomStateListModel;
    private List<EntrantCustomStateCI> _entrantCustomStateList;

    // IEnrollmentCampaignSelectModel

    @Override
    public IDataSettings getSettings()
    {
        return null;
    }

    @Override
    public List<EnrollmentCampaign> getSelectedEnrollmentCampaignList()
    {
        return MultiEnrollmentDirectionUtil.getSingletonList(getEnrollmentCampaign());
    }

    @Override
    public List<OrgUnit> getSelectedFormativeOrgUnitList()
    {
        return isFormativeOrgUnitActive() ? getFormativeOrgUnitList() : null;
    }

    @Override
    public List<OrgUnit> getSelectedTerritorialOrgUnitList()
    {
        return isTerritorialOrgUnitActive() ? getTerritorialOrgUnitList() : null;
    }

    @Override
    public List<EducationLevelsHighSchool> getSelectedEducationLevelHighSchoolList()
    {
        return isEducationLevelHighSchoolActive() ? getEducationLevelHighSchoolList() : null;
    }

    @Override
    public List<DevelopForm> getSelectedDevelopFormList()
    {
        return isDevelopFormActive() ? getDevelopFormList() : null;
    }

    @Override
    public List<DevelopCondition> getSelectedDevelopConditionList()
    {
        return isDevelopConditionActive() ? getDevelopConditionList() : null;
    }

    @Override
    public List<DevelopTech> getSelectedDevelopTechList()
    {
        return isDevelopTechActive() ? getDevelopTechList() : null;
    }

    @Override
    public List<DevelopPeriod> getSelectedDevelopPeriodList()
    {
        return isDevelopPeriodActive() ? getDevelopPeriodList() : null;
    }

    // Getters & Setters

    public byte[] getContent()
    {
        return _content;
    }

    public void setContent(byte[] content)
    {
        _content = content;
    }

    public IPrincipalContext getPrincipalContext()
    {
        return _principalContext;
    }

    public void setPrincipalContext(IPrincipalContext principalContext)
    {
        _principalContext = principalContext;
    }

    @Override
    public MultiEnrollmentDirectionUtil.Parameters getParameters()
    {
        return _parameters;
    }

    public void setParameters(MultiEnrollmentDirectionUtil.Parameters parameters)
    {
        _parameters = parameters;
    }

    public boolean isQualificationActive()
    {
        return _qualificationActive;
    }

    public void setQualificationActive(boolean qualificationActive)
    {
        _qualificationActive = qualificationActive;
    }

    public boolean isCompensTypeActive()
    {
        return _compensTypeActive;
    }

    public void setCompensTypeActive(boolean compensTypeActive)
    {
        _compensTypeActive = compensTypeActive;
    }

    public boolean isStudentCategoryActive()
    {
        return _studentCategoryActive;
    }

    public void setStudentCategoryActive(boolean studentCategoryActive)
    {
        _studentCategoryActive = studentCategoryActive;
    }

    public boolean isFormativeOrgUnitActive()
    {
        return _formativeOrgUnitActive;
    }

    public void setFormativeOrgUnitActive(boolean formativeOrgUnitActive)
    {
        _formativeOrgUnitActive = formativeOrgUnitActive;
    }

    public boolean isTerritorialOrgUnitActive()
    {
        return _territorialOrgUnitActive;
    }

    public void setTerritorialOrgUnitActive(boolean territorialOrgUnitActive)
    {
        _territorialOrgUnitActive = territorialOrgUnitActive;
    }

    public boolean isEducationLevelHighSchoolActive()
    {
        return _educationLevelHighSchoolActive;
    }

    public void setEducationLevelHighSchoolActive(boolean educationLevelHighSchoolActive)
    {
        _educationLevelHighSchoolActive = educationLevelHighSchoolActive;
    }

    public boolean isDevelopFormActive()
    {
        return _developFormActive;
    }

    public void setDevelopFormActive(boolean developFormActive)
    {
        _developFormActive = developFormActive;
    }

    public boolean isDevelopConditionActive()
    {
        return _developConditionActive;
    }

    public void setDevelopConditionActive(boolean developConditionActive)
    {
        _developConditionActive = developConditionActive;
    }

    public boolean isDevelopTechActive()
    {
        return _developTechActive;
    }

    public void setDevelopTechActive(boolean developTechActive)
    {
        _developTechActive = developTechActive;
    }

    public boolean isDevelopPeriodActive()
    {
        return _developPeriodActive;
    }

    public void setDevelopPeriodActive(boolean developPeriodActive)
    {
        _developPeriodActive = developPeriodActive;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public ISelectModel getQualificationListModel()
    {
        return _qualificationListModel;
    }

    public void setQualificationListModel(ISelectModel qualificationListModel)
    {
        _qualificationListModel = qualificationListModel;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public ISelectModel getStudentCategoryListModel()
    {
        return _studentCategoryListModel;
    }

    public void setStudentCategoryListModel(ISelectModel studentCategoryListModel)
    {
        _studentCategoryListModel = studentCategoryListModel;
    }

    public ISelectModel getFormativeOrgUnitListModel()
    {
        return _formativeOrgUnitListModel;
    }

    public void setFormativeOrgUnitListModel(ISelectModel formativeOrgUnitListModel)
    {
        _formativeOrgUnitListModel = formativeOrgUnitListModel;
    }

    public ISelectModel getTerritorialOrgUnitListModel()
    {
        return _territorialOrgUnitListModel;
    }

    public void setTerritorialOrgUnitListModel(ISelectModel territorialOrgUnitListModel)
    {
        _territorialOrgUnitListModel = territorialOrgUnitListModel;
    }

    public ISelectModel getEducationLevelHighSchoolListModel()
    {
        return _educationLevelHighSchoolListModel;
    }

    public void setEducationLevelHighSchoolListModel(ISelectModel educationLevelHighSchoolListModel)
    {
        _educationLevelHighSchoolListModel = educationLevelHighSchoolListModel;
    }

    public ISelectModel getDevelopFormListModel()
    {
        return _developFormListModel;
    }

    public void setDevelopFormListModel(ISelectModel developFormListModel)
    {
        _developFormListModel = developFormListModel;
    }

    public ISelectModel getDevelopConditionListModel()
    {
        return _developConditionListModel;
    }

    public void setDevelopConditionListModel(ISelectModel developConditionListModel)
    {
        _developConditionListModel = developConditionListModel;
    }

    public ISelectModel getDevelopTechListModel()
    {
        return _developTechListModel;
    }

    public void setDevelopTechListModel(ISelectModel developTechListModel)
    {
        _developTechListModel = developTechListModel;
    }

    public ISelectModel getDevelopPeriodListModel()
    {
        return _developPeriodListModel;
    }

    public void setDevelopPeriodListModel(ISelectModel developPeriodListModel)
    {
        _developPeriodListModel = developPeriodListModel;
    }

    public Date getDateFrom()
    {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom)
    {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo()
    {
        return dateTo;
    }

    public void setDateTo(Date dateTo)
    {
        this.dateTo = dateTo;
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return enrollmentCampaign;
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        this.enrollmentCampaign = enrollmentCampaign;
    }

    public CompensationType getCompensationType()
    {
        return compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        this.compensationType = compensationType;
    }

    public List<Qualifications> getQualificationList()
    {
        return _qualificationList;
    }

    public void setQualificationList(List<Qualifications> qualificationList)
    {
        _qualificationList = qualificationList;
    }

    public List<StudentCategory> getStudentCategoryList()
    {
        return _studentCategoryList;
    }

    public void setStudentCategoryList(List<StudentCategory> studentCategoryList)
    {
        _studentCategoryList = studentCategoryList;
    }

    public List<OrgUnit> getFormativeOrgUnitList()
    {
        return _formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(List<OrgUnit> formativeOrgUnitList)
    {
        _formativeOrgUnitList = formativeOrgUnitList;
    }

    public List<OrgUnit> getTerritorialOrgUnitList()
    {
        return _territorialOrgUnitList;
    }

    public void setTerritorialOrgUnitList(List<OrgUnit> territorialOrgUnitList)
    {
        _territorialOrgUnitList = territorialOrgUnitList;
    }

    public List<EducationLevelsHighSchool> getEducationLevelHighSchoolList()
    {
        return _educationLevelHighSchoolList;
    }

    public void setEducationLevelHighSchoolList(List<EducationLevelsHighSchool> educationLevelHighSchoolList)
    {
        _educationLevelHighSchoolList = educationLevelHighSchoolList;
    }

    public List<DevelopForm> getDevelopFormList()
    {
        return _developFormList;
    }

    public void setDevelopFormList(List<DevelopForm> developFormList)
    {
        _developFormList = developFormList;
    }

    public List<DevelopCondition> getDevelopConditionList()
    {
        return _developConditionList;
    }

    public void setDevelopConditionList(List<DevelopCondition> developConditionList)
    {
        _developConditionList = developConditionList;
    }

    public List<DevelopTech> getDevelopTechList()
    {
        return _developTechList;
    }

    public void setDevelopTechList(List<DevelopTech> developTechList)
    {
        _developTechList = developTechList;
    }

    public List<DevelopPeriod> getDevelopPeriodList()
    {
        return _developPeriodList;
    }

    public void setDevelopPeriodList(List<DevelopPeriod> developPeriodList)
    {
        _developPeriodList = developPeriodList;
    }

    public boolean isParallelActive()
    {
        return _parallelActive;
    }

    public void setParallelActive(boolean parallelActive)
    {
        _parallelActive = parallelActive;
    }

    public List<CommonYesNoUIObject> getParallelList()
    {
        return _parallelList;
    }

    public void setParallelList(List<CommonYesNoUIObject> parallelList)
    {
        _parallelList = parallelList;
    }

    public CommonYesNoUIObject getParallel()
    {
        return _parallel;
    }

    public void setParallel(CommonYesNoUIObject parallel)
    {
        _parallel = parallel;
    }


    public DataWrapper getCustomStateRule()
    {
        return _customStateRule;
    }

    public Boolean getCustomStateRuleValue()
    {
        return TwinComboDataSourceHandler.getSelectedValue(_customStateRule);
    }

    public void setCustomStateRule(DataWrapper customStateRule)
    {
        _customStateRule = customStateRule;
    }

    public ISelectModel getCustomStateRulesModel()
    {
        return _customStateRulesModel;
    }

    public void setCustomStateRulesModel(ISelectModel customStateRulesModel)
    {
        _customStateRulesModel = customStateRulesModel;
    }

    public List<EntrantCustomStateCI> getEntrantCustomStateList()
    {
        return _entrantCustomStateList;
    }

    public void setEntrantCustomStateList(List<EntrantCustomStateCI> entrantCustomStateList)
    {
        _entrantCustomStateList = entrantCustomStateList;
    }

    public ISelectModel getEntrantCustomStateListModel()
    {
        return _entrantCustomStateListModel;
    }

    public void setEntrantCustomStateListModel(ISelectModel entrantCustomStateListModel)
    {
        _entrantCustomStateListModel = entrantCustomStateListModel;
    }

    public boolean getEntrantCustomStateListDisabled()
    {
        return _customStateRule == null;
    }
}
