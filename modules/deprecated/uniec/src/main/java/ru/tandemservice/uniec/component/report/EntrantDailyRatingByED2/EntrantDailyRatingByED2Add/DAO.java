/* $Id$ */
package ru.tandemservice.uniec.component.report.EntrantDailyRatingByED2.EntrantDailyRatingByED2Add;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI;
import ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report;
import ru.tandemservice.uniec.ui.EnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.Date;

/**
 * @author Andrey Andreev
 * @since 16.06.2016
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    @SuppressWarnings("deprecation")
    public void prepare(final Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.getReport().setDateFrom(model.getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : model.getEnrollmentCampaign().getStartDate());
        model.getReport().setDateTo(CoreDateUtils.getDayLastTimeMoment(new Date()));
        model.setTerritorialOrgUnit(TopOrgUnit.getInstance());

        model.setStudentCategoryListModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(StudentCategory.class)));
        model.setFormativeOrgUnitModel(EnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setTerritorialOrgUnitModel(EnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setEducationLevelsHighSchoolModel(EnrollmentDirectionUtil.createEducationLevelsHighSchoolModel(model));
        model.setDevelopFormModel(EnrollmentDirectionUtil.createDevelopFormModel(model));
        model.setDevelopConditionModel(EnrollmentDirectionUtil.createDevelopConditionModel(model));
        model.setDevelopTechModel(EnrollmentDirectionUtil.createDevelopTechModel(model));
        model.setDevelopPeriodModel(EnrollmentDirectionUtil.createDevelopPeriodModel(model));
        model.setCompensationTypes(getCatalogItemList(CompensationType.class));
        model.setQualificationListModel(new QualificationModel(getSession()));
        model.setDevelopFormListModel(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setDevelopConditionListModel(EducationCatalogsManager.getDevelopConditionSelectModel());
        model.setCompetitionKindModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(CompetitionKind.class)));
        model.setCustomStateRulesModel(TwinComboDataSourceHandler.getCustomSelectModel("Содержит", "Не содержит"));
        model.setEntrantCustomStateListModel(new LazySimpleSelectModel<>(EntrantCustomStateCI.class));
    }

    @Override
    public void update(Model model)
    {
        final Session session = getSession();

        EntrantDailyRatingByED2Report report = model.getReport();
        report.setFormingDate(new Date());
        report.setEnrollmentDirection(model.isByAllEnrollmentDirections() ? null : EnrollmentDirectionUtil.getEnrollmentDirection(model, session));
        report.setStudentCategoryTitle(UniStringUtils.join(model.getStudentCategoryList(), StudentCategory.P_TITLE, "; "));

        if (model.isByAllEnrollmentDirections())
        {
            report.setQualificationTitle(UniStringUtils.join(model.getQualificationList(), Qualifications.P_TITLE, "; "));
            report.setDevelopFormTitle(UniStringUtils.join(model.getDevelopFormList(), DevelopForm.P_TITLE, "; "));
            report.setDevelopConditionTitle(UniStringUtils.join(model.getDevelopConditionList(), DevelopCondition.P_TITLE, "; "));
        }

        if (CollectionUtils.isNotEmpty(model.getEntrantCustomStateList()))
            report.setEntrantCustomStateTitle(model.getCustomStateRule().getTitle() + ": "
                                                      + UniStringUtils.join(model.getEntrantCustomStateList(), EntrantCustomStateCI.P_TITLE, ", "));

        DatabaseFile databaseFile = generateContent(model, session);
        session.save(databaseFile);

        report.setContent(databaseFile);
        session.save(report);
    }

    protected DatabaseFile generateContent(Model model, Session session)
    {
        return new EntrantDailyRatingByED2ReportBuilder(model, session).getContent();
    }
}
