package ru.tandemservice.uniec.entity.report;

import ru.tandemservice.uniec.component.settings.ProtocolVisaList.IProtocolEntity;
import ru.tandemservice.uniec.entity.report.gen.OrgUnitSelectionSecretaryGen;

/**
 * Секретарь отборочной комиссии
 */
public class OrgUnitSelectionSecretary extends OrgUnitSelectionSecretaryGen implements IProtocolEntity
{
}