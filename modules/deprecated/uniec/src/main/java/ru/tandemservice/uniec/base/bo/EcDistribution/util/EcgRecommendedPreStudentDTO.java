/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.util;

import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;

/**
 * @author Vasily Zhukov
 * @since 02.08.2011
 */
public class EcgRecommendedPreStudentDTO extends EcgRecommendedDTO implements IEcgRecommendedPreStudentDTO
{
    private static final long serialVersionUID = 1L;

    private PreliminaryEnrollmentStudent _preStudent;

    private String _enrollmentConditions;
    private String _orderNumber;
    private boolean _preEnrollVisible;
    private boolean _changeOrderDisabled;
    private boolean _changeEnrollmentConditionDisabled;

    /**
     * @param entrantRecommended рекомендованный
     * @param finalMark          сумма баллов
     * @param priorities         приоритеты
     * @param documentStatus     состояние документов
     * @param preStudent         предзачисленный студент, либо null, если еще не предзачислен
     * @param orderNumber        номер приказа, в который включен предзачисленный абитуриент (preStudent != null), либо null, если в приказ еще не включен
     */
    public EcgRecommendedPreStudentDTO(IEcgEntrantRecommended entrantRecommended, Double finalMark, String priorities, DocumentStatus documentStatus, PreliminaryEnrollmentStudent preStudent, String orderNumber)
    {
        super(entrantRecommended, finalMark, priorities, documentStatus);
        _preStudent = preStudent;
        _enrollmentConditions = _preStudent == null ? null : preStudent.getEnrollmentConditions();
        _orderNumber = orderNumber;

        // можно предзачислять только рекомендованных с оригиналами
        _preEnrollVisible = DocumentStatus.BRING_ORIGINAL.equals(getDocumentStatus());

        // если уже есть приказ, то нельзя менять тип приказа, по которому будет предзачисление
        _changeOrderDisabled = orderNumber != null;

        // если нет предазачисления, то нельзя менять условия презачисления
        // если есть приказ, то можно менять условия только для предзачисляемых слушателей
        _changeEnrollmentConditionDisabled = preStudent == null || (_changeOrderDisabled && !UniDefines.STUDENT_CATEGORY_LISTENER.equals(preStudent.getStudentCategory().getCode()));
    }

    // Getters

    @Override
    public PreliminaryEnrollmentStudent getPreStudent()
    {
        return _preStudent;
    }

    @Override
    public String getEnrollmentConditions()
    {
        return _enrollmentConditions;
    }

    @Override
    public String getOrderNumber()
    {
        return _orderNumber;
    }

    @Override
    public boolean isNoPreStudent()
    {
        return _preEnrollVisible && !_changeOrderDisabled && _preStudent == null;
    }

    @Override
    public boolean isPreEnrollVisible()
    {
        return _preEnrollVisible;
    }

    @Override
    public boolean isChangeOrderDisabled()
    {
        return _changeOrderDisabled;
    }

    @Override
    public boolean isChangeEnrollmentConditionDisabled()
    {
        return _changeEnrollmentConditionDisabled;
    }
}
