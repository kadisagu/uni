package ru.tandemservice.uniec.entity.entrant;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;

import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniec.entity.entrant.gen.PreliminaryEnrollmentStudentGen;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.unimove.IAbstractExtract;

/**
 * Студент предварительного зачисления
 */
public class PreliminaryEnrollmentStudent extends PreliminaryEnrollmentStudentGen implements ITitled
{
    @Override
    @EntityDSLSupport
    public String getEnrollmentConditions()
    {
        List<String> titleList = new ArrayList<String>();
        titleList.add(getCompensationType().getShortTitle());
        if (isTargetAdmission())
            titleList.add("ЦП");
        if (!UniDefines.STUDENT_CATEGORY_STUDENT.equals(getStudentCategory().getCode()))
            titleList.add(getStudentCategory().getTitle().toLowerCase());
        if (isTransferred())
            titleList.add("переведен");
        if (isStudiedOPP())
            titleList.add("учился в ОПП");
        if (isParallel())
            titleList.add("паралл.");
        if (getContractAdmissionKind() != null)
            titleList.add(getContractAdmissionKind().getShortTitle());
        return StringUtils.join(titleList, ", ");
    }

    public static final String P_TRANSFER_SOURCE = "transferSource";

    public RequestedEnrollmentDirection getTransferSource()
    {
        return (isTransferred()) ? getRequestedEnrollmentDirection() : null;
    }

    private boolean isTransferred()
    {
        return !getEducationOrgUnit().equals(getRequestedEnrollmentDirection().getEnrollmentDirection().getEducationOrgUnit());
    }

    public static final String P_TITLE = "title";

    @Override
    public String getTitle()
    {
        return getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getPerson().getFullFio();
    }

    public static final String VIEW_P_L_ENROLLMENT_ORDER = "enrollmentOrder";

    public EnrollmentOrder getEnrollmentOrder()
    {
        EnrollmentExtract extract = UniDaoFacade.getCoreDao().get(EnrollmentExtract.class, IAbstractExtract.L_ENTITY, this);
        return extract == null ? null : extract.getOrder();
    }
}