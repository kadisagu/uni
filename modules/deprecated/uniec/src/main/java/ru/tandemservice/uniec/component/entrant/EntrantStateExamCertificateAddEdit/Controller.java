/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantStateExamCertificateAddEdit;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import org.tandemframework.shared.person.base.bo.Person.ui.IdentityCardAddEdit.PersonIdentityCardAddEdit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;

/**
 * @author agolubenko
 * @since 02.03.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    public static final String MARK_COLUMN_NAME="mark";
    public static final String NUMBER_COLUMN_NAME="number";
    public static final String YEAR_COLUMN_NAME="year";

    public static final String SEARCHLIST_FIELD_ID_PREFIX="stateExam";

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        prepareListDataSource(component);

        getDao().prepare(model);
    }

    public void prepareListDataSource(IBusinessComponent component){

        Model model = getModel(component);

        DynamicListDataSource<StateExamSubject> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Предмет", StateExamSubject.P_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn(MARK_COLUMN_NAME, "Балл").setClickable(false));
        dataSource.addColumn(new BlockColumn(NUMBER_COLUMN_NAME, "Номер свидетельства").setClickable(false));
        dataSource.addColumn(new BlockColumn(YEAR_COLUMN_NAME, "Год сдачи").setClickable(false));

        model.setDataSource(dataSource);
    }

    public synchronized void onClickApply(IBusinessComponent component)
    {
        getDao().update(getModel(component));

        deactivate(component);
    }

    public synchronized void onClickAddIdentityCard(IBusinessComponent component)
    {
        Person person =  ((Model)component.getModel()).getCertificate().getEntrant().getPerson();
        component.createDialogRegion(new ComponentActivator(PersonIdentityCardAddEdit.class.getSimpleName(),
                                                            ParametersMap.createWith(PublisherActivator.PUBLISHER_ID_KEY, person.getId())));
    }
}
