package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.ExamGroupSet;
import ru.tandemservice.uniec.entity.entrant.ExamGroupSetOpened;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Открытые наборы экзаменационных групп
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ExamGroupSetOpenedGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.ExamGroupSetOpened";
    public static final String ENTITY_NAME = "examGroupSetOpened";
    public static final int VERSION_HASH = 1828394375;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_EXAM_GROUP_SET = "examGroupSet";

    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private ExamGroupSet _examGroupSet;     // Набор экзаменационной группы

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null и должно быть уникальным.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Набор экзаменационной группы. Свойство не может быть null.
     */
    @NotNull
    public ExamGroupSet getExamGroupSet()
    {
        return _examGroupSet;
    }

    /**
     * @param examGroupSet Набор экзаменационной группы. Свойство не может быть null.
     */
    public void setExamGroupSet(ExamGroupSet examGroupSet)
    {
        dirty(_examGroupSet, examGroupSet);
        _examGroupSet = examGroupSet;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ExamGroupSetOpenedGen)
        {
            setEnrollmentCampaign(((ExamGroupSetOpened)another).getEnrollmentCampaign());
            setExamGroupSet(((ExamGroupSetOpened)another).getExamGroupSet());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ExamGroupSetOpenedGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ExamGroupSetOpened.class;
        }

        public T newInstance()
        {
            return (T) new ExamGroupSetOpened();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "examGroupSet":
                    return obj.getExamGroupSet();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "examGroupSet":
                    obj.setExamGroupSet((ExamGroupSet) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "examGroupSet":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "examGroupSet":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "examGroupSet":
                    return ExamGroupSet.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ExamGroupSetOpened> _dslPath = new Path<ExamGroupSetOpened>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ExamGroupSetOpened");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.entrant.ExamGroupSetOpened#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Набор экзаменационной группы. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamGroupSetOpened#getExamGroupSet()
     */
    public static ExamGroupSet.Path<ExamGroupSet> examGroupSet()
    {
        return _dslPath.examGroupSet();
    }

    public static class Path<E extends ExamGroupSetOpened> extends EntityPath<E>
    {
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private ExamGroupSet.Path<ExamGroupSet> _examGroupSet;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.entrant.ExamGroupSetOpened#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Набор экзаменационной группы. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamGroupSetOpened#getExamGroupSet()
     */
        public ExamGroupSet.Path<ExamGroupSet> examGroupSet()
        {
            if(_examGroupSet == null )
                _examGroupSet = new ExamGroupSet.Path<ExamGroupSet>(L_EXAM_GROUP_SET, this);
            return _examGroupSet;
        }

        public Class getEntityClass()
        {
            return ExamGroupSetOpened.class;
        }

        public String getEntityName()
        {
            return "examGroupSetOpened";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
