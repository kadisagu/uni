package ru.tandemservice.uniec.entity.report;

import ru.tandemservice.uniec.entity.report.gen.EntranceExaminationMeetingReportGen;

public class EntranceExaminationMeetingReport extends EntranceExaminationMeetingReportGen
{
    public static final String TEMPLATE_NAME = "entranceExaminationMeetingReport";
}