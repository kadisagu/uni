/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantEnrollmentConditionsEdit;

import java.util.Arrays;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.ContractAdmissionKind;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;


/**
 * @author agolubenko
 * @since 07.04.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setPreliminaryEnrollmentStudent(getNotNull(PreliminaryEnrollmentStudent.class, model.getPreliminaryEnrollmentStudentId()));
        model.setCompensationTypeList(getCatalogItemListOrderByCode(CompensationType.class));
        model.setStudentCategoryList(getCatalogItemListOrderByCode(StudentCategory.class));
        model.setContractAdmissionKindModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult<ContractAdmissionKind> findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(ContractAdmissionKind.ENTITY_CLASS, "contractAdmissionKind");
                if (StringUtils.isNotEmpty(filter))
                {
                    builder.add(MQExpression.like("contractAdmissionKind", ContractAdmissionKind.title().s(), CoreStringUtils.escapeLike(filter)));
                }
                builder.addOrder("contractAdmissionKind", ContractAdmissionKind.title().s());
                return new ListResult<ContractAdmissionKind>(builder.<ContractAdmissionKind> getResultList(getSession(), 0, 50), builder.getResultCount(getSession()));
            }
        });
        model.setParallelList(Arrays.asList(
                new IdentifiableWrapper(Model.AUTO, "Расчитывать автоматически"),
                new IdentifiableWrapper(Model.YES_LOCKED, "Установить «да» принудительно")
        ));
        model.setParallelValue(model.getParallelList().get(model.getPreliminaryEnrollmentStudent().isParallelLocked() ? Model.YES_LOCKED.intValue() : Model.AUTO.intValue()));
        model.setExistOrder(model.getPreliminaryEnrollmentStudent().getEnrollmentOrder() != null);
    }

    @Override
    public void update(Model model)
    {
        model.getPreliminaryEnrollmentStudent().setParallelLocked(model.isListener() && Model.YES_LOCKED.equals(model.getParallelValue().getId()));

        if (model.getPreliminaryEnrollmentStudent().getEnrollmentOrder() == null)
        {
            CoreCollectionUtils.Pair<String, PreliminaryEnrollmentStudent> result = UniecDAOFacade.getEntrantDAO().saveOrUpdatePreliminaryStudent(model.getPreliminaryEnrollmentStudent());
            String error = result.getX();
            if (null != error)
                throw new ApplicationException(error);
        }
    }
}
