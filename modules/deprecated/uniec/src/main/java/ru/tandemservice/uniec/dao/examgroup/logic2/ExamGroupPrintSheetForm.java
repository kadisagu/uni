/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.dao.examgroup.logic2;

import java.util.List;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.examgroup.AbstractExamSheetPrintBuilder;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

/**
 * @author Vasily Zhukov
 * @since 31.05.2010
 */
class ExamGroupPrintSheetForm extends AbstractExamSheetPrintBuilder
{
    @Override
    protected String getTemplateCode()
    {
        return UniecDefines.ENROLLMENT_PASS_DISCIPLINE_SHEET_ALG2;
    }

    @Override
    protected String[] getTableRow(int rowNumber, EntrantRequest entrantRequest, SubjectPassForm subjectPassForm)
    {
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "d");
        builder.add(MQExpression.eq("d", RequestedEnrollmentDirection.L_ENTRANT_REQUEST, entrantRequest));
        builder.addOrder("d", RequestedEnrollmentDirection.P_PRIORITY);
        List<RequestedEnrollmentDirection> list = builder.getResultList(getSession(), 0, 1);
        boolean targetAdmission = !list.isEmpty() && list.get(0).isTargetAdmission();

        String number = Integer.toString(rowNumber + 1);                   // номер по порядку
        String stringNumber = entrantRequest.getStringNumber();            // номер заявления
        String fio = entrantRequest.getEntrant().getPerson().getFullFio(); // фамилия

        // для целевиков приписывать букву Ц
        if (targetAdmission) fio += " (Ц)";

        return new String[]{
                /*0*/ number,
                /*1*/ stringNumber,
                /*2*/ fio,
                /*3*/ null,
                /*4*/ null,
                /*5*/ null,
                /*6*/ number,
                /*7*/ stringNumber,
                /*8*/ fio
        };
    }
}
