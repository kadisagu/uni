/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcCampaign.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;

import ru.tandemservice.uniec.entity.entrant.CurrentExamGroupLogic;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

/**
 * @author Vasily Zhukov
 * @since 26.05.2011
 */
public class EnrollmentCampaignComboDSHandler extends DefaultComboDataSourceHandler
{
    public enum CampaignSelectType
    {
        ALL,                             // показываются все приемные кампании
        WITHOUT_AUTO_FORMING,            // только те, где нет автоформирование экзам. групп
        WITH_AUTO_FORMING,               // только те, где есть автоформирование экзам. групп
        WITH_AUTO_FORMING_AND_ALGORITHM  // только те, где есть автоформирование экзам. групп и выбран алгоритм этого автоформирования
    }

    private CampaignSelectType _campaignSelectType;

    public EnrollmentCampaignComboDSHandler(String ownerId)
    {
        this(ownerId, CampaignSelectType.ALL);
    }

    public EnrollmentCampaignComboDSHandler(String ownerId, CampaignSelectType campaignSelectType)
    {
        super(ownerId, EnrollmentCampaign.class);
        _campaignSelectType = campaignSelectType;
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        if (StringUtils.isNotEmpty(ep.input.getComboFilterByValue()))
            ep.dqlBuilder.where(DQLExpressions.likeUpper(DQLExpressions.property(EnrollmentCampaign.title().fromAlias("e")), DQLExpressions.value(CoreStringUtils.escapeLike(ep.input.getComboFilterByValue()))));
        
        ep.dqlBuilder.fetchPath(DQLJoinType.inner, EnrollmentCampaign.educationYear().fromAlias("e"), "y");
        ep.dqlBuilder.fetchPath(DQLJoinType.inner, EnrollmentCampaign.entrantExamListsFormingFeature().fromAlias("e"), "f");

        switch (_campaignSelectType)
        {
            case ALL:
                break;
            case WITHOUT_AUTO_FORMING:
                ep.dqlBuilder.where(DQLExpressions.eq(DQLExpressions.property(EnrollmentCampaign.formingExamGroupAuto().fromAlias("e")), DQLExpressions.value(Boolean.FALSE)));
                break;
            case WITH_AUTO_FORMING:
                ep.dqlBuilder.where(DQLExpressions.eq(DQLExpressions.property(EnrollmentCampaign.formingExamGroupAuto().fromAlias("e")), DQLExpressions.value(Boolean.TRUE)));
                break;
            case WITH_AUTO_FORMING_AND_ALGORITHM:
                ep.dqlBuilder.where(DQLExpressions.eq(DQLExpressions.property(EnrollmentCampaign.formingExamGroupAuto().fromAlias("e")), DQLExpressions.value(Boolean.TRUE)));
                ep.dqlBuilder.joinEntity("e", DQLJoinType.inner, CurrentExamGroupLogic.class, "c", DQLExpressions.eq(DQLExpressions.property(EnrollmentCampaign.id().fromAlias("e")), DQLExpressions.property(CurrentExamGroupLogic.enrollmentCampaign().id().fromAlias("c"))));
                break;
            default:
                throw new RuntimeException("Unknown selectType: " + _campaignSelectType);
        }

        ep.dqlBuilder.order(DQLExpressions.property(EnrollmentCampaign.id().fromAlias("e")), OrderDirection.desc);
    }
}
