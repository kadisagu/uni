/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.order.EnrollmentOrderAddEdit;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.ui.OrderExecutorSelectModel;
import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager;
import ru.tandemservice.uniec.entity.catalog.EnrollmentOrderBasic;
import ru.tandemservice.uniec.entity.catalog.EnrollmentOrderReason;
import ru.tandemservice.uniec.entity.orders.*;
import ru.tandemservice.uniec.util.EntrantFilterUtil;
import ru.tandemservice.unimove.UnimoveDefines;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

/**
 * @author vip_delete
 * @since 07.04.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.setOrderTypeListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                return new ListResult<>(EcOrderManager.instance().dao().getOrderTypeList(model.getEnrollmentCampaign()));
            }
        });
        model.setEmployeePostModel(new OrderExecutorSelectModel());

        if (model.getOrder().getId() == null)
        {
            // форма добавления
            model.setAddForm(true);
            model.setCreateDate(new Date());
        }
        else
        {
            // форма редактирования
            model.setAddForm(false);
            model.setOrder(getNotNull(EnrollmentOrder.class, model.getOrder().getId()));
            model.setCreateDate(model.getOrder().getCreateDate());
            model.setEnrollmentCampaignDisabled(model.getOrder().getParagraphCount() > 0);
            model.setOrderTypeDisabled(model.isEnrollmentCampaignDisabled());
        }
        model.setReasonModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(EnrollmentOrderReason.ENTITY_CLASS, "reason");
                builder.add(MQExpression.like("reason", EnrollmentOrderReason.P_TITLE, CoreStringUtils.escapeLike(filter)));
                builder.addOrder("reason", EnrollmentOrderReason.P_TITLE);
                return new ListResult<>(builder.<EnrollmentOrderReason>getResultList(getSession()));
            }
        });

        model.setBasicModel(new FullCheckSelectModel()
        {

            @Override
            public ListResult findValues(String filter)
            {
                if (null == model.getOrder().getReason())
                    return new ListResult<>(Collections.emptyList());

                MQBuilder builder = new MQBuilder(EnrollmentOrderReasonToBasicsRel.ENTITY_CLASS, "rel", new String[]{EnrollmentOrderReasonToBasicsRel.L_BASIC});
                builder.add(MQExpression.eq("rel", EnrollmentOrderReasonToBasicsRel.L_REASON, model.getOrder().getReason()));
                builder.addOrder("rel", EnrollmentOrderReasonToBasicsRel.L_BASIC + "." + EnrollmentOrderBasic.P_TITLE);
                return new ListResult<>(builder.getResultList(getSession()));
            }
        });
        if (model.getOrder().getId() != null)
            model.setBasic(model.getOrder().getBasic());
    }

    @Override
    public boolean isTypeBasic(Model model)
    {
        EnrollmentOrderType enrollmentOrderType = EcOrderManager.instance().dao().getEnrollmentOrderType(model.getOrder().getEnrollmentCampaign(), model.getOrder().getType());
        return enrollmentOrderType != null && enrollmentOrderType.isBasic();
    }

    @Override
    public boolean isTypeCommand(Model model)
    {
        EnrollmentOrderType enrollmentOrderType = EcOrderManager.instance().dao().getEnrollmentOrderType(model.getOrder().getEnrollmentCampaign(), model.getOrder().getType());
        return enrollmentOrderType != null && enrollmentOrderType.isCommand();
    }

    @Override
    public boolean isTypeReasonAndBasic(Model model)
    {
        EnrollmentOrderType enrollmentOrderType = EcOrderManager.instance().dao().getEnrollmentOrderType(model.getOrder().getEnrollmentCampaign(), model.getOrder().getType());
        return enrollmentOrderType != null && enrollmentOrderType.isReasonAndBasic();
    }

    @Override
    public void update(Model model, ErrorCollector errors)
    {
        if (!model.isAddForm())
        {
            if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(MoveStudentDaoFacade.getMoveStudentDao().getObjectStateCode(AbstractEntrantOrder.class, model.getOrder().getId())))
                throw new ApplicationException("Редактирование приказа возможно только на этапе формирования.");

            Calendar c1 = Calendar.getInstance();
            c1.setTime(model.getCreateDate());
            c1.set(Calendar.SECOND, 0);
            c1.set(Calendar.MILLISECOND, 0);
            Calendar c2 = Calendar.getInstance();
            c2.setTime(model.getOrder().getCreateDate());
            c2.set(Calendar.SECOND, 0);
            c2.set(Calendar.MILLISECOND, 0);
            if (!c1.equals(c2))
                model.getOrder().setCreateDate(CoreDateUtils.getDateWithMiliseconds(model.getCreateDate()));
        }
        else
        {
            model.getOrder().setCreateDate(model.getCreateDate());
        }
        if (!model.isOrderTypeUseEnrollmentDate())
            model.getOrder().setEnrollmentDate(null);
        EcOrderManager.instance().dao().checkOrderNumber(model.getOrder());
        String executor = OrderExecutorSelectModel.getExecutor(model.getEmployeePost());
        if (executor != null)
            model.getOrder().setExecutor(executor);
        getSession().saveOrUpdate(model.getOrder());

        if (model.getBasic() != null)
        {
            if (null == model.getOrder().getBasic())
            {
                EnrollmentOrderToBasicRel rel = new EnrollmentOrderToBasicRel();
                rel.setBasic(model.getBasic());
                rel.setOrder(model.getOrder());
                getSession().save(rel);
            }
            else
            {
                if (!model.getOrder().getBasic().equals(model.getBasic()))
                {
                    EnrollmentOrderToBasicRel rel = get(EnrollmentOrderToBasicRel.class, EnrollmentOrderToBasicRel.L_ORDER, model.getOrder());
                    rel.setBasic(model.getBasic());
                    getSession().update(rel);
                }
            }
        }
    }
}
