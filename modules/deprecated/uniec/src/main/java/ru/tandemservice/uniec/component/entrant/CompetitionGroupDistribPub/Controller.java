package ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub;

import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.gen.OrgUnitGen;
import ru.tandemservice.uni.entity.catalog.gen.EducationLevelsHighSchoolGen;
import ru.tandemservice.uni.entity.education.gen.EducationOrgUnitGen;
import ru.tandemservice.uniec.IUniecComponents;
import ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.Model.Wrapper;
import ru.tandemservice.uniec.dao.ecg.IEnrollmentCompetitionGroupDAO;
import ru.tandemservice.uniec.entity.ecg.gen.EcgDistribQuotaGen;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.gen.EnrollmentDirectionGen;

import java.util.Collections;

/**
 * @author vdanilov
 */
public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    @SuppressWarnings({ "unchecked", "deprecation" })
    public void onRefreshComponent(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);

        final DynamicListDataSource<Model.Wrapper> ds = new DynamicListDataSource<>(component, component1 -> {
            Controller.this.getDao().prepareQuotaListDataSource(Controller.this.getModel(component1));
        });


        ds.addColumn(new SimpleColumn("Шифр", EcgDistribQuotaGen.L_DIRECTION+"."+EnrollmentDirectionGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnitGen.L_EDUCATION_LEVEL_HIGH_SCHOOL+"."+EducationLevelsHighSchoolGen.P_SHORT_TITLE).setClickable(false).setOrderable(false));
        ds.addColumn(new SimpleColumn("Направление подготовки (специальность)", EcgDistribQuotaGen.L_DIRECTION+"."+EnrollmentDirection.P_TITLE).setClickable(false).setOrderable(false));
        ds.addColumn(new SimpleColumn("Формирующее подр.", EcgDistribQuotaGen.L_DIRECTION+"."+EnrollmentDirectionGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnitGen.L_FORMATIVE_ORG_UNIT+"."+OrgUnitGen.P_SHORT_TITLE).setClickable(false).setOrderable(false));
        ds.addColumn(new SimpleColumn("Территориальное подр.", EcgDistribQuotaGen.L_DIRECTION+"."+EnrollmentDirectionGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnitGen.L_TERRITORIAL_ORG_UNIT+"."+ OrgUnitGen.P_TERRITORIAL_SHORT_TITLE).setClickable(false).setOrderable(false));
        ds.addColumn(new SimpleColumn("Форма", EcgDistribQuotaGen.L_DIRECTION+"."+EnrollmentDirectionGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnitGen.L_DEVELOP_FORM+".title").setClickable(false).setOrderable(false));
        ds.addColumn(new SimpleColumn("Условие", EcgDistribQuotaGen.L_DIRECTION+"."+EnrollmentDirectionGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnitGen.L_DEVELOP_CONDITION+".title").setClickable(false).setOrderable(false));
        ds.addColumn(new SimpleColumn("Технология", EcgDistribQuotaGen.L_DIRECTION+"."+EnrollmentDirectionGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnitGen.L_DEVELOP_TECH+".title").setClickable(false).setOrderable(false));
        ds.addColumn(new SimpleColumn("Срок", EcgDistribQuotaGen.L_DIRECTION+"."+EnrollmentDirectionGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnitGen.L_DEVELOP_PERIOD+".title").setClickable(false).setOrderable(false));

        final AbstractColumn quotaColumn = new SimpleColumn("Свободных мест / план", "quotaString").setClickable(false).setOrderable(false).setWidth(15);
        quotaColumn.setStyleResolver(rowEntity -> {
            final Wrapper wrapper = (Wrapper)rowEntity;
            return "font-weight:bold"+(wrapper.getRealQuota() < 0 ? ";color:red" :
                wrapper.getRealQuota() > 0 ? "" : ";color:green");
        });
        ds.addColumn(quotaColumn);

        model.setQuotaDataSource(ds);

    }


    public void onClickApprove(final IBusinessComponent component) {
        IEventServiceLock eventLock = CoreServices.eventService().lock();
        try {
            getDao().doApprove(getModel(component));
        } finally {
            eventLock.release();
        }
    }

    public void onClickDelete(final IBusinessComponent component) {
        IEnrollmentCompetitionGroupDAO.INSTANCE.get().doDelete(getModel(component).getDistrib());
        deactivate(component);
    }

    public void onClickEdit(final IBusinessComponent component) {
        component.createDefaultChildRegion(new ComponentActivator(
                IUniecComponents.COMPETITION_GROUP_DISTRIB_ADD_EDIT,
                Collections.<String, Object>singletonMap(PublisherActivator.PUBLISHER_ID_KEY, this.getModel(component).getDistrib().getId())
        ));
    }

    public void onClickAddTargetAdmissionEntrants(final IBusinessComponent component) {
        component.createDefaultChildRegion(new ComponentActivator(
                ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.add.TargetAdmission.Model.class.getPackage().getName(),
                Collections.<String, Object>singletonMap(PublisherActivator.PUBLISHER_ID_KEY, this.getModel(component).getDistrib().getId())
        ));
    }

    public void onClickAddNonCompetitiveAdmissionEntrants(final IBusinessComponent component) {
        component.createDefaultChildRegion(new ComponentActivator(
                ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.add.NonCompetitiveAdmission.Model.class.getPackage().getName(),
                Collections.<String, Object>singletonMap(PublisherActivator.PUBLISHER_ID_KEY, this.getModel(component).getDistrib().getId())
        ));
    }

    public void onClickAddCompetitiveAdmissionEntrants(final IBusinessComponent component) {
        component.createDefaultChildRegion(new ComponentActivator(
                ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.add.CompetitiveAdmission.Model.class.getPackage().getName(),
                Collections.<String, Object>singletonMap(PublisherActivator.PUBLISHER_ID_KEY, this.getModel(component).getDistrib().getId())
        ));
    }





}
