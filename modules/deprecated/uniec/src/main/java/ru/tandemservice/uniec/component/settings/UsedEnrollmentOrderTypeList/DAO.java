/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.settings.UsedEnrollmentOrderTypeList;

import java.util.List;

import org.hibernate.Session;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrderType;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

/**
 * @author vip_delete
 * @since 22.07.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(EnrollmentOrderType.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", EnrollmentOrderType.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
        builder.addOrder("e", EnrollmentOrderType.P_PRIORITY);
        List<EnrollmentOrderType> list = builder.getResultList(getSession());

        model.getDataSource().setCountRow(list.size());
        UniBaseUtils.createPage(model.getDataSource(), list);
    }

    @Override
    public void updateToggleUsed(Long orderTypeId)
    {
        final Session session = getSession();

        EnrollmentOrderType enrollmentOrderType = getNotNull(EnrollmentOrderType.class, orderTypeId);
        session.refresh(enrollmentOrderType);
        enrollmentOrderType.setUsed(!enrollmentOrderType.isUsed());
        session.update(enrollmentOrderType);
    }

    @Override
    public void updateToggleBasic(Long orderTypeId)
    {
        final Session session = getSession();

        EnrollmentOrderType enrollmentOrderType = getNotNull(EnrollmentOrderType.class, orderTypeId);
        session.refresh(enrollmentOrderType);
        enrollmentOrderType.setBasic(!enrollmentOrderType.isBasic());
        session.update(enrollmentOrderType);
    }

    @Override
    public void updateToggleCommand(Long orderTypeId)
    {
        final Session session = getSession();

        EnrollmentOrderType enrollmentOrderType = getNotNull(EnrollmentOrderType.class, orderTypeId);
        session.refresh(enrollmentOrderType);
        enrollmentOrderType.setCommand(!enrollmentOrderType.isCommand());
        session.update(enrollmentOrderType);
    }

    @Override
    public void updateToggleGroup(Long orderTypeId)
    {
        final Session session = getSession();

        EnrollmentOrderType enrollmentOrderType = getNotNull(EnrollmentOrderType.class, orderTypeId);
        session.refresh(enrollmentOrderType);
        enrollmentOrderType.setGroup(!enrollmentOrderType.isGroup());
        session.update(enrollmentOrderType);
    }

    @Override
    public void updateToggleSelectHeadman(Long orderTypeId)
    {
        final Session session = getSession();

        EnrollmentOrderType enrollmentOrderType = getNotNull(EnrollmentOrderType.class, orderTypeId);
        session.refresh(enrollmentOrderType);
        enrollmentOrderType.setSelectHeadman(!enrollmentOrderType.isSelectHeadman());
        session.update(enrollmentOrderType);
    }

    @Override
    public void updateToggleReasonAndBasic(Long orderTypeId)
    {
        final Session session = getSession();

        EnrollmentOrderType enrollmentOrderType = getNotNull(EnrollmentOrderType.class, orderTypeId);
        session.refresh(enrollmentOrderType);
        enrollmentOrderType.setReasonAndBasic(!enrollmentOrderType.isReasonAndBasic());
        session.update(enrollmentOrderType);
    }

    @Override
    public void updateToggleUseEnrollmentDate(Long orderTypeId)
    {
        final Session session = getSession();

        EnrollmentOrderType enrollmentOrderType = getNotNull(EnrollmentOrderType.class, orderTypeId);
        session.refresh(enrollmentOrderType);
        enrollmentOrderType.setUseEnrollmentDate(!enrollmentOrderType.isUseEnrollmentDate());
        session.update(enrollmentOrderType);
    }
}
