/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.settings.ConversionScale;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.info.ErrorCollector;

/**
 * @author agolubenko
 * @since 25.06.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    public static final String BUDGET_TAB = "Бюджет";
    public static final String CONTRACT_TAB = "Контракт";

    @Override
    public void onRefreshComponent(IBusinessComponent context)
    {
        getModel(context).setSettings(context.getSettings());
        onRefresh(context);
    }

    public void onRefresh(IBusinessComponent component)
    {
        component.saveSettings();

        Model model = getModel(component);
        IDAO dao = getDao();

        dao.prepare(model);
        dao.prepareMatrices(model);
    }

    public void onClickBudgetSave(IBusinessComponent component)
    {
        IDAO dao = getDao();
        Model model = getModel(component);
        ErrorCollector errorCollector = component.getUserContext().getErrorCollector();

        dao.validateBudget(model, errorCollector);
        if (!errorCollector.hasErrors())
        {
            dao.updateBudget(model);
        }
    }

    public void onClickContractSave(IBusinessComponent component)
    {
        IDAO dao = getDao();
        Model model = getModel(component);
        ErrorCollector errorCollector = component.getUserContext().getErrorCollector();

        dao.validateContract(model, errorCollector);
        if (!errorCollector.hasErrors())
        {
            getDao().updateContract(model);
        }
    }
}
