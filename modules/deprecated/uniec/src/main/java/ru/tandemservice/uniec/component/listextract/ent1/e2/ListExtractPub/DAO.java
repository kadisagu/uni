/* $Id$ */
package ru.tandemservice.uniec.component.listextract.ent1.e2.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.AbstractListExtractPubDAO;
import ru.tandemservice.uniec.entity.orders.SplitContractBachelorEntrantsStuListExtract;

/**
 * @author Dmitry Seleznev
 * @since 21.08.2012
 */
public class DAO extends AbstractListExtractPubDAO<SplitContractBachelorEntrantsStuListExtract, Model> implements IDAO
{
}