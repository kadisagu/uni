package ru.tandemservice.uniec.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * ????????????? ??????????????? ????????
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniec_2x8x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность entranceExamMeetingByCKReport2010

		// создано обязательное свойство notIncludeEnrollment
		{
            // создать колонку
			tool.createColumn("ntrncexmmtngbyckrprt2010_t", new DBColumn("notincludeenrollment_p", DBType.BOOLEAN));

            // задать значение по умолчанию
            java.lang.Boolean notIncludeEnrollment = false;
            tool.executeUpdate("update ntrncexmmtngbyckrprt2010_t set notincludeenrollment_p=? where notincludeenrollment_p is null", notIncludeEnrollment);

            // сделать колонку NOT NULL
            tool.setColumnNullable("ntrncexmmtngbyckrprt2010_t", "notincludeenrollment_p", false);

		}


    }
}