/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EnrollmentResults3NK.EnrollmentResults3NKAdd;

import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import org.tandemframework.shared.person.catalog.entity.EducationLevelStage;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.report.EnrollmentResults3NKReport;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vip_delete
 * @since 12.08.2009
 */
class EnrollmentResult3NK2008ReportBuilder
{
    private Session _session;
    private Model _model;

    EnrollmentResult3NK2008ReportBuilder(Model model, Session session)
    {
        _model = model;
        _session = session;
    }

    DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    private byte[] buildReport()
    {
        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("year", _model.getReport().getEnrollmentCampaign().getEducationYear().getTitle());

        int[] requestedRowData = getRowData(getRequestedBuilder());
        for (int i = 0; i < requestedRowData.length; i++)
            injectModifier.put("T0" + (i == 10 ? "A" : Integer.toString(i)), Integer.toString(requestedRowData[i]));

        int[] preliminaryRowData = getRowData(getPreliminaryBuilder());
        for (int i = 0; i < preliminaryRowData.length; i++)
            injectModifier.put("T2" + (i == 10 ? "A" : Integer.toString(i)), Integer.toString(preliminaryRowData[i]));

        injectModifier.put("T10", "0");
        injectModifier.put("T30", Integer.toString(getT30()));
        injectModifier.put("T40", "0");
        injectModifier.put("T50", Integer.toString(getT50()));
        injectModifier.put("T60", Integer.toString(getT60()));

        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ENROLLMENT_RESULTS_3NK);
        return RtfUtil.toByteArray(templateDocument.getCurrentTemplate(), injectModifier, null);
    }

    @SuppressWarnings("deprecation")
    private int[] getRowData(DQLSelectBuilder baseBuilder)
    {
        DQLSelectBuilder builder = baseBuilder
                .column(property("entrant.id"), "entrantId")
                .column(property("pei", PersonEduInstitution.P_YEAR_END), "year")
                .column(property("ls", EducationLevelStage.P_CODE), "lscode")
                .column(property("lsp", EducationLevelStage.P_CODE), "lspcode")
                .joinPath(DQLJoinType.left, Entrant.person().personEduInstitution().fromAlias("entrant"), "pei")
                .joinEntity("pei", DQLJoinType.left, EducationLevelStage.class, "ls", "ls.id=pei." + PersonEduInstitution.L_EDUCATION_LEVEL_STAGE)
                .joinEntity("ls", DQLJoinType.left, EducationLevelStage.class, "lsp", "lsp.id=ls." + EducationLevelStage.L_PARENT)
                .order(property("entrant.id"));

        List<Object[]> rowList = builder.createStatement(new DQLExecutionContext(_session)).list();

        int currentYear = CoreDateUtils.getYear(new Date());
        long prevEntrantId = 0;
        int[] result = new int[11];
        for (Object[] row : rowList)
        {
            long entrantId = ((Number) row[1]).longValue();
            if (entrantId == prevEntrantId) continue; // ignore this row

            Number yearEnd = (Number) row[2];
            int year = yearEnd == null ? -1 : yearEnd.intValue();
            String kindCode = (String) row[3];
            String kindParentCode = (String) row[4];

            prevEntrantId = entrantId;
            result[0]++;
            if (UniDefines.EDUCATION_LEVEL_STAGE_FULL_EDUCATION.equals(kindCode) || UniDefines.EDUCATION_LEVEL_STAGE_FULL_EDUCATION.equals(kindParentCode))
            {
                result[1]++;
                if (year == currentYear)
                {
                    result[2]++;
                    result[3]++;
                }
            } else if (UniDefines.EDUCATION_LEVEL_STAGE_BEGIN_PROFILE.equals(kindCode) || UniDefines.EDUCATION_LEVEL_STAGE_BEGIN_PROFILE.equals(kindParentCode))
            {
                result[4]++;
                if (year == currentYear)
                    result[5]++;
            } else if (UniDefines.EDUCATION_LEVEL_STAGE_FULL_PROFILE.equals(kindCode) || UniDefines.EDUCATION_LEVEL_STAGE_FULL_PROFILE.equals(kindParentCode))
            {
                result[6]++;
                if (year == currentYear)
                    result[7]++;
            } else if (UniDefines.EDUCATION_LEVEL_STAGE_BACHELORS_DEGREE.equals(kindCode))
            {
                result[8]++;
                if (year == currentYear)
                    result[9]++;
            } else if (UniDefines.EDUCATION_LEVEL_STAGE_HIGH_PROFILE.equals(kindCode) || UniDefines.EDUCATION_LEVEL_STAGE_CERTIFICATED_SPEICALISTS_TRAINING.equals(kindCode) || UniDefines.EDUCATION_LEVEL_STAGE_MASTERS_DEGREE.equals(kindCode))
            {
                result[10]++;
            }
        }
        return result;
    }

    private int getT30()
    {
        Number count = new DQLSelectBuilder().fromDataSource(
                getPreliminaryBuilder()
                        .joinEntity("d", DQLJoinType.inner, EntrantAccessDepartment.class, "ac", "ac." + EntrantAccessDepartment.L_ENTRANT + "=entrant.id")
                        .group(property("entrant.id")).buildQuery(), "t")
                .createCountStatement(new DQLExecutionContext(_session))
                .uniqueResult();
        return count == null ? 0 : count.intValue();
    }

    private int getT60()
    {
        Number count = new DQLSelectBuilder().fromDataSource(
                getPreliminaryBuilder()
                        .joinEntity("d", DQLJoinType.inner, ChosenEntranceDiscipline.class, "ch", "ch." + ChosenEntranceDiscipline.L_CHOSEN_ENROLLMENT_DIRECTION + "=d." + RequestedEnrollmentDirection.P_ID)
                        .where(in(property("ch." + ChosenEntranceDiscipline.P_FINAL_MARK_SOURCE), Arrays.asList(UniecDefines.FINAL_MARK_STATE_EXAM_WAVE1, UniecDefines.FINAL_MARK_STATE_EXAM_WAVE2, UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL, UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL_APPEAL)))
                        .group(("entrant.id")).buildQuery(), "t")
                .createCountStatement(new DQLExecutionContext(_session))
                .uniqueResult();
        return count == null ? 0 : count.intValue();
    }

    private int getT50()
    {
        Number count = new DQLSelectBuilder().fromDataSource(
                getPreliminaryBuilder()
                        .where(eq(property("p." + PreliminaryEnrollmentStudent.P_TARGET_ADMISSION), value(Boolean.TRUE)))
                        .group(("entrant.id")).buildQuery(), "t")
                .createCountStatement(new DQLExecutionContext(_session))
                .uniqueResult();
        return count == null ? 0 : count.intValue();
    }

    private DQLSelectBuilder getRequestedBuilder()
    {
        EnrollmentResults3NKReport report = _model.getReport();

        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.column(value("0"), "x");
        builder.fromEntity(RequestedEnrollmentDirection.class, "d");
        builder.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, "ed");
        builder.joinPath(DQLJoinType.inner, "ed." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "ou");
        builder.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_COMPENSATION_TYPE, "ct");

        patchEduOrgUnit(builder, report, "d." + PreliminaryEnrollmentStudent.L_STUDENT_CATEGORY);

        return builder;
    }

    private DQLSelectBuilder getPreliminaryBuilder()
    {
        EnrollmentResults3NKReport report = _model.getReport();

        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.column(value("0"), "x");
        builder.fromEntity(PreliminaryEnrollmentStudent.class, "p");
        builder.joinPath(DQLJoinType.inner, "p." + PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, "d");
        builder.joinPath(DQLJoinType.inner, "p." + PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT, "ou");
        builder.joinPath(DQLJoinType.inner, "p." + PreliminaryEnrollmentStudent.L_COMPENSATION_TYPE, "ct");

        patchEduOrgUnit(builder, report, "p." + PreliminaryEnrollmentStudent.L_STUDENT_CATEGORY);

        if (_model.isParallelActive() && _model.getReport().getExcludeParallel())
            builder.where(eq(property("p." + PreliminaryEnrollmentStudent.P_PARALLEL), value(Boolean.FALSE)));

        return builder;
    }

    private void patchEduOrgUnit(DQLSelectBuilder builder, EnrollmentResults3NKReport report, String studentCategoryPath)
    {
        builder.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "request");
        builder.joinPath(DQLJoinType.inner, "request." + EntrantRequest.L_ENTRANT, "entrant");
        builder.joinPath(DQLJoinType.inner, "ou." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "hs");
        builder.joinPath(DQLJoinType.inner, "hs." + EducationLevelsHighSchool.L_EDUCATION_LEVEL, "el");
        builder.where(eq(property("entrant." + Entrant.P_ARCHIVAL), value(Boolean.FALSE)));
        builder.where(ne(property("d." + RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE), value(UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY)));
        builder.where(eq(property("entrant." + Entrant.L_ENROLLMENT_CAMPAIGN), value(report.getEnrollmentCampaign())));
        builder.where(betweenDays("request." + EntrantRequest.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));
        if (_model.isCompensationTypeActive())
            builder.where(eq(property("ct." + CompensationType.P_ID), value(_model.getReport().getCompensationType().getId())));
        builder.where(eq(property("ou." + EducationOrgUnit.L_DEVELOP_FORM), value(_model.getReport().getDevelopForm())));
        if (_model.isDevelopConditionActive())
            builder.where(in(property("ou." + EducationOrgUnit.L_DEVELOP_CONDITION), _model.getDevelopConditionList()));
        if (_model.isQualificationActive())
            builder.where(in(property("el." + EducationLevels.L_QUALIFICATION), _model.getQualificationList()));
        if (_model.isFormativeOrgUnitActive())
            builder.where(in(property("ou." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT), _model.getFormativeOrgUnitList()));
        if (_model.isTerritorialOrgUnitActive())
        {
            if (_model.getTerritorialOrgUnitList().isEmpty())
                builder.where(isNull(property("ou." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT)));
            else
                builder.where(in(property("ou." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT), _model.getTerritorialOrgUnitList()));
        }
        if (_model.isStudentCategoryActive())
            builder.where(in(property(studentCategoryPath), _model.getStudentCategoryList()));
    }
}