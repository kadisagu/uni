package ru.tandemservice.uniec.base.entity.ecg;

import ru.tandemservice.uniec.base.entity.ecg.codes.EcgDistributionStateCodes;
import ru.tandemservice.uniec.base.entity.ecg.gen.EcgDistributionStateGen;

/**
 * Состояние распределения
 */
public class EcgDistributionState extends EcgDistributionStateGen
{
    public boolean isFormative()
    {
        return getCode().equals(EcgDistributionStateCodes.FORMATIVE);
    }

    public boolean isLocked()
    {
        return getCode().equals(EcgDistributionStateCodes.LOCKED);
    }

    public boolean isApproved()
    {
        return getCode().equals(EcgDistributionStateCodes.APPROVED);
    }
    
    public boolean isLockedOrApproved()
    {
        return isLocked() || isApproved();
    }
}