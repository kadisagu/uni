/* $Id$ */
package ru.tandemservice.uniec.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author Nikolay Fedorovskih
 * @since 13.08.2014
 */
@SuppressWarnings({"unused"})
public class MS_uniec_2x6x4_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.15"),
                        new ScriptDependency("org.tandemframework.shared", "1.6.4")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        tool.executeUpdate("update enrollmentorder_t \n" +
                                   "set type_id=(select id from entrantenrollmentordertype_t where code_p='revert') \n" +
                                   "where id=any(select a.order_id from enrollmentrevertparagraph_t r join abstractentrantparagraph_t a on r.id=a.id)");
    }
}
