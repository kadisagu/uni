/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcOrder.logic;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.sec.entity.Admin;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.ScriptExecutor;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.catalog.entity.ScriptItem;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.commonbase.utils.ooffice.OpenOfficeServiceUtil;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.base.bo.UniReport.util.UniReportUtils;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.services.UniServiceFacade;
import ru.tandemservice.uni.ui.OrderExecutorSelectModel;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.IPrintableEnrollmentExtract;
import ru.tandemservice.uniec.entity.catalog.EnrollmentOrderBasic;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.orders.*;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.dao.MoveDaoFacade;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;
import ru.tandemservice.unimove.entity.catalog.OrderStates;
import ru.tandemservice.unimv.UnimvDefines;
import ru.tandemservice.unimv.UnimvUtil;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimv.entity.visa.VisaTask;
import ru.tandemservice.unimv.services.visatask.ISendToCoordinationService;
import ru.tandemservice.unimv.services.visatask.ITouchVisaTaskHandler;
import ru.tandemservice.unimv.services.visatask.ITouchVisaTaskService;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Vasily Zhukov
 * @since 24.05.2011
 */
public class EcOrderDao extends UniBaseDao implements IEcOrderDao
{
    public static final String TEMPLATE_DOCUMENT_VARIABLE = "templateDocument";

    @Override
    public void deleteOrder(EnrollmentOrder order)
    {
        Session session = getSession();
        // при удалении проведенного приказа абитуриент остается зачисленным
        if (!UnimoveDefines.CATALOG_ORDER_STATE_FINISHED.equals(order.getState().getCode()))
        {
            EntrantState preEnrolled = getByCode(EntrantState.class, UniecDefines.ENTRANT_STATE_PRELIMENARY_ENROLLED_CODE);

            // устанавливается состояние «Пред. зачислен» для всех выбранных направлений приема из приказа
            MQBuilder builder = new MQBuilder(EnrollmentExtract.ENTITY_CLASS, "e");
            builder.add(MQExpression.eq("e", IAbstractExtract.L_PARAGRAPH + "." + IAbstractParagraph.L_ORDER, order));
            List<EnrollmentExtract> list = builder.getResultList(session);
            for (EnrollmentExtract extract : list)
            {
                extract.getEntity().getRequestedEnrollmentDirection().setState(preEnrolled);
                session.update(extract.getEntity().getRequestedEnrollmentDirection());
            }
        }

        // очищаем старост
        if (!order.isRevert())
        {
            for (IAbstractParagraph item : order.getParagraphList())
            {
                EnrollmentParagraph paragraph = (EnrollmentParagraph) item;
                if (paragraph.getGroupManager() != null)
                    paragraph.setGroupManager(null);
            }
        }

        // удаляем приказ со всеми параграфами и выписками
        MoveDaoFacade.getMoveDao().deleteOrderWithExtracts(order);
    }

    @Override
    public void deleteParagraph(EnrollmentParagraph paragraph)
    {
        final Session session = getSession();
        session.refresh(paragraph.getOrder());

        if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(paragraph.getOrder().getState().getCode()))
            throw new ApplicationException("Изменять приказ можно только в состоянии «Формируется».");

        EntrantState preEnrolled = getByCode(EntrantState.class, UniecDefines.ENTRANT_STATE_PRELIMENARY_ENROLLED_CODE);

        // устанавливается состояние «Пред. зачислен» для всех выбранных направлений приема из параграфа
        for (EnrollmentExtract extract : getList(EnrollmentExtract.class, IAbstractExtract.L_PARAGRAPH, paragraph))
        {
            extract.getEntity().getRequestedEnrollmentDirection().setState(preEnrolled);
            session.update(extract.getEntity().getRequestedEnrollmentDirection());
        }

        // удаляем старосту из параграфа, если он есть
        if (paragraph.getGroupManager() != null)
        {
            paragraph.setGroupManager(null);
            session.update(paragraph);
            session.flush();
        }

        // удаляем параграф со всеми выписками
        MoveDaoFacade.getMoveDao().deleteParagraph(paragraph, null);
    }

    @Override
    public void deleteExtract(EnrollmentExtract extract)
    {
        IAbstractParagraph paragraph = refresh(extract.getParagraph());
        if (paragraph.getExtractCount() == 1)
            throw new ApplicationException("Параграф не может быть пустым.");

        delete(extract);

        // Перенумеруем все выписки, так чтобы сортировка была по ФИО
        List<EnrollmentExtract> extractList = getList(EnrollmentExtract.class, IAbstractExtract.L_PARAGRAPH, paragraph);
        extractList.sort(CommonCollator.comparing(e -> e.getEntity().getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getFullFio(), true));

        // присваиваем уникальные номера выпискам
        int counter = -1;
        for (EnrollmentExtract enrollmentExtract : extractList)
        {
            enrollmentExtract.setNumber(counter--);
            getSession().update(enrollmentExtract);
        }
        getSession().flush();

        // нумеруем выписки с единицы
        counter = 1;
        for (EnrollmentExtract enrollmentExtract : extractList)
        {
            enrollmentExtract.setNumber(counter++);
            update(enrollmentExtract);
        }
    }

    @Override
    public void executeAnnulExtract(EnrollmentExtract extract)
    {
        PreliminaryEnrollmentStudent preStudent = extract.getEntity();
        EnrollmentParagraph paragraph = (EnrollmentParagraph) extract.getParagraph();

        boolean needDeleteParagraph = paragraph.getExtractCount() == 1;

        if (extract.equals(paragraph.getGroupManager()))
        {
            paragraph.setGroupManager(null);
            update(paragraph);
        }

        delete(extract);

        delete(preStudent);

        getSession().flush();

        if (needDeleteParagraph)
            delete(paragraph);
    }

    @Override
    public byte[] getOrderTemplate(EntrantEnrollmentOrderType orderType)
    {
        Criteria c = getSession().createCriteria(UniecScriptItem.class);
        c.add(Restrictions.eq(UniecScriptItem.P_CODE, orderType.getCode() + ".order"));
        UniecScriptItem template = (UniecScriptItem) c.uniqueResult();
        return template.getCurrentTemplate();
    }

    @Override
    public byte[] getParagraphTemplate(EntrantEnrollmentOrderType orderType)
    {
        Criteria c = getSession().createCriteria(UniecScriptItem.class);
        c.add(Restrictions.eq(UniecScriptItem.P_CODE, orderType.getCode() + ".paragraph"));
        UniecScriptItem template = (UniecScriptItem) c.uniqueResult();
        return template.getCurrentTemplate();
    }

    @Override
    public EnrollmentOrderType getEnrollmentOrderType(EnrollmentCampaign enrollmentCampaign, EntrantEnrollmentOrderType type)
    {
        MQBuilder builder = new MQBuilder(EnrollmentOrderType.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", EnrollmentOrderType.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign));
        builder.add(MQExpression.eq("e", EnrollmentOrderType.L_ENTRANT_ENROLLMENT_ORDER_TYPE, type));
        return ((EnrollmentOrderType) builder.uniqueResult(getSession()));
    }

    @Override
    public List<EntrantEnrollmentOrderType> getOrderTypeList(EnrollmentCampaign enrollmentCampaign)
    {
        MQBuilder builder = new MQBuilder(EnrollmentOrderType.ENTITY_CLASS, "t", new String[]{EnrollmentOrderType.L_ENTRANT_ENROLLMENT_ORDER_TYPE});
        builder.add(MQExpression.eq("t", EnrollmentOrderType.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign));
        builder.add(MQExpression.eq("t", EnrollmentOrderType.P_USED, Boolean.TRUE));
        builder.addOrder("t", EnrollmentOrderType.P_PRIORITY);
        return builder.getResultList(getSession());
    }

    @Override
    public void saveEnrollmentOrderText(EnrollmentOrder order)
    {
        //1. проверяем существование печатной формы
        Criteria c = getSession().createCriteria(EnrollmentOrderTextRelation.class);
        c.add(Restrictions.eq(EnrollmentOrderTextRelation.L_ORDER, order));
        EnrollmentOrderTextRelation rel = (EnrollmentOrderTextRelation) c.uniqueResult();
        if (rel == null)
        {
            //2. если нету - создаем
            rel = new EnrollmentOrderTextRelation();
            rel.setOrder(order);
        }
        IScriptItem script = getPrintOrderScript(order);
        Map<String, Object> result = CommonManager.instance().scriptDao().getScriptResult(script, IScriptExecutor.TEMPLATE_VARIABLE, getOrderTemplate(order.getType()), IScriptExecutor.OBJECT_VARIABLE, order.getId(), IScriptExecutor.PRINT_OBJECT_VARIABLE, order, IScriptExecutor.PRINT_FILE_NAME, "EnrollmentOrder.rtf");

        // ничего не напечаталось
        if (result == null)
            throw new ApplicationException("Скрип печати приказа о зачислении абитуриентов не создал печатную форму.");

        Object document = result.get(IScriptExecutor.DOCUMENT);
        if (!(document instanceof byte[]))
            throw new ApplicationException("Скрипт печати приказа о зачислении абитуриентов не создал печатную форму.");

        Object fileNameObject = result.get(IScriptExecutor.FILE_NAME);
        String fileName = fileNameObject == null ? "EnrollmentOrder.rtf" : fileNameObject.toString();
        if (!fileName.endsWith(".rtf"))
            throw new ApplicationException("Скрипт печати приказа о зачислении абитуриентов создал неверное имя файла. Имя файла должно иметь расширение rtf.");

        rel.setText((byte[]) document);
        rel.setFileName(fileName);
        getSession().saveOrUpdate(rel);

        // Сохраняем печатные формы выписок
        saveEnrollmentExtractsText(order);
    }

    private void saveEnrollmentExtractsText(EnrollmentOrder order)
    {
        List<IPrintableEnrollmentExtract> extracts = new DQLSelectBuilder().fromEntity(order.isRevert() ? EnrollmentRevertExtract.class : EnrollmentExtract.class, "e")
                .where(eq(property("e." + IPrintableEnrollmentExtract.L_PARAGRAPH + "." + IAbstractParagraph.L_ORDER), value(order)))
                .createStatement(getSession()).list();

        for (IPrintableEnrollmentExtract extract : extracts)
        {
            byte[] content = getExtractPrintForm(extract.getId());
            DatabaseFile file = extract.getContent();
            if (file == null)
            {
                file = new DatabaseFile();
                file.setContent(content);
                save(file);

                extract.setContent(file);
                update(extract);
            }
            else
            {
                file.setContent(content);
                update(file);
            }
        }
    }

    @Override
    public void checkOrderNumber(AbstractEntrantOrder order)
    {
        if (order.getNumber() == null) return;
        if (order.getCreateDate() == null)
            throw new ApplicationException("Нельзя проверить номер приказа: не указана дата формирования.");

        int year = CoreDateUtils.getYear(order.getCreateDate());
        MQBuilder builder = new MQBuilder(AbstractEntrantOrder.ENTITY_NAME, "s");
        builder.add(MQExpression.greatOrEq("s", IAbstractOrder.P_CREATE_DATE, CoreDateUtils.getYearFirstTimeMoment(year)));
        builder.add(MQExpression.less("s", IAbstractOrder.P_CREATE_DATE, CoreDateUtils.getYearFirstTimeMoment(year + 1)));
        builder.add(MQExpression.eq("s", IAbstractOrder.P_NUMBER, order.getNumber().trim()));
        if (order.getId() != null)
            builder.add(MQExpression.notEq("s", "id", order.getId()));

        List<AbstractEntrantOrder> list = builder.getResultList(getSession());
        if (!list.isEmpty())
            throw new ApplicationException("Номер приказа должен быть уникален в рамках календарного года среди всех приказов по абитуриентам.");
    }

    @Override
    public void saveOrUpdateEnrollmentParagraph(EnrollmentOrder order,
                                                EnrollmentParagraph paragraph,
                                                IEnrollmentExtractFactory extractFactory,
                                                boolean enrollParagraphPerEduOrgUnit,
                                                Collection<PreliminaryEnrollmentStudent> preStudents,
                                                PreliminaryEnrollmentStudent manager,
                                                OrgUnit formativeOrgUnit,
                                                EducationLevelsHighSchool educationLevelsHighSchool,
                                                DevelopForm developForm,
                                                Course course,
                                                String groupTitle)
    {
        // В один и тот же приказ нельзя одновременно добавлять параграф

        NamedSyncInTransactionCheckLocker.register(getSession(), "EnrollmentOrder" + order.getId());

        // V A L I D A T E

        if (preStudents == null || preStudents.isEmpty())
            throw new ApplicationException("Параграф не может быть пустым.");

        final Session session = getSession();
        session.refresh(order);

        if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(order.getState().getCode()))
            throw new ApplicationException("Приказ уже не в состоянии формирования. Изменение приказа невозможно.");

        List<PreliminaryEnrollmentStudent> list = new ArrayList<>(preStudents);
        Collections.sort(list, ITitled.TITLED_COMPARATOR);

        if (manager != null && !list.contains(manager))
            throw new ApplicationException("Старостой может быть только один из предварительно зачисленных абитуриентов.");

        // пока мы были на форме state мог очень сильно поменяться, поэтому надо все данные проверить на актуаность
        for (PreliminaryEnrollmentStudent preStudent : list)
        {
            final Entrant entrant = preStudent.getRequestedEnrollmentDirection().getEntrantRequest().getEntrant();

            // абитуриент не должен быть в другом приказе
            EnrollmentExtract enrollmentExtract = get(EnrollmentExtract.class, IAbstractExtract.L_ENTITY, preStudent);
            if (enrollmentExtract != null && !(paragraph != null && paragraph.equals(enrollmentExtract.getParagraph())))
            {
                String orderNumber = enrollmentExtract.getOrder().getNumber();
                throw new ApplicationException("Абитуриент «" + entrant.getPerson().getFullFio() + "» уже в приказе" + (StringUtils.isEmpty(orderNumber) ? "" : " №" + orderNumber) + ".");
            }

            // абитуриент не может быть архивным
            if (entrant.isArchival())
            {
                throw new ApplicationException("Абитуриент «" + entrant.getPerson().getFullFio() + "» уже в архиве.");
            }

            // приемная кампания приказа должна совпадать с той, что в студенте предзачисления
            if (!order.getEnrollmentCampaign().equals(entrant.getEnrollmentCampaign()))
                throw new ApplicationException("Приемная кампания у предварительно зачисленного абитуриента «" + entrant.getPerson().getFullFio() + "» отличается от указанной в приказе.");

            // тип приказа должен совпадать
            if (!order.getType().equals(preStudent.getEntrantEnrollmentOrderType()))
                throw new ApplicationException("Тип приказа у предварительно зачисленного абитуриента «" + entrant.getPerson().getFullFio() + "» отличается от типа приказа.");

            // формирующее подр. должно совпадать с тем, что выбрано на форме
            if (!formativeOrgUnit.equals(preStudent.getEducationOrgUnit().getFormativeOrgUnit()))
                throw new ApplicationException("Формирующее подразделение у предварительно зачисленного абитуриента «" + entrant.getPerson().getFullFio() + "» отличается от указанного в приказе.");

            // направление подготовки должно совпадать с тем, что выбрано на форме
            if (!educationLevelsHighSchool.equals(preStudent.getEducationOrgUnit().getEducationLevelHighSchool()))
                throw new ApplicationException("Направление подготовки (специальность) у предварительно зачисленного абитуриента «" + entrant.getPerson().getFullFio() + "» отличается от указанного в приказе.");

            // форма освоения должно совпадать с тем, что выбрано на форме
            if (!developForm.equals(preStudent.getEducationOrgUnit().getDevelopForm()))
                throw new ApplicationException("Форма освоения у предварительно зачисленного абитуриента «" + entrant.getPerson().getFullFio() + "» отличается от указанной в приказе.");

            //EntrantState entrantState = preStudent.getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getState();
            // абитуриент не должен быть зачисленным
            //if (entrantState != null && UniecDefines.ENTRANT_STATE_ENROLED_CODE.equals(entrantState.getCode()))
            //    throw new ApplicationException("Абитуриент «" + entrant.getPerson().getFullFio() + "» уже зачислен!");

            // у абитуриента должны быть сданы оригиналы документов
            if (order.getEnrollmentCampaign().isNeedOriginDocForOrder())
            {
                if (preStudent.getStudentCategory().getCode().equals(UniDefines.STUDENT_CATEGORY_STUDENT) && !preStudent.getRequestedEnrollmentDirection().isOriginalDocumentHandedIn())
                    throw new ApplicationException("Абитуриент «" + entrant.getPerson().getFullFio() + "» не сдал оригиналы документов.");

                Criteria criteria = getSession().createCriteria(RequestedEnrollmentDirection.class);
                criteria.createAlias(RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "entrantRequest");
                criteria.add(Restrictions.eq("entrantRequest." + EntrantRequest.L_ENTRANT, entrant));
                criteria.add(Restrictions.eq(RequestedEnrollmentDirection.P_ORIGINAL_DOCUMENT_HANDED_IN, Boolean.TRUE));

                if (criteria.list().isEmpty())
                    throw new ApplicationException("Абитуриент «" + entrant.getPerson().getFullFio() + "» не сдал оригиналы документов ни по одному выбранному направлению подготовки (специальности).");
            }

            // у абитуриента должны быть согласие на зачисление для включения в приказ
            if (order.getEnrollmentCampaign().isRequiredAgreement4Order() && !preStudent.getRequestedEnrollmentDirection().isAgree4Enrollment())
                throw new ApplicationException("Абитуриент «" + entrant.getPerson().getFullFio() + "» не предоставил согласие на зачисление для включения в приказ.");
        }

        if (paragraph != null)
        {
            // E D I T   P A R A G R A P H
            // удаляем выписки у студентов, которые не выбранны на форме. тут специально не перенумеровываем выписки,
            // так как потом будет общая перенумерация по ФИО
            for (IAbstractExtract item : paragraph.getExtractList())
            {
                EnrollmentExtract enrollmentExtract = (EnrollmentExtract) item;
                if (!list.remove(enrollmentExtract.getEntity()))
                {
                    if (paragraph.getGroupManager() != null && paragraph.getGroupManager().equals(enrollmentExtract))
                    {
                        paragraph.setGroupManager(null);
                        session.update(paragraph);
                    }
                    session.delete(enrollmentExtract);
                }
            }
        }
        else
        {
            // C R E A T E   P A R A G R A P H
            paragraph = new EnrollmentParagraph();
            paragraph.setEnrollParagraphPerEduOrgUnit(enrollParagraphPerEduOrgUnit);
            paragraph.setOrder(order);
            paragraph.setNumber(order.getParagraphCount() + 1);  // номер параграфа с единицы
            session.save(paragraph);
        }

        int counter = -1;
        for (PreliminaryEnrollmentStudent preStudent : list)
        {
            // создаем выписку
            EnrollmentExtract extract = extractFactory.createEnrollmentExtract(preStudent);

            // в preStudent уже есть все данные для проведения выписки
            extract.setEntity(preStudent);
            extract.setParagraph(paragraph);
            extract.setCreateDate(order.getCreateDate());
            extract.setNumber(counter--); // нет номера. перенумеруем в конце метода update
            extract.setState(getByCode(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER));
            extract.setCourse(course);
            extract.setGroupTitle(groupTitle);

            // устанавливается состояние «В приказе» для выбранного направления приема
            extract.getEntity().getRequestedEnrollmentDirection().setState(getByCode(EntrantState.class, UniecDefines.ENTRANT_STATE_IN_ORDER));
            session.update(extract.getEntity().getRequestedEnrollmentDirection());

            // сохраняем выписку
            session.save(extract);
        }

        // перенумеруем все выписки, так чтобы сортировка была по ФИО
        session.flush(); // flush перед запросом, чтобы исзлечь ВСЕ выписки
        List<EnrollmentExtract> extractList = getList(EnrollmentExtract.class, IAbstractExtract.L_PARAGRAPH, paragraph);
        extractList.sort(CommonCollator.comparing(e -> e.getEntity().getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getFullFio(), true));

        // присваиваем уникальные номера выпискам
        counter = -1 - extractList.size(); /* заведомо неиспользованные номера */
        for (EnrollmentExtract enrollmentExtract : extractList)
        {
            enrollmentExtract.setNumber(counter--);
            session.update(enrollmentExtract);
        }
        session.flush();

        // нумеруем выписки с единицы, расставляем имена групп зачисления, вычисляем выписку старосты
        counter = 1;
        EnrollmentExtract groupManager = null;
        for (EnrollmentExtract enrollmentExtract : extractList)
        {
            enrollmentExtract.setNumber(counter++);
            enrollmentExtract.setGroupTitle(groupTitle);

            if (enrollmentExtract.getEntity().equals(manager)) groupManager = enrollmentExtract;

            session.update(enrollmentExtract);
        }

        paragraph.setGroupManager(groupManager);
        session.update(paragraph);
    }

    @Override
    public void getDownloadPrintForm(Long enrollmentOrderId, boolean printToPdf)
    {
        if (printToPdf)
            OpenOfficeServiceUtil.checkPdfConverterAvailable();

        EnrollmentOrder order = getNotNull(EnrollmentOrder.class, enrollmentOrderId);
        refresh(order);

        String stateCode = order.getState().getCode();

        // если приказ на согласовании, согласован или проведен, то печатная форма должна быть сохранена
        if (UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTABLE.equals(stateCode)
                || UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTED.equals(stateCode)
                || UnimoveDefines.CATALOG_ORDER_STATE_FINISHED.equals(stateCode))
        {
            // приказ проведен => печатная форма сохранена
            EnrollmentOrderTextRelation text = new DQLSelectBuilder().fromEntity(EnrollmentOrderTextRelation.class, "e")
                    .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentOrderTextRelation.order().fromAlias("e")), DQLExpressions.value(order)))
                    .createStatement(getSession()).uniqueResult();

            if (text == null)
                throw new RuntimeException("Where are no EnrollmentOrderTextRelation for EnrollmentOrder: id=" + enrollmentOrderId + ", stateCode = " + stateCode);

            // загружаем сохраненную печатную форму
            if (printToPdf)
                BusinessComponentUtils.downloadDocument(UniReportUtils.createConvertingToPdfRenderer(text.getFileName().replaceAll("rtf", "pdf"), text.getText()), false);
            else
                BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().document(text.getText()).fileName(text.getFileName()).rtf(), false);
        }
        else
        {
            // создаем новую печатную форму и загружаем ее
            if (printToPdf)
                BusinessComponentUtils.downloadDocument(UniReportUtils.createConvertingToPdfRenderer("EnrollmentOrder.pdf", getOrderPrintFormCreatedByScript(order)), false);
            else
            {
                CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(
                        getPrintOrderScript(order),
                        IScriptExecutor.TEMPLATE_VARIABLE, getOrderTemplate(order.getType()),
                        IScriptExecutor.OBJECT_VARIABLE, enrollmentOrderId,
                        IScriptExecutor.PRINT_OBJECT_VARIABLE, order,
                        IScriptExecutor.PRINT_FILE_NAME, "EnrollmentOrder.rtf"
                );
            }
        }
    }

    @Override
    public IScriptItem getPrintOrderScript(EnrollmentOrder order)
    {
        return getByCode(ScriptItem.class, order.isRevert() ? UniecDefines.SCRIPT_ENROLLMENT_REVERT_ORDER : UniecDefines.SCRIPT_ENROLLMENT_ORDER);
    }

    @Override
    public IScriptItem getPrintExtractScript(IPrintableEnrollmentExtract extract)
    {
        IScriptItem template = getByCode(ScriptItem.class, extract.getOrder().getType().getCode() + ".extract");
        if (template == null)
            template = getByCode(ScriptItem.class, extract.getPrintScriptCatalogCode());
        return template;
    }

    @Override
    public byte[] getOrderPrintFormCreatedByScript(EnrollmentOrder enrollmentOrder)
    {
        IScriptItem script = getPrintOrderScript(enrollmentOrder);
        if (script == null) throw new RuntimeException("Script cannot be null");

        Map<String, Object> result = CommonManager.instance().scriptDao().getScriptResult(script, IScriptExecutor.TEMPLATE_VARIABLE, getOrderTemplate(enrollmentOrder.getType()), IScriptExecutor.OBJECT_VARIABLE, enrollmentOrder.getId(), IScriptExecutor.PRINT_OBJECT_VARIABLE, enrollmentOrder, IScriptExecutor.PRINT_FILE_NAME, "EnrollmentOrder.pdf");

        // ничего не напечаталось
        if (result == null) return null;

        final byte[] document = (byte[]) result.get(ScriptExecutor.DOCUMENT);
        if (document == null) throw new RuntimeException("Document result not found!");

        return document;
    }

    @Override
    public byte[] getExtractPrintForm(Long enrollmentExtractId)
    {
        return RtfUtil.toByteArray(getExtractDocument(this.getNotNull(enrollmentExtractId)));
    }

    @Override
    public void getDownloadExtractPrintForm(Long enrollmentExtractId, boolean printPdf)
    {
        if (printPdf)
        {
            BusinessComponentUtils.downloadDocument(UniReportUtils.createConvertingToPdfRenderer("EnrollmentExtract_" + enrollmentExtractId + ".pdf",
                                                                                                 getExtractDocument(this.getNotNull(enrollmentExtractId))), false);
        }
        else
        {
            BusinessComponentUtils.downloadDocument(
                    new CommonBaseRenderer()
                            .document(getExtractDocument(this.getNotNull(enrollmentExtractId)))
                            .fileName("EnrollmentExtract_" + enrollmentExtractId + ".rtf")
                            .rtf(),
                    false);
        }
    }

    protected int getExtractCountByList()
    {
        return 3;
    }

    @Override
    public RtfDocument getExtractDocument(IPrintableEnrollmentExtract extract)
    {
        IScriptItem printScript = getPrintExtractScript(extract);
        return getExtractDocument(extract, printScript, new RtfReader().read(printScript.getTemplate()));
    }

    @Override
    public RtfDocument getExtractDocument(IPrintableEnrollmentExtract extract, IScriptItem printScript, RtfDocument template)
    {
        // если приказ на согласовании, согласован или проведен, то печатная форма должна быть сохранена
        String stateCode = extract.getOrder().getState().getCode();
        if (extract.getContent() != null && extract.getContent().getContent() != null &&
                (UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTABLE.equals(stateCode)
                        || UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTED.equals(stateCode)
                        || UnimoveDefines.CATALOG_ORDER_STATE_FINISHED.equals(stateCode)))
        {
            return new RtfReader().read(extract.getContent().getContent());
        }

        // Если выписка в другом состоянии или сохраненной формы нет, генерируем новую печатную форму скриптом
        Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(
                printScript,
                TEMPLATE_DOCUMENT_VARIABLE, template,
                IScriptExecutor.OBJECT_VARIABLE, extract.getId(),
                IScriptExecutor.PRINT_OBJECT_VARIABLE, extract
        );

        return (RtfDocument) scriptResult.get(IScriptExecutor.DOCUMENT);
    }

    @Override
    public void getMassExtractPrintForms(Long orderId)
    {
        final List<AbstractEntrantExtract> extracts = getExtractList(orderId);

        if (extracts.isEmpty())
            throw new ApplicationException("Нет выписок для печати.");

        final Iterator<AbstractEntrantExtract> iterator = extracts.iterator();
        final IRtfControl pageBreak = RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE);
        final AbstractEntrantExtract firstExtract = iterator.next();
        final IScriptItem scriptItem = getPrintExtractScript(firstExtract);
        scriptItem.setUserTemplate(new byte[0]); // небольшой хак, чтобы чуть быстрее работало. шаблон мы сами положим в параметры скрипта под другим именем
        final RtfDocument template = new RtfReader().read(scriptItem.getTemplate());
        final RtfDocument mainDoc = getExtractDocument(firstExtract, scriptItem, template.getClone());
        final List<IRtfElement> mainElementList = new ArrayList<>((mainDoc.getElementList().size() + 5) * extracts.size());
        mainElementList.addAll(mainDoc.getElementList());

        final int extractCountByList = getExtractCountByList();
        int counter = 1;
        while (iterator.hasNext())
        {
            if (counter++ >= extractCountByList)
            {
                // Делаем по x выписок на листе (x зависит от шаблона - может 3 влезти, а можем и 2)
                mainElementList.add(pageBreak);
                counter = 1;
            }
            RtfDocument nextDoc = getExtractDocument(iterator.next(), scriptItem, template.getClone());
            mainElementList.addAll(nextDoc.getElementList());
        }
        mainDoc.setElementList(mainElementList);

        BusinessComponentUtils.downloadDocument(
                new CommonBaseRenderer()
                        .document(mainDoc)
                        .fileName("EnrollmentExtractList.rtf")
                        .rtf(),
                false);
    }

    @Override
    public void getMassExamSheetPrintForms(Long orderId)
    {
        final List<AbstractEntrantExtract> extracts = getExtractList(orderId);

        if (extracts.isEmpty())
            throw new ApplicationException("Нет выписок для печети экзаменационных листов");

        final Iterator<AbstractEntrantExtract> iterator = extracts.iterator();
        final IRtfControl pageBreak = RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE);
        final IScriptItem scriptItem = getByCode(UniecScriptItem.class, UniecDefines.SCRIPT_ENROLLMENT_EXAM_SHEET);
        final byte[] template = scriptItem.getTemplate();

        final List<IRtfElement> mainElementList = new ArrayList<>();
        final RtfDocument mainDoc = new RtfDocument();

        while (iterator.hasNext())
        {
            EntrantRequest entrantRequest = iterator.next().getEntity().getRequestedEnrollmentDirection().getEntrantRequest();
            RtfDocument nextDoc = RtfDocument.fromByteArray((byte[]) CommonManager.instance().scriptDao().getScriptResult(
                    scriptItem,
                    "template", template,
                    IScriptExecutor.OBJECT_VARIABLE, entrantRequest.getId()
            ).get(IScriptExecutor.DOCUMENT));
            mainElementList.addAll(nextDoc.getElementList());
            if (iterator.hasNext()) mainElementList.add(pageBreak);
            if (mainDoc.getSettings() == null) mainDoc.setSettings(nextDoc.getSettings());
            if (mainDoc.getHeader() == null) mainDoc.setHeader(nextDoc.getHeader());
        }
        mainDoc.setElementList(mainElementList);

        BusinessComponentUtils.downloadDocument(
                new CommonBaseRenderer()
                        .document(mainDoc)
                        .fileName("EnrollmentExamSheets.rtf")
                        .rtf(),
                false);
    }

    @Override
    public List<AbstractEntrantExtract> getExtractList(Long orderId)
    {
        refresh(orderId);
        return getList(new DQLSelectBuilder().fromEntity(AbstractEntrantExtract.class, "e").column("e")
                .where(eq(property(AbstractEntrantExtract.paragraph().order().fromAlias("e")), value(orderId)))
                .joinPath(DQLJoinType.inner, AbstractEntrantExtract.entity().requestedEnrollmentDirection().entrantRequest().entrant().person().identityCard().fromAlias("e"), "i")
                .order(property(IdentityCard.lastName().fromAlias("i")))
                .order(property(IdentityCard.firstName().fromAlias("i")))
                .order(property(IdentityCard.middleName().fromAlias("i")))
        );
    }

    @Override
    public void saveOrUpdateOrder(EnrollmentOrder order, EmployeePost executor, EnrollmentOrderBasic basic)
    {
        checkOrderNumber(order);
        refresh(executor);

        String executorStr = OrderExecutorSelectModel.getExecutor(executor);
        if (executorStr != null)
            order.setExecutor(executorStr);

        saveOrUpdate(order);

        if (basic != null)
        {
            EnrollmentOrderBasic b = order.getBasic();
            if (b == null)
            {
                EnrollmentOrderToBasicRel rel = new EnrollmentOrderToBasicRel();
                rel.setBasic(basic);
                rel.setOrder(order);
                save(rel);
            }
            else if (!basic.equals(b))
            {
                EnrollmentOrderToBasicRel rel = get(EnrollmentOrderToBasicRel.class, EnrollmentOrderToBasicRel.L_ORDER, order);
                rel.setBasic(basic);
                update(rel);
            }
        }
    }

    @Override
    public void doSendToCoordination(EnrollmentOrder order, IPrincipalContext initiator)
    {
        if (!(initiator instanceof IPersistentPersonable) && !(initiator instanceof Admin && Debug.isEnabled()))
            throw new ApplicationException(EntityRuntime.getMeta(initiator.getId()).getTitle() + " не может отправлять документы на согласование.");

        refresh(order);

        //1. надо проверить, что у приказа сейчас не идет процедура согласования
        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(order);
        if (visaTask != null)
            throw new ApplicationException("Нельзя отправить приказ на согласование, так как он уже на согласовании.");

        if (StringUtils.isEmpty(order.getNumber()))
            throw new ApplicationException("Нельзя отправить приказ на согласование без номера.");

        if (order.getCommitDate() == null)
            throw new ApplicationException("Нельзя отправить приказ на согласование без даты приказа.");

        //2. надо сохранить печатную форму приказа
        saveEnrollmentOrderText(order);

        //3. отправляем на согласование
        ISendToCoordinationService sendToCoordinationService = UniServiceFacade.getService(ISendToCoordinationService.SEND_TO_COORDINATION_SERVICE);
        if (initiator instanceof Admin)
            sendToCoordinationService.init(order, null);
        else
            sendToCoordinationService.init(order, (IPersistentPersonable) initiator);
        sendToCoordinationService.execute();
    }

    @Override
    public void doSendToFormative(EnrollmentOrder order)
    {
        refresh(order);

        //1. надо проверить, что у приказа сейчас не идет процедура согласования
        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(order);
        if (visaTask != null)
            throw new ApplicationException("Нельзя отправить приказ на формирование, так как он на согласовании.");

        //2. надо сменить состояние на формируется
        order.setState(getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE));
        update(order);
    }

    @Override
    public void doReject(EnrollmentOrder order)
    {
        refresh(order);

        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(order);
        if (visaTask != null)
        {
            ITouchVisaTaskService rejectService = UniServiceFacade.getService(ITouchVisaTaskService.REJECT_VISA_TASK_SERVICE_NAME);
            rejectService.init(visaTask.getId(), "SYSTEM: Отменено пользователем с публикатора");
            rejectService.execute();
        }
        else
        {
            //задачи нет => документ уже согласован,
            //в историю согласования ничего не попадет. так как нет задачи согласования
            //короче запускаем процедуру отрицательного завершения процедуры согласования
            ITouchVisaTaskHandler touchService = UnimvUtil.findTouchService(order, UnimvDefines.VISA_REJECT_SERVICE_MAP);
            touchService.init(order);
            touchService.execute();
        }
    }

    @Override
    public void doCommit(EnrollmentOrder order)
    {
        refresh(order);

        if (order.getNumber() == null)
            throw new ApplicationException("Нельзя проводить приказ без номера.");

        if (order.getCommitDate() == null)
            throw new ApplicationException("Нельзя проводить приказ без даты приказа.");

        synchronized (this)
        {
            if (NamedSyncInTransactionCheckLocker.hasLock("enrollmentOrderCommitSync"))
                throw new ApplicationException("Нельзя провести приказ параллельно с проведением другого приказа о зачислении. Повторите попытку проведения через несколько минут.");
            NamedSyncInTransactionCheckLocker.register(getSession(), "enrollmentOrderCommitSync");
        }

        //обновляем печатную форму
        saveEnrollmentOrderText(order);

        MoveDaoFacade.getMoveDao().doCommitOrder(order, UniDaoFacade.getCoreDao().getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FINISHED), UniDaoFacade.getCoreDao().getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED), null);
    }

    @Override
    public void doRollback(EnrollmentOrder order)
    {
        refresh(order);

        // Надо проверить наличие приказов об изменении, созданных на данный приказ
        if (!order.isRevert() && existsEntity(EnrollmentRevertParagraph.class, EnrollmentRevertParagraph.L_CANCELED_ORDER, order))
        {
            throw new ApplicationException("Нельзя откатить приказ, т.к. на него создан приказ об изменении.");
        }

        MoveDaoFacade.getMoveDao().doRollbackOrder(
                order,
                UniDaoFacade.getCoreDao().getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTED),
                UniDaoFacade.getCoreDao().getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER),
                null
        );
    }

    @Override
    public void deleteRevertParagraph(Long paragraphId)
    {
        EnrollmentRevertParagraph paragraph = getNotNull(EnrollmentRevertParagraph.class, paragraphId);

        refresh(paragraph);
        refresh(paragraph.getOrder());

        if (paragraph.getOrder().isReadonly())
            throw new ApplicationException("Изменять приказ можно только в состоянии «Формируется».");

        MoveDaoFacade.getMoveDao().deleteParagraph(paragraph, null);
    }

    @Override
    public void updatePriority(Long paragraphId, int direction)
    {
        final Session session = getSession();

        EnrollmentParagraph paragraph = getNotNull(EnrollmentParagraph.class, paragraphId);

        session.refresh(paragraph);
        session.refresh(paragraph.getOrder());

        List<EnrollmentParagraph> list = getList(EnrollmentParagraph.class, EnrollmentParagraph.order().s(), paragraph.getOrder(), EnrollmentParagraph.number().s());
        int i = 0;
        while (i < list.size() && !list.get(i).equals(paragraph))
        {
            i++;
        }
        if (direction == -1 && i == 0)
            return;
        if (direction == 1 && i == list.size() - 1)
            return;

        EnrollmentParagraph prev = list.get(i + direction);

        int currNumber = paragraph.getNumber();
        int prevNumber = prev.getNumber();

        prev.setNumber(Integer.MIN_VALUE);
        session.update(prev);
        session.flush();

        paragraph.setNumber(prevNumber);
        session.update(paragraph);
        session.flush();

        prev.setNumber(currNumber);
        session.update(paragraph);
        session.flush();
    }

    @Override
    public void saveOrUpdateEnrollmentRevertParagraph(EnrollmentRevertParagraph paragraph, Set<EnrollmentExtract> canceledExtracts)
    {
        final Session session = getSession();

        session.refresh(paragraph.getOrder());
        session.refresh(paragraph.getCanceledOrder());

        if (paragraph.getOrder().isReadonly())
            throw new ApplicationException("Редактировать приказ можно только в состоянии «Формируется».");

        if (canceledExtracts == null || canceledExtracts.isEmpty())
            throw new ApplicationException("Параграф не может быть пустым.");

        if (paragraph.getId() != null)
        {
            // Удаляем имеющиеся выписки
            new DQLDeleteBuilder(EnrollmentRevertExtract.class)
                    .where(eq(property(EnrollmentRevertExtract.paragraph().id()), value(paragraph.getId())))
                    .createStatement(session).execute();
        }
        else
        {
            paragraph.setNumber(paragraph.getOrder().getParagraphCount() + 1);
        }

        session.saveOrUpdate(paragraph);
        session.flush();

        Integer idx = 1;
        ICatalogItem formativeExtractState = getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_FORMATIVE);
        for (EnrollmentExtract canceledExtract : canceledExtracts)
        {
            if (!canceledExtract.getOrder().getId().equals(paragraph.getCanceledOrder().getId()))
                throw new IllegalStateException();

            if (!paragraph.getCanceledOrder().getState().getCode().equals(UnimoveDefines.CATALOG_ORDER_STATE_FINISHED))
                throw new ApplicationException("Отменять можно только выписки из проведенного приказа.");

            if (get(EnrollmentRevertExtract.class, EnrollmentRevertExtract.L_CANCELED_EXTRACT, canceledExtract) != null)
                throw new ApplicationException("Нельзя создать несколько выписок об изменении на одну выписку о зачислении.");

            EnrollmentRevertExtract revertExtract = new EnrollmentRevertExtract();
            revertExtract.setParagraph(paragraph);
            revertExtract.setCreateDate(new Date());
            revertExtract.setState(formativeExtractState);
            revertExtract.setNumber(idx++);
            revertExtract.setCommitted(false);
            revertExtract.setCanceledExtract(canceledExtract);
            session.save(revertExtract);
        }
    }

    @Override
    public void deleteRevertExtract(Long extractId)
    {
        final Session session = getSession();
        EnrollmentRevertExtract extract = getNotNull(EnrollmentRevertExtract.class, extractId);

        session.refresh(extract);
        session.refresh(extract.getParagraph());

        if (extract.getParagraph().getExtractCount() == 1)
            throw new ApplicationException("Параграф не может быть пустым.");

        session.delete(extract);

        // Перенумеруем все выписки, так чтобы сортировка была по ФИО
        List<EnrollmentRevertExtract> extractList = getList(EnrollmentRevertExtract.class, IAbstractExtract.L_PARAGRAPH, extract.getParagraph());
        extractList.sort(CommonCollator.comparing(e -> e.getEntity().getEntrantRequest().getEntrant().getFullFio(), true));

        // присваиваем уникальные номера выпискам
        int counter = -1;
        for (EnrollmentRevertExtract enrollmentExtract : extractList)
        {
            enrollmentExtract.setNumber(counter--);
            session.update(enrollmentExtract);
        }
        session.flush();

        // нумеруем выписки с единицы
        counter = 1;
        for (EnrollmentRevertExtract enrollmentExtract : extractList)
        {
            enrollmentExtract.setNumber(counter++);
            session.update(enrollmentExtract);
        }
    }
}