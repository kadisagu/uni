package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline;
import ru.tandemservice.uniec.entity.entrant.ExcludeSubjectPassForm;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Исключенная форма сдачи
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ExcludeSubjectPassFormGen extends VersionedEntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.ExcludeSubjectPassForm";
    public static final String ENTITY_NAME = "excludeSubjectPassForm";
    public static final int VERSION_HASH = -1596775253;
    private static IEntityMeta ENTITY_META;

    public static final String P_VERSION = "version";
    public static final String L_CHOSEN_ENTRANCE_DISCIPLINE = "chosenEntranceDiscipline";
    public static final String L_SUBJECT_PASS_FORM = "subjectPassForm";

    private int _version; 
    private ChosenEntranceDiscipline _chosenEntranceDiscipline;     // Выбранное вступительное испытание
    private SubjectPassForm _subjectPassForm;     // Форма сдачи дисциплины

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public int getVersion()
    {
        return _version;
    }

    /**
     * @param version  Свойство не может быть null.
     */
    public void setVersion(int version)
    {
        dirty(_version, version);
        _version = version;
    }

    /**
     * @return Выбранное вступительное испытание. Свойство не может быть null.
     */
    @NotNull
    public ChosenEntranceDiscipline getChosenEntranceDiscipline()
    {
        return _chosenEntranceDiscipline;
    }

    /**
     * @param chosenEntranceDiscipline Выбранное вступительное испытание. Свойство не может быть null.
     */
    public void setChosenEntranceDiscipline(ChosenEntranceDiscipline chosenEntranceDiscipline)
    {
        dirty(_chosenEntranceDiscipline, chosenEntranceDiscipline);
        _chosenEntranceDiscipline = chosenEntranceDiscipline;
    }

    /**
     * @return Форма сдачи дисциплины. Свойство не может быть null.
     */
    @NotNull
    public SubjectPassForm getSubjectPassForm()
    {
        return _subjectPassForm;
    }

    /**
     * @param subjectPassForm Форма сдачи дисциплины. Свойство не может быть null.
     */
    public void setSubjectPassForm(SubjectPassForm subjectPassForm)
    {
        dirty(_subjectPassForm, subjectPassForm);
        _subjectPassForm = subjectPassForm;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ExcludeSubjectPassFormGen)
        {
            setVersion(((ExcludeSubjectPassForm)another).getVersion());
            setChosenEntranceDiscipline(((ExcludeSubjectPassForm)another).getChosenEntranceDiscipline());
            setSubjectPassForm(((ExcludeSubjectPassForm)another).getSubjectPassForm());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ExcludeSubjectPassFormGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ExcludeSubjectPassForm.class;
        }

        public T newInstance()
        {
            return (T) new ExcludeSubjectPassForm();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "version":
                    return obj.getVersion();
                case "chosenEntranceDiscipline":
                    return obj.getChosenEntranceDiscipline();
                case "subjectPassForm":
                    return obj.getSubjectPassForm();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "version":
                    obj.setVersion((Integer) value);
                    return;
                case "chosenEntranceDiscipline":
                    obj.setChosenEntranceDiscipline((ChosenEntranceDiscipline) value);
                    return;
                case "subjectPassForm":
                    obj.setSubjectPassForm((SubjectPassForm) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "version":
                        return true;
                case "chosenEntranceDiscipline":
                        return true;
                case "subjectPassForm":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "version":
                    return true;
                case "chosenEntranceDiscipline":
                    return true;
                case "subjectPassForm":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "version":
                    return Integer.class;
                case "chosenEntranceDiscipline":
                    return ChosenEntranceDiscipline.class;
                case "subjectPassForm":
                    return SubjectPassForm.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ExcludeSubjectPassForm> _dslPath = new Path<ExcludeSubjectPassForm>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ExcludeSubjectPassForm");
    }
            

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExcludeSubjectPassForm#getVersion()
     */
    public static PropertyPath<Integer> version()
    {
        return _dslPath.version();
    }

    /**
     * @return Выбранное вступительное испытание. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExcludeSubjectPassForm#getChosenEntranceDiscipline()
     */
    public static ChosenEntranceDiscipline.Path<ChosenEntranceDiscipline> chosenEntranceDiscipline()
    {
        return _dslPath.chosenEntranceDiscipline();
    }

    /**
     * @return Форма сдачи дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExcludeSubjectPassForm#getSubjectPassForm()
     */
    public static SubjectPassForm.Path<SubjectPassForm> subjectPassForm()
    {
        return _dslPath.subjectPassForm();
    }

    public static class Path<E extends ExcludeSubjectPassForm> extends EntityPath<E>
    {
        private PropertyPath<Integer> _version;
        private ChosenEntranceDiscipline.Path<ChosenEntranceDiscipline> _chosenEntranceDiscipline;
        private SubjectPassForm.Path<SubjectPassForm> _subjectPassForm;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExcludeSubjectPassForm#getVersion()
     */
        public PropertyPath<Integer> version()
        {
            if(_version == null )
                _version = new PropertyPath<Integer>(ExcludeSubjectPassFormGen.P_VERSION, this);
            return _version;
        }

    /**
     * @return Выбранное вступительное испытание. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExcludeSubjectPassForm#getChosenEntranceDiscipline()
     */
        public ChosenEntranceDiscipline.Path<ChosenEntranceDiscipline> chosenEntranceDiscipline()
        {
            if(_chosenEntranceDiscipline == null )
                _chosenEntranceDiscipline = new ChosenEntranceDiscipline.Path<ChosenEntranceDiscipline>(L_CHOSEN_ENTRANCE_DISCIPLINE, this);
            return _chosenEntranceDiscipline;
        }

    /**
     * @return Форма сдачи дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExcludeSubjectPassForm#getSubjectPassForm()
     */
        public SubjectPassForm.Path<SubjectPassForm> subjectPassForm()
        {
            if(_subjectPassForm == null )
                _subjectPassForm = new SubjectPassForm.Path<SubjectPassForm>(L_SUBJECT_PASS_FORM, this);
            return _subjectPassForm;
        }

        public Class getEntityClass()
        {
            return ExcludeSubjectPassForm.class;
        }

        public String getEntityName()
        {
            return "excludeSubjectPassForm";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
