/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntranceDisciplineAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.uniec.UniecDefines;

/**
 * @author Боба
 * @since 01.08.2008
 */
public class Controller extends AbstractBusinessController<IDAO, EntranceDisciplineAddEditModel>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        EntranceDisciplineAddEditModel model = getModel(component);
        getDao().prepare(model);
        model.setMessageSource(getMessageSource());
    }

    public void onClickApply(IBusinessComponent component)
    {
        final EntranceDisciplineAddEditModel model = getModel(component);

        ErrorCollector errors = component.getUserContext().getErrorCollector();
        getDao().validate(model, errors);
        if (errors.hasErrors())
        {
            return;
        }

        getDao().update(model);
        deactivate(component);
    }

    public void onChangeType(IBusinessComponent component)
    {
        EntranceDisciplineAddEditModel model = getModel(component);
        if (model.getEntranceDiscipline().getKind() != null && UniecDefines.ENTRANCE_DISCIPLINE_KIND_PROFILE.equals(model.getEntranceDiscipline().getKind().getCode()))
        {
            model.getChoiceDisciplinesList().clear();
        }
    }
}
