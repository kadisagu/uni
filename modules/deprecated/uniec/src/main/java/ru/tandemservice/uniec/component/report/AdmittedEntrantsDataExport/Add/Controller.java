/* $Id$ */
package ru.tandemservice.uniec.component.report.AdmittedEntrantsDataExport.Add;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;

/**
 * Created by IntelliJ IDEA.
 * User: azhebko
 * Date: 24.07.12
 * Time: 17:07
 * To change this template use File | Settings | File Templates.
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    @SuppressWarnings("unchecked")
    public void onClickApply(IBusinessComponent component) throws Exception
    {
        Model model = getModel(component);

        CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(DataAccessServices.dao().getByCode(UniecScriptItem.class, UniecDefines.SCRIPT_ADMITTED_ENTRANTS_DATA_EXPORT),
                "template", new byte[]{}, // Шаблон не используем, делаем заглушку
                "campaignId", model.getEnrollmentCampaign().getId(),
                "dateFrom", model.isDateFromActive() ? model.getDateFrom() : null,
                "dateTo", model.isDateToActive() ? model.getDateTo() : null);
    }
}
