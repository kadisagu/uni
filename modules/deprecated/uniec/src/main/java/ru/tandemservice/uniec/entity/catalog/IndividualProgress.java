package ru.tandemservice.uniec.entity.catalog;

import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.BaseDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import ru.tandemservice.uniec.entity.catalog.gen.IndividualProgressGen;

/** @see ru.tandemservice.uniec.entity.catalog.gen.IndividualProgressGen */
public class IndividualProgress extends IndividualProgressGen implements IDynamicCatalogItem
{
    public static IDynamicCatalogDesc getUiDesc() {
        return new BaseDynamicCatalogDesc() {
            @Override public String getAdditionalPropertiesAddEditPageName() {
                return "ru.tandemservice.uniec.catalog.bo.IndividualProgress.IndividualProgressAdditionalPropertiesAddEdit";
            }
        };
    }


}