/* $Id$ */
package ru.tandemservice.uniec.base.ext.Depersonalization;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.Depersonalization.DepersonalizationManager;
import org.tandemframework.shared.commonbase.base.bo.Depersonalization.logic.IDepersonalizationObject;
import org.tandemframework.shared.commonbase.base.bo.Depersonalization.logic.ScriptItemResetToDefaultObject;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrderTextRelation;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * @author Nikolay Fedorovskih
 * @since 14.11.2014
 */
@Configuration
public class DepersonalizationExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private DepersonalizationManager _depersonalizationManager;

    @Bean
    public ItemListExtension<IDepersonalizationObject> depersonalizationObjectListExtension()
    {
        return _depersonalizationManager.extensionBuilder()
                .add(_depersonalizationManager.updateBuilder(EnrollmentOrderTextRelation.class)
                             .replaceRtf(EnrollmentOrderTextRelation.P_TEXT)
                             .replace(EnrollmentOrderTextRelation.P_FILE_NAME, "replacement.rtf"))
                .add(_depersonalizationManager.updateBuilder(OnlineEntrant.class)
                             .autoReplaceAllStringFields()
                             .replace(OnlineEntrant.P_BIRTH_DATE, new GregorianCalendar(1990, Calendar.JANUARY, 1).getTime())
                             .replace(OnlineEntrant.P_PASSPORT_ISSUANCE_DATE, new GregorianCalendar(2000, Calendar.JANUARY, 1).getTime())
                             .autoCleanSomeFields())
                .add(new ScriptItemResetToDefaultObject(UniecScriptItem.class))
                .build();
    }
}