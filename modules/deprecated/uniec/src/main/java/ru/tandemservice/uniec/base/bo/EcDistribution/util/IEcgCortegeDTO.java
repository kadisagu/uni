/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.util;

import org.tandemframework.core.entity.IEntity;

import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;

/**
 * @author Vasily Zhukov
 * @since 23.07.2011
 */
public interface IEcgCortegeDTO
{
    IEntity getEntity();
    
    boolean isTargetAdmission();

    TargetAdmissionKind getTargetAdmissionKind();

    CompetitionKind getCompetitionKind();

    Double getFinalMark();

    Double getProfileMark();

    Boolean isGraduatedProfileEduInstitution();

    Double getCertificateAverageMark();

    String getFio();
}
