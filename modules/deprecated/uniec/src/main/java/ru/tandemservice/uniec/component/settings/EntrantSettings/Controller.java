/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.settings.EntrantSettings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.runtime.BusinessComponentRuntime;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.source.SimpleListDataSource;

import ru.tandemservice.uniec.IUniecComponents;

/**
 * @author agolubenko
 * @since 23.06.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    private static final String[] COMPONENTS = new String[]{
            IUniecComponents.DISCIPLINE_TO_REALIZATION_WAY,
            IUniecComponents.DISCIPLINE_TO_REALIZATION_FORM,
            IUniecComponents.DISCIPLINES_GROUP_LIST,
            IUniecComponents.DISCIPLINE_MIN_MAX_MARK,
            IUniecComponents.CONVERSION_SCALE};

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null) return;

        List<IdentifiableWrapper> list = new ArrayList<IdentifiableWrapper>();
        for (int i = 0; i < COMPONENTS.length; i++)
            list.add(new IdentifiableWrapper((long) i, BusinessComponentRuntime.getInstance().getComponentMeta(COMPONENTS[i]).getTitle()));

        SimpleListDataSource<IdentifiableWrapper> dataSource = new SimpleListDataSource<IdentifiableWrapper>(list);
        PublisherLinkColumn linkColumn = new PublisherLinkColumn("Название", "title");
        linkColumn.setResolver(new IPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                return new HashMap<Object, Object>();
            }

            @Override
            public String getComponentName(IEntity entity)
            {
                return COMPONENTS[entity.getId().intValue()];
            }
        });
        dataSource.addColumn(linkColumn.setOrderable(false));
        dataSource.setCountRow(COMPONENTS.length);
        model.setDataSource(dataSource);
    }
}
