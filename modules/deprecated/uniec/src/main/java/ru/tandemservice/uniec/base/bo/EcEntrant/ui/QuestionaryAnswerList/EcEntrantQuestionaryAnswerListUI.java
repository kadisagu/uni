/* $Id: $ */
package ru.tandemservice.uniec.base.bo.EcEntrant.ui.QuestionaryAnswerList;


import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;

/**
 * @author Andrey Andreev
 * @since 23.03.2017
 */
@Input({@Bind(key = EcEntrantQuestionaryAnswerList.ENTRANT_ID_PARAM, binding = "entrantId", required = true),
        @Bind(key = EcEntrantQuestionaryAnswerList.TEMPLATE_ID_PARAM, binding = "templateId"),
        @Bind(key = EcEntrantQuestionaryAnswerList.FOR_CAMPAIGN_PARAM, binding = "forCampaign")})
public class EcEntrantQuestionaryAnswerListUI extends UIPresenter
{
    private Long _entrantId;
    private Long _templateId;
    private boolean _forCampaign;


    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EcEntrantQuestionaryAnswerList.ENTRANT_ID_PARAM, _entrantId);
        dataSource.put(EcEntrantQuestionaryAnswerList.TEMPLATE_ID_PARAM, _templateId);
        dataSource.put(EcEntrantQuestionaryAnswerList.FOR_CAMPAIGN_PARAM, _forCampaign);
    }

    public Long getEntrantId()
    {
        return _entrantId;
    }

    public void setEntrantId(Long entrantId)
    {
        _entrantId = entrantId;
    }

    public Long getTemplateId()
    {
        return _templateId;
    }

    public void setTemplateId(Long templateId)
    {
        _templateId = templateId;
    }

    public boolean isForCampaign()
    {
        return _forCampaign;
    }

    public void setForCampaign(boolean forCampaign)
    {
        _forCampaign = forCampaign;
    }
}
