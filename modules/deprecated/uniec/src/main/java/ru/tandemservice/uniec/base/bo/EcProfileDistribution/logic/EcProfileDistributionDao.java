/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec.base.bo.EcProfileDistribution.logic;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.*;
import ru.tandemservice.uniec.base.bo.EcProfileDistribution.util.*;
import ru.tandemservice.uniec.base.entity.ecgp.EcgpDistribution;
import ru.tandemservice.uniec.base.entity.ecgp.EcgpDistributionConfig;
import ru.tandemservice.uniec.base.entity.ecgp.EcgpEntrantRecommended;
import ru.tandemservice.uniec.base.entity.ecgp.gen.EcgpDistributionConfigGen;
import ru.tandemservice.uniec.base.entity.ecgp.gen.EcgpDistributionGen;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.catalog.codes.TargetAdmissionKindCodes;
import ru.tandemservice.uniec.entity.catalog.gen.CompetitionKindGen;
import ru.tandemservice.uniec.entity.catalog.gen.TargetAdmissionKindGen;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.NotUsedTargetAdmissionKind;

import java.util.*;

/**
 * @author Vasily Zhukov
 * @since 10.05.2012
 */
public class EcProfileDistributionDao extends CommonDAO implements IEcProfileDistributionDao
{
    @Override
    public IEcgpQuotaDTO getCurrentQuotaDTO(EcgpConfigDTO configDTO)
    {
        EnrollmentDirection ecgItem = configDTO.getEcgItem();
        List<ProfileEducationOrgUnit> list = getList(ProfileEducationOrgUnit.class, ProfileEducationOrgUnit.enrollmentDirection(), ecgItem);

        Map<Long, Integer> quotaMap = new HashMap<>();

        boolean budget = configDTO.getCompensationType().isBudget();

        boolean allProfileHasPlan = true;
        int sumProfilePlan = 0;
        for (ProfileEducationOrgUnit item : list)
        {
            Integer plan = budget ? item.getBudgetPlan() : item.getContractPlan();
            if (plan == null)
                allProfileHasPlan = false;
            if (allProfileHasPlan)
                sumProfilePlan += plan;
            quotaMap.put(item.getId(), plan);
        }

        Integer directionPlan = budget ? ecgItem.getMinisterialPlan() : ecgItem.getContractPlan();
        int directionPlanInt = directionPlan == null ? 0 : directionPlan;

        int totalQuota = allProfileHasPlan ? Math.min(directionPlanInt, sumProfilePlan) : directionPlanInt;
        boolean totalQuotaValid = !allProfileHasPlan || sumProfilePlan == directionPlanInt;

        return new EcgpQuotaDTO(totalQuota, totalQuotaValid, quotaMap);
    }

    @Override
    public IEcgpQuotaUsedDTO getUsedQuotaDTO(EcgpDistribution distribution)
    {
        List<Object[]> data = new DQLSelectBuilder().fromEntity(EcgpEntrantRecommended.class, "e")
                .column(DQLExpressions.property(EcgpEntrantRecommended.profile().id().fromAlias("e")))
                .column(DQLFunctions.countStar())
                .where(DQLExpressions.eq(DQLExpressions.property(EcgpEntrantRecommended.distribution().fromAlias("e")), DQLExpressions.value(distribution)))
                .group(DQLExpressions.property(EcgpEntrantRecommended.profile().id().fromAlias("e")))
                .createStatement(getSession()).list();

        Map<Long, Integer> id2count = new HashMap<>();
        for (Object[] row : data)
        {
            Long id = (Long) row[0];
            Number number = (Number) row[1];
            id2count.put(id, number == null ? 0 : number.intValue());
        }

        int totalCount = 0;
        Map<Long, Integer> usedMap = new HashMap<>();

        for (ProfileEducationOrgUnit item : getList(ProfileEducationOrgUnit.class, ProfileEducationOrgUnit.enrollmentDirection(), distribution.getConfig().getEcgItem()))
        {
            Integer count = id2count.get(item.getId());
            int countInt = count == null ? 0 : count;
            totalCount += countInt;
            usedMap.put(item.getId(), countInt);
        }

        return new EcgpQuotaUsedDTO(totalCount, usedMap);
    }

    @Override
    public IEcgpQuotaFreeDTO getFreeQuotaDTO(EcgpDistribution distribution)
    {
        IEcgpQuotaDTO quotaDTO = getCurrentQuotaDTO(new EcgpConfigDTO(distribution.getConfig()));
        IEcgpQuotaUsedDTO quotaUsedDTO = getUsedQuotaDTO(distribution);

        int totalFree = quotaDTO.getTotalQuota() - quotaUsedDTO.getTotalUsed();
        Map<Long, Integer> freeMap = new HashMap<>();

        for (Map.Entry<Long, Integer> entry : quotaDTO.getQuotaMap().entrySet())
        {
            Long id = entry.getKey();
            Integer quota = entry.getValue();
            if (quota == null)
                freeMap.put(id, null); // план по профилю не ограничен
            else
                freeMap.put(id, quota - quotaUsedDTO.getUsedMap().get(id));
        }

        return new EcgpQuotaFreeDTO(quotaDTO, quotaUsedDTO, totalFree, freeMap);
    }

    @Override
    public List<? extends IEcgEntrantRateRowDTO> getEntrantRateRowList(EcgpDistribution distribution, List<TargetAdmissionKind> usedTaKindList, Map<CompetitionKind, Integer> competitionKindPriorityMap, IEcgEntrantRateRowDTO.Rule rateRule)
    {
        Map<Long, TargetAdmissionKind> taKindMap = new HashMap<>();
        for (TargetAdmissionKind item : usedTaKindList)
            taKindMap.put(item.getId(), item);

        Map<Long, CompetitionKind> coKindMap = new HashMap<>();
        for (CompetitionKind item : competitionKindPriorityMap.keySet())
            if (item != null)
                coKindMap.put(item.getId(), item);

        // получим всех студентов предзачисления, по которым нужно будет построить рейтинг
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(PreliminaryEnrollmentStudent.class, "ps");
        builder.joinPath(DQLJoinType.inner, PreliminaryEnrollmentStudent.requestedEnrollmentDirection().fromAlias("ps"), "e");

        // учитываем конфигурацию распределения
        builder.where(DQLExpressions.eq(DQLExpressions.property(PreliminaryEnrollmentStudent.compensationType().fromAlias("ps")), DQLExpressions.value(distribution.getConfig().getCompensationType())));
        builder.where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().id().fromAlias("e")), DQLExpressions.value(distribution.getConfig().getEcgItem())));
        builder.where(DQLExpressions.in(DQLExpressions.property(PreliminaryEnrollmentStudent.studentCategory().code().fromAlias("ps")), distribution.getConfig().isSecondHighAdmission() ?
                Arrays.asList(UniDefines.STUDENT_CATEGORY_SECOND_HIGH) : Arrays.asList(UniDefines.STUDENT_CATEGORY_STUDENT, UniDefines.STUDENT_CATEGORY_LISTENER)
        ));

        // НПП должны совпадать
        builder.where(DQLExpressions.eq(DQLExpressions.property(PreliminaryEnrollmentStudent.educationOrgUnit().fromAlias("ps")), DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().fromAlias("e"))));

        // исключаем тех студентов пред. зачисления, кто уже рекомендован в это распределение
        builder.where(DQLExpressions.notIn(DQLExpressions.property(PreliminaryEnrollmentStudent.id().fromAlias("ps")), new DQLSelectBuilder().fromEntity(EcgpEntrantRecommended.class, "r")
                .column(DQLExpressions.property(EcgpEntrantRecommended.preStudent().id().fromAlias("r")))
                .where(DQLExpressions.eq(DQLExpressions.property(EcgpEntrantRecommended.distribution().fromAlias("r")), DQLExpressions.value(distribution)))
                .buildQuery()
        ));

        boolean targetAdmission = rateRule.equals(IEcgEntrantRateRowDTO.Rule.RULE_TARGET_ADMISSION);

        // учитываем правило выборки строк рейтинга
        switch (rateRule)
        {
            case RULE_TARGET_ADMISSION:
                // по целевому приему
                builder.where(DQLExpressions.eq(DQLExpressions.property(PreliminaryEnrollmentStudent.targetAdmission().fromAlias("ps")), DQLExpressions.value(Boolean.TRUE)));
                break;
            case RULE_NON_COMPETITIVE:
                // не по общим основаниям (не по конкурсному приему)
                builder.where(DQLExpressions.ne(DQLExpressions.property(RequestedEnrollmentDirection.competitionKind().fromAlias("e")), DQLExpressions.commonValue(getByNaturalId(new CompetitionKindGen.NaturalId(UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION)))));
                break;
            case RULE_COMPETITIVE:
                // по общим основаниям (по конкурсному приему)
                builder.where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.competitionKind().fromAlias("e")), DQLExpressions.commonValue(getByNaturalId(new CompetitionKindGen.NaturalId(UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION)))));
                break;
            default:
                throw new IllegalArgumentException("Unknown rateRule: " + rateRule);
        }

        builder.joinPath(DQLJoinType.inner, RequestedEnrollmentDirection.entrantRequest().fromAlias("e"), "req");
        builder.joinPath(DQLJoinType.inner, RequestedEnrollmentDirection.competitionKind().fromAlias("e"), "com");
        builder.joinPath(DQLJoinType.left, RequestedEnrollmentDirection.targetAdmissionKind().fromAlias("e"), "tar");
        builder.joinPath(DQLJoinType.inner, RequestedEnrollmentDirection.enrollmentDirection().fromAlias("e"), "dir");
        builder.joinPath(DQLJoinType.inner, EnrollmentDirection.educationOrgUnit().fromAlias("dir"), "ou");
        builder.joinPath(DQLJoinType.inner, EntrantRequest.entrant().fromAlias("req"), "ent");
        builder.joinPath(DQLJoinType.inner, Entrant.person().fromAlias("ent"), "per");
        builder.joinPath(DQLJoinType.inner, Person.identityCard().fromAlias("per"), "card");
        builder.joinPath(DQLJoinType.left, Person.personEduInstitution().fromAlias("per"), "edu");
        builder.joinPath(DQLJoinType.left, RequestedEnrollmentDirection.profileChosenEntranceDiscipline().fromAlias("e"), "pcd");

        builder.joinEntity("ps", DQLJoinType.left, ChosenEntranceDiscipline.class, "c",
                DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.id().fromAlias("e")), DQLExpressions.property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("c")))
        );

        builder.joinEntity("ps", DQLJoinType.inner, PriorityProfileEduOu.class, "pri",
                DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.id().fromAlias("e")), DQLExpressions.property(PriorityProfileEduOu.requestedEnrollmentDirection().id().fromAlias("pri")))
        );
        builder.joinPath(DQLJoinType.inner, PriorityProfileEduOu.profileEducationOrgUnit().educationOrgUnit().fromAlias("pri"), "priOu");

        final int PRE_STUDENT_ID_IDX = 0;
        final int GRADUATED_PROFILE_EDU_INSTITUTION_IDX = 1;
        final int ORIGINAL_DOCUMENT_HANDED_IN_IDX = 2;
        final int COMPETITION_KIND_IDX = 3;
        final int TARGET_ADMISSION_KIND_IDX = 4;
        final int FULL_FIO_IDX = 5;
        final int MARK3_IDX = 6;
        final int MARK4_IDX = 7;
        final int MARK5_IDX = 8;
        final int PROFILE_MARK_IDX = 9;
        final int FINAL_MARK_IDX = 10;
        final int PRIORITY_EDU_OU_ID_IDX = 11;
        final int PRIORITY_EDU_OU_PROFILE_IDX = 12;
        final int PRIORITY_EDU_OU_PRI_IDX = 13;
        final int SHORT_TITLE_IDX = 14;
        final int DEVELOP_FORM_SHORT_TITLE_IDX = 15;
        final int DEVELOP_CONDITION_SHORT_TITLE_IDX = 16;

        builder.group(DQLExpressions.property(PreliminaryEnrollmentStudent.id().fromAlias("ps")));
        builder.group(DQLExpressions.property(RequestedEnrollmentDirection.graduatedProfileEduInstitution().fromAlias("e")));
        builder.group(DQLExpressions.property(RequestedEnrollmentDirection.originalDocumentHandedIn().fromAlias("e")));
        builder.group(DQLExpressions.property(CompetitionKind.id().fromAlias("com")));
        builder.group(DQLExpressions.property(TargetAdmissionKind.id().fromAlias("tar")));
        builder.group(DQLExpressions.property(IdentityCard.fullFio().fromAlias("card")));
        builder.group(DQLExpressions.property(PersonEduInstitution.mark3().fromAlias("edu")));
        builder.group(DQLExpressions.property(PersonEduInstitution.mark4().fromAlias("edu")));
        builder.group(DQLExpressions.property(PersonEduInstitution.mark5().fromAlias("edu")));
        builder.group(DQLExpressions.property(ChosenEntranceDiscipline.finalMark().fromAlias("pcd")));
        builder.group(DQLExpressions.property(PriorityProfileEduOu.id().fromAlias("pri")));
        builder.group(DQLExpressions.property(PriorityProfileEduOu.profileEducationOrgUnit().id().fromAlias("pri")));
        builder.group(DQLExpressions.property(PriorityProfileEduOu.priority().fromAlias("pri")));
        builder.group(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().shortTitle().fromAlias("priOu")));
        builder.group(DQLExpressions.property(EducationOrgUnit.developForm().shortTitle().fromAlias("priOu")));
        builder.group(DQLExpressions.property(EducationOrgUnit.developCondition().shortTitle().fromAlias("priOu")));

        builder.column(DQLExpressions.property(PreliminaryEnrollmentStudent.id().fromAlias("ps")));
        builder.column(DQLExpressions.property(RequestedEnrollmentDirection.graduatedProfileEduInstitution().fromAlias("e")));
        builder.column(DQLExpressions.property(RequestedEnrollmentDirection.originalDocumentHandedIn().fromAlias("e")));
        builder.column(DQLExpressions.property(CompetitionKind.id().fromAlias("com")));
        builder.column(DQLExpressions.property(TargetAdmissionKind.id().fromAlias("tar")));
        builder.column(DQLExpressions.property(IdentityCard.fullFio().fromAlias("card")));
        builder.column(DQLExpressions.property(PersonEduInstitution.mark3().fromAlias("edu")));
        builder.column(DQLExpressions.property(PersonEduInstitution.mark4().fromAlias("edu")));
        builder.column(DQLExpressions.property(PersonEduInstitution.mark5().fromAlias("edu")));
        builder.column(DQLExpressions.property(ChosenEntranceDiscipline.finalMark().fromAlias("pcd")));
        builder.column(DQLFunctions.sum(DQLExpressions.property(ChosenEntranceDiscipline.finalMark().fromAlias("c"))));
        builder.column(DQLExpressions.property(PriorityProfileEduOu.id().fromAlias("pri")));
        builder.column(DQLExpressions.property(PriorityProfileEduOu.profileEducationOrgUnit().id().fromAlias("pri")));
        builder.column(DQLExpressions.property(PriorityProfileEduOu.priority().fromAlias("pri")));
        builder.column(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().shortTitle().fromAlias("priOu")));
        builder.column(DQLExpressions.property(EducationOrgUnit.developForm().shortTitle().fromAlias("priOu")));
        builder.column(DQLExpressions.property(EducationOrgUnit.developCondition().shortTitle().fromAlias("priOu")));

        List<Object[]> dataRowList = builder.createStatement(getSession()).list();

        TargetAdmissionKind defaultTaKind = getByNaturalId(new TargetAdmissionKindGen.NaturalId(TargetAdmissionKindCodes.TA_DEFAULT));

        // preStudentId -> список строк
        Map<Long, List<Object[]>> preRateMap = new HashMap<>();

        // группируем все строки по студенту предзачисления, сохраняя данные по текущей строке
        for (Object[] dataRow : dataRowList)
        {
            long preStudentId = (Long) dataRow[PRE_STUDENT_ID_IDX];
            List<Object[]> list = preRateMap.get(preStudentId);
            if (list == null)
                preRateMap.put(preStudentId, list = new ArrayList<>());
            list.add(dataRow);
        }

        List<EcgEntrantRateRowDTO> resultList = new ArrayList<>();

        for (Map.Entry<Long, List<Object[]>> entry : preRateMap.entrySet())
        {
            // можно взять любую строку (все данные, кроме PRIORITY_EDU_OU_ID_IDX и PRIORITY_EDU_OU_PRI_IDX, будут одинаковые)
            Object[] dataRow = entry.getValue().get(0);

            // taKind
            TargetAdmissionKind taKind = null;
            if (targetAdmission)
            {
                Long targetAdmissionKindId = (Long) dataRow[TARGET_ADMISSION_KIND_IDX];
                taKind = targetAdmissionKindId == null ? defaultTaKind : taKindMap.get(targetAdmissionKindId);
                // если такого вида цп нет в планах приема, то вообще исключаем такое выбранное направление приема 
                if (taKind == null) continue;
            }

            // competitionKind
            long competitionKindId = (Long) dataRow[COMPETITION_KIND_IDX];
            CompetitionKind competitionKind = coKindMap.get(competitionKindId);

            List<IEcgEntrantRateDirectionDTO> directionList = new ArrayList<>();

            Collections.sort(entry.getValue(), new Comparator<Object[]>()
            {
                @Override
                public int compare(Object[] o1, Object[] o2)
                {
                    return ((Integer) o1[PRIORITY_EDU_OU_PRI_IDX]).compareTo((Integer) o2[PRIORITY_EDU_OU_PRI_IDX]);
                }
            });
            for (Object[] row : entry.getValue())
            {
                long priorityEduOuId = (Long) row[PRIORITY_EDU_OU_ID_IDX];
                long profileDirectionId = (Long) row[PRIORITY_EDU_OU_PROFILE_IDX];
                String shortTitle = (String) row[SHORT_TITLE_IDX];
                String developFormShortTitle = (String) row[DEVELOP_FORM_SHORT_TITLE_IDX];
                String developConditionShortTitle = (String) row[DEVELOP_CONDITION_SHORT_TITLE_IDX];

                List<String> list = new ArrayList<>();
                if (StringUtils.isNotEmpty(developFormShortTitle))
                    list.add(developFormShortTitle);
                if (StringUtils.isNotEmpty(developConditionShortTitle))
                    list.add(developConditionShortTitle);

                directionList.add(new EcgEntrantRateDirectionDTO(priorityEduOuId, profileDirectionId, shortTitle + " (" + StringUtils.join(list, ", ") + ")"));
            }

            String fio = (String) dataRow[FULL_FIO_IDX];
            boolean originalDocumentHandedIn = (Boolean) dataRow[ORIGINAL_DOCUMENT_HANDED_IN_IDX];
            Double profileMark = (Double) dataRow[PROFILE_MARK_IDX];
            Double finalMark = (Double) dataRow[FINAL_MARK_IDX];
            boolean graduatedProfileEduInstitution = (Boolean) dataRow[GRADUATED_PROFILE_EDU_INSTITUTION_IDX];

            Integer m3 = (Integer) dataRow[MARK3_IDX];
            Integer m4 = (Integer) dataRow[MARK4_IDX];
            Integer m5 = (Integer) dataRow[MARK5_IDX];
            m3 = (m3 != null) ? m3 : 0;
            m4 = (m4 != null) ? m4 : 0;
            m5 = (m5 != null) ? m5 : 0;
            int sum = m3 + m4 + m5;
            double total = m3 * 3 + m4 * 4 + m5 * 5;
            Double averageMark = (sum != 0) ? total / sum : null;

            resultList.add(new EcgEntrantRateRowDTO(entry.getKey(), fio, finalMark == null ? 0 : finalMark, profileMark, averageMark, originalDocumentHandedIn, graduatedProfileEduInstitution, targetAdmission, taKind, competitionKind, directionList));
        }

        Collections.sort(resultList, new EcgContegeComparator(competitionKindPriorityMap));

        return resultList;
    }

    @Override
    public void saveEntrantRecommendedList(EcgpDistribution distribution, Collection<Long> chosenDirectionIds)
    {
        Session session = getSession();

        session.refresh(distribution);

        EnrollmentDirection ecgItem = distribution.getConfig().getEcgItem();
        EnrollmentCampaign enrollmentCampaign = ecgItem.getEnrollmentCampaign();
        Set<ProfileEducationOrgUnit> directionSet = new HashSet<>(getList(ProfileEducationOrgUnit.class, ProfileEducationOrgUnit.enrollmentDirection(), ecgItem));

        Set<Long> usedPreStudentIds = new HashSet<>(new DQLSelectBuilder().fromEntity(EcgpEntrantRecommended.class, "e")
                .column(DQLExpressions.property(EcgpEntrantRecommended.preStudent().id().fromAlias("e")))
                .where(DQLExpressions.eq(DQLExpressions.property(EcgpEntrantRecommended.distribution().fromAlias("e")), DQLExpressions.value(distribution)))
                .createStatement(session).<Long>list());

        List<PriorityProfileEduOu> priorityProfileEduOuList = new DQLSelectBuilder().fromEntity(PriorityProfileEduOu.class, "e").column("e")
                .where(DQLExpressions.in(DQLExpressions.property(PriorityProfileEduOu.id().fromAlias("e")), chosenDirectionIds))
                .createStatement(session).list();

        for (PriorityProfileEduOu priorityProfileEduOu : priorityProfileEduOuList)
        {
            RequestedEnrollmentDirection direction = priorityProfileEduOu.getRequestedEnrollmentDirection();
            PreliminaryEnrollmentStudent preStudent = get(PreliminaryEnrollmentStudent.class, PreliminaryEnrollmentStudent.requestedEnrollmentDirection(), direction);
            session.refresh(preStudent);

            // должен существовать студент предзачисления
            if (preStudent == null)
                throw new RuntimeException("PreStudet is null");

            // НПП студента пред. зачисления должно совпадать с НПП из ВНП
            if (!preStudent.getEducationOrgUnit().equals(preStudent.getRequestedEnrollmentDirection().getEnrollmentDirection().getEducationOrgUnit()))
                throw new ApplicationException("Направление подготовки зачисленного абитуриента «" + preStudent.getTitle() + "» должно совпадать с направлением подготовки выбранного направления приема.");

            // должны совпадать приемные кампании
            if (!direction.getEntrantRequest().getEntrant().getEnrollmentCampaign().equals(enrollmentCampaign))
                throw new RuntimeException("EnrollmentCampaign '" + direction.getEntrantRequest().getEntrant().getEnrollmentCampaign().getTitle() + "' is not allowed for distribution '" + distribution.getId() + "'.");

            // профиль должен быть в направлении приема
            if (!directionSet.contains(priorityProfileEduOu.getProfileEducationOrgUnit()))
                throw new RuntimeException("ProfileEducationOrgUnit '" + priorityProfileEduOu.getProfileEducationOrgUnit().getId() + "' is not allowed for distribution '" + distribution.getId() + "'.");

            // студент пред. зачисления не должен быть в этом распределении
            if (usedPreStudentIds.contains(direction.getId()))
                throw new ApplicationException("Зачисленного абитуриента «" + preStudent.getTitle() + "» уже добавили в это распределение.");

            // конфигурация распределения должна поддерживать студента пред. зачисления (вид затрат и категория поступающего должна быть соответствующей)
            if (!isConfigAllowDirection(distribution.getConfig(), preStudent))
                throw new ApplicationException("Зачисленный абитуриент «" + preStudent.getTitle() + "» не может быть добавлен в распределение, т.е. его вид возмещения затрат или категория поступающего не соответствуют данному распределению.");

            EcgpEntrantRecommended item = new EcgpEntrantRecommended();
            item.setDistribution(distribution);
            item.setPreStudent(preStudent);
            item.setProfile(priorityProfileEduOu.getProfileEducationOrgUnit());
            session.save(item);
        }
    }

    @Override
    public List<IEcgpRecommendedDTO> getEntrantRecommendedRowList(EcgpDistribution distribution)
    {
        // получаем всех рекомендованных
        List<EcgpEntrantRecommended> entrantRecommendedList = getList(EcgpEntrantRecommended.class, EcgpEntrantRecommended.distribution(), distribution);

        // разредяем их на активных и не активных
        List<EcgpEntrantRecommended> activeList = new ArrayList<>();
        List<EcgpEntrantRecommended> nonActiveList = new ArrayList<>();
        for (EcgpEntrantRecommended recommended : entrantRecommendedList)
            (isConfigAllowDirection(distribution.getConfig(), recommended.getPreStudent()) ? activeList : nonActiveList).add(recommended);

        // у активных получаем id ВНП
        List<Long> directionIds = new ArrayList<>();
        for (EcgpEntrantRecommended recommended : activeList)
            directionIds.add(recommended.getPreStudent().getRequestedEnrollmentDirection().getId());

        EnrollmentCampaign enrollmentCampaign = distribution.getConfig().getEcgItem().getEnrollmentCampaign();

        // определяем данные для колонки приоритеты - все профили рекомендованного студента пред. зачисления, удовлетворяющие конфигурации распределения
        final int DIRECTION_ID = 0;
        final int SHORT_TITLE_IX = 1;
        final int DEVELOP_FORM_SHORT_TITLE_IX = 2;
        final int DEVELOP_CONDITION_SHORT_TITLE_IX = 3;
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(PriorityProfileEduOu.class, "e")
                .column(DQLExpressions.property(PriorityProfileEduOu.requestedEnrollmentDirection().id().fromAlias("e")))
                .column(DQLExpressions.property(PriorityProfileEduOu.profileEducationOrgUnit().educationOrgUnit().educationLevelHighSchool().shortTitle().fromAlias("e")))
                .column(DQLExpressions.property(PriorityProfileEduOu.profileEducationOrgUnit().educationOrgUnit().developForm().shortTitle().fromAlias("e")))
                .column(DQLExpressions.property(PriorityProfileEduOu.profileEducationOrgUnit().educationOrgUnit().developCondition().shortTitle().fromAlias("e")))
                .where(DQLExpressions.in(DQLExpressions.property(PriorityProfileEduOu.requestedEnrollmentDirection().id().fromAlias("e")), directionIds))
                .order(DQLExpressions.property(PriorityProfileEduOu.priority().fromAlias("e")));

        Map<Long, List<String>> prioritiesMap = new HashMap<>();
        Map<Long, List<String>> prioritiesShortMap = new HashMap<>();
        for (Object[] dataRow : dql.createStatement(getSession()).<Object[]>list())
        {
            Long entrantId = (Long) dataRow[DIRECTION_ID];
            String shortTitle = (String) dataRow[SHORT_TITLE_IX];
            String developFormShortTitle = (String) dataRow[DEVELOP_FORM_SHORT_TITLE_IX];
            String developConditionShortTitle = (String) dataRow[DEVELOP_CONDITION_SHORT_TITLE_IX];

            List<String> list = new ArrayList<>();
            if (StringUtils.isNotEmpty(developFormShortTitle))
                list.add(developFormShortTitle);
            if (StringUtils.isNotEmpty(developConditionShortTitle))
                list.add(developConditionShortTitle);

            List<String> set = prioritiesMap.get(entrantId);
            if (set == null)
                prioritiesMap.put(entrantId, set = new ArrayList<>());
            set.add(shortTitle + " (" + StringUtils.join(list, ", ") + ")");

            List<String> shortSet = prioritiesShortMap.get(entrantId);
            if (shortSet == null)
                prioritiesShortMap.put(entrantId, shortSet = new ArrayList<>());
            shortSet.add(shortTitle);
        }

        Map<Long, Double> finalMarkMap = new HashMap<>();

        final int REQUESTED_ENROLLMENT_DIRECTION_ID_IX = 0;
        final int FINAL_MARK_IX = 1;
        for (Object[] dataRow : new DQLSelectBuilder().fromEntity(ChosenEntranceDiscipline.class, "e")
                .column(DQLExpressions.property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("e")))
                .column(DQLFunctions.sum(DQLExpressions.property(ChosenEntranceDiscipline.finalMark().fromAlias("e"))))
                .where(DQLExpressions.in(DQLExpressions.property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("e")), directionIds))
                .where(DQLExpressions.isNotNull(ChosenEntranceDiscipline.finalMark().fromAlias("e")))
                .group(DQLExpressions.property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("e")))
                .createStatement(getSession()).<Object[]>list())
        {
            Long requestedEnrollmentDirectionId = (Long) dataRow[REQUESTED_ENROLLMENT_DIRECTION_ID_IX];
            Double finalMark = (Double) dataRow[FINAL_MARK_IX];
            finalMarkMap.put(requestedEnrollmentDirectionId, finalMark);
        }

        // каждому активному рекомендованному можно сопоставить строку рейтинга
        List<IEcgpRecommendedDTO> rateList = new ArrayList<>();

        for (EcgpEntrantRecommended recommended : activeList)
        {
            List<String> priorityList = prioritiesMap.get(recommended.getPreStudent().getRequestedEnrollmentDirection().getId());
            String priorities = priorityList == null ? null : StringUtils.join(priorityList, ", ");
            List<String> shortPriorityList = prioritiesShortMap.get(recommended.getPreStudent().getRequestedEnrollmentDirection().getId());
            String shortPriorities = shortPriorityList == null ? null : StringUtils.join(shortPriorityList, ", ");
            Double finalMark = finalMarkMap.get(recommended.getPreStudent().getRequestedEnrollmentDirection().getId());
            rateList.add(new EcgpRecommendedDTO(recommended, finalMark, priorities, shortPriorities));
        }

        // строки рейтинга активных рекомендованных сортируем стандартно
        Collections.sort(rateList, new EcgContegeComparator(UniecDAOFacade.getSettingsDAO().getCompetitionKindPriorities(enrollmentCampaign)));

        // неактивных рекомендованных сортируем по фио
        nonActiveList.sort(CommonCollator.comparing(e -> e.getPreStudent().getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getFullFio()));

        // к списку рекомендованных добавляем всех неактивных рекомендованных
        for (EcgpEntrantRecommended recommended : nonActiveList)
            rateList.add(new EcgpRecommendedDTO(recommended, null, null, null));

        return rateList;
    }

    @Override
    public Map<Long, String> getQuotaHTMLDescription(IEcgpQuotaFreeDTO freeDTO)
    {
        String style = " style='white-space:nowrap;'";
        String styleRed = " style='white-space:nowrap;color:red;'";

        Map<Long, String> quotaMap = new HashMap<>();

        for (Map.Entry<Long, Integer> entry : freeDTO.getFreeMap().entrySet())
        {
            Long id = entry.getKey();
            Integer free = entry.getValue();
            if (free == null)
                quotaMap.put(id, "");
            else
                quotaMap.put(id, "<div" + (free < 0 ? styleRed : style) + ">" + free + " / " + freeDTO.getQuotaDTO().getQuotaMap().get(id) + "</div>");
        }
        quotaMap.put(0L, "<div" + (freeDTO.getTotalFree() < 0 || !freeDTO.getQuotaDTO().isTotalQuotaValid() ? styleRed : style) + ">" + freeDTO.getTotalFree() + " / " + freeDTO.getQuotaDTO().getTotalQuota() + "</div>");

        return quotaMap;
    }

    @Override
    public boolean isConfigAllowDirection(EcgpDistributionConfig config, PreliminaryEnrollmentStudent preStudent)
    {
        if (!config.getCompensationType().equals(preStudent.getCompensationType()))
            return false;

        String code = preStudent.getStudentCategory().getCode();

        if (config.isSecondHighAdmission())
            return code.equals(UniDefines.STUDENT_CATEGORY_SECOND_HIGH);
        else
            return code.equals(UniDefines.STUDENT_CATEGORY_STUDENT) || code.equals(UniDefines.STUDENT_CATEGORY_LISTENER);
    }

    @Override
    public void doDistributionFill(EcgpDistribution distribution)
    {
        Session session = getSession();

        session.refresh(distribution);

        // свободные места в распределении
        IEcgpQuotaFreeDTO freeQuotaDTO = getFreeQuotaDTO(distribution);

        // получаем используемые виды цп
        List<TargetAdmissionKind> targetAdmissionKindList = new DQLSelectBuilder().fromEntity(TargetAdmissionKind.class, "e")
                .where(DQLExpressions.notIn(DQLExpressions.property(TargetAdmissionKind.id().fromAlias("e")), new DQLSelectBuilder().fromEntity(NotUsedTargetAdmissionKind.class, "nu")
                        .column(DQLExpressions.property(NotUsedTargetAdmissionKind.targetAdmissionKind().id().fromAlias("nu")))
                        .where(DQLExpressions.eq(DQLExpressions.property(NotUsedTargetAdmissionKind.enrollmentCampaign().fromAlias("nu")), DQLExpressions.value(distribution.getConfig().getEcgItem().getEnrollmentCampaign())))
                        .buildQuery()
                )).createStatement(session).list();

        Map<CompetitionKind, Integer> competitionKindPriorityMap = UniecDAOFacade.getSettingsDAO().getCompetitionKindPriorities(distribution.getConfig().getEcgItem().getEnrollmentCampaign());

        // алгоритм заполнения планов
        IEcgQuotaManager quotaManager = new EcgpQuotaManager(freeQuotaDTO);

        // рекомендуемые абитуриенты 
        List<Long> directionList = new ArrayList<>();

        // уже рекомендованные абитуриенты
        Set<Long> usedEntrantIds = new HashSet<>();

        // добавляем по цп
        accumulateRateRow(distribution, targetAdmissionKindList, competitionKindPriorityMap, usedEntrantIds, quotaManager, IEcgEntrantRateRowDTO.Rule.RULE_TARGET_ADMISSION, directionList);

        // добавляем не по общим основаниям
        accumulateRateRow(distribution, targetAdmissionKindList, competitionKindPriorityMap, usedEntrantIds, quotaManager, IEcgEntrantRateRowDTO.Rule.RULE_NON_COMPETITIVE, directionList);

        // добавляем по общим основаниям
        accumulateRateRow(distribution, targetAdmissionKindList, competitionKindPriorityMap, usedEntrantIds, quotaManager, IEcgEntrantRateRowDTO.Rule.RULE_COMPETITIVE, directionList);

        // сохраняем рекомендованных
        saveEntrantRecommendedList(distribution, directionList);
    }

    @Override
    public void doBulkMakeDistribution(Set<EcgpConfigDTO> configSet)
    {
        for (EcgpConfigDTO config : configSet)
        {
            boolean budget = config.getCompensationType().isBudget();
            Integer plan = budget ? config.getEcgItem().getMinisterialPlan() : config.getEcgItem().getContractPlan();
            if (plan != null && plan > 0)
                saveDistribution(config);
        }
    }

    @Override
    public void doBulkFillDistribution(Set<EcgpConfigDTO> configSet)
    {
        for (EcgpConfigDTO configDTO : configSet)
        {
            EcgpDistributionConfig config = getByNaturalId(new EcgpDistributionConfigGen.NaturalId(configDTO.getEcgItem(), configDTO.getCompensationType(), configDTO.isSecondHighAdmission()));

            if (config != null)
                doDistributionFill(getNotNull(EcgpDistribution.class, EcgpDistribution.config(), config));
        }
    }

    @Override
    public void doBulkDeleteDistribution(Set<EcgpConfigDTO> configSet)
    {
        for (EcgpConfigDTO configDTO : configSet)
        {
            EcgpDistributionConfig config = getByNaturalId(new EcgpDistributionConfigGen.NaturalId(configDTO.getEcgItem(), configDTO.getCompensationType(), configDTO.isSecondHighAdmission()));

            if (config != null)
                deleteDistribution(getNotNull(EcgpDistribution.class, EcgpDistribution.config(), config).getId());
        }
    }

    @Override
    public List<EcgpDistributionDTO> getDistributionDTOList(EnrollmentCampaign enrollmentCampaign, CompensationType compensationType, boolean secondHighAdmission, List<OrgUnit> formativeOrgUnitList, List<OrgUnit> territorialOrgUnitList, List<EducationLevelsHighSchool> educationLevelHighSchoolList, List<DevelopForm> developFormList, List<DevelopCondition> developConditionList, List<DevelopTech> developTechList, List<DevelopPeriod> developPeriodList)
    {
        if (enrollmentCampaign == null) return Collections.emptyList();

        // создаем мап: конфигурация распределения -> список основных распределений
        Map<EcgpConfigDTO, EcgpDistribution> configKey2distribution = new HashMap<>();

        // получаем сохраненные распределения
        for (EcgpDistribution distribution : getDistributionList(enrollmentCampaign, compensationType, secondHighAdmission, formativeOrgUnitList, territorialOrgUnitList, educationLevelHighSchoolList, developFormList, developConditionList, developTechList, developPeriodList))
            configKey2distribution.put(new EcgpConfigDTO(distribution.getConfig()), distribution);

        // подготавливаем итоговый список строк
        List<EcgpDistributionDTO> result = new ArrayList<>();

        // счетчик идентификаторов для загрушек
        long id = 0;

        // создаем строки для отображения
        for (EnrollmentDirection ecgItem : getEnrollmentDirectionList(enrollmentCampaign, formativeOrgUnitList, territorialOrgUnitList, educationLevelHighSchoolList, developFormList, developConditionList, developTechList, developPeriodList))
        {
            EcgpConfigDTO configDTO = new EcgpConfigDTO(ecgItem, compensationType, secondHighAdmission);

            EcgpDistribution distribution = configKey2distribution.remove(configDTO);

            if (distribution != null)
                result.add(new EcgpDistributionDTO(id++, distribution));
            else
                result.add(new EcgpDistributionDTO(id++, ecgItem, compensationType, secondHighAdmission));
        }

        // добавляем оставшиеся (неактуальные) распределения
        for (EcgpDistribution distribution : configKey2distribution.values())
            result.add(new EcgpDistributionDTO(id++, distribution));

        // сортируем по названию
        Collections.sort(result, new Comparator<EcgpDistributionDTO>()
        {
            @Override
            public int compare(EcgpDistributionDTO o1, EcgpDistributionDTO o2)
            {
                int r = o1.getEcgItem().getTitle().compareTo(o2.getEcgItem().getTitle());
                if (r != 0) return r;
                return o1.getEcgItem().getId().compareTo(o2.getEcgItem().getId());
            }
        });

        return result;
    }

    @Override
    public EcgpDistribution saveDistribution(EcgpConfigDTO configDTO)
    {
        EcgpDistributionConfig config = getByNaturalId(new EcgpDistributionConfigGen.NaturalId(configDTO.getEcgItem(), configDTO.getCompensationType(), configDTO.isSecondHighAdmission()));

        if (config == null)
        {
            // создаем конфигурацию
            config = new EcgpDistributionConfig();
            config.setEcgItem(configDTO.getEcgItem());
            config.setCompensationType(configDTO.getCompensationType());
            config.setSecondHighAdmission(configDTO.isSecondHighAdmission());
            save(config);
        }

        EcgpDistribution distribution = getByNaturalId(new EcgpDistributionGen.NaturalId(config));

        if (distribution == null)
        {
            // создаем распределение
            distribution = new EcgpDistribution();
            distribution.setConfig(config);
            distribution.setFormingDate(new Date());
            save(distribution);
        }

        return distribution;
    }

    @Override
    public void deleteDistribution(Long distributionId)
    {
        EcgpDistribution distribution = getNotNull(EcgpDistribution.class, distributionId);
        EcgpDistributionConfig config = distribution.getConfig();

        delete(distribution);
        delete(config); // конфиг без distribution быть не может, поэтому его тоже удаляем
    }

    @Override
    public void deleteEntrantRecommended(EcgpEntrantRecommended entrantRecommended)
    {
        delete(entrantRecommended);
    }

    @Override
    public List<ProfileEducationOrgUnit> getDistributionProfileList(EcgpConfigDTO configDTO)
    {
        List<ProfileEducationOrgUnit> list = new DQLSelectBuilder().fromEntity(ProfileEducationOrgUnit.class, "e")
                .where(DQLExpressions.eq(DQLExpressions.property(ProfileEducationOrgUnit.enrollmentDirection().fromAlias("e")), DQLExpressions.value(configDTO.getEcgItem())))
                .createStatement(getSession()).list();

        Comparator<ProfileEducationOrgUnit> comparator = new EntityComparator<>(
                new EntityOrder(ProfileEducationOrgUnit.educationOrgUnit().educationLevelHighSchool().fullTitle().s()),
                new EntityOrder(ProfileEducationOrgUnit.educationOrgUnit().developForm().code().s()),
                new EntityOrder(ProfileEducationOrgUnit.educationOrgUnit().developCondition().code().s()),
                new EntityOrder(ProfileEducationOrgUnit.educationOrgUnit().developTech().code().s()),
                new EntityOrder(ProfileEducationOrgUnit.educationOrgUnit().developPeriod().code().s()),
                new EntityOrder(ProfileEducationOrgUnit.educationOrgUnit().id())
        );
        Collections.sort(list, comparator);

        return list;
    }

    // private

    /**
     * Получает список направлений приема с профилями из указанной приемной кампании
     *
     * @param enrollmentCampaign           приемная кампания
     * @param formativeOrgUnitList         формирующие подр.
     * @param territorialOrgUnitList       территориальные подр.
     * @param educationLevelHighSchoolList направления подготовки (специальности) вуза
     * @param developFormList              формы освоения
     * @param developConditionList         условия освоения
     * @param developTechList              технологии освоения
     * @param developPeriodList            сроки освоения
     * @return список направлений приема
     */
    private List<EnrollmentDirection> getEnrollmentDirectionList(EnrollmentCampaign enrollmentCampaign, List<OrgUnit> formativeOrgUnitList, List<OrgUnit> territorialOrgUnitList, List<EducationLevelsHighSchool> educationLevelHighSchoolList, List<DevelopForm> developFormList, List<DevelopCondition> developConditionList, List<DevelopTech> developTechList, List<DevelopPeriod> developPeriodList)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, "e").column("e")
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.enrollmentCampaign().fromAlias("e")), DQLExpressions.value(enrollmentCampaign)))
                .where(DQLExpressions.in(DQLExpressions.property(EnrollmentDirection.id().fromAlias("e")), new DQLSelectBuilder().fromEntity(ProfileEducationOrgUnit.class, "p")
                        .column(DQLExpressions.property(ProfileEducationOrgUnit.enrollmentDirection().id().fromAlias("p")))
                        .where(DQLExpressions.eq(DQLExpressions.property(ProfileEducationOrgUnit.enrollmentDirection().enrollmentCampaign().fromAlias("p")), DQLExpressions.value(enrollmentCampaign)))
                        .buildQuery()
                ));

        builder.joinPath(DQLJoinType.inner, EnrollmentDirection.educationOrgUnit().fromAlias("e"), "ou");

        if (formativeOrgUnitList != null)
            builder.where(DQLExpressions.in(DQLExpressions.property(EducationOrgUnit.formativeOrgUnit().fromAlias("ou")), formativeOrgUnitList));

        if (territorialOrgUnitList != null)
            builder.where(DQLExpressions.in(DQLExpressions.property(EducationOrgUnit.territorialOrgUnit().fromAlias("ou")), territorialOrgUnitList));

        if (educationLevelHighSchoolList != null)
            builder.where(DQLExpressions.in(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().fromAlias("ou")), educationLevelHighSchoolList));

        if (developFormList != null)
            builder.where(DQLExpressions.in(DQLExpressions.property(EducationOrgUnit.developForm().fromAlias("ou")), developFormList));

        if (developConditionList != null)
            builder.where(DQLExpressions.in(DQLExpressions.property(EducationOrgUnit.developCondition().fromAlias("ou")), developConditionList));

        if (developTechList != null)
            builder.where(DQLExpressions.in(DQLExpressions.property(EducationOrgUnit.developTech().fromAlias("ou")), developTechList));

        if (developPeriodList != null)
            builder.where(DQLExpressions.in(DQLExpressions.property(EducationOrgUnit.developPeriod().fromAlias("ou")), developPeriodList));

        return builder.createStatement(getSession()).list();
    }

    /**
     * Получает список распределений абитуриентов по профилям
     * Список не сортируется
     *
     * @param enrollmentCampaign           приемная кампания
     * @param compensationType             вид возмещения затрат
     * @param secondHighAdmission          true, если следует выбрать распределения только по второму высшему
     * @param formativeOrgUnitList         формирующие подр.
     * @param territorialOrgUnitList       территориальные подр.
     * @param educationLevelHighSchoolList направления подготовки (специальности) вуза
     * @param developFormList              формы освоения
     * @param developConditionList         условия освоения
     * @param developTechList              технологии освоения
     * @param developPeriodList            сроки освоения
     * @return список DTO распределений
     */
    private List<EcgpDistribution> getDistributionList(EnrollmentCampaign enrollmentCampaign,
                                                       CompensationType compensationType,
                                                       boolean secondHighAdmission,
                                                       List<OrgUnit> formativeOrgUnitList,
                                                       List<OrgUnit> territorialOrgUnitList,
                                                       List<EducationLevelsHighSchool> educationLevelHighSchoolList,
                                                       List<DevelopForm> developFormList,
                                                       List<DevelopCondition> developConditionList,
                                                       List<DevelopTech> developTechList,
                                                       List<DevelopPeriod> developPeriodList)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EcgpDistribution.class, "e").column("e");

        builder.joinPath(DQLJoinType.inner, EcgpDistribution.config().fromAlias("e"), "config");
        builder.joinPath(DQLJoinType.inner, EcgpDistributionConfig.ecgItem().fromAlias("config"), "d");
        builder.joinPath(DQLJoinType.inner, EnrollmentDirection.educationOrgUnit().fromAlias("d"), "ou");

        builder.where(DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.enrollmentCampaign().fromAlias("d")), DQLExpressions.value(enrollmentCampaign)));
        builder.where(DQLExpressions.eq(DQLExpressions.property(EcgpDistributionConfig.secondHighAdmission().fromAlias("config")), DQLExpressions.value(secondHighAdmission)));

        if (compensationType != null)
            builder.where(DQLExpressions.eq(DQLExpressions.property(EcgpDistribution.config().compensationType().fromAlias("e")), DQLExpressions.value(compensationType)));

        if (formativeOrgUnitList != null)
            builder.where(DQLExpressions.in(DQLExpressions.property(EducationOrgUnit.formativeOrgUnit().fromAlias("ou")), formativeOrgUnitList));

        if (territorialOrgUnitList != null)
            builder.where(DQLExpressions.in(DQLExpressions.property(EducationOrgUnit.territorialOrgUnit().fromAlias("ou")), territorialOrgUnitList));

        if (educationLevelHighSchoolList != null)
            builder.where(DQLExpressions.in(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().fromAlias("ou")), educationLevelHighSchoolList));

        if (developFormList != null)
            builder.where(DQLExpressions.in(DQLExpressions.property(EducationOrgUnit.developForm().fromAlias("ou")), developFormList));

        if (developConditionList != null)
            builder.where(DQLExpressions.in(DQLExpressions.property(EducationOrgUnit.developCondition().fromAlias("ou")), developConditionList));

        if (developTechList != null)
            builder.where(DQLExpressions.in(DQLExpressions.property(EducationOrgUnit.developTech().fromAlias("ou")), developTechList));

        if (developPeriodList != null)
            builder.where(DQLExpressions.in(DQLExpressions.property(EducationOrgUnit.developPeriod().fromAlias("ou")), developPeriodList));

        return builder.createStatement(getSession()).list();
    }

    private void accumulateRateRow(EcgpDistribution distribution, List<TargetAdmissionKind> targetAdmissionKindList, Map<CompetitionKind, Integer> competitionKindPriorityMap, Set<Long> usedEntrantIds, IEcgQuotaManager quotaManager, IEcgEntrantRateRowDTO.Rule rateRule, List<Long> directionList)
    {
        // получаем строки рейтинга
        List<? extends IEcgEntrantRateRowDTO> rateList = getEntrantRateRowList(distribution, targetAdmissionKindList, competitionKindPriorityMap, rateRule);

        // общее число строк
        int len = rateList.size();

        int i = 0;

        // общее число свободных мест
        int freeTotal = quotaManager.getFreeTotal();

        while (i < len && freeTotal > 0)
        {
            IEcgEntrantRateRowDTO rateRowDTO = rateList.get(i);

            if (!usedEntrantIds.contains(rateRowDTO.getId()))
            {
                IEcgPlanChoiceResult choiceResult = quotaManager.getPossibleDirectionList(rateRowDTO, null);

                IEcgEntrantRateDirectionDTO chosenDirection = choiceResult.getChosenDirection();

                if (chosenDirection != null)
                {
                    freeTotal--;
                    directionList.add(chosenDirection.getId());
                    usedEntrantIds.add(rateRowDTO.getId());
                }
            }

            i++;
        }
    }
}
