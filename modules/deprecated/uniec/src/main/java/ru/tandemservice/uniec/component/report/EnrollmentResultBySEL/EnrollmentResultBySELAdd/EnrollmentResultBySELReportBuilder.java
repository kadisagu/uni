package ru.tandemservice.uniec.component.report.EnrollmentResultBySEL.EnrollmentResultBySELAdd;

import org.hibernate.Session;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantDataUtil;

import java.util.*;

/**
 * @author oleyba
 * @since 16.09.2009
 */
class EnrollmentResultBySELReportBuilder
{
    private Model model;
    private Session session;

    private static final DoubleFormatter doubleFormatter = new DoubleFormatter(1, true);

    public EnrollmentResultBySELReportBuilder(Model model, Session session)
    {
        this.model = model;
        this.session = session;
    }

    public DatabaseFile getContent()
    {
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ENROLLMENT_RESULT_BY_SEL);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());
        TopOrgUnit academy = TopOrgUnit.getInstance();

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("academyTitle", academy.getNominativeCaseTitle() == null ? academy.getTitle() == null ? "" : academy.getTitle() : academy.getNominativeCaseTitle());
        injectModifier.put("year", Integer.toString(model.getReport().getEnrollmentCampaign().getEducationYear().getIntValue()));
        injectModifier.put("developForm", getDevelopFormTitle(model.getReport().getDevelopForm()));
        injectModifier.modify(document);

        List<EnrollmentDirection> enrollmentDirectionList = MultiEnrollmentDirectionUtil.getEnrollmentDirections(model, session);
        if (model.isQualificationActive())
            enrollmentDirectionList = filterByQualification(enrollmentDirectionList);

        Map<Long, ReportRow> budgetTableRows = new HashMap<>();
        Map<Long, ReportRow> contractTableRows = new HashMap<>();

        for (RequestedEnrollmentDirection direction : getDirections(enrollmentDirectionList))
        {
            Map<Long, ReportRow> rowMap = direction.getCompensationType().isBudget() ? budgetTableRows : contractTableRows;
            EducationLevels level = getGroupLevel(direction.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel());
            ReportRow row = rowMap.get(level.getId());
            if (null == row)
                rowMap.put(level.getId(), row = new ReportRow(level.getDisplayableTitle()));
            row.requestId.add(direction.getEntrantRequest().getId());
        }

        EntrantDataUtil util = new EntrantDataUtil(session, model.getReport().getEnrollmentCampaign(), getPreliminaryDirectionBuilder(enrollmentDirectionList));
        List<Long> notEmptyOlympiadDirectionIds = getNotEmptyOlympiadDirectionIds(getPreliminaryDirectionBuilder(enrollmentDirectionList));
        for (PreliminaryEnrollmentStudent student : getPreliminaryStudents(enrollmentDirectionList))
        {
            EducationLevels level = getGroupLevel(student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel());
            Map<Long, ReportRow> rowMap = student.getCompensationType().isBudget() ? budgetTableRows : contractTableRows;
            ReportRow row = rowMap.get(level.getId());
            if (null == row)
                rowMap.put(level.getId(), row = new ReportRow(level.getDisplayableTitle()));
            row.entered++;
            // и во вторую таблицу в колонку "Всего" тоже считаем о.О
            //            {
            //                Map<Long, ReportRow> secondRowMap = !student.getCompensationType().isBudget() ? budgetTableRows : contractTableRows;
            //                ReportRow secondRow = secondRowMap.get(level.getId());
            //                if (null == secondRow)
            //                    secondRowMap.put(level.getId(), secondRow = new ReportRow(level.getDisplayableTitle()));
            //                secondRow.entered++;
            //            }
            RequestedEnrollmentDirection direction = student.getRequestedEnrollmentDirection();
            String compKind = direction.getCompetitionKind().getCode();
            if (UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION.equals(compKind))
                row.enteredCompetetive++;
            if (UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION.equals(compKind))
                row.enteredBeyondCompetiton++;
            if (notEmptyOlympiadDirectionIds.contains(direction.getId()))
                row.enteredOlympiad++;
            if (student.isTargetAdmission())
                row.enteredTarget++;
            Set<ChosenEntranceDiscipline> entranceDisciplines = util.getChosenEntranceDisciplineSet(direction);
            for (ChosenEntranceDiscipline discipline : entranceDisciplines)
                if (discipline.getFinalMark() != null && (discipline.getFinalMarkSource() == UniecDefines.FINAL_MARK_STATE_EXAM_WAVE1
                        || discipline.getFinalMarkSource() == UniecDefines.FINAL_MARK_STATE_EXAM_WAVE2
                        || discipline.getFinalMarkSource() == UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL
                        || discipline.getFinalMarkSource() == UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL_APPEAL))
                {
                    row.totalMark += discipline.getFinalMark();
                    row.markCount++;
                }
        }

        fillTable(document, budgetTableRows, "TB");
        fillTable(document, contractTableRows, "TC");

        DatabaseFile file = new DatabaseFile();
        file.setContent(RtfUtil.toByteArray(document));
        return file;
    }

    private EducationLevels getGroupLevel(EducationLevels educationLevel)
    {
        while (educationLevel.getParentLevel() != null)
            educationLevel = educationLevel.getParentLevel();
        return educationLevel;
    }

    @SuppressWarnings("unchecked")
    private void fillTable(RtfDocument document, Map<Long, ReportRow> tableRows, String tableKey)
    {
        List<ReportRow> rowList = new ArrayList<>(tableRows.values());
        Collections.sort(rowList);
        List<String[]> tableData = new ArrayList<>();
        for (ReportRow row : rowList)
            tableData.add(row.format());
        ReportRow total = new ReportRow("");
        long requestsTotal = total.sum(rowList);

        RtfTableModifier tableModifier = new RtfTableModifier();

        tableModifier.put(tableKey, tableData.toArray(new String[tableData.size()][]));
        tableModifier.modify(document);

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put(tableKey + "1", String.valueOf(requestsTotal));
        injectModifier.put(tableKey + "2", String.valueOf(total.enteredCompetetive));
        injectModifier.put(tableKey + "3", String.valueOf(total.enteredOlympiad));
        injectModifier.put(tableKey + "4", String.valueOf(total.enteredBeyondCompetiton));
        injectModifier.put(tableKey + "5", String.valueOf(total.enteredTarget));
        injectModifier.put(tableKey + "6", String.valueOf(total.entered));
        injectModifier.modify(document);
    }

    private List<EnrollmentDirection> filterByQualification(List<EnrollmentDirection> enrollmentDirectionList)
    {
        List<EnrollmentDirection> result = new ArrayList<>();
        for (EnrollmentDirection direction : enrollmentDirectionList)
            if (model.getQualificationList().contains(direction.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getQualification()))
                result.add(direction);
        return result;
    }

    private List<RequestedEnrollmentDirection> getDirections(List<EnrollmentDirection> enrollmentDirectionList)
    {
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "r");
        builder.add(UniMQExpression.betweenDate("r", RequestedEnrollmentDirection.P_REG_DATE, model.getReport().getDateFrom(), model.getReport().getDateTo()));
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, Boolean.FALSE));
        builder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        if (model.isStudentCategoryActive())
            builder.add(MQExpression.in("r", RequestedEnrollmentDirection.L_STUDENT_CATEGORY, model.getStudentCategoryList()));
        builder.add(MQExpression.in("r", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, enrollmentDirectionList));
        return builder.getResultList(session);
    }

    private List<PreliminaryEnrollmentStudent> getPreliminaryStudents(List<EnrollmentDirection> enrollmentDirectionList)
    {
        MQBuilder builder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "p");
        if (model.isStudentCategoryActive())
            builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.L_STUDENT_CATEGORY, model.getStudentCategoryList()));
        if (model.isParallelActive() && model.getParallel().isTrue())
            builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.P_PARALLEL, false));

        builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT, CommonBaseEntityUtil.getPropertiesSet(enrollmentDirectionList, EnrollmentDirection.L_EDUCATION_ORG_UNIT)));
        builder.addJoin("p", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, "r");
        builder.add(UniMQExpression.betweenDate("r", RequestedEnrollmentDirection.P_REG_DATE, model.getReport().getDateFrom(), model.getReport().getDateTo()));
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_ENROLLMENT_CAMPAIGN, model.getReport().getEnrollmentCampaign()));
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, Boolean.FALSE));
        builder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        return builder.getResultList(session);
    }

    private MQBuilder getPreliminaryDirectionBuilder(List<EnrollmentDirection> enrollmentDirectionList)
    {
        MQBuilder builder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "p");
        if (model.isStudentCategoryActive())
            builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.L_STUDENT_CATEGORY, model.getStudentCategoryList()));
        if (model.isParallelActive() && model.getParallel().isTrue())
            builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.P_PARALLEL, false));

        builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT, CommonBaseEntityUtil.getPropertiesSet(enrollmentDirectionList, EnrollmentDirection.L_EDUCATION_ORG_UNIT)));
        builder.addJoin("p", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, "r");
        builder.add(UniMQExpression.betweenDate("r", RequestedEnrollmentDirection.P_REG_DATE, model.getReport().getDateFrom(), model.getReport().getDateTo()));
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_ENROLLMENT_CAMPAIGN, model.getReport().getEnrollmentCampaign()));
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, Boolean.FALSE));
        builder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        builder.getSelectAliasList().clear();
        builder.addSelect("r");
        return builder;
    }

    private List<Long> getNotEmptyOlympiadDirectionIds(MQBuilder directionBuilder)
    {
        MQBuilder builder = new MQBuilder(ChosenEntranceDiscipline.ENTITY_CLASS, "chosen", new String[]{
                ChosenEntranceDiscipline.L_CHOSEN_ENROLLMENT_DIRECTION + ".id"});
        builder.add(MQExpression.in("chosen", ChosenEntranceDiscipline.L_CHOSEN_ENROLLMENT_DIRECTION, directionBuilder));
        builder.add(MQExpression.isNotNull("chosen", ChosenEntranceDiscipline.P_FINAL_MARK));
        builder.add(MQExpression.eq("chosen", ChosenEntranceDiscipline.P_FINAL_MARK_SOURCE, UniecDefines.FINAL_MARK_OLYMPIAD));
        builder.setNeedDistinct(true);
        return builder.<Long>getResultList(session);
    }

    private static String getDevelopFormTitle(DevelopForm developForm)
    {
        String code = developForm.getCode();
        if (DevelopFormCodes.FULL_TIME_FORM.equals(code))
            return "очной";
        if (DevelopFormCodes.CORESP_FORM.equals(code))
            return "заочной";
        return "очно-заочной";
    }

    @SuppressWarnings("unchecked")
    private static class ReportRow implements Comparable
    {
        private String title;

        private Set<Long> requestId = new HashSet<>();

        private long enteredCompetetive;
        private long enteredOlympiad;
        private long enteredBeyondCompetiton;
        private long enteredTarget;
        private long entered;

        private double totalMark;
        private long markCount;

        private ReportRow(String title)
        {
            this.title = title;
        }

        @Override
        public int compareTo(Object o)
        {
            return title.compareTo(((ReportRow) o).title);
        }

        public long sum(List<ReportRow> rowList)
        {
            long result = 0;
            for (ReportRow row : rowList)
            {
                result = result + row.requestId.size();
                enteredCompetetive = enteredCompetetive + row.enteredCompetetive;
                enteredOlympiad = enteredOlympiad + row.enteredOlympiad;
                enteredBeyondCompetiton = enteredBeyondCompetiton + row.enteredBeyondCompetiton;
                enteredTarget = enteredTarget + row.enteredTarget;
                entered = entered + row.entered;
            }
            return result;
        }

        public String[] format()
        {
            return new String[]{
                    title,
                    String.valueOf(requestId.size()),
                    String.valueOf(enteredCompetetive),
                    String.valueOf(enteredOlympiad),
                    String.valueOf(enteredBeyondCompetiton),
                    String.valueOf(enteredTarget),
                    String.valueOf(entered),
                    markCount == 0 ? "" : doubleFormatter.format(totalMark / markCount)
            };
        }
    }
}
