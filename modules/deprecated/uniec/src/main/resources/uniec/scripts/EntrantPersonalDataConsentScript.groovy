package uniec.scripts

import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.shared.organization.base.entity.OrgUnit
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.entity.Person
import ru.tandemservice.uniec.entity.entrant.EntrantRequest

/**
 * @author Andrey Andreev
 * @since 25.11.2016
 */
return new EntrantPersonalDataConsentScriptPrint(
        _template: template,                      // Шаблон
        _entrantRequestId: entrantRequestId,      // Заявление абитуриента
        ).print()

class EntrantPersonalDataConsentScriptPrint
{
    byte[] _template
    Long _entrantRequestId

    def print()
    {
        def dao = DataAccessServices.dao()

        RtfInjectModifier modifier = new RtfInjectModifier()
        RtfDocument document = new RtfReader().read(_template)
        EntrantRequest entrantRequest = dao.get(EntrantRequest.class, _entrantRequestId);

        Person person = entrantRequest.entrant.person
        OrgUnit topOrgUnit = TopOrgUnit.getInstance()

        modifier.put("FIO", person.fullFio)
        modifier.put("topOrgUnit", topOrgUnit.dativeCaseTitle == null ? topOrgUnit.printTitle : topOrgUnit.dativeCaseTitle)
        modifier.put("topOrgUnitShort", topOrgUnit.shortTitle)
        modifier.put("passport", person.identityCard.title)
        modifier.put("registrationAddress", person.addressRegistration != null ? person.addressRegistration.titleWithFlat : "")
        modifier.put("date", DateFormatter.DEFAULT_DATE_FORMATTER.format(entrantRequest.getRegDate()))

        modifier.modify(document)

        return [document: document.toByteArray(), fileName: 'PersonalDataConsent.rtf']
    }
}