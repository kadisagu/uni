package uniec.scripts

import jxl.Workbook
import jxl.WorkbookSettings
import jxl.format.PageOrientation
import jxl.write.biff.RowsExceededException
import org.hibernate.Session
import org.tandemframework.core.exception.ApplicationException
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.shared.fias.base.entity.Address
import org.tandemframework.shared.fias.base.entity.AddressCountry
import org.tandemframework.shared.fias.base.entity.AddressItem
import org.tandemframework.shared.fias.base.entity.AddressStreet
import org.tandemframework.shared.organization.base.entity.OrgUnit
import org.tandemframework.shared.person.base.entity.IdentityCard
import org.tandemframework.shared.person.base.entity.Person
import org.tandemframework.shared.person.base.entity.PersonContactData
import org.tandemframework.shared.person.base.entity.PersonRole
import org.tandemframework.shared.person.catalog.entity.Sex
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes
import ru.tandemservice.uni.UniDefines
import ru.tandemservice.uni.entity.catalog.CompensationType
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool
import ru.tandemservice.uni.entity.education.EducationOrgUnit
import ru.tandemservice.uniec.entity.orders.AbstractEntrantExtract
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract
import jxl.write.*
import org.tandemframework.hibsupport.dql.*
import ru.tandemservice.uniec.entity.entrant.*

/**
 * Created by IntelliJ IDEA.
 * User: azhebko
 * Date: 7/30/12
 * Time: 12:07 PM
 * To change this template use File | Settings | File Templates.
 */

return new AdmittedEntrantsDataExportPrint(
    session: session,
    campaign: session.get(EnrollmentCampaign.class, campaignId),
    dateFrom: dateFrom,
    dateTo: dateTo
).print()


class AdmittedEntrantsDataExportPrint
{
    Session session
    EnrollmentCampaign campaign
    Date dateFrom
    Date dateTo

    def print()
    {
        List<Object[]> list = getStudentList();

        // создаем печатную форму
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        WritableWorkbook workbook = Workbook.createWorkbook(out, ws);

        WritableSheet sheet = workbook.createSheet("Экспорт", 0);
        sheet.getSettings().setOrientation(PageOrientation.LANDSCAPE);
        sheet.getSettings().setCopies(1);

        // Формат шрифта
        WritableFont arial8 = new WritableFont(WritableFont.ARIAL, 8);
        WritableFont arial10Bold = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);

        // Формат ячейки
        WritableCellFormat arial8Format = new WritableCellFormat(arial8);
        WritableCellFormat arial10BoldFormat = new WritableCellFormat(arial10Bold);

        // Задаем ширину колонок
        sheet.setColumnView(0, 15);
        sheet.setColumnView(1, 15);
        sheet.setColumnView(2, 18);
        sheet.setColumnView(3, 7);
        sheet.setColumnView(4, 12);
        sheet.setColumnView(5, 7);
        sheet.setColumnView(6, 7);
        sheet.setColumnView(7, 10);
        sheet.setColumnView(8, 60);
        sheet.setColumnView(9, 7);
        sheet.setColumnView(10, 7);
        sheet.setColumnView(11, 10);
        sheet.setColumnView(12, 30);
        sheet.setColumnView(13, 8);
        sheet.setColumnView(14, 8);
        sheet.setColumnView(15, 30);
        sheet.setColumnView(16, 20);
        sheet.setColumnView(17, 40);
        sheet.setColumnView(18, 14);
        sheet.setColumnView(19, 14);

        // Заполняем шапку отчета
        sheet.addCell(new Label(0, 0, "FAM", arial10BoldFormat));
        sheet.addCell(new Label(1, 0, "IMA", arial10BoldFormat));
        sheet.addCell(new Label(2, 0, "OTC", arial10BoldFormat));
        sheet.addCell(new Label(3, 0, "LICHD", arial10BoldFormat));
        sheet.addCell(new Label(4, 0, "FACUL", arial10BoldFormat));
        sheet.addCell(new Label(5, 0, "PAS_S", arial10BoldFormat));
        sheet.addCell(new Label(6, 0, "PAS_N", arial10BoldFormat));
        sheet.addCell(new Label(7, 0, "PAS_D", arial10BoldFormat));
        sheet.addCell(new Label(8, 0, "PAS_V", arial10BoldFormat));
        sheet.addCell(new Label(9, 0, "KONT", arial10BoldFormat));
        sheet.addCell(new Label(10, 0, "POL", arial10BoldFormat));
        sheet.addCell(new Label(11, 0, "DATR", arial10BoldFormat));
        sheet.addCell(new Label(12, 0, "MESROJ", arial10BoldFormat));
        sheet.addCell(new Label(13, 0, "GRAJD", arial10BoldFormat));
        sheet.addCell(new Label(14, 0, "IND", arial10BoldFormat));
        sheet.addCell(new Label(15, 0, "RESP", arial10BoldFormat));
        sheet.addCell(new Label(16, 0, "GOROD", arial10BoldFormat));
        sheet.addCell(new Label(17, 0, "UL", arial10BoldFormat));
        sheet.addCell(new Label(18, 0, "TEL", arial10BoldFormat));
        sheet.addCell(new Label(19, 0, "SOT_TEL", arial10BoldFormat));

        // Заполняем строки отчета
        int i = 1;

        try {
            for (Object[] row : list) {
                sheet.addCell(new Label(0, i, (String) row[0], arial8Format));   // Фамилия студента
                sheet.addCell(new Label(1, i, (String) row[1], arial8Format));   // Имя студента
                sheet.addCell(new Label(2, i, (String) row[2], arial8Format));   // Отчество студента
                sheet.addCell(new Label(3, i, String.valueOf(row[3]), arial8Format));   // Номер заявления абитуриента
                sheet.addCell(new Label(4, i, (String) row[4], arial8Format));   // Сокр. название формирующего подразделения
                sheet.addCell(new Label(5, i, (String) row[5], arial8Format));   // Серия УЛ
                sheet.addCell(new Label(6, i, (String) row[6], arial8Format));   // Номер УЛ
                sheet.addCell(new Label(7, i, DateFormatter.DEFAULT_DATE_FORMATTER.format((Date) row[7]), arial8Format));   // Дата выдачи УЛ
                sheet.addCell(new Label(8, i, (String) row[8], arial8Format));   // Кем выдано УЛ
                sheet.addCell(new Label(9, i, (String) row[9], arial8Format));   // Обучение по контракту
                sheet.addCell(new Label(10, i, (String) row[10], arial8Format));    // Пол
                sheet.addCell(new Label(11, i, DateFormatter.DEFAULT_DATE_FORMATTER.format((Date) row[11]), arial8Format));    // Дата рождения
                sheet.addCell(new Label(12, i, (String) row[12], arial8Format));    // Место рождения
                sheet.addCell(new Label(13, i, (String) row[13], arial8Format));    // Гражданство
                sheet.addCell(new Label(14, i, (String) row[14], arial8Format));    // Почтовый индекс
                sheet.addCell(new Label(15, i, row[15] != null ? getRegion((Long) row[15]) : null, arial8Format));    // Область
                sheet.addCell(new Label(16, i, (String) row[16], arial8Format));    // Город
                sheet.addCell(new Label(17, i, (String) row[17], arial8Format));    // Адрес
                sheet.addCell(new Label(18, i, (String) row[18], arial8Format));    // Телефон
                sheet.addCell(new Label(19, i, (String) row[19], arial8Format));    // Сотовый телефон

                i++;
            }
        } catch (RowsExceededException e) {
            throw new ApplicationException("Невозможно сформировать отчет, так как объем итоговых данных превышает допустимый предел в 65536 строк.");
        }

        workbook.write();
        workbook.close();

        return [document: out.toByteArray(), fileName: 'Экспорт данных о зачисленных абитуриентах.xls'];
    }

    public List<Object[]> getStudentList()
    {

        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder
                .fromEntity(PreliminaryEnrollmentStudent.class, "a")
                .joinEntity(
                        "a",
                        DQLJoinType.left,
                        EducationOrgUnit.class,
                        "f",
                        DQLExpressions.eq(DQLExpressions.property(PreliminaryEnrollmentStudent.educationOrgUnit().id().fromAlias("a")), DQLExpressions.property(EducationOrgUnit.id().fromAlias("f"))))
                .joinEntity(
                        "f",
                        DQLJoinType.left,
                        EducationLevelsHighSchool.class,
                        "g",
                        DQLExpressions.eq(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().id().fromAlias("f")), DQLExpressions.property(EducationLevelsHighSchool.id().fromAlias("g"))))
                .joinEntity(
                        "f",
                        DQLJoinType.left,
                        OrgUnit.class,
                        "h",
                        DQLExpressions.eq(DQLExpressions.property(EducationOrgUnit.formativeOrgUnit().id().fromAlias("f")), DQLExpressions.property(OrgUnit.id().fromAlias("h"))))
                .joinEntity(
                        "a",
                        DQLJoinType.left,
                        CompensationType.class,
                        "ct",
                        DQLExpressions.eq(DQLExpressions.property(PreliminaryEnrollmentStudent.compensationType().id().fromAlias("a")), DQLExpressions.property(CompensationType.id().fromAlias("ct"))))
                .joinEntity(
                        "a",
                        DQLJoinType.inner,
                        RequestedEnrollmentDirection.class,
                        "red",
                        DQLExpressions.eq(DQLExpressions.property(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().id().fromAlias("a")), DQLExpressions.property(RequestedEnrollmentDirection.id().fromAlias("red"))))
                .joinEntity(
                        "red",
                        DQLJoinType.inner,
                        EntrantRequest.class,
                        "er",
                        DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().id().fromAlias("red")), DQLExpressions.property(EntrantRequest.id().fromAlias("er"))))
                .joinEntity(
                        "er",
                        DQLJoinType.inner,
                        Entrant.class,
                        "entr",
                        DQLExpressions.eq(DQLExpressions.property(EntrantRequest.entrant().id().fromAlias("er")), DQLExpressions.property(Entrant.id().fromAlias("entr"))))
                .joinEntity(
                        "a",
                        DQLJoinType.left,
                        AbstractEntrantExtract.class,
                        "aee",
                        DQLExpressions.eq(DQLExpressions.property(PreliminaryEnrollmentStudent.id().fromAlias("a")), DQLExpressions.property(AbstractEntrantExtract.entity().id().fromAlias("aee"))))
                .joinEntity(
                        "aee",
                        DQLJoinType.left,
                        EnrollmentExtract.class,
                        "ee",
                        DQLExpressions.eq(DQLExpressions.property(AbstractEntrantExtract.id().fromAlias("aee")), DQLExpressions.property(EnrollmentExtract.id().fromAlias("ee"))))
                .joinEntity(
                        "entr",
                        DQLJoinType.left,
                        PersonRole.class,
                        "c",
                        DQLExpressions.eq(DQLExpressions.property(Entrant.id().fromAlias("entr")), DQLExpressions.property(PersonRole.id().fromAlias("c"))))
                .joinEntity(
                        "c",
                        DQLJoinType.left,
                        Person.class,
                        "d",
                        DQLExpressions.eq(DQLExpressions.property(PersonRole.person().id().fromAlias("c")), DQLExpressions.property(Person.id().fromAlias("d"))))
                .joinEntity(
                        "d",
                        DQLJoinType.left,
                        IdentityCard.class,
                        "e",
                        DQLExpressions.eq(DQLExpressions.property(Person.id().fromAlias("d")), DQLExpressions.property(IdentityCard.person().id().fromAlias("e"))))
                .joinEntity(
                        "e",
                        DQLJoinType.left,
                        Sex.class,
                        "s",
                        DQLExpressions.eq(DQLExpressions.property(IdentityCard.sex().id().fromAlias("e")), DQLExpressions.property(Sex.id().fromAlias("s"))))
                .joinEntity(
                        "e",
                        DQLJoinType.left,
                        Address.class,
                        "adr",
                        DQLExpressions.eq(DQLExpressions.property(IdentityCard.address().id().fromAlias("e")), DQLExpressions.property(Address.id().fromAlias("adr"))))
                .joinEntity(
                        "adr",
                        DQLJoinType.left,
                        AddressCountry.class,
                        "ac",
                        DQLExpressions.eq(DQLExpressions.property(Address.country().id().fromAlias("adr")), DQLExpressions.property(AddressCountry.id().fromAlias("ac"))))
                .joinEntity(
                        "adr",
                        DQLJoinType.left,
                        AddressItem.class,
                        "adrs",
                        DQLExpressions.eq(DQLExpressions.property(Address.settlement().id().fromAlias("adr")), DQLExpressions.property(AddressItem.id().fromAlias("adrs"))))
                .joinEntity(
                        "adr",
                        DQLJoinType.left,
                        AddressStreet.class,
                        "adrst",
                        DQLExpressions.eq(DQLExpressions.property(Address.street().id().fromAlias("adr")), DQLExpressions.property(AddressStreet.id().fromAlias("adrst"))))
                .joinEntity(
                        "d",
                        DQLJoinType.left,
                        Address.class,
                        "adrf",
                        DQLExpressions.eq(DQLExpressions.property(Person.address().id().fromAlias("d")), DQLExpressions.property(Address.id().fromAlias("adrf"))))
                .joinEntity(
                        "d",
                        DQLJoinType.left,
                        PersonContactData.class,
                        "pcd",
                        DQLExpressions.eq(DQLExpressions.property(Person.contactData().id().fromAlias("d")), DQLExpressions.property(PersonContactData.id().fromAlias("pcd"))))

                .column(DQLExpressions.property(IdentityCard.lastName().fromAlias("e")), "FAM")
                .column(DQLExpressions.property(IdentityCard.firstName().fromAlias("e")), "IMA")
                .column(DQLExpressions.property(IdentityCard.middleName().fromAlias("e")), "OTC")
                .column(DQLExpressions.property(EntrantRequest.regNumber().fromAlias("er")), "LICHD")
                .column(DQLExpressions.property(OrgUnit.shortTitle().fromAlias("h")), "FACUL")
                .column(DQLExpressions.property(IdentityCard.seria().fromAlias("e")), "PAS_S")
                .column(DQLExpressions.property(IdentityCard.number().fromAlias("e")), "PAS_N")
                .column(DQLExpressions.property(IdentityCard.issuanceDate().fromAlias("e")), "PAS_D")
                .column(DQLExpressions.property(IdentityCard.issuancePlace().fromAlias("e")), "PAS_V")
                .column(DQLExpressions.caseExpr(
                        [DQLExpressions.eq(DQLExpressions.property(CompensationType.code().fromAlias("ct")), DQLExpressions.value(UniDefines.COMPENSATION_TYPE_CONTRACT))] as IDQLExpression[],
                        [DQLExpressions.value("да")] as IDQLExpression[],
                        DQLExpressions.value("нет")), "KONT")
                .column(DQLExpressions.caseExpr(
                        [DQLExpressions.eq(DQLExpressions.property(Sex.code().fromAlias("s")), DQLExpressions.value(SexCodes.MALE))] as IDQLExpression[],
                        [DQLExpressions.value("муж")] as IDQLExpression[],
                        DQLExpressions.value("жен")), "POL")
                .column(DQLExpressions.property(IdentityCard.birthDate().fromAlias("e")), "DATR")
                .column(DQLExpressions.property(IdentityCard.birthPlace().fromAlias("e")), "MESROJ")
                .column(DQLExpressions.property(AddressCountry.shortTitle().fromAlias("ac")), "GRAJD")
                .column(DQLExpressions.property(Address.inheritedPostCode().fromAlias("adr")), "IND")
                .column(DQLExpressions.property(Address.id().fromAlias("adr")), "NAS_PUNKT")
                .column(DQLExpressions.property(AddressItem.title().fromAlias("adrs")), "GOROD")
                .column(DQLExpressions.caseExpr(
                        [DQLExpressions.isNotNull(Address.houseUnitNumber().fromAlias("adr"))] as IDQLExpression[],
                        [DQLFunctions.concat(
                                DQLExpressions.property(AddressStreet.title().fromAlias("adrst")),
                                DQLExpressions.value(", корп."),
                                DQLExpressions.property(Address.houseUnitNumber().fromAlias("adr")),
                                DQLExpressions.value(", д."),
                                DQLExpressions.property(Address.houseNumber().fromAlias("adr")),
                                DQLExpressions.value(", кв."),
                                DQLExpressions.property(Address.flatNumber().fromAlias("adr")))] as IDQLExpression[],
                        DQLFunctions.concat(
                                DQLExpressions.property(AddressStreet.title().fromAlias("adrst")),
                                DQLExpressions.value(", д."),
                                DQLExpressions.property(Address.houseNumber().fromAlias("adr")),
                                DQLExpressions.value(", кв."),
                                DQLExpressions.property(Address.flatNumber().fromAlias("adr")))), "UL")

                .column(DQLExpressions.property(PersonContactData.phoneFact().fromAlias("pcd")), "TEL")
                .column(DQLExpressions.property(PersonContactData.phoneMobile().fromAlias("pcd")), "SOT_TEL")

                .where(DQLExpressions.eq(DQLExpressions.property(Entrant.enrollmentCampaign().fromAlias("entr")), DQLExpressions.value(campaign)))
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentExtract.committed().fromAlias("ee")), DQLExpressions.value(java.lang.Boolean.TRUE)))
        ;

        if (dateFrom != null)
            builder.where(DQLExpressions.ge(DQLExpressions.property(AbstractEntrantExtract.paragraph().order().commitDate().fromAlias("aee")), DQLExpressions.valueTimestamp(dateFrom)));

        if (dateTo != null)
            builder.where(DQLExpressions.le(DQLExpressions.property(AbstractEntrantExtract.paragraph().order().commitDate().fromAlias("aee")), DQLExpressions.valueTimestamp(dateTo)));
        ;

        builder
                .order(DQLExpressions.property(IdentityCard.lastName().fromAlias("e")))
                .order(DQLExpressions.property(IdentityCard.firstName().fromAlias("e")))
                .order(DQLExpressions.property(IdentityCard.middleName().fromAlias("e")))
        ;

        return builder.createStatement(session).list();
    }

    def static getRegion(long addressItemId)
    {
        Address address = DataAccessServices.dao().get(Address, addressItemId);
        if (address.settlement == null) return;
        AddressItem item = address.settlement;
        for (; item.parent != null;)
            item = item.parent;
        return item.fullTitle;
    }
}