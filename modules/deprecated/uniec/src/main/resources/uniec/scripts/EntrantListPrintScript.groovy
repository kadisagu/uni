/* $Id$ */
// Copyright 2006-2012 Tandem Service Software
package uniec.scripts

import org.hibernate.Session
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfUtil
import ru.tandemservice.uniec.entity.entrant.Entrant
import ru.tandemservice.uniec.entity.entrant.EntrantRequest

return new EntrantListPrint(     // стандартные входные параметры скрипта
        session: session,        // сессия
        template: template,      // шаблон
        entrantIds: object,      // объект печати
        headerTable: headerTable // таблица значений фильтров 
).print()

/**
 * Алгоритм печати списка абитуриентов
 *
 * @author Vasily Zhukov
 * @since 23.02.2012
 */
class EntrantListPrint
{
    Session session
    byte[] template
    List<Long> entrantIds
    String[][] headerTable
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()

    def print()
    {
        def current = new Date().format('dd.MM.yyyy')
        im.put('date', current)
        tm.put('HT', headerTable)

        def entrantTableMap = new HashMap<Long, String[]>()

        // анализируем список абитуриентов блоками
        int batchSize = 1000
        for (int i = 0; i < entrantIds.size(); i += batchSize)
        {
            def ids = entrantIds.subList(i, Math.min(entrantIds.size(), i + batchSize)).join(', ')

            // получаем список абитуриентов и список их номеров заявлений
            def data = DQL.createStatement(session, /
                    select e, r.regNumber
                    from ${Entrant.class.simpleName} e
                    left join ${EntrantRequest.class.simpleName} r with e.id=r.entrant.id 
                    where e.id in (${ids})
            /).<Object[]> list().groupBy { it[0] as Entrant }

            // для каждого абитуриента создаем строку в таблице
            for (def entry: data.entrySet())
            {
                def entrant = entry.key
                def regNumberList = entry.value.collect {it[1] as Integer}.grep()

                entrantTableMap.put(entrant.id, [
                        null,                                          // №
                        entrant.personalNumber,                        // Личный №
                        regNumberList.join(', '),                      // № заявления
                        entrant.person.identityCard.fullFio,           // ФИО абитуриента
                        entrant.person.identityCard.sex.shortTitle,    // Пол
                        entrant.person.identityCard.fullNumber,        // Паспорт
                        entrant.state?.title,                          // Состояние
                        entrant.registrationDate.format('dd.MM.yyyy'), // Дата добавления
                        entrant.archival ? 'да' : ''                   // Архивный
                ] as String[])
            }
        }

        // заполняем таблицу абитуриентов порядке полученных id
        def entrantTable = []
        int i = 1
        for (def entrantId: entrantIds)
        {
            def row = entrantTableMap.get(entrantId)
            row[0] = i++
            entrantTable.add(row)
        }
        tm.put('T', entrantTable as String[][])

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(template, im, tm), fileName: "Список абитуриентов на ${current}.rtf"]
    }
}
