/**
 * $Id:$
 */
// Copyright 2006-2012 Tandem Service Software
package uniec.scripts

import com.google.common.collect.ArrayListMultimap
import com.google.common.collect.Multimap
import jxl.Workbook
import jxl.WorkbookSettings
import jxl.write.Label
import org.hibernate.Session
import org.tandemframework.core.util.BatchUtils
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLJoinType
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.shared.fias.base.entity.AddressBase
import org.tandemframework.shared.fias.base.entity.AddressDetailed
import org.tandemframework.shared.fias.base.entity.AddressRu
import org.tandemframework.shared.person.base.entity.IdentityCard
import org.tandemframework.shared.person.base.entity.Person
import org.tandemframework.shared.person.base.entity.PersonForeignLanguage
import org.tandemframework.shared.person.base.entity.PersonNextOfKin
import org.tandemframework.shared.person.catalog.entity.codes.GraduationHonourCodes
import org.tandemframework.shared.person.catalog.entity.codes.RelationDegreeCodes
import ru.tandemservice.uni.entity.education.EducationOrgUnit
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection
import ru.tandemservice.uniec.entity.entrant.Entrant
import ru.tandemservice.uniec.entity.entrant.EntrantRequest
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

return new EntrantDataExportPrint(                           // входные параметры скрипта
        session: session,                                    // сессия
        enrollmentCampaignId: enrollmentCampaignId,          // приемная компания
        requestFromLong: requestFromLong,                    // заявления с
        requestToLong: requestToLong,                        // заявления по
        compensationTypeId: compensationTypeId,              // вид возмещения затрат
        statusIdList: statusIdList,                          // состояние
        enrollmentDirectionIdList: enrollmentDirectionIdList // НПП
).print()

/**
 * Алгоритм печати отчета Экспорт данных абитуриентов
 *
 * @author Alexander Shaburov
 * @since 02.08.12
 */
class EntrantDataExportPrint
{
    // входные параметры
    Session session
    Long enrollmentCampaignId
    Long requestFromLong
    Long requestToLong
    Long compensationTypeId
    List<Long> statusIdList
    List<Long> enrollmentDirectionIdList
    // данные для печати, заполняются в методе prepareData
    def out = new ByteArrayOutputStream()
    List<RequestedEnrollmentDirection> directionList // список ВНП
    Map<Long, String> fatherMap // мапа Персоны на Места работы отца
    Map<Long, String> matherMap // мапа Персоны на Места работы матери
    Multimap<Long, String> foreignLanguageMap // мапа Персоны на список Иностранных языков персоны

    /**
     * Основной метод печати.
     */
    def print()
    {
        // создаем печатную форму
        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        def workbook = Workbook.createWorkbook(out, ws);

        // создаем лист
        def sheet = workbook.createSheet("Данные абитуриентов", 0);

        prepareData()

        printTable(sheet)

        workbook.write();
        workbook.close();

        // стандартные выходные параметры скрипта
        return [document: out.toByteArray(),
                fileName: "Report_" + new Date().format("dd.MM.yyyy") + ".xls"]
    }

    /**
     * Подготавливает данные для печати отчета.
     */
    def prepareData()
    {
        def builder = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "r")
                .column("r")
                .fetchPath(DQLJoinType.inner, RequestedEnrollmentDirection.entrantRequest().fromAlias("r"), "er")
                .fetchPath(DQLJoinType.inner, EntrantRequest.entrant().fromAlias("er"), "e")
                .fetchPath(DQLJoinType.inner, Entrant.person().fromAlias("e"), "p")
                .joinPath(DQLJoinType.inner, RequestedEnrollmentDirection.enrollmentDirection().fromAlias("r"), "ed")
                .where(eqValue(property("e", Entrant.archival()), false))
                .where(eqValue(property("ed", EnrollmentDirection.enrollmentCampaign()), enrollmentCampaignId))
                .where(betweenDays(EntrantRequest.regDate().fromAlias("er"), new Date(requestFromLong), new Date(requestToLong)))

        if (compensationTypeId != null)
            builder.where(eqValue(property("r", RequestedEnrollmentDirection.compensationType()), compensationTypeId))

        if (statusIdList != null && !statusIdList.empty)
            builder.where(DQLExpressions.in(property("r", RequestedEnrollmentDirection.state()), statusIdList))

        builder.where(DQLExpressions.in("ed.id", enrollmentDirectionIdList))

        builder.joinPath(DQLJoinType.inner, Person.identityCard().fromAlias("p"), "i")
        builder.order(property("i", IdentityCard.P_LAST_NAME))
        builder.order(property("i", IdentityCard.P_FIRST_NAME))
        builder.order(property("i", IdentityCard.P_MIDDLE_NAME))

        directionList = builder.createStatement(session).list()

        def personIds = new HashSet<Long>(directionList.collect{ e -> e.entrantRequest.entrant.person.id})

        fatherMap = [:]
        matherMap = [:]
        foreignLanguageMap = ArrayListMultimap.create()

        BatchUtils.execute(personIds, 200, new BatchUtils.Action<Long>() {
            @Override
            void execute(Collection<Long> elements)
            {
                List<Object[]> personNextOfKinList = new DQLSelectBuilder().fromEntity(PersonNextOfKin.class, "n")
                        .column(property("n", PersonNextOfKin.person().id()))
                        .column(property("n", PersonNextOfKin.relationDegree().code()))
                        .column(property("n", PersonNextOfKin.P_EMPLOYMENT_PLACE))
                        .where(DQLExpressions.in(property("n", PersonNextOfKin.relationDegree().code()), RelationDegreeCodes.MOTHER, RelationDegreeCodes.FATHER))
                        .where(DQLExpressions.in(property("n", PersonNextOfKin.person()), elements))
                        .createStatement(session).list()

                for (def personNextOfKin : personNextOfKinList)
                {
                    switch ((String) personNextOfKin[1])
                    {
                        case RelationDegreeCodes.FATHER:
                            fatherMap.put((Long) personNextOfKin[0], (String) personNextOfKin[2])
                            break
                        case RelationDegreeCodes.MOTHER:
                            matherMap.put((Long) personNextOfKin[0], (String) personNextOfKin[2])
                            break
                    }
                }

                List<Object[]> personForeignLanguageList = new DQLSelectBuilder()
                        .fromEntity(PersonForeignLanguage.class, "l")
                        .column(property("l", PersonForeignLanguage.person().id()))
                        .column(property("l", PersonForeignLanguage.language().title()))
                        .where(DQLExpressions.in(property("l", PersonForeignLanguage.person()), elements))
                        .createStatement(session).list();

                for (def language : personForeignLanguageList)
                {
                    foreignLanguageMap.put((Long) language[0], (String) language[1])
                }
            }
        })
    }

    /**
     * Рисует таблицу
     * @param sheet лист
     */
    def printTable(def sheet)
    {
        int colN = 0;
        ["id", "facul", "fam", "ima", "otc", "UCHD", "Pas_s", "Pas_n", "Pas_d", "Pas_v", "Pol",
         "Fath_wor", "Moth_wor", "Shtook", "Godok", "Uchzav", "Nom_dip", "Medal", "Krdipl", "Inyaz",
         "Obcheg", "Mesroj", "Grajd", "Mesgit", "Ind", "Resp", "Gorod", "Ul", "Tel", "Sot_tel", "Mail", "Prim"
        ].each {
            sheet.addCell(new Label(colN++, 0, it))
        }

        int rowNumber = 1;
        for (RequestedEnrollmentDirection direction : directionList)
        {
            EntrantRequest request = direction.entrantRequest
            EducationOrgUnit educationOrgUnit = direction.enrollmentDirection.educationOrgUnit
            Person person = request.entrant.person
            IdentityCard identityCard = person.identityCard
            AddressBase address = identityCard.address

            sheet.addCell(new Label(0, rowNumber, request.entrant.personalNumber))
            sheet.addCell(new Label(1, rowNumber, educationOrgUnit.educationLevelHighSchool.shortTitle + ", " + educationOrgUnit.developForm.shortTitle + ", " + direction.compensationType.shortTitle))
            sheet.addCell(new Label(2, rowNumber, identityCard.lastName))
            sheet.addCell(new Label(3, rowNumber, identityCard.firstName))
            sheet.addCell(new Label(4, rowNumber, identityCard.middleName))
            sheet.addCell(new Label(5, rowNumber, request.regNumber.toString()))
            sheet.addCell(new Label(6, rowNumber, identityCard.seria))
            sheet.addCell(new Label(7, rowNumber, identityCard.number))
            sheet.addCell(new Label(8, rowNumber, identityCard.issuanceDate?.format("dd.MM.yyyy")))
            sheet.addCell(new Label(9, rowNumber, identityCard.issuancePlace))
            sheet.addCell(new Label(10, rowNumber, identityCard.sex.male ? "муж" : "жен"))
            sheet.addCell(new Label(11, rowNumber, fatherMap.get(person.id) ?: ""))
            sheet.addCell(new Label(12, rowNumber, matherMap.get(person.id) ?: ""))
            sheet.addCell(new Label(13, rowNumber, person.personEduInstitution?.eduInstitutionKind?.shortTitle))
            sheet.addCell(new Label(14, rowNumber, person.personEduInstitution?.yearEnd?.toString()))
            sheet.addCell(new Label(15, rowNumber, person.personEduInstitution?.eduInstitution?.title))
            sheet.addCell(new Label(16, rowNumber, [person.personEduInstitution?.seria ?: "", person.personEduInstitution?.number ?: ""].join(" ").trim()))
            sheet.addCell(new Label(17, rowNumber, Arrays.asList(GraduationHonourCodes.GOLD_MEDAL, GraduationHonourCodes.SILVER_MEDAL).contains(person.personEduInstitution?.graduationHonour?.code) ? "да" : "нет"))
            sheet.addCell(new Label(18, rowNumber, GraduationHonourCodes.RED_DIPLOMA.equals(person.personEduInstitution?.graduationHonour?.code) ? "да" : "нет"))
            sheet.addCell(new Label(19, rowNumber, foreignLanguageMap.get(person.id)?.join(" ") ?: ""))
            sheet.addCell(new Label(20, rowNumber, person.needDormitory ? "да" : "нет"))
            sheet.addCell(new Label(21, rowNumber, identityCard.birthPlace))
            sheet.addCell(new Label(22, rowNumber, identityCard.citizenship.shortTitle))
            sheet.addCell(new Label(23, rowNumber, getSettlement(address)))
            sheet.addCell(new Label(24, rowNumber, (address instanceof AddressRu) ? (address as AddressRu).inheritedPostCode : ""))
            sheet.addCell(new Label(25, rowNumber, getRegion(address)))
            sheet.addCell(new Label(26, rowNumber, address?.title))
            sheet.addCell(new Label(27, rowNumber, address?.shortTitleWithFlat ?: ""))
            sheet.addCell(new Label(28, rowNumber, person.contactData.phoneFact))
            sheet.addCell(new Label(29, rowNumber, person.contactData.phoneMobile))
            sheet.addCell(new Label(30, rowNumber, person.contactData.email))
            sheet.addCell(new Label(31, rowNumber++, request.entrant.additionalInfo))
        }
    }

    /**
     * @param address рег. адресс персоны
     * @return насел. пункт, либо пусто
     */
    static String getSettlement(AddressBase address)
    {
        def settlement = (address instanceof AddressDetailed) ? (address as AddressDetailed).settlement : null
        if (settlement == null)
            return ""

        switch (settlement.inheritedRegionCode)
        {
            case "77":
                return "Москва"
            case "50":
                return settlement.addressType.countryside ? "М.О. (село)" : "М.О. (город)"
        }
        return settlement.addressType.countryside ? "Др. село" : "Др. город"
    }

    /**
     * @param address рег. адресс персоны
     * @return регион
     */
    static String getRegion(AddressBase address)
    {
        def parent = (address instanceof AddressDetailed) ? (address as AddressDetailed).settlement : null
        def result = ""
        while (parent != null)
        {
            result = parent.title
            parent = parent.parent
        }
        return result;
    }
}
