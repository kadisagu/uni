/* $Id$ */
// Copyright 2006-2012 Tandem Service Software
package uniec.scripts

import org.hibernate.Session
import org.tandemframework.core.exception.ApplicationException
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil
import org.tandemframework.shared.commonbase.base.util.NumberSpellingUtil
import ru.tandemservice.uni.entity.catalog.DevelopForm
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool
import ru.tandemservice.uniec.entity.entrant.Entrant
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection
import org.tandemframework.shared.organization.base.entity.TopOrgUnit

return new EnrollmentExamListPrint(         // стандартные входные параметры скрипта
        session: session,                   // сессия
        template: template,                 // шаблон
        hs: !hsId ? null : (EducationLevelsHighSchool) session.get(EducationLevelsHighSchool.class, hsId),
        developForm: !developFormId ? null : (DevelopForm) session.get(DevelopForm.class, developFormId),
        examGroupTitle: examGroupTitle,
        disciplineTitle: disciplineTitle,
        examPassDate: examPassDate,
        table: table
).print()

/**
 * Алгоритм печати описи и расписки абитуриента
 *
 * @author Vasily Zhukov
 * @since 24.02.2012
 */
class EnrollmentExamListPrint
{
    Session session
    byte[] template
    EducationLevelsHighSchool hs     // значение фильтра 'Направление подготовки (специальность)'
    DevelopForm developForm          // значение фильтра 'Форма освоения'
    String examGroupTitle            // значение фильтра 'Экзам. группа'
    String disciplineTitle           // значение фильтра 'Дисциплина'
    Date examPassDate                // дата сдачи

    /**
     * cтроки таблицы, каждая строка - это map [поле -> значение]
     * все поля not-null, mark, paperCode, examiners может быть пустой строкой
     * <поле>                  <тип>      <описание>
     * entrantId               Long       идентификатор Entrant (Абитуриент)
     * disciplineId            Long       идентификатор ExamPassDiscipline (Дисциплина для сдачи)
     * mark                    String     оценка
     * paperCode               String     Шифр работы
     * examiners               String     ФИО экзаменаторов
     * directionIds            Set<Long>  идентификаторы RequestedEnrollmentDirection (Выбранное направление приема)
     */
    List<Map<String, Object>> table

    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()

    def print()
    {
        Set<String> commissions = new HashSet<>()

        def data = []
        for (def row: table)
        {
            Entrant entrant = (Entrant) session.get(Entrant.class, (Long) row['entrantId'])

            // если абитуриент не архивный и есть внп с состоянием не забрал документы, то печатаем такую строку
            if (!entrant.archival && ((Set<Long>) row['directionIds']).find {
                def direction = (RequestedEnrollmentDirection) session.get(RequestedEnrollmentDirection.class, it)
                !direction.entrantRequest.takeAwayDocument
            })
            {
                def paperCode = (String) row['paperCode']
                def mark = (String) row['mark']
                if (mark != null && mark.isNumber())
                    mark = mark + " (" + NumberSpellingUtil.spellNumberMasculineGender(Integer.parseInt(mark)) + ")"

                def male = entrant.person.identityCard.sex.male
                data.add([data.size() + 1,                                              // № п/п
                          entrant.person.identityCard.fullFio,                          // ФИО
                          paperCode,                                                    // Шифр работы
                          mark == 'н' ? (male ? 'не явился' : 'не явилась') : mark      // Оценки цифрой и прописью
                ] as String[])

                commissions.add((String) row['examiners'])
            }
        }

        if (data.empty)
            throw new ApplicationException('Нет абитуриентов для печати. Архивные, забравшие документы и ' +
                    'выбывшие из конкурса абитуриенты не попадают в ведомость при печати.')

        im.put('highSchoolTitle', TopOrgUnit.instance.title)
        im.put('educationOrgUnit', hs ? new RtfString().append(
                hs.educationLevel.levelType.specialityPrintTitleMode ? 'Специальность: ' : 'Направление подготовки: ')
                .boldBegin().append(hs.printTitle).boldEnd() : new RtfString())
        im.put('group', examGroupTitle ? new RtfString().append('группа ')
                .boldBegin().append(examGroupTitle).boldEnd() : new RtfString())
        im.put('developForm', developForm ? new RtfString().append('форма ')
                .boldBegin().append(developForm.title).boldEnd() : new RtfString())
        im.put('examPassDiscipline', disciplineTitle)
        im.put('examPassDate', DateFormatter.STRING_MONTHS_AND_QUOTES.format(examPassDate))
        im.put('commission', CommonBaseStringUtil.joinNotEmpty(commissions, "; "))
        tm.put('T', data as String[][])

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(template, im, tm), fileName: 'Экзаменационная ведомость.rtf']
    }
}
