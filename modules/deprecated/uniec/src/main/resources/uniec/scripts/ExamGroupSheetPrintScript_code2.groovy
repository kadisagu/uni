/* $Id$ */
// Copyright 2006-2012 Tandem Service Software
package uniec.scripts

import org.hibernate.Session
import org.tandemframework.core.exception.ApplicationException
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.rtf.RtfBean
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.util.key.PairKey
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.uniec.dao.examgroup.IExamGroupSetDao
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm
import ru.tandemservice.uniec.entity.entrant.DisciplineDateSetting
import ru.tandemservice.uniec.entity.entrant.ExamGroup
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation

/**
 * @author Vasily Zhukov
 * @since 24.02.2012
 */
return new ExamGroupSheetPrint_code2(// стандартные входные параметры скрипта
        session: session,            // сессия
        template: template,          // шаблон
        ids: object                  // объект печати (идентификаторы экзам. групп)
).print()

/**
 * Алгоритм печати списка ведомостей экзам. групп (Механизм 2)
 *
 * @author Vasily Zhukov
 * @since 22.12.2011
 */
class ExamGroupSheetPrint_code2
{
    Session session
    byte[] template
    List<Long> ids

    def print()
    {
        if (ids.empty)
            throw new ApplicationException('Нет данных для печати.')

        // загружаем настройку 'Даты сдачи вступительных испытаний'
        def firstExamGroup = IUniBaseDao.instance.get().getNotNull(ExamGroup.class, ids.get(0))
        def enrollmentCampaign = firstExamGroup.examGroupSet.enrollmentCampaign
        def settingList = DQL.createStatement(session, /
                from ${DisciplineDateSetting.class.simpleName}
                where ${DisciplineDateSetting.discipline().enrollmentCampaign().id()} = ${enrollmentCampaign.id}
        /).<DisciplineDateSetting> list()
        def passDateMap = new Hashtable<PairKey<Discipline2RealizationWayRelation, SubjectPassForm>, Date>()
        settingList.collectEntries(passDateMap, {[PairKey.create(it.discipline, it.subjectPassForm), it.passDate]})

        // загружаем сокращенное название вуза
        def vuzShortTitle = TopOrgUnit.instance.shortTitle

        // загружаем шаблон
        def template = new RtfReader().read(template)

        // создаем результирующий документ, наследуя настройки из шаблона
        def result = RtfBean.elementFactory.createRtfDocument()
        result.header = template.header
        result.settings = template.settings

        for (def id: ids)
        {
            // загружаем экзаменационную группу
            def group = (ExamGroup) session.get(ExamGroup.class, id)

            // нет группы, нечего печатать
            if (group == null) continue

            // получаем детализацию экзаменационной группы
            // Map<PairKey<Discipline2RealizationWayRelation, SubjectPassForm>, Set<RequestedEnrollmentDirection>>
            def detailMap = IExamGroupSetDao.instance.get().getExamPassDisciplineList(group)

            detailMap.each {key, value ->
                def discipline = key.first;          // дисциплина
                def subjectPassForm = key.second;    // форма сдачи
                def document = template.clone;       // не портим шаблон
                def passDate = passDateMap.get(key)  // дата сдачи по настройке
                def directionSet = value;            // мн-во выбранных направлений приема в экзам. группе

                // подставляем значения меток
                def im = new RtfInjectModifier()
                im.put('groupTitle', group.shortTitle)
                im.put('examPassDisciplineTitle', discipline.title + ' (' + subjectPassForm.title + ')')
                im.put('passDate', passDate?.format('dd.MM.yyyy'))
                im.put('vuzShortTitle', vuzShortTitle)
                im.modify(document)

                // подставляем данные в таблицу документа
                def tm = new RtfTableModifier()
                tm.put('T', getTableData(subjectPassForm, directionSet))
                tm.modify(document)

                // добавляем получившийся документ в результирующий
                result.elementList.addAll(document.elementList)
            }
        }

        if (result.elementList.empty)
            throw new ApplicationException('Нет данных для печати.')

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(result), fileName: 'examGroupSheet.rtf']
    }

    String[][] getTableData(SubjectPassForm subjectPassForm, Set<RequestedEnrollmentDirection> directionSet)
    {
        def requests = directionSet.collect {it.entrantRequest}
        def rows = requests.unique().sort {it.entrant.person.identityCard.fullFio}
        int i = 1
        return rows.collect {request ->
            def number = i++                          // номер по порядку
            def requestNumber = request.stringNumber  // номер заявления
            def targetAdmission = DQL.createStatement(session, /
                    from ${RequestedEnrollmentDirection.class.simpleName}
                    where ${RequestedEnrollmentDirection.entrantRequest().id()} = ${request.id}
                    order by ${RequestedEnrollmentDirection.priority()}
            /).setMaxResults(1).<RequestedEnrollmentDirection> uniqueResult().targetAdmission;
            def fullFio = request.entrant.person.identityCard.fullFio + (targetAdmission ? ' (Ц)' : '')  // фио
            [       /*0*/ number,
                    /*1*/ requestNumber,
                    /*2*/ fullFio,
                    /*3*/ null,
                    /*4*/ null,
                    /*5*/ null,
                    /*6*/ number,
                    /*7*/ requestNumber,
                    /*8*/ fullFio]
        } as String[][]
    }
}
