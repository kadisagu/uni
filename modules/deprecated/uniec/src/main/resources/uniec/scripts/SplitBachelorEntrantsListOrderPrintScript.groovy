package uniec.scripts

import org.apache.commons.collections15.map.LinkedMap
import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.common.base.entity.IPersistentIdentityCard
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.RtfBean
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.node.IRtfGroup
import org.tandemframework.shared.organization.base.entity.OrgUnit
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import ru.tandemservice.movestudent.dao.IMoveStudentDao
import ru.tandemservice.movestudent.entity.StuListOrderToBasicRelation
import ru.tandemservice.movestudent.entity.StudentListOrder
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType
import ru.tandemservice.uni.UniDefines
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels
import ru.tandemservice.uni.entity.education.EducationOrgUnit
import ru.tandemservice.uni.services.ContractAttributes
import org.tandemframework.shared.commonbase.utils.CommonCollator
import ru.tandemservice.uni.util.rtf.RtfSearchResult
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.uniec.base.bo.EcProfileDistribution.EcProfileDistributionManager
import ru.tandemservice.uniec.base.bo.EcProfileDistribution.util.IEcgpRecommendedDTO
import ru.tandemservice.uniec.base.entity.ecgp.EcgpDistribution
import ru.tandemservice.uniec.entity.entrant.ProfileEducationOrgUnit
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder
import ru.tandemservice.uniec.entity.orders.SplitEntrantsStuListExtract
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising
import ru.tandemservice.unimv.entity.visa.Visa

/**
 * Omnia mea mecum porto
 *
 * Скрипт печати списочного приказа "О распределении зачисленных абитуриентов по профилям (бакалавриат/специалитет)
 *
 * ! Внимание: Печатные шаблоны для данного приказа расположены а справочнике печатных шаблонов модуля "Движение студентов"
 *
 * @author Dmitry Seleznev
 * @since 15.08.2012
 */

return new SplitBachelorEntrantsListOrderPrint(
        session: session,
        template: template,
        order: order,
        contractsMap: contractsMap
).print()

class SplitBachelorEntrantsListOrderPrint
{
    Session session
    byte[] template
    StudentListOrder order
    Map<Long, ContractAttributes> contractsMap
    def coreDao = IMoveStudentDao.instance.get()

    def print()
    {
        order = session.get(StudentListOrder.class, order.id) as StudentListOrder

        RtfInjectModifier modifier = new RtfInjectModifier();
        RtfDocument document = new RtfReader().read(template);
        injectOrderData(modifier);
        injectVisas(document);
        modifier.modify(document);

        List<Object[]> extractsList = new DQLSelectBuilder().fromEntity(SplitEntrantsStuListExtract.class, "e")
                .column("e")
                .column(DQLExpressions.property(SplitEntrantsStuListExtract.type().fromAlias("e")))
                .where(DQLExpressions.eq(DQLExpressions.property(SplitEntrantsStuListExtract.paragraph().order().fromAlias("e")), DQLExpressions.value(order))).createStatement(session).list();

        List<EcgpDistribution> distributions = new ArrayList<EcgpDistribution>();
        Map<EcgpDistribution, ListParagraph> paragraphsMap = new HashMap<EcgpDistribution, ListParagraph>();
        for (Object[] abstrExtract: extractsList)
        {
            StudentExtractType type = (StudentExtractType) abstrExtract[1];
            SplitEntrantsStuListExtract extract = (SplitEntrantsStuListExtract) abstrExtract[0];
            EcgpDistribution distribution = extract.getEcgpEntrantRecommended().getDistribution();

            EnrollmentOrder enrOrder = extract.enrollmentExtract.order;
            OrderData orderData = new OrderData(enrOrder.getNumber(), enrOrder.getCommitDate());

            ListParagraph paragraph = paragraphsMap.get(distribution);
            if(null == paragraph)
            {
                paragraph = new ListParagraph(distribution, type);
                paragraph.number = extract.paragraph.number;
            };
            if(!paragraph.ordersList.contains(orderData)) paragraph.ordersList.add(orderData);

            ProfileEducationOrgUnit profile = extract.getEcgpEntrantRecommended().getProfile();
            ListSubParagraph subParagraph = paragraph.subParagraphsMap.get(profile);
            if(null == subParagraph) subParagraph = new ListSubParagraph(profile, paragraph);

            ListExtract extractWrapper = new ListExtract(extract, null); //TODO DTO
            subParagraph.extractsList.add(extractWrapper);


            paragraph.subParagraphsMap.put(profile, subParagraph)
            paragraphsMap.put(distribution, paragraph);

            if(!distributions.contains(distribution)) distributions.add(distribution);
        }

        List<IEcgpRecommendedDTO> recomList = new ArrayList<IEcgpRecommendedDTO>();
        Map<Long, IEcgpRecommendedDTO> map = new LinkedMap<Long, IEcgpRecommendedDTO>();

        for(EcgpDistribution distrib : distributions)
        {
            recomList.addAll(EcProfileDistributionManager.instance().dao().getEntrantRecommendedRowList(distrib));
        }

        for(IEcgpRecommendedDTO dto : recomList)
        {
            map.put(dto.entrantRecommended.id, dto);
        }

        List<ListParagraph> paragraphsList = new ArrayList<ListParagraph>();
        for(ListParagraph paragraph : paragraphsMap.values())
        {
            paragraph.subParagraphsList.addAll(paragraph.subParagraphsMap.values());
            for(ListSubParagraph sub : paragraph.subParagraphsList)
            {
                for(ListExtract ext : sub.extractsList)
                {
                    ext.priority = map.indexOf(ext.extract.ecgpEntrantRecommended.id);
                    ext.data = map.get(ext.extract.ecgpEntrantRecommended.id);
                }
                Collections.sort(sub.extractsList);
            }
            Collections.sort(paragraph.subParagraphsList)
            paragraphsList.add(paragraph)
        }

        Collections.sort(paragraphsList);
        injectParagraphs(document, paragraphsList);

        return [document: document];
    }

    def injectOrderData(RtfInjectModifier modifier)
    {
        modifier.put("orderNumber", order.number);
        modifier.put("commitDate", order.commitDate?.format('dd.MM.yyyy'));

        modifier.put("highSchoolTitle", TopOrgUnit.instance.title);
        if(null != TopOrgUnit.instance.address && null != TopOrgUnit.instance.address.settlement)
            modifier.put("highSchoolCity", TopOrgUnit.instance.address.settlement.titleWithType);
        else modifier.put("highSchoolCity","");

        if(null == order.reason) modifier.put("orderReason", "");
        else modifier.put("orderReason", null != order.reason.printTitle ? order.reason.printTitle : order.reason.title);

        List<StuListOrderToBasicRelation> basicsList = new DQLSelectBuilder()
                .fromEntity(StuListOrderToBasicRelation.class, "b").column("b")
                .where(DQLExpressions.eq(DQLExpressions.property(StuListOrderToBasicRelation.order().fromAlias("b")), DQLExpressions.value(order)))
                .createStatement(session).list();

        StringBuilder basicsStr = new StringBuilder();
        for(StuListOrderToBasicRelation rel : basicsList)
        {
            basicsStr.append(basicsStr.length() > 0 ? ", " : "");
            basicsStr.append(rel.basic.title);
            basicsStr.append(null != rel.comment ? " " : "");
            basicsStr.append(null != rel.comment ? rel.comment : "");
        }
        modifier.put("orderBasic", basicsStr.toString());
    }

    def injectParagraphs(RtfDocument document, List<ListParagraph> paragraphsList)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, "PARAGRAPHS");

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            int paragraphNumber = 0;
            List<IRtfElement> parList = new ArrayList<IRtfElement>();

            for (ListParagraph paragraphWrapper : paragraphsList)
            {
                // Получаем шаблон параграфа
                byte[] paragraphTemplate = coreDao.getTemplate(paragraphWrapper.extractType, 1);
                RtfDocument paragraphPart = new RtfReader().read(paragraphTemplate);

                // Вносим необходимые метки
                RtfInjectModifier paragraphInjectModifier = new RtfInjectModifier();
                paragraphInjectModifier.put("parNumber", String.valueOf(++paragraphNumber));
                paragraphInjectModifier.put("parNumberConditional", paragraphsList.size() > 1 ? String.valueOf(paragraphNumber) + ". " : "");
                paragraphInjectModifier.put("oe", paragraphWrapper.ordersList.size() > 1 ? "ами" : "ом");
                StringBuilder orderNumbers = new StringBuilder();

                for(OrderData orderData : paragraphWrapper.getOrdersList())
                {
                    orderNumbers.append(orderNumbers.length() > 0 ? ", №" : "№");
                    orderNumbers.append(orderData.number).append(" от ");
                    orderNumbers.append(orderData.date?.format('dd.MM.yyyy'));
                }
                paragraphInjectModifier.put("orders", orderNumbers.toString());

                EducationOrgUnit eduOrgUnit = paragraphWrapper.distribution.config.ecgItem.educationOrgUnit;

                injectEducationData(paragraphInjectModifier, eduOrgUnit);
                paragraphInjectModifier.put("FCTP", eduOrgUnit.developCombinationFullTitle);

                paragraphInjectModifier.modify(paragraphPart);

                // Вставляем список подпараграфов
                injectSubParagraphs(paragraphPart, new ArrayList<ListSubParagraph>(paragraphWrapper.subParagraphsList));

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraphPart.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    def injectSubParagraphs(RtfDocument paragraphPart, List<ListSubParagraph> paragraphPartsList)
    {
        //Collections.sort(paragraphPartsList);

        // 1. ищем ключевое слово
        final RtfSearchResult rtfPartsSearchResult = UniRtfUtil.findRtfMark(paragraphPart, "SUBPARAGRAPHS");

        // 2. Если нашли, то вместо него вставляем все подпараграфы
        if (rtfPartsSearchResult.isFound())
        {
            List<IRtfElement> parPartsList = new ArrayList<IRtfElement>();

            for (ListSubParagraph paragraphPartWrapper : paragraphPartsList)
            {
                // Получаем шаблон подпараграфа
                byte[] paragraphPartTemplate = coreDao.getTemplate(paragraphPartWrapper.parent.extractType, 2);
                RtfDocument paragraphPartPart = new RtfReader().read(paragraphPartTemplate);

                // Вносим необходимые метки
                RtfInjectModifier subParagraphInjectModifier = new RtfInjectModifier();
                EducationOrgUnit eduOrgUnit = paragraphPartWrapper.profile.educationOrgUnit;
                injectProfileData(subParagraphInjectModifier, eduOrgUnit);

                OrgUnit formativeOrgUnit = eduOrgUnit.formativeOrgUnit;
                subParagraphInjectModifier.put("formativeOrgUnit", null != formativeOrgUnit.nominativeCaseTitle ? formativeOrgUnit.nominativeCaseTitle : formativeOrgUnit.fullTitle);
                subParagraphInjectModifier.modify(paragraphPartPart);

                // вставляем список слушателей
                injectExtracts(paragraphPartPart, paragraphPartWrapper.extractsList);

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraphPartPart.getElementList());
                parPartsList.add(rtfGroup);

            }

            // полученный документ (набор подпараграфов вставляем вместо ключевого слова)
            rtfPartsSearchResult.getElementList().remove(rtfPartsSearchResult.getIndex());
            rtfPartsSearchResult.getElementList().addAll(rtfPartsSearchResult.getIndex(), parPartsList);
        }
    }

    def injectExtracts(RtfDocument subParagraph, List<ListExtract> extractsList)
    {
        RtfTableModifier tableModifier = new RtfTableModifier();
        List<String[]> table = new ArrayList<String[]>();

        int extractNumber = 1;
        for(ListExtract extract : extractsList)
        {
            String[] line = new String[4];
            line[0] = extractNumber++;
            line[1] = extract.extract.entity.person.fullFio;
            line[2] = null != extract.data.finalMark ? extract.data.finalMark.intValue() : "";
            if(UniDefines.COMPENSATION_TYPE_BUDGET.equals(extract.extract.ecgpEntrantRecommended.distribution.config.compensationType.code))
                line[3] = extract.data.prioritiesShort;
            else
            {
                ContractAttributes contrData = contractsMap.get(extract.extract.entity.id);
                if(null != contrData) line[3] = contrData.number
                else line[3] = "";
            }

            table.add(line);
        }
        tableModifier.put("T", table.toArray(new String[table.size()][4]))
        tableModifier.modify(subParagraph);
    }

    def static injectEducationData(RtfInjectModifier modifier, EducationOrgUnit eduOrgUnit)
    {
        StructureEducationLevels struct = eduOrgUnit.getEducationLevelHighSchool().getEducationLevel().getLevelType();
        if (struct.bachelor || struct.master) modifier.put("educationType_D", "направлению подготовки");
        else if (struct.specialty) modifier.put("educationType_D", "специальности");
        modifier.put("educationOrgUnit", eduOrgUnit.educationLevelHighSchool.printTitle);
    }

    def static injectProfileData(RtfInjectModifier modifier, EducationOrgUnit eduOrgUnit)
    {
        StructureEducationLevels struct = eduOrgUnit.getEducationLevelHighSchool().getEducationLevel().getLevelType();
        if (struct.bachelor) modifier.put("profileType", "профиль");
        else if (struct.master) modifier.put("profileType", "магистерская программа");
        else modifier.put("profileType", "специализация");

        modifier.put("profileEducationOrgUnit", eduOrgUnit.educationLevelHighSchool.title + " (" + eduOrgUnit.educationLevelHighSchool.shortTitle + ")");
    }

    def injectVisas(RtfDocument document)
    {
        RtfTableModifier tableModifier = new RtfTableModifier();

        List<Visa> visaList = new DQLSelectBuilder().fromEntity(Visa.class, "v").column("v")
                .where(DQLExpressions.eq(DQLExpressions.property(Visa.document().fromAlias("v")), DQLExpressions.value(order)))
                .createStatement(session).list();

        Collections.sort(visaList, new Comparator<Visa>(){
            @Override
            public int compare(Visa o1, Visa o2)
            {
                if (o1.getGroupMemberVising().getIndex() > o2.getGroupMemberVising().getIndex())
                    return 1;
                else if (o1.getGroupMemberVising().getIndex() < o2.getGroupMemberVising().getIndex())
                    return -1;
                else if (o1.getIndex() > o2.getIndex())
                    return 1;
                else if (o1.getIndex() < o2.getIndex())
                    return -1;
                else
                    return 0;
            }
        });

        List<String[]> visaData = new ArrayList<String[]>();
        List<String[]> primaryVisaData = new ArrayList<String[]>();
        List<String[]> secondaryVisaData = new ArrayList<String[]>();
        Map<String, List<String[]>> printLabelMap = new HashMap<String, List<String[]>>();

        List<GroupsMemberVising> groupsMemberVisingList = new DQLSelectBuilder()
                .fromEntity(GroupsMemberVising.class, "g").column("g")
                .createStatement(session).list();

        for (GroupsMemberVising group : groupsMemberVisingList)
            printLabelMap.put(group.getPrintLabel(), new LinkedList<String[]>());

        for (Visa visa : visaList)
        {
            IPersistentIdentityCard identityCard = visa.getPossibleVisa().getEntity().getPerson().getIdentityCard();
            String lastName = identityCard.getLastName();
            String firstName = identityCard.getFirstName();
            String middleName = identityCard.getMiddleName();

            StringBuilder str = new StringBuilder();
            if (StringUtils.isNotEmpty(firstName))
                str.append(firstName.substring(0, 1).toUpperCase()).append(".");
            if (StringUtils.isNotEmpty(middleName))
                str.append(middleName.substring(0, 1).toUpperCase()).append(".");
            str.append(" ").append(lastName);


            printLabelMap.get(visa.getGroupMemberVising().getPrintLabel()).add((String[])[visa.possibleVisa.title, str.toString()]);

            visaData.add((String[])[visa.possibleVisa.title, str.toString()]);

            if (visa.getGroupMemberVising().getCode().equals("2"))
                primaryVisaData.add((String[])[visa.possibleVisa.title, str.toString()]);
            else
                secondaryVisaData.add((String[])[visa.possibleVisa.title, str.toString()]);
        }
        for (Map.Entry<String, List<String[]>> entry : printLabelMap.entrySet())
                tableModifier.put(entry.getKey(), entry.getValue().toArray(new String[entry.getValue().size()][2]));

        tableModifier.put("VISAS", visaData.toArray(new String[visaData.size()][2]));
        tableModifier.put("PRIMARY_VISAS", primaryVisaData.toArray(new String[primaryVisaData.size()][2]));
        tableModifier.put("SECONDARY_VISAS", secondaryVisaData.toArray(new String[secondaryVisaData.size()][2]));

        tableModifier.modify(document);
    }
}

class ListParagraph implements Comparable<ListParagraph>
{
    int number;
    List<OrderData> ordersList;
    EcgpDistribution distribution;
    Map<ProfileEducationOrgUnit, ListSubParagraph> subParagraphsMap;
    List<ListSubParagraph> subParagraphsList;
    StudentExtractType extractType;

    ListParagraph(EcgpDistribution distribution, StudentExtractType extractType)
    {
        this.distribution = distribution;
        this.extractType = extractType;
        ordersList = new ArrayList<OrderData>();
        subParagraphsMap = new HashMap<ProfileEducationOrgUnit, ListSubParagraph>();
        subParagraphsList = new ArrayList<ListSubParagraph>();
    }

    @Override
    boolean equals(Object obj)
    {
        if (this.is(obj)) return true;
        if (getClass() != obj.class) return false;
        ListParagraph paragraph = (ListParagraph) obj;
        if (!distribution.equals(paragraph.distribution)) return false;
        return true;
    }

    int hashCode() { return distribution.hashCode(); }

    @Override
    int compareTo(ListParagraph o)
    {
        return number.compareTo(o.number);
    }
}

class ListSubParagraph implements Comparable<ListSubParagraph>
{
    ProfileEducationOrgUnit profile;
    List<ListExtract> extractsList;
    ListParagraph parent;

    ListSubParagraph(ProfileEducationOrgUnit profile, ListParagraph parent)
    {
        this.parent = parent
        this.profile = profile
        extractsList = new ArrayList<ListExtract>();
    }

    @Override
    boolean equals(Object obj)
    {
        if (this.is(obj)) return true;
        if (getClass() != obj.class) return false;
        ListSubParagraph subParagraph = (ListSubParagraph) obj;
        if (!profile.equals(subParagraph.profile)) return false;
        return true;
    }

    int hashCode() { return profile.hashCode(); }

    @Override
    int compareTo(ListSubParagraph o)
    {
        return profile.educationOrgUnit.educationLevelHighSchool.displayableTitle.compareTo(o.profile.educationOrgUnit.educationLevelHighSchool.displayableTitle);
    }
}

class ListExtract implements Comparable<ListExtract>
{
    SplitEntrantsStuListExtract extract;
    IEcgpRecommendedDTO data;
    Integer priority;

    ListExtract(SplitEntrantsStuListExtract extract, IEcgpRecommendedDTO data)
    {
        this.extract = extract
        this.data = data
    }

    @Override
    int compareTo(ListExtract o)
    {
        return CommonCollator.RUSSIAN_COLLATOR.compare(extract.entity.person.fullFio, o.extract.entity.person.fullFio);
        // Сортировка по аналогии с сортировкой, реализованной в распределении
        // return priority.compareTo(o.priority);
    }
}

class OrderData
{
    String number;
    Date date;

    OrderData(String number, Date date)
    {
        this.number = number
        this.date = date
    }

    @Override
    boolean equals(Object obj)
    {
        if (this.is(obj)) return true;
        if (getClass() != obj.class) return false;

        OrderData orderData = (OrderData) obj;
        if (!date.time.equals(orderData.date.time)) return false;
        if (!number.equals(orderData.number)) return false;
        return true;
    }
}