/* $Id$ */
package uniec.scripts

import org.hibernate.Session
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract

import java.text.SimpleDateFormat

return new EnrollmentExtractPrint(
        session: session,
        template: templateDocument,
        extract: session.get(EnrollmentExtract.class, object)
).print()

/**
 * @author Nikolay Fedorovskih
 * @since 31.07.2013
 */
class EnrollmentExtractPrint
{
    Session session
    RtfDocument template
    EnrollmentExtract extract

    def print()
    {
        def order = extract.order
        def academy = TopOrgUnit.instance
        def document = template
        def im = new RtfInjectModifier()

        im.put('highSchoolTitle_G', academy.genitiveCaseTitle ?: academy.printTitle)
        im.put('highSchoolShortTitle', academy.shortTitle)

        CommonExtractPrint.initDevelopForm(im, extract.entity.educationOrgUnit.developForm, '')
        im.put('orderDate', order.commitDate != null ? (new SimpleDateFormat("«dd» MMMMM yyyy", RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU).format(order.commitDate) + ' г.') : null)
        im.put('orderNumber', order.number)
        im.put('course', extract.course.intValue as String)
        im.put('compensationType', getCompensationType())
        im.put('entrant', extract.entity.requestedEnrollmentDirection.entrantRequest.entrant.person.identityCard.fullFio)
        im.put('direction', extract.entity.educationOrgUnit.educationLevelHighSchool.title)

        im.modify(document)

        return [document: document, fileName: 'EnrollmentExtract.rtf']
    }

    RtfString getCompensationType()
    {
        new RtfString()
                .append('места, ')
                .boldBegin()
                .append('финансируемые из средств ')
                .append(extract.entity.compensationType.budget ? 'федерального бюджета' : 'физических или юридических лиц')
                .boldEnd()
                .append(',')
    }
}