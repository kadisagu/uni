/* $Id$ */
package uniec.scripts

import org.hibernate.Session
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.RtfBean
import org.tandemframework.rtf.data.IRtfData
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.document.RtfHeader
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels
import ru.tandemservice.uni.entity.education.EducationOrgUnit
import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder
import ru.tandemservice.uniec.entity.orders.EnrollmentRevertExtract
import ru.tandemservice.uniec.entity.orders.EnrollmentRevertParagraph
import ru.tandemservice.unimove.IAbstractParagraph

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

return new EnrollmentRevertOrderPrint(                          // стандартные входные параметры скрипта
        session: session,                                 // сессия
        template: template,                               // шаблон
        order: session.get(EnrollmentOrder.class, object) // объект печати
).print()

/**
 * @author Nikolay Fedorovskih
 * @since 08.08.2013
 */

class EnrollmentRevertOrderPrint
{
    Session session
    byte[] template
    EnrollmentOrder order
    def rtfFactory = RtfBean.elementFactory
    def qcElement = rtfFactory.createRtfControl(IRtfData.QC) // выравнивание по центру
    def qjElement = rtfFactory.createRtfControl(IRtfData.QJ) // выравнивание по ширине
    def parElement = rtfFactory.createRtfControl(IRtfData.PAR) // перевод строки (каретка)

    def print()
    {
        def academy = TopOrgUnit.instance
        def im = new RtfInjectModifier()
        RtfDocument doc = new RtfReader().read(template)

        im.put('commitDate', order.commitDate?.format('dd.MM.yyyy'))
        im.put('enrollmentDate', order.enrollmentDate?.format('dd.MM.yyyy'))
        im.put('orderNumber', order.number)
        im.put('highSchoolTitle', academy.title)
        im.put('highSchoolCity', academy.address?.settlement?.titleWithType)
        im.put('orderBasicText', order.orderBasicText)
        im.put('orderText', order.orderText)
        im.put('textParagraph', order.textParagraph)
        im.put('executor', order.executor)
        im.put('orderReason', order.reason?.title)
        im.put('orderBasic', order.basic?.title)
        im.put('PARAGRAPHS', getParagraphs(doc.header))

        im.modify(doc)
        return [document: RtfUtil.toByteArray(doc), fileName: 'EnrollmentRevertOrder.rtf']
    }

    def List<IRtfElement> getSubParagraphs(IAbstractParagraph paragraph)
    {
        Map<EducationOrgUnit, List<EnrollmentRevertExtract>> map = [:]
        List<EnrollmentRevertExtract> extractList = new DQLSelectBuilder().fromEntity(EnrollmentRevertExtract.class, "e")
                .where(eq(property("e", EnrollmentRevertExtract.paragraph()), value(paragraph)))
                .order(property("e", EnrollmentRevertExtract.entity().entrantRequest().entrant().person().identityCard().fullFio()))
                .createStatement(session).list()

        extractList.each {
            List<EnrollmentRevertExtract> list = map.get(it.educationOrgUnit)
            if (list == null)
                map.put(it.educationOrgUnit, list = [])
            list.add(it)
        }

        List<IRtfElement> elements = []

        for (EducationOrgUnit educationOrgUnit : getSortedEducationOrgUnits(map.keySet()))
        {
            elements.add(qcElement)

            def rtfString = new RtfString()
                    .boldBegin()
                    .append('по ')
                    .append(getLevelTypeStr_G(educationOrgUnit.educationLevelHighSchool.educationLevel.levelType))
                    .append(' «').append(educationOrgUnit.educationLevelHighSchool.printTitle).append('»')
                    .boldEnd()
                    .par()
                    .append('(')
                    .append(educationOrgUnit.developForm.title.toLowerCase()).append(' форма обучения')
                    .append(', ')
                    .append(educationOrgUnit.developPeriod.title)
                    .append(')')

            elements.addAll(rtfString.toList())
            elements.addAll([parElement, parElement, qjElement])

            rtfString = new RtfString()
            int idx = 1
            for (EnrollmentRevertExtract revertExtract : map.get(educationOrgUnit))
            {
                rtfString.append(String.valueOf(idx++))
                rtfString.append('. ')
                rtfString.append(revertExtract.entity.entrantRequest.entrant.person.identityCard.fullFio)
                rtfString.par()
            }
            elements.addAll(rtfString.toList())
            elements.add(parElement)
        }

        return elements
    }

    def List<IRtfElement> getParagraphs(RtfHeader header)
    {
        byte[] tpl = EcOrderManager.instance().dao().getParagraphTemplate(order.type)
        RtfDocument paragraphDocument = new RtfReader().read(tpl)

        List<IRtfElement> paragraphList = []
        order.paragraphList.each {
            RtfDocument paragraphPart = paragraphDocument.clone
            RtfUtil.modifySourceList(header, paragraphPart.header, paragraphPart.elementList)

            EnrollmentOrder canceledOrder = (it as EnrollmentRevertParagraph).canceledOrder
            new RtfInjectModifier()
                    .put('parNumber', it.number.toString())
                    .put('canceledOrderDate', canceledOrder.commitDate?.format('dd.MM.yyyy'))
                    .put('canceledOrderNumber', canceledOrder.number)
                    .put('STUDENT_LIST', getSubParagraphs(it))
                    .modify(paragraphPart)

            def group = rtfFactory.createRtfGroup()
            group.elementList = paragraphPart.elementList
            paragraphList.add(group)
        }
        return paragraphList
    }

    static List<EducationOrgUnit> getSortedEducationOrgUnits(Set<EducationOrgUnit> set)
    {
        def ret = new ArrayList<>(set)
        Collections.sort(ret, new Comparator<EducationOrgUnit>() {
            @Override
            int compare(EducationOrgUnit o1, EducationOrgUnit o2)
            {
                return o1.educationLevelHighSchool.title.compareToIgnoreCase(o2.educationLevelHighSchool.title)
            }
        })
        return ret
    }

    static String getLevelTypeStr_G(StructureEducationLevels levelType)
    {
        if (levelType.isSpecialization() || (levelType.isProfile() && (levelType.isMaster() || levelType.isBachelor())))
        {
            if (levelType.isSpecialization()) return 'специализации'
            if (levelType.isMaster()) return 'магистерской программе'
            return 'бакалаврскому профилю'
        }
        return levelType.isSpecialty() ? 'специальности' : 'направлению'
    }

}