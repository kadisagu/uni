package uniec.scripts

import org.hibernate.Session
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.rtf.RtfBean
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.fias.base.entity.Address
import org.tandemframework.shared.fias.base.entity.AddressCountry
import org.tandemframework.shared.fias.base.entity.AddressItem
import org.tandemframework.shared.fias.base.entity.AddressStreet
import org.tandemframework.shared.organization.base.entity.OrgUnit
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import ru.tandemservice.uni.UniDefines
import ru.tandemservice.uni.entity.education.EducationOrgUnit
import ru.tandemservice.uniec.entity.orders.AbstractEntrantExtract
import org.tandemframework.hibsupport.dql.*
import org.tandemframework.shared.person.base.entity.*
import org.tandemframework.shared.person.catalog.entity.*
import ru.tandemservice.uni.entity.catalog.*
import ru.tandemservice.uniec.entity.entrant.*

/**
 * Created by IntelliJ IDEA.
 * User: azhebko
 * Date: 8/1/12
 * Time: 5:06 PM
 * To change this template use File | Settings | File Templates.
 */

return new EntrantStudentFormPrint(
        session: session,
        template: template,
        campaign: enrollmentCampaign,
        dateFrom: dateFrom,
        dateTo: dateTo,
        compensationType: compensationType,
        studentCategoryList: studentCategory,
        qualificationsList: qualification,
        formativeOrgUnitList: formativeOrgUnit,
        territorialOrgUnitList: territorialOrgUnit,
        educationLevelsHighSchoolList: educationLevelHighSchool,
        developFormList: developForm,
        developConditionList: developCondition,
        developTechList: developTech,
        developPeriodList: developPeriod,
        parallel: parallel
).print()

class EntrantStudentFormPrint
{
    Session session
    byte [] template
    EnrollmentCampaign campaign
    Date dateFrom
    Date dateTo
    CompensationType compensationType
    List<StudentCategory> studentCategoryList
    List<Qualifications> qualificationsList
    List<OrgUnit> formativeOrgUnitList
    List<OrgUnit> territorialOrgUnitList
    List<EducationLevelsHighSchool> educationLevelsHighSchoolList
    List<DevelopForm> developFormList
    List<DevelopCondition> developConditionList
    List<DevelopTech> developTechList
    List<DevelopPeriod> developPeriodList
    boolean parallel

    def print()
    {
        RtfDocument template = new RtfReader().read(template)

        RtfDocument report = RtfBean.getElementFactory().createRtfDocument();
        report.setSettings(template.getSettings());
        report.setHeader(template.getHeader());

        long academySettlementId = TopOrgUnit.getInstance().address.settlement.id;

        for (Object[] student: getStudentList())
        {
            RtfDocument form = template.getClone();
            RtfInjectModifier modifier = new RtfInjectModifier();

            Long personId = (Long) student[0];

            modifier.put("lastName", (String) student[1]);
            modifier.put("firstName", (String) student[2]);
            modifier.put("middleName", (String) student[3]);
            modifier.put("birthDate", DateFormatter.DEFAULT_DATE_FORMATTER.format((Date) student[4]));
            modifier.put("birthPlace", (String) student[5]);
            modifier.put("sex", (String) student[6]);
            modifier.put("citizenship", (String) student[7]);
            modifier.put("familyStatus", (String) student[8]);
            modifier.put("benefits", getBenefits(personId));
            modifier.put("seria", (String) student[9]);
            modifier.put("number", (String) student[10]);
            modifier.put("issuanceDate", DateFormatter.DEFAULT_DATE_FORMATTER.format((Date) student[11]));
            modifier.put("issuancePlace", (String) student[12]);
            modifier.put("regPostCode", (String) student[13]);
            modifier.put("regCountry", (String) student[14]);
            modifier.put("regRegion", student[15] != null ? getRegion((Long) student[15]) : null);
            modifier.put("regDistrict", student[15] != null ? getDistrict((Long) student[15]) : null);
            modifier.put("regSettlement", (String) student[16]);
            modifier.put("regAddress", (String) student[17]);
            modifier.put("homePhoneNumber", (String) student[18]);
            modifier.put("postCode", (String) student[19]);
            modifier.put("country", (String) student[20]);
            modifier.put("region", student[21] != null ? getRegion((Long) student[21]) : "");
            modifier.put("district", student[21] != null ? getDistrict((Long) student[21]) : "");
            modifier.put("settlement", (String) student[22]);
            modifier.put("address", (String) student[23]);
            modifier.put("mobilePhoneNumber", (String) student[24]);
            modifier.put("eduInstitutionKind", (String) student[25]);
            modifier.put("docType", (String) student[26]);
            modifier.put("graduationHonour", (String) student[27]);
            modifier.put("docSeria", (String) student[28]);
            modifier.put("docNumber", (String) student[29]);
            modifier.put("docIssuanceDate", DateFormatter.DEFAULT_DATE_FORMATTER.format((Date) student[30]));
            modifier.put("docIssuancePlace", (String) student[31]);
            modifier.put("employeeSpeciality", (String) student[32]);
            modifier.put("language", getLanguages(personId));
            modifier.put("workPlace", (String) student[33]);
            modifier.put("workPlacePosition", (String) student[34]);

            PersonNextOfKin father = getPersonNextOfKin(personId, "03");
            PersonNextOfKin mother = getPersonNextOfKin(personId, "04");

            modifier.put("fatherLastName",  father != null ? father.lastName : "");
            modifier.put("fatherFirstName", father != null ? father.firstName : "");
            modifier.put("fatherMiddleName", father != null ? father.middleName : "");
            modifier.put("fatherBirthYear", father != null ? DateFormatter.DATE_FORMATTER_JUST_YEAR.format(father.birthDate) : "");
            modifier.put("fatherWorkPlace", father != null ? father.employmentPlace : "");
            modifier.put("fatherWorkPlacePosition", father != null ? father.post : "");
            modifier.put("fatherPhone", father != null ? father.phones : "");

            modifier.put("motherLastName", mother != null ? mother.lastName : "");
            modifier.put("motherFirstName", mother != null ? mother.firstName : "");
            modifier.put("motherMiddleName", mother != null ? mother.middleName : "");
            modifier.put("motherBirthYear", mother != null ? DateFormatter.DATE_FORMATTER_JUST_YEAR.format(mother.birthDate) : "");
            modifier.put("motherWorkPlace", mother != null ? mother.employmentPlace : "");
            modifier.put("motherWorkPlacePosition", mother != null ? mother.post : "");
            modifier.put("motherPhone", mother != null ? mother.phones : "");

            modifier.put("militaryOffice", getMilitaryOffice(personId, academySettlementId, false));
            modifier.put("incomerMilitaryOffice", getMilitaryOffice(personId, academySettlementId, true));

            modifier.put("isContract", (String) student[35]);
            modifier.put("isTargetAdmission", (String) student[36]);

            modifier.put("date", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));

/*
            modifier.put("commitDate", DateFormatter.DEFAULT_DATE_FORMATTER.format((Date) student[37]));
            modifier.put("orderNumber", (String) student[38]);
*/

            modifier.modify(form);
            report.getElementList().addAll(form.getElementList());
        }

        return [document: RtfUtil.toByteArray(report), fileName: 'EntrantStudentForm.rtf'];
    }

    def getStudentList()
    {
        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder
            .fromEntity(PreliminaryEnrollmentStudent.class, "s")
            .joinEntity(
                "s",
                DQLJoinType.left,
                RequestedEnrollmentDirection.class,
                "red",
                DQLExpressions.eq(DQLExpressions.property(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().id().fromAlias("s")), DQLExpressions.property(RequestedEnrollmentDirection.id().fromAlias("red"))))
            .joinEntity(
                "red",
                DQLJoinType.left,
                EntrantRequest.class,
                "er",
                DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().id().fromAlias("red")), DQLExpressions.property(EntrantRequest.id().fromAlias("er"))))
            .joinEntity(
                "er",
                DQLJoinType.left,
                Entrant.class,
                "e",
                DQLExpressions.eq(DQLExpressions.property(EntrantRequest.entrant().id().fromAlias("er")), DQLExpressions.property(Entrant.id().fromAlias("e"))))
            .joinEntity(
                "e",
                DQLJoinType.left,
                PersonRole.class,
                "pr",
                DQLExpressions.eq(DQLExpressions.property(Entrant.id().fromAlias("e")), DQLExpressions.property(PersonRole.id().fromAlias("pr"))))
            .joinEntity(
                "pr",
                DQLJoinType.left,
                Person.class,
                "p",
                DQLExpressions.eq(DQLExpressions.property(PersonRole.person().id().fromAlias("pr")), DQLExpressions.property(Person.id().fromAlias("p"))))
            .joinEntity(
                "p",
                DQLJoinType.left,
                IdentityCard.class,
                "ic",
                DQLExpressions.eq(DQLExpressions.property(Person.identityCard().id().fromAlias("p")), DQLExpressions.property(IdentityCard.id().fromAlias("ic"))))
            .joinEntity(
                "ic",
                DQLJoinType.left,
                Address.class,
                "rad",
                DQLExpressions.eq(DQLExpressions.property(IdentityCard.address().id().fromAlias("ic")), DQLExpressions.property(Address.id().fromAlias("rad"))))
            .joinEntity(
                "p",
                DQLJoinType.left,
                Address.class,
                "fad",
                DQLExpressions.eq(DQLExpressions.property(Person.address().id().fromAlias("p")), DQLExpressions.property(Address.id().fromAlias("fad"))))
            .joinEntity(
                "p",
                DQLJoinType.left,
                PersonEduInstitution.class,
                "pei",
                DQLExpressions.eq(DQLExpressions.property(Person.personEduInstitution().id().fromAlias("p")), DQLExpressions.property(PersonEduInstitution.id().fromAlias("pei"))))
            .joinEntity(
                "s",
                DQLJoinType.left,
                AbstractEntrantExtract.class,
                "aee",
                DQLExpressions.eq(DQLExpressions.property(PreliminaryEnrollmentStudent.id().fromAlias("s")), DQLExpressions.property(AbstractEntrantExtract.entity().id().fromAlias("aee"))))
            .joinEntity(
                "s",
                DQLJoinType.left,
                EducationOrgUnit.class,
                "eou",
                DQLExpressions.eq(DQLExpressions.property(PreliminaryEnrollmentStudent.educationOrgUnit().id().fromAlias("s")), DQLExpressions.property(EducationOrgUnit.id().fromAlias("eou"))))
            .joinEntity(
                "pei",
                DQLJoinType.left,
                GraduationHonour.class,
                "gh",
                DQLExpressions.eq(DQLExpressions.property(PersonEduInstitution.graduationHonour().id().fromAlias("pei")), DQLExpressions.property(GraduationHonour.id().fromAlias("gh"))))
            .joinEntity(
                "pei",
                DQLJoinType.left,
                EduInstitution.class,
                "ei",
                DQLExpressions.eq(DQLExpressions.property(PersonEduInstitution.eduInstitution().id().fromAlias("pei")), DQLExpressions.property(EduInstitution.id().fromAlias("ei"))))
            .joinEntity(
                "pei",
                DQLJoinType.left,
                EducationalInstitutionTypeKind.class,
                "eitk",
                DQLExpressions.eq(DQLExpressions.property(PersonEduInstitution.eduInstitutionKind().id().fromAlias("pei")), DQLExpressions.property(EducationalInstitutionTypeKind.id().fromAlias("eitk"))))
            .joinEntity(
                "pei",
                DQLJoinType.left,
                EducationDocumentType.class,
                "edt",
                DQLExpressions.eq(DQLExpressions.property(PersonEduInstitution.documentType().id().fromAlias("pei")), DQLExpressions.property(EducationDocumentType.id().fromAlias("edt"))))
            .joinEntity(
                "pei",
                DQLJoinType.left,
                EmployeeSpeciality.class,
                "es",
                DQLExpressions.eq(DQLExpressions.property(PersonEduInstitution.employeeSpeciality().id().fromAlias("pei")), DQLExpressions.property(EmployeeSpeciality.id().fromAlias("es"))))
            .joinEntity(
                "rad",
                DQLJoinType.left,
                AddressStreet.class,
                "ras",
                DQLExpressions.eq(DQLExpressions.property(Address.street().id().fromAlias("rad")), DQLExpressions.property(AddressStreet.id().fromAlias("ras"))))
            .joinEntity(
                "rad",
                DQLJoinType.left,
                AddressCountry.class,
                "rac",
                DQLExpressions.eq(DQLExpressions.property(Address.country().id().fromAlias("rad")), DQLExpressions.property(AddressCountry.id().fromAlias("rac"))))
            .joinEntity(
                "fad",
                DQLJoinType.left,
                AddressStreet.class,
                "fas",
                DQLExpressions.eq(DQLExpressions.property(Address.street().id().fromAlias("fad")), DQLExpressions.property(AddressStreet.id().fromAlias("fas"))))
            .joinEntity(
                "fad",
                DQLJoinType.left,
                AddressCountry.class,
                "fac",
                DQLExpressions.eq(DQLExpressions.property(Address.country().id().fromAlias("fad")), DQLExpressions.property(AddressCountry.id().fromAlias("fac"))))
            .joinEntity(
                "rad",
                DQLJoinType.left,
                AddressItem.class,
                "rset",
                DQLExpressions.eq(DQLExpressions.property(Address.settlement().id().fromAlias("rad")), DQLExpressions.property(AddressItem.id().fromAlias("rset"))))
            .joinEntity(
                "fad",
                DQLJoinType.left,
                AddressItem.class,
                "fset",
                DQLExpressions.eq(DQLExpressions.property(Address.settlement().id().fromAlias("fad")), DQLExpressions.property(AddressItem.id().fromAlias("fset"))))
            .joinEntity(
                "p",
                DQLJoinType.left,
                FamilyStatus.class,
                "fs",
                DQLExpressions.eq(DQLExpressions.property(Person.familyStatus().id().fromAlias("p")), DQLExpressions.property(FamilyStatus.id().fromAlias("fs"))))
            .joinEntity(
                "p",
                DQLJoinType.left,
                PersonContactData.class,
                "pcd",
                DQLExpressions.eq(DQLExpressions.property(Person.contactData().id().fromAlias("p")), DQLExpressions.property(PersonContactData.id().fromAlias("pcd"))))


            .column(DQLExpressions.property(Person.id().fromAlias("p")), "p_id")
            .column(DQLExpressions.property(IdentityCard.lastName().fromAlias("ic")), "fam")
            .column(DQLExpressions.property(IdentityCard.firstName().fromAlias("ic")), "ima")
            .column(DQLExpressions.property(IdentityCard.middleName().fromAlias("ic")), "otc")
            .column(DQLExpressions.property(IdentityCard.birthDate().fromAlias("ic")), "data_roj")
            .column(DQLExpressions.property(IdentityCard.birthPlace().fromAlias("ic")), "mes_roj")
            .column(DQLExpressions.property(IdentityCard.sex().title().fromAlias("ic")), "pol")
            .column(DQLExpressions.property(AddressCountry.shortTitle().fromAlias("rac")), "grajd")
            .column(DQLExpressions.property(FamilyStatus.title().fromAlias("fs")), "sem_pol") //
            .column(DQLExpressions.property(IdentityCard.seria().fromAlias("ic")), "pas_s")
            .column(DQLExpressions.property(IdentityCard.number().fromAlias("ic")), "pas_n")
            .column(DQLExpressions.property(IdentityCard.issuanceDate().fromAlias("ic")), "pas_d")
            .column(DQLExpressions.property(IdentityCard.issuancePlace().fromAlias("ic")), "pas_v")
            .column(DQLExpressions.property(Address.inheritedPostCode().fromAlias("rad")), "addr")
            .column(DQLExpressions.property(AddressCountry.title().fromAlias("rac")), "strana")
            .column(DQLExpressions.property(Address.id().fromAlias("rad")), "reg")
            .column(DQLExpressions.property(AddressItem.title().fromAlias("rset")), "nas_p")
            .column(DQLExpressions.caseExpr(
                [DQLExpressions.isNotNull(Address.houseUnitNumber().fromAlias("rad"))] as IDQLExpression[],
                [DQLFunctions.concat(
                        DQLExpressions.property(AddressStreet.title().fromAlias("ras")),
                        DQLExpressions.value(", корп."),
                        DQLExpressions.property(Address.houseUnitNumber().fromAlias("rad")),
                        DQLExpressions.value(", д."),
                        DQLExpressions.property(Address.houseNumber().fromAlias("rad")),
                        DQLExpressions.value(", кв."),
                        DQLExpressions.property(Address.flatNumber().fromAlias("rad")))] as IDQLExpression[],
                DQLFunctions.concat(
                        DQLExpressions.property(AddressStreet.title().fromAlias("ras")),
                        DQLExpressions.value(", д."),
                        DQLExpressions.property(Address.houseNumber().fromAlias("rad")),
                        DQLExpressions.value(", кв."),
                        DQLExpressions.property(Address.flatNumber().fromAlias("rad")))), "ul")
            .column(DQLExpressions.property(PersonContactData.phoneFact().fromAlias("pcd")), "tel")
            .column(DQLExpressions.property(Address.inheritedPostCode().fromAlias("fad")), "addr_fac")
            .column(DQLExpressions.property(AddressCountry.title().fromAlias("fac")), "strana")
            .column(DQLExpressions.property(Address.id().fromAlias("fad")), "reg_fac")
            .column(DQLExpressions.property(AddressItem.title().fromAlias("fset")), "nas_p_fac")
            .column(DQLExpressions.caseExpr(
                [DQLExpressions.isNotNull(Address.houseUnitNumber().fromAlias("fad"))] as IDQLExpression[],
                [DQLFunctions.concat(
                        DQLExpressions.property(AddressStreet.title().fromAlias("fas")),
                        DQLExpressions.value(", корп."),
                        DQLExpressions.property(Address.houseUnitNumber().fromAlias("fad")),
                        DQLExpressions.value(", д."),
                        DQLExpressions.property(Address.houseNumber().fromAlias("fad")),
                        DQLExpressions.value(", кв."),
                        DQLExpressions.property(Address.flatNumber().fromAlias("fad")))] as IDQLExpression[],
                DQLFunctions.concat(
                        DQLExpressions.property(AddressStreet.title().fromAlias("fas")),
                        DQLExpressions.value(", д."),
                        DQLExpressions.property(Address.houseNumber().fromAlias("fad")),
                        DQLExpressions.value(", кв."),
                        DQLExpressions.property(Address.flatNumber().fromAlias("fad")))), "ul_fac")
            .column(DQLExpressions.property(PersonContactData.phoneMobile().fromAlias("pcd")), "sot_tel_fac")
            .column(DQLExpressions.property(EducationalInstitutionTypeKind.shortTitle().fromAlias("eitk")), "obr_uch")
            .column(DQLExpressions.property(EducationDocumentType.title().fromAlias("edt")), "tip_doc")
            .column(DQLExpressions.property(GraduationHonour.title().fromAlias("gh")), "step_otl")
            .column(DQLExpressions.property(PersonEduInstitution.seria().fromAlias("pei")), "ser")
            .column(DQLExpressions.property(PersonEduInstitution.number().fromAlias("pei")), "nom")
            .column(DQLExpressions.property(PersonEduInstitution.issuanceDate().fromAlias("pei")), "dat_vyd")
            .column(DQLExpressions.property(EduInstitution.title().fromAlias("ei")), "ou")
            .column(DQLExpressions.property(EmployeeSpeciality.title().fromAlias("es")), "dip_spec")
            .column(DQLExpressions.property(Person.workPlace().fromAlias("p")), "mes_rab")
            .column(DQLExpressions.property(Person.workPlacePosition().fromAlias("p")), "dolj")
            .column(DQLExpressions.caseExpr(
                [DQLExpressions.eq(DQLExpressions.property(PreliminaryEnrollmentStudent.compensationType().code().fromAlias("s")), DQLExpressions.value(UniDefines.COMPENSATION_TYPE_CONTRACT))] as IDQLExpression[],
                [DQLExpressions.value("Да")] as IDQLExpression[],
                DQLExpressions.value("Нет")), "kontr")
            .column(DQLExpressions.caseExpr(
                [DQLExpressions.eq(DQLExpressions.property(PreliminaryEnrollmentStudent.targetAdmission().fromAlias("s")), DQLExpressions.value(Boolean.TRUE))] as IDQLExpression[],
                [DQLExpressions.value("Да")] as IDQLExpression[],
                DQLExpressions.value("Нет")), "cel_pr")

            .column(DQLExpressions.property(AbstractEntrantExtract.paragraph().order().commitDate().fromAlias("aee")), "data_pr")
            .column(DQLExpressions.property(AbstractEntrantExtract.paragraph().order().number().fromAlias("aee")), "nom_pr")
        ;

        builder.where(DQLExpressions.eq(DQLExpressions.property(Entrant.enrollmentCampaign().fromAlias("e")), DQLExpressions.value(campaign)));
        builder.where(DQLExpressions.betweenDays(RequestedEnrollmentDirection.regDate().fromAlias("red"), dateFrom, dateTo));

        if (compensationType != null)
            builder.where(DQLExpressions.eq(DQLExpressions.property(PreliminaryEnrollmentStudent.compensationType().fromAlias("s")), DQLExpressions.value(compensationType)));

        if (studentCategoryList != null)
            builder.where(DQLExpressions.in(DQLExpressions.property(PreliminaryEnrollmentStudent.studentCategory().fromAlias("s")), studentCategoryList));

        if (qualificationsList != null)
            builder.where(DQLExpressions.in(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().educationLevel().qualification().fromAlias("eou")), qualificationsList));

        if (formativeOrgUnitList != null)
            builder.where(DQLExpressions.in(DQLExpressions.property(EducationOrgUnit.formativeOrgUnit().fromAlias("eou")), formativeOrgUnitList));

        if (territorialOrgUnitList != null)
            builder.where(DQLExpressions.in(DQLExpressions.property(EducationOrgUnit.territorialOrgUnit().fromAlias("eou")), territorialOrgUnitList));

        if (educationLevelsHighSchoolList != null)
            builder.where(DQLExpressions.in(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().fromAlias("eou")), educationLevelsHighSchoolList));

        if (developFormList != null)
            builder.where(DQLExpressions.in(DQLExpressions.property(EducationOrgUnit.developForm().fromAlias("eou")), developFormList));

        if (developConditionList != null)
            builder.where(DQLExpressions.in(DQLExpressions.property(EducationOrgUnit.developCondition().fromAlias("eou")), developConditionList));

        if (developTechList != null)
            builder.where(DQLExpressions.in(DQLExpressions.property(EducationOrgUnit.developTech().fromAlias("eou")), developTechList));

        if (developPeriodList != null)
            builder.where(DQLExpressions.in(DQLExpressions.property(EducationOrgUnit.developPeriod().fromAlias("eou")), developPeriodList));

        if (parallel)
            builder.where(DQLExpressions.eq(DQLExpressions.property(PreliminaryEnrollmentStudent.parallel().fromAlias("s")), DQLExpressions.value(Boolean.FALSE)));

        builder.order(DQLExpressions.property(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().entrantRequest().entrant().person().identityCard().lastName().fromAlias("s")));
        builder.order(DQLExpressions.property(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().entrantRequest().entrant().person().identityCard().firstName().fromAlias("s")));
        builder.order(DQLExpressions.property(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().entrantRequest().entrant().person().identityCard().middleName().fromAlias("s")));

        return builder.createStatement(session).list();
    }

    def getBenefits(Long personId)
    {
        return new DQLSelectBuilder()
            .fromEntity(PersonBenefit.class, "pb")
            .column(DQLExpressions.property(PersonBenefit.benefit().shortTitle().fromAlias("pb")))
            .where(DQLExpressions.eq(DQLExpressions.property(PersonBenefit.person().id().fromAlias("pb")), DQLExpressions.value(personId)))
            .createStatement(session)
            .<String> list().join(", ");
    }

    def getLanguages(Long personId)
    {
         String mainLanguage = new DQLSelectBuilder()
            .fromEntity(PersonForeignLanguage.class, "pfl1")
            .column(DQLExpressions.property(PersonForeignLanguage.language().title().fromAlias("pfl1")), "title")
            .where(DQLExpressions.eq(DQLExpressions.property(PersonForeignLanguage.person().id().fromAlias("pfl1")), DQLExpressions.value(personId)))
            .where(DQLExpressions.eq(DQLExpressions.property(PersonForeignLanguage.main().fromAlias("pfl1")), DQLExpressions.value(Boolean.TRUE)))
            .createStatement(session)
            .setMaxResults(1)
            .<String> uniqueResult()

        List<String> list = new ArrayList<String>();

        if (mainLanguage != null)
            list.add(mainLanguage);

        list.addAll(new DQLSelectBuilder()
            .fromEntity(PersonForeignLanguage.class, "pfl")
            .column(DQLExpressions.property(PersonForeignLanguage.language().title().fromAlias("pfl")), "title")
            .where(DQLExpressions.eq(DQLExpressions.property(PersonForeignLanguage.person().id().fromAlias("pfl")), DQLExpressions.value(personId)))
            .where(DQLExpressions.eq(DQLExpressions.property(PersonForeignLanguage.main().fromAlias("pfl")), DQLExpressions.value(Boolean.FALSE)))
            .order(DQLExpressions.property(PersonForeignLanguage.language().title().fromAlias("pfl")))
            .createStatement(session)
            .<String> list())

        return list.join(", ");

    }

    def getPersonNextOfKin(Long personId, String userCode)
    {
        return new DQLSelectBuilder()
            .fromEntity(PersonNextOfKin.class, "pnok")
            .where(DQLExpressions.eq(DQLExpressions.property(PersonNextOfKin.person().id().fromAlias("pnok")), DQLExpressions.value(personId)))
            .where(DQLExpressions.eq(DQLExpressions.property(PersonNextOfKin.relationDegree().userCode().fromAlias("pnok")), DQLExpressions.value(userCode)))
            .createStatement(session)
            .setMaxResults(1)
            .<PersonNextOfKin> uniqueResult();
    }

    def getMilitaryOffice(Long personId, Long academySettlementId, boolean isIncomer)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder
            .fromEntity(PersonMilitaryStatus.class, "ms");
        if (isIncomer)
            builder.column(DQLExpressions.caseExpr(
                [DQLExpressions.ne(DQLExpressions.property(PersonMilitaryStatus.militaryOffice().settlement().id().fromAlias("ms")), DQLExpressions.value(academySettlementId))] as IDQLExpression[],
                [DQLExpressions.property(PersonMilitaryStatus.militaryOffice().title().fromAlias("ms"))] as IDQLExpression[],
                DQLExpressions.value("")), "nas_p")
        else
            builder.column(DQLExpressions.caseExpr(
                [DQLExpressions.eq(DQLExpressions.property(PersonMilitaryStatus.militaryOffice().settlement().id().fromAlias("ms")), DQLExpressions.value(academySettlementId))] as IDQLExpression[],
                [DQLExpressions.property(PersonMilitaryStatus.militaryOffice().title().fromAlias("ms"))] as IDQLExpression[],
                DQLExpressions.value("")), "nas_p")

        builder.where(DQLExpressions.eq(DQLExpressions.property(PersonMilitaryStatus.person().id().fromAlias("ms")), DQLExpressions.value(personId)));

        return builder.createStatement(session).setMaxResults(1).<String> uniqueResult()
    }

    def static getRegion(long addressId)
    {
        Address address = DataAccessServices.dao().get(Address.class, addressId);
        if (address.settlement == null)
            return null;
        AddressItem item = address.settlement;
        for (;item.parent != null;)
            item = item.parent;
        return item.fullTitle;
    }

    def static getDistrict(long addressId)
    {
        Address address = DataAccessServices.dao().get(Address.class, addressId);
        if (address.settlement == null)
            return null;
        AddressItem item = address.settlement;
        int i = 0;
        for (; item.parent != null; i++)
            item = item.parent;
        return i > 1 ? address.settlement.parent.fullTitle : "";
    }
}
