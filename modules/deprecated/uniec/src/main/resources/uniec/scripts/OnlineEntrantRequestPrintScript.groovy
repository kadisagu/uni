/* $Id$ */
// Copyright 2006-2012 Tandem Service Software
package uniec.scripts

import org.hibernate.Session
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant
import ru.tandemservice.uniec.entity.onlineentrant.OnlineRequestedEnrollmentDirection

return new OnlineEntrantRequestPrint(                           // стандартные входные параметры скрипта
        session: session,                                       // сессия
        template: template,                                     // шаблон
        onlineEntrant: session.get(OnlineEntrant.class, object) // объект печати
).print()

/**
 * Алгоритм печати онлайн-заявления абитуриента
 *
 * @author Vasily Zhukov
 * @since 22.02.2012
 */
class OnlineEntrantRequestPrint
{
    Session session
    byte[] template
    OnlineEntrant onlineEntrant
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()

    def print()
    {
        def direction = DQL.createStatement(session, /
                from ${OnlineRequestedEnrollmentDirection.class.simpleName}
                where ${OnlineRequestedEnrollmentDirection.entrant().id()} = ${onlineEntrant.id}
        /).setMaxResults(1).<OnlineRequestedEnrollmentDirection> uniqueResult()
        def ou = !direction ? null : direction.enrollmentDirection.educationOrgUnit

        fillNextOfKin()

        TopOrgUnit academy = TopOrgUnit.instance

        // данные по абитуриенту
        im.put('s', onlineEntrant.sex.male ? '' : 'а')
        im.put('academyTitle', academy.genitiveCaseTitle ?: academy.title)
        im.put('academyShortTitle', academy.shortTitle)
        im.put('personalNumber', onlineEntrant.personalNumber as String)
        im.put('lastName', onlineEntrant.lastName)
        im.put('firstName', onlineEntrant.firstName)
        im.put('middleName', onlineEntrant.middleName)
        im.put('birthDate', onlineEntrant.birthDate.format('dd.MM.yyyy'))
        im.put('birthPlace', onlineEntrant.birthPlace)
        im.put('passportCitizenship', onlineEntrant.passportCitizenship?.title)
        im.put('passportNumber', onlineEntrant.passportSeria ?
            (onlineEntrant.passportSeria + (onlineEntrant.passportNumber ? ' № ' + onlineEntrant.passportNumber : '')) :
            (onlineEntrant.passportNumber ? '№ ' + onlineEntrant.passportNumber : ''))
        im.put('passportIssuanceDate', onlineEntrant.passportIssuanceDate.format('dd.MM.yyyy'))
        im.put('passportIssuancePlace', onlineEntrant.passportIssuancePlace)
        im.put('registrationAddress', onlineEntrant.registrationAddress.titleWithFlat)
        im.put('homePhoneNumber', onlineEntrant.phoneFact)
        im.put('mobilePhoneNumber', onlineEntrant.phoneMobile)
        im.put('actualAddress', onlineEntrant.actualAddress.titleWithFlat)
        im.put('benefit', onlineEntrant.benefit?.title)
        im.put('competition', onlineEntrant.certificateNumber ? 'ЕГЭ' : '')
        im.put('graduationYear', onlineEntrant.eduDocumentYearEnd ?
            onlineEntrant.eduDocumentYearEnd + ' г.:' :
            '          ')
        im.put('educationLevel', onlineEntrant.eduDocumentLevel ?
            onlineEntrant.eduDocumentLevel.shortTitle + ' — ' + onlineEntrant.eduDocumentLevel.title :
            null)
        im.put('eduInstitution', !onlineEntrant.eduInstitution ? '' : (onlineEntrant.eduInstitution.title + ' ((' +
                onlineEntrant.eduInstitution.address.settlement.inheritedRegionCode + '), ' +
                onlineEntrant.eduInstitution.address.settlement.titleWithType + ')'))
        im.put('eduDocument', [onlineEntrant.eduDocumentType.title,
                [onlineEntrant.eduDocumentSeria, onlineEntrant.eduDocumentNumber].grep().join(' '),
                onlineEntrant.eduDocumentYearEnd ? onlineEntrant.eduDocumentYearEnd + ' г.' : null].grep().join(', '))
        im.put('graduationHonour', onlineEntrant.eduDocumentGraduationHonour ? 'да' : 'нет')
        im.put('foreignLanguage', onlineEntrant.foreignLanguage?.title)
        im.put('workPlace', onlineEntrant.workPlace ?
            onlineEntrant.workPlace + (onlineEntrant.workPost ? ', ' : '') :
            '')
        im.put('workPost', onlineEntrant.workPost)
        im.put('beginArmy', onlineEntrant.beginArmy?.format('yyyy'))
        im.put('endArmy', onlineEntrant.endArmy?.format('yyyy'))
        im.put('endArmyYear', onlineEntrant.endArmyYear as String)

        // данные по выбранному направлению
        im.put('formativeOrgUnit', !ou ? '' : ou.formativeOrgUnit.with { it.nominativeCaseTitle ?: it.title })
        im.put('educationLevelType', !ou ? '' : ou.educationLevelHighSchool.educationLevel.with {
            it.specialityPrintTitleMode || it.specialization ? 'специальности' : 'направлению подготовки'
        })
        im.put('educationLevelTitle', !ou ? '' : ou.educationLevelHighSchool.with {
            it.educationLevel.levelType.profile ? it.educationLevel.parentLevel.displayableTitle : it.printTitle
        })
        im.put('developForm', !ou ? '' : ou.developForm.accCaseTitle)
        im.put('compensationType', !ou ? '' : direction.compensationType.budget ?
            'на госбюджетные места' :
            'на места по договору с оплатой стоимости обучения')
        im.put('profile', !ou ? '' : ou.educationLevelHighSchool.with {
            it.educationLevel.levelType.profile ? it.title : null
        })
        im.put('studentCategory', !ou ? '' : ou.educationLevelHighSchool.educationLevel.levelType.master ?
            'магистрант' :
            direction.studentCategory.title)

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(template, im, tm),
                fileName: 'Онлайн-заявление абитуриента.rtf']
    }

    def fillNextOfKin()
    {
        def relationDegree = onlineEntrant.nextOfKinRelationDegree
        if (relationDegree)
        {
            def fullFio = [onlineEntrant.nextOfKinLastName,
                    onlineEntrant.nextOfKinFirstName,
                    onlineEntrant.nextOfKinMiddleName].grep().join(' ')
            def description = [fullFio,
                    onlineEntrant.nextOfKinWorkPlace,
                    onlineEntrant.nextOfKinWorkPost,
                    onlineEntrant.nextOfKinPhone].grep().join(', ')
            tm.put('NOK', [ [ relationDegree.title, description ] ] as String[][])
        } else
        {
            tm.put('NOK', null as String[][])
        }
    }
}
