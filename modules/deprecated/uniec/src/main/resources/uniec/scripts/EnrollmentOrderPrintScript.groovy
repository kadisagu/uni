/* $Id$ */
// Copyright 2006-2012 Tandem Service Software
package uniec.scripts

import org.hibernate.Session
import org.tandemframework.core.exception.ApplicationException
import org.tandemframework.core.view.formatter.DoubleFormatter
import org.tandemframework.rtf.RtfBean
import org.tandemframework.rtf.data.IRtfData
import org.tandemframework.rtf.document.RtfHeader
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import ru.tandemservice.uni.dao.IUniBaseDao
import org.tandemframework.shared.person.base.entity.IdentityCard
import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager
import ru.tandemservice.uniec.dao.IEntrantDAO
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder
import ru.tandemservice.unimove.IAbstractExtract
import ru.tandemservice.unimove.IAbstractParagraph
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising
import ru.tandemservice.unimv.dao.visa.IVisaDao
import org.tandemframework.shared.organization.base.entity.TopOrgUnit

return new EnrollmentOrderPrint(                          // стандартные входные параметры скрипта
        session: session,                                 // сессия
        template: template,                               // шаблон
        order: session.get(EnrollmentOrder.class, object) // объект печати
).print()

/**
 * Алгоритм печати приказов о зачислении абитуриентов
 *
 * @author Vasily Zhukov
 * @since 29.12.2011
 */
class EnrollmentOrderPrint
{
    Session session
    byte[] template
    EnrollmentOrder order
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()

    def print()
    {
        def coreDao = IUniBaseDao.instance.get()
        def academy = TopOrgUnit.instance

        // заполнение меток
        im.put('commitDate', order.commitDate?.format('dd.MM.yyyy'))
        im.put('enrollmentDate', order.enrollmentDate?.format('dd.MM.yyyy'))
        im.put('orderNumber', order.number)
        im.put('highSchoolTitle', academy.title)
        im.put('highSchoolCity', academy.address?.settlement?.titleWithType)
        im.put('orderBasicText', order.orderBasicText)
        im.put('orderText', order.orderText)
        im.put('textParagraph', order.textParagraph)
        im.put('executor', order.executor)
        im.put('orderReason', order.reason?.title)
        im.put('orderBasic', order.basic?.title)

        List<IAbstractParagraph> paragraphList = order.paragraphList
        if (paragraphList.size() > 0)
        {
            List<IAbstractExtract> extractList = paragraphList[0].extractList
            if (extractList.size() > 0)
            {
                EnrollmentExtract extract = (EnrollmentExtract) extractList[0]
                ru.tandemservice.uniec.component.enrollmentextract.EnrollmentOrderPrint.initOrgUnit(im,
                        extract.entity.educationOrgUnit.formativeOrgUnit, 'formativeOrgUnit', '')
            }
        }

        // заполнение виз
        def visaGroupList = coreDao.getList(GroupsMemberVising.class)
        def printVisaGroupMap = visaGroupList.<String, List<String[]>> collectEntries { [it.printLabel, []] }
        def printVisaList = []
        for (def visa : IVisaDao.instance.get().getVisaList(order))
        {
            def iof = (visa.possibleVisa.entity.person.identityCard as IdentityCard).iof
            def row = [visa.possibleVisa.title, iof] as String[]
            printVisaGroupMap.get(visa.groupMemberVising.printLabel).add(row)
            printVisaList.add(row)
        }
        printVisaGroupMap.each { tm.put(it.key, it.value as String[][]) }
        tm.put('VISAS', printVisaList as String[][])

        // заполнение параграфов
        def document = new RtfReader().read(template)
        fillParagraphs(document.header)
        im.modify(document)
        tm.modify(document)

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(document), fileName: 'EnrollmentOrder.rtf']
    }

    void fillParagraphs(RtfHeader header)
    {
        def paragraphDocument = new RtfReader()
                .read(EcOrderManager.instance().dao().getParagraphTemplate(order.type))

        List<IRtfElement> paragraphList = []
        for (IAbstractParagraph paragraph : order.paragraphList)
        {
            List<EnrollmentExtract> enrollmentExtractList = (List<EnrollmentExtract>) paragraph.extractList
            if (enrollmentExtractList.size() == 0)
                throw new ApplicationException("Пустой параграф №${paragraph.number}.")

            // первая выписка из параграфа
            def extract = enrollmentExtractList.get(0)

            // получаем rtf-строку студентов из параграфа
            def rtf = new RtfString().par()
            int counter = 1
            for (def enrollmentExtract : enrollmentExtractList)
            {
                def requestedEnrollmentDirection = enrollmentExtract.entity.requestedEnrollmentDirection
                def person = requestedEnrollmentDirection.entrantRequest.entrant.person
                rtf.append((counter++).toString()).append('.  ').append(person.fullFio)

                def levelTitle = person.personEduInstitution?.educationLevel?.shortTitle
                if (levelTitle)
                    rtf.append(' (').append(levelTitle).append(')')

                def sumMark = IEntrantDAO.instance.get().getSumFinalMarks(requestedEnrollmentDirection)
                if (sumMark)
                    rtf.append(' ').append(IRtfData.ENDASH).append(' ').append(DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS.format(sumMark))
                rtf.par()
            }

            // подготавливаем клон шаблона параграфа для вставки в печатную форму приказа
            def paragraphPart = paragraphDocument.clone
            RtfUtil.modifySourceList(header, paragraphPart.header, paragraphPart.elementList)

            // заполняем метки в шаблоне параграфа
            def ou = extract.entity.educationOrgUnit
            new RtfInjectModifier()
                    .put('STUDENT_LIST', rtf)
                    .put('parNumber', paragraph.number.toString())
                    .put('developForm', ou.developForm.title.toLowerCase())
                    .put('developPeriod', ou.developPeriod.title)
                    .put('educationOrgUnit', ou.educationLevelHighSchool.printTitle)
                    .put('educationType_D', ou.educationLevelHighSchool.educationLevel.levelType.dativeCaseShortTitle)
                    .modify(paragraphPart)

            def group = RtfBean.elementFactory.createRtfGroup()
            group.elementList = paragraphPart.elementList
            paragraphList.add(group)
        }

        im.put('PARAGRAPHS', paragraphList)
    }
}
