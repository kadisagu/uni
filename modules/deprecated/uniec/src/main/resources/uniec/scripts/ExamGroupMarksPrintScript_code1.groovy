/* $Id$ */
// Copyright 2006-2012 Tandem Service Software
package uniec.scripts

import java.text.NumberFormat
import org.apache.commons.collections.keyvalue.MultiKey
import org.hibernate.Session
import org.tandemframework.core.exception.ApplicationException
import org.tandemframework.hibsupport.builder.MQBuilder
import org.tandemframework.hibsupport.builder.expression.MQExpression
import org.tandemframework.rtf.RtfBean
import org.tandemframework.rtf.document.text.table.RtfTable
import org.tandemframework.rtf.document.text.table.cell.RtfCell
import org.tandemframework.rtf.document.text.table.row.RtfRow
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.util.IRtfRowSplitInterceptor
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.entity.Person
import ru.tandemservice.uniec.dao.examgroup.IExamGroupSetDao
import ru.tandemservice.uniec.dao.examgroup.logic1.ExamGroupLogicSample1
import ru.tandemservice.uniec.entity.catalog.codes.EntrantStateCodes
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation
import ru.tandemservice.uniec.util.EntrantDataUtil
import ru.tandemservice.uniec.entity.entrant.*

/**
 * @author Vasily Zhukov
 * @since 24.02.2012
 */
return new ExamGroupMarksPrint_code1(// стандартные входные параметры скрипта
        session: session,            // сессия
        template: template,          // шаблон
        ids: object,                 // объект печати (идентификаторы экзам. групп)
        withResult: true
).print()

/**
 * Алгоритм печати списка экзам. групп (с результатами) (Механизм 1)
 *
 * @author Vasily Zhukov
 * @since 22.12.2011
 */
class ExamGroupMarksPrint_code1
{
    Session session
    byte[] template
    List<Long> ids
    boolean withResult

    def print()
    {
        if (ids.empty)
            throw new ApplicationException('Нет данных для печати.')

        // загружаем сокращенное название вуза
        def vuzShortTitle = TopOrgUnit.instance.shortTitle

        // загружаем шаблон
        def template = new RtfReader().read(template)

        // создаем результирующий документ, наследуя настройки из шаблона
        def result = RtfBean.elementFactory.createRtfDocument()
        result.header = template.header
        result.settings = template.settings

        // создаем красный цвет
        def redColorIndex = result.header.colorTable.addColor(255, 0, 0)
        def createDate = new Date().format('dd.MM.yyyy')
        for (def id: ids)
        {
            // загружаем экзаменационную группу
            def group = (ExamGroup) session.get(ExamGroup.class, id)

            // нет группы, нечего печатать
            if (group == null) continue

            // не портим шаблон
            def document = template.clone

            // декодируем ключ группы
            def key = ExamGroupLogicSample1.parseKey(session, group.key)

            // подставляем значения меток
            def im = new RtfInjectModifier()
            im.put('examGroupTypeTitle', key.examGroupType.title)
            im.put('developForm', key.developForm.title)
            im.put('compensationTypeStr', key.compensationType.budget ?
                'За счет средств федерального бюджета' : 'На места с оплатой стоимости обучения')
            im.put('reportTitle', withResult ? 'РЕЗУЛЬТАТЫ' : 'СПИСОК')
            im.put('subTitle', withResult ? 'сдачи' : 'допущенных к сдаче')
            im.put('createDate', createDate)
            im.put('groupTitle', group.title)
            im.put('vuzShortTitle', vuzShortTitle)
            im.modify(document)

            // заполняем таблицу документа
            def tm = getRtfTableModifier(group, 4, 6, redColorIndex)
            tm.modify(document)

            // добавляем получившийся документ в результирующий
            result.elementList.addAll(document.elementList)
        }

        if (result.elementList.empty)
            throw new ApplicationException('Нет данных для печати.')

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(result), fileName: 'examGroupMarks.rtf']
    }

    RtfTableModifier getRtfTableModifier(ExamGroup group, int fioColNum, int disciplineColNum, int redColorIndex)
    {
        def tm = new RtfTableModifier()

        def disciplineList = new ArrayList<Discipline2RealizationWayRelation>()
        def invalidRowIndex = new HashSet<Integer>()

        tm.put('T', getTableData(group, invalidRowIndex, disciplineList))
        tm.put('T', new RtfRowIntercepterBase(){
            @Override
            void beforeModify(RtfTable table, int currentRowIndex)
            {
                if (disciplineList.empty) return

                def parts = (1..disciplineList.size()).each { i -> 1 } as int[]
                
                // в заголовок таблицы вставляем названия дисциплин, разделяя ячейку в равных пропорциях
                RtfUtil.splitRow(table.rowList.get(currentRowIndex - 1), disciplineColNum, new IRtfRowSplitInterceptor(){
                    @Override
                    public void intercept(RtfCell newCell, int index)
                    {
                        newCell.elementList = new RtfString().append(disciplineList.get(index).title).toList()
                    }
                }, parts)

                // в строке с меткой таблицы тоже нужно разделить колонку disciplineColNum
                RtfUtil.splitRow(table.rowList.get(currentRowIndex), disciplineColNum, null, parts)
            }

            @Override
            List<IRtfElement> beforeInject(RtfTable t, RtfRow row, RtfCell cell, int rowNum, int colNum, String value)
            {
                // колонку ФИО печатаем красным
                if (colNum == fioColNum && invalidRowIndex.contains(rowNum))
                    return new RtfString().color(redColorIndex).append(value).toList();
                return null
            }
        })

        return tm
    }

    String[][] getTableData(ExamGroup examGroup,
                            Set<Integer> invalidRowIndex,
                            List<Discipline2RealizationWayRelation> disciplineList)
    {
        def rowList = new MQBuilder(ExamGroupRow.ENTITY_CLASS, 'r')
                .add(MQExpression.eq('r', ExamGroupRow.L_EXAM_GROUP, examGroup))
                .<ExamGroupRow> getResultList(session)

        def validRowIds = new HashSet<Long>()
        for (def row: rowList)
            if (IExamGroupSetDao.instance.get().isExamGroupRowValid(row))
                validRowIds.add(row.id)

        def stateCodePath = RequestedEnrollmentDirection.state().code()
        def requestPath = RequestedEnrollmentDirection.entrantRequest()
        def query = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, 'd')
                .addJoinFetch('d', requestPath, 'request')
                .addJoinFetch('request', EntrantRequest.entrant(), 'entrant')
                .addJoinFetch('entrant', Entrant.person(), 'person')
                .addJoinFetch('person', Person.identityCard(), 'card')
                .addDomain('g', ExamGroupRow.ENTITY_CLASS)
                .add(MQExpression.eqProperty('d', requestPath, 'g', ExamGroupRow.entrantRequest()))
                .add(MQExpression.eq('g', ExamGroupRow.examGroup(), examGroup))

        if (withResult)
        {
            query.add(MQExpression.in('g', ExamGroupRow.id(), validRowIds))
            query.add(MQExpression.notEq('d', stateCodePath, EntrantStateCodes.TAKE_DOCUMENTS_AWAY))
            query.add(MQExpression.notEq('d', stateCodePath, EntrantStateCodes.OUT_OF_COMPETITION))
            query.add(MQExpression.eq('d', requestPath.entrant().archival(), Boolean.FALSE))
        }

        def dataUtil = new EntrantDataUtil(session, examGroup.examGroupSet.enrollmentCampaign, query)

        def targetAdmissionRequestIds = new HashSet<Long>()
        for (RequestedEnrollmentDirection direction: dataUtil.directionSet)
            if (direction.targetAdmission)
                targetAdmissionRequestIds.add(direction.entrantRequest.id)

        def benefitMap = new HashMap<Long, String>()
        EntrantDataUtil.getPersonBenefitMap(session, query).collectEntries(benefitMap,
                { key, value -> [key.id, value.collect {it.benefit.shortTitle}.sort().join(', ')] })

        def disciplineSet = new HashSet<Discipline2RealizationWayRelation>()
        def chosenMarkData = new Hashtable<MultiKey, Double>()
        def directionMarkData = new HashMap<EntrantRequest, Double>()
        def directionData = new HashMap<EntrantRequest, RequestedEnrollmentDirection>()

        for (def direction: dataUtil.directionSet)
        {
            for (def chosen: dataUtil.getChosenEntranceDisciplineSet(direction))
            {
                disciplineSet.add(chosen.enrollmentCampaignDiscipline)
                MultiKey key = new MultiKey(direction.entrantRequest, chosen.enrollmentCampaignDiscipline)
                def chosenMark = chosenMarkData.get(key)
                def finalMark = chosen.finalMark ?: -1 // -1 означает, что еще не сдал, но должен сдавать
                chosenMarkData.put(key, chosenMark == null ? finalMark : Math.max(chosenMark, finalMark))
            }

            def directionMark = directionMarkData.get(direction.entrantRequest)
            def finalMark = dataUtil.getFinalMark(direction)
            if (directionMark == null || finalMark > directionMark)
                directionMarkData.put(direction.entrantRequest, finalMark)
            def firstDirection = directionData.get(direction.entrantRequest)
            if (firstDirection == null || direction.priority < firstDirection.priority)
                directionData.put(direction.entrantRequest, direction);
        }
        disciplineList.addAll(disciplineSet)
        disciplineList.sort {it.title}
        rowList.sort {it.entrantRequest.entrant.person.identityCard.fullFio}

        def format = NumberFormat.instance
        format.maximumFractionDigits = 2
        def result = new ArrayList<String[]>()
        for (int i = 0; i < rowList.size(); i++)
        {
            def row = rowList.get(i)
            if (withResult)
            {
                if (!validRowIds.contains(row.id)) continue
            } else
            {
                if (!validRowIds.contains(row.id)) invalidRowIndex.add(i)
            }

            def request = row.entrantRequest
            def person = request.entrant.person
            def medalist = person.personEduInstitution?.medalist
            def targetAdmission = targetAdmissionRequestIds.contains(request.id)

            def line = new String[7 + disciplineList.size()]
            int k = 0
            line[k++] = i + 1
            line[k++] = (medalist ? 'М' : '') + (targetAdmission ? 'Ц' : '')
            line[k++] = benefitMap.get(person.id)
            line[k++] = directionData.get(request)?.enrollmentDirection?.educationOrgUnit?.shortTitle
            line[k++] = person.identityCard.fullFio
            line[k++] = request.stringNumber

            if (disciplineList.empty)
                k++
            else
                for (def discipline: disciplineList)
                {
                    def mark = chosenMarkData.get(new MultiKey(request, discipline))
                    line[k++] = mark == null ? 'x' : (mark == -1 ? '' : format.format(mark))
                }

            if (withResult)
            {
                def mark = directionMarkData.get(request)
                // если нет оценки, значит ни одно направление приема из этого заявления не попало в статистику
                // значит, либо абитуриент архивный, либо по всем направлениям этого заявления у него
                // состояние 'забрал документы' или 'выбыл из конкурса'
                if (mark == null) continue
                line[k] = format.format(mark)
            }
            result.add(line)
        }

        // добавляем еще три пустые строки 
        result.add(null)
        result.add(null)
        result.add(null)

        return result as String[][]
    }
}
