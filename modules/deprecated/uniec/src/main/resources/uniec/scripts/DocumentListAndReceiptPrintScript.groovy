/* $Id$ */
// Copyright 2006-2012 Tandem Service Software
package uniec.scripts

import org.hibernate.Session
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.bo.Person.PersonManager
import ru.tandemservice.uniec.entity.catalog.EnrollmentDocument
import ru.tandemservice.uniec.entity.entrant.EntrantEnrollmentDocument
import ru.tandemservice.uniec.entity.entrant.EntrantRequest
import ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate
import ru.tandemservice.uniec.entity.settings.UsedEnrollmentDocument

return new DocumentListAndReceiptPrint(                           // стандартные входные параметры скрипта
        session: session,                                         // сессия
        template: template,                                       // шаблон
        entrantRequest: session.get(EntrantRequest.class, object) // объект печати
).print()

/**
 * Алгоритм печати описи и расписки абитуриента
 *
 * @author Vasily Zhukov
 * @since 23.02.2012
 */
class DocumentListAndReceiptPrint
{
    Session session
    byte[] template
    EntrantRequest entrantRequest
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()

    def print()
    {
        def person = entrantRequest.entrant.person

        im.put('highSchoolTitle', TopOrgUnit.instance.title)
        im.put('inventoryNumber', entrantRequest.stringNumber)
        im.put('FIO', person.fullFio)
        im.put('receiptNumber', entrantRequest.stringNumber)
        im.put('fromFIO', PersonManager.instance().declinationDao().getCalculatedFIODeclination(person.identityCard, InflectorVariantCodes.RU_GENITIVE))

        // получаем названия документов согласно настройке 'Используемые документы для подачи в ОУ и их порядок' 
        def titles = getTitles()

        int i = 1
        tm.put('T1', titles.collect {[i++, it]} as String[][])
        i = 1
        tm.put('T2', titles.collect {[(i++) + '.', it]} as String[][])

//        def directions = DQL.createStatement(session, /
//                from ${RequestedEnrollmentDirection.class.simpleName}
//                where ${RequestedEnrollmentDirection.entrantRequest().id()} = ${entrantRequest.id}
//                order by ${RequestedEnrollmentDirection.priority()}
//        /).<RequestedEnrollmentDirection> list()
//
//        im.put('groupFromDirection', directions.empty ? "" : StringUtils.trimToEmpty(directions[0].group))

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(template, im, tm),
                fileName: "Опись и расписка абитуриента ${person.identityCard.fullFio}.rtf"]
    }

    def getTitles()
    {
        def entrant = entrantRequest.entrant          // абитуриент
        def edu = entrant.person.personEduInstitution // основное законченное образовательное учреждение

        // получаем список документов из заявления в порядке приоритета
        def documents = DQL.createStatement(session, /
                from ${EntrantEnrollmentDocument.class.simpleName}
                where ${EntrantEnrollmentDocument.entrantRequest().id()} = ${entrantRequest.id}
                order by ${EntrantEnrollmentDocument.enrollmentDocument().priority()}
        /).<EntrantEnrollmentDocument> list()

        // получаем номера всех зачтенных свидетельств ЕГЭ абитуриента
        def stateExamCertificateList = DQL.createStatement(session, /
                select ${EntrantStateExamCertificate.number()}
                from ${EntrantStateExamCertificate.class.simpleName}
                where ${EntrantStateExamCertificate.entrant().id()} = ${entrant.id} and
                      ${EntrantStateExamCertificate.accepted()} = ${Boolean.TRUE}
        /).<String> list()

        def stateExamCertificateStr = new StringBuilder()
        for (String certificate: stateExamCertificateList)
            stateExamCertificateStr.append("№ " + [certificate.substring(0,2), certificate.substring(2,11), certificate.substring(11)].join("-")).append(", ")

        def stateExamCertificate = stateExamCertificateStr.toList().isEmpty() ? "" : stateExamCertificateStr.substring(0, stateExamCertificateStr.size() - 2)

        // загружаем настройку 'Используемые документы для подачи в ОУ и их порядок'
        def settingMap = DQL.createStatement(session, /
                from ${UsedEnrollmentDocument.class.simpleName}
                where ${UsedEnrollmentDocument.enrollmentCampaign().id()} = ${entrant.enrollmentCampaign.id}
        /).<UsedEnrollmentDocument> list()
                .<EnrollmentDocument, UsedEnrollmentDocument> collectEntries {[it.enrollmentDocument, it]}

        // для каждого документа, согласно настройке, получаем его название
        def titles = new ArrayList<String>()
        for (def document: documents)
        {
            def title = new StringBuilder(document.enrollmentDocument.title)

            def setting = settingMap.get(document.enrollmentDocument)

            if (setting?.printOriginalityInfo)
                title.append(' (').append(document.copy ? 'копия' : 'оригинал').append(')')

            if (setting?.printEduDocumentAttachmentInfo)
            {
                if (edu.seriaApplication != null) title.append(', серия ').append(edu.seriaApplication);
                if (edu.numberApplication != null) title.append(', № ').append(edu.numberApplication);
            }

            if ((setting?.printStateExamCertificateNumber && !stateExamCertificate.isEmpty()) || (setting?.printEducationDocumentInfo && edu))
            {
                title.append(' - ')

                if (setting?.printEducationDocumentInfo && edu)
                {

                    if (edu.eduInstitution)
                        title.append(edu.eduInstitution.title).append(' ')

                    title.append(edu.addressItem.titleWithType).append(' ').append('в ').append(edu.yearEnd).append('г. ')
                    if (edu.seria || edu.number)
                        title.append('документ №').append([edu.seria, edu.number].grep().join(' '))
                }

                if ((setting?.printStateExamCertificateNumber && !stateExamCertificate.isEmpty()) && (setting?.printEducationDocumentInfo && edu))
                    title.append("; ")

                if (setting?.printStateExamCertificateNumber && !stateExamCertificate.isEmpty())
                {
                    title.append(stateExamCertificate)
                }
            }
            titles.add(title.toString())
        }
        return titles
    }
}
