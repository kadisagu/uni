/* $Id$*/

package uniec.scripts

import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.core.view.formatter.DoubleFormatter
import org.tandemframework.hibsupport.builder.MQBuilder
import org.tandemframework.hibsupport.builder.expression.MQExpression
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.rtf.document.text.table.RtfTable
import org.tandemframework.rtf.document.text.table.cell.RtfCell
import org.tandemframework.rtf.document.text.table.row.RtfRow
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.person.base.entity.Person
import org.tandemframework.shared.person.base.entity.PersonBenefit
import ru.tandemservice.uni.UniDefines
import ru.tandemservice.uniec.UniecDefines
import ru.tandemservice.uniec.entity.entrant.EntrantRequest
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection
import ru.tandemservice.uniec.util.EntrantDataUtil

return new EntrantLetterboxPrint(                                   // стандартные входные параметры скрипта
        session: session,                                         // сессия
        template: template,                                       // шаблон
        entrantRequest: session.get(EntrantRequest.class, object) // объект печати
).print()

/**
 * Алгоритм печати заявления абитуриента
 *
 * @author Vasily Zhukov
 * @since 22.12.2011
 */
class EntrantLetterboxPrint
{
  Session session
  byte[] template
  EntrantRequest entrantRequest
  def im = new RtfInjectModifier()
  def tm = new RtfTableModifier()

  def print()
  {
    // получаем список выбранных направлений приема
    def directions = DQL.createStatement(session, /
                from ${RequestedEnrollmentDirection.class.simpleName}
                where ${RequestedEnrollmentDirection.entrantRequest().entrant().id()} = ${entrantRequest.entrant.id}
                order by ${RequestedEnrollmentDirection.entrantRequest().regDate()}, ${RequestedEnrollmentDirection.priority()}
        /).<RequestedEnrollmentDirection> list()

    fillRequestedDirectionList(directions)
    fillInjectParameters(directions[0])

    // стандартные выходные параметры скрипта
    return [document: RtfUtil.toByteArray(template, im, tm),
            fileName: "Титульный лист личного дела ${entrantRequest.entrant.person.identityCard.lastName} ${entrantRequest.entrant.person.identityCard.firstName}.rtf"]
  }

  void fillRequestedDirectionList(List<RequestedEnrollmentDirection> directions)
  {
    def fpd = directions.get(0);

    def query = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, 'd').add(MQExpression.in("d", "id", directions.collect { it.id }))
    def dataUtil = new EntrantDataUtil(session, fpd.entrantRequest.entrant.enrollmentCampaign, query)

    def benefits = getBenefits(fpd.entrantRequest.entrant.person);

    // заполняем блок с выбранными направлениями приема
    def i = 1
    String[][] directionData = directions.collect {
      def chosen = dataUtil.getChosenEntranceDisciplineSet(it).sort({it.id});
      def sum = chosen.collect({it.finalMark == null ? 1 : 0}).sum() == 0 ? chosen.collect({it.finalMark}).sum() : null
      [
              i++,
              getCode(it),
              it.entrantRequest.stringNumber,
              DateFormatter.DEFAULT_DATE_FORMATTER.format(it.entrantRequest.regDate),
              chosen.collect({"$it.enrollmentCampaignDiscipline.title" + (it.finalMark != null ? " – ${StringUtils.trimToEmpty(DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(it.finalMark))}" : "")}).grep().join("[PAR]"),
              sum,
              it.competitionKind.code.equals(UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION) ? benefits : "",
              it.competitionKind.code.equals(UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION) ? "" : it.competitionKind.title]
    }
    tm.put('T', directionData)

      tm.put('T', new RtfRowIntercepterBase() {
          @Override
          List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
          {
              if (value != null && value.contains("[PAR]"))
              {
                def subject = new RtfString()
                def b = 0;
                def a = value.indexOf("[PAR]", 0)
                while (a != -1)
                {
                    subject.append(value.substring(b, a)).par()
                    b = a + 5
                    a = value.indexOf("[PAR]", a + 1)
                }
                subject.append(value.substring(b))
                return subject.toList()
              }
          }
      })
  }

  static String getCode(RequestedEnrollmentDirection direction)
  {
    def ou = direction.enrollmentDirection.educationOrgUnit
    return ([ou.educationLevelHighSchool.shortTitle,
            ou.developForm.shortTitle,
            ou.developCondition.code.equals(UniDefines.DEVELOP_CONDITION_FULL_TIME) ? "" : ou.developCondition.shortTitle,
            direction.compensationType.shortTitle]).grep().join('-')
  }

  void fillInjectParameters(RequestedEnrollmentDirection firstPriorityDirection)
  {
    def entrant = entrantRequest.entrant
    def person = entrant.person
    def card = person.identityCard
    def lastEduInstitution = person.personEduInstitution

    im.put('entrantNumber', entrant.personalNumber)
    im.put('lastName', card.lastName)
    im.put('firstName', card.firstName)
    im.put('middleName', card.middleName)
    im.put('passport', card.title)
    im.put('birthDate', card.birthDate?.format('dd.MM.yyyy'))
    im.put('edEndYear', lastEduInstitution?.yearEnd as String)
  }

  def getBenefits(Person person)
  {
    def benefits = DQL.createStatement(session, /
                select ${PersonBenefit.benefit().title()}
                from ${PersonBenefit.class.simpleName}
                where ${PersonBenefit.person().id()}=${person.id}
                order by ${PersonBenefit.benefit().title()}
        /).<String> list()

    return benefits.empty ? 'Льгот нет' : benefits.join(', ')
  }
}
