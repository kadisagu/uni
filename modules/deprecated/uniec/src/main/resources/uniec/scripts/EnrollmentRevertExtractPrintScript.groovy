/* $Id$ */
package uniec.scripts

import org.hibernate.Session
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.bo.Person.PersonManager
import ru.tandemservice.uniec.entity.orders.EnrollmentRevertExtract

import java.text.SimpleDateFormat

return new EnrollmentRevertExtractPrint(
        session: session,
        template: templateDocument,
        extract: session.get(EnrollmentRevertExtract.class, object)
).print()
/**
 * @author Nikolay Fedorovskih
 * @since 08.08.2013
 */
class EnrollmentRevertExtractPrint
{
    Session session
    RtfDocument template
    EnrollmentRevertExtract extract

    def print()
    {
        def document = template
        def im = new RtfInjectModifier()

        def order = extract.order
        def academy = TopOrgUnit.instance
        def canceledOrder = extract.canceledExtract.order
        def person = extract.entity.entrantRequest.entrant.person

        im.put('highSchoolTitle_G', academy.genitiveCaseTitle ?: academy.printTitle)
        im.put('orderDate', order.commitDate != null ? (new SimpleDateFormat("«dd» MMMMM yyyy", RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU).format(order.commitDate) + ' г.') : null)
        im.put('orderNumber', order.number)
        im.put('canceledOrderDate', canceledOrder.commitDate != null ? new SimpleDateFormat("dd.MM.yyyy").format(canceledOrder.commitDate) : null)
        im.put('canceledOrderNumber', canceledOrder.number)
        im.put('fioEntrant_A', PersonManager.instance().declinationDao().getDeclinationFIO(person.identityCard, GrammaCase.ACCUSATIVE))
        im.put('direction', extract.educationOrgUnit.educationLevelHighSchool.printTitle)
        im.put('enrFine', person.male ? 'ого' : 'ую')

        im.modify(document)

        return [document: document, fileName: 'EnrollmentRevertExtract.rtf']
    }
}