/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.base.bo.Load.logic;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import org.tandemframework.shared.employeebase.catalog.entity.codes.PostTypeCodes;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.catalog.codes.YearDistributionPartCodes;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.group.IEppRealGroupRowDAO;
import ru.tandemservice.uniepp.entity.catalog.*;
import ru.tandemservice.uniepp.entity.catalog.codes.EppALoadTypeCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRealEduGroupCompleteLevelCodes;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.pps.gen.IEppPPSCollectionOwnerGen;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModuleALoad;
import ru.tandemservice.uniepp.entity.settings.EppPlanStructure4SubjectIndex;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.student.group.*;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;
import ru.tandemservice.uniload_demo.base.bo.Load.util.PpsLoadDataWrapper;
import ru.tandemservice.uniload_demo.base.bo.Load.util.WorkPlanRowPpsWrapper;
import ru.tandemservice.uniload_demo.base.bo.Load.util.print.ConsolidatedDocumentSheetWrapper;
import ru.tandemservice.uniload_demo.base.bo.Load.util.print.ConsolidatedDocumentWrapper;
import ru.tandemservice.uniload_demo.base.bo.Load.util.print.LoadDocumentPrintSharedUtils.Term;
import ru.tandemservice.uniload_demo.base.bo.Load.util.print.PpsLoadDocumentWrapper;
import ru.tandemservice.uniload_demo.base.bo.Load.vo.LoadSizeValueObject;
import ru.tandemservice.uniload_demo.entity.*;
import ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule;
import ru.tandemservice.uniload_demo.entity.catalog.PpsLoadCathedraPart;
import ru.tandemservice.uniload_demo.entity.catalog.PpsLoadGroup;
import ru.tandemservice.uniload_demo.entity.catalog.PpsLoadType;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.uniload_demo.base.bo.Load.util.print.ConsolidatedLoadDocumentBuilder.HourType;
import static ru.tandemservice.uniload_demo.base.bo.Load.util.print.ConsolidatedLoadDocumentBuilder.SheetType;

/**
 * @author Alexander Zhebko
 * @since 12.12.2013
 */
public class LoadDAO extends CommonDAO implements ILoadDAO
{
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isVisibleOrgUnitLoadTab(Long orgUnitId)
	{
		for (OrgUnitToKindRelation rel : getList(OrgUnitToKindRelation.class, OrgUnitToKindRelation.orgUnit().id(), orgUnitId))
		{
			if (rel.isAllowAddStudent() || rel.isAllowAddGroup())
				return true;
		}

		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EppYearEducationProcess getNextYear()
	{
		EducationYear currentYear;
		EducationYear nextYear;
		EppYearEducationProcess result;

		if ((currentYear = get(EducationYear.class, EducationYear.current(), Boolean.TRUE)) == null ||
				(nextYear = get(EducationYear.class, EducationYear.intValue(), currentYear.getIntValue() + 1)) == null ||
				(result = getByNaturalId(new EppYearEducationProcess.NaturalId(nextYear))) == null)
			return null;

		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EppYearEducationProcess getCurrentYear()
	{
		EducationYear currentYear;
		EppYearEducationProcess result;

		if ((currentYear = get(EducationYear.class, EducationYear.current(), Boolean.TRUE)) == null ||
				(result = getByNaturalId(new EppYearEducationProcess.NaturalId(currentYear))) == null)
			return null;

		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void createNextYearSummaries()
	{
		EppYearEducationProcess nextYear = getNextYear();
		if (nextYear == null)
		{
			throw new ApplicationException("Не задан следующий учебный год.");
		}

		IDQLSelectableQuery summaryIdQuery = new DQLSelectBuilder()
				.fromEntity(EppRealEduGroupSummary.class, "egs")
				.column(property("egs", EppRealEduGroupSummary.id()))
				.where(eq(property("egs", EppRealEduGroupSummary.yearPart().year()), value(nextYear)))
				.buildQuery();

		new DQLDeleteBuilder(EppRealEduGroup4ActionType.class).where(in(property(EppRealEduGroup4ActionType.summary().id()), summaryIdQuery)).createStatement(getSession()).execute();
		new DQLDeleteBuilder(EppRealEduGroup4LoadType.class).where(in(property(EppRealEduGroup4ActionType.summary().id()), summaryIdQuery)).createStatement(getSession()).execute();
		new DQLDeleteBuilder(EppRealEduGroupSummary.class).where(in(property(EppRealEduGroupSummary.id()), summaryIdQuery)).createStatement(getSession()).execute();

		List<YearDistributionPart> distributionParts = new DQLSelectBuilder()
				.fromEntity(EppStudent2WorkPlan.class, "swp")
				.joinEntity("swp", DQLJoinType.inner, EppWorkPlan.class, "wp", eq(property("wp", EppWorkPlan.id()), property("swp", EppStudent2WorkPlan.workPlan().id())))
				.joinPath(DQLJoinType.inner, EppWorkPlan.parent().eduPlanVersion().eduPlan().developGrid().fromAlias("wp"), "d")
				.joinEntity("wp", DQLJoinType.inner, DevelopGridTerm.class, "dgt", and(
						eq(property("dgt", DevelopGridTerm.developGrid().id()), property("d", DevelopGrid.id())),
						eq(property("dgt", DevelopGridTerm.term()), property("wp", EppWorkPlan.term()))))
				.column(property("dgt", DevelopGridTerm.part()))
				.predicate(DQLPredicateType.distinct)
				.where(eq(property("wp", EppWorkPlan.year()), value(nextYear)))
				.createStatement(getSession())
				.list();

		List<OrgUnit> operationOrgUnits = new DQLSelectBuilder()
				.fromEntity(OrgUnit.class, "ou")
				.where(in(property("ou", OrgUnit.id()),
				          new DQLSelectBuilder()
						          .fromEntity(EducationOrgUnit.class, "eou")
						          .column(property("eou", EducationOrgUnit.operationOrgUnit().id()))
						          .buildQuery()))
				.createStatement(getSession()).list();

		for (YearDistributionPart part : distributionParts)
			for (OrgUnit operationOrgUnit : operationOrgUnits)
			{
				EppYearPart yearPart = getByNaturalId(new EppYearPart.NaturalId(nextYear, part));
				EppRealEduGroupSummary summary = new EppRealEduGroupSummary(operationOrgUnit, yearPart);
				summary.setModificationDate(new Date());
				save(summary);
			}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateNextYearEduGroupsCompleteLevel()
	{
		EppRealEduGroupCompleteLevel operationOrgUnitTakeCompleteLevel = getByNaturalId(new EppRealEduGroupCompleteLevel.NaturalId(EppRealEduGroupCompleteLevelCodes.LEVEL_OPERATION_ORG_UNIT_TAKED));
		new DQLUpdateBuilder(EppRealEduGroup.class)
				.set(EppRealEduGroup.level().s(), value(operationOrgUnitTakeCompleteLevel))
				.where(eq(property(EppRealEduGroup.summary().yearPart().year()), value(getNextYear())))
				.createStatement(getSession())
				.execute();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<Long, PpsLoadDataWrapper> getPpsLoadDataWrapperMap(Collection<Long> loadPpsIds)
	{
		Map<Long, PpsLoadDataWrapper> result = new HashMap<>();
		if (loadPpsIds == null)
		{
			throw new IllegalStateException("ppsIds is null");
		}

		for (Long loadPpsId : loadPpsIds)
		{
			Long totalEduLoad = 0L;
			List<EduLoadWrapper> eduLoadWrappers = new ArrayList<>();

			Map<Long, LoadEduGroupPpsDistr> eduGroup2Distribution = new HashMap<>();

			for (LoadEduGroupPpsDistr distr : getList(LoadEduGroupPpsDistr.class, LoadEduGroupPpsDistr.owner().id(), loadPpsId))
			{
				Long eduGroupId = distr.getEduGroup().getId();
				eduGroup2Distribution.put(eduGroupId, distr);
			}

			DQLSelectBuilder loadBuilder = new DQLSelectBuilder()
					.fromEntity(LoadEduGroupPpsDistr.class, "d")
					.column(property("d", LoadEduGroupPpsDistr.eduGroup().id()))
					.where(eq(property("d", LoadEduGroupPpsDistr.owner().id()), value(loadPpsId)));

			DQLSelectBuilder loadTypePartBuilder = new DQLSelectBuilder()
					.fromEntity(EppPpsCollectionItem.class, "ci")
					.joinEntity("ci", DQLJoinType.inner, EppRealEduGroup4LoadType.class, "lt", eq(property("lt", EppRealEduGroup4LoadType.id()), property("ci", EppPpsCollectionItem.list().id())))
					.column(property("lt", EppRealEduGroup4LoadType.activityPart().id()))
                    .predicate(DQLPredicateType.distinct)
					.where(in(property("lt", EppRealEduGroup4LoadType.id()), loadBuilder.buildQuery()));

			Session session = getSession();

			DQLSelectBuilder loadSizeBuilder = new DQLSelectBuilder()
					.fromEntity(EppRegistryElementPartModule.class, "repm")
					.joinEntity("repm", DQLJoinType.inner, EppRegistryModuleALoad.class, "rmal", eq(property("rmal", EppRegistryModuleALoad.module()), property("repm", EppRegistryElementPartModule.module())))
					.column(property("repm", EppRegistryElementPartModule.part()))
					.column(property("rmal", EppRegistryModuleALoad.loadType()))
					.column(property("rmal", EppRegistryModuleALoad.load()))
					.where(in(property("repm", EppRegistryElementPartModule.part().id()), loadTypePartBuilder.buildQuery()));

			//Часть элемента реестра -> [Вид аудиторной нагрузки-> нагрузка]
			Map<EppRegistryElementPart, Map<EppALoadType, Long>> loadSizePartMap = SafeMap.get(HashMap.class);
			for (Object[] row : loadSizeBuilder.createStatement(session).<Object[]>list())
			{
				EppRegistryElementPart part = (EppRegistryElementPart) row[0];
				EppALoadType loadType = (EppALoadType) row[1];
				Long loadSize = (Long) row[2];

				Long oldLoadSize = loadSizePartMap.get(part).get(loadType);

				loadSizePartMap.get(part).put(loadType, loadSize + (oldLoadSize == null ? 0L : oldLoadSize));
			}

			DQLSelectBuilder loadEduGroupsBuilder = new DQLSelectBuilder()
					.fromEntity(EppRealEduGroup4LoadType.class, "lt")
					.where(in(property("lt", EppRealEduGroup4LoadType.id()), loadBuilder.buildQuery()));

			for (EppRealEduGroup4LoadType eduGroup : loadEduGroupsBuilder.createStatement(session).<EppRealEduGroup4LoadType>list())
			{
				LoadEduGroupPpsDistr ppsDistr = eduGroup2Distribution.get(eduGroup.getId());
				Long eduLoad = 0L;
				Set<String> timeRules = new HashSet<>();
				for (LoadTimeRule timeRule : getTimeRules4EduGroup(ppsDistr.getId()))
				{
					long value = getEduLoad4EduGroup(ppsDistr, timeRule);
					eduLoad += value;
					timeRules.add(timeRule.getTitle() + " - " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(UniEppUtils.wrap(value)));
				}

				eduLoadWrappers.add(new EduLoadWrapper(eduGroup, ppsDistr, UniEppUtils.wrap(eduLoad), timeRules));
				totalEduLoad += eduLoad;
			}

			DQLSelectBuilder controlBuilder = new DQLSelectBuilder()
					.fromEntity(EppRealEduGroup4ActionType.class, "g4a")
					.where(in(property("g4a", EppRealEduGroup4ActionType.id()), loadBuilder.buildQuery()));

			for (EppRealEduGroup4ActionType actionType : controlBuilder.createStatement(session).<EppRealEduGroup4ActionType>list())
			{
				LoadEduGroupPpsDistr ppsDistr = eduGroup2Distribution.get(actionType.getId());
				Long eduLoad = 0L;
				Set<String> timeRuleList = new HashSet<>();
				for (LoadTimeRule timeRule : getTimeRules4EduGroup(ppsDistr.getId()))
				{
					long value = getEduLoad4EduGroup(ppsDistr, timeRule);
					eduLoad += value;
					timeRuleList.add(timeRule.getTitle() + " - " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(UniEppUtils.wrap(value)));
				}

				eduLoadWrappers.add(new EduLoadWrapper(actionType, ppsDistr, UniEppUtils.wrap(eduLoad), timeRuleList));
				totalEduLoad += eduLoad;
			}

			Collections.sort(eduLoadWrappers, EduLoadWrapper.COMPARATOR);

			/********************************************************************************************************************************/

			List<OtherLoadWrapper> otherLoadWrappers = new ArrayList<>();
			DQLSelectBuilder otherLoadTRBuilder = new DQLSelectBuilder()
					.fromEntity(LoadImEduGroupTimeRuleRel.class, "rel")
					.column(property("rel"))
					.joinEntity("rel", DQLJoinType.inner, OtherLoad.class, "nel", eq(property("nel", OtherLoad.id()), property("rel", LoadImEduGroupTimeRuleRel.eduGroup().id())))
					.where(eq(property("nel", OtherLoad.owner().id()), value(loadPpsId)))
					.order(property("nel", OtherLoad.id()))
					.order(property("rel", LoadImEduGroupTimeRuleRel.timeRule().title()));

			Map<OtherLoad, List<LoadTimeRule>> nonEduLoadTimeRuleMap = SafeMap.get(ArrayList.class);
			for (LoadImEduGroupTimeRuleRel rel : this.<LoadImEduGroupTimeRuleRel>getList(otherLoadTRBuilder))
			{
				OtherLoad otherLoad = (OtherLoad) rel.getEduGroup();
				nonEduLoadTimeRuleMap.get(otherLoad).add(rel.getTimeRule());
			}

			List<OtherLoad> nonEduLoadList = new DQLSelectBuilder()
					.fromEntity(OtherLoad.class, "nel")
					.where(eq(property("nel", OtherLoad.owner().id()), value(loadPpsId)))
					.createStatement(session).list();

			Long totalOtherNonEduLoad = 0L;
			Long totalOtherEduLoad = 0L;
			for (OtherLoad otherLoad : nonEduLoadList)
			{
				Set<String> timeRules = new HashSet<>();

				Double hoursAsDouble = otherLoad.getHoursAsDouble();
				Long otherLoadSize = 0L;

				for (LoadTimeRule timeRule : nonEduLoadTimeRuleMap.get(otherLoad))
				{
					Long value = getEduLoad4EduGroup(otherLoad, timeRule);
					otherLoadSize += value;
					timeRules.add(timeRule.getTitle() + " - " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(UniEppUtils.wrap(value)));
				}

				otherLoadWrappers.add(new OtherLoadWrapper(otherLoad, hoursAsDouble, UniEppUtils.wrap(otherLoadSize), timeRules));

				if (otherLoad.isConsiderInEduLoad())
					totalOtherEduLoad += otherLoadSize;
				else
					totalOtherNonEduLoad += otherLoadSize;
			}

			/********************************************************************************************************************************/

			Long totalFirstCourseEduLoad = 0L;
			List<LoadFirstCourseImEduGroup> firstCourseImEduGroups = new DQLSelectBuilder()
					.fromEntity(LoadFirstCourseImEduGroup.class, "fc")
					.column(property("fc"))
					.where(eq(property("fc", LoadFirstCourseImEduGroup.owner().id()), value(loadPpsId)))
					.createStatement(session).list();

			List<FirstCourseImEduGroupViewWrapper> firstCourseImEduGroupViewWrappers = new ArrayList<>();
			for (LoadFirstCourseImEduGroup firstCourseImEduGroup : firstCourseImEduGroups)
			{
				Long eduLoad = 0L;
				Set<String> timeRules = new HashSet<>();
				for (LoadTimeRule timeRule : getTimeRules4EduGroup(firstCourseImEduGroup.getId()))
				{
					Long value = getEduLoad4EduGroup(firstCourseImEduGroup, timeRule);
					eduLoad += value;
					timeRules.add(timeRule.getTitle() + " - " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(UniEppUtils.wrap(value)));
				}

				firstCourseImEduGroupViewWrappers.add(new FirstCourseImEduGroupViewWrapper(firstCourseImEduGroup, UniEppUtils.wrap(eduLoad), timeRules));
				totalFirstCourseEduLoad += eduLoad;
			}

			LoadRecordOwner recordOwner = getNotNull(LoadRecordOwner.class, loadPpsId);
			result.put(loadPpsId, new PpsLoadDataWrapper(recordOwner.getPost(), eduLoadWrappers, otherLoadWrappers, firstCourseImEduGroupViewWrappers, totalEduLoad, totalFirstCourseEduLoad, totalOtherNonEduLoad, totalOtherEduLoad));
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PpsLoadDataWrapper getPpsLoadDataWrapper(Long loadPpsId)
	{
		return getPpsLoadDataWrapperMap(Collections.singletonList(loadPpsId)).get(loadPpsId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteEduGroupDistr(Long ppsId, Long eduGroupId)
	{
		new DQLDeleteBuilder(EppPpsCollectionItem.class)
				.where(eq(property(EppPpsCollectionItem.list().id()), value(eduGroupId)))
				.where(eq(property(EppPpsCollectionItem.pps().id()),
				          new DQLSelectBuilder()
						          .fromEntity(LoadRecordOwner.class, "i")
						          .column(property("i", LoadRecordOwner.realPps().pps().id()))
						          .where(eq(property("i", LoadRecordOwner.id()), value(ppsId))).buildQuery()))
				.createStatement(getSession()).execute();
		new DQLDeleteBuilder(LoadEduGroupPpsDistr.class)
				.where(eq(property(LoadEduGroupPpsDistr.owner().id()), value(ppsId)))
				.where(eq(property(LoadEduGroupPpsDistr.eduGroup().id()), value(eduGroupId)))
				.createStatement(getSession()).execute();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void saveImEduGroupTimeRules(Long eduGroupId, Collection<LoadTimeRule> timeRules)
	{
		ILoadImEduGroup eduGroup = get(eduGroupId);
		Session session = getSession();
		new DQLDeleteBuilder(LoadImEduGroupTimeRuleRel.class)
				.where(eq(property(LoadImEduGroupTimeRuleRel.eduGroup().id()), value(eduGroupId)))
				.createStatement(session).execute();

		for (LoadTimeRule timeRule : timeRules)
			session.save(new LoadImEduGroupTimeRuleRel(eduGroup, timeRule));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long getFirstCourseEduGroupMaxHoursUnwrapped(Long workPlanRowId, EppALoadType loadType)
	{
		EppWorkPlanRegistryElementRow row = get(EppWorkPlanRegistryElementRow.class, workPlanRowId);
		if (row.getRegistryElementPart() == null)
			return 0L;
		return getFirstCourseEduGroupMaxHoursUnwrapped(row, loadType);
	}

	private Long getFirstCourseEduGroupMaxHoursUnwrapped(EppWorkPlanRegistryElementRow row, EppALoadType loadType)
	{
		Long maxHours = new DQLSelectBuilder()
				.fromEntity(EppRegistryModuleALoad.class, "ml")
				.column(DQLFunctions.sum(property("ml", EppRegistryModuleALoad.load())))
				.joinEntity("ml", DQLJoinType.inner, EppRegistryElementPartModule.class, "epm", eq(property("epm", EppRegistryElementPartModule.module()), property("ml", EppRegistryModuleALoad.module())))
				.where(eq(property("epm", EppRegistryElementPartModule.part()), value(row.getRegistryElementPart())))
				.where(eq(property("ml", EppRegistryModuleALoad.loadType()), value(loadType)))
				.createStatement(getSession()).uniqueResult();
		if (maxHours == null)
			return 0L;
		return maxHours;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long getFirstCourseEduGroupUsedHoursUnwrapped(Long workPlanRowId, Long eduGroupId)
	{
		Long hoursUsed = new DQLSelectBuilder()
				.fromEntity(LoadFirstCourseImEduGroup.class, "fc")
				.column(DQLFunctions.sum(property("fc", LoadFirstCourseImEduGroup.hours())))
				.joinPath(DQLJoinType.inner, LoadFirstCourseImEduGroup.row().workPlan().fromAlias("fc"), "wpb")
				.joinEntity("fc", DQLJoinType.inner, EppWorkPlan.class, "wp", eq(property("wp", EppWorkPlan.id()), property("wpb", EppWorkPlanBase.id())))
				.where(eq(property("wp", EppWorkPlan.year()), value(getNextYear())))
				.where(eq(property("fc", LoadFirstCourseImEduGroup.row().id()), value(workPlanRowId)))
				.where(eduGroupId == null ? null : ne(property("fc", LoadFirstCourseImEduGroup.id()), value(eduGroupId)))
				.createStatement(getSession()).uniqueResult();
		if (hoursUsed == null)
			return 0L;
		return hoursUsed;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long getFirstCourseEduGroupLeftHoursUnwrapped(Long workPlanRowId, EppALoadType loadType, Long eduGroupId)
	{
		EppWorkPlanRegistryElementRow row = get(EppWorkPlanRegistryElementRow.class, workPlanRowId);
		if (row.getRegistryElementPart() == null)
			return 0L;

		Long totalHours = getFirstCourseEduGroupMaxHoursUnwrapped(row, loadType);
		if (totalHours == 0L)
			return 0L;

		Long hoursInUse = getFirstCourseEduGroupUsedHoursUnwrapped(workPlanRowId, eduGroupId);
		return totalHours - hoursInUse;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void doBindNextYearEduGroup2WorkPlanRow()
	{
		IDQLSelectableQuery eduGroupIdsQuery = new DQLSelectBuilder()
				.fromEntity(EppRealEduGroup.class, "reg")
				.column(property("reg", EppRealEduGroup.id()))
				.where(eq(property("reg", EppRealEduGroup.summary().yearPart().year()), value(getNextYear())))
				.buildQuery();

		DQLSelectBuilder group2sourceBuilder = new DQLSelectBuilder()
				.fromEntity(EppRealEduGroupRow.class, "row")
				.column(property("row", EppRealEduGroupRow.group().id()))
				.column(property("row", EppRealEduGroupRow.studentWpePart().studentWpe().sourceRow().id()))
				.where(in(property("row", EppRealEduGroupRow.group()), eduGroupIdsQuery));

		Map<Long, Long> group2sourceMap = new HashMap<>();
		for (Object[] row : this.<Object[]>getList(group2sourceBuilder))
		{
			group2sourceMap.put((Long) row[0], (Long) row[1]);
		}

		Map<Long, EppWorkPlanRegistryElementRow> workPlanRowMap = new HashMap<>();
		for (EppWorkPlanRegistryElementRow row : this.<EppWorkPlanRegistryElementRow>getList(new DQLSelectBuilder().fromEntity(EppWorkPlanRegistryElementRow.class, "r").where(in(property("r", EppWorkPlanRegistryElementRow.id()), group2sourceMap.values()))))
		{
			workPlanRowMap.put(row.getId(), row);
		}

		List<EppRealEduGroup> groupList = getList(new DQLSelectBuilder().fromEntity(EppRealEduGroup.class, "g").where(in(property("g", EppRealEduGroup.id()), eduGroupIdsQuery)));
		for (EppRealEduGroup group : groupList)
		{
			Long id = group.getId();
			EppWorkPlanRegistryElementRow workPlanRow = workPlanRowMap.get(group2sourceMap.get(id));
			if (group instanceof EppRealEduGroup4ActionType)
			{
				EppRealEduGroup4ActionType group4ActionType = (EppRealEduGroup4ActionType) group;
				group4ActionType.setWorkPlanRow(workPlanRow);

			}
			else
			{
				EppRealEduGroup4LoadType group4LoadType = (EppRealEduGroup4LoadType) group;
				group4LoadType.setWorkPlanRow(workPlanRow);
			}

			update(group);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, Long> getWorkPlanRowLoad(Long workPlanRowId)
	{
		EppRegistryElementPart part = get(EppWorkPlanRegistryElementRow.class, workPlanRowId).getRegistryElementPart();
		if (part == null)
			return null;

		DQLSelectBuilder builder = new DQLSelectBuilder()
				.fromEntity(EppRegistryModuleALoad.class, "ml")
				.joinEntity("ml", DQLJoinType.inner, EppRegistryElementPartModule.class, "repm", eq(property("repm", EppRegistryElementPartModule.module()), property("ml", EppRegistryModuleALoad.module())))

				.column(property("ml", EppRegistryModuleALoad.loadType().code()))
				.column(property("ml", EppRegistryModuleALoad.load()))

				.where(eq(property("repm", EppRegistryElementPartModule.part()), value(part)));

		Map<String, Long> result = new HashMap<>();
		for (Object[] row : this.<Object[]>getList(builder))
		{
			String loadTypeCode = (String) row[0];
			Long oldValue = result.get(loadTypeCode);
			result.put(loadTypeCode, (Long) row[1] + (oldValue == null ? 0L : oldValue));
		}

		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<WorkPlanRowPpsWrapper> getWorkPlanRowPpsWrappers(Long workPlanRowId)
	{
		DQLSelectBuilder ppsDistrBuilder = new DQLSelectBuilder()
				.fromEntity(LoadEduGroupPpsDistr.class, "d")
				.joinPath(DQLJoinType.inner, LoadEduGroupPpsDistr.eduGroup().fromAlias("d"), "co")
				.joinEntity("co", DQLJoinType.inner, EppRealEduGroup4LoadType.class, "l", eq(property("l", EppRealEduGroup4LoadType.id()), property("co", IEppPPSCollectionOwnerGen.id())))
                .joinEntity("l", DQLJoinType.inner, EppALoadType.class, "lt", eq(property("l", EppRealEduGroup4LoadType.type()), property("lt", EppALoadType.eppGroupType())))

				.column(property("d", LoadEduGroupPpsDistr.owner().id()))     //0
				.column(property("lt", EppALoadType.code()))                  //1
				.column(property("d", LoadEduGroupPpsDistr.hours()))          //2

				.where(eq(property("l", EppRealEduGroup4LoadType.workPlanRow().id()), value(workPlanRowId)));

		Map<Long, Map<String, Long>> ppsLoadMap = SafeMap.get(HashMap.class);
		for (Object[] row : this.<Object[]>getList(ppsDistrBuilder))
		{
			Long ownerId = (Long) row[0];
			ppsLoadMap.get(ownerId).put((String) row[1], (Long) row[2]);
		}

		DQLSelectBuilder firstCourseBuilder = new DQLSelectBuilder()
				.fromEntity(LoadFirstCourseImEduGroup.class, "fc")
				.joinEntity("fc", DQLJoinType.inner, EppALoadType.class, "l", eq(property("l", EppALoadType.eppGroupType()), property("fc", LoadFirstCourseImEduGroup.type())))

				.column(property("fc", LoadFirstCourseImEduGroup.owner().id()))
				.column(property("l", EppALoadType.code()))
				.column(property("fc", LoadFirstCourseImEduGroup.hours()))

				.where(eq(property("fc", LoadFirstCourseImEduGroup.row().id()), value(workPlanRowId)))
				.where(ppsLoadMap.isEmpty() ? null : notIn(property("fc", LoadFirstCourseImEduGroup.owner().id()), ppsLoadMap.keySet()));

		Map<Long, Map<String, Long>> firstCourseLoadMap = SafeMap.get(HashMap.class);
		for (Object[] row : this.<Object[]>getList(firstCourseBuilder))
		{
			Long ownerId = (Long) row[0];
			firstCourseLoadMap.get(ownerId).put((String) row[1], (Long) row[2]);
		}

		Set<Long> ppsIds = new HashSet<>();
		ppsIds.addAll(ppsLoadMap.keySet());
		ppsIds.addAll(firstCourseLoadMap.keySet());

		List<WorkPlanRowPpsWrapper> wrappers = new ArrayList<>();
		for (LoadRecordOwner pps : getList(LoadRecordOwner.class, ppsIds))
		{
			Long id = pps.getId();
			Map<String, Long> loadMap = ppsLoadMap.containsKey(id) ? ppsLoadMap.get(id) : firstCourseLoadMap.get(id);
			wrappers.add(new WorkPlanRowPpsWrapper(pps, loadMap));
		}

		Collections.sort(wrappers, WorkPlanRowPpsWrapper.COMPARATOR);
		return wrappers;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updatePpsLoad(Long workPlanRowId, List<WorkPlanRowPpsWrapper> wrappers)
	{
		//Распределение часов УГС
		DQLSelectBuilder ppsDistrBuilder = new DQLSelectBuilder()
				.fromEntity(LoadEduGroupPpsDistr.class, "d")
				.joinPath(DQLJoinType.inner, LoadEduGroupPpsDistr.eduGroup().fromAlias("d"), "co")
				.joinEntity("co", DQLJoinType.inner, EppRealEduGroup4LoadType.class, "l", eq(property("l", EppRealEduGroup4LoadType.id()), property("co", IEppPPSCollectionOwnerGen.id())))
                .joinEntity("l", DQLJoinType.inner, EppALoadType.class, "lt", eq(property("l", EppRealEduGroup4LoadType.type()), property("lt", EppALoadType.eppGroupType())))

				.column(property("d", LoadEduGroupPpsDistr.owner().id()))
				.column(property("lt", EppALoadType.code()))
				.column(property("d", LoadEduGroupPpsDistr.hours()))

				.where(eq(property("l", EppRealEduGroup4LoadType.workPlanRow().id()), value(workPlanRowId)));

		Map<Long, Map<String, Long>> ppsLoadMap = SafeMap.get(HashMap.class);
		for (Object[] row : this.<Object[]>getList(ppsDistrBuilder))
		{
			Long ownerId = (Long) row[0];
			ppsLoadMap.get(ownerId).put((String) row[1], (Long) row[2]);
		}

		//Планируемая УГС первого курса (демо)
		DQLSelectBuilder firstCourseBuilder = new DQLSelectBuilder()
				.fromEntity(LoadFirstCourseImEduGroup.class, "fc")
				.joinEntity("fc", DQLJoinType.inner, EppALoadType.class, "l", eq(property("l", EppALoadType.eppGroupType()), property("fc", LoadFirstCourseImEduGroup.type())))

				.column(property("fc", LoadFirstCourseImEduGroup.owner().id()))
				.column(property("l", EppALoadType.code()))
				.column(property("fc", LoadFirstCourseImEduGroup.hours()))

				.where(eq(property("fc", LoadFirstCourseImEduGroup.row().id()), value(workPlanRowId)))
				.where(notIn(property("fc", LoadFirstCourseImEduGroup.owner().id()), ppsLoadMap.keySet()));

		Map<Long, Map<String, Long>> firstCourseLoadMap = SafeMap.get(HashMap.class);
		for (Object[] row : this.<Object[]>getList(firstCourseBuilder))
		{
			Long ownerId = (Long) row[0];
			firstCourseLoadMap.get(ownerId).put((String) row[1], (Long) row[2]);
		}

		/******/  /******/  /******/  /******/  /******/  /******/
		/******/  /******/  /******/  /******/  /******/  /******/

		DQLSelectBuilder distrBuilder = new DQLSelectBuilder()
				.fromEntity(LoadEduGroupPpsDistr.class, "d")
				.column(property("d"))
				.joinPath(DQLJoinType.inner, LoadEduGroupPpsDistr.eduGroup().fromAlias("d"), "co")
				.joinEntity("co", DQLJoinType.inner, EppRealEduGroup4LoadType.class, "l", eq(property("l", EppRealEduGroup4LoadType.id()), property("co", IEppPPSCollectionOwnerGen.id())))
				.where(eq(property("l", EppRealEduGroup4LoadType.workPlanRow().id()), value(workPlanRowId)));

		Map<Long, Map<String, LoadEduGroupPpsDistr>> distrMap = SafeMap.get(HashMap.class);
		for (LoadEduGroupPpsDistr distr : this.<LoadEduGroupPpsDistr>getList(distrBuilder))
		{
			distrMap.get(distr.getOwner().getId()).put(((EppRealEduGroup4LoadType) distr.getEduGroup()).getLoadType().getCode(), distr);
		}

		DQLSelectBuilder fcBuilder = new DQLSelectBuilder()
				.fromEntity(LoadFirstCourseImEduGroup.class, "fc")
				.column(property("fc"))
                .column(property("l", EppALoadType.code()))
				.joinEntity("fc", DQLJoinType.inner, EppALoadType.class, "l", eq(property("l", EppALoadType.eppGroupType()), property("fc", LoadFirstCourseImEduGroup.type())))
				.where(eq(property("fc", LoadFirstCourseImEduGroup.row().id()), value(workPlanRowId)));

		Map<Long, Map<String, LoadFirstCourseImEduGroup>> fcMap = SafeMap.get(HashMap.class);
		for (Object[] item : this.<Object[]>getList(fcBuilder))
		{
            LoadFirstCourseImEduGroup firstCourseImEduGroup = (LoadFirstCourseImEduGroup)item[0];
            String loadTypeCode = (String) item[1];
			fcMap.get(firstCourseImEduGroup.getOwner().getId()).put(loadTypeCode, firstCourseImEduGroup);
		}

		/******/  /******/  /******/  /******/  /******/  /******/
		/******/  /******/  /******/  /******/  /******/  /******/

		for (WorkPlanRowPpsWrapper wrapper : wrappers)
		{
			Long ppsId = wrapper.getId();
			boolean senior = ppsLoadMap.containsKey(ppsId);
			Map<String, Long> oldLoadMap = senior ? ppsLoadMap.get(ppsId) : firstCourseLoadMap.get(ppsId);

			for (final String code : EppALoadTypeCodes.CODES)
			{
				Long oldValue = oldLoadMap.get(code);
				final Long newValue = wrapper.getLoad(code);

				if (oldValue == null && newValue == null)
					continue;

				if (oldValue == null)
				{
					LoadRecordOwner owner = getNotNull(ppsId);
					savePpsWorkPlanRowLoad(workPlanRowId, ppsId, new HashMap<String, Long>()
					{{
							put(code, newValue);
						}}, owner.getYear().getId());
					continue;
				}

				if (newValue == null)
				{
					if (senior)
						delete(distrMap.get(ppsId).get(code));
					else
						delete(fcMap.get(ppsId).get(code));
					continue;
				}

				if (!oldValue.equals(newValue))
				{
					if (senior)
					{
						LoadEduGroupPpsDistr distr = distrMap.get(ppsId).get(code);
						distr.setHours(newValue);
						update(distr);

					}
					else
					{
						LoadFirstCourseImEduGroup eduGroup = fcMap.get(ppsId).get(code);
						eduGroup.setHours(newValue);
						update(eduGroup);
					}
				}
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deletePpsRecord(Long workPlanRowId, Long loadPpsId)
	{
		DQLSelectBuilder distrBuilder = new DQLSelectBuilder()
				.fromEntity(LoadEduGroupPpsDistr.class, "d")
				.column(property("d"))
				.joinPath(DQLJoinType.inner, LoadEduGroupPpsDistr.eduGroup().fromAlias("d"), "co")
				.joinEntity("co", DQLJoinType.inner, EppRealEduGroup4LoadType.class, "l", eq(property("l", EppRealEduGroup4LoadType.id()), property("co", IEppPPSCollectionOwnerGen.id())))
				.where(eq(property("l", EppRealEduGroup4LoadType.workPlanRow().id()), value(workPlanRowId)))
				.where(eq(property("d", LoadEduGroupPpsDistr.owner().id()), value(loadPpsId)));

		List<LoadEduGroupPpsDistr> distrList = getList(distrBuilder);
		if (!distrList.isEmpty())
		{
			//
			LoadRecordOwner recordOwner = get(loadPpsId);
			for (LoadEduGroupPpsDistr distr : distrList)
			{
				if (recordOwner.isReal())
				{
					EppRealEduGroup eduGroup = distr.getEduGroup();
					new DQLDeleteBuilder(EppPpsCollectionItem.class)
							.where(eq(property(EppPpsCollectionItem.L_LIST), value(eduGroup.getId())))
							.where(eq(property(EppPpsCollectionItem.L_PPS), value(recordOwner.getRealPps().getPps())))
							.createStatement(getSession()).execute();
				}
				delete(distr);
			}
		}
		else
		{
			DQLSelectBuilder fcBuilder = new DQLSelectBuilder()
					.fromEntity(LoadFirstCourseImEduGroup.class, "fc")
					.column(property("fc"))
					.joinEntity("fc", DQLJoinType.inner, EppALoadType.class, "l", eq(property("l", EppALoadType.eppGroupType()), property("fc", LoadFirstCourseImEduGroup.type())))
					.where(eq(property("fc", LoadFirstCourseImEduGroup.row().id()), value(workPlanRowId)))
					.where(eq(property("fc", LoadFirstCourseImEduGroup.owner().id()), value(loadPpsId)));

			for (LoadFirstCourseImEduGroup eduGroup : this.<LoadFirstCourseImEduGroup>getList(fcBuilder))
			{
				delete(eduGroup);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void savePpsWorkPlanRowLoad(Long workPlanRowId, Long loadPpsId, Map<String, Long> loadMap, Long yearId)
	{
		Session session = getSession();
		LoadRecordOwner pps = get(loadPpsId);
		EppWorkPlanRegistryElementRow row = get(workPlanRowId);

		// Получаем номер курса это просто !@#$
		IDQLSelectableQuery workPlanIdQuery = new DQLSelectBuilder()
				.fromEntity(EppWorkPlanRegistryElementRow.class, "rer")
				.joinPath(DQLJoinType.inner, EppWorkPlanRegistryElementRow.workPlan().fromAlias("rer"), "wpb")
				.joinEntity("wpb", DQLJoinType.left, EppWorkPlan.class, "wp", eq(property("wp", EppWorkPlan.id()), property("wpb", EppWorkPlanBase.id())))
				.joinEntity("wpb", DQLJoinType.left, EppWorkPlanVersion.class, "wpv", eq(property("wpv", EppWorkPlanVersion.id()), property("wpb", EppWorkPlanBase.id())))
				.joinPath(DQLJoinType.left, EppWorkPlanVersion.parent().fromAlias("wpv"), "wpvp")
				.column(DQLFunctions.coalesce(property("wp", EppWorkPlan.id()), property("wpvp", EppWorkPlan.id())))
				.where(eq(property("rer", EppWorkPlanRegistryElementRow.id()), value(workPlanRowId)))
				.buildQuery();

		Integer courseNumber = new DQLSelectBuilder()
				.fromEntity(EppWorkPlan.class, "wp")
				.joinPath(DQLJoinType.inner, EppWorkPlan.parent().eduPlanVersion().eduPlan().developGrid().fromAlias("wp"), "dg")
				.joinEntity("wp", DQLJoinType.inner, DevelopGridTerm.class, "dgt", and(
						eq(property("wp", EppWorkPlan.term()), property("dgt", DevelopGridTerm.term())),
						eq(property("dg"), property("dgt", DevelopGridTerm.developGrid()))))
				.column(property("dgt", DevelopGridTerm.course().intValue()))
				.where(eq(property("wp", EppWorkPlan.id()), workPlanIdQuery))
				.createStatement(session)
				.uniqueResult();

		boolean firstCourse = courseNumber == 1;

		Map<String, EppALoadType> loadTypeMap = SafeMap.get(key -> getByCode(EppALoadType.class, key));

		for (Map.Entry<String, Long> loadEntry : loadMap.entrySet())
		{
			String loadTypeCode = loadEntry.getKey();
			Long loadSize = loadEntry.getValue();
			if (loadSize != null && loadSize > 0L)
			{
				//для нагрузки первого курса
				if (firstCourse)
				{
					LoadFirstCourseImEduGroup eduGroup = new DQLSelectBuilder()
							.fromEntity(LoadFirstCourseImEduGroup.class, "fc")
							.column(property("fc"))
							.where(eq(property("fc", LoadFirstCourseImEduGroup.owner().id()), value(loadPpsId)))
							.where(eq(property("fc", LoadFirstCourseImEduGroup.row().id()), value(workPlanRowId)))
							.joinEntity("fc", DQLJoinType.inner, EppALoadType.class, "lt", eq(property("lt", EppALoadType.eppGroupType()), property("fc", LoadFirstCourseImEduGroup.type())))
							.where(eq(property("lt", EppALoadType.code()), value(loadTypeCode)))
							.createStatement(session)
							.uniqueResult();

					if (eduGroup != null)
						delete(eduGroup);

					eduGroup = new LoadFirstCourseImEduGroup();
					eduGroup.setHours(loadSize);
					eduGroup.setOwner(pps);
					eduGroup.setRow(row);
					eduGroup.setActivityPart(row.getRegistryElementPart());
					eduGroup.setTitle(row.getTitle());
					EppALoadType eppALoadType = loadTypeMap.get(loadTypeCode);
					eduGroup.setType(eppALoadType.getEppGroupType());

					Collection<LoadTimeRule> timeRuleList = getTimeRule4EduGroupType(eppALoadType.getEppGroupType(), yearId);
					save(eduGroup);
					saveImEduGroupTimeRules(eduGroup.getId(), timeRuleList);
				}
				//для нагрузки НЕ первого курса
				else
				{
					IDQLSelectableQuery loadTypeGroupQuery = new DQLSelectBuilder()
							.fromEntity(EppRealEduGroup4LoadType.class, "lt")
                            .joinEntity("lt", DQLJoinType.inner, EppALoadType.class, "alt", eq(property("lt", EppRealEduGroup4LoadType.type()), property("alt", EppALoadType.eppGroupType())))
							.column(property("lt", EppRealEduGroup4LoadType.id()))
							.where(eq(property("alt", EppALoadType.code()), value(loadTypeCode)))
							.where(eq(property("lt", EppRealEduGroup4LoadType.workPlanRow().id()), value(workPlanRowId)))
							.buildQuery();
					//TODO perhaps will not be unique
					LoadEduGroupPpsDistr eduFroup = null;
					List<LoadEduGroupPpsDistr> eduFroupList = new DQLSelectBuilder()
							.fromEntity(LoadEduGroupPpsDistr.class, "distr")
							.column(property("distr"))
							.where(eq(property("distr", LoadEduGroupPpsDistr.owner().id()), value(loadPpsId)))
							.where(in(property("distr", LoadEduGroupPpsDistr.eduGroup().id()), loadTypeGroupQuery))
							.createStatement(session)
							.list();
					EppRealEduGroup4LoadType regLoad = null;
					List<EppRealEduGroup4LoadType> regLoadList = new DQLSelectBuilder()
							.fromEntity(EppRealEduGroup4LoadType.class, "reglt")
							.where(in(property("reglt", EppRealEduGroup4LoadType.id()), loadTypeGroupQuery))
							.createStatement(session).list();

					if (regLoadList.isEmpty() || (regLoadList.size() > 1 && eduFroupList.size() > 1))
						/*увы*/ continue;

					if (eduFroupList.size() == 1)
					{
						eduFroup = eduFroupList.get(0);
						for (EppRealEduGroup4LoadType load : regLoadList)
						{
							if (eduFroup.getEduGroup().equals(load))
								regLoad = load;
						}
					}
					if (regLoad == null) regLoad = regLoadList.get(0);

					if (pps.isReal())
					{
						PpsEntry ppsEntry = pps.getRealPps().getPps();
						EppPpsCollectionItem ppsCollectionItem = getByNaturalId(new EppPpsCollectionItem.NaturalId(regLoad, ppsEntry));
						if (ppsCollectionItem == null)
							save(new EppPpsCollectionItem(regLoad, ppsEntry));
					}

					if (eduFroup == null)
						eduFroup = new LoadEduGroupPpsDistr();//delete(eduFroup);
					eduFroup.setOwner(pps);
					eduFroup.setEduGroup(regLoad);
					eduFroup.setHours(loadSize);
					eduFroup.setEduGroupTitle(IEppRealGroupRowDAO.instance.get().calculateTitle(regLoad));
					int students = getCount(EppRealEduGroupRow.class, EppRealEduGroupRow.group().id().s(), regLoad);
					eduFroup.setStudents(students);

					Collection<LoadTimeRule> timeRuleList = getTimeRule4EduGroupType(regLoad.getType(), yearId);
					saveOrUpdate(eduFroup);
					saveImEduGroupTimeRules(eduFroup.getId(), timeRuleList);
				}
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isOrgUnitWorkPlanRowEditDisabled(Long orgUnitId)
	{
		OrgUnit orgUnit = get(orgUnitId);
		EppYearEducationProcess nextYear = getNextYear();
		return nextYear == null || isOrgUnitLoadEditDisabled(orgUnit, nextYear);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isPpsLoadRecordAddEditDisabled(Long ppsId)
	{
		LoadRecordOwner pps;
		EppYearEducationProcess nextYear;
		return (pps = get(LoadRecordOwner.class, ppsId)) == null || (nextYear = getNextYear()) == null || isOrgUnitLoadEditDisabled(pps.getOrgUnit(), nextYear);
	}

	private boolean isOrgUnitLoadEditDisabled(OrgUnit orgUnit, EppYearEducationProcess nextYear)
	{
		OrgUnitLoadState orgUnitLoadState = getByNaturalId(new OrgUnitLoadState.NaturalId(orgUnit, nextYear));
		return orgUnitLoadState != null && orgUnitLoadState.getLoadState() != LoadState.FORMATIVE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<Long> getLoadPpsIdList(Long orgUnitId, Long pupnagId)
	{
		return getAllPpsListFromOrgUnit(orgUnitId, pupnagId).createStatement(getSession()).idList();
	}

	private DQLSelectBuilder getAllLoadPpsListByYear(@NotNull Long pupnagId)
	{
		return new DQLSelectBuilder()
				.column("lro")
				.fromEntity(LoadRecordOwner.class, "lro")
				.joinEntity("lro", DQLJoinType.left, LoadRealPpsEntry.class, "loa", eq(property("lro", LoadRecordOwner.realPps().id()), property("loa", LoadRealPpsEntry.id())))
				.joinEntity("loa", DQLJoinType.left, PpsEntry.class, "pe", eq(property("loa", LoadRealPpsEntry.pps().id()), property("pe", PpsEntry.id())))
				.joinEntity("lro", DQLJoinType.left, LoadFakePpsEntry.class, "fpe", eq(property("lro", LoadRecordOwner.fakePps().id()), property("fpe", LoadFakePpsEntry.id())))
				.where(eq(property("lro", LoadRecordOwner.year().id()), value(pupnagId)));
	}

	private DQLSelectBuilder getAllPpsListFromOrgUnit(@NotNull Long orgUnitId, @NotNull Long pupnagId)
	{
		return getAllLoadPpsListByYear(pupnagId)
				.where(or(eq(property("pe", PpsEntry.L_ORG_UNIT), value(orgUnitId)), eq(property("fpe", LoadFakePpsEntry.L_ORG_UNIT), value(orgUnitId))));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LoadRecordOwner getLoadRecordOwner(Long ppsId, Long pupnagId)
	{
		return getAllLoadPpsListByYear(pupnagId)
				.where(or(eq(property("pe", PpsEntry.P_ID), value(ppsId)), eq(property("fpe", LoadFakePpsEntry.P_ID), value(ppsId))))
				.createStatement(getSession()).uniqueResult();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void saveOrUpdateLoadRecordOwner(LoadRecordOwner pps)
	{
		if (!pps.isReal())
			saveOrUpdate(pps.getFakePps());
		else
			saveOrUpdate(pps.getRealPps());
		saveOrUpdate(pps);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteLoadRecordOwner(Long recordOwnerId)
	{
		LoadRecordOwner recordOwner = getNotNull(recordOwnerId);
		Session session = getSession();

		if (recordOwner.isFake())
			session.delete(recordOwner.getFakePps());
		else
			session.delete(recordOwner.getRealPps());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void saveOrUpdateLoadMaxSize(Long pupnagId, Map<PostBoundedWithQGandQL, LoadSizeValueObject> dataMap)
	{
		new DQLDeleteBuilder(LoadSize.class)
				.where(eq(property(LoadSize.L_YEAR), value(pupnagId)))
				.createStatement(getSession()).execute();

		if (dataMap != null && !dataMap.isEmpty())
		{
			DQLInsertValuesBuilder insertBuilder = new DQLInsertValuesBuilder(LoadSize.class);
			for (Map.Entry<PostBoundedWithQGandQL, LoadSizeValueObject> item : dataMap.entrySet())
			{
				LoadSizeValueObject load = item.getValue();
				if (load != null && (load.getMinSize() != null || load.getMaxSize() != null || load.getTotalSize() != null))
					insertBuilder = insertBuilder
							.value(LoadSize.L_POST, item.getKey().getId())
							.value(LoadSize.L_YEAR, pupnagId)
							.value(LoadSize.P_MIN_SIZE, load.getMinSize())
							.value(LoadSize.P_MAX_SIZE, load.getMaxSize())
							.value(LoadSize.P_TOTAL_SIZE, load.getTotalSize())
							.addBatch();
			}
			insertBuilder.createStatement(getSession()).execute();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void doResetCurrentYearEduGroup(Long pupnagId)
	{
		new DQLUpdateBuilder(EppRealEduGroup.class)
				.set(EppRealEduGroup.level().s(), nul())
				.where(eq(property(EppRealEduGroup.summary().yearPart().year()), value(pupnagId)))
				.where(ne(property(EppRealEduGroup.L_LEVEL), nul()))
				.createStatement(getSession()).execute();

		IDQLSelectableQuery query = new DQLSelectBuilder().fromEntity(EppRealEduGroupRow.class, "r")
				.column(property("r.id"), "id")
				.column(property("g", Group.P_TITLE), "title")
				.joinPath(DQLJoinType.inner, EppRealEduGroupRow.group().fromAlias("r"), "eg")
				.joinPath(DQLJoinType.inner, EppRealEduGroupRow.studentWpePart().studentWpe().student().group().fromAlias("r"), "g")
				.where(eq(property("eg", EppRealEduGroup.summary().yearPart().year().id()), value(pupnagId)))
				.where(ne(property("r", EppRealEduGroupRow.P_STUDENT_GROUP_TITLE), property("g", Group.P_TITLE))).buildQuery();

		new DQLUpdateBuilder(EppRealEduGroupRow.class)
				.fromDataSource(query, "z")
				.set(EppRealEduGroupRow.P_STUDENT_GROUP_TITLE, "z.title")
				.where(eq(property(EppRealEduGroupRow.P_ID), property("z.id")))
				.createStatement(getSession()).execute();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Multimap<EppGroupType, LoadTimeRule> getTimeRule2EduGroupTypeMultimap(@NotNull Long pupnagId)
	{
		List<TimeRule2EduGroup> rule2EduGroupList = new DQLSelectBuilder().fromEntity(TimeRule2EduGroup.class, "tte")
				.column("tte")
				.where(eq(property("tte", TimeRule2EduGroup.L_YEAR), value(pupnagId)))
				.createStatement(getSession()).list();
		Multimap<EppGroupType, LoadTimeRule> result = HashMultimap.create();
		for (TimeRule2EduGroup item : rule2EduGroupList)
		{
			result.put(item.getGroupType(), item.getTimeRule());
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateTimeRule2EduGroupTypeRel(@NotNull Long pupnagId, Multimap<EppGroupType, LoadTimeRule> newValues)
	{
		new DQLDeleteBuilder(TimeRule2EduGroup.class)
				.where(eq(property(TimeRule2EduGroup.L_YEAR), value(pupnagId)))
				.createStatement(getSession()).execute();

		if (newValues != null && !newValues.isEmpty())
		{
			DQLInsertValuesBuilder insertBuilder = new DQLInsertValuesBuilder(TimeRule2EduGroup.class);
			for (Map.Entry<EppGroupType, LoadTimeRule> item : newValues.entries())
			{
				if (item.getValue() != null)
					insertBuilder = insertBuilder
							.value(TimeRule2EduGroup.L_YEAR, pupnagId)
							.value(TimeRule2EduGroup.L_GROUP_TYPE, item.getKey())
							.value(TimeRule2EduGroup.L_TIME_RULE, item.getValue())
							.addBatch();
			}
			insertBuilder.createStatement(getSession()).execute();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<LoadTimeRule> getTimeRule4EduGroupType(@NotNull EppGroupType groupType, @NotNull Long pupnagId)
	{
		return new DQLSelectBuilder().fromEntity(TimeRule2EduGroup.class, "tr")
				.column(property("tr", TimeRule2EduGroup.L_TIME_RULE))
				.where(eq(property("tr", TimeRule2EduGroup.L_YEAR), value(pupnagId)))
				.where(eq(property("tr", TimeRule2EduGroup.L_GROUP_TYPE), value(groupType)))
				.createStatement(getSession()).list();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Multimap<PpsLoadType, LoadTimeRule> getPpsLoadType2TimeRuleMultimap()
	{
		List<PpsLoadType2TimeRule> loadType2TimeRules = getList(PpsLoadType2TimeRule.class);
		Multimap<PpsLoadType, LoadTimeRule> result = HashMultimap.create();
		for (PpsLoadType2TimeRule item : loadType2TimeRules)
		{
			result.put(item.getPpsLoadType(), item.getTimeRule());
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Multimap<PpsLoadGroup, LoadTimeRule> getPpsLoadGroup2TimeRuleMultimap()
	{
		List<PpsLoadGroup2TimeRule> loadType2TimeRules = getList(PpsLoadGroup2TimeRule.class);
		Multimap<PpsLoadGroup, LoadTimeRule> result = HashMultimap.create();
		for (PpsLoadGroup2TimeRule item : loadType2TimeRules)
		{
			result.put(item.getPpsLoadGroup(), item.getTimeRule());
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Multimap<PpsLoadCathedraPart, EppPlanStructure> getCathedraPart2PlanStructureMultimap()
	{
		List<LoadCathedraPart2EppPlanStructure> cathedraPart2EppPlanStructures = getList(LoadCathedraPart2EppPlanStructure.class);
		Multimap<PpsLoadCathedraPart, EppPlanStructure> result = HashMultimap.create();
		for (LoadCathedraPart2EppPlanStructure item : cathedraPart2EppPlanStructures)
		{
			result.put(item.getCathedraPart(), item.getPlanStructure());
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Multimap<String, String> getPpsLoadGroup2TimeRuleCodeMultimap()
	{
		List<PpsLoadGroup2TimeRule> loadType2TimeRules = getList(PpsLoadGroup2TimeRule.class);
		Multimap<String, String> result = HashMultimap.create();
		for (PpsLoadGroup2TimeRule item : loadType2TimeRules)
		{
			result.put(item.getPpsLoadGroup().getCode(), item.getTimeRule().getCode());
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updatePpsLoadType2TimeRule(Multimap<PpsLoadType, LoadTimeRule> newValues)
	{
		new DQLDeleteBuilder(PpsLoadType2TimeRule.class)
				.createStatement(getSession()).execute();

		if (newValues != null && !newValues.isEmpty())
		{
			DQLInsertValuesBuilder insertBuilder = new DQLInsertValuesBuilder(PpsLoadType2TimeRule.class);
			for (Map.Entry<PpsLoadType, LoadTimeRule> item : newValues.entries())
			{
				if (item.getValue() != null)
					insertBuilder = insertBuilder
							.value(PpsLoadType2TimeRule.L_PPS_LOAD_TYPE, item.getKey())
							.value(PpsLoadType2TimeRule.L_TIME_RULE, item.getValue())
							.addBatch();
			}
			insertBuilder.createStatement(getSession()).execute();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updatePpsLoadGroup2TimeRule(Multimap<PpsLoadGroup, LoadTimeRule> newValues)
	{
		new DQLDeleteBuilder(PpsLoadGroup2TimeRule.class)
				.createStatement(getSession()).execute();

		if (newValues != null && !newValues.isEmpty())
		{
			DQLInsertValuesBuilder insertBuilder = new DQLInsertValuesBuilder(PpsLoadGroup2TimeRule.class);
			for (Map.Entry<PpsLoadGroup, LoadTimeRule> item : newValues.entries())
			{
				if (item.getValue() != null)
					insertBuilder = insertBuilder
							.value(PpsLoadGroup2TimeRule.L_PPS_LOAD_GROUP, item.getKey())
							.value(PpsLoadGroup2TimeRule.L_TIME_RULE, item.getValue())
							.addBatch();
			}
			insertBuilder.createStatement(getSession()).execute();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateCathedraPart2PlanStructureMultimap(Multimap<PpsLoadCathedraPart, EppPlanStructure> newValues)
	{
		new DQLDeleteBuilder(LoadCathedraPart2EppPlanStructure.class)
				.createStatement(getSession()).execute();

		if (newValues != null && !newValues.isEmpty())
		{
			DQLInsertValuesBuilder insertBuilder = new DQLInsertValuesBuilder(LoadCathedraPart2EppPlanStructure.class);
			for (Map.Entry<PpsLoadCathedraPart, EppPlanStructure> item : newValues.entries())
			{
				if (item.getValue() != null)
					insertBuilder = insertBuilder
							.value(LoadCathedraPart2EppPlanStructure.L_CATHEDRA_PART, item.getKey())
							.value(LoadCathedraPart2EppPlanStructure.L_PLAN_STRUCTURE, item.getValue())
							.addBatch();
			}
			insertBuilder.createStatement(getSession()).execute();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<Long, Boolean> getPpsSecondJobInfoMap(Collection<Long> ppsIdList)
	{
		List<Object[]> dataList = new DQLSelectBuilder().fromEntity(LoadRecordOwner.class, "lro")
				.column(property("pe.id"))
				.column(property("ep", EmployeePost.L_POST_TYPE))
				.where(in(property("lro", LoadRecordOwner.P_ID), ppsIdList))
				.joinPath(DQLJoinType.inner, LoadRecordOwner.realPps().pps().fromAlias("lro"), "pe")
				.joinEntity("pe", DQLJoinType.inner, Person.class, "pr", eq(property("pr.id"), property("pe", PpsEntry.person().id())))
				.joinEntity("lro", DQLJoinType.inner, EmployeePost.class, "ep", eq(property("ep", EmployeePost.person().id()), property("pr.id")))
				.createStatement(getSession()).list();

		Map<Long, Boolean> result = new HashMap<>(dataList.size());

		for (Object[] item : dataList)
		{
			Long ppsId = (Long) item[0];
			String postTypeCode = ((PostType) item[1]).getCode();
			Boolean secondJob = Boolean.FALSE;
			switch (postTypeCode)
			{
				case PostTypeCodes.SECOND_JOB:
				case PostTypeCodes.SECOND_JOB_INNER:
				case PostTypeCodes.SECOND_JOB_OUTER:
					secondJob = Boolean.TRUE;
			}
			result.put(ppsId, secondJob);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<Long> getEduGroupStudentsId(Long eduGroupId)
	{
		String egr = "egr";
		return new DQLSelectBuilder().fromEntity(EppRealEduGroupRow.class, egr)
				.where(DQLExpressions.eq(DQLExpressions.property(egr, EppRealEduGroupRow.L_GROUP), DQLExpressions.value(eduGroupId)))
				.distinct()
				.createStatement(getSession()).idList();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getEduLevelHighSchoolTitleList(Long orgUnitId)
	{
		String elhs = "elhs";//alias
		return new DQLSelectBuilder().fromEntity(EducationLevelsHighSchool.class, elhs)
				.column(property(elhs, EducationLevelsHighSchool.P_TITLE))
				.where(eq(property(elhs, EducationLevelsHighSchool.orgUnit().id()), value(orgUnitId)))
				.distinct()
				.createStatement(getSession()).list();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, String> getTimeRuleCode2TitleMap(Collection<String> timeRuleCodes)
	{
		String lt = "lt";//alias
		DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(LoadTimeRule.class, lt)
				.column(DQLExpressions.property(lt, LoadTimeRule.P_CODE))
				.column(DQLExpressions.property(lt, LoadTimeRule.P_TITLE))
				.where(DQLExpressions.in(DQLExpressions.property(lt, LoadTimeRule.P_CODE), timeRuleCodes));
		Map<String, String> result = new HashMap<>();

		for (Object[] item : builder.createStatement(getSession()).<Object[]>list())
		{
			String code = (String) item[0];
			String title = (String) item[1];
			result.put(code, title);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PpsLoadDocumentWrapper preparePpsLoadDocumentWrapper(Long loadRecordOwnerId)
	{
		PpsLoadDocumentWrapper document = new PpsLoadDocumentWrapper();

		EppWorkPlanRegistryElementRow row;

		for (LoadEduGroupPpsDistr eduGroupDistr : getEduGroupPpsDistr4ALoadType(loadRecordOwnerId))
		{
			row = eduGroupDistr.getEduGroup().getWorkPlanRow();
			if (row == null) continue;//пропускаем такие
			document.add(eduGroupDistr, getTerm(row));
		}
		/***********************************************************************************************************/
		for (LoadFirstCourseImEduGroup firstCourse : getFirstCourseImEduGroup4ALoadType(loadRecordOwnerId))
		{
			row = firstCourse.getRow();
			document.add(firstCourse, getTerm(row));
		}
		/***********************************************************************************************************/
		for (OtherLoad otherLoad : getOtherLoad(loadRecordOwnerId))
		{
			document.add(otherLoad, getTerm(otherLoad.getYearDistributionPart()));
		}
		return document;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConsolidatedDocumentWrapper prepareConsolidatedDocumentWrapper(Long pupnagId, Long orgUnitId)
	{

		//collect all load PPS
		Collection<Long> loadPpsIds = getLoadPpsIdList(orgUnitId, pupnagId);
		ConsolidatedDocumentWrapper document = new ConsolidatedDocumentWrapper(/*orgUnitId, pupnagId*/);

		for (Long loadPpsId : loadPpsIds)
		{
			final LoadRecordOwner owner = getNotNull(loadPpsId);
			final boolean isHourly = owner.getHourlyLoad();
            final HourType hourType = getHourType(owner);

			EduProgramForm eduForm;
			Collection<PpsLoadCathedraPart> cathedraPart;
			EppWorkPlanRegistryElementRow row;

			//Распределение часов УГС

            for (LoadEduGroupPpsDistr eduGroupDistr : getEduGroupPpsDistr4ALoadType(loadPpsId))
			{
				document.getWrappedPpsRow(owner).add(eduGroupDistr);

				row = eduGroupDistr.getEduGroup().getWorkPlanRow();
				if (row == null) continue;//пропускаем такие

				eduForm = getEduForm(row);
				cathedraPart = getCathedraPart(row);

				ConsolidatedDocumentSheetWrapper hourSheet = document.getSheet(hourType, getTerm(row));
				hourSheet.getBlock(eduForm).addToBlock(cathedraPart, row, owner, eduGroupDistr);

				if (!isHourly)
				{
					OrgUnit orgUnit = getNotNull(orgUnitId);
					ConsolidatedDocumentSheetWrapper typeSheet = document.getSheet(getSheetType(orgUnit, row), getTerm(row));

					typeSheet.getBlock(eduForm).addToBlock(cathedraPart, row, owner, eduGroupDistr);
				}
			}
			/***********************************************************************************************************/
			//Планируемая УГС первого курса (демо)
			for (LoadFirstCourseImEduGroup firstCourse : getFirstCourseImEduGroup4ALoadType(loadPpsId))
			{
				document.getWrappedPpsRow(owner).add(firstCourse);

				row = firstCourse.getRow();
				eduForm = getEduForm(row);
				cathedraPart = getCathedraPart(row);

				ConsolidatedDocumentSheetWrapper hourSheet = document.getSheet(hourType, getTerm(row));
				hourSheet.getBlock(eduForm).addToBlock(cathedraPart, row, owner, firstCourse);

				if (!isHourly)
				{
					OrgUnit orgUnit = getNotNull(orgUnitId);
					ConsolidatedDocumentSheetWrapper typeSheet = document.getSheet(getSheetType(orgUnit, row), getTerm(row));
					typeSheet.getBlock(eduForm).addToBlock(cathedraPart, row, owner, firstCourse);
				}
			}
			/***********************************************************************************************************/
            //Другие виды нагрузки
            for (OtherLoad otherLoad : getOtherLoad(loadPpsId))
            {
                eduForm = otherLoad.getPostgraduateProgramForm();
                if (eduForm != null)
                {
                    document.getWrappedPpsRow(owner).add(otherLoad);
                    final YearDistributionPart distributionPart = otherLoad.getYearDistributionPart();
                    ConsolidatedDocumentSheetWrapper hourSheet = document.getSheet(hourType, getTerm(distributionPart));
                    hourSheet.getBlock(eduForm).addToPostgraduateRow(otherLoad);
                }
            }
        }
        return document;
	}

	//Распределение часов УГС только по виду аудиторной нагрузки для ППС (нагрузки)
	private List<LoadEduGroupPpsDistr> getEduGroupPpsDistr4ALoadType(Long loadPpsId)
	{
		String egpd = "egpd";
		String alt = "alt";
		return new DQLSelectBuilder().fromEntity(LoadEduGroupPpsDistr.class, egpd)
				.column(property(egpd))
				.where(eq(property(egpd, LoadEduGroupPpsDistr.L_OWNER), value(loadPpsId)))
				.joinPath(DQLJoinType.inner, LoadEduGroupPpsDistr.eduGroup().type().fromAlias(egpd), alt)
				.where(instanceOf(alt, EppGroupTypeALT.class))//только УГС-ВАН
				.createStatement(getSession()).list();
	}

	//Планируемая УГС первого курса (демо) только по виду аудиторной нагрузки для ППС (нагрузки)
	private List<LoadFirstCourseImEduGroup> getFirstCourseImEduGroup4ALoadType(Long loadPpsId)
	{
		String fceg = "fceg";
		String alt = "alt";
		return new DQLSelectBuilder().fromEntity(LoadFirstCourseImEduGroup.class, fceg)
				.column(property(fceg))
				.where(eq(property(fceg, LoadFirstCourseImEduGroup.L_OWNER), value(loadPpsId)))
				.joinPath(DQLJoinType.inner, LoadFirstCourseImEduGroup.type().fromAlias(fceg), alt)
				.where(instanceOf(alt, EppGroupTypeALT.class))//только УГС-ВАН
				.createStatement(getSession()).list();
	}

	//Другие виды нагрузки для ППС (нагрузки)
	private List<OtherLoad> getOtherLoad(Long loadPpsId)
	{
		String ol = "ol";
		return new DQLSelectBuilder().fromEntity(OtherLoad.class, ol)
				.column(property(ol))
				.where(eq(property(ol, OtherLoad.L_OWNER), value(loadPpsId)))
				.createStatement(getSession()).list();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<LoadTimeRule> getTimeRules4EduGroup(Long loadImEdugroupId)
	{
		return new DQLSelectBuilder().fromEntity(LoadImEduGroupTimeRuleRel.class, "rel")
				.column(property("rel", LoadImEduGroupTimeRuleRel.timeRule()))
				.where(eq(property("rel", LoadImEduGroupTimeRuleRel.eduGroup().id()), value(loadImEdugroupId)))
				.createStatement(getSession()).list();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Double getTotalEduLoad4EduGroup(ILoadImEduGroup loadImEdugroup)
	{
		Collection<LoadTimeRule> timeRules= getTimeRules4EduGroup(loadImEdugroup.getId());
		Long eduLoad = 0L;
		for (LoadTimeRule timeRule : timeRules)
		{
			eduLoad += getEduLoad4EduGroup(loadImEdugroup, timeRule);
		}
		return UniEppUtils.wrap(eduLoad);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Double getEduLoad4EduGroupAsDouble(ILoadImEduGroup loadImEdugroup, LoadTimeRule timeRules){
		Long eduLoad = 0L;
		eduLoad += getEduLoad4EduGroup(loadImEdugroup, timeRules);
		return UniEppUtils.wrap(eduLoad);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long getEduLoad4EduGroup(ILoadImEduGroup loadImEdugroup, LoadTimeRule timeRule)
	{
		switch (timeRule.getBase())
		{
			case STUDENTS:
				return timeRule.getCoefficient() * loadImEdugroup.getStudents();
			case HOURS:
				return UniEppUtils.unwrap(timeRule.getCoefficientAsDouble() * UniEppUtils.wrap(loadImEdugroup.getHours()));
			case CONSTANT:
				return timeRule.getCoefficient();
			default:
				return 0L;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isSchool(OrgUnit orgUnit)
	{
		//TODO добавить в справочник типов подразделений (только для ДВФУ)
		return orgUnit.getOrgUnitType().getTitle().toLowerCase().equals("школа");
	}

	private HourType getHourType(LoadRecordOwner owner)
	{
		if (owner.getHourlyLoad())
			return HourType.HOUR;
		else
			return HourType.NON_HOUR;
	}

	private EduProgramForm getEduForm(@NotNull EppWorkPlanRegistryElementRow registryElementRow)
	{
		EppWorkPlan workPlan = registryElementRow.getWorkPlan().getWorkPlan();
		return workPlan.getParent().getEduPlanVersion().getEduPlan().getProgramForm();
	}

	private Term getTerm(EppWorkPlanRegistryElementRow registryElementRow)
	{
		EppWorkPlan workPlan = registryElementRow.getWorkPlan().getWorkPlan();
		return getTerm(workPlan.getGridTerm().getPart());
	}

	private Term getTerm(YearDistributionPart yearPart) throws IllegalArgumentException
	{
		if (yearPart.getCode().equals(YearDistributionPartCodes.SUMMER_SEMESTER))
			return Term.SUMMER;

		return Term.WINTER;
	}

	private Collection<PpsLoadCathedraPart> getCathedraPart(EppWorkPlanRegistryElementRow registryElementRow)
	{
		EppWorkPlan workPlan = registryElementRow.getWorkPlan().getWorkPlan();
		EppStateEduStandard parent = workPlan.getParent().getEduPlanVersion().getEduPlan().getParent();

		if (parent == null) return Collections.emptyList();

		EduProgramSubjectIndex subjectIndex = parent.getProgramSubject().getSubjectIndex();

		String ps4si = "ps4si";
		DQLSelectBuilder planStructureBuilder = new DQLSelectBuilder().fromEntity(EppPlanStructure4SubjectIndex.class, ps4si)
				.column(property(ps4si, EppPlanStructure4SubjectIndex.L_EPP_PLAN_STRUCTURE))
				.where(eq(property(ps4si, EppPlanStructure4SubjectIndex.L_EDU_PROGRAM_SUBJECT_INDEX), value(subjectIndex)))
				.distinct();
		List<EppPlanStructure> planStructure = planStructureBuilder.createStatement(getSession()).list();

		String cp2ps = "cp2ps";
		DQLSelectBuilder loadCathedraPartBuilder = new DQLSelectBuilder().fromEntity(LoadCathedraPart2EppPlanStructure.class, cp2ps)
				.column(property(cp2ps, LoadCathedraPart2EppPlanStructure.L_CATHEDRA_PART))
				.where(in(property(cp2ps, LoadCathedraPart2EppPlanStructure.L_PLAN_STRUCTURE), planStructure))
				.distinct();
		return loadCathedraPartBuilder.createStatement(getSession()).list();
	}

	private SheetType getSheetType(final OrgUnit cathedra, EppWorkPlanRegistryElementRow row)
	{
		if (!isSchool(cathedra)) return SheetType.OTHER;

		OrgUnit rowSchool = getSchool(row);

		if (cathedra.equals(rowSchool)) return SheetType.CATHEDRA;
		else return SheetType.OTHER;
	}

	private OrgUnit getSchool(EppWorkPlanRegistryElementRow row)
	{
		OrgUnit baseOrgUnit = row.getRegistryElementPart().getRegistryElement().getOwner();
		return getSchool(baseOrgUnit);
	}

	private OrgUnit getSchool(OrgUnit baseOrgUnit)
	{
		int maxDeep = 3;
		//читающее подразделение
		OrgUnit nextOrgUnit = baseOrgUnit;
		while (maxDeep-- > 0)
		{
			if (isSchool(nextOrgUnit)) return nextOrgUnit;
			nextOrgUnit = nextOrgUnit.getParent();
			if (nextOrgUnit == null) break;
		}
		return baseOrgUnit;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<DeviationViewWrapper> getDeviationWrapperList(Long orgUnitId, Long pupnagId)
	{
		Collection<Long> loadPpsIdList = getLoadPpsIdList(orgUnitId, pupnagId);
		String alias = "ld";
		List<LoadDeviation> deviationList = new DQLSelectBuilder().fromEntity(LoadDeviation.class, alias)
				.where(in(property(alias, LoadDeviation.L_OWNER), loadPpsIdList))
				.createStatement(getSession()).list();

		Map<Long, PpsLoadDataWrapper> loadDataWrapperMap = getPpsLoadDataWrapperMap(loadPpsIdList);

		List<DeviationViewWrapper> res = new ArrayList<>();
		for (LoadDeviation deviation : deviationList) {
			res.add(new DeviationViewWrapper(deviation, loadDataWrapperMap.get(deviation.getOwner().getId())));
		}
		return res;
	}

	/**
	 * {@inheritDoc}
	 */
	@Nullable
	@Override
	public LoadSize getLoadSize(Long pupnagId, @Nullable Long postId)
	{
		if(postId == null)
			return null;
		EppYearEducationProcess pupnag = getNotNull(pupnagId);
		PostBoundedWithQGandQL post = getNotNull(postId);
		return getByNaturalId(new LoadSize.NaturalId(pupnag, post));
	}
}