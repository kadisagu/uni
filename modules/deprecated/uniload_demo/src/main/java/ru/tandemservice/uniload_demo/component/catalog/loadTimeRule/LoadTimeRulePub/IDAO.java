/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.component.catalog.loadTimeRule.LoadTimeRulePub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.IDefaultCatalogPubDAO;
import ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule;

/**
 * @author Alexander Zhebko
 * @since 23.12.2013
 */
public interface IDAO extends IDefaultCatalogPubDAO<LoadTimeRule, Model>
{
    /**
     * Меняет признак "Используется" у выбранной нормы времени на противоположный.
     * @param timeRuleId id нормы времени
     */
    public void updateTimeRuleStatus(Long timeRuleId);

	/**
	 * Меняет признак "Разрешить добавлять без связи с УГС" у выбранной нормы времени на противоположный.
	 * @param timeRuleId id нормы времени
	 */
    public void updateTimeRulePermission(Long timeRuleId);
}