/*$Id$*/
package ru.tandemservice.uniload_demo.base.bo.Load.util.print;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.*;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramFormCodes;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniload_demo.base.bo.Load.LoadManager;
import ru.tandemservice.uniload_demo.base.bo.Load.logic.ILoadDAO;
import ru.tandemservice.uniload_demo.entity.PpsLoadGroup2TimeRule;
import ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule;
import ru.tandemservice.uniload_demo.entity.catalog.PpsLoadCathedraPart;
import ru.tandemservice.uniload_demo.entity.catalog.codes.PpsLoadGroupCodes;

import javax.validation.constraints.NotNull;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;

import static ru.tandemservice.uniload_demo.base.bo.Load.util.print.LoadDocumentPrintSharedUtils.*;

/**
 * @author DMITRY KNYAZEV
 * @since 19.11.2014
 */
public final class ConsolidatedLoadDocumentBuilder
{
	private final int ROW_COEFFICIENT = 100;
	private final int COLUMN_COEFFICIENT = 3;

	// создаем шрифты
	private final WritableFont times14b = new WritableFont(WritableFont.TIMES, 14, WritableFont.BOLD);
	private final WritableFont times12b = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
	private final WritableFont times09b = new WritableFont(WritableFont.TIMES, 9, WritableFont.BOLD);

	private final ILoadDAO _dao = LoadManager.instance().dao();
	private final GroupData _groupData = new GroupData();
	private final WritableCellFormatFactory _format = WritableCellFormatFactory.newInstance(times14b);

	private final EppYearEducationProcess _pupnag;
	private final OrgUnit _orgUnit;
	private final ConsolidatedDocumentWrapper _dataWrapper;

	public ConsolidatedLoadDocumentBuilder(final Long orgUnitId, final Long pupnagId)
	{
		_pupnag = getDao().getNotNull(pupnagId);
		_orgUnit = getDao().getNotNull(orgUnitId);
		_dataWrapper = getDao().prepareConsolidatedDocumentWrapper(pupnagId, orgUnitId);
	}

	public IDocumentRenderer buildDocumentRenderer() throws IOException, WriteException
	{
		String fileName = "Отчет по нагрузке_" + _orgUnit.getShortTitle() + "_" + _pupnag.getTitle() + ".xls";
		return new CommonBaseRenderer().xml().fileName(fileName).document(buildDocument());
	}

	private ByteArrayOutputStream buildDocument() throws WriteException, IOException
	{
		// создаем печатную форму
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		WorkbookSettings ws = new WorkbookSettings();
		ws.setEncoding("UTF-8");
		ws.setMergedCellChecking(false);
		ws.setRationalization(false);
		ws.setGCDisabled(true);
		WritableWorkbook workbook = Workbook.createWorkbook(out, ws);

		for (SheetType type : SheetType.values())
		{
			for (Term term : Term.values())
			{
				//Создает страницу
				WritableSheet sheet = workbook.createSheet(term.getShortTitle() + type.getTitle(), workbook.getSheets().length);
				sheet.getSettings().setOrientation(PageOrientation.LANDSCAPE);
				sheet.getSettings().setCopies(1);
				//first six sheets
				createDocumentSheet(sheet, term, type);
			}
		}
		for (HourType hourType : HourType.values())
		{
			for (Term term : Term.values())
			{
				//Создает страницу
				WritableSheet sheet = workbook.createSheet(term.getShortTitle() + hourType.getTitle(), workbook.getSheets().length);
				sheet.getSettings().setOrientation(PageOrientation.LANDSCAPE);
				sheet.getSettings().setCopies(1);

				createDocumentSheet(sheet, term, hourType);
			}
		}

		//Создает страницу
		WritableSheet sheet = workbook.createSheet("Свод_общ", workbook.getSheets().length);
		sheet.getSettings().setOrientation(PageOrientation.LANDSCAPE);
		sheet.getSettings().setCopies(1);
		createSummarySheet(sheet);

		//Создает страницу
		sheet = workbook.createSheet("Свод_аудит нагр", workbook.getSheets().length);
		sheet.getSettings().setOrientation(PageOrientation.LANDSCAPE);
		sheet.getSettings().setCopies(1);
		createSummaryASheet(sheet);

		//Создает страницу
		sheet = workbook.createSheet("штат", workbook.getSheets().length);
		sheet.getSettings().setOrientation(PageOrientation.LANDSCAPE);
		sheet.getSettings().setCopies(1);
		createStateSheet(sheet);

		//посылаем отчет
		workbook.write();
		workbook.close();
		return out;
	}

	//METHODS SHEET_1-6
	private void createDocumentSheet(WritableSheet sheet, Term term, ISheetType sheetType) throws WriteException
	{
		final SheetVO sheetVO;
		if (sheetType.isSheetType() && sheetType.equals(SheetType.CATHEDRA))
			sheetVO = new SheetVO(34, 11, getVarLength(), 3, sheet);
		else
			sheetVO = new SheetVO(36, 12, getVarLength(), 4, sheet);

		final int statLength = sheetVO.getStatLength(); //количество колонок в неизменяемой части
		final int varLength = sheetVO.getVarLength();   //getVarLength() + 3; //количество колонок в изменяемой части
		final int rowLength = sheetVO.getRowLength();   //действительное количество колонок

		//-------------------------------------------------------
		//set column width
		Map<Integer, Integer> columnWidth = new HashMap<>();
		columnWidth.put(0, 1);
		columnWidth.put(1, 8);
		if (isLongForm(sheetVO))
		{
			columnWidth.put(rowLength, 3);
			columnWidth.put(rowLength - 1, 3);
			columnWidth.put(rowLength - 2, 4);
			columnWidth.put(rowLength - 3, 6);
		}
		else
		{
			columnWidth.put(rowLength, 3);
			columnWidth.put(rowLength - 1, 4);
			columnWidth.put(rowLength - 2, 6);
		}

		setColumnWidth(sheet, columnWidth, 2, COLUMN_COEFFICIENT, rowLength);
		// 1 row--------------------------------------------------------
		{
			WritableCellFormat rowFormat14 = _format.setFont(times14b)
					.setAlignment(Alignment.CENTRE)
					.setVerticalAlignment(VerticalAlignment.BOTTOM)
					.getCellFormat();

			String label = "РАСЧЕТ ПЛАНИРУЕМОГО ОБЪЕМА УЧЕБНОЙ РАБОТЫ ПО КАФЕДРЕ ШТАТНЫХ ПРЕПОДАВАТЕЛЕЙ И СОВМЕСТИТЕЛЕЙ\nНА " + getEduYear() + " УЧЕБНЫЙ ГОД";
			sheet.setRowView(sheetVO.getCurrentRow(), 10 * ROW_COEFFICIENT);
			fillRow(sheetVO.getCurrentRow(), sheet, rowLength, label, rowFormat14);
			sheetVO.incCurrentRow();
			//2 row -------------------------------------------------------
			rowFormat14 = _format.setFont(times14b)
					.setAlignment(Alignment.CENTRE)
					.setVerticalAlignment(VerticalAlignment.BOTTOM)
					.getCellFormat();

			String[] labelRow = {"Школа", getSchoolTitle(_orgUnit), "Кафедра", getCathedraTitle(_orgUnit), term.getTitle(), "учебный год", getEduYear()};
			Integer[] labelLen = {/*stat*/2, 5, 4,/*var*/11, 3, 6, 3};

			fillRow(sheetVO.getCurrentRow(), sheet, labelRow, labelLen, rowFormat14);
			sheetVO.incCurrentRow();
			//3 row -------------------------------------------------------
			WritableCellFormat rowFormat12 = _format.setFont(times12b)
					.setAlignment(Alignment.LEFT)
					.setVerticalAlignment(VerticalAlignment.BOTTOM)
					.getCellFormat();

			label = "Направление(я) подготовки, закрепленное (ые) за кафедрой:";
			fillRow(sheetVO.getCurrentRow(), sheet, rowLength, label, rowFormat12);
			sheetVO.incCurrentRow();
		}
		//N rows -------------------------------------------------------
		{
			WritableCellFormat rowFormat14 = _format.setFont(times14b)
					.setAlignment(Alignment.LEFT)
					.setVerticalAlignment(VerticalAlignment.BOTTOM)
					.getCellFormat();

			String[] labelRow = new String[2];
			Integer[] labelLen = new Integer[]{1, rowLength - 1};

			Integer counter = 0;
			List<String> eduLevelHighSchoolTitleLIst = getDao().getEduLevelHighSchoolTitleList(getOrgUnitId());
			for (String title : eduLevelHighSchoolTitleLIst)
			{
				labelRow[0] = (++counter).toString();
				labelRow[1] = title;
				fillRow(sheetVO.getCurrentRow(), sheet, labelRow, labelLen, rowFormat14);
				sheetVO.incCurrentRow();

				if(ApplicationRuntime.existProperty("demo.uniload.loadreport.shortheader"))
				{
					if (sheetVO.getCurrentRow() > 5)
					{
						labelRow[0] = (++counter).toString();
						labelRow[1] = "Количество направлений подготовки превышает " + eduLevelHighSchoolTitleLIst.size();
						fillRow(sheetVO.getCurrentRow(), sheet, labelRow, labelLen, rowFormat14);
						sheetVO.incCurrentRow();
						break;
					}
				}
			}
		}
		//-------------------------------------------------------
		sheetVO.incCurrentRow();
		//-------------------------------------------------------
		{
			WritableCellFormat rowFormat12 = _format.setFont(times12b)
					.setAlignment(Alignment.CENTRE)
					.setVerticalAlignment(VerticalAlignment.BOTTOM)
					.setBordered(true)
					.getCellFormat();

			String[] labelRow = new String[]{"Сведения о читаемых дисциплинах", "Сведения об ООП", "Контингент", "Нагрузка в часах", "Сведения о преподавателях"};
			Integer[] labelLen = new Integer[labelRow.length];
			{
				int pos = 0;
		/*stat*/
				labelLen[pos++] = 4;
				if (isLongForm(sheetVO))
					labelLen[pos++] = 4;
				else
					labelLen[pos++] = 3;
				labelLen[pos++] = 4;
		/*var*/
				labelLen[pos++] = varLength + 1;
				if (isLongForm(sheetVO))
					labelLen[pos] = 3;
				else
					labelLen[pos] = 2;
			}
			fillRow(sheetVO.getCurrentRow(), sheet, labelRow, labelLen, rowFormat12);
			sheetVO.incCurrentRow();
		}
		//-------------------------------------------------------
		{
			WritableCellFormat rowFormat09 = _format.setFont(times09b)
					.setAlignment(Alignment.CENTRE)
					.setVerticalAlignment(VerticalAlignment.CENTRE)
					.setBordered(true)
					.getCellFormat();

			WritableCellFormat rowFormat09Rotate = _format.setVerticalAlignment(VerticalAlignment.BOTTOM)
					.setOrientation(Orientation.PLUS_90)
					.getCellFormat();


			Map<Integer, CellFormat> cellFormatMap = new HashMap<>();

			putMutikeyMap(cellFormatMap, rowFormat09Rotate, 2, 3, 4, 5, 6, 7, 8, 9, 10);
			if (isLongForm(sheetVO))
				putMutikeyMap(cellFormatMap, rowFormat09Rotate, 11, 19, 20);
			else
				cellFormatMap.put(18, rowFormat09Rotate);

			String[] labelRow = new String[rowLength];
			{
				int pos = 0;
				labelRow[pos++] = "№";
				labelRow[pos++] = "Дисциплина";
				labelRow[pos++] = "Форма отчётности (э/з/зэ)";
				labelRow[pos++] = "Количество письменных к/р.; расчетно-графических заданий";

				labelRow[pos++] = "Государственный образовательный стандарт (ГОС-II; ФГОС)";
				labelRow[pos++] = "Код направления подготовки (специальности)";
				labelRow[pos++] = "Наименование направления подготовки (специальности)";
				if (isLongForm(sheetVO))
				{
					labelRow[pos++] = "Школа";
				}

				labelRow[pos++] = "курс";
				labelRow[pos++] = "количество студентов/аспирантов";
				labelRow[pos++] = "№ группы";
				labelRow[pos++] = "количество групп";

				labelRow[pos++] = _groupData.getGroupTitle(PpsLoadGroupCodes.AUDITORNYE_ZANYATIYA);
				labelRow[pos++] = _groupData.getGroupTitle(PpsLoadGroupCodes.KONSULTATSII);
				labelRow[pos++] = _groupData.getGroupTitle(PpsLoadGroupCodes.KONTROL);
				labelRow[pos++] = _groupData.getGroupTitle(PpsLoadGroupCodes.PRAKTIKA);
				labelRow[pos++] = _groupData.getGroupTitle(PpsLoadGroupCodes.RUKOVODSTVO);

				labelRow[pos++] = "Всего часов";
				labelRow[pos++] = "ФИО преподавателя";
				labelRow[pos] = "Условия занятости преподавателя \n(штатный (ш), внутр.совм. (с),\nвнеш.совм. (в), почасовик (п))";
				if (isLongForm(sheetVO))
				{
					labelRow[++pos] = "№ заявки от Школы (если есть)";
				}
			}
			Integer[] labelLen = new Integer[labelRow.length];
			{
				int pos = statLength;
				labelLen[pos++] = _groupData.getGroupLen(PpsLoadGroupCodes.AUDITORNYE_ZANYATIYA);
				labelLen[pos++] = _groupData.getGroupLen(PpsLoadGroupCodes.KONSULTATSII);
				labelLen[pos++] = _groupData.getGroupLen(PpsLoadGroupCodes.KONTROL);
				labelLen[pos++] = _groupData.getGroupLen(PpsLoadGroupCodes.PRAKTIKA);
				labelLen[pos] = _groupData.getGroupLen(PpsLoadGroupCodes.RUKOVODSTVO);
			}
			fillRow(sheetVO.getCurrentRow(), sheet, labelRow, labelLen, cellFormatMap, rowFormat09);
			sheetVO.incCurrentRow();
		}
		//-------------------------------------------------------
		Map<Integer, String> timeRulePositionMap;
		{
			WritableCellFormat rowFormat09Rotate = _format.setFont(times09b)
					.setAlignment(Alignment.CENTRE)
					.setVerticalAlignment(VerticalAlignment.BOTTOM)
					.setOrientation(Orientation.PLUS_90)
					.setBordered(true)
					.getCellFormat();

			//соответсвие номера колонки и нормк времени
			timeRulePositionMap = fillTimeRuleRow(statLength, sheetVO.getCurrentRow(), sheet, rowFormat09Rotate, _groupData);
			timeRulePositionMap.put(statLength + varLength, ConsolidatedDocumentRowWrapper.ROW_TOTAL_LOAD_CODE);
			sheetVO.incCurrentRow();
		}
		//-------------------------------------------------------
		for (int i = 0; i < statLength; i++)
		{
			sheet.mergeCells(i, sheetVO.getCurrentRow() - 2, i, sheetVO.getCurrentRow() - 1);
		}
		for (int i = rowLength - sheetVO.getEndLength(); i < rowLength; i++)
		{
			sheet.mergeCells(i, sheetVO.getCurrentRow() - 2, i, sheetVO.getCurrentRow() - 1);
		}
		//-------------------------------------------------------
		{
			WritableCellFormat rowFormat09 = _format.setFont(times09b)
					.setAlignment(Alignment.CENTRE)
					.setVerticalAlignment(VerticalAlignment.BOTTOM)
					.setOrientation(Orientation.HORIZONTAL)
					.setBordered(true)
					.getCellFormat();

			String[] labelRow = new String[rowLength];
			for (int i = 0; i < rowLength; i++)
			{
				labelRow[i] = String.valueOf(i + 1);
			}

			fillRow(sheetVO.getCurrentRow(), sheet, labelRow, rowFormat09);
			sheetVO.incCurrentRow();
		}
		//-------------------------------------------------------
		//freeze first rows
		sheet.getSettings().setVerticalFreeze(sheetVO.getCurrentRow());
		//-------------------------------------------------------
		{
			ConsolidatedDocumentSheetWrapper documentSheet;
			if (sheetType.isSheetType())
				documentSheet = getDocument().getSheet(((SheetType) sheetType), term);
			else
				documentSheet = getDocument().getSheet(((HourType) sheetType), term);

			Map<String, Double> totalSheetLoadMap = new HashMap<>();//суммарная нагрузка по всему листу
			for (String eduFormCode : EduProgramFormCodes.CODES)
			{
				Map<String, Double> totalEduBlockLoadMap;//Суммарная нагрузка по блоку формы обучения
				EduProgramForm eduForm = getEduProgramForm(eduFormCode);

				WritableCellFormat rowFormat09 = _format.setFont(times09b)
						.setAlignment(Alignment.CENTRE)
						.setVerticalAlignment(VerticalAlignment.BOTTOM)
						.setBordered(true)
						.getCellFormat();

				fillRow(sheetVO.getCurrentRow(), sheet, rowLength, eduForm.getTitle().toUpperCase(), rowFormat09);
				sheetVO.incCurrentRow();
				ConsolidatedDocumentSheetWrapper.ConsolidatedDocumentSuperBlockWrapper documentBlock = documentSheet.getBlock(eduForm);
				//SuperBlock
				totalEduBlockLoadMap = createEduFormBlock(sheetVO, documentBlock, timeRulePositionMap);

				rowFormat09 = _format.setAlignment(Alignment.RIGHT).getCellFormat();
				fillRow(sheetVO.getCurrentRow(), sheet, statLength, "ИТОГО АУДИТОРНЫХ ЗАНЯТИЙ ПО " + sheetType.getPrintTitle().toUpperCase() + " " + changeCase(eduForm).toUpperCase() + " ОТДЕЛЕНИЕ", rowFormat09);

				rowFormat09 = _format.setAlignment(Alignment.CENTRE).getCellFormat();
				fillLoadRowPart(sheetVO.getCurrentRow(), statLength, sheet, varLength + 1, rowFormat09, totalEduBlockLoadMap, timeRulePositionMap);
				sheetVO.incCurrentRow();
				//add block load to total load
				mergeMaps(totalEduBlockLoadMap, totalEduBlockLoadMap);
			}
			WritableCellFormat rowFormat09 = _format.setAlignment(Alignment.RIGHT).getCellFormat();
			fillRow(sheetVO.getCurrentRow(), sheet, statLength, "Итого аудиторных занятий по " + sheetType.getPrintTitle() + " за " + term.getTitle() + " (по всем формам обучения)", rowFormat09);

			rowFormat09 = _format.setAlignment(Alignment.CENTRE).getCellFormat();
			fillLoadRowPart(sheetVO.getCurrentRow(), statLength, sheet, varLength + 1, rowFormat09, totalSheetLoadMap, timeRulePositionMap);
			sheetVO.incCurrentRow();
		}
		{
			//-------------------------------------------------------
			createEduGroupBlock(sheetVO, _groupData, term, timeRulePositionMap);

			WritableCellFormat rowFormat09 = _format.setAlignment(Alignment.RIGHT).getCellFormat();
			fillRow(sheetVO.getCurrentRow(), sheet, statLength, "Итого кроме аудиторных занятий по " + sheetType.getPrintTitle() + " за " + term.getTitle() + " (по всем формам обучения)", rowFormat09);
			fillLoadRowPart(sheetVO.getCurrentRow(), statLength, sheet, varLength + 1, rowFormat09, Collections.<String, Double>emptyMap(), timeRulePositionMap);
			sheetVO.incCurrentRow();

			fillRow(sheetVO.getCurrentRow(), sheet, statLength, "Итого по " + sheetType.getPrintTitle() + " за " + term.getTitle() + " (по всем формам обучения)", rowFormat09);
			fillLoadRowPart(sheetVO.getCurrentRow(), statLength, sheet, varLength + 1, rowFormat09, Collections.<String, Double>emptyMap(), timeRulePositionMap);
			sheetVO.incCurrentRow();
		}
	}

	//METHODS SHEET_7 "Свод_общ"
	private void createSummarySheet(WritableSheet sheet) throws WriteException
	{
		final SheetVO sheetVO = new SheetVO(23, 2, getVarLength(), 1, sheet);

		final int statLength = sheetVO.getStatLength(); //количество колонок в неизменяемой части
		final int varLength = sheetVO.getVarLength();//getVarLength() + 3; //количество колонок в изменяемой части
		final int rowLength = sheetVO.getRowLength(); //действительное количество колонок

		//-------------------------------------------------------
		Map<Integer, Integer> columnWidth = new HashMap<>();
		columnWidth.put(0, 7);
		columnWidth.put(1, 7);
		columnWidth.put(rowLength, 3);
		setColumnWidth(sheet, columnWidth, 2, COLUMN_COEFFICIENT, rowLength);
		// 1 row--------------------------------------------------------
		{
			WritableCellFormat rowFormat14 = _format.setFont(times14b)
					.setAlignment(Alignment.CENTRE)
					.setVerticalAlignment(VerticalAlignment.BOTTOM)
					.setOrientation(Orientation.HORIZONTAL)
					.setBordered(false)
					.getCellFormat();

			String label = "СВОДНЫЕ ДАННЫЕ ПО РАСЧЕТУ ПЛАНИРУЕМОГО ОБЪЕМА УЧЕБНОЙ РАБОТЫ ПО КАФЕДРЕ";
			sheet.setRowView(sheetVO.getCurrentRow(), 8 * ROW_COEFFICIENT);
			fillRow(sheetVO.getCurrentRow(), sheet, rowLength, label, rowFormat14);
			sheetVO.incCurrentRow();
		}
		//2 row -------------------------------------------------------
		{
			WritableCellFormat rowFormat14 = _format.setFont(times14b)
					.setAlignment(Alignment.CENTRE)
					.setVerticalAlignment(VerticalAlignment.BOTTOM)
					.setOrientation(Orientation.HORIZONTAL)
					.setBordered(false)
					.getCellFormat();
            final WritableCellFormat rowFormat12Borderd = _format.setBordered(true).getCellFormat();

			String[] labelRow = {"Школа", getSchoolTitle(_orgUnit), "Кафедра", getCathedraTitle(_orgUnit), "учебный год", getEduYear()};
			Integer[] labelLen = {/*stat*/1, 5, /*var*/5, 6, 3, 3};


            Map<Integer, CellFormat> labelFormat = new HashMap<Integer, CellFormat>(){{put(4, rowFormat12Borderd);put(5, rowFormat12Borderd);}};

			fillRow(sheetVO.getCurrentRow(), sheet, labelRow, labelLen, labelFormat, rowFormat14);
			sheetVO.incCurrentRow();
		}
		//3 row -------------------------------------------------------
		{
			WritableCellFormat rowFormat12 = _format.setFont(times14b)
					.setAlignment(Alignment.LEFT)
					.setVerticalAlignment(VerticalAlignment.BOTTOM)
					.setOrientation(Orientation.HORIZONTAL)
					.setBordered(false)
					.getCellFormat();

			String label = "Направление(я) подготовки, закрепленное (ые) за кафедрой:";
			fillRow(sheetVO.getCurrentRow(), sheet, rowLength, label, rowFormat12);
			sheetVO.incCurrentRow();
		}
		//N rows -------------------------------------------------------
		{
			WritableCellFormat rowFormat14 = _format.setFont(times14b)
					.setAlignment(Alignment.LEFT)
					.setVerticalAlignment(VerticalAlignment.BOTTOM)
					.setOrientation(Orientation.HORIZONTAL)
					.setBordered(false)
					.getCellFormat();

			{
				Integer counter = 0;
				List<String> eduLevelHighSchoolTitleLIst = getDao().getEduLevelHighSchoolTitleList(getOrgUnitId());
				for (String title : eduLevelHighSchoolTitleLIst)
				{
					String label = (++counter).toString() + " " + title;
					fillRow(sheetVO.getCurrentRow(), sheet, rowLength, label, rowFormat14);
					sheetVO.incCurrentRow();

					if(ApplicationRuntime.existProperty("demo.uniload.loadreport.shortheader"))
					{
						if (sheetVO.getCurrentRow() > 5)
						{
							label = (++counter).toString() + " Количество направлений подготовки превышает " + eduLevelHighSchoolTitleLIst.size();
							fillRow(sheetVO.getCurrentRow(), sheet, rowLength, label, rowFormat14);
							sheetVO.incCurrentRow();
							break;
						}
					}
				}
			}
		}
		//-------------------------------------------------------
		sheetVO.incCurrentRow();
		//-------------------------------------------------------
		{
			WritableCellFormat rowFormat12 = _format.setFont(times12b)
					.setAlignment(Alignment.CENTRE)
					.setVerticalAlignment(VerticalAlignment.CENTRE)
					.setOrientation(Orientation.HORIZONTAL)
					.setBordered(true)
					.getCellFormat();

			String[] labelRow = new String[]{"Семестр", "Форма обучения", "Объем учебной работы по кафедре (в часах)"};
			Integer[] labelLen = new Integer[]{1, 1, varLength + 1};
			fillRow(sheetVO.getCurrentRow(), sheet, labelRow, labelLen, rowFormat12);
			sheetVO.incCurrentRow();
			//-------------------------------------------------------
			WritableCellFormat rowFormat09 = _format.setFont(times09b)
					.setAlignment(Alignment.CENTRE)
					.setVerticalAlignment(VerticalAlignment.CENTRE)
					.setOrientation(Orientation.HORIZONTAL)
					.setBordered(true)
					.getCellFormat();

			labelRow = new String[8];
			{
				int pos = statLength;
				for (String code : PpsLoadGroupCodes.CODES)
				{
					labelRow[pos++] = _groupData.getGroupTitle(code);
				}
				labelRow[pos] = "Всего часов";
			}
			labelLen = new Integer[labelRow.length];
			{
				int pos = statLength;
				for (String code : PpsLoadGroupCodes.CODES)
				{
					labelLen[pos++] = _groupData.getGroupLen(code);
				}
			}
			fillRow(sheetVO.getCurrentRow(), sheet, labelRow, labelLen, rowFormat09);
			sheetVO.incCurrentRow();
		}
		//-------------------------------------------------------
		Map<Integer, String> timeRulePositionMap;
		{
			WritableCellFormat rowFormat09Rotate = _format.setFont(times09b)
					.setAlignment(Alignment.CENTRE)
					.setVerticalAlignment(VerticalAlignment.BOTTOM)
					.setBordered(true)
					.setOrientation(Orientation.PLUS_90)
					.getCellFormat();

			//соответсвие номера колонки и нормк времени
			timeRulePositionMap = fillTimeRuleRow(statLength, sheetVO.getCurrentRow(), sheet, rowFormat09Rotate, _groupData);
			timeRulePositionMap.put(statLength + varLength, ConsolidatedDocumentRowWrapper.ROW_TOTAL_LOAD_CODE);
			sheetVO.incCurrentRow();
		}
		//-------------------------------------------------------
        sheet.mergeCells(rowLength, sheetVO.getCurrentRow() - 3, rowLength, sheetVO.getCurrentRow() - 1);//if it delete, first column do not align vertically
		for (int i = 0; i < statLength; i++)
		{
			sheet.mergeCells(i, sheetVO.getCurrentRow() - 3, i, sheetVO.getCurrentRow() - 1);
		}
		for (int i = rowLength - 1; i < rowLength; i++)
		{
			sheet.mergeCells(i, sheetVO.getCurrentRow() - 2, i, sheetVO.getCurrentRow() - 1);
		}
		//-------------------------------------------------------
		{
			WritableCellFormat rowFormat09 = _format.setOrientation(Orientation.HORIZONTAL)
					.setAlignment(Alignment.CENTRE)
					.setVerticalAlignment(VerticalAlignment.CENTRE)
					.getCellFormat();
			String[] labelRow = new String[rowLength];
			for (int i = 0; i < rowLength; i++)
			{
				labelRow[i] = String.valueOf(i + 1);
			}

			fillRow(sheetVO.getCurrentRow(), sheet, labelRow, rowFormat09);
			sheetVO.incCurrentRow();
		}
		//-------------------------------------------------------
		//main cycle
		{
			int varTotalLength = varLength + 1;
			for (Term term : Term.values())
			{
				WritableCellFormat rowFormat09 = _format.getCellFormat();
				int startColumn = 0;
				sheet.addCell(new Label(startColumn++, sheetVO.getCurrentRow(), term.getTitle(), rowFormat09));
				ConsolidatedDocumentSheetWrapper sheetWrapper = getDocument().getSheet(HourType.NON_HOUR, term);

				for (String eduFormCod : EduProgramFormCodes.CODES)
				{
					EduProgramForm eduForm = getEduProgramForm(eduFormCod);
					Map<String, Double> blockLoad = sheetWrapper.getBlockLoad(eduForm);
					sheet.addCell(new Label(startColumn, sheetVO.getCurrentRow(), eduForm.getTitle(), rowFormat09));
					//add load block
					rowFormat09 = _format.getCellFormat();
					fillLoadRowPart(sheetVO.getCurrentRow(), startColumn + 1, sheet, varTotalLength, rowFormat09, blockLoad, timeRulePositionMap);
					sheetVO.incCurrentRow();
				}
				//total non hour load
				rowFormat09 = _format.getCellFormat();
				sheet.addCell(new Label(startColumn, sheetVO.getCurrentRow(), "Итого за осенний семестр(без учета почасового фонда)", rowFormat09));
				fillLoadRowPart(sheetVO.getCurrentRow(), startColumn + 1, sheet, varTotalLength, rowFormat09, sheetWrapper.getSheetLoad(), timeRulePositionMap);
				sheetVO.incCurrentRow();
				//total hour load
				sheetWrapper = getDocument().getSheet(HourType.HOUR, term);
				rowFormat09 = _format.getCellFormat();

				sheet.addCell(new Label(startColumn, sheetVO.getCurrentRow(), "почасовой фонд", rowFormat09));
				fillLoadRowPart(sheetVO.getCurrentRow(), startColumn + 1, sheet, varTotalLength, rowFormat09, sheetWrapper.getSheetLoad(), timeRulePositionMap);
				sheetVO.incCurrentRow();

				sheet.mergeCells(0, sheetVO.getCurrentRow() - 5, 0, sheetVO.getCurrentRow() - 1);
				//total hour load
				rowFormat09 = _format.getCellFormat();
				sheet.addCell(new Label(startColumn - 1, sheetVO.getCurrentRow(), "Итого нагрузка за " + term.getTitle(), rowFormat09));

				fillLoadRowPart(sheetVO.getCurrentRow(), startColumn + 1, sheet, varTotalLength, rowFormat09, getDocument().getDocumentHourLoad(term), timeRulePositionMap);
				sheet.mergeCells(startColumn - 1, sheetVO.getCurrentRow(), startColumn, sheetVO.getCurrentRow());
				sheetVO.incCurrentRow();
			}
		}
	}

	//METHODS SHEET_8 "Свод_аудит нагр"
	private void createSummaryASheet(WritableSheet sheet) throws WriteException
	{

		int currentRow = 0;
		final int rowLength = 10; //действительное количество колонок
		//-------------------------------------------------------
		Map<Integer, Integer> columnWidth = new HashMap<>();
		columnWidth.put(0, 10);
		setColumnWidth(sheet, columnWidth, 4, COLUMN_COEFFICIENT, rowLength);
		// 1 row--------------------------------------------------------
		{
			WritableCellFormat rowFormat12 = _format.setFont(times12b)
					.setOrientation(Orientation.HORIZONTAL)
					.setAlignment(Alignment.CENTRE)
					.setVerticalAlignment(VerticalAlignment.BOTTOM)
                    .setBordered(false)
					.getCellFormat();

			String label = "СВОДНЫЕ ДАННЫЕ ПО РАСЧЕТУ ПЛАНИРУЕМОГО ОБЪЕМА АУДИТОРНОЙ УЧЕБНОЙ РАБОТЫ ПО КАФЕДРЕ";
			sheet.setRowView(currentRow, 7 * ROW_COEFFICIENT);
			fillRow(currentRow, sheet, rowLength, label, rowFormat12);
			currentRow++;
		}
		//2 row -------------------------------------------------------
		{
			WritableCellFormat rowFormat12 = _format.setFont(times12b)
					.setOrientation(Orientation.HORIZONTAL)
					.setAlignment(Alignment.CENTRE)
					.setVerticalAlignment(VerticalAlignment.BOTTOM)
                    .setBordered(false)
                    .getCellFormat();

			String[] labelRow = {"учебный год", getEduYear()};
			Integer[] labelLen = {/*stat*/2, 2};

			fillRow(rowLength - 5, currentRow, sheet, labelRow, labelLen, null, rowFormat12);
			currentRow++;
		}
		//3 row -------------------------------------------------------
		{
			WritableCellFormat rowFormat12 = _format.setFont(times12b)
					.setOrientation(Orientation.HORIZONTAL)
					.setAlignment(Alignment.CENTRE)
					.setVerticalAlignment(VerticalAlignment.BOTTOM)
                    .setBordered(false)
                    .getCellFormat();

			String[] labelRow = new String[]{"Школа", getSchoolTitle(_orgUnit), "Кафедра", getCathedraTitle(_orgUnit)};
			Integer[] labelLen = new Integer[]{/*stat*/1, 3, 2, 4};
			fillRow(currentRow, sheet, labelRow, labelLen, rowFormat12);
			currentRow++;
		}
		//4 row -------------------------------------------------------
		currentRow++;
		{
			//5 row -------------------------------------------------------
			WritableCellFormat rowFormat12 = _format.setFont(times12b)
					.setOrientation(Orientation.HORIZONTAL)
					.setAlignment(Alignment.CENTRE)
					.setVerticalAlignment(VerticalAlignment.BOTTOM)
					.setBordered(true)
					.getCellFormat();

			String[] labelRow = new String[]{"Учебный цикл дисциплин", "Объем аудиторной учебной работы по кафедре (в часах)"};
			Integer[] labelLen = new Integer[]{1, rowLength - 1};
			fillRow(currentRow, sheet, labelRow, labelLen, rowFormat12);
			currentRow++;
			//6 row -------------------------------------------------------

			labelRow = new String[]{"очная форма обучения", "заочная форма обучения", "очно-заочная форма обучения"};
			labelLen = new Integer[]{3, 3, 3};
			fillRow(1, currentRow, sheet, labelRow, labelLen, null, rowFormat12);
			currentRow++;
			//7 row -------------------------------------------------------
			rowFormat12 = new WritableCellFormat(rowFormat12);
			labelRow = new String[]{"по кафедре", "межшкольная нагрузка", "почасовой фонд"};
			fillRow(1, currentRow, sheet, labelRow, rowFormat12);
			fillRow(4, currentRow, sheet, labelRow, rowFormat12);
			fillRow(7, currentRow, sheet, labelRow, rowFormat12);
			currentRow++;
			//-------------------------------------------------------
			sheet.mergeCells(0, currentRow - 3, 0, currentRow - 1);
		}

		//-------------------------------------------------------
		{
			//получить все ppsLoadCathedraPart верхнего уровня
			List<PpsLoadCathedraPart> cathedraPartList = getTopCathedraPartList();
			int startColumn = 0;
			int loadLen = rowLength - 1;
			Double[] totalRow = getZeroMassive(loadLen);
			for (PpsLoadCathedraPart cathedraPart : cathedraPartList)
			{
				//get all child nodes
				WritableCellFormat rowFormat12 = _format.setFont(times12b)
						.setOrientation(Orientation.HORIZONTAL)
						.setAlignment(Alignment.CENTRE)
						.setVerticalAlignment(VerticalAlignment.BOTTOM)
						.getCellFormat();

				sheet.addCell(new Label(startColumn, currentRow, cathedraPart.getShortTitle(), rowFormat12));
				int localRow = currentRow;
				currentRow++;
				List<PpsLoadCathedraPart> childCathedraPartList = getChildCathedraPartList(cathedraPart.getId());

				if (childCathedraPartList.isEmpty())
					childCathedraPartList = Collections.singletonList(cathedraPart);

				Double[] totalLoadRow = getZeroMassive(loadLen);

				for (PpsLoadCathedraPart childCathedraPart : childCathedraPartList)
				{
					Double[] loadRow = getZeroMassive(loadLen);

					int pos = 0;
					for (SheetType sheetType : SheetType.values())
					{
						getCathedraPartLoad(sheetType, childCathedraPart, loadRow, pos);
						pos++;
					}

					getCathedraPartLoad(HourType.HOUR, childCathedraPart, loadRow, pos);

					if (childCathedraPartList.size() > 1)
					{

						sheet.addCell(new Label(startColumn, currentRow, childCathedraPart.getShortTitle(), rowFormat12));
						fillRow(startColumn + 1, currentRow, sheet, getStringMass(loadRow), rowFormat12);
						currentRow++;

						mergeMassive(totalLoadRow, loadRow);
					}
					else
					{
						totalLoadRow = loadRow;
						break;
					}
				}
				fillRow(startColumn + 1, localRow, sheet, getStringMass(totalLoadRow), rowFormat12);
				mergeMassive(totalRow, totalLoadRow);
			}

			WritableCellFormat rowFormat12 = _format.setFont(times12b)
					.setOrientation(Orientation.HORIZONTAL)
					.setAlignment(Alignment.CENTRE)
					.setVerticalAlignment(VerticalAlignment.BOTTOM)
					.getCellFormat();

			sheet.addCell(new Label(startColumn, currentRow, "Итого", rowFormat12));
			fillRow(startColumn + 1, currentRow, sheet, getStringMass(totalRow), rowFormat12);
		}
	}

	//METHODS SHEET_9 "штат"
	private void createStateSheet(WritableSheet sheet) throws WriteException
	{
		int currentRow = 0;
		final int rowLength = 10; //действительное количество колонок
		//-------------------------------------------------------
		Map<Integer, Integer> columnWidth = new HashMap<>();
		columnWidth.put(0, 1);
		columnWidth.put(1, 9);
		setColumnWidth(sheet, columnWidth, 5, COLUMN_COEFFICIENT, rowLength);
		{
			// 1 row--------------------------------------------------------
			sheet.setRowView(currentRow, 11 * ROW_COEFFICIENT);
			sheet.mergeCells(0, currentRow, rowLength - 1, currentRow);
			try
			{
				byte[] template = LoadManager.instance().getLogo();
				sheet.addImage(new WritableImage(4.8, 0.1, 0.4, 0.8, template));
			}
			catch (IOException e)
			{
				throw CoreExceptionUtils.getRuntimeException(e);
			}
			currentRow++;
		}
		{
			//2 row -------------------------------------------------------
			WritableCellFormat rowFormat12 = _format.setFont(times12b)
					.setOrientation(Orientation.HORIZONTAL)
					.setAlignment(Alignment.CENTRE)
					.setVerticalAlignment(VerticalAlignment.BOTTOM)
					.setBordered(false)
					.getCellFormat();

			String label = "МИНИСТЕРСТВО ОБРАЗОВАНИЯ И НАУКИ РОССИЙСКОЙ ФЕДЕРАЦИИ";
			fillRow(currentRow, sheet, rowLength, label, rowFormat12);
			currentRow++;
			//3 row -------------------------------------------------------
			label = "Федеральное государственное автономное образовательное учреждение";
			fillRow(currentRow, sheet, rowLength, label, rowFormat12);
			currentRow++;
			//4 row -------------------------------------------------------
			label = "высшего профессионального образования";
			fillRow(currentRow, sheet, rowLength, label, rowFormat12);
			currentRow++;
			//5 row -------------------------------------------------------
			label = "«Дальневосточный федеральный университет»";
			fillRow(currentRow, sheet, rowLength, label, rowFormat12);
			currentRow++;
			//6 row -------------------------------------------------------
			label = "(ДВФУ)";
			fillRow(currentRow, sheet, rowLength, label, rowFormat12);
			currentRow++;
		}
		//7 - 11 row -------------------------------------------------------
		{
			WritableCellFormat rowFormat12 = _format.setAlignment(Alignment.LEFT).getCellFormat();

			int strLen = 3;
			String label = "«УТВЕРЖДАЮ»";
			fillRow(rowLength - strLen, currentRow, sheet, strLen, label, rowFormat12);
			currentRow++;

			label = "Проректор по учебной и";
			fillRow(rowLength - strLen, currentRow, sheet, strLen, label, rowFormat12);
			currentRow++;

			label = "воспитательной работе ДВФУ";
			fillRow(rowLength - strLen, currentRow, sheet, strLen, label, rowFormat12);
			currentRow++;

			label = "_________________И.В. Соппа";
			fillRow(rowLength - strLen, currentRow, sheet, strLen, label, rowFormat12);
			currentRow++;

			label = "«_____» ______________2014 г.";
			fillRow(rowLength - strLen, currentRow, sheet, strLen, label, rowFormat12);
			currentRow++;
		}
		//12 row -------------------------------------------------------
		currentRow++;
		//13 row -------------------------------------------------------
		{
			WritableCellFormat rowFormat12 = _format.setAlignment(Alignment.CENTRE).getCellFormat();

			String label = "Школа " + getSchoolTitle(_orgUnit);
			fillRow(currentRow, sheet, rowLength, label, rowFormat12);
			currentRow++;
			//14 row -------------------------------------------------------
			label = "Кафедра " + getCathedraTitle(_orgUnit);
			fillRow(currentRow, sheet, rowLength, label, rowFormat12);
			currentRow++;
			//15 row -------------------------------------------------------
			label = "СВОДНЫЙ РАСЧЕТ ПЛАНИРУЕМОГО ОБЪЕМА УЧЕБНОЙ РАБОТЫ ШТАТНЫХ ПРЕПОДАВАТЕЛЕЙ И СОВМЕСТИТЕЛЕЙ НА 2014-2015 УЧЕБНЫЙ ГОД";
			fillRow(currentRow, sheet, rowLength, label, rowFormat12);
			currentRow++;
		}
		//16 row -------------------------------------------------------
		currentRow++;
		//-------------------------------------------------------
		{
			//header
			WritableCellFormat rowFormat12 = _format.setBordered(true).getCellFormat();
//			addBorders(rowFormat12);
			String[] labelRow = new String[]{"№", "ФИО", "ученая степень", "ученое звание", "должность", "ставка", "часы", "Должность", "Итого ставок", "Итого часов"};
			fillRow(currentRow, sheet, labelRow, rowFormat12);
			currentRow++;
		}
		//main cycle-------------------------------------------------------
		{
			Double totalRate = 0D;
			Double stateRate = 0D;
			Double totalHours = 0D;
			Integer totalPps = 0;

			WritableCellFormat rowFormat12 = _format.setVerticalAlignment(VerticalAlignment.CENTRE).getCellFormat();

			Multimap</*post title*/String, ConsolidatedDocumentPpsRowWrapper> state = HashMultimap.create();
			Multimap</*post title*/String, ConsolidatedDocumentPpsRowWrapper> freelance = HashMultimap.create();
			getDocument().getWrappedPpsRows(state, freelance);

			totalPps += state.size() + freelance.size();
			//"Штатные преподаватели";"Совместители";
			ReturnValue rv = createPpsBlock(currentRow, sheet, rowLength, state, rowFormat12, "Штатные преподаватели", "Итого по штату");
			currentRow = rv.getRowNumber();
			stateRate += rv.getTotalRate();
			totalHours += rv.getTotalHour();

			rv = createPpsBlock(currentRow, sheet, rowLength, freelance, rowFormat12, "Совместители", "Итого по совместителям");
			currentRow = rv.getRowNumber();
			totalRate += rv.getTotalRate() + stateRate;
			totalHours += rv.getTotalHour();
			//-------------------------------------------------------
			currentRow++;

			String label = getEduYear() + " учебный год";
			fillRow(3, currentRow, sheet, 5, label, rowFormat12);
			currentRow++;

			String[] labelRow = new String[]{"Объем учебной работы по кафедре(в часах)", toString(totalHours)};
			Integer[] labelLen = new Integer[]{4, 1};
			fillRow(3, currentRow, sheet, labelRow, labelLen, null, rowFormat12);
			currentRow++;

			labelRow = new String[]{"Средний объем учебной работы преподавателя по кафедре  (в часах)", totalPps == 0 ? "N/A" : toString(totalHours / totalPps)};
			labelLen = new Integer[]{4, 1};
			fillRow(3, currentRow, sheet, labelRow, labelLen, null, rowFormat12);
			currentRow++;

			labelRow = new String[]{"Количество ставок(штатные преподаватели и совместители)", toString(totalRate)};
			labelLen = new Integer[]{4, 1};
			fillRow(3, currentRow, sheet, labelRow, labelLen, null, rowFormat12);
			currentRow++;

			labelRow = new String[]{"в том числе занятых штатными преподавателями", toString(stateRate)};
			labelLen = new Integer[]{4, 1};
			fillRow(3, currentRow, sheet, labelRow, labelLen, null, rowFormat12);
			currentRow += 2;
		}
		//-------------------------------------------------------
		{
			WritableCellFormat rowFormat12 = _format.setFont(times12b)
					.setAlignment(Alignment.LEFT)
					.setVerticalAlignment(VerticalAlignment.BOTTOM)
					.setBordered(false)
					.getCellFormat();

			String[] labelRow = new String[]{"Начальник отдела образовательных программ ДКУР", null, null, "Е.В. Жилина"};
			Integer[] labelLen = new Integer[]{4};
			fillRow(1, currentRow, sheet, labelRow, labelLen, null, rowFormat12);
			currentRow += 2;

			labelRow = new String[]{"Директор школы (либо лицо, уполномоченное им)", null, null, "И.О. Фамилия"};
			labelLen = new Integer[]{4};
			fillRow(1, currentRow, sheet, labelRow, labelLen, null, rowFormat12);
			currentRow += 2;

			labelRow = new String[]{"Начальник УМУ Школы", null, null, "И.О. Фамилия"};
			labelLen = new Integer[]{4};
			fillRow(1, currentRow, sheet, labelRow, labelLen, null, rowFormat12);
			currentRow += 2;

			labelRow = new String[]{"Заведующий кафедрой", null, null, "И.О. Фамилия"};
			labelLen = new Integer[]{4};
			fillRow(1, currentRow, sheet, labelRow, labelLen, null, rowFormat12);
			currentRow += 2;
			//-------------------------------------------------------
			labelRow = new String[]{"\"_______\"_________________________ 2014 г."};
			labelLen = new Integer[]{3};
			fillRow(1, currentRow, sheet, labelRow, labelLen, null, rowFormat12);
		}
		//-------------------------------------------------------
	}

	private ReturnValue createPpsBlock(int currentRow, final WritableSheet sheet, int rowLength, Multimap/*post title*/<String, ConsolidatedDocumentPpsRowWrapper> state, WritableCellFormat rowFormat12, String label, String label2) throws WriteException
	{
		int firstPart = 7;
		int secondPart = rowLength - firstPart;

		Double totalRate = 0D;
		Double totalHours = 0D;

		fillRow(currentRow, sheet, rowLength, label, rowFormat12);
		currentRow++;

		String[] rowPart = new String[secondPart];
		for (String post : state.keySet())
		{
			Double postTotalRate = 0D;
			Double postTotalHours = 0D;
			int localRow = currentRow;
			{
				List<ConsolidatedDocumentPpsRowWrapper> wrappedPpsRows = new ArrayList<>();
				wrappedPpsRows.addAll(state.get(post));
				Collections.sort(wrappedPpsRows, (o1, o2) -> CommonCollator.RUSSIAN_COLLATOR.compare(o1.getPpsFio(), o2.getPpsFio()));

				int counter = 0;
				for (ConsolidatedDocumentPpsRowWrapper ppsRowWrapper : wrappedPpsRows)
				{
					postTotalRate += ppsRowWrapper.getRateValue();
					postTotalHours += ppsRowWrapper.getHoursValue();

					String[] row = createPpsRow(++counter, ppsRowWrapper, firstPart);
					fillRow(currentRow, sheet, row, rowFormat12);
					currentRow++;
				}
			}
			//total by post
			rowPart[0] = post;
			rowPart[1] = postTotalRate.toString();
			rowPart[2] = postTotalHours.toString();
			fillRow(firstPart, localRow, sheet, rowPart, rowFormat12);

			for (int i = 0; i < 3; i++)
			{
				sheet.mergeCells(firstPart + i, localRow, firstPart + i, currentRow - 1);
			}

			totalRate += postTotalRate;
			totalHours += postTotalHours;
		}
		//total
		rowPart[0] = label2;
		rowPart[1] = totalRate.toString();
		rowPart[2] = totalHours.toString();
		fillRow(currentRow, sheet, rowPart, new Integer[]{firstPart + 1}, rowFormat12);
		currentRow++;
		return new ReturnValue(totalRate, totalHours, currentRow);
	}

	private Double[] getZeroMassive(int len)
	{
		Double[] mas = new Double[len];
		for (int i = 0; i < mas.length; i++)
		{
			mas[i] = 0D;
		}
		return mas;
	}

	private void mergeMassive(Double[] dstn, Double[] src)
	{
		for (int i = 0; i < dstn.length; i++)
		{
			dstn[i] = dstn[i] + src[i];
		}
	}

	private String[] getStringMass(Double[] mas)
	{
		String[] res = new String[mas.length];
		for (int i = 0; i < mas.length; i++)
		{
			res[i] = mas[i].toString();
		}
		return res;
	}

	private void getCathedraPartLoad(ISheetType sheetType, PpsLoadCathedraPart childPart, Double[] loadRow, int pos)
	{
		for (Term term : Term.values())
		{
			int localPos = pos;
			ConsolidatedDocumentSheetWrapper sheetWrapper;
			if (sheetType instanceof SheetType)
				sheetWrapper = getDocument().getSheet((SheetType) sheetType, term);
			else
				sheetWrapper = getDocument().getSheet((HourType) sheetType, term);

			for (String eduFormCode : EduProgramFormCodes.CODES)
			{
				Double totalLoad = sheetWrapper.getBlock(getEduProgramForm(eduFormCode)).getBlockTotalLoad(childPart);
				if (totalLoad == null) totalLoad = 0D;
				loadRow[localPos] = loadRow[localPos] + totalLoad;
				localPos += 3;
			}
		}
	}

	private String[] createPpsRow(int rowNumber, ConsolidatedDocumentPpsRowWrapper ppsRowWrapper, int rowLength)
	{
		String[] row = new String[rowLength];
		int pos = 0;
		row[pos++] = String.valueOf(rowNumber);
		row[pos++] = ppsRowWrapper.getPpsFio();
		row[pos++] = ppsRowWrapper.getScienceDegreeType();
		row[pos++] = ppsRowWrapper.getScienceStatusType();
		row[pos++] = ppsRowWrapper.getPost();
		row[pos++] = ppsRowWrapper.getRate();
		row[pos] = ppsRowWrapper.getHours();
		return row;
	}

	private Map<String, Double> createEduFormBlock(
			@NotNull SheetVO sheetVO,
			@NotNull ConsolidatedDocumentSheetWrapper.ConsolidatedDocumentSuperBlockWrapper documentBlock,
			@NotNull Map<Integer, String> timeRulePositionMap
	) throws WriteException
	{
		final int rowLength = sheetVO.getRowLength();
		final int statLength = sheetVO.getStatLength();
		final int varLength = sheetVO.getVarLength();
		final WritableSheet sheet = sheetVO.getSheet();

		Map<String, Double> totalBlockLoadMap = new HashMap<>();

		//получить все ppsLoadCathedraPart верхнего уровня
		List<PpsLoadCathedraPart> cathedraPartList = getTopCathedraPartList();

		for (PpsLoadCathedraPart cathedraPart : cathedraPartList)
		{
			Map<String, Double> totalSubBlockLoadMap = new HashMap<>();
			//get all child nodes
			WritableCellFormat rowFormat09 = _format.setFont(times09b)
					.setAlignment(Alignment.CENTRE)
					.setVerticalAlignment(VerticalAlignment.BOTTOM)
					.setBordered(true)
					.getCellFormat();

			fillRow(sheetVO.getCurrentRow(), sheet, rowLength, cathedraPart.getTitle().toUpperCase(), rowFormat09);
			sheetVO.incCurrentRow();

			List<PpsLoadCathedraPart> childCathedraPartList = getChildCathedraPartList(cathedraPart.getId());
			if (childCathedraPartList.isEmpty())
				childCathedraPartList = Collections.singletonList(cathedraPart);
			for (PpsLoadCathedraPart childCathedraPart : childCathedraPartList)
			{
				Map<String, Double> totalRowsLoadMap = new HashMap<>();
				if (childCathedraPartList.size() > 1)
				{
					fillRow(sheetVO.getCurrentRow(), sheet, rowLength, childCathedraPart.getTitle().toUpperCase(), rowFormat09);
					sheetVO.incCurrentRow();
				}
				List<ConsolidatedDocumentRowWrapper> blockRows = documentBlock.getBlockRows(childCathedraPart);
				//fill  block rows
				{
					WritableCellFormat rowFormat = _format.getCellFormat();
					Integer counter = 0;
					for (ConsolidatedDocumentRowWrapper rowWrapper : blockRows)
					{
						Map<String, Double> rowLoadMap = rowWrapper.getRowLoadMap();
						String[] row = createRow(sheetVO, ++counter, rowWrapper);
						fillRow(sheetVO.getCurrentRow(), sheet, row, rowFormat);

						fillLoadRowPart(sheetVO.getCurrentRow(), statLength, sheet, varLength + 1, rowFormat09, rowLoadMap, timeRulePositionMap);
						sheetVO.incCurrentRow();

						mergeMaps(totalRowsLoadMap, rowLoadMap);
					}

				}
				if (childCathedraPartList.size() > 1)
				{
					rowFormat09 = _format.setAlignment(Alignment.RIGHT).getCellFormat();
					fillRow(sheetVO.getCurrentRow(), sheet, statLength, "Итого по " + childCathedraPart.getShortTitle(), rowFormat09);

					rowFormat09 = _format.setAlignment(Alignment.CENTRE).getCellFormat();
					fillLoadRowPart(sheetVO.getCurrentRow(), statLength, sheet, varLength + 1, rowFormat09, totalRowsLoadMap, timeRulePositionMap);
					sheetVO.incCurrentRow();
				}
				mergeMaps(totalSubBlockLoadMap, totalRowsLoadMap);

			}

			rowFormat09 = _format.setAlignment(Alignment.RIGHT).getCellFormat();
			fillRow(sheetVO.getCurrentRow(), sheet, statLength, "Итого по " + cathedraPart.getShortTitle(), rowFormat09);

			rowFormat09 = _format.setAlignment(Alignment.CENTRE).getCellFormat();
			fillLoadRowPart(sheetVO.getCurrentRow(), statLength, sheet, varLength + 1, rowFormat09, totalSubBlockLoadMap, timeRulePositionMap);
			sheetVO.incCurrentRow();
			mergeMaps(totalBlockLoadMap, totalSubBlockLoadMap);
		}
        {
            //Блок "Аспирантура" всегда постояный
            WritableCellFormat rowFormat09 = _format.setAlignment(Alignment.CENTRE).getCellFormat();
            fillRow(sheetVO.getCurrentRow(), sheet, rowLength, "Аспирантура", rowFormat09);
            sheetVO.incCurrentRow();

            for (ConsolidatedDocumentPostgraduateRowWrapper row : documentBlock.getPostgraduateBlockRows())
            {
                rowFormat09 = _format.setAlignment(Alignment.RIGHT).getCellFormat();
                fillRow(sheetVO.getCurrentRow(), sheet, statLength, row.getTitle(), rowFormat09);

                rowFormat09 = _format.setAlignment(Alignment.CENTRE).getCellFormat();
                fillLoadRowPart(sheetVO.getCurrentRow(), statLength, sheet, varLength + 1, rowFormat09, row.getRowLoadMap(), timeRulePositionMap);
                sheetVO.incCurrentRow();
            }
            final Map<String, Double> blockLoad = documentBlock.getPostgraduateBlockLoad();
            mergeMaps(totalBlockLoadMap, blockLoad);
            rowFormat09 = _format.setAlignment(Alignment.RIGHT).getCellFormat();
            fillRow(sheetVO.getCurrentRow(), sheet, statLength, "Итого по Аспирантуре", rowFormat09);

            rowFormat09 = _format.setAlignment(Alignment.CENTRE).getCellFormat();
            fillLoadRowPart(sheetVO.getCurrentRow(), statLength, sheet, varLength + 1, rowFormat09, blockLoad, timeRulePositionMap);
            sheetVO.incCurrentRow();
        }
		return totalBlockLoadMap;
	}

	private String[] createRow(SheetVO sheetVO, Integer counter, ConsolidatedDocumentRowWrapper rowWrapper)
	{
		final int rowLength = sheetVO.getRowLength();
		final int statLength = sheetVO.getStatLength();
		final int varLength = sheetVO.getVarLength();

		String[] row = new String[rowLength];
		//Copy-paste for Copy-paste god
		int pos = 0;
		row[pos++] = counter.toString();
		row[pos++] = rowWrapper.getDisciplineTitle();
		row[pos++] = rowWrapper.getGroupType();
		row[pos++] = rowWrapper.getTaskNumber();
		row[pos++] = rowWrapper.getGos();
		row[pos++] = rowWrapper.getSpecializeCode();
		row[pos++] = rowWrapper.getSpecializeTitle();

		if (isLongForm(sheetVO))
		{
			row[pos++] = rowWrapper.getSchoolTitle();
		}
		row[pos++] = rowWrapper.getCourse();
		row[pos++] = rowWrapper.getStudentNumber();
		row[pos++] = rowWrapper.getGroupTitle();
		row[pos] = rowWrapper.getGroupNumber();

		pos = statLength + varLength;
		row[pos++] = rowWrapper.getTotalRowLoad().toString();
		row[pos++] = rowWrapper.getPpsFio();
		row[pos] = rowWrapper.getPpsEmployeeStatus();

		if (isLongForm(sheetVO))
			row[++pos] = rowWrapper.getSchoolRequest();

		return row;
	}

	private void createEduGroupBlock(@NotNull SheetVO sheetVO, GroupData groupData, Term term, Map<Integer, String> timeRulePositionMap) throws WriteException
	{

		final int rowLength = sheetVO.getRowLength();
		final int statLength = sheetVO.getStatLength();
		final int varLength = sheetVO.getVarLength();
		final WritableSheet sheet = sheetVO.getSheet();

		Multimap<String, LoadTimeRule> loadGroup2TimeRuleCode = getLoadGroup2TimeRuleMultimap();
		String[] row = getEmptyString(rowLength);
		for (String code : PpsLoadGroupCodes.CODES)
		{

			WritableCellFormat rowFormat09 = _format.setFont(times09b)
					.setAlignment(Alignment.CENTRE)
					.setVerticalAlignment(VerticalAlignment.BOTTOM)
					.setBordered(true)
					.getCellFormat();

			String groupTitle = groupData.getGroupTitle(code);
			fillRow(sheetVO.getCurrentRow(), sheet, rowLength, groupTitle.toUpperCase(), rowFormat09);
			sheetVO.incCurrentRow();

			for (LoadTimeRule timeRule : loadGroup2TimeRuleCode.get(code))
			{
				rowFormat09 = _format.setAlignment(Alignment.RIGHT).getCellFormat();
				row[0] = timeRule.getTitle();
				fillRow(sheetVO.getCurrentRow(), sheet, row, rowFormat09);
				sheet.mergeCells(0, sheetVO.getCurrentRow(), 1, sheetVO.getCurrentRow());
				sheetVO.incCurrentRow();
			}

			rowFormat09 = _format.setAlignment(Alignment.RIGHT).getCellFormat();
			fillRow(sheetVO.getCurrentRow(), sheet, statLength, "Итого " + groupTitle.toLowerCase() + " за " + term.getTitle() + " семестр (по всем формам обучения)", rowFormat09);
			fillLoadRowPart(sheetVO.getCurrentRow(), statLength, sheet, varLength + 1, rowFormat09, Collections.<String, Double>emptyMap(), timeRulePositionMap);
			sheetVO.incCurrentRow();
		}
	}

	public Multimap<String, LoadTimeRule> getLoadGroup2TimeRuleMultimap()
	{
		List<PpsLoadGroup2TimeRule> loadType2TimeRules = getDao().getList(PpsLoadGroup2TimeRule.class);
		Multimap<String, LoadTimeRule> result = HashMultimap.create();
		for (PpsLoadGroup2TimeRule item : loadType2TimeRules)
		{
			result.put(item.getPpsLoadGroup().getCode(), item.getTimeRule());
		}
		return result;
	}

	private EduProgramForm getEduProgramForm(String code)
	{
		return getDao().getNotNull(EduProgramForm.class, EduProgramForm.code(), code);
	}

	private List<PpsLoadCathedraPart> getTopCathedraPartList()
	{
		String lcp = "lcp";
		DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(PpsLoadCathedraPart.class, lcp)
				.column(DQLExpressions.property(lcp))
				.where(DQLExpressions.isNull(DQLExpressions.property(lcp, PpsLoadCathedraPart.L_PARENT)))
				.order(DQLExpressions.property(lcp, PpsLoadCathedraPart.code()))
				.distinct();
		return LoadManager.instance().dao().getList(builder);
	}

	private List<PpsLoadCathedraPart> getChildCathedraPartList(Long cathedraPartId)
	{
		String lcp = "lcp";
		DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(PpsLoadCathedraPart.class, lcp)
				.column(DQLExpressions.property(lcp))
				.where(DQLExpressions.eq(DQLExpressions.property(lcp, PpsLoadCathedraPart.parent().id()), DQLExpressions.value(cathedraPartId)))
				.order(DQLExpressions.property(lcp, PpsLoadCathedraPart.P_CODE))
				.distinct();
		return LoadManager.instance().dao().getList(builder);
	}

	String toString(Double value)
	{
		return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(value);
	}

	//GETTERS/SETTERS
	public String getCathedraTitle(OrgUnit baseOrgUnit)
	{
		if (isCathedra(baseOrgUnit))
			return baseOrgUnit.getTitle();
		else
			return "-";
	}

	private boolean isCathedra(OrgUnit orgUnit)
	{
		return orgUnit.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.CATHEDRA);
	}

	private String changeCase(EduProgramForm eduForm)
	{
		return eduForm.getTitle().toLowerCase().replace("ая", "ое");
	}

	private String getEduYear()
	{
		return _pupnag.getEducationYear().getTitle();
	}

	private int getVarLength()
	{
		return _groupData.getSum();
	}

	private Long getOrgUnitId()
	{
		return _orgUnit.getId();
	}

	private ILoadDAO getDao()
	{
		return _dao;
	}

	public ConsolidatedDocumentWrapper getDocument()
	{
		return _dataWrapper;
	}

    private boolean isLongForm(SheetVO vo)
    {
        return vo.getStatLength() > 11;
    }

	//HELPER CLASSES
	public enum SheetType implements ISheetType
	{
		CATHEDRA("кафедра", "кафедре"),
		OTHER("межфак", "межфаку");

		private final String title;
		private final String printTitle;

		SheetType(String title, String printTitle)
		{
			this.title = title;
			this.printTitle = printTitle;
		}

		@Override
		public String getTitle()
		{
			return "_" + this.title;
		}

		@Override
		public boolean isSheetType()
		{
			return true;
		}

		@Override
		public String getPrintTitle()
		{
			return this.printTitle;
		}

	}

	public enum HourType implements ISheetType
	{
		HOUR("почасовая", "часовому фонду"),
		NON_HOUR("не почасовая", "вне часовому фонду");

		private final String title;
		private final String printTitle;

		HourType(String title, String printTitle)
		{
			this.title = title;
			this.printTitle = printTitle;
		}

		@Override
		public String getTitle()
		{
			return "_" + this.title;
		}

		@Override
		public boolean isSheetType()
		{
			return false;
		}

		@Override
		public String getPrintTitle()
		{
			return this.printTitle;
		}

	}

	private interface ISheetType
	{
		boolean isSheetType();

		String getTitle();

		String getPrintTitle();
	}

	private class SheetVO
	{
		private final int statLength; //количество колонок в неизменяемой части
		private final int varLength; //количество колонок в изменяемой части
		private final int rowLength; //действительное количество колонок
		private final int endLength;
		private final WritableSheet sheet;
		private int currentRow = 0;

		public SheetVO(int minRowLength, int statLength, int varLength, int endStatLength, WritableSheet sheet)
		{
			this.statLength = statLength;
			this.varLength = varLength;
			this.endLength = endStatLength;
			this.rowLength = Math.max(this.statLength + this.varLength + this.endLength, minRowLength); //действительное количество колонок
			this.sheet = sheet;
		}

		public int getStatLength()
		{
			return statLength;
		}

		public int getVarLength()
		{
			return varLength;
		}

		public int getRowLength()
		{
			return rowLength;
		}

		public WritableSheet getSheet()
		{
			return sheet;
		}

		public int getCurrentRow()
		{
			return currentRow;
		}

		public void incCurrentRow()
		{
			this.currentRow++;
		}

		public int getEndLength()
		{
			return endLength;
		}
	}

	private class ReturnValue
	{
		private final Double totalRate;
		private final Double totalHour;
		private final int rowNumber;

		public ReturnValue(Double totalRate, Double totalHour, int rowNumber)
		{
			this.totalRate = totalRate;
			this.totalHour = totalHour;
			this.rowNumber = rowNumber;
		}

		public Double getTotalRate()
		{
			return totalRate;
		}

		public Double getTotalHour()
		{
			return totalHour;
		}

		public int getRowNumber()
		{
			return rowNumber;
		}
	}
}
