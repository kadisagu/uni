package ru.tandemservice.uniload_demo.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniload_demo_2x6x6_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.6")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность loadEduGroupPpsDistr

		// создано обязательное свойство eduGroupTitle
		{
			// создать колонку
			tool.createColumn("loadedugroupppsdistr_t", new DBColumn("edugrouptitle_p", DBType.createVarchar(255)));

			// задать значение по умолчанию
			tool.getStatement().execute("UPDATE loadedugroupppsdistr_t SET edugrouptitle_p = (SELECT eg.title_p FROM iepprealedugroup_v eg where loadedugroupppsdistr_t.edugroup_id = eg.id)");

			// сделать колонку NOT NULL
			tool.setColumnNullable("loadedugroupppsdistr_t", "edugrouptitle_p", false);

		}


    }
}