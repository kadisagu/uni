/*$Id$*/
package ru.tandemservice.uniload_demo.base.bo.Load.ui.TimeRule2EduGroupTypeRel;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeALT;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeFCA;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniload_demo.entity.ImEduGroupLoadBase;
import ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule;

import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 01.09.2014
 */
@Configuration
public class LoadTimeRule2EduGroupTypeRel extends BusinessComponentManager
{
	public static final String EPP_LOAD_TYPE_DS = "eppLoadTypeDS";
	public static final String EPP_CONTROL_TYPE_DS = "eppControlTypeDS";
	public static final String PUPNAG_DS = "pupnagDS";

	public static final String LOAD_TYPE_TIME_RULE_DS = "loadTypeTimeRuleDS";
	public static final String CONTROL_TYPE_TIME_RULE_DS = "controlTypeTimeRuleDS";

	public static final String USED_TIME_RULE_LIST = "usedLoadTimeRuleList";
	public static final String LOAD_BASE_EXCLUDE = "loadBaseExclude";
	public static final String ADDED_TIME_RULE_LIST= "addedTimeRules";

	@Override
	@Bean
	public PresenterExtPoint presenterExtPoint()
	{
		return presenterExtPointBuilder()
				.addDataSource(selectDS(PUPNAG_DS, pupnagDSHandler()))
				.addDataSource(searchListDS(EPP_LOAD_TYPE_DS, eppLoadTypeDSColumns(), eppLoadTypeDSHandler()))
				.addDataSource(searchListDS(EPP_CONTROL_TYPE_DS, eppControlTypeDSColumns(), eppControlTypeDSHandler()))
				.addDataSource(selectDS(LOAD_TYPE_TIME_RULE_DS, loadTimeRuleDSHandler()))
				.addDataSource(selectDS(CONTROL_TYPE_TIME_RULE_DS, loadTimeRuleDSHandler()))
				.create();
	}


	@Bean
	public ColumnListExtPoint eppLoadTypeDSColumns()
	{
		return columnListExtPointBuilder(EPP_LOAD_TYPE_DS)
			.addColumn(textColumn("title", EppGroupTypeALT.title()).width("200px"))
			.addColumn(blockColumn("time", "loadTypeColumn"))
			.create();
	}

	@Bean
	public ColumnListExtPoint eppControlTypeDSColumns()
	{
		return columnListExtPointBuilder(EPP_CONTROL_TYPE_DS)
			.addColumn(textColumn("title", EppGroupTypeFCA.title()).width("200px"))
			.addColumn(blockColumn("time", "controlTypeColumn"))
			.create();
	}

	@Bean
	public IDefaultSearchDataSourceHandler eppLoadTypeDSHandler()
	{
		return new EntityComboDataSourceHandler(getName(), EppGroupTypeALT.class).order(EppGroupTypeALT.priority());
	}

	@Bean
	public IDefaultSearchDataSourceHandler eppControlTypeDSHandler()
	{
		return new EntityComboDataSourceHandler(getName(), EppGroupTypeFCA.class).order(EppGroupTypeFCA.priority());
	}

	@Bean
	public IDefaultComboDataSourceHandler pupnagDSHandler()
	{
		return new EntityComboDataSourceHandler(getName(), EppYearEducationProcess.class)
				.order(EppYearEducationProcess.educationYear().intValue());
	}

	@Bean
	public IDefaultComboDataSourceHandler loadTimeRuleDSHandler()
	{
		return new EntityComboDataSourceHandler(getName(), LoadTimeRule.class)
		{
			@Override
			protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
			{
				super.applyWhereConditions(alias, dql, context);

				ImEduGroupLoadBase loadBase = context.get(LOAD_BASE_EXCLUDE);
				Collection<LoadTimeRule> usedLoadTimeRule = context.get(USED_TIME_RULE_LIST);
				Collection<LoadTimeRule> addedLoadTimeRule = context.get(ADDED_TIME_RULE_LIST);

				if(usedLoadTimeRule != null && !usedLoadTimeRule.isEmpty() && addedLoadTimeRule != null && !addedLoadTimeRule.isEmpty()) {
					dql.where(or(in(property(alias, LoadTimeRule.P_ID), addedLoadTimeRule), notIn(property(alias, LoadTimeRule.P_ID), usedLoadTimeRule)));
				}
				else
				{
					if(usedLoadTimeRule != null && !usedLoadTimeRule.isEmpty())
						dql.where(notIn(property(alias, LoadTimeRule.P_ID), usedLoadTimeRule));

					if(addedLoadTimeRule != null && !addedLoadTimeRule.isEmpty())
						dql.where(in(property(alias, LoadTimeRule.P_ID), addedLoadTimeRule));
				}

				dql.where(ne(property(alias, LoadTimeRule.P_BASE), value(loadBase)))
						.where(eq(property(alias, LoadTimeRule.P_IN_USE), value(Boolean.TRUE)));
			}
		}.filter(LoadTimeRule.title());
	}
}
