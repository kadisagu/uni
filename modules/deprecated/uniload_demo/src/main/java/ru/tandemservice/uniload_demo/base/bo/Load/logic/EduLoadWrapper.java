/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.base.bo.Load.logic;

import org.tandemframework.core.entity.ViewWrapper;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.uniload_demo.entity.ILoadImEduGroup;

import java.util.*;

/**
 * @author Alexander Zhebko
 * @since 20.12.2013
 */
public class EduLoadWrapper extends ViewWrapper<EppRealEduGroup>
{
	public static final String STUDENTS = "students";
	public static final String HOURS = "loadSize";
	public static final String LOAD = "result";
	public static final String DEVELOP_COMBINATION = "developCombination";
	public static final String TIME_RULE_TITLE_LIST = "timeRule";

	public static final Comparator<EduLoadWrapper> COMPARATOR = new Comparator<EduLoadWrapper>()
	{
		@Override
		public int compare(EduLoadWrapper o1, EduLoadWrapper o2)
		{
			int result = o1.getEntity().getTitle().compareTo(o2.getEntity().getTitle());
			if (result == 0)
				result = o1.getEntity().getActivityPart().getTitle().compareTo(o2.getEntity().getActivityPart().getTitle());
			if (result == 0)
				result = o1.getEntity().getType().getClass().getSimpleName().compareTo(o2.getEntity().getType().getClass().getSimpleName());
			return result;
		}
	};

	public EduLoadWrapper(EppRealEduGroup eduGroup, ILoadImEduGroup loadImEduGroup, Double load, Set<String> timeRuleTitleList)
	{
		this(eduGroup, loadImEduGroup.getStudents(), UniEppUtils.wrap(loadImEduGroup.getHours()), load, timeRuleTitleList);
	}

	private EduLoadWrapper(EppRealEduGroup eduGroup, Integer students, Double hours, Double load, Set<String> timeRuleTitleList)
	{
		super(eduGroup);
		setViewProperty(LOAD, load);
		List<String> timeRuleTitle = new ArrayList<>(timeRuleTitleList);
		Collections.sort(timeRuleTitle);
		setViewProperty(TIME_RULE_TITLE_LIST, timeRuleTitle);
		setViewProperty(HOURS, hours);
		setViewProperty(STUDENTS, students);

		EppWorkPlanRegistryElementRow row = eduGroup.getWorkPlanRow();
		if (row == null)
			setViewProperty(DEVELOP_COMBINATION, "");
		else
			setViewProperty(DEVELOP_COMBINATION, row.getWorkPlan().getEduPlan().getDevelopCombinationTitle());
	}


	public Double getLoadSize()
	{
		return (Double) this.getProperty(HOURS);
	}
}