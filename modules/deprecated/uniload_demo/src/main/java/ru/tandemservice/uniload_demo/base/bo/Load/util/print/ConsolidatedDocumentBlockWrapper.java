/*$Id$*/
package ru.tandemservice.uniload_demo.base.bo.Load.util.print;

import org.apache.commons.collections15.map.MultiKeyMap;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.uniload_demo.entity.ILoadImEduGroup;
import ru.tandemservice.uniload_demo.entity.LoadRecordOwner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author DMITRY KNYAZEV
 * @since 02.12.2014
 */
class ConsolidatedDocumentBlockWrapper
{
	private final MultiKeyMap<Long, ConsolidatedDocumentRowWrapper> _rowWrapperMap;

	public ConsolidatedDocumentBlockWrapper()
	{
		_rowWrapperMap = new MultiKeyMap<>();
	}

	public List<ConsolidatedDocumentRowWrapper> getRowWrappers()
	{
		//noinspection unchecked
		return new ArrayList<>(_rowWrapperMap.values());
	}

	public Map<String, Double> getBlockLoadMap()
	{
		Map<String, Double> loadMap = new HashMap<>();
		for (ConsolidatedDocumentRowWrapper rowWrapper : getRowWrappers())
		{
			LoadDocumentPrintSharedUtils.mergeMaps(loadMap, rowWrapper.getRowLoadMap());
		}
		return loadMap;
	}

	public void addRow(EppWorkPlanRegistryElementRow discipline, LoadRecordOwner person, ILoadImEduGroup imEduGroup)
	{
		ConsolidatedDocumentRowWrapper rowWrapper = _rowWrapperMap.get(discipline.getId(), person.getId());
		if (rowWrapper == null)
		{
			rowWrapper = new ConsolidatedDocumentRowWrapper(discipline, person);
			_rowWrapperMap.put(discipline.getId(), person.getId(), rowWrapper);
		}
		rowWrapper.add(imEduGroup);
	}
}
