/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.base.bo.Load.logic;

import org.tandemframework.core.entity.ViewWrapper;
import ru.tandemservice.uniload_demo.entity.LoadFirstCourseImEduGroup;

import java.util.*;

/**
 * @author Alexander Zhebko
 * @since 20.12.2013
 */
public class FirstCourseImEduGroupViewWrapper extends ViewWrapper<LoadFirstCourseImEduGroup>
{
	public static final String LOAD = "result";
	public static final String TIME_RULE_TITLE_LIST = "timeRule";
	public static final String DEVELOP_COMBINATION = "developCombination";

	public FirstCourseImEduGroupViewWrapper(LoadFirstCourseImEduGroup firstCourseImEduGroup, Double result, Set<String> timeRuleList)
	{
		super(firstCourseImEduGroup);
		setViewProperty(LOAD, result);
		List<String> timeRuleTitle = new ArrayList<>(timeRuleList);
		Collections.sort(timeRuleTitle);
		setViewProperty(TIME_RULE_TITLE_LIST, timeRuleTitle);
		setViewProperty(DEVELOP_COMBINATION, firstCourseImEduGroup.getRow().getWorkPlan().getEduPlan().getDevelopCombinationTitle());
	}

	public Double getLoadSize()
	{
		return (Double) this.getProperty(LOAD);
	}
}