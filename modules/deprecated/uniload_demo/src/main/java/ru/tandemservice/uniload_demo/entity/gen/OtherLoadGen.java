package ru.tandemservice.uniload_demo.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniload_demo.entity.ILoadImEduGroup;
import ru.tandemservice.uniload_demo.entity.LoadRecordOwner;
import ru.tandemservice.uniload_demo.entity.OtherLoad;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Другие виды нагрузки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OtherLoadGen extends EntityBase
 implements ILoadImEduGroup{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniload_demo.entity.OtherLoad";
    public static final String ENTITY_NAME = "otherLoad";
    public static final int VERSION_HASH = 830657210;
    private static IEntityMeta ENTITY_META;

    public static final String L_OWNER = "owner";
    public static final String P_TITLE = "title";
    public static final String P_HOURS = "hours";
    public static final String P_STUDENTS = "students";
    public static final String P_CONSIDER_IN_EDU_LOAD = "considerInEduLoad";
    public static final String L_POSTGRADUATE_PROGRAM_FORM = "postgraduateProgramForm";
    public static final String L_YEAR_DISTRIBUTION_PART = "yearDistributionPart";
    public static final String P_HOURS_AS_DOUBLE = "hoursAsDouble";

    private LoadRecordOwner _owner;     // ППС (нагрузка)
    private String _title;     // Название
    private long _hours;     // Часы
    private int _students = -1;     // Студенты
    private boolean _considerInEduLoad = false;     // Учитывать в учебной нагрузке
    private EduProgramForm _postgraduateProgramForm;     // Нагрузка аспирантуры (форма обучения)
    private YearDistributionPart _yearDistributionPart;     // Часть учебного года

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ППС (нагрузка). Свойство не может быть null.
     */
    @NotNull
    public LoadRecordOwner getOwner()
    {
        return _owner;
    }

    /**
     * @param owner ППС (нагрузка). Свойство не может быть null.
     */
    public void setOwner(LoadRecordOwner owner)
    {
        dirty(_owner, owner);
        _owner = owner;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Часы. Свойство не может быть null.
     */
    @NotNull
    public long getHours()
    {
        return _hours;
    }

    /**
     * @param hours Часы. Свойство не может быть null.
     */
    public void setHours(long hours)
    {
        dirty(_hours, hours);
        _hours = hours;
    }

    /**
     * @return Студенты. Свойство не может быть null.
     */
    @NotNull
    public int getStudents()
    {
        return _students;
    }

    /**
     * @param students Студенты. Свойство не может быть null.
     */
    public void setStudents(int students)
    {
        dirty(_students, students);
        _students = students;
    }

    /**
     * @return Учитывать в учебной нагрузке. Свойство не может быть null.
     */
    @NotNull
    public boolean isConsiderInEduLoad()
    {
        return _considerInEduLoad;
    }

    /**
     * @param considerInEduLoad Учитывать в учебной нагрузке. Свойство не может быть null.
     */
    public void setConsiderInEduLoad(boolean considerInEduLoad)
    {
        dirty(_considerInEduLoad, considerInEduLoad);
        _considerInEduLoad = considerInEduLoad;
    }

    /**
     * @return Нагрузка аспирантуры (форма обучения).
     */
    public EduProgramForm getPostgraduateProgramForm()
    {
        return _postgraduateProgramForm;
    }

    /**
     * @param postgraduateProgramForm Нагрузка аспирантуры (форма обучения).
     */
    public void setPostgraduateProgramForm(EduProgramForm postgraduateProgramForm)
    {
        dirty(_postgraduateProgramForm, postgraduateProgramForm);
        _postgraduateProgramForm = postgraduateProgramForm;
    }

    /**
     * @return Часть учебного года. Свойство не может быть null.
     */
    @NotNull
    public YearDistributionPart getYearDistributionPart()
    {
        return _yearDistributionPart;
    }

    /**
     * @param yearDistributionPart Часть учебного года. Свойство не может быть null.
     */
    public void setYearDistributionPart(YearDistributionPart yearDistributionPart)
    {
        dirty(_yearDistributionPart, yearDistributionPart);
        _yearDistributionPart = yearDistributionPart;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof OtherLoadGen)
        {
            setOwner(((OtherLoad)another).getOwner());
            setTitle(((OtherLoad)another).getTitle());
            setHours(((OtherLoad)another).getHours());
            setStudents(((OtherLoad)another).getStudents());
            setConsiderInEduLoad(((OtherLoad)another).isConsiderInEduLoad());
            setPostgraduateProgramForm(((OtherLoad)another).getPostgraduateProgramForm());
            setYearDistributionPart(((OtherLoad)another).getYearDistributionPart());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OtherLoadGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OtherLoad.class;
        }

        public T newInstance()
        {
            return (T) new OtherLoad();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "owner":
                    return obj.getOwner();
                case "title":
                    return obj.getTitle();
                case "hours":
                    return obj.getHours();
                case "students":
                    return obj.getStudents();
                case "considerInEduLoad":
                    return obj.isConsiderInEduLoad();
                case "postgraduateProgramForm":
                    return obj.getPostgraduateProgramForm();
                case "yearDistributionPart":
                    return obj.getYearDistributionPart();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "owner":
                    obj.setOwner((LoadRecordOwner) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "hours":
                    obj.setHours((Long) value);
                    return;
                case "students":
                    obj.setStudents((Integer) value);
                    return;
                case "considerInEduLoad":
                    obj.setConsiderInEduLoad((Boolean) value);
                    return;
                case "postgraduateProgramForm":
                    obj.setPostgraduateProgramForm((EduProgramForm) value);
                    return;
                case "yearDistributionPart":
                    obj.setYearDistributionPart((YearDistributionPart) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "owner":
                        return true;
                case "title":
                        return true;
                case "hours":
                        return true;
                case "students":
                        return true;
                case "considerInEduLoad":
                        return true;
                case "postgraduateProgramForm":
                        return true;
                case "yearDistributionPart":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "owner":
                    return true;
                case "title":
                    return true;
                case "hours":
                    return true;
                case "students":
                    return true;
                case "considerInEduLoad":
                    return true;
                case "postgraduateProgramForm":
                    return true;
                case "yearDistributionPart":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "owner":
                    return LoadRecordOwner.class;
                case "title":
                    return String.class;
                case "hours":
                    return Long.class;
                case "students":
                    return Integer.class;
                case "considerInEduLoad":
                    return Boolean.class;
                case "postgraduateProgramForm":
                    return EduProgramForm.class;
                case "yearDistributionPart":
                    return YearDistributionPart.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OtherLoad> _dslPath = new Path<OtherLoad>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OtherLoad");
    }
            

    /**
     * @return ППС (нагрузка). Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.OtherLoad#getOwner()
     */
    public static LoadRecordOwner.Path<LoadRecordOwner> owner()
    {
        return _dslPath.owner();
    }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.OtherLoad#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Часы. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.OtherLoad#getHours()
     */
    public static PropertyPath<Long> hours()
    {
        return _dslPath.hours();
    }

    /**
     * @return Студенты. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.OtherLoad#getStudents()
     */
    public static PropertyPath<Integer> students()
    {
        return _dslPath.students();
    }

    /**
     * @return Учитывать в учебной нагрузке. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.OtherLoad#isConsiderInEduLoad()
     */
    public static PropertyPath<Boolean> considerInEduLoad()
    {
        return _dslPath.considerInEduLoad();
    }

    /**
     * @return Нагрузка аспирантуры (форма обучения).
     * @see ru.tandemservice.uniload_demo.entity.OtherLoad#getPostgraduateProgramForm()
     */
    public static EduProgramForm.Path<EduProgramForm> postgraduateProgramForm()
    {
        return _dslPath.postgraduateProgramForm();
    }

    /**
     * @return Часть учебного года. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.OtherLoad#getYearDistributionPart()
     */
    public static YearDistributionPart.Path<YearDistributionPart> yearDistributionPart()
    {
        return _dslPath.yearDistributionPart();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniload_demo.entity.OtherLoad#getHoursAsDouble()
     */
    public static SupportedPropertyPath<Double> hoursAsDouble()
    {
        return _dslPath.hoursAsDouble();
    }

    public static class Path<E extends OtherLoad> extends EntityPath<E>
    {
        private LoadRecordOwner.Path<LoadRecordOwner> _owner;
        private PropertyPath<String> _title;
        private PropertyPath<Long> _hours;
        private PropertyPath<Integer> _students;
        private PropertyPath<Boolean> _considerInEduLoad;
        private EduProgramForm.Path<EduProgramForm> _postgraduateProgramForm;
        private YearDistributionPart.Path<YearDistributionPart> _yearDistributionPart;
        private SupportedPropertyPath<Double> _hoursAsDouble;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ППС (нагрузка). Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.OtherLoad#getOwner()
     */
        public LoadRecordOwner.Path<LoadRecordOwner> owner()
        {
            if(_owner == null )
                _owner = new LoadRecordOwner.Path<LoadRecordOwner>(L_OWNER, this);
            return _owner;
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.OtherLoad#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(OtherLoadGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Часы. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.OtherLoad#getHours()
     */
        public PropertyPath<Long> hours()
        {
            if(_hours == null )
                _hours = new PropertyPath<Long>(OtherLoadGen.P_HOURS, this);
            return _hours;
        }

    /**
     * @return Студенты. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.OtherLoad#getStudents()
     */
        public PropertyPath<Integer> students()
        {
            if(_students == null )
                _students = new PropertyPath<Integer>(OtherLoadGen.P_STUDENTS, this);
            return _students;
        }

    /**
     * @return Учитывать в учебной нагрузке. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.OtherLoad#isConsiderInEduLoad()
     */
        public PropertyPath<Boolean> considerInEduLoad()
        {
            if(_considerInEduLoad == null )
                _considerInEduLoad = new PropertyPath<Boolean>(OtherLoadGen.P_CONSIDER_IN_EDU_LOAD, this);
            return _considerInEduLoad;
        }

    /**
     * @return Нагрузка аспирантуры (форма обучения).
     * @see ru.tandemservice.uniload_demo.entity.OtherLoad#getPostgraduateProgramForm()
     */
        public EduProgramForm.Path<EduProgramForm> postgraduateProgramForm()
        {
            if(_postgraduateProgramForm == null )
                _postgraduateProgramForm = new EduProgramForm.Path<EduProgramForm>(L_POSTGRADUATE_PROGRAM_FORM, this);
            return _postgraduateProgramForm;
        }

    /**
     * @return Часть учебного года. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.OtherLoad#getYearDistributionPart()
     */
        public YearDistributionPart.Path<YearDistributionPart> yearDistributionPart()
        {
            if(_yearDistributionPart == null )
                _yearDistributionPart = new YearDistributionPart.Path<YearDistributionPart>(L_YEAR_DISTRIBUTION_PART, this);
            return _yearDistributionPart;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniload_demo.entity.OtherLoad#getHoursAsDouble()
     */
        public SupportedPropertyPath<Double> hoursAsDouble()
        {
            if(_hoursAsDouble == null )
                _hoursAsDouble = new SupportedPropertyPath<Double>(OtherLoadGen.P_HOURS_AS_DOUBLE, this);
            return _hoursAsDouble;
        }

        public Class getEntityClass()
        {
            return OtherLoad.class;
        }

        public String getEntityName()
        {
            return "otherLoad";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract Double getHoursAsDouble();
}
