/*$Id$*/
package ru.tandemservice.uniload_demo.base.bo.Load.ui.ResetEduGroups;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;

/**
 * @author DMITRY KNYAZEV
 * @since 08.08.2014
 */
@Configuration
public class LoadResetEduGroups extends BusinessComponentManager
{
	public static final String PUPNAG_DS = "pupnagDS";

	@Override
	@Bean
	public PresenterExtPoint presenterExtPoint()
	{
		return presenterExtPointBuilder()
				.addDataSource(selectDS(PUPNAG_DS, pupnagDSHandler()))
				.create();
	}

	@Bean
	public IDefaultComboDataSourceHandler pupnagDSHandler()
	{
		return new DefaultComboDataSourceHandler(getName(), EppYearEducationProcess.class);
	}
}
