package ru.tandemservice.uniload_demo.entity;

import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniload_demo.entity.gen.*;

/**
 * Максимальная и минимальная нагрузка ППС
 */
public class LoadSize extends LoadSizeGen
{
	public LoadSize()
	{
	}

	public LoadSize(EppYearEducationProcess year, PostBoundedWithQGandQL post)
	{
		this.setYear(year);
		this.setPost(post);
	}
}