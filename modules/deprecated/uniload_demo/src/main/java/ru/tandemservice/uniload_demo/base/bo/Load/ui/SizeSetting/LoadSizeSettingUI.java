/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.base.bo.Load.ui.SizeSetting;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniload_demo.base.bo.Load.LoadManager;
import ru.tandemservice.uniload_demo.base.bo.Load.logic.ILoadDAO;
import ru.tandemservice.uniload_demo.base.bo.Load.vo.LoadSizeValueObject;
import ru.tandemservice.uniload_demo.entity.LoadSize;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Alexander Zhebko
 * @since 20.12.2013
 */
public class LoadSizeSettingUI extends UIPresenter
{
	private EppYearEducationProcess _pupnag;
	private Map<PostBoundedWithQGandQL, LoadSizeValueObject> _currentYearMap = new HashMap<>();

	@Override
	public void onComponentRefresh()
	{
		setPupnag(LoadManager.instance().dao().getNextYear());
		onChangeYear();
	}

	public void onChangeYear()
	{
		if (getPupnag() != null)
		{
			ILoadDAO dao = LoadManager.instance().dao();
			for (LoadSize loadSize : dao.getList(LoadSize.class, LoadSize.L_YEAR, getPupnag()))
			{
				_currentYearMap.put(loadSize.getPost(), new LoadSizeValueObject(loadSize.getMinSize(), loadSize.getMaxSize(), loadSize.getTotalSize()));
			}
		}
	}

	//Handlers
	public void onClickApply()
	{
		LoadManager.instance().dao().saveOrUpdateLoadMaxSize(getPupnag().getId(), _currentYearMap);
	}

	public void onClickSave()
	{
		onClickApply();
		deactivate();
	}

	//Getters/Setters
	public EppYearEducationProcess getPupnag()
	{
		return _pupnag;
	}

	public void setPupnag(EppYearEducationProcess pupnag)
	{
		_pupnag = pupnag;
	}

	private IUIDataSource getPostDS()
	{
		return getConfig().getDataSource(LoadSizeSetting.POST_DS);
	}

	private PostBoundedWithQGandQL getCurrentRow()
	{
		return ((PostBoundedWithQGandQL) getPostDS().getCurrent());
	}

	private LoadSizeValueObject getCurrentLoad()
	{
		LoadSizeValueObject load = _currentYearMap.get(getCurrentRow());
		if(load != null) return load;
		load = new LoadSizeValueObject();
		_currentYearMap.put(getCurrentRow(), load);
		return load;
	}

	public Double getMaxSize()
	{
		return getCurrentLoad().getMaxSizeAsDouble();
	}

	public void setMaxSize(Double value)
	{
		getCurrentLoad().setMaxSize(value);
	}

	public Double getMinSize()
	{
		return getCurrentLoad().getMinSizeAsDouble();
	}

	public void setMinSize(Double value)
	{
		getCurrentLoad().setMinSize(value);
	}

	public Double getTotalSize()
	{
		return getCurrentLoad().getTotalSizeAsDouble();
	}

	public void setTotalSize(Double value)
	{
		getCurrentLoad().setTotalSize(value);
	}
}