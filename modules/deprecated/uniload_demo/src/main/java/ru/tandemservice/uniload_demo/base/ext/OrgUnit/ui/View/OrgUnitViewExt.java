/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.base.ext.OrgUnit.ui.View;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitView;
import ru.tandemservice.uniload_demo.base.bo.Load.ui.CathedraTab.LoadCathedraTab;

/**
 * @author Alexander Zhebko
 * @since 20.12.2013
 */
@Configuration
public class OrgUnitViewExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "load" + OrgUnitViewExtUI.class.getSimpleName();

    // таб "Нагрузка ППС"
    public static final String ORG_UNIT_LOAD_TAB = "orgUnitLoadDemoTab";

    @Autowired
    private OrgUnitView _orgUnitView;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_orgUnitView.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, OrgUnitViewExtUI.class))
                .create();
    }

    @Bean
    public TabPanelExtension tabPanelExtension()
    {
        return tabPanelExtensionBuilder(_orgUnitView.orgUnitTabPanelExtPoint())
                .addTab(componentTab(ORG_UNIT_LOAD_TAB, LoadCathedraTab.class)
                        .parameters("mvel:['orgUnitId':presenter.orgUnit.id]")
                        .permissionKey("ui:secModel.orgUnit_viewLoadTab"))
                .create();
    }
}
