package ru.tandemservice.uniload_demo.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniload_demo_2x6x6_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.6")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность loadRecordOwner

		// создано обязательное свойство year
		{
			// создать колонку
			tool.createColumn("loadrecordowner_t", new DBColumn("year_id", DBType.LONG));

			// задать значение по умолчанию
			PreparedStatement statement = tool.prepareStatement("SELECT p.id FROM epp_year_epp_t p INNER JOIN educationyear_t y ON (y.ID = p.EDUCATIONYEAR_ID) AND y.intvalue_p <= (SELECT intvalue_p FROM educationyear_t WHERE CURRENT_P = ?)+ 1 ORDER BY p.TITLE_P DESC;", Boolean.TRUE);
			ResultSet set = statement.executeQuery();
			Long defaultYear = null;
			if(set.next())
				defaultYear = set.getLong("id");

			tool.executeUpdate("update loadrecordowner_t set year_id=? where year_id is null", defaultYear);

			// сделать колонку NOT NULL
			tool.setColumnNullable("loadrecordowner_t", "year_id", false);
		}

	    ////////////////////////////////////////////////////////////////////////////////
	    // сущность otherLoad

	    // удалено свойство year
	    {
		    // удалить колонку
		    tool.dropColumn("otherload_t", "year_id");

	    }
    }
}
