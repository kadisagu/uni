/*$Id$*/
package ru.tandemservice.uniload_demo.base.bo.Load.util.print;

import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.uniload_demo.base.bo.Load.LoadManager;
import ru.tandemservice.uniload_demo.base.bo.Load.util.print.LoadDocumentPrintSharedUtils.Term;
import ru.tandemservice.uniload_demo.entity.*;
import ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule;
import ru.tandemservice.uniload_demo.entity.catalog.PpsLoadType;
import ru.tandemservice.uniload_demo.entity.catalog.codes.PpsLoadTypeCodes;

import java.util.*;

/**
 * @author DMITRY KNYAZEV
 * @since 28.10.2014
 */
public final class PpsLoadDocumentWrapper
{
	private final Map<Term, EduLoadPpsDocumentTermWrapper> _eduLoadTermWrapper;
	private final Map<Term, OtherLoadPpsDocumentTermWrapper> _otherLoadTermWrapper;

	public PpsLoadDocumentWrapper()
	{
		_eduLoadTermWrapper = createEduMap();
		_otherLoadTermWrapper = createOtherMap();
	}

	public void add(ILoadImEduGroup imEduGroup, Term term)
	{
		if (imEduGroup instanceof OtherLoad)
		{
			_otherLoadTermWrapper.get(term).add((OtherLoad) imEduGroup);
		}
		else
		{
			EppWorkPlanRegistryElementRow workPlanRow;
			if (imEduGroup instanceof LoadEduGroupPpsDistr)
				workPlanRow = ((LoadEduGroupPpsDistr) imEduGroup).getEduGroup().getWorkPlanRow();
			else
				workPlanRow = ((LoadFirstCourseImEduGroup) imEduGroup).getRow();

			_eduLoadTermWrapper.get(term).getRowWrapper(workPlanRow).add(imEduGroup);
		}
	}

	public Collection<PpsDocumentFirstSheetRowWrapper> getEduLoadRowWrapper(Term term)
	{
		return _eduLoadTermWrapper.get(term).getAllRowWrappers();
	}

	public Collection<PssDocumentSecondSheetRowWrapper> getOtherLoadRowWrapper(Term term, String ppsLoadTypeCode)
	{
		return _otherLoadTermWrapper.get(term).getRows(ppsLoadTypeCode);
	}

	private Map<Term, EduLoadPpsDocumentTermWrapper> createEduMap()
	{
		Map<Term, EduLoadPpsDocumentTermWrapper> map = new HashMap<>();
		for (Term term : Term.values())
		{
			map.put(term, new EduLoadPpsDocumentTermWrapper());
		}
		return map;
	}

	private Map<Term, OtherLoadPpsDocumentTermWrapper> createOtherMap()
	{
		Map<Term, OtherLoadPpsDocumentTermWrapper> map = new HashMap<>();
		for (Term term : Term.values())
		{
			map.put(term, new OtherLoadPpsDocumentTermWrapper());
		}
		return map;
	}

	//HELPER CLASSES
	private class EduLoadPpsDocumentTermWrapper
	{
		private final Map<Long, PpsDocumentFirstSheetRowWrapper> _map;

		public EduLoadPpsDocumentTermWrapper()
		{
			_map = createMap();
		}

		private Map<Long, PpsDocumentFirstSheetRowWrapper> createMap()
		{
			return new HashMap<>();
		}

		private Collection<PpsDocumentFirstSheetRowWrapper> getAllRowWrappers()
		{
			return _map.values();
		}

		private PpsDocumentFirstSheetRowWrapper getRowWrapper(EppWorkPlanRegistryElementRow row)
		{
			PpsDocumentFirstSheetRowWrapper sheet1RowWrapper;
			if (_map.containsKey(row.getId()))
			{
				sheet1RowWrapper = _map.get(row.getId());
			}
			else
			{
				sheet1RowWrapper = new PpsDocumentFirstSheetRowWrapper(row);
				_map.put(row.getId(), sheet1RowWrapper);
			}
			return sheet1RowWrapper;
		}
	}

	private class OtherLoadPpsDocumentTermWrapper
	{
		private final Map<String, Set<PssDocumentSecondSheetRowWrapper>> _map;

		public OtherLoadPpsDocumentTermWrapper()
		{
			_map = createMap();
		}

		private Map<String, Set<PssDocumentSecondSheetRowWrapper>> createMap()
		{
			Map<String, Set<PssDocumentSecondSheetRowWrapper>> map = new HashMap<>();
			for (String ppsLoadTypeCode : PpsLoadTypeCodes.CODES)
			{
				map.put(ppsLoadTypeCode, new HashSet<PssDocumentSecondSheetRowWrapper>());
			}
			return map;
		}

		private void add(OtherLoad imEduGroup)
		{
			Collection<LoadTimeRule> loadTimeRules = LoadManager.instance().dao().getTimeRules4EduGroup(imEduGroup.getId());
			for (LoadTimeRule timeRule : loadTimeRules)
			{
				PpsLoadType ppsLoadType = getPpsLoadTypeCodes4TimeRule(timeRule);
				if (ppsLoadType != null)
				{
					PssDocumentSecondSheetRowWrapper sheet2RowWrapper = new PssDocumentSecondSheetRowWrapper(imEduGroup);
					getRows(ppsLoadType.getCode()).add(sheet2RowWrapper);
				}
			}
		}

		private Collection<PssDocumentSecondSheetRowWrapper> getRows(String ppsLoadTypeCode)
		{
			if (!_map.containsKey(ppsLoadTypeCode))
				throw new ApplicationException("Неизвестный код типа нагрузки ППС:" + ppsLoadTypeCode);
			else
				return _map.get(ppsLoadTypeCode);
		}

		private PpsLoadType getPpsLoadTypeCodes4TimeRule(LoadTimeRule timeRule)
		{
			PpsLoadType2TimeRule ppsLoadType2TimeRule = LoadManager.instance().dao().getUnique(PpsLoadType2TimeRule.class, PpsLoadType2TimeRule.L_TIME_RULE, timeRule.getId());
			if (ppsLoadType2TimeRule == null) return null;
			else return ppsLoadType2TimeRule.getPpsLoadType();
		}

	}
}
