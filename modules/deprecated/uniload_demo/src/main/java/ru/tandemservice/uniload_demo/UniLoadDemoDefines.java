/**
 *$Id$
 */
package ru.tandemservice.uniload_demo;

/**
 * @author Alexander Zhebko
 * @since 23.12.2013
 */
public interface UniLoadDemoDefines
{
    /* Коды справочника "Нормы времени"*/
    public static final String LOAD_TIME_RULE_LECTURE = "1";
    public static final String LOAD_TIME_RULE_PRACTICE = "2";
    public static final String LOAD_TIME_RULE_LABS = "3";
}