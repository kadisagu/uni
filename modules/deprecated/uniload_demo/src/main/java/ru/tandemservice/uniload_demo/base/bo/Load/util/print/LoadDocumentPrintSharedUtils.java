/*$Id$*/
package ru.tandemservice.uniload_demo.base.bo.Load.util.print;

import com.google.common.collect.Multimap;
import jxl.format.CellFormat;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableSheet;
import jxl.write.WriteException;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.catalog.codes.YearDistributionPartCodes;
import ru.tandemservice.uniload_demo.base.bo.Load.LoadManager;
import ru.tandemservice.uniload_demo.base.bo.Load.logic.ILoadDAO;
import ru.tandemservice.uniload_demo.entity.ILoadImEduGroup;
import ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule;
import ru.tandemservice.uniload_demo.entity.catalog.PpsLoadGroup;
import ru.tandemservice.uniload_demo.entity.catalog.codes.PpsLoadGroupCodes;

import javax.annotation.Nullable;
import java.util.*;

/**
 * @author DMITRY KNYAZEV
 * @since 20.11.2014
 */
public final class LoadDocumentPrintSharedUtils
{
	/**
	 * Утилитарный класс для печатных форм
	 * Нельзя создавать экземпляры этого класса.
	 */
	private LoadDocumentPrintSharedUtils()
	{
		throw new AssertionError();
	}

	/**
	 * Создает ячейку на заданную длинну строки
	 *
	 * @param sheet      лист
	 * @param currentRow номер строки
	 * @param rowLength  количество ячеек занимаемое строкой
	 * @param label      содержимое строки
	 * @param cf         формат ячейки
	 * @throws WriteException
	 */
	public static void fillRow(int currentRow, WritableSheet sheet, Integer rowLength, String label, CellFormat cf) throws WriteException
	{
		int firstColumn = 0;
		fillRow(firstColumn, currentRow, sheet, rowLength, label, cf);
	}

	/**
	 * Создает ячейку на заданную длинну строки с заданной колонки
	 *
	 * @param firstColumn начальная колонка
	 * @param currentRow  номер строки
	 * @param rowLength   количество ячеек занимаемое строкой
	 * @param label       содержимое строки
	 * @param cf          формат ячейки
	 * @throws WriteException
	 */
	public static void fillRow(int firstColumn, int currentRow, WritableSheet sheet, Integer rowLength, String label, CellFormat cf) throws WriteException
	{
		sheet.addCell(new Label(firstColumn, currentRow, label, cf));
		sheet.mergeCells(firstColumn, currentRow, firstColumn + --rowLength, currentRow);
	}

	/**
	 * Создает строку на листе xls.<br/>
	 * Особенности работы смотри {@link #fillRow(int, int, jxl.write.WritableSheet, String[], Integer[], java.util.Map, jxl.format.CellFormat)}
	 *
	 * @param rowNumber     номер стоки
	 * @param sheet         лист xls
	 * @param cellLabel     список выводимых ячеек
	 * @param defaultFormat формат ячеек по умолчанию
	 * @throws WriteException
	 */
	public static void fillRow(int rowNumber, WritableSheet sheet, String[] cellLabel, CellFormat defaultFormat) throws WriteException
	{
		fillRow(0, rowNumber, sheet, cellLabel, null, null, defaultFormat);
	}

	/**
	 * Создает строку на листе xls.<br/>
	 * Особенности работы смотри {@link #fillRow(int, int, jxl.write.WritableSheet, String[], Integer[], java.util.Map, jxl.format.CellFormat)}
	 *
	 * @param rowNumber     номер стоки
	 * @param sheet         лист xls
	 * @param cellLabel     список выводимых ячеек
	 * @param cellLength    длинна соответствующей ячейки (в количестве колонок)
	 * @param defaultFormat формат ячеек по умолчанию
	 * @throws WriteException
	 */
	public static void fillRow(int rowNumber, WritableSheet sheet, String[] cellLabel, @Nullable Integer[] cellLength, CellFormat defaultFormat) throws WriteException
	{
		fillRow(0, rowNumber, sheet, cellLabel, cellLength, null, defaultFormat);
	}

	/**
	 * Создает строку на листе xls.<br/>
	 * Особенности работы смотри {@link #fillRow(int, int, jxl.write.WritableSheet, String[], Integer[], java.util.Map, jxl.format.CellFormat)}
	 *
	 * @param rowNumber     номер стоки
	 * @param sheet         лист xls
	 * @param cellLabel     список выводимых ячеек
	 * @param cellFormat    формат соответствующей ячейки.
	 * @param defaultFormat формат ячеек по умолчанию
	 * @throws WriteException
	 */
	public static void fillRow(int rowNumber, WritableSheet sheet, String[] cellLabel, @Nullable Map<Integer, CellFormat> cellFormat, CellFormat defaultFormat) throws WriteException
	{
		fillRow(0, rowNumber, sheet, cellLabel, null, cellFormat, defaultFormat);
	}

	/**
	 * Создает строку на листе xls.<br/>
	 * Особенности работы смотри {@link #fillRow(int, int, jxl.write.WritableSheet, String[], Integer[], java.util.Map, jxl.format.CellFormat)}
	 *
	 * @param rowNumber     номер стоки
	 * @param sheet         лист xls
	 * @param cellLabel     список выводимых ячеек
	 * @param cellLength    длинна соответствующей ячейки (в количестве колонок)
	 * @param cellFormat    формат соответствующей ячейки.
	 * @param defaultFormat формат ячеек по умолчанию
	 * @throws WriteException
	 */
	public static void fillRow(int rowNumber, WritableSheet sheet, String[] cellLabel, @Nullable Integer[] cellLength, @Nullable Map<Integer, CellFormat> cellFormat, CellFormat defaultFormat) throws WriteException
	{
		fillRow(0, rowNumber, sheet, cellLabel, cellLength, cellFormat, defaultFormat);
	}

	/**
	 * Создает строку на листе xls.<br/>
	 * Особенности работы смотри {@link #fillRow(int, int, jxl.write.WritableSheet, String[], Integer[], java.util.Map, jxl.format.CellFormat)}
	 *
	 * @param colNumber     начальная колонка
	 * @param rowNumber     номер стоки
	 * @param sheet         лист xls
	 * @param cellLabel     список выводимых ячеек
	 * @param defaultFormat формат ячеек по умолчанию
	 * @throws WriteException
	 */
	public static void fillRow(int colNumber, int rowNumber, WritableSheet sheet, String[] cellLabel, CellFormat defaultFormat) throws WriteException
	{
		fillRow(colNumber, rowNumber, sheet, cellLabel, null, null, defaultFormat);
	}

	/**
	 * Создает строку на листе xls.<br/>
	 * Если длинна для заданной ячейки не задана то она принимается равной 1.<br/>
	 * Если формат ячейки не задан то берется формат по умолчанию<br/>
	 * Длинна ячейки задается в виде строки чисел где i-ой ячейке из списка соответствует i-я длинна<br/>
	 * Формат ячейки задается в виде мапы где {@code key} - порядковый номер в списке ячеек, {@code value} - формат<br/>
	 *
	 * @param colNumber     начальная колонка
	 * @param rowNumber     номер строки
	 * @param sheet         лист xls
	 * @param cellLabel     список выводимых ячеек
	 * @param cellLength    длинна соответствующей ячейки (в количестве колонок)
	 * @param cellFormat    формат соответствующей ячейки.
	 * @param defaultFormat формат ячеек по умолчанию
	 * @throws WriteException
	 */
	public static void fillRow(final int colNumber, final int rowNumber, WritableSheet sheet, String[] cellLabel, @Nullable Integer[] cellLength, @Nullable Map<Integer, CellFormat> cellFormat, CellFormat defaultFormat) throws WriteException
	{
		int i = 0;
		int columnNumber = colNumber;
		while (i < cellLabel.length)
		{
			CellFormat cf;
			if (cellFormat != null && cellFormat.containsKey(i)) cf = cellFormat.get(i);
			else cf = defaultFormat;
			//fill if not empty
			if (!(cellLabel[i] == null))
				sheet.addCell(new Label(columnNumber, rowNumber, cellLabel[i], cf));
			//merge columns
			if (cellLength != null && i < cellLength.length && cellLength[i] != null)
			{
				int length = cellLength[i];
				int lastCol = columnNumber + length - 1;
				sheet.mergeCells(columnNumber, rowNumber, lastCol, rowNumber);
				columnNumber += length;
			}
			else columnNumber++;
			i++;
		}
	}

	/**
	 * Заполняет строку нагрузки для соответствующих норм времени.<br/>
	 * Если нагрузка для заданной ячейки не найдена она заполнятся 0<br/>
	 * Позиция нормы времени задается в виде мапы где {@code key} - порядковый номер в ячейки(считается от 0), {@code value} - код нормы времени<br/>
	 * Нагрузка нормы времени задается в виде мапы где {@code key} - код нормы времени, {@code value} - нагрузка<br/>
	 *
	 * @param rowNumber   номер строки
	 * @param startColumn начальная клонка
	 * @param sheet       лист
	 * @param lenColumn   количество заполняемых колонок
	 * @param format      формат ячеек
	 * @param data        нагрузка нормы времени
	 * @param position    позиции норм времени
	 * @throws WriteException
	 */
	public static void fillLoadRowPart(int rowNumber, final int startColumn, WritableSheet sheet, int lenColumn, WritableCellFormat format, Map<String, Double> data, Map<Integer, String> position) throws WriteException
	{
		String[] row = new String[lenColumn];
		int currentColumn = startColumn;
		for (int i = 0; i < lenColumn; i++)
		{
			Double value = 0D;
			if (position.containsKey(currentColumn))
			{
				String code = position.get(currentColumn);
				if (data.containsKey(code))
				{
					value = data.get(code);
				}
			}
			row[i] = value.toString();
			currentColumn++;
		}
		fillRow(startColumn, rowNumber, sheet, row, format);
	}

	/**
	 * Заполняет строку с названиями норм времени<br/>
	 * Возвращает их позиции в формате мапа где {@code key} - порядковый номер в ячейки(считается от 0), {@code value} - код нормы времени<br/>
	 *
	 * @param startColumn начальная колонка
	 * @param startRow    строка
	 * @param sheet       лист
	 * @param cf          формат ячеек
	 * @param groupData   нормы времени распределенные  по группам нагрузки ППС и мне лень его описывать
	 * @return Позиции норм времени ввиде
	 * @throws WriteException
	 */
	public static Map<Integer, String> fillTimeRuleRow(int startColumn, int startRow, WritableSheet sheet, CellFormat cf, GroupData groupData) throws WriteException
	{
		Map<Integer, String> timeRulePositionMap = new HashMap<>();
		Multimap<String, String> Group2TimeRuleCode = LoadManager.instance().dao().getPpsLoadGroup2TimeRuleCodeMultimap();
		String[] timeRuleRow = new String[groupData.getSum()];
		Integer position = startColumn;
		Integer i = 0;
		for (String code : PpsLoadGroupCodes.CODES)
		{
			List<String> timeRuleCodes = new ArrayList<>(Group2TimeRuleCode.get(code));
			Collections.sort(timeRuleCodes);
			Map<String, String> timeRuleCode2TitleMap = LoadManager.instance().dao().getTimeRuleCode2TitleMap(timeRuleCodes);
			for (String timeRuleCode : timeRuleCodes)
			{
				timeRuleRow[i++] = timeRuleCode2TitleMap.get(timeRuleCode);
				timeRulePositionMap.put(position++, timeRuleCode);
			}
			Integer diff = groupData.getGroupLen(code) - timeRuleCodes.size();
			while (diff-- > 0)
			{
				timeRuleRow[i++] = "";
				position++;
			}
		}
		fillRow(startColumn, startRow, sheet, timeRuleRow, null, null, cf);
		return timeRulePositionMap;
	}

	/**
	 * Устанавливает заданную ширину колонок из мапы. Если в мапе ширина не указана, то устанавливается значение по умолчанию.
	 *
	 * @param sheet          лист
	 * @param columnWidthMap Мап с заданной шириной колонки. {@code key} - номер колонки, {@code value} - ширина
	 * @param defaultWidth   ширина колонки по умолчанию
	 * @param coefficient    коэффициент ширины колонки
	 * @param rowLength      количество колонок
	 */
	public static void setColumnWidth(WritableSheet sheet, Map<Integer, Integer> columnWidthMap, int defaultWidth, int coefficient, int rowLength)
	{
		int i = 0;
		while (i < rowLength)
		{
			int width;
			if (columnWidthMap.containsKey(i))
			{
				width = columnWidthMap.get(i);
			}
			else width = defaultWidth;
			width *= coefficient;
			sheet.setColumnView(i, width);
			i += 1;
		}
	}

	/**
	 * Определяет размер группы нагрузки ППС
	 *
	 * @param code код размер группы нагрузки ППС
	 * @return размер группы нагрузки ППС
	 */
	private static Integer getGroupSize(String code)
	{
		Multimap<String, String> loadGroup2TimeRuleCode = LoadManager.instance().dao().getPpsLoadGroup2TimeRuleCodeMultimap();
		if (!loadGroup2TimeRuleCode.containsKey(code)) return 0;
		return loadGroup2TimeRuleCode.get(code).size();
	}


	/**
	 * Помещает в мапу значение соответствующее нескольким ключам
	 */
	@SafeVarargs
	public static <K, V> void putMutikeyMap(Map<K, V> map, V value, K... keys)
	{
		for (K key : keys)
		{
			map.put(key, value);
		}
	}

	/**
	 * Добавляет все значения из источника в приемник.<br/>
	 * Источник при этом не изменяется
	 * Если в мапах имеется значение с оинаковым ключом, то сохраняется сумма этих значений<br/>
	 *
	 * @param dest приемник
	 * @param src  источник
	 * @param <K>  тип ключа
	 */
	public static <K> void mergeMaps(Map<K, Double> dest, Map<K, Double> src)
	{
		for (K key : src.keySet())
		{
			if (dest.containsKey(key))
			{
				Double value = src.get(key);
				value += dest.get(key);
				dest.put(key, value);
			}
			else
			{
				dest.put(key, src.get(key));
			}
		}
	}

	/**
	 * Возвращает название школы в которой находися подразделение.<br/>
	 * Проверка идет максимум на 3 уровня вверх<br/>
	 *
	 * @param orgUnit подразделение
	 * @return название школы. если оно не найдено то {@code null}
	 */
	@Nullable
	public static String getSchoolTitle(OrgUnit orgUnit)
	{
		int maxDeep = 3;
		OrgUnit nextOrgUnit = orgUnit;
		while (maxDeep-- > 0)
		{
			if (LoadManager.instance().dao().isSchool(nextOrgUnit)) return nextOrgUnit.getTitle();
			nextOrgUnit = nextOrgUnit.getParent();
			if (nextOrgUnit == null) break;
		}
		return null;
	}

	/**
	 * Создает массив заполненный пустыми строками
	 * @param strLen длина массива
	 * @return массив пустых строк
	 */
	public static String[] getEmptyString(int strLen)
	{
		String[] str = new String[strLen];
		for (int i = 0; i < str.length; i++) str[i] = "";
		return str;
	}

    /**
     * Добавляет к нагрузке нормы времени нагрузку планируемой УГС и возвращает сумму добавленной нагрузки
     * Нагрузка нормы времени задается в виде мапы где {@code key} - код нормы времени, {@code value} - нагрузка<br/>
     *
     * @param imEduGroup Планируемая УГС (демо)
     * @param rowLoadMap Нагрузка нормы времени
     * @return сумму добавленной нагрузки
     */
    public static Double addEduGroupLoadToLoadMap(ILoadImEduGroup imEduGroup, Map<String, Double> rowLoadMap)
    {
        final ILoadDAO dao = LoadManager.instance().dao();
        Collection<LoadTimeRule> loadTimeRules = dao.getTimeRules4EduGroup(imEduGroup.getId());
        Double totalAddedLoad = 0D;

        for (LoadTimeRule timeRule : loadTimeRules)
        {
            String code = timeRule.getCode();
            Double load = dao.getEduLoad4EduGroupAsDouble(imEduGroup, timeRule);
            if (rowLoadMap.containsKey(code))
            {
                Double newValue = rowLoadMap.get(code) + load;
                rowLoadMap.put(code, newValue);
            } else
                rowLoadMap.put(code, load);
            totalAddedLoad += load;
        }
        return totalAddedLoad;
    }

	// HELPER CLASSES
	public static class GroupData
	{
		private final Map<String, Integer> groupLen;
		private final Map<String, String> groupTitle;
		private final Integer sum;

		public GroupData()
		{
			this(new HashMap<String, Integer>()
			{{
					put(PpsLoadGroupCodes.AUDITORNYE_ZANYATIYA, 3);
					put(PpsLoadGroupCodes.KONSULTATSII, 2);
					put(PpsLoadGroupCodes.KONTROL, 6);
					put(PpsLoadGroupCodes.PRAKTIKA, 3);
					put(PpsLoadGroupCodes.RUKOVODSTVO, 6);
				}});
		}

		public GroupData(Map<String, Integer> defaultLenMap)
		{
			Map<String, Integer> lenMap = new HashMap<>();
			Map<String, String> titleMap = new HashMap<>();
			int sum = 0;
			for (String code : PpsLoadGroupCodes.CODES)
			{
				Integer len;
				if (defaultLenMap.get(code) == null || getGroupSize(code) > defaultLenMap.get(code)) len = getGroupSize(code);
				else len = defaultLenMap.get(code);
				sum += len;
				lenMap.put(code, len);

				titleMap.put(code, LoadManager.instance().dao().getNotNull(PpsLoadGroup.class, PpsLoadGroup.code(), code).getTitle());
			}

			this.groupLen = Collections.unmodifiableMap(lenMap);
			this.groupTitle = Collections.unmodifiableMap(titleMap);
			this.sum = sum;
		}

		public Integer getGroupLen(String code)
		{
			if (groupLen.containsKey(code)) return groupLen.get(code);
			throw new IllegalStateException("Illegal  PpsLoadGroup code");
		}

		public String getGroupTitle(String code)
		{
			if (groupTitle.containsKey(code)) return groupTitle.get(code);
			throw new IllegalStateException("Illegal PpsLoadGroup code");
		}

		public Integer getSum()
		{
			return sum;
		}
	}

	public static enum Term
	{
		//is autumn term at fefu
		WINTER(YearDistributionPartCodes.WINTER_SEMESTER),
		//is spring term at fefu
		SUMMER(YearDistributionPartCodes.SUMMER_SEMESTER);

		private final String title;
		private final String shortTitle;

		Term(String code)
		{
			YearDistributionPart part = LoadManager.instance().dao().getNotNull(YearDistributionPart.class, YearDistributionPart.code(), code);
			this.title = part.getTitle();
			this.shortTitle = part.getShortTitle();
		}

		public String getTitle()
		{
			return title;
		}

		public String getShortTitle()
		{
			return shortTitle;
		}
	}
}
