package ru.tandemservice.uniload_demo.entity;

import org.tandemframework.shared.employeebase.base.bo.EmployeePost.logic.IEmployeeAdditionalInfo;
import ru.tandemservice.uniload_demo.entity.gen.LoadFakePpsEntryGen;

/**
 * Фиктивный ППС (демо)
 */
public class LoadFakePpsEntry extends LoadFakePpsEntryGen {

    public double getStaffRateDouble() {
        return getStaffRateInteger() / IEmployeeAdditionalInfo.MULTIPLIER.doubleValue();
    }

    public void setStaffRateDouble(double staffRateDouble) {
        setStaffRateInteger((int) (staffRateDouble * IEmployeeAdditionalInfo.MULTIPLIER));
    }
}