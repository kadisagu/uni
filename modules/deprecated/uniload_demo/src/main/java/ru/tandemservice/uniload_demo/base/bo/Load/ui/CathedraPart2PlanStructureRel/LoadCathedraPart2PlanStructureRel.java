/*$Id$*/
package ru.tandemservice.uniload_demo.base.bo.Load.ui.CathedraPart2PlanStructureRel;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;
import ru.tandemservice.uniload_demo.entity.catalog.PpsLoadCathedraPart;

import java.util.Collection;

/**
 * @author DMITRY KNYAZEV
 * @since 27.11.2014
 */
@Configuration
@SuppressWarnings("SpringFacetCodeInspection")
public class LoadCathedraPart2PlanStructureRel extends BusinessComponentManager
{
	public static final String PPS_CATHEDRA_PART_DS = "ppsCathedraPartDS";
	public static final String EPP_PLAN_STRUCTURE_DS = "eppPlanStructureDS";

	public static final String USED_PLAN_STRUCTURE = "usedLoadTimeRuleList";

	@Override
	@Bean
	public PresenterExtPoint presenterExtPoint()
	{
		return presenterExtPointBuilder()
				.addDataSource(searchListDS(PPS_CATHEDRA_PART_DS, ppsCathedraPartDSColumns(), ppsCathedraPartDSHandler()))
				.addDataSource(selectDS(EPP_PLAN_STRUCTURE_DS, eppPlanStructureDSHandler()).addColumn(EppPlanStructure.P_HIERARCHY_TITLE))
				.create();
	}

	@Bean
	public ColumnListExtPoint ppsCathedraPartDSColumns()
	{
		return columnListExtPointBuilder(PPS_CATHEDRA_PART_DS)
				.addColumn(textColumn("title", PpsLoadCathedraPart.P_TITLE).treeable(Boolean.TRUE).width("400px"))
				.addColumn(blockColumn("time", "eppPlanStructureColumn"))
				.create();
	}

	@Bean
	public IDefaultSearchDataSourceHandler ppsCathedraPartDSHandler()
	{
		return new EntityComboDataSourceHandler(getName(), PpsLoadCathedraPart.class).order(PpsLoadCathedraPart.code());
	}

	@Bean
	public IDefaultComboDataSourceHandler eppPlanStructureDSHandler()
	{
		return new EntityComboDataSourceHandler(getName(), EppPlanStructure.class)
		{
			@Override
			protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
			{
				super.applyWhereConditions(alias, dql, context);
				Collection<EppPlanStructure> usedPlanStructure = context.get(USED_PLAN_STRUCTURE);

				if (usedPlanStructure != null && !usedPlanStructure.isEmpty())
					dql.where(DQLExpressions.notIn(DQLExpressions.property(alias, EppPlanStructure.P_ID), usedPlanStructure));

			}
		}.filter(EppPlanStructure.title());
	}
}