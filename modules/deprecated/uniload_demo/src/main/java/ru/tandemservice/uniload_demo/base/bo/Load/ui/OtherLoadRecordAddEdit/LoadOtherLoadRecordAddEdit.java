/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.base.bo.Load.ui.OtherLoadRecordAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramFormCodes;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniload_demo.entity.ImEduGroupLoadBase;
import ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule;

import java.util.Arrays;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 20.12.2013
 */
@Configuration
public class LoadOtherLoadRecordAddEdit extends BusinessComponentManager
{
	public static final String TIME_RULE_DS = "timeRuleDS";
	public static final String EDU_YEAR_PART_DS = "eduYearPartDS";
	public static final String PROGRAM_FORM_DS = "programFormDS";
	public static final String PUPNAG_ID = "pupnagId";

	@Override
	@Bean
	public PresenterExtPoint presenterExtPoint()
	{
		return presenterExtPointBuilder()
				.addDataSource(selectDS(TIME_RULE_DS, timeRuleDSHandler()))
				.addDataSource(selectDS(EDU_YEAR_PART_DS, eduYearPartDSHandler()))
				.addDataSource(selectDS(PROGRAM_FORM_DS, programFormDSHandler()))
				.create();
	}

	@Bean
	public IDefaultComboDataSourceHandler timeRuleDSHandler()
	{
		return new EntityComboDataSourceHandler(getName(), LoadTimeRule.class)
		{
			@Override
			protected DQLSelectBuilder query(String alias, String filter)
			{
				return super.query(alias, filter)
						.where(likeUpper(property(alias, LoadTimeRule.title()), value(CoreStringUtils.escapeLike(filter))))
						.where(eq(property(alias, LoadTimeRule.inUse()), value(Boolean.TRUE)))
						.where(ne(property(alias, LoadTimeRule.base()), value(ImEduGroupLoadBase.STUDENTS)));
			}
		};
	}

	@Bean
	public IDefaultComboDataSourceHandler eduYearPartDSHandler()
	{
		return new EntityComboDataSourceHandler(getName(), YearDistributionPart.class)
		{
			protected DQLSelectBuilder query(final String alias, final String filter)
			{
				return super.query(alias, filter)
						.joinEntity(alias, DQLJoinType.inner, EppYearPart.class, "ep", eq(property("ep", EppYearPart.L_PART), property(alias, YearDistributionPart.P_ID)));
			}

			@Override
			protected void applyWhereConditions(final String alias, final DQLSelectBuilder dql, final ExecutionContext context)
			{
				super.applyWhereConditions(alias, dql, context);
				Long pupnagId = context.get(PUPNAG_ID);
				dql.where(eq(property("ep", EppYearPart.L_YEAR), value(pupnagId)));
			}
		};
	}

    @Bean
    public IDefaultComboDataSourceHandler programFormDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramForm.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(in(property(alias, EduProgramForm.code()), Arrays.asList(
                                EduProgramFormCodes.OCHNAYA,
                                EduProgramFormCodes.ZAOCHNAYA,
                                EduProgramFormCodes.OCHNO_ZAOCHNAYA)
                ));
            }
        }
		.filter(EduProgramForm.title())
		.order(EduProgramForm.code());
    }
}