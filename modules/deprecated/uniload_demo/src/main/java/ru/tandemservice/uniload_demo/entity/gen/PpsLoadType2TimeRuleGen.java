package ru.tandemservice.uniload_demo.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniload_demo.entity.PpsLoadType2TimeRule;
import ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule;
import ru.tandemservice.uniload_demo.entity.catalog.PpsLoadType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь типа нагрузки ППС
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PpsLoadType2TimeRuleGen extends EntityBase
 implements INaturalIdentifiable<PpsLoadType2TimeRuleGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniload_demo.entity.PpsLoadType2TimeRule";
    public static final String ENTITY_NAME = "ppsLoadType2TimeRule";
    public static final int VERSION_HASH = -1471296601;
    private static IEntityMeta ENTITY_META;

    public static final String L_TIME_RULE = "timeRule";
    public static final String L_PPS_LOAD_TYPE = "ppsLoadType";

    private LoadTimeRule _timeRule;     // Норма времени
    private PpsLoadType _ppsLoadType;     // Тип нагрузки ППС

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Норма времени. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public LoadTimeRule getTimeRule()
    {
        return _timeRule;
    }

    /**
     * @param timeRule Норма времени. Свойство не может быть null и должно быть уникальным.
     */
    public void setTimeRule(LoadTimeRule timeRule)
    {
        dirty(_timeRule, timeRule);
        _timeRule = timeRule;
    }

    /**
     * @return Тип нагрузки ППС. Свойство не может быть null.
     */
    @NotNull
    public PpsLoadType getPpsLoadType()
    {
        return _ppsLoadType;
    }

    /**
     * @param ppsLoadType Тип нагрузки ППС. Свойство не может быть null.
     */
    public void setPpsLoadType(PpsLoadType ppsLoadType)
    {
        dirty(_ppsLoadType, ppsLoadType);
        _ppsLoadType = ppsLoadType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PpsLoadType2TimeRuleGen)
        {
            if (withNaturalIdProperties)
            {
                setTimeRule(((PpsLoadType2TimeRule)another).getTimeRule());
            }
            setPpsLoadType(((PpsLoadType2TimeRule)another).getPpsLoadType());
        }
    }

    public INaturalId<PpsLoadType2TimeRuleGen> getNaturalId()
    {
        return new NaturalId(getTimeRule());
    }

    public static class NaturalId extends NaturalIdBase<PpsLoadType2TimeRuleGen>
    {
        private static final String PROXY_NAME = "PpsLoadType2TimeRuleNaturalProxy";

        private Long _timeRule;

        public NaturalId()
        {}

        public NaturalId(LoadTimeRule timeRule)
        {
            _timeRule = ((IEntity) timeRule).getId();
        }

        public Long getTimeRule()
        {
            return _timeRule;
        }

        public void setTimeRule(Long timeRule)
        {
            _timeRule = timeRule;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof PpsLoadType2TimeRuleGen.NaturalId) ) return false;

            PpsLoadType2TimeRuleGen.NaturalId that = (NaturalId) o;

            if( !equals(getTimeRule(), that.getTimeRule()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getTimeRule());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getTimeRule());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PpsLoadType2TimeRuleGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PpsLoadType2TimeRule.class;
        }

        public T newInstance()
        {
            return (T) new PpsLoadType2TimeRule();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "timeRule":
                    return obj.getTimeRule();
                case "ppsLoadType":
                    return obj.getPpsLoadType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "timeRule":
                    obj.setTimeRule((LoadTimeRule) value);
                    return;
                case "ppsLoadType":
                    obj.setPpsLoadType((PpsLoadType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "timeRule":
                        return true;
                case "ppsLoadType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "timeRule":
                    return true;
                case "ppsLoadType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "timeRule":
                    return LoadTimeRule.class;
                case "ppsLoadType":
                    return PpsLoadType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PpsLoadType2TimeRule> _dslPath = new Path<PpsLoadType2TimeRule>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PpsLoadType2TimeRule");
    }
            

    /**
     * @return Норма времени. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniload_demo.entity.PpsLoadType2TimeRule#getTimeRule()
     */
    public static LoadTimeRule.Path<LoadTimeRule> timeRule()
    {
        return _dslPath.timeRule();
    }

    /**
     * @return Тип нагрузки ППС. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.PpsLoadType2TimeRule#getPpsLoadType()
     */
    public static PpsLoadType.Path<PpsLoadType> ppsLoadType()
    {
        return _dslPath.ppsLoadType();
    }

    public static class Path<E extends PpsLoadType2TimeRule> extends EntityPath<E>
    {
        private LoadTimeRule.Path<LoadTimeRule> _timeRule;
        private PpsLoadType.Path<PpsLoadType> _ppsLoadType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Норма времени. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniload_demo.entity.PpsLoadType2TimeRule#getTimeRule()
     */
        public LoadTimeRule.Path<LoadTimeRule> timeRule()
        {
            if(_timeRule == null )
                _timeRule = new LoadTimeRule.Path<LoadTimeRule>(L_TIME_RULE, this);
            return _timeRule;
        }

    /**
     * @return Тип нагрузки ППС. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.PpsLoadType2TimeRule#getPpsLoadType()
     */
        public PpsLoadType.Path<PpsLoadType> ppsLoadType()
        {
            if(_ppsLoadType == null )
                _ppsLoadType = new PpsLoadType.Path<PpsLoadType>(L_PPS_LOAD_TYPE, this);
            return _ppsLoadType;
        }

        public Class getEntityClass()
        {
            return PpsLoadType2TimeRule.class;
        }

        public String getEntityName()
        {
            return "ppsLoadType2TimeRule";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
