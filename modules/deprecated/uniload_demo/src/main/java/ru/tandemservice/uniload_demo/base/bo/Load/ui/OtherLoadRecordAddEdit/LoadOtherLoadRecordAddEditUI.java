/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.base.bo.Load.ui.OtherLoadRecordAddEdit;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Transformer;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniload_demo.base.bo.Load.LoadManager;
import ru.tandemservice.uniload_demo.base.bo.Load.logic.ILoadDAO;
import ru.tandemservice.uniload_demo.entity.ImEduGroupLoadBase;
import ru.tandemservice.uniload_demo.entity.LoadImEduGroupTimeRuleRel;
import ru.tandemservice.uniload_demo.entity.LoadRecordOwner;
import ru.tandemservice.uniload_demo.entity.OtherLoad;
import ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule;
import ru.tandemservice.uniload_demo.entity.gen.LoadImEduGroupTimeRuleRelGen;

import java.util.Collection;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 20.12.2013
 */
@Input({
		@Bind(key = LoadOtherLoadRecordAddEditUI.RECORD_ID, binding = LoadOtherLoadRecordAddEditUI.RECORD_ID),
		@Bind(key = LoadOtherLoadRecordAddEditUI.PPS_ID, binding = LoadOtherLoadRecordAddEditUI.PPS_ID),
		@Bind(key = LoadOtherLoadRecordAddEditUI.PUPNAG_ID, binding = LoadOtherLoadRecordAddEditUI.PUPNAG_ID, required = true)})
public class LoadOtherLoadRecordAddEditUI extends UIPresenter
{
	public static final String RECORD_ID = "recordId";
	public static final String PPS_ID = "ppsId";
	public static final String PUPNAG_ID = "pupnagId";

	private Long recordId;
	private Long ppsId;
	private Long pupnagId;

	private OtherLoad record;
	private Collection<LoadTimeRule> timeRules;
	private boolean postgraduateLoad;

	@Override
	public void onComponentRefresh()
	{
		ILoadDAO dao = LoadManager.instance().dao();
		if (getRecordId() == null)
		{
			setRecord(new OtherLoad());
			getRecord().setOwner(dao.<LoadRecordOwner>getNotNull(getPpsId()));
		}
		else
		{
			setRecord(dao.getNotNull(getRecordId()));
			List<LoadImEduGroupTimeRuleRel> timeRuleRels = LoadManager.instance().dao().getList(LoadImEduGroupTimeRuleRel.class, LoadImEduGroupTimeRuleRel.eduGroup().id(), getRecordId(), LoadImEduGroupTimeRuleRel.timeRule().title().s());
			Transformer<LoadImEduGroupTimeRuleRel, LoadTimeRule> transformer = LoadImEduGroupTimeRuleRelGen::getTimeRule;

			setTimeRules(CollectionUtils.collect(timeRuleRels, transformer));
            setPostgraduateLoad(getRecord().getPostgraduateProgramForm() != null);
		}
	}

	@Override
	public void onBeforeDataSourceFetch(IUIDataSource dataSource)
	{
		if (dataSource.getName().equals(LoadOtherLoadRecordAddEdit.EDU_YEAR_PART_DS))
		{
			dataSource.put(LoadOtherLoadRecordAddEdit.PUPNAG_ID, getPupnagId());
		}
	}

	//Handlers
	public void onClickApply()
	{
		ILoadDAO dao = LoadManager.instance().dao();
		dao.saveOrUpdate(getRecord());
		dao.saveImEduGroupTimeRules(getRecord().getId(), getTimeRules());
		deactivate();
	}

    public void onClickFillLoadSize() {
        Double loadSize = 0D;
        for(LoadTimeRule timeRule : getTimeRules()){
            if(ImEduGroupLoadBase.CONSTANT.equals(timeRule.getBase())){
                loadSize += timeRule.getCoefficientAsDouble();
            }
        }
        setLoadSizeAsDouble(loadSize);
    }

    public void onChangepostgraduateLoad()
    {
        if (!isPostgraduateLoad())
        {
            getRecord().setPostgraduateProgramForm(null);
        }
    }

    //getters/setters
	public Double getLoadSizeAsDouble()
	{
		return UniEppUtils.wrap(getRecord().getHours());
	}

	public void setLoadSizeAsDouble(Double load)
	{
		getRecord().setHours(UniEppUtils.unwrap(load));
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public Long getPpsId() {
		return ppsId;
	}

	public void setPpsId(Long ppsId) {
		this.ppsId = ppsId;
	}

	public Long getPupnagId() {
		return pupnagId;
	}

	public void setPupnagId(Long pupnagId) {
		this.pupnagId = pupnagId;
	}

	public OtherLoad getRecord() {
		return record;
	}

	public void setRecord(OtherLoad record) {
		this.record = record;
	}

	public Collection<LoadTimeRule> getTimeRules() {
		return timeRules;
	}

	public void setTimeRules(Collection<LoadTimeRule> timeRules) {
		this.timeRules = timeRules;
	}

	public boolean isPostgraduateLoad()
	{
		return postgraduateLoad;
	}

	public void setPostgraduateLoad(boolean postgraduateLoad)
	{
		this.postgraduateLoad = postgraduateLoad;
	}
}