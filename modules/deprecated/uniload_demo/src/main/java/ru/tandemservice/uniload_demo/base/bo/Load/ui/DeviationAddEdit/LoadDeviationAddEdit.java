/*$Id$*/
package ru.tandemservice.uniload_demo.base.bo.Load.ui.DeviationAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniload_demo.entity.ILoadImEduGroup;
import ru.tandemservice.uniload_demo.entity.LoadFakePpsEntry;
import ru.tandemservice.uniload_demo.entity.LoadRealPpsEntry;
import ru.tandemservice.uniload_demo.entity.LoadRecordOwner;
import ru.tandemservice.uniload_demo.entity.gen.ILoadImEduGroupGen;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 25.12.2014
 */
@Configuration
@SuppressWarnings("SpringFacetCodeInspection")
public class LoadDeviationAddEdit extends BusinessComponentManager
{
	public final static String LOAD_PPS_DS = "ppsDS";
	public final static String EDU_GROUP_DS = "eduGroupDS";

	public static final String KEY_OWNER_ID = "loadOwnerId";
	public static final String KEY_ORG_UNIT_ID = "orgUnitId";
	public static final String KEY_PUPNAG_ID = "pupnagIdr";

	@Override
	@Bean
	public PresenterExtPoint presenterExtPoint()
	{
		return presenterExtPointBuilder()
				.addDataSource(selectDS(LOAD_PPS_DS, loadPpsDSHandler()))
				.addDataSource(selectDS(EDU_GROUP_DS, eduGroupDSHandler()))
				.create();
	}

	@Bean
	public IDefaultComboDataSourceHandler loadPpsDSHandler()
	{
		return new EntityComboDataSourceHandler(getName(), LoadRecordOwner.class)
		{

			@Override
			protected DQLSelectBuilder query(final String alias, final String filter)
			{
				return super.query(alias, filter)
						.joinEntity(alias, DQLJoinType.left, LoadRealPpsEntry.class, "rpe", eq(property(alias, LoadRecordOwner.realPps().id()), property("rpe", LoadRealPpsEntry.id())))
						.joinEntity("rpe", DQLJoinType.left, PpsEntry.class, "pe", eq(property("rpe", LoadRealPpsEntry.pps().id()), property("pe", PpsEntry.id())))
						.joinEntity(alias, DQLJoinType.left, LoadFakePpsEntry.class, "fpe", eq(property(alias, LoadRecordOwner.fakePps().id()), property("fpe", LoadFakePpsEntry.id())));
			}

			@Override
			protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
			{
				Long orgUnitId = context.get(KEY_ORG_UNIT_ID);
				Long pupnagId = context.get(KEY_PUPNAG_ID);
				dql.where(eq(property(alias, LoadRecordOwner.year().id()), value(pupnagId)));
				dql.where(or(eq(property("pe", PpsEntry.L_ORG_UNIT), value(orgUnitId)), eq(property("fpe", LoadFakePpsEntry.L_ORG_UNIT), value(orgUnitId))));
				dql.distinct();
			}
		};
	}

	@Bean
	public IDefaultComboDataSourceHandler eduGroupDSHandler()
	{
		return new EntityComboDataSourceHandler(getName(), ILoadImEduGroup.class)
		{
			@Override
			protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
			{
				Long recordOwnerId = context.get(KEY_OWNER_ID);
				if (recordOwnerId == null)
					dql.where(DQLExpressions.isNull(DQLExpressions.property(alias)));
				else
					dql.where(DQLExpressions.eq(DQLExpressions.property(alias, ILoadImEduGroupGen.L_OWNER), DQLExpressions.value(recordOwnerId)));
			}
		};
	}
}
