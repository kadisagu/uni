/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.base.ext.Employee;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Alexander Zhebko
 * @since 20.12.2013
 */
@Configuration
public class EmployeeExtManager extends BusinessObjectExtensionManager
{
}
