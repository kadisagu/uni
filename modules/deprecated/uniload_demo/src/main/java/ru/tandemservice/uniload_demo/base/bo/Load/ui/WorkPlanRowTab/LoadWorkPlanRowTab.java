/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.base.bo.Load.ui.WorkPlanRowTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.uniload_demo.base.bo.Load.logic.SpecialPupnagDSHandler;
import ru.tandemservice.uniload_demo.base.bo.Load.logic.WorkPlanRowDSHandler;
import ru.tandemservice.uniload_demo.base.bo.Load.logic.WorkPlanRowWrapper;

/**
 * @author Alexander Zhebko
 * @since 20.12.2013
 */
@Configuration
@SuppressWarnings("SpringFacetCodeInspection")
public class LoadWorkPlanRowTab extends BusinessComponentManager
{
	public static final String WORK_PLAN_ROW_DS = "workPlanRowDS";
	public static final String PUPNAG_DS = "pupnagDS";
	public static final String EDU_YEAR_PART_DS = "eduYearPartDS";

	public static final String PUPNAG = "pupnag";

	public static final String DEVELOP_CONDITION = "developCondition";
	public static final String EDU_YEAR_PART = "eduYearPart";
	public static final String DEVELOP_TECH = "developTech";
	public static final String DEVELOP_GRID = "developGrid";
	public static final String ORG_UNIT_ID = "orgUnitId";

	public static final String TITLE = "title";
	public static final String WORK_PLAN = "workPlan";
	public static final String COURSE = "course";


	@Override
	@Bean
	public PresenterExtPoint presenterExtPoint()
	{
		return presenterExtPointBuilder()
				.addDataSource(selectDS(PUPNAG_DS, pupnagDSHandler()))
				.addDataSource(selectDS(EDU_YEAR_PART_DS, eduYearPartDSHandler()))
				.addDataSource(EducationCatalogsManager.instance().developFormDSConfig())
				.addDataSource(EducationCatalogsManager.instance().developConditionDSConfig())
				.addDataSource(EducationCatalogsManager.instance().developTechDSConfig())
				.addDataSource(EducationCatalogsManager.instance().developGridDSConfig())
				.addDataSource(searchListDS(WORK_PLAN_ROW_DS, workPlanRowDSColumns(), workPlanRowDSHandler()))
				.create();
	}

	@Bean
	public ColumnListExtPoint workPlanRowDSColumns()
	{
		return columnListExtPointBuilder(WORK_PLAN_ROW_DS)
				.addColumn(publisherColumn(TITLE, EppWorkPlanRegistryElementRow.title()).primaryKeyPath(EppWorkPlanRegistryElementRow.registryElementPart().registryElement().id()).order())
				.addColumn(publisherColumn(WORK_PLAN, EppWorkPlanRegistryElementRow.workPlan().title()).primaryKeyPath(EppWorkPlanRegistryElementRow.workPlan().id()).order())
				.addColumn(textColumn("developCombination", WorkPlanRowWrapper.DEVELOP_COMBINATION))
				.addColumn(textColumn("course", WorkPlanRowWrapper.COURSE).order().create())
				.addColumn(textColumn("eduGroup", WorkPlanRowWrapper.EDU_GROUP).create())
				.addColumn(headerColumn("control")
						           .addSubColumn(textColumn(WorkPlanRowWrapper.EXAM, WorkPlanRowWrapper.EXAM).width("15px").align("center").headerAlign("center").create())
						           .addSubColumn(textColumn(WorkPlanRowWrapper.SET_OFF, WorkPlanRowWrapper.SET_OFF).width("15px").align("center").headerAlign("center").create())
						           .addSubColumn(textColumn(WorkPlanRowWrapper.SET_OFF_DIFF, WorkPlanRowWrapper.SET_OFF_DIFF).width("15px").align("center").headerAlign("center").create())
						           .addSubColumn(textColumn(WorkPlanRowWrapper.COURSE_WORK, WorkPlanRowWrapper.COURSE_WORK).width("15px").align("center").headerAlign("center").create())
						           .addSubColumn(textColumn(WorkPlanRowWrapper.COURSE_PROJECT, WorkPlanRowWrapper.COURSE_PROJECT).width("15px").align("center").headerAlign("center").create())
						           .addSubColumn(textColumn(WorkPlanRowWrapper.EXAM_ACCUM, WorkPlanRowWrapper.EXAM_ACCUM).width("15px").align("center").headerAlign("center").create()))

				.addColumn(headerColumn("load")
						           .addSubColumn(textColumn(WorkPlanRowWrapper.LECTURES, WorkPlanRowWrapper.LECTURES).styleResolver(WorkPlanRowWrapper.LECTURE_COLOR_RESOLVER).width("15px").headerAlign("center").create())
						           .addSubColumn(textColumn(WorkPlanRowWrapper.PRACTICE, WorkPlanRowWrapper.PRACTICE).styleResolver(WorkPlanRowWrapper.PRACTICE_COLOR_RESOLVER).width("15px").headerAlign("center").create())
						           .addSubColumn(textColumn(WorkPlanRowWrapper.LABS, WorkPlanRowWrapper.LABS).styleResolver(WorkPlanRowWrapper.LABS_COLOR_RESOLVER).width("15px").headerAlign("center").create()))

				.addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).disabled("ui:workPlanRowEditDisabled"))
				.create();
	}

	@Bean
	public IDefaultSearchDataSourceHandler workPlanRowDSHandler()
	{
		return new WorkPlanRowDSHandler(getName());
	}

	@Bean
	public IDefaultComboDataSourceHandler pupnagDSHandler()
	{
		return new SpecialPupnagDSHandler(getName());
	}

	@Bean
	public IDefaultComboDataSourceHandler eduYearPartDSHandler()
	{
		return new EntityComboDataSourceHandler(getName(), EppYearPart.class).where(EppYearPart.year(), PUPNAG, true);
	}
}