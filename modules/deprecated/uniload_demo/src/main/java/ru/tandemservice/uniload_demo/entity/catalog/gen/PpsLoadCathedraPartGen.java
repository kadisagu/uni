package ru.tandemservice.uniload_demo.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniload_demo.entity.catalog.PpsLoadCathedraPart;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Разделы нагрузки кафедры
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PpsLoadCathedraPartGen extends EntityBase
 implements INaturalIdentifiable<PpsLoadCathedraPartGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniload_demo.entity.catalog.PpsLoadCathedraPart";
    public static final String ENTITY_NAME = "ppsLoadCathedraPart";
    public static final int VERSION_HASH = -415199309;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String L_PARENT = "parent";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private PpsLoadCathedraPart _parent;     // Разделы нагрузки кафедры
    private String _shortTitle;     // Сокращенное название
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Разделы нагрузки кафедры.
     */
    public PpsLoadCathedraPart getParent()
    {
        return _parent;
    }

    /**
     * @param parent Разделы нагрузки кафедры.
     */
    public void setParent(PpsLoadCathedraPart parent)
    {
        dirty(_parent, parent);
        _parent = parent;
    }

    /**
     * @return Сокращенное название.
     */
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PpsLoadCathedraPartGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((PpsLoadCathedraPart)another).getCode());
            }
            setParent(((PpsLoadCathedraPart)another).getParent());
            setShortTitle(((PpsLoadCathedraPart)another).getShortTitle());
            setTitle(((PpsLoadCathedraPart)another).getTitle());
        }
    }

    public INaturalId<PpsLoadCathedraPartGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<PpsLoadCathedraPartGen>
    {
        private static final String PROXY_NAME = "PpsLoadCathedraPartNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof PpsLoadCathedraPartGen.NaturalId) ) return false;

            PpsLoadCathedraPartGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PpsLoadCathedraPartGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PpsLoadCathedraPart.class;
        }

        public T newInstance()
        {
            return (T) new PpsLoadCathedraPart();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "parent":
                    return obj.getParent();
                case "shortTitle":
                    return obj.getShortTitle();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "parent":
                    obj.setParent((PpsLoadCathedraPart) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "parent":
                        return true;
                case "shortTitle":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "parent":
                    return true;
                case "shortTitle":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "parent":
                    return PpsLoadCathedraPart.class;
                case "shortTitle":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PpsLoadCathedraPart> _dslPath = new Path<PpsLoadCathedraPart>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PpsLoadCathedraPart");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniload_demo.entity.catalog.PpsLoadCathedraPart#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Разделы нагрузки кафедры.
     * @see ru.tandemservice.uniload_demo.entity.catalog.PpsLoadCathedraPart#getParent()
     */
    public static PpsLoadCathedraPart.Path<PpsLoadCathedraPart> parent()
    {
        return _dslPath.parent();
    }

    /**
     * @return Сокращенное название.
     * @see ru.tandemservice.uniload_demo.entity.catalog.PpsLoadCathedraPart#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uniload_demo.entity.catalog.PpsLoadCathedraPart#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends PpsLoadCathedraPart> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PpsLoadCathedraPart.Path<PpsLoadCathedraPart> _parent;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniload_demo.entity.catalog.PpsLoadCathedraPart#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(PpsLoadCathedraPartGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Разделы нагрузки кафедры.
     * @see ru.tandemservice.uniload_demo.entity.catalog.PpsLoadCathedraPart#getParent()
     */
        public PpsLoadCathedraPart.Path<PpsLoadCathedraPart> parent()
        {
            if(_parent == null )
                _parent = new PpsLoadCathedraPart.Path<PpsLoadCathedraPart>(L_PARENT, this);
            return _parent;
        }

    /**
     * @return Сокращенное название.
     * @see ru.tandemservice.uniload_demo.entity.catalog.PpsLoadCathedraPart#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(PpsLoadCathedraPartGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uniload_demo.entity.catalog.PpsLoadCathedraPart#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(PpsLoadCathedraPartGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return PpsLoadCathedraPart.class;
        }

        public String getEntityName()
        {
            return "ppsLoadCathedraPart";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
