/*$Id$*/
package ru.tandemservice.uniload_demo.base.bo.Load.util.print;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.*;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uniload_demo.base.bo.Load.LoadManager;
import ru.tandemservice.uniload_demo.base.bo.Load.logic.ILoadDAO;
import ru.tandemservice.uniload_demo.entity.LoadRecordOwner;
import ru.tandemservice.uniload_demo.entity.catalog.PpsLoadType;
import ru.tandemservice.uniload_demo.entity.catalog.codes.PpsLoadGroupCodes;
import ru.tandemservice.uniload_demo.entity.catalog.codes.PpsLoadTypeCodes;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;

import static ru.tandemservice.uniload_demo.base.bo.Load.util.print.LoadDocumentPrintSharedUtils.*;

/**
 * @author DMITRY KNYAZEV
 * @since 28.10.2014
 */
public final class PpsLoadDocumentBuilder
{
	private final String EMPTY_FIELD = "";

	private final int ROW_COEFFICIENT = 100;
	private final int COLUMN_COEFFICIENT = 3;

	// создаем шрифты
	private final WritableFont times14b = new WritableFont(WritableFont.TIMES, 14, WritableFont.BOLD);
	private final WritableFont times12b = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
	private final WritableFont times12i = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD, true);
	private final WritableFont times09b = new WritableFont(WritableFont.TIMES, 9, WritableFont.BOLD);

	private final ILoadDAO _dao = LoadManager.instance().dao();
	private final GroupData _groupData = new GroupData();
	private final WritableCellFormatFactory _format = WritableCellFormatFactory.newInstance(times14b);

	private final LoadRecordOwner _owner;
	private final PpsLoadDocumentWrapper _ppsLoadDocumentWrapper;
	private Map<Integer, String> _timeRulePositionMap = null;

	public PpsLoadDocumentBuilder(Long loadRecordOwnerId)
	{
		_owner = getDao().getNotNull(loadRecordOwnerId);
		_ppsLoadDocumentWrapper = getDao().preparePpsLoadDocumentWrapper(loadRecordOwnerId);
	}

	public IDocumentRenderer buildDocumentRenderer() throws IOException, WriteException
	{
		String fileName = getFileName() + ".xls";
		return new CommonBaseRenderer().xml().fileName(fileName).document(buildDocument());
	}

	private ByteArrayOutputStream buildDocument() throws IOException, WriteException
	{
		// создаем печатную форму
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		WorkbookSettings ws = new WorkbookSettings();
		ws.setEncoding("UTF-8");
		ws.setMergedCellChecking(false);
		ws.setRationalization(false);
		ws.setGCDisabled(true);
		WritableWorkbook workbook = Workbook.createWorkbook(out, ws);

		//Создает страницу 1 (Первая половина дня)
		WritableSheet sheet = workbook.createSheet("Первая половина дня", workbook.getSheets().length);
		sheet.getSettings().setOrientation(PageOrientation.LANDSCAPE);
		sheet.getSettings().setCopies(1);
		firstSheet(sheet);

		//Создает страницу 2 (Вторая половина дня)
		sheet = workbook.createSheet("Вторая половина дня", workbook.getSheets().length);
		sheet.getSettings().setOrientation(PageOrientation.LANDSCAPE);
		sheet.getSettings().setCopies(1);
		secondSheet(sheet);

		//посылаем отчет
		workbook.write();
		workbook.close();
		return out;
	}

	private void firstSheet(final WritableSheet sheet) throws WriteException
	{
		final SheetVO sheetVO = new SheetVO(31, 10, getVarLength(), 1, sheet);
		final int rowLength = sheetVO.getRowLength();   //действительное количество колонок
		final int statLength = sheetVO.getStatLength(); //количество колонок в неизменяемой части
		final int varLength = sheetVO.getVarLength();   //getVarLength() + 3; //количество колонок в изменяемой части
		//-------------------------------------------------------
		{
			//set column width
			Map<Integer, Integer> columnWidth = new HashMap<>();
			columnWidth.put(3, 10);
			columnWidth.put(5, 3);
			columnWidth.put(10, 3);

			setColumnWidth(sheet, columnWidth, 2, COLUMN_COEFFICIENT, rowLength);
		}
		//------------------------------------------------
		{
			WritableCellFormat rowFormat14 = _format.setFont(times14b)
					.setAlignment(Alignment.CENTRE)
					.setVerticalAlignment(VerticalAlignment.BOTTOM)
					.setBordered(true)
					.getCellFormat();

			sheet.setRowView(sheetVO.getCurrentRow(), 8 * ROW_COEFFICIENT);
			String[] labelRow = {
					"План работы преподавателя на", getEduYear(), "учебный год",
					"Школа (Филиал)", getAffiliateTitle(),
					"Название кафедры\n(полное)", getCathedraFullTitle()
			};

			Integer[] labelLen = {/*stat*/4, 3, 3,/*var*/5, 5, 5, 6};
			resizeColumns(sheetVO, labelLen, 3);

			fillRow(sheetVO.getCurrentRow(), sheet, labelRow, labelLen, rowFormat14);
			sheetVO.incCurrentRow();
		}
		//------------------------------------------------
		{
			WritableCellFormat rowFormat14 = _format.setVerticalAlignment(VerticalAlignment.CENTRE).getCellFormat();
			sheet.setRowView(sheetVO.getCurrentRow(), 15 * ROW_COEFFICIENT);

			String[] labelRow = {
					"ФИО", _owner.getFullFio(),
					"Ученая степень", EMPTY_FIELD,
					"Ученое звание", EMPTY_FIELD,
					"Другие звания", EMPTY_FIELD,
					"Должность", _owner.getPostTitle(EMPTY_FIELD),
					"Количество\nзанимаемых ставок", getRate(),
					"Штатный,\nсовместитель\n(внешний, внутренний),\nпочасовик", getEmployeeStatus()
			};

			Integer[] labelLen = {/*stat*/2, 2, 2, 2, 2,/*var*/2, 2, 2, 2, 2, 3, 1, 4, 3};
			resizeColumns(sheetVO, labelLen, 5);

			fillRow(sheetVO.getCurrentRow(), sheet, labelRow, labelLen, rowFormat14);
			sheetVO.incCurrentRow();
		}
		//------------------------------------------------
		{
			WritableCellFormat rowFormat14 = _format.setVerticalAlignment(VerticalAlignment.CENTRE).getCellFormat();
			WritableCellFormat rowFormat14R = _format.setAlignment(Alignment.RIGHT).getCellFormat();
			WritableCellFormat rowFormat14L = _format.setAlignment(Alignment.LEFT).getCellFormat();

			Map<Integer, CellFormat> formatMap = new HashMap<>();
			formatMap.put(0, rowFormat14L);
			putMutikeyMap(formatMap, rowFormat14R, 2, 4);

			sheet.setRowView(sheetVO.getCurrentRow(), 10 * ROW_COEFFICIENT);
			String[] labelRow = {
					"Наименование специальности\nпо диплому (дипломам)", EMPTY_FIELD,
					"Запланировано на год\nвсего:", EMPTY_FIELD,
					"Выполнено за год\nвсего:", EMPTY_FIELD
			};
			Integer[] labelLen = {/*stat*/4, 6,/*var*/9, 1, 10, 1};

			resizeColumns(sheetVO, labelLen, 2);

			fillRow(sheetVO.getCurrentRow(), sheet, labelRow, labelLen, formatMap, rowFormat14);
			sheetVO.incCurrentRow();
		}
		//------------------------------------------------
		Map<String, Double> totalLoadTerm = new HashMap<>();
		for (Term term : Term.values())
		{
			Map<String, Double> loadTerm = firstSheetCreateTable(sheetVO, term);
			mergeMaps(totalLoadTerm, loadTerm);
		}
		//------------------------------------------------
		{
			WritableCellFormat rowFormat12 = _format.setAlignment(Alignment.CENTRE).getCellFormat();

			int firstRow = sheetVO.getCurrentRow();
			int firstColLen = 4;
			int secondColLen = statLength - firstColLen;
			//------------------------------------------------
			fillRow(0, sheetVO.getCurrentRow(), sheet, firstColLen, "Всего за учебный год", rowFormat12);

			//second column
			fillRow(firstColLen, sheetVO.getCurrentRow(), sheet, secondColLen, "Запланировано", rowFormat12);
			fillLoadRowPart(sheetVO.getCurrentRow(), statLength, sheet, varLength + 1, rowFormat12, totalLoadTerm, _timeRulePositionMap);
			sheetVO.incCurrentRow();
			//second column
			fillRow(firstColLen, sheetVO.getCurrentRow(), sheet, secondColLen, "Выполнено", rowFormat12);
			fillLoadRowPart(sheetVO.getCurrentRow(), statLength, sheet, varLength + 1, rowFormat12, Collections.<String, Double>emptyMap(), _timeRulePositionMap);
			sheetVO.incCurrentRow();
			//second column
			fillRow(firstColLen, sheetVO.getCurrentRow(), sheet, secondColLen, "Расхождение", rowFormat12);
			fillLoadRowPart(sheetVO.getCurrentRow(), statLength, sheet, varLength + 1, rowFormat12, Collections.<String, Double>emptyMap(), _timeRulePositionMap);
			sheetVO.incCurrentRow();
			//------------------------------------------------
			sheet.mergeCells(0, firstRow, firstColLen - 1, sheetVO.getCurrentRow() - 1);
		}
	}

	private void secondSheet(final WritableSheet sheet) throws WriteException
	{
		final SheetVO sheetVO = new SheetVO(14, 14, 0, 0, sheet);
		final int rowLength = sheetVO.getRowLength();   //действительное количество колонок

		{
			Map</*col num*/Integer, /*col width*/Integer> columnWidth = new HashMap<>();
			columnWidth.put(3, 13);
			putMutikeyMap(columnWidth, 4, 2, 11);
			putMutikeyMap(columnWidth, 5, 10, 13);

			setColumnWidth(sheet, columnWidth, 3, COLUMN_COEFFICIENT, rowLength);
		}
		Double totalLoad = 0D;
		for (String typeCode : PpsLoadTypeCodes.CODES)
		{
			totalLoad += secondSheetCreateTable(sheetVO, typeCode);
		}

		{
			sheet.setRowView(sheetVO.getCurrentRow(), 7 * ROW_COEFFICIENT);
			String totalLoadString = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(totalLoad);
			String[] totalRow = {EMPTY_FIELD, EMPTY_FIELD, "Запланировано всех видов работ на весь учебный год:", totalLoadString, "Выполнено всех видов работ за весь учебный год:", "0", EMPTY_FIELD};
			Integer[] totalRowLen = {1, 1, 2, 2, 5, 2, 1};


			WritableCellFormat rowFormat12 = _format.setFont(times12b).setOrientation(Orientation.HORIZONTAL).setAlignment(Alignment.CENTRE).getCellFormat();
			final WritableCellFormat rowFormat12R = _format.setAlignment(Alignment.RIGHT).getCellFormat();
			Map<Integer, CellFormat> formatMap = new HashMap<Integer, CellFormat>(){{ put(2, rowFormat12R);	put(4, rowFormat12R);}};
			fillRow(sheetVO.getCurrentRow(), sheet, totalRow, totalRowLen, formatMap, rowFormat12);
			sheetVO.incCurrentRow();
		}
		{
			sheet.setRowView(sheetVO.getCurrentRow(), 10 * ROW_COEFFICIENT);
			String[] totalRow = {"Утверждение плана", "Протокол заседания кафедры,\nдата:", "Подпись преподаветеля:", EMPTY_FIELD, "Зав. каф:", EMPTY_FIELD, "Начальник УМУ\nШколы\n(Филиала)", EMPTY_FIELD};
			Integer[] totalRowLen = {2, 2, 2, 2, 1, 2, 2, 1};

			WritableCellFormat rowFormat12 = _format.setAlignment(Alignment.LEFT).getCellFormat();
			final WritableCellFormat rowFormat12C = _format.setAlignment(Alignment.CENTRE).getCellFormat();
			Map<Integer, CellFormat> formatMap = new HashMap<Integer, CellFormat>(){{put(0, rowFormat12C);}};

			fillRow(sheetVO.getCurrentRow(), sheet, totalRow, totalRowLen, formatMap, rowFormat12);
			sheetVO.incCurrentRow();

			sheet.setRowView(sheetVO.getCurrentRow(), 10 * ROW_COEFFICIENT);
			totalRow[0] = "Отчет о проделанной работе";
			fillRow(sheetVO.getCurrentRow(), sheet, totalRow, totalRowLen, formatMap, rowFormat12);
			sheetVO.incCurrentRow();
		}
	}

	private void firstSheetCreateTableHeader(SheetVO sheetVO) throws WriteException
	{
		final WritableSheet sheet = sheetVO.getSheet();
		final int statLength = sheetVO.getStatLength();
		final int rowLength = sheetVO.getRowLength();
		final int varLength = sheetVO.getVarLength();
		//------------------------------------------------
		WritableCellFormat rowFormat12 = _format.setFont(times12b)
				.setAlignment(Alignment.CENTRE)
				.setVerticalAlignment(VerticalAlignment.CENTRE)
				.getCellFormat();

		WritableCellFormat rowFormat12RotateC = _format.setOrientation(Orientation.PLUS_90).getCellFormat();

		WritableCellFormat rowFormat12RotateB = _format.setVerticalAlignment(VerticalAlignment.BOTTOM).getCellFormat();
		//------------------------------------------------

		Map<Integer, CellFormat> formatMap = new HashMap<>();
		putMutikeyMap(formatMap, rowFormat12RotateC, 0, 1);
		putMutikeyMap(formatMap, rowFormat12RotateB, 4, 5, 6, 7, 8, 9, 15);

		Integer firstRowNumber = sheetVO.getCurrentRow();

		String[] labelRow = {
				"Планирование / Выполнение",
				"№ п.п.",
				EMPTY_FIELD,
				"Наименование дисциплин согласно учебному плану / вид работы в соответствии с приказом",
				"Школа",
				"Код направления подготовки\n(специальности)",
				"Форма обучения (О, З, О/З)",
				"Курс",
				"Количество студентов / аспирантов",
				"Число групп / подгрупп",

				_groupData.getGroupTitle(PpsLoadGroupCodes.AUDITORNYE_ZANYATIYA),
				_groupData.getGroupTitle(PpsLoadGroupCodes.KONSULTATSII),
				_groupData.getGroupTitle(PpsLoadGroupCodes.KONTROL),
				_groupData.getGroupTitle(PpsLoadGroupCodes.PRAKTIKA),
				_groupData.getGroupTitle(PpsLoadGroupCodes.RUKOVODSTVO),

				"Всего",
		};

		Integer[] labelLen = new Integer[rowLength];//{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 2, 6, 3, 6, 1};
		{
			int pos = statLength;
			labelLen[pos++] = _groupData.getGroupLen(PpsLoadGroupCodes.AUDITORNYE_ZANYATIYA);
			labelLen[pos++] = _groupData.getGroupLen(PpsLoadGroupCodes.KONSULTATSII);
			labelLen[pos++] = _groupData.getGroupLen(PpsLoadGroupCodes.KONTROL);
			labelLen[pos++] = _groupData.getGroupLen(PpsLoadGroupCodes.PRAKTIKA);
			labelLen[pos] = _groupData.getGroupLen(PpsLoadGroupCodes.RUKOVODSTVO);
		}

		fillRow(sheetVO.getCurrentRow(), sheet, labelRow, labelLen, formatMap, rowFormat12);
		sheetVO.incCurrentRow();
		//------------------------------------------------
		Integer lastRowNumber = sheetVO.getCurrentRow();
		sheet.setRowView(lastRowNumber, 30 * ROW_COEFFICIENT);

		Map<Integer, String> timeRulePositionMap = fillTimeRuleRow(statLength, sheetVO.getCurrentRow(), sheet, rowFormat12RotateB, _groupData);
		if (_timeRulePositionMap == null)
		{
			_timeRulePositionMap = timeRulePositionMap;
			_timeRulePositionMap.put(statLength + varLength, ConsolidatedDocumentRowWrapper.ROW_TOTAL_LOAD_CODE);
		}
		sheetVO.incCurrentRow();

		//row merge
		for (int i = 0; i < statLength; i++)
		{
			sheet.mergeCells(i, firstRowNumber, i, lastRowNumber);
		}
		int endLength = sheetVO.getEndLength();
		sheet.mergeCells(rowLength - endLength, firstRowNumber, rowLength - endLength, lastRowNumber);
	}

	private Map<String, Double> firstSheetCreateTable(final SheetVO sheetVO, Term term) throws WriteException
	{
		final WritableSheet sheet = sheetVO.getSheet();
		final int rowLength = sheetVO.getRowLength();
		final int statLength = sheetVO.getStatLength();
		final int varLength = sheetVO.getVarLength();

		//TITLE
		if (term.equals(Term.WINTER))
		{
			sheet.setRowView(sheetVO.getCurrentRow(), 5 * ROW_COEFFICIENT);
			WritableCellFormat rowFormat14 = _format.setFont(times14b).
					setAlignment(Alignment.CENTRE).
					setVerticalAlignment(VerticalAlignment.CENTRE).getCellFormat();
			fillRow(sheetVO.getCurrentRow(), sheet, rowLength, StringUtils.capitalize(term.getTitle()), rowFormat14);
			sheetVO.incCurrentRow();
		}
		else
		{
			WritableCellFormat rowFormat14 = _format.setFont(times14b)
					.setVerticalAlignment(VerticalAlignment.CENTRE)
					.getCellFormat();
			sheet.setRowView(sheetVO.getCurrentRow(), 5 * ROW_COEFFICIENT);
			String[] firstRow = {
					"План работы преподавателя на " + term.getTitle(),
					"учебный год",
					getEduYear(),
					_owner.getFio(),
					"0",
					EMPTY_FIELD
			};
			int firstCol = statLength + 5;
			Integer[] firstRowLength = {firstCol, 3, 4, 4, 4};
			resizeColumns(sheetVO, firstRowLength, 1);
			fillRow(sheetVO.getCurrentRow(), sheet, firstRow, firstRowLength, rowFormat14);
			sheetVO.incCurrentRow();
		}

		//TABLE HEADER
		firstSheetCreateTableHeader(sheetVO);

		Map<String, Double> timeRuleTotalLoad = new HashMap<>();
		final int minRowNumber = 14;
		{
			//first part "Планирование"
			WritableCellFormat rowFormat12 = _format.setFont(times12b)
					.setAlignment(Alignment.CENTRE)
					.setVerticalAlignment(VerticalAlignment.CENTRE)
					.setOrientation(Orientation.HORIZONTAL)
					.getCellFormat();

			int firstLineNumber = sheetVO.getCurrentRow();
			int counter = 1;

			for (PpsDocumentFirstSheetRowWrapper rowWrapper : getDocument().getEduLoadRowWrapper(term))
			{
				String[] rowLabel = firstSheetCreateRow(counter++, rowWrapper, sheetVO);

				fillRow(sheetVO.getCurrentRow(), sheet, rowLabel, rowFormat12);

				Map<String, Double> rowLoadMap = rowWrapper.getRowLoadMap();
				fillLoadRowPart(sheetVO.getCurrentRow(), statLength, sheet, varLength + 1, rowFormat12, rowLoadMap, _timeRulePositionMap);
				sheetVO.incCurrentRow();

				mergeMaps(timeRuleTotalLoad, rowLoadMap);
			}

			String[] emptyRow = getEmptyString(rowLength);
			for (int i = counter; i < minRowNumber; i++, counter++)
			{
				emptyRow[1] = String.valueOf(counter);
				fillRow(sheetVO.getCurrentRow(), sheet, emptyRow, rowFormat12);
				sheetVO.incCurrentRow();
			}

			WritableCellFormat rowFormat12Rotate = _format.setFont(times12b)
					.setAlignment(Alignment.CENTRE)
					.setVerticalAlignment(VerticalAlignment.CENTRE)
					.setOrientation(Orientation.PLUS_90)
					.getCellFormat();

			fillRow(firstLineNumber, sheet, new String[]{"Планирование", null, "Аудиторные занятия"}, rowFormat12Rotate);
			sheet.mergeCells(0, firstLineNumber, 0, sheetVO.getCurrentRow() - 1);
			sheet.mergeCells(2, firstLineNumber, 2, sheetVO.getCurrentRow() - 1);

			rowFormat12 = _format.setAlignment(Alignment.RIGHT)
					.setOrientation(Orientation.HORIZONTAL)
					.getCellFormat();
			fillRow(sheetVO.getCurrentRow(), sheet, statLength, "Итого", rowFormat12);

			rowFormat12 = _format.setAlignment(Alignment.CENTRE).getCellFormat();
			fillLoadRowPart(sheetVO.getCurrentRow(), statLength, sheet, varLength + 1, rowFormat12, timeRuleTotalLoad, _timeRulePositionMap);
			sheetVO.incCurrentRow();
		}
		//------------------------------------------------
		{
			//second part "Выполнение"
			WritableCellFormat rowFormat12 = _format.setFont(times12b)
					.setAlignment(Alignment.CENTRE)
					.setVerticalAlignment(VerticalAlignment.CENTRE)
					.setOrientation(Orientation.HORIZONTAL)
					.getCellFormat();

			int firstLineNumber = sheetVO.getCurrentRow();
			int counter = 1;
			String[] emptyRow = getEmptyString(rowLength);//new String[rowLength];
			for (int i = counter; i < minRowNumber; i++, counter++)
			{
				emptyRow[1] = String.valueOf(counter);
				fillRow(sheetVO.getCurrentRow(), sheet, emptyRow, rowFormat12);
				sheetVO.incCurrentRow();
			}
			//------------------------------------------------
			rowFormat12 = _format.setOrientation(Orientation.PLUS_90).getCellFormat();
			fillRow(firstLineNumber, sheet, new String[]{"Выполнение", null, "Аудиторные занятия"}, rowFormat12);
			sheet.mergeCells(0, firstLineNumber, 0, sheetVO.getCurrentRow() - 1);
			sheet.mergeCells(2, firstLineNumber, 2, sheetVO.getCurrentRow() - 1);
			//
			rowFormat12 = _format.setAlignment(Alignment.RIGHT)
					.setOrientation(Orientation.HORIZONTAL)
					.getCellFormat();
			fillRow(sheetVO.getCurrentRow(), sheet, statLength, "Итого", rowFormat12);

			rowFormat12 = _format.setAlignment(Alignment.CENTRE).getCellFormat();
			fillLoadRowPart(sheetVO.getCurrentRow(), statLength, sheet, varLength + 1, rowFormat12, Collections.<String, Double>emptyMap(), _timeRulePositionMap);
			sheetVO.incCurrentRow();
		}
		return timeRuleTotalLoad;
	}

	private String[] firstSheetCreateRow(int counter, PpsDocumentFirstSheetRowWrapper rowWrapper, SheetVO sheetVO)
	{
		int statLength = sheetVO.getStatLength();
		String[] res = new String[statLength];
		{
			int pos = 1;
			res[pos] = String.valueOf(counter);
			pos += 2;
			res[pos++] = rowWrapper.getDisciplineTitle();
			res[pos++] = rowWrapper.getSchoolTitle();
			res[pos++] = rowWrapper.getSpecializeCode();
			res[pos++] = rowWrapper.getEduForm();
			res[pos++] = rowWrapper.getCourse();
			res[pos++] = rowWrapper.getStudentNumber();
			res[pos] = rowWrapper.getGroupNumber();

		}
		return res;
	}

	private Double secondSheetCreateTable(SheetVO sheetVO, String ppsLoadTypeCode) throws WriteException
	{
		final int rowLength = sheetVO.getRowLength();
		final WritableSheet sheet = sheetVO.getSheet();

		WritableCellFormat rowFormat14 = _format.setFont(times14b)
				.setAlignment(Alignment.CENTRE)
				.setVerticalAlignment(VerticalAlignment.CENTRE)
				.setOrientation(Orientation.HORIZONTAL)
				.setBordered(true)
				.getCellFormat();

		WritableCellFormat rowFormat12 = _format.setFont(times12b).getCellFormat();

		WritableCellFormat rowFormat09 = _format.setFont(times09b).getCellFormat();
		//------------------------------------------------
		{
			sheet.setRowView(sheetVO.getCurrentRow(), 6 * ROW_COEFFICIENT);
			String[] labelRow = {
					"План работы преподавателя",
					"на " + getEduYear() + " учебный год:",
					"Вторая половина дня"
			};
			Integer[] labelLen = {4, 7, 3};
			fillRow(sheetVO.getCurrentRow(), sheet, labelRow, labelLen, rowFormat14);
			sheetVO.incCurrentRow();
		}
		//-----------------------------------------
		{
			String[] labelRow = new String[]{
					"Полное название кафедры",
					getCathedraFullTitle(),
			};
			Integer[] labelLen = new Integer[]{3, 11};
			fillRow(sheetVO.getCurrentRow(), sheet, labelRow, labelLen, rowFormat12);
			sheetVO.incCurrentRow();
		}
		//-----------------------------------------
		{
			sheet.setRowView(sheetVO.getCurrentRow(), 10 * ROW_COEFFICIENT);
			String[] labelRow = new String[]{
					"Ф.И.О",
					_owner.getFullFio(),
					"Ученая степень, звание:",
					"0",
					"0",
					"Должность",
					"0",
					"Выбрать нужное",
					getEmployeeStatus(),
			};
			Integer[] labelLen = new Integer[]{2, 2, 3, 1, 1, 1, 1, 1, 2};
			fillRow(sheetVO.getCurrentRow(), sheet, labelRow, labelLen, rowFormat12);
			sheetVO.incCurrentRow();
		}
		//-----------------------------------------
		{
			WritableCellFormat rowFormat12L = _format.setAlignment(Alignment.LEFT).getCellFormat();
			fillRow(sheetVO.getCurrentRow(), sheet, rowLength, "Наименование специальности по диплому (дипломам):", rowFormat12L);
			sheetVO.incCurrentRow();
		}
		//-----------------------------------------
		{
			sheet.setRowView(sheetVO.getCurrentRow(), 5 * ROW_COEFFICIENT);
			fillRow(sheetVO.getCurrentRow(), sheet, rowLength, getLoadTypeTitle(ppsLoadTypeCode), rowFormat14);
			sheetVO.incCurrentRow();
		}
		//-----------------------------------------
		{
			WritableCellFormat rowFormat12i = _format.setFont(times12i)
					.setAlignment(Alignment.CENTRE)
					.getCellFormat();

			fillRow(sheetVO.getCurrentRow(), sheet, new String[]{"ПЛАНИРОВАНИЕ на начало семестра", "ВЫПОЛНЕНИЕ на конец семестра"}, new Integer[]{10, 4}, rowFormat12i);
			sheetVO.incCurrentRow();
		}
		{
			//-------------Header----------------------
			WritableCellFormat rowFormat12Rotate = _format.setFont(times12b)
					.setOrientation(Orientation.PLUS_90)
					.getCellFormat();

			Map<Integer, CellFormat> formatMap = new HashMap<>();
			formatMap.put(2, rowFormat12);
			putMutikeyMap(formatMap, rowFormat12Rotate, 0, 1);

			sheet.setRowView(sheetVO.getCurrentRow(), 12 * ROW_COEFFICIENT);

			String[] labelRow = new String[]{
					"Семестр",
					"№.п.п.",
					"Виды планируемой работы,\nсогласно приказу",
					"Дата начала работы",
					"Дата завершения работы",
					"Единица работы",
					"Количество времени, планируемого на выполнение, в час",
					"Примечание",
					"Завершенность работы (в процентах)",
					"Количество времени, затраченного на выполнение, в час",
					"Примечание",
			};
			Integer[] labelLen = new Integer[]{1, 1, 2, 1, 1, 1, 2, 1, 1, 2, 1};
			fillRow(sheetVO.getCurrentRow(), sheet, labelRow, labelLen, formatMap, rowFormat09);
			sheetVO.incCurrentRow();
		}
		{
			//-------------Data----------------------
			Double totalLoad = 0D;
			for (Term term : Term.values())
			{
				totalLoad += secondSheetCreateTermBlock(sheetVO, term, ppsLoadTypeCode);
			}
			//-------------End Data-------------------
			final WritableCellFormat rowFormat12Right = _format.setAlignment(Alignment.RIGHT).setOrientation(Orientation.HORIZONTAL).getCellFormat();
			Map<Integer, CellFormat> formatMap = new HashMap<Integer, CellFormat>(){{put(1, rowFormat12Right);}};

			String totalLoadString = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(totalLoad);
			sheet.setRowView(sheetVO.getCurrentRow(), 5 * ROW_COEFFICIENT);

			String[] totalRow = {null, "Итого за весь учебный год:", EMPTY_FIELD, totalLoadString, EMPTY_FIELD, "0", EMPTY_FIELD};
			Integer[] totalRowLen = {1, 5, 1, 2, 2, 2, 1};
			fillRow(sheetVO.getCurrentRow(), sheet, totalRow, totalRowLen, formatMap, rowFormat12);
			sheetVO.incCurrentRow();
			return totalLoad;
		}
	}

	private Double secondSheetCreateTermBlock(SheetVO sheetVO, Term term, String ppsLoadTypeCode) throws WriteException
	{
		final Collection<PssDocumentSecondSheetRowWrapper> otherLoadRowWrapper = getDocument().getOtherLoadRowWrapper(term, ppsLoadTypeCode);
		final int minRowNumber = 14;
		WritableSheet sheet = sheetVO.getSheet();
		//-----------------------------------------
		WritableCellFormat rowFormat12 = _format.setFont(times12b)
				.setOrientation(Orientation.HORIZONTAL)
				.getCellFormat();

		final WritableCellFormat rowFormat12R = _format.setAlignment(Alignment.RIGHT).getCellFormat();
		WritableCellFormat rowFormat12Rotate = _format.setAlignment(Alignment.CENTRE).setOrientation(Orientation.PLUS_90).getCellFormat();
		//-----------------------------------------

		Integer[] labelLen = {1, 1, 2, 1, 1, 1, 2, 1, 1, 2, 1};
		String[] emptyRow = getEmptyString(labelLen.length);
		emptyRow[0] = null;
		//--------------fill table-----------------
		Integer firstLineNumber = sheetVO.getCurrentRow();
		Double totalLoad = 0D;
		int counter = 1;
		String[] labelRow = Arrays.copyOf(emptyRow, emptyRow.length);

		for (PssDocumentSecondSheetRowWrapper wrappedRow : otherLoadRowWrapper)
		{
			totalLoad += wrappedRow.getLoadAsDouble();
			labelRow[1] = String.valueOf(counter++);
			labelRow[2] = wrappedRow.getDisciplineTitle();
			labelRow[6] = wrappedRow.getLoad();
			fillRow(sheetVO.getCurrentRow(), sheet, labelRow, labelLen, rowFormat12);
			sheetVO.incCurrentRow();
		}
		while (counter < minRowNumber)
		{
			emptyRow[1] = String.valueOf(counter++);
			fillRow(sheetVO.getCurrentRow(), sheet, emptyRow, labelLen, rowFormat12);
			sheetVO.incCurrentRow();
		}

		String totalLoadString = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(totalLoad);
		Integer[] totalRowLen = {1, 5, 1, 2, 2, 2, 1};

		String termTitle = term.getTitle();

		sheet.setRowView(sheetVO.getCurrentRow(), 5 * ROW_COEFFICIENT);
		Map<Integer, CellFormat> formatMap = new HashMap<Integer, CellFormat>(){{put(1, rowFormat12R);}};
		String[] totalRow = {null, "Итого за " + termTitle + ":", EMPTY_FIELD, totalLoadString, EMPTY_FIELD, "0", EMPTY_FIELD};
		fillRow(sheetVO.getCurrentRow(), sheet, totalRow, totalRowLen, formatMap, rowFormat12);

		fillRow(firstLineNumber, sheet, new String[]{StringUtils.capitalize(termTitle)}, null, null, rowFormat12Rotate);
		sheet.mergeCells(0, firstLineNumber, 0, sheetVO.getCurrentRow());
		sheetVO.incCurrentRow();

		return totalLoad;
	}

	//getters/setters
	public PpsLoadDocumentWrapper getDocument()
	{
		return _ppsLoadDocumentWrapper;
	}

	private String getLoadTypeTitle(String typeCode)
	{
		PpsLoadType ppsLoadType = LoadManager.instance().dao().get(PpsLoadType.class, PpsLoadType.P_CODE, typeCode);
		return ppsLoadType.getTitle();
	}

	private void resizeColumns(SheetVO sheetVO, Integer[] mass, int startPos)
	{
		if (!sheetVO.isExpandable()) return;
		Integer counter = sheetVO.getDifLength();
		while (counter > 0)
		{
			Integer minNum = startPos;
			for (int i = startPos + 1; i < mass.length; i++)
			{
				if (mass[i] < mass[minNum]) minNum = i;
			}
			mass[minNum] += 1;
			counter--;
		}
	}

	public ILoadDAO getDao()
	{
		return _dao;
	}

	private int getVarLength()
	{
		return _groupData.getSum();
	}

	public String getEduYear()
	{
		return _owner.getYear().getEducationYear().getTitle();
	}

	public String getAffiliateTitle()
	{
		return _owner.getOrgUnit().getPrintTitle();
	}

	public String getCathedraFullTitle()
	{
		return _owner.getOrgUnit().getFullTitle();
	}

	public String getFileName()
	{
		return _owner.getTitle();
	}

	public String getRate()
	{
		return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(_owner.getRate());
	}

	public String getEmployeeStatus()
	{
		return StringUtils.join(_owner.getPpsEmployeeStatus(), ',');
	}

	//HELPER CLASSES
	private class SheetVO
	{
		private final int statLength; //количество колонок в неизменяемой части
		private final int varLength; //количество колонок в изменяемой части
		private final int rowLength; //действительное количество колонок
		private final int endLength;
		private final WritableSheet sheet;
		private int currentRow = 0;
		private final boolean expandable;
		private final int difLength;

		public SheetVO(int minRowLength, int statLength, int varLength, int endStatLength, WritableSheet sheet)
		{
			this.statLength = statLength;
			this.varLength = varLength;
			this.endLength = endStatLength;
			this.rowLength = Math.max(this.statLength + this.varLength + this.endLength, minRowLength); //действительное количество колонок
			this.sheet = sheet;
			expandable = this.rowLength > minRowLength;
			this.difLength = this.rowLength - minRowLength;
		}

		public int getStatLength()
		{
			return statLength;
		}

		public int getVarLength()
		{
			return varLength;
		}

		public int getRowLength()
		{
			return rowLength;
		}

		public WritableSheet getSheet()
		{
			return sheet;
		}

		public int getCurrentRow()
		{
			return currentRow;
		}

		public void incCurrentRow()
		{
			this.currentRow++;
		}

		public int getEndLength()
		{
			return endLength;
		}

		public boolean isExpandable()
		{
			return this.expandable;
		}

		public int getDifLength()
		{
			return difLength;
		}
	}
}
