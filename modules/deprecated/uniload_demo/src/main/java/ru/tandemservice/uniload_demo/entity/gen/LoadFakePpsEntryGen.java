package ru.tandemservice.uniload_demo.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniload_demo.entity.LoadFakePpsEntry;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Фиктивный ППС (демо)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class LoadFakePpsEntryGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniload_demo.entity.LoadFakePpsEntry";
    public static final String ENTITY_NAME = "loadFakePpsEntry";
    public static final int VERSION_HASH = 2143368146;
    private static IEntityMeta ENTITY_META;

    public static final String P_VACANCY_NAME = "vacancyName";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_POST = "post";
    public static final String P_STAFF_RATE_INTEGER = "staffRateInteger";

    private String _vacancyName;     // Преподаватель (наименование вакансии)
    private OrgUnit _orgUnit;     // Подразделение
    private PostBoundedWithQGandQL _post;     // Должность отнесенная к ПКГ и КУ
    private int _staffRateInteger;     // Количество штатных единиц (целое)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Преподаватель (наименование вакансии). Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getVacancyName()
    {
        return _vacancyName;
    }

    /**
     * @param vacancyName Преподаватель (наименование вакансии). Свойство не может быть null.
     */
    public void setVacancyName(String vacancyName)
    {
        dirty(_vacancyName, vacancyName);
        _vacancyName = vacancyName;
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Должность отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    @NotNull
    public PostBoundedWithQGandQL getPost()
    {
        return _post;
    }

    /**
     * @param post Должность отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    public void setPost(PostBoundedWithQGandQL post)
    {
        dirty(_post, post);
        _post = post;
    }

    /**
     * @return Количество штатных единиц (целое). Свойство не может быть null.
     */
    @NotNull
    public int getStaffRateInteger()
    {
        return _staffRateInteger;
    }

    /**
     * @param staffRateInteger Количество штатных единиц (целое). Свойство не может быть null.
     */
    public void setStaffRateInteger(int staffRateInteger)
    {
        dirty(_staffRateInteger, staffRateInteger);
        _staffRateInteger = staffRateInteger;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof LoadFakePpsEntryGen)
        {
            setVacancyName(((LoadFakePpsEntry)another).getVacancyName());
            setOrgUnit(((LoadFakePpsEntry)another).getOrgUnit());
            setPost(((LoadFakePpsEntry)another).getPost());
            setStaffRateInteger(((LoadFakePpsEntry)another).getStaffRateInteger());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends LoadFakePpsEntryGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) LoadFakePpsEntry.class;
        }

        public T newInstance()
        {
            return (T) new LoadFakePpsEntry();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "vacancyName":
                    return obj.getVacancyName();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "post":
                    return obj.getPost();
                case "staffRateInteger":
                    return obj.getStaffRateInteger();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "vacancyName":
                    obj.setVacancyName((String) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "post":
                    obj.setPost((PostBoundedWithQGandQL) value);
                    return;
                case "staffRateInteger":
                    obj.setStaffRateInteger((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "vacancyName":
                        return true;
                case "orgUnit":
                        return true;
                case "post":
                        return true;
                case "staffRateInteger":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "vacancyName":
                    return true;
                case "orgUnit":
                    return true;
                case "post":
                    return true;
                case "staffRateInteger":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "vacancyName":
                    return String.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "post":
                    return PostBoundedWithQGandQL.class;
                case "staffRateInteger":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<LoadFakePpsEntry> _dslPath = new Path<LoadFakePpsEntry>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "LoadFakePpsEntry");
    }
            

    /**
     * @return Преподаватель (наименование вакансии). Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadFakePpsEntry#getVacancyName()
     */
    public static PropertyPath<String> vacancyName()
    {
        return _dslPath.vacancyName();
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadFakePpsEntry#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Должность отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadFakePpsEntry#getPost()
     */
    public static PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> post()
    {
        return _dslPath.post();
    }

    /**
     * @return Количество штатных единиц (целое). Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadFakePpsEntry#getStaffRateInteger()
     */
    public static PropertyPath<Integer> staffRateInteger()
    {
        return _dslPath.staffRateInteger();
    }

    public static class Path<E extends LoadFakePpsEntry> extends EntityPath<E>
    {
        private PropertyPath<String> _vacancyName;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> _post;
        private PropertyPath<Integer> _staffRateInteger;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Преподаватель (наименование вакансии). Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadFakePpsEntry#getVacancyName()
     */
        public PropertyPath<String> vacancyName()
        {
            if(_vacancyName == null )
                _vacancyName = new PropertyPath<String>(LoadFakePpsEntryGen.P_VACANCY_NAME, this);
            return _vacancyName;
        }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadFakePpsEntry#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Должность отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadFakePpsEntry#getPost()
     */
        public PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> post()
        {
            if(_post == null )
                _post = new PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL>(L_POST, this);
            return _post;
        }

    /**
     * @return Количество штатных единиц (целое). Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadFakePpsEntry#getStaffRateInteger()
     */
        public PropertyPath<Integer> staffRateInteger()
        {
            if(_staffRateInteger == null )
                _staffRateInteger = new PropertyPath<Integer>(LoadFakePpsEntryGen.P_STAFF_RATE_INTEGER, this);
            return _staffRateInteger;
        }

        public Class getEntityClass()
        {
            return LoadFakePpsEntry.class;
        }

        public String getEntityName()
        {
            return "loadFakePpsEntry";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
