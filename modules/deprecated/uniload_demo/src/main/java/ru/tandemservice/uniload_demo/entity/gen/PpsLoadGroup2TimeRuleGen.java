package ru.tandemservice.uniload_demo.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniload_demo.entity.PpsLoadGroup2TimeRule;
import ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule;
import ru.tandemservice.uniload_demo.entity.catalog.PpsLoadGroup;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь группы нагрузки ППС
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PpsLoadGroup2TimeRuleGen extends EntityBase
 implements INaturalIdentifiable<PpsLoadGroup2TimeRuleGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniload_demo.entity.PpsLoadGroup2TimeRule";
    public static final String ENTITY_NAME = "ppsLoadGroup2TimeRule";
    public static final int VERSION_HASH = 1994894558;
    private static IEntityMeta ENTITY_META;

    public static final String L_TIME_RULE = "timeRule";
    public static final String L_PPS_LOAD_GROUP = "ppsLoadGroup";

    private LoadTimeRule _timeRule;     // Норма времени
    private PpsLoadGroup _ppsLoadGroup;     // Группа нагрузки ППС

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Норма времени. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public LoadTimeRule getTimeRule()
    {
        return _timeRule;
    }

    /**
     * @param timeRule Норма времени. Свойство не может быть null и должно быть уникальным.
     */
    public void setTimeRule(LoadTimeRule timeRule)
    {
        dirty(_timeRule, timeRule);
        _timeRule = timeRule;
    }

    /**
     * @return Группа нагрузки ППС. Свойство не может быть null.
     */
    @NotNull
    public PpsLoadGroup getPpsLoadGroup()
    {
        return _ppsLoadGroup;
    }

    /**
     * @param ppsLoadGroup Группа нагрузки ППС. Свойство не может быть null.
     */
    public void setPpsLoadGroup(PpsLoadGroup ppsLoadGroup)
    {
        dirty(_ppsLoadGroup, ppsLoadGroup);
        _ppsLoadGroup = ppsLoadGroup;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PpsLoadGroup2TimeRuleGen)
        {
            if (withNaturalIdProperties)
            {
                setTimeRule(((PpsLoadGroup2TimeRule)another).getTimeRule());
            }
            setPpsLoadGroup(((PpsLoadGroup2TimeRule)another).getPpsLoadGroup());
        }
    }

    public INaturalId<PpsLoadGroup2TimeRuleGen> getNaturalId()
    {
        return new NaturalId(getTimeRule());
    }

    public static class NaturalId extends NaturalIdBase<PpsLoadGroup2TimeRuleGen>
    {
        private static final String PROXY_NAME = "PpsLoadGroup2TimeRuleNaturalProxy";

        private Long _timeRule;

        public NaturalId()
        {}

        public NaturalId(LoadTimeRule timeRule)
        {
            _timeRule = ((IEntity) timeRule).getId();
        }

        public Long getTimeRule()
        {
            return _timeRule;
        }

        public void setTimeRule(Long timeRule)
        {
            _timeRule = timeRule;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof PpsLoadGroup2TimeRuleGen.NaturalId) ) return false;

            PpsLoadGroup2TimeRuleGen.NaturalId that = (NaturalId) o;

            if( !equals(getTimeRule(), that.getTimeRule()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getTimeRule());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getTimeRule());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PpsLoadGroup2TimeRuleGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PpsLoadGroup2TimeRule.class;
        }

        public T newInstance()
        {
            return (T) new PpsLoadGroup2TimeRule();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "timeRule":
                    return obj.getTimeRule();
                case "ppsLoadGroup":
                    return obj.getPpsLoadGroup();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "timeRule":
                    obj.setTimeRule((LoadTimeRule) value);
                    return;
                case "ppsLoadGroup":
                    obj.setPpsLoadGroup((PpsLoadGroup) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "timeRule":
                        return true;
                case "ppsLoadGroup":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "timeRule":
                    return true;
                case "ppsLoadGroup":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "timeRule":
                    return LoadTimeRule.class;
                case "ppsLoadGroup":
                    return PpsLoadGroup.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PpsLoadGroup2TimeRule> _dslPath = new Path<PpsLoadGroup2TimeRule>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PpsLoadGroup2TimeRule");
    }
            

    /**
     * @return Норма времени. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniload_demo.entity.PpsLoadGroup2TimeRule#getTimeRule()
     */
    public static LoadTimeRule.Path<LoadTimeRule> timeRule()
    {
        return _dslPath.timeRule();
    }

    /**
     * @return Группа нагрузки ППС. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.PpsLoadGroup2TimeRule#getPpsLoadGroup()
     */
    public static PpsLoadGroup.Path<PpsLoadGroup> ppsLoadGroup()
    {
        return _dslPath.ppsLoadGroup();
    }

    public static class Path<E extends PpsLoadGroup2TimeRule> extends EntityPath<E>
    {
        private LoadTimeRule.Path<LoadTimeRule> _timeRule;
        private PpsLoadGroup.Path<PpsLoadGroup> _ppsLoadGroup;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Норма времени. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniload_demo.entity.PpsLoadGroup2TimeRule#getTimeRule()
     */
        public LoadTimeRule.Path<LoadTimeRule> timeRule()
        {
            if(_timeRule == null )
                _timeRule = new LoadTimeRule.Path<LoadTimeRule>(L_TIME_RULE, this);
            return _timeRule;
        }

    /**
     * @return Группа нагрузки ППС. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.PpsLoadGroup2TimeRule#getPpsLoadGroup()
     */
        public PpsLoadGroup.Path<PpsLoadGroup> ppsLoadGroup()
        {
            if(_ppsLoadGroup == null )
                _ppsLoadGroup = new PpsLoadGroup.Path<PpsLoadGroup>(L_PPS_LOAD_GROUP, this);
            return _ppsLoadGroup;
        }

        public Class getEntityClass()
        {
            return PpsLoadGroup2TimeRule.class;
        }

        public String getEntityName()
        {
            return "ppsLoadGroup2TimeRule";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
