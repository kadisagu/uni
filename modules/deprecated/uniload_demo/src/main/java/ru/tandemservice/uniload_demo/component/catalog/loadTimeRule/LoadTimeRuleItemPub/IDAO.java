/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.component.catalog.loadTimeRule.LoadTimeRuleItemPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogItemPub.IDefaultCatalogItemPubDAO;
import ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule;

/**
 * @author Alexander Zhebko
 * @since 23.12.2013
 */
public interface IDAO extends IDefaultCatalogItemPubDAO<LoadTimeRule, Model>
{
}