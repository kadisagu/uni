package ru.tandemservice.uniload_demo.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniload_demo.entity.ImEduGroupLoadBase;
import ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Нормы времени (демо)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class LoadTimeRuleGen extends EntityBase
 implements INaturalIdentifiable<LoadTimeRuleGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule";
    public static final String ENTITY_NAME = "loadTimeRule";
    public static final int VERSION_HASH = -2107152891;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_COEFFICIENT = "coefficient";
    public static final String P_BASE = "base";
    public static final String P_IN_USE = "inUse";
    public static final String P_COMMENT = "comment";
    public static final String P_MANUAL_ADD_PERMIT = "manualAddPermit";
    public static final String P_TITLE = "title";
    public static final String P_COEFFICIENT_AS_DOUBLE = "coefficientAsDouble";

    private String _code;     // Системный код
    private long _coefficient;     // Коэффициент
    private ImEduGroupLoadBase _base;     // Основание
    private boolean _inUse;     // Используется
    private String _comment;     // Примечание
    private boolean _manualAddPermit = false;     // Разрешить добавлять без связи с УГС
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Коэффициент. Свойство не может быть null.
     */
    @NotNull
    public long getCoefficient()
    {
        return _coefficient;
    }

    /**
     * @param coefficient Коэффициент. Свойство не может быть null.
     */
    public void setCoefficient(long coefficient)
    {
        dirty(_coefficient, coefficient);
        _coefficient = coefficient;
    }

    /**
     * @return Основание. Свойство не может быть null.
     */
    @NotNull
    public ImEduGroupLoadBase getBase()
    {
        return _base;
    }

    /**
     * @param base Основание. Свойство не может быть null.
     */
    public void setBase(ImEduGroupLoadBase base)
    {
        dirty(_base, base);
        _base = base;
    }

    /**
     * @return Используется. Свойство не может быть null.
     */
    @NotNull
    public boolean isInUse()
    {
        return _inUse;
    }

    /**
     * @param inUse Используется. Свойство не может быть null.
     */
    public void setInUse(boolean inUse)
    {
        dirty(_inUse, inUse);
        _inUse = inUse;
    }

    /**
     * @return Примечание.
     */
    @Length(max=255)
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Примечание.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * @return Разрешить добавлять без связи с УГС. Свойство не может быть null.
     */
    @NotNull
    public boolean isManualAddPermit()
    {
        return _manualAddPermit;
    }

    /**
     * @param manualAddPermit Разрешить добавлять без связи с УГС. Свойство не может быть null.
     */
    public void setManualAddPermit(boolean manualAddPermit)
    {
        dirty(_manualAddPermit, manualAddPermit);
        _manualAddPermit = manualAddPermit;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof LoadTimeRuleGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((LoadTimeRule)another).getCode());
            }
            setCoefficient(((LoadTimeRule)another).getCoefficient());
            setBase(((LoadTimeRule)another).getBase());
            setInUse(((LoadTimeRule)another).isInUse());
            setComment(((LoadTimeRule)another).getComment());
            setManualAddPermit(((LoadTimeRule)another).isManualAddPermit());
            setTitle(((LoadTimeRule)another).getTitle());
        }
    }

    public INaturalId<LoadTimeRuleGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<LoadTimeRuleGen>
    {
        private static final String PROXY_NAME = "LoadTimeRuleNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof LoadTimeRuleGen.NaturalId) ) return false;

            LoadTimeRuleGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends LoadTimeRuleGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) LoadTimeRule.class;
        }

        public T newInstance()
        {
            return (T) new LoadTimeRule();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "coefficient":
                    return obj.getCoefficient();
                case "base":
                    return obj.getBase();
                case "inUse":
                    return obj.isInUse();
                case "comment":
                    return obj.getComment();
                case "manualAddPermit":
                    return obj.isManualAddPermit();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "coefficient":
                    obj.setCoefficient((Long) value);
                    return;
                case "base":
                    obj.setBase((ImEduGroupLoadBase) value);
                    return;
                case "inUse":
                    obj.setInUse((Boolean) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "manualAddPermit":
                    obj.setManualAddPermit((Boolean) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "coefficient":
                        return true;
                case "base":
                        return true;
                case "inUse":
                        return true;
                case "comment":
                        return true;
                case "manualAddPermit":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "coefficient":
                    return true;
                case "base":
                    return true;
                case "inUse":
                    return true;
                case "comment":
                    return true;
                case "manualAddPermit":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "coefficient":
                    return Long.class;
                case "base":
                    return ImEduGroupLoadBase.class;
                case "inUse":
                    return Boolean.class;
                case "comment":
                    return String.class;
                case "manualAddPermit":
                    return Boolean.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<LoadTimeRule> _dslPath = new Path<LoadTimeRule>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "LoadTimeRule");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Коэффициент. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule#getCoefficient()
     */
    public static PropertyPath<Long> coefficient()
    {
        return _dslPath.coefficient();
    }

    /**
     * @return Основание. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule#getBase()
     */
    public static PropertyPath<ImEduGroupLoadBase> base()
    {
        return _dslPath.base();
    }

    /**
     * @return Используется. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule#isInUse()
     */
    public static PropertyPath<Boolean> inUse()
    {
        return _dslPath.inUse();
    }

    /**
     * @return Примечание.
     * @see ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @return Разрешить добавлять без связи с УГС. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule#isManualAddPermit()
     */
    public static PropertyPath<Boolean> manualAddPermit()
    {
        return _dslPath.manualAddPermit();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule#getCoefficientAsDouble()
     */
    public static SupportedPropertyPath<Double> coefficientAsDouble()
    {
        return _dslPath.coefficientAsDouble();
    }

    public static class Path<E extends LoadTimeRule> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<Long> _coefficient;
        private PropertyPath<ImEduGroupLoadBase> _base;
        private PropertyPath<Boolean> _inUse;
        private PropertyPath<String> _comment;
        private PropertyPath<Boolean> _manualAddPermit;
        private PropertyPath<String> _title;
        private SupportedPropertyPath<Double> _coefficientAsDouble;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(LoadTimeRuleGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Коэффициент. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule#getCoefficient()
     */
        public PropertyPath<Long> coefficient()
        {
            if(_coefficient == null )
                _coefficient = new PropertyPath<Long>(LoadTimeRuleGen.P_COEFFICIENT, this);
            return _coefficient;
        }

    /**
     * @return Основание. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule#getBase()
     */
        public PropertyPath<ImEduGroupLoadBase> base()
        {
            if(_base == null )
                _base = new PropertyPath<ImEduGroupLoadBase>(LoadTimeRuleGen.P_BASE, this);
            return _base;
        }

    /**
     * @return Используется. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule#isInUse()
     */
        public PropertyPath<Boolean> inUse()
        {
            if(_inUse == null )
                _inUse = new PropertyPath<Boolean>(LoadTimeRuleGen.P_IN_USE, this);
            return _inUse;
        }

    /**
     * @return Примечание.
     * @see ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(LoadTimeRuleGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @return Разрешить добавлять без связи с УГС. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule#isManualAddPermit()
     */
        public PropertyPath<Boolean> manualAddPermit()
        {
            if(_manualAddPermit == null )
                _manualAddPermit = new PropertyPath<Boolean>(LoadTimeRuleGen.P_MANUAL_ADD_PERMIT, this);
            return _manualAddPermit;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(LoadTimeRuleGen.P_TITLE, this);
            return _title;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule#getCoefficientAsDouble()
     */
        public SupportedPropertyPath<Double> coefficientAsDouble()
        {
            if(_coefficientAsDouble == null )
                _coefficientAsDouble = new SupportedPropertyPath<Double>(LoadTimeRuleGen.P_COEFFICIENT_AS_DOUBLE, this);
            return _coefficientAsDouble;
        }

        public Class getEntityClass()
        {
            return LoadTimeRule.class;
        }

        public String getEntityName()
        {
            return "loadTimeRule";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract Double getCoefficientAsDouble();
}
