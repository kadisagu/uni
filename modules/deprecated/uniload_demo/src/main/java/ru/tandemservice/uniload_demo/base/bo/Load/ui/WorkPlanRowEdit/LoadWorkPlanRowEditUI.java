/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.base.bo.Load.ui.WorkPlanRowEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.catalog.codes.EppALoadTypeCodes;
import ru.tandemservice.uniload_demo.base.bo.Load.LoadManager;
import ru.tandemservice.uniload_demo.base.bo.Load.logic.ILoadDAO;
import ru.tandemservice.uniload_demo.base.bo.Load.util.WorkPlanRowPpsWrapper;
import ru.tandemservice.uniload_demo.entity.LoadRecordOwner;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Alexander Zhebko
 * @since 20.12.2013
 */
@Input({@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "rowId", required = true),
		@Bind(key = LoadWorkPlanRowEditUI.PUPNAG_ID, binding = LoadWorkPlanRowEditUI.PUPNAG_ID, required = true)})
public class LoadWorkPlanRowEditUI extends UIPresenter
{
	private static final IdentifiableWrapper<IEntity> SOURCE_OWNER_ORG_UNIT = new IdentifiableWrapper<>(1L, "С читающего подразделения");
	private static final IdentifiableWrapper<IEntity> SOURCE_ALL_ORG_UNIT = new IdentifiableWrapper<>(2L, "Со всех подразделений");
	private static final List<IdentifiableWrapper<IEntity>> SOURCE_FILTER_LIST = Arrays.asList(SOURCE_OWNER_ORG_UNIT, SOURCE_ALL_ORG_UNIT);

	public static final String PUPNAG_ID = "pupnagId";

	private Long _rowId;
	private Long _pupnagId;

	private Map<String, Long> _totalLoadMap;
	private Map<String, Long> _remainLoadMap;
	private Map<String, Long> _newLoadMap;

	private List<WorkPlanRowPpsWrapper> _source;

	private FormState _currentState;

	private IEntity _sourceFilter;
	private LoadRecordOwner _pps;

	@Override
	public void onComponentRefresh()
	{
		ILoadDAO dao = LoadManager.instance().dao();
		_totalLoadMap = dao.getWorkPlanRowLoad(_rowId);
		_source = dao.getWorkPlanRowPpsWrappers(_rowId);

		long remainLecture = SafeUtil.safeGet(_totalLoadMap.get(EppALoadTypeCodes.TYPE_LECTURES));
		long remainPractice = SafeUtil.safeGet(_totalLoadMap.get(EppALoadTypeCodes.TYPE_PRACTICE));
		long remainLabs = SafeUtil.safeGet(_totalLoadMap.get(EppALoadTypeCodes.TYPE_LABS));

		for (WorkPlanRowPpsWrapper wrapper : _source)
		{
			remainLecture = SafeUtil.subtract(remainLecture, wrapper.getLecture());
			remainPractice = SafeUtil.subtract(remainPractice, wrapper.getPractice());
			remainLabs = SafeUtil.subtract(remainLabs, wrapper.getLabs());
		}

		_remainLoadMap = new HashMap<>();
		_remainLoadMap.put(EppALoadTypeCodes.TYPE_LECTURES, SafeUtil.meaningValue(remainLecture));
		_remainLoadMap.put(EppALoadTypeCodes.TYPE_PRACTICE, SafeUtil.meaningValue(remainPractice));
		_remainLoadMap.put(EppALoadTypeCodes.TYPE_LABS, SafeUtil.meaningValue(remainLabs));

		_sourceFilter = SOURCE_OWNER_ORG_UNIT;
		_currentState = FormState.BASE;
	}

	@Override
	public void onBeforeDataSourceFetch(IUIDataSource dataSource)
	{
		if (dataSource.getName().equals(LoadWorkPlanRowEdit.WORK_PLAN_ROW_PPS_DS))
		{
			dataSource.put(LoadWorkPlanRowEdit.KEY_SOURCE, _source);
		}

		if (dataSource.getName().equals(LoadWorkPlanRowEdit.PPS_COMBO_DS))
		{
			dataSource.put(LoadWorkPlanRowEdit.KEY_ROW_ID, _rowId);
			dataSource.put(LoadWorkPlanRowEdit.KEY_OWNER_ORG_UNIT, _sourceFilter.equals(SOURCE_OWNER_ORG_UNIT));
		}
	}

	// getters && setters
	public Long getRowId()
	{
		return _rowId;
	}

	public void setRowId(Long rowId)
	{
		_rowId = rowId;
	}

	public IEntity getSourceFilter()
	{
		return _sourceFilter;
	}

	public void setSourceFilter(IEntity sourceFilter)
	{
		_sourceFilter = sourceFilter;
	}

	public LoadRecordOwner getPps()
	{
		return _pps;
	}

	public void setPps(LoadRecordOwner pps)
	{
		_pps = pps;
	}

	@SuppressWarnings("UnusedDeclaration")
	public List<IdentifiableWrapper<IEntity>> getSourceFilterList()
	{
		return SOURCE_FILTER_LIST;
	}

	@SuppressWarnings("UnusedDeclaration")
	public Double getTotalLecture()
	{
		return UniEppUtils.wrap(_totalLoadMap.get(EppALoadTypeCodes.TYPE_LECTURES));
	}

	public void setTotalLecture(Double totalLecture)
	{}

	@SuppressWarnings("UnusedDeclaration")
	public Double getTotalPractice()
	{
		return UniEppUtils.wrap(_totalLoadMap.get(EppALoadTypeCodes.TYPE_PRACTICE));
	}

	public void setTotalPractice(Double totalPractice)
	{}

	@SuppressWarnings("UnusedDeclaration")
	public Double getTotalLabs()
	{
		return UniEppUtils.wrap(_totalLoadMap.get(EppALoadTypeCodes.TYPE_LABS));
	}

	public void setTotalLabs(Double totalLabs)
	{}

	@SuppressWarnings("UnusedDeclaration")
	public Double getRemainLecture()
	{
		return UniEppUtils.wrap(_remainLoadMap.get(EppALoadTypeCodes.TYPE_LECTURES));
	}

	public void setRemainLecture(Double remainLecture)
	{}

	@SuppressWarnings("UnusedDeclaration")
	public Double getRemainPractice()
	{
		return UniEppUtils.wrap(_remainLoadMap.get(EppALoadTypeCodes.TYPE_PRACTICE));
	}

	public void setRemainPractice(Double remainPractice)
	{}

	@SuppressWarnings("UnusedDeclaration")
	public Double getRemainLabs()
	{
		return UniEppUtils.wrap(_remainLoadMap.get(EppALoadTypeCodes.TYPE_LABS));
	}

	public void setRemainLabs(Double remainLabs)
	{}

	@SuppressWarnings("UnusedDeclaration")
	public Double getCurrentLecture()
	{
		return UniEppUtils.wrap(getCurrentRow().getLecture());
	}

	@SuppressWarnings("UnusedDeclaration")
	public void setCurrentLecture(Double currentLecture)
	{
		getCurrentRow().setLecture(UniEppUtils.unwrap(currentLecture));
	}

	@SuppressWarnings("UnusedDeclaration")
	public Double getCurrentPractice()
	{
		return UniEppUtils.wrap(getCurrentRow().getPractice());
	}

	@SuppressWarnings("UnusedDeclaration")
	public void setCurrentPractice(Double currentPractice)
	{
		getCurrentRow().setPractice(UniEppUtils.unwrap(currentPractice));
	}

	@SuppressWarnings("UnusedDeclaration")
	public Double getCurrentLabs()
	{
		return UniEppUtils.wrap(getCurrentRow().getLabs());
	}

	@SuppressWarnings("UnusedDeclaration")
	public void setCurrentLabs(Double currentLabs)
	{
		getCurrentRow().setLabs(UniEppUtils.unwrap(currentLabs));
	}

	@SuppressWarnings("UnusedDeclaration")
	public Double getNewLecture()
	{
		return UniEppUtils.wrap(_newLoadMap.get(EppALoadTypeCodes.TYPE_LECTURES));
	}

	@SuppressWarnings("UnusedDeclaration")
	public void setNewLecture(Double newLecture)
	{
		_newLoadMap.put(EppALoadTypeCodes.TYPE_LECTURES, UniEppUtils.unwrap(newLecture));
	}

	@SuppressWarnings("UnusedDeclaration")
	public Double getNewPractice()
	{
		return UniEppUtils.wrap(_newLoadMap.get(EppALoadTypeCodes.TYPE_PRACTICE));
	}

	@SuppressWarnings("UnusedDeclaration")
	public void setNewPractice(Double newPractice)
	{
		_newLoadMap.put(EppALoadTypeCodes.TYPE_PRACTICE, UniEppUtils.unwrap(newPractice));
	}

	@SuppressWarnings("UnusedDeclaration")
	public Double getNewLabs()
	{
		return UniEppUtils.wrap(_newLoadMap.get(EppALoadTypeCodes.TYPE_LABS));
	}

	@SuppressWarnings("UnusedDeclaration")
	public void setNewLabs(Double newLabs)
	{
		_newLoadMap.put(EppALoadTypeCodes.TYPE_LABS, UniEppUtils.unwrap(newLabs));
	}

	public Long getPupnagId()
	{
		return _pupnagId;
	}

	public void setPupnagId(Long pupnagId)
	{
		_pupnagId = pupnagId;
	}

	// calculated methods
	private IUIDataSource getPpsDS()
	{
		return getConfig().getDataSource(LoadWorkPlanRowEdit.WORK_PLAN_ROW_PPS_DS);
	}

	private WorkPlanRowPpsWrapper getCurrentRow()
	{
		return ((WorkPlanRowPpsWrapper) getPpsDS().getCurrent());
	}

	@SuppressWarnings("UnusedDeclaration")
	public boolean isAddPpsState()
	{
		return _currentState == FormState.ADD_PPS;
	}

	@SuppressWarnings("UnusedDeclaration")
	public boolean isAddPpsVisible()
	{
		return _currentState == FormState.BASE;
	}

	@SuppressWarnings("UnusedDeclaration")
	public boolean isCommonState()
	{
		return _currentState == FormState.ADD_PPS;
	}

	@SuppressWarnings("UnusedDeclaration")
	public boolean isLectureDisabled()
	{
		return !hasLoad(EppALoadTypeCodes.TYPE_LECTURES);
	}

	@SuppressWarnings("UnusedDeclaration")
	public boolean isPracticeDisabled()
	{
		return !hasLoad(EppALoadTypeCodes.TYPE_PRACTICE);
	}

	@SuppressWarnings("UnusedDeclaration")
	public boolean isLabsDisabled()
	{
		return !hasLoad(EppALoadTypeCodes.TYPE_LABS);
	}

	private boolean hasLoad(String code)
	{
		return _totalLoadMap.containsKey(code);
	}

	@SuppressWarnings("UnusedDeclaration")
	public boolean isNewLectureDisabled()
	{
		return !hasLoad(EppALoadTypeCodes.TYPE_LECTURES);
	}

	@SuppressWarnings("UnusedDeclaration")
	public boolean isNewPracticeDisabled()
	{
		return !hasLoad(EppALoadTypeCodes.TYPE_PRACTICE);
	}

	@SuppressWarnings("UnusedDeclaration")
	public boolean isNewLabsDisabled()
	{
		return !hasLoad(EppALoadTypeCodes.TYPE_LABS);
	}

	@SuppressWarnings("UnusedDeclaration")
	public String getLectureId()
	{
		return EppALoadTypeCodes.TYPE_LECTURES + "_" + getCurrentRow().getId();
	}

	@SuppressWarnings("UnusedDeclaration")
	public String getPracticeId()
	{
		return EppALoadTypeCodes.TYPE_PRACTICE + "_" + getCurrentRow().getId();
	}

	@SuppressWarnings("UnusedDeclaration")
	public String getLabsId()
	{
		return EppALoadTypeCodes.TYPE_LABS + "_" + getCurrentRow().getId();
	}

	// listeners
	public void onClickApply()
	{
		ErrorCollector errorCollector = ContextLocal.getErrorCollector();
		for (Map.Entry<String, Long> loadTypeEntry : _remainLoadMap.entrySet())
		{
			if (loadTypeEntry.getValue() != null && loadTypeEntry.getValue() < 0)
			{
				String loadTypeCode = loadTypeEntry.getKey();
				String errorMessage = "Превышено число часов.";
				for (WorkPlanRowPpsWrapper wrapper : _source)
				{
					errorCollector.add(errorMessage, loadTypeCode + "_" + wrapper.getId());
				}
			}
		}
		if (errorCollector.hasErrors())
			return;

		LoadManager.instance().dao().updatePpsLoad(_rowId, _source);
		deactivate();
	}

	public void onClickAddPps()
	{
		_currentState = FormState.ADD_PPS;
		_newLoadMap = new HashMap<>();
	}

	@SuppressWarnings("UnusedDeclaration")
	public void onClickDeleteRecord()
	{
		LoadManager.instance().dao().deletePpsRecord(_rowId, getListenerParameterAsLong());
		_uiSupport.doRefresh();
	}

	public void onClickSavePps()
	{
		boolean empty = true;
		for (Long loadSize : _newLoadMap.values())
		{
			if (loadSize != null && loadSize != 0L)
			{
				empty = false;
				break;
			}
		}

		if (empty)
		{
			ContextLocal.getErrorCollector().add("Необходимы выбрать хотя бы один тип нагрузки.", "newLecture", "newPractice", "newLabs");
			return;
		}

		LoadManager.instance().dao().savePpsWorkPlanRowLoad(_rowId, _pps.getId(), _newLoadMap, getPupnagId());
		_uiSupport.doRefresh();
	}

	public void onClickCancel()
	{
		_currentState = FormState.BASE;
	}

	public void onChangeLecture()
	{
		onChangeLoadSize(EppALoadTypeCodes.TYPE_LECTURES);
	}

	public void onChangePractice()
	{
		onChangeLoadSize(EppALoadTypeCodes.TYPE_PRACTICE);
	}

	public void onChangeLabs()
	{
		onChangeLoadSize(EppALoadTypeCodes.TYPE_LABS);
	}

	private void onChangeLoadSize(String loadTypeCode)
	{
		Long loadSize = _totalLoadMap.get(loadTypeCode);
		for (WorkPlanRowPpsWrapper wrapper : _source)
		{
			loadSize = SafeUtil.subtract(loadSize, wrapper.getLoad(loadTypeCode));
		}

		_remainLoadMap.put(loadTypeCode, SafeUtil.meaningValue(loadSize));
	}

	// helpers
	private static class SafeUtil
	{
		public static Long meaningValue(Long value)
		{
			return value == null || value == 0L ? null : value;
		}

		public static long safeGet(Long unsafeValue)
		{
			return unsafeValue == null ? 0L : unsafeValue;
		}

		public static long subtract(Long unsafeValue1, Long unsafeValue2)
		{
			return safeGet(unsafeValue1) - safeGet(unsafeValue2);
		}
	}

	private enum FormState
	{
		BASE,
		ADD_PPS
	}
}