/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.base.ext.SystemAction.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import ru.tandemservice.uniepp.dao.group.EppRealGroupRowDAO;
import ru.tandemservice.uniload_demo.base.bo.Load.LoadManager;
import ru.tandemservice.uniload_demo.base.bo.Load.logic.ILoadDAO;
import ru.tandemservice.uniload_demo.base.bo.Load.ui.ResetEduGroups.LoadResetEduGroups;

/**
 * @author Alexander Zhebko
 * @since 20.12.2013
 */
public class SystemActionPubAddon extends UIAddon
{
    public SystemActionPubAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public void onClickCreateNextYearEduGroups()
    {
        synchronized (SystemActionPubAddon.class)
        {
            ILoadDAO dao = LoadManager.instance().dao();
            dao.createNextYearSummaries();
            EppRealGroupRowDAO.DAEMON.wakeUpAndWaitDaemon(5 * 60);
            dao.doBindNextYearEduGroup2WorkPlanRow();
        }
    }

	public void onClickResetEduGroups()
	{
		getActivationBuilder().asRegionDialog(LoadResetEduGroups.class).activate();
	}
}