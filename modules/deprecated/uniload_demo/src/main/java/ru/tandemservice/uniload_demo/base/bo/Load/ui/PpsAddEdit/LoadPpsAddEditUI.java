/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.base.bo.Load.ui.PpsAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.employeebase.base.bo.EmployeePost.EmployeePostManager;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.employee.pps.EmployeePost4PpsEntry;
import ru.tandemservice.uni.entity.employee.pps.PpsEntryByEmployeePost;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniload_demo.base.bo.Load.LoadManager;
import ru.tandemservice.uniload_demo.base.bo.Load.logic.ILoadDAO;
import ru.tandemservice.uniload_demo.base.bo.Load.vo.LoadRateStepVO;
import ru.tandemservice.uniload_demo.entity.LoadFakePpsEntry;
import ru.tandemservice.uniload_demo.entity.LoadRealPpsEntry;
import ru.tandemservice.uniload_demo.entity.LoadRecordOwner;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Alexander Zhebko
 * @since 20.12.2013
 */
@Input({
		@Bind(key = LoadPpsAddEditUI.PPS_ID, binding = LoadPpsAddEditUI.PPS_ID),
		@Bind(key = LoadPpsAddEditUI.ORG_UNIT_ID, binding = LoadPpsAddEditUI.ORG_UNIT_ID),
		@Bind(key = LoadPpsAddEditUI.PUPNAG_ID, binding = LoadPpsAddEditUI.PUPNAG_ID)})
public class LoadPpsAddEditUI extends UIPresenter
{
	public static final String PPS_ID = "ppsId";
	public static final String ORG_UNIT_ID = "orgUnitId";
	public static final String PUPNAG_ID = "pupnagId";

	private Long ppsId;
	private Long orgUnitId;
	private Long pupnagId;
	private LoadRecordOwner pps;

	private boolean _realPps;
	private LoadRealPpsEntry realPps;
	private LoadFakePpsEntry fakePps;

	@Override
	public void onComponentRefresh()
	{
		ILoadDAO dao = LoadManager.instance().dao();
		if (isEditForm())
		{
			pps = dao.get(LoadRecordOwner.class, ppsId);
			setRealPps(pps.isReal());
			setOrgUnitId(pps.getOrgUnit().getId());
		}
		else
		{
			pps = new LoadRecordOwner();
			pps.setYear(getPupnag());
			setRealPps(true);
			realPps  = new LoadRealPpsEntry();

			fakePps = new LoadFakePpsEntry();
			fakePps.setOrgUnit(dao.get(OrgUnit.class, getOrgUnitId()));
			onClickSwitchLoadPps();
		}
	}

	@Override
	public void onBeforeDataSourceFetch(IUIDataSource dataSource)
	{
		if (dataSource.getName().equals(LoadPpsAddEdit.PPS_DS))
		{
			dataSource.put(LoadPpsAddEdit.ORG_UNIT_ID, getOrgUnitId());
			dataSource.put(LoadPpsAddEdit.PPS_ID, pps.isReal() && pps.getRealPps().getPps() != null ? pps.getRealPps().getPps().getId() : null);
			dataSource.put(LoadPpsAddEdit.PUPNAG_ID, getPupnagId());
		}
	}

	public void onClickSwitchLoadPps()
	{
		if (isRealPps())
		{
			getPps().setFakePps(null);
			getPps().setRealPps(realPps);
		} else
		{
			getPps().setRealPps(null);
			getPps().setFakePps(fakePps);
		}
	}

	public void onClickChangeRealPps(){
		if(getPps().isTimeworker())
			getPps().setHourlyLoad(Boolean.TRUE);
	}

	public void onClickApply()
	{
		ErrorCollector errorCollector = ContextLocal.getErrorCollector();
		LoadRateStepVO instance = LoadRateStepVO.getInstance();
		Long currentRate;
		String field;

		if (isRealPps())
		{
			currentRate = UniEppUtils.unwrap(pps.getRealPps().getRateAsDouble());
			field = "realStaffRate";
		}
		else
		{
			currentRate = UniEppUtils.unwrap(pps.getFakePps().getStaffRateDouble());
			field = "fakeStaffRate";
		}

		if (currentRate > instance.getMaxRate())
			errorCollector.add("Значение в поле \"Доля ставки\" превышает максимальную ставку сотрудника", field);

		if (currentRate % instance.getRateStep() != 0)
		{
			String rateStep = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(UniEppUtils.wrap(instance.getRateStep()));
			errorCollector.add("Значение в поле \"Доля ставки\" не соответствует шагу ставки (" + rateStep + ")", field);
		}

		if (errorCollector.hasErrors()) return;

		LoadManager.instance().dao().saveOrUpdateLoadRecordOwner(pps);
		deactivate();
	}

	public void onClickToFillRate()
	{
		if (isAllowFillRate()) //realPps always PpsEntryByEmployeePost and not null
		{
			PpsEntryByEmployeePost ppsEntry = (PpsEntryByEmployeePost) pps.getRealPps().getPps();

			List<EmployeePost> employeePosts = DataAccessServices.dao().getList(EmployeePost4PpsEntry.class, EmployeePost4PpsEntry.ppsEntry(), ppsEntry)
					.stream().map(EmployeePost4PpsEntry::getEmployeePost)
					.collect(Collectors.toList());

			if (!employeePosts.isEmpty())
			{
				Long employeePostId = employeePosts.get(0).getId();
				Integer rate = EmployeePostManager.instance().employeeInfo().getRate(employeePostId);
				Boolean freelance = EmployeePostManager.instance().employeeInfo().isFreelance(employeePostId);

				pps.getRealPps().setRate(rate);
				pps.getRealPps().setFreelance(freelance);
			}
		}
	}

	//Getters/Setters
	public boolean isAllowFillRate()
	{
		return !isEditForm() && isRealPps() && pps.isEmployeePost();
	}

	public boolean isEditForm()
	{
		return ppsId != null;
	}

	public EppYearEducationProcess getPupnag()
	{
		return LoadManager.instance().dao().getNotNull(getPupnagId());
	}

	public boolean isRealPps() {
		return _realPps;
	}

	public void setRealPps(boolean realPps) {
		_realPps = realPps;
	}

	public LoadRecordOwner getPps() {
		return pps;
	}

	public Long getPupnagId() {
		return pupnagId;
	}

	public void setPupnagId(Long pupnagId) {
		this.pupnagId = pupnagId;
	}

	public Long getOrgUnitId() {
		return orgUnitId;
	}

	public void setOrgUnitId(Long orgUnitId) {
		this.orgUnitId = orgUnitId;
	}

	public Long getPpsId() {
		return ppsId;
	}

	public void setPpsId(Long ppsId) {
		this.ppsId = ppsId;
	}
}