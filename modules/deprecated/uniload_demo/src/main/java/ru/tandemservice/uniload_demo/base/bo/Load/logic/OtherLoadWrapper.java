/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.base.bo.Load.logic;

import org.tandemframework.core.entity.ViewWrapper;
import ru.tandemservice.uniload_demo.entity.OtherLoad;

import java.util.*;

/**
 * @author Alexander Zhebko
 * @since 20.12.2013
 */
public class OtherLoadWrapper extends ViewWrapper<OtherLoad>
{
	public static final String LOAD_SIZE = "loadSize";
	public static final String RESULT = "result";
	public static final String TIME_RULE_TITLE_LIST = "timeRule";

	public OtherLoadWrapper(OtherLoad otherLoad, Double loadSize, Double result, Set<String> timeRuleList)
	{
		super(otherLoad);
		setViewProperty(LOAD_SIZE, loadSize);
		List<String> timeRuleTitle = new ArrayList<>(timeRuleList);
		Collections.sort(timeRuleTitle);
		setViewProperty(TIME_RULE_TITLE_LIST, timeRuleTitle);
		setViewProperty(RESULT, result);
	}
}