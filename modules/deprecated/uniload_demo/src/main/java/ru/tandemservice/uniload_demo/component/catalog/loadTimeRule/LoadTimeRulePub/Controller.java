/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.component.catalog.loadTimeRule.LoadTimeRulePub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.uniload_demo.UniLoadDemoDefines;
import ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule;

import java.util.Arrays;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 23.12.2013
 */
public class Controller extends DefaultCatalogPubController<LoadTimeRule, Model, IDAO>
{
    @Override
    protected DynamicListDataSource<LoadTimeRule> createListDataSource(IBusinessComponent context)
    {
        Model model = getModel(context);

        final List<String> systemItemCodes = Arrays.asList(UniLoadDemoDefines.LOAD_TIME_RULE_LECTURE, UniLoadDemoDefines.LOAD_TIME_RULE_PRACTICE, UniLoadDemoDefines.LOAD_TIME_RULE_LABS);
        DynamicListDataSource<LoadTimeRule> dataSource = new DynamicListDataSource<>(context, this);
        dataSource.addColumn(getCatalogItemLinkColumn(model, "Название", LoadTimeRule.P_TITLE).setOrderable(false));

        dataSource.addColumn(new SimpleColumn("Коэффициент", LoadTimeRule.coefficientAsDouble()).setFormatter(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Основание", LoadTimeRule.base().s() + ".title").setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Примечание", LoadTimeRule.comment()).setOrderable(false));
        dataSource.addColumn(new ToggleColumn("Используется", LoadTimeRule.inUse()).setListener("onClickChangeStatus")
                                     .setDisableHandler(entity -> systemItemCodes.contains(((LoadTimeRule) entity).getCode())));
	    dataSource.addColumn(new ToggleColumn("Разрешить добавлять без связи с УГС", LoadTimeRule.manualAddPermit()).setListener("onClickChangePermission"));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem", "Удалить элемент «{0}» из справочника?", LoadTimeRule.P_TITLE).setPermissionKey(model.getCatalogItemDelete()).setDisableHandler(model.getDisabledEntityHandler()));

        return dataSource;
    }

    public void onClickChangeStatus(IBusinessComponent component)
    {
        getDao().updateTimeRuleStatus(component.<Long>getListenerParameter());
    }

	public void onClickChangePermission( IBusinessComponent component)
	{
		getDao().updateTimeRulePermission(component.<Long>getListenerParameter());
	}
}