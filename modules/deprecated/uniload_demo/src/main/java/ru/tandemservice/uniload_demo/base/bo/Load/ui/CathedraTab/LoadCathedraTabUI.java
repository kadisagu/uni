/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.base.bo.Load.ui.CathedraTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniload_demo.base.bo.Load.LoadManager;
import ru.tandemservice.uniload_demo.base.bo.Load.logic.ILoadDAO;
import ru.tandemservice.uniload_demo.base.bo.Load.ui.PpsAddEdit.LoadPpsAddEdit;
import ru.tandemservice.uniload_demo.base.bo.Load.ui.PpsAddEdit.LoadPpsAddEditUI;
import ru.tandemservice.uniload_demo.base.bo.Load.util.PpsLoadDataAggregator;
import ru.tandemservice.uniload_demo.base.bo.Load.util.print.ConsolidatedLoadDocumentBuilder;
import ru.tandemservice.uniload_demo.base.bo.Load.util.print.PpsLoadDocumentBuilder;
import ru.tandemservice.uniload_demo.entity.LoadState;
import ru.tandemservice.uniload_demo.entity.OrgUnitLoadState;

import java.util.Collection;

import static org.tandemframework.core.view.formatter.DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS;

/**
 * @author Alexander Zhebko
 * @since 20.12.2013
 */

@Input({@Bind(key = "orgUnitId", binding = "orgUnitHolder.id", required = true),
		@Bind(key = "selectedTab", binding = "selectedTab")})
@SuppressWarnings("UnusedDeclaration")
public class LoadCathedraTabUI extends UIPresenter
{
	private final OrgUnitHolder _orgUnitHolder = new OrgUnitHolder();
	private final PpsLoadDataAggregator _ppsDataSupport = new PpsLoadDataAggregator();

	private String _selectedTab = LoadCathedraTab.PPS_TAB;
	private OrgUnitLoadState _currentOrgUnitLoadState;
	private LoadState _nextLoadState;

	@Override
	public void onComponentRefresh()
	{
		_orgUnitHolder.refresh();
		setPupnag();

		if (getPupnag() != null)
		{
			ILoadDAO dao = LoadManager.instance().dao();
			Collection<Long> ppsIdList = dao.getLoadPpsIdList(getOrgUnitHolder().getId(), getPupnag().getId());
			_ppsDataSupport.refresh(getPupnag().getId(), ppsIdList);
			_currentOrgUnitLoadState = dao.getByNaturalId(new OrgUnitLoadState.NaturalId(_orgUnitHolder.getValue(), getPupnag()));
		}
	}

	@Override
	public void onBeforeDataSourceFetch(IUIDataSource dataSource)
	{
		if (LoadCathedraTab.PPS_DS.equals(dataSource.getName()))
		{
			dataSource.put(LoadCathedraTab.PPS_WRAPPERS, _ppsDataSupport.getPpsWrappers());
			dataSource.put(LoadCathedraTab.CURRENT_PUPNAG, getPupnag());

		}
	}

	//Handlers
	public void onClickSearch()
	{
		getSettings().save();
	}

	public void onClickClear()
	{
		getSettings().clear();
		setPupnag();
		onClickSearch();
	}

	public void onClickChangeLoadState()
	{
		ILoadDAO dao = LoadManager.instance().dao();
		if (_currentOrgUnitLoadState == null)
		{
			_currentOrgUnitLoadState = new OrgUnitLoadState(_orgUnitHolder.getValue(), dao.getNextYear());
			_currentOrgUnitLoadState.setLoadState(LoadState.FORMATIVE);
		}

		LoadState newLoadState = getListenerParameter();
		LoadState currentLoadState = _currentOrgUnitLoadState.getLoadState();
		if (!currentLoadState.getPossibleTransitions().contains(newLoadState))
		{
			throw new IllegalStateException("Can't change state from «" + currentLoadState.getTitle() + " » to « " + newLoadState.getTitle() + " »");
		}

		_currentOrgUnitLoadState.setLoadState(newLoadState);
		dao.saveOrUpdate(_currentOrgUnitLoadState);
	}

	public void onClickToFormativeState()
	{
		_currentOrgUnitLoadState.setLoadState(LoadState.FORMATIVE);
		LoadManager.instance().dao().saveOrUpdate(_currentOrgUnitLoadState);
	}

	public void onClickAddPps()
	{
		_uiActivation.asRegionDialog(LoadPpsAddEdit.class).parameter(LoadPpsAddEditUI.ORG_UNIT_ID, _orgUnitHolder.getId()).parameter(LoadPpsAddEditUI.PUPNAG_ID, getPupnag().getId()).activate();
	}

	@SuppressWarnings("UnusedDeclaration")
	public void onClickEditPps()
	{
		_uiActivation.asRegionDialog(LoadPpsAddEdit.class).parameter(LoadPpsAddEditUI.PPS_ID, getListenerParameterAsLong()).activate();
	}

	@SuppressWarnings("UnusedDeclaration")
	public void onClickPrintPps()
	{
		try
		{
			Long loadRecordOwnerId = getListenerParameterAsLong();
			BusinessComponentUtils.downloadDocument(new PpsLoadDocumentBuilder(loadRecordOwnerId).buildDocumentRenderer(), true);
		}
		catch (Throwable t)
		{
			throw CoreExceptionUtils.getRuntimeException(t);
		}
	}

	public void onClickPrintConsolidated()
	{
		try
		{
			BusinessComponentUtils.downloadDocument(new ConsolidatedLoadDocumentBuilder(getOrgUnitHolder().getId(), getPupnag().getId()).buildDocumentRenderer(), true);
		}
		catch (Throwable t)
		{
			throw CoreExceptionUtils.getRuntimeException(t);
		}
	}

	public void onClickDeletePps()
	{
		LoadManager.instance().dao().deleteLoadRecordOwner(this.<Long>getListenerParameter());
		_uiSupport.doRefresh();
	}

	private String employeeRatePrintFormatter(String prefix, Double... values)
	{
		StringBuilder sb = new StringBuilder(prefix).append(": ");
		if (values == null || values[0].equals(0D))
		{
			sb.append("нет");
			return sb.toString();
		}
		for (int i = 0; i < values.length; i++)
		{
			if (i == 0) sb.append(DOUBLE_FORMATTER_2_DIGITS.format(values[i]));
			else sb.append(" (").append(DOUBLE_FORMATTER_2_DIGITS.format(values[i])).append(")");
		}
		return sb.toString();
	}

	//Getters/setters
	@SuppressWarnings("UnusedDeclaration")
	public boolean isAcceptableState()
	{
		return _currentOrgUnitLoadState != null && _currentOrgUnitLoadState.getLoadState().equals(LoadState.ACCEPTABLE);
	}

	public OrgUnitHolder getOrgUnitHolder()
	{
		return _orgUnitHolder;
	}

	public CommonPostfixPermissionModelBase getSecModel()
	{
		return getOrgUnitHolder().getSecModel();
	}

	@SuppressWarnings("UnusedDeclaration")
	public boolean isActionsDisabled()
	{
		return getPupnag() == null;
	}

	public String getSelectedTab()
	{
		return _selectedTab;
	}

	public void setSelectedTab(String selectedTab)
	{
		_selectedTab = selectedTab;
	}

	@SuppressWarnings("UnusedDeclaration")
	public LoadState getNextLoadState()
	{
		return _nextLoadState;
	}

	@SuppressWarnings("UnusedDeclaration")
	public void setNextLoadState(LoadState loadState)
	{
		_nextLoadState = loadState;
	}

	private EppYearEducationProcess getPupnag()
	{
		return getSettings().get("pupnag");
	}

	private void setPupnag()
	{
		getSettings().set("pupnag", LoadManager.instance().dao().getNextYear());
	}

	public boolean isEditable()
	{
		return getCurrentLoadState() == LoadState.FORMATIVE;
	}

	public LoadState getCurrentLoadState()
	{
		return _currentOrgUnitLoadState == null ? LoadState.FORMATIVE : _currentOrgUnitLoadState.getLoadState();
	}

	@SuppressWarnings("UnusedDeclaration")
	public String getLoadStateChangeAlert()
	{
		return _nextLoadState.getActionTitle() + "?";
	}

	@SuppressWarnings("UnusedDeclaration")
	public String getChangeOrgUnitLoadStatePermission()
	{
		return getSecModel().getPermission("orgUnit_changeOrgUnitLoadState_" + _nextLoadState.toString());
	}

	@SuppressWarnings("UnusedDeclaration")
	public String getTotalEduLoadSize()
	{
		return employeeRatePrintFormatter("Учебной нагрузки", _ppsDataSupport.getTotalEduLoadSize());
	}

	@SuppressWarnings("UnusedDeclaration")
	public String getTotalEduLoadSizeWithOther()
	{
		return employeeRatePrintFormatter("Всего учебной нагрузки", _ppsDataSupport.getTotalEduLoadIncludeOtherLoad());
	}

	@SuppressWarnings("UnusedDeclaration")
	public String getTotalOtherLoadSize()
	{
		return employeeRatePrintFormatter("Всего других видов нагрузки", _ppsDataSupport.getTotalOtherLoadSize());
	}

	@SuppressWarnings("UnusedDeclaration")
	public String getTotalHourlyLoadSize()
	{
		return employeeRatePrintFormatter("Всего почасовой нагрузки", _ppsDataSupport.getTotalHourlyLoadSize());
	}

	@SuppressWarnings("UnusedDeclaration")
	public String getTotalLoadSize()
	{
		return employeeRatePrintFormatter("Всего", _ppsDataSupport.getTotalLoadSize());
	}

	@SuppressWarnings("UnusedDeclaration")
	public String getSecondJobEmployeeRateSum()
	{
		return employeeRatePrintFormatter("Количество ставок (совместительство)", _ppsDataSupport.getSecondJobEmployeeRate());
	}

	@SuppressWarnings("UnusedDeclaration")
	public String getSecondJobFreelanceEmployeeRateSum()
	{
		return employeeRatePrintFormatter("Количество ставок (совместительство, вне штата)", _ppsDataSupport.getFreelanceEmployeeRate());
	}

	public String getStaffEmployeeRateSum()
	{
		return employeeRatePrintFormatter("Количество ставок (штат)", _ppsDataSupport.getStaffEmployeeRateIncludeVacancyRate(), _ppsDataSupport.getTotalVacancyEmployeeRate());
	}

	public String getTotalEmployeeRateSum()
	{
		return employeeRatePrintFormatter("Количество ставок (всего)", _ppsDataSupport.getTotalEmployeeRate(), _ppsDataSupport.getTotalVacancyEmployeeRate());
	}

	@SuppressWarnings("UnusedDeclaration")
	public String getAverageLoad()
	{
		return employeeRatePrintFormatter("Средняя нагрузка", _ppsDataSupport.getAverageLoad());
	}
}