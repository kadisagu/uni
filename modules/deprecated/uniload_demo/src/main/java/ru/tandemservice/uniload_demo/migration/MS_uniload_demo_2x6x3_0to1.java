package ru.tandemservice.uniload_demo.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniload_demo_2x6x3_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность loadMaxSize

		// создано обязательное свойство minSize
		{
			// создать колонку
			tool.createColumn("loadmaxsize_t", new DBColumn("minsize_p", DBType.LONG));

			// задать значение по умолчанию
			java.lang.Long defaultMinSize = 0L;
			tool.executeUpdate("update loadmaxsize_t set minsize_p=? where minsize_p is null", defaultMinSize);

			// сделать колонку NOT NULL
			tool.setColumnNullable("loadmaxsize_t", "minsize_p", false);
		}

	    // size колонку переименована в maxSize
	    {
		    tool.renameColumn("loadmaxsize_t", "size_p", "maxsize_p");
	    }

	    // Сущность loadMaxSize переименована в loadSize
	    {
		    tool.renameTable("loadmaxsize_t", "loadsize_t");
		    tool.entityCodes().rename("loadMaxSize", "loadSize");
	    }

	    ////////////////////////////////////////////////////////////////////////////////
	    // сущность loadSize

	    //  свойство maxSize стало необязательным
	    {
		    // сделать колонку NULL
		    tool.setColumnNullable("loadsize_t", "maxsize_p", true);

	    }

	    //  свойство minSize стало необязательным
	    {
		    // сделать колонку NULL
		    tool.setColumnNullable("loadsize_t", "minsize_p", true);

	    }

    }
}