/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.component.catalog.loadTimeRule.LoadTimeRulePub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule;

/**
 * @author Alexander Zhebko
 * @since 23.12.2013
 */
public class DAO extends DefaultCatalogPubDAO<LoadTimeRule, Model> implements IDAO
{
    @Override
    public void updateTimeRuleStatus(Long timeRuleId)
    {
        LoadTimeRule timeRule = getNotNull(timeRuleId);
        timeRule.setInUse(!timeRule.isInUse());
        update(timeRule);
    }

	@Override
	public void updateTimeRulePermission(Long timeRuleId)
	{
		LoadTimeRule timeRule = getNotNull(timeRuleId);
		timeRule.setManualAddPermit(!timeRule.isManualAddPermit());
		update(timeRule);
	}
}