/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.base.ext.SystemAction.ui.Pub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.ui.Pub.SystemActionPub;

/**
 * @author Alexander Zhebko
 * @since 20.12.2013
 */
@Configuration
public class SystemActionPubExt extends BusinessComponentExtensionManager
{
    public static final String SYSTEM_ACTION_PUB_ADDON_NAME = "loadSystemActionPubAddon";

    @Autowired
    private SystemActionPub _systemActionPub;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_systemActionPub.presenterExtPoint())
                .addAddon(uiAddon(SYSTEM_ACTION_PUB_ADDON_NAME, SystemActionPubAddon.class))
                .create();
    }
}
