/**
 * $Id$
 */
package ru.tandemservice.uniload_demo.base.bo.Load.logic;

import com.google.common.collect.Multimap;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniload_demo.base.bo.Load.util.PpsLoadDataWrapper;
import ru.tandemservice.uniload_demo.base.bo.Load.util.WorkPlanRowPpsWrapper;
import ru.tandemservice.uniload_demo.base.bo.Load.util.print.ConsolidatedDocumentWrapper;
import ru.tandemservice.uniload_demo.base.bo.Load.util.print.PpsLoadDocumentWrapper;
import ru.tandemservice.uniload_demo.base.bo.Load.vo.LoadSizeValueObject;
import ru.tandemservice.uniload_demo.entity.ILoadImEduGroup;
import ru.tandemservice.uniload_demo.entity.LoadRecordOwner;
import ru.tandemservice.uniload_demo.entity.LoadSize;
import ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule;
import ru.tandemservice.uniload_demo.entity.catalog.PpsLoadCathedraPart;
import ru.tandemservice.uniload_demo.entity.catalog.PpsLoadGroup;
import ru.tandemservice.uniload_demo.entity.catalog.PpsLoadType;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Alexander Zhebko
 * @since 12.12.2013
 */
public interface ILoadDAO extends ICommonDAO, INeedPersistenceSupport
{

    /**
     * Проверяет, доступна ли вкладка "Нагрузка ППС" на подразделении
     *
     * @param orgUnitId Id сущности "Подразделение" ({@link OrgUnit})
     * @return доступность вкладки
     */
    boolean isVisibleOrgUnitLoadTab(Long orgUnitId);

    /**
     * Возвращает следующий учебный год (после текущего).
     *
     * @return следующий учебный год, null если следующий не задан
     */
    EppYearEducationProcess getNextYear();

    /**
     * Возвращает текущий учебный год.
     *
     * @return текущий учебный год, null если текущий не задан
     */
    EppYearEducationProcess getCurrentYear();

    /**
     * Удаляет созданные сводки контингента на следующий год, создает новые.
     */
    void createNextYearSummaries();

    /**
     * Обновляет статус УГС следующего года.
     */
    void updateNextYearEduGroupsCompleteLevel();

    /**
     * Возвращает справочник с нагрузкой ППС.
     *
     * @param loadPpsIds Id сущности "ППС(нагрузки)" {@link LoadRecordOwner} , null если нужны все
     * @return справочники с нагрузкой ППС указанной
     */
    Map<Long, PpsLoadDataWrapper> getPpsLoadDataWrapperMap(Collection<Long> loadPpsIds);

    /**
     * Возвращает информацию о нагрузке преподавателя.
     *
     * @param loadPpsId Id сущности "ППС(нагрузки)" {@link LoadRecordOwner}
     * @return информацию о нагрузке преподавателя
     */
    PpsLoadDataWrapper getPpsLoadDataWrapper(Long loadPpsId);

    /**
     * Удаляет учебную группу преподавателя.
     *
     * @param ppsId      id преподавателя
     * @param eduGroupId id группы
     */
    void deleteEduGroupDistr(Long ppsId, Long eduGroupId);

    /**
     * Сохраняет выбранные нормы времени, которые следует учитывать при подсчете нагрузки данной планируемой УГС.
     *
     * @param eduGroupId id планируемой УГС
     * @param timeRules  список норм времени
     */
    void saveImEduGroupTimeRules(Long eduGroupId, Collection<LoadTimeRule> timeRules);


    /**
     * Возвращает максимальное количество часов ("сырых") нагрузки данного типа для данной строки РУП.
     *
     * @param workPlanRowId id строки РУП
     * @param loadType      тип аудиторной нагрузки
     * @return максимальное количество часов нагрузки данного типа для данной строки РУП
     */
    Long getFirstCourseEduGroupMaxHoursUnwrapped(Long workPlanRowId, EppALoadType loadType);

    /**
     * Возвращает распределенные часы ("сырые") нагрузки данного типа для данной строки РУП. Указанная запись нагрузки на первый курс не учитывается.
     *
     * @param workPlanRowId id строки РУП
     * @param eduGroupId    id запись нагрузки первого курса, null если запись в расчет не берется
     * @return распределенные часы нагрузки данного типа для данной строки РУП
     */

    Long getFirstCourseEduGroupUsedHoursUnwrapped(Long workPlanRowId, @Nullable Long eduGroupId);

    /**
     * Возвращает нераспределенные часы ("сырые") нагрузки данного типа для данной строки РУП. Указанная запись нагрузки на первый курс не учитывается.
     *
     * @param workPlanRowId id строки РУП
     * @param loadType      тип аудиторной нагрузки
     * @param eduGroupId    id запись нагрузки первого курса, null если запись в расчет не берется
     * @return нераспределенные часы нагрузки данного типа для данной строки РУП
     */
    Long getFirstCourseEduGroupLeftHoursUnwrapped(Long workPlanRowId, EppALoadType loadType, @Nullable Long eduGroupId);

    /**
     * Привязывает в УГС следующего учебного года строки РУП, на основании которых они были сформированы (по МСРП).
     */
    void doBindNextYearEduGroup2WorkPlanRow();

    /**
     * Нагрузка строки РУП.
     *
     * @param workPlanRowId id строки РУП
     * @return справочник типа нагрузки на число часов
     */
    Map<String, Long> getWorkPlanRowLoad(Long workPlanRowId);

    /**
     * Список оберток ППС на форме редактирования часов строки РУП.
     *
     * @param workPlanRowId id строки РУП
     * @return список ППС
     */
    List<WorkPlanRowPpsWrapper> getWorkPlanRowPpsWrappers(Long workPlanRowId);

    /**
     * Сохраняет новые значения распределения нагрузки ППС на форме троки РУП.
     *
     * @param workPlanRowId id строки РУП
     * @param wrappers      список ППС
     */
    void updatePpsLoad(Long workPlanRowId, List<WorkPlanRowPpsWrapper> wrappers);

    /**
     * Удаляет нагрузку ППС, связанную с указанной строкой РУП.
     *
     * @param workPlanRowId id строки РУП
     * @param loadPpsId     id ППС (нагрузка)
     */
    void deletePpsRecord(Long workPlanRowId, Long loadPpsId);

    /**
     * Сохраняет записи о нагрузке ППС по строке РУП.
     *
     * @param workPlanRowId id строки РУП
     * @param loadPpsId     id ППС (нагрузка)
     * @param loadMap       справочник сохраняемой нагрузки по типам
     * @param yearId        id учебный год
     */
    void savePpsWorkPlanRowLoad(Long workPlanRowId, Long loadPpsId, Map<String, Long> loadMap, Long yearId);

    /**
     * Проверяет, доступно ли редактирование строк рабочего плана на данном подразделении.
     *
     * @param orgUnitId id подразделение
     * @return доступно ли редактирование строк
     */
    boolean isOrgUnitWorkPlanRowEditDisabled(Long orgUnitId);

    /**
     * Проверяет, доступно ли Форма добавления и редактирования записей нагрузки ППС.
     *
     * @param ppsId id ППС
     * @return доступно ли Форма добавления и редактирования записей
     */
    boolean isPpsLoadRecordAddEditDisabled(Long ppsId);

    /**
     * Возвращает список id ППС подразделения (действительные и вакансии).
     *
     * @param orgUnitId Id сущности "Подразделение" ({@link OrgUnit})
     * @param pupnagId Id сущности "Начало учебного года" ({@link EppYearEducationProcess})
     * @return список id ППС подразделения
     */
    Collection<Long> getLoadPpsIdList(Long orgUnitId, Long pupnagId);

    /**
     * Сохраняет (обновляет) ППС нагрузки.
     *
     * @param pps ППС
     */
    void saveOrUpdateLoadRecordOwner(LoadRecordOwner pps);

    /**
     * Удаляет ППС нагрузки.
     *
     * @param recordOwnerId id ППС нагрузки
     */
    void deleteLoadRecordOwner(Long recordOwnerId);

    /**
     * Сохраняет (обновляет) значение максимальной нагрузки ППС по годам.
     *
     * @param pupnagId id ПУПнаГ
     * @param dataMap  справочник должность на значение нагрузки (подготовленное)
     */
    void saveOrUpdateLoadMaxSize(@NotNull Long pupnagId, Map<PostBoundedWithQGandQL, LoadSizeValueObject> dataMap);

    /**
     * Сбрасывает статус согласования для учебных групп заданного учебного года и
     * Обновлять поле "studentGroupTitle" для записей студентов в УГС (выбранного учебного года) по актуальным наименованиям академических групп
     *
     * @param pupnagId учебного года для которого нужно произвести сброс
     */
    void doResetCurrentYearEduGroup(@NotNull Long pupnagId);

    /**
     * Получает все связи видов учебной группы (видов нагрузки) с нормами времени за указанный год
     *
     * @param pupnagId id ПУПнаГ
     * @return мультимап {@code key} - виды учебной группы (виды нагрузки), {@code value} - нормы времени
     */
    Multimap<EppGroupType, LoadTimeRule> getTimeRule2EduGroupTypeMultimap(@NotNull Long pupnagId);

    /**
     * Обновляет связи видов учебной группы (видов нагрузки) с нормами времени за указанный год
     *
     * @param pupnagId Id сущности "Начало учебного года" ({@link EppYearEducationProcess})
     * @param newValues новое состояние связей видов учебной группы (видов нагрузки) с нормами времени
     */
    void updateTimeRule2EduGroupTypeRel(@NotNull Long pupnagId, Multimap<EppGroupType, LoadTimeRule> newValues);

    /**
     * Возвращает нормы времени связанные с заданным видом учебной группы (видом нагрузки) за указанный год
     *
     * @param groupType вид учебной группы (вид нагрузки)
     * @param pupnagId Id сущности "Начало учебного года" ({@link EppYearEducationProcess})
     * @return коллекция норм времени
     */
    Collection<LoadTimeRule> getTimeRule4EduGroupType(@NotNull EppGroupType groupType, @NotNull Long pupnagId);

    /**
     * Получить запись ППС (нагрузки) по id записи в реестре ППС или id фиктивного ППС и учебному году
     *
     * @param ppsId    Id Фиктивного ППС (демо) или записи в реестре ППС
     * @param pupnagId Id ПУПнаГ
     * @return запись ППС (нагрузки)
     */
    LoadRecordOwner getLoadRecordOwner(@NotNull Long ppsId, @NotNull Long pupnagId);

    /**
     * Возвращает нормы времени связанные с заданным типом нагрузки ППС
     *
     * @return мультимап {@code key} - тип нагрузки ППС, {@code value} - нормы времени
     */
    Multimap<PpsLoadType, LoadTimeRule> getPpsLoadType2TimeRuleMultimap();

    /**
     * Возвращает нормы времени связанные с заданной группой нагрузки ППС
     *
     * @return мультимап {@code key} - группа нагрузки ППС, {@code value} - нормы времени
     */
    Multimap<PpsLoadGroup, LoadTimeRule> getPpsLoadGroup2TimeRuleMultimap();

    /**
     * Возвращает коды нормы времени связанные с заданной группой нагрузки ППС
     *
     * @return мультимап {@code key} - код группы нагрузки ППС, {@code value} - коды нормы времени
     */
    Multimap<String, String> getPpsLoadGroup2TimeRuleCodeMultimap();

    /**
     * Возвращает структуры ГОС или УП соответствующие разделам нагрузки кафедры
     *
     * @return мультимап {@code key} - раздел нагрузки кафедры, {@code value} - структуры ГОС или УП
     */
    Multimap<PpsLoadCathedraPart, EppPlanStructure> getCathedraPart2PlanStructureMultimap();

    /**
     * Обновляет связи типов нагрузки ППС с нормами времени
     *
     * @param newValues новое состояние типов нагрузки ППС с нормами времени
     */
    void updatePpsLoadType2TimeRule(Multimap<PpsLoadType, LoadTimeRule> newValues);

    /**
     * Обновляет связи групп нагрузки ППС с нормами времени
     *
     * @param newValues новое состояние групп нагрузки ППС с нормами времени
     */
    void updatePpsLoadGroup2TimeRule(Multimap<PpsLoadGroup, LoadTimeRule> newValues);

    /**
     * Обновляет связи разделов нагрузки кафедры со структурами ГОС или УП
     *
     * @param newValues новое состояние разделов нагрузки кафедры со структурами ГОС или УП
     */
    void updateCathedraPart2PlanStructureMultimap(Multimap<PpsLoadCathedraPart, EppPlanStructure> newValues);

    /**
     * Возвращает информацию о том является ли преподаватель сотрудником по совместительству
     *
     * @param ppsIdList список id записей в реестре ППС
     * @return связный список:<br/>{@code key} - id Записи в реестре ППС (только для реальных персон);<br/>
     * {@code value} вляется ли преподаватель совместителем - {@code true} -является;  {@code false} - неявляется;
     */
    Map<Long, Boolean> getPpsSecondJobInfoMap(Collection<Long> ppsIdList);

    /**
     * Возвращает id студентов привязанных к УГС
     *
     * @param eduGroupId id учебной группы студентов
     * @return список уникальных id
     */
    Collection<Long> getEduGroupStudentsId(Long eduGroupId);

    /**
     * Возвращает список направления подготовки подразделения
     *
     * @param orgUnitId Id сущности "Подразделение" ({@link OrgUnit})
     * @return список направлений подготовки для заданного подразделения
     */
    List<String> getEduLevelHighSchoolTitleList(Long orgUnitId);

    /**
     * Создает словарь сопоставления названий норм времени к их кодам
     *
     * @param timeRuleCodes коды норм времени
     * @return {@code key} - код нормы времени, {@code value} - название нормы времени
     */
    Map<String, String> getTimeRuleCode2TitleMap(Collection<String> timeRuleCodes);

    /**
     * Подготавливает данные для печати сводного отчета по нагрузке преподавателя
     *
     * @param loadRecordOwnerId id ППС (нагрузка)
     * @return данные для печати отчета
     */
    PpsLoadDocumentWrapper preparePpsLoadDocumentWrapper(Long loadRecordOwnerId);

    /**
     * Подготавливает данные для печати сводного отчета по нагрузке кафедры
     *
     * @param pupnagId Id сущности "Начало учебного года" ({@link EppYearEducationProcess})
     * @param orgUnitId Id сущности "Подразделение" ({@link OrgUnit})
     * @return данные для печати отчета
     */
    ConsolidatedDocumentWrapper prepareConsolidatedDocumentWrapper(Long pupnagId, Long orgUnitId);

    /**
     * Поиск всех норм времени связанных с планируемой УГС (демо)
     *
     * @param loadImEdugroupId Id сущности "Планируемая УГС (демо)" ({@link ILoadImEduGroup})
     * @return список всех связанных норм времени
     */
    Collection<LoadTimeRule> getTimeRules4EduGroup(Long loadImEdugroupId);

    /**
     * Вычисляет суммарную нагрузку для  планируемой УГС (демо)
     *
     * @param loadImEdugroup Планируемая УГС (демо)
     * @return суммарная нагрузка
     */
    Double getTotalEduLoad4EduGroup(ILoadImEduGroup loadImEdugroup);

    /**
     * Вычисляет нагрузку для  планируемой УГС (демо) с учётом норм времени
     *
     * @param loadImEdugroup Планируемая УГС (демо)
     * @param timeRules      норма времени
     * @return нагрузка
     */
    Double getEduLoad4EduGroupAsDouble(ILoadImEduGroup loadImEdugroup, LoadTimeRule timeRules);

    /**
     * Подсчёт нагрузки для планируемой УГС (демо)
     *
     * @param loadImEdugroup Планируемая УГС (демо)
     * @param timeRule       норма времени соответствующая планируемой УГС
     * @return нагрузка в формате {@code Long}. Для перевода в {@code Double} использовать {@link ru.tandemservice.uniepp.UniEppUtils#wrap(long) UniEppUtils.wrap()} method
     */
    Long getEduLoad4EduGroup(ILoadImEduGroup loadImEdugroup, LoadTimeRule timeRule);

    /**
     * Определяет является ли подразделение школой
     *
     * @param orgUnit подразделение для проверки
     * @return {@code true} - является, {@code false} - не является
     */
    boolean isSchool(OrgUnit orgUnit);

    /**
     * Получить список отклонений для подразделения в заданный год
     *
     * @param orgUnitId Id сущности "Подразделение" ({@link OrgUnit})
     * @param pupnagId Id сущности "Начало учебного года" ({@link EppYearEducationProcess})
     * @return Отклонения обернутые во враппер
     */
    List<DeviationViewWrapper> getDeviationWrapperList(Long orgUnitId, Long pupnagId);

    /**
     * Найти максимальную и минимальную нагрузку ППС для заданного учебного года и должности
     *
     * @param pupnagId Id сущности "Начало учебного года" ({@link EppYearEducationProcess})
     * @param postId   Id сущности "Должность или профессия отнесенная к ПКГ и КУ" ({@link PostBoundedWithQGandQL})
     * @return максимальную и минимальную нагрузку ППС для заданного учебного года и должности.<br/>
     * Если нагрузка не найдена, то возвращает {@code null}<br/>
     * Если {@code postId} равен {@code null} - возвращает {@code null}<br/>
     */
    @Nullable
    LoadSize getLoadSize(Long pupnagId, @Nullable Long postId);
}