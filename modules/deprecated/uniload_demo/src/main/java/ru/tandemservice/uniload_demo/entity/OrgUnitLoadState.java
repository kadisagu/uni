package ru.tandemservice.uniload_demo.entity;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniload_demo.entity.gen.*;

/**
 * Состояние нагрузки (подразделение)
 */
public class OrgUnitLoadState extends OrgUnitLoadStateGen
{
    public OrgUnitLoadState()
    {

    }

    public OrgUnitLoadState(OrgUnit orgUnit, EppYearEducationProcess year)
    {
        this.setOrgUnit(orgUnit);
        this.setYear(year);
    }
}