/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.base.bo.Load.logic;

import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniload_demo.base.bo.Load.LoadManager;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 20.12.2013
 */
public class SpecialPupnagDSHandler extends EntityComboDataSourceHandler
{
    public SpecialPupnagDSHandler(String ownerId)
    {
        super(ownerId, EppYearEducationProcess.class);
    }

    @Override
    protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
    {
        dql.where(eq(property("e"), value(LoadManager.instance().dao().getNextYear())));
    }
}