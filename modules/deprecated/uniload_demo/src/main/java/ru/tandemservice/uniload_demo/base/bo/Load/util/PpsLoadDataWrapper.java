/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.base.bo.Load.util;

import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import ru.tandemservice.uniload_demo.base.bo.Load.logic.EduLoadWrapper;
import ru.tandemservice.uniload_demo.base.bo.Load.logic.FirstCourseImEduGroupViewWrapper;
import ru.tandemservice.uniload_demo.base.bo.Load.logic.OtherLoadWrapper;

import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 20.12.2013
 */
public class PpsLoadDataWrapper
{
	private List<EduLoadWrapper> _eduLoadRows;
    private List<OtherLoadWrapper> _otherLoadRows;
    private List<FirstCourseImEduGroupViewWrapper> _firstCourseImEduGroups;
//	private PostBoundedWithQGandQL _post;
	private Long  _postId;

    private Long _totalEduLoad;
    private Long _eduLoad;
	private Long _firstCourseLoad;

    private Long _totalOtherLoad;
    private Long _otherEduLoad;
    private Long _otherNonEduLoad;

	private Long _totalLoad;

	public PpsLoadDataWrapper(PostBoundedWithQGandQL post, List<EduLoadWrapper> eduLoadRows, List<OtherLoadWrapper> otherLoadRows, List<FirstCourseImEduGroupViewWrapper> firstCourseImEduGroups, Long eduLoad, Long firstCourseLoad, Long otherNonEduLoad, Long otherEduLoad)
	{
		_postId = post == null ? null : post.getId();
		_eduLoadRows = eduLoadRows;
		_otherLoadRows = otherLoadRows;
		_firstCourseImEduGroups = firstCourseImEduGroups;

		_eduLoad = eduLoad == null ? 0L : eduLoad;
		_firstCourseLoad = firstCourseLoad == null ? 0L : firstCourseLoad;

		_otherEduLoad = otherEduLoad  == null ? 0L : otherEduLoad;
		_otherNonEduLoad = otherNonEduLoad  == null ? 0L : otherNonEduLoad;

		_totalEduLoad = _eduLoad + _firstCourseLoad + _otherEduLoad;
		_totalOtherLoad = _otherEduLoad + _otherNonEduLoad;

		_totalLoad = _totalEduLoad + _otherNonEduLoad;
	}

	public List<EduLoadWrapper> getEduLoadRows()
	{
		return _eduLoadRows;
	}

	public List<OtherLoadWrapper> getOtherLoadRows()
	{
		return _otherLoadRows;
	}

	public List<FirstCourseImEduGroupViewWrapper> getFirstCourseImEduGroups()
	{
		return _firstCourseImEduGroups;
	}

	public Long getTotalEduLoad()
	{
		return _totalEduLoad;
	}

	public Long getTotalOtherLoad()
	{
		return _totalOtherLoad;
	}

	public Long getOtherEduLoad()
	{
		return _otherEduLoad;
	}

	public Long getFirstCourseLoad()
	{
		return _firstCourseLoad;
	}

	public Long getPostId(){
		return _postId;
	}

	public Long getEduLoad()
	{
		return _eduLoad;
	}

	public Long getOtherNonEduLoad()
	{
		return _otherNonEduLoad;
	}

	public Long getTotalLoad()
	{
		return _totalLoad;
	}
}