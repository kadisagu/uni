/*$Id$*/
package ru.tandemservice.uniload_demo.base.bo.Load.util.print;

import ru.tandemservice.uniload_demo.entity.OtherLoad;

import java.util.HashMap;
import java.util.Map;

/**
 * @author DMITRY KNYAZEV
 * @since 02.06.2015
 */
class ConsolidatedDocumentPostgraduateRowWrapper
{
    private final String title;
    //#13-32 Нагрузка в часах
    private final Map<String, Double> rowLoadMap;
    //#33 всего часов
    private final Double rowTotalLoad;

    public ConsolidatedDocumentPostgraduateRowWrapper(OtherLoad imEduGroup)
    {
        title = imEduGroup.getTitle();
        rowLoadMap = new HashMap<>();
        rowTotalLoad = LoadDocumentPrintSharedUtils.addEduGroupLoadToLoadMap(imEduGroup,rowLoadMap);
    }

    public Map<String, Double> getRowLoadMap()
    {
        HashMap<String, Double> totalLoad = new HashMap<>(rowLoadMap);
        totalLoad.put(PpsDocumentFirstSheetRowWrapper.ROW_TOTAL_LOAD_CODE, getRowTotalLoad());
        return totalLoad;
    }

    public Double getRowTotalLoad()
    {
        return rowTotalLoad;
    }

    public String getTitle()
    {
        return title;
    }
}
