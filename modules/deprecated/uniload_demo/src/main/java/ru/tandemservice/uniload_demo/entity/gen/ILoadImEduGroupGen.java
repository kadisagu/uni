package ru.tandemservice.uniload_demo.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniload_demo.entity.ILoadImEduGroup;
import ru.tandemservice.uniload_demo.entity.LoadRecordOwner;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import ru.tandemservice.uniload_demo.entity.ILoadImEduGroup;

/**
 * Планируемая УГС (демо)
 */
@SuppressWarnings({"all", "unchecked", "unused"})
public abstract class ILoadImEduGroupGen extends InterfaceStubBase
 implements ILoadImEduGroup{
    public static final int VERSION_HASH = 859647147;

    public static final String L_OWNER = "owner";
    public static final String P_HOURS = "hours";
    public static final String P_STUDENTS = "students";

    private LoadRecordOwner _owner;
    private long _hours;
    private int _students;

    @NotNull

    public LoadRecordOwner getOwner()
    {
        return _owner;
    }

    public void setOwner(LoadRecordOwner owner)
    {
        _owner = owner;
    }

    @NotNull

    public long getHours()
    {
        return _hours;
    }

    public void setHours(long hours)
    {
        _hours = hours;
    }

    @NotNull

    public int getStudents()
    {
        return _students;
    }

    public void setStudents(int students)
    {
        _students = students;
    }

    private static final Path<ILoadImEduGroup> _dslPath = new Path<ILoadImEduGroup>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ru.tandemservice.uniload_demo.entity.ILoadImEduGroup");
    }
            

    /**
     * @return ППС (нагрузка). Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.ILoadImEduGroup#getOwner()
     */
    public static LoadRecordOwner.Path<LoadRecordOwner> owner()
    {
        return _dslPath.owner();
    }

    /**
     * @return Часы. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.ILoadImEduGroup#getHours()
     */
    public static PropertyPath<Long> hours()
    {
        return _dslPath.hours();
    }

    /**
     * @return Студенты. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.ILoadImEduGroup#getStudents()
     */
    public static PropertyPath<Integer> students()
    {
        return _dslPath.students();
    }

    public static class Path<E extends ILoadImEduGroup> extends EntityPath<E>
    {
        private LoadRecordOwner.Path<LoadRecordOwner> _owner;
        private PropertyPath<Long> _hours;
        private PropertyPath<Integer> _students;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ППС (нагрузка). Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.ILoadImEduGroup#getOwner()
     */
        public LoadRecordOwner.Path<LoadRecordOwner> owner()
        {
            if(_owner == null )
                _owner = new LoadRecordOwner.Path<LoadRecordOwner>(L_OWNER, this);
            return _owner;
        }

    /**
     * @return Часы. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.ILoadImEduGroup#getHours()
     */
        public PropertyPath<Long> hours()
        {
            if(_hours == null )
                _hours = new PropertyPath<Long>(ILoadImEduGroupGen.P_HOURS, this);
            return _hours;
        }

    /**
     * @return Студенты. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.ILoadImEduGroup#getStudents()
     */
        public PropertyPath<Integer> students()
        {
            if(_students == null )
                _students = new PropertyPath<Integer>(ILoadImEduGroupGen.P_STUDENTS, this);
            return _students;
        }

        public Class getEntityClass()
        {
            return ILoadImEduGroup.class;
        }

        public String getEntityName()
        {
            return "ru.tandemservice.uniload_demo.entity.ILoadImEduGroup";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
