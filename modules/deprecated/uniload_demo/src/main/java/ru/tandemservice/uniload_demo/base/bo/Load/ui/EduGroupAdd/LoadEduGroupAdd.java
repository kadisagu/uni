/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.base.bo.Load.ui.EduGroupAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniload_demo.base.bo.Load.LoadManager;
import ru.tandemservice.uniload_demo.entity.ImEduGroupLoadBase;
import ru.tandemservice.uniload_demo.entity.LoadEduGroupPpsDistr;
import ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule;

import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 20.12.2013
 */
@Configuration
public class LoadEduGroupAdd extends BusinessComponentManager
{
	public static final String EDU_GROUP_DS = "eduGroupDS";
	public static final String TIME_RULE_DS = "timeRuleDS";

	public static final String PPS_ID = "ppsId";
	public static final String CURRENT = "current";
	public static final String ORG_UNIT_ID = "orgUnitId";

	public static final String LOAD_BASE_EXCLUDE = "exclude";
	public static final String TIME_RULE_INCLUDE = "include";


	@Override
	@Bean
	public PresenterExtPoint presenterExtPoint()
	{
		return presenterExtPointBuilder()
				.addDataSource(selectDS(EDU_GROUP_DS, eduGroupDSHandler())
						               .addColumn("title")
						               .addColumn(EppRealEduGroup.activityPart().registryElement().title().s())
						               .addColumn(EppRealEduGroup.type().title().s())
				)
				.addDataSource(selectDS(TIME_RULE_DS, timeRuleDSHandler()))
				.create();
	}

	@Bean
	public IDefaultComboDataSourceHandler eduGroupDSHandler()
	{
		return new EntityComboDataSourceHandler(getName(), EppRealEduGroup.class)
		{
			@Override
			protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
			{
				super.applyWhereConditions(alias, dql, context);
				Long orgUnitId = context.get(ORG_UNIT_ID);
				Long currentId = context.get(CURRENT);
				Long ppsId = context.get(PPS_ID);

				if (orgUnitId != null)
					dql.where(eq(property(alias, EppRealEduGroup.activityPart().registryElement().owner().id()), value(orgUnitId)));

				DQLSelectBuilder groupsBuilder = new DQLSelectBuilder()
						.fromEntity(EppPpsCollectionItem.class, "ci")
						.joinEntity("ci", DQLJoinType.inner, LoadEduGroupPpsDistr.class, "distr", eq(property("distr", LoadEduGroupPpsDistr.owner().id()), property("ci", EppPpsCollectionItem.id())))
						.where(eq(property("ci", EppPpsCollectionItem.pps().id()), value(ppsId)))
						.column(property("ci", EppPpsCollectionItem.list().id()));

				IDQLExpression inStatement = notIn(property(alias, EppRealEduGroup.id()), groupsBuilder.buildQuery());
				if (currentId != null)
					inStatement = or(inStatement, eq(property(alias, EppRealEduGroup.id()), value(currentId)));

				EppYearEducationProcess nextYear = LoadManager.instance().dao().getNextYear();
				inStatement = and(inStatement, eq(property(alias, EppRealEduGroup.summary().yearPart().year()), value(nextYear)));

				dql.where(inStatement);
			}

			@Override
			protected DQLSelectBuilder query(String alias, String filter)
			{
				return super.query(alias, filter).where(likeUpper(DQLFunctions.concat(property(alias, EppRealEduGroup.title()), property(alias, EppRealEduGroup.activityPart().registryElement().title())), value(CoreStringUtils.escapeLike(filter, true))));
			}
		};
	}

	@Bean
	public IDefaultComboDataSourceHandler timeRuleDSHandler()
	{
		return new EntityComboDataSourceHandler(getName(), LoadTimeRule.class)
		{
			@Override
			protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
			{
				super.applyWhereConditions(alias, dql, context);

				ImEduGroupLoadBase loadBase = context.get(LOAD_BASE_EXCLUDE);
				Collection<LoadTimeRule> addedRules = context.get(TIME_RULE_INCLUDE);

				if (addedRules != null)
					dql.where(or(in(property(alias, LoadTimeRule.id()), addedRules),
					             and(ne(property(alias, LoadTimeRule.P_BASE), value(loadBase)), eq(property(alias, LoadTimeRule.P_MANUAL_ADD_PERMIT), value(Boolean.TRUE)))));
				else
					dql.where(and(ne(property(alias, LoadTimeRule.P_BASE), value(loadBase)), eq(property(alias, LoadTimeRule.P_MANUAL_ADD_PERMIT), value(Boolean.TRUE))));

				dql.where(eq(property(alias, LoadTimeRule.P_IN_USE), value(Boolean.TRUE)));
			}
		}.filter(LoadTimeRule.title());
	}
}