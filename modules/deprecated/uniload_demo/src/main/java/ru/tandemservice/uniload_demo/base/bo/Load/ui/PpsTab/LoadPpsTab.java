/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.base.bo.Load.ui.PpsTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.column.IPublisherColumnBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.RowCollectionFormatter;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import ru.tandemservice.uniepp.IUniEppDefines;
import ru.tandemservice.uniepp.component.edugroup.EppEduGroupPublisherLinkResolver;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniload_demo.base.bo.Load.logic.EduLoadWrapper;
import ru.tandemservice.uniload_demo.base.bo.Load.logic.FirstCourseImEduGroupViewWrapper;
import ru.tandemservice.uniload_demo.base.bo.Load.logic.OtherLoadWrapper;
import ru.tandemservice.uniload_demo.base.bo.Load.logic.SpecialPupnagDSHandler;
import ru.tandemservice.uniload_demo.entity.LoadFirstCourseImEduGroup;
import ru.tandemservice.uniload_demo.entity.OtherLoad;

import java.util.Collections;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 20.12.2013
 */
@Configuration
@SuppressWarnings("SpringFacetCodeInspection")
public class LoadPpsTab extends BusinessComponentManager
{
	public static final String EDU_LOAD_DS = "eduLoadDS";
	public static final String FIRST_COURSE_EDU_LOAD_DS = "firstCourseEduLoadDS";
	public static final String OTHER_LOAD_DS = "otherLoadDS";
	public static final String PUPNAG_DS = "pupnagDS";

	@Override
	@Bean
	public PresenterExtPoint presenterExtPoint()
	{
		return presenterExtPointBuilder()
				.addDataSource(selectDS(PUPNAG_DS, pupnagDSHandler()))
				.addDataSource(searchListDS(EDU_LOAD_DS, eduLoadCL(), eduLoadDSHandler()))
				.addDataSource(searchListDS(FIRST_COURSE_EDU_LOAD_DS, firstCourseDSColumns(), firstCourseImEduGroupDSHandler()))
				.addDataSource(searchListDS(OTHER_LOAD_DS, otherLoadCL(), otherLoadDSHandler()))
				.create();
	}

	@Bean
	public ColumnListExtPoint eduLoadCL()
	{
		IPublisherColumnBuilder eduGroupColumn = publisherColumn("eduGroupTitle", EppRealEduGroup.title()).publisherLinkResolver(new EppEduGroupPublisherLinkResolver()
		{
			@Override
			protected Long getOrgUnitId(IEntity entity)
			{
				return null;
			}

			@Override
			public String getComponentName(IEntity entity)
			{
				return "ru.tandemservice.uniepp.component.edugroup.pub.GlobalGroupPub";
			}
		});

		IPublisherColumnBuilder registryElementPartColumn = publisherColumn("registryElementPartTitle", EppRealEduGroup.activityPart().title()).publisherLinkResolver(new IPublisherLinkResolver()
		{
			@Override
			public Object getParameters(IEntity entity)
			{
				@SuppressWarnings("unchecked") EppRealEduGroup eduGroup = ((ViewWrapper<EppRealEduGroup>) entity).getEntity();
				return Collections.singletonMap(PublisherActivator.PUBLISHER_ID_KEY, eduGroup.getActivityPart().getRegistryElement().getId());
			}

			@Override
			public String getComponentName(IEntity entity)
			{
				return IUniEppDefines.EPP_DISCIPLINE_REGISTRY_PUB;
			}
		});

		return columnListExtPointBuilder(EDU_LOAD_DS)
				.addColumn(eduGroupColumn)
				.addColumn(registryElementPartColumn)
				.addColumn(textColumn("type", EppRealEduGroup.type().title()))
				.addColumn(textColumn("students", EduLoadWrapper.STUDENTS))
				.addColumn(textColumn("hours", EduLoadWrapper.HOURS).formatter(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS))
				.addColumn(textColumn("load", EduLoadWrapper.LOAD).formatter(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS))
				.addColumn(textColumn("developCombination", EduLoadWrapper.DEVELOP_COMBINATION))
				.addColumn(textColumn("timeRule", EduLoadWrapper.TIME_RULE_TITLE_LIST).formatter(RowCollectionFormatter.INSTANCE).width("30%"))
				.addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, "onClickEditEduLoadDistribution").permissionKey("employeePost_editLoadDistribution").disabled("ui:ppsRecordsAddEditDisabled"))
				.addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).permissionKey("employeePost_deleteLoadDistribution").disabled("ui:ppsRecordsAddEditDisabled"))
				.create();
	}

	@Bean
	public ColumnListExtPoint firstCourseDSColumns()
	{
		IPublisherColumnBuilder registryElementPartColumn = publisherColumn("registryElementPartTitle", EppRealEduGroup.activityPart().title()).publisherLinkResolver(new IPublisherLinkResolver()
		{
			@Override
			public Object getParameters(IEntity entity)
			{
				@SuppressWarnings("unchecked") LoadFirstCourseImEduGroup eduGroup = ((ViewWrapper<LoadFirstCourseImEduGroup>) entity).getEntity();
				return Collections.singletonMap(PublisherActivator.PUBLISHER_ID_KEY, eduGroup.getActivityPart().getRegistryElement().getId());
			}

			@Override
			public String getComponentName(IEntity entity)
			{
				return IUniEppDefines.EPP_DISCIPLINE_REGISTRY_PUB;
			}
		});

		return columnListExtPointBuilder(FIRST_COURSE_EDU_LOAD_DS)
				.addColumn(textColumn("title", LoadFirstCourseImEduGroup.title()))
				.addColumn(registryElementPartColumn)
				.addColumn(textColumn("type", LoadFirstCourseImEduGroup.type().title()))
				.addColumn(textColumn("students", LoadFirstCourseImEduGroup.students()))
				.addColumn(textColumn("loadSize", LoadFirstCourseImEduGroup.hoursAsDouble()).formatter(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS))
				.addColumn(textColumn("result", FirstCourseImEduGroupViewWrapper.LOAD).formatter(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS))
				.addColumn(textColumn("developCombination", FirstCourseImEduGroupViewWrapper.DEVELOP_COMBINATION))
				.addColumn(textColumn("timeRule", FirstCourseImEduGroupViewWrapper.TIME_RULE_TITLE_LIST).formatter(RowCollectionFormatter.INSTANCE).width("30%"))
				.addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, "onClickEditFirstCourseImEduGroup").permissionKey("employeePost_editFirstCourseEduLoadRecord").disabled("ui:ppsRecordsAddEditDisabled"))
				.addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).permissionKey("employeePost_deleteFirstCourseEduLoadRecord").disabled("ui:ppsRecordsAddEditDisabled"))
				.create();
	}

	@Bean
	public ColumnListExtPoint otherLoadCL()
	{
		return columnListExtPointBuilder(OTHER_LOAD_DS)
				.addColumn(textColumn("title", OtherLoad.title()))
				.addColumn(textColumn("hours", OtherLoad.hoursAsDouble()).formatter(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).width("10%"))
				.addColumn(textColumn("result", OtherLoadWrapper.RESULT).formatter(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).width("10%"))
				.addColumn(textColumn("timeRule", OtherLoadWrapper.TIME_RULE_TITLE_LIST).formatter(RowCollectionFormatter.INSTANCE).width("30%"))
				.addColumn(textColumn("yearDistributionPart", OtherLoad.yearDistributionPart().title()))
				.addColumn(textColumn("postgraduateLoad", OtherLoad.postgraduateProgramForm().title()))
				.addColumn(booleanColumn("considerInEduLoad", OtherLoad.considerInEduLoad()))
				.addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("employeePost_editNonEduLoadRecord").disabled("ui:ppsRecordsAddEditDisabled"))
				.addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).permissionKey("employeePost_deleteNonEduLoadRecord").disabled("ui:ppsRecordsAddEditDisabled"))
				.create();
	}

	@Bean
	public IDefaultSearchDataSourceHandler eduLoadDSHandler()
	{
		return new DefaultSearchDataSourceHandler(getName())
		{
			@Override
			public DSOutput execute(DSInput input, ExecutionContext context)
			{
				return ListOutputBuilder.get(input, context.<List>get(LoadPpsTabUI.EDU_LOAD)).pageable(true).build();
			}
		};
	}

	@Bean
	public IDefaultSearchDataSourceHandler firstCourseImEduGroupDSHandler()
	{
		return new DefaultSearchDataSourceHandler(getName())
		{
			@Override
			public DSOutput execute(DSInput input, ExecutionContext context)
			{
				return ListOutputBuilder.get(input, context.<List>get(LoadPpsTabUI.FIRST_COURSE_EDU_LOAD)).pageable(true).build();
			}
		};
	}

	@Bean
	public IDefaultSearchDataSourceHandler otherLoadDSHandler()
	{
		return new DefaultSearchDataSourceHandler(getName())
		{
			@Override
			public DSOutput execute(DSInput input, ExecutionContext context)
			{
				return ListOutputBuilder.get(input, context.<List>get(LoadPpsTabUI.OTHER_LOAD)).pageable(true).build();
			}
		};
	}

	@Bean
	public IDefaultComboDataSourceHandler pupnagDSHandler()
	{
		return new SpecialPupnagDSHandler(getName());
	}
}