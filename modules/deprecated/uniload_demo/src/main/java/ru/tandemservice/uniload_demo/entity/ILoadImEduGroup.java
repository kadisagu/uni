/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.entity;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;

/**
 * @author Alexander Zhebko
 * @since 12.12.2013
 */
public interface ILoadImEduGroup extends IEntity, ITitled
{
    public long getHours();

    public int getStudents();
}