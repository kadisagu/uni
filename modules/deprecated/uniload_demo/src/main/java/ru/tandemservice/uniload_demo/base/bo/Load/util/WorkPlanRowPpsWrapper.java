/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.base.bo.Load.util;

import org.tandemframework.core.entity.IdentifiableWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniload_demo.entity.LoadRecordOwner;

import java.util.Comparator;
import java.util.Map;

/**
 * @author Alexander Zhebko
 * @since 20.12.2013
 */
public class WorkPlanRowPpsWrapper extends IdentifiableWrapper<LoadRecordOwner>
{
	public static final Comparator<WorkPlanRowPpsWrapper> COMPARATOR = new Comparator<WorkPlanRowPpsWrapper>()
	{
		@Override
		public int compare(WorkPlanRowPpsWrapper o1, WorkPlanRowPpsWrapper o2)
		{
			return o1.getTitle().compareTo(o2.getTitle());
		}
	};

	public static final String SCIENCE_DEGREE_TYPE = "scienceDegreeType";
	public static final String SCIENCE_STATUS_TYPE = "scienceStatusType";

	private Map<String, Long> _loadMap;
	private final String scienceDegreeTypeTitle;
	private final String scienceStatusTypeTitle;

	public WorkPlanRowPpsWrapper(LoadRecordOwner pps, Map<String, Long> loadMap)
	{
		super(pps);
		_loadMap = loadMap;
		scienceDegreeTypeTitle = pps.getScienceDegreeType();
		scienceStatusTypeTitle = pps.getScienceStatusType();
	}

	public Long getLoad(String loadTypeCode)
	{
		return _loadMap.get(loadTypeCode);
	}

	public Long getLecture()
	{
		return _loadMap.get(EppALoadType.TYPE_LECTURES);
	}

	public void setLecture(Long lecture)
	{
		_loadMap.put(EppALoadType.TYPE_LECTURES, lecture);
	}

	public Long getPractice()
	{
		return _loadMap.get(EppALoadType.TYPE_PRACTICE);
	}

	public void setPractice(Long practice)
	{
		_loadMap.put(EppALoadType.TYPE_PRACTICE, practice);
	}

	public Long getLabs()
	{
		return _loadMap.get(EppALoadType.TYPE_LABS);
	}

	public void setLabs(Long labs)
	{
		_loadMap.put(EppALoadType.TYPE_LABS, labs);
	}

	//Тип ученой степени
	public String getScienceDegreeType()
	{
		return this.scienceDegreeTypeTitle;
	}

	//Тип ученого звания.
	public String getScienceStatusType()
	{
		return this.scienceStatusTypeTitle;
	}
}