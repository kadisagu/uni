/**
 * $Id$
 */
package ru.tandemservice.uniload_demo.base.bo.Load.logic;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uniload_demo.base.bo.Load.util.PpsLoadDataWrapper;
import ru.tandemservice.uniload_demo.entity.LoadRecordOwner;
import ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker;

import java.util.List;

import static ru.tandemservice.uniepp.UniEppUtils.wrap;

/**
 * @author Alexander Zhebko
 * @since 20.12.2013
 */
public class PpsLoadWrapper extends ViewWrapper<LoadRecordOwner>
{

    public static final String EDU_LOAD = "eduLoad";
    public static final String OTHER_LOAD = "otherLoad";
    public static final String OTHER_EDU_LOAD = "otherEduLoad";
    public static final String TOTAL_LOAD = "totalLoad";
    public static final String TOTAL_EDU_LOAD = "totalEduLoad";
    public static final String TITLE = "title";
    public static final String HOURLY_LOAD = "hourlyLoad";
    public static final String RATE = "rate";
    public static final String IS_SECOND_JOB = "isSecondJob";
    public static final String IS_FREELANCER = "isFreelancer";
    public static final String SCIENCE_DEGREE_TYPE = "scienceDegreeType";
    public static final String SCIENCE_STATUS_TYPE = "scienceStatusType";

    public static final String EMPLOYEE_POSTS = "employeePosts";
    public static final String TIMEWORKERS = "timeWorkers";

    private boolean _exceededTotalLoad;
    private Boolean _exceededTotalEduLoad;

    public PpsLoadWrapper(LoadRecordOwner recordOwner, PpsLoadDataWrapper loadDataWrapper, Double rate, boolean isSecondJob, boolean isFreelance, boolean exceededTotalLoad, Boolean exceededTotalEduLoad,List<EmployeePost> employeePosts, List<UnisnppsTimeworker> timeWorkers)
    {
        super(recordOwner);
        _exceededTotalLoad = exceededTotalLoad;
        _exceededTotalEduLoad = exceededTotalEduLoad;

        this.setViewProperty(TITLE, recordOwner.isFake() ? recordOwner.getFakePps().getVacancyName() : recordOwner.getRealPps().getPps().getTitle());

        this.setViewProperty(HOURLY_LOAD, recordOwner.getHourlyLoad());

        this.setViewProperty(EDU_LOAD, wrap(loadDataWrapper.getEduLoad()));
        this.setViewProperty(TOTAL_EDU_LOAD, wrap(loadDataWrapper.getTotalEduLoad()));
        this.setViewProperty(OTHER_LOAD, wrap(loadDataWrapper.getTotalOtherLoad()));
        this.setViewProperty(OTHER_EDU_LOAD, wrap(loadDataWrapper.getOtherEduLoad()));
        this.setViewProperty(TOTAL_LOAD, wrap(loadDataWrapper.getTotalLoad()));

        this.setViewProperty(RATE, rate);
        this.setViewProperty(IS_SECOND_JOB, isSecondJob);
        this.setViewProperty(IS_FREELANCER, isFreelance);

        this.setViewProperty(SCIENCE_DEGREE_TYPE, recordOwner.getScienceDegreeType());
        this.setViewProperty(SCIENCE_STATUS_TYPE, recordOwner.getScienceStatusType());

        this.setViewProperty(EMPLOYEE_POSTS, employeePosts);
        this.setViewProperty(TIMEWORKERS, timeWorkers);
    }

    /**
     * Показывает уровень учебной нагрузки относитьльно значения общего объема рабочего времени из настройки "Максимальная и минимальная нагрузка ППС"
     *
     * @return {@code true} если суммарная нагрузка превышает значение общего объем рабочего времени
     */
    public boolean isExceededTotalLoad()
    {
        return _exceededTotalLoad;
    }

    /**
     * Показывает уровень учебной нагрузки относитьльно занчений максимальной и минимальной нагрузки из настройки "Максимальная и минимальная нагрузка ППС"
     *
     * @return {@code true} если нагрузка больше максимальной, {@code false} если нагрузка меньше минимальной, {@code null} если нагрузка в пределах нормы
     */
    public Boolean isExceededTotalEduLoad()
    {
        return _exceededTotalEduLoad;
    }
}