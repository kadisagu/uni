package ru.tandemservice.uniload_demo.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniload_demo.entity.ILoadImEduGroup;
import ru.tandemservice.uniload_demo.entity.LoadDeviation;
import ru.tandemservice.uniload_demo.entity.LoadRecordOwner;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Отклонения
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class LoadDeviationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniload_demo.entity.LoadDeviation";
    public static final String ENTITY_NAME = "loadDeviation";
    public static final int VERSION_HASH = -1071917869;
    private static IEntityMeta ENTITY_META;

    public static final String L_OWNER = "owner";
    public static final String L_EDU_GROUP = "eduGroup";
    public static final String P_DEVIATION = "deviation";
    public static final String P_COMMENT = "comment";

    private LoadRecordOwner _owner;     // ППС (нагрузка)
    private ILoadImEduGroup _eduGroup;     // Планируемая УГС
    private long _deviation;     // Отклонение
    private String _comment;     // Комментарий

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ППС (нагрузка). Свойство не может быть null.
     */
    @NotNull
    public LoadRecordOwner getOwner()
    {
        return _owner;
    }

    /**
     * @param owner ППС (нагрузка). Свойство не может быть null.
     */
    public void setOwner(LoadRecordOwner owner)
    {
        dirty(_owner, owner);
        _owner = owner;
    }

    /**
     * @return Планируемая УГС. Свойство не может быть null.
     */
    @NotNull
    public ILoadImEduGroup getEduGroup()
    {
        return _eduGroup;
    }

    /**
     * @param eduGroup Планируемая УГС. Свойство не может быть null.
     */
    public void setEduGroup(ILoadImEduGroup eduGroup)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && eduGroup!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(ILoadImEduGroup.class);
            IEntityMeta actual =  eduGroup instanceof IEntity ? EntityRuntime.getMeta((IEntity) eduGroup) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_eduGroup, eduGroup);
        _eduGroup = eduGroup;
    }

    /**
     * @return Отклонение. Свойство не может быть null.
     */
    @NotNull
    public long getDeviation()
    {
        return _deviation;
    }

    /**
     * @param deviation Отклонение. Свойство не может быть null.
     */
    public void setDeviation(long deviation)
    {
        dirty(_deviation, deviation);
        _deviation = deviation;
    }

    /**
     * @return Комментарий.
     */
    @Length(max=255)
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof LoadDeviationGen)
        {
            setOwner(((LoadDeviation)another).getOwner());
            setEduGroup(((LoadDeviation)another).getEduGroup());
            setDeviation(((LoadDeviation)another).getDeviation());
            setComment(((LoadDeviation)another).getComment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends LoadDeviationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) LoadDeviation.class;
        }

        public T newInstance()
        {
            return (T) new LoadDeviation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "owner":
                    return obj.getOwner();
                case "eduGroup":
                    return obj.getEduGroup();
                case "deviation":
                    return obj.getDeviation();
                case "comment":
                    return obj.getComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "owner":
                    obj.setOwner((LoadRecordOwner) value);
                    return;
                case "eduGroup":
                    obj.setEduGroup((ILoadImEduGroup) value);
                    return;
                case "deviation":
                    obj.setDeviation((Long) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "owner":
                        return true;
                case "eduGroup":
                        return true;
                case "deviation":
                        return true;
                case "comment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "owner":
                    return true;
                case "eduGroup":
                    return true;
                case "deviation":
                    return true;
                case "comment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "owner":
                    return LoadRecordOwner.class;
                case "eduGroup":
                    return ILoadImEduGroup.class;
                case "deviation":
                    return Long.class;
                case "comment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<LoadDeviation> _dslPath = new Path<LoadDeviation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "LoadDeviation");
    }
            

    /**
     * @return ППС (нагрузка). Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadDeviation#getOwner()
     */
    public static LoadRecordOwner.Path<LoadRecordOwner> owner()
    {
        return _dslPath.owner();
    }

    /**
     * @return Планируемая УГС. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadDeviation#getEduGroup()
     */
    public static ILoadImEduGroupGen.Path<ILoadImEduGroup> eduGroup()
    {
        return _dslPath.eduGroup();
    }

    /**
     * @return Отклонение. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadDeviation#getDeviation()
     */
    public static PropertyPath<Long> deviation()
    {
        return _dslPath.deviation();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniload_demo.entity.LoadDeviation#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    public static class Path<E extends LoadDeviation> extends EntityPath<E>
    {
        private LoadRecordOwner.Path<LoadRecordOwner> _owner;
        private ILoadImEduGroupGen.Path<ILoadImEduGroup> _eduGroup;
        private PropertyPath<Long> _deviation;
        private PropertyPath<String> _comment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ППС (нагрузка). Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadDeviation#getOwner()
     */
        public LoadRecordOwner.Path<LoadRecordOwner> owner()
        {
            if(_owner == null )
                _owner = new LoadRecordOwner.Path<LoadRecordOwner>(L_OWNER, this);
            return _owner;
        }

    /**
     * @return Планируемая УГС. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadDeviation#getEduGroup()
     */
        public ILoadImEduGroupGen.Path<ILoadImEduGroup> eduGroup()
        {
            if(_eduGroup == null )
                _eduGroup = new ILoadImEduGroupGen.Path<ILoadImEduGroup>(L_EDU_GROUP, this);
            return _eduGroup;
        }

    /**
     * @return Отклонение. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadDeviation#getDeviation()
     */
        public PropertyPath<Long> deviation()
        {
            if(_deviation == null )
                _deviation = new PropertyPath<Long>(LoadDeviationGen.P_DEVIATION, this);
            return _deviation;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniload_demo.entity.LoadDeviation#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(LoadDeviationGen.P_COMMENT, this);
            return _comment;
        }

        public Class getEntityClass()
        {
            return LoadDeviation.class;
        }

        public String getEntityName()
        {
            return "loadDeviation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
