package ru.tandemservice.uniload_demo.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;
import ru.tandemservice.uniload_demo.entity.LoadCathedraPart2EppPlanStructure;
import ru.tandemservice.uniload_demo.entity.catalog.PpsLoadCathedraPart;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Соответствие циклов, частей и компонентов разделам нагрузки кафедры
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class LoadCathedraPart2EppPlanStructureGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniload_demo.entity.LoadCathedraPart2EppPlanStructure";
    public static final String ENTITY_NAME = "loadCathedraPart2EppPlanStructure";
    public static final int VERSION_HASH = -1478277528;
    private static IEntityMeta ENTITY_META;

    public static final String L_CATHEDRA_PART = "cathedraPart";
    public static final String L_PLAN_STRUCTURE = "planStructure";

    private PpsLoadCathedraPart _cathedraPart;     // Раздел нагрузки кафедры
    private EppPlanStructure _planStructure;     // Структура ГОС или УП

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Раздел нагрузки кафедры. Свойство не может быть null.
     */
    @NotNull
    public PpsLoadCathedraPart getCathedraPart()
    {
        return _cathedraPart;
    }

    /**
     * @param cathedraPart Раздел нагрузки кафедры. Свойство не может быть null.
     */
    public void setCathedraPart(PpsLoadCathedraPart cathedraPart)
    {
        dirty(_cathedraPart, cathedraPart);
        _cathedraPart = cathedraPart;
    }

    /**
     * @return Структура ГОС или УП. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EppPlanStructure getPlanStructure()
    {
        return _planStructure;
    }

    /**
     * @param planStructure Структура ГОС или УП. Свойство не может быть null и должно быть уникальным.
     */
    public void setPlanStructure(EppPlanStructure planStructure)
    {
        dirty(_planStructure, planStructure);
        _planStructure = planStructure;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof LoadCathedraPart2EppPlanStructureGen)
        {
            setCathedraPart(((LoadCathedraPart2EppPlanStructure)another).getCathedraPart());
            setPlanStructure(((LoadCathedraPart2EppPlanStructure)another).getPlanStructure());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends LoadCathedraPart2EppPlanStructureGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) LoadCathedraPart2EppPlanStructure.class;
        }

        public T newInstance()
        {
            return (T) new LoadCathedraPart2EppPlanStructure();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "cathedraPart":
                    return obj.getCathedraPart();
                case "planStructure":
                    return obj.getPlanStructure();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "cathedraPart":
                    obj.setCathedraPart((PpsLoadCathedraPart) value);
                    return;
                case "planStructure":
                    obj.setPlanStructure((EppPlanStructure) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "cathedraPart":
                        return true;
                case "planStructure":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "cathedraPart":
                    return true;
                case "planStructure":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "cathedraPart":
                    return PpsLoadCathedraPart.class;
                case "planStructure":
                    return EppPlanStructure.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<LoadCathedraPart2EppPlanStructure> _dslPath = new Path<LoadCathedraPart2EppPlanStructure>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "LoadCathedraPart2EppPlanStructure");
    }
            

    /**
     * @return Раздел нагрузки кафедры. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadCathedraPart2EppPlanStructure#getCathedraPart()
     */
    public static PpsLoadCathedraPart.Path<PpsLoadCathedraPart> cathedraPart()
    {
        return _dslPath.cathedraPart();
    }

    /**
     * @return Структура ГОС или УП. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniload_demo.entity.LoadCathedraPart2EppPlanStructure#getPlanStructure()
     */
    public static EppPlanStructure.Path<EppPlanStructure> planStructure()
    {
        return _dslPath.planStructure();
    }

    public static class Path<E extends LoadCathedraPart2EppPlanStructure> extends EntityPath<E>
    {
        private PpsLoadCathedraPart.Path<PpsLoadCathedraPart> _cathedraPart;
        private EppPlanStructure.Path<EppPlanStructure> _planStructure;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Раздел нагрузки кафедры. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadCathedraPart2EppPlanStructure#getCathedraPart()
     */
        public PpsLoadCathedraPart.Path<PpsLoadCathedraPart> cathedraPart()
        {
            if(_cathedraPart == null )
                _cathedraPart = new PpsLoadCathedraPart.Path<PpsLoadCathedraPart>(L_CATHEDRA_PART, this);
            return _cathedraPart;
        }

    /**
     * @return Структура ГОС или УП. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniload_demo.entity.LoadCathedraPart2EppPlanStructure#getPlanStructure()
     */
        public EppPlanStructure.Path<EppPlanStructure> planStructure()
        {
            if(_planStructure == null )
                _planStructure = new EppPlanStructure.Path<EppPlanStructure>(L_PLAN_STRUCTURE, this);
            return _planStructure;
        }

        public Class getEntityClass()
        {
            return LoadCathedraPart2EppPlanStructure.class;
        }

        public String getEntityName()
        {
            return "loadCathedraPart2EppPlanStructure";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
