/*$Id$*/
package ru.tandemservice.uniload_demo.base.bo.Load.ui.ResetEduGroups;

import org.tandemframework.caf.ui.UIPresenter;
import ru.tandemservice.uniepp.dao.group.EppRealGroupRowDAO;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniload_demo.base.bo.Load.LoadManager;
import ru.tandemservice.uniload_demo.base.bo.Load.logic.ILoadDAO;

/**
 * @author DMITRY KNYAZEV
 * @since 08.08.2014
 */
public class LoadResetEduGroupsUI extends UIPresenter
{
	private EppYearEducationProcess _pupnag;

	@Override
	public void onComponentRefresh()
	{
		_pupnag = LoadManager.instance().dao().getCurrentYear();
	}

	public void onClickApply()
	{
		ILoadDAO dao = LoadManager.instance().dao();
		dao.doResetCurrentYearEduGroup(getPupnag().getId());
		EppRealGroupRowDAO.DAEMON.wakeUpDaemon();
		deactivate();
	}

	public EppYearEducationProcess getPupnag()
	{
		return _pupnag;
	}

	public void setPupnag(EppYearEducationProcess pupnag)
	{
		this._pupnag = pupnag;
	}
}
