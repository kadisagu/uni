package ru.tandemservice.uniload_demo.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniload_demo.entity.TimeRule2EduGroup;
import ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь норм времени с типом УГС
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TimeRule2EduGroupGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniload_demo.entity.TimeRule2EduGroup";
    public static final String ENTITY_NAME = "timeRule2EduGroup";
    public static final int VERSION_HASH = 1720227608;
    private static IEntityMeta ENTITY_META;

    public static final String L_YEAR = "year";
    public static final String L_TIME_RULE = "timeRule";
    public static final String L_GROUP_TYPE = "groupType";

    private EppYearEducationProcess _year;     // ПУПнаГ
    private LoadTimeRule _timeRule;     // Норма времени
    private EppGroupType _groupType;     // Обобщенный вид нагрузки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ПУПнаГ. Свойство не может быть null.
     */
    @NotNull
    public EppYearEducationProcess getYear()
    {
        return _year;
    }

    /**
     * @param year ПУПнаГ. Свойство не может быть null.
     */
    public void setYear(EppYearEducationProcess year)
    {
        dirty(_year, year);
        _year = year;
    }

    /**
     * @return Норма времени. Свойство не может быть null.
     */
    @NotNull
    public LoadTimeRule getTimeRule()
    {
        return _timeRule;
    }

    /**
     * @param timeRule Норма времени. Свойство не может быть null.
     */
    public void setTimeRule(LoadTimeRule timeRule)
    {
        dirty(_timeRule, timeRule);
        _timeRule = timeRule;
    }

    /**
     * @return Обобщенный вид нагрузки. Свойство не может быть null.
     */
    @NotNull
    public EppGroupType getGroupType()
    {
        return _groupType;
    }

    /**
     * @param groupType Обобщенный вид нагрузки. Свойство не может быть null.
     */
    public void setGroupType(EppGroupType groupType)
    {
        dirty(_groupType, groupType);
        _groupType = groupType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof TimeRule2EduGroupGen)
        {
            setYear(((TimeRule2EduGroup)another).getYear());
            setTimeRule(((TimeRule2EduGroup)another).getTimeRule());
            setGroupType(((TimeRule2EduGroup)another).getGroupType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TimeRule2EduGroupGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TimeRule2EduGroup.class;
        }

        public T newInstance()
        {
            return (T) new TimeRule2EduGroup();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "year":
                    return obj.getYear();
                case "timeRule":
                    return obj.getTimeRule();
                case "groupType":
                    return obj.getGroupType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "year":
                    obj.setYear((EppYearEducationProcess) value);
                    return;
                case "timeRule":
                    obj.setTimeRule((LoadTimeRule) value);
                    return;
                case "groupType":
                    obj.setGroupType((EppGroupType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "year":
                        return true;
                case "timeRule":
                        return true;
                case "groupType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "year":
                    return true;
                case "timeRule":
                    return true;
                case "groupType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "year":
                    return EppYearEducationProcess.class;
                case "timeRule":
                    return LoadTimeRule.class;
                case "groupType":
                    return EppGroupType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TimeRule2EduGroup> _dslPath = new Path<TimeRule2EduGroup>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TimeRule2EduGroup");
    }
            

    /**
     * @return ПУПнаГ. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.TimeRule2EduGroup#getYear()
     */
    public static EppYearEducationProcess.Path<EppYearEducationProcess> year()
    {
        return _dslPath.year();
    }

    /**
     * @return Норма времени. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.TimeRule2EduGroup#getTimeRule()
     */
    public static LoadTimeRule.Path<LoadTimeRule> timeRule()
    {
        return _dslPath.timeRule();
    }

    /**
     * @return Обобщенный вид нагрузки. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.TimeRule2EduGroup#getGroupType()
     */
    public static EppGroupType.Path<EppGroupType> groupType()
    {
        return _dslPath.groupType();
    }

    public static class Path<E extends TimeRule2EduGroup> extends EntityPath<E>
    {
        private EppYearEducationProcess.Path<EppYearEducationProcess> _year;
        private LoadTimeRule.Path<LoadTimeRule> _timeRule;
        private EppGroupType.Path<EppGroupType> _groupType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ПУПнаГ. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.TimeRule2EduGroup#getYear()
     */
        public EppYearEducationProcess.Path<EppYearEducationProcess> year()
        {
            if(_year == null )
                _year = new EppYearEducationProcess.Path<EppYearEducationProcess>(L_YEAR, this);
            return _year;
        }

    /**
     * @return Норма времени. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.TimeRule2EduGroup#getTimeRule()
     */
        public LoadTimeRule.Path<LoadTimeRule> timeRule()
        {
            if(_timeRule == null )
                _timeRule = new LoadTimeRule.Path<LoadTimeRule>(L_TIME_RULE, this);
            return _timeRule;
        }

    /**
     * @return Обобщенный вид нагрузки. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.TimeRule2EduGroup#getGroupType()
     */
        public EppGroupType.Path<EppGroupType> groupType()
        {
            if(_groupType == null )
                _groupType = new EppGroupType.Path<EppGroupType>(L_GROUP_TYPE, this);
            return _groupType;
        }

        public Class getEntityClass()
        {
            return TimeRule2EduGroup.class;
        }

        public String getEntityName()
        {
            return "timeRule2EduGroup";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
