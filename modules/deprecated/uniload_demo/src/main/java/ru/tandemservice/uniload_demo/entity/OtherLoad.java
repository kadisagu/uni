package ru.tandemservice.uniload_demo.entity;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniload_demo.entity.gen.*;

/**
 * Другие виды нагрузки
 */
public class OtherLoad extends OtherLoadGen
{
	@EntityDSLSupport
	@Override
	public Double getHoursAsDouble()
	{
		return UniEppUtils.wrap(this.getHours());
	}

	public void setHoursAsDouble(Double hours)
	{
		this.setHours(UniEppUtils.unwrap(hours));
	}

	@Override
	public int getStudents()
	{
        /* stub */
		return -1;
	}

	@Override
	public void setStudents(int students)
	{
        /* stub */
	}
}