package ru.tandemservice.uniload_demo.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniload_demo_2x6x6_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.6")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность timeRule2EduGroup

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("timerule2edugroup_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("year_id", DBType.LONG).setNullable(false), 
				new DBColumn("timerule_id", DBType.LONG).setNullable(false), 
				new DBColumn("grouptype_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("timeRule2EduGroup");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность loadTimeRule

		// создано обязательное свойство manualAddPermit
		{
			// создать колонку
			tool.createColumn("loadtimerule_t", new DBColumn("manualaddpermit_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			java.lang.Boolean defaultManualAddPermit = false;
			tool.executeUpdate("update loadtimerule_t set manualaddpermit_p=? where manualaddpermit_p is null", defaultManualAddPermit);

			// сделать колонку NOT NULL
			tool.setColumnNullable("loadtimerule_t", "manualaddpermit_p", false);

		}


    }
}