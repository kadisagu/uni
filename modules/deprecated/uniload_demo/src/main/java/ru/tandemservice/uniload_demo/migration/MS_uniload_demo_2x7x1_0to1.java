package ru.tandemservice.uniload_demo.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import ru.tandemservice.uni.entity.catalog.codes.YearDistributionPartCodes;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniload_demo_2x7x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность otherLoad

		// создано обязательное свойство yearDistributionPart
        if (tool.tableExists("otherload_t"))
		{
			// создать колонку
			tool.createColumn("otherload_t", new DBColumn("yeardistributionpart_id", DBType.LONG));

			// задать значение по умолчанию
			PreparedStatement statement = tool.prepareStatement("SELECT id FROM yeardistributionpart_t WHERE CODE_P = ?", YearDistributionPartCodes.WINTER_SEMESTER);
			ResultSet set = statement.executeQuery();
			java.lang.Long defaultYearDistributionPart = null;
			if(set.next())
				defaultYearDistributionPart = set.getLong("id");
			tool.executeUpdate("update otherload_t set yeardistributionpart_id=? where yeardistributionpart_id is null", defaultYearDistributionPart);

			// сделать колонку NOT NULL
			tool.setColumnNullable("otherload_t", "yeardistributionpart_id", false);
		}
    }
}