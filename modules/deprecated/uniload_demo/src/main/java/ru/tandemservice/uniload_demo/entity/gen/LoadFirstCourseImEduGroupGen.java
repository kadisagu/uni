package ru.tandemservice.uniload_demo.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.uniload_demo.entity.ILoadImEduGroup;
import ru.tandemservice.uniload_demo.entity.LoadFirstCourseImEduGroup;
import ru.tandemservice.uniload_demo.entity.LoadRecordOwner;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Планируемая УГС первого курса (демо)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class LoadFirstCourseImEduGroupGen extends EntityBase
 implements ILoadImEduGroup{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniload_demo.entity.LoadFirstCourseImEduGroup";
    public static final String ENTITY_NAME = "loadFirstCourseImEduGroup";
    public static final int VERSION_HASH = 1422898115;
    private static IEntityMeta ENTITY_META;

    public static final String L_OWNER = "owner";
    public static final String P_TITLE = "title";
    public static final String L_ROW = "row";
    public static final String L_ACTIVITY_PART = "activityPart";
    public static final String L_TYPE = "type";
    public static final String P_HOURS = "hours";
    public static final String P_STUDENTS = "students";
    public static final String P_HOURS_AS_DOUBLE = "hoursAsDouble";

    private LoadRecordOwner _owner;     // ППС (нагрузка)
    private String _title;     // Название
    private EppWorkPlanRegistryElementRow _row;     // Запись РУП
    private EppRegistryElementPart _activityPart;     // Часть элемента реестра
    private EppGroupType _type;     // Обобщенный вид нагрузки
    private long _hours;     // Часы
    private int _students;     // Студенты

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ППС (нагрузка). Свойство не может быть null.
     */
    @NotNull
    public LoadRecordOwner getOwner()
    {
        return _owner;
    }

    /**
     * @param owner ППС (нагрузка). Свойство не может быть null.
     */
    public void setOwner(LoadRecordOwner owner)
    {
        dirty(_owner, owner);
        _owner = owner;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Запись РУП. Свойство не может быть null.
     */
    @NotNull
    public EppWorkPlanRegistryElementRow getRow()
    {
        return _row;
    }

    /**
     * @param row Запись РУП. Свойство не может быть null.
     */
    public void setRow(EppWorkPlanRegistryElementRow row)
    {
        dirty(_row, row);
        _row = row;
    }

    /**
     * @return Часть элемента реестра. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryElementPart getActivityPart()
    {
        return _activityPart;
    }

    /**
     * @param activityPart Часть элемента реестра. Свойство не может быть null.
     */
    public void setActivityPart(EppRegistryElementPart activityPart)
    {
        dirty(_activityPart, activityPart);
        _activityPart = activityPart;
    }

    /**
     * @return Обобщенный вид нагрузки. Свойство не может быть null.
     */
    @NotNull
    public EppGroupType getType()
    {
        return _type;
    }

    /**
     * @param type Обобщенный вид нагрузки. Свойство не может быть null.
     */
    public void setType(EppGroupType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Часы. Свойство не может быть null.
     */
    @NotNull
    public long getHours()
    {
        return _hours;
    }

    /**
     * @param hours Часы. Свойство не может быть null.
     */
    public void setHours(long hours)
    {
        dirty(_hours, hours);
        _hours = hours;
    }

    /**
     * @return Студенты. Свойство не может быть null.
     */
    @NotNull
    public int getStudents()
    {
        return _students;
    }

    /**
     * @param students Студенты. Свойство не может быть null.
     */
    public void setStudents(int students)
    {
        dirty(_students, students);
        _students = students;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof LoadFirstCourseImEduGroupGen)
        {
            setOwner(((LoadFirstCourseImEduGroup)another).getOwner());
            setTitle(((LoadFirstCourseImEduGroup)another).getTitle());
            setRow(((LoadFirstCourseImEduGroup)another).getRow());
            setActivityPart(((LoadFirstCourseImEduGroup)another).getActivityPart());
            setType(((LoadFirstCourseImEduGroup)another).getType());
            setHours(((LoadFirstCourseImEduGroup)another).getHours());
            setStudents(((LoadFirstCourseImEduGroup)another).getStudents());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends LoadFirstCourseImEduGroupGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) LoadFirstCourseImEduGroup.class;
        }

        public T newInstance()
        {
            return (T) new LoadFirstCourseImEduGroup();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "owner":
                    return obj.getOwner();
                case "title":
                    return obj.getTitle();
                case "row":
                    return obj.getRow();
                case "activityPart":
                    return obj.getActivityPart();
                case "type":
                    return obj.getType();
                case "hours":
                    return obj.getHours();
                case "students":
                    return obj.getStudents();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "owner":
                    obj.setOwner((LoadRecordOwner) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "row":
                    obj.setRow((EppWorkPlanRegistryElementRow) value);
                    return;
                case "activityPart":
                    obj.setActivityPart((EppRegistryElementPart) value);
                    return;
                case "type":
                    obj.setType((EppGroupType) value);
                    return;
                case "hours":
                    obj.setHours((Long) value);
                    return;
                case "students":
                    obj.setStudents((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "owner":
                        return true;
                case "title":
                        return true;
                case "row":
                        return true;
                case "activityPart":
                        return true;
                case "type":
                        return true;
                case "hours":
                        return true;
                case "students":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "owner":
                    return true;
                case "title":
                    return true;
                case "row":
                    return true;
                case "activityPart":
                    return true;
                case "type":
                    return true;
                case "hours":
                    return true;
                case "students":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "owner":
                    return LoadRecordOwner.class;
                case "title":
                    return String.class;
                case "row":
                    return EppWorkPlanRegistryElementRow.class;
                case "activityPart":
                    return EppRegistryElementPart.class;
                case "type":
                    return EppGroupType.class;
                case "hours":
                    return Long.class;
                case "students":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<LoadFirstCourseImEduGroup> _dslPath = new Path<LoadFirstCourseImEduGroup>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "LoadFirstCourseImEduGroup");
    }
            

    /**
     * @return ППС (нагрузка). Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadFirstCourseImEduGroup#getOwner()
     */
    public static LoadRecordOwner.Path<LoadRecordOwner> owner()
    {
        return _dslPath.owner();
    }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadFirstCourseImEduGroup#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Запись РУП. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadFirstCourseImEduGroup#getRow()
     */
    public static EppWorkPlanRegistryElementRow.Path<EppWorkPlanRegistryElementRow> row()
    {
        return _dslPath.row();
    }

    /**
     * @return Часть элемента реестра. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadFirstCourseImEduGroup#getActivityPart()
     */
    public static EppRegistryElementPart.Path<EppRegistryElementPart> activityPart()
    {
        return _dslPath.activityPart();
    }

    /**
     * @return Обобщенный вид нагрузки. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadFirstCourseImEduGroup#getType()
     */
    public static EppGroupType.Path<EppGroupType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Часы. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadFirstCourseImEduGroup#getHours()
     */
    public static PropertyPath<Long> hours()
    {
        return _dslPath.hours();
    }

    /**
     * @return Студенты. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadFirstCourseImEduGroup#getStudents()
     */
    public static PropertyPath<Integer> students()
    {
        return _dslPath.students();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniload_demo.entity.LoadFirstCourseImEduGroup#getHoursAsDouble()
     */
    public static SupportedPropertyPath<Double> hoursAsDouble()
    {
        return _dslPath.hoursAsDouble();
    }

    public static class Path<E extends LoadFirstCourseImEduGroup> extends EntityPath<E>
    {
        private LoadRecordOwner.Path<LoadRecordOwner> _owner;
        private PropertyPath<String> _title;
        private EppWorkPlanRegistryElementRow.Path<EppWorkPlanRegistryElementRow> _row;
        private EppRegistryElementPart.Path<EppRegistryElementPart> _activityPart;
        private EppGroupType.Path<EppGroupType> _type;
        private PropertyPath<Long> _hours;
        private PropertyPath<Integer> _students;
        private SupportedPropertyPath<Double> _hoursAsDouble;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ППС (нагрузка). Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadFirstCourseImEduGroup#getOwner()
     */
        public LoadRecordOwner.Path<LoadRecordOwner> owner()
        {
            if(_owner == null )
                _owner = new LoadRecordOwner.Path<LoadRecordOwner>(L_OWNER, this);
            return _owner;
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadFirstCourseImEduGroup#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(LoadFirstCourseImEduGroupGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Запись РУП. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadFirstCourseImEduGroup#getRow()
     */
        public EppWorkPlanRegistryElementRow.Path<EppWorkPlanRegistryElementRow> row()
        {
            if(_row == null )
                _row = new EppWorkPlanRegistryElementRow.Path<EppWorkPlanRegistryElementRow>(L_ROW, this);
            return _row;
        }

    /**
     * @return Часть элемента реестра. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadFirstCourseImEduGroup#getActivityPart()
     */
        public EppRegistryElementPart.Path<EppRegistryElementPart> activityPart()
        {
            if(_activityPart == null )
                _activityPart = new EppRegistryElementPart.Path<EppRegistryElementPart>(L_ACTIVITY_PART, this);
            return _activityPart;
        }

    /**
     * @return Обобщенный вид нагрузки. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadFirstCourseImEduGroup#getType()
     */
        public EppGroupType.Path<EppGroupType> type()
        {
            if(_type == null )
                _type = new EppGroupType.Path<EppGroupType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Часы. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadFirstCourseImEduGroup#getHours()
     */
        public PropertyPath<Long> hours()
        {
            if(_hours == null )
                _hours = new PropertyPath<Long>(LoadFirstCourseImEduGroupGen.P_HOURS, this);
            return _hours;
        }

    /**
     * @return Студенты. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadFirstCourseImEduGroup#getStudents()
     */
        public PropertyPath<Integer> students()
        {
            if(_students == null )
                _students = new PropertyPath<Integer>(LoadFirstCourseImEduGroupGen.P_STUDENTS, this);
            return _students;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniload_demo.entity.LoadFirstCourseImEduGroup#getHoursAsDouble()
     */
        public SupportedPropertyPath<Double> hoursAsDouble()
        {
            if(_hoursAsDouble == null )
                _hoursAsDouble = new SupportedPropertyPath<Double>(LoadFirstCourseImEduGroupGen.P_HOURS_AS_DOUBLE, this);
            return _hoursAsDouble;
        }

        public Class getEntityClass()
        {
            return LoadFirstCourseImEduGroup.class;
        }

        public String getEntityName()
        {
            return "loadFirstCourseImEduGroup";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract Double getHoursAsDouble();
}
