/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.base.ext.Employee.ui.PostView;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostView.EmployeePostViewUI;
import ru.tandemservice.uni.entity.employee.pps.EmployeePost4PpsEntry;
import ru.tandemservice.uniload_demo.base.bo.Load.LoadManager;
import ru.tandemservice.uniload_demo.base.bo.Load.logic.ILoadDAO;
import ru.tandemservice.uniload_demo.base.bo.Load.ui.PpsTab.LoadPpsTabUI;
import ru.tandemservice.uniload_demo.entity.LoadRecordOwner;

/**
 * @author Alexander Zhebko
 * @since 20.12.2013
 */
public class EmployeePostViewExtUI extends UIAddon
{
	public EmployeePostViewExtUI(IUIPresenter presenter, String name, String componentId)
	{
		super(presenter, name, componentId);
	}

	private Long _ppsId = null;

	public boolean isVisibleLoadTab()
	{
		//do it always visible
		return true;
	}

	public ParametersMap getParameters()
	{
		return ParametersMap.createWith(LoadPpsTabUI.LOAD_RECORD_OWNER_ID, _ppsId);
	}

	@Override
	public void onComponentRefresh()
	{
		_ppsId = null;
		Long employeePostId = ((EmployeePostViewUI) getPresenter()).getEmployeePost().getId();
		ILoadDAO dao = LoadManager.instance().dao();
		EmployeePost4PpsEntry employeePost4PpsEntry = dao.get(EmployeePost4PpsEntry.class, EmployeePost4PpsEntry.employeePost().id(), employeePostId);
		if(employeePost4PpsEntry != null && dao.getNextYear() != null) {
			LoadRecordOwner owner = dao.getLoadRecordOwner(employeePost4PpsEntry.getPpsEntry().getId(), dao.getNextYear().getId());
			if(owner != null) _ppsId = owner.getId();
		}

	}
}