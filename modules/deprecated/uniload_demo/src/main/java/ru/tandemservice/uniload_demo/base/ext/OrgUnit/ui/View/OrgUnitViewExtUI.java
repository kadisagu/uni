/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.base.ext.OrgUnit.ui.View;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitViewUI;
import ru.tandemservice.uniload_demo.base.bo.Load.LoadManager;

/**
 * @author Alexander Zhebko
 * @since 20.12.2013
 */
public class OrgUnitViewExtUI extends UIAddon
{
    public OrgUnitViewExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public boolean isVisibleLoadTab()
    {
        OrgUnitViewUI presenter = (OrgUnitViewUI) this.getPresenter();
        return LoadManager.instance().dao().isVisibleOrgUnitLoadTab(presenter.getOrgUnit().getId());
    }
}
