package ru.tandemservice.uniload_demo.entity.catalog;

import org.tandemframework.core.tool.tree.IHierarchyItem;
import ru.tandemservice.uniload_demo.entity.catalog.gen.PpsLoadCathedraPartGen;

/**
 * Разделы нагрузки кафедры
 */
public class PpsLoadCathedraPart extends PpsLoadCathedraPartGen implements IHierarchyItem
{
	@Override
	public IHierarchyItem getHierarhyParent()
	{
		return this.getParent();
	}
}