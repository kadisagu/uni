/*$Id$*/
package ru.tandemservice.uniload_demo.base.bo.Load.util.print;

import jxl.format.*;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WriteException;
import org.apache.commons.collections.keyvalue.MultiKey;

import java.util.HashMap;
import java.util.Map;

/**
 * @author DMITRY KNYAZEV
 * @since 15.12.2014
 *
 * Предназначен для предотвращения переполнения буфера шрифтов в MS Excel.
 * Создает набор используемых шрифтов к которым можно обращаться по параметрам
 */

public final class WritableCellFormatFactory
{
	private WritableFont _font;
	private Alignment _alignment;
	private VerticalAlignment _verticalAlignment;
	private Orientation _orientation;
	private Boolean _bordered;

	private final Map<MultiKey, WritableCellFormat> _cellFormatMap;

	private WritableCellFormatFactory(WritableFont font)
	{
		this._font = font;
		this._alignment = Alignment.CENTRE;
		this._verticalAlignment = VerticalAlignment.CENTRE;
		this._orientation = Orientation.HORIZONTAL;
		this._bordered = false;
		this._cellFormatMap = new HashMap<>();
	}

	public static WritableCellFormatFactory newInstance(WritableFont font)
	{
		return new WritableCellFormatFactory(font);
	}

	public WritableCellFormatFactory setFont(WritableFont font)
	{
		_font = font;
		return this;
	}

	public WritableCellFormatFactory setAlignment(Alignment alignment)
	{
		_alignment = alignment;
		return this;
	}

	public WritableCellFormatFactory setVerticalAlignment(VerticalAlignment verticalAlignment)
	{
		_verticalAlignment = verticalAlignment;
		return this;
	}

	public WritableCellFormatFactory setOrientation(Orientation orientation)
	{
		_orientation = orientation;
		return this;
	}

	public WritableCellFormatFactory setBordered(boolean bordered)
	{
		_bordered = bordered;
		return this;
	}

	public WritableCellFormat getCellFormat() throws WriteException
	{
		Object key1 = _font;
		Object key2 = _alignment.getDescription();
		Object key3 = _verticalAlignment.getDescription();
		Object key4 = _orientation.getDescription();
		Object key5 = _bordered.toString();

		MultiKey multiKey = new MultiKey(key1, key2, key3, key4, key5);

		if (_cellFormatMap.containsKey(multiKey)) return _cellFormatMap.get(multiKey);

		WritableCellFormat newCellFormat = new WritableCellFormat(_font);

		newCellFormat.setAlignment(_alignment);
		newCellFormat.setVerticalAlignment(_verticalAlignment);
		newCellFormat.setOrientation(_orientation);
		if (_bordered)
		{
			newCellFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
		}

		newCellFormat.setWrap(true);
		newCellFormat.setShrinkToFit(true);
		_cellFormatMap.put(multiKey, newCellFormat);
		return newCellFormat;
	}
}
