/*$Id$*/
package ru.tandemservice.uniload_demo.base.bo.Load.ui.DeviationTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import ru.tandemservice.uniload_demo.base.bo.Load.logic.DeviationViewWrapper;
import ru.tandemservice.uniload_demo.base.bo.Load.logic.SpecialPupnagDSHandler;
import ru.tandemservice.uniload_demo.entity.LoadDeviation;

import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 25.12.2014
 */
@Configuration
@SuppressWarnings("SpringFacetCodeInspection")
public class LoadDeviationTab extends BusinessComponentManager
{
	public static final String DEVIATION_DS = "deviationDS";
	public static final String PUPNAG_DS = "pupnagDS";
	public static final String KEY_SOURCE = "source";

	@Override
	@Bean
	public PresenterExtPoint presenterExtPoint()
	{
		return presenterExtPointBuilder()
				.addDataSource(selectDS(PUPNAG_DS, pupnagDSHandler()))
				.addDataSource(searchListDS(DEVIATION_DS, deviationDSColumns(), deviationDSHandler()))
				.create();
	}

	@Bean
	public ColumnListExtPoint deviationDSColumns()
	{
		return columnListExtPointBuilder(DEVIATION_DS)
				.addColumn(textColumn("fio", DeviationViewWrapper.FIO))
				.addColumn(textColumn("rate", DeviationViewWrapper.RATE))
				.addColumn(textColumn("totalLoad", DeviationViewWrapper.TOTAL_LOAD))
				.addColumn(textColumn("realLoad", DeviationViewWrapper.REAL_LOAD))
				.addColumn(textColumn("eduGroup", DeviationViewWrapper.EDU_GROUP_TITLE))
				.addColumn(textColumn("comment", LoadDeviation.comment()))
				.addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, "onClickEditRecord").permissionKey("ui:secModel.view_deviationAddEdit"))
				.addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, "onClickDeleteRecord").alert("Удалить отклонение?").permissionKey("ui:secModel.view_deviationDelete"))
				.create();
	}

	@Bean
	IDefaultSearchDataSourceHandler deviationDSHandler()
	{
		return new DefaultSearchDataSourceHandler(getName())
		{
			@Override
			protected DSOutput execute(DSInput input, ExecutionContext context)
			{
				return ListOutputBuilder.get(input, context.<List>get(KEY_SOURCE)).pageable(true).build();
			}
		};
	}

	@Bean
	public IDefaultComboDataSourceHandler pupnagDSHandler()
	{
		return new SpecialPupnagDSHandler(getName());
	}
}
