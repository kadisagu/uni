/*$Id$*/
package ru.tandemservice.uniload_demo.base.bo.Load.ui.PpsLoadType2TimeRule;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule;
import ru.tandemservice.uniload_demo.entity.catalog.PpsLoadGroup;
import ru.tandemservice.uniload_demo.entity.catalog.PpsLoadType;

import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 16.09.2014
 */
@Configuration
public class LoadPpsLoadType2TimeRule extends BusinessComponentManager
{
	public static final String PPS_LOAD_TYPE_DS = "ppsLoadTypeDS";
	public static final String PPS_LOAD_GROUP_DS = "ppsLoadGroupDS";
	public static final String LOAD_TYPE_TIME_RULE_DS = "loadTypeTimeRuleDS";
	public static final String LOAD_GROUP_TIME_RULE_DS = "loadGroupTimeRuleDS";

	public static final String USED_TIME_RULE_LIST = "usedLoadTimeRuleList";
	public static final String ADDED_TIME_RULE_LIST= "addedLoadTimeRuleList";

	@Override
	@Bean
	public PresenterExtPoint presenterExtPoint()
	{
		return presenterExtPointBuilder()
				.addDataSource(searchListDS(PPS_LOAD_TYPE_DS, ppsLoadTypeDSColumns(), ppsLoadTypeDSHandler()))
				.addDataSource(searchListDS(PPS_LOAD_GROUP_DS, ppsLoadGroupDSColumns(), ppsLoadGroupDSHandler()))
				.addDataSource(selectDS(LOAD_TYPE_TIME_RULE_DS, loadTimeRuleDSHandler()))
				.addDataSource(selectDS(LOAD_GROUP_TIME_RULE_DS, loadTimeRuleDSHandler()))
				.create();
	}

	@Bean
	public ColumnListExtPoint ppsLoadTypeDSColumns()
	{
		return columnListExtPointBuilder(PPS_LOAD_TYPE_DS)
				.addColumn(textColumn("title", PpsLoadType.title()).width("300px"))
				.addColumn(blockColumn("time", "ppsLoadTypeColumn"))
				.create();
	}

	@Bean
	public IDefaultSearchDataSourceHandler ppsLoadTypeDSHandler()
	{
		return new EntityComboDataSourceHandler(getName(), PpsLoadType.class).order(PpsLoadType.title());
	}

	@Bean
	public ColumnListExtPoint ppsLoadGroupDSColumns()
	{
		return columnListExtPointBuilder(PPS_LOAD_GROUP_DS)
				.addColumn(textColumn("title", PpsLoadGroup.title()).width("300px"))
				.addColumn(blockColumn("time", "ppsLoadGroupColumn"))
				.create();
	}

	@Bean
	public IDefaultSearchDataSourceHandler ppsLoadGroupDSHandler()
	{
		return new EntityComboDataSourceHandler(getName(), PpsLoadGroup.class).order(PpsLoadGroup.title());
	}

	@Bean
	public IDefaultComboDataSourceHandler loadTimeRuleDSHandler()
	{
		return new EntityComboDataSourceHandler(getName(), LoadTimeRule.class)
		{
			@Override
			protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
			{
				super.applyWhereConditions(alias, dql, context);

				Collection<LoadTimeRule> usedLoadTimeRule = context.get(USED_TIME_RULE_LIST);
				Collection<LoadTimeRule> addedLoadTimeRule = context.get(ADDED_TIME_RULE_LIST);

				if(usedLoadTimeRule != null && !usedLoadTimeRule.isEmpty() && addedLoadTimeRule != null && !addedLoadTimeRule.isEmpty()) {
					dql.where(or(in(property(alias, LoadTimeRule.P_ID), addedLoadTimeRule), notIn(property(alias, LoadTimeRule.P_ID), usedLoadTimeRule)));
				}
				else
				{
					if(usedLoadTimeRule != null && !usedLoadTimeRule.isEmpty())
						dql.where(notIn(property(alias, LoadTimeRule.P_ID), usedLoadTimeRule));

					if(addedLoadTimeRule != null && !addedLoadTimeRule.isEmpty())
						dql.where(in(property(alias, LoadTimeRule.P_ID), addedLoadTimeRule));
				}
			}
		}.filter(LoadTimeRule.title());
	}
}
