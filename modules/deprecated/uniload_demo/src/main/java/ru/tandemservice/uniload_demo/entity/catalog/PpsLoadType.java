package ru.tandemservice.uniload_demo.entity.catalog;

import ru.tandemservice.uniload_demo.entity.catalog.codes.PpsLoadTypeCodes;
import ru.tandemservice.uniload_demo.entity.catalog.gen.PpsLoadTypeGen;

/**
 * Типы нагрузки ППС
 */
public class PpsLoadType extends PpsLoadTypeGen implements PpsLoadTypeCodes
{
}
