/*$Id$*/
package ru.tandemservice.uniload_demo.base.bo.Load.ui.CathedraPart2PlanStructureRel;

import com.google.common.collect.Multimap;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;
import ru.tandemservice.uniload_demo.base.bo.Load.LoadManager;
import ru.tandemservice.uniload_demo.entity.catalog.PpsLoadCathedraPart;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author DMITRY KNYAZEV
 * @since 27.11.2014
 */

@SuppressWarnings("UnusedDeclaration")
public class LoadCathedraPart2PlanStructureRelUI extends UIPresenter
{
	private Multimap<PpsLoadCathedraPart, EppPlanStructure> _source;
	private Set<Long> _excluded;

	@Override
	public void onComponentRefresh()
	{
		_source = LoadManager.instance().dao().getCathedraPart2PlanStructureMultimap();
		_excluded = getDisabledCathedraPartIds();
	}

	@Override
	public void onBeforeDataSourceFetch(IUIDataSource dataSource)
	{
		if (dataSource.getName().equals(LoadCathedraPart2PlanStructureRel.EPP_PLAN_STRUCTURE_DS))
		{
			HashSet<EppPlanStructure> disjunction = new HashSet<>(getSource().values());
			disjunction.removeAll(getPlanStructure2CathedraPart());
			dataSource.put(LoadCathedraPart2PlanStructureRel.USED_PLAN_STRUCTURE, disjunction);
		}
	}

	//getters/setters
	private Multimap<PpsLoadCathedraPart, EppPlanStructure> getSource()
	{
		return _source;
	}

	private Set<Long> getExcluded()
	{
		return _excluded;
	}

	public Collection<EppPlanStructure> getPlanStructure2CathedraPart()
	{
		return getSource().get(getCurrentRow());
	}

	public void setPlanStructure2CathedraPart(Collection<EppPlanStructure> planStructures)
	{
		PpsLoadCathedraPart cathedraPart = getCurrentRow();
		if (getSource().containsKey(cathedraPart))
			getSource().replaceValues(cathedraPart, planStructures);
		else
			getSource().putAll(cathedraPart, planStructures);
	}

	private PpsLoadCathedraPart getCurrentRow()
	{
		return ((PpsLoadCathedraPart) getConfig().getDataSource(LoadCathedraPart2PlanStructureRel.PPS_CATHEDRA_PART_DS).getCurrent());
	}

	private Set<Long> getDisabledCathedraPartIds()
	{
		String lcp = "lcp";
		DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(PpsLoadCathedraPart.class, lcp)
				.column(DQLExpressions.property(lcp, PpsLoadCathedraPart.parent().id()))
				.where(DQLExpressions.isNotNull(DQLExpressions.property(lcp, PpsLoadCathedraPart.L_PARENT)))
				.distinct();
		return new HashSet<>(LoadManager.instance().dao().<Long>getList(builder));
	}

	public boolean isDisabled()
	{
		return getExcluded().contains(getCurrentRow().getId());
	}

	//handlers
	public void onClickApply()
	{
		if (getSource().values().size() != new HashSet<>(getSource().values()).size())
		{
			ContextLocal.getErrorCollector().add("Каждая структура ГОС или УП должна быть использована только один раз");
			return;
		}
		LoadManager.instance().dao().updateCathedraPart2PlanStructureMultimap(getSource());
	}

	public void onClickSave()
	{
		onClickApply();
		deactivate();
	}
}
