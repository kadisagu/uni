/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.base.bo.Load.logic;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.IStyleResolver;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.catalog.codes.EppALoadTypeCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionTypeCodes;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadType;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;

import javax.annotation.Nullable;
import java.util.*;

/**
 * @author Alexander Zhebko
 * @since 20.12.2013
 */
public class WorkPlanRowWrapper extends ViewWrapper<EppWorkPlanRegistryElementRow>
{
	public static final String DEVELOP_COMBINATION = "devCombination";
	public static final String EXAM = "exam";
	public static final String SET_OFF = "setOff";
	public static final String SET_OFF_DIFF = "setOffDiff";
	public static final String COURSE_WORK = "courseWork";
	public static final String COURSE_PROJECT = "courseProject";
	public static final String EXAM_ACCUM = "examAccum";
	public static final String LECTURES = "lectures";
	public static final String PRACTICE = "practice";
	public static final String LABS = "labs";
	public static final String COURSE = "course";
	public static final String EDU_GROUP = "eduGroup";

	private final Set<String> _coloredLoadTypes;

	public WorkPlanRowWrapper(EppWorkPlanRegistryElementRow row, Set<String> controlActions, Map<String, Long> unwrappedLoadMap, Map<String, Long> unwrappedDistrLoadMap, Map<String, Long> unwrappedFirstCourseLoadMap, @Nullable Collection<EppRealEduGroup4LoadType> eduGroupList)
	{
		super(row);
		_coloredLoadTypes = new HashSet<>();
		final Map<String, String> valueMap = new HashMap<>();

		for (String loadCode : new String[]{EppALoadTypeCodes.TYPE_LECTURES, EppALoadTypeCodes.TYPE_PRACTICE, EppALoadTypeCodes.TYPE_LABS})
		{
			Long load = unwrappedLoadMap.get(loadCode);
			Long distrLoad = unwrappedDistrLoadMap.get(loadCode);
			Long fcLoad = unwrappedFirstCourseLoadMap.get(loadCode);

			if (load == null && distrLoad == null && fcLoad == null)
				continue;
			if (load == null)
				load = 0L;
			if (distrLoad == null)
				distrLoad = 0L;
			if (fcLoad == null)
				fcLoad = 0L;

			String value = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(UniEppUtils.wrap(load));

			boolean distributed = load == distrLoad + fcLoad;
			valueMap.put(loadCode, distributed ? value : value + "(" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(UniEppUtils.wrap(load - distrLoad - fcLoad)) + ")");

			if (!distributed)
				_coloredLoadTypes.add(loadCode);
		}

		String eduGroupTitle = "Нет УГС связанных с данной строкой РУП ";
		if (eduGroupList != null && !eduGroupList.isEmpty())
			eduGroupTitle = CommonBaseStringUtil.join(eduGroupList, EppRealEduGroup4LoadType.P_TITLE, "; ");

		String course = String.valueOf(getWorkPlan().getGridTerm().getCourseNumber());

		setViewProperty(DEVELOP_COMBINATION, getDevelopCombination());
		setViewProperty(EXAM, getExam(controlActions));
		setViewProperty(SET_OFF, getSetOff(controlActions));
		setViewProperty(SET_OFF_DIFF, getSetOffDiff(controlActions));
		setViewProperty(COURSE_WORK, getCourseWork(controlActions));
		setViewProperty(COURSE_PROJECT, getCourseProject(controlActions));
		setViewProperty(EXAM_ACCUM, getExamAccum(controlActions));
		setViewProperty(LECTURES, getLectures(valueMap));
		setViewProperty(PRACTICE, getPractice(valueMap));
		setViewProperty(LABS, getLabs(valueMap));
		setViewProperty(COURSE, course);
		setViewProperty(EDU_GROUP, eduGroupTitle);
	}

	private String getDevelopCombination()
	{
		return getEntity().getWorkPlan().getEduPlan().getDevelopCombinationTitle();
	}

	private String getExam(Set<String> controlActions)
	{
		return controlActions.contains(EppFControlActionTypeCodes.CONTROL_ACTION_EXAM) ? "1" : null;
	}

	private String getSetOff(Set<String> controlActions)
	{
		return controlActions.contains(EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF) ? "1" : null;
	}

	private String getSetOffDiff(Set<String> controlActions)
	{
		return controlActions.contains(EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF_DIFF) ? "1" : null;
	}

	private String getCourseWork(Set<String> controlActions)
	{
		return controlActions.contains(EppFControlActionTypeCodes.CONTROL_ACTION_COURSE_WORK) ? "1" : null;
	}

	private String getCourseProject(Set<String> controlActions)
	{
		return controlActions.contains(EppFControlActionTypeCodes.CONTROL_ACTION_COURSE_PROJECT) ? "1" : null;
	}

	private String getExamAccum(Set<String> controlActions)
	{
		return controlActions.contains(EppFControlActionTypeCodes.CONTROL_ACTION_EXAM_ACCUM) ? "1" : null;
	}

	private String getLectures(Map<String, String> _valueMap)
	{
		return _valueMap.get(EppALoadTypeCodes.TYPE_LECTURES);
	}

	private String getPractice(Map<String, String> _valueMap)
	{
		return _valueMap.get(EppALoadTypeCodes.TYPE_PRACTICE);
	}

	private String getLabs(Map<String, String> _valueMap)
	{
		return _valueMap.get(EppALoadTypeCodes.TYPE_LABS);
	}

	private EppWorkPlanBase getWorkPlan()
	{
		return getEntity().getWorkPlan();
	}

    public static final Comparator<WorkPlanRowWrapper> COMPARATOR_WORK_PLAN = (o1, o2) -> o1.getEntity().getWorkPlan().getTitle().compareTo(o2.getEntity().getWorkPlan().getTitle());
    public static final Comparator<WorkPlanRowWrapper> COMPARATOR_TITLE = (o1, o2) -> o1.getEntity().getTitle().compareTo(o2.getEntity().getTitle());
    public static final Comparator<WorkPlanRowWrapper> COMPARATOR = (o1, o2) -> {
        int result;
        if ((result = COMPARATOR_WORK_PLAN.compare(o1, o2)) == 0 && (result = COMPARATOR_TITLE.compare(o1, o2)) == 0)
            return result;
        return result;
    };

	public static final Comparator<WorkPlanRowWrapper> COMPARATOR_COURSE = (o1, o2) -> Integer.compare(o1.getEntity().getWorkPlan().getGridTerm().getCourseNumber(), o2.getEntity().getWorkPlan().getGridTerm().getCourseNumber());

	public static final IStyleResolver LECTURE_COLOR_RESOLVER = new LoadColorResolver()
	{
		@Override
		protected String getLoadCode()
		{
			return EppALoadTypeCodes.TYPE_LECTURES;
		}
	};
	public static final IStyleResolver PRACTICE_COLOR_RESOLVER = new LoadColorResolver()
	{
		@Override
		protected String getLoadCode()
		{
			return EppALoadTypeCodes.TYPE_PRACTICE;
		}
	};
	public static final IStyleResolver LABS_COLOR_RESOLVER = new LoadColorResolver()
	{
		@Override
		protected String getLoadCode()
		{
			return EppALoadTypeCodes.TYPE_LABS;
		}
	};

	private static abstract class LoadColorResolver implements IStyleResolver
	{
		@Override
		public String getStyle(IEntity rowEntity)
		{
			return ((WorkPlanRowWrapper) rowEntity)._coloredLoadTypes.contains(getLoadCode()) ? "color: red;" : "";
		}

		protected abstract String getLoadCode();
	}
}