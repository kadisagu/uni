/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.base.bo.Load.ui.WorkPlanRowEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.employee.pps.PpsEntryByEmployeePost;
import ru.tandemservice.uniepp.entity.pps.gen.IEppPPSCollectionOwnerGen;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadType;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.uniload_demo.base.bo.Load.util.WorkPlanRowPpsWrapper;
import ru.tandemservice.uniload_demo.entity.LoadEduGroupPpsDistr;
import ru.tandemservice.uniload_demo.entity.LoadFakePpsEntry;
import ru.tandemservice.uniload_demo.entity.LoadFirstCourseImEduGroup;
import ru.tandemservice.uniload_demo.entity.LoadRecordOwner;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 20.12.2013
 */
@Configuration
@SuppressWarnings("SpringFacetCodeInspection")
public class LoadWorkPlanRowEdit extends BusinessComponentManager
{
    public static final String WORK_PLAN_ROW_PPS_DS = "workPlanRowPpsDS";
    public static final String PPS_COMBO_DS = "ppsComboDS";

    public static final String NAME_TITLE = "title";
    public static final String NAME_LECTURE = "lecture";
    public static final String NAME_PRACTICE = "practice";
    public static final String NAME_LABS = "labs";

    public static final String KEY_SOURCE = "source";
    public static final String KEY_ROW_ID = "rowId";
    public static final String KEY_OWNER_ORG_UNIT = "ownerOrgUnit";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(PPS_COMBO_DS, ppsComboDSHandler()))
                .addDataSource(searchListDS(WORK_PLAN_ROW_PPS_DS, ppsDSColumns(), ppsDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint ppsDSColumns()
    {
        return columnListExtPointBuilder(WORK_PLAN_ROW_PPS_DS)
                .addColumn(textColumn(NAME_TITLE, PpsEntry.title()).width("150px"))
                .addColumn(textColumn(WorkPlanRowPpsWrapper.SCIENCE_DEGREE_TYPE, WorkPlanRowPpsWrapper.SCIENCE_DEGREE_TYPE))
                .addColumn(textColumn(WorkPlanRowPpsWrapper.SCIENCE_STATUS_TYPE, WorkPlanRowPpsWrapper.SCIENCE_STATUS_TYPE))
                .addColumn(blockColumn(NAME_LECTURE, "lectureBlockColumn").headerAlign("center"))
                .addColumn(blockColumn(NAME_PRACTICE, "practiceBlockColumn").headerAlign("center"))
                .addColumn(blockColumn(NAME_LABS, "labsBlockColumn").headerAlign("center"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, "onClickDeleteRecord"))
                .create();
    }

    @Bean
    IDefaultSearchDataSourceHandler ppsDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                return ListOutputBuilder.get(input, context.<List>get(KEY_SOURCE)).build();
            }
        };
    }

    @Bean
    public IDefaultComboDataSourceHandler ppsComboDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), LoadRecordOwner.class)
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                IDQLSelectableQuery ppsIdsQuery = new DQLSelectBuilder()
                        .fromEntity(LoadRecordOwner.class, "ro")
                        .column(property("ro", LoadRecordOwner.id()))
                        .joinPath(DQLJoinType.inner, LoadRecordOwner.realPps().pps().fromAlias("ro"), "ep")
                        .where(likeUpper(
                                DQLFunctions.concat(
                                        property("ep", PpsEntryByEmployeePost.person().identityCard().fullFio()),
                                        property("ep", PpsEntryByEmployeePost.post().title()),
                                        property("ep", PpsEntryByEmployeePost.orgUnit().shortTitle())),
                                value(CoreStringUtils.escapeLike(filter, true))))
                        .buildQuery();

                IDQLSelectableQuery fakePpsIdsQuery = new DQLSelectBuilder()
                        .fromEntity(LoadRecordOwner.class, "ro")
                        .column(property("ro", LoadRecordOwner.id()))
                        .joinPath(DQLJoinType.inner, LoadRecordOwner.fakePps().fromAlias("ro"), "fp")
                        .where(likeUpper(property("fp", LoadFakePpsEntry.vacancyName()), value(CoreStringUtils.escapeLike(filter, true))))
                        .buildQuery();

                return super.query(alias, filter)
                        .where(or(
                                in(property(alias, LoadRecordOwner.id()), ppsIdsQuery),
                                in(property(alias, LoadRecordOwner.id()), fakePpsIdsQuery)));

            }

            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                Long rowId = context.get(KEY_ROW_ID);
                boolean ownerOrgUnit = context.get(KEY_OWNER_ORG_UNIT);

                if (ownerOrgUnit)
                {
                    IDQLSelectableQuery orgUnitIdQuery = new DQLSelectBuilder()
                            .fromEntity(EppWorkPlanRegistryElementRow.class, "rer")
                            .column(property("rer", EppWorkPlanRegistryElementRow.registryElementPart().registryElement().owner().id()))
                            .where(eq(property("rer", EppWorkPlanRegistryElementRow.id()), value(rowId)))
                            .buildQuery();

                    IDQLSelectableQuery ppsIdsQuery = new DQLSelectBuilder()
                            .fromEntity(LoadRecordOwner.class, "ro")
                            .column(property("ro", LoadRecordOwner.id()))
                            .joinPath(DQLJoinType.inner, LoadRecordOwner.realPps().pps().fromAlias("ro"), "ep")
                            .where(eq(property("ep", PpsEntry.orgUnit().id()), orgUnitIdQuery))
                            .buildQuery();

                    IDQLSelectableQuery fakePpsIdsQuery = new DQLSelectBuilder()
                            .fromEntity(LoadRecordOwner.class, "ro")
                            .column(property("ro", LoadRecordOwner.id()))
                            .joinPath(DQLJoinType.inner, LoadRecordOwner.fakePps().fromAlias("ro"), "fp")
                            .where(eq(property("fp", LoadFakePpsEntry.orgUnit().id()), orgUnitIdQuery))
                            .buildQuery();

                    dql.where(or(
                            in(property(alias, LoadRecordOwner.id()), ppsIdsQuery),
                            in(property(alias, LoadRecordOwner.id()), fakePpsIdsQuery)));
                }

                IDQLSelectableQuery ppsIdsJuniorQuery = new DQLSelectBuilder()
                        .fromEntity(LoadEduGroupPpsDistr.class, "d")
                        .column(property("d", LoadEduGroupPpsDistr.owner().id()))
                        .joinPath(DQLJoinType.inner, LoadEduGroupPpsDistr.eduGroup().fromAlias("d"), "l")
                        .joinEntity("d", DQLJoinType.inner, EppRealEduGroup4LoadType.class, "lt", eq(property("lt", EppRealEduGroup4LoadType.id()), property("l", IEppPPSCollectionOwnerGen.id())))
                        .where(eq(property("lt", EppRealEduGroup4LoadType.workPlanRow().id()), value(rowId)))
                        .predicate(DQLPredicateType.distinct)
                        .buildQuery();

                IDQLSelectableQuery ppsIdsSeniorQuery = new DQLSelectBuilder()
                        .fromEntity(LoadFirstCourseImEduGroup.class, "fc")
                        .column(property("fc", LoadFirstCourseImEduGroup.owner().id()))
                        .where(eq(property("fc", LoadFirstCourseImEduGroup.row().id()), value(rowId)))
                        .predicate(DQLPredicateType.distinct)
                        .buildQuery();

                dql.where(and(
                        notIn(property(alias, LoadRecordOwner.id()), ppsIdsJuniorQuery),
                        notIn(property(alias, LoadRecordOwner.id()), ppsIdsSeniorQuery)));

                super.applyWhereConditions(alias, dql, context);
            }
        };
    }
}