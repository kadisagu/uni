/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.base.bo.Load.ui.FirstCourseImEduGroupAddEdit;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Transformer;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeALT;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeFCA;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniload_demo.base.bo.Load.LoadManager;
import ru.tandemservice.uniload_demo.base.bo.Load.logic.ILoadDAO;
import ru.tandemservice.uniload_demo.base.bo.Load.ui.EduGroupAdd.LoadEduGroupAdd;
import ru.tandemservice.uniload_demo.entity.ImEduGroupLoadBase;
import ru.tandemservice.uniload_demo.entity.LoadFirstCourseImEduGroup;
import ru.tandemservice.uniload_demo.entity.LoadImEduGroupTimeRuleRel;
import ru.tandemservice.uniload_demo.entity.LoadRecordOwner;
import ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule;
import ru.tandemservice.uniload_demo.entity.gen.LoadImEduGroupTimeRuleRelGen;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 20.12.2013
 */
@Input({
		@Bind(key = LoadFirstCourseImEduGroupAddEditUI.RECORD_ID, binding = LoadFirstCourseImEduGroupAddEditUI.RECORD_ID),
		@Bind(key = LoadFirstCourseImEduGroupAddEditUI.PPS_ID, binding = LoadFirstCourseImEduGroupAddEditUI.PPS_ID),
		@Bind(key = LoadFirstCourseImEduGroupAddEditUI.PUPNAG_ID, binding = LoadFirstCourseImEduGroupAddEditUI.PUPNAG_ID, required = true)})
public class LoadFirstCourseImEduGroupAddEditUI extends UIPresenter
{
	public static final String RECORD_ID = "recordId";
	public static final String PPS_ID = "ppsId";
	public static final String PUPNAG_ID = "pupnagId";

	private static final IdentifiableWrapper<IEntity> SOURCE_OWNER_ORG_UNIT = new IdentifiableWrapper<>(1L, "С читающего подразделения");
	private static final IdentifiableWrapper<IEntity> SOURCE_ALL_ORG_UNIT = new IdentifiableWrapper<>(2L, "Со всех подразделений");
	private static final List<IdentifiableWrapper<IEntity>> SOURCE_FILTER_LIST = Arrays.asList(
			SOURCE_OWNER_ORG_UNIT,
			SOURCE_ALL_ORG_UNIT
	);

	private Long _recordId;
	private Long _ppsId;
	private Long _pupnagId;
	private LoadFirstCourseImEduGroup _record = new LoadFirstCourseImEduGroup();
	private EppWorkPlan _workPlan;
	private Collection<LoadTimeRule> _timeRules;
	private Long _hoursMaxUnwrapped;
	private IEntity _sourceFilter;

	@Override
	public void onComponentRefresh()
	{
		if (isEditForm())
		{
			setRecord(LoadManager.instance().dao().getNotNull(LoadFirstCourseImEduGroup.class, getRecordId()));
			setWorkPlan((EppWorkPlan) getRecord().getRow().getWorkPlan());

			List<LoadImEduGroupTimeRuleRel> timeRuleRels = LoadManager.instance().dao().getList(LoadImEduGroupTimeRuleRel.class, LoadImEduGroupTimeRuleRel.eduGroup().id(), getRecordId(), LoadImEduGroupTimeRuleRel.timeRule().title().s());
			Transformer<LoadImEduGroupTimeRuleRel, LoadTimeRule> transformer = LoadImEduGroupTimeRuleRelGen::getTimeRule;

			setTimeRules(CollectionUtils.collect(timeRuleRels, transformer));
		}
		boolean owner = isAddForm() || getRecord().getRow().getRegistryElementPart().getRegistryElement().getOwner().getId().equals(getPps().getOrgUnit().getId());
		setSourceFilter(owner ? SOURCE_OWNER_ORG_UNIT : SOURCE_ALL_ORG_UNIT);
	}

	@Override
	public void onBeforeDataSourceFetch(IUIDataSource dataSource)
	{
		switch (dataSource.getName())
		{
			case LoadFirstCourseImEduGroupAddEdit.WORK_PLAN_ROW_DS:
			{
				dataSource.put(LoadFirstCourseImEduGroupAddEdit.WORK_PLAN, getWorkPlan() == null ? null : getWorkPlan().getId());
				dataSource.put(LoadFirstCourseImEduGroupAddEdit.ORG_UNIT_ID, getSourceFilter() == SOURCE_OWNER_ORG_UNIT ? getPps().getOrgUnit().getId() : null);
				break;
			}
			case LoadFirstCourseImEduGroupAddEdit.GROUP_TYPE_DS:
			{
				dataSource.put(LoadFirstCourseImEduGroupAddEdit.WORK_PLAN_ROW, getRecord().getRow() == null ? null : getRecord().getRow().getId());
				break;
			}
			case LoadEduGroupAdd.TIME_RULE_DS:
			{
				dataSource.put(LoadFirstCourseImEduGroupAddEdit.TIME_RULE_INCLUDE, getTimeRules());
				if(isStudentDependency() != null)
				{
					if (isStudentDependency())
						dataSource.put(LoadFirstCourseImEduGroupAddEdit.LOAD_BASE_EXCLUDE, ImEduGroupLoadBase.HOURS);
					else
						dataSource.put(LoadFirstCourseImEduGroupAddEdit.LOAD_BASE_EXCLUDE, ImEduGroupLoadBase.STUDENTS);
				}

			}
		}
	}

	//Handlers
	public void onClickApply()
	{
		ILoadDAO dao = LoadManager.instance().dao();
		if (_hoursMaxUnwrapped != null && getRecord().getHours() > _hoursMaxUnwrapped)
		{
			ContextLocal.getErrorCollector().add("В выбранной учебной группе не распределено " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(UniEppUtils.wrap(_hoursMaxUnwrapped)) + " часов. Укажите число часов не превосходящее данное.", "hours");
			return;
		}

		if (isAddForm())
		{
			getRecord().setOwner(dao.getNotNull(LoadRecordOwner.class, getPpsId()));
			getRecord().setActivityPart(getRecord().getRow().getRegistryElementPart());
		}

		dao.saveOrUpdate(getRecord());
		dao.saveImEduGroupTimeRules(getRecord().getId(), getTimeRules());
		deactivate();
	}

	public void onClickChangeGroupType()
	{
		if (isStudentDependency() != null)
		{
			ILoadDAO dao = LoadManager.instance().dao();
			onClickRestoreLoadTimeRuleRelation();

			if (getRecord().getRow() != null && !isStudentDependency())
			{
				EppALoadType loadType = IUniBaseDao.instance.get().getNotNull(EppALoadType.class, EppALoadType.eppGroupType(), (EppGroupTypeALT) getRecord().getType());
				Long firstCourseEduGroupMaxHoursUnwrapped = dao.getFirstCourseEduGroupLeftHoursUnwrapped(getRecord().getRow().getId(), loadType, getRecord().getId());
				firstCourseEduGroupMaxHoursUnwrapped = Math.max(firstCourseEduGroupMaxHoursUnwrapped, 0L);
				getRecord().setHours(firstCourseEduGroupMaxHoursUnwrapped);
				_hoursMaxUnwrapped = firstCourseEduGroupMaxHoursUnwrapped;
			}
			else
			{
				getRecord().setHours(0);
				_hoursMaxUnwrapped = null;
			}
		}
	}

	public void onClickRestoreLoadTimeRuleRelation()
	{
		setTimeRules(LoadManager.instance().dao().getTimeRule4EduGroupType(getRecord().getType(), getPupnagId()));
	}

	//Utils
	/**
	 * Определеляет зависимость УГС от основания нормы времени ()
	 *
	 * @return {@code true} если УГС зависит только от количества студентов, {@code false} УГС зависит только от часов
	 */
	private Boolean isStudentDependency()
	{
		if(getRecord() != null && getRecord().getType() != null)
		{
			if(getRecord().getType() instanceof EppGroupTypeFCA)
				return true;
			if(getRecord().getType() instanceof EppGroupTypeALT)
				return false;
		}
		return null;
	}

	//Getters/setters
	public void onClickChangeSourceFilter()
	{
		getRecord().setRow(null);
		getRecord().setType(null);
		getRecord().setHours(0);
		getRecord().setStudents(0);
	}

	public Long getRecordId()
	{
		return _recordId;
	}

	public void setRecordId(Long recordId)
	{
		_recordId = recordId;
	}

	public Long getPpsId()
	{
		return _ppsId;
	}

	public void setPpsId(Long ppsId)
	{
		_ppsId = ppsId;
	}

	public Long getPupnagId()
	{
		return _pupnagId;
	}

	public void setPupnagId(Long pupnagId)
	{
		_pupnagId = pupnagId;
	}

	public LoadRecordOwner getPps()
	{
		if(isAddForm())
			return LoadManager.instance().dao().getNotNull(LoadRecordOwner.class, getPpsId());
		else
			return getRecord().getOwner();
	}

	public LoadFirstCourseImEduGroup getRecord()
	{
		return _record;
	}

	public void setRecord(LoadFirstCourseImEduGroup record)
	{
		_record = record;
	}

	public EppWorkPlan getWorkPlan()
	{
		return _workPlan;
	}

	public void setWorkPlan(EppWorkPlan workPlan)
	{
		_workPlan = workPlan;
	}

	public Collection<LoadTimeRule> getTimeRules()
	{
		return _timeRules;
	}

	public void setTimeRules(Collection<LoadTimeRule> timeRules)
	{
		_timeRules = timeRules;
	}

	public boolean isAddForm()
	{
		return _recordId == null;
	}

	public boolean isEditForm()
	{
		return !isAddForm();
	}

	public IEntity getSourceFilter()
	{
		return _sourceFilter;
	}

	public void setSourceFilter(IEntity sourceFilter)
	{
		_sourceFilter = sourceFilter;
	}

	public List<IdentifiableWrapper<IEntity>> getSourceFilterList()
	{
		return SOURCE_FILTER_LIST;
	}

	public boolean isLoadTimeRuleDisabled()
	{
		return getRecord() == null || getRecord().getType() == null;
	}

	public boolean isStudentsDisable()
	{
		return isStudentDependency() == null;
	}

	public boolean isHoursDisable()
	{
		return isStudentDependency() == null || isStudentDependency();
	}
}