/*$Id$*/
package ru.tandemservice.uniload_demo.base.bo.Load.util.print;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import ru.tandemservice.uniload_demo.entity.LoadRecordOwner;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static ru.tandemservice.uniload_demo.base.bo.Load.util.print.ConsolidatedLoadDocumentBuilder.HourType;
import static ru.tandemservice.uniload_demo.base.bo.Load.util.print.ConsolidatedLoadDocumentBuilder.SheetType;
import static ru.tandemservice.uniload_demo.base.bo.Load.util.print.LoadDocumentPrintSharedUtils.Term;

/**
 * @author DMITRY KNYAZEV
 * @since 24.11.2014
 */
public final class ConsolidatedDocumentWrapper
{
	private final Map<SheetType, ConsolidatedDocumentTermWrapper> _wrappedDocumentSheet;
	private final Map<HourType, ConsolidatedDocumentTermWrapper> _wrappedDocumentHour;
	private final Map<Long, ConsolidatedDocumentPpsRowWrapper> _wrapperPpsRows;

	public ConsolidatedDocumentWrapper()
	{
		_wrappedDocumentSheet = createDocumentSheet();
		_wrappedDocumentHour = createDocumentHour();
		_wrapperPpsRows = new HashMap<>();
	}

	private Map<SheetType, ConsolidatedDocumentTermWrapper> createDocumentSheet()
	{
		Map<SheetType, ConsolidatedDocumentTermWrapper> map = new HashMap<>();
		for (SheetType type : SheetType.values())
			map.put(type, new ConsolidatedDocumentTermWrapper());
		return map;
	}

	private Map<HourType, ConsolidatedDocumentTermWrapper> createDocumentHour()
	{
		Map<HourType, ConsolidatedDocumentTermWrapper> map = new HashMap<>();
		for (HourType type : HourType.values())
			map.put(type, new ConsolidatedDocumentTermWrapper());
		return map;
	}

	public void getWrappedPpsRows(Multimap</*post title*/String, ConsolidatedDocumentPpsRowWrapper> state, Multimap</*post title*/String, ConsolidatedDocumentPpsRowWrapper> freelance)
	{
		ConsolidatedDocumentPpsRowSplitter rowSplitter = new ConsolidatedDocumentPpsRowSplitter(getWrappedPpsRows());
		state.putAll(rowSplitter.getState());
		freelance.putAll(rowSplitter.getFreelance());
	}

	private Collection<ConsolidatedDocumentPpsRowWrapper> getWrappedPpsRows()
	{
		return _wrapperPpsRows.values();
	}

	public ConsolidatedDocumentPpsRowWrapper getWrappedPpsRow(LoadRecordOwner owner)
	{
		ConsolidatedDocumentPpsRowWrapper res;
		if (_wrapperPpsRows.containsKey(owner.getId()))
			res = _wrapperPpsRows.get(owner.getId());
		else
		{
			res = new ConsolidatedDocumentPpsRowWrapper(owner);
			_wrapperPpsRows.put(owner.getId(), res);
		}
		return res;
	}

	public Map<String, Double> getDocumentHourLoad(Term term)
	{
		Map<String, Double> loadMap = new HashMap<>();
		for (ConsolidatedDocumentTermWrapper termWrapper : _wrappedDocumentHour.values())
			LoadDocumentPrintSharedUtils.mergeMaps(loadMap, termWrapper.getTerm(term).getSheetLoad());
		return loadMap;
	}

	public ConsolidatedDocumentSheetWrapper getSheet(SheetType type, Term term)
	{
		return _wrappedDocumentSheet.get(type).getTerm(term);
	}

	public ConsolidatedDocumentSheetWrapper getSheet(HourType type, Term term)
	{
		return _wrappedDocumentHour.get(type).getTerm(term);
	}

	private class ConsolidatedDocumentTermWrapper
	{
		private final Map<Term, ConsolidatedDocumentSheetWrapper> _wrappedDocumentTerm;

		public ConsolidatedDocumentTermWrapper()
		{
			_wrappedDocumentTerm = createDocumentTerm();
		}

		private Map<Term, ConsolidatedDocumentSheetWrapper> createDocumentTerm()
		{
			Map<Term, ConsolidatedDocumentSheetWrapper> map = new HashMap<>();
			for (Term term : Term.values())
				map.put(term, new ConsolidatedDocumentSheetWrapper());
			return map;
		}

		private ConsolidatedDocumentSheetWrapper getTerm(Term term)
		{
			return _wrappedDocumentTerm.get(term);
		}

	}

	private class ConsolidatedDocumentPpsRowSplitter
	{
		private final Multimap</*post title*/String, ConsolidatedDocumentPpsRowWrapper> _state;
		private final Multimap</*post title*/String, ConsolidatedDocumentPpsRowWrapper> _freelance;

		public Multimap<String, ConsolidatedDocumentPpsRowWrapper> getState()
		{
			return _state;
		}

		public Multimap<String, ConsolidatedDocumentPpsRowWrapper> getFreelance()
		{
			return _freelance;
		}

		private ConsolidatedDocumentPpsRowSplitter(Collection<ConsolidatedDocumentPpsRowWrapper> sourceData)
		{
			_state = HashMultimap.create();
			_freelance = HashMultimap.create();
			for (ConsolidatedDocumentPpsRowWrapper ppsRowWrapper : sourceData)
			{
				String post = ppsRowWrapper.getPost();
				if (ppsRowWrapper.isFreelance())
					_freelance.put(post, ppsRowWrapper);
				else
					_state.put(post, ppsRowWrapper);
			}
		}
	}
}
