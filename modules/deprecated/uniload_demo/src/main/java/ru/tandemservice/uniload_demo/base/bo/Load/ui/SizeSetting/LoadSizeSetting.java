/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.base.bo.Load.ui.SizeSetting;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeeTypeCodes;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 20.12.2013
 */
@Configuration
public class LoadSizeSetting extends BusinessComponentManager
{
    public static final String POST_DS = "postDS";
    public static final String PUPNAG_DS = "pupnagDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(PUPNAG_DS, pupnagDSHandler()))
                .addDataSource(searchListDS(POST_DS, postDSColumns(), postDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler pupnagDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppYearEducationProcess.class)
                .order(EppYearEducationProcess.educationYear().intValue());
    }

    @Bean
    public ColumnListExtPoint postDSColumns()
    {
        return columnListExtPointBuilder(POST_DS)
                .addColumn(textColumn("title", EppYearEducationProcess.title()).width("200px"))
                .addColumn(blockColumn("maxSize", "maxSizeBlockColumn"))
		        .addColumn(blockColumn("minSize", "minSizeBlockColumn"))
		        .addColumn(blockColumn("totalSize", "totalSizeBlockColumn"))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler postDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), PostBoundedWithQGandQL.class)
        {
	        @Override
	        protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
	        {
		        dql.where(eq(property(alias, PostBoundedWithQGandQL.post().employeeType().code()), value(EmployeeTypeCodes.EDU_STAFF)));
		        super.applyWhereConditions(alias, dql, context);
	        }
        }.filter(PostBoundedWithQGandQL.title());
    }
}