/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.base.bo.Load.ui.WorkPlanRowTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniload_demo.base.bo.Load.LoadManager;
import ru.tandemservice.uniload_demo.base.bo.Load.ui.WorkPlanRowEdit.LoadWorkPlanRowEdit;
import ru.tandemservice.uniload_demo.base.bo.Load.ui.WorkPlanRowEdit.LoadWorkPlanRowEditUI;

/**
 * @author Alexander Zhebko
 * @since 20.12.2013
 */
@Input(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "orgUnitId"))
@SuppressWarnings("UnusedDeclaration")
public class LoadWorkPlanRowTabUI extends UIPresenter
{
	private Long _orgUnitId;
	private boolean _workPlanRowEditDisabled;

	public Long getOrgUnitId()
	{
		return _orgUnitId;
	}

	public void setOrgUnitId(Long orgUnitId)
	{
		_orgUnitId = orgUnitId;
	}

	public boolean isWorkPlanRowEditDisabled()
	{
		return _workPlanRowEditDisabled;
	}

	@Override
	public void onComponentRefresh()
	{
		_workPlanRowEditDisabled = LoadManager.instance().dao().isOrgUnitWorkPlanRowEditDisabled(_orgUnitId);
		setPupnag();
	}

	@Override
	public void onBeforeDataSourceFetch(IUIDataSource dataSource)
	{
		if (dataSource.getName().equals(LoadWorkPlanRowTab.WORK_PLAN_ROW_DS))
		{
			dataSource.put(LoadWorkPlanRowTab.ORG_UNIT_ID, _orgUnitId);
			dataSource.put(LoadWorkPlanRowTab.PUPNAG, getPupnag());
			dataSource.put(LoadWorkPlanRowTab.EDU_YEAR_PART, getSettings().get(LoadWorkPlanRowTab.EDU_YEAR_PART));
			dataSource.put(EducationCatalogsManager.DS_DEVELOP_FORM, getSettings().get(EducationCatalogsManager.DS_DEVELOP_FORM));
			dataSource.put(LoadWorkPlanRowTab.DEVELOP_CONDITION, getSettings().get(LoadWorkPlanRowTab.DEVELOP_CONDITION));
			dataSource.put(LoadWorkPlanRowTab.DEVELOP_TECH, getSettings().get(LoadWorkPlanRowTab.DEVELOP_TECH));
			dataSource.put(LoadWorkPlanRowTab.DEVELOP_GRID, getSettings().get(LoadWorkPlanRowTab.DEVELOP_GRID));
		}
		if (dataSource.getName().equals(LoadWorkPlanRowTab.EDU_YEAR_PART_DS))
		{
			dataSource.put(LoadWorkPlanRowTab.PUPNAG, getPupnag());
		}
	}

	public void onClickSearch()
	{
		saveSettings();
	}

	public void onClickClear()
	{
		clearSettings();
		setPupnag();
		saveSettings();
	}

	public void onEditEntityFromList()
	{
		_uiActivation.asRegionDialog(LoadWorkPlanRowEdit.class)
				.parameter(PUBLISHER_ID, getListenerParameterAsLong())
				.parameter(LoadWorkPlanRowEditUI.PUPNAG_ID, getPupnag().getId()).activate();
	}

	private EppYearEducationProcess getPupnag()
	{
		return getSettings().get("pupnag");
	}

	private void setPupnag()
	{
		getSettings().set("pupnag", LoadManager.instance().dao().getNextYear());
	}
}