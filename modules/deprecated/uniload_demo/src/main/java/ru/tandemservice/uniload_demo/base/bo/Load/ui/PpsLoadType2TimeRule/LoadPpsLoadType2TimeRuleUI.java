/*$Id$*/
package ru.tandemservice.uniload_demo.base.bo.Load.ui.PpsLoadType2TimeRule;

import com.google.common.collect.Multimap;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.context.ContextLocal;
import ru.tandemservice.uniload_demo.base.bo.Load.LoadManager;
import ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule;
import ru.tandemservice.uniload_demo.entity.catalog.PpsLoadGroup;
import ru.tandemservice.uniload_demo.entity.catalog.PpsLoadType;
import ru.tandemservice.uniload_demo.entity.catalog.codes.PpsLoadGroupCodes;

import java.util.Collection;
import java.util.HashSet;

/**
 * @author DMITRY KNYAZEV
 * @since 16.09.2014
 */
public class LoadPpsLoadType2TimeRuleUI extends UIPresenter
{
	private Multimap<PpsLoadType, LoadTimeRule> _loadTypeSource;
	private Multimap<PpsLoadGroup, LoadTimeRule> _loadGroupSource;

	@Override
	public void onComponentRefresh()
	{
		_loadTypeSource = LoadManager.instance().dao().getPpsLoadType2TimeRuleMultimap();
		_loadGroupSource = LoadManager.instance().dao().getPpsLoadGroup2TimeRuleMultimap();
	}

	@Override
	public void onBeforeDataSourceFetch(IUIDataSource dataSource)
	{
		if (dataSource.getName().equals(LoadPpsLoadType2TimeRule.LOAD_TYPE_TIME_RULE_DS))
		{
			dataSource.put(LoadPpsLoadType2TimeRule.USED_TIME_RULE_LIST, getLoadTypeSource().values());
			dataSource.put(LoadPpsLoadType2TimeRule.ADDED_TIME_RULE_LIST, getTimeRule2TypeList());
		}
		if (dataSource.getName().equals(LoadPpsLoadType2TimeRule.LOAD_GROUP_TIME_RULE_DS))
		{
			dataSource.put(LoadPpsLoadType2TimeRule.USED_TIME_RULE_LIST, getLoadGroupSource().values());
			dataSource.put(LoadPpsLoadType2TimeRule.ADDED_TIME_RULE_LIST, getTimeRule2GroupList());
		}
	}

	//handlers
	public void onClickApply()
	{
		if(getLoadTypeSource().values().size() != new HashSet<>(getLoadTypeSource().values()).size())
		{
			ContextLocal.getErrorCollector().add("В рамках уч.года каждая норма времени должна быть использована только один раз");
			return;
		}
		if(getLoadGroupSource().values().size() != new HashSet<>(getLoadGroupSource().values()).size())
		{
			ContextLocal.getErrorCollector().add("В рамках уч.года каждая норма времени должна быть использована только один раз");
			return;
		}
		LoadManager.instance().dao().updatePpsLoadType2TimeRule(getLoadTypeSource());
		LoadManager.instance().dao().updatePpsLoadGroup2TimeRule(getLoadGroupSource());
	}

	public void onClickSave()
	{
		onClickApply();
		deactivate();
	}

	//getters/setters
	public Collection<LoadTimeRule> getTimeRule2TypeList()
	{
		return getLoadTypeSource().get(getCurrentTypeRow());
	}

	public Collection<LoadTimeRule> getTimeRule2GroupList()
	{
		return getLoadGroupSource().get(getCurrentGroupRow());
	}

	public void setTimeRule2TypeList(Collection<LoadTimeRule> timeRuleList)
	{
		PpsLoadType eppType = getCurrentTypeRow();
		if(getLoadTypeSource().containsKey(eppType))
			getLoadTypeSource().replaceValues(eppType, timeRuleList);
		else
			getLoadTypeSource().putAll(eppType, timeRuleList);
	}

	public void setTimeRule2GroupList(Collection<LoadTimeRule> timeRuleList)
	{
		PpsLoadGroup eppType = getCurrentGroupRow();
		if(getLoadGroupSource().containsKey(eppType))
			getLoadGroupSource().replaceValues(eppType, timeRuleList);
		else
			getLoadGroupSource().putAll(eppType, timeRuleList);
	}

	private Multimap<PpsLoadType, LoadTimeRule> getLoadTypeSource()
	{
		return _loadTypeSource;
	}

	public Multimap<PpsLoadGroup, LoadTimeRule> getLoadGroupSource()
	{
		return _loadGroupSource;
	}

	private PpsLoadType getCurrentTypeRow()
	{
		return ((PpsLoadType) getConfig().getDataSource(LoadPpsLoadType2TimeRule.PPS_LOAD_TYPE_DS).getCurrent());
	}

	private PpsLoadGroup getCurrentGroupRow()
	{
		return ((PpsLoadGroup) getConfig().getDataSource(LoadPpsLoadType2TimeRule.PPS_LOAD_GROUP_DS).getCurrent());
	}

	public boolean isGroupDisabled(){
		return getCurrentGroupRow().getCode().equals(PpsLoadGroupCodes.AUDITORNYE_ZANYATIYA);
	}
}
