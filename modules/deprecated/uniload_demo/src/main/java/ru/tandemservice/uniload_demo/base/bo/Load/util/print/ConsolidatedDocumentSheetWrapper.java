/*$Id$*/
package ru.tandemservice.uniload_demo.base.bo.Load.util.print;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramFormCodes;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.uniload_demo.base.bo.Load.LoadManager;
import ru.tandemservice.uniload_demo.entity.ILoadImEduGroup;
import ru.tandemservice.uniload_demo.entity.LoadRecordOwner;
import ru.tandemservice.uniload_demo.entity.OtherLoad;
import ru.tandemservice.uniload_demo.entity.catalog.PpsLoadCathedraPart;

import java.util.*;

/**
 * @author DMITRY KNYAZEV
 * @since 02.12.2014
 */
public final class ConsolidatedDocumentSheetWrapper
{
	private final Map<String, ConsolidatedDocumentSuperBlockWrapper> wrappedSheet;

	public ConsolidatedDocumentSheetWrapper()
	{
		wrappedSheet = createSheet();
	}

	public ConsolidatedDocumentSuperBlockWrapper getBlock(EduProgramForm eduForm)
	{
		String eduFormCode = eduForm.getCode();
		ConsolidatedDocumentSuperBlockWrapper superBlock = wrappedSheet.get(eduFormCode);
		if (superBlock == null)
			throw new ApplicationException("Нет данных блока с указанным кодом формы обучения: " + eduFormCode);
		return superBlock;
	}

	public Map<String, Double> getSheetLoad()
	{
		Map<String, Double> loadMap = new HashMap<>();
		for (ConsolidatedDocumentSuperBlockWrapper superBlockWrapper : getSuperBlockWrappers())
		{
			LoadDocumentPrintSharedUtils.mergeMaps(loadMap, superBlockWrapper.getSuperBlockLoad());
		}
		return loadMap;
	}

	public Map<String, Double> getBlockLoad(EduProgramForm eduForm)
	{
		return getBlock(eduForm).getSuperBlockLoad();
	}

	private Map<String, ConsolidatedDocumentSuperBlockWrapper> createSheet()
	{
		Map<String, ConsolidatedDocumentSuperBlockWrapper> map = new HashMap<>();
		for (String eduFormCod : EduProgramFormCodes.CODES)
			map.put(eduFormCod, new ConsolidatedDocumentSuperBlockWrapper());
		return map;
	}

	private Collection<ConsolidatedDocumentSuperBlockWrapper> getSuperBlockWrappers()
	{
		return wrappedSheet.values();
	}

	public class ConsolidatedDocumentSuperBlockWrapper
	{
		private final Map<Long , ConsolidatedDocumentPostgraduateRowWrapper> postgraduateLoadMap;
		private final Set<String> _usedCathedraPartCodes;
		private final Map<String, ConsolidatedDocumentBlockWrapper> _superBockMap;

		private ConsolidatedDocumentSuperBlockWrapper()
		{
			_usedCathedraPartCodes = getDisabledCathedraPartIds();
			_superBockMap = createSuperBlock();
			postgraduateLoadMap = new HashMap<>();
		}

		private Map<String, ConsolidatedDocumentBlockWrapper> createSuperBlock()
		{
			Map<String, ConsolidatedDocumentBlockWrapper> map = new HashMap<>();
			for (String eduFormCod : _usedCathedraPartCodes)
				map.put(eduFormCod, new ConsolidatedDocumentBlockWrapper());
			return map;
		}

		public void addToBlock(Collection<PpsLoadCathedraPart> cathedraPartList, EppWorkPlanRegistryElementRow discipline, LoadRecordOwner person, ILoadImEduGroup imEduGroup)
		{
			for (PpsLoadCathedraPart cathedraPart : cathedraPartList)
			{
				if (isBlockExist(cathedraPart)) getBlock(cathedraPart).addRow(discipline, person, imEduGroup);
			}
		}

        public void addToPostgraduateRow(OtherLoad otherLoad)
        {
            if (postgraduateLoadMap.containsKey(otherLoad.getId()))
                throw new ApplicationException("Данная нагрузка уже добавлена:" + otherLoad.getTitle());
            else
            {
                final ConsolidatedDocumentPostgraduateRowWrapper postgraduateLoadWrapper = new ConsolidatedDocumentPostgraduateRowWrapper(otherLoad);
                postgraduateLoadMap.put(otherLoad.getId(), postgraduateLoadWrapper);
            }
        }

		public Map<String, Double> getSuperBlockLoad()
		{
			Map<String, Double> loadMap = new HashMap<>();
			for (ConsolidatedDocumentBlockWrapper rowWrapper : getBlockWrappers())
				LoadDocumentPrintSharedUtils.mergeMaps(loadMap, rowWrapper.getBlockLoadMap());
			return loadMap;
		}

		public List<ConsolidatedDocumentRowWrapper> getBlockRows(PpsLoadCathedraPart cathedraPart)
		{
			if (isBlockExist(cathedraPart))
				return getBlock(cathedraPart).getRowWrappers();
			else
				throw new ApplicationException("Указан не верный элемент раздела нагрузки кафедры: " + cathedraPart.getTitle());
		}

		public Double getBlockTotalLoad(PpsLoadCathedraPart cathedraPart)
		{
			if (isBlockExist(cathedraPart))
				return getBlock(cathedraPart).getBlockLoadMap().get(ConsolidatedDocumentRowWrapper.ROW_TOTAL_LOAD_CODE);
			else
				throw new ApplicationException("Указан не верный элемент раздела нагрузки кафедры: " + cathedraPart.getTitle());
		}

		public List<ConsolidatedDocumentPostgraduateRowWrapper> getPostgraduateBlockRows()
		{
			return new ArrayList<>(postgraduateLoadMap.values());
		}

		public Map<String, Double> getPostgraduateBlockLoad()
		{
			Map<String, Double> loadMap = new HashMap<>();
			for (ConsolidatedDocumentPostgraduateRowWrapper rowWrapper : postgraduateLoadMap.values())
				LoadDocumentPrintSharedUtils.mergeMaps(loadMap, rowWrapper.getRowLoadMap());
			return loadMap;
		}

		private ConsolidatedDocumentBlockWrapper getBlock(PpsLoadCathedraPart cathedraPart)
		{
			return _superBockMap.get(cathedraPart.getCode());
		}

		private boolean isBlockExist(PpsLoadCathedraPart cathedraPart)
		{
			return _superBockMap.containsKey(cathedraPart.getCode());
		}

		private Collection<ConsolidatedDocumentBlockWrapper> getBlockWrappers()
		{
			return _superBockMap.values();
		}

		private Set<String> getDisabledCathedraPartIds()
		{
			String lcp = "lcp";
			DQLSelectBuilder unused = new DQLSelectBuilder().fromEntity(PpsLoadCathedraPart.class, lcp)
					.column(DQLExpressions.property(lcp, PpsLoadCathedraPart.parent().id()))
					.where(DQLExpressions.isNotNull(DQLExpressions.property(lcp, PpsLoadCathedraPart.L_PARENT)))
					.distinct();

			DQLSelectBuilder used = new DQLSelectBuilder().fromEntity(PpsLoadCathedraPart.class, lcp)
					.column(DQLExpressions.property(lcp, PpsLoadCathedraPart.P_CODE))
					.where(DQLExpressions.notIn(DQLExpressions.property(lcp, PpsLoadCathedraPart.P_ID), unused.buildQuery()));

			return new HashSet<>(LoadManager.instance().dao().<String>getList(used));
		}
	}
}

