/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.base.bo.Load.ui.PpsAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLSelectableQuery;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import ru.tandemservice.uni.entity.employee.pps.EmployeePost4PpsEntry;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.employee.pps.PpsEntryByEmployeePost;
import ru.tandemservice.uniload_demo.entity.LoadRecordOwner;
import ru.tandemservice.unisnpps.entity.pps.PpsEntryByTimeworker;
import ru.tandemservice.unisnpps.entity.pps.Timeworker4PpsEntry;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 20.12.2013
 */
@Configuration
public class LoadPpsAddEdit extends BusinessComponentManager
{
	public static final String PPS_DS = "ppsDS";
	public static final String POST_DS = "postDS";

	public static final String ORG_UNIT_ID = "orgUnitId";
	public static final String PPS_ID = "ppsId";
    public static final String PUPNAG_ID = "pupnagId";

    @Override
	@Bean
	public PresenterExtPoint presenterExtPoint()
	{
		return presenterExtPointBuilder()
				.addDataSource(selectDS(PPS_DS, ppsDSHandler()))
				.addDataSource(selectDS(POST_DS, postDSHandler()))
				.create();
	}

	@Bean
	public IDefaultComboDataSourceHandler ppsDSHandler()
	{
		return new EntityComboDataSourceHandler(getName(), PpsEntry.class)
		{
			@Override
			protected DQLSelectBuilder query(String alias, String filter)
			{
				final DQLSelectBuilder query = super.query(alias, filter);
				query.joinEntity(alias, DQLJoinType.left, PpsEntryByEmployeePost.class, "eep", eq(property("eep", PpsEntryByEmployeePost.id()), property(alias, PpsEntry.id())));
				query.joinEntity(alias, DQLJoinType.left, PpsEntryByTimeworker.class, "et", eq(property("et", PpsEntryByTimeworker.id()), property(alias, PpsEntry.id())));

				query.joinEntity(alias, DQLJoinType.left, EmployeePost4PpsEntry.class, "ep", eq(property("ep", EmployeePost4PpsEntry.ppsEntry().id()), property("eep.id")));
				query.joinEntity(alias, DQLJoinType.left, Timeworker4PpsEntry.class, "tw", eq(property("tw", Timeworker4PpsEntry.ppsEntry().id()), property("et.id")));

				query.joinPath(DQLJoinType.left, EmployeePost4PpsEntry.employeePost().postStatus().fromAlias("ep"), "ps");

				query.where(or(
								likeUpper(
										DQLFunctions.concat(
												property(alias, PpsEntry.person().identityCard().fullFio()),
												property(alias, PpsEntry.orgUnit().shortTitle())),
										value(CoreStringUtils.escapeLike(filter, true))
								),
								likeUpper(
										property("eep", PpsEntryByEmployeePost.post().title()),
										value(CoreStringUtils.escapeLike(filter, true))
								)
						)
				);
				return query;
			}

			@Override
			protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
			{
				Long orgUnitId = context.get(ORG_UNIT_ID);
				Long ppsId = context.get(PPS_ID);
                Long pupnagId= context.get(PUPNAG_ID);

				IDQLSelectableQuery distributedPpsQuery = new DQLSelectBuilder()
						.fromEntity(LoadRecordOwner.class, "ro")
						.joinPath(DQLJoinType.inner, LoadRecordOwner.realPps().pps().fromAlias("ro"), "p")
						.column(property("p", PpsEntry.id()))
						.where(eq(property("p", PpsEntry.orgUnit().id()), value(orgUnitId)))
						.where(ppsId == null ? null : ne(property("p", PpsEntry.id()), value(ppsId)))
                        .where(eq(property("ro", LoadRecordOwner.year()), value(pupnagId)))
						.buildQuery();

                dql.where(eq(property(alias, PpsEntry.orgUnit().id()), value(orgUnitId)));
                dql.where(notIn(property(alias, PpsEntry.id()), distributedPpsQuery));
                dql.where(or(
						eq(property("ps", EmployeePostStatus.active()), value(Boolean.TRUE)),
						eq(property("tw", Timeworker4PpsEntry.timeworker().archival()), value(Boolean.FALSE))
				));

				super.applyWhereConditions(alias, dql, context);
			}
		};
	}

	@Bean
	public IDefaultComboDataSourceHandler postDSHandler()
	{
		return new EntityComboDataSourceHandler(getName(), PostBoundedWithQGandQL.class).filter(PostBoundedWithQGandQL.title());
	}
}