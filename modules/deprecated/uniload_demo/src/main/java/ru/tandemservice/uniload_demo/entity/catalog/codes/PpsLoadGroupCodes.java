package ru.tandemservice.uniload_demo.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Группы нагрузки ППС"
 * Имя сущности : ppsLoadGroup
 * Файл data.xml : uniload_demo.catalog.data.xml
 */
public interface PpsLoadGroupCodes
{
    /** Константа кода (code) элемента : Аудиторные занятия (title) */
    String AUDITORNYE_ZANYATIYA = "1";
    /** Константа кода (code) элемента : Консультации (title) */
    String KONSULTATSII = "2";
    /** Константа кода (code) элемента : Контроль (title) */
    String KONTROL = "3";
    /** Константа кода (code) элемента : Практика (title) */
    String PRAKTIKA = "4";
    /** Константа кода (code) элемента : Руководство (title) */
    String RUKOVODSTVO = "5";

    Set<String> CODES = ImmutableSet.of(AUDITORNYE_ZANYATIYA, KONSULTATSII, KONTROL, PRAKTIKA, RUKOVODSTVO);
}
