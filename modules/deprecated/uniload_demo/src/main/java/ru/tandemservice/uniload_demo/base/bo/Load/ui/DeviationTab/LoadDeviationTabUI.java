/*$Id$*/
package ru.tandemservice.uniload_demo.base.bo.Load.ui.DeviationTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniload_demo.base.bo.Load.LoadManager;
import ru.tandemservice.uniload_demo.base.bo.Load.ui.DeviationAddEdit.LoadDeviationAddEdit;
import ru.tandemservice.uniload_demo.base.bo.Load.ui.DeviationAddEdit.LoadDeviationAddEditUI;

/**
 * @author DMITRY KNYAZEV
 * @since 25.12.2014
 */
@Input(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "orgUnitHolder.id", required = true))
@SuppressWarnings("UnusedDeclaration")
public class LoadDeviationTabUI extends UIPresenter
{
	private final OrgUnitHolder _orgUnitHolder = new OrgUnitHolder();

	@Override
	public void onComponentRefresh()
	{
		setPupnag();
	}

	@Override
	public void onBeforeDataSourceFetch(IUIDataSource dataSource)
	{
		if (dataSource.getName().equals(LoadDeviationTab.DEVIATION_DS))
		{
			dataSource.put(LoadDeviationTab.KEY_SOURCE, LoadManager.instance().dao().getDeviationWrapperList(getOrgUnitId(), getPupnagId()));
		}
	}

	//HANDLERS
	public void onClickEditRecord()
	{
		_uiActivation.asRegionDialog(LoadDeviationAddEdit.class)
				.parameter(PublisherActivator.PUBLISHER_ID_KEY, getOrgUnitId())
				.parameter(LoadDeviationAddEditUI.DEVIATION_ID, this.<Long>getListenerParameter())
				.parameter(LoadDeviationAddEditUI.PUPNAG_ID, getPupnagId()).activate();
	}

	public void onClickDeleteRecord()
	{
		LoadManager.instance().dao().delete(this.<Long>getListenerParameter());
		_uiSupport.doRefresh();
	}

	public void onClickAddDeviation()
	{
		_uiActivation.asRegionDialog(LoadDeviationAddEdit.class)
				.parameter(PublisherActivator.PUBLISHER_ID_KEY, getOrgUnitId())
				.parameter(LoadDeviationAddEditUI.PUPNAG_ID, getPupnagId()).activate();
	}

	public void onClickSearch()
	{
		saveSettings();
	}

	public void onClickClear()
	{
		clearSettings();
		setPupnag();
		saveSettings();
	}

	//GETTERS/SETTERS
	public boolean isActionsDisabled()
	{
		return getPupnag() == null;
	}

	public Long getOrgUnitId()
	{
		return getOrgUnitHolder().getId();
	}

	public OrgUnitHolder getOrgUnitHolder()
	{
		return _orgUnitHolder;
	}

	public CommonPostfixPermissionModelBase getSecModel()
	{
		return getOrgUnitHolder().getSecModel();
	}

	private EppYearEducationProcess getPupnag()
	{
		return getSettings().get("pupnag");
	}

	private void setPupnag()
	{
		getSettings().set("pupnag", LoadManager.instance().dao().getNextYear());
	}

	private Long getPupnagId()
	{
		return getPupnag().getId();
	}
}
