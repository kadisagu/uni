/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.entity;

import org.tandemframework.core.common.ITitled;

/**
 * Основание нормы времени. Персистентное перечисление. Удаление/переименование элементов должно сопровождаться МИГРАЦИЕЙ.
 * @author Alexander Zhebko
 * @since 12.12.2013
 */
public enum ImEduGroupLoadBase implements ITitled
{
    CONSTANT("Фикс. число"),
    STUDENTS("Студенты"),
    HOURS("Часы");

    private String _title;
    public String getTitle(){ return _title; }

    private ImEduGroupLoadBase(String title){ _title = title; }
}