/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.component.catalog.loadTimeRule.LoadTimeRuleAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import org.tandemframework.shared.commonbase.utils.CommonYesNoUIObject;
import ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule;

/**
 * @author Alexander Zhebko
 * @since 23.12.2013
 */
public class DAO extends DefaultCatalogAddEditDAO<LoadTimeRule, Model> implements IDAO
{
	@Override
	public void prepare(Model model)
	{
		super.prepare(model);
		model.setPermissionModel(CommonYesNoUIObject.createYesNoList());
		model.setPermission(CommonYesNoUIObject.getObject(model.getCatalogItem().isManualAddPermit()));
	}

	@Override
	public void update(Model model)
	{
		model.getCatalogItem().setManualAddPermit(model.getPermission().isTrue());
		super.update(model);
	}
}