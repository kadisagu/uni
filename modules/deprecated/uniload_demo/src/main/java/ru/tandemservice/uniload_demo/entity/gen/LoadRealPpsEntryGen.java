package ru.tandemservice.uniload_demo.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniload_demo.entity.LoadRealPpsEntry;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Дополнительные данные по сотруднику
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class LoadRealPpsEntryGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniload_demo.entity.LoadRealPpsEntry";
    public static final String ENTITY_NAME = "loadRealPpsEntry";
    public static final int VERSION_HASH = 992759932;
    private static IEntityMeta ENTITY_META;

    public static final String L_PPS = "pps";
    public static final String P_FREELANCE = "freelance";
    public static final String P_RATE = "rate";

    private PpsEntry _pps;     // ППС
    private boolean _freelance;     // признака штатный/нештатный
    private int _rate;     // ставка

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ППС. Свойство не может быть null.
     */
    @NotNull
    public PpsEntry getPps()
    {
        return _pps;
    }

    /**
     * @param pps ППС. Свойство не может быть null.
     */
    public void setPps(PpsEntry pps)
    {
        dirty(_pps, pps);
        _pps = pps;
    }

    /**
     * @return признака штатный/нештатный. Свойство не может быть null.
     */
    @NotNull
    public boolean isFreelance()
    {
        return _freelance;
    }

    /**
     * @param freelance признака штатный/нештатный. Свойство не может быть null.
     */
    public void setFreelance(boolean freelance)
    {
        dirty(_freelance, freelance);
        _freelance = freelance;
    }

    /**
     * @return ставка. Свойство не может быть null.
     */
    @NotNull
    public int getRate()
    {
        return _rate;
    }

    /**
     * @param rate ставка. Свойство не может быть null.
     */
    public void setRate(int rate)
    {
        dirty(_rate, rate);
        _rate = rate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof LoadRealPpsEntryGen)
        {
            setPps(((LoadRealPpsEntry)another).getPps());
            setFreelance(((LoadRealPpsEntry)another).isFreelance());
            setRate(((LoadRealPpsEntry)another).getRate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends LoadRealPpsEntryGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) LoadRealPpsEntry.class;
        }

        public T newInstance()
        {
            return (T) new LoadRealPpsEntry();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "pps":
                    return obj.getPps();
                case "freelance":
                    return obj.isFreelance();
                case "rate":
                    return obj.getRate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "pps":
                    obj.setPps((PpsEntry) value);
                    return;
                case "freelance":
                    obj.setFreelance((Boolean) value);
                    return;
                case "rate":
                    obj.setRate((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "pps":
                        return true;
                case "freelance":
                        return true;
                case "rate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "pps":
                    return true;
                case "freelance":
                    return true;
                case "rate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "pps":
                    return PpsEntry.class;
                case "freelance":
                    return Boolean.class;
                case "rate":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<LoadRealPpsEntry> _dslPath = new Path<LoadRealPpsEntry>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "LoadRealPpsEntry");
    }
            

    /**
     * @return ППС. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadRealPpsEntry#getPps()
     */
    public static PpsEntry.Path<PpsEntry> pps()
    {
        return _dslPath.pps();
    }

    /**
     * @return признака штатный/нештатный. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadRealPpsEntry#isFreelance()
     */
    public static PropertyPath<Boolean> freelance()
    {
        return _dslPath.freelance();
    }

    /**
     * @return ставка. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadRealPpsEntry#getRate()
     */
    public static PropertyPath<Integer> rate()
    {
        return _dslPath.rate();
    }

    public static class Path<E extends LoadRealPpsEntry> extends EntityPath<E>
    {
        private PpsEntry.Path<PpsEntry> _pps;
        private PropertyPath<Boolean> _freelance;
        private PropertyPath<Integer> _rate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ППС. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadRealPpsEntry#getPps()
     */
        public PpsEntry.Path<PpsEntry> pps()
        {
            if(_pps == null )
                _pps = new PpsEntry.Path<PpsEntry>(L_PPS, this);
            return _pps;
        }

    /**
     * @return признака штатный/нештатный. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadRealPpsEntry#isFreelance()
     */
        public PropertyPath<Boolean> freelance()
        {
            if(_freelance == null )
                _freelance = new PropertyPath<Boolean>(LoadRealPpsEntryGen.P_FREELANCE, this);
            return _freelance;
        }

    /**
     * @return ставка. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadRealPpsEntry#getRate()
     */
        public PropertyPath<Integer> rate()
        {
            if(_rate == null )
                _rate = new PropertyPath<Integer>(LoadRealPpsEntryGen.P_RATE, this);
            return _rate;
        }

        public Class getEntityClass()
        {
            return LoadRealPpsEntry.class;
        }

        public String getEntityName()
        {
            return "loadRealPpsEntry";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
