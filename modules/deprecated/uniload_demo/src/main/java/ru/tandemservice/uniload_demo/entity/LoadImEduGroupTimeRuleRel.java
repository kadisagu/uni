package ru.tandemservice.uniload_demo.entity;

import ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule;
import ru.tandemservice.uniload_demo.entity.gen.*;

/**
 * Связь нормы времени и планируемой УГС
 */
public class LoadImEduGroupTimeRuleRel extends LoadImEduGroupTimeRuleRelGen
{
    public LoadImEduGroupTimeRuleRel()
    {

    }

    public LoadImEduGroupTimeRuleRel(ILoadImEduGroup eduGroup, LoadTimeRule timeRule)
    {
        this.setEduGroup(eduGroup);
        this.setTimeRule(timeRule);
    }
}