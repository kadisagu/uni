/*$Id$*/
package ru.tandemservice.uniload_demo.base.bo.Load.ui.TimeRule2EduGroupTypeRel;

import com.google.common.collect.Multimap;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.context.ContextLocal;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeALT;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeFCA;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniload_demo.base.bo.Load.LoadManager;
import ru.tandemservice.uniload_demo.entity.ImEduGroupLoadBase;
import ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule;

import java.util.Collection;
import java.util.HashSet;

/**
 * @author DMITRY KNYAZEV
 * @since 01.09.2014
 */
@SuppressWarnings("UnusedDeclaration")
public class LoadTimeRule2EduGroupTypeRelUI extends UIPresenter
{
	private EppYearEducationProcess _year;
	private Multimap<EppGroupType, LoadTimeRule> _source;

	@Override
	public void onBeforeDataSourceFetch(IUIDataSource dataSource)
	{
		if (dataSource.getName().equals(LoadTimeRule2EduGroupTypeRel.LOAD_TYPE_TIME_RULE_DS))
		{
			dataSource.put(LoadTimeRule2EduGroupTypeRel.USED_TIME_RULE_LIST, getSource().values());
			dataSource.put(LoadTimeRule2EduGroupTypeRel.ADDED_TIME_RULE_LIST, getLoadTimeRule());
			dataSource.put(LoadTimeRule2EduGroupTypeRel.LOAD_BASE_EXCLUDE, ImEduGroupLoadBase.STUDENTS);
		}

		if (dataSource.getName().equals(LoadTimeRule2EduGroupTypeRel.CONTROL_TYPE_TIME_RULE_DS))
		{
			dataSource.put(LoadTimeRule2EduGroupTypeRel.USED_TIME_RULE_LIST, getSource().values());
			dataSource.put(LoadTimeRule2EduGroupTypeRel.ADDED_TIME_RULE_LIST, getControlTimeRule());
			dataSource.put(LoadTimeRule2EduGroupTypeRel.LOAD_BASE_EXCLUDE, ImEduGroupLoadBase.HOURS);
		}
	}

	@Override
	public void onComponentRefresh()
	{
		setYear(LoadManager.instance().dao().getNextYear());
		onChangeYear();
	}

	//handlers
	public void onChangeYear()
	{
		_source = LoadManager.instance().dao().getTimeRule2EduGroupTypeMultimap(getYear().getId());
	}

	public void onClickApply()
	{
		if (getSource().values().size() != new HashSet<>(getSource().values()).size())
		{
			ContextLocal.getErrorCollector().add("В рамках уч.года каждая норма времени должна быть использована только один раз");
			return;
		}
		LoadManager.instance().dao().updateTimeRule2EduGroupTypeRel(getYear().getId(), getSource());
	}

	public void onClickSave()
	{
		onClickApply();
		deactivate();
	}

	//getters/setters
	private EppGroupTypeALT getCurrentLoadRow()
	{
		return ((EppGroupTypeALT) getConfig().getDataSource(LoadTimeRule2EduGroupTypeRel.EPP_LOAD_TYPE_DS).getCurrent());
	}

	private EppGroupTypeFCA getCurrentControlRow()
	{
		return ((EppGroupTypeFCA) getConfig().getDataSource(LoadTimeRule2EduGroupTypeRel.EPP_CONTROL_TYPE_DS).getCurrent());
	}

	public Collection<LoadTimeRule> getLoadTimeRule()
	{
		return getTimeRuleList(getCurrentLoadRow());
	}

	public void setLoadTimeRule(Collection<LoadTimeRule> timeRuleList)
	{
		setTimeRuleList(getCurrentLoadRow(), timeRuleList);
	}

	public Collection<LoadTimeRule> getControlTimeRule()
	{
		return getTimeRuleList(getCurrentControlRow());
	}

	public void setControlTimeRule(Collection<LoadTimeRule> timeRuleList)
	{
		setTimeRuleList(getCurrentControlRow(), timeRuleList);
	}

	public EppYearEducationProcess getYear()
	{
		return _year;
	}

	public void setYear(EppYearEducationProcess year)
	{
		_year = year;
	}

	private Collection<LoadTimeRule> getTimeRuleList(EppGroupType eppType)
	{
		return getSource().get(eppType);
	}

	private void setTimeRuleList(EppGroupType eppType, Collection<LoadTimeRule> timeRuleList)
	{
		if (getSource().containsKey(eppType))
			getSource().replaceValues(eppType, timeRuleList);
		else
			getSource().putAll(eppType, timeRuleList);
	}

	private Multimap<EppGroupType, LoadTimeRule> getSource()
	{
		return _source;
	}
}
