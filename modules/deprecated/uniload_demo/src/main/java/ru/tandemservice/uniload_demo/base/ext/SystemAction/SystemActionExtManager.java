/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.base.ext.SystemAction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.SystemActionManager;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.util.SystemActionDefinition;
import ru.tandemservice.uniload_demo.base.ext.SystemAction.ui.Pub.SystemActionPubExt;


/**
 * @author Alexander Zhebko
 * @since 20.12.2013
 */
@Configuration
@SuppressWarnings("SpringFacetCodeInspection")
public class SystemActionExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private SystemActionManager _systemActionManager;

    @Bean
    public ItemListExtension<SystemActionDefinition> buttonListExtension()
    {
        return itemListExtension(_systemActionManager.buttonListExtPoint())
                .add("uniload_createNextYearEduGroups", new SystemActionDefinition("uniload_demo", "createNextYearEduGroups", "onClickCreateNextYearEduGroups", SystemActionPubExt.SYSTEM_ACTION_PUB_ADDON_NAME))
		        .add("uniload_resetEduGroups", new SystemActionDefinition("uniload_demo", "resetEduGroups", "onClickResetEduGroups", SystemActionPubExt.SYSTEM_ACTION_PUB_ADDON_NAME))
                .create();
    }
}
