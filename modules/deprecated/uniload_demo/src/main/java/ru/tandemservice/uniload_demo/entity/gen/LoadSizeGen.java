package ru.tandemservice.uniload_demo.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniload_demo.entity.LoadSize;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Максимальная и минимальная нагрузка ППС
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class LoadSizeGen extends EntityBase
 implements INaturalIdentifiable<LoadSizeGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniload_demo.entity.LoadSize";
    public static final String ENTITY_NAME = "loadSize";
    public static final int VERSION_HASH = -1128337098;
    private static IEntityMeta ENTITY_META;

    public static final String L_YEAR = "year";
    public static final String L_POST = "post";
    public static final String P_MAX_SIZE = "maxSize";
    public static final String P_MIN_SIZE = "minSize";
    public static final String P_TOTAL_SIZE = "totalSize";

    private EppYearEducationProcess _year;     // ПУПнаГ
    private PostBoundedWithQGandQL _post;     // Должность
    private Long _maxSize;     // Размер максимальной нагрузки
    private Long _minSize;     // Размер минимальной нагрузки
    private Long _totalSize;     // Общий объем рабочего времени

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ПУПнаГ. Свойство не может быть null.
     */
    @NotNull
    public EppYearEducationProcess getYear()
    {
        return _year;
    }

    /**
     * @param year ПУПнаГ. Свойство не может быть null.
     */
    public void setYear(EppYearEducationProcess year)
    {
        dirty(_year, year);
        _year = year;
    }

    /**
     * @return Должность. Свойство не может быть null.
     */
    @NotNull
    public PostBoundedWithQGandQL getPost()
    {
        return _post;
    }

    /**
     * @param post Должность. Свойство не может быть null.
     */
    public void setPost(PostBoundedWithQGandQL post)
    {
        dirty(_post, post);
        _post = post;
    }

    /**
     * @return Размер максимальной нагрузки.
     */
    public Long getMaxSize()
    {
        return _maxSize;
    }

    /**
     * @param maxSize Размер максимальной нагрузки.
     */
    public void setMaxSize(Long maxSize)
    {
        dirty(_maxSize, maxSize);
        _maxSize = maxSize;
    }

    /**
     * @return Размер минимальной нагрузки.
     */
    public Long getMinSize()
    {
        return _minSize;
    }

    /**
     * @param minSize Размер минимальной нагрузки.
     */
    public void setMinSize(Long minSize)
    {
        dirty(_minSize, minSize);
        _minSize = minSize;
    }

    /**
     * @return Общий объем рабочего времени.
     */
    public Long getTotalSize()
    {
        return _totalSize;
    }

    /**
     * @param totalSize Общий объем рабочего времени.
     */
    public void setTotalSize(Long totalSize)
    {
        dirty(_totalSize, totalSize);
        _totalSize = totalSize;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof LoadSizeGen)
        {
            if (withNaturalIdProperties)
            {
                setYear(((LoadSize)another).getYear());
                setPost(((LoadSize)another).getPost());
            }
            setMaxSize(((LoadSize)another).getMaxSize());
            setMinSize(((LoadSize)another).getMinSize());
            setTotalSize(((LoadSize)another).getTotalSize());
        }
    }

    public INaturalId<LoadSizeGen> getNaturalId()
    {
        return new NaturalId(getYear(), getPost());
    }

    public static class NaturalId extends NaturalIdBase<LoadSizeGen>
    {
        private static final String PROXY_NAME = "LoadSizeNaturalProxy";

        private Long _year;
        private Long _post;

        public NaturalId()
        {}

        public NaturalId(EppYearEducationProcess year, PostBoundedWithQGandQL post)
        {
            _year = ((IEntity) year).getId();
            _post = ((IEntity) post).getId();
        }

        public Long getYear()
        {
            return _year;
        }

        public void setYear(Long year)
        {
            _year = year;
        }

        public Long getPost()
        {
            return _post;
        }

        public void setPost(Long post)
        {
            _post = post;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof LoadSizeGen.NaturalId) ) return false;

            LoadSizeGen.NaturalId that = (NaturalId) o;

            if( !equals(getYear(), that.getYear()) ) return false;
            if( !equals(getPost(), that.getPost()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getYear());
            result = hashCode(result, getPost());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getYear());
            sb.append("/");
            sb.append(getPost());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends LoadSizeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) LoadSize.class;
        }

        public T newInstance()
        {
            return (T) new LoadSize();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "year":
                    return obj.getYear();
                case "post":
                    return obj.getPost();
                case "maxSize":
                    return obj.getMaxSize();
                case "minSize":
                    return obj.getMinSize();
                case "totalSize":
                    return obj.getTotalSize();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "year":
                    obj.setYear((EppYearEducationProcess) value);
                    return;
                case "post":
                    obj.setPost((PostBoundedWithQGandQL) value);
                    return;
                case "maxSize":
                    obj.setMaxSize((Long) value);
                    return;
                case "minSize":
                    obj.setMinSize((Long) value);
                    return;
                case "totalSize":
                    obj.setTotalSize((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "year":
                        return true;
                case "post":
                        return true;
                case "maxSize":
                        return true;
                case "minSize":
                        return true;
                case "totalSize":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "year":
                    return true;
                case "post":
                    return true;
                case "maxSize":
                    return true;
                case "minSize":
                    return true;
                case "totalSize":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "year":
                    return EppYearEducationProcess.class;
                case "post":
                    return PostBoundedWithQGandQL.class;
                case "maxSize":
                    return Long.class;
                case "minSize":
                    return Long.class;
                case "totalSize":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<LoadSize> _dslPath = new Path<LoadSize>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "LoadSize");
    }
            

    /**
     * @return ПУПнаГ. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadSize#getYear()
     */
    public static EppYearEducationProcess.Path<EppYearEducationProcess> year()
    {
        return _dslPath.year();
    }

    /**
     * @return Должность. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadSize#getPost()
     */
    public static PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> post()
    {
        return _dslPath.post();
    }

    /**
     * @return Размер максимальной нагрузки.
     * @see ru.tandemservice.uniload_demo.entity.LoadSize#getMaxSize()
     */
    public static PropertyPath<Long> maxSize()
    {
        return _dslPath.maxSize();
    }

    /**
     * @return Размер минимальной нагрузки.
     * @see ru.tandemservice.uniload_demo.entity.LoadSize#getMinSize()
     */
    public static PropertyPath<Long> minSize()
    {
        return _dslPath.minSize();
    }

    /**
     * @return Общий объем рабочего времени.
     * @see ru.tandemservice.uniload_demo.entity.LoadSize#getTotalSize()
     */
    public static PropertyPath<Long> totalSize()
    {
        return _dslPath.totalSize();
    }

    public static class Path<E extends LoadSize> extends EntityPath<E>
    {
        private EppYearEducationProcess.Path<EppYearEducationProcess> _year;
        private PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> _post;
        private PropertyPath<Long> _maxSize;
        private PropertyPath<Long> _minSize;
        private PropertyPath<Long> _totalSize;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ПУПнаГ. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadSize#getYear()
     */
        public EppYearEducationProcess.Path<EppYearEducationProcess> year()
        {
            if(_year == null )
                _year = new EppYearEducationProcess.Path<EppYearEducationProcess>(L_YEAR, this);
            return _year;
        }

    /**
     * @return Должность. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadSize#getPost()
     */
        public PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> post()
        {
            if(_post == null )
                _post = new PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL>(L_POST, this);
            return _post;
        }

    /**
     * @return Размер максимальной нагрузки.
     * @see ru.tandemservice.uniload_demo.entity.LoadSize#getMaxSize()
     */
        public PropertyPath<Long> maxSize()
        {
            if(_maxSize == null )
                _maxSize = new PropertyPath<Long>(LoadSizeGen.P_MAX_SIZE, this);
            return _maxSize;
        }

    /**
     * @return Размер минимальной нагрузки.
     * @see ru.tandemservice.uniload_demo.entity.LoadSize#getMinSize()
     */
        public PropertyPath<Long> minSize()
        {
            if(_minSize == null )
                _minSize = new PropertyPath<Long>(LoadSizeGen.P_MIN_SIZE, this);
            return _minSize;
        }

    /**
     * @return Общий объем рабочего времени.
     * @see ru.tandemservice.uniload_demo.entity.LoadSize#getTotalSize()
     */
        public PropertyPath<Long> totalSize()
        {
            if(_totalSize == null )
                _totalSize = new PropertyPath<Long>(LoadSizeGen.P_TOTAL_SIZE, this);
            return _totalSize;
        }

        public Class getEntityClass()
        {
            return LoadSize.class;
        }

        public String getEntityName()
        {
            return "loadSize";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
