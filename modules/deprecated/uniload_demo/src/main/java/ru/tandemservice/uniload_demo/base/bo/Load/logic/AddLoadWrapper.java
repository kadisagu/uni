/*$Id$*/
package ru.tandemservice.uniload_demo.base.bo.Load.logic;

import org.tandemframework.shared.employeebase.base.bo.EmployeePost.logic.IEmployeeAdditionalInfo;

/**
 * @author DMITRY KNYAZEV
 * @since 04.08.2014
 */
public class AddLoadWrapper
{
	private final Double _rate;
	private final Boolean _freelance;

	public AddLoadWrapper(Integer rate, Boolean freelance)
	{
		_rate = rate == null ? 0D : rate / IEmployeeAdditionalInfo.MULTIPLIER.doubleValue();
		_freelance = freelance == null ? false : freelance;
	}

	public Double getRate()
	{
		return _rate;
	}

	public Boolean getFreelance()
	{
		return _freelance;
	}
}
