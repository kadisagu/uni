/**
 * $Id$
 */
package ru.tandemservice.uniload_demo.base.bo.Load.ui.EduGroupAdd;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Transformer;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.group.IEppRealGroupRowDAO;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeALT;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeFCA;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModuleALoad;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniload_demo.base.bo.Load.LoadManager;
import ru.tandemservice.uniload_demo.base.bo.Load.logic.ILoadDAO;
import ru.tandemservice.uniload_demo.entity.ImEduGroupLoadBase;
import ru.tandemservice.uniload_demo.entity.LoadEduGroupPpsDistr;
import ru.tandemservice.uniload_demo.entity.LoadImEduGroupTimeRuleRel;
import ru.tandemservice.uniload_demo.entity.LoadRecordOwner;
import ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 20.12.2013
 */
@Input({
        @Bind(key = LoadEduGroupAddUI.PPS_ID_KEY, binding = LoadEduGroupAddUI.PPS_ID_KEY, required = true),
        @Bind(key = LoadEduGroupAddUI.PUPNAG_ID_KEY, binding = LoadEduGroupAddUI.PUPNAG_ID_KEY, required = true),
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = LoadEduGroupAddUI.EDU_GROUP_ID_KEY)})
public class LoadEduGroupAddUI extends UIPresenter
{

    public static final String PPS_ID_KEY = "ppsId";
    public static final String PUPNAG_ID_KEY = "pupnagId";
    public static final String EDU_GROUP_ID_KEY = "eduGroupId";

    private static final IdentifiableWrapper<IEntity> SOURCE_OWNER_ORG_UNIT = new IdentifiableWrapper<>(1L, "С читающего подразделения");
    private static final IdentifiableWrapper<IEntity> SOURCE_ALL_ORG_UNIT = new IdentifiableWrapper<>(2L, "Со всех подразделений");
    private static final List<IdentifiableWrapper<IEntity>> SOURCE_FILTER_LIST = Arrays.asList(
            SOURCE_OWNER_ORG_UNIT,
            SOURCE_ALL_ORG_UNIT
    );

    private Long _ppsId;
    private Long _eduGroupId;
    private Long _pupnagId;
    private EppRealEduGroup _eduGroup;
    private IEntity _sourceFilter;
    private String _eduGroupTitle;

    private Integer _students;
    private Double _loadSize;

    private Integer _studentsUsed;
    private Double _loadSizeUsed;

    private Integer _maxStudents;
    private Double _maxLoadSize;

    private Collection<LoadTimeRule> _timeRules;
    private LoadRecordOwner _pps;

    @Override
    public void onComponentRefresh()
    {
        ILoadDAO dao = LoadManager.instance().dao();
        _pps = dao.get(LoadRecordOwner.class, getPpsId());
        if (isEditForm())
        {
            setEduGroup(dao.get(getEduGroupId()));
            LoadEduGroupPpsDistr distr = dao.getByNaturalId(new LoadEduGroupPpsDistr.NaturalId(_pps, getEduGroup()));
            setEduGroupTitle(distr.getEduGroupTitle());
            fillStudentsAndLoadSize();
            setStudents(distr.getStudents());
            setLoadSize(UniEppUtils.wrap(distr.getHours()));
            if (getEduGroupDependency().isStudent())
                _studentsUsed -= getStudents();
            _loadSizeUsed -= getLoadSize();
            List<LoadImEduGroupTimeRuleRel> timeRuleRels = LoadManager.instance().dao().getList(LoadImEduGroupTimeRuleRel.class, LoadImEduGroupTimeRuleRel.eduGroup().id(), distr.getId(), LoadImEduGroupTimeRuleRel.timeRule().title().s());
            Transformer<LoadImEduGroupTimeRuleRel, LoadTimeRule> transformer = LoadImEduGroupTimeRuleRel::getTimeRule;
            setTimeRules(CollectionUtils.collect(timeRuleRels, transformer));
        }

        boolean owner = isAddForm() || getEduGroup().getActivityPart().getRegistryElement().getOwner().getId().equals(_pps.getOrgUnit().getId());
        setSourceFilter(owner ? SOURCE_OWNER_ORG_UNIT : SOURCE_ALL_ORG_UNIT);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(LoadEduGroupAdd.EDU_GROUP_DS))
        {
            dataSource.put(LoadEduGroupAdd.PPS_ID, getPpsId());
            dataSource.put(LoadEduGroupAdd.ORG_UNIT_ID, getSourceFilter() == SOURCE_OWNER_ORG_UNIT ? _pps.getOrgUnit().getId() : null);
            if (isEditForm())
                dataSource.put(LoadEduGroupAdd.CURRENT, getEduGroup().getId());
        }
        if (dataSource.getName().equals(LoadEduGroupAdd.TIME_RULE_DS))
        {
            dataSource.put(LoadEduGroupAdd.TIME_RULE_INCLUDE, getTimeRules());

            final EduGrDep eduGroupDependency = getEduGroupDependency();
            if (eduGroupDependency.isHours())
                dataSource.put(LoadEduGroupAdd.LOAD_BASE_EXCLUDE, ImEduGroupLoadBase.HOURS);
            if (eduGroupDependency.isStudent())
                dataSource.put(LoadEduGroupAdd.LOAD_BASE_EXCLUDE, ImEduGroupLoadBase.STUDENTS);
        }
    }

    //Utils

    /**
     * Определеляет зависимость УГС от основания нормы времени ()
     *
     * @return {@link EduGrDep#STUDENTS} если УГС зависит только от количества студентов,<br/>
     * {@link EduGrDep#HOURS} УГС зависит только от часов,<br/>
     * {@link EduGrDep#UNKNOWN} УГС равна {@code null}.
     * @throws IllegalArgumentException если УГС не равна {@code null} и невозможно определить от какой нормы времени зависит УГС
     */
    private EduGrDep getEduGroupDependency()
    {
        if (getEduGroup() != null)
        {
            if (getEduGroup().getType() instanceof EppGroupTypeFCA)
                return EduGrDep.STUDENTS;
            if (getEduGroup().getType() instanceof EppGroupTypeALT)
                return EduGrDep.HOURS;
            throw new IllegalArgumentException(getEduGroup().getTitle() + "has illegal type:" + getEduGroup().getType());
        } else
            return EduGrDep.UNKNOWN;

    }

    // use only if _eduGroup != null
    private void fillStudentsAndLoadSize()
    {
        ILoadDAO dao = LoadManager.instance().dao();

        _studentsUsed = 0;
        _maxStudents = dao.getCount(EppRealEduGroupRow.class, EppRealEduGroupRow.group().id().s(), getEduGroup().getId());
        _loadSizeUsed = _maxLoadSize = 0D;

        for (LoadEduGroupPpsDistr distr : dao.getList(LoadEduGroupPpsDistr.class, LoadEduGroupPpsDistr.eduGroup().id(), getEduGroup().getId()))
        {
            final EduGrDep eduGroupDependency = getEduGroupDependency();
            if (eduGroupDependency.isStudent())
                _studentsUsed += distr.getStudents();
            if (eduGroupDependency.isHours())
                _loadSizeUsed += UniEppUtils.wrap(distr.getHours());
        }
        if (getEduGroup().getType() instanceof EppGroupTypeALT)
        {
            DQLSelectBuilder moduleLoadBuilder = new DQLSelectBuilder()
                    .fromEntity(EppRegistryModuleALoad.class, "ml")
                    .joinEntity("ml", DQLJoinType.inner, EppRegistryElementPartModule.class, "repm", eq(property("repm", EppRegistryElementPartModule.module()), property("ml", EppRegistryModuleALoad.module())))
                    .where(eq(property("repm", EppRegistryElementPartModule.part()), value(getEduGroup().getActivityPart())))
                    .where(eq(property("ml", EppRegistryModuleALoad.loadType().eppGroupType()), value(getEduGroup().getType())))
                    .column(property("ml", EppRegistryModuleALoad.load()));

            for (Long load : dao.<Long>getList(moduleLoadBuilder))
            {
                _maxLoadSize += UniEppUtils.wrap(load);
            }
        }
    }

    // Handlers
    public void onClickApply()
    {
        ErrorCollector errorCollector = ContextLocal.getErrorCollector();
        // если учитываем только количество студентов
        final EduGrDep eduGroupDependency = getEduGroupDependency();
        if (eduGroupDependency.isStudent())
        {
            if (getStudents() > getStudentsLeft())
                errorCollector.add("В выбранной учебной группе не распределено " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(getStudentsLeft()) + " студентов. Укажите число студентов не превосходящее данное.", "students");
            if (getStudents() == 0)
                errorCollector.add("В выбранной учебной группе нет студентов для распределения.", "students");
        }
        // если учитываем только часы
        if (eduGroupDependency.isHours())
        {
            if (getLoadSize() > getLoadSizeLeft())
                errorCollector.add("В выбранной учебной группе не распределено " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(getLoadSizeLeft()) + " часов. Укажите число часов не превосходящее данное.", "hours");
            if (getStudents() == 0)
                errorCollector.add("В выбранной учебной группе нет часов для распределения.", "hours");
        }

        if (errorCollector.hasErrors())
            return;

        ILoadDAO dao = LoadManager.instance().dao();

        if (_pps.isReal())
        {
            PpsEntry ppsEntry = _pps.getRealPps().getPps();
            EppPpsCollectionItem ppsGroup = dao.getByNaturalId(new EppPpsCollectionItem.NaturalId(getEduGroup(), ppsEntry));
            if (ppsGroup == null)
            {
                ppsGroup = new EppPpsCollectionItem(getEduGroup(), ppsEntry);
                dao.save(ppsGroup);
            }
        }

        LoadEduGroupPpsDistr distr = dao.getByNaturalId(new LoadEduGroupPpsDistr.NaturalId(_pps, getEduGroup()));
        if (distr == null)
            distr = new LoadEduGroupPpsDistr(_pps, getEduGroup());

        distr.setEduGroupTitle(getEduGroupTitle());
        distr.setStudents(getStudents());
        distr.setHours(UniEppUtils.unwrap(getLoadSize()));

        dao.saveOrUpdate(distr);
        dao.saveImEduGroupTimeRules(distr.getId(), getTimeRules());
        deactivate();
    }

    public void onClickChangeSourceFilter()
    {
        setEduGroup(null);
        setEduGroupTitle(null);
        setTimeRules(null);
        setStudents(null);
        setLoadSize(null);
        _studentsUsed = null;
        _maxStudents = null;
        _loadSizeUsed = null;
        _maxLoadSize = null;
    }

    public void onClickSelectEduGroup()
    {
        if (getEduGroup() != null)
        {
            fillStudentsAndLoadSize();
            final EduGrDep eduGroupDependency = getEduGroupDependency();
            // если учитываем только количество студентов
            if (eduGroupDependency.isStudent())
            {
                setLoadSize(0D);
                setStudents(getStudentsLeft());
            }
            // если учитываем только часы
            if (eduGroupDependency.isHours())
            {
                setLoadSize(getLoadSizeLeft());
                setStudents(getMaxStudents());
            }
            setTimeRules(LoadManager.instance().dao().getTimeRule4EduGroupType(getEduGroup().getType(), getPupnagId()));
            setEduGroupTitle(IEppRealGroupRowDAO.instance.get().calculateTitle(getEduGroup()));
        } else
        {
            setStudents(null);
            setLoadSize(null);
            setTimeRules(null);
            setEduGroupTitle(null);
        }
    }

    public void onClickRestoreLoadTimeRuleRelation()
    {
        setTimeRules(LoadManager.instance().dao().getTimeRule4EduGroupType(getEduGroup().getType(), getPupnagId()));
    }

    //Getters/Setters
    public Long getPupnagId()
    {
        return _pupnagId;
    }

    public void setPupnagId(Long pupnagId)
    {
        _pupnagId = pupnagId;
    }

    public Long getPpsId()
    {
        return _ppsId;
    }

    public void setPpsId(Long ppsId)
    {
        _ppsId = ppsId;
    }

    public EppYearEducationProcess getPupnag()
    {
        return LoadManager.instance().dao().getNotNull(getPupnagId());
    }

    public Long getEduGroupId()
    {
        return _eduGroupId;
    }

    public void setEduGroupId(Long eduGroupId)
    {
        _eduGroupId = eduGroupId;
    }

    public EppRealEduGroup getEduGroup()
    {
        return _eduGroup;
    }

    public void setEduGroup(EppRealEduGroup eduGroup)
    {
        _eduGroup = eduGroup;
    }

    public IEntity getSourceFilter()
    {
        return _sourceFilter;
    }

    public void setSourceFilter(IEntity sourceFilter)
    {
        _sourceFilter = sourceFilter;
    }

    public Integer getStudents()
    {
        return _students;
    }

    public void setStudents(Integer students)
    {
        _students = students;
    }

    public Double getLoadSize()
    {
        return _loadSize;
    }

    public void setLoadSize(Double loadSize)
    {
        _loadSize = loadSize;
    }

    public Integer getMaxStudents()
    {
        return _maxStudents;
    }

    public Double getMaxLoadSize()
    {
        return _maxLoadSize;
    }

    public Double getLoadSizeUsed()
    {
        return _loadSizeUsed;
    }

    public Integer getStudentsUsed()
    {
        return _studentsUsed;
    }

    public Double getLoadSizeLeft()
    {
        if (_maxLoadSize != null && _loadSizeUsed != null)
            return _maxLoadSize - _loadSizeUsed;
        return null;
    }

    public Integer getStudentsLeft()
    {
        if (_maxStudents != null && _studentsUsed != null)
            return _maxStudents - _studentsUsed;
        return null;
    }

    public Collection<LoadTimeRule> getTimeRules()
    {
        return _timeRules;
    }

    public void setTimeRules(Collection<LoadTimeRule> timeRules)
    {
        _timeRules = timeRules;
    }

    public List<IdentifiableWrapper<IEntity>> getSourceFilterList()
    {
        return SOURCE_FILTER_LIST;
    }

    public boolean isEditForm()
    {
        return _eduGroupId != null;
    }

    public boolean isAddForm()
    {
        return !isEditForm();
    }

    public boolean isStudentsDisable()
    {
        return !getEduGroupDependency().isStudent();
    }

    public boolean isLoadSizeDisable()
    {
        return !getEduGroupDependency().isHours();
    }

    public boolean isInfoTableEnable()
    {
        return !isLoadTimeRuleDisabled();
    }

    public boolean isLoadTimeRuleDisabled()
    {
        return getEduGroup() == null;
    }

    public String getEduGroupTitle()
    {
        return _eduGroupTitle;
    }

    public void setEduGroupTitle(String eduGroupTitle)
    {
        _eduGroupTitle = eduGroupTitle;
    }

    //Help classes
    private enum EduGrDep
    {
        STUDENTS,
        HOURS,
        UNKNOWN;

        public boolean isStudent()
        {
            return STUDENTS.equals(this);
        }

        public boolean isHours()
        {
            return HOURS.equals(this);
        }
    }
}