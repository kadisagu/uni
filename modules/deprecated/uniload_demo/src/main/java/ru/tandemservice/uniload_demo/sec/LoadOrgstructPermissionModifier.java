/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.sec;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.*;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.uniload_demo.entity.LoadState;

import java.util.Map;

/**
 * @author Alexander Zhebko
 * @since 20.12.2013
 */
public class LoadOrgstructPermissionModifier implements ISecurityConfigMetaMapModifier
{
	@Override
	public void modify(Map<String, SecurityConfigMeta> securityConfigMetaMap)
	{
		SecurityConfigMeta config = new SecurityConfigMeta();
		config.setModuleName("uniload_demo");
		config.setName("uniload_demo-sec-config");
		config.setTitle("");

		ModuleGlobalGroupMeta moduleGlobalGroupMeta = PermissionMetaUtil.createModuleGlobalGroup(config, "orgstructModuleGlobalGroup", "Модуль «Орг. структура»");
		ModuleLocalGroupMeta moduleLocalGroupMeta = PermissionMetaUtil.createModuleLocalGroup(config, "orgstructLocalGroup", "Модуль «Орг. структура»");

		for (OrgUnitType description : DataAccessServices.dao().getList(OrgUnitType.class, OrgUnitType.P_TITLE))
		{
			String code = description.getCode();
			ClassGroupMeta globalClassGroup = PermissionMetaUtil.createClassGroup(moduleGlobalGroupMeta, code + "PermissionClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());
			ClassGroupMeta localClassGroup = PermissionMetaUtil.createClassGroup(moduleLocalGroupMeta, code + "LocalClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());

			// "Вкладка «Нагрузка ППС»"
			final PermissionGroupMeta permissionGroupLoadTab = PermissionMetaUtil.createPermissionGroup(config, code + "LoadPermissionGroup", "Вкладка «Нагрузка ППС»");

			// "Вкладка «ППС»"
			final PermissionGroupMeta permissionGroupPpsTab = PermissionMetaUtil.createPermissionGroup(permissionGroupLoadTab, code + "PpsPermissionGroup", "Вкладка «ППС»");
			PermissionMetaUtil.createPermission(permissionGroupPpsTab, "orgUnit_viewLoadTab_" + code, "Просмотр");
			PermissionMetaUtil.createPermission(permissionGroupPpsTab, "orgUnit_changeOrgUnitLoadState_" + LoadState.FORMATIVE.toString() + "_" + code, "Отправка на формирование нагрузки подразделения");
			PermissionMetaUtil.createPermission(permissionGroupPpsTab, "orgUnit_changeOrgUnitLoadState_" + LoadState.ACCEPTABLE.toString() + "_" + code, "Отправка на согласование нагрузки подразделения");
			PermissionMetaUtil.createPermission(permissionGroupPpsTab, "orgUnit_changeOrgUnitLoadState_" + LoadState.ACCEPTED.toString() + "_" + code, "Согласование нагрузки подразделения");
			PermissionMetaUtil.createPermission(permissionGroupPpsTab, "orgUnit_addPpsLoadRecord_" + code, "Добавление преподавателя");
			PermissionMetaUtil.createPermission(permissionGroupPpsTab, "orgUnit_printConsolidatedReport_" + code, "Печать свода");
			PermissionMetaUtil.createPermission(permissionGroupPpsTab, "orgUnit_editPpsLoadRecord_" + code, "Редактирование преподавателя");
			PermissionMetaUtil.createPermission(permissionGroupPpsTab, "orgUnit_printPpsLoadRecord_" + code, "Печать преподавателя");
			PermissionMetaUtil.createPermission(permissionGroupPpsTab, "orgUnit_deletePpsLoadRecord_" + code, "Удаление преподавателя");

			// "Вкладка «Строки РУП»"
			final PermissionGroupMeta permissionGroupWorkPlanRowTab = PermissionMetaUtil.createPermissionGroup(permissionGroupLoadTab, code + "WorkPlanRowPermissionGroup", "Вкладка «Строки РУП»");
			PermissionMetaUtil.createPermission(permissionGroupWorkPlanRowTab, "view_workPlaRowsLoad_" + code, "Просмотр вкладки «Строки РУП»");

			// "Вкладка «Отклонения»"
			final PermissionGroupMeta permissionGroupDeviationTab = PermissionMetaUtil.createPermissionGroup(permissionGroupLoadTab, code + "DeviationPermissionGroup", "Вкладка «Отклонения»");
			PermissionMetaUtil.createPermission(permissionGroupDeviationTab, "view_deviationLoadTab_" + code, "Просмотр вкладки «Отклонения»");
			PermissionMetaUtil.createPermission(permissionGroupDeviationTab, "view_deviationAddEdit_" + code, "Форма добавления и редактирования отклонения");
			PermissionMetaUtil.createPermission(permissionGroupDeviationTab, "view_deviationDelete_" + code, "Удаление отклонения");

			PermissionMetaUtil.createGroupRelation(config, permissionGroupLoadTab.getName(), globalClassGroup.getName());
			PermissionMetaUtil.createGroupRelation(config, permissionGroupLoadTab.getName(), localClassGroup.getName());
		}

		securityConfigMetaMap.put(config.getName(), config);
	}
}