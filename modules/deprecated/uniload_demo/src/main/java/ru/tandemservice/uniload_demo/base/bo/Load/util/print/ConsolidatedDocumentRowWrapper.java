/*$Id$*/
package ru.tandemservice.uniload_demo.base.bo.Load.util.print;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModuleIControlAction;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.uniload_demo.entity.ILoadImEduGroup;
import ru.tandemservice.uniload_demo.entity.LoadEduGroupPpsDistr;
import ru.tandemservice.uniload_demo.entity.LoadFirstCourseImEduGroup;
import ru.tandemservice.uniload_demo.entity.LoadRecordOwner;

import java.util.*;

/**
 * @author DMITRY KNYAZEV
 * @since 25.11.2014
 */
class ConsolidatedDocumentRowWrapper extends PpsDocumentFirstSheetRowWrapper
{
	private final LoadRecordOwner _owner;

	//#3 Форма отчётности (э/з/зэ)
	Set<String> _groupTypeSet = new HashSet<>();
	//#4 Количество письменных к/р., расчетно-графических заданий
	Collection<String> _tuskNumber;

	//#11 № группы
	private final Set<String> _groupsTitle = new HashSet<>();

	public ConsolidatedDocumentRowWrapper(EppWorkPlanRegistryElementRow row, LoadRecordOwner owner)
	{
		super(row);
		_owner = owner;
		_tuskNumber = prepareTuskNumber();
	}

	@Override
	public void add(ILoadImEduGroup imEduGroup)
	{
		super.add(imEduGroup);
		//#3 Форма отчётности (э/з/зэ)
		addGroupType(imEduGroup);
		//#11 № группы
		addGroupTitle(imEduGroup);
	}

	//#3 Форма отчётности (э/з/зэ)
	private void addGroupType(ILoadImEduGroup imEduGroup)
	{
		EppGroupType type = getGroupType(imEduGroup);
		_groupTypeSet.add(type.getAbbreviation().toLowerCase());
	}

	//#4 Количество письменных к/р., расчетно-графических заданий
	private Collection<String> prepareTuskNumber()
	{
		EppRegistryElementPart registryElementPart = getRow().getRegistryElementPart();

		DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppRegistryElementPartModule.class, "m")
				.column(DQLExpressions.property("m", EppRegistryElementPartModule.module()))
				.where(DQLExpressions.eq(DQLExpressions.property("m", EppRegistryElementPartModule.module().id()), DQLExpressions.value(registryElementPart.getId())));

		Collection<EppRegistryElementPartModule> modules = getDao().getList(builder);

		builder = new DQLSelectBuilder().fromEntity(EppRegistryModuleIControlAction.class, "m")
				.where(DQLExpressions.in(DQLExpressions.property("m", EppRegistryModuleIControlAction.module()), modules));

		Collection<EppRegistryModuleIControlAction> controlActions = getDao().getList(builder);

		List<String> res = new ArrayList<>();
		for (EppRegistryModuleIControlAction controlAction : controlActions)
		{
			String s = controlAction.getControlAction().getShortTitle() + "-" + controlAction.getAmount();
			res.add(s);
		}
		return res;
	}

	//#11 № группы
	private void addGroupTitle(ILoadImEduGroup imEduGroup)
	{
		if (imEduGroup instanceof LoadEduGroupPpsDistr)
		{
			LoadEduGroupPpsDistr groupPpsDistr = (LoadEduGroupPpsDistr) imEduGroup;
			_groupsTitle.add(groupPpsDistr.getEduGroup().getTitle());
		}
		else if (imEduGroup instanceof LoadFirstCourseImEduGroup)
		{
			LoadFirstCourseImEduGroup firstCourse = (LoadFirstCourseImEduGroup) imEduGroup;
			_groupsTitle.add(firstCourse.getTitle());
		}
		else
			throw wrongType(imEduGroup);
	}

	//#3 Форма отчётности (э/з/зэ)
	public String getGroupType()
	{
		return StringUtils.join(_groupTypeSet, '/');
	}

	//#4 Количество письменных к/р., расчетно-графических заданий (смотри задачу DEV-4922)
	public String getTaskNumber()
	{
		return StringUtils.join(_tuskNumber, ";");
	}

	//#11 № группы
	public String getGroupTitle()
	{
		return StringUtils.join(_groupsTitle, ';');
	}

	//#34 Ф.И.О. преподавателя
	public String getPpsFio()
	{
		return _owner.getFullFio();
	}


	//#35 Условия занятости преподавателя (штатный (ш), внутр.совм. (с), внеш.совм. (в), почасовик (п))
	public String getPpsEmployeeStatus()
	{
		return StringUtils.join(_owner.getPpsEmployeeStatus(), ',');
	}

	//#36 № заявки от Школы (если есть)
	public String getSchoolRequest()
	{
		return "";//always empty
	}

	@Override
	public int hashCode()
	{
		return super.hashCode() & _owner.hashCode();
	}

	@Override
	public boolean equals(Object obj)
	{
		if (!(obj instanceof ConsolidatedDocumentRowWrapper)) return false;
		ConsolidatedDocumentRowWrapper instance = ((ConsolidatedDocumentRowWrapper) obj);
		return _owner.getId().equals(instance._owner.getId()) && super.equals(obj);
	}
}
