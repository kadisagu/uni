/*$Id$*/
package ru.tandemservice.uniload_demo.base.bo.Load.util.print;

import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.uniload_demo.base.bo.Load.LoadManager;
import ru.tandemservice.uniload_demo.entity.OtherLoad;

/**
 * @author DMITRY KNYAZEV
 * @since 11.11.2014
 */
class PssDocumentSecondSheetRowWrapper
{
	final private OtherLoad _otherLoad;
	final private Double _totalLoad;

	public PssDocumentSecondSheetRowWrapper(OtherLoad otherLoad)
	{
		_otherLoad = otherLoad;
		_totalLoad = LoadManager.instance().dao().getTotalEduLoad4EduGroup(_otherLoad);
	}


	public Double getLoadAsDouble()
	{
		return _totalLoad;
	}

	public String getLoad()
	{
		return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(getLoadAsDouble());
	}

	public String getDisciplineTitle()
	{
		return _otherLoad.getTitle();
	}

	@Override
	public boolean equals(Object obj)
	{
		return !(obj instanceof PssDocumentSecondSheetRowWrapper) && this._otherLoad.equals(obj);
	}

	@Override
	public int hashCode()
	{
		return _otherLoad.hashCode();
	}
}
