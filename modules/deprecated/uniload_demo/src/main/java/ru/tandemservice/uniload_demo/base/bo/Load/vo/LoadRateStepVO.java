/*$Id$*/
package ru.tandemservice.uniload_demo.base.bo.Load.vo;

import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;

/**
 * @author DMITRY KNYAZEV
 * @since 18.12.2014
 */
public class LoadRateStepVO
{
	private static final String LOAD_RATE_SETTINGS = "uniLoadSettings";
	private static final String RATE_STEP_SETTING_NAME = "rateStepSettings";
	private static final String MAX_RATE_SETTING_NAME = "maxRateSettings";

	public static final Long DEFAULT_RATE_STEP = 1L;
	public static final Long DEFAULT_MAX_RATE = 200L;

	private final Long _rateStep;
	private final Long _maxRate;

	private LoadRateStepVO(long rateStep, long maxRate)
	{
		_rateStep = rateStep;
		_maxRate = maxRate;
	}
	public static LoadRateStepVO getInstance(){
		return getLoadRateStep();
	}

	public Long getRateStep()
	{
		return _rateStep;
	}

	public Long getMaxRate()
	{
		return _maxRate;
	}
	//
	private static LoadRateStepVO getLoadRateStep(){
		IDataSettings settings = DataSettingsFacade.getSettings(LOAD_RATE_SETTINGS);

		Long rateStep = settings.get(RATE_STEP_SETTING_NAME);
		if(rateStep == null) rateStep = DEFAULT_RATE_STEP;

		Long maxRate = settings.get(MAX_RATE_SETTING_NAME);
		if(maxRate == null) maxRate = DEFAULT_MAX_RATE;

		return new LoadRateStepVO(rateStep, maxRate);
	}

	public static void saveLoadRateStep(long rateStep, long maxRate ){
		IDataSettings dataSettings = DataSettingsFacade.getSettings(LOAD_RATE_SETTINGS);

		dataSettings.set(RATE_STEP_SETTING_NAME, rateStep);
		dataSettings.set(MAX_RATE_SETTING_NAME, maxRate);

		DataSettingsFacade.saveSettings(dataSettings);
	}
}
