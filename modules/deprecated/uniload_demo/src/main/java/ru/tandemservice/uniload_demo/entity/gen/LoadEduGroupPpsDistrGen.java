package ru.tandemservice.uniload_demo.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniload_demo.entity.ILoadImEduGroup;
import ru.tandemservice.uniload_demo.entity.LoadEduGroupPpsDistr;
import ru.tandemservice.uniload_demo.entity.LoadRecordOwner;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Распределение часов УГС
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class LoadEduGroupPpsDistrGen extends EntityBase
 implements ILoadImEduGroup, INaturalIdentifiable<LoadEduGroupPpsDistrGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniload_demo.entity.LoadEduGroupPpsDistr";
    public static final String ENTITY_NAME = "loadEduGroupPpsDistr";
    public static final int VERSION_HASH = -1480569219;
    private static IEntityMeta ENTITY_META;

    public static final String L_OWNER = "owner";
    public static final String L_EDU_GROUP = "eduGroup";
    public static final String P_HOURS = "hours";
    public static final String P_STUDENTS = "students";
    public static final String P_EDU_GROUP_TITLE = "eduGroupTitle";
    public static final String P_TITLE = "title";

    private LoadRecordOwner _owner;     // ППС (нагрузка)
    private EppRealEduGroup _eduGroup;     // УГС
    private long _hours;     // Часы
    private int _students;     // Студенты
    private String _eduGroupTitle;     // Название планируемой группы

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ППС (нагрузка). Свойство не может быть null.
     */
    @NotNull
    public LoadRecordOwner getOwner()
    {
        return _owner;
    }

    /**
     * @param owner ППС (нагрузка). Свойство не может быть null.
     */
    public void setOwner(LoadRecordOwner owner)
    {
        dirty(_owner, owner);
        _owner = owner;
    }

    /**
     * @return УГС. Свойство не может быть null.
     */
    @NotNull
    public EppRealEduGroup getEduGroup()
    {
        return _eduGroup;
    }

    /**
     * @param eduGroup УГС. Свойство не может быть null.
     */
    public void setEduGroup(EppRealEduGroup eduGroup)
    {
        dirty(_eduGroup, eduGroup);
        _eduGroup = eduGroup;
    }

    /**
     * @return Часы. Свойство не может быть null.
     */
    @NotNull
    public long getHours()
    {
        return _hours;
    }

    /**
     * @param hours Часы. Свойство не может быть null.
     */
    public void setHours(long hours)
    {
        dirty(_hours, hours);
        _hours = hours;
    }

    /**
     * @return Студенты. Свойство не может быть null.
     */
    @NotNull
    public int getStudents()
    {
        return _students;
    }

    /**
     * @param students Студенты. Свойство не может быть null.
     */
    public void setStudents(int students)
    {
        dirty(_students, students);
        _students = students;
    }

    /**
     * @return Название планируемой группы. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getEduGroupTitle()
    {
        return _eduGroupTitle;
    }

    /**
     * @param eduGroupTitle Название планируемой группы. Свойство не может быть null.
     */
    public void setEduGroupTitle(String eduGroupTitle)
    {
        dirty(_eduGroupTitle, eduGroupTitle);
        _eduGroupTitle = eduGroupTitle;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof LoadEduGroupPpsDistrGen)
        {
            if (withNaturalIdProperties)
            {
                setOwner(((LoadEduGroupPpsDistr)another).getOwner());
                setEduGroup(((LoadEduGroupPpsDistr)another).getEduGroup());
            }
            setHours(((LoadEduGroupPpsDistr)another).getHours());
            setStudents(((LoadEduGroupPpsDistr)another).getStudents());
            setEduGroupTitle(((LoadEduGroupPpsDistr)another).getEduGroupTitle());
        }
    }

    public INaturalId<LoadEduGroupPpsDistrGen> getNaturalId()
    {
        return new NaturalId(getOwner(), getEduGroup());
    }

    public static class NaturalId extends NaturalIdBase<LoadEduGroupPpsDistrGen>
    {
        private static final String PROXY_NAME = "LoadEduGroupPpsDistrNaturalProxy";

        private Long _owner;
        private Long _eduGroup;

        public NaturalId()
        {}

        public NaturalId(LoadRecordOwner owner, EppRealEduGroup eduGroup)
        {
            _owner = ((IEntity) owner).getId();
            _eduGroup = ((IEntity) eduGroup).getId();
        }

        public Long getOwner()
        {
            return _owner;
        }

        public void setOwner(Long owner)
        {
            _owner = owner;
        }

        public Long getEduGroup()
        {
            return _eduGroup;
        }

        public void setEduGroup(Long eduGroup)
        {
            _eduGroup = eduGroup;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof LoadEduGroupPpsDistrGen.NaturalId) ) return false;

            LoadEduGroupPpsDistrGen.NaturalId that = (NaturalId) o;

            if( !equals(getOwner(), that.getOwner()) ) return false;
            if( !equals(getEduGroup(), that.getEduGroup()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getOwner());
            result = hashCode(result, getEduGroup());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getOwner());
            sb.append("/");
            sb.append(getEduGroup());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends LoadEduGroupPpsDistrGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) LoadEduGroupPpsDistr.class;
        }

        public T newInstance()
        {
            return (T) new LoadEduGroupPpsDistr();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "owner":
                    return obj.getOwner();
                case "eduGroup":
                    return obj.getEduGroup();
                case "hours":
                    return obj.getHours();
                case "students":
                    return obj.getStudents();
                case "eduGroupTitle":
                    return obj.getEduGroupTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "owner":
                    obj.setOwner((LoadRecordOwner) value);
                    return;
                case "eduGroup":
                    obj.setEduGroup((EppRealEduGroup) value);
                    return;
                case "hours":
                    obj.setHours((Long) value);
                    return;
                case "students":
                    obj.setStudents((Integer) value);
                    return;
                case "eduGroupTitle":
                    obj.setEduGroupTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "owner":
                        return true;
                case "eduGroup":
                        return true;
                case "hours":
                        return true;
                case "students":
                        return true;
                case "eduGroupTitle":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "owner":
                    return true;
                case "eduGroup":
                    return true;
                case "hours":
                    return true;
                case "students":
                    return true;
                case "eduGroupTitle":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "owner":
                    return LoadRecordOwner.class;
                case "eduGroup":
                    return EppRealEduGroup.class;
                case "hours":
                    return Long.class;
                case "students":
                    return Integer.class;
                case "eduGroupTitle":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<LoadEduGroupPpsDistr> _dslPath = new Path<LoadEduGroupPpsDistr>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "LoadEduGroupPpsDistr");
    }
            

    /**
     * @return ППС (нагрузка). Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadEduGroupPpsDistr#getOwner()
     */
    public static LoadRecordOwner.Path<LoadRecordOwner> owner()
    {
        return _dslPath.owner();
    }

    /**
     * @return УГС. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadEduGroupPpsDistr#getEduGroup()
     */
    public static EppRealEduGroup.Path<EppRealEduGroup> eduGroup()
    {
        return _dslPath.eduGroup();
    }

    /**
     * @return Часы. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadEduGroupPpsDistr#getHours()
     */
    public static PropertyPath<Long> hours()
    {
        return _dslPath.hours();
    }

    /**
     * @return Студенты. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadEduGroupPpsDistr#getStudents()
     */
    public static PropertyPath<Integer> students()
    {
        return _dslPath.students();
    }

    /**
     * @return Название планируемой группы. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadEduGroupPpsDistr#getEduGroupTitle()
     */
    public static PropertyPath<String> eduGroupTitle()
    {
        return _dslPath.eduGroupTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniload_demo.entity.LoadEduGroupPpsDistr#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends LoadEduGroupPpsDistr> extends EntityPath<E>
    {
        private LoadRecordOwner.Path<LoadRecordOwner> _owner;
        private EppRealEduGroup.Path<EppRealEduGroup> _eduGroup;
        private PropertyPath<Long> _hours;
        private PropertyPath<Integer> _students;
        private PropertyPath<String> _eduGroupTitle;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ППС (нагрузка). Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadEduGroupPpsDistr#getOwner()
     */
        public LoadRecordOwner.Path<LoadRecordOwner> owner()
        {
            if(_owner == null )
                _owner = new LoadRecordOwner.Path<LoadRecordOwner>(L_OWNER, this);
            return _owner;
        }

    /**
     * @return УГС. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadEduGroupPpsDistr#getEduGroup()
     */
        public EppRealEduGroup.Path<EppRealEduGroup> eduGroup()
        {
            if(_eduGroup == null )
                _eduGroup = new EppRealEduGroup.Path<EppRealEduGroup>(L_EDU_GROUP, this);
            return _eduGroup;
        }

    /**
     * @return Часы. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadEduGroupPpsDistr#getHours()
     */
        public PropertyPath<Long> hours()
        {
            if(_hours == null )
                _hours = new PropertyPath<Long>(LoadEduGroupPpsDistrGen.P_HOURS, this);
            return _hours;
        }

    /**
     * @return Студенты. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadEduGroupPpsDistr#getStudents()
     */
        public PropertyPath<Integer> students()
        {
            if(_students == null )
                _students = new PropertyPath<Integer>(LoadEduGroupPpsDistrGen.P_STUDENTS, this);
            return _students;
        }

    /**
     * @return Название планируемой группы. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadEduGroupPpsDistr#getEduGroupTitle()
     */
        public PropertyPath<String> eduGroupTitle()
        {
            if(_eduGroupTitle == null )
                _eduGroupTitle = new PropertyPath<String>(LoadEduGroupPpsDistrGen.P_EDU_GROUP_TITLE, this);
            return _eduGroupTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniload_demo.entity.LoadEduGroupPpsDistr#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(LoadEduGroupPpsDistrGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return LoadEduGroupPpsDistr.class;
        }

        public String getEntityName()
        {
            return "loadEduGroupPpsDistr";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTitle();
}
