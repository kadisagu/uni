/*$Id$*/
package ru.tandemservice.uniload_demo.base.bo.Load.ui.RateStep;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniload_demo.base.bo.Load.vo.LoadRateStepVO;

/**
 * @author DMITRY KNYAZEV
 * @since 17.12.2014
 */
@SuppressWarnings({"UnusedDeclaration"})
public class LoadRateStepUI extends UIPresenter
{
	private Long _rateStep;
	private Long _maxRate;


	@Override
	public void onComponentRefresh()
	{
		LoadRateStepVO loadRateStepVO = LoadRateStepVO.getInstance();
		_rateStep = loadRateStepVO.getRateStep();
		_maxRate = loadRateStepVO.getMaxRate();
	}

	//handlers
	public void onClickApply()
	{
		ErrorCollector errorCollector = ContextLocal.getErrorCollector();
		if(_rateStep > _maxRate)
		{
			errorCollector.add("Шаг ставки не может превышать максимальную ставку", "rateStep");
		}
		if (errorCollector.hasErrors()) return;
		LoadRateStepVO.saveLoadRateStep(_rateStep, _maxRate);
	}

	public void onClickSave()
	{
		onClickApply();
		if(!ContextLocal.getErrorCollector().hasErrors())deactivate();
	}

	//getters/setters
	public Double getMaxRate()
	{
		return UniEppUtils.wrap(_maxRate);
	}

	public void setMaxRate(Double maxRate)
	{
		_maxRate = UniEppUtils.unwrap(maxRate);
	}

	public Double getRateStep()
	{
		return UniEppUtils.wrap(_rateStep);
	}

	public void setRateStep(Double rateStep)
	{
		_rateStep = UniEppUtils.unwrap(rateStep);
	}


}
