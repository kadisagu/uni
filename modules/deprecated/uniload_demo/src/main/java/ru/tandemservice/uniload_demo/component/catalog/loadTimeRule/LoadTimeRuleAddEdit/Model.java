/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.component.catalog.loadTimeRule.LoadTimeRuleAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import org.tandemframework.shared.commonbase.utils.CommonYesNoUIObject;
import ru.tandemservice.uniload_demo.entity.ImEduGroupLoadBase;
import ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule;

import java.util.Arrays;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 23.12.2013
 */
public class Model extends DefaultCatalogAddEditModel<LoadTimeRule>
{

	private CommonYesNoUIObject _permission;
	List<CommonYesNoUIObject> _permissionModel;

	public CommonYesNoUIObject getPermission()
	{
		return _permission;
	}

	public void setPermission(CommonYesNoUIObject permission)
	{
		_permission = permission;
	}

	public void setPermissionModel(List<CommonYesNoUIObject> permissionModel)
	{
		_permissionModel = permissionModel;
	}

	public List<CommonYesNoUIObject> getPermissionModel()
	{
		return _permissionModel;
	}

	private List<ImEduGroupLoadBase> _baseOptions = Arrays.asList(ImEduGroupLoadBase.CONSTANT, ImEduGroupLoadBase.STUDENTS, ImEduGroupLoadBase.HOURS);

	public List<ImEduGroupLoadBase> getBaseOptions()
	{
		return _baseOptions;
	}


}