package ru.tandemservice.uniload_demo.entity.catalog;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniload_demo.entity.catalog.gen.*;

/**
 * Нормы времени (демо)
 */
public class LoadTimeRule extends LoadTimeRuleGen
{
    @EntityDSLSupport
    @Override
    public Double getCoefficientAsDouble()
    {
        return UniEppUtils.wrap(this.getCoefficient());
    }

    public void setCoefficientAsDouble(Double coefficientAsDouble)
    {
        this.setCoefficient(UniEppUtils.unwrap(coefficientAsDouble));
    }
}