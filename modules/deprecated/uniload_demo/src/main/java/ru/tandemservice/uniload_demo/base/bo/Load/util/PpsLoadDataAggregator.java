/*$Id$*/
package ru.tandemservice.uniload_demo.base.bo.Load.util;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uni.entity.employee.pps.EmployeePost4PpsEntry;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniload_demo.base.bo.Load.LoadManager;
import ru.tandemservice.uniload_demo.base.bo.Load.logic.ILoadDAO;
import ru.tandemservice.uniload_demo.base.bo.Load.logic.PpsLoadWrapper;
import ru.tandemservice.uniload_demo.entity.LoadRecordOwner;
import ru.tandemservice.uniload_demo.entity.LoadSize;
import ru.tandemservice.unisnpps.entity.pps.Timeworker4PpsEntry;
import ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author DMITRY KNYAZEV
 * @since 16.09.2014
 */
public class PpsLoadDataAggregator
{
	final private ILoadDAO dao = LoadManager.instance().dao();

	private final List<PpsLoadWrapper> _ppsWrappers = new ArrayList<>();
	//Education Load
	private Long _totalEduLoadSize = 0L;
	private Long _totalOtherEduLoadSize = 0L;
	private Long _totalOtherLoadSize = 0L;
	private Long _totalHourlyLoadSize = 0L;
	private Long _totalLoadSize = 0L;
	//stuff rate
	private Double _staffEmployeeRate = 0D;
	private Double _freelanceEmployeeRate = 0D;
	private Double _secondJobEmployeeRate = 0D;
	private Double _mainJobEmployeeRate = 0D;
	private Double _totalVacancyEmployeeRate = 0D;

	public void refresh(@NotNull Long pupnagId, @NotNull Collection<Long> ppsLoadIdList)
	{
		clearAll();
		Collection<LoadRecordOwner> ppsList = dao.getList(LoadRecordOwner.class, ppsLoadIdList);
		Map<Long, PpsLoadDataWrapper> dataMap = dao.getPpsLoadDataWrapperMap(ppsLoadIdList);
		Map<Long, Boolean> mainJobInfo = dao.getPpsSecondJobInfoMap(ppsLoadIdList);

		List<Long> ppsEntryIds = ppsList.stream()
				.filter(item -> null != item.getRealPps())
				.map(item -> item.getRealPps().getPps().getId())
				.collect(Collectors.toList());

		Map<Long, List<EmployeePost>> ppsEntry2EmployeePostMap = Maps.newHashMap();
		Map<Long, List<UnisnppsTimeworker>> ppsEntry2TimeWorkerMap = Maps.newHashMap();

		dao.getList(EmployeePost4PpsEntry.class, EmployeePost4PpsEntry.ppsEntry().id(), ppsEntryIds)
				.forEach(item -> SafeMap.safeGet(ppsEntry2EmployeePostMap, item.getPpsEntry().getId(), ArrayList.class).add(item.getEmployeePost()));

		dao.getList(Timeworker4PpsEntry.class, Timeworker4PpsEntry.ppsEntry().id(), ppsEntryIds)
				.forEach(item -> SafeMap.safeGet(ppsEntry2TimeWorkerMap, item.getPpsEntry().getId(), ArrayList.class).add(item.getTimeworker()));

		for (LoadRecordOwner pps : ppsList)
		{
			PpsLoadDataWrapper data = dataMap.get(pps.getId());
			Long eduLoadSize = data.getEduLoad();
			Long otherLoadSize = data.getTotalOtherLoad();
			Long otherLoadEduSize = data.getOtherEduLoad();
			Long totalLoadSize = data.getTotalLoad();
			Long totalEduLoadSize = data.getTotalEduLoad();

			Long postId = data.getPostId();
			LoadSize loadSize = dao.getLoadSize(pupnagId, postId);

			Long loadMaxSizeValue = loadSize == null ? null : loadSize.getMaxSize();
			Long loadMinSizeValue = loadSize == null ? 0L : loadSize.getMinSize() == null ? 0L : loadSize.getMinSize();
			Long loadTotalSizeValue = loadSize == null ? null : loadSize.getTotalSize();

			boolean isFreelance = false;
			boolean isSecondJob = false;
			final Double rate = pps.getRate();
			List<EmployeePost> employeePosts = Lists.newArrayList();
			List<UnisnppsTimeworker> timeWorkers = Lists.newArrayList();

			if (pps.isReal())
			{
				if(mainJobInfo.containsKey(pps.getRealPps().getPps().getId()))
					isSecondJob = mainJobInfo.get(pps.getRealPps().getPps().getId());
                isFreelance = pps.getRealPps().isFreelance();

				employeePosts = ppsEntry2EmployeePostMap.getOrDefault(pps.getRealPps().getPps().getId(), Lists.newArrayList());
				timeWorkers = ppsEntry2TimeWorkerMap.getOrDefault(pps.getRealPps().getPps().getId(), Lists.newArrayList());

				if (isFreelance)
					_freelanceEmployeeRate += rate;
				else
					_staffEmployeeRate += rate;

				if (isSecondJob)
					_secondJobEmployeeRate += rate;
				else
					_mainJobEmployeeRate += rate;
			}
			else
			{
				_totalVacancyEmployeeRate += rate;
			}

			Boolean exceededTotalEduLoad = existInWindow(loadMinSizeValue, loadMaxSizeValue, totalEduLoadSize, rate);
			boolean exceededTotalLoad = loadTotalSizeValue != null && totalLoadSize * rate > loadTotalSizeValue;

			_ppsWrappers.add(new PpsLoadWrapper(pps, data, rate, isSecondJob, isFreelance, exceededTotalLoad, exceededTotalEduLoad, employeePosts, timeWorkers));

			_totalEduLoadSize += eduLoadSize;
			_totalOtherLoadSize += otherLoadSize;
			_totalLoadSize += totalLoadSize;
			_totalOtherEduLoadSize += otherLoadEduSize;
			if (pps.getHourlyLoad())
				_totalHourlyLoadSize += totalLoadSize;
		}
	}

	// Utils
	private Boolean existInWindow(@Nullable Long minValue, @Nullable Long maxValue, Long value, Double rate)
	{
		if (minValue != null && (value * rate) < minValue)
			return false;
		if (maxValue != null && (value * rate) > maxValue)
			return true;
		return null;
	}

	private void clearAll()
	{
		_ppsWrappers.clear();
		//Education Load
		_totalEduLoadSize = _totalOtherEduLoadSize = _totalOtherLoadSize = _totalHourlyLoadSize = _totalLoadSize = 0L;
		//stuff rate
		_staffEmployeeRate = _freelanceEmployeeRate = _totalVacancyEmployeeRate = _secondJobEmployeeRate = _mainJobEmployeeRate = 0D;
	}

	//Getters/setters
	public List<PpsLoadWrapper> getPpsWrappers()
	{
		return _ppsWrappers;
	}

	/**
	 * Учебная нагрузка
	 */
	public Double getTotalEduLoadSize()
	{
		return UniEppUtils.wrap(_totalEduLoadSize);
	}

	/**
	 * Другие виды нагрузки с признаком "Учитывать в учебной нагрузке"
	 */
	public Double getTotalOtherEduLoadSize()
	{
		return UniEppUtils.wrap(_totalOtherEduLoadSize);
	}

	/**
	 * Другие виды нагрузки
	 */
	public Double getTotalOtherLoadSize()
	{
		return UniEppUtils.wrap(_totalOtherLoadSize);
	}

	/**
	 * Почасовая нагрузка
	 */
	public Double getTotalHourlyLoadSize()
	{
		return UniEppUtils.wrap(_totalHourlyLoadSize);
	}

	/**
	 * Всего нагрузки
	 */
	public Double getTotalLoadSize()
	{
		return UniEppUtils.wrap(_totalLoadSize);
	}

	/**
	 * Учебной нагрузки с учетом других видов нагрузки
	 */
	public Double getTotalEduLoadIncludeOtherLoad()
	{
		return getTotalEduLoadSize() + getTotalOtherEduLoadSize();
	}

	/**
	 * Сумма ставок штатных работников (только для реальных персон)
	 */
	public Double getStaffEmployeeRate()
	{
		return _staffEmployeeRate;
	}

	/**
	 * Сумма ставок вне штатных работников (только для реальных персон)
	 */
	public Double getFreelanceEmployeeRate()
	{
		return _freelanceEmployeeRate;
	}

	/**
	 * Сумма всех занятых ставок
	 */
	public Double getTotalTakeEmployeeRate()
	{
		return getFreelanceEmployeeRate() + getStaffEmployeeRate();
	}

	/**
	 * Сумма всех вакантных ставок
	 */
	public Double getTotalVacancyEmployeeRate()
	{
		return _totalVacancyEmployeeRate;
	}

	/**
	 * Сумма всех ставок (занятых и вакантынх)
	 */
	public Double getTotalEmployeeRate()
	{
		return getTotalTakeEmployeeRate() + getTotalVacancyEmployeeRate();
	}

	public Double getStaffEmployeeRateIncludeVacancyRate()
	{
		return getStaffEmployeeRate() + getTotalVacancyEmployeeRate();
	}

	/**
	 * Сумма ставок работников по совместительству
	 */
	public Double getSecondJobEmployeeRate()
	{
		return _secondJobEmployeeRate;
	}

	/**
	 * Сумма ставок работников по основному месту работы
	 */
	public Double getMainJobEmployeeRate()
	{
		return _mainJobEmployeeRate;
	}

	/**
	 * Средняя нагрузка
	 */
	public Double getAverageLoad()
	{
		if (getTotalLoadSize().equals(0D)) return 0D;
		return getTotalLoadSize() / getTotalEmployeeRate();
	}
}
