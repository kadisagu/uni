/*$Id$*/
package ru.tandemservice.uniload_demo.base.bo.Load.vo;

import ru.tandemservice.uniepp.UniEppUtils;

/**
 * @author DMITRY KNYAZEV
 * @since 22.09.2014
 */
public class LoadSizeValueObject
{
	private Long minSize;
	private Long maxSize;
	private Long totalSize;

	public LoadSizeValueObject()
	{
		this(null, null, null);
	}

	public LoadSizeValueObject(Long minSize, Long maxSize, Long totalSize)
	{
		this.minSize = minSize;
		this.maxSize = maxSize;
		this.totalSize = totalSize;
	}

	public Long getMinSize()
	{
		return minSize;
	}

	public void setMinSize(Long minSize)
	{
		this.minSize = minSize;
	}

	public Double getMinSizeAsDouble()
	{
		return UniEppUtils.wrap(minSize);
	}

	public void setMinSize(Double minSize)
	{
		this.minSize = UniEppUtils.unwrap(minSize);
	}

	public Long getMaxSize()
	{
		return maxSize;
	}

	public void setMaxSize(Long maxSize)
	{
		this.maxSize = maxSize;
	}

	public Double getMaxSizeAsDouble()
	{
		return UniEppUtils.wrap(maxSize);
	}

	public void setMaxSize(Double maxSize)
	{
		this.maxSize = UniEppUtils.unwrap(maxSize);
	}

	public Long getTotalSize()
	{
		return totalSize;
	}

	public void setTotalSize(Long totalSize)
	{
		this.totalSize = totalSize;
	}

	public Double getTotalSizeAsDouble()
	{
		return UniEppUtils.wrap(totalSize);
	}

	public void setTotalSize(Double totalSize)
	{
		this.totalSize = UniEppUtils.unwrap(totalSize);
	}

	@Override
	public boolean equals(Object obj)
	{
		if(!(obj instanceof LoadSizeValueObject)) return false;
		LoadSizeValueObject other = (LoadSizeValueObject) obj;
		return this.getMinSize().equals(other.getMinSize()) && this.getMaxSize().equals(other.getMaxSize()) && this.getTotalSize().equals(other.getTotalSize());
	}

	@Override
	public int hashCode()
	{
		return getMinSize().hashCode() & getMaxSize().hashCode() & getTotalSize().hashCode();
	}
}
