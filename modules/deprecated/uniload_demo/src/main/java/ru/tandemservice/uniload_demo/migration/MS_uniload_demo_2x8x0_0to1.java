package ru.tandemservice.uniload_demo.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniload_demo_2x8x0_0to1 extends IndependentMigrationScript
{

    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.17"),
                        new ScriptDependency("org.tandemframework.shared", "1.8.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность loadRecordOwner

        // создано свойство realPps
        {
            // создать колонку
            tool.createColumn("loadrecordowner_t", new DBColumn("realpps_id", DBType.LONG));
        }
        // создаем недостающие loadOwnerAdditional
        {
            final ResultSet resultSet = tool.getStatement().executeQuery("SELECT lro.PPS_ID FROM loadrecordowner_t lro LEFT JOIN LOADOWNERADDITIONAL_T loa ON lro.PPS_ID = loa.PPS_ID WHERE lro.PPS_ID IS NOT NULL AND loa.PPS_ID IS NULL;");

            final PreparedStatement insert = tool.prepareStatement("INSERT INTO LOADOWNERADDITIONAL_T (ID, DISCRIMINATOR, PPS_ID, FREELANCE_P, RATE_P) VALUES (?,?,?,?,?);");

            short entityCode = tool.entityCodes().get("loadOwnerAdditional");
            while (resultSet.next())
            {
                final long id = EntityIDGenerator.generateNewId(entityCode);
                final long ppsId = resultSet.getLong(1);
                final boolean freelance = false;
                final int rate = 0;

                insert.setLong(1, id);
                insert.setShort(2, entityCode);
                insert.setLong(3, ppsId);
                insert.setBoolean(4, freelance);
                insert.setInt(5, rate);
                insert.execute();
            }

            {//присваиваем новые значения для свойства realPps
                final boolean isExecute = tool.getStatement().execute("UPDATE loadrecordowner_t SET realpps_id = (SELECT loa.ID FROM LOADOWNERADDITIONAL_T AS loa WHERE loadrecordowner_t.pps_id = loa.pps_id);");
            }
        }
    }
}