/*$Id$*/
package ru.tandemservice.uniload_demo.base.bo.Load.util.print;

import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.uniload_demo.base.bo.Load.LoadManager;
import ru.tandemservice.uniload_demo.base.bo.Load.logic.ILoadDAO;
import ru.tandemservice.uniload_demo.entity.ILoadImEduGroup;
import ru.tandemservice.uniload_demo.entity.LoadRecordOwner;

/**
 * @author DMITRY KNYAZEV
 * @since 12.12.2014
 */
public final class ConsolidatedDocumentPpsRowWrapper
{
	public static final String EMPTY_STRING = "-";
	private final LoadRecordOwner _owner;
	private final ILoadDAO _dao;
	private Double _hours = 0D;

	public ConsolidatedDocumentPpsRowWrapper(LoadRecordOwner owner)
	{
		_owner = owner;
		_dao = LoadManager.instance().dao();
	}

	public void add(ILoadImEduGroup imEduGroup)
	{
		Double load = getDao().getTotalEduLoad4EduGroup(imEduGroup);
		_hours += load;
	}

	//Ф.И.О. преподавателя
	public String getPpsFio()
	{
		return _owner.getFullFio();
	}

	//Тип ученой степени
	public String getScienceDegreeType()
	{
		return _owner.getScienceDegreeType(EMPTY_STRING);
	}

	//Тип ученого звания.
	public String getScienceStatusType()
	{
		return _owner.getScienceStatusType(EMPTY_STRING);
	}

	//Должность
	public String getPost()
	{
		return _owner.getPostTitle(EMPTY_STRING);
	}

	//ставка
	public String getRate()
	{
		return double2String(getRateValue());
	}

	//ставка (число)
	public Double getRateValue()
	{
		if (_owner.isReal())
			return _owner.getRealPps().getRateAsDouble();
		else
			return _owner.getFakePps().getStaffRateDouble();
	}

	//нагрузка в часах
	public String getHours()
	{
		return double2String(getHoursValue());
	}

	//нагрузка (число)
	public Double getHoursValue()
	{
		return _hours;
	}

	/**
	 * признак является ли преподаватель штатным или нет
	 *
	 * @return {@code true} - нештатный, {@code false} - штатный
	 */
	public boolean isFreelance()
	{
		return _owner.isReal() && _owner.getRealPps().isFreelance();
	}

	private ILoadDAO getDao()
	{
		return _dao;
	}

	private String double2String(double val)
	{
		return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(val);
	}
}
