package ru.tandemservice.uniload_demo.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniload_demo_2x6x4_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.4")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность nonEduLoad

		// создано обязательное свойство considerInEduLoad
		{
			// создать колонку
			tool.createColumn("noneduload_t", new DBColumn("considerineduload_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			java.lang.Boolean defaultConsiderInEduLoad = false;
			tool.executeUpdate("update noneduload_t set considerineduload_p=? where considerineduload_p is null", defaultConsiderInEduLoad);

			// сделать колонку NOT NULL
			tool.setColumnNullable("noneduload_t", "considerineduload_p", false);

		}

	    // Сущность nonEduLoad переименована в otherLoad
	    {
		    tool.renameTable("noneduload_t", "otherload_t");
		    tool.entityCodes().rename("nonEduLoad", "otherLoad");
	    }


    }
}