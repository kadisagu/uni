/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.base.bo.Load.logic;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLSelectableQuery;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.pps.gen.IEppPPSCollectionOwnerGen;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModuleALoad;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadType;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;
import ru.tandemservice.uniload_demo.base.bo.Load.ui.WorkPlanRowTab.LoadWorkPlanRowTab;
import ru.tandemservice.uniload_demo.entity.LoadEduGroupPpsDistr;
import ru.tandemservice.uniload_demo.entity.LoadFirstCourseImEduGroup;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 20.12.2013
 */
public class WorkPlanRowDSHandler extends DefaultSearchDataSourceHandler
{
    public WorkPlanRowDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long orgUnitId = context.get(LoadWorkPlanRowTab.ORG_UNIT_ID);
        EppYearEducationProcess year = context.get(LoadWorkPlanRowTab.PUPNAG);
        DevelopForm developForm = context.get(EducationCatalogsManager.DS_DEVELOP_FORM);
        DevelopCondition developCondition = context.get(LoadWorkPlanRowTab.DEVELOP_CONDITION);
        DevelopTech developTech = context.get(LoadWorkPlanRowTab.DEVELOP_TECH);
        DevelopGrid developGrid = context.get(LoadWorkPlanRowTab.DEVELOP_GRID);
        EppYearPart yearPart = context.get(LoadWorkPlanRowTab.EDU_YEAR_PART);

        IDQLSelectableQuery partIdQuery = new DQLSelectBuilder()
                .fromEntity(EppRegistryElementPart.class, "rp")
                .column(property("rp", EppRegistryElementPart.id()))
                .where(eq(property("rp", EppRegistryElementPart.registryElement().owner().id()), value(orgUnitId)))
                .buildQuery();

        DQLSelectBuilder rowIdBuilder = new DQLSelectBuilder()
                .fromEntity(EppWorkPlanRegistryElementRow.class, "rer")
                .joinEntity("rer", DQLJoinType.inner, EppWorkPlanBase.class, "wpb", eq(property("wpb", EppWorkPlanBase.id()), property("rer", EppWorkPlanRegistryElementRow.workPlan().id())))
                .column(property("rer", EppWorkPlanRegistryElementRow.id()))
                .where(in(property("rer", EppWorkPlanRegistryElementRow.registryElementPart().id()), partIdQuery));

        IDQLSelectableQuery rowIdQuery = updateWorkPlanBuilder(rowIdBuilder, "wpb", year, developForm, developCondition, developTech, developGrid, yearPart).buildQuery();

        DQLSelectBuilder actionBuilder = new DQLSelectBuilder()
                .fromEntity(EppRegistryElementPartFControlAction.class, "rca")
                .column(property("rca", EppRegistryElementPartFControlAction.part().id()))
                .column(property("rca", EppRegistryElementPartFControlAction.controlAction().code()))
                .where(in(property("rca", EppRegistryElementPartFControlAction.part().id()), partIdQuery));

        Map<Long, Set<String>> partActionMap = SafeMap.get(HashSet.class);
        for (Object[] row: DataAccessServices.dao().<Object[]>getList(actionBuilder))
        {
            Long rowId= (Long) row[0];
            partActionMap.get(rowId).add((String) row[1]);
        }

        DQLSelectBuilder loadBuilder = new DQLSelectBuilder()
                .fromEntity(EppRegistryModuleALoad.class, "ml")
                .joinEntity("ml", DQLJoinType.inner, EppRegistryElementPartModule.class, "pm", eq(property("pm", EppRegistryElementPartModule.module()), property("ml", EppRegistryModuleALoad.module())))
                .column(property("pm", EppRegistryElementPartModule.part().id()))
                .column(property("ml", EppRegistryModuleALoad.loadType().code()))
                .column(property("ml", EppRegistryModuleALoad.load()))
                .where(in(property("pm", EppRegistryElementPartModule.part().id()), partIdQuery));

        Map<Long, Map<String, Long>> partLoadMap = SafeMap.get(HashMap.class);
        for (Object[] row: DataAccessServices.dao().<Object[]>getList(loadBuilder))
        {
            Long rowId= (Long) row[0];
            Map<String, Long> loadMap = partLoadMap.get(rowId);
            String loadCode = (String) row[1];
            loadMap.put(loadCode, ((Long) row[2]) + (loadMap.containsKey(loadCode) ? loadMap.get(loadCode) : 0L));
        }


        IDQLSelectableQuery eduGroupQuery = new DQLSelectBuilder()
                .fromEntity(EppRealEduGroup.class, "reg")
                .column(property("reg", EppRealEduGroup.id()))
                .where(in(property("reg", EppRealEduGroup.workPlanRow().id()), rowIdQuery))
                .buildQuery();

        DQLSelectBuilder distrLoadBuilder = new DQLSelectBuilder()
                .fromEntity(LoadEduGroupPpsDistr.class, "d")
                .joinPath(DQLJoinType.inner, LoadEduGroupPpsDistr.eduGroup().fromAlias("d"), "co")
                .joinEntity("co", DQLJoinType.inner, EppRealEduGroup4LoadType.class, "lt", eq(property("lt", EppRealEduGroup4LoadType.id()), property("co", IEppPPSCollectionOwnerGen.id())))
                .joinEntity("lt", DQLJoinType.inner, EppALoadType.class, "alt", eq(property("lt", EppRealEduGroup4LoadType.type()), property("alt", EppALoadType.eppGroupType())))

                .column(property("lt", EppRealEduGroup4LoadType.activityPart().id()))
                .column(property("alt", EppALoadType.code()))
                .column(property("d", LoadEduGroupPpsDistr.hours()))

                .where(in(property("lt", EppRealEduGroup4LoadType.id()), eduGroupQuery));

        Map<Long, Map<String, Long>> distrPartLoadMap = SafeMap.get(HashMap.class);
        for (Object[] row: DataAccessServices.dao().<Object[]>getList(distrLoadBuilder))
        {
            Long rowId= (Long) row[0];
            Map<String, Long> distrLoadMap = distrPartLoadMap.get(rowId);
            String loadCode = (String) row[1];
            distrLoadMap.put(loadCode, ((Long) row[2]) + (distrLoadMap.containsKey(loadCode) ? distrLoadMap.get(loadCode) : 0L));
        }

        DQLSelectBuilder firstCourseLoadBuilder = new DQLSelectBuilder()
                .fromEntity(LoadFirstCourseImEduGroup.class, "e")
                .joinEntity("e", DQLJoinType.inner, EppALoadType.class, "l", eq(property("l", EppALoadType.eppGroupType()), property("e", LoadFirstCourseImEduGroup.type())))
                .column(property("e", LoadFirstCourseImEduGroup.activityPart().id()))
                .column(property("l", EppALoadType.code()))
                .column(property("e", LoadFirstCourseImEduGroup.hours()))
                .where(in(property("e", LoadFirstCourseImEduGroup.row().id()), rowIdQuery));

        Map<Long, Map<String, Long>> firstCourseLoadMap = SafeMap.get(HashMap.class);
        for (Object[] row: DataAccessServices.dao().<Object[]>getList(firstCourseLoadBuilder))
        {
            Long rowId= (Long) row[0];
            Map<String, Long> fcLoadMap = firstCourseLoadMap.get(rowId);
            String loadCode = (String) row[1];
            fcLoadMap.put(loadCode, ((Long) row[2]) + (fcLoadMap.containsKey(loadCode) ? fcLoadMap.get(loadCode) : 0L));
        }

	    //Собираем все УГС принадлежащие РУП
	    DQLSelectBuilder eppRealEduGroupBuilder = new DQLSelectBuilder()
			    .column("rer")
			    .column("eg")
			    .fromEntity(EppWorkPlanRegistryElementRow.class, "rer")
			    .where(in(property("rer", EppWorkPlanRegistryElementRow.id()), rowIdQuery))
			    .joinEntity("rer", DQLJoinType.left, EppRealEduGroup.class, "eg", eq(property("eg", EppRealEduGroup.L_WORK_PLAN_ROW), property("rer.id")));
	    Multimap<Long, EppRealEduGroup4LoadType> workPlan2EduGroupMap = HashMultimap.create();
	    for (Object[] item : eppRealEduGroupBuilder.createStatement(context.getSession()).<Object[]>list())
	    {
		    if (item[1] instanceof EppRealEduGroup4LoadType)
		    {
			    EppRealEduGroup4LoadType eduGroup = (EppRealEduGroup4LoadType) item[1];
                EppWorkPlanRegistryElementRow workPlan = (EppWorkPlanRegistryElementRow) item[0];
                //to avoid ConcurrentModificationException create new collection
                Collection<EppRealEduGroup4LoadType> addedEduGroups = new ArrayList<>(workPlan2EduGroupMap.get(workPlan.getId()));
                if (addedEduGroups.isEmpty() || addedEduGroups.contains(eduGroup))
	                workPlan2EduGroupMap.put(workPlan.getId(), eduGroup);
                else
                {
	                for (EppRealEduGroup4LoadType group : addedEduGroups)
	                {
		                if (group.getType().getCode().equals(eduGroup.getType().getCode()))
			                workPlan2EduGroupMap.put(workPlan.getId(), eduGroup);
	                }
                }

            }
	    }

        List<WorkPlanRowWrapper> wrappers = new ArrayList<>();
        DQLSelectBuilder workPlanRowBuilder = new DQLSelectBuilder()
                .fromEntity(EppWorkPlanRegistryElementRow.class, "rer")
                .where(in(property("rer", EppWorkPlanRegistryElementRow.id()), rowIdQuery));


        for (EppWorkPlanRegistryElementRow row: DataAccessServices.dao().<EppWorkPlanRegistryElementRow>getList(workPlanRowBuilder))
        {
            Long partId = row.getRegistryElementPart().getId();
	        Collection<EppRealEduGroup4LoadType> eduGroupList = workPlan2EduGroupMap.get(row.getId());
            wrappers.add(new WorkPlanRowWrapper(row, partActionMap.get(partId), partLoadMap.get(partId), distrPartLoadMap.get(partId), firstCourseLoadMap.get(partId),eduGroupList));
        }

        Comparator<WorkPlanRowWrapper> comparator;
        switch (input.getEntityOrder().getColumnName())
        {
            case LoadWorkPlanRowTab.TITLE:
            {
                comparator = WorkPlanRowWrapper.COMPARATOR_TITLE;
                break;
            }
            case LoadWorkPlanRowTab.WORK_PLAN:
            {
                comparator = WorkPlanRowWrapper.COMPARATOR_WORK_PLAN;
                break;
            }
            case LoadWorkPlanRowTab.COURSE:
            {
                comparator = WorkPlanRowWrapper.COMPARATOR_COURSE;
                break;
            }
            default:
                comparator = WorkPlanRowWrapper.COMPARATOR;
        }

        Collections.sort(wrappers, comparator);
        if (input.getEntityOrder().getDirection().equals(OrderDirection.desc)) Collections.reverse(wrappers);

        return ListOutputBuilder.get(input, wrappers).pageable(true).build();
    }

    /**
     * Метод добавляет условия к запросу с базовым РУПом.
     * @param workPlanBuilder входящий запрос с базовым РУПом
     * @param workPlanBaseAlias алиас базового РУПа
     * @param year учебный год
     * @param developForm форма освоения
     * @param developCondition условие освоения
     * @param developTech технология освоения
     * @param developGrid учебная сетка
     * @return обновленный билдер
     */
    private DQLSelectBuilder updateWorkPlanBuilder(DQLSelectBuilder workPlanBuilder, String workPlanBaseAlias, EppYearEducationProcess year, DevelopForm developForm, DevelopCondition developCondition, DevelopTech developTech, DevelopGrid developGrid, EppYearPart yearPart)
    {
        return workPlanBuilder

                .joinEntity(workPlanBaseAlias, DQLJoinType.left, EppWorkPlan.class, "uwp", eq(property("uwp", EppWorkPlan.id()), property(workPlanBaseAlias, EppWorkPlanBase.id())))
                .joinEntity(workPlanBaseAlias, DQLJoinType.left, EppWorkPlanVersion.class, "uwpv", eq(property("uwpv", EppWorkPlanVersion.id()), property(workPlanBaseAlias, EppWorkPlanBase.id())))
                .joinPath(DQLJoinType.left, EppWorkPlanVersion.parent().fromAlias("uwpv"), "vp")

                .joinPath(DQLJoinType.left, EppWorkPlan.parent().fromAlias("uwp"), "wpblock")
                .joinPath(DQLJoinType.left, EppEduPlanVersionBlock.eduPlanVersion().fromAlias("wpblock"), "wpversion")
                .joinPath(DQLJoinType.left, EppEduPlanVersion.eduPlan().fromAlias("wpversion"), "wpplan")
                .joinEntity("uwp", DQLJoinType.left, DevelopGridTerm.class, "wpdgt", and(eq(property("wpdgt", DevelopGridTerm.L_TERM), property("uwp", EppWorkPlan.L_TERM)),
                                                                                         eq(property("wpdgt", DevelopGridTerm.L_DEVELOP_GRID), property("uwp", EppWorkPlan.parent().eduPlanVersion().eduPlan().developGrid()))))
                .joinPath(DQLJoinType.left, EppWorkPlan.parent().fromAlias("vp"), "vblock")
                .joinPath(DQLJoinType.left, EppEduPlanVersionBlock.eduPlanVersion().fromAlias("vblock"), "vversion")
                .joinPath(DQLJoinType.left, EppEduPlanVersion.eduPlan().fromAlias("vversion"), "vplan")
                .joinEntity("vp", DQLJoinType.left, DevelopGridTerm.class, "vdgt", and(eq(property("vdgt", DevelopGridTerm.L_TERM), property("vp", EppWorkPlan.L_TERM)),
                                                                                       eq(property("vdgt", DevelopGridTerm.L_DEVELOP_GRID), property("vp", EppWorkPlan.parent().eduPlanVersion().eduPlan().developGrid()))))
                .where(year == null ? null : eq(DQLFunctions.coalesce(property("uwp", EppWorkPlan.year()), property("vp", EppWorkPlan.year())), value(year)))
                .where(yearPart == null ? null : eq(DQLFunctions.coalesce(property("wpdgt", DevelopGridTerm.part()), property("vdgt", DevelopGridTerm.part())), value(yearPart.getPart())))
                .where(developForm == null ? null : eq(DQLFunctions.coalesce(property("wpplan", EppEduPlan.programForm()), property("vplan", EppEduPlan.programForm())), value(developForm.getProgramForm())))
                .where(developCondition == null ? null : eq(DQLFunctions.coalesce(property("wpplan", EppEduPlan.developCondition()), property("vplan", EppEduPlan.developCondition())), value(developCondition)))
                .where(developTech == null ? null : eq(DQLFunctions.coalesce(property("wpplan", EppEduPlan.programTrait()), property("vplan", EppEduPlan.programTrait())), value(developTech.getProgramTrait())))
                .where(developGrid == null ? null : eq(DQLFunctions.coalesce(property("wpplan", EppEduPlan.developGrid()), property("vplan", EppEduPlan.developGrid())), value(developGrid)));

    }
}