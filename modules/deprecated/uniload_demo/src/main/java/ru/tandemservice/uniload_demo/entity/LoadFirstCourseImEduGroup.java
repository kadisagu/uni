package ru.tandemservice.uniload_demo.entity;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniload_demo.entity.gen.*;

/**
 * Планируемая УГС первого курса (демо)
 */
public class LoadFirstCourseImEduGroup extends LoadFirstCourseImEduGroupGen
{
    @EntityDSLSupport
    @Override
    public Double getHoursAsDouble()
    {
        return UniEppUtils.wrap(this.getHours());
    }

    public void setHoursAsDouble(Double loadSize)
    {
        this.setHours(UniEppUtils.unwrap(loadSize));
    }
}