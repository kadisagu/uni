package ru.tandemservice.uniload_demo.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Типы нагрузки ППС"
 * Имя сущности : ppsLoadType
 * Файл data.xml : uniload_demo.catalog.data.xml
 */
public interface PpsLoadTypeCodes
{
    /** Константа кода (code) элемента : Учебная работа (title) */
    String UCHEBNAYA_RABOTA = "1";
    /** Константа кода (code) элемента : Учебно-методическая работа (title) */
    String UCHEBNO_METODICHESKAYA_RABOTA = "2";
    /** Константа кода (code) элемента : Организационно-методическая работа (title) */
    String ORGANIZATSIONNO_METODICHESKAYA_RABOTA = "3";
    /** Константа кода (code) элемента : Научно-исследовательская работа (title) */
    String NAUCHNO_ISSLEDOVATELSKAYA_RABOTA = "4";
    /** Константа кода (code) элемента : Внеучебная работа (title) */
    String VNEUCHEBNAYA_RABOTA = "5";

    Set<String> CODES = ImmutableSet.of(UCHEBNAYA_RABOTA, UCHEBNO_METODICHESKAYA_RABOTA, ORGANIZATSIONNO_METODICHESKAYA_RABOTA, NAUCHNO_ISSLEDOVATELSKAYA_RABOTA, VNEUCHEBNAYA_RABOTA);
}
