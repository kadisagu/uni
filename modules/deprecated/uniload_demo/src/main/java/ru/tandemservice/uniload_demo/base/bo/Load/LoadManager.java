/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.base.bo.Load;

import org.apache.commons.io.IOUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniload_demo.base.bo.Load.logic.ILoadDAO;
import ru.tandemservice.uniload_demo.base.bo.Load.logic.LoadDAO;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author Alexander Zhebko
 * @since 12.12.2013
 */
@Configuration
@SuppressWarnings("SpringFacetCodeInspection")
public class LoadManager extends BusinessObjectManager
{
	public static LoadManager instance()
	{
		return instance(LoadManager.class);
	}

	@Bean
	public ILoadDAO dao()
	{
		return new LoadDAO();
	}

	@Bean
	public byte[] getLogo() throws IOException
	{
		try (InputStream in = getClass().getResourceAsStream("/uniload_demo/img/fefu_logo.png"))
		{
			return IOUtils.toByteArray(in);
		}
	}
}