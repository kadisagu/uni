package ru.tandemservice.uniload_demo.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import ru.tandemservice.uniepp.migration.MS_uniepp_2x7x2_0to1;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniload_demo_2x7x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.2")
		};
    }

    @Override
    public ScriptDependency[] getBeforeDependencies()
    {
        return new ScriptDependency[]{ new ScriptDependency("uniepp", "2.7.2", 1) };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность loadFirstCourseImEduGroup

		// раньше свойство type ссылалось на сущность ru.tandemservice.uniepp.entity.catalog.IEppGroupType
		// теперь оно ссылается на сущность eppGroupType
        if (tool.tableExists("loadfirstcourseimedugroup_t"))
		{
            MS_uniepp_2x7x2_0to1.migrateGroupTypeLinks(tool, "loadfirstcourseimedugroup_t", "type_id");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность timeRule2EduGroup

		// раньше свойство groupType ссылалось на сущность ru.tandemservice.uniepp.entity.catalog.IEppGroupType
		// теперь оно ссылается на сущность eppGroupType
        if (tool.tableExists("timerule2edugroup_t"))
		{
            MS_uniepp_2x7x2_0to1.migrateGroupTypeLinks(tool, "timerule2edugroup_t", "grouptype_id");
		}
    }
}