/*$Id$*/
package ru.tandemservice.uniload_demo.base.bo.Load.ui.DeviationAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniload_demo.base.bo.Load.LoadManager;
import ru.tandemservice.uniload_demo.entity.ILoadImEduGroup;
import ru.tandemservice.uniload_demo.entity.LoadDeviation;

/**
 * @author DMITRY KNYAZEV
 * @since 25.12.2014
 */
@Input({@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "orgUnitId", required = true),
		@Bind(key = LoadDeviationAddEditUI.PUPNAG_ID, binding = LoadDeviationAddEditUI.PUPNAG_ID, required = true),
		@Bind(key = LoadDeviationAddEditUI.DEVIATION_ID, binding = LoadDeviationAddEditUI.DEVIATION_ID)})
@SuppressWarnings("UnusedDeclaration")
public class LoadDeviationAddEditUI extends UIPresenter
{
	public static final String PUPNAG_ID = "pupnagId";
	public static final String DEVIATION_ID = "deviationId";

	private Long _deviationId;
	private Long _pupnagId;
	private Long _orgUnitId;

	private LoadDeviation _entity;

	@Override
	public void onComponentRefresh()
	{
		if (isEditForm())
			_entity = LoadManager.instance().dao().getNotNull(getDeviationId());
		else
			_entity = new LoadDeviation();
	}

	@Override
	public void onBeforeDataSourceFetch(IUIDataSource dataSource)
	{
		if (dataSource.getName().equals(LoadDeviationAddEdit.LOAD_PPS_DS))
		{
			dataSource.put(LoadDeviationAddEdit.KEY_ORG_UNIT_ID, getOrgUnitId());
			dataSource.put(LoadDeviationAddEdit.KEY_PUPNAG_ID, getPupnagId());
		}
		if (dataSource.getName().equals(LoadDeviationAddEdit.EDU_GROUP_DS))
		{
			dataSource.put(LoadDeviationAddEdit.KEY_OWNER_ID, getEntity().getOwner() == null ? null : getEntity().getOwner().getId());
		}
	}

	//HANDLERS
	public void onClickApply()
	{
		ErrorCollector errorCollector = ContextLocal.getErrorCollector();
		Long deviation = getEntity().getDeviation();
		if (deviation < 0)
		{
			Double totalLoad = UniEppUtils.wrap(LoadManager.instance().dao().getPpsLoadDataWrapper(getEntity().getOwner().getId()).getTotalLoad());
			//noinspection ConstantConditions
			if (Math.abs(deviation) > totalLoad)
			{
				String s = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(totalLoad);
				errorCollector.add("Отрицательное отклонение по модулю не может быть больше плана (" + s + ")", "deviation");
			}
		}
		if (errorCollector.hasErrors())
			return;
		LoadManager.instance().dao().saveOrUpdate(getEntity());
		deactivate();
	}

	public void onClickChangePps()
	{
		getEntity().setEduGroup(null);
	}

	//GETTERS/SETTERS
	public Long getPupnagId()
	{
		return _pupnagId;
	}

	public void setPupnagId(Long pupnagId)
	{
		_pupnagId = pupnagId;
	}

	public Long getOrgUnitId()
	{
		return _orgUnitId;
	}

	public void setOrgUnitId(Long orgUnitId)
	{
		_orgUnitId = orgUnitId;
	}

	public Long getDeviationId()
	{
		return _deviationId;
	}

	public void setDeviationId(Long deviationId)
	{
		_deviationId = deviationId;
	}

	public boolean isEditForm()
	{
		return getDeviationId() != null;
	}

	public LoadDeviation getEntity()
	{
		return _entity;
	}

	public String getEduGroupLoad()
	{
		Double res = null;
		ILoadImEduGroup eduGroup = getEntity().getEduGroup();

		if (eduGroup != null)
			res = LoadManager.instance().dao().getTotalEduLoad4EduGroup(eduGroup);

		return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(res);
	}
}
