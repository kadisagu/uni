package ru.tandemservice.uniload_demo.entity;

import org.tandemframework.shared.employeebase.base.bo.EmployeePost.logic.IEmployeeAdditionalInfo;
import ru.tandemservice.uniload_demo.entity.gen.*;

/**
 * Дополнительные данные по сотруднику
 */
public class LoadRealPpsEntry extends LoadRealPpsEntryGen
{
	public LoadRealPpsEntry()
	{
		setFreelance(false);
		setRate(0);
	}

	public Double getRateAsDouble()
	{
		return getRate() / IEmployeeAdditionalInfo.MULTIPLIER.doubleValue();
	}

	public void setRateAsDouble(Double value)
	{
		setRate(new Double(value * IEmployeeAdditionalInfo.MULTIPLIER).intValue());
	}
}