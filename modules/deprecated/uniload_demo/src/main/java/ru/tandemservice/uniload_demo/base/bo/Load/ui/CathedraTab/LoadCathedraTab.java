/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.base.bo.Load.ui.CathedraTab;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.column.IPublisherColumnBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.IStyleResolver;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostView.EmployeePostView;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uniload_demo.base.bo.Load.logic.PpsLoadWrapper;
import ru.tandemservice.uniload_demo.base.bo.Load.logic.SpecialPupnagDSHandler;
import ru.tandemservice.uniload_demo.base.bo.Load.ui.DeviationTab.LoadDeviationTab;
import ru.tandemservice.uniload_demo.base.bo.Load.ui.PpsTab.LoadPpsTab;
import ru.tandemservice.uniload_demo.base.bo.Load.ui.PpsTab.LoadPpsTabUI;
import ru.tandemservice.uniload_demo.base.bo.Load.ui.WorkPlanRowTab.LoadWorkPlanRowTab;
import ru.tandemservice.uniload_demo.base.ext.Employee.ui.PostView.EmployeePostViewExt;
import ru.tandemservice.uniload_demo.entity.LoadRecordOwner;
import ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.core.view.formatter.DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS;

/**
 * @author Alexander Zhebko
 * @since 20.12.2013
 */
@Configuration
@SuppressWarnings("SpringFacetCodeInspection")
public class LoadCathedraTab extends BusinessComponentManager
{
	public static final String PPS_DS = "ppsDS";
	public static final String PUPNAG_DS = "pupnagDS";

	public static final String LOAD_TAB_PANEL = "loadTabPanel";
	public static final String PPS_TAB = "ppsTab";
	public static final String WORK_PLAN_ROWS_TAB = "workPLanRowsTab";
	public static final String DEVIATION_TAB = "deviationTab";

	public static final String CURRENT_PUPNAG = "currentPupnag";
	public static final String PPS_WRAPPERS = "ppsWrappers";

	public static final String PRINT_COLUMN_NAME = "print";

	private static final String WIDTH = "7%";

	@Override
	@Bean
	public PresenterExtPoint presenterExtPoint()
	{
		return presenterExtPointBuilder()
				.addDataSource(selectDS(PUPNAG_DS, pupnagDSHandler()))
				.addDataSource(searchListDS(PPS_DS, ppsCL(), ppsDSHandler()))
				.create();
	}

	@Bean
	public TabPanelExtPoint orgUnitLoadTabPanelExtPoint()
	{
		return tabPanelExtPointBuilder(LOAD_TAB_PANEL)
				.addTab(htmlTab(PPS_TAB, LoadCathedraTab.class.getPackage() + ".Pps"))
				.addTab(componentTab(WORK_PLAN_ROWS_TAB, LoadWorkPlanRowTab.class).permissionKey("ui:secModel.view_workPlaRowsLoad").parameters("mvel:['publisherId':presenter.orgUnitHolder.id]"))
				.addTab(componentTab(DEVIATION_TAB, LoadDeviationTab.class).permissionKey("ui:secModel.view_deviationLoadTab").parameters("mvel:['publisherId':presenter.orgUnitHolder.id]"))
				.create();
	}

	@Bean
	public ColumnListExtPoint ppsCL()
	{
		IPublisherColumnBuilder ppsColumn = publisherColumn("title", PpsLoadWrapper.TITLE).publisherLinkResolver(new IPublisherLinkResolver()
		{
			@Override
			@SuppressWarnings("unchecked")
			public Object getParameters(IEntity entity)
			{
				ViewWrapper<LoadRecordOwner> loadWrapper = (ViewWrapper<LoadRecordOwner>) entity;
				List<EmployeePost> employeePosts = (List<EmployeePost>) loadWrapper.getViewProperty(PpsLoadWrapper.EMPLOYEE_POSTS);
				List<UnisnppsTimeworker> timeworkers = (List<UnisnppsTimeworker>) loadWrapper.getViewProperty(PpsLoadWrapper.TIMEWORKERS);
				LoadRecordOwner recordOwner = loadWrapper.getEntity();

				Long publisherId = null;

				// TODO: может быть несколько ППС, но пока выводим первого
				if (recordOwner.isFake())
					publisherId = recordOwner.getFakePps().getId();
				else if (recordOwner.isEmployeePost() && CollectionUtils.isNotEmpty(employeePosts))
					publisherId = employeePosts.get(0).getId();
				else if (recordOwner.isTimeworker() && CollectionUtils.isNotEmpty(timeworkers))
					publisherId = timeworkers.get(0).getId();

				Map<String, Object> parameters = new HashMap<>();
				parameters.put(PublisherActivator.PUBLISHER_ID_KEY, publisherId);
				parameters.put(LoadPpsTabUI.LOAD_RECORD_OWNER_ID, recordOwner.getId());
				if (recordOwner.isEmployeePost())
				{
					parameters.put("selectedTab", EmployeePostView.EMPLOYEE_POST_TAB);
					parameters.put("selectedDataTab", EmployeePostViewExt.LOAD_TAB_NAME);
				}

				return parameters;
			}

			@SuppressWarnings("unchecked")
			@Override
			public String getComponentName(IEntity entity)
			{
				return ((ViewWrapper<LoadRecordOwner>) entity).getEntity().isEmployeePost() ? EmployeePostView.class.getSimpleName() : LoadPpsTab.class.getSimpleName();
			}
		});

		IStyleResolver redColorStyleResolver = rowEntity -> {
            PpsLoadWrapper loadWrapper = (PpsLoadWrapper) rowEntity;
            return loadWrapper.isExceededTotalLoad() ? "color: red;" : "";
        };

		IStyleResolver eduLoadColorStyleResolver = rowEntity -> {
            PpsLoadWrapper loadWrapper = (PpsLoadWrapper) rowEntity;
            return loadWrapper.isExceededTotalEduLoad() == null ? "" : loadWrapper.isExceededTotalEduLoad() ? "color: red;" : "color: green;";
        };
		return columnListExtPointBuilder(PPS_DS)
				.addColumn(ppsColumn)
				.addColumn(textColumn("scienceDegreeType", PpsLoadWrapper.SCIENCE_DEGREE_TYPE))
				.addColumn(textColumn("scienceStatusType", PpsLoadWrapper.SCIENCE_STATUS_TYPE))
				.addColumn(textColumn("rate", PpsLoadWrapper.RATE).formatter(DOUBLE_FORMATTER_2_DIGITS).width(WIDTH))
				.addColumn(booleanColumn("isSecondJob", PpsLoadWrapper.IS_SECOND_JOB).width(WIDTH))
				.addColumn(booleanColumn("isFreelancer", PpsLoadWrapper.IS_FREELANCER).width(WIDTH))
				.addColumn(textColumn("eduLoadSize", PpsLoadWrapper.EDU_LOAD).formatter(DOUBLE_FORMATTER_2_DIGITS).width(WIDTH))
				.addColumn(textColumn("otherLoadSize", PpsLoadWrapper.OTHER_LOAD).formatter(DOUBLE_FORMATTER_2_DIGITS).width(WIDTH))
				.addColumn(textColumn("totalLoadSize", PpsLoadWrapper.TOTAL_LOAD).formatter(DOUBLE_FORMATTER_2_DIGITS).width(WIDTH).styleResolver(redColorStyleResolver))
				.addColumn(textColumn("otherLoadEduSize", PpsLoadWrapper.OTHER_EDU_LOAD).formatter(DOUBLE_FORMATTER_2_DIGITS).width(WIDTH))
				.addColumn(textColumn("totalEduLoadSize", PpsLoadWrapper.TOTAL_EDU_LOAD).formatter(DOUBLE_FORMATTER_2_DIGITS).width(WIDTH).styleResolver(eduLoadColorStyleResolver))
				.addColumn(booleanColumn("hourlyLoad", PpsLoadWrapper.HOURLY_LOAD).width(WIDTH))
				.addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, "onClickEditPps").permissionKey("ui:secModel.orgUnit_editPpsLoadRecord").visible("ui:editable"))
				.addColumn(actionColumn(PRINT_COLUMN_NAME, CommonDefines.ICON_PRINT, "onClickPrintPps").permissionKey("ui:secModel.orgUnit_printPpsLoadRecord"))
				.addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, "onClickDeletePps").alert("Удалить преподавателя?").visible("ui:editable").permissionKey("ui:secModel.orgUnit_deletePpsLoadRecord"))
				.create();
	}

	@Bean
	public IDefaultComboDataSourceHandler pupnagDSHandler()
	{
		return new SpecialPupnagDSHandler(getName());
	}

	@Bean
	public IDefaultSearchDataSourceHandler ppsDSHandler()
	{
		return new DefaultSearchDataSourceHandler(getName())
		{
			@Override
			public DSOutput execute(DSInput input, ExecutionContext context)
			{
				return ListOutputBuilder.get(input, context.<List>get(PPS_WRAPPERS)).pageable(true).build();
			}
		};
	}
}