package ru.tandemservice.uniload_demo.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniload_demo.entity.LoadState;
import ru.tandemservice.uniload_demo.entity.OrgUnitLoadState;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Состояние нагрузки (подразделение)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OrgUnitLoadStateGen extends EntityBase
 implements INaturalIdentifiable<OrgUnitLoadStateGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniload_demo.entity.OrgUnitLoadState";
    public static final String ENTITY_NAME = "orgUnitLoadState";
    public static final int VERSION_HASH = 1105602135;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_YEAR = "year";
    public static final String P_LOAD_STATE = "loadState";

    private OrgUnit _orgUnit;     // Подразделение
    private EppYearEducationProcess _year;     // Учебный год
    private LoadState _loadState;     // Состояние нагрузки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     */
    @NotNull
    public EppYearEducationProcess getYear()
    {
        return _year;
    }

    /**
     * @param year Учебный год. Свойство не может быть null.
     */
    public void setYear(EppYearEducationProcess year)
    {
        dirty(_year, year);
        _year = year;
    }

    /**
     * @return Состояние нагрузки. Свойство не может быть null.
     */
    @NotNull
    public LoadState getLoadState()
    {
        return _loadState;
    }

    /**
     * @param loadState Состояние нагрузки. Свойство не может быть null.
     */
    public void setLoadState(LoadState loadState)
    {
        dirty(_loadState, loadState);
        _loadState = loadState;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof OrgUnitLoadStateGen)
        {
            if (withNaturalIdProperties)
            {
                setOrgUnit(((OrgUnitLoadState)another).getOrgUnit());
                setYear(((OrgUnitLoadState)another).getYear());
            }
            setLoadState(((OrgUnitLoadState)another).getLoadState());
        }
    }

    public INaturalId<OrgUnitLoadStateGen> getNaturalId()
    {
        return new NaturalId(getOrgUnit(), getYear());
    }

    public static class NaturalId extends NaturalIdBase<OrgUnitLoadStateGen>
    {
        private static final String PROXY_NAME = "OrgUnitLoadStateNaturalProxy";

        private Long _orgUnit;
        private Long _year;

        public NaturalId()
        {}

        public NaturalId(OrgUnit orgUnit, EppYearEducationProcess year)
        {
            _orgUnit = ((IEntity) orgUnit).getId();
            _year = ((IEntity) year).getId();
        }

        public Long getOrgUnit()
        {
            return _orgUnit;
        }

        public void setOrgUnit(Long orgUnit)
        {
            _orgUnit = orgUnit;
        }

        public Long getYear()
        {
            return _year;
        }

        public void setYear(Long year)
        {
            _year = year;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof OrgUnitLoadStateGen.NaturalId) ) return false;

            OrgUnitLoadStateGen.NaturalId that = (NaturalId) o;

            if( !equals(getOrgUnit(), that.getOrgUnit()) ) return false;
            if( !equals(getYear(), that.getYear()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getOrgUnit());
            result = hashCode(result, getYear());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getOrgUnit());
            sb.append("/");
            sb.append(getYear());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OrgUnitLoadStateGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OrgUnitLoadState.class;
        }

        public T newInstance()
        {
            return (T) new OrgUnitLoadState();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "year":
                    return obj.getYear();
                case "loadState":
                    return obj.getLoadState();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "year":
                    obj.setYear((EppYearEducationProcess) value);
                    return;
                case "loadState":
                    obj.setLoadState((LoadState) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "orgUnit":
                        return true;
                case "year":
                        return true;
                case "loadState":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "orgUnit":
                    return true;
                case "year":
                    return true;
                case "loadState":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "year":
                    return EppYearEducationProcess.class;
                case "loadState":
                    return LoadState.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OrgUnitLoadState> _dslPath = new Path<OrgUnitLoadState>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OrgUnitLoadState");
    }
            

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.OrgUnitLoadState#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.OrgUnitLoadState#getYear()
     */
    public static EppYearEducationProcess.Path<EppYearEducationProcess> year()
    {
        return _dslPath.year();
    }

    /**
     * @return Состояние нагрузки. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.OrgUnitLoadState#getLoadState()
     */
    public static PropertyPath<LoadState> loadState()
    {
        return _dslPath.loadState();
    }

    public static class Path<E extends OrgUnitLoadState> extends EntityPath<E>
    {
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private EppYearEducationProcess.Path<EppYearEducationProcess> _year;
        private PropertyPath<LoadState> _loadState;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.OrgUnitLoadState#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.OrgUnitLoadState#getYear()
     */
        public EppYearEducationProcess.Path<EppYearEducationProcess> year()
        {
            if(_year == null )
                _year = new EppYearEducationProcess.Path<EppYearEducationProcess>(L_YEAR, this);
            return _year;
        }

    /**
     * @return Состояние нагрузки. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.OrgUnitLoadState#getLoadState()
     */
        public PropertyPath<LoadState> loadState()
        {
            if(_loadState == null )
                _loadState = new PropertyPath<LoadState>(OrgUnitLoadStateGen.P_LOAD_STATE, this);
            return _loadState;
        }

        public Class getEntityClass()
        {
            return OrgUnitLoadState.class;
        }

        public String getEntityName()
        {
            return "orgUnitLoadState";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
