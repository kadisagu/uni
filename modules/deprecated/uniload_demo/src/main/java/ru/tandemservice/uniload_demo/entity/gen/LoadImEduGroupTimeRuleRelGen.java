package ru.tandemservice.uniload_demo.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniload_demo.entity.ILoadImEduGroup;
import ru.tandemservice.uniload_demo.entity.LoadImEduGroupTimeRuleRel;
import ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь нормы времени и планируемой УГС
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class LoadImEduGroupTimeRuleRelGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniload_demo.entity.LoadImEduGroupTimeRuleRel";
    public static final String ENTITY_NAME = "loadImEduGroupTimeRuleRel";
    public static final int VERSION_HASH = -1288801723;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDU_GROUP = "eduGroup";
    public static final String L_TIME_RULE = "timeRule";

    private ILoadImEduGroup _eduGroup;     // Планируемая УГС
    private LoadTimeRule _timeRule;     // Норма времени

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Планируемая УГС. Свойство не может быть null.
     */
    @NotNull
    public ILoadImEduGroup getEduGroup()
    {
        return _eduGroup;
    }

    /**
     * @param eduGroup Планируемая УГС. Свойство не может быть null.
     */
    public void setEduGroup(ILoadImEduGroup eduGroup)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && eduGroup!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(ILoadImEduGroup.class);
            IEntityMeta actual =  eduGroup instanceof IEntity ? EntityRuntime.getMeta((IEntity) eduGroup) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_eduGroup, eduGroup);
        _eduGroup = eduGroup;
    }

    /**
     * @return Норма времени. Свойство не может быть null.
     */
    @NotNull
    public LoadTimeRule getTimeRule()
    {
        return _timeRule;
    }

    /**
     * @param timeRule Норма времени. Свойство не может быть null.
     */
    public void setTimeRule(LoadTimeRule timeRule)
    {
        dirty(_timeRule, timeRule);
        _timeRule = timeRule;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof LoadImEduGroupTimeRuleRelGen)
        {
            setEduGroup(((LoadImEduGroupTimeRuleRel)another).getEduGroup());
            setTimeRule(((LoadImEduGroupTimeRuleRel)another).getTimeRule());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends LoadImEduGroupTimeRuleRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) LoadImEduGroupTimeRuleRel.class;
        }

        public T newInstance()
        {
            return (T) new LoadImEduGroupTimeRuleRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eduGroup":
                    return obj.getEduGroup();
                case "timeRule":
                    return obj.getTimeRule();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eduGroup":
                    obj.setEduGroup((ILoadImEduGroup) value);
                    return;
                case "timeRule":
                    obj.setTimeRule((LoadTimeRule) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eduGroup":
                        return true;
                case "timeRule":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eduGroup":
                    return true;
                case "timeRule":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eduGroup":
                    return ILoadImEduGroup.class;
                case "timeRule":
                    return LoadTimeRule.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<LoadImEduGroupTimeRuleRel> _dslPath = new Path<LoadImEduGroupTimeRuleRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "LoadImEduGroupTimeRuleRel");
    }
            

    /**
     * @return Планируемая УГС. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadImEduGroupTimeRuleRel#getEduGroup()
     */
    public static ILoadImEduGroupGen.Path<ILoadImEduGroup> eduGroup()
    {
        return _dslPath.eduGroup();
    }

    /**
     * @return Норма времени. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadImEduGroupTimeRuleRel#getTimeRule()
     */
    public static LoadTimeRule.Path<LoadTimeRule> timeRule()
    {
        return _dslPath.timeRule();
    }

    public static class Path<E extends LoadImEduGroupTimeRuleRel> extends EntityPath<E>
    {
        private ILoadImEduGroupGen.Path<ILoadImEduGroup> _eduGroup;
        private LoadTimeRule.Path<LoadTimeRule> _timeRule;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Планируемая УГС. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadImEduGroupTimeRuleRel#getEduGroup()
     */
        public ILoadImEduGroupGen.Path<ILoadImEduGroup> eduGroup()
        {
            if(_eduGroup == null )
                _eduGroup = new ILoadImEduGroupGen.Path<ILoadImEduGroup>(L_EDU_GROUP, this);
            return _eduGroup;
        }

    /**
     * @return Норма времени. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadImEduGroupTimeRuleRel#getTimeRule()
     */
        public LoadTimeRule.Path<LoadTimeRule> timeRule()
        {
            if(_timeRule == null )
                _timeRule = new LoadTimeRule.Path<LoadTimeRule>(L_TIME_RULE, this);
            return _timeRule;
        }

        public Class getEntityClass()
        {
            return LoadImEduGroupTimeRuleRel.class;
        }

        public String getEntityName()
        {
            return "loadImEduGroupTimeRuleRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
