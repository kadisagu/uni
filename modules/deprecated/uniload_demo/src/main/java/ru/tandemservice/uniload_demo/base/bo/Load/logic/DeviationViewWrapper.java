/*$Id$*/
package ru.tandemservice.uniload_demo.base.bo.Load.logic;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniload_demo.base.bo.Load.util.PpsLoadDataWrapper;
import ru.tandemservice.uniload_demo.entity.LoadDeviation;

/**
 * @author DMITRY KNYAZEV
 * @since 25.12.2014
 */
public class DeviationViewWrapper extends ViewWrapper<LoadDeviation>
{
	public static final String FIO = "fio";
	public static final String RATE = "rate";
	public static final String TOTAL_LOAD = "totalLoad";
	public static final String REAL_LOAD= "realLoad";
	public static final String EDU_GROUP_TITLE = "eduGroupTitle";

	public DeviationViewWrapper(LoadDeviation entity, PpsLoadDataWrapper loadDataWrapper)
	{
		super(entity);
		this.setViewProperty(FIO, entity.getOwner().getFio());
		this.setViewProperty(RATE, entity.getOwner().getRate());
		Double totalLoad = UniEppUtils.wrap(loadDataWrapper.getTotalLoad());
		this.setViewProperty(TOTAL_LOAD, doubleToString(totalLoad));
		Double realLoad = totalLoad + entity.getDeviation();
		this.setViewProperty(REAL_LOAD,doubleToString(realLoad));
		this.setViewProperty(EDU_GROUP_TITLE, entity.getEduGroup().getTitle());
	}

	private String doubleToString(Double value){
		return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(value);
	}
}
