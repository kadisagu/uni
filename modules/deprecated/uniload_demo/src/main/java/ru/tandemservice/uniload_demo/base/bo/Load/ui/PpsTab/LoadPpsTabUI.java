/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.base.bo.Load.ui.PpsTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniload_demo.base.bo.Load.LoadManager;
import ru.tandemservice.uniload_demo.base.bo.Load.logic.ILoadDAO;
import ru.tandemservice.uniload_demo.base.bo.Load.ui.EduGroupAdd.LoadEduGroupAdd;
import ru.tandemservice.uniload_demo.base.bo.Load.ui.EduGroupAdd.LoadEduGroupAddUI;
import ru.tandemservice.uniload_demo.base.bo.Load.ui.FirstCourseImEduGroupAddEdit.LoadFirstCourseImEduGroupAddEdit;
import ru.tandemservice.uniload_demo.base.bo.Load.ui.FirstCourseImEduGroupAddEdit.LoadFirstCourseImEduGroupAddEditUI;
import ru.tandemservice.uniload_demo.base.bo.Load.ui.OtherLoadRecordAddEdit.LoadOtherLoadRecordAddEdit;
import ru.tandemservice.uniload_demo.base.bo.Load.ui.OtherLoadRecordAddEdit.LoadOtherLoadRecordAddEditUI;
import ru.tandemservice.uniload_demo.base.bo.Load.util.PpsLoadDataWrapper;
import ru.tandemservice.uniload_demo.entity.LoadRecordOwner;

import java.util.HashMap;

/**
 * @author Alexander Zhebko
 * @since 20.12.2013
 */
@Input({@Bind(key = LoadPpsTabUI.LOAD_RECORD_OWNER_ID, binding = LoadPpsTabUI.LOAD_RECORD_OWNER_ID)})
public class LoadPpsTabUI extends UIPresenter
{
	public static final String LOAD_RECORD_OWNER_ID = "loadRecordOwnerId";

	public static final String FIRST_COURSE_EDU_LOAD = "firstCourseEduLoad";
	public static final String OTHER_LOAD = "otherLoad";
	public static final String EDU_LOAD = "eduLoad";

	private Long _loadRecordOwnerId;

	private PpsLoadDataWrapper _ppsData;
	private boolean _ppsRecordsAddEditDisabled;
	private boolean _fakePps;
	private String _fakePpsCaption;

	@Override
	public void onComponentRefresh()
	{
		ILoadDAO dao = LoadManager.instance().dao();
		setPupnag();
		if (!isPageDisabled())
		{
			LoadRecordOwner loadRecordOwner = dao.getNotNull(LoadRecordOwner.class, getLoadRecordOwnerId());

			_ppsData = dao.getPpsLoadDataWrapper(getLoadRecordOwnerId());
			_ppsRecordsAddEditDisabled = dao.isPpsLoadRecordAddEditDisabled(getLoadRecordOwnerId());
			_fakePps = false;
			if (loadRecordOwner.getFakePps() != null)
			{
				_fakePps = true;
				_fakePpsCaption = loadRecordOwner.getFakePps().getVacancyName() + " (" + loadRecordOwner.getFakePps().getOrgUnit().getShortTitle() + ")";
			}
		}
	}

	@Override
	public void onBeforeDataSourceFetch(IUIDataSource dataSource)
	{
		if (dataSource.getName().equals(LoadPpsTab.EDU_LOAD_DS))
			dataSource.put(EDU_LOAD, _ppsData.getEduLoadRows());

		if (dataSource.getName().equals(LoadPpsTab.OTHER_LOAD_DS))
			dataSource.put(OTHER_LOAD, _ppsData.getOtherLoadRows());

		if (dataSource.getName().equals(LoadPpsTab.FIRST_COURSE_EDU_LOAD_DS))
			dataSource.put(FIRST_COURSE_EDU_LOAD, _ppsData.getFirstCourseImEduGroups());
	}

	//handlers
	public void onClickAddEduGroup()
	{
		_uiActivation.asRegionDialog(LoadEduGroupAdd.class)
				.parameters(new HashMap<String, Object>()
				{{
						put(LoadEduGroupAddUI.PPS_ID_KEY, getLoadRecordOwnerId());
						put(LoadEduGroupAddUI.PUPNAG_ID_KEY, getPupnag().getId());
					}})
				.activate();
	}

	public void onClickAddOtherLoad()
	{
		_uiActivation.asRegionDialog(LoadOtherLoadRecordAddEdit.class)
				.parameter(LoadOtherLoadRecordAddEditUI.PUPNAG_ID, getPupnag().getId())
				.parameter(LoadOtherLoadRecordAddEditUI.PPS_ID, getLoadRecordOwnerId()).activate();
	}

	public void onEditEntityFromList()
	{
		_uiActivation.asRegionDialog(LoadOtherLoadRecordAddEdit.class)
				.parameter(LoadOtherLoadRecordAddEditUI.PUPNAG_ID, getPupnag().getId())
				.parameter(LoadOtherLoadRecordAddEditUI.RECORD_ID, getListenerParameter()).activate();
	}

	public void onClickAddFirstCourseImEduGroup()
	{
		_uiActivation.asRegionDialog(LoadFirstCourseImEduGroupAddEdit.class)
				.parameter(LoadFirstCourseImEduGroupAddEditUI.PPS_ID, getLoadRecordOwnerId())
				.parameter(LoadFirstCourseImEduGroupAddEditUI.PUPNAG_ID, getPupnag().getId())
				.activate();
	}

	@SuppressWarnings("UnusedDeclaration")
	public void onClickEditEduLoadDistribution()
	{
		_uiActivation.asRegionDialog(LoadEduGroupAdd.class)
				.parameters(new HashMap<String, Object>()
				{{
						put(LoadEduGroupAddUI.PPS_ID_KEY, getLoadRecordOwnerId());
						put(LoadEduGroupAddUI.PUPNAG_ID_KEY, getPupnag().getId());
						put(PublisherActivator.PUBLISHER_ID_KEY, getListenerParameter());
					}})
				.activate();
	}

	@SuppressWarnings("UnusedDeclaration")
	public void onClickEditFirstCourseImEduGroup()
	{
		_uiActivation.asRegionDialog(LoadFirstCourseImEduGroupAddEdit.class)
				.parameter(LoadFirstCourseImEduGroupAddEditUI.RECORD_ID, this.getListenerParameter())
				.parameter(LoadFirstCourseImEduGroupAddEditUI.PUPNAG_ID, getPupnag().getId())
				.activate();
	}

	@SuppressWarnings("UnusedDeclaration")
	public void onClickDeleteEduLoadDistribution()
	{
		LoadManager.instance().dao().deleteEduGroupDistr(getLoadRecordOwnerId(), this.<Long>getListenerParameter());
		_ppsData = LoadManager.instance().dao().getPpsLoadDataWrapper(getLoadRecordOwnerId());
	}

	@SuppressWarnings("UnusedDeclaration")
	public void onClickDeleteFirstCourseImEduGroup()
	{
		LoadManager.instance().dao().delete(this.<Long>getListenerParameter());
		_ppsData = LoadManager.instance().dao().getPpsLoadDataWrapper(getLoadRecordOwnerId());
	}

	public void onDeleteEntityFromList()
	{
		LoadManager.instance().dao().delete(this.<Long>getListenerParameter());
		_ppsData = LoadManager.instance().dao().getPpsLoadDataWrapper(getLoadRecordOwnerId());
	}

	public void onClickSearch()
	{
		getSettings().save();
	}

	public void onClickClear()
	{
		getSettings().clear();
		setPupnag();
		onClickSearch();
	}

	//getters/setters
	@SuppressWarnings("UnusedDeclaration")
	public void setLoadRecordOwnerId(Long loadRecordOwnerId)
	{
		_loadRecordOwnerId = loadRecordOwnerId;
	}

	Long getLoadRecordOwnerId()
	{
		return _loadRecordOwnerId;
	}


	private EppYearEducationProcess getPupnag()
	{
		return getSettings().get("pupnag");
	}

	private void setPupnag()
	{
		EppYearEducationProcess pupnag = LoadManager.instance().dao().getNextYear();
		getSettings().set("pupnag", pupnag);
	}

	public boolean isPpsRecordsAddEditDisabled()
	{
		return _ppsRecordsAddEditDisabled;
	}

	@SuppressWarnings("UnusedDeclaration")
	public boolean isFakePps()
	{
		return _fakePps;
	}

	public boolean isPageDisabled()
	{
		return getLoadRecordOwnerId() == null || getPupnag() == null;
	}

	@SuppressWarnings("UnusedDeclaration")
	public String getFakePpsCaption()
	{
		return _fakePpsCaption;
	}

	@SuppressWarnings("UnusedDeclaration")
	public String getTotalEduLoadSize()
	{
		Double totalEduLoadSize = UniEppUtils.wrap(_ppsData.getTotalEduLoad());
		return "Всего учебной нагрузки: " + (_ppsData.getEduLoadRows().isEmpty() ? "нет" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(totalEduLoadSize));
	}

	@SuppressWarnings("UnusedDeclaration")
	public String getTotalOtherLoadSize()
	{
		Double totalOtherLoadSize = UniEppUtils.wrap(_ppsData.getTotalOtherLoad());
		return "Всего других видов нагрузки: " + (_ppsData.getOtherLoadRows().isEmpty() ? "нет" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(totalOtherLoadSize));
	}

	@SuppressWarnings("UnusedDeclaration")
	public String getTotalLoadSize()
	{
		Double totalLoadSize = UniEppUtils.wrap(_ppsData.getTotalLoad());
		return "Всего: " + (totalLoadSize == 0L ? "нет" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(totalLoadSize));
	}

	@SuppressWarnings("UnusedDeclaration")
	public boolean isAddOtherLoadDisabled()
	{
		return LoadManager.instance().dao().getNextYear() == null || isPpsRecordsAddEditDisabled();
	}
}