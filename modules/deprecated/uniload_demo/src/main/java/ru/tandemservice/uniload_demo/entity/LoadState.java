/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.entity;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Alexander Zhebko
 * @since 12.12.2013
 */
public enum LoadState
{
    FORMATIVE("Формируется", "Отправить на формирование"),
    ACCEPTABLE("На согласовании", "Отправить на согласование"),
    ACCEPTED("Согласовано", "Согласовать");

    private final static Map<LoadState, List<LoadState>> POSSIBLE_TRANSITIONS_MAP = new HashMap<>();
    static
    {
        POSSIBLE_TRANSITIONS_MAP.put(FORMATIVE, Arrays.asList(ACCEPTABLE));
        POSSIBLE_TRANSITIONS_MAP.put(ACCEPTABLE, Arrays.asList(ACCEPTED));
        POSSIBLE_TRANSITIONS_MAP.put(ACCEPTED, Arrays.asList(FORMATIVE));
    }

    private String _title;
    private String _actionTitle;

    LoadState(String title, String actionTitle)
    {
        _title = title;
        _actionTitle = actionTitle;
    }

    /** @return название состояние */
    public String getTitle(){ return _title; }

    /** @return заголовок действия перевода */
    public String getActionTitle(){ return _actionTitle; }

    /** @return список возможных состояний */
    public List<LoadState> getPossibleTransitions(){ return POSSIBLE_TRANSITIONS_MAP.get(this); }
}