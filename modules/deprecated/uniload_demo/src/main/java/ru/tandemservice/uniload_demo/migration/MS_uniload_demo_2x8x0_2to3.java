package ru.tandemservice.uniload_demo.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused"})
public class MS_uniload_demo_2x8x0_2to3 extends IndependentMigrationScript
{

    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.17"),
                        new ScriptDependency("org.tandemframework.shared", "1.8.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность loadOwnerAdditional переименована в loadRealPpsEntry
        {
            tool.renameTable("loadowneradditional_t", "loadrealppsentry_t");
            tool.entityCodes().rename("loadOwnerAdditional", "loadRealPpsEntry");
        }
    }
}