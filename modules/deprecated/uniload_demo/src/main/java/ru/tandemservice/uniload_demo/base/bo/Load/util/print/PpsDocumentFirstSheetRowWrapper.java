/*$Id$*/
package ru.tandemservice.uniload_demo.base.bo.Load.util.print;

import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.uniload_demo.base.bo.Load.LoadManager;
import ru.tandemservice.uniload_demo.base.bo.Load.logic.ILoadDAO;
import ru.tandemservice.uniload_demo.entity.ILoadImEduGroup;
import ru.tandemservice.uniload_demo.entity.LoadEduGroupPpsDistr;
import ru.tandemservice.uniload_demo.entity.LoadFirstCourseImEduGroup;

import javax.annotation.Nullable;
import java.util.*;

/**
 * @author DMITRY KNYAZEV
 * @since 30.10.2014
 */
class PpsDocumentFirstSheetRowWrapper
{
	public static final String ROW_TOTAL_LOAD_CODE = "total";

	private final EppWorkPlanRegistryElementRow _row;
	private final EppWorkPlan _workPlan;
	private final ILoadDAO _dao = LoadManager.instance().dao();
	private final EppStateEduStandard _eduStandard;

	//#10 количество студентов / аспирантов
	private Integer _firstCurseStudentNumber = 0;
	private final Set<Long> _eduGroupStudentNumber = new HashSet<>();

	//#12 количество групп
	private final Map<String, Integer> _groupNumber = new HashMap<>();

	//#13-32 Нагрузка в часах
	private final Map<String, Double> _rowLoadMap;

	//#33 всего часов
	private Double _rowTotalLoad = 0D;


	public PpsDocumentFirstSheetRowWrapper(EppWorkPlanRegistryElementRow row)
	{
		_row = row;
		_workPlan = _row.getWorkPlan().getWorkPlan();
		_rowLoadMap = new HashMap<>();
		_eduStandard = _workPlan.getParent().getEduPlanVersion().getEduPlan().getParent();
	}


	public void add(ILoadImEduGroup imEduGroup)
	{
		//#10 количество студентов / аспирантов
		addStudentNumber(imEduGroup);
		//#12 количество групп
		addGroupCounter(imEduGroup);
		//#13-32 Нагрузка в часах + #33 ВСЕГО ЧАСОВ
		addLoad(imEduGroup);
	}


	//#10 количество студентов / аспирантов
	private void addStudentNumber(ILoadImEduGroup imEduGroup)
	{
		if (imEduGroup instanceof LoadEduGroupPpsDistr)
		{
			Collection<Long> eduGroupStudentList = getDao().getEduGroupStudentsId(((LoadEduGroupPpsDistr) imEduGroup).getEduGroup().getId());
			_eduGroupStudentNumber.addAll(eduGroupStudentList);
		}
		else if (imEduGroup instanceof LoadFirstCourseImEduGroup)
		{
			LoadFirstCourseImEduGroup firstCourse = (LoadFirstCourseImEduGroup) imEduGroup;
			_firstCurseStudentNumber = Math.max(_firstCurseStudentNumber, firstCourse.getStudents());
		}
		else
			throw wrongType(imEduGroup);
	}

	//#12 количество групп
	private void addGroupCounter(ILoadImEduGroup imEduGroup)
	{
		EppGroupType groupType = getGroupType(imEduGroup);
		String typeCode = groupType.getCode();
		//Число групп / подгрупп
		if (_groupNumber.containsKey(typeCode))
		{
			Integer newValue = _groupNumber.get(typeCode) + 1;
			_groupNumber.put(typeCode, newValue);
		}
		else
			_groupNumber.put(typeCode, 1);
	}

	//#13-32 Нагрузка в часах + #33 ВСЕГО ЧАСОВ
	private void addLoad(ILoadImEduGroup imEduGroup)
	{
		_rowTotalLoad += LoadDocumentPrintSharedUtils.addEduGroupLoadToLoadMap(imEduGroup, _rowLoadMap);
	}

	//PUBLIC FIELDS
	//#2 Дисциплина
	public String getDisciplineTitle()
	{
		return _row.getDisplayableTitle();
	}

	//Форма обучения
	public String getEduForm()
	{
		return _workPlan.getParent().getEduPlanVersion().getEduPlan().getProgramForm().getShortTitle();
	}

	//#5 Государственный образовательный стандарт (ГОС-II; ФГОС)
	public String getGos()
	{
		return getEduStandard() == null ? "-" : getEduStandard().getGeneration().getShortTitle();
	}

	//#6 Код направления подготовки (специальности)
	public String getSpecializeCode()
	{
		return getEduStandard() == null ? "-" : getEduStandard().getProgramSubject().getSubjectCode();
	}

	//#7 Наименование направления подготовки (специальности)
	public String getSpecializeTitle()
	{
		return getEduStandard() == null ? "-" : getEduStandard().getProgramSubject().getTitle();
	}

	//#8 Школа
	@Nullable
	public String getSchoolTitle()
	{
		return LoadDocumentPrintSharedUtils.getSchoolTitle(_row.getRegistryElementPart().getRegistryElement().getOwner());
	}

	//#9 курс
	public String getCourse()
	{
		return String.valueOf(_workPlan.getGridTerm().getCourseNumber());
	}

	//#10 количество студентов / аспирантов
	public String getStudentNumber()
	{
		Integer studNumber = Math.max(_firstCurseStudentNumber, _eduGroupStudentNumber.size());
		return studNumber.toString();
	}

	//#12 количество групп
	public String getGroupNumber()
	{
		return Collections.max(_groupNumber.values()).toString();
	}

	//#13-32 Нагрузка в часах
	public Map<String, Double> getRowLoadMap()
	{
		HashMap<String, Double> totalLoad = new HashMap<>(_rowLoadMap);
		totalLoad.put(ROW_TOTAL_LOAD_CODE, getTotalRowLoad());
		return totalLoad;
	}

	//#33 ВСЕГО ЧАСОВ
	public Double getTotalRowLoad()
	{
		return _rowTotalLoad;
	}

	//INNER FIELDS
	protected EppWorkPlanRegistryElementRow getRow()
	{
		return _row;
	}

	protected EppWorkPlan getWorkPlan()
	{
		return _workPlan;
	}

	@Nullable
	protected EppStateEduStandard getEduStandard()
	{
		return _eduStandard;
	}

	//SHARED UTILS
	protected RuntimeException wrongType(ILoadImEduGroup imEduGroup)
	{
		return new IllegalStateException("Недопустимый объект планируемой УГС (демо):" + imEduGroup.getClass().getSimpleName());
	}

	protected EppGroupType getGroupType(ILoadImEduGroup imEduGroup)
	{
		if (imEduGroup instanceof LoadEduGroupPpsDistr)
			return ((LoadEduGroupPpsDistr) imEduGroup).getEduGroup().getType();
		else if (imEduGroup instanceof LoadFirstCourseImEduGroup)
			return ((LoadFirstCourseImEduGroup) imEduGroup).getType();
		else
			throw wrongType(imEduGroup);
	}

	protected ILoadDAO getDao()
	{
		return _dao;
	}

	@Override
	public int hashCode()
	{
		return _row.hashCode();
	}

	@Override
	public boolean equals(Object obj)
	{
		if (!(obj instanceof PpsDocumentFirstSheetRowWrapper)) return false;
		PpsDocumentFirstSheetRowWrapper instance = ((PpsDocumentFirstSheetRowWrapper) obj);
		return _row.getId().equals(instance._row.getId());
	}
}
