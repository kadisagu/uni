/**
 *$Id$
 */
package ru.tandemservice.uniload_demo.base.ext.Employee.ui.PostView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostView.EmployeePostView;
import ru.tandemservice.uniload_demo.base.bo.Load.ui.PpsTab.LoadPpsTab;

/**
 * @author Alexander Zhebko
 * @since 20.12.2013
 */
@Configuration
public class EmployeePostViewExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "load" + EmployeePostViewExtUI.class.getSimpleName();

    public static final String LOAD_TAB_NAME = "Load";

    @Autowired
    private EmployeePostView _employeePostView;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_employeePostView.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, EmployeePostViewExtUI.class))
                .create();
    }

    @Bean
    public TabPanelExtension tabPanelExtension()
    {
        return tabPanelExtensionBuilder(_employeePostView.employeePostTabPanelExtPoint())
                .addAllAfter(EmployeePostView.EMPLOYEE_POST_DATA_TAB)
                .addTab(componentTab(LOAD_TAB_NAME, LoadPpsTab.class)
                        .parameters("addon:" + ADDON_NAME + ".parameters")
                        .visible("addon:" + ADDON_NAME + ".visibleLoadTab")
                        .permissionKey("employeePost_viewLoadTab"))
                .create();
    }
}