package ru.tandemservice.uniload_demo.entity;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniload_demo.entity.gen.*;

/**
 * Распределение часов УГС
 */
public class LoadEduGroupPpsDistr extends LoadEduGroupPpsDistrGen
{
    public LoadEduGroupPpsDistr()
    {}

    public LoadEduGroupPpsDistr(LoadRecordOwner owner, EppRealEduGroup eduGroup)
    {
        this.setOwner(owner);
        this.setEduGroup(eduGroup);
    }

    @Override
    @EntityDSLSupport
    public String getTitle()
    {
        return this.getEduGroupTitle();
    }
}