package ru.tandemservice.uniload_demo.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniload_demo.entity.LoadFakePpsEntry;
import ru.tandemservice.uniload_demo.entity.LoadRealPpsEntry;
import ru.tandemservice.uniload_demo.entity.LoadRecordOwner;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * ППС (нагрузка)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class LoadRecordOwnerGen extends EntityBase
 implements INaturalIdentifiable<LoadRecordOwnerGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniload_demo.entity.LoadRecordOwner";
    public static final String ENTITY_NAME = "loadRecordOwner";
    public static final int VERSION_HASH = -1413017321;
    private static IEntityMeta ENTITY_META;

    public static final String L_REAL_PPS = "realPps";
    public static final String L_FAKE_PPS = "fakePps";
    public static final String L_YEAR = "year";
    public static final String P_HOURLY_LOAD = "hourlyLoad";

    private LoadRealPpsEntry _realPps;     // Реальный ППС
    private LoadFakePpsEntry _fakePps;     // Фиктивный ППС
    private EppYearEducationProcess _year;     // Учебный год
    private Boolean _hourlyLoad;     // Признак почасовой нагрузки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Реальный ППС.
     */
    public LoadRealPpsEntry getRealPps()
    {
        return _realPps;
    }

    /**
     * @param realPps Реальный ППС.
     */
    public void setRealPps(LoadRealPpsEntry realPps)
    {
        dirty(_realPps, realPps);
        _realPps = realPps;
    }

    /**
     * @return Фиктивный ППС.
     */
    public LoadFakePpsEntry getFakePps()
    {
        return _fakePps;
    }

    /**
     * @param fakePps Фиктивный ППС.
     */
    public void setFakePps(LoadFakePpsEntry fakePps)
    {
        dirty(_fakePps, fakePps);
        _fakePps = fakePps;
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     */
    @NotNull
    public EppYearEducationProcess getYear()
    {
        return _year;
    }

    /**
     * @param year Учебный год. Свойство не может быть null.
     */
    public void setYear(EppYearEducationProcess year)
    {
        dirty(_year, year);
        _year = year;
    }

    /**
     * @return Признак почасовой нагрузки.
     */
    public Boolean getHourlyLoad()
    {
        return _hourlyLoad;
    }

    /**
     * @param hourlyLoad Признак почасовой нагрузки.
     */
    public void setHourlyLoad(Boolean hourlyLoad)
    {
        dirty(_hourlyLoad, hourlyLoad);
        _hourlyLoad = hourlyLoad;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof LoadRecordOwnerGen)
        {
            if (withNaturalIdProperties)
            {
                setRealPps(((LoadRecordOwner)another).getRealPps());
                setFakePps(((LoadRecordOwner)another).getFakePps());
                setYear(((LoadRecordOwner)another).getYear());
            }
            setHourlyLoad(((LoadRecordOwner)another).getHourlyLoad());
        }
    }

    public INaturalId<LoadRecordOwnerGen> getNaturalId()
    {
        return new NaturalId(getRealPps(), getFakePps(), getYear());
    }

    public static class NaturalId extends NaturalIdBase<LoadRecordOwnerGen>
    {
        private static final String PROXY_NAME = "LoadRecordOwnerNaturalProxy";

        private Long _realPps;
        private Long _fakePps;
        private Long _year;

        public NaturalId()
        {}

        public NaturalId(LoadRealPpsEntry realPps, LoadFakePpsEntry fakePps, EppYearEducationProcess year)
        {
            _realPps = realPps==null ? null : ((IEntity) realPps).getId();
            _fakePps = fakePps==null ? null : ((IEntity) fakePps).getId();
            _year = ((IEntity) year).getId();
        }

        public Long getRealPps()
        {
            return _realPps;
        }

        public void setRealPps(Long realPps)
        {
            _realPps = realPps;
        }

        public Long getFakePps()
        {
            return _fakePps;
        }

        public void setFakePps(Long fakePps)
        {
            _fakePps = fakePps;
        }

        public Long getYear()
        {
            return _year;
        }

        public void setYear(Long year)
        {
            _year = year;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof LoadRecordOwnerGen.NaturalId) ) return false;

            LoadRecordOwnerGen.NaturalId that = (NaturalId) o;

            if( !equals(getRealPps(), that.getRealPps()) ) return false;
            if( !equals(getFakePps(), that.getFakePps()) ) return false;
            if( !equals(getYear(), that.getYear()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getRealPps());
            result = hashCode(result, getFakePps());
            result = hashCode(result, getYear());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getRealPps());
            sb.append("/");
            sb.append(getFakePps());
            sb.append("/");
            sb.append(getYear());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends LoadRecordOwnerGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) LoadRecordOwner.class;
        }

        public T newInstance()
        {
            return (T) new LoadRecordOwner();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "realPps":
                    return obj.getRealPps();
                case "fakePps":
                    return obj.getFakePps();
                case "year":
                    return obj.getYear();
                case "hourlyLoad":
                    return obj.getHourlyLoad();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "realPps":
                    obj.setRealPps((LoadRealPpsEntry) value);
                    return;
                case "fakePps":
                    obj.setFakePps((LoadFakePpsEntry) value);
                    return;
                case "year":
                    obj.setYear((EppYearEducationProcess) value);
                    return;
                case "hourlyLoad":
                    obj.setHourlyLoad((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "realPps":
                        return true;
                case "fakePps":
                        return true;
                case "year":
                        return true;
                case "hourlyLoad":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "realPps":
                    return true;
                case "fakePps":
                    return true;
                case "year":
                    return true;
                case "hourlyLoad":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "realPps":
                    return LoadRealPpsEntry.class;
                case "fakePps":
                    return LoadFakePpsEntry.class;
                case "year":
                    return EppYearEducationProcess.class;
                case "hourlyLoad":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<LoadRecordOwner> _dslPath = new Path<LoadRecordOwner>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "LoadRecordOwner");
    }
            

    /**
     * @return Реальный ППС.
     * @see ru.tandemservice.uniload_demo.entity.LoadRecordOwner#getRealPps()
     */
    public static LoadRealPpsEntry.Path<LoadRealPpsEntry> realPps()
    {
        return _dslPath.realPps();
    }

    /**
     * @return Фиктивный ППС.
     * @see ru.tandemservice.uniload_demo.entity.LoadRecordOwner#getFakePps()
     */
    public static LoadFakePpsEntry.Path<LoadFakePpsEntry> fakePps()
    {
        return _dslPath.fakePps();
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadRecordOwner#getYear()
     */
    public static EppYearEducationProcess.Path<EppYearEducationProcess> year()
    {
        return _dslPath.year();
    }

    /**
     * @return Признак почасовой нагрузки.
     * @see ru.tandemservice.uniload_demo.entity.LoadRecordOwner#getHourlyLoad()
     */
    public static PropertyPath<Boolean> hourlyLoad()
    {
        return _dslPath.hourlyLoad();
    }

    public static class Path<E extends LoadRecordOwner> extends EntityPath<E>
    {
        private LoadRealPpsEntry.Path<LoadRealPpsEntry> _realPps;
        private LoadFakePpsEntry.Path<LoadFakePpsEntry> _fakePps;
        private EppYearEducationProcess.Path<EppYearEducationProcess> _year;
        private PropertyPath<Boolean> _hourlyLoad;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Реальный ППС.
     * @see ru.tandemservice.uniload_demo.entity.LoadRecordOwner#getRealPps()
     */
        public LoadRealPpsEntry.Path<LoadRealPpsEntry> realPps()
        {
            if(_realPps == null )
                _realPps = new LoadRealPpsEntry.Path<LoadRealPpsEntry>(L_REAL_PPS, this);
            return _realPps;
        }

    /**
     * @return Фиктивный ППС.
     * @see ru.tandemservice.uniload_demo.entity.LoadRecordOwner#getFakePps()
     */
        public LoadFakePpsEntry.Path<LoadFakePpsEntry> fakePps()
        {
            if(_fakePps == null )
                _fakePps = new LoadFakePpsEntry.Path<LoadFakePpsEntry>(L_FAKE_PPS, this);
            return _fakePps;
        }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.uniload_demo.entity.LoadRecordOwner#getYear()
     */
        public EppYearEducationProcess.Path<EppYearEducationProcess> year()
        {
            if(_year == null )
                _year = new EppYearEducationProcess.Path<EppYearEducationProcess>(L_YEAR, this);
            return _year;
        }

    /**
     * @return Признак почасовой нагрузки.
     * @see ru.tandemservice.uniload_demo.entity.LoadRecordOwner#getHourlyLoad()
     */
        public PropertyPath<Boolean> hourlyLoad()
        {
            if(_hourlyLoad == null )
                _hourlyLoad = new PropertyPath<Boolean>(LoadRecordOwnerGen.P_HOURLY_LOAD, this);
            return _hourlyLoad;
        }

        public Class getEntityClass()
        {
            return LoadRecordOwner.class;
        }

        public String getEntityName()
        {
            return "loadRecordOwner";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
