package ru.tandemservice.uniload_demo.entity;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.employee.pps.PpsEntryByEmployeePost;
import ru.tandemservice.uniload_demo.base.bo.Load.LoadManager;
import ru.tandemservice.uniload_demo.entity.gen.LoadRecordOwnerGen;
import ru.tandemservice.unisnpps.entity.pps.PpsEntryByTimeworker;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * ППС (нагрузка)
 */
public class LoadRecordOwner extends LoadRecordOwnerGen implements ITitled
{

	@Override
	public void setRealPps(LoadRealPpsEntry realPps)
	{
		if(this.getFakePps() != null && realPps != null)
			getIllegalStateException();
		else
			super.setRealPps(realPps);
	}

	@Override
	public void setFakePps(LoadFakePpsEntry fakePps)
	{
		if(this.getRealPps() != null && fakePps != null)
			getIllegalStateException();
		else
			super.setFakePps(fakePps);
	}

	@Override
	public String getTitle()
	{
		return this.isReal() ? this.getPps().getTitle() : this.getFakePps().getVacancyName();
	}

	@Override
	public Boolean getHourlyLoad()
	{
		if (super.getHourlyLoad() == null) return false;
		return super.getHourlyLoad();
	}

	private void getIllegalStateException()
	{
		throw new IllegalStateException("Only RealPps or FakePps can exist, not both");
	}

	public Long getPpsId()
	{
		if (isFake())
			return this.getFakePps().getId();
//		if (isEmployeePost())
//			return ((PpsEntryByEmployeePost) this.getPps()).getSource().getId();
//		if (isTimeworker())
//			return ((PpsEntryByTimeworker) this.getPps()).getSource().getId();
		throw new IllegalStateException();
	}

	public boolean isReal()
	{
		return this.getFakePps() == null && this.getRealPps() != null;
	}

	public boolean isFake()
	{
		return this.getFakePps() != null && this.getRealPps() == null;
	}

	public boolean isTimeworker(){
		return isReal() && getPps() instanceof PpsEntryByTimeworker;
	}

	public boolean isEmployeePost(){
		return isReal() && getPps() instanceof PpsEntryByEmployeePost;
	}

	public boolean isSecondJob(){
		if(this.isEmployeePost())
			return LoadManager.instance().dao().getPpsSecondJobInfoMap(Collections.singleton(this.getId())).get(this.getPps().getId());
		return false;
	}
	public OrgUnit getOrgUnit()
	{
		return this.isReal() ? this.getPps().getOrgUnit() : this.getFakePps().getOrgUnit();
	}

	public String getFio()
	{
		if (this.isReal()) return this.getPps().getFio();
		else return this.getFakePps().getVacancyName();
	}

	public String getFullFio()
	{
		if (this.isReal()) return this.getPps().getFullFio();
		else return this.getFakePps().getVacancyName();
	}

	public Double getRate()
	{
		if (isFake())
			return this.getFakePps().getStaffRateDouble();
		if (isReal())
			return this.getRealPps().getRateAsDouble();
		throw new IllegalStateException();
	}

	@Nullable
	public PostBoundedWithQGandQL getPost()
	{
		if (isFake())
			return this.getFakePps().getPost();
//		if (isEmployeePost())
//			return ((PpsEntryByEmployeePost) this.getPps()).getSource().getPostRelation().getPostBoundedWithQGandQL();
		return null;
	}

	public String getScienceDegreeType()
	{
		return getScienceDegreeType("-");
	}

	public String getScienceDegreeType(String defaultString)
	{
		if (getPost() == null || getPost().getScienceDegreeType() == null)
			return defaultString;
		return getPost().getScienceDegreeType().getTitle();
	}

	public String getScienceStatusType()
	{
		return getScienceStatusType("-");
	}

	public String getScienceStatusType(String defaultString)
	{
		if (getPost() == null || getPost().getScienceStatusType() == null)
			return defaultString;
		return getPost().getScienceStatusType().getTitle();
	}

	public String getPostTitle(String defaultString)
	{
		if (getPost() == null)
			return defaultString;
		return this.getPost().getTitle();
	}

	public List<String> getPpsEmployeeStatus()
	{
		List<String> res = new ArrayList<>();
		if (this.isReal())
		{
			boolean isFreelance = this.getRealPps().isFreelance();
			if (isFreelance) res.add("ш");
			else res.add("нш");

			if (isSecondJob()) res.add("с");
			else res.add("нс");
		}

		if (this.getHourlyLoad()) res.add("п");
		else res.add("нп");

		return res;
	}

	private PpsEntry getPps(){
		return this.getRealPps().getPps();
	}
}