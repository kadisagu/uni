package ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodKey;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Даты приемной кампании ФИС (ключ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EcfSettingsECPeriodKeyGen extends EntityBase
 implements INaturalIdentifiable<EcfSettingsECPeriodKeyGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodKey";
    public static final String ENTITY_NAME = "ecfSettingsECPeriodKey";
    public static final int VERSION_HASH = -140375061;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_EDUCATION_LEVEL = "educationLevel";
    public static final String L_EDUCATION_FORM = "educationForm";
    public static final String L_EDUCATION_SOURCE = "educationSource";
    public static final String L_COURSE = "course";
    public static final String P_UID = "uid";
    public static final String P_SIZE = "size";
    public static final String P_SIZE_TITLE = "sizeTitle";
    public static final String P_TITLE = "title";

    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private EcfCatalogItem _educationLevel;     // Уровень образования ФИС
    private EcfCatalogItem _educationForm;     // Форма обучения ФИС
    private EcfCatalogItem _educationSource;     // Источник финансирования ФИС
    private Course _course;     // Курс (на который осуществляется прием)
    private String _uid;     // Уникальный идентификатор (для выгрузки в ФИС)
    private int _size;     // Количество этапов

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Уровень образования ФИС. Свойство не может быть null.
     */
    @NotNull
    public EcfCatalogItem getEducationLevel()
    {
        return _educationLevel;
    }

    /**
     * @param educationLevel Уровень образования ФИС. Свойство не может быть null.
     */
    public void setEducationLevel(EcfCatalogItem educationLevel)
    {
        dirty(_educationLevel, educationLevel);
        _educationLevel = educationLevel;
    }

    /**
     * @return Форма обучения ФИС. Свойство не может быть null.
     */
    @NotNull
    public EcfCatalogItem getEducationForm()
    {
        return _educationForm;
    }

    /**
     * @param educationForm Форма обучения ФИС. Свойство не может быть null.
     */
    public void setEducationForm(EcfCatalogItem educationForm)
    {
        dirty(_educationForm, educationForm);
        _educationForm = educationForm;
    }

    /**
     * @return Источник финансирования ФИС. Свойство не может быть null.
     */
    @NotNull
    public EcfCatalogItem getEducationSource()
    {
        return _educationSource;
    }

    /**
     * @param educationSource Источник финансирования ФИС. Свойство не может быть null.
     */
    public void setEducationSource(EcfCatalogItem educationSource)
    {
        dirty(_educationSource, educationSource);
        _educationSource = educationSource;
    }

    /**
     * @return Курс (на который осуществляется прием). Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс (на который осуществляется прием). Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * Формируется на основе полей ключа
     *
     * @return Уникальный идентификатор (для выгрузки в ФИС). Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getUid()
    {
        return _uid;
    }

    /**
     * @param uid Уникальный идентификатор (для выгрузки в ФИС). Свойство не может быть null и должно быть уникальным.
     */
    public void setUid(String uid)
    {
        dirty(_uid, uid);
        _uid = uid;
    }

    /**
     * 0 - нет приема
     * 1 - прием без этапов (1 единственный этап)
     * 2,3,... - число этапов
     *
     * @return Количество этапов. Свойство не может быть null.
     */
    @NotNull
    public int getSize()
    {
        return _size;
    }

    /**
     * @param size Количество этапов. Свойство не может быть null.
     */
    public void setSize(int size)
    {
        dirty(_size, size);
        _size = size;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EcfSettingsECPeriodKeyGen)
        {
            if (withNaturalIdProperties)
            {
                setEnrollmentCampaign(((EcfSettingsECPeriodKey)another).getEnrollmentCampaign());
                setEducationLevel(((EcfSettingsECPeriodKey)another).getEducationLevel());
                setEducationForm(((EcfSettingsECPeriodKey)another).getEducationForm());
                setEducationSource(((EcfSettingsECPeriodKey)another).getEducationSource());
                setCourse(((EcfSettingsECPeriodKey)another).getCourse());
            }
            setUid(((EcfSettingsECPeriodKey)another).getUid());
            setSize(((EcfSettingsECPeriodKey)another).getSize());
        }
    }

    public INaturalId<EcfSettingsECPeriodKeyGen> getNaturalId()
    {
        return new NaturalId(getEnrollmentCampaign(), getEducationLevel(), getEducationForm(), getEducationSource(), getCourse());
    }

    public static class NaturalId extends NaturalIdBase<EcfSettingsECPeriodKeyGen>
    {
        private static final String PROXY_NAME = "EcfSettingsECPeriodKeyNaturalProxy";

        private Long _enrollmentCampaign;
        private Long _educationLevel;
        private Long _educationForm;
        private Long _educationSource;
        private Long _course;

        public NaturalId()
        {}

        public NaturalId(EnrollmentCampaign enrollmentCampaign, EcfCatalogItem educationLevel, EcfCatalogItem educationForm, EcfCatalogItem educationSource, Course course)
        {
            _enrollmentCampaign = ((IEntity) enrollmentCampaign).getId();
            _educationLevel = ((IEntity) educationLevel).getId();
            _educationForm = ((IEntity) educationForm).getId();
            _educationSource = ((IEntity) educationSource).getId();
            _course = ((IEntity) course).getId();
        }

        public Long getEnrollmentCampaign()
        {
            return _enrollmentCampaign;
        }

        public void setEnrollmentCampaign(Long enrollmentCampaign)
        {
            _enrollmentCampaign = enrollmentCampaign;
        }

        public Long getEducationLevel()
        {
            return _educationLevel;
        }

        public void setEducationLevel(Long educationLevel)
        {
            _educationLevel = educationLevel;
        }

        public Long getEducationForm()
        {
            return _educationForm;
        }

        public void setEducationForm(Long educationForm)
        {
            _educationForm = educationForm;
        }

        public Long getEducationSource()
        {
            return _educationSource;
        }

        public void setEducationSource(Long educationSource)
        {
            _educationSource = educationSource;
        }

        public Long getCourse()
        {
            return _course;
        }

        public void setCourse(Long course)
        {
            _course = course;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EcfSettingsECPeriodKeyGen.NaturalId) ) return false;

            EcfSettingsECPeriodKeyGen.NaturalId that = (NaturalId) o;

            if( !equals(getEnrollmentCampaign(), that.getEnrollmentCampaign()) ) return false;
            if( !equals(getEducationLevel(), that.getEducationLevel()) ) return false;
            if( !equals(getEducationForm(), that.getEducationForm()) ) return false;
            if( !equals(getEducationSource(), that.getEducationSource()) ) return false;
            if( !equals(getCourse(), that.getCourse()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEnrollmentCampaign());
            result = hashCode(result, getEducationLevel());
            result = hashCode(result, getEducationForm());
            result = hashCode(result, getEducationSource());
            result = hashCode(result, getCourse());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEnrollmentCampaign());
            sb.append("/");
            sb.append(getEducationLevel());
            sb.append("/");
            sb.append(getEducationForm());
            sb.append("/");
            sb.append(getEducationSource());
            sb.append("/");
            sb.append(getCourse());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EcfSettingsECPeriodKeyGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EcfSettingsECPeriodKey.class;
        }

        public T newInstance()
        {
            return (T) new EcfSettingsECPeriodKey();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "educationLevel":
                    return obj.getEducationLevel();
                case "educationForm":
                    return obj.getEducationForm();
                case "educationSource":
                    return obj.getEducationSource();
                case "course":
                    return obj.getCourse();
                case "uid":
                    return obj.getUid();
                case "size":
                    return obj.getSize();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "educationLevel":
                    obj.setEducationLevel((EcfCatalogItem) value);
                    return;
                case "educationForm":
                    obj.setEducationForm((EcfCatalogItem) value);
                    return;
                case "educationSource":
                    obj.setEducationSource((EcfCatalogItem) value);
                    return;
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "uid":
                    obj.setUid((String) value);
                    return;
                case "size":
                    obj.setSize((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "educationLevel":
                        return true;
                case "educationForm":
                        return true;
                case "educationSource":
                        return true;
                case "course":
                        return true;
                case "uid":
                        return true;
                case "size":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "educationLevel":
                    return true;
                case "educationForm":
                    return true;
                case "educationSource":
                    return true;
                case "course":
                    return true;
                case "uid":
                    return true;
                case "size":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "educationLevel":
                    return EcfCatalogItem.class;
                case "educationForm":
                    return EcfCatalogItem.class;
                case "educationSource":
                    return EcfCatalogItem.class;
                case "course":
                    return Course.class;
                case "uid":
                    return String.class;
                case "size":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EcfSettingsECPeriodKey> _dslPath = new Path<EcfSettingsECPeriodKey>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EcfSettingsECPeriodKey");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodKey#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Уровень образования ФИС. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodKey#getEducationLevel()
     */
    public static EcfCatalogItem.Path<EcfCatalogItem> educationLevel()
    {
        return _dslPath.educationLevel();
    }

    /**
     * @return Форма обучения ФИС. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodKey#getEducationForm()
     */
    public static EcfCatalogItem.Path<EcfCatalogItem> educationForm()
    {
        return _dslPath.educationForm();
    }

    /**
     * @return Источник финансирования ФИС. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodKey#getEducationSource()
     */
    public static EcfCatalogItem.Path<EcfCatalogItem> educationSource()
    {
        return _dslPath.educationSource();
    }

    /**
     * @return Курс (на который осуществляется прием). Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodKey#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * Формируется на основе полей ключа
     *
     * @return Уникальный идентификатор (для выгрузки в ФИС). Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodKey#getUid()
     */
    public static PropertyPath<String> uid()
    {
        return _dslPath.uid();
    }

    /**
     * 0 - нет приема
     * 1 - прием без этапов (1 единственный этап)
     * 2,3,... - число этапов
     *
     * @return Количество этапов. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodKey#getSize()
     */
    public static PropertyPath<Integer> size()
    {
        return _dslPath.size();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodKey#getSizeTitle()
     */
    public static SupportedPropertyPath<String> sizeTitle()
    {
        return _dslPath.sizeTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodKey#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EcfSettingsECPeriodKey> extends EntityPath<E>
    {
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private EcfCatalogItem.Path<EcfCatalogItem> _educationLevel;
        private EcfCatalogItem.Path<EcfCatalogItem> _educationForm;
        private EcfCatalogItem.Path<EcfCatalogItem> _educationSource;
        private Course.Path<Course> _course;
        private PropertyPath<String> _uid;
        private PropertyPath<Integer> _size;
        private SupportedPropertyPath<String> _sizeTitle;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodKey#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Уровень образования ФИС. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodKey#getEducationLevel()
     */
        public EcfCatalogItem.Path<EcfCatalogItem> educationLevel()
        {
            if(_educationLevel == null )
                _educationLevel = new EcfCatalogItem.Path<EcfCatalogItem>(L_EDUCATION_LEVEL, this);
            return _educationLevel;
        }

    /**
     * @return Форма обучения ФИС. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodKey#getEducationForm()
     */
        public EcfCatalogItem.Path<EcfCatalogItem> educationForm()
        {
            if(_educationForm == null )
                _educationForm = new EcfCatalogItem.Path<EcfCatalogItem>(L_EDUCATION_FORM, this);
            return _educationForm;
        }

    /**
     * @return Источник финансирования ФИС. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodKey#getEducationSource()
     */
        public EcfCatalogItem.Path<EcfCatalogItem> educationSource()
        {
            if(_educationSource == null )
                _educationSource = new EcfCatalogItem.Path<EcfCatalogItem>(L_EDUCATION_SOURCE, this);
            return _educationSource;
        }

    /**
     * @return Курс (на который осуществляется прием). Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodKey#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * Формируется на основе полей ключа
     *
     * @return Уникальный идентификатор (для выгрузки в ФИС). Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodKey#getUid()
     */
        public PropertyPath<String> uid()
        {
            if(_uid == null )
                _uid = new PropertyPath<String>(EcfSettingsECPeriodKeyGen.P_UID, this);
            return _uid;
        }

    /**
     * 0 - нет приема
     * 1 - прием без этапов (1 единственный этап)
     * 2,3,... - число этапов
     *
     * @return Количество этапов. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodKey#getSize()
     */
        public PropertyPath<Integer> size()
        {
            if(_size == null )
                _size = new PropertyPath<Integer>(EcfSettingsECPeriodKeyGen.P_SIZE, this);
            return _size;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodKey#getSizeTitle()
     */
        public SupportedPropertyPath<String> sizeTitle()
        {
            if(_sizeTitle == null )
                _sizeTitle = new SupportedPropertyPath<String>(EcfSettingsECPeriodKeyGen.P_SIZE_TITLE, this);
            return _sizeTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodKey#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(EcfSettingsECPeriodKeyGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EcfSettingsECPeriodKey.class;
        }

        public String getEntityName()
        {
            return "ecfSettingsECPeriodKey";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getSizeTitle();

    public abstract String getTitle();
}
