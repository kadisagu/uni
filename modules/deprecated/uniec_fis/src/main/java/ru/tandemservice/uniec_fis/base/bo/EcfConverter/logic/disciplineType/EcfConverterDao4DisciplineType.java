/**
 *$Id$
 */
package ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.disciplineType;

import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineType;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.EcfConverterStaticDao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Alexander Shaburov
 * @since 09.01.13
 */
public class EcfConverterDao4DisciplineType extends EcfConverterStaticDao<EntranceDisciplineType, String> implements IEcfConverterDao4DisciplineType
{
    /* { uni.code, uni.title, fis.code, fis.title } */
    private static final String[][] DATA = {
        { "1", "Обычное",                                            "1", "Вступительные испытания"},
        { "2", "Профильной направленности",                          "3", "Вступительные испытания профильной направленности"},
        { "3", "Творческой и (или) профессиональной направленности", "2", "Вступительные испытания творческой и (или) профессиональной направленности"}
    };

    /* проверка того, что в ФИС ничего не поменялось { fis.code -> fis.title } */
    private static final Map<String, String> STATIC_TITLE_CHECK_MAP = new HashMap<String, String>();

    /* сопоставление элементов { uni.code -> fis.code }*/
    private static final Map<String, String> STATIC_ITEM_CODE_MAP = new HashMap<String, String>();

    static
    {
        for (final String[] row: DATA)
        {
            if (null != row[2])
            {
                if (null != STATIC_ITEM_CODE_MAP.put(row[0], row[2]))
                    throw new IllegalStateException("STATIC_ITEM_CODE_MAP: Duplicate value for key=«"+row[0]+"»");

                final String prev = STATIC_TITLE_CHECK_MAP.put(row[2], row[3]);
                if (null != prev && !prev.equals(row[3]))
                    throw new IllegalStateException("COUNTRY_CHECK_MAP: Duplicate value for key=«"+row[2]+"»");
            }
        }
    }

    @Override
    public String getEcfCatalogCode()
    {
        return "11";
    }

    @Override
    protected List<EntranceDisciplineType> getEntityList()
    {
        return getCatalogItemList(EntranceDisciplineType.class);
    }

    @Override
    protected String getItemMapKey(EntranceDisciplineType entity)
    {
        return entity.getCode();
    }

    @Override
    protected Map<String, String> getStaticTitleCheckMap()
    {
        return STATIC_TITLE_CHECK_MAP;
    }

    @Override
    protected Map<String, String> getStaticItemCodeMap()
    {
        return STATIC_ITEM_CODE_MAP;
    }
}
