/* $Id: DppOrgUnitAddEditUI.java 22487 2012-04-04 13:16:00Z vzhukov $ */
package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.ui.AddPkg4Compaign;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.process.*;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.hibsupport.transaction.sync.RuntimeTimeoutException;
import ru.tandemservice.uni.util.BackgroundProcessHolder;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.EcfOrgUnitManager;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.logic.EcfOrgUnitPackageDao;

import java.util.List;

@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "holder.id")
})
public class EcfOrgUnitAddPkg4CompaignUI extends UIPresenter
{
    private final EntityHolder<EcfOrgUnit> holder = new EntityHolder<EcfOrgUnit>();

    public EntityHolder<EcfOrgUnit> getHolder() { return this.holder; }
    public EcfOrgUnit getEcfOrgUnit() { return this.getHolder().getValue(); }

    private List<EnrollmentCampaign> enrollmentCampaignList;
    public List<EnrollmentCampaign> getEnrollmentCampaignList() { return this.enrollmentCampaignList; }
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList) { this.enrollmentCampaignList = enrollmentCampaignList; }

    private EnrollmentCampaign enrollmentCampaign;
    public EnrollmentCampaign getEnrollmentCampaign() { return this.enrollmentCampaign; }
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign) { this.enrollmentCampaign = enrollmentCampaign; }

    private List<EcfCatalogItem> statusList;
    public List<EcfCatalogItem> getStatusList() { return this.statusList; }
    public void setStatusList(List<EcfCatalogItem> statusList) { this.statusList = statusList; }

    private EcfCatalogItem status;
    public EcfCatalogItem getStatus() { return this.status; }
    public void setStatus(EcfCatalogItem status) { this.status = status; }

    @Override
    public void onComponentRefresh() {
        final EcfOrgUnit ecfOrgUnit = this.getHolder().refresh();

        setStatusList(DataAccessServices.dao().getList(EcfCatalogItem.class, EcfCatalogItem.fisCatalogCode(), "34", EcfCatalogItem.fisItemCode().s()));

        final List<EnrollmentCampaign> enrollmentCampaignList = EcfOrgUnitManager.instance().dao().getEnrollmentCampaignList(ecfOrgUnit);
        setEnrollmentCampaignList(enrollmentCampaignList);
        if (null == getEnrollmentCampaign() && !enrollmentCampaignList.isEmpty()) {
            //            setEnrollmentCampaign(
            //                CollectionUtils.find(enrollmentCampaignList, new Predicate<EnrollmentCampaign>() {
            //                    @Override public boolean evaluate(EnrollmentCampaign ec) {
            //                        return Boolean.TRUE.equals(ec.getEducationYear().getCurrent());
            //                    }
            //                })
            //            );
            setEnrollmentCampaign(enrollmentCampaignList.get(enrollmentCampaignList.size()-1));
        }
    }

    public void onClickApply()
    {
        final IBackgroundProcess process = new BackgroundProcessBase()
        {
            @Override
            public ProcessResult run(ProcessState state)
            {
                state.setCurrentValue(1);

                try
                {
                    EcfOrgUnitPackageDao.setupProcessState(state);
                    EcfOrgUnitManager.instance().pkgCompaignDao().doCreatePackage(getEcfOrgUnit(), getEnrollmentCampaign(), getStatus());
                }
                catch (RuntimeTimeoutException e)
                {
                    throw new ApplicationException("Формирование пакетов уже запущено.");
                }
                catch (Exception e)
                {
                    //TODO когда будет логирование ошибок, нужно выводить общие слова
                    if (e instanceof ApplicationException)
                        return new ProcessResult(e);
                    else
                        throw e;
                }

                if (UserContext.getInstance().getErrorCollector().hasErrors())
                {
                    return new ProcessResult("Пакет «Информация о приемных кампаниях» не удалось сформировать.", true);
                }
                state.setCurrentValue(100);

                return new ProcessResult("Пакет «Информация о приемных кампаниях» успешно сформирован.");
            }
        };

        new BackgroundProcessHolder().start("Формирование пакета «Информация о приемных кампаниях»", process, ProcessDisplayMode.percent);
    }
}
