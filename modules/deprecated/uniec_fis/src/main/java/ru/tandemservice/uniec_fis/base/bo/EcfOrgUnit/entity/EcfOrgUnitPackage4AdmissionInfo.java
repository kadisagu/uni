package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity;

import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.gen.EcfOrgUnitPackage4AdmissionInfoGen;

/**
 * Пакет: Сведения об объеме и структуре приема
 *
 * Root / PackageData / CampaignInfo / AdmissionInfo
 */
public class EcfOrgUnitPackage4AdmissionInfo extends EcfOrgUnitPackage4AdmissionInfoGen
{
    @Override
    public String getTitle() {
        if (getEnrollmentCampaign() == null) {
            return this.getClass().getSimpleName();
        }
        return "Пакет 2: Сведения об объеме и структуре приема «" + getEnrollmentCampaign().getTitle() + "»";
    }
}