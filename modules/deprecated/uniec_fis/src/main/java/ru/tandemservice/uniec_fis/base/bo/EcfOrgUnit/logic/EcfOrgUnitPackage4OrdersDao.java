/**
 * $Id$
 */
package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.logic;

import com.google.common.collect.Lists;
import fis.schema.pkg.send.req.PackageData;
import fis.schema.pkg.send.req.PackageData.Orders.OrdersOfAdmission;
import fis.schema.pkg.send.req.PackageData.Orders.OrdersOfException;
import fis.schema.pkg.send.req.Root;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.orders.*;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.gen.EcfCatalogItemGen;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.EcfConverterManager;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.IEcfConverter;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.benefit.IEcfBenefitDao;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.ecg.IEcfEcgDao;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.eduOu.IEcfConverterDao4EduOu;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.finSources.IEcfFinSourceDao;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.EcfOrgUnitManager;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage4AdmissionOrders;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage4ApplicationsInfo;
import ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodItem;
import ru.tandemservice.uniec_fis.util.FisServiceImpl;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 15.01.13
 */
public class EcfOrgUnitPackage4OrdersDao extends EcfOrgUnitPackageDao implements IEcfOrgUnitPackage4OrdersDao
{
    @Override
    public EcfOrgUnitPackage4AdmissionOrders doCreatePackage(EcfOrgUnit ecfOrgUnit, EnrollmentCampaign enrollmentCampaign)
    {
        final EcfOrgUnitPackage4AdmissionOrders dbo = new EcfOrgUnitPackage4AdmissionOrders();
        dbo.setCreationDate(new Date());
        dbo.setEcfOrgUnit(ecfOrgUnit);
        dbo.setEnrollmentCampaign(enrollmentCampaign);
        dbo.setXmlPackageRequest(this.generateXmlPackageRequest(dbo));
        this.saveOrUpdate(dbo);
        return dbo;
    }

    protected byte[] generateXmlPackageRequest(final EcfOrgUnitPackage4AdmissionOrders dbo)
    {
        final EnrollmentCampaign enrollmentCampaign = dbo.getEnrollmentCampaign();
        final EcfOrgUnit ecfOrgUnit = dbo.getEcfOrgUnit();

        // третий пакет должен быть выгружен
        {
            final List<EcfOrgUnitPackage4ApplicationsInfo> packages3 = new DQLSelectBuilder().fromEntity(EcfOrgUnitPackage4ApplicationsInfo.class, "x").column(property("x"))
                    .where(eq(property(EcfOrgUnitPackage4ApplicationsInfo.enrollmentCampaign().fromAlias("x")), value(enrollmentCampaign)))
                    .where(eq(property(EcfOrgUnitPackage4ApplicationsInfo.ecfOrgUnit().fromAlias("x")), value(ecfOrgUnit)))
                    .createStatement(this.getSession()).list();
            if (packages3.isEmpty())
                throw new ApplicationException(EcfOrgUnitManager.instance().getProperty("error.admissionOrders.no-applications-pkg", enrollmentCampaign.getTitle()));
        }

        PackageData.Orders orders = new PackageData.Orders();

        final List<EnrollmentDirection> directions = EcfOrgUnitManager.instance().dao().getEnrollmentDirections(ecfOrgUnit, enrollmentCampaign);
        setProcessState(10);

        final List<EnrollmentExtract> admissionExtracts = getEnrollmentExtractList(directions);

        //приказ с первым попавшимся выбраным направлением приема
        Map<EnrollmentOrder, RequestedEnrollmentDirection> admissionOrders = admissionExtracts.stream()
                .collect(Collectors.toMap(AbstractEntrantExtract::getOrder,
                                          extract -> extract.getEntity().getRequestedEnrollmentDirection(),
                                          (e1, e2) -> e1));
        setProcessState(15);
        addOrdersOfAdmission(orders, enrollmentCampaign, ecfOrgUnit, admissionOrders);

        List<EnrollmentRevertExtract> exceptionExtracts = getEnrollmentRevertExtractList(directions);
        List<EnrollmentOrder> exceptionOrders = exceptionExtracts.stream()
                .map(EnrollmentRevertExtract::getOrder)
                .distinct()
                .collect(Collectors.toList());
        addOrdersOfException(orders, enrollmentCampaign, ecfOrgUnit, exceptionOrders);
        setProcessState(70);

        orders.setApplications(getApplications(ecfOrgUnit, enrollmentCampaign, admissionExtracts, exceptionExtracts));

        // obtain new root
        final Root root = this.newRoot(dbo.getEcfOrgUnit());
        root.getPackageData().setOrders(orders);
        return FisServiceImpl.toXml(root);
    }


    protected List<EnrollmentExtract> getEnrollmentExtractList(List<EnrollmentDirection> directions)
    {
        final List<EnrollmentExtract> extractList = new LinkedList<>();
        BatchUtils.execute(directions, DQL.MAX_VALUES_ROW_NUMBER, elements -> extractList.addAll(
                new DQLSelectBuilder()
                        .fromEntity(EnrollmentExtract.class, "e")
                        .column(property("e"))
                        .where(in(property(EnrollmentExtract.entity().requestedEnrollmentDirection().enrollmentDirection().fromAlias("e")), elements))
                        .where(isNotNull(property(EnrollmentExtract.paragraph().fromAlias("e"))))
                        .createStatement(getSession()).<EnrollmentExtract>list()
        ));

        return extractList;
    }

    protected void addOrdersOfAdmission(PackageData.Orders orders, EnrollmentCampaign enrollmentCampaign, EcfOrgUnit ecfOrgUnit,
                                        Map<EnrollmentOrder, RequestedEnrollmentDirection> admissionOrders)
    {
        if(admissionOrders.isEmpty())
            return;

        final IEcfConverterDao4EduOu converterDao4EduOu = EcfConverterManager.instance().educationOrgUnit();
        final IEcfConverter<DevelopForm> developFormEcfConverter = EcfConverterManager.instance().educationForm().getConverter();
        final IEcfFinSourceDao finSourceDao = EcfConverterManager.instance().financingSource();
        final IEcfBenefitDao benefitDao = EcfConverterManager.instance().benefit();

        final int[] index = {0};
        List<OrdersOfAdmission.OrderOfAdmission> orderOfAdmissionList = admissionOrders.entrySet().stream()
                .map(entry -> {
                    AbstractEntrantOrder order = entry.getKey();
                    final Date orderDate = order.getCommitDate();
                    if (orderDate == null)
                        throw new ApplicationException(EcfOrgUnitManager.instance().getProperty("error.admissionOrders.no-commitDate-enrollmentExtract", order.getNumber()));

                    RequestedEnrollmentDirection requestedDirection = entry.getValue();
                    EnrollmentDirection direction = requestedDirection.getEnrollmentDirection();

                    final OrdersOfAdmission.OrderOfAdmission orderOfAdmission = new OrdersOfAdmission.OrderOfAdmission();
                    orderOfAdmission.setEducationFormID(developFormEcfConverter.getEcfCatalogItem(direction.getEducationOrgUnit().getDevelopForm(), true).getFisID());
                    orderOfAdmission.setEducationLevelID(converterDao4EduOu.getEcfItem4EducationLevel(direction.getEducationOrgUnit()).getFisID());
                    orderOfAdmission.setOrderName("О зачислении на 1 курс");
                    orderOfAdmission.setOrderDate(xmlDate(orderDate));
                    orderOfAdmission.setOrderNumber(order.getNumber());
                    orderOfAdmission.setOrderOfAdmissionUID(Long.toHexString(order.getId()));
                    orderOfAdmission.setCampaignUID(uid(ecfOrgUnit, enrollmentCampaign));

                    final Integer benefitCode = benefitDao.getBenefitCode4RequestedEnrollmentDirection(requestedDirection);
                    boolean isBeneficiary = benefitCode != null && (benefitCode.equals(1) || benefitCode.equals(2));

                    if (isBeneficiary)
                        orderOfAdmission.setFinanceSourceID(this.<EcfCatalogItem>getByNaturalId(
                                new EcfCatalogItemGen.NaturalId(IEcfFinSourceDao.CATALOG_CODE, IEcfFinSourceDao.QUOTE_CODE)).getFisID());
                    else
                        orderOfAdmission.setFinanceSourceID(finSourceDao.getSource(requestedDirection).getFisID());

                    final Integer periodStage = new DQLSelectBuilder()
                            .fromEntity(EcfSettingsECPeriodItem.class, "p")
                            .column(property(EcfSettingsECPeriodItem.stage().fromAlias("p")))
                            .where(eq(property(EcfSettingsECPeriodItem.key().enrollmentCampaign().fromAlias("p")), value(enrollmentCampaign)))
                            .where(eq(property(EcfSettingsECPeriodItem.key().course().intValue().fromAlias("p")), value(1)))
                            .where(eq(property(EcfSettingsECPeriodItem.key().educationForm().fisItemCode().fromAlias("p")), value(String.valueOf(orderOfAdmission.getEducationFormID()))))
                            .where(eq(property(EcfSettingsECPeriodItem.key().educationLevel().fisItemCode().fromAlias("p")), value(String.valueOf(orderOfAdmission.getEducationLevelID()))))
                            .where(eq(property(EcfSettingsECPeriodItem.key().educationSource().fisItemCode().fromAlias("p")), value(String.valueOf(orderOfAdmission.getFinanceSourceID()))))
                            .where(gt(property(EcfSettingsECPeriodItem.key().size().fromAlias("p")), value(1)))
                            .where(inDay(EcfSettingsECPeriodItem.dateOrder().fromAlias("p"), orderDate))
                            .createStatement(getSession()).uniqueResult();

                    if (periodStage != null)
                        orderOfAdmission.setStage(periodStage.longValue());

                    setProcessState(60, index[0], admissionOrders.size());

                    return orderOfAdmission;
                })
                .collect(Collectors.toList());

        if (CollectionUtils.isNotEmpty(orderOfAdmissionList))
        {
            final OrdersOfAdmission ordersOfAdmission = new OrdersOfAdmission();
            ordersOfAdmission.getOrderOfAdmission().addAll(orderOfAdmissionList);
            orders.setOrdersOfAdmission(ordersOfAdmission);
        }
    }


    protected List<EnrollmentRevertExtract> getEnrollmentRevertExtractList(List<EnrollmentDirection> directions)
    {
        final List<EnrollmentRevertExtract> extractList = new ArrayList<>();
        for(List<EnrollmentDirection> part: Lists.partition(directions, DQL.MAX_VALUES_ROW_NUMBER))
        {
            extractList.addAll(
                    new DQLSelectBuilder()
                            .fromEntity(EnrollmentRevertExtract.class, "e")
                            .column(property("e"))
                            .where(in(property(EnrollmentRevertExtract.entity().enrollmentDirection().fromAlias("e")), part))
                            .where(isNotNull(property(EnrollmentExtract.paragraph().fromAlias("e"))))
                            .createStatement(getSession()).<EnrollmentRevertExtract>list()
            );
        }

        return extractList;
    }

    protected void addOrdersOfException(PackageData.Orders orders, EnrollmentCampaign enrollmentCampaign, EcfOrgUnit ecfOrgUnit,
                                        List<EnrollmentOrder> exceptionOrders)
    {
        if(CollectionUtils.isEmpty(exceptionOrders))
            return;

        String campaignUid = uid(ecfOrgUnit, enrollmentCampaign);
        List<OrdersOfException.OrderOfException> orderOfExceptionList = exceptionOrders.stream()
                .map(order -> {
                    OrdersOfException.OrderOfException orderOfException = new OrdersOfException.OrderOfException();
                    orderOfException.setOrderOfExceptionUID(Long.toHexString(order.getId()));
                    orderOfException.setCampaignUID(campaignUid);
                    orderOfException.setOrderDate(xmlDate(order.getCommitDate()));
                    orderOfException.setOrderName(order.getTitle());
                    orderOfException.setOrderNumber(order.getNumber());
                    return orderOfException;
                })
                .collect(Collectors.toList());


        if (CollectionUtils.isNotEmpty(orderOfExceptionList))
        {
            OrdersOfException ordersOfException = new OrdersOfException();
            ordersOfException.getOrderOfException().addAll(orderOfExceptionList);
            orders.setOrdersOfException(ordersOfException);
        }
    }

    protected PackageData.Orders.Applications getApplications(EcfOrgUnit ecfOrgUnit, EnrollmentCampaign enrollmentCampaign,
                                                              List<EnrollmentExtract> admissionExtracts,
                                                              List<EnrollmentRevertExtract> exceptionExtracts)
    {
        PackageData.Orders.Applications applications = new PackageData.Orders.Applications();

        final IEcfEcgDao ecgDao = EcfConverterManager.instance().ecg();
        final IEcfFinSourceDao finSourceDao = EcfConverterManager.instance().financingSource();
        final IEcfBenefitDao benefitDao = EcfConverterManager.instance().benefit();

        Map<EnrollmentDirection, Map<StudentCategory, String>> competitionGroupUidMap = getCompetitionGroupUidMap(ecfOrgUnit, enrollmentCampaign);
        for (final EnrollmentExtract extract : admissionExtracts)
        {
            PackageData.Orders.Applications.Application application = getApplication(ecfOrgUnit, ecgDao, finSourceDao, benefitDao,
                                                                                     extract.getEntity().getRequestedEnrollmentDirection(),
                                                                                     extract.getOrder(), competitionGroupUidMap);

            applications.getApplication().add(application);
            setProcessState(85, extract, admissionExtracts);
        }

        for (final EnrollmentRevertExtract extract : exceptionExtracts)
        {
            PackageData.Orders.Applications.Application application = getApplication(ecfOrgUnit, ecgDao, finSourceDao, benefitDao,
                                                                                     extract.getEntity(), extract.getOrder(), competitionGroupUidMap);

            applications.getApplication().add(application);
            setProcessState(90, extract, exceptionExtracts);
        }

        return applications;
    }

    private PackageData.Orders.Applications.Application getApplication(EcfOrgUnit ecfOrgUnit,
                                                                       IEcfEcgDao ecgDao, IEcfFinSourceDao finSourceDao, IEcfBenefitDao benefitDao,
                                                                       RequestedEnrollmentDirection requestedDirection,
                                                                       EnrollmentOrder order,
                                                                       Map<EnrollmentDirection, Map<StudentCategory, String>> competitionGroupUidMap)
    {
        IEcfEcgDao.IEcfEcgKey ecgKey;
        try
        {
            ecgKey = ecgDao.getKey(requestedDirection.getEnrollmentDirection(), requestedDirection.getStudentCategory());
            if (ecgKey == null)
                throw new ApplicationException(""); // хз что произошло (ниже будет написаны общие слова)
        }
        catch (final ApplicationException e)
        {
            final String message = EcfOrgUnitManager.instance().getProperty(
                    "error.applications.no-ecg-key",
                    requestedDirection.getEntrantRequest().getEntrant().getTitle(),
                    requestedDirection.getEntrantRequest().getStringNumber(),
                    requestedDirection.getEnrollmentDirection().getTitle()
            ) + (StringUtils.isBlank(e.getMessage()) ? "" : (": " + e.getMessage()));

            if (Debug.isDisplay())
            {
                ContextLocal.getInfoCollector().add(message);
                return null;
            }
            else
                throw new ApplicationException(message, e);
        }

        EnrollmentCampaign enrollmentCampaign = requestedDirection.getEnrollmentDirection().getEnrollmentCampaign();
        EntrantRequest request = requestedDirection.getEntrantRequest();

        PackageData.Orders.Applications.Application application = new PackageData.Orders.Applications.Application();

        String competitionGroupUid = getCompetitionGroupUid(ecfOrgUnit, competitionGroupUidMap, requestedDirection)
                + "-" + finSourceDao.getSource(requestedDirection).getFisItemCode()
                + getCrimeaPostfix(request.getEntrant().isFromCrimea());

        if(requestedDirection.getId().equals(1474930672402495117L))
            System.out.println("sfg");

        application.setApplicationUID(uid(ecfOrgUnit, enrollmentCampaign, request, ecgKey));
        if (requestedDirection.getCompensationType().isBudget())
            application.setOrderIdLevelBudget(1L);

        application.setOrderUID(Long.toHexString(order.getId()));
        application.setOrderTypeID(order.getType().getCode().equals("revert") ? 2L : 1L);
        application.setCompetitiveGroupUID(competitionGroupUid);

        return application;
    }
}
