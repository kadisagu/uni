package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

/**
 * @author vdanilov
 */
public interface IEcfOrgUnitPackageDao extends INeedPersistenceSupport {

    /**
     * Отправляет пакет в очередь (все пакеты, которе были созданы до указанного, но не находящиеся в очереди, будут заархивированы)
     * @param packageId
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doQueue(Long packageId);

    /**
     * Выставляет флаг архивности
     * @param packageId
     * @param archiveFlag
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doSwitchArchive(Long packageId, boolean archiveFlag);

    /**
     * Удаляет пакет (если пакет архивный)
     * @param packageId
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doDeletePackage(Long packageId);

    /**
     * @param packageId
     * @return true, если пакет является первый, который можно поместить в очедеь: нет пакетов, созданных раньше текущего, которые можно поместить в очередь
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    boolean isFirstUnqueuedPackage(Long packageId);

}
