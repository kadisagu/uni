/* $Id$ */
package ru.tandemservice.uniec_fis.base.bo.EcfConverter.ui.Config4IndAchievement;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniec.entity.catalog.IndividualProgress;
import ru.tandemservice.uniec.entity.entrant.EnrCampaignEntrantIndividualProgress;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.EcfConverterManager;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.indAchievement.IEcfConverterDao4IndAchievement;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.ui.EcfConverterConfig;

import java.util.LinkedList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * @author Ekaterina Zvereva
 * @since 04.06.2015
 */
@Configuration
public class EcfConverterConfig4IndAchievement extends EcfConverterConfig
{
    public static final String ENROLLMENT_CAMPAIGN_DS = "enrollmentCampaignDS";
    public static final String PROPERTY_ECF_VALUE = "value";

    public static final String ECF_CONVERTER_RECORD_DS = "ecfConverterRecordDS";
    public static final String ECF_CONVERTER_VALUE_DS = "ecfConverterValueDS";

    @Override
    protected IEcfConverterDao4IndAchievement getConverterDao() {
        return EcfConverterManager.instance().individualAchievement();
    }

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint() {
        return super.presenterExtPoint(
                presenterExtPointBuilder()
                        .addDataSource(selectDS(ENROLLMENT_CAMPAIGN_DS, enrollmentCampaignDSHandler()))
        );
    }

    @Bean
    @Override
    public ColumnListExtPoint ecfConverterRecordDS() {
        return this.columnListExtPointBuilder(ECF_CONVERTER_RECORD_DS)
                .addColumn(textColumn("title", IndividualProgress.P_TITLE).create())
                .addColumn(blockColumn("value", "valueBlock").width("460px").create())
                .addColumn(blockColumn("action", "actionBlock").width("1px").hasBlockHeader(true).create())
                .create();
    }

    @Override
    protected List<IEntity> filterConverterRecordDS(final List<IEntity> entityList, final ExecutionContext context) {
        final EnrollmentCampaign enrollmentCampaign = context.get(EcfConverterConfig4IndAchievementUI.PARAM_ENROLLMENT_CAMPAIGN);

        // filter EnrollmentCampaign
        if (enrollmentCampaign != null)
        {
            DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(EnrCampaignEntrantIndividualProgress.class, "enPr")
                    .column(property("enPr", EnrCampaignEntrantIndividualProgress.individualProgress()))
                    .where(eq(property("enPr", EnrCampaignEntrantIndividualProgress.enrollmentCampaign()), value(enrollmentCampaign)));

            List<IndividualProgress> usedList = DataAccessServices.dao().getList(subBuilder);
            for (IEntity achievement : new LinkedList<>(entityList))
            {
                if (achievement instanceof IndividualProgress && !(usedList.contains(achievement)))
                    entityList.remove(achievement);
            }
        }

        else
        {
            return new LinkedList<>();
        }

        return entityList;
    }

    @Bean
    @Override
    public IReadAggregateHandler<DSInput, DSOutput> ecfConverterRecordDSHandler() {
        return super.ecfConverterRecordDSHandler();
    }

    @Bean
    @Override
    public IReadAggregateHandler<DSInput, DSOutput> ecfConverterValueDSHandler() {
        return super.ecfConverterValueDSHandler();
    }


    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> enrollmentCampaignDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrollmentCampaign.class)
                .order(EnrollmentCampaign.educationYear().intValue(), OrderDirection.desc)
                .filter(EnrollmentCampaign.title())
                .pageable(true);
    }

}