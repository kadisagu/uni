package ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.eduOu;

import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.codes.DevelopConditionCodes;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.gen.EcfCatalogItemGen;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4EduOu;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.gen.EcfConv4EduOuGen;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.EcfConverterBaseDao;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.IEcfConverter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author vdanilov
 */
public class EcfConverterDao4EduOu extends EcfConverterBaseDao<EducationOrgUnit, Long> implements IEcfConverterDao4EduOu {

    @Override
    public String getEcfCatalogCode() {
        return "10";
    }

    @Override
    public List<EducationOrgUnit> getEntityList() {
        // все направления министерства, для которых есть направления ВУЗа
        return new DQLSelectBuilder()
                .fromEntity(EducationOrgUnit.class, "eduOu")
                .column(property("eduOu"))
                .where(or(
                        in(property("eduOu.id"),
                                new DQLSelectBuilder()
                                        .fromEntity(EnrollmentDirection.class, "ed")
                                        .column(property(EnrollmentDirection.educationOrgUnit().id().fromAlias("ed")))
                                        .buildQuery()),
                        in(property("eduOu.id"),
                                new DQLSelectBuilder().fromEntity(EcfConv4EduOu.class, "c")
                                        .column(property(EcfConv4EduOu.educationOrgUnit().id().fromAlias("c")))
                                        .buildQuery())
                        ))
                .order(property(EducationOrgUnit.educationLevelHighSchool().displayableTitle().fromAlias("eduOu")))
                .order(property(EducationOrgUnit.formativeOrgUnit().title().fromAlias("eduOu")))
                .order(property(EducationOrgUnit.territorialOrgUnit().title().fromAlias("eduOu")))
                .order(property(EducationOrgUnit.developForm().code().fromAlias("eduOu")))
                .order(property(EducationOrgUnit.developCondition().code().fromAlias("eduOu")))
                .order(property(EducationOrgUnit.developTech().code().fromAlias("eduOu")))
                .order(property(EducationOrgUnit.developPeriod().code().fromAlias("eduOu")))
                .createStatement(this.getSession()).list();
    }

    @Override
    protected Long getItemMapKey(final EducationOrgUnit entity) {
        return entity.getId();
    }

    @Override
    protected Map<Long, Long> getItemMap() {
        return EcfConverterBaseDao.<Long, Long>map(scrollRows(
            new DQLSelectBuilder()
            .fromEntity(EcfConv4EduOu.class, "x")
            .column(property(EcfConv4EduOu.educationOrgUnit().id().fromAlias("x")), "lvl_id")
            .column(property(EcfConv4EduOu.value().id().fromAlias("x")), "value_id")
            .createStatement(this.getSession())
        ));
    }

    @Override
    public void update(final Map<EducationOrgUnit, EcfCatalogItem> values, final boolean clearNulls) {

        // формируем перечень требуемых строк
        final List<EcfConv4EduOu> targetRecords = new ArrayList<EcfConv4EduOu>(values.size());
        for (final Map.Entry<EducationOrgUnit, EcfCatalogItem> e: values.entrySet()) {
            if (null != e.getValue()) {
                targetRecords.add(new EcfConv4EduOu(e.getKey(), e.getValue()));
            }
        }

        // обновляем данные в базе
        new MergeAction.SessionMergeAction<EcfConv4EduOuGen.NaturalId, EcfConv4EduOu>() {
            @Override protected EcfConv4EduOuGen.NaturalId key(final EcfConv4EduOu source) {
                return (EcfConv4EduOuGen.NaturalId)source.getNaturalId();
            }
            @Override protected EcfConv4EduOu buildRow(final EcfConv4EduOu source) {
                return new EcfConv4EduOu(source.getEducationOrgUnit(), source.getValue());
            }
            @Override protected void fill(final EcfConv4EduOu target, final EcfConv4EduOu source) {
                target.update(source, false);
            }
            @Override protected void doDeleteRecord(final EcfConv4EduOu databaseRecord) {
                if (clearNulls) { super.doDeleteRecord(databaseRecord); }
            }
        }.merge(this.getList(EcfConv4EduOu.class), targetRecords);

    }


    private static final Pattern OKSO_PREFIX_PATTERN = Pattern.compile("([0-9]{6}[.][0-9]{2}) .*");

    @Override
    public void doAutoSync()
    {
        final boolean hard = true; // false использовать только для тестовых целей (чтобы руками не набивать справочник)

        final List<EcfCatalogItem> itemList = this.getList(EcfCatalogItem.class, EcfCatalogItem.fisCatalogCode(), this.getEcfCatalogCode());
        final Map<String, List<EcfCatalogItem>> oksoPrefix2ItemMap = new HashMap<String, List<EcfCatalogItem>>(itemList.size());
        for (final EcfCatalogItem item: itemList) {
            final Matcher matcher = OKSO_PREFIX_PATTERN.matcher(item.getTitle());
            if (matcher.matches()) {
                final String oksoPrefix = matcher.group(1);
                SafeMap.safeGet(oksoPrefix2ItemMap, oksoPrefix, ArrayList.class).add(item);
            }
        }

        final IEcfConverter<EducationOrgUnit> converter = this.getConverter();
        final List<EducationOrgUnit> eduOuList = this.getEntityList();
        final Map<EducationOrgUnit, EcfCatalogItem> values = new HashMap<EducationOrgUnit, EcfCatalogItem>(eduOuList.size());
        for (final EducationOrgUnit eduOu: eduOuList) {
            final Matcher matcher = OKSO_PREFIX_PATTERN.matcher(eduOu.getEducationLevelHighSchool().getDisplayableTitle());
            if (matcher.matches()) {
                final String oksoPrefix = matcher.group(1);
                final EcfCatalogItem current = converter.getEcfCatalogItem(eduOu, false);
                if (null != current) {
                    values.put(eduOu, current);
                } else {
                    final List<EcfCatalogItem> list = oksoPrefix2ItemMap.get(oksoPrefix);
                    if ((null != list) && (hard ? (1 == list.size()) : (list.size() > 0))) {
                        // только если нашли единственное направление
                        values.put(eduOu, list.iterator().next());
                    }
                }
            }
        }

        this.update(values, false);
    }


    @Override
    public EcfCatalogItem getEcfItem4EducationLevel(final EducationOrgUnit eduOu) {
        final String lvlCode = this.getEcfItemCode4EducationLevel(eduOu);
        if (null == lvlCode) { return null; }
        return this.getByNaturalId(new EcfCatalogItemGen.NaturalId("2", lvlCode));
    }

    @Override
    protected List<EcfCatalogItem> getUsedFisItemList()
    {
        return new DQLSelectBuilder().fromEntity(EcfConv4EduOu.class, "b").column(property(EcfConv4EduOu.value().fromAlias("b")))
                .where(isNotNull(property(EcfConv4EduOu.value().fromAlias("b"))))
                .createStatement(getSession()).list();
    }

    private String getEcfItemCode4EducationLevel(final EducationOrgUnit eduOu) {
        /**
            http://wiki.tandemservice.ru/pages/viewpage.action?pageId=9864743
            62 нормативный срок обучения   2
            62 не "нормативный срок обучения" 3
            65 неважно 5
            68 неважно 4
            51, 52 неважно 17
            А не важно 18
         */

        final Qualifications qualification = eduOu.getEducationLevelHighSchool().getEducationLevel().getQualification();
        if (null != qualification) {
            final String qcode = qualification.getCode();
            if (QualificationsCodes.BAKALAVR.equals(qcode)) {
                if (DevelopConditionCodes.FULL_PERIOD.equals(eduOu.getDevelopCondition().getCode())) {
                    return "2";
                }
                return "3";
            } else if (QualificationsCodes.SPETSIALIST.equals(qcode)) {
                return "5";
            } else if (QualificationsCodes.MAGISTR.equals(qcode)) {
                return "4";
            } else if (QualificationsCodes.BAZOVYY_UROVEN_S_P_O.equals(qcode) || QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O.equals(qcode)) {
                return "17";
            } else if (QualificationsCodes.ASPIRANTURA.equals(qcode))
                return "18";
        }

        return null;
    }


}
