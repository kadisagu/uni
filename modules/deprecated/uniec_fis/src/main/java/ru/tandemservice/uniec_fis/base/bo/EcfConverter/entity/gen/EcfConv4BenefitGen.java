package ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.person.catalog.entity.Benefit;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4Benefit;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * ФИС: Сопоставление: Льготы
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EcfConv4BenefitGen extends EntityBase
 implements INaturalIdentifiable<EcfConv4BenefitGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4Benefit";
    public static final String ENTITY_NAME = "ecfConv4Benefit";
    public static final int VERSION_HASH = -1699634057;
    private static IEntityMeta ENTITY_META;

    public static final String L_BENEFIT = "benefit";
    public static final String L_VALUE = "value";

    private Benefit _benefit;     // Льгота
    private EcfCatalogItem _value;     // Значение (элемент справочника ФИС)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Льгота. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public Benefit getBenefit()
    {
        return _benefit;
    }

    /**
     * @param benefit Льгота. Свойство не может быть null и должно быть уникальным.
     */
    public void setBenefit(Benefit benefit)
    {
        dirty(_benefit, benefit);
        _benefit = benefit;
    }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     */
    @NotNull
    public EcfCatalogItem getValue()
    {
        return _value;
    }

    /**
     * @param value Значение (элемент справочника ФИС). Свойство не может быть null.
     */
    public void setValue(EcfCatalogItem value)
    {
        dirty(_value, value);
        _value = value;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EcfConv4BenefitGen)
        {
            if (withNaturalIdProperties)
            {
                setBenefit(((EcfConv4Benefit)another).getBenefit());
            }
            setValue(((EcfConv4Benefit)another).getValue());
        }
    }

    public INaturalId<EcfConv4BenefitGen> getNaturalId()
    {
        return new NaturalId(getBenefit());
    }

    public static class NaturalId extends NaturalIdBase<EcfConv4BenefitGen>
    {
        private static final String PROXY_NAME = "EcfConv4BenefitNaturalProxy";

        private Long _benefit;

        public NaturalId()
        {}

        public NaturalId(Benefit benefit)
        {
            _benefit = ((IEntity) benefit).getId();
        }

        public Long getBenefit()
        {
            return _benefit;
        }

        public void setBenefit(Long benefit)
        {
            _benefit = benefit;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EcfConv4BenefitGen.NaturalId) ) return false;

            EcfConv4BenefitGen.NaturalId that = (NaturalId) o;

            if( !equals(getBenefit(), that.getBenefit()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getBenefit());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getBenefit());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EcfConv4BenefitGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EcfConv4Benefit.class;
        }

        public T newInstance()
        {
            return (T) new EcfConv4Benefit();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "benefit":
                    return obj.getBenefit();
                case "value":
                    return obj.getValue();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "benefit":
                    obj.setBenefit((Benefit) value);
                    return;
                case "value":
                    obj.setValue((EcfCatalogItem) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "benefit":
                        return true;
                case "value":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "benefit":
                    return true;
                case "value":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "benefit":
                    return Benefit.class;
                case "value":
                    return EcfCatalogItem.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EcfConv4Benefit> _dslPath = new Path<EcfConv4Benefit>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EcfConv4Benefit");
    }
            

    /**
     * @return Льгота. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4Benefit#getBenefit()
     */
    public static Benefit.Path<Benefit> benefit()
    {
        return _dslPath.benefit();
    }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4Benefit#getValue()
     */
    public static EcfCatalogItem.Path<EcfCatalogItem> value()
    {
        return _dslPath.value();
    }

    public static class Path<E extends EcfConv4Benefit> extends EntityPath<E>
    {
        private Benefit.Path<Benefit> _benefit;
        private EcfCatalogItem.Path<EcfCatalogItem> _value;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Льгота. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4Benefit#getBenefit()
     */
        public Benefit.Path<Benefit> benefit()
        {
            if(_benefit == null )
                _benefit = new Benefit.Path<Benefit>(L_BENEFIT, this);
            return _benefit;
        }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4Benefit#getValue()
     */
        public EcfCatalogItem.Path<EcfCatalogItem> value()
        {
            if(_value == null )
                _value = new EcfCatalogItem.Path<EcfCatalogItem>(L_VALUE, this);
            return _value;
        }

        public Class getEntityClass()
        {
            return EcfConv4Benefit.class;
        }

        public String getEntityName()
        {
            return "ecfConv4Benefit";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
