/**
 *$Id$
 */
package ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.disciplineType;

import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineType;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.IEcfConverterStaticDao;

/**
 * @author Alexander Shaburov
 * @since 09.01.13
 */
public interface IEcfConverterDao4DisciplineType extends IEcfConverterStaticDao<EntranceDisciplineType>
{
}
