package ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.transaction.DaoCache;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.EcfOrgUnitManager;

import java.util.*;

/**
 * @author vdanilov
 */
public abstract class EcfConverterBaseDao<T extends IEntity, KEY> extends UniBaseDao implements IEcfConverterDao<T> {

    /**
     * @return код справочника ФИС, с которым происходит сопоставление
     */
    @Override
    public abstract String getEcfCatalogCode();

    /**
     * @return список сущностей, которые необходимо сопоставить
     */
    protected abstract List<T> getEntityList();

    /**
     * @param entity
     * @return код (для сущности uni), на базе которого осуществляется сопоставление
     */
    protected abstract KEY getItemMapKey(T entity);

    /**
     * @return { код (для сущности uni) -> ecfCatalogItem.id }
     */
    protected abstract Map<KEY, Long> getItemMap();


    /**
     * 
     * @param rows [ id(key), id(value) ]
     * @return { id(key) -> id(value) }
     */
    @SuppressWarnings("unchecked")
    protected static <K, V> Map<K, V> map(Iterable<Object[]> rows) {
        final Map<K, V> map = new LinkedHashMap<K, V>();
        for (Object[] row: rows) {
            if (null != map.put((K)row[0], (V)row[1])) {
                throw new IllegalStateException("duplicate value for key=«"+row[0]+"»");
            }
        }
        return map;
    }

    protected IEcfConverter<T> buildNewConverter() {
        return new IEcfConverter<T>() {

            final List<T> entityList = EcfConverterBaseDao.this.getEntityList();
            final Map<KEY, Long> itemIdsMap = EcfConverterBaseDao.this.getItemMap();
            final Map<Long, EcfCatalogItem> cacheMap = EcfConverterBaseDao.this.getLoadCacheMap(EcfCatalogItem.class);

            @Override public String getEcfCatalogCode() {  return EcfConverterBaseDao.this.getEcfCatalogCode(); }
            @Override public List<T> getEntityList() { return this.entityList; }
            @Override public EcfCatalogItem getEcfCatalogItem(final T entity, boolean required) {
                final KEY id = EcfConverterBaseDao.this.getItemMapKey(entity);
                final Long itemId = this.itemIdsMap.get(id);
                if (null == itemId) {
                    if (required) { throw new ApplicationException(EcfOrgUnitManager.instance().getProperty("error.no-ecf-catalog-converter-item", getEcfCatalogCode(), entity.getProperty("title")), new NoSuchElementException(String.valueOf(id)));}
                    return null;
                }
                return this.cacheMap.get(itemId);
            }
        };
    }

    @Override
    public IEcfConverter<T> getConverter() {
        final String key = "converter."+this.getClass().getName();
        IEcfConverter<T> converter = DaoCache.get(key);
        if (null == converter) {
            DaoCache.put(key, converter = this.buildNewConverter());
        }
        return converter;
    }


    @Override
    public void update(T key, EcfCatalogItem value) {
        final IEcfConverter<T> converter = this.getConverter();
        final List<T> entityList = this.getEntityList();
        final Map<T, EcfCatalogItem> values = new HashMap<T, EcfCatalogItem>(entityList.size());
        for (final T entity: entityList) {
            final EcfCatalogItem current = converter.getEcfCatalogItem(entity, false);
            if (null != current) {
                values.put(entity, current);
            }
        }
        values.put(key, value);
        update(values, true);
    }

    @Override
    public void update(Map<T, EcfCatalogItem> values, boolean clearNulls) {
        throw new UnsupportedOperationException();
    }


    @Override
    public void validateUsedInUserConverter(Map<Long, String> itemMap)
    {
        for (EcfCatalogItem item : getUsedFisItemList())
        {
            if (!itemMap.containsKey(item.getFisID()))// используемый элемент не пришел в ответе
            {
                UserContext.getInstance().getErrorCollector().add("Невозможно обновить справочники: изменились элементы справочников ФИС, использованные в правилах преобразования данных.");
                throw new IllegalStateException("fisCatalogItem(catalog="+getEcfCatalogCode()+", code="+item.getFisItemCode()+"): used fis catalog item has been deleted");
            }

            final String title = itemMap.get(item.getFisID());
            final String expectedTitle = item.getTitle();
            if (!expectedTitle.equals(title))// у используемого элемента в ответе поменялось название
            {
                UserContext.getInstance().getErrorCollector().add("Невозможно обновить справочники: изменились элементы справочников ФИС, использованные в правилах преобразования данных.");
                throw new IllegalStateException("fisCatalogItem(catalog="+getEcfCatalogCode()+", code="+item.getFisItemCode()+"): used fis catalog item title mismatch: title=«"+title+"», expectedTitle=«"+expectedTitle+"»");
            }
        }
    }

    /**
     * @return спикок элементов справочника ФИС, которые поиспользованы в пользовательских конвертерах
     */
    protected List<EcfCatalogItem> getUsedFisItemList()
    {
        throw new UnsupportedOperationException("Need to be implemented. Don't used for not user converters.");
    }
}
