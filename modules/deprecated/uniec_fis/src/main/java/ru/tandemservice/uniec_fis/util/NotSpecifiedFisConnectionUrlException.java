/**
 *$Id$
 */
package ru.tandemservice.uniec_fis.util;

/**
 * Исключение сигнализирует о том, что не заданы реквизиты сервера ФИС.
 * Пропертя uni.ec.fis.connection.url.
 *
 * @author Alexander Shaburov
 * @since 11.03.13
 */
@SuppressWarnings("serial")
public class NotSpecifiedFisConnectionUrlException extends IllegalStateException
{
    public NotSpecifiedFisConnectionUrlException()
    {

    }

    public NotSpecifiedFisConnectionUrlException(String s)
    {
        super(s);
    }
}
