/**
 *$Id$
 */
package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage4DelAdmissionOrders;

/**
 * @author Alexander Shaburov
 * @since 03.07.13
 */
public interface IEcfOrgUnitPackage4DelAdmissionOrdersDao extends IEcfOrgUnitPackageDao
{
    /**
     * Создает пакет на удаление всех заявлений из приказов.
     * @param ecfOrgUnit акк. ОУ
     * @param enrollmentCampaign ПК
     */
    @Transactional(propagation= Propagation.REQUIRED, readOnly=false)
    EcfOrgUnitPackage4DelAdmissionOrders doCreatePackage(EcfOrgUnit ecfOrgUnit, EnrollmentCampaign enrollmentCampaign);
}
