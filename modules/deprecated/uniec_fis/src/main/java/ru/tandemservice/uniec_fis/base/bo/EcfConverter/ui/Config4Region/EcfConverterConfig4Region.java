/* $Id$ */
package ru.tandemservice.uniec_fis.base.bo.EcfConverter.ui.Config4Region;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.EcfConverterManager;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.IEcfConverterDao;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.ui.EcfConverterConfig;

/**
 * @author Andrey Andreev
 * @since 12.05.2016
 */
@Configuration
public class EcfConverterConfig4Region extends EcfConverterConfig
{

    @Override
    protected IEcfConverterDao getConverterDao()
    {
        return EcfConverterManager.instance().converterDao4Region();
    }

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return super.presenterExtPoint(presenterExtPointBuilder());
    }

    @Bean
    @Override
    public ColumnListExtPoint ecfConverterRecordDS()
    {
        return this.columnListExtPointBuilder(ECF_CONVERTER_RECORD_DS)
                .addColumn(textColumn("region", AddressItem.title().s()).create())
                .addColumn(textColumn("regionCode", AddressItem.inheritedRegionCode().s()).create())
                .addColumn(blockColumn("value", "valueBlock").width("400px").create())
                .addColumn(blockColumn("action", "actionBlock").width("1px").hasBlockHeader(true).create())
                .create();
    }

    @Bean
    @Override
    public IReadAggregateHandler<DSInput, DSOutput> ecfConverterRecordDSHandler()
    {
        return super.ecfConverterRecordDSHandler();
    }

    @Bean
    @Override
    public IReadAggregateHandler<DSInput, DSOutput> ecfConverterValueDSHandler()
    {
        return super.ecfConverterValueDSHandler();
    }
}