package ru.tandemservice.uniec_fis.base.bo.EcfConverter.ui.Config4SetDiscipline;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.settings.SetDiscipline;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.EcfConverterManager;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.discipline.IEcfConverterDao4SetDiscipline;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.ui.EcfConverterConfig;

import java.util.LinkedList;
import java.util.List;

/**
 * @author vdanilov
 */
@Configuration
public class EcfConverterConfig4SetDiscipline extends EcfConverterConfig {

    public static final String ENROLLMENT_CAMPAIGN_DS = "enrollmentCampaignDS";

    @Override
    protected IEcfConverterDao4SetDiscipline getConverterDao() {
        return EcfConverterManager.instance().setDiscipline();
    }

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint() {
        return super.presenterExtPoint(
                presenterExtPointBuilder()
                .addDataSource(selectDS(ENROLLMENT_CAMPAIGN_DS, enrollmentCampaignDSHandler()))
        );
    }

    @Bean
    @Override
    public ColumnListExtPoint ecfConverterRecordDS() {
        return this.columnListExtPointBuilder(ECF_CONVERTER_RECORD_DS)
        .addColumn(textColumn("shortTitle", SetDiscipline.P_SHORT_TITLE).create())
        .addColumn(textColumn("fullTitle", SetDiscipline.P_FULL_TITLE).create())
        .addColumn(blockColumn("value", "valueBlock").width("460px").create())
        .addColumn(blockColumn("action", "actionBlock").width("1px").hasBlockHeader(true).create())
        .create();
    }

    @Bean
    @Override
    public IReadAggregateHandler<DSInput, DSOutput> ecfConverterRecordDSHandler() {
        return super.ecfConverterRecordDSHandler();
    }

    @Override
    protected List<IEntity> filterConverterRecordDS(List<IEntity> entityList, ExecutionContext context)
    {
        final EnrollmentCampaign enrollmentCampaign = context.get(EcfConverterConfig4SetDisciplineUI.PARAM_ENROLLMENT_CAMPAIGN);

        // filter EnrollmentCampaign
        if (enrollmentCampaign != null)
        {
            for (IEntity discipline : new LinkedList<>(entityList))
            {
                if (discipline instanceof SetDiscipline && !((SetDiscipline) discipline).getEnrollmentCampaign().equals(enrollmentCampaign))
                    entityList.remove(discipline);
            }
        }
        else
        {
            return new LinkedList<>();
        }

        return entityList;
    }

    @Bean
    @Override
    public IReadAggregateHandler<DSInput, DSOutput> ecfConverterValueDSHandler() {
        return super.ecfConverterValueDSHandler();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> enrollmentCampaignDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrollmentCampaign.class)
                .order(EnrollmentCampaign.educationYear().intValue(), OrderDirection.desc)
                .filter(EnrollmentCampaign.title())
                .pageable(true);
    }
}
