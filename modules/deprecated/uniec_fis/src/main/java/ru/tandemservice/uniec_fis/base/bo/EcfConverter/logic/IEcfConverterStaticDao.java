package ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic;

import org.tandemframework.core.entity.IEntity;

/**
 * @author vdanilov
 */
public interface IEcfConverterStaticDao<T extends IEntity> extends IEcfConverterDao<T> {

    /**
     * проверяет, что title совпадает с сохраненным в коде
     * @return title
     */
    String checkTitle(String code, String title);

}
