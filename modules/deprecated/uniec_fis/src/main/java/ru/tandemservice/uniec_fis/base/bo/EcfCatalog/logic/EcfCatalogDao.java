package ru.tandemservice.uniec_fis.base.bo.EcfCatalog.logic;

import com.google.common.collect.ImmutableMap;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.EcfCatalogManager;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.gen.EcfCatalogItemGen;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit;
import ru.tandemservice.uniec_fis.util.IFisAuthentication;
import ru.tandemservice.uniec_fis.util.IFisService;
import ru.tandemservice.uniec_fis.util.IFisService.FisConnection;
import ru.tandemservice.uniec_fis.util.NotSpecifiedFisConnectionUrlException;

import java.nio.file.Paths;
import java.util.*;
import java.util.Map.Entry;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class EcfCatalogDao extends UniBaseDao implements IEcfCatalogDao
{

    private static final boolean debug = Debug.isEnabled();

    protected static Logger initFileLogger(final Class<?> klass, final String name, final Level threshold) {
        final Logger logger = Logger.getLogger(klass);
        try {
            final FileAppender appender = new FileAppender(new PatternLayout("%d{yyyyMMdd HH:mm:ss,SSS} [%t] %p - %m%n"), Paths.get(System.getProperty("catalina.base"), "logs", name + ".log").toString());
            appender.setName("file-"+name+"-appender");
            appender.setThreshold(threshold);
            logger.addAppender(appender);
            return logger;
        } catch (final Throwable t) {
            logger.error(t.getMessage(), t);
            return null;
        }
    }

    /**
     * TODO: перетащить в общий класс
     * регистрирует для данного DAO логирование в файл
     */
    protected static Logger initFileLogger(final Class<?> klass, final Level threshold) {
        final String name = klass.getName().toLowerCase().replace('.', '-').replace('$', '-');
        return initFileLogger(klass, name, threshold);
    }

    protected final Logger logger = initFileLogger(this.getClass(), "ecfCatalogSync", Level.INFO);


    //////////////////////////////////////////////////////////////////////////////////////////////////////
    // получение данных об элементах справочника

    protected <T, I> Map<Long, String> getFisCatalogItems(final Entry<Long, String> entry, final IFisAuthentication auth, final DictionaryDataHandler<T, I> handler)
    {
        this.logger.info("getFisCatalogItems["+entry.getKey()+"]: begin");
        final Map<Long, String> data = new LinkedHashMap<>();
        try {
            final IFisService service = IFisService.instance.get();

            // готовим запрос
            final fis.schema.catalog_items.req.Root root = new fis.schema.catalog_items.req.Root();
            root.setGetDictionaryContent(new fis.schema.catalog_items.req.GetDictionaryContent());
            root.setAuthData(new fis.schema.catalog_items.req.AuthData());
            root.getGetDictionaryContent().setDictionaryCode(entry.getKey());
            root.getAuthData().setLogin(auth.getLogin());
            root.getAuthData().setPass(auth.getPassword());

            // посылаем запрос, получаем ответ
            final T dictionaryData;
            {
                final FisConnection connection = service.connect("dictionarydetails");
                try {
                    dictionaryData = connection.call(handler.getResponseKlass(), root);
                } catch (Throwable t) {
                    this.logger.error(connection.getRequest());
                    this.logger.error(connection.getResponse());
                    throw t;
                }

                if (debug) {
                    this.logger.info(connection.getRequest());
                    this.logger.info(connection.getResponse());
                }
            }

            // проверяем, что справочник именно тот, который запрашивали (были случаи, когда не совпадало)
            final Long catalogCode = handler.getCatalogCode(dictionaryData);
            if (!entry.getKey().equals(catalogCode)) {
                throw new IllegalStateException("getFisCatalogItems["+entry.getKey()+"]: illegal catalog code «"+catalogCode+"» (req=«"+entry.getKey()+"»)");
            }

            final String catalogName = handler.getCatalogName(dictionaryData);
            if (!entry.getValue().equals(catalogName)) {
                throw new IllegalStateException("getFisCatalogItems["+entry.getKey()+"]: illegal catalog name «"+catalogName+"» (req=«"+entry.getValue()+"»)");
            }

            // предварительный показ справочника
            for (final I item: handler.getItems(dictionaryData)) {
                final Long id = handler.getItemID(item);
                final String name = StringUtils.trimToEmpty(handler.getItemName(item));
                this.logger.info("getFisCatalogItems["+entry.getKey()+"]: ["+id+"]=«"+name+"»");
            }

            // обработка результата
            final Set<Long> ducplicateIdSet = new HashSet<>();
            final Set<String> nameSet = new HashSet<>();
            for (final I item: handler.getItems(dictionaryData)) {
                final Long id = handler.getItemID(item);
                final String name = StringUtils.trimToEmpty(handler.getItemName(item));

                if (!nameSet.add(name)) {
                    ducplicateIdSet.add(id);
                    this.logger.warn("getFisCatalogItems["+entry.getKey()+"]: duplicate item name «"+name+"»");
                }

                final String old = data.put(id, name);
                if (null != old) {
                    final String message = "getFisCatalogItems["+entry.getKey()+"]: duplicate item id «"+id+"»";
                    if (old.equals(name)) {
                        this.logger.warn(message);
                    } else {
                        throw new IllegalStateException(message);
                    }
                }
            }

            // если для элемента есть дубль - то добавляем в него id
            // http://tracker.tandemservice.ru/browse/DEV-478?focusedCommentId=163108&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-163108
            for (Long id: ducplicateIdSet) {
                data.put(id, data.get(id) + ", id="+id);
            }

        } catch (final Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        } finally {
            this.logger.info("getFisCatalogItems["+entry.getKey()+"]: end\n");
        }
        return data;
    }


    protected void sync(final Long catalogCode, final Map<Long, String> items, final boolean ro, final Date now)
    {
        final String catalogCodeAsString = String.valueOf(catalogCode);

        final List<EcfCatalogItem> databaseList = new DQLSelectBuilder()
        .fromEntity(EcfCatalogItem.class, "i").column(property("i"))
        .where(eq(property(EcfCatalogItem.fisCatalogCode().fromAlias("i")), value(catalogCodeAsString)))
        .createStatement(this.getSession()).list();

        final List<EcfCatalogItem> targetList = new ArrayList<>();
        for (final Map.Entry<Long, String> e: items.entrySet()) {
            final EcfCatalogItem item = new EcfCatalogItem();
            item.setFisCatalogCode(String.valueOf(catalogCodeAsString));
            item.setFisItemCode(String.valueOf(e.getKey()));
            item.setTitle(e.getValue());
            targetList.add(item);
        }

        new MergeAction.SessionMergeAction<EcfCatalogItemGen.NaturalId, EcfCatalogItem>() {
            @Override protected EcfCatalogItemGen.NaturalId key(final EcfCatalogItem source) {
                return (EcfCatalogItemGen.NaturalId)source.getNaturalId();
            }
            @Override protected EcfCatalogItem buildRow(final EcfCatalogItem source) {
                if (ro) {
                    // если ro, то нельзя добавять элементы
                    throw new IllegalStateException("try-to-create: catalog="+source.getFisCatalogCode()+", item="+source.getFisItemCode());
                }
                return source.clone();
            }
            @Override protected void fill(final EcfCatalogItem target, final EcfCatalogItem source) {
                if (!target.getTitle().equals(source.getTitle())) {
                    if (ro) {
                        // если ro, то нельзя менять названия элементов
                        throw new IllegalStateException("try-to-change: catalog="+source.getFisCatalogCode()+", item="+source.getFisItemCode());
                    } else {
                        EcfCatalogDao.this.logger.warn("sync["+source.getFisCatalogCode()+", "+source.getFisItemCode()+"]: «"+target.getTitle()+"» -> «"+source.getTitle()+"»");
                    }
                }
                target.update(source);
                target.setRemovalDate(null);
            }
            @Override protected void doDeleteRecord(final EcfCatalogItem databaseRecord) {
                if (null == databaseRecord.getRemovalDate()) {
                    if (ro) {
                        // если ro, то нельзя удалять
                        throw new IllegalStateException("try-to-delete: catalog="+databaseRecord.getFisCatalogCode()+", item="+databaseRecord.getFisItemCode());
                    }
                    EcfCatalogDao.this.logger.warn("sync["+databaseRecord.getFisCatalogCode()+", "+databaseRecord.getFisItemCode()+"]: delete");
                    databaseRecord.setRemovalDate(now);
                    EcfCatalogDao.this.saveOrUpdate(databaseRecord);
                }
            }
        }.merge(databaseList, targetList);
    }



    //////////////////////////////////////////////////////////////////////////////////////////////////////
    // получение данных о справочниках

    // проверка того, что справочники действительно те, на которые мы заложились
    // (ФИСовцы любят менять перечень справочников, их коды и названия - если упадет, то мы об этом тут же узнаем)
    private static final Map<Long, Object[]> REQUIRED_CATALOGS;
    static {
        final Map<Long, Object[]> map = new LinkedHashMap<>();
        map.put(1L, new Object[] { "Общеобразовательные предметы",                  DictionaryDataHandler.CATALOG_1_DISCIPLINE /* ! */ });
        map.put(2L, new Object[] { "Уровень образования",                           DictionaryDataHandler.CATALOG_DEFAULT });
        map.put(3L, new Object[] { "Уровень олимпиады",                             DictionaryDataHandler.CATALOG_DEFAULT });
        map.put(4L, new Object[] { "Статус заявления",                              DictionaryDataHandler.CATALOG_DEFAULT });
        map.put(5L, new Object[] { "Пол",                                           DictionaryDataHandler.CATALOG_DEFAULT });
        map.put(6L, new Object[] { "Основание для оценки",                          DictionaryDataHandler.CATALOG_DEFAULT });
        map.put(7L, new Object[] { "Страна",                                        DictionaryDataHandler.CATALOG_7_COUNTRIES /* ! */ });
        map.put(8L, new Object[] { "Регион",                                        DictionaryDataHandler.CATALOG_DEFAULT });
        map.put(10L, new Object[] { "Направления подготовки",                        DictionaryDataHandler.CATALOG_10_SPEC     /* ! */ });
        map.put(11L, new Object[] { "Тип вступительных испытаний",                   DictionaryDataHandler.CATALOG_11_DISCIPLINE /* ! */ });
        map.put(12L, new Object[] { "Статус проверки заявлений",                     DictionaryDataHandler.CATALOG_DEFAULT });
        map.put(13L, new Object[] { "Статус проверки документа",                     DictionaryDataHandler.CATALOG_DEFAULT });
        map.put(14L, new Object[] { "Форма обучения",                                DictionaryDataHandler.CATALOG_14_EDUFORM  /* ! */ });
        map.put(15L, new Object[] { "Источник финансирования",                       DictionaryDataHandler.CATALOG_DEFAULT });
        map.put(17L, new Object[] { "Сообщения об ошибках",                          DictionaryDataHandler.CATALOG_DEFAULT });
        map.put(18L, new Object[] { "Тип диплома",                                   DictionaryDataHandler.CATALOG_18_OLYMP_TYPE /* ! */ });
        map.put(19L, new Object[] { "Олимпиады",                                     DictionaryDataHandler.CATALOG_19_OLYMP    /* ! */ });
        map.put(22L, new Object[] { "Тип документа, удостоверяющего личность",       DictionaryDataHandler.CATALOG_DEFAULT });
        map.put(23L, new Object[] { "Группа инвалидности",                           DictionaryDataHandler.CATALOG_DEFAULT });
        map.put(30L, new Object[] { "Вид льготы",                                    DictionaryDataHandler.CATALOG_DEFAULT });
        map.put(31L, new Object[] { "Тип документа",                                 DictionaryDataHandler.CATALOG_DEFAULT });
        map.put(33L, new Object[] { "Тип документа для вступительного испытания ОУ", DictionaryDataHandler.CATALOG_DEFAULT });
        map.put(34L, new Object[] { "Статус приемной кампании",                      DictionaryDataHandler.CATALOG_DEFAULT });
        map.put(35L, new Object[] { "Уровень бюджета",                                DictionaryDataHandler.CATALOG_DEFAULT });
        map.put(36L, new Object[] { "Категории индивидуальных достижений",           DictionaryDataHandler.CATALOG_DEFAULT });
        map.put(38L, new Object[] { "Тип приемной кампании",                         DictionaryDataHandler.CATALOG_DEFAULT });
        map.put(41L, new Object[] { "Тип населенного пункта",                        DictionaryDataHandler.CATALOG_DEFAULT });
        REQUIRED_CATALOGS = Collections.unmodifiableMap(map);
    }

    protected String getFisCatalogName(final Long code) {
        final Object[] row = REQUIRED_CATALOGS.get(code);
        if (null == row) { return null; }
        return (String)row[0];
    }

    protected DictionaryDataHandler getFisCatalogHandler(final Long code) {
        final Object[] row = REQUIRED_CATALOGS.get(code);
        if (null == row) { return DictionaryDataHandler.CATALOG_DEFAULT; }
        return (DictionaryDataHandler)row[1];
    }

    // список справочников
    protected Map<Long, String> getFisCatalogCodes(final IFisAuthentication auth)
    {

        if (true) {
            return ImmutableMap.<Long, String>builder()
                    .put(1L, "Общеобразовательные предметы")
                    .put(2L, "Уровень образования")
                    .put(3L, "Уровень олимпиады")
                    .put(4L, "Статус заявления")
                    .put(5L, "Пол")
                    .put(6L, "Основание для оценки")
                    .put(7L, "Страна")
                    .put(8L, "Регион")
                    .put(9L, "Коды направлений подготовки")
                    .put(10L, "Направления подготовки")
                    .put(11L, "Тип вступительных испытаний")
                    .put(12L, "Статус проверки заявлений")
                    .put(13L, "Статус проверки документа")
                    .put(14L, "Форма обучения")
                    .put(15L, "Источник финансирования")
                    .put(17L, "Сообщения об ошибках")
                    .put(18L, "Тип диплома")
                    .put(19L, "Олимпиады")
                    .put(21L, "Гражданство")
                    .put(22L, "Тип документа, удостоверяющего личность")
                    .put(23L, "Группа инвалидности")
                    .put(30L, "Вид льготы")
                    .put(31L, "Тип документа")
                    .put(33L, "Тип документа для вступительного испытания ОУ")
                    .put(34L, "Статус приемной кампании")
                    .put(35L, "Уровень бюджета")
                    .put(36L, "Категории индивидуальных достижений")
                    .put(38L, "Тип приемной кампании")
                    .put(41L, "Тип населенного пункта")
                    .build();
        }

        this.logger.info("getFisCatalogCodes: begin");
        final Map<Long, String> data = new LinkedHashMap<>();
        try {
            final IFisService service = IFisService.instance.get();

            // готовим запрос
            final fis.schema.catalog_list.req.Root root = new fis.schema.catalog_list.req.Root();
            root.setAuthData(new fis.schema.catalog_list.req.AuthData());
            root.getAuthData().setLogin(auth.getLogin());
            root.getAuthData().setPass(auth.getPassword());

            // посылаем запрос, получаем ответ
            final fis.schema.catalog_list.resp.Dictionaries dictionaries;
            {
                final FisConnection connection = service.connect("dictionary");
                try {
                    dictionaries = connection.call(fis.schema.catalog_list.resp.Dictionaries.class, root);
                }
                catch (NotSpecifiedFisConnectionUrlException e)
                {
                    UserContext.getInstance().getErrorCollector().add(EcfCatalogManager.instance().getProperty("error.auth-error.not-fis-connection-url"));
                    throw e;
                }
                catch (Throwable t) {
                    this.logger.error(connection.getRequest());
                    this.logger.error(connection.getResponse());
                    throw t;
                }

                if (debug) {
                    this.logger.info(connection.getRequest());
                    this.logger.info(connection.getResponse());
                }
            }

            // предварительный показ справочника
            for (final fis.schema.catalog_list.resp.Dictionaries.Dictionary dict: dictionaries.getDictionary()) {
                final String reqName = this.getFisCatalogName(dict.getCode());
                final String name = StringUtils.trimToEmpty(dict.getName());
                this.logger.info("getFisCatalogCodes: ["+dict.getCode()+"]=«"+name+"» (req=«"+reqName+"»)");
            }

            // обработка результата
            for (final fis.schema.catalog_list.resp.Dictionaries.Dictionary dict: dictionaries.getDictionary()) {
                final String reqName = this.getFisCatalogName(dict.getCode());
                final String name = StringUtils.trimToEmpty(dict.getName());
                if (null == reqName) {
                    // выводим сообщение, что у нас появился левый справочник - плохой знак, но не фатально
                    this.logger.warn("getFisCatalogCodes: undefined catalog with code «"+dict.getCode()+"»");
                } else if (!reqName.equals(name)) {
                    // название справочника изменилось - они что-то изменили и не сказали нам, при этом мы ничего не исправляли
                    // это фатальный баг, надо править код, даже если все кажется работающим (есть шанс выгрузить значение не того справочника, который они ждали)
                    throw new IllegalStateException("getFisCatalogCodes: illegal catalog name for «"+dict.getCode()+"»=«"+name+"» (req=«"+reqName+"»)");
                }

                final String old = data.put(dict.getCode(), name);
                if (null != old) {
                    final String message = "getFisCatalogCodes: duplicate catalog code «"+dict.getCode()+"»";
                    if (old.equals(name)) {
                        this.logger.warn(message);
                    } else {
                        throw new IllegalStateException(message);
                    }
                }
            }
        } catch (final Exception t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        } finally {
            this.logger.info("getFisCatalogCodes: end\n");
        }
        return data;
    }


    @SuppressWarnings("unchecked")
    @Override
    public void doSyncCatalogs(final EcfOrgUnit ecfOrgUnit)
    {
        if (StringUtils.isBlank(ecfOrgUnit.getLogin())) {
            throw new ApplicationException(EcfCatalogManager.instance().getProperty("error.auth-error.no-login"));
        }
        if (StringUtils.isBlank(ecfOrgUnit.getPassword())) {
            throw new ApplicationException(EcfCatalogManager.instance().getProperty("error.auth-error.no-password"));
        }


        this.logger.info("\n--------------------------------------------------------------------------------\n");
        this.logger.info("doSyncCatalogs.start");
        try
        {
            final Date now = new Date();
            final boolean ro = !TopOrgUnit.getInstance().equals(ecfOrgUnit.getOrgUnit()); // синхранизировать можно только от головного ОУ, для неголовных - только сверка

            // получаем список справочников, затем синхранизируем каждый по отдельности
            final Map<Long, String> codes = this.getFisCatalogCodes(ecfOrgUnit);
            for (final Map.Entry<Long, String> e: codes.entrySet()) {
                final DictionaryDataHandler handler = this.getFisCatalogHandler(e.getKey());
                final Map<Long, String> items = this.getFisCatalogItems(e, ecfOrgUnit, handler);
                this.sync(e.getKey(), items, ro, now);
            }

            this.logger.info("doSyncCatalogs.complte\n");
        }
        catch (final Exception t)
        {
            this.logger.error("doSyncCatalogs.error: " + t.getMessage(), t);
            Debug.exception(t.getMessage(), t);
            throw new ApplicationException(EcfCatalogManager.instance().getProperty("error.sync-error"), t);
        }
    }

}
