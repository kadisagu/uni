package ru.tandemservice.uniec_fis.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniec_fis_2x10x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность ecfConv4EntranceDiscipline

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("ecf_conv_4_entdiscipline_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_ecfconv4entrancediscipline"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("discipline_id", DBType.LONG).setNullable(false), 
				new DBColumn("value_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("ecfConv4EntranceDiscipline");

		}


		////////////////////////////////////////////////////////////////////////////////
		// сущность ecfConv4Region

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("ecf_conv_4_region_t",
									  new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_ecfconv4region"),
									  new DBColumn("discriminator", DBType.SHORT).setNullable(false),
									  new DBColumn("region_id", DBType.LONG).setNullable(false),
									  new DBColumn("value_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("ecfConv4Region");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность ecfConv4TownType

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("ecf_conv_4_towntype_t",
									  new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_ecfconv4towntype"),
									  new DBColumn("discriminator", DBType.SHORT).setNullable(false),
									  new DBColumn("towntype_id", DBType.LONG).setNullable(false),
									  new DBColumn("value_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("ecfConv4TownType");

		}
    }
}