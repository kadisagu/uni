/* $Id: DppOrgUnitAddEditUI.java 22487 2012-04-04 13:16:00Z vzhukov $ */
package ru.tandemservice.uniec_fis.base.bo.EcfSettings.ui.EcPeriodEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uniec_fis.base.bo.EcfSettings.EcfSettingsManager;
import ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodItem;
import ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodKey;

import java.util.*;

@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "holder.id")
})
public class EcfSettingsEcPeriodEditUI extends UIPresenter
{

    private static final IdentifiableWrapper STAGE_COUNT_OPTIONS[] = {
        wrapper(0),
        wrapper(1),
        wrapper(2),
//        wrapper(3) В связи с тем, что ФИС в этом году активно отвергает 3 этапа, необходимо в интерфейсе в селекте убрать возможность выбора 3 этапа.
//                   http://tracker.tandemservice.ru/browse/DEV-1483?focusedCommentId=164629&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-164629
    };

    private static IdentifiableWrapper wrapper(int size) {
        return new IdentifiableWrapper((long) size, EcfSettingsECPeriodKey.getSizeTitle(size));
    }

    private final EntityHolder<EcfSettingsECPeriodKey> holder = new EntityHolder<EcfSettingsECPeriodKey>();

    public EntityHolder<EcfSettingsECPeriodKey> getHolder() { return this.holder; }
    public EcfSettingsECPeriodKey getKey() { return this.getHolder().getValue(); }

    private List<EcfSettingsECPeriodItem> globalItemList = Collections.emptyList();
    public List<EcfSettingsECPeriodItem> getGlobalItemList() { return this.globalItemList; }
    public void setGlobalItemList(List<EcfSettingsECPeriodItem> globalItemList) { this.globalItemList = globalItemList; }

    public List<EcfSettingsECPeriodItem> getItemList() {
        final List<EcfSettingsECPeriodItem> list = getGlobalItemList();
        return list.subList(0, Math.min(getKey().getSize(), list.size()));
    }

    private EcfSettingsECPeriodItem currentItem;
    public EcfSettingsECPeriodItem getCurrentItem() { return this.currentItem; }
    public void setCurrentItem(EcfSettingsECPeriodItem currentItem) { this.currentItem = currentItem; }

    private List<IdentifiableWrapper> stageCountOptionList = Arrays.asList(STAGE_COUNT_OPTIONS);
    public List<IdentifiableWrapper> getStageCountOptionList() { return this.stageCountOptionList; }

    public IdentifiableWrapper getStageCount() {
        return wrapper(getKey().getSize());
    }
    public void setStageCount(IdentifiableWrapper stageCount) {
        if (null == stageCount || null == stageCount.getId() || stageCount.getId().intValue() < 0) {
            getKey().setSize(0);
        } else {
            getKey().setSize(stageCount.getId().intValue());
        }
    }

    @Override
    public void onComponentRefresh() {
        EcfSettingsECPeriodKey key = this.getHolder().refresh();
        setGlobalItemList(DataAccessServices.dao().getList(EcfSettingsECPeriodItem.class, EcfSettingsECPeriodItem.key(), key, EcfSettingsECPeriodItem.stage().s()));
        onSelectSize();
    }

    public void onSelectSize() {

        EcfSettingsECPeriodKey key = getKey();

        // добавляем недостающие
        final Map<Integer, EcfSettingsECPeriodItem> map = new HashMap<Integer, EcfSettingsECPeriodItem>();
        for (EcfSettingsECPeriodItem item: getGlobalItemList()) {
            map.put(item.getStage(), item);
        }

        int size = key.getSize();
        for (int i=1; i<= size; i++) {
            if (null == map.get(i)) {
                map.put(i, new EcfSettingsECPeriodItem(key, i));
            }
        }
        setGlobalItemList(new ArrayList<EcfSettingsECPeriodItem>(map.values()));

        // сортируем
        Collections.sort(getGlobalItemList());
    }

    public void onClickApply() {
        if (EcfSettingsManager.instance().dao().doValidateEcfSettingsECPeriodKey(getKey(), getItemList()).hasErrors())
            return;

        EcfSettingsManager.instance().dao().doSaveEcfSettingsECPeriodKey(getKey(), getItemList());
        deactivate();
    }




}
