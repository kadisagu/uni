package ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.ecg;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.transaction.DaoCache;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.codes.StudentCategoryCodes;
import ru.tandemservice.uni.entity.catalog.gen.StudentCategoryGen;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineType;
import ru.tandemservice.uniec.entity.catalog.codes.EntranceDisciplineTypeCodes;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.examset.EnrollmentDirectionExamSetData;
import ru.tandemservice.uniec.entity.examset.ExamSetItem;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationFormRelation;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.entity.settings.DisciplinesGroup;
import ru.tandemservice.uniec.entity.settings.SetDiscipline;
import ru.tandemservice.uniec.util.ExamSetUtil;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.EcfConverterManager;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.finSources.EcfFinSourceDao;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.EcfOrgUnitManager;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit;

import java.util.*;

/**
 * @author vdanilov
 */
public class EcfEcgDao extends UniBaseDao implements IEcfEcgDao
{
    public static final boolean LETS_TRY_IT_WITH_EMPTY_EXAM_SET = true;

    private Map<EnrollmentDirection, EnrollmentDirectionExamSetData> getDirectionExamSetDataMap(final EnrollmentCampaign enrollmentCampaign) {
        final String cacheKey = "ecfEcgDao.directionExamSetDataCache."+enrollmentCampaign.getId();
        Map<EnrollmentDirection, EnrollmentDirectionExamSetData> result = DaoCache.get(cacheKey);
        if (null == result) {
            DaoCache.put(cacheKey, result = ExamSetUtil.getDirectionExamSetDataMap(this.getSession(), enrollmentCampaign, null, null));
        }
        return result;
    }

    private String fixCategoryCode(final EnrollmentDirection direction, final String categoryCode) {
        final EnrollmentCampaign ec = direction.getEnrollmentCampaign();
        if (ec.isExamSetDiff()) { return categoryCode; }
        return  StudentCategoryCodes.STUDENT_CATEGORY_STUDENT;
    }

    private String getParamKey(final EnrollmentDirection direction, final String categoryCode) {
        return this.getId4Dir(direction)+"."+this.fixCategoryCode(direction, categoryCode);
    }

    /** @return отвортированный набор ВИ для направления и кода категории */
    protected Collection<ExamSetItem> getExamSet(final EnrollmentDirection direction, String categoryCode)
    {
        // исправляем категорию, если требуется
        categoryCode = this.fixCategoryCode(direction, categoryCode);

        // инициализируем кэш (при первом вызове метода)
        final String cacheKey = "getExamSet.keyCache";
        Map<String, Collection<ExamSetItem>> cache = DaoCache.get(cacheKey);
        if (null == cache) { DaoCache.put(cacheKey, cache = new HashMap<>()); }

        final String paramKey = this.getParamKey(direction, categoryCode);
        Collection<ExamSetItem> result = cache.get(paramKey);
        if (null != result) { return result; }

        // получаем данные
        final EnrollmentDirectionExamSetData examSetData = this.getDirectionExamSetDataMap(direction.getEnrollmentCampaign()).get(direction);
        final StudentCategory category = this.getByNaturalId(new StudentCategoryGen.NaturalId(categoryCode));

        // получаем гарантированно уникальный набор элементов
        final List<ExamSetItem> list = new ArrayList<>(new HashSet<>(examSetData.getCategoryExamSet(category)));

        // сотрируем его (порядок элементов при сравнении не должен учитываться - поэтому они все должны быть одинаково отсортированны)
        Collections.sort(list, new Comparator<ExamSetItem>() {
            @Override public int compare(final ExamSetItem o1, final ExamSetItem o2) {
                return o1.getExamSetItemId().compareTo(o2.getExamSetItemId());
            }
        });

        // сохраняем набор, возвращаем результат
        cache.put(paramKey, result = Collections.unmodifiableList(list));
        return result;
    }


    @Override
    public IEcfEcgKey getKey(final EnrollmentDirection direction, final StudentCategory category) {
        return this.getKey(direction, category.getCode(), false);
    }

    protected IEcfEcgKey getKey(final EnrollmentDirection direction, String categoryCode, boolean keepSilence)
    {
        // исправляем категорию, если требуется
        categoryCode = this.fixCategoryCode(direction, categoryCode);

        // инициализируем кэш (при первом вызове метода)
        final String cacheKey = "ecfEcgDao.keyCache";
        Map<String, IEcfEcgKey> cache = DaoCache.get(cacheKey);
        if (null == cache) { DaoCache.put(cacheKey, cache = new HashMap<>()); }

        // ищем в кэше ключ КГ ФИС по параметрам, если нашли - его и возвращаем
        final String paramKey = this.getParamKey(direction, categoryCode);
        IEcfEcgKey result = cache.get(paramKey);
        if (null != result) { return result; }

        if (direction.getEnrollmentCampaign().isExamSetDiff())
        {
            final Collection<ExamSetItem> examSetStudent = this.getExamSet(direction, StudentCategoryCodes.STUDENT_CATEGORY_STUDENT);
            final Collection<ExamSetItem> examSetListener = this.getExamSet(direction, StudentCategoryCodes.STUDENT_CATEGORY_LISTENER);
            if (!examSetListener.equals(examSetStudent))
                throw new ApplicationException(EcfConverterManager.instance().getProperty("error.ecg.exam-set-different", direction.getTitle()));
        }

        final Collection<ExamSetItem> examSet = this.getExamSet(direction, categoryCode);
        if (examSet.isEmpty() && !LETS_TRY_IT_WITH_EMPTY_EXAM_SET) {
            if (keepSilence) { return null; } /* игнорируем направление */
            final StudentCategory category = this.getByNaturalId(new StudentCategoryGen.NaturalId(categoryCode));
            throw new ApplicationException(EcfConverterManager.instance().getProperty("error.ecg.examset-is-empty", direction.getTitle(), direction.getEnrollmentCampaign().getTitle(), category.getTitle()));
        }

        // http://wiki.tandemservice.ru/pages/viewpage.action?pageId=9864369
        // если у НП Уни для вступительного испытания в наборе есть дисциплина по выбору (не путать с группой дисциплин), то при подготовке пакета выводить человеко-читаемую ошибку.
        for (final ExamSetItem examItem: examSet) {
            if (examItem.getChoice().size() > 0) {
                final StudentCategory category = this.getByNaturalId(new StudentCategoryGen.NaturalId(categoryCode));
                String message = EcfConverterManager.instance().getProperty("error.ecg.examset-has-choice", direction.getTitle(), direction.getEnrollmentCampaign().getTitle(), category.getTitle(), examItem.getSubject().getTitle());
                if (Debug.isDisplay()) {
                    ContextLocal.getInfoCollector().add(message);
                } else {
                    // если это рабочий сервер - то должно падать (такое можно выгружать только для тестов - на рабочей сборке такое в ФИС отправлять нельзя)
                    throw new ApplicationException(message);
                }
            }
        }

        final EnrollmentCampaign ec = direction.getEnrollmentCampaign();

        // обобщенная конкурсная группа UNI (КГ или НП в зависимости от настройки)
        // категория (отделяем второе высшее от студентов и стушателей)
        final StringBuilder titleBuilder;
        final String genericCompetitionGroupId;
        final String studentCategotyKey;

        if (ec.isExamSetDiff())
        {
            // если набор разделяется - то для первого высшего образования выводим НП или КГ Юни
            // для второго - всегода НП Юни (в не зависимости от того, есть там КГ или нет)

            if (StudentCategoryCodes.STUDENT_CATEGORY_STUDENT.equals(categoryCode) || StudentCategoryCodes.STUDENT_CATEGORY_LISTENER.equals(categoryCode))
            {
                if (ec.isUseCompetitionGroup()) {
                    studentCategotyKey = "1";
                    genericCompetitionGroupId = this.getId4Ecg(direction);
                    titleBuilder = this.getTitleBuilder4Ecg(direction);
                } else {
                    studentCategotyKey = "1";
                    genericCompetitionGroupId = this.getId4Dir(direction);
                    titleBuilder = this.getTitleBuilder4Dir(direction);
                }
            }
            else if (StudentCategoryCodes.STUDENT_CATEGORY_SECONDARY.equals(categoryCode))
            {
                // обязательно должен быть пран приема по второму высшему
                if (!EcfFinSourceDao.isSecondEdu(direction)) {
                    if (keepSilence) { return null; } /* игнорируем напрвление */
                    final StudentCategory category = this.getByNaturalId(new StudentCategoryGen.NaturalId(categoryCode));
                    throw new ApplicationException(EcfConverterManager.instance().getProperty("error.ecg.no-second-edu-plan", direction.getTitle(), direction.getEnrollmentCampaign().getTitle(), category.getTitle()));
                }

                studentCategotyKey = "2";
                genericCompetitionGroupId = this.getId4Dir(direction);  // всегда только направления
                titleBuilder = this.getTitleBuilder4Dir(direction).append(", ВВ");
            }
            else
            {
                throw new IllegalStateException("error.unexpected-category: dir=" + direction.getTitle()+", cat="+categoryCode);
            }
        }
        else
        {
            if (ec.isUseCompetitionGroup()) {
                studentCategotyKey = "0";
                genericCompetitionGroupId = this.getId4Ecg(direction);
                titleBuilder = this.getTitleBuilder4Ecg(direction);
            } else {
                studentCategotyKey = "0";
                genericCompetitionGroupId = this.getId4Dir(direction);
                titleBuilder = this.getTitleBuilder4Dir(direction);
            }
        }

        // название формирующего подразделения и формы обучения
        if (genericCompetitionGroupId.equals(this.getId4Dir(direction)))
        {
            titleBuilder.append(", ").append(direction.getEducationOrgUnit().getFormativeOrgUnit().getShortTitle());
            titleBuilder.append(", ").append(direction.getEducationOrgUnit().getTerritorialOrgUnit().getTerritorialShortTitle());
            titleBuilder.append(", ").append(direction.getEducationOrgUnit().getDevelopForm().getShortTitle());
            titleBuilder.append(", ").append(direction.getEducationOrgUnit().getDevelopPeriod().getTitle());
        }

        // курс
        final Integer course = 1; // TODO: брать из НПП

        // технология (разделяем по технологиям UNI)
        final DevelopTech developTech = direction.getEducationOrgUnit().getDevelopTech();
        final String techKey = developTech.getCode();
        final String developTechTitle = StringUtils.trimToNull(developTech.getShortTitle());
        if (null != developTechTitle) { titleBuilder.append(", ").append(developTechTitle); }

        // условия (разделяем по условиям UNI)
        final DevelopCondition developCond = direction.getEducationOrgUnit().getDevelopCondition();
        final String condKey = developCond.getCode();
        final String developCondTitle = StringUtils.trimToNull(developCond.getShortTitle());
        if (null != developCondTitle) { titleBuilder.append(", ").append(developCondTitle); }

        // набор
        final StringBuilder sb = new StringBuilder();
        for (final ExamSetItem item: examSet) {
            if (sb.length() > 0) { sb.append(", "); }
            sb.append(item.getSubject().getShortTitle());
            final String kindTitle = StringUtils.trimToNull(item.getKind().getShortTitle());
            // сокр. название вида дисциплины выводить только для вида "профильный"
            if (null != kindTitle && item.getKind().getCode().equals(UniecDefines.ENTRANCE_DISCIPLINE_KIND_PROFILE)) {
                sb.append(" (").append(kindTitle).append(")");
            }
        }
        if (sb.length() > 0)
            titleBuilder.append(" (").append(sb).append(")");


        final String title = titleBuilder.toString();
        cache.put(paramKey, result = new IEcfEcgKey() {

            @Override public String getTitle() { return title; }

            @Override public Integer getCourse() { return course; }
            @Override public String getGenericCompetitionGroupKey() { return genericCompetitionGroupId; }
            @Override public String getStudentCategoryKey() { return studentCategotyKey; }
            @Override public String getDevelopCondKey() { return condKey; }
            @Override public String getDevelopTechKey() { return techKey; }
            @Override public Collection<ExamSetItem> getExamSet() { return examSet; }

            // key & hash
            private final String _key;
            @Override public String getKey() { return this._key; }

            {
                final StringBuilder sb = new StringBuilder()
                .append(this.getGenericCompetitionGroupKey())
                .append(".").append(this.getStudentCategoryKey())
                .append(".").append(this.getCourse())
                .append(".").append(this.getDevelopTechKey())
                .append(".").append(this.getDevelopCondKey());

                for (final ExamSetItem item: this.getExamSet() /* отсортированный набор */) {
                    sb.append("=").append(item.getExamSetItemId());
                }
                this._key = sb.toString();
            }

            @Override
            public String toString() {
                return this.getKey();
            }

            @Override
            public int hashCode() {
                return this.getKey().hashCode();
            }

            @Override
            public boolean equals(final Object obj) {
                if (this == obj) { return true; }
                if (obj instanceof IEcfEcgKey) {
                    final IEcfEcgKey o = (IEcfEcgKey)obj;
                    return this.getKey().equals(o.getKey());
                }
                return false;
            }

        });

        return result;
    }

    private String getId4Dir(final EnrollmentDirection direction) {
        return Long.toString(direction.getId(), 32);
    }

    private String getId4Ecg(final EnrollmentDirection direction) {
        return Long.toString(direction.getCompetitionGroup().getId(), 32);
    }

    private StringBuilder getTitleBuilder4Dir(final EnrollmentDirection direction) {
        return new StringBuilder(direction.getTitle());
    }

    private StringBuilder getTitleBuilder4Ecg(final EnrollmentDirection direction) {
        return new StringBuilder(direction.getCompetitionGroup().getTitle());
    }


    @Override
    public Map<IEcfEcgKey, Collection<EnrollmentDirection>> getEcgMap(final EcfOrgUnit ecfOrgUnit, final EnrollmentCampaign enrollmentCampaign) {
        final List<String> possibleCategories = new ArrayList<>(4);
        possibleCategories.add(StudentCategoryCodes.STUDENT_CATEGORY_STUDENT);
        if (enrollmentCampaign.isExamSetDiff()) {
            // если различается - то добавляем все
            possibleCategories.add(StudentCategoryCodes.STUDENT_CATEGORY_SECONDARY);
            possibleCategories.add(StudentCategoryCodes.STUDENT_CATEGORY_LISTENER);
        }

        final List<EnrollmentDirection> directions = EcfOrgUnitManager.instance().dao().getEnrollmentDirections(ecfOrgUnit, enrollmentCampaign);
        final Map<IEcfEcgKey, Collection<EnrollmentDirection>> result = new HashMap<>();
        for (final EnrollmentDirection dir: directions) {
            for (final String categoryCode: possibleCategories) {
                final IEcfEcgKey key = this.getKey(dir, categoryCode, true);
                if (null != key) {
                    SafeMap.safeGet(result, key, HashSet.class).add(dir);
                }
            }
        }
        return result;
    }



    @Override
    public Long getTestTypeId(EntranceDisciplineType type) {
        // Вступительные испытания 1
        // Вступительные испытания профильной направленности   3
        // Вступительные испытания творческой и (или) профессиональной направленности  2
        if (null == type) { throw new IllegalArgumentException("type is null"); }

        if (EntranceDisciplineTypeCodes.USUAL.equals(type.getCode())) { return 1L; }
        if (EntranceDisciplineTypeCodes.PROFILE.equals(type.getCode())) { return 3L; }
        if (EntranceDisciplineTypeCodes.CREATIVE_PROFESSIONAL.equals(type.getCode())) { return 2L; }
        throw new IllegalStateException("Unexpected EntranceDisciplineType: " + type.getCode());
    }

    @Override
    public String getFormString(SetDiscipline subject) {
        if (null == subject) { throw new IllegalArgumentException("subject is null"); }

        final String cacheKey = "ecfEcgDao.getFormString";
        Map<Long, String> cache = DaoCache.get(cacheKey);
        if (null == cache) { DaoCache.put(cacheKey, cache = new HashMap<>()); }

        String result = cache.get(subject.getId());
        if (null == result) {
            List<String> list = new ArrayList<>(getFormStringSet(subject));
            Collections.sort(list);
            cache.put(subject.getId(), result = StringUtils.join(list, ", "));
        }
        return result;

    }

    protected Set<String> getFormStringSet(SetDiscipline subject) {

        final String cacheKey = "ecfEcgDao.getFormStringSet";
        Map<Long, Set<String>> cache = DaoCache.get(cacheKey);
        if (null == cache) { DaoCache.put(cacheKey, cache = new HashMap<>()); }

        Set<String> result = cache.get(subject.getId());
        if (null != result) { return result; }

        if (subject instanceof Discipline2RealizationWayRelation) {
            result = new HashSet<>();
            List<Discipline2RealizationFormRelation> rels = getList(Discipline2RealizationFormRelation.class, Discipline2RealizationFormRelation.discipline(), (Discipline2RealizationWayRelation)subject);
            for (Discipline2RealizationFormRelation rel: rels) {
                result.add(rel.getSubjectPassForm().getTitle());
            }
            cache.put(subject.getId(), result);
            return result;
        }

        if (subject instanceof DisciplinesGroup) {
            result = new HashSet<>();
            for (Discipline2RealizationWayRelation s: ((DisciplinesGroup)subject).getDisciplines()) {
                result.addAll(getFormStringSet(s));
            }
            cache.put(subject.getId(), result);
            return result;
        }

        throw new IllegalStateException("Unexpected class: " + subject.getClass());
    }


}
