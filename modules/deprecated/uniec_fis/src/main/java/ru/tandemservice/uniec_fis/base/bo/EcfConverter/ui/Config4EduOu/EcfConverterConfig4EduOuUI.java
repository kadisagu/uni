package ru.tandemservice.uniec_fis.base.bo.EcfConverter.ui.Config4EduOu;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.EcfConverterManager;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.ui.EcfConverterConfigUI;

/**
 * @author vdanilov
 */
public class EcfConverterConfig4EduOuUI extends EcfConverterConfigUI {

    public static final String PARAM_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String PARAM_STRUCTURE_EDU_LVL = "structureEduLvl";
    public static final String PARAM_ITEM_TITLE = "itemTitle";

    public void onClickAutoSync() {
        EcfConverterManager.instance().educationOrgUnit().doAutoSync();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(PARAM_ENROLLMENT_CAMPAIGN, getSettings().get(PARAM_ENROLLMENT_CAMPAIGN));
        dataSource.put(PARAM_STRUCTURE_EDU_LVL, getSettings().get(PARAM_STRUCTURE_EDU_LVL));
        dataSource.put(PARAM_ITEM_TITLE, getSettings().get(PARAM_ITEM_TITLE));
    }
}
