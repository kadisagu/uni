package ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.eduForm;

import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.IEcfConverterStaticDao;

/**
 * @author vdanilov
 */
public interface IEcfConverterDao4EduForm extends IEcfConverterStaticDao<DevelopForm> {

    /**
     * @return O, Z, OZ для очной, заочной и очно-заочной формы соответственно
     */
    String getEduFormPostfix(DevelopForm developForm);

}
