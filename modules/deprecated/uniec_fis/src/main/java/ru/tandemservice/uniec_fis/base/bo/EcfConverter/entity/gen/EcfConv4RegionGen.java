package ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4Region;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * ФИС: Сопоставление: Регион
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EcfConv4RegionGen extends EntityBase
 implements INaturalIdentifiable<EcfConv4RegionGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4Region";
    public static final String ENTITY_NAME = "ecfConv4Region";
    public static final int VERSION_HASH = -1214307409;
    private static IEntityMeta ENTITY_META;

    public static final String L_REGION = "region";
    public static final String L_VALUE = "value";

    private AddressItem _region;     // Регион
    private EcfCatalogItem _value;     // Значение (элемент справочника ФИС)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Регион. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public AddressItem getRegion()
    {
        return _region;
    }

    /**
     * @param region Регион. Свойство не может быть null и должно быть уникальным.
     */
    public void setRegion(AddressItem region)
    {
        dirty(_region, region);
        _region = region;
    }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     */
    @NotNull
    public EcfCatalogItem getValue()
    {
        return _value;
    }

    /**
     * @param value Значение (элемент справочника ФИС). Свойство не может быть null.
     */
    public void setValue(EcfCatalogItem value)
    {
        dirty(_value, value);
        _value = value;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EcfConv4RegionGen)
        {
            if (withNaturalIdProperties)
            {
                setRegion(((EcfConv4Region)another).getRegion());
            }
            setValue(((EcfConv4Region)another).getValue());
        }
    }

    public INaturalId<EcfConv4RegionGen> getNaturalId()
    {
        return new NaturalId(getRegion());
    }

    public static class NaturalId extends NaturalIdBase<EcfConv4RegionGen>
    {
        private static final String PROXY_NAME = "EcfConv4RegionNaturalProxy";

        private Long _region;

        public NaturalId()
        {}

        public NaturalId(AddressItem region)
        {
            _region = ((IEntity) region).getId();
        }

        public Long getRegion()
        {
            return _region;
        }

        public void setRegion(Long region)
        {
            _region = region;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EcfConv4RegionGen.NaturalId) ) return false;

            EcfConv4RegionGen.NaturalId that = (NaturalId) o;

            if( !equals(getRegion(), that.getRegion()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getRegion());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getRegion());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EcfConv4RegionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EcfConv4Region.class;
        }

        public T newInstance()
        {
            return (T) new EcfConv4Region();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "region":
                    return obj.getRegion();
                case "value":
                    return obj.getValue();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "region":
                    obj.setRegion((AddressItem) value);
                    return;
                case "value":
                    obj.setValue((EcfCatalogItem) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "region":
                        return true;
                case "value":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "region":
                    return true;
                case "value":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "region":
                    return AddressItem.class;
                case "value":
                    return EcfCatalogItem.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EcfConv4Region> _dslPath = new Path<EcfConv4Region>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EcfConv4Region");
    }
            

    /**
     * @return Регион. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4Region#getRegion()
     */
    public static AddressItem.Path<AddressItem> region()
    {
        return _dslPath.region();
    }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4Region#getValue()
     */
    public static EcfCatalogItem.Path<EcfCatalogItem> value()
    {
        return _dslPath.value();
    }

    public static class Path<E extends EcfConv4Region> extends EntityPath<E>
    {
        private AddressItem.Path<AddressItem> _region;
        private EcfCatalogItem.Path<EcfCatalogItem> _value;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Регион. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4Region#getRegion()
     */
        public AddressItem.Path<AddressItem> region()
        {
            if(_region == null )
                _region = new AddressItem.Path<AddressItem>(L_REGION, this);
            return _region;
        }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4Region#getValue()
     */
        public EcfCatalogItem.Path<EcfCatalogItem> value()
        {
            if(_value == null )
                _value = new EcfCatalogItem.Path<EcfCatalogItem>(L_VALUE, this);
            return _value;
        }

        public Class getEntityClass()
        {
            return EcfConv4Region.class;
        }

        public String getEntityName()
        {
            return "ecfConv4Region";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
