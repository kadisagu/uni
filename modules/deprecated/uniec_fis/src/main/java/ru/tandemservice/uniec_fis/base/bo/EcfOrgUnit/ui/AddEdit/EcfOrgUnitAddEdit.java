/* $Id: DppOrgUnitAddEdit.java 22487 2012-04-04 13:16:00Z vzhukov $ */
package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.ui.AddEdit;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit;

@Configuration
public class EcfOrgUnitAddEdit extends BusinessComponentManager
{
    public static final String ORG_UNIT_DS = "orgUnitDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
        .addDataSource(this.selectDS(ORG_UNIT_DS, this.orgUnitComboDSHandler()).addColumn(OrgUnit.P_FULL_TITLE))
        .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler orgUnitComboDSHandler() {
        return new DefaultComboDataSourceHandler(this.getName(), OrgUnit.class, OrgUnit.P_TITLE) {
            @Override protected void prepareConditions(final ExecutionParameters<DSInput, DSOutput> ep) {
                super.prepareConditions(ep);
                ep.dqlBuilder.where(notExists(
                    new DQLSelectBuilder()
                    .fromEntity(EcfOrgUnit.class, "x")
                    .column(property("x.id"))
                    .where(eq(property(EcfOrgUnit.orgUnit().fromAlias("x")), property("e")))
                    .buildQuery()
                ));
            }
        };
    }

}
