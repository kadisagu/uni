package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity;

import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.gen.EcfOrgUnitPackage4DelAdmissionOrdersGen;

/**
 * Пакет: на удаление всех заявлений из приказов
 *
 * Root / DataForDelete / OrdersOfAdmission
 */
public class EcfOrgUnitPackage4DelAdmissionOrders extends EcfOrgUnitPackage4DelAdmissionOrdersGen
{
    @Override
    public String getTitle() {
        if (getEnrollmentCampaign() == null) {
            return this.getClass().getSimpleName();
        }
        return "Пакет на удаление всех заявлений из приказов «" + getEnrollmentCampaign().getTitle() + "»";
    }
}