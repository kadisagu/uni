/* $Id$ */
package ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.indAchievement;

import ru.tandemservice.uniec.entity.catalog.IndividualProgress;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.IEcfConverterDao;

/**
 * @author Ekaterina Zvereva
 * @since 04.06.2015
 */
public interface IEcfConverterDao4IndAchievement extends IEcfConverterDao<IndividualProgress>
{

}