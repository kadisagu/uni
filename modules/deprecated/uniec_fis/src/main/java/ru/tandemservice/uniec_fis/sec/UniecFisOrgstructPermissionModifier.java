/**
 *$Id$
 */
package ru.tandemservice.uniec_fis.sec;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.*;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;

import java.util.Map;

/**
 * @author Alexander Shaburov
 * @since 21.01.13
 */
public class UniecFisOrgstructPermissionModifier implements ISecurityConfigMetaMapModifier
{
    @Override
    public void modify(Map<String, SecurityConfigMeta> securityConfigMetaMap)
    {
        SecurityConfigMeta config = new SecurityConfigMeta();
        config.setModuleName("uniec_fis");
        config.setName("uniec-fis-sec-config");
        config.setTitle("");

        ModuleGlobalGroupMeta mggOrgstruct = PermissionMetaUtil.createModuleGlobalGroup(config, "orgstructModuleGlobalGroup", "Модуль «Орг. структура»");
        ModuleLocalGroupMeta mlgOrgstruct = PermissionMetaUtil.createModuleLocalGroup(config, "orgstructLocalGroup", "Модуль «Орг. структура»");

        for (OrgUnitType description : DataAccessServices.dao().getList(OrgUnitType.class, OrgUnitType.P_TITLE))
        {
            String code = description.getCode();
            ClassGroupMeta gcg = PermissionMetaUtil.createClassGroup(mggOrgstruct, code + "PermissionClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());
            ClassGroupMeta lcg = PermissionMetaUtil.createClassGroup(mlgOrgstruct, code + "LocalClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());

            PermissionGroupMeta pgEcfTab = PermissionMetaUtil.createPermissionGroup(config, code + "EcfTabPermissionGroup", "Вкладка «ФИС»");
            PermissionMetaUtil.createGroupRelation(config, pgEcfTab.getName(), gcg.getName());
            PermissionMetaUtil.createGroupRelation(config, pgEcfTab.getName(), lcg.getName());
            PermissionMetaUtil.createPermission(pgEcfTab, "orgUnit_viewEcfTab_" + code, "Просмотр");

            PermissionGroupMeta pgEcfPkgTab = PermissionMetaUtil.createPermissionGroup(pgEcfTab, code + "EcfPackagesTabPermissionGroup", "Вкладка «Пакеты»");
            PermissionMetaUtil.createPermission(pgEcfPkgTab, "orgUnit_viewEcfPackagesTab_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(pgEcfPkgTab, "orgUnit_pkgOperationEcfPackagesTab_" + code, "Добавление и отправка пакетов в ФИС");
            PermissionMetaUtil.createPermission(pgEcfPkgTab, "orgUnit_xmlPkgOperationEcfPackagesTab_" + code, "Скачивание XML файлов пакетов для отправки в ФИС и файлов ответа ФИС");
        }

        securityConfigMetaMap.put(config.getName(), config);
    }
}
