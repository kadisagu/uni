package ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.ecg;

import java.util.Collection;
import java.util.Map;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.commonbase.base.util.Wiki;

import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.examset.ExamSetItem;
import ru.tandemservice.uniec.entity.settings.SetDiscipline;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit;

/**
 * @author vdanilov
 */
public interface IEcfEcgDao extends INeedPersistenceSupport {

    interface IEcfEcgKey {

        final String ST_CATEGORY_ALL = "0";
        final String ST_CATEGORY_FIRST = "1";
        final String ST_CATEGORY_SECOND = "2";

        /** @return (ключ) идентификатор обобщенной КГ в UNI */
        String getGenericCompetitionGroupKey();

        /** @return (ключ) 0 - категории не разделяются, 1 - студент/слушатель, 2 - второе высшее */
        String getStudentCategoryKey();

        /** @return (ключ) курс, на который ведется прием*/
        Integer getCourse();

        /** @return (ключ) технология обучения (все наши) - код справочника developTech */
        String getDevelopTechKey();

        /** @return (ключ) условие обучения (все наши) - код справочника developCond */
        String getDevelopCondKey();

        /** @return (ключ) вступительные испытания */
        Collection<ExamSetItem> getExamSet();

        /** @return составной уникальный ключ */
        String getKey();

        /** @return (значение) занвание КГ */
        String getTitle();
    }

    /**
     * @param direction
     * @param category
     * @return IEcfEcgKey
     * @throws org.tandemframework.core.exception.ApplicationException если есть косяки
     */
    IEcfEcgKey getKey(EnrollmentDirection direction, StudentCategory category);

    /**
     * @param ecfOrgUnit
     * @param enrollmentCampaign
     * @return
     */
    Map<IEcfEcgKey, Collection<EnrollmentDirection>> getEcgMap(EcfOrgUnit ecfOrgUnit, EnrollmentCampaign enrollmentCampaign);

    /**
     * http://wiki.tandemservice.ru/pages/viewpage.action?pageId=9864754
     * @return код элемента справочника ФИС
     */
    @Wiki(url="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=9864754")
    Long getTestTypeId(EntranceDisciplineType type);

    /**
     * http://wiki.tandemservice.ru/pages/viewpage.action?pageId=9864369
     * {quote}
     * выводим через запятую с пробелом все формы сдачи (названия форм сдачи), заданные настройкой "Формы сдачи для дисциплин вступительных испытаний" для данной дисциплины ВИ.
     * если передается группа дисциплин, то нужно взять из группы дисциплин все дисциплины и взять все их формы (уникальный набор).
     * {quote}
     * @return строка с названиями форм
     */
    String getFormString(SetDiscipline subject);


}
