package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackageIOLog;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Лог взаимодействия с сервером
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EcfOrgUnitPackageIOLogGen extends EntityBase
 implements ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackageIOLog";
    public static final String ENTITY_NAME = "ecfOrgUnitPackageIOLog";
    public static final int VERSION_HASH = 744525518;
    private static IEntityMeta ENTITY_META;

    public static final String L_OWNER = "owner";
    public static final String P_OPERATION_DATE = "operationDate";
    public static final String P_OPERATION_URL = "operationUrl";
    public static final String P_STATUS = "status";
    public static final String P_FIS_ERROR_CODE = "fisErrorCode";
    public static final String P_FIS_ERROR_TEXT = "fisErrorText";
    public static final String P_ZIP_XML_REQUEST = "zipXmlRequest";
    public static final String P_ZIP_XML_RESPONSE = "zipXmlResponse";
    public static final String P_ERROR = "error";
    public static final String P_ZIP_ERROR_LOG = "zipErrorLog";
    public static final String P_TITLE = "title";

    private EcfOrgUnitPackage _owner;     // Пакет
    private Date _operationDate;     // Время проведения операции
    private String _operationUrl;     // Адрес, через который проводилась операция
    private String _status;     // Результирующее состояние операции при передачи пакета в ФИС
    private String _fisErrorCode;     // Код ошибки в ответе ФИС
    private String _fisErrorText;     // Сообщение об ошибке в ответе ФИС
    private byte[] _zipXmlRequest;     // ZIP-XML: отправленные данные в ФИС
    private byte[] _zipXmlResponse;     // ZIP-XML: ответ из ФИС
    private boolean _error;     // Ошибка взаимодействия
    private byte[] _zipErrorLog;     // ZIP: сообщение об ошибке

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * Пакет, в тамках которого происходит операция
     *
     * @return Пакет. Свойство не может быть null.
     */
    @NotNull
    public EcfOrgUnitPackage getOwner()
    {
        return _owner;
    }

    /**
     * @param owner Пакет. Свойство не может быть null.
     */
    public void setOwner(EcfOrgUnitPackage owner)
    {
        dirty(_owner, owner);
        _owner = owner;
    }

    /**
     * @return Время проведения операции. Свойство не может быть null.
     */
    @NotNull
    public Date getOperationDate()
    {
        return _operationDate;
    }

    /**
     * @param operationDate Время проведения операции. Свойство не может быть null.
     */
    public void setOperationDate(Date operationDate)
    {
        dirty(_operationDate, operationDate);
        _operationDate = operationDate;
    }

    /**
     * @return Адрес, через который проводилась операция. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getOperationUrl()
    {
        return _operationUrl;
    }

    /**
     * @param operationUrl Адрес, через который проводилась операция. Свойство не может быть null.
     */
    public void setOperationUrl(String operationUrl)
    {
        dirty(_operationUrl, operationUrl);
        _operationUrl = operationUrl;
    }

    /**
     * @return Результирующее состояние операции при передачи пакета в ФИС. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getStatus()
    {
        return _status;
    }

    /**
     * @param status Результирующее состояние операции при передачи пакета в ФИС. Свойство не может быть null.
     */
    public void setStatus(String status)
    {
        dirty(_status, status);
        _status = status;
    }

    /**
     * @return Код ошибки в ответе ФИС.
     */
    @Length(max=255)
    public String getFisErrorCode()
    {
        return _fisErrorCode;
    }

    /**
     * @param fisErrorCode Код ошибки в ответе ФИС.
     */
    public void setFisErrorCode(String fisErrorCode)
    {
        dirty(_fisErrorCode, fisErrorCode);
        _fisErrorCode = fisErrorCode;
    }

    /**
     * @return Сообщение об ошибке в ответе ФИС.
     */
    public String getFisErrorText()
    {
        return _fisErrorText;
    }

    /**
     * @param fisErrorText Сообщение об ошибке в ответе ФИС.
     */
    public void setFisErrorText(String fisErrorText)
    {
        dirty(_fisErrorText, fisErrorText);
        _fisErrorText = fisErrorText;
    }

    /**
     * @return ZIP-XML: отправленные данные в ФИС.
     */
    public byte[] getZipXmlRequest()
    {
        initLazyForGet("zipXmlRequest");
        return _zipXmlRequest;
    }

    /**
     * @param zipXmlRequest ZIP-XML: отправленные данные в ФИС.
     */
    public void setZipXmlRequest(byte[] zipXmlRequest)
    {
        initLazyForSet("zipXmlRequest");
        dirty(_zipXmlRequest, zipXmlRequest);
        _zipXmlRequest = zipXmlRequest;
    }

    /**
     * @return ZIP-XML: ответ из ФИС.
     */
    public byte[] getZipXmlResponse()
    {
        initLazyForGet("zipXmlResponse");
        return _zipXmlResponse;
    }

    /**
     * @param zipXmlResponse ZIP-XML: ответ из ФИС.
     */
    public void setZipXmlResponse(byte[] zipXmlResponse)
    {
        initLazyForSet("zipXmlResponse");
        dirty(_zipXmlResponse, zipXmlResponse);
        _zipXmlResponse = zipXmlResponse;
    }

    /**
     * @return Ошибка взаимодействия. Свойство не может быть null.
     *
     * Это формула "case when zipErrorLog is not null then true else false end".
     */
    // @NotNull
    public boolean isError()
    {
        return _error;
    }

    /**
     * @param error Ошибка взаимодействия. Свойство не может быть null.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setError(boolean error)
    {
        dirty(_error, error);
        _error = error;
    }

    /**
     * @return ZIP: сообщение об ошибке.
     */
    public byte[] getZipErrorLog()
    {
        initLazyForGet("zipErrorLog");
        return _zipErrorLog;
    }

    /**
     * @param zipErrorLog ZIP: сообщение об ошибке.
     */
    public void setZipErrorLog(byte[] zipErrorLog)
    {
        initLazyForSet("zipErrorLog");
        dirty(_zipErrorLog, zipErrorLog);
        _zipErrorLog = zipErrorLog;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EcfOrgUnitPackageIOLogGen)
        {
            setOwner(((EcfOrgUnitPackageIOLog)another).getOwner());
            setOperationDate(((EcfOrgUnitPackageIOLog)another).getOperationDate());
            setOperationUrl(((EcfOrgUnitPackageIOLog)another).getOperationUrl());
            setStatus(((EcfOrgUnitPackageIOLog)another).getStatus());
            setFisErrorCode(((EcfOrgUnitPackageIOLog)another).getFisErrorCode());
            setFisErrorText(((EcfOrgUnitPackageIOLog)another).getFisErrorText());
            setZipXmlRequest(((EcfOrgUnitPackageIOLog)another).getZipXmlRequest());
            setZipXmlResponse(((EcfOrgUnitPackageIOLog)another).getZipXmlResponse());
            setError(((EcfOrgUnitPackageIOLog)another).isError());
            setZipErrorLog(((EcfOrgUnitPackageIOLog)another).getZipErrorLog());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EcfOrgUnitPackageIOLogGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EcfOrgUnitPackageIOLog.class;
        }

        public T newInstance()
        {
            return (T) new EcfOrgUnitPackageIOLog();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "owner":
                    return obj.getOwner();
                case "operationDate":
                    return obj.getOperationDate();
                case "operationUrl":
                    return obj.getOperationUrl();
                case "status":
                    return obj.getStatus();
                case "fisErrorCode":
                    return obj.getFisErrorCode();
                case "fisErrorText":
                    return obj.getFisErrorText();
                case "zipXmlRequest":
                    return obj.getZipXmlRequest();
                case "zipXmlResponse":
                    return obj.getZipXmlResponse();
                case "error":
                    return obj.isError();
                case "zipErrorLog":
                    return obj.getZipErrorLog();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "owner":
                    obj.setOwner((EcfOrgUnitPackage) value);
                    return;
                case "operationDate":
                    obj.setOperationDate((Date) value);
                    return;
                case "operationUrl":
                    obj.setOperationUrl((String) value);
                    return;
                case "status":
                    obj.setStatus((String) value);
                    return;
                case "fisErrorCode":
                    obj.setFisErrorCode((String) value);
                    return;
                case "fisErrorText":
                    obj.setFisErrorText((String) value);
                    return;
                case "zipXmlRequest":
                    obj.setZipXmlRequest((byte[]) value);
                    return;
                case "zipXmlResponse":
                    obj.setZipXmlResponse((byte[]) value);
                    return;
                case "error":
                    obj.setError((Boolean) value);
                    return;
                case "zipErrorLog":
                    obj.setZipErrorLog((byte[]) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "owner":
                        return true;
                case "operationDate":
                        return true;
                case "operationUrl":
                        return true;
                case "status":
                        return true;
                case "fisErrorCode":
                        return true;
                case "fisErrorText":
                        return true;
                case "zipXmlRequest":
                        return true;
                case "zipXmlResponse":
                        return true;
                case "error":
                        return true;
                case "zipErrorLog":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "owner":
                    return true;
                case "operationDate":
                    return true;
                case "operationUrl":
                    return true;
                case "status":
                    return true;
                case "fisErrorCode":
                    return true;
                case "fisErrorText":
                    return true;
                case "zipXmlRequest":
                    return true;
                case "zipXmlResponse":
                    return true;
                case "error":
                    return true;
                case "zipErrorLog":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "owner":
                    return EcfOrgUnitPackage.class;
                case "operationDate":
                    return Date.class;
                case "operationUrl":
                    return String.class;
                case "status":
                    return String.class;
                case "fisErrorCode":
                    return String.class;
                case "fisErrorText":
                    return String.class;
                case "zipXmlRequest":
                    return byte[].class;
                case "zipXmlResponse":
                    return byte[].class;
                case "error":
                    return Boolean.class;
                case "zipErrorLog":
                    return byte[].class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EcfOrgUnitPackageIOLog> _dslPath = new Path<EcfOrgUnitPackageIOLog>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EcfOrgUnitPackageIOLog");
    }
            

    /**
     * Пакет, в тамках которого происходит операция
     *
     * @return Пакет. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackageIOLog#getOwner()
     */
    public static EcfOrgUnitPackage.Path<EcfOrgUnitPackage> owner()
    {
        return _dslPath.owner();
    }

    /**
     * @return Время проведения операции. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackageIOLog#getOperationDate()
     */
    public static PropertyPath<Date> operationDate()
    {
        return _dslPath.operationDate();
    }

    /**
     * @return Адрес, через который проводилась операция. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackageIOLog#getOperationUrl()
     */
    public static PropertyPath<String> operationUrl()
    {
        return _dslPath.operationUrl();
    }

    /**
     * @return Результирующее состояние операции при передачи пакета в ФИС. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackageIOLog#getStatus()
     */
    public static PropertyPath<String> status()
    {
        return _dslPath.status();
    }

    /**
     * @return Код ошибки в ответе ФИС.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackageIOLog#getFisErrorCode()
     */
    public static PropertyPath<String> fisErrorCode()
    {
        return _dslPath.fisErrorCode();
    }

    /**
     * @return Сообщение об ошибке в ответе ФИС.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackageIOLog#getFisErrorText()
     */
    public static PropertyPath<String> fisErrorText()
    {
        return _dslPath.fisErrorText();
    }

    /**
     * @return ZIP-XML: отправленные данные в ФИС.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackageIOLog#getZipXmlRequest()
     */
    public static PropertyPath<byte[]> zipXmlRequest()
    {
        return _dslPath.zipXmlRequest();
    }

    /**
     * @return ZIP-XML: ответ из ФИС.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackageIOLog#getZipXmlResponse()
     */
    public static PropertyPath<byte[]> zipXmlResponse()
    {
        return _dslPath.zipXmlResponse();
    }

    /**
     * @return Ошибка взаимодействия. Свойство не может быть null.
     *
     * Это формула "case when zipErrorLog is not null then true else false end".
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackageIOLog#isError()
     */
    public static PropertyPath<Boolean> error()
    {
        return _dslPath.error();
    }

    /**
     * @return ZIP: сообщение об ошибке.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackageIOLog#getZipErrorLog()
     */
    public static PropertyPath<byte[]> zipErrorLog()
    {
        return _dslPath.zipErrorLog();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackageIOLog#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EcfOrgUnitPackageIOLog> extends EntityPath<E>
    {
        private EcfOrgUnitPackage.Path<EcfOrgUnitPackage> _owner;
        private PropertyPath<Date> _operationDate;
        private PropertyPath<String> _operationUrl;
        private PropertyPath<String> _status;
        private PropertyPath<String> _fisErrorCode;
        private PropertyPath<String> _fisErrorText;
        private PropertyPath<byte[]> _zipXmlRequest;
        private PropertyPath<byte[]> _zipXmlResponse;
        private PropertyPath<Boolean> _error;
        private PropertyPath<byte[]> _zipErrorLog;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * Пакет, в тамках которого происходит операция
     *
     * @return Пакет. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackageIOLog#getOwner()
     */
        public EcfOrgUnitPackage.Path<EcfOrgUnitPackage> owner()
        {
            if(_owner == null )
                _owner = new EcfOrgUnitPackage.Path<EcfOrgUnitPackage>(L_OWNER, this);
            return _owner;
        }

    /**
     * @return Время проведения операции. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackageIOLog#getOperationDate()
     */
        public PropertyPath<Date> operationDate()
        {
            if(_operationDate == null )
                _operationDate = new PropertyPath<Date>(EcfOrgUnitPackageIOLogGen.P_OPERATION_DATE, this);
            return _operationDate;
        }

    /**
     * @return Адрес, через который проводилась операция. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackageIOLog#getOperationUrl()
     */
        public PropertyPath<String> operationUrl()
        {
            if(_operationUrl == null )
                _operationUrl = new PropertyPath<String>(EcfOrgUnitPackageIOLogGen.P_OPERATION_URL, this);
            return _operationUrl;
        }

    /**
     * @return Результирующее состояние операции при передачи пакета в ФИС. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackageIOLog#getStatus()
     */
        public PropertyPath<String> status()
        {
            if(_status == null )
                _status = new PropertyPath<String>(EcfOrgUnitPackageIOLogGen.P_STATUS, this);
            return _status;
        }

    /**
     * @return Код ошибки в ответе ФИС.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackageIOLog#getFisErrorCode()
     */
        public PropertyPath<String> fisErrorCode()
        {
            if(_fisErrorCode == null )
                _fisErrorCode = new PropertyPath<String>(EcfOrgUnitPackageIOLogGen.P_FIS_ERROR_CODE, this);
            return _fisErrorCode;
        }

    /**
     * @return Сообщение об ошибке в ответе ФИС.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackageIOLog#getFisErrorText()
     */
        public PropertyPath<String> fisErrorText()
        {
            if(_fisErrorText == null )
                _fisErrorText = new PropertyPath<String>(EcfOrgUnitPackageIOLogGen.P_FIS_ERROR_TEXT, this);
            return _fisErrorText;
        }

    /**
     * @return ZIP-XML: отправленные данные в ФИС.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackageIOLog#getZipXmlRequest()
     */
        public PropertyPath<byte[]> zipXmlRequest()
        {
            if(_zipXmlRequest == null )
                _zipXmlRequest = new PropertyPath<byte[]>(EcfOrgUnitPackageIOLogGen.P_ZIP_XML_REQUEST, this);
            return _zipXmlRequest;
        }

    /**
     * @return ZIP-XML: ответ из ФИС.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackageIOLog#getZipXmlResponse()
     */
        public PropertyPath<byte[]> zipXmlResponse()
        {
            if(_zipXmlResponse == null )
                _zipXmlResponse = new PropertyPath<byte[]>(EcfOrgUnitPackageIOLogGen.P_ZIP_XML_RESPONSE, this);
            return _zipXmlResponse;
        }

    /**
     * @return Ошибка взаимодействия. Свойство не может быть null.
     *
     * Это формула "case when zipErrorLog is not null then true else false end".
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackageIOLog#isError()
     */
        public PropertyPath<Boolean> error()
        {
            if(_error == null )
                _error = new PropertyPath<Boolean>(EcfOrgUnitPackageIOLogGen.P_ERROR, this);
            return _error;
        }

    /**
     * @return ZIP: сообщение об ошибке.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackageIOLog#getZipErrorLog()
     */
        public PropertyPath<byte[]> zipErrorLog()
        {
            if(_zipErrorLog == null )
                _zipErrorLog = new PropertyPath<byte[]>(EcfOrgUnitPackageIOLogGen.P_ZIP_ERROR_LOG, this);
            return _zipErrorLog;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackageIOLog#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(EcfOrgUnitPackageIOLogGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EcfOrgUnitPackageIOLog.class;
        }

        public String getEntityName()
        {
            return "ecfOrgUnitPackageIOLog";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTitle();
}
