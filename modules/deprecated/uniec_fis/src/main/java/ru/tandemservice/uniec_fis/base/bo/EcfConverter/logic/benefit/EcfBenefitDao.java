/**
 *$Id$
 */
package ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.benefit;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.DaoCache;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.entity.settings.DisciplinesGroup;
import ru.tandemservice.uniec.entity.settings.Group2DisciplineRelation;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 28.12.12
 */
public class EcfBenefitDao extends UniBaseDao implements IEcfBenefitDao
{
    final static String RESULT_CAHE_KEY = "getBenefitCode4RequestedEnrollmentDirection.keyCache";
    final static String OLYMPIAD_CAHE_KEY = "getBenefitCode4RequestedEnrollmentDirection.keyCache.olympiad";
    final static String BENEFIT_CAHE_KEY = "getBenefitCode4RequestedEnrollmentDirection.keyCache.benefit";
    final static String RECOMMENDATION_CAHE_KEY = "getBenefitCode4RequestedEnrollmentDirection.keyCache.recommendation";
    final static String ENTRANCE_DISCIPLINE_CAHE_KEY = "getBenefitCode4RequestedEnrollmentDirection.keyCache.entranceDiscipline";

    @Override
    public Integer getBenefitCode4RequestedEnrollmentDirection(RequestedEnrollmentDirection direction)
    {
        // инициализируем кэш
        // ВНП -> [вид общей льготы; передаваемое название льготы]
        Map<RequestedEnrollmentDirection, CoreCollectionUtils.Pair<Integer, String>> cache = DaoCache.get(RESULT_CAHE_KEY);
        if (null == cache)
            DaoCache.put(RESULT_CAHE_KEY, cache = new HashMap<>());

        if (cache.containsKey(direction))
            return cache.get(direction).getX();

        EnrollmentCampaign enrollmentCampaign = direction.getEnrollmentDirection().getEnrollmentCampaign();

        // поднимаем данные по олимпиадам - инициализируем кеш
        Map<Long, List<Discipline2OlympiadDiplomaRelation>> olympiadMapCahe = getOlympiadMapCache(enrollmentCampaign);

        // поднимаем данные по ВИ направлений приема (id НПП -> ids дисциплин) - инициализируем кеш
        Map<Long, List<Long>> entranceDisciplineListCache = DaoCache.get(ENTRANCE_DISCIPLINE_CAHE_KEY);
        if (entranceDisciplineListCache == null)
        {
            final List<EntranceDiscipline> edDiscList = new DQLSelectBuilder().fromEntity(EntranceDiscipline.class, "d").column(property("d"))
                    .where(eq(property(EntranceDiscipline.enrollmentDirection().enrollmentCampaign().fromAlias("d")), value(enrollmentCampaign)))
                    .fetchPath(DQLJoinType.inner, EntranceDiscipline.discipline().fromAlias("d"), "disc")
                    .where(instanceOf("disc", Discipline2RealizationWayRelation.class))
                    .createStatement(getSession()).list();

            entranceDisciplineListCache = new HashMap<>();
            for (EntranceDiscipline discipline : edDiscList)
            {
                SafeMap.safeGet(entranceDisciplineListCache, discipline.getEnrollmentDirection().getId(), LinkedList.class)
                        .add(discipline.getDiscipline().getId());
            }

            final List<EntranceDiscipline> edGroupList = new DQLSelectBuilder().fromEntity(EntranceDiscipline.class, "g").column(property("g"))
                    .where(eq(property(EntranceDiscipline.enrollmentDirection().enrollmentCampaign().fromAlias("g")), value(enrollmentCampaign)))
                    .fetchPath(DQLJoinType.inner, EntranceDiscipline.discipline().fromAlias("g"), "disc")
                    .where(instanceOf("disc", DisciplinesGroup.class))
                    .createStatement(getSession()).list();

            for (EntranceDiscipline discipline : edGroupList)
            {
                final List<Long> discGroupIdList = new DQLSelectBuilder().fromEntity(Group2DisciplineRelation.class, "d").column(property(Group2DisciplineRelation.discipline().id().fromAlias("d")))
                        .where(eq(property(Group2DisciplineRelation.group().fromAlias("d")), value(discipline.getDiscipline())))
                        .createStatement(getSession()).list();

                for (Long id : discGroupIdList)
                {
                    SafeMap.safeGet(entranceDisciplineListCache, discipline.getEnrollmentDirection().getId(), LinkedList.class)
                        .add(id);
                }
            }

            DaoCache.put(ENTRANCE_DISCIPLINE_CAHE_KEY, entranceDisciplineListCache);
        }

        // поднимаем данные по льготам - инициализируем кеш
        Map<Long, PersonBenefit> benefitMapCache = getBenefitMapCache(enrollmentCampaign);

        // поднимаем данные по приемуществам при равенстве конкурсных баллов - инициализируем кеш
        Map<Long, List<EntrantEnrolmentRecommendation>> recommendationMapCache = DaoCache.get(RECOMMENDATION_CAHE_KEY);
        if (recommendationMapCache == null)
        {
            final List<EntrantEnrolmentRecommendation> recommendationList = new DQLSelectBuilder().fromEntity(EntrantEnrolmentRecommendation.class, "r").column(property("r"))
                    .where(eq(property(EntrantEnrolmentRecommendation.entrant().enrollmentCampaign().fromAlias("r")), value(enrollmentCampaign)))
                    .createStatement(getSession()).list();

            recommendationMapCache = new HashMap<>();
            for (EntrantEnrolmentRecommendation recommendation : recommendationList)
                SafeMap.safeGet(recommendationMapCache, recommendation.getEntrant().getId(), LinkedList.class)
                        .add(recommendation);

            DaoCache.put(RECOMMENDATION_CAHE_KEY, recommendationMapCache);
        }

        final Long personId = direction.getEntrantRequest().getEntrant().getPerson().getId();
        final Long entrantId = direction.getEntrantRequest().getEntrant().getId();

        final Discipline2OlympiadDiplomaRelation olympiad = resolveOlympiadDiploma(olympiadMapCahe, entranceDisciplineListCache, direction);
        // у абитуриента существует диплом участника олимпиады ( по DEV-3422 )
        final boolean hasOlympiadDiploma = olympiad != null;
        // у персоны абитуриента выбрана льгота
        final boolean hasPersonBenefit = benefitMapCache.containsKey(personId);
        // выбранное преимущество при равенстве конкурсных баллов абитуриента
        final boolean hasRecommendation = recommendationMapCache.containsKey(entrantId);
        // вид конкурса для ВНП
        final String competitionKindCode = direction.getCompetitionKind().getCode();

        if (competitionKindCode.equals(UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES) && hasOlympiadDiploma)
        {
            OlympiadDiploma diploma = olympiad.getDiploma();

            String name = diploma.getOlympiad() +
                    ", предмет: " + olympiad.getDiscipline().getTitle() +
                    ", диплом: " + diploma.getDegree().getTitle() +
                    (diploma.getSeria() != null && diploma.getNumber() != null ? ", №" + diploma.getSeria() + " " + diploma.getNumber() : "") +
                    (diploma.getIssuanceDate() != null ? " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(diploma.getIssuanceDate()) : "");

            // сохраняем название в кэш
            cache.put(direction, new CoreCollectionUtils.Pair<>(1, name));
            DaoCache.put(RESULT_CAHE_KEY, cache);

            return 1;
        }
        if (competitionKindCode.equals(UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION) && hasPersonBenefit)
        {
            PersonBenefit personBenefit = benefitMapCache.get(personId);

            String name = personBenefit.getTitleWhithDocDateBasic();

            // сохраняем название в кэш
            cache.put(direction, new CoreCollectionUtils.Pair<>(2, name));
            DaoCache.put(RESULT_CAHE_KEY, cache);

            return 2;
        }
        if (hasRecommendation)
        {
            String name = UniStringUtils.join(recommendationMapCache.get(entrantId), EntrantEnrolmentRecommendation.recommendation().title().s(), ", ");

            // сохраняем название в кэш
            cache.put(direction, new CoreCollectionUtils.Pair<>(3, name));
            DaoCache.put(RESULT_CAHE_KEY, cache);

            return 3;
        }

        return null;
    }

    @Override
    public EcfCatalogItem getEcfItem4BenefitCode(int code)
    {
        if (code == 1)
            return getByNaturalId(new EcfCatalogItem.NaturalId("30", "1"));
        if (code == 2)
            return getByNaturalId(new EcfCatalogItem.NaturalId("30", "4"));
        if (code == 3)
            return getByNaturalId(new EcfCatalogItem.NaturalId("30", "5"));

        return null;
    }

    @Override
    public EcfCatalogItem getEcfItem4RequestedEnrollmentDirection(RequestedEnrollmentDirection direction)
    {
        return getEcfItem4BenefitCode(getBenefitCode4RequestedEnrollmentDirection(direction));
    }

    @Override
    public String getDocumentTypeNameText(RequestedEnrollmentDirection direction)
    {
        Map<RequestedEnrollmentDirection, CoreCollectionUtils.Pair<Integer, String>> cache = DaoCache.get(RESULT_CAHE_KEY);
        if (cache != null)
        {
            if (cache.containsKey(direction))
                return cache.get(direction).getY();
        }

        Integer code = getBenefitCode4RequestedEnrollmentDirection(direction);
        return code != null ? getDocumentTypeNameText(direction) : null;
    }


    @Override
    public List<Discipline2OlympiadDiplomaRelation> getDiscipline2OlympiadDiplomaRelations(Entrant entrant)
    {
        return getOlympiadMapCache(entrant.getEnrollmentCampaign()).get(entrant.getId());
    }

    @Override
    public List<OlympiadDiploma> getOlympiadDiplomas(Entrant entrant)
    {
        List<Discipline2OlympiadDiplomaRelation> discipline2OlympiadDiplomaRelations = getDiscipline2OlympiadDiplomaRelations(entrant);

        List<OlympiadDiploma> olympiadDiplomas = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(discipline2OlympiadDiplomaRelations))
        {
            for (Discipline2OlympiadDiplomaRelation discipline2OlympiadDiplomaRelation : discipline2OlympiadDiplomaRelations)
            {
                OlympiadDiploma diploma = discipline2OlympiadDiplomaRelation.getDiploma();
                if (!olympiadDiplomas.contains(diploma))
                    olympiadDiplomas.add(diploma);
            }
        }

        return olympiadDiplomas;
    }

    /**
     * DEV-3422
     * У абитуриента существует диплом участника олимпиады.
     * @param olympiadMapCahe Абитуриент -> список его Зачтенных олимпиад
     * @param entranceDisciplineListCache НПП -> список его Вступительных испытаний
     * @param direction ВНП
     * @return диплом олимпиады абитуриента зачтеный по ВИ данного ВНП
     */
    protected Discipline2OlympiadDiplomaRelation resolveOlympiadDiploma(Map<Long, List<Discipline2OlympiadDiplomaRelation>> olympiadMapCahe, Map<Long, List<Long>> entranceDisciplineListCache, RequestedEnrollmentDirection direction)
    {
        List<Discipline2OlympiadDiplomaRelation> olympiadList = olympiadMapCahe.get(direction.getEntrantRequest().getEntrant().getId());
        if (CollectionUtils.isNotEmpty(olympiadList))
        {
            return olympiadList.get(0);

            /* DEV-3422 */
            /*List<Long> discIdList = entranceDisciplineListCache.get(direction.getEnrollmentDirection().getId());
            if (discIdList != null)
            {
                for (Discipline2OlympiadDiplomaRelation olymp : olympiadList)
                {
                    for (Long discId : discIdList)
                    {
                        if (discId.equals(olymp.getDiscipline().getId()))
                        {
                            return olymp;
                        }
                    }
                }
            }*/
        }

        return null;
    }

    protected Map<Long, List<Discipline2OlympiadDiplomaRelation>> getOlympiadMapCache(EnrollmentCampaign campaign)
    {
        // поднимаем данные по олимпиадам - инициализируем кеш
        Map<Long, List<Discipline2OlympiadDiplomaRelation>> olympiadMapCache = DaoCache.get(OLYMPIAD_CAHE_KEY);
        if (olympiadMapCache == null)
        {
            final List<Discipline2OlympiadDiplomaRelation> olympiadList = new DQLSelectBuilder()
                    .fromEntity(Discipline2OlympiadDiplomaRelation.class, "o").column(property("o"))
                    .where(eq(property(Discipline2OlympiadDiplomaRelation.diploma().entrant().enrollmentCampaign().fromAlias("o")), value(campaign)))
                    .order(property(Discipline2OlympiadDiplomaRelation.diploma().entrant().id().fromAlias("o")), OrderDirection.asc)
                    .order(property(Discipline2OlympiadDiplomaRelation.diploma().id().fromAlias("o")), OrderDirection.asc)
                    .createStatement(getSession()).list();

            olympiadMapCache = new HashMap<>();
            for (Discipline2OlympiadDiplomaRelation olympiad : olympiadList)
            {
                SafeMap.safeGet(olympiadMapCache, olympiad.getDiploma().getEntrant().getId(), LinkedList.class)
                        .add(olympiad);
            }

            DaoCache.put(OLYMPIAD_CAHE_KEY, olympiadMapCache);
        }
        return olympiadMapCache;
    }


    @Override
    public PersonBenefit getPersonBenefit(Entrant entrant)
    {
        return getBenefitMapCache(entrant.getEnrollmentCampaign()).get(entrant.getPerson().getId());
    }

    protected Map<Long, PersonBenefit> getBenefitMapCache(EnrollmentCampaign campaign)
    {
        Map<Long, PersonBenefit> benefitMapCache = DaoCache.get(BENEFIT_CAHE_KEY);
        if (benefitMapCache == null)
        {
            final List<PersonBenefit> personBenefitList = new DQLSelectBuilder()
                    .fromEntity(PersonBenefit.class, "b")
                    .column(property("b"))
                    .where(exists(Entrant.class, Entrant.enrollmentCampaign().s(), campaign))
                    .createStatement(getSession()).list();

            benefitMapCache = new HashMap<>();
            for (PersonBenefit benefit : personBenefitList)
            {
                PersonBenefit addedBenefit = benefitMapCache.get(benefit.getPerson().getId());
                if (addedBenefit == null)
                {
                    benefitMapCache.put(benefit.getPerson().getId(), benefit);
                }
                else
                {
                    if (benefit.getId() < addedBenefit.getId())
                        benefitMapCache.put(benefit.getPerson().getId(), benefit);
                }
            }

            DaoCache.put(BENEFIT_CAHE_KEY, benefitMapCache);
        }

        return benefitMapCache;
    }
}
