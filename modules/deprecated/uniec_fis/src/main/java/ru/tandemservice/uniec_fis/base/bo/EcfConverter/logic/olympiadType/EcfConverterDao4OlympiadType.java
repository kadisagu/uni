/**
 *$Id$
 */
package ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.olympiadType;

import ru.tandemservice.uniec.entity.catalog.OlympiadDiplomaDegree;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.EcfConverterStaticDao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Alexander Shaburov
 * @since 10.01.13
 */
public class EcfConverterDao4OlympiadType extends EcfConverterStaticDao<OlympiadDiplomaDegree, String> implements IEcfConverterDao4OlympiadType
{
    /* { uni.code, uni.title, fis.code, fis.title } */
    private static final String[][] DATA = {
        { "1", "победитель", "1", "победитель"},
        { "2", "призер",     "2", "призер"}
    };

    /* проверка того, что в ФИС ничего не поменялось { fis.code -> fis.title } */
    private static final Map<String, String> STATIC_TITLE_CHECK_MAP = new HashMap<String, String>();

    /* сопоставление элементов { uni.code -> fis.code }*/
    private static final Map<String, String> STATIC_ITEM_CODE_MAP = new HashMap<String, String>();

    static
    {
        for (final String[] row: DATA)
        {
            if (null != row[2])
            {
                if (null != STATIC_ITEM_CODE_MAP.put(row[0], row[2]))
                    throw new IllegalStateException("STATIC_ITEM_CODE_MAP: Duplicate value for key=«"+row[0]+"»");

                final String prev = STATIC_TITLE_CHECK_MAP.put(row[2], row[3]);
                if (null != prev && !prev.equals(row[3]))
                    throw new IllegalStateException("COUNTRY_CHECK_MAP: Duplicate value for key=«"+row[2]+"»");
            }
        }
    }

    @Override
    protected Map<String, String> getStaticTitleCheckMap()
    {
        return STATIC_TITLE_CHECK_MAP;
    }

    @Override
    protected Map<String, String> getStaticItemCodeMap()
    {
        return STATIC_ITEM_CODE_MAP;
    }

    @Override
    public String getEcfCatalogCode()
    {
        return "18";
    }

    @Override
    protected List<OlympiadDiplomaDegree> getEntityList()
    {
        return getCatalogItemList(OlympiadDiplomaDegree.class);
    }

    @Override
    protected String getItemMapKey(OlympiadDiplomaDegree entity)
    {
        return entity.getCode();
    }
}
