package ru.tandemservice.uniec_fis.base.bo.EcfCatalog.logic;

import fis.schema.catalog_items.resp.DictionaryData;
import org.apache.commons.lang.StringUtils;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.EcfConverterManager;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author vdanilov
 */
public interface DictionaryDataHandler<T, I>
{
    Class<T> getResponseKlass();

    Long getCatalogCode(T response);
    String getCatalogName(T response);

    Collection<I> getItems(T response);

    Long getItemID(I item);
    String getItemName(I item);


    static class Default implements DictionaryDataHandler<fis.schema.catalog_items.resp.DictionaryData, fis.schema.catalog_items.resp.DictionaryData.DictionaryItems.DictionaryItem> {

        @Override public
        Class<fis.schema.catalog_items.resp.DictionaryData> getResponseKlass() {
            return fis.schema.catalog_items.resp.DictionaryData.class;
        }

        @Override public Long
        getCatalogCode(final fis.schema.catalog_items.resp.DictionaryData response) {
            return response.getCode(); // чертов ФИС, достали, пусть уже разбирутся с типами
        }

        @Override public String
        getCatalogName(final fis.schema.catalog_items.resp.DictionaryData response) {
            return response.getName();
        }

        @Override public Collection<fis.schema.catalog_items.resp.DictionaryData.DictionaryItems.DictionaryItem>
        getItems(final fis.schema.catalog_items.resp.DictionaryData response) {
            return response.getDictionaryItems().getDictionaryItem();
        }

        @Override public Long
        getItemID(final fis.schema.catalog_items.resp.DictionaryData.DictionaryItems.DictionaryItem item) {
            return item.getID();
        }

        @Override public String
        getItemName(final fis.schema.catalog_items.resp.DictionaryData.DictionaryItems.DictionaryItem item) {
            return item.getName();
        }
    }


    // стандартная обработка справочника
    final DictionaryDataHandler CATALOG_DEFAULT = new Default();

    // Общеобразовательные предметы (с проверкой на использование в пользовательских справочниках и проверкой названий)
    final DictionaryDataHandler CATALOG_1_DISCIPLINE = new Default()
    {
        @Override
        public String getItemName(DictionaryData.DictionaryItems.DictionaryItem item)
        {
            return EcfConverterManager.instance().stateExamSubject().checkTitle(
                String.valueOf(getItemID(item)),
                item.getName()
            );
        }

        @Override
        public Collection<DictionaryData.DictionaryItems.DictionaryItem> getItems(DictionaryData response)
        {
            Map<Long, String> itemMap = new HashMap<>();
            for (DictionaryData.DictionaryItems.DictionaryItem item : super.getItems(response))
            {
                final Long id = getItemID(item);
                final String name = StringUtils.trimToEmpty(getItemName(item));
                itemMap.put(id, name);
            }
            EcfConverterManager.instance().setDiscipline().validateUsedInUserConverter(itemMap);

            return super.getItems(response);
        }
    };

    // обработка справочника страны (с проверкой)
    final DictionaryDataHandler CATALOG_7_COUNTRIES = new Default() {
        @Override public String
        getItemName(final fis.schema.catalog_items.resp.DictionaryData.DictionaryItems.DictionaryItem item) {
            return EcfConverterManager.instance().addressCountry().checkTitle(
                String.valueOf(getItemID(item)),
                item.getName()
            );
        }
    };

    // обработка справочника форма обучения (с проверкой)
    final DictionaryDataHandler CATALOG_14_EDUFORM = new Default() {
        @Override public String
        getItemName(final fis.schema.catalog_items.resp.DictionaryData.DictionaryItems.DictionaryItem item) {
            return EcfConverterManager.instance().educationForm().checkTitle(
                String.valueOf(getItemID(item)),
                item.getName()
            );
        }
    };

    // обработка справочника Тип вступительных испытаний (с проверкой)
    final DictionaryDataHandler CATALOG_11_DISCIPLINE = new Default() {
        @Override public String
        getItemName(final fis.schema.catalog_items.resp.DictionaryData.DictionaryItems.DictionaryItem item) {
            return EcfConverterManager.instance().educationForm().checkTitle(
                String.valueOf(getItemID(item)),
                item.getName()
            );
        }
    };

    // обработка справочника Тип вступительных испытаний (с проверкой)
    final DictionaryDataHandler CATALOG_18_OLYMP_TYPE = new Default() {
        @Override public String
        getItemName(final fis.schema.catalog_items.resp.DictionaryData.DictionaryItems.DictionaryItem item) {
            return EcfConverterManager.instance().educationForm().checkTitle(
                String.valueOf(getItemID(item)),
                item.getName()
            );
        }
    };


    // направления подготовки (10) (с проверкой на использование в пользовательских справочниках)
    final DictionaryDataHandler CATALOG_10_SPEC = new DictionaryDataHandler<fis.schema.catalog_items.resp.spec.DictionaryData, fis.schema.catalog_items.resp.spec.DictionaryData.DictionaryItems.DictionaryItem>() {

        @Override public Class<fis.schema.catalog_items.resp.spec.DictionaryData>
        getResponseKlass() {
            return fis.schema.catalog_items.resp.spec.DictionaryData.class;
        }

        @Override public Long
        getCatalogCode(final fis.schema.catalog_items.resp.spec.DictionaryData response) {
            return response.getCode(); // чертов ФИС, достали, пусть уже разбирутся с типами
        }

        @Override public String
        getCatalogName(final fis.schema.catalog_items.resp.spec.DictionaryData response) {
            return response.getName();
        }

        @Override public Collection<fis.schema.catalog_items.resp.spec.DictionaryData.DictionaryItems.DictionaryItem>
        getItems(final fis.schema.catalog_items.resp.spec.DictionaryData response) {
            Map<Long, String> itemMap = new HashMap<>();
            for (fis.schema.catalog_items.resp.spec.DictionaryData.DictionaryItems.DictionaryItem item : response.getDictionaryItems().getDictionaryItem())
            {
                final Long id = getItemID(item);
                final String name = StringUtils.trimToEmpty(getItemName(item));
                itemMap.put(id, name);
            }
            EcfConverterManager.instance().educationOrgUnit().validateUsedInUserConverter(itemMap);

            return response.getDictionaryItems().getDictionaryItem();
        }

        @Override public Long
        getItemID(final fis.schema.catalog_items.resp.spec.DictionaryData.DictionaryItems.DictionaryItem item) {
            return item.getID();
        }

        @Override public String
        getItemName(final fis.schema.catalog_items.resp.spec.DictionaryData.DictionaryItems.DictionaryItem item) {
	        String code;
	        if(StringUtils.isNotBlank(item.getNewCode()))
	        {
		        code = item.getNewCode();
	        }
	        else
	        {
		        code = item.getCode() == null ? "" : item.getCode();
	        }
            return code + "." + item.getQualificationCode() + " " + item.getName() + " (" + item.getPeriod() + ")";
        }
    };

    // олимпиады (19) (с проверкой на использование в пользовательских справочниках)
    final DictionaryDataHandler CATALOG_19_OLYMP = new DictionaryDataHandler<fis.schema.catalog_items.resp.olymp.DictionaryData, fis.schema.catalog_items.resp.olymp.DictionaryData.DictionaryItems.DictionaryItem>() {

        @Override public Class<fis.schema.catalog_items.resp.olymp.DictionaryData>
        getResponseKlass() {
            return fis.schema.catalog_items.resp.olymp.DictionaryData.class;
        }

        @Override public Long
        getCatalogCode(final fis.schema.catalog_items.resp.olymp.DictionaryData response) {
            return response.getCode(); // чертов ФИС, достали, пусть уже разбирутся с типами
        }

        @Override public String
        getCatalogName(final fis.schema.catalog_items.resp.olymp.DictionaryData response) {
            return response.getName();
        }

        @Override public Collection<fis.schema.catalog_items.resp.olymp.DictionaryData.DictionaryItems.DictionaryItem>
        getItems(final fis.schema.catalog_items.resp.olymp.DictionaryData response) {
            Map<Long, String> itemMap = new HashMap<>();
            for (fis.schema.catalog_items.resp.olymp.DictionaryData.DictionaryItems.DictionaryItem item : response.getDictionaryItems().getDictionaryItem())
            {
                final Long id = getItemID(item);
                final String name = StringUtils.trimToEmpty(getItemName(item));
                itemMap.put(id, name);
            }
            EcfConverterManager.instance().olympiad().validateUsedInUserConverter(itemMap);

            return response.getDictionaryItems().getDictionaryItem();
        }

        @Override public Long
        getItemID(final fis.schema.catalog_items.resp.olymp.DictionaryData.DictionaryItems.DictionaryItem item) {
            return item.getOlympicID();
        }

        @Override public String
        getItemName(final fis.schema.catalog_items.resp.olymp.DictionaryData.DictionaryItems.DictionaryItem item) {
            return "["+item.getOlympicLevelID()+"] № "+item.getOlympicNumber()+" "+item.getOlympicName();
        }
    };


}