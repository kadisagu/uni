package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.ui.PackagePub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;

import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage;

/**
 * @author vdanilov
 */
@State({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "holder.id", required=true)
})
public class EcfOrgUnitPackagePubUI extends UIPresenter {

    private final EntityHolder<EcfOrgUnitPackage> holder = new EntityHolder<EcfOrgUnitPackage>();
    public EntityHolder<EcfOrgUnitPackage> getHolder() { return this.holder; }
    public EcfOrgUnitPackage getOrgUnitPackage() { return getHolder().getValue(); }

    private OrgUnitSecModel _secModel;
    public  OrgUnitSecModel getSecModel() { return _secModel; }

    @Override
    public void onComponentRefresh() {
        getHolder().refresh();
        _secModel = new OrgUnitSecModel(getOrgUnitPackage().getEcfOrgUnit().getOrgUnit());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        dataSource.put("owner", getOrgUnitPackage());
    }


}
