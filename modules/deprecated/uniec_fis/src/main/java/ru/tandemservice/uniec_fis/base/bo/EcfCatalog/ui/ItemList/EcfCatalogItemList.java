/* $Id: CommonCatalogList.java 6 2012-04-04 12:30:37Z vzhukov $ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec_fis.base.bo.EcfCatalog.ui.ItemList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.util.NumberAsStringComparator;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.logic.ItemListDataHandler;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

@Configuration
public class EcfCatalogItemList extends BusinessComponentManager
{

    public static final String ECF_CATALOG_ITEM_DS = "ecfCatalogItemDS";
    public static final String ECF_CATALOG_ITEM_CODE_DS = "ecfCatalogItemCodeDS";

    @Bean
    public ColumnListExtPoint ecfCatalogItemDS() {
        return this.columnListExtPointBuilder(ECF_CATALOG_ITEM_DS)
        .addColumn(textColumn("fisCatalogCode", EcfCatalogItem.fisCatalogCode()).width("1px").create())
        .addColumn(textColumn("fisItemCode", EcfCatalogItem.fisItemCode()).width("1px").create())
        .addColumn(textColumn("title", EcfCatalogItem.title()).create())
        .create();
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
                .addDataSource(selectDS(ECF_CATALOG_ITEM_CODE_DS, ecfCatalogItemCodeDSHandler()))
                .addDataSource(this.searchListDS(ECF_CATALOG_ITEM_DS, this.ecfCatalogItemDS()).handler(this.ecfCatalogItemDSHandler()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> ecfCatalogItemDSHandler() {
        return new ItemListDataHandler(this.getName()).order(EcfCatalogItem.fisCatalogCode());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> ecfCatalogItemCodeDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
        {
            @Override
            protected void initFromList(Map<Long, Object> resultMap, Object listObj)
            {
                final List<String> codeList = new DQLSelectBuilder().fromEntity(EcfCatalogItem.class, "c").column(property(EcfCatalogItem.fisCatalogCode().fromAlias("c")))
                        .predicate(DQLPredicateType.distinct)
                        .createStatement(getSession()).list();

                Collections.sort(codeList, NumberAsStringComparator.INSTANCE);
                super.initFromList(resultMap, codeList);
            }
        }
                .filtered(true);
    }
}
