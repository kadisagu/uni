/* $Id: DppOrgUnitAddEdit.java 22487 2012-04-04 13:16:00Z vzhukov $ */
package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.ui.AddPkg4Admission;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

@Configuration
public class EcfOrgUnitAddPkg4Admission extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder().create();
    }

}
