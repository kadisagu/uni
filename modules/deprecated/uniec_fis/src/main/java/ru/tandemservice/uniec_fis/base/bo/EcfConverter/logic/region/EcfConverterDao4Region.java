/* $Id$ */
package ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.region;


import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.gen.EcfCatalogItemGen;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4Region;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.gen.EcfConv4RegionGen;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.EcfConverterBaseDao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 12.05.2016
 */
public class EcfConverterDao4Region extends EcfConverterBaseDao<AddressItem, Long> implements IEcfConverterDao4Region
{
    @Override
    public String getEcfCatalogCode()
    {
        return "8";
    }

    @Override
    protected List<AddressItem> getEntityList()
    {
        String alias = "reg";
        return getList(
                new DQLSelectBuilder()
                        .fromEntity(AddressItem.class, alias)
                        .column(property(alias))
                        .where(isNull(property(alias, AddressItem.parent())))
                        .where(eq(property(alias, AddressItem.country().code()), value(IKladrDefines.RUSSIA_COUNTRY_CODE)))
                        .order(property(alias, AddressItem.title()))
        );
    }

    @Override
    protected Long getItemMapKey(AddressItem entity)
    {
        return entity.getId();
    }

    @Override
    protected Map<Long, Long> getItemMap()
    {
        return EcfConverterBaseDao.<Long, Long>map(scrollRows(
                new DQLSelectBuilder()
                        .fromEntity(EcfConv4Region.class, "x")
                        .column(property("x", EcfConv4Region.region().id()), "reg_id")
                        .column(property("x", EcfConv4Region.value().id()), "value_id")
                        .createStatement(this.getSession())
        ));
    }

    @Override
    public void update(final Map<AddressItem, EcfCatalogItem> values, final boolean clearNulls)
    {
        // формируем перечень требуемых строк
        final List<EcfConv4Region> targetRecords = values.entrySet().stream()
                .filter(e -> e.getValue() != null)
                .map(e -> new EcfConv4Region(e.getKey(), e.getValue()))
                .collect(Collectors.toList());

        // обновляем данные в базе
        new MergeAction.SessionMergeAction<EcfConv4RegionGen.NaturalId, EcfConv4Region>()
        {
            @Override
            protected EcfConv4RegionGen.NaturalId key(final EcfConv4Region source)
            {
                return (EcfConv4RegionGen.NaturalId) source.getNaturalId();
            }

            @Override
            protected EcfConv4Region buildRow(final EcfConv4Region source)
            {
                return new EcfConv4Region(source.getRegion(), source.getValue());
            }

            @Override
            protected void fill(final EcfConv4Region target, final EcfConv4Region source)
            {
                target.update(source, false);
            }

            @Override
            protected void doDeleteRecord(final EcfConv4Region databaseRecord)
            {
                if (clearNulls)
                {
                    super.doDeleteRecord(databaseRecord);
                }
            }
        }.merge(this.getList(EcfConv4Region.class), targetRecords);

    }

    @Override
    public void doAutoSync()
    {
        final Map<AddressItem, EcfCatalogItem> values = new HashMap<>();

        Map<String, EcfCatalogItem> itemByCodeMap = getList(EcfCatalogItem.class, EcfCatalogItem.fisCatalogCode(), "8")
                .stream().collect(Collectors.toMap(EcfCatalogItemGen::getFisItemCode, item -> item));

        getEntityList().forEach(reg->{
            EcfCatalogItem catalogItem = itemByCodeMap.get(reg.getInheritedRegionCode());
            if(catalogItem!=null)
                values.put(reg, catalogItem);
        });

        getList(EcfConv4Region.class).stream()
                .filter(reg -> reg.getValue() != null)
                .forEach(reg -> values.put(reg.getRegion(), reg.getValue()));

        this.update(values, false);
    }
}