/**
 *$Id$
 */
package ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.olympiad;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniec.entity.entrant.OlympiadDiploma;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4Olympiad;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.EcfConverterBaseDao;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 10.01.13
 */
public class EcfConverterDao4Olympiad extends EcfConverterBaseDao<IdentifiableWrapper, String> implements IEcfConverterDao4Olympiad
{
    @Override
    public String getEcfCatalogCode()
    {
        return "19";
    }

    @Override
    protected List<IdentifiableWrapper> getEntityList()
    {
        final List<String> list = new DQLSelectBuilder().fromEntity(OlympiadDiploma.class, "o")
                .column(property(OlympiadDiploma.olympiad().fromAlias("o")))
                .predicate(DQLPredicateType.distinct)
                .order(property(OlympiadDiploma.olympiad().fromAlias("o")))
                .createStatement(getSession()).list();

        Map<String, IdentifiableWrapper> resultMap = new LinkedHashMap<>();
        for (String title : list)
            resultMap.put(title.toUpperCase(), new IdentifiableWrapper((long) title.hashCode(), StringUtils.capitalize(title.toLowerCase())));

        return new ArrayList<>(resultMap.values());
    }

    @Override
    protected String getItemMapKey(IdentifiableWrapper entity)
    {
        return entity.getTitle().toUpperCase();
    }

    @Override
    protected Map<String, Long> getItemMap()
    {
        return EcfConverterBaseDao.<String, Long>map(scrollRows(
                new DQLSelectBuilder()
                        .fromEntity(EcfConv4Olympiad.class, "x")
                        .column(DQLFunctions.upper(property(EcfConv4Olympiad.olympiadTitle().fromAlias("x"))), "lvl_id")
                        .column(property(EcfConv4Olympiad.value().id().fromAlias("x")), "value_id")
                        .createStatement(getSession())
        ));
    }

    @Override
    public void update(Map<IdentifiableWrapper, EcfCatalogItem> values, final boolean clearNulls)
    {
        // формируем перечень требуемых строк
        final List<EcfConv4Olympiad> targetRecords = new ArrayList<EcfConv4Olympiad>(values.size());
        for (final Map.Entry<IdentifiableWrapper, EcfCatalogItem> e: values.entrySet())
        {
            if (null != e.getValue())
                targetRecords.add(new EcfConv4Olympiad(getItemMapKey(e.getKey()), e.getValue()));
        }

        // обновляем данные в базе
        new MergeAction.SessionMergeAction<EcfConv4Olympiad.NaturalId, EcfConv4Olympiad>()
        {
            @Override
            protected EcfConv4Olympiad.NaturalId key(final EcfConv4Olympiad source)
            {
                return (EcfConv4Olympiad.NaturalId) source.getNaturalId();
            }
            @Override
            protected EcfConv4Olympiad buildRow(final EcfConv4Olympiad source)
            {
                return new EcfConv4Olympiad(source.getOlympiadTitle(), source.getValue());
            }
            @Override
            protected void fill(final EcfConv4Olympiad target, final EcfConv4Olympiad source)
            {
                target.update(source, false);
            }
            @Override
            protected void doDeleteRecord(final EcfConv4Olympiad databaseRecord)
            {
                if (clearNulls) { super.doDeleteRecord(databaseRecord); }
            }
        }.merge(this.getList(EcfConv4Olympiad.class), targetRecords);
    }

    @Override
    protected List<EcfCatalogItem> getUsedFisItemList()
    {
        return new DQLSelectBuilder().fromEntity(EcfConv4Olympiad.class, "b").column(property(EcfConv4Olympiad.value().fromAlias("b")))
                .where(isNotNull(property(EcfConv4Olympiad.value().fromAlias("b"))))
                .createStatement(getSession()).list();
    }

    @Override
    public void doAutoSync()
    {
        final List<String> titleConvList = new DQLSelectBuilder().fromEntity(EcfConv4Olympiad.class, "b")
                .column(property(EcfConv4Olympiad.olympiadTitle().fromAlias("b")))
                .createStatement(getSession()).list();

        final List<EcfCatalogItem> ecfCatalogItemList = new DQLSelectBuilder().fromEntity(EcfCatalogItem.class, "i").column(property("i"))
                .where(eq(property(EcfCatalogItem.fisCatalogCode().fromAlias("i")), value(getEcfCatalogCode())))
                .createStatement(getSession()).list();

        final List<String> existConvList = new LinkedList<>();
        for (String title : titleConvList)
            existConvList.add(title.toUpperCase());

        final Map<String, EcfCatalogItem> nameItemMap = new HashMap<>();
        for (EcfCatalogItem catalogItem : ecfCatalogItemList)
        {
            // в названии олимпиад у нас префиксом являются Номер и Уровень олимпиады, поэтому отсекаем их и берем только Название(пример: "[4] № 2 Байкальская олимпиада школьников" -> "Байкальская олимпиада школьников")
            String title = catalogItem.getTitle();
            title = title.replace(title.split(" ")[0], "").replace(title.split(" ")[1], "").replace(title.split(" ")[2], "").trim();
            if (!nameItemMap.containsKey(title.toUpperCase()))
                nameItemMap.put(title.toUpperCase(), catalogItem);
        }

        Set<String> titleSet = new HashSet<>();
        for (OlympiadDiploma olympiad : getList(OlympiadDiploma.class))
            titleSet.add(olympiad.getOlympiad().toUpperCase());

        for (String titel : titleSet)
        {
            if (existConvList.contains(titel))
                continue;

            EcfCatalogItem catalogItem = nameItemMap.get(titel.toUpperCase());
            if (catalogItem != null)
                save(new EcfConv4Olympiad(titel, catalogItem));
        }
    }
}
