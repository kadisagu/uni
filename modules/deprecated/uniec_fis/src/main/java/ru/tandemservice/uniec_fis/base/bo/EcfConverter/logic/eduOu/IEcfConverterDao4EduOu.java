package ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.eduOu;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.shared.commonbase.base.util.Wiki;

import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.IEcfConverterDao;

/**
 * @author vdanilov
 */
public interface IEcfConverterDao4EduOu extends IEcfConverterDao<EducationOrgUnit> {

    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doAutoSync();

    /** @return ecfCatalogItem.fisItemCode | ecfCatalogItem.fisCatalogCode='2' (Уровень образования) */
    @Wiki(url="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=9864743")
    EcfCatalogItem getEcfItem4EducationLevel(EducationOrgUnit eduOu);

}
