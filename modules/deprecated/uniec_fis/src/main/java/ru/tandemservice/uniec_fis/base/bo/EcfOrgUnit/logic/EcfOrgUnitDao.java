package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.logic;

import org.apache.commons.collections15.CollectionUtils;
import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.DaoCache;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.entity.entrant.CompetitionGroup;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.finSources.EcfFinSourceDao;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.EcfOrgUnitManager;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.gen.EcfOrgUnitGen;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.max;

/**
 * @author vdanilov
 */
public class EcfOrgUnitDao extends UniBaseDao implements IEcfOrgUnitDao
{
    private static final String ECF_ORG_UNIT_DAO_ACQUISITION_CACHE = "EcfOrgUnitDao.acquisitionCache";

    // блокировка
    protected Session lock(final OrgUnit orgUnit)
    {
        if (null == orgUnit) {
            throw new NullPointerException("orgUnit is null");
        }

        // блокируем стандартным механизмом
        return this.lock("EcfOrgUnit."+orgUnit.getId());
    }

    // кэш { ou.id -> ecfOu.id } с заимствованием
    protected Map<Long, Long> getAcquisitionCache() {
        Map<Long, Long> acquisitionCache = DaoCache.get(ECF_ORG_UNIT_DAO_ACQUISITION_CACHE);
        if (null == acquisitionCache) {
            DaoCache.put(ECF_ORG_UNIT_DAO_ACQUISITION_CACHE, acquisitionCache = new HashMap<>());
            final List<EcfOrgUnit> list = this.getList(EcfOrgUnit.class, EcfOrgUnit.removalDate(), (Date)null);
            for (final EcfOrgUnit ecfOrgUnit: list) {
                if (null != acquisitionCache.put(ecfOrgUnit.getOrgUnit().getId(), ecfOrgUnit.getId())) {
                    throw new IllegalStateException("duplicate-ecf-ou-for: " + ecfOrgUnit.getOrgUnit().getId());
                }
            }
        }
        return acquisitionCache;
    }

    private Long getAcquiredEcfOrgUnit_lookUp(final Map<Long, Long> acquisitionCache, final OrgUnit orgUnit)
    {
        // если подразделения нет - то сразу null
        if (null == orgUnit) { return null; }

        // смотрим в кэше
        Long ecfOrgUnitId = acquisitionCache.get(orgUnit.getId());
        if (null == ecfOrgUnitId) {
            // если нет - смотрим для додительского OU
            acquisitionCache.put(orgUnit.getId(), ecfOrgUnitId = this.getAcquiredEcfOrgUnit_lookUp(acquisitionCache, orgUnit.getParent()));
        }
        return ecfOrgUnitId;
    }

    @Override
    public EcfOrgUnit getAcquiredEcfOrgUnit(final OrgUnit orgUnit)
    {
        // ищем элемент в иерархии вверх, если его нет - ругамеся (он должен быть всегда как минимум для академии)
        final Long ecfOrgUnitId = this.getAcquiredEcfOrgUnit_lookUp(this.getAcquisitionCache(), orgUnit);
        if (null == ecfOrgUnitId) { throw new IllegalStateException("no-ecf-org-unit:" + orgUnit.getId()); }

        // возвращаем найденный элемент
        return this.get(EcfOrgUnit.class, ecfOrgUnitId);
    }

    @Override
    public EcfOrgUnit getEcfOrgUnit(final OrgUnit orgUnit) {
        return this.getByNaturalId(new EcfOrgUnitGen.NaturalId(orgUnit));
    }

    protected EcfOrgUnit doCreateOrgUnit(final OrgUnit orgUnit)
    {
        // если что-то уже сохранено в базу - отлично - его и берем
        EcfOrgUnit ecfOu = this.getEcfOrgUnit(orgUnit);
        if (null != ecfOu) { return ecfOu; }

        this.lock(orgUnit); // локальная блокировка

        // создаем orgUnit и сохраняем его в базу
        ecfOu = new EcfOrgUnit(orgUnit);
        ecfOu.setUid(generateNextUid()); // здесь глобальная блокировка
        this.save(ecfOu);
        return ecfOu;
    }

    private long generateNextUid()
    {
        this.lock(EcfOrgUnit.class.getName()); // глобальная блокировка (выделение uid)

        Long uid = new DQLSelectBuilder()
        .fromEntity(EcfOrgUnit.class, "ecfOu")
        .column(max(property(EcfOrgUnit.uid().fromAlias("ecfOu"))))
        .createStatement(getSession())
        .uniqueResult();
        if (null == uid) { uid = 0L; }

        return (1L + uid);
    }

    @Override
    public EcfOrgUnit initDefaultExfOrgUnit() {
        return this.doCreateOrgUnit(TopOrgUnit.getInstance());
    }

    @Override
    public void save(final EcfOrgUnit ecfOrgUnit) {
        final Session session = this.lock(ecfOrgUnit.getOrgUnit());
        if (ecfOrgUnit.getId() == null)
        {
            /* Согласно задаче DEV-4588 было решено править баг в версиях 2,2,5, 2.5.1 (на тот момент стабильная) и 2,5.2 (на тот момент нестабильная). В промежуточные версии изменения не вносились. */
            ecfOrgUnit.setUid(generateNextUid());
        }
        session.saveOrUpdate(ecfOrgUnit);
    }

    @Override
    public void delete(final EcfOrgUnit ecfOrgUnit)
    {
        // удалять корневые ОУ нельзя - придется вставлять проверку
        if (ecfOrgUnit.isTopOrgUnit()) {
            throw new ApplicationException(
                EcfOrgUnitManager.instance().getProperty("error.delete.orgunit-is-top-orgunit")
            );
        }

        // а вот теперь можно удалять
        final Session session = this.lock(ecfOrgUnit.getOrgUnit());
        session.delete(ecfOrgUnit);
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList(final EcfOrgUnit ecfOrgUnit)
    {
        // @Wiki(url="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=9866163")
        // фильтруем по принадлежности к подразделению (через формирующее)

        final Map<Long, OrgUnit> ouMap = this.getLoadCacheMap(OrgUnit.class);
        final Map<Long, Long> acquisitionCache = this.getAcquisitionCache();

        final Iterable<Object[]> rows = scrollRows(
            new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, "ed")
            .column(property(EnrollmentDirection.enrollmentCampaign().id().fromAlias("ed")))
            .column(property(EnrollmentDirection.educationOrgUnit().formativeOrgUnit().id().fromAlias("ed")))
            .predicate(DQLPredicateType.distinct)
            .createStatement(this.getSession())
        );

        final Set<Long> selectedIds = new HashSet<>();
        for (final Object[] row: rows) {
            final Long ecId = (Long)row[0];
            if (selectedIds.contains(ecId)) { continue; }

            final OrgUnit orgUnit = ouMap.get(row[1]);
            final Long ecfOrgUnitId = this.getAcquiredEcfOrgUnit_lookUp(acquisitionCache, orgUnit);
            if (ecfOrgUnit.getId().equals(ecfOrgUnitId)) {
                selectedIds.add(ecId);
            }
        }

        return this.getList(EnrollmentCampaign.class, EnrollmentCampaign.id(), selectedIds, EnrollmentCampaign.educationYear().intValue().s(), EnrollmentCampaign.title().s());
    }

    /** @return список НП приемной каспании, доступных на указанном подразделении ФИС */
    @Override
    public List<EnrollmentDirection> getEnrollmentDirections(final EcfOrgUnit ecfOrgUnit, final EnrollmentCampaign enrollmentCampaign) {

        // исключить из обработки НПП с формой освоения "Экстернат" и "Самостоятельное обучение и итоговая аттестация"
        final List<EnrollmentDirection> directions = new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, "d").column(property("d"))
                .where(eq(property(EnrollmentDirection.enrollmentCampaign().fromAlias("d")), value(enrollmentCampaign)))
                .where(notIn(property(EnrollmentDirection.educationOrgUnit().developForm().code().fromAlias("d")), Arrays.asList(DevelopFormCodes.EXTERNAL_FORM, DevelopFormCodes.APPLICANT_FORM)))
                .createStatement(getSession()).list();

        // оставляем только те, для который допустим прием на бюджет или контракт (и задан план приема)
        CollectionUtils.filter(directions, dir -> EcfFinSourceDao.isBudget(dir) || EcfFinSourceDao.isContract(dir) || EcfFinSourceDao.isTargetAdmission(dir));

        if (null != ecfOrgUnit) {
            // @Wiki(url="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=9866163")
            // фильтруем по принадлежности к подразделению (через формирующее)
            final IEcfOrgUnitDao dao = EcfOrgUnitManager.instance().dao();
            CollectionUtils.filter(directions, (EnrollmentDirection direction) -> {
                final EducationOrgUnit eduOu = direction.getEducationOrgUnit();
                final EcfOrgUnit acquiredEcfOrgUnit = dao.getAcquiredEcfOrgUnit(eduOu.getFormativeOrgUnit());
                return ecfOrgUnit.equals(acquiredEcfOrgUnit);
            });
        }


        if (enrollmentCampaign.isUseCompetitionGroup()) {
            for (EnrollmentDirection dir: directions) {
                CompetitionGroup competitionGroup = dir.getCompetitionGroup();
                if (null == competitionGroup) {
                    throw new ApplicationException(
                        EcfOrgUnitManager.instance().getProperty("error.ecg.is-null", dir.getTitle())
                    );
                }
                List<EnrollmentDirection> groupDirs = getList(EnrollmentDirection.class, EnrollmentDirection.competitionGroup(), competitionGroup);
                for (EnrollmentDirection groupDir: groupDirs) {
                    if (!directions.contains(groupDir)) {
                        throw new ApplicationException(
                            EcfOrgUnitManager.instance().getProperty("error.ecg.is-not-closed", competitionGroup.getTitle(), dir.getTitle(), groupDir.getTitle())
                        );
                    }
                }
            }
        }


        return directions;
    }


}
