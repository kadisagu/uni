package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity;

import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.gen.EcfOrgUnitPackage4AdmissionOrdersGen;

/**
 * Пакет: Списки заявлений абитуриентов, включенных в приказ
 *
 * Root / PackageData / OrdersOfAdmission
 */
public class EcfOrgUnitPackage4AdmissionOrders extends EcfOrgUnitPackage4AdmissionOrdersGen
{
    @Override
    public String getTitle() {
        if (getEnrollmentCampaign() == null) {
            return this.getClass().getSimpleName();
        }
        return "Пакет 4: Списки заявлений абитуриентов, включенных в приказ «" + getEnrollmentCampaign().getTitle() + "»";
    }
}