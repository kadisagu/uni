package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.gen.EcfOrgUnitPackageGen;
import ru.tandemservice.uniec_fis.util.FisServiceImpl;

import java.util.Date;

/**
 * Пакет
 */
public class EcfOrgUnitPackage extends EcfOrgUnitPackageGen implements ITitled
{
    public void setXmlPackageRequest(byte[] content) { this.setXmlPackage(content); }
    public byte[] getXmlPackageRequest() { return getXmlPackage(); }

    public String getFormattedXmlPackageRequest() {
        return FisServiceImpl.prettyFormat(getXmlPackageRequest());
    }

    @Override
    @EntityDSLSupport
    public Date getPackageRequestDate() {
        EcfOrgUnitPackageIOLog io = getPkgImportSuccess();
        return (null == io ? null : io.getOperationDate());
    }

    @Override
    @EntityDSLSupport
    public Date getPackageResultDate() {
        EcfOrgUnitPackageIOLog io = getPkgResultSuccess();
        return (null == io ? null : io.getOperationDate());
    }

    @Override
    @EntityDSLSupport
    public String getTitle() {
        return "";
    }

//    @Override
//    @EntityDSLSupport
//    public String getStateTitle() {
//        if (null != getPkgResultSuccess()) { return "Получен результат обработки данных в ФИС"; }
//        if (null != getPkgImportSuccess()) { return "Данные переданы в ФИС"; }
//        if (isArchived()) { return "Исключен из обработки"; }
//        if (isQueued()) { return "Обрабатывается"; }
//        return "";
//    }


    @Override
    @EntityDSLSupport
    public boolean isQueued() {
        return (null != this.getQueueDate());
    }

    @Override
    @EntityDSLSupport
    public boolean isArchived() {
        return (null != this.getArchiveDate());
    }

    @Override
    @EntityDSLSupport
    public boolean isAlive() {
        return (isQueued()) && !isArchived();
    }


    @Override
    @EntityDSLSupport
    public boolean isQueueDisabled() {
        return isQueued() || isArchived();
    }

    @Override
    @EntityDSLSupport
    public boolean isArchiveDisabled() {
        return isArchived();
    }

    @Override
    @EntityDSLSupport
    public boolean isDeleteDisabled() {
        return (!isArchived()) || (null != getPkgResultSuccess()) || (null != getPkgImportSuccess());
    }


}