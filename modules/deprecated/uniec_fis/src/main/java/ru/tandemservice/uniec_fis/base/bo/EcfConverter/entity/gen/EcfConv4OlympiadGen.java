package ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4Olympiad;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * ФИС: Сопоставление: Тип диплома олимпиады ФИС
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EcfConv4OlympiadGen extends EntityBase
 implements INaturalIdentifiable<EcfConv4OlympiadGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4Olympiad";
    public static final String ENTITY_NAME = "ecfConv4Olympiad";
    public static final int VERSION_HASH = 919469590;
    private static IEntityMeta ENTITY_META;

    public static final String P_OLYMPIAD_TITLE = "olympiadTitle";
    public static final String L_VALUE = "value";

    private String _olympiadTitle;     // Название олимпиады
    private EcfCatalogItem _value;     // Значение (элемент справочника ФИС)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Название олимпиады. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getOlympiadTitle()
    {
        return _olympiadTitle;
    }

    /**
     * @param olympiadTitle Название олимпиады. Свойство не может быть null и должно быть уникальным.
     */
    public void setOlympiadTitle(String olympiadTitle)
    {
        dirty(_olympiadTitle, olympiadTitle);
        _olympiadTitle = olympiadTitle;
    }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     */
    @NotNull
    public EcfCatalogItem getValue()
    {
        return _value;
    }

    /**
     * @param value Значение (элемент справочника ФИС). Свойство не может быть null.
     */
    public void setValue(EcfCatalogItem value)
    {
        dirty(_value, value);
        _value = value;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EcfConv4OlympiadGen)
        {
            if (withNaturalIdProperties)
            {
                setOlympiadTitle(((EcfConv4Olympiad)another).getOlympiadTitle());
            }
            setValue(((EcfConv4Olympiad)another).getValue());
        }
    }

    public INaturalId<EcfConv4OlympiadGen> getNaturalId()
    {
        return new NaturalId(getOlympiadTitle());
    }

    public static class NaturalId extends NaturalIdBase<EcfConv4OlympiadGen>
    {
        private static final String PROXY_NAME = "EcfConv4OlympiadNaturalProxy";

        private String _olympiadTitle;

        public NaturalId()
        {}

        public NaturalId(String olympiadTitle)
        {
            _olympiadTitle = olympiadTitle;
        }

        public String getOlympiadTitle()
        {
            return _olympiadTitle;
        }

        public void setOlympiadTitle(String olympiadTitle)
        {
            _olympiadTitle = olympiadTitle;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EcfConv4OlympiadGen.NaturalId) ) return false;

            EcfConv4OlympiadGen.NaturalId that = (NaturalId) o;

            if( !equals(getOlympiadTitle(), that.getOlympiadTitle()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getOlympiadTitle());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getOlympiadTitle());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EcfConv4OlympiadGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EcfConv4Olympiad.class;
        }

        public T newInstance()
        {
            return (T) new EcfConv4Olympiad();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "olympiadTitle":
                    return obj.getOlympiadTitle();
                case "value":
                    return obj.getValue();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "olympiadTitle":
                    obj.setOlympiadTitle((String) value);
                    return;
                case "value":
                    obj.setValue((EcfCatalogItem) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "olympiadTitle":
                        return true;
                case "value":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "olympiadTitle":
                    return true;
                case "value":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "olympiadTitle":
                    return String.class;
                case "value":
                    return EcfCatalogItem.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EcfConv4Olympiad> _dslPath = new Path<EcfConv4Olympiad>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EcfConv4Olympiad");
    }
            

    /**
     * @return Название олимпиады. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4Olympiad#getOlympiadTitle()
     */
    public static PropertyPath<String> olympiadTitle()
    {
        return _dslPath.olympiadTitle();
    }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4Olympiad#getValue()
     */
    public static EcfCatalogItem.Path<EcfCatalogItem> value()
    {
        return _dslPath.value();
    }

    public static class Path<E extends EcfConv4Olympiad> extends EntityPath<E>
    {
        private PropertyPath<String> _olympiadTitle;
        private EcfCatalogItem.Path<EcfCatalogItem> _value;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Название олимпиады. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4Olympiad#getOlympiadTitle()
     */
        public PropertyPath<String> olympiadTitle()
        {
            if(_olympiadTitle == null )
                _olympiadTitle = new PropertyPath<String>(EcfConv4OlympiadGen.P_OLYMPIAD_TITLE, this);
            return _olympiadTitle;
        }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4Olympiad#getValue()
     */
        public EcfCatalogItem.Path<EcfCatalogItem> value()
        {
            if(_value == null )
                _value = new EcfCatalogItem.Path<EcfCatalogItem>(L_VALUE, this);
            return _value;
        }

        public Class getEntityClass()
        {
            return EcfConv4Olympiad.class;
        }

        public String getEntityName()
        {
            return "ecfConv4Olympiad";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
