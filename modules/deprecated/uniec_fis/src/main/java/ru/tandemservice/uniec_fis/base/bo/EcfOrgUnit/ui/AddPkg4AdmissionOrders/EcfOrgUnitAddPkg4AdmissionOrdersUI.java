/**
 *$Id$
 */
package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.ui.AddPkg4AdmissionOrders;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.process.*;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.hibsupport.transaction.sync.RuntimeTimeoutException;
import ru.tandemservice.uni.util.BackgroundProcessHolder;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.EcfOrgUnitManager;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.logic.EcfOrgUnitPackageDao;

import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 15.01.13
 */
@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "holder.id")
})
public class EcfOrgUnitAddPkg4AdmissionOrdersUI extends UIPresenter
{
    private final EntityHolder<EcfOrgUnit> holder = new EntityHolder<EcfOrgUnit>();

    public EntityHolder<EcfOrgUnit> getHolder() { return this.holder; }
    public EcfOrgUnit getEcfOrgUnit() { return this.getHolder().getValue(); }

    private List<EnrollmentCampaign> enrollmentCampaignList;
    public List<EnrollmentCampaign> getEnrollmentCampaignList() { return this.enrollmentCampaignList; }
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList) { this.enrollmentCampaignList = enrollmentCampaignList; }

    private EnrollmentCampaign enrollmentCampaign;
    public EnrollmentCampaign getEnrollmentCampaign() { return this.enrollmentCampaign; }
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign) { this.enrollmentCampaign = enrollmentCampaign; }


    @Override
    public void onComponentRefresh() {
        final EcfOrgUnit ecfOrgUnit = this.getHolder().refresh();

        final List<EnrollmentCampaign> enrollmentCampaignList = EcfOrgUnitManager.instance().dao().getEnrollmentCampaignList(ecfOrgUnit);
        setEnrollmentCampaignList(enrollmentCampaignList);
        if (null == getEnrollmentCampaign() && !enrollmentCampaignList.isEmpty()) {
            //            setEnrollmentCampaign(
            //                CollectionUtils.find(enrollmentCampaignList, new Predicate<EnrollmentCampaign>() {
            //                    @Override public boolean evaluate(EnrollmentCampaign ec) {
            //                        return Boolean.TRUE.equals(ec.getEducationYear().getCurrent());
            //                    }
            //                })
            //            );
            setEnrollmentCampaign(enrollmentCampaignList.get(enrollmentCampaignList.size()-1));
        }
    }

    public void onClickApply()
    {
        final IBackgroundProcess process = new BackgroundProcessBase()
        {
            @Override
            public ProcessResult run(ProcessState state)
            {
                state.setCurrentValue(1);

                try
                {
                    EcfOrgUnitPackageDao.setupProcessState(state);
                    EcfOrgUnitManager.instance().pkgAdmissionOrdersDao().doCreatePackage(getEcfOrgUnit(), getEnrollmentCampaign());
                }
                catch (RuntimeTimeoutException e)
                {
                    throw new ApplicationException("Формирование пакетов уже запущено.");
                }
                catch (Exception e)
                {
                    //TODO когда будет логирование ошибок, нужно выводить общие слова
                    if (e instanceof ApplicationException)
                        return new ProcessResult(e);
                    else
                        throw e;
                }

                if (UserContext.getInstance().getErrorCollector().hasErrors())
                {
                    return new ProcessResult("Пакет «Списки заявлений абитуриентов, включенных в приказ» не удалось сформировать.", true);
                }
                state.setCurrentValue(100);

                return new ProcessResult("Пакет «Списки заявлений абитуриентов, включенных в приказ» успешно сформирован.");
            }
        };

        new BackgroundProcessHolder().start("Формирование пакета «Списки заявлений абитуриентов, включенных в приказ»", process, ProcessDisplayMode.percent);
    }
}
