/* $Id$ */
package ru.tandemservice.uniec_fis.base.bo.EcfConverter.ui.Config4Region;

import ru.tandemservice.uniec_fis.base.bo.EcfConverter.EcfConverterManager;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.ui.EcfConverterConfigUI;

/**
 * @author Andrey Andreev
 * @since 12.05.2016
 */
public class EcfConverterConfig4RegionUI extends EcfConverterConfigUI
{

    public void onClickAutoSync()
    {
        EcfConverterManager.instance().converterDao4Region().doAutoSync();
    }
}