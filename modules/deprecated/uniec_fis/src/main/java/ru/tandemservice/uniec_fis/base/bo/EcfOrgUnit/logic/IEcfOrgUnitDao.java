package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.logic;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit;

/**
 * @author vdanilov
 */
public interface IEcfOrgUnitDao extends INeedPersistenceSupport {

    /** @return ecfOrgUnit, к которому относится данное подразделение (первый найденный вверх по иерархии) */
    EcfOrgUnit getAcquiredEcfOrgUnit(OrgUnit orgUnit);

    /** @return ecfOrgUnit, созданный на orgUnit или null, если такового нет */
    EcfOrgUnit getEcfOrgUnit(OrgUnit orgUnit);

    /** сохраняет (обновляет) EcfOrgUnit */
    @Transactional(readOnly=false, propagation=Propagation.REQUIRED)
    void save(EcfOrgUnit ecfOrgUnit);

    /** удаляет EcfOrgUnit */
    @Transactional(readOnly=false, propagation=Propagation.REQUIRED)
    void delete(EcfOrgUnit ecfOrgUnit);

    /** создает EcfOrgUnit для UniBaseDao.getAcademy() **/
    @Transactional(readOnly=false, propagation=Propagation.REQUIRED)
    EcfOrgUnit initDefaultExfOrgUnit();

    /** формирует перечень приемных кампаний, доступных для указанного подразделения ФИС */
    List<EnrollmentCampaign> getEnrollmentCampaignList(EcfOrgUnit ecfOrgUnit);

    /**
     * Формирует перечень направлений подготовки, доступных для указанного подразделения ФИС
     * @param ecfOrgUnit подразделение или null, если фильтрация по подразделению ФИС не требуется
     * @param enrollmentCampaign
     */
    List<EnrollmentDirection> getEnrollmentDirections(EcfOrgUnit ecfOrgUnit, EnrollmentCampaign enrollmentCampaign);

}
