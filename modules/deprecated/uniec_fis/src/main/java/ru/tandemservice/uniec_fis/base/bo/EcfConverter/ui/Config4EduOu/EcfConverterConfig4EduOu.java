package ru.tandemservice.uniec_fis.base.bo.EcfConverter.ui.Config4EduOu;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.EcfConverterManager;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4EduOu;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.eduOu.IEcfConverterDao4EduOu;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.ui.EcfConverterConfig;

import java.util.LinkedList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * @author vdanilov
 */
@Configuration
public class EcfConverterConfig4EduOu extends EcfConverterConfig {

    public static final String ENROLLMENT_CAMPAIGN_DS = "enrollmentCampaignDS";
    public static final String STRUCTURE_EDU_LVL_DS = "structureEduLvlDS";

    @Override
    protected IEcfConverterDao4EduOu getConverterDao() {
        return EcfConverterManager.instance().educationOrgUnit();
    }

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint() {
        return super.presenterExtPoint(
                presenterExtPointBuilder()
                        .addDataSource(selectDS(STRUCTURE_EDU_LVL_DS, structureEduLvlDSHandler()).addColumn(StructureEducationLevels.shortTitle().s()))
                        .addDataSource(selectDS(ENROLLMENT_CAMPAIGN_DS, enrollmentCampaignDSHandler()))
        );
    }

    @Bean
    @Override
    public ColumnListExtPoint ecfConverterRecordDS() {
        return this.columnListExtPointBuilder(ECF_CONVERTER_RECORD_DS)
        .addColumn(textColumn("title", EducationOrgUnit.educationLevelHighSchool().fullTitle()).create())
        .addColumn(textColumn("formativeOrgUnit", EducationOrgUnit.formativeOrgUnit().fullTitle()).create())
        .addColumn(textColumn("territorialOrgUnit", EducationOrgUnit.territorialOrgUnit().territorialShortTitle()).create())
        .addColumn(textColumn("developCombination", EducationOrgUnit.developCombinationTitle()).create())
        .addColumn(blockColumn("value", "valueBlock").width("460px").create())
        .addColumn(blockColumn("action", "actionBlock").width("1px").hasBlockHeader(true).create())
        .create();
    }

    @Bean
    @Override
    public IReadAggregateHandler<DSInput, DSOutput> ecfConverterRecordDSHandler() {
        return super.ecfConverterRecordDSHandler();
    }

    @SuppressWarnings("unchecked")
    @Override
    protected List<IEntity> filterConverterRecordDS(List<IEntity> entityList, ExecutionContext context)
    {
        final EnrollmentCampaign enrollmentCampaign = context.get(EcfConverterConfig4EduOuUI.PARAM_ENROLLMENT_CAMPAIGN);
        final StructureEducationLevels structureEduLvl = context.get(EcfConverterConfig4EduOuUI.PARAM_STRUCTURE_EDU_LVL);
        final String itemTitle = context.get(EcfConverterConfig4EduOuUI.PARAM_ITEM_TITLE);

        // filter EnrollmentCampaign
        if (enrollmentCampaign != null)
        {
            final List<Long> eduOrgUnitIdList = DataAccessServices.dao().getList(
                    new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, "d").column(property(EnrollmentDirection.educationOrgUnit().id().fromAlias("d")))
                            .where(eq(property(EnrollmentDirection.enrollmentCampaign().fromAlias("d")), value(enrollmentCampaign)))
            );

            eduOrgUnitIdList.addAll(DataAccessServices.dao().<Long>getList(
                    new DQLSelectBuilder().fromEntity(EcfConv4EduOu.class, "c").column(property(EcfConv4EduOu.educationOrgUnit().id().fromAlias("c")))
                            .where(notExists(
                                    new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, "d").column(property(EnrollmentDirection.id().fromAlias("d")))
                                            .where(eq(property(EnrollmentDirection.educationOrgUnit().fromAlias("d")), property(EcfConv4EduOu.educationOrgUnit().fromAlias("c"))))
                                            .buildQuery()))));

            for (IEntity educationOrgUnit : new LinkedList<>(entityList))
            {
                if (!eduOrgUnitIdList.contains(educationOrgUnit.getId()))
                    entityList.remove(educationOrgUnit);
            }

        }

        // filter StructureEducationLevels
        if (structureEduLvl != null)
        {
            for (IEntity educationOrgUnit : new LinkedList<>(entityList))
            {
                if (educationOrgUnit instanceof EducationOrgUnit)
                {
                    boolean exclud = true;
                    StructureEducationLevels parent = ((EducationOrgUnit) educationOrgUnit).getEducationLevelHighSchool().getEducationLevel().getLevelType();
                    while (parent != null)
                    {
                        if (parent.getCode().equals(structureEduLvl.getCode()))
                        {
                            exclud = false;
                            break;
                        }
                        parent = parent.getParent();
                    }
                    if (exclud)
                        entityList.remove(educationOrgUnit);
                }
            }
        }

        // filter title
        if (itemTitle != null)
        {
            for (IEntity educationOrgUnit : new LinkedList<>(entityList))
            {
                if (educationOrgUnit instanceof EducationOrgUnit && !StringUtils.containsIgnoreCase(((EducationOrgUnit) educationOrgUnit).getEducationLevelHighSchool().getFullTitle(), itemTitle))
                    entityList.remove(educationOrgUnit);
            }
        }

        return entityList;
    }

    @Bean
    @Override
    public IReadAggregateHandler<DSInput, DSOutput> ecfConverterValueDSHandler() {
        return super.ecfConverterValueDSHandler();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> enrollmentCampaignDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrollmentCampaign.class)
                .order(EnrollmentCampaign.educationYear().intValue(), OrderDirection.desc)
                .filter(EnrollmentCampaign.title())
                .pageable(true);
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> structureEduLvlDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), StructureEducationLevels.class)
                .where(StructureEducationLevels.parent(), (Object) null)
                .order(StructureEducationLevels.priority())
                .filter(StructureEducationLevels.shortTitle())
                .pageable(true);
    }
}
