package ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity;

import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.gen.*;

/** @see ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.gen.EcfConv4EntranceDisciplineGen */
public class EcfConv4EntranceDiscipline extends EcfConv4EntranceDisciplineGen
{

    public EcfConv4EntranceDiscipline()
    {
    }

    public EcfConv4EntranceDiscipline(EntranceDiscipline disciplin, EcfCatalogItem value)
    {
        setDiscipline(disciplin);
        setValue(value);
    }
}