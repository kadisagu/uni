package ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4EntranceDiscipline;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * ФИС: Сопоставление: Вступительное испытание для направления приема
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EcfConv4EntranceDisciplineGen extends EntityBase
 implements INaturalIdentifiable<EcfConv4EntranceDisciplineGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4EntranceDiscipline";
    public static final String ENTITY_NAME = "ecfConv4EntranceDiscipline";
    public static final int VERSION_HASH = 624182213;
    private static IEntityMeta ENTITY_META;

    public static final String L_DISCIPLINE = "discipline";
    public static final String L_VALUE = "value";

    private EntranceDiscipline _discipline;     // Вступительное испытание для направления для приема
    private EcfCatalogItem _value;     // Значение (элемент справочника ФИС)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Вступительное испытание для направления для приема. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EntranceDiscipline getDiscipline()
    {
        return _discipline;
    }

    /**
     * @param discipline Вступительное испытание для направления для приема. Свойство не может быть null и должно быть уникальным.
     */
    public void setDiscipline(EntranceDiscipline discipline)
    {
        dirty(_discipline, discipline);
        _discipline = discipline;
    }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     */
    @NotNull
    public EcfCatalogItem getValue()
    {
        return _value;
    }

    /**
     * @param value Значение (элемент справочника ФИС). Свойство не может быть null.
     */
    public void setValue(EcfCatalogItem value)
    {
        dirty(_value, value);
        _value = value;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EcfConv4EntranceDisciplineGen)
        {
            if (withNaturalIdProperties)
            {
                setDiscipline(((EcfConv4EntranceDiscipline)another).getDiscipline());
            }
            setValue(((EcfConv4EntranceDiscipline)another).getValue());
        }
    }

    public INaturalId<EcfConv4EntranceDisciplineGen> getNaturalId()
    {
        return new NaturalId(getDiscipline());
    }

    public static class NaturalId extends NaturalIdBase<EcfConv4EntranceDisciplineGen>
    {
        private static final String PROXY_NAME = "EcfConv4EntranceDisciplineNaturalProxy";

        private Long _discipline;

        public NaturalId()
        {}

        public NaturalId(EntranceDiscipline discipline)
        {
            _discipline = ((IEntity) discipline).getId();
        }

        public Long getDiscipline()
        {
            return _discipline;
        }

        public void setDiscipline(Long discipline)
        {
            _discipline = discipline;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EcfConv4EntranceDisciplineGen.NaturalId) ) return false;

            EcfConv4EntranceDisciplineGen.NaturalId that = (NaturalId) o;

            if( !equals(getDiscipline(), that.getDiscipline()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getDiscipline());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getDiscipline());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EcfConv4EntranceDisciplineGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EcfConv4EntranceDiscipline.class;
        }

        public T newInstance()
        {
            return (T) new EcfConv4EntranceDiscipline();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "discipline":
                    return obj.getDiscipline();
                case "value":
                    return obj.getValue();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "discipline":
                    obj.setDiscipline((EntranceDiscipline) value);
                    return;
                case "value":
                    obj.setValue((EcfCatalogItem) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "discipline":
                        return true;
                case "value":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "discipline":
                    return true;
                case "value":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "discipline":
                    return EntranceDiscipline.class;
                case "value":
                    return EcfCatalogItem.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EcfConv4EntranceDiscipline> _dslPath = new Path<EcfConv4EntranceDiscipline>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EcfConv4EntranceDiscipline");
    }
            

    /**
     * @return Вступительное испытание для направления для приема. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4EntranceDiscipline#getDiscipline()
     */
    public static EntranceDiscipline.Path<EntranceDiscipline> discipline()
    {
        return _dslPath.discipline();
    }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4EntranceDiscipline#getValue()
     */
    public static EcfCatalogItem.Path<EcfCatalogItem> value()
    {
        return _dslPath.value();
    }

    public static class Path<E extends EcfConv4EntranceDiscipline> extends EntityPath<E>
    {
        private EntranceDiscipline.Path<EntranceDiscipline> _discipline;
        private EcfCatalogItem.Path<EcfCatalogItem> _value;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Вступительное испытание для направления для приема. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4EntranceDiscipline#getDiscipline()
     */
        public EntranceDiscipline.Path<EntranceDiscipline> discipline()
        {
            if(_discipline == null )
                _discipline = new EntranceDiscipline.Path<EntranceDiscipline>(L_DISCIPLINE, this);
            return _discipline;
        }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4EntranceDiscipline#getValue()
     */
        public EcfCatalogItem.Path<EcfCatalogItem> value()
        {
            if(_value == null )
                _value = new EcfCatalogItem.Path<EcfCatalogItem>(L_VALUE, this);
            return _value;
        }

        public Class getEntityClass()
        {
            return EcfConv4EntranceDiscipline.class;
        }

        public String getEntityName()
        {
            return "ecfConv4EntranceDiscipline";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
