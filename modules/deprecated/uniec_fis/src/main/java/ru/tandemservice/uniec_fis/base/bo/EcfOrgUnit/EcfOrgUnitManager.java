package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IUidRegistryDefinition;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.UidRegistryDefinition;
import org.tandemframework.shared.commonbase.base.entity.UidRegistry;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.*;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.logic.*;

import java.util.Map;

/**
 * @author vdanilov
 */
@Configuration
public class EcfOrgUnitManager extends BusinessObjectManager {

    public static EcfOrgUnitManager instance() {
        return instance(EcfOrgUnitManager.class);
    }

    @Bean
    public IEcfOrgUnitDao dao() {
        return new EcfOrgUnitDao();
    }

    @Bean
    public IEcfOrgUnitPackage4CompaignDao pkgCompaignDao() {
        return new EcfOrgUnitPackage4CompaignDao();
    }

    @Bean
    public IEcfOrgUnitPackage4AdmissionDao pkgAdmissionDao() {
        return new EcfOrgUnitPackage4AdmissionDao();
    }

    @Bean
    public IEcfOrgUnitPackage4ApplicationsDao pkgApplicationsDao() {
        return new EcfOrgUnitPackage4ApplicationsDao();
    }

    @Bean
    public IEcfOrgUnitPackage4OrdersDao pkgAdmissionOrdersDao()
    {
        return new EcfOrgUnitPackage4OrdersDao();
    }

    @Bean
    public IEcfOrgUnitPackage4DelApplicationsDao pkgDelApplicationsDao()
    {
        return new EcfOrgUnitPackage4DelApplicationsDao();
    }

    @Bean
    public IEcfOrgUnitPackage4DelAdmissionOrdersDao pkgDelAdmissionOrdersDao()
    {
        return new EcfOrgUnitPackage4DelAdmissionOrdersDao();
    }

    @Bean
    public IUidRegistryDefinition uidRegistry() {
        return new UidRegistryDefinition("ecfOrgUnit") {
            @Override protected String generateNewUid(UidRegistry registry, Map<String, String> inverseMap, String key) {
                long value = (key.hashCode() & 0xFFFFF); // 5xF, т.к. потому 2^5 = 32 => число битов должно быть кратным 5
                String uid;
                while (inverseMap.containsKey(uid = Long.toString(value, 32))) { value++; }
                return uid;
            }
        };
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> ecfOrgUnitPackageHandler() {
        return new EcfOrgUnitPackageDSHandler(this.getName());
        //        return new EntityComboDataSourceHandler(this.getName(), EcfOrgUnitPackage.class)
        //        .where(EcfOrgUnitPackage.enrollmentCampaign(), "enrollmentCampaign")
        //        .where(EcfOrgUnitPackage.ecfOrgUnit(), "ecfOrgUnit")
        //        .order(EcfOrgUnitPackage.creationDate(), OrderDirection.desc)
        //        .pageable(true);
    }

    public static IEcfOrgUnitPackageDao pkgDao(EcfOrgUnitPackage ecfOrgUnitPackage)
    {
        if (ecfOrgUnitPackage instanceof EcfOrgUnitPackage4CampaignInfo) { return EcfOrgUnitManager.instance().pkgCompaignDao(); }
        if (ecfOrgUnitPackage instanceof EcfOrgUnitPackage4AdmissionInfo) { return EcfOrgUnitManager.instance().pkgAdmissionDao(); }
        if (ecfOrgUnitPackage instanceof EcfOrgUnitPackage4ApplicationsInfo) { return EcfOrgUnitManager.instance().pkgApplicationsDao(); }
        if (ecfOrgUnitPackage instanceof EcfOrgUnitPackage4AdmissionOrders) { return EcfOrgUnitManager.instance().pkgAdmissionOrdersDao(); }
        if (ecfOrgUnitPackage instanceof EcfOrgUnitPackage4DelApplications) { return EcfOrgUnitManager.instance().pkgDelApplicationsDao(); }
        if (ecfOrgUnitPackage instanceof EcfOrgUnitPackage4DelAdmissionOrders) { return EcfOrgUnitManager.instance().pkgDelAdmissionOrdersDao(); }
        throw new IllegalStateException("Unexpected class: " + ecfOrgUnitPackage.getClass());
    }

}
