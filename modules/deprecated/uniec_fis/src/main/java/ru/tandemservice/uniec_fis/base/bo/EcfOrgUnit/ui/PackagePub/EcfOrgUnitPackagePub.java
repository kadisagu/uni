package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.ui.PackagePub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;

import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackageIOLog;

/**
 * @author vdanilov
 */
@Configuration
public class EcfOrgUnitPackagePub extends BusinessComponentManager {

    public static final String ECF_ORG_UNIT_PACKAGE_IO_DS = "ecfOrgUnitPackageIODS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
        .addDataSource(this.searchListDS(ECF_ORG_UNIT_PACKAGE_IO_DS, this.ecfOrgUnitPackageIODS()).handler(this.ecfOrgUnitPackageIOHandler()))
        .create();
    }

    @Bean
    public ColumnListExtPoint ecfOrgUnitPackageIODS() {
        return this.columnListExtPointBuilder(ECF_ORG_UNIT_PACKAGE_IO_DS)
        .addColumn(textColumn("title", EcfOrgUnitPackageIOLog.title()).clickable(true).create())
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> ecfOrgUnitPackageIOHandler() {
        return new EntityComboDataSourceHandler(this.getName(), EcfOrgUnitPackageIOLog.class)
        .where(EcfOrgUnitPackageIOLog.owner(), "owner")
        .order(EcfOrgUnitPackageIOLog.operationDate(), OrderDirection.desc)
        .pageable(false);
    }
}
