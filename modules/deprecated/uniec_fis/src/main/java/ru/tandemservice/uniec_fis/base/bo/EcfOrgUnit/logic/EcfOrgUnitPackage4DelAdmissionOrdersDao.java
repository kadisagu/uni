/**
 *$Id$
 */
package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.logic;

import fis.schema.pkg.del.req.AuthData;
import fis.schema.pkg.del.req.DataForDelete;
import fis.schema.pkg.del.req.Root;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.EcfConverterManager;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.ecg.IEcfEcgDao;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.EcfOrgUnitManager;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage4DelAdmissionOrders;
import ru.tandemservice.uniec_fis.util.FisServiceImpl;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Alexander Shaburov
 * @since 03.07.13
 */
public class EcfOrgUnitPackage4DelAdmissionOrdersDao extends EcfOrgUnitPackageDao implements IEcfOrgUnitPackage4DelAdmissionOrdersDao
{
    @Override
    public EcfOrgUnitPackage4DelAdmissionOrders doCreatePackage(EcfOrgUnit ecfOrgUnit, EnrollmentCampaign enrollmentCampaign)
    {
        final EcfOrgUnitPackage4DelAdmissionOrders dbo = new EcfOrgUnitPackage4DelAdmissionOrders();
        dbo.setCreationDate(new Date());
        dbo.setEcfOrgUnit(ecfOrgUnit);
        dbo.setEnrollmentCampaign(enrollmentCampaign);
        dbo.setXmlPackageRequest(this.generateXmlPackageRequest(dbo, ecfOrgUnit, enrollmentCampaign));
        this.saveOrUpdate(dbo);
        return dbo;
    }

    protected byte[] generateXmlPackageRequest(EcfOrgUnitPackage4DelAdmissionOrders dbo, final EcfOrgUnit ecfOrgUnit, final EnrollmentCampaign enrollmentCampaign)
    {
        final IEcfEcgDao ecgDao = EcfConverterManager.instance().ecg();

        final DataForDelete dataForDelete = new DataForDelete();

        /** (EntrantRequest, IEcfEcgKey) -> DataForDelete.OrdersOfAdmission.Application */
        final Map<MultiKey, DataForDelete.OrdersOfAdmission.Application> applicationMap = SafeMap.get(key -> {
            final EntrantRequest request = (EntrantRequest) key.getKey(0);
            final IEcfEcgDao.IEcfEcgKey ecgKey = (IEcfEcgDao.IEcfEcgKey) key.getKey(1);
            final String uid = uid(ecfOrgUnit, enrollmentCampaign, request, ecgKey);

            if (dataForDelete.getOrdersOfAdmission() == null)
                dataForDelete.setOrdersOfAdmission(new DataForDelete.OrdersOfAdmission());

            final DataForDelete.OrdersOfAdmission.Application application = new DataForDelete.OrdersOfAdmission.Application();
            application.setApplicationNumber(uid + "/" + request.getRegNumber());
            application.setRegistrationDate(xml(request.getRegDate()));

            dataForDelete.getOrdersOfAdmission().getApplication().add(application);

            return application;
        });

        // разбиваем ВНП по КГФИС: RequestedEnrollmentDirection -> key = (entrant, ecgFis(direction))
        final List<EnrollmentDirection> directions = EcfOrgUnitManager.instance().dao().getEnrollmentDirections(ecfOrgUnit, enrollmentCampaign);
        for (final EnrollmentDirection dir : directions)
        {
            for (final RequestedEnrollmentDirection reqDir : getList(RequestedEnrollmentDirection.class, RequestedEnrollmentDirection.enrollmentDirection(), dir))
            {
                final IEcfEcgDao.IEcfEcgKey ecgKey;
                try
                {
                    if (null == (ecgKey = ecgDao.getKey(reqDir.getEnrollmentDirection(), reqDir.getStudentCategory())))
                        throw new ApplicationException(""); // хз что произошло (ниже будет написаны общие слова)
                } catch (final ApplicationException e)
                {
                    final String message = EcfOrgUnitManager.instance().getProperty(
                            "error.applications.no-ecg-key",
                            reqDir.getEntrantRequest().getEntrant().getTitle(),
                            reqDir.getEntrantRequest().getStringNumber(),
                            reqDir.getEnrollmentDirection().getTitle()
                    ) + (StringUtils.isBlank(e.getMessage()) ? "" : (": " + e.getMessage()));

                    if (Debug.isDisplay())
                    {
                        ContextLocal.getInfoCollector().add(message);
                        continue;
                    } else
                    {
                        throw new ApplicationException(message, e);
                    }
                }

                applicationMap.get(new MultiKey(reqDir.getEntrantRequest(), ecgKey));
            }

            setProcessState(98, dir, directions);
        }

        // obtain new root
        final Root root = new Root();

        // auth data
        root.setAuthData(new AuthData());
        root.getAuthData().setLogin(ecfOrgUnit.getLogin());
        root.getAuthData().setPass(ecfOrgUnit.getPassword());
        root.setDataForDelete(dataForDelete);
        return FisServiceImpl.toXml(root);
    }
}
