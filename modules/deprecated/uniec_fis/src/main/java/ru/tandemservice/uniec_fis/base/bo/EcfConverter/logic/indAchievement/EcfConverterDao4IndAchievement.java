/* $Id$ */
package ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.indAchievement;


import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniec.entity.catalog.IndividualProgress;
import ru.tandemservice.uniec.entity.settings.ConversionScale;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4IndAchievement;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.gen.EcfConv4IndAchievementGen;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.EcfConverterBaseDao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Ekaterina Zvereva
 * @since 04.06.2015
 */
public class EcfConverterDao4IndAchievement extends EcfConverterBaseDao<IndividualProgress, Long> implements IEcfConverterDao4IndAchievement
{
    /**
     * @return код справочника ФИС, с которым происходит сопоставление
     */
    @Override
    public String getEcfCatalogCode()
    {
        return "36";
    }

    /**
     * @return список сущностей, которые необходимо сопоставить
     */
    @Override
    protected List<IndividualProgress> getEntityList()
    {
        return getList(IndividualProgress.class, IndividualProgress.P_TITLE);
    }

    /**
     * @param entity
     * @return код (для сущности uni), на базе которого осуществляется сопоставление
     */
    @Override
    protected Long getItemMapKey(IndividualProgress entity)
    {
        return entity.getId();
    }

    /**
     * @return { код (для сущности uni) -> ecfCatalogItem.id }
     */
    @Override
    protected Map<Long, Long> getItemMap()
    {
        return EcfConverterBaseDao.<Long, Long>map(scrollRows(
                new DQLSelectBuilder()
                        .fromEntity(EcfConv4IndAchievement.class, "x")
                        .column(property(EcfConv4IndAchievement.individualAchievement().id().fromAlias("x")), "ach_id")
                        .column(property(EcfConv4IndAchievement.value().id().fromAlias("x")), "value_id")
                        .createStatement(this.getSession())
        ));
    }

    @Override
    public void update(Map<IndividualProgress, EcfCatalogItem> values, final boolean clearNulls)
    {
        // формируем перечень требуемых строк
        final List<EcfConv4IndAchievement> targetRecords = new ArrayList<EcfConv4IndAchievement>(values.size());
        for (final Map.Entry<IndividualProgress, EcfCatalogItem> e: values.entrySet()) {
            if (null != e.getValue()) {
                targetRecords.add(new EcfConv4IndAchievement(e.getKey(), e.getValue()));
            }
        }

        // обновляем данные в базе
        new MergeAction.SessionMergeAction<EcfConv4IndAchievementGen.NaturalId, EcfConv4IndAchievement>() {
            @Override protected EcfConv4IndAchievementGen.NaturalId key(final EcfConv4IndAchievement source) {
                return (EcfConv4IndAchievementGen.NaturalId)source.getNaturalId();
            }
            @Override protected EcfConv4IndAchievement buildRow(final EcfConv4IndAchievement source) {
                return new EcfConv4IndAchievement(source.getIndividualAchievement(), source.getValue());
            }
            @Override protected void fill(final EcfConv4IndAchievement target, final EcfConv4IndAchievement source) {
                target.update(source, false);
            }
            @Override protected void doDeleteRecord(final EcfConv4IndAchievement databaseRecord) {
                if (clearNulls) { super.doDeleteRecord(databaseRecord); }
            }
        }.merge(this.getList(EcfConv4IndAchievement.class), targetRecords);
    }

}