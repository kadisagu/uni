package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.logic;

import fis.schema.pkg.send.req.PackageData;
import fis.schema.pkg.send.req.Root;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaignPeriod;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.EcfConverterManager;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.IEcfConverter;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.eduOu.IEcfConverterDao4EduOu;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.EcfOrgUnitManager;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage4CampaignInfo;
import ru.tandemservice.uniec_fis.util.FisServiceImpl;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author vdanilov
 */
public class EcfOrgUnitPackage4CompaignDao extends EcfOrgUnitPackageDao implements IEcfOrgUnitPackage4CompaignDao {

    @Override
    public EcfOrgUnitPackage4CampaignInfo doCreatePackage(final EcfOrgUnit ecfOrgUnit, final EnrollmentCampaign enrollmentCampaign, final EcfCatalogItem status)
    {
        final EcfOrgUnitPackage4CampaignInfo dbo = new EcfOrgUnitPackage4CampaignInfo();
        dbo.setCreationDate(new Date());
        dbo.setEcfOrgUnit(ecfOrgUnit);
        dbo.setEnrollmentCampaign(enrollmentCampaign);
        dbo.setStatus(status);
        dbo.setXmlPackageRequest(this.generateXmlPackageRequest(dbo));
        this.saveOrUpdate(dbo);
        return dbo;
    }


    protected byte[] generateXmlPackageRequest(final EcfOrgUnitPackage4CampaignInfo dbo) {


        final IEcfConverter<DevelopForm> educationFormConverter = EcfConverterManager.instance().educationForm().getConverter();
        final IEcfConverterDao4EduOu eduOrgUnitDao = EcfConverterManager.instance().educationOrgUnit();

        // compaign
        final PackageData.CampaignInfo.Campaigns.Campaign compaign = new PackageData.CampaignInfo.Campaigns.Campaign();
        compaign.setUID(uid(dbo.getEcfOrgUnit(), dbo.getEnrollmentCampaign()));
        compaign.setName(dbo.getEnrollmentCampaign().getTitle());
        compaign.setStatusID(Long.valueOf(dbo.getStatus().getFisItemCode()));

        // periods
        final List<EnrollmentCampaignPeriod> periodList = dbo.getEnrollmentCampaign().getPeriodList();
        if (periodList.isEmpty()) {
            throw new ApplicationException(EcfOrgUnitManager.instance().getProperty("error.compaign.period-list-is-empty"));
        } else {
            int yearMin = Integer.MAX_VALUE, yearMax = Integer.MIN_VALUE;
            for (final EnrollmentCampaignPeriod period: periodList) {
                yearMin = Math.min(yearMin, CoreDateUtils.getYear(period.getDateFrom()));
                yearMax = Math.max(yearMax, CoreDateUtils.getYear(period.getDateTo()));
            }

            if (yearMin > yearMax) {
                throw new ApplicationException(EcfOrgUnitManager.instance().getProperty("error.compaign.perios-dates-are-wrong"));
            }

            compaign.setYearStart(yearMin);
            compaign.setYearEnd(yearMax);
        }

        // enrollment directions
        final List<EnrollmentDirection> directions = EcfOrgUnitManager.instance().dao().getEnrollmentDirections(dbo.getEcfOrgUnit(), dbo.getEnrollmentCampaign());
        if (directions.isEmpty()) {
            throw new ApplicationException(EcfOrgUnitManager.instance().getProperty("error.compaign.no-directions"));
        }

        // education forms (develop forms)
        {
            final Set<Long> educationFromIdSet = new HashSet<>();
            for (final EnrollmentDirection d: directions) {
                final DevelopForm developForm = d.getEducationOrgUnit().getDevelopForm();
                final EcfCatalogItem fisEduForm = educationFormConverter.getEcfCatalogItem(developForm, false);
                if (null == fisEduForm) {
                    throw new ApplicationException(EcfOrgUnitManager.instance().getProperty("error.compaign.missing-develop-form", developForm.getCode(), developForm.getTitle()));
                }
                educationFromIdSet.add(fisEduForm.getFisID());
            }
            compaign.setEducationForms(new PackageData.CampaignInfo.Campaigns.Campaign.EducationForms());
            compaign.getEducationForms().getEducationFormID().addAll(educationFromIdSet);

            setProcessState(20);
        }

        // education levels
        {
            compaign.setEducationLevels(new PackageData.CampaignInfo.Campaigns.Campaign.EducationLevels());
            compaign.getEducationLevels().getEducationLevelID().addAll(
                    directions.stream()
                            .map(d -> {
                                final EcfCatalogItem fisEduLvl = eduOrgUnitDao.getEcfItem4EducationLevel(d.getEducationOrgUnit());
                                if (null == fisEduLvl)
                                    throw new ApplicationException(EcfOrgUnitManager.instance().getProperty("error.compaign.missing-education-level", d.getEducationOrgUnit().getTitle()));
                                return fisEduLvl;
                            })
                            .map(EcfCatalogItem::getFisID)
                            .distinct()
                            .collect(Collectors.toList())
            );
            setProcessState(60);
        }

        String uid4Fis = dbo.getEnrollmentCampaign().getEnrollmentCampaignType().getUid4Fis();
        compaign.setCampaignTypeID(uid4Fis == null ? "0" : uid4Fis);

        // obtain new root
        final Root root = this.newRoot(dbo.getEcfOrgUnit());
        root.getPackageData().setCampaignInfo(new PackageData.CampaignInfo());
        root.getPackageData().getCampaignInfo().setCampaigns(new PackageData.CampaignInfo.Campaigns());
        root.getPackageData().getCampaignInfo().getCampaigns().getCampaign().add(compaign);
        return FisServiceImpl.toXml(root);
    }

}
