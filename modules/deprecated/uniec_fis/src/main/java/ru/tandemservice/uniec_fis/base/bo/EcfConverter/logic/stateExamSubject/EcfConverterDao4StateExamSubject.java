/**
 *$Id$
 */
package ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.stateExamSubject;

import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.EcfConverterStaticDao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Alexander Shaburov
 * @since 11.01.13
 */
public class EcfConverterDao4StateExamSubject extends EcfConverterStaticDao<StateExamSubject, String> implements IEcfConverterDao4StateExamSubject
{
    /* { uni.code, uni.title, fis.code, fis.title } */
    private static final String[][] DATA = {
            { "1", "Русский язык",      "1",  "Русский язык"},
            { "2", "Математика",        "2",  "Математика"},
            { "3", "Физика",            "10", "Физика"},
            { "4", "Химия",             "11", "Химия"},
            { "5", "Биология",          "4",  "Биология"},
            { "6", "История России",    "7",  "История"},
            { "7", "География",         "5",  "География"},
            { "8", "Английский язык",   "24",  null},
            { "9", "Немецкий язык",     "12", "Иностранный язык - немецкий"},
            { "10", "Французский язык", "13", "Иностранный язык - французский"},
            { "11", "Обществознание",   "9",  "Обществознание"},
            { "12", "Литература",       "8",  "Литература"},
            { "13", "Испанский язык",   "14", "Иностранный язык - испанский"},
            { "14", "Информатика",      "3",  "Информатика и ИКТ"}
    };

    /* проверка того, что в ФИС ничего не поменялось { fis.code -> fis.title } */
    private static final Map<String, String> STATIC_TITLE_CHECK_MAP = new HashMap<String, String>();

    /* сопоставление элементов { uni.code -> fis.code }*/
    private static final Map<String, String> STATIC_ITEM_CODE_MAP = new HashMap<String, String>();

    static
    {
        for (final String[] row: DATA)
        {
            if (null != row[2])
            {
                if (null != STATIC_ITEM_CODE_MAP.put(row[0], row[2]))
                    throw new IllegalStateException("STATIC_ITEM_CODE_MAP: Duplicate value for key=«"+row[0]+"»");

                final String prev = STATIC_TITLE_CHECK_MAP.put(row[2], row[3]);
                if (null != prev && !prev.equals(row[3]))
                    throw new IllegalStateException("COUNTRY_CHECK_MAP: Duplicate value for key=«"+row[2]+"»");
            }
        }
    }

    @Override
    protected Map<String, String> getStaticTitleCheckMap()
    {
        return STATIC_TITLE_CHECK_MAP;
    }

    @Override
    protected Map<String, String> getStaticItemCodeMap()
    {
        return STATIC_ITEM_CODE_MAP;
    }

    @Override
    public String getEcfCatalogCode()
    {
        return "1";
    }

    @Override
    protected List<StateExamSubject> getEntityList()
    {
        return getCatalogItemList(StateExamSubject.class);
    }

    @Override
    protected String getItemMapKey(StateExamSubject entity)
    {
        return entity.getCode();
    }
}
