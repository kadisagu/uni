package ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic;

import java.util.List;
import java.util.NoSuchElementException;

import org.tandemframework.core.entity.IEntity;

import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;

/**
 * @author vdanilov
 */
public interface IEcfConverter<T extends IEntity> {

    /** @return код справочника ФИС (из которого будут выбираться значения для сопоставления) */
    String getEcfCatalogCode();

    /** @return список объектов uni, для которых требуется провести сопоставление */
    List<T> getEntityList();

    /**
     * @return элемент справочника ФИС, соответсвующее объекту в uni (соответсвует текущей настройке, кэшируется)
     * @throws NoSuchElementException если элемента не найдено (если required=true)
     **/
    EcfCatalogItem getEcfCatalogItem(T entity, boolean required) throws NoSuchElementException;

}
