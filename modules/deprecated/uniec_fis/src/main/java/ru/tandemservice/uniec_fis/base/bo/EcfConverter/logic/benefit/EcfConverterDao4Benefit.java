/* $Id$ */
package ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.benefit;


import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.catalog.entity.Benefit;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4Benefit;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.gen.EcfConv4BenefitGen;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.EcfConverterBaseDao;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 02.08.2016
 */
public class EcfConverterDao4Benefit extends EcfConverterBaseDao<Benefit, Long> implements IEcfConverterDao4Benefit
{
    @Override
    public String getEcfCatalogCode()
    {
        return "31";
    }

    @Override
    protected List<Benefit> getEntityList()
    {
        return getList(Benefit.class, Benefit.title().s());
    }

    @Override
    protected Long getItemMapKey(Benefit entity)
    {
        return entity.getId();
    }

    @Override
    protected Map<Long, Long> getItemMap()
    {
        return EcfConverterBaseDao.<Long, Long>map(scrollRows(
                new DQLSelectBuilder()
                        .fromEntity(EcfConv4Benefit.class, "x")
                        .column(property("x", EcfConv4Benefit.benefit().id()), "benefit_id")
                        .column(property("x", EcfConv4Benefit.value().id()), "value_id")
                        .createStatement(this.getSession())
        ));
    }

    @Override
    public void update(final Map<Benefit, EcfCatalogItem> values, final boolean clearNulls)
    {
        // формируем перечень требуемых строк
        final List<EcfConv4Benefit> targetRecords = values.entrySet().stream()
                .filter(e -> e.getValue() != null)
                .map(e -> new EcfConv4Benefit(e.getKey(), e.getValue()))
                .collect(Collectors.toList());

        // обновляем данные в базе
        new MergeAction.SessionMergeAction<EcfConv4BenefitGen.NaturalId, EcfConv4Benefit>()
        {
            @Override
            protected EcfConv4BenefitGen.NaturalId key(final EcfConv4Benefit source)
            {
                return (EcfConv4BenefitGen.NaturalId) source.getNaturalId();
            }

            @Override
            protected EcfConv4Benefit buildRow(final EcfConv4Benefit source)
            {
                return new EcfConv4Benefit(source.getBenefit(), source.getValue());
            }

            @Override
            protected void fill(final EcfConv4Benefit target, final EcfConv4Benefit source)
            {
                target.update(source, false);
            }

            @Override
            protected void doDeleteRecord(final EcfConv4Benefit databaseRecord)
            {
                if (clearNulls)
                {
                    super.doDeleteRecord(databaseRecord);
                }
            }
        }.merge(this.getList(EcfConv4Benefit.class), targetRecords);

    }

}