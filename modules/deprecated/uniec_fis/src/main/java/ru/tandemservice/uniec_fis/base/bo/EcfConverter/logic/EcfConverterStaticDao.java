package ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;

/**
 * @author vdanilov
 */
public abstract class EcfConverterStaticDao<T extends IEntity, KEY> extends EcfConverterBaseDao<T, KEY> implements IEcfConverterStaticDao<T> {

    /** @return !static! { fis-item.code -> fis-item.title} */
    protected abstract Map<String, String> getStaticTitleCheckMap();

    /** @return !static! { key(entity) -> getItemCode(fis-item) } */
    protected abstract Map<KEY, String> getStaticItemCodeMap();

    public String getItemCode(EcfCatalogItem item) {
        return item.getFisItemCode();
    }

    @Override
    public String checkTitle(String code, String title)
    {
        Map<String, String> staticTitleCheckMap = getStaticTitleCheckMap();

        title = StringUtils.trimToEmpty(title);
        code = StringUtils.trimToEmpty(code);

        final String expectedTitle = StringUtils.trimToNull(staticTitleCheckMap.get(code));
        if (null != expectedTitle) {
            // сопоставление есть, мы на него завязались (требуем, чтобы название сохранилось)
            if (!expectedTitle.equals(title)) {
                throw new IllegalStateException("fisCatalogItem(catalog="+this.getEcfCatalogCode()+", code="+code+"): title missmatch: title=«"+title+"», expectedTitle=«"+expectedTitle+"»");
            }
        }
        return title;
    }

    @Override
    protected Map<KEY, Long> getItemMap()
    {
        final List<EcfCatalogItem> items = new DQLSelectBuilder()
        .fromEntity(EcfCatalogItem.class, "x")
        .column(property("x"))
        .where(eq(property(EcfCatalogItem.fisCatalogCode().fromAlias("x")), value(this.getEcfCatalogCode())))
        .createStatement(this.getSession()).list();

        final Map<String, Long> code2idMap = new HashMap<>();
        for (final EcfCatalogItem item: items) {
            final Long id = item.getId();
            final String code = getItemCode(item);

            if (null != code2idMap.put(code, id)) {
                throw new IllegalStateException("code2idMap: duplicate value for key=«"+code+"»");
            }
        }

        final Map<KEY, String> staticItemCodeMap = getStaticItemCodeMap();
        return SafeMap.get(key -> {

            final String code = staticItemCodeMap.get(key);
            if (null == code) {
                throw new NoSuchElementException("Missing fisCode for addressCountry with code=«"+key+"»");
            }

            final Long id = code2idMap.get(code);
            if (null == id) {
                throw new NoSuchElementException("Missing ecfCatalogItem with fisItemCode=«"+code+"» (addressCountry.code=«"+key+"»)");
            }

            return id;
        });
    }

}
