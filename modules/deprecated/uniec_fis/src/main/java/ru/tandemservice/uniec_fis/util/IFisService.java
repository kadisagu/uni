package ru.tandemservice.uniec_fis.util;

import org.tandemframework.core.util.cache.SpringBeanCache;

import java.net.URL;


/**
 * @author vdanilov
 */
public interface IFisService {

    public static final SpringBeanCache<IFisService> instance = new SpringBeanCache<IFisService>(IFisService.class.getName());

    interface FisConnection {

        byte[] call(final byte[] request);
        <T> T call(Class<T> responseKlass, final byte[] request);
        <T> T call(Class<T> responseKlass, final Object root);

        String getRequestRaw();
        String getRequest();

        String getResponseRaw();
        String getResponse();

        URL getUrl();

        void writeDebugToLogFile(String postfix);

    }

    /** @return интерфейс запроса к ФИС (по указанному сервису) */
    FisConnection connect(String urlPostfix);

    // ошибка ФИС
    // Либо валидная ошибка из xsd
    // Либо невалидный xml
    class FisError extends RuntimeException {
        private static final long serialVersionUID = 1L;

        private final Object errorObject;
        private String fisRespErrorText;
        private String fisRespErrorCode;

        @SuppressWarnings("unchecked")
        public <T> T getErrorObject() { return (T)this.errorObject; }
        public String getFisRespErrorText() { return fisRespErrorText; }
        public String getFisRespErrorCode() { return fisRespErrorCode; }

        public FisError(Object error, String message) {
            super(message);
            this.errorObject = error;
        }

        public FisError(Object error, String errorCode, String errorText) {
            this(error, errorCode+": " + errorText);
            fisRespErrorCode = errorCode;
            fisRespErrorText = errorText;
        }
    }
}
