package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity;

import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.gen.EcfOrgUnitPackage4ApplicationsInfoGen;

/**
 * Пакет: Заявления абитуриентов
 *
 * Root / PackageData / Applications
 */
public class EcfOrgUnitPackage4ApplicationsInfo extends EcfOrgUnitPackage4ApplicationsInfoGen
{
    @Override
    public String getTitle() {
        if (getEnrollmentCampaign() == null) {
            return this.getClass().getSimpleName();
        }
        return "Пакет 3: Заявления абитуриентов «" + getEnrollmentCampaign().getTitle() + "»"
                + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getEntrantRequestRegDate())
                + " (" + getStatus().getTitle() + ")";
    }
}