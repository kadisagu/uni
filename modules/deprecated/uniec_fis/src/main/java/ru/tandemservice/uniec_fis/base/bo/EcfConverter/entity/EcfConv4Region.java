package ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity;

import org.tandemframework.shared.fias.base.entity.AddressItem;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.gen.*;

/** @see ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.gen.EcfConv4RegionGen */
public class EcfConv4Region extends EcfConv4RegionGen
{

    public EcfConv4Region()
    {
    }

    public EcfConv4Region(AddressItem region, EcfCatalogItem value)
    {
        setRegion(region);
        setValue(value);
    }
}