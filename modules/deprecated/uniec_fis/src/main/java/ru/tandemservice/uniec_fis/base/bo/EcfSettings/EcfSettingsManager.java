package ru.tandemservice.uniec_fis.base.bo.EcfSettings;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

import ru.tandemservice.uniec_fis.base.bo.EcfSettings.logic.EcfSettingsDao;
import ru.tandemservice.uniec_fis.base.bo.EcfSettings.logic.IEcfSettingsDao;

/**
 * @author vdanilov
 */
@Configuration
public class EcfSettingsManager extends BusinessObjectManager {

    public static EcfSettingsManager instance() {
        return instance(EcfSettingsManager.class);
    }

    @Bean
    public IEcfSettingsDao dao() {
        return new EcfSettingsDao();
    }


}
