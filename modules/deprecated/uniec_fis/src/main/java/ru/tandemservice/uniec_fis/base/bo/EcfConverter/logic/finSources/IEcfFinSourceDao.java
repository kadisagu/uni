package ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.finSources;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;

import java.util.List;

/**
 * @author vdanilov
 */
public interface IEcfFinSourceDao extends INeedPersistenceSupport {

    /**
     * Справочник ФИС №15. Источник финансирования (старый модуль)
     */
    String CATALOG_CODE = "15";
    /**
     * Бюджетные места
     */
    String BUDGET_CODE = "14";
    /**
     * С оплатой обучения
     */
    String CONTRACT_CODE = "15";
    /**
     * Целевой прием
     */
    String TARGET_CODE = "16";
    /**
     * Квота приема лиц, имеющих особое право
     */
    String QUOTE_CODE = "20";


    /**
     * @param dir
     * @return
     */
    List<EcfCatalogItem> getSourceList(EnrollmentDirection dir);

    /**
     * @param reqDir
     * @return
     */
    List<EcfCatalogItem> getSourceList(RequestedEnrollmentDirection reqDir);

    EcfCatalogItem getSource(RequestedEnrollmentDirection reqDir);
    EcfCatalogItem getSource(PreliminaryEnrollmentStudent preEnrStudent);
}
