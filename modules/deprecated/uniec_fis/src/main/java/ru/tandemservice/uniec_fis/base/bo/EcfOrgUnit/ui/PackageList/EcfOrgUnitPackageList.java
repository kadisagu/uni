package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.ui.PackageList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.button.ButtonListExtPoint;
import org.tandemframework.caf.ui.config.button.IButtonListExtPointBuilder;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.EcfOrgUnitManager;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage;

/**
 * @author vdanilov
 */
@Configuration
public class EcfOrgUnitPackageList extends BusinessComponentManager {

    public static final String ECF_ORG_UNIT_PACKAGE_DS = "ecfOrgUnitPackageDS";
    public static final String ECF_ORG_UNIT_PACKAGE_ACTIONS_DS = "ecfOrgUnitPackageActions";
    public static final String ENROLLMENT_CAMPAIGN_DS = "enrollmentCampaignDS";

    @Bean
    public ColumnListExtPoint ecfOrgUnitPackageDS() {
        return this.columnListExtPointBuilder(ECF_ORG_UNIT_PACKAGE_DS)
        .addColumn(textColumn("title", EcfOrgUnitPackage.title()).clickable(true).create())
        .addColumn(textColumn("creationDate", EcfOrgUnitPackage.creationDate()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).create())
        .addColumn(textColumn("packageRequestDate", EcfOrgUnitPackage.packageRequestDate()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).create())
        .addColumn(textColumn("packageResultDate", EcfOrgUnitPackage.packageResultDate()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).create())
        .addColumn(textColumn("stateTitle", "vPropStateTitle").create())

        .addColumn(textColumn("log", "vPropLog").formatter(NewLineFormatter.NOBR_IN_LINES).create())
        .addColumn(blockColumn("actions", "actionsColumnBlock").hasBlockHeader(Boolean.TRUE).width("1px").permissionKey("ui:holder.secModel.orgUnit_xmlPkgOperationEcfPackagesTab"))
        .addColumn(actionColumn("downloadPackageRequestXml", CommonDefines.ICON_PRINT, "onClickDownload").permissionKey("ui:holder.secModel.orgUnit_xmlPkgOperationEcfPackagesTab"))
        .create();
    }

    @Bean
    public ButtonListExtPoint ecfOrgUnitPackageActions() {
        IButtonListExtPointBuilder builder = buttonListExtPointBuilder(ECF_ORG_UNIT_PACKAGE_ACTIONS_DS);
        // if (Debug.isDisplay()) {
        builder.addButton(submitButton("addPkg4Compaign", "onClickAddPkg4Compaign").permissionKey("ui:holder.secModel.orgUnit_pkgOperationEcfPackagesTab"));
        builder.addButton(submitButton("addPkg4Admission", "onClickAddPkg4Admission").permissionKey("ui:holder.secModel.orgUnit_pkgOperationEcfPackagesTab"));
        builder.addButton(submitButton("addPkg4Applications", "onClickAddPkg4Applications").permissionKey("ui:holder.secModel.orgUnit_pkgOperationEcfPackagesTab"));
        builder.addButton(submitButton("addPkg4AdmissionOrders", "onClickAddPkg4AdmissionOrders").permissionKey("ui:holder.secModel.orgUnit_pkgOperationEcfPackagesTab"));
        builder.addButton(submitButton("addPkg4DelApplications", "onClickAddPkg4DelApplications").permissionKey("ui:holder.secModel.orgUnit_pkgOperationEcfPackagesTab"));
        builder.addButton(submitButton("addPkg4DelAdmissionOrders", "onClickAddPkg4DelAdmissionOrders").permissionKey("ui:holder.secModel.orgUnit_pkgOperationEcfPackagesTab"));
        // }
        return builder.create();
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
        .addDataSource(selectDS(ENROLLMENT_CAMPAIGN_DS, enrollmentCampaignDSHandler()))
        .addDataSource(this.searchListDS(ECF_ORG_UNIT_PACKAGE_DS, this.ecfOrgUnitPackageDS()).handler(EcfOrgUnitManager.instance().ecfOrgUnitPackageHandler()))
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> enrollmentCampaignDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrollmentCampaign.class)
        .order(EnrollmentCampaign.educationYear().intValue(), OrderDirection.desc)
        .filter(EnrollmentCampaign.title())
        .pageable(true);
    }
}
