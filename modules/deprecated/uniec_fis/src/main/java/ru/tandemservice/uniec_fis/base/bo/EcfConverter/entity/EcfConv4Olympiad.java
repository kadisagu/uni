package ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity;

import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.gen.*;

/**
 * ФИС: Сопоставление: Элемент набора вступительных испытаний
 */
public class EcfConv4Olympiad extends EcfConv4OlympiadGen
{
    public EcfConv4Olympiad()
    {
    }

    public EcfConv4Olympiad(String olympiadTitle, EcfCatalogItem value)
    {
        setOlympiadTitle(olympiadTitle);
        setValue(value);
    }
}