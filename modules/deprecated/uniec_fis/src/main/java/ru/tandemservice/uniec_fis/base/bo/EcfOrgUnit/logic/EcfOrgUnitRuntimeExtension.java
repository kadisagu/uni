package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.logic;

import org.tandemframework.core.runtime.IRuntimeExtension;

import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.EcfOrgUnitManager;

/**
 * @author vdanilov
 */
public class EcfOrgUnitRuntimeExtension implements IRuntimeExtension {

    @Override
    public void init(final Object object) {
        EcfOrgUnitManager.instance().dao().initDefaultExfOrgUnit();
    }

    @Override
    public void destroy() {

    }

}
