package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.logic;

import fis.schema.pkg.send.req.PackageData.Applications;
import fis.schema.pkg.send.req.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import org.tandemframework.shared.fias.base.entity.*;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.Sex;
import org.tandemframework.shared.person.catalog.entity.codes.EducationDocumentTypeCodes;
import org.tandemframework.shared.person.catalog.entity.codes.EducationLevelStageCodes;
import org.tandemframework.shared.person.catalog.entity.codes.IdentityCardTypeCodes;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.DebugUtils;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.IEntrantDAO;
import ru.tandemservice.uniec.entity.catalog.*;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.examset.EnrollmentDirectionExamSetData;
import ru.tandemservice.uniec.entity.examset.ExamSet;
import ru.tandemservice.uniec.entity.examset.ExamSetStructure;
import ru.tandemservice.uniec.entity.examset.ExamSetStructureItem;
import ru.tandemservice.uniec.entity.settings.ConversionScale;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.entity.settings.SetDiscipline;
import ru.tandemservice.uniec.ui.EntrantStateExamCertificateNumberFormatter;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.ExamSetUtil;
import ru.tandemservice.uniec.util.MarkDistributionUtil;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.gen.EcfCatalogItemGen;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.EcfConverterManager;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.IEcfConverter;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.benefit.IEcfBenefitDao;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.ecg.IEcfEcgDao;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.ecg.IEcfEcgDao.IEcfEcgKey;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.eduOu.IEcfConverterDao4EduOu;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.finSources.EcfFinSourceDao;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.finSources.IEcfFinSourceDao;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.EcfOrgUnitManager;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage4AdmissionInfo;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage4ApplicationsInfo;
import ru.tandemservice.uniec_fis.util.FisServiceImpl;

import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static fis.schema.pkg.send.req.PackageData.Applications.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * @author vdanilov
 */
public class EcfOrgUnitPackage4ApplicationsDao extends EcfOrgUnitPackageDao implements IEcfOrgUnitPackage4ApplicationsDao
{

    protected static final Logger logger = Logger.getLogger(EcfOrgUnitPackage4ApplicationsDao.class);

    @Override
    public List<EcfOrgUnitPackage4ApplicationsInfo> doCreatePackages(final EcfOrgUnit ecfOrgUnit, EcfCatalogItem applicationsStatus,
                                                                     final EnrollmentCampaign enrollmentCampaign,
                                                                     final Date regDateFrom, final Date regDateTo) throws Exception
    {
        final List<EcfOrgUnitPackage4ApplicationsInfo> resultList = new LinkedList<>();

        DebugUtils.debug(logger, new DebugUtils.Section("doCreatePackages")
        {
            @Override
            public void execute() throws Exception
            {
                final DQLSelectBuilder reqDateDQL = new DQLSelectBuilder().fromEntity(EntrantRequest.class, "r").column(property(EntrantRequest.regDate().fromAlias("r")))
                        .where(eq(property(EntrantRequest.entrant().enrollmentCampaign().fromAlias("r")), value(enrollmentCampaign)))
                        .order(property(EntrantRequest.regDate().fromAlias("r")));
                if (regDateFrom != null)
                    reqDateDQL.where(ge(property(EntrantRequest.regDate().fromAlias("r")), value(CoreDateUtils.getDayFirstTimeMoment(regDateFrom), PropertyType.TIMESTAMP)));
                if (regDateTo != null)
                    reqDateDQL.where(lt(property(EntrantRequest.regDate().fromAlias("r")), value(CoreDateUtils.getNextDayFirstTimeMoment(regDateTo, 1), PropertyType.TIMESTAMP)));

                final Set<Date> uniqDateSet = reqDateDQL
                        .createStatement(getSession()).<Date>list()
                        .stream().map(CommonBaseDateUtil::trimTime).collect(Collectors.toCollection(LinkedHashSet::new));

                if (uniqDateSet.isEmpty())
                    throw new ApplicationException(EcfOrgUnitManager.instance().getProperty("error.noRequest"));

                final MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "r");
                builder.addJoinFetch("r", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, "direction");
                builder.addJoinFetch("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "request");
                builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.enrollmentDirection().enrollmentCampaign().s(), enrollmentCampaign));

                final EntrantDataUtil dataUtil = new EntrantDataUtil(getSession(), enrollmentCampaign, builder, EntrantDataUtil.DetailLevel.EXAM_PASS_WITH_MARK_STATISTICS);

                final Map<EnrollmentDirection, EnrollmentDirectionExamSetData> direction2examSetDataMap = ExamSetUtil.getDirectionExamSetDataMap(getSession(), enrollmentCampaign, null, null);

                final List<StateExamSubjectMark> subjectMarkList = new DQLSelectBuilder().fromEntity(StateExamSubjectMark.class, "e").column(property("e"))
                        .where(eq(property(StateExamSubjectMark.certificate().accepted().fromAlias("e")), value(true)))
                        .where(eq(property(StateExamSubjectMark.certificate().sent().fromAlias("e")), value(true)))
                        .createStatement(getSession()).list();

                final Map<MultiKey, List<EntrantStateExamCertificate>> examCertificateMap = new HashMap<>();
                for (StateExamSubjectMark mark : subjectMarkList)
                {
                    final Long entrantId = mark.getCertificate().getEntrant().getId();
                    final Long subjectId = mark.getSubject().getId();
                    final int subjectMark = mark.getMark();

                    final MultiKey key = new MultiKey(entrantId, subjectId, subjectMark);

                    SafeMap.safeGet(examCertificateMap, key, LinkedList.class)
                            .add(mark.getCertificate());
                }

                for (Map.Entry<MultiKey, List<EntrantStateExamCertificate>> entry : examCertificateMap.entrySet())
                    Collections.sort(entry.getValue(), new EntityComparator<>(new EntityOrder(EntrantStateExamCertificate.id(), OrderDirection.asc)));

                setProcessState(5);

                for (Date date : uniqDateSet)
                {
                    resultList.add(doCreatePackage(ecfOrgUnit, applicationsStatus, enrollmentCampaign, date, dataUtil, direction2examSetDataMap, examCertificateMap));

                    setProcessState(95, date, new ArrayList<>(uniqDateSet));
                }
            }
        });


        return resultList;
    }

    protected EcfOrgUnitPackage4ApplicationsInfo doCreatePackage(final EcfOrgUnit ecfOrgUnit, EcfCatalogItem applicationsStatus,
                                                                 final EnrollmentCampaign enrollmentCampaign, Date entrantRequestRegDate,
                                                                 final EntrantDataUtil dataUtil, final Map<EnrollmentDirection,
            EnrollmentDirectionExamSetData> direction2examSetDataMap, final Map<MultiKey, List<EntrantStateExamCertificate>> examCertificateMap) throws Exception
    {
        final EcfOrgUnitPackage4ApplicationsInfo dbo = new EcfOrgUnitPackage4ApplicationsInfo();
        dbo.setCreationDate(new Date());
        dbo.setEcfOrgUnit(ecfOrgUnit);
        dbo.setStatus(applicationsStatus);
        dbo.setEnrollmentCampaign(enrollmentCampaign);
        dbo.setXmlPackageRequest(this.generateXmlPackageRequest(dbo, entrantRequestRegDate, dataUtil, direction2examSetDataMap, examCertificateMap));
        dbo.setEntrantRequestRegDate(entrantRequestRegDate);
        this.save(dbo);
        return dbo;
    }


    protected byte[] generateXmlPackageRequest(final EcfOrgUnitPackage4ApplicationsInfo dbo, final Date entrantRequestRegDate,
                                               final EntrantDataUtil dataUtil,
                                               final Map<EnrollmentDirection, EnrollmentDirectionExamSetData> direction2examSetDataMap,
                                               final Map<MultiKey, List<EntrantStateExamCertificate>> examCertificateMap) throws Exception
    {
        final EnrollmentCampaign enrollmentCampaign = dbo.getEnrollmentCampaign();
        final EcfOrgUnit ecfOrgUnit = dbo.getEcfOrgUnit();
        EcfCatalogItem status = dbo.getStatus();

        // второй (а значит и первый) пакеты должен быть выгружены
        {
            final List<EcfOrgUnitPackage4AdmissionInfo> packages2 = new DQLSelectBuilder()
                    .fromEntity(EcfOrgUnitPackage4AdmissionInfo.class, "x").column(property("x"))
                    .where(eq(property(EcfOrgUnitPackage4AdmissionInfo.enrollmentCampaign().fromAlias("x")), value(enrollmentCampaign)))
                    .where(eq(property(EcfOrgUnitPackage4AdmissionInfo.ecfOrgUnit().fromAlias("x")), value(ecfOrgUnit)))
                    .createStatement(this.getSession()).list();
            if (packages2.isEmpty())
            {
                throw new ApplicationException(EcfOrgUnitManager.instance().getProperty("error.applications.no-admission-info-pkg", enrollmentCampaign.getTitle()));
            }
        }

        final Map<Discipline2RealizationWayRelation, ConversionScale> conversionScaleMap = IEntrantDAO.instance.get().getConversionScales(enrollmentCampaign);

        final IEcfEcgDao ecgDao = EcfConverterManager.instance().ecg();
        final IEcfConverterDao4EduOu eduOrgUnitDao = EcfConverterManager.instance().educationOrgUnit();
        final IEcfFinSourceDao finSourceDao = EcfConverterManager.instance().financingSource();
        final IEcfConverter<EducationOrgUnit> eduOrgUnitConverter = eduOrgUnitDao.getConverter();
        final IEcfConverter<DevelopForm> educationFormConverter = EcfConverterManager.instance().educationForm().getConverter();
        final IEcfConverter<SetDiscipline> disciplineConverter = EcfConverterManager.instance().setDiscipline().getConverter();
        final IEcfConverter<EntranceDisciplineType> disciplineTypeConverter = EcfConverterManager.instance().disciplineType().getConverter();
        final IEcfConverter<OlympiadDiplomaDegree> olympiadTypeConverter = EcfConverterManager.instance().olympiadType().getConverter();
        final IEcfConverter<IdentifiableWrapper> olympiadConverter = EcfConverterManager.instance().olympiad().getConverter();
        final IEcfConverter<StateExamSubject> stateExamSubjectConverter = EcfConverterManager.instance().stateExamSubject().getConverter();
        final IEcfBenefitDao benefitDao = EcfConverterManager.instance().benefit();

        Map<EnrollmentDirection, Map<StudentCategory, String>> competitionGroupUidMap = getCompetitionGroupUidMap(ecfOrgUnit, enrollmentCampaign);

        final Applications applications = new Applications();

        /** (EntrantRequest, IEcfEcgKey) -> Applications.Application */
        final Map<MultiKey, Applications.Application> applicationMap = SafeMap.get(key -> {
            final EntrantRequest request = (EntrantRequest) key.getKey(0);
            final IEcfEcgKey ecgKey = (IEcfEcgKey) key.getKey(1);

            final Application application = new Application();
            application.setUID(uid(ecfOrgUnit, enrollmentCampaign, request, ecgKey));

            // данные персоны
            application.setEntrant(getEntrant(ecfOrgUnit, request.getEntrant()));

            // контейнеров
            application.setFinSourceAndEduForms(new Application.FinSourceAndEduForms());
            application.setApplicationDocuments(new Application.ApplicationDocuments());

            // Application / ApplicationDocuments / IdentityDocument - Основное удостоврение личности
            application.getApplicationDocuments().setIdentityDocument(getIdentityDocument(application.getUID(), request.getEntrant().getPerson().getIdentityCard()));

            // Application / ApplicationDocuments / OtherIdentityDocuments - Другие удостоврения личности
            getList(IdentityCard.class, IdentityCard.person().s(), request.getEntrant().getPerson());
            List<TIdentityDocument> otherDocs = getList(IdentityCard.class, IdentityCard.person().s(), request.getEntrant().getPerson())
                    .stream()
                    .filter(ic -> !ic.isActive())
                    .map(ic-> getIdentityDocument(application.getUID(), ic))
                    .collect(Collectors.toList());

            if (!otherDocs.isEmpty())
            {
                Application.ApplicationDocuments.OtherIdentityDocuments otherIdentityDocuments = new Application.ApplicationDocuments.OtherIdentityDocuments();
                application.getApplicationDocuments().setOtherIdentityDocuments(otherIdentityDocuments);
                otherIdentityDocuments.getIdentityDocument().addAll(otherDocs);
            }

            //  Application / ApplicationDocuments / EduDocuments - Документы об образовании
            if (request.getEntrant().getPerson().getPersonEduInstitution() != null)
            {
                application.getApplicationDocuments().setEduDocuments(new Application.ApplicationDocuments.EduDocuments());
                application.getApplicationDocuments().getEduDocuments().getEduDocument().add(getEduDocuments(application.getUID(), request));
            }

            // данные заявления
            application.setApplicationNumber(application.getUID() + "/" + request.getRegNumber());
            application.setRegistrationDate(xml(request.getRegDate()));
            application.setNeedHostel(request.getEntrant().getPerson().isNeedDormitory());
            application.setStatusID(getEcfItemId4EntrantRequest(request, status));
            //application.setLastDenyDate(); в этом году необязательна для передачи
            //application.setStatusComment(); не передаем в этом году

            // добавляем в список и возвращаем
            applications.getApplication().add(application);
            return application;
        });

        final Map<MultiKey, Application.EntranceTestResults.EntranceTestResult> applicationEntranceTestResultMap = SafeMap.get(key -> {
            final Application application = (Application) key.getKey(0);
            final ChosenEntranceDiscipline chosenDiscipline = (ChosenEntranceDiscipline) key.getKey(1);

            if (chosenDiscipline.getFinalMark() == null) // если нет финальной оценки, то весь блок не передаем
                return null;

            if (application.getEntranceTestResults() == null)
                application.setEntranceTestResults(new Application.EntranceTestResults());

            final Application.EntranceTestResults.EntranceTestResult entranceTestResult = new Application.EntranceTestResults.EntranceTestResult();
            entranceTestResult.setUID(EcfOrgUnitManager.instance().uidRegistry().doGetUid(Long.toString(chosenDiscipline.getId(), 32)));
            entranceTestResult.setResultValue(new BigDecimal(chosenDiscipline.getFinalMark().longValue()));
            entranceTestResult.setResultSourceTypeID(getEcfItemId4FinalMarkSource(chosenDiscipline.getFinalMarkSource()));

            RequestedEnrollmentDirection requestedDirection = chosenDiscipline.getChosenEnrollmentDirection();
            String competitionGroupUid = getCompetitionGroupUid(ecfOrgUnit, competitionGroupUidMap, requestedDirection);
            entranceTestResult.setCompetitiveGroupUID(
                    competitionGroupUid + "-" + finSourceDao.getSource(requestedDirection).getFisItemCode()
                            + getCrimeaPostfix(requestedDirection.getEntrantRequest().getEntrant().isFromCrimea()));

            {
                long resultSourceTypeID = entranceTestResult.getResultSourceTypeID();
                //результат ЕГЭ
                if (resultSourceTypeID == 1)
                {
                    if (!conversionScaleMap.containsKey(chosenDiscipline.getEnrollmentCampaignDiscipline()))
                        throw new ApplicationException(EcfOrgUnitManager.instance().getProperty("error.applications.no-conversionScale", enrollmentCampaign.getTitle()));

                    final MultiKey certificateKey = new MultiKey(requestedDirection.getEntrantRequest().getEntrant().getId(), conversionScaleMap.get(chosenDiscipline.getEnrollmentCampaignDiscipline()).getSubject().getId(), chosenDiscipline.getFinalMark().intValue());
                    if (examCertificateMap.containsKey(certificateKey) && !examCertificateMap.get(certificateKey).isEmpty())
                    {
//                            entranceTestResult.setResultDocument(new Applications.Application.EntranceTestResults.EntranceTestResult.ResultDocument());

                        final EntrantStateExamCertificate certificate = examCertificateMap.get(certificateKey).get(0);
                        String[] numberParts = EntrantStateExamCertificateNumberFormatter.split(certificate.getNumber());
                        // блок ResultDocument передаем для каждого свидетельства ЕГЭ, приложенного к заявлению, у которого номер не ХХ000000000ХХ
                        if (numberParts[1].matches("[0]+"))
                            entranceTestResult.setResultDocument(null);
                        else
                        {
                            entranceTestResult.setResultDocument(new Application.EntranceTestResults.EntranceTestResult.ResultDocument());
                            entranceTestResult.getResultDocument().setEgeDocumentID(uid(ecfOrgUnit.getUid(), certificate, application.getUID()));
                        }

//                            entranceTestResult.getResultDocument().setEgeDocumentID(uid(ecfOrgUnit.getUid(), certificate, application.getUID()));
                    }
                }
                // Олимпиада
                else if (resultSourceTypeID == 3)
                {
                    entranceTestResult.setResultDocument(new Application.EntranceTestResults.EntranceTestResult.ResultDocument());

                    final List<Discipline2OlympiadDiplomaRelation> olympiadList = benefitDao.getDiscipline2OlympiadDiplomaRelations(requestedDirection.getEntrantRequest().getEntrant());

                    if (!olympiadList.isEmpty())
                    {
                        OlympiadDiploma olympiadDiploma = olympiadList.get(0).getDiploma();
                        String olympicDocumentUID = getSimpleUID(olympiadDiploma, olympiadDiploma.getDiplomaType().getTitle(), null);
                        if (olympiadDiploma.getDiplomaType().getCode().equals(UniecDefines.DIPLOMA_TYPE_OTHER))
                        {
                            TOlympicDocument olympicDocument = new TOlympicDocument();
                            olympicDocument.setUID(olympicDocumentUID);
                            olympicDocument.setOriginalReceivedDate(xmlDate(requestedDirection.getEntrantRequest().getRegDate()));
                            olympicDocument.setDocumentSeries(olympiadDiploma.getSeria());
                            olympicDocument.setDocumentNumber(olympiadDiploma.getNumber());
                            //olympicDocument.setDocumentDate(); не передаем в этом году
                            olympicDocument.setDiplomaTypeID(olympiadTypeConverter.getEcfCatalogItem(olympiadDiploma.getDegree(), true).getFisID());
                            olympicDocument.setOlympicID(olympiadConverter.getEcfCatalogItem(new IdentifiableWrapper((long) olympiadDiploma.getOlympiad().hashCode(), olympiadDiploma.getOlympiad()), true).getFisID());
                            entranceTestResult.getResultDocument().setOlympicDocument(olympicDocument);
                        }
                        else
                        {
                            TOlympicTotalDocument olympicTotalDocument = new TOlympicTotalDocument();
                            olympicTotalDocument.setUID(olympicDocumentUID);
                            olympicTotalDocument.setOriginalReceivedDate(xmlDate(requestedDirection.getEntrantRequest().getRegDate()));
                            olympicTotalDocument.setDocumentSeries(olympiadDiploma.getSeria());
                            olympicTotalDocument.setDocumentNumber(olympiadDiploma.getNumber());
                            //olympicTotalDocument.setOlympicPlace(); не передаем в этом году
                            //olympicTotalDocument.setOlympicDate(); не передаем в этом году
                            olympicTotalDocument.setDiplomaTypeID(olympiadTypeConverter.getEcfCatalogItem(olympiadDiploma.getDegree(), true).getFisID());
                            olympicTotalDocument.setSubjects(new TOlympicTotalDocument.Subjects());
                            entranceTestResult.getResultDocument().setOlympicTotalDocument(olympicTotalDocument);
                            {
                                TOlympicTotalDocument.Subjects.SubjectBriefData subjectBriefData = new TOlympicTotalDocument.Subjects.SubjectBriefData();
                                subjectBriefData.setSubjectID(disciplineConverter.getEcfCatalogItem(chosenDiscipline.getEnrollmentCampaignDiscipline(), true).getFisID());
                                entranceTestResult.getResultDocument().getOlympicTotalDocument().getSubjects().getSubjectBriefData().add(subjectBriefData);
                            }
                        }
                    }
                }
                else if (resultSourceTypeID == 2)
                {
                    entranceTestResult.setResultDocument(new Application.EntranceTestResults.EntranceTestResult.ResultDocument());
                    TInstitutionDocument document = new TInstitutionDocument();
                    document.setDocumentNumber("1");
                    document.setDocumentTypeID(1L);
                    entranceTestResult.getResultDocument().setInstitutionDocument(document);
                }
            }

            {
                final EnrollmentDirection enrollmentDirection = requestedDirection.getEnrollmentDirection();
                final StudentCategory studentCategory = enrollmentCampaign.isExamSetDiff() ? requestedDirection.getStudentCategory() : UniDaoFacade.getCoreDao().getCatalogItem(StudentCategory.class, StudentCategory.STUDENT_CATEGORY_STUDENT);
                ExamSet examSet = ExamSetUtil.getExamSet(enrollmentDirection, studentCategory);

                // получаем информацию по наборам экзаменов для направления приема
                final EnrollmentDirectionExamSetData examSetData = direction2examSetDataMap.get(enrollmentDirection);

                // получаем структуру набора экзамена для текущего выбранного направления приема
                final ExamSetStructure examSetStructure = examSetData.getCategoryExamSetStructure(studentCategory);

                // получаем список строк структуры набора экзамена
                final List<ExamSetStructureItem> itemList = examSetStructure.getItemList();

                // находим наилучшее распределение выбранных вступительных испытаний по строкам структуры набора
                final List<ChosenEntranceDiscipline> distributionList = Arrays.asList(MarkDistributionUtil.getChosenDistribution(dataUtil.getChosenEntranceDisciplineSet(requestedDirection), itemList));

                if (distributionList.indexOf(chosenDiscipline) == -1 || examSet == null || examSet.getSetItemList().get(distributionList.indexOf(chosenDiscipline)) == null || examSet.getSetItemList().get(distributionList.indexOf(chosenDiscipline)).getList() == null || examSet.getSetItemList().get(distributionList.indexOf(chosenDiscipline)).getList().isEmpty())
                    throw new ApplicationException(EcfOrgUnitManager.instance().getProperty("error.applications.no-entranceDiscipline", requestedDirection.getEntrantRequest().getEntrant().getPerson().getFio()));

                EntranceDiscipline entranceDiscipline = null;
                final List<EntranceDiscipline> entranceDisciplineList = examSet.getSetItemList().get(distributionList.indexOf(chosenDiscipline)).getList();
                if (entranceDisciplineList.size() > 1)
                {
                    for (EntranceDiscipline discipline : entranceDisciplineList)
                        if (enrollmentDirection.equals(discipline.getEnrollmentDirection()))
                        {
                            if (entranceDiscipline != null)
                                throw new ApplicationException(EcfOrgUnitManager.instance().getProperty("error.applications.no-entranceDiscipline", requestedDirection.getEntrantRequest().getEntrant().getPerson().getFio()));
                            entranceDiscipline = discipline;
                        }
                }
                else
                {
                    entranceDiscipline = entranceDisciplineList.get(0);
                }

                if (!enrollmentDirection.equals(entranceDiscipline.getEnrollmentDirection()) || !entranceDiscipline.getStudentCategory().equals(studentCategory))
                    throw new ApplicationException(EcfOrgUnitManager.instance().getProperty("error.applications.no-entranceDiscipline", requestedDirection.getEntrantRequest().getEntrant().getPerson().getFio()));

                entranceTestResult.setEntranceTestTypeID(disciplineTypeConverter.getEcfCatalogItem(entranceDiscipline.getType(), true).getFisID());
            }

            {
                entranceTestResult.setEntranceTestSubject(new TEntranceTestSubject());
                final EcfCatalogItem disciplineEcfCatalogItem = disciplineConverter.getEcfCatalogItem(chosenDiscipline.getEnrollmentCampaignDiscipline(), false);
                if (disciplineEcfCatalogItem != null)
                    entranceTestResult.getEntranceTestSubject().setSubjectID(disciplineEcfCatalogItem.getFisID());
                else
                    entranceTestResult.getEntranceTestSubject().setSubjectName(chosenDiscipline.getEnrollmentCampaignDiscipline().getTitle());
            }

            application.getEntranceTestResults().getEntranceTestResult().add(entranceTestResult);
            return entranceTestResult;
        });

        final Map<MultiKey, Applications.Application.ApplicationDocuments.EgeDocuments.EgeDocument> applicationEgeDocumentsMap = SafeMap.get(key -> {
            final Application application = (Application) key.getKey(0);
            final EntrantStateExamCertificate certificate = (EntrantStateExamCertificate) key.getKey(1);

            String[] numberParts = EntrantStateExamCertificateNumberFormatter.split(certificate.getNumber());
            // блок EgeDocument передаем для каждого свидетельства ЕГЭ, приложенного к заявлению, у которого номер не ХХ000000000ХХ
            if (numberParts[1].matches("[0]+"))
                return null;

            if (application.getApplicationDocuments().getEgeDocuments() == null)
                application.getApplicationDocuments().setEgeDocuments(new Application.ApplicationDocuments.EgeDocuments());

            Application.ApplicationDocuments.EgeDocuments.EgeDocument egeDocument = new Application.ApplicationDocuments.EgeDocuments.EgeDocument();
            egeDocument.setUID(uid(ecfOrgUnit.getUid(), certificate, application.getUID()));
            egeDocument.setDocumentNumber(numberParts[0] + "-" + numberParts[1] + "-" + numberParts[2]);

            {
                Integer documentYear;
                final Date issuanceDate = certificate.getIssuanceDate();
                if (issuanceDate == null)
                {
                    String number = certificate.getNumber();
                    documentYear = Integer.valueOf("20" + number.substring(number.length() - 2, number.length()));
                }
                else
                {
                    GregorianCalendar date = new GregorianCalendar();
                    date.setTime(issuanceDate);
                    documentYear = date.get(Calendar.YEAR);
                }
                egeDocument.setDocumentYear(documentYear.longValue());
            }

            {
                final List<StateExamSubjectMark> markList = new DQLSelectBuilder().fromEntity(StateExamSubjectMark.class, "m").column(property("m"))
                        .where(eq(property(StateExamSubjectMark.certificate().fromAlias("m")), value(certificate)))
                        .createStatement(getSession()).list();

                egeDocument.setSubjects(new Application.ApplicationDocuments.EgeDocuments.EgeDocument.Subjects());
                for (StateExamSubjectMark mark : markList)
                {
                    Application.ApplicationDocuments.EgeDocuments.EgeDocument.Subjects.SubjectData subjectData = new Application.ApplicationDocuments.EgeDocuments.EgeDocument.Subjects.SubjectData();
                    subjectData.setSubjectID(stateExamSubjectConverter.getEcfCatalogItem(mark.getSubject(), true).getFisID()); // ???? Английский язык - 24 ????
                    subjectData.setValue(mark.getMark());
                    egeDocument.getSubjects().getSubjectData().add(subjectData);
                }
            }

            application.getApplicationDocuments().getEgeDocuments().getEgeDocument().add(egeDocument);
            return egeDocument;
        });

        final Map<MultiKey, List<Applications.Application.IndividualAchievements.IndividualAchievement>> individualAchievementMap = SafeMap.get(key -> {
            final Application application = (Application) key.getKey(0);
            final EntrantRequest request = (EntrantRequest) key.getKey(1);

            if (request == null)
                return null;

            final IEcfConverter<IndividualProgress> individProgressIEcfConverter = EcfConverterManager.instance().individualAchievement().getConverter();
            List<EntrantIndividualProgress> entrantAchievementsList = getAchievementsList(request);

            if (entrantAchievementsList.isEmpty())
                return null;

            if (application.getIndividualAchievements() == null)
                application.setIndividualAchievements(new Application.IndividualAchievements());

            List<TCustomDocument> customDocuments = new ArrayList<>();
            int achievIndex = 1;
            for (EntrantIndividualProgress item : entrantAchievementsList)
            {
                Application.IndividualAchievements.IndividualAchievement achievement = new Application.IndividualAchievements.IndividualAchievement();
                achievement.setInstitutionAchievementUID(item.getIndividualProgressType().getCode());
                achievement.setIAMark((long) item.getMark());
                EcfCatalogItem catalogFisItem = individProgressIEcfConverter.getEcfCatalogItem(item.getIndividualProgressType(), false);
                achievement.setIAUID(application.getUID() + "." + getSimpleUID(item, item.getIndividualProgressType().getTitle(), null));

                String iaDocumentUID = application.getUID()
                        + "-" + (catalogFisItem == null ? "" : catalogFisItem.getFisID())
                        + "-" + String.valueOf(achievIndex++);
                achievement.setIADocumentUID(iaDocumentUID);
                application.getIndividualAchievements().getIndividualAchievement().add(achievement);

                TCustomDocument customDocument = new TCustomDocument();
                customDocument.setUID(iaDocumentUID);
                customDocument.setDocumentName("Подтверждение индивидуального достижения");
                customDocument.setDocumentOrganization("Внешняя организация");
                customDocument.setDocumentDate(xmlDate(request.getRegDate()));
                customDocuments.add(customDocument);
            }

            if (!customDocuments.isEmpty())
            {
                if (application.getApplicationDocuments().getCustomDocuments() == null)
                    application.getApplicationDocuments().setCustomDocuments(new Application.ApplicationDocuments.CustomDocuments());
                application.getApplicationDocuments().getCustomDocuments().getCustomDocument().addAll(customDocuments);
            }

            return application.getIndividualAchievements().getIndividualAchievement();
        });

        final Map<MultiKey, List<RequestedEnrollmentDirection>> appReqDirMap = new HashMap<>();

        // разбиваем ВНП по КГФИС: RequestedEnrollmentDirection -> key = (entrant, ecgFis(direction))
        DebugUtils.debug(logger, new DebugUtils.Section("RequestedEnrollmentDirection -> key = (entrant, ecgFis(direction))")
        {
            @Override
            public void execute()
            {
                final List<EnrollmentDirection> directions = EcfOrgUnitManager.instance().dao().getEnrollmentDirections(ecfOrgUnit, enrollmentCampaign);
                for (final EnrollmentDirection dir : directions)
                {

                    final EcfCatalogItem fisEduForm = educationFormConverter.getEcfCatalogItem(dir.getEducationOrgUnit().getDevelopForm(), false);
                    if (null == fisEduForm)
                    {
                        throw new ApplicationException(EcfOrgUnitManager.instance().getProperty("error.compaign.missing-develop-form", dir.getEducationOrgUnit().getDevelopForm().getCode(), dir.getEducationOrgUnit().getDevelopForm().getTitle()));
                    }

                    final EcfCatalogItem fisEduLvl = eduOrgUnitDao.getEcfItem4EducationLevel(dir.getEducationOrgUnit());
                    if (null == fisEduLvl)
                    {
                        throw new ApplicationException(EcfOrgUnitManager.instance().getProperty("error.admission.no-fis-level", dir.getEducationOrgUnit().getTitle()));
                    }

                    final EcfCatalogItem fisDirection = eduOrgUnitConverter.getEcfCatalogItem(dir.getEducationOrgUnit(), false);
                    if (null == fisDirection)
                    {
                        throw new ApplicationException(EcfOrgUnitManager.instance().getProperty("error.admission.no-fis-direction", dir.getEducationOrgUnit().getTitle()));
                    }

                    // Добавлять сразу несколько пакетов, разделяя заявления по дате добавления заявлений в Uni.
                    final List<RequestedEnrollmentDirection> requestedDirections = new DQLSelectBuilder()
                            .fromEntity(RequestedEnrollmentDirection.class, "rd").column(property("rd"))
                            .where(inDay(RequestedEnrollmentDirection.entrantRequest().regDate().fromAlias("rd"), entrantRequestRegDate))
                            .where(eq(property(RequestedEnrollmentDirection.enrollmentDirection().fromAlias("rd")), value(dir)))
                            .createStatement(getSession()).list();
                    for (final RequestedEnrollmentDirection requestedDirection : requestedDirections)
                    {
                        final IEcfEcgKey ecgKey;
                        try
                        {
                            if (null == (ecgKey = ecgDao.getKey(requestedDirection.getEnrollmentDirection(), requestedDirection.getStudentCategory())))
                            {
                                throw new ApplicationException(""); // хз что произошло (ниже будет написаны общие слова)
                            }
                        }
                        catch (final ApplicationException e)
                        {
                            final String message = EcfOrgUnitManager.instance().getProperty(
                                    "error.applications.no-ecg-key",
                                    requestedDirection.getEntrantRequest().getEntrant().getTitle(),
                                    requestedDirection.getEntrantRequest().getStringNumber(),
                                    requestedDirection.getEnrollmentDirection().getTitle()
                            ) + (StringUtils.isBlank(e.getMessage()) ? "" : (": " + e.getMessage()));

                            if (Debug.isDisplay())
                            {
                                ContextLocal.getInfoCollector().add(message);
                                continue;
                            }
                            else
                            {
                                throw new ApplicationException(message, e);
                            }
                        }

                        final Application application = applicationMap.get(new MultiKey(requestedDirection.getEntrantRequest(), ecgKey));

                        // формы освоения, источники финансирования и виды ЦП
                        {
                            final List<EcfCatalogItem> sourceList = finSourceDao.getSourceList(requestedDirection);
                            if (sourceList.isEmpty())
                                throw new IllegalStateException();

                            Map<StudentCategory, String> uidByStudCategory = competitionGroupUidMap.get(requestedDirection.getEnrollmentDirection());
                            String uid = null;
                            if (uidByStudCategory != null)
                                uid = uidByStudCategory.get(requestedDirection.getStudentCategory());
                            if (uid == null)
                                uid = getCompetitionGroupUid(ecfOrgUnit, requestedDirection.getEnrollmentDirection(), requestedDirection.getStudentCategory().getCode(), null);
                            final String competitionGroupUid = uid;

                            sourceList.forEach(source -> addFinSourceEduForm(application, competitionGroupUid, requestedDirection, source));
                        }

                        // группипуем ВНП по заявлениям ФИС. делаем так потому что далее нужен сразу весь список ВНП по заявлению ФИС или по ГК ФИС, сейчас на одно ГК ФИС - одно заявление ФИС
                        {
                            List<RequestedEnrollmentDirection> directionList = SafeMap.safeGet(appReqDirMap, new MultiKey(requestedDirection.getEntrantRequest(), ecgKey), LinkedList.class);
                            directionList.add(requestedDirection);
                        }
                    }
                }
            }
        });

        DebugUtils.debug(logger, new DebugUtils.Section("Other blocks")
        {
            @Override
            public void execute()
            {
                for (Map.Entry<MultiKey, List<RequestedEnrollmentDirection>> entry : appReqDirMap.entrySet())
                {
                    // Общая льгота предоставленная абитуриенту
                    addApplicationCommonBenefits(ecfOrgUnit, applicationMap.get(entry.getKey()), entry.getValue(),
                                                 benefitDao, finSourceDao, competitionGroupUidMap);

                    //Индивидуальные достижения
                    {
                        final Application application = applicationMap.get(entry.getKey());
                        individualAchievementMap.get(new MultiKey(application, entry.getValue().get(0).getEntrantRequest()));
                    }

                    // Результаты вступительных испытаний
                    {
                        final IEcfEcgKey ecgKey = (IEcfEcgKey) entry.getKey().getKey(1);
                        final Application application = applicationMap.get(entry.getKey());
                        final List<RequestedEnrollmentDirection> directionList = new LinkedList<>(entry.getValue());
                        Collections.sort(directionList, new EntityComparator<>(new EntityOrder(RequestedEnrollmentDirection.competitionKind().priority().s(), OrderDirection.asc)));

                        RequestedEnrollmentDirection takeReqDir;

                        List<RequestedEnrollmentDirection> takeDirList = new LinkedList<>();
                        for (RequestedEnrollmentDirection direction : directionList)
                        {
                            if (takeDirList.isEmpty())
                                takeDirList.add(direction);
                            else if (takeDirList.get(0).getCompetitionKind().getPriority() == direction.getCompetitionKind().getPriority())
                                takeDirList.add(direction);
                        }

                        if (takeDirList.size() > 1)
                        {
                            Collections.sort(takeDirList, new EntityComparator<>(new EntityOrder(RequestedEnrollmentDirection.priority().s(), OrderDirection.asc)));
                            takeReqDir = takeDirList.get(0);
                        }
                        else
                            takeReqDir = takeDirList.get(0);

                        if (!takeReqDir.getCompetitionKind().getCode().equals(UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES))
                        {
                            for (ChosenEntranceDiscipline chosenDiscipline : dataUtil.getChosenEntranceDisciplineSet(takeReqDir))
                            {
                                applicationEntranceTestResultMap.get(new MultiKey(application, chosenDiscipline, ecgKey));
                            }
                        }
                    }

                    // Свидетельства о результатах ЕГЭ
                    {
                        final Application application = applicationMap.get(entry.getKey());
                        final List<RequestedEnrollmentDirection> directionList = new LinkedList<>(entry.getValue());

                        final Long entrantId = directionList.get(0).getEntrantRequest().getEntrant().getId();
                        Boolean originalReceived = false;
                        Set<CoreCollectionUtils.Pair<StateExamSubject, Double>> subjectMarkSet = new HashSet<>();
                        for (RequestedEnrollmentDirection reqDir : directionList)
                        {
                            if (reqDir.isOriginalDocumentHandedIn())
                                originalReceived = true;

                            subjectMarkSet.addAll(dataUtil.getChosenEntranceDisciplineSet(reqDir).stream()
                                                          .filter(chosenDiscipline -> chosenDiscipline.getFinalMarkSource() != null
                                                                  && getEcfItemId4FinalMarkSource(chosenDiscipline.getFinalMarkSource()) == 1)
                                                          .map(chosenDiscipline -> new CoreCollectionUtils.Pair<>(conversionScaleMap.get(chosenDiscipline.getEnrollmentCampaignDiscipline()).getSubject(),
                                                                                                                  chosenDiscipline.getFinalMark()))
                                                          .collect(Collectors.toList()));
                        }

                        for (CoreCollectionUtils.Pair<StateExamSubject, Double> pair : subjectMarkSet)
                        {
                            final MultiKey certificateKey = new MultiKey(entrantId, pair.getX().getId(), pair.getY().intValue());
                            if (!examCertificateMap.containsKey(certificateKey) || examCertificateMap.get(certificateKey).isEmpty())
                                continue;

                            final EntrantStateExamCertificate certificate = examCertificateMap.get(certificateKey).get(0);

                            applicationEgeDocumentsMap.get(new MultiKey(application, certificate, originalReceived));
                        }
                    }

                    // Документ об образовании - Признак предоставления оригиналов документов
                    final Application application = applicationMap.get(entry.getKey());
                    entry.getValue().stream()
                            .filter(reqDir -> reqDir.isOriginalDocumentHandedIn() && reqDir.getEntrantRequest().getEntrant().getPerson().getPersonEduInstitution() != null)
                            .forEach(reqDir -> {
                                XMLGregorianCalendar xmlDate = xmlDate(reqDir.getEntrantRequest().getRegDate());
                                application.getApplicationDocuments().getEduDocuments().getEduDocument().stream()
                                        .forEach(eduDocument -> {
                                            if (eduDocument.getSchoolCertificateBasicDocument() != null)
                                                eduDocument.getSchoolCertificateBasicDocument().setOriginalReceivedDate(xmlDate);
                                            if (eduDocument.getSchoolCertificateDocument() != null)
                                                eduDocument.getSchoolCertificateDocument().setOriginalReceivedDate(xmlDate);
                                            if (eduDocument.getHighEduDiplomaDocument() != null)
                                                eduDocument.getHighEduDiplomaDocument().setOriginalReceivedDate(xmlDate);
                                            if (eduDocument.getIncomplHighEduDiplomaDocument() != null)
                                                eduDocument.getIncomplHighEduDiplomaDocument().setOriginalReceivedDate(xmlDate);
                                            if (eduDocument.getMiddleEduDiplomaDocument() != null)
                                                eduDocument.getMiddleEduDiplomaDocument().setOriginalReceivedDate(xmlDate);
                                            if (eduDocument.getBasicDiplomaDocument() != null)
                                                eduDocument.getBasicDiplomaDocument().setOriginalReceivedDate(xmlDate);
                                            if (eduDocument.getEduCustomDocument() != null)
                                                eduDocument.getEduCustomDocument().setOriginalReceivedDate(xmlDate);
                                        });
                            });
                }
            }
        });

        // obtain new root
        final Root root = this.newRoot(dbo.getEcfOrgUnit());
        root.getPackageData().setApplications(applications);
        return FisServiceImpl.toXml(root);
    }


    protected void addFinSourceEduForm(Application application, String competitiveGroupUID,
                                       RequestedEnrollmentDirection requestedDirection, EcfCatalogItem finSource)
    {
        Application.FinSourceAndEduForms.FinSourceEduForm finSourceEduForm = new Application.FinSourceAndEduForms.FinSourceEduForm();

        EnrollmentDirection direction = requestedDirection.getEnrollmentDirection();

        finSourceEduForm.setCompetitiveGroupUID(competitiveGroupUID
                                                        + "-" + finSource.getFisItemCode()
                                                        + getCrimeaPostfix(requestedDirection.getEntrantRequest().getEntrant().isFromCrimea()));

        boolean isTargetAdmission = EcfFinSourceDao.isTargetAdmission(finSource);
        if (isTargetAdmission)
        {
            TargetAdmissionKind targetAdmissionKind = getUsedSimpleTargetAdmissionKind(direction.getEnrollmentCampaign());
            if (targetAdmissionKind != null)
                finSourceEduForm.setTargetOrganizationUID(uid4TargetAdmissionKind(direction.getEnrollmentCampaign(), targetAdmissionKind));
        }

        if (requestedDirection.isAgree4Enrollment())
        {
            Date date = requestedDirection.getEntrantRequest().getRegDate();
            XMLGregorianCalendar xmlDate = xml(date);
            finSourceEduForm.setIsAgreedDate(xmlDate);
        }

        application.getFinSourceAndEduForms().getFinSourceEduForm().add(finSourceEduForm);
    }

    /**
     * @return regexp=\d{3}-\d{3}-\d{3} \d{2}
     */
    protected String snils(String snils)
    {
        if (StringUtils.isEmpty(snils))
            return null;

        try
        {
            snils = snils.replaceAll("\\D", "");
            return snils.substring(0, 3) + "-" + snils.substring(3, 6) + "-" + snils.substring(6, 9) + " " + snils.substring(9);
        }
        catch (Exception ignored)
        {
        }

        return null;
    }

    protected TIdentityDocument getIdentityDocument(String applicationUID, IdentityCard identityCard)
    {
        TIdentityDocument document = new TIdentityDocument();
        document.setUID(applicationUID + "-" + EcfOrgUnitManager.instance().uidRegistry().doGetUid(Long.toString(identityCard.getId(), 32)));

        ICitizenship citizenship = identityCard.getCitizenship();
        EcfCatalogItem ecfCitizenship;
        if (citizenship instanceof AddressCountry)
        {
            final IEcfConverter<AddressCountry> addressCountryConverter = EcfConverterManager.instance().addressCountry().getConverter();
            ecfCitizenship = addressCountryConverter.getEcfCatalogItem((AddressCountry) citizenship, false);
        }
        else
        {
            ecfCitizenship = DataAccessServices.dao().getByNaturalId(new EcfCatalogItemGen.NaturalId("7", "237"));
        }
        if (null == ecfCitizenship)
        {
            throw new ApplicationException(EcfOrgUnitManager.instance().getProperty(
                    "error.applications.no-fis-country",
                    identityCard.getTitle(),
                    identityCard.getCitizenship().getTitle()
            ));
        }
        document.setNationalityTypeID(ecfCitizenship.getFisID());

        document.setFirstName(identityCard.getFirstName());
        if (identityCard.getMiddleName() != null)
            document.setMiddleName(identityCard.getMiddleName());
        document.setLastName(identityCard.getLastName());
        document.setGenderID(getGenderId(identityCard.getSex()));
        document.setIdentityDocumentTypeID(getIdCardTypeID(identityCard.getCardType()));
        document.setBirthDate(xmlDate(identityCard.getBirthDate()));
        document.setBirthPlace(identityCard.getBirthPlace());
        document.setDocumentSeries(identityCard.getSeria());
        document.setDocumentNumber(identityCard.getNumber());
        document.setDocumentDate(xmlDate(identityCard.getIssuanceDate()));
        document.setDocumentOrganization(identityCard.getIssuancePlace());

        return document;
    }

    // Application / ApplicationDocuments / EduDocuments / EduDocument
    protected Application.ApplicationDocuments.EduDocuments.EduDocument getEduDocuments(String applicationUID, EntrantRequest request)
    {
        Application.ApplicationDocuments.EduDocuments.EduDocument eduDocument = new Application.ApplicationDocuments.EduDocuments.EduDocument();

        PersonEduInstitution personEduInstitution = request.getEntrant().getPerson().getPersonEduInstitution();
        final int ecfEduDocumentNumber = getEcfEduDocumentNumber(personEduInstitution);
        String seria = personEduInstitution.getSeria();
        String number = personEduInstitution.getNumber();
        String uid = applicationUID + "-" + getSimpleUID(personEduInstitution, personEduInstitution.getShortTitle(), personEduInstitution.getFullTitle());
        switch (ecfEduDocumentNumber)
        {
            case 1:
                TSchoolCertificateDocument schoolCertificateBasicDocument = new TSchoolCertificateDocument();
                schoolCertificateBasicDocument.setUID(uid);
                schoolCertificateBasicDocument.setDocumentSeries(seria);
                schoolCertificateBasicDocument.setDocumentNumber(number);
                eduDocument.setSchoolCertificateBasicDocument(schoolCertificateBasicDocument);
            case 2:
                TSchoolCertificateDocument schoolCertificateDocument = new TSchoolCertificateDocument();
                schoolCertificateDocument.setUID(uid);
                schoolCertificateDocument.setDocumentSeries(seria);
                schoolCertificateDocument.setDocumentNumber(number);
                eduDocument.setSchoolCertificateDocument(schoolCertificateDocument);
                break;
            case 3:
                THighEduDiplomaDocument highEduDiplomaDocument = new THighEduDiplomaDocument();
                highEduDiplomaDocument.setUID(uid);
                highEduDiplomaDocument.setDocumentSeries(seria);
                highEduDiplomaDocument.setDocumentNumber(number);
                eduDocument.setHighEduDiplomaDocument(highEduDiplomaDocument);
                break;
            case 4:
                TIncomplHighEduDiplomaDocument incomplHighEduDiplomaDocument = new TIncomplHighEduDiplomaDocument();
                incomplHighEduDiplomaDocument.setUID(uid);
                incomplHighEduDiplomaDocument.setDocumentSeries(seria);
                incomplHighEduDiplomaDocument.setDocumentNumber(number);
                eduDocument.setIncomplHighEduDiplomaDocument(incomplHighEduDiplomaDocument);
                break;
            case 5:
                TMiddleEduDiplomaDocument middleEduDiplomaDocument = new TMiddleEduDiplomaDocument();
                middleEduDiplomaDocument.setUID(uid);
                middleEduDiplomaDocument.setDocumentSeries(seria);
                middleEduDiplomaDocument.setDocumentNumber(number);
                eduDocument.setMiddleEduDiplomaDocument(middleEduDiplomaDocument);
                break;
            case 6:
                TBasicDiplomaDocument basicDiplomaDocument = new TBasicDiplomaDocument();
                basicDiplomaDocument.setUID(uid);
                basicDiplomaDocument.setDocumentSeries(seria);
                basicDiplomaDocument.setDocumentNumber(number);
                eduDocument.setBasicDiplomaDocument(basicDiplomaDocument);
                break;
            default:
                TEduCustomDocument eduCustomDocument = new TEduCustomDocument();
                eduCustomDocument.setUID(uid);
                eduCustomDocument.setDocumentDate(xmlDate(personEduInstitution.getIssuanceDate()));
                eduCustomDocument.setDocumentTypeNameText(personEduInstitution.getFullTitle());
                eduDocument.setEduCustomDocument(eduCustomDocument);
                break;
        }

        return eduDocument;
    }


    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=9864767")
    protected long getIdCardTypeID(IdentityCardType cardType)
    {
        if (null != cardType)
            switch (cardType.getCode())
            {
                case IdentityCardTypeCodes.PASPORT_GRAJDANINA_ROSSIYSKOY_FEDERATSII:
                    return 1L;
                case IdentityCardTypeCodes.ROSSIYSKIY_ZAGRANICHNYY_PASPORT:
                    return 2L;
                case IdentityCardTypeCodes.PASPORT_GRAJDANINA_INOSTRANNOGO_GOSUDARSTVA:
                    return 3L;
                case IdentityCardTypeCodes.SVIDETELSTVO_O_ROJDENII:
                    return 4L;
                case IdentityCardTypeCodes.DRUGOY_DOKUMENT:
                    return 5L;
                case IdentityCardTypeCodes.PASPORT_GRAJDANINA_S_S_S_R:
                    return 9L;
                case IdentityCardTypeCodes.VREMENNOE_UDOSTOVERENIE:
                    return 10L;
                case IdentityCardTypeCodes.VID_NA_JITELSTVO:
                    return 11L;
            }

        return 0L;
    }

    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=9864749")
    protected long getGenderId(final Sex sex)
    {
        if (null != sex)
        {
            if (SexCodes.MALE.equals(sex.getCode()))
            {
                return 1L;
            }
            if (SexCodes.FEMALE.equals(sex.getCode()))
            {
                return 2L;
            }
        }
        return 0L;
    }

    /**
     * fisCatalogCode='4'
     *
     * @return элемент справочника ФИС №4. Статус заявления для Заявление абитуриента Uni
     */
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=9864745")
    protected long getEcfItemId4EntrantRequest(EntrantRequest request, EcfCatalogItem status)
    {
        return request.isTakeAwayDocument() ? 6L : status.getFisID();
    }

    /**
     * fisCatalogCode='6'
     *
     * @param finalMarkSource 1..11
     * @return элемент справочника ФИС №6. Основание для оценки для Источника итога
     */
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=9865192")
    protected long getEcfItemId4FinalMarkSource(int finalMarkSource)
    {
        if (2 <= finalMarkSource && finalMarkSource <= 7)
            return 2L;
        if (finalMarkSource == 1)
            return 3L;
        if (8 <= finalMarkSource && finalMarkSource <= 11)
            return 1L;

        return 0L;
    }

    /**
     * Определяет по законченному учебному заведению в Уни тип EduDocuments ФИС.
     *
     * @return порядковый номер из таблицы в wiki типа EduDocuments ФИС
     */
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=9864384")
    protected int getEcfEduDocumentNumber(final PersonEduInstitution personEduInstitution)
    {
        final String eduDocTypeCode = personEduInstitution.getDocumentType().getCode();

        if (eduDocTypeCode.equals(EducationDocumentTypeCodes.ATTESTATE) && personEduInstitution.getEducationLevel().getCode().equals(EducationLevelStageCodes.MAIN_SCHOOL))
        {
            return 1;
        }
        if (eduDocTypeCode.equals(EducationDocumentTypeCodes.ATTESTATE) && personEduInstitution.getEducationLevel().getCode().equals(EducationLevelStageCodes.MIDDLE_SCHOOL))
        {
            return 2;
        }
        if (eduDocTypeCode.equals(EducationDocumentTypeCodes.DIPLOMA))
        {
            if (personEduInstitution.getEducationLevel().getCode().equals(EducationLevelStageCodes.HIGH_PROF) && !personEduInstitution.getEducationLevelStage().getCode().equals(EducationLevelStageCodes.NEPOLNOE_VYSSHEE))
                return 3;
        }
        if (eduDocTypeCode.equals(EducationDocumentTypeCodes.DIPLOMA) && personEduInstitution.getEducationLevelStage().getCode().equals(EducationLevelStageCodes.NEPOLNOE_VYSSHEE))
        {
            return 4;
        }
        if (eduDocTypeCode.equals(EducationDocumentTypeCodes.DIPLOMA) && personEduInstitution.getEducationLevel().getCode().equals(EducationLevelStageCodes.MIDDLE_PROF))
        {
            return 5;
        }
        if (eduDocTypeCode.equals(EducationDocumentTypeCodes.DIPLOMA) && personEduInstitution.getEducationLevel().getCode().equals(EducationLevelStageCodes.BEGIN_PROF))
        {
            return 6;
        }

        return -1;
        //        throw new ApplicationException("Для основного документа о полученном образовании (его типа и уровня/ступени образования) абитуриента «" +personEduInstitution.getPerson().getFullFio() +
        //            "» нет соответствующего типа документа об образовании в ФИС.", new IllegalStateException("Unknown personEduInstitution"));
    }

    private List<EntrantIndividualProgress> getAchievementsList(EntrantRequest request)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EntrantIndividualProgress.class, "ind")
                .where(eq(property("ind", EntrantIndividualProgress.entrant()), value(request.getEntrant())))
                .where(IndProg2Request4Exclude.getExpression4ExcludeEntrantIndividualProgressById(request.getId(), property("ind")));

        return builder.createStatement(getSession()).list();
    }

    // Applications / Application / Entrant
    protected Application.Entrant getEntrant(EcfOrgUnit ecfOrgUnit, Entrant entrant)
    {
        Person person = entrant.getPerson();

        Application.Entrant fisEntrant = new Application.Entrant();
        fisEntrant.setUID(uid(ecfOrgUnit, entrant.getEnrollmentCampaign(), entrant));
        fisEntrant.setFirstName(person.getIdentityCard().getFirstName());
        fisEntrant.setLastName(person.getIdentityCard().getLastName());
        fisEntrant.setMiddleName(person.getIdentityCard().getMiddleName());
        fisEntrant.setGenderID(getGenderId(person.getIdentityCard().getSex()));

        // Applications / Application / Entrant / EmailOrMailAddress
        Application.Entrant.EmailOrMailAddress emailOrMailAddress = new Application.Entrant.EmailOrMailAddress();
        String email = person.getEmail();
        emailOrMailAddress.setEmail(StringUtils.isEmpty(email) ? "no" : email);

        // Applications / Application / Entrant / EmailOrMailAddress / MailAddress
        AddressBase address = person.getAddressRegistration();

        Application.Entrant.EmailOrMailAddress.MailAddress mailAddress = new Application.Entrant.EmailOrMailAddress.MailAddress();

        Long regionId = 90L;
        Long townTypeId = 0L;
        String addressTitle = "";
        if (address != null)
        {
            addressTitle = address.getTitle();
            if (address instanceof AddressDetailed)
            {
                EcfCatalogItem fisRegion = EcfConverterManager.instance().converterDao4Region().getConverter()
                        .getEcfCatalogItem(getRegion(((AddressDetailed) address).getSettlement()), false);
                if (fisRegion != null)
                    regionId = fisRegion.getFisID();

                townTypeId = EcfConverterManager.getTownId(((AddressDetailed) address).getSettlement());
                if (townTypeId == null)
                    townTypeId = 0L;
            }
        }
        mailAddress.setAddress(addressTitle);
        mailAddress.setRegionID(regionId);
        mailAddress.setTownTypeID(townTypeId);

        emailOrMailAddress.setMailAddress(mailAddress);
        fisEntrant.setEmailOrMailAddress(emailOrMailAddress);

        if (entrant.isFromCrimea())
        {
            Application.Entrant.IsFromKrym isFromKrym = new Application.Entrant.IsFromKrym();
            isFromKrym.setDocumentUID(EcfOrgUnitManager.instance().uidRegistry().doGetUid(Long.toString(entrant.getPerson().getIdentityCard().getId(), 32)));
            fisEntrant.setIsFromKrym(isFromKrym);
        }

        return fisEntrant;
    }

    protected AddressItem getRegion(AddressItem address)
    {
        while (address.getParent() != null)
            address = address.getParent();
        return address;
    }

    protected void addApplicationCommonBenefits(
            EcfOrgUnit ecfOrgUnit, Application application,
            List<RequestedEnrollmentDirection> directions,
            IEcfBenefitDao benefitDao, IEcfFinSourceDao finSourceDao,
            Map<EnrollmentDirection, Map<StudentCategory, String>> competitionGroupUidMap)
    {
        if (CollectionUtils.isEmpty(directions))
            return;

        RequestedEnrollmentDirection reqDir = null;
        Map<Integer, Integer> codePriorityMap = new HashMap<>();

        for (RequestedEnrollmentDirection requestedDirection : directions)
        {
            Integer code = benefitDao.getBenefitCode4RequestedEnrollmentDirection(requestedDirection);
            if (code == null)
                continue;

            // получаем минимальный код из всех полученных вариантов кодов,
            // если полученный минимальный код соответствует нескольким ВНП в рамках одной КГ Уни, то берем ВНП с минимальным приоритетом
            {
                boolean skip = false;
                for (Integer beforeCode : codePriorityMap.keySet())
                    if (beforeCode < code)
                        skip = true;

                if (codePriorityMap.containsKey(code))
                    if (codePriorityMap.get(code) < requestedDirection.getPriority())
                        skip = true;

                if (skip)
                    continue;
            }

            reqDir = requestedDirection;
            codePriorityMap.put(code, requestedDirection.getPriority());
        }

        // если Преимущество при равенстве конкурсных баллов(3), то данную льготу передавать только если существует студент пред. зачисления для какого-либо ВНП из заявления ФИС
        if (reqDir != null && benefitDao.getBenefitCode4RequestedEnrollmentDirection(reqDir) == 3)
        {
            final Long count = new DQLSelectBuilder()
                    .fromEntity(PreliminaryEnrollmentStudent.class, "s")
                    .column(property(PreliminaryEnrollmentStudent.id().fromAlias("s")))
                    .where(in(property(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().fromAlias("s")), directions))
                    .createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();

            if (count == 0)
                reqDir = null;
        }

        if (reqDir == null)
            return;

        Map<StudentCategory, String> uidByStudCategory = competitionGroupUidMap.get(reqDir.getEnrollmentDirection());
        String competitionGroupUid = null;
        if (uidByStudCategory != null)
            competitionGroupUid = uidByStudCategory.get(reqDir.getStudentCategory());
        if (competitionGroupUid == null)
            competitionGroupUid = getCompetitionGroupUid(ecfOrgUnit, reqDir.getEnrollmentDirection(), reqDir.getStudentCategory().getCode(), null);
        competitionGroupUid = competitionGroupUid + "-" + finSourceDao.getSource(reqDir).getFisItemCode()
                + getCrimeaPostfix(reqDir.getEntrantRequest().getEntrant().isFromCrimea());

        application.setApplicationCommonBenefits(new Application.ApplicationCommonBenefits());
        application.getApplicationCommonBenefits().getApplicationCommonBenefit().add(
                getApplicationCommonBenefit(application, reqDir, competitionGroupUid, benefitDao));

    }

    protected Application.ApplicationCommonBenefits.ApplicationCommonBenefit getApplicationCommonBenefit(
            Application application, RequestedEnrollmentDirection requestedDirection,
            String competitionGroupUid, IEcfBenefitDao benefitDao
    )
    {
        final Application.ApplicationCommonBenefits.ApplicationCommonBenefit commonBenefit =
                new Application.ApplicationCommonBenefits.ApplicationCommonBenefit();
        commonBenefit.setUID(EcfOrgUnitManager.instance().uidRegistry().doGetUid(application.getUID()));
        commonBenefit.setCompetitiveGroupUID(competitionGroupUid);

        Integer benefitCode = benefitDao.getBenefitCode4RequestedEnrollmentDirection(requestedDirection);
        Long benefitFisId = benefitDao.getEcfItem4BenefitCode(benefitCode).getFisID();
        commonBenefit.setBenefitKindID(benefitFisId);
        commonBenefit.setDocumentReason(new Application.ApplicationCommonBenefits.ApplicationCommonBenefit.DocumentReason());

        String documentUid = application.getUID() + "-" + String.valueOf(benefitFisId);
        XMLGregorianCalendar receivedDate = xmlDate(requestedDirection.getEntrantRequest().getRegDate());

        switch (benefitCode)
        {
            case 1:
                OlympiadDiploma diploma = benefitDao.getOlympiadDiplomas(requestedDirection.getEntrantRequest().getEntrant()).get(0);
                if (diploma.getDiplomaType().getCode().equals("53"))//всеросийская олимпиада
                    commonBenefit.setDocumentTypeID(10L);
                else
                    commonBenefit.setDocumentTypeID(9L);

                TOlympicDocument olympicDocument = new TOlympicDocument();
                olympicDocument.setUID(documentUid);
                olympicDocument.setOriginalReceivedDate(receivedDate);
                olympicDocument.setDocumentSeries(diploma.getSeria());
                olympicDocument.setDocumentNumber(diploma.getNumber());

                if (diploma.getDegree().getCode().equals("1"))//победитель
                    olympicDocument.setDiplomaTypeID(1L);
                else
                    olympicDocument.setDiplomaTypeID(2L);
                olympicDocument.setOlympicID(EcfConverterManager.instance().olympiad().getConverter()
                                                     .getEcfCatalogItem(new IdentifiableWrapper((long) diploma.getOlympiad().hashCode(), diploma.getOlympiad()), true)
                                                     .getFisID());
                commonBenefit.getDocumentReason().setOlympicDocument(olympicDocument);
                return commonBenefit;
            case 2:
                PersonBenefit personBenefit = benefitDao.getPersonBenefit(requestedDirection.getEntrantRequest().getEntrant());
                EcfCatalogItem benefitFisItem = EcfConverterManager.instance().converterDao4Benefit().getConverter().getEcfCatalogItem(personBenefit.getBenefit(), false);
                if(benefitFisItem==null) break;
                String benefitFisCode = benefitFisItem.getFisItemCode();
                commonBenefit.setDocumentTypeID(benefitFisItem.getFisID());

                if (benefitFisCode.equals("11") || benefitFisCode.equals("12")) //Медицина
                {
                    Application.ApplicationCommonBenefits.ApplicationCommonBenefit.DocumentReason.MedicalDocuments medicalDocuments =
                            new Application.ApplicationCommonBenefits.ApplicationCommonBenefit.DocumentReason.MedicalDocuments();

                    Application.ApplicationCommonBenefits.ApplicationCommonBenefit.DocumentReason.MedicalDocuments.BenefitDocument benefitDocument =
                            new Application.ApplicationCommonBenefits.ApplicationCommonBenefit.DocumentReason.MedicalDocuments.BenefitDocument();
                    if (benefitFisCode.equals("11"))//Справка об установлении инвалидности
                    {
                        TDisabilityDocument disabilityDocument = new TDisabilityDocument();
                        disabilityDocument.setDocumentDate(receivedDate);
                        disabilityDocument.setUID(documentUid + "-1");
                        disabilityDocument.setDocumentSeries("0");
                        disabilityDocument.setDocumentNumber("0");
                        disabilityDocument.setDisabilityTypeID(1L);
                        benefitDocument.setDisabilityDocument(disabilityDocument);
                    }
                    else//Заключение психолого-медико-педагогической комиссии
                    {
                        TMedicalDocument medicalDocument = new TMedicalDocument();
                        medicalDocument.setUID(documentUid + "-2");
                        medicalDocument.setDocumentNumber("0");
                        benefitDocument.setMedicalDocument(medicalDocument);
                    }
                    medicalDocuments.setBenefitDocument(benefitDocument);

                    TAllowEducationDocument allowEducationDocument = new TAllowEducationDocument();
                    allowEducationDocument.setUID(documentUid);
                    allowEducationDocument.setDocumentNumber("0");
                    allowEducationDocument.setDocumentDate(receivedDate);
                    medicalDocuments.setAllowEducationDocument(allowEducationDocument);

                    commonBenefit.getDocumentReason().setMedicalDocuments(medicalDocuments);
                    return commonBenefit;
                }

                if (benefitFisCode.equals("30") ) //Сироты
                {
                    TOrphanDocument orphanDocument = new TOrphanDocument();
                    orphanDocument.setUID(documentUid);
                    orphanDocument.setOrphanCategoryID(8L);
                    orphanDocument.setOriginalReceivedDate(receivedDate);
                    orphanDocument.setDocumentName(benefitDao.getDocumentTypeNameText(requestedDirection));
                    orphanDocument.setDocumentOrganization("Внешняя организация");
                    orphanDocument.setDocumentDate(receivedDate);
                    commonBenefit.getDocumentReason().setOrphanDocument(orphanDocument);
                    return commonBenefit;
                }

                if (benefitFisCode.equals("14") || benefitFisCode.equals("31"))//Ветераны
                {
                    TVeteranDocument veteranDocument = new TVeteranDocument();
                    veteranDocument.setUID(documentUid);
                    veteranDocument.setVeteranCategoryID(1L);
                    veteranDocument.setOriginalReceivedDate(receivedDate);
                    veteranDocument.setDocumentName(benefitDao.getDocumentTypeNameText(requestedDirection));
                    veteranDocument.setDocumentOrganization("Внешняя организация");
                    veteranDocument.setDocumentDate(receivedDate);
                    commonBenefit.getDocumentReason().setVeteranDocument(veteranDocument);
                    return commonBenefit;
                }

                break;
        }


        TCustomDocument customDocument = new TCustomDocument();
        customDocument.setUID(documentUid);
        customDocument.setOriginalReceivedDate(receivedDate);
        customDocument.setDocumentName(benefitDao.getDocumentTypeNameText(requestedDirection));
        customDocument.setDocumentOrganization("Внешняя организация");
        customDocument.setDocumentDate(receivedDate);
        commonBenefit.setDocumentTypeID(15L);
        commonBenefit.getDocumentReason().setCustomDocument(customDocument);
        return commonBenefit;
    }

}
