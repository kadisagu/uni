package ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodItem;
import ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodKey;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Даты приемной кампании ФИС (период)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EcfSettingsECPeriodItemGen extends EntityBase
 implements INaturalIdentifiable<EcfSettingsECPeriodItemGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodItem";
    public static final String ENTITY_NAME = "ecfSettingsECPeriodItem";
    public static final int VERSION_HASH = -2103083118;
    private static IEntityMeta ENTITY_META;

    public static final String L_KEY = "key";
    public static final String P_STAGE = "stage";
    public static final String P_DATE_START = "dateStart";
    public static final String P_DATE_END = "dateEnd";
    public static final String P_DATE_ORDER = "dateOrder";
    public static final String P_DATE_END_TITLE = "dateEndTitle";
    public static final String P_DATE_ORDER_TITLE = "dateOrderTitle";
    public static final String P_DATE_START_TITLE = "dateStartTitle";
    public static final String P_TITLE = "title";

    private EcfSettingsECPeriodKey _key;     // Ключ
    private int _stage;     // Этап (порядковый номер)
    private Date _dateStart;     // Дата начала приема
    private Date _dateEnd;     // Дата окончания приема
    private Date _dateOrder;     // Дата включения в приказ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Ключ. Свойство не может быть null.
     */
    @NotNull
    public EcfSettingsECPeriodKey getKey()
    {
        return _key;
    }

    /**
     * @param key Ключ. Свойство не может быть null.
     */
    public void setKey(EcfSettingsECPeriodKey key)
    {
        dirty(_key, key);
        _key = key;
    }

    /**
     * Порядковый номер этапа
     *
     * @return Этап (порядковый номер). Свойство не может быть null.
     */
    @NotNull
    public int getStage()
    {
        return _stage;
    }

    /**
     * @param stage Этап (порядковый номер). Свойство не может быть null.
     */
    public void setStage(int stage)
    {
        dirty(_stage, stage);
        _stage = stage;
    }

    /**
     * @return Дата начала приема.
     */
    public Date getDateStart()
    {
        return _dateStart;
    }

    /**
     * @param dateStart Дата начала приема.
     */
    public void setDateStart(Date dateStart)
    {
        dirty(_dateStart, dateStart);
        _dateStart = dateStart;
    }

    /**
     * @return Дата окончания приема.
     */
    public Date getDateEnd()
    {
        return _dateEnd;
    }

    /**
     * @param dateEnd Дата окончания приема.
     */
    public void setDateEnd(Date dateEnd)
    {
        dirty(_dateEnd, dateEnd);
        _dateEnd = dateEnd;
    }

    /**
     * @return Дата включения в приказ.
     */
    public Date getDateOrder()
    {
        return _dateOrder;
    }

    /**
     * @param dateOrder Дата включения в приказ.
     */
    public void setDateOrder(Date dateOrder)
    {
        dirty(_dateOrder, dateOrder);
        _dateOrder = dateOrder;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EcfSettingsECPeriodItemGen)
        {
            if (withNaturalIdProperties)
            {
                setKey(((EcfSettingsECPeriodItem)another).getKey());
                setStage(((EcfSettingsECPeriodItem)another).getStage());
            }
            setDateStart(((EcfSettingsECPeriodItem)another).getDateStart());
            setDateEnd(((EcfSettingsECPeriodItem)another).getDateEnd());
            setDateOrder(((EcfSettingsECPeriodItem)another).getDateOrder());
        }
    }

    public INaturalId<EcfSettingsECPeriodItemGen> getNaturalId()
    {
        return new NaturalId(getKey(), getStage());
    }

    public static class NaturalId extends NaturalIdBase<EcfSettingsECPeriodItemGen>
    {
        private static final String PROXY_NAME = "EcfSettingsECPeriodItemNaturalProxy";

        private Long _key;
        private int _stage;

        public NaturalId()
        {}

        public NaturalId(EcfSettingsECPeriodKey key, int stage)
        {
            _key = ((IEntity) key).getId();
            _stage = stage;
        }

        public Long getKey()
        {
            return _key;
        }

        public void setKey(Long key)
        {
            _key = key;
        }

        public int getStage()
        {
            return _stage;
        }

        public void setStage(int stage)
        {
            _stage = stage;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EcfSettingsECPeriodItemGen.NaturalId) ) return false;

            EcfSettingsECPeriodItemGen.NaturalId that = (NaturalId) o;

            if( !equals(getKey(), that.getKey()) ) return false;
            if( !equals(getStage(), that.getStage()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getKey());
            result = hashCode(result, getStage());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getKey());
            sb.append("/");
            sb.append(getStage());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EcfSettingsECPeriodItemGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EcfSettingsECPeriodItem.class;
        }

        public T newInstance()
        {
            return (T) new EcfSettingsECPeriodItem();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "key":
                    return obj.getKey();
                case "stage":
                    return obj.getStage();
                case "dateStart":
                    return obj.getDateStart();
                case "dateEnd":
                    return obj.getDateEnd();
                case "dateOrder":
                    return obj.getDateOrder();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "key":
                    obj.setKey((EcfSettingsECPeriodKey) value);
                    return;
                case "stage":
                    obj.setStage((Integer) value);
                    return;
                case "dateStart":
                    obj.setDateStart((Date) value);
                    return;
                case "dateEnd":
                    obj.setDateEnd((Date) value);
                    return;
                case "dateOrder":
                    obj.setDateOrder((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "key":
                        return true;
                case "stage":
                        return true;
                case "dateStart":
                        return true;
                case "dateEnd":
                        return true;
                case "dateOrder":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "key":
                    return true;
                case "stage":
                    return true;
                case "dateStart":
                    return true;
                case "dateEnd":
                    return true;
                case "dateOrder":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "key":
                    return EcfSettingsECPeriodKey.class;
                case "stage":
                    return Integer.class;
                case "dateStart":
                    return Date.class;
                case "dateEnd":
                    return Date.class;
                case "dateOrder":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EcfSettingsECPeriodItem> _dslPath = new Path<EcfSettingsECPeriodItem>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EcfSettingsECPeriodItem");
    }
            

    /**
     * @return Ключ. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodItem#getKey()
     */
    public static EcfSettingsECPeriodKey.Path<EcfSettingsECPeriodKey> key()
    {
        return _dslPath.key();
    }

    /**
     * Порядковый номер этапа
     *
     * @return Этап (порядковый номер). Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodItem#getStage()
     */
    public static PropertyPath<Integer> stage()
    {
        return _dslPath.stage();
    }

    /**
     * @return Дата начала приема.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodItem#getDateStart()
     */
    public static PropertyPath<Date> dateStart()
    {
        return _dslPath.dateStart();
    }

    /**
     * @return Дата окончания приема.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodItem#getDateEnd()
     */
    public static PropertyPath<Date> dateEnd()
    {
        return _dslPath.dateEnd();
    }

    /**
     * @return Дата включения в приказ.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodItem#getDateOrder()
     */
    public static PropertyPath<Date> dateOrder()
    {
        return _dslPath.dateOrder();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodItem#getDateEndTitle()
     */
    public static SupportedPropertyPath<String> dateEndTitle()
    {
        return _dslPath.dateEndTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodItem#getDateOrderTitle()
     */
    public static SupportedPropertyPath<String> dateOrderTitle()
    {
        return _dslPath.dateOrderTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodItem#getDateStartTitle()
     */
    public static SupportedPropertyPath<String> dateStartTitle()
    {
        return _dslPath.dateStartTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodItem#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EcfSettingsECPeriodItem> extends EntityPath<E>
    {
        private EcfSettingsECPeriodKey.Path<EcfSettingsECPeriodKey> _key;
        private PropertyPath<Integer> _stage;
        private PropertyPath<Date> _dateStart;
        private PropertyPath<Date> _dateEnd;
        private PropertyPath<Date> _dateOrder;
        private SupportedPropertyPath<String> _dateEndTitle;
        private SupportedPropertyPath<String> _dateOrderTitle;
        private SupportedPropertyPath<String> _dateStartTitle;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Ключ. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodItem#getKey()
     */
        public EcfSettingsECPeriodKey.Path<EcfSettingsECPeriodKey> key()
        {
            if(_key == null )
                _key = new EcfSettingsECPeriodKey.Path<EcfSettingsECPeriodKey>(L_KEY, this);
            return _key;
        }

    /**
     * Порядковый номер этапа
     *
     * @return Этап (порядковый номер). Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodItem#getStage()
     */
        public PropertyPath<Integer> stage()
        {
            if(_stage == null )
                _stage = new PropertyPath<Integer>(EcfSettingsECPeriodItemGen.P_STAGE, this);
            return _stage;
        }

    /**
     * @return Дата начала приема.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodItem#getDateStart()
     */
        public PropertyPath<Date> dateStart()
        {
            if(_dateStart == null )
                _dateStart = new PropertyPath<Date>(EcfSettingsECPeriodItemGen.P_DATE_START, this);
            return _dateStart;
        }

    /**
     * @return Дата окончания приема.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodItem#getDateEnd()
     */
        public PropertyPath<Date> dateEnd()
        {
            if(_dateEnd == null )
                _dateEnd = new PropertyPath<Date>(EcfSettingsECPeriodItemGen.P_DATE_END, this);
            return _dateEnd;
        }

    /**
     * @return Дата включения в приказ.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodItem#getDateOrder()
     */
        public PropertyPath<Date> dateOrder()
        {
            if(_dateOrder == null )
                _dateOrder = new PropertyPath<Date>(EcfSettingsECPeriodItemGen.P_DATE_ORDER, this);
            return _dateOrder;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodItem#getDateEndTitle()
     */
        public SupportedPropertyPath<String> dateEndTitle()
        {
            if(_dateEndTitle == null )
                _dateEndTitle = new SupportedPropertyPath<String>(EcfSettingsECPeriodItemGen.P_DATE_END_TITLE, this);
            return _dateEndTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodItem#getDateOrderTitle()
     */
        public SupportedPropertyPath<String> dateOrderTitle()
        {
            if(_dateOrderTitle == null )
                _dateOrderTitle = new SupportedPropertyPath<String>(EcfSettingsECPeriodItemGen.P_DATE_ORDER_TITLE, this);
            return _dateOrderTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodItem#getDateStartTitle()
     */
        public SupportedPropertyPath<String> dateStartTitle()
        {
            if(_dateStartTitle == null )
                _dateStartTitle = new SupportedPropertyPath<String>(EcfSettingsECPeriodItemGen.P_DATE_START_TITLE, this);
            return _dateStartTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodItem#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(EcfSettingsECPeriodItemGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EcfSettingsECPeriodItem.class;
        }

        public String getEntityName()
        {
            return "ecfSettingsECPeriodItem";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getDateEndTitle();

    public abstract String getDateOrderTitle();

    public abstract String getDateStartTitle();

    public abstract String getTitle();
}
