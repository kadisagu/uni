package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity;

import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.gen.EcfOrgUnitPackage4DelApplicationsGen;

/**
 * Пакет: на удаление всех заявлений
 *
 * Root / DataForDelete / Applications
 */
public class EcfOrgUnitPackage4DelApplications extends EcfOrgUnitPackage4DelApplicationsGen
{
    @Override
    public String getTitle() {
        if (getEnrollmentCampaign() == null) {
            return this.getClass().getSimpleName();
        }
        return "Пакет на удаление всех заявлений «" + getEnrollmentCampaign().getTitle() + "»";
    }
}