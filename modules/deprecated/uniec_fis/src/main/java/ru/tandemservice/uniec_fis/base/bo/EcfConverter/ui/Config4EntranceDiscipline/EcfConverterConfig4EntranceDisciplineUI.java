package ru.tandemservice.uniec_fis.base.bo.EcfConverter.ui.Config4EntranceDiscipline;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.EcfConverterManager;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.ui.EcfConverterConfigUI;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author vdanilov
 */
public class EcfConverterConfig4EntranceDisciplineUI extends EcfConverterConfigUI {

    public static final String PARAM_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String PARAM_DISCIPLINE_FILTERE = "disciplineFilter";
    public static final String PARAM_DIRECTION_FILTER = "directionFilter";

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();

        if (getSettings().get(PARAM_ENROLLMENT_CAMPAIGN) == null)
        {
            final List<Object> ecList = DataAccessServices.dao().getList(
                    new DQLSelectBuilder()
                            .fromEntity(EnrollmentCampaign.class, "c")
                            .column(property("c"))
                            .order(property("c", EnrollmentCampaign.id()), OrderDirection.desc)
                            .top(1)
            );

            if (CoreCollectionUtils.isEmpty(ecList)) return;

            getSettings().set(PARAM_ENROLLMENT_CAMPAIGN, ecList.get(0));
        }
    }

    public void onClickAutoSync()
    {
        EcfConverterManager.instance().setDiscipline().doAutoSync();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(PARAM_ENROLLMENT_CAMPAIGN, getSettings().get(PARAM_ENROLLMENT_CAMPAIGN));
        dataSource.put(PARAM_DISCIPLINE_FILTERE, getSettings().get(PARAM_DISCIPLINE_FILTERE));
        dataSource.put(PARAM_DIRECTION_FILTER, getSettings().get(PARAM_DIRECTION_FILTER));
    }
}
