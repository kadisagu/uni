/**
 *$Id$
 */
package ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.olympiadType;

import ru.tandemservice.uniec.entity.catalog.OlympiadDiplomaDegree;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.IEcfConverterStaticDao;

/**
 * @author Alexander Shaburov
 * @since 10.01.13
 */
public interface IEcfConverterDao4OlympiadType extends IEcfConverterStaticDao<OlympiadDiplomaDegree>
{
}
