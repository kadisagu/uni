package ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.discipline;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.IEcfConverterDao;

/**
 * @author vdanilov
 */
public interface IEcfConverterDao4EntranceDiscipline extends IEcfConverterDao<EntranceDiscipline> {

    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doAutoSync();

}
