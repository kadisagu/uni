package ru.tandemservice.uniec_fis.base.bo.EcfCatalog.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit;

/**
 * @author vdanilov
 */
public interface IEcfCatalogDao extends INeedPersistenceSupport {

    /** обновляет данные справочников ФИС */
    @Transactional(readOnly=false, propagation=Propagation.REQUIRED)
    void doSyncCatalogs(EcfOrgUnit orgUnit);

}
