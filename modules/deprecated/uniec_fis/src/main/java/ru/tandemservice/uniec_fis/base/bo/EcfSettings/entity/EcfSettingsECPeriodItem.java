package ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;

import ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.gen.EcfSettingsECPeriodItemGen;

/**
 * Даты приемной кампании ФИС (период)
 */
public class EcfSettingsECPeriodItem extends EcfSettingsECPeriodItemGen implements Comparable<EcfSettingsECPeriodItem>, ITitled
{

    public EcfSettingsECPeriodItem() {
    }

    public EcfSettingsECPeriodItem(EcfSettingsECPeriodItem source) {
        this.update(source, true);
    }

    public EcfSettingsECPeriodItem(EcfSettingsECPeriodKey key, int stage) {
        setKey(key);
        setStage(stage);
    }

    @Override
    public int compareTo(EcfSettingsECPeriodItem o)
    {
        // если это одно и то же, то 0
        if (this.equals(o)) {
            return 0;
        }

        int result;

        // по ключу
        if (0 != (result = this.getKey().compareTo(o.getKey()))) {
            return result;
        }

        // по номеру этапа
        return (this.getStage() - o.getStage());
    }


    @Override
    @EntityDSLSupport
    public String getTitle() {
        if (getKey() == null) {
            return this.getClass().getSimpleName();
        }
        if (getKey().getSize() == 1) { return ""; }
        return getStage() + " этап";
    }

    @Override
    @EntityDSLSupport
    public String getDateStartTitle() {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateStart());
    }

    @Override
    @EntityDSLSupport
    public String getDateEndTitle() {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateEnd());
    }

    @Override
    @EntityDSLSupport
    public String getDateOrderTitle() {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateOrder());
    }

    public String getUid() {
        return (getKey().getUid()+"."+getStage());
    }


}