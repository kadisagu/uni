package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage4ApplicationsInfo;

import java.util.Date;
import java.util.List;

/**
 * @author vdanilov
 */
public interface IEcfOrgUnitPackage4ApplicationsDao extends IEcfOrgUnitPackageDao {

    /**
     * Создает пакеты (пакет 3: Заявления абитуриентов). На каждую дату добавления заявлений в Uni добавится по пакету.
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    List<EcfOrgUnitPackage4ApplicationsInfo> doCreatePackages(EcfOrgUnit ecfOrgUnit,  EcfCatalogItem applicationsStatus, EnrollmentCampaign enrollmentCampaign, Date regDateFrom, Date regDateTo) throws Exception;

}
