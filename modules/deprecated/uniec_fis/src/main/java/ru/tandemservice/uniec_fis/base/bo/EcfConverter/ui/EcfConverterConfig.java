package ru.tandemservice.uniec_fis.base.bo.EcfConverter.ui;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.IPresenterExtPointBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.logic.ItemListDataHandler;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.IEcfConverter;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.IEcfConverterDao;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public abstract class EcfConverterConfig extends BusinessComponentManager {

    public static final String PARAM_CONVERTER_NAME = "converterName";
    public static final String PROPERTY_ECF_VALUE = "value";

    public static final String ECF_CONVERTER_RECORD_DS = "ecfConverterRecordDS";
    public static final String ECF_CONVERTER_VALUE_DS = "ecfConverterValueDS";

    protected abstract IEcfConverterDao getConverterDao();

    public PresenterExtPoint presenterExtPoint(IPresenterExtPointBuilder builder) {
        return builder
        .addDataSource(this.searchListDS(ECF_CONVERTER_RECORD_DS, this.ecfConverterRecordDS()).handler(this.ecfConverterRecordDSHandler()))
        .addDataSource(this.selectDS(ECF_CONVERTER_VALUE_DS, this.ecfConverterValueDSHandler()))
        .create();
    }

    public ColumnListExtPoint ecfConverterRecordDS() {
        return this.columnListExtPointBuilder(ECF_CONVERTER_RECORD_DS)
        .addColumn(textColumn("title", "title").create())
        .addColumn(textColumn("value", PROPERTY_ECF_VALUE+".title").create())
        .create();
    }

    protected List<IEntity> filterConverterRecordDS(final List<IEntity> entityList, final ExecutionContext context) {
        return entityList; // ФИЛЬТРЫ ПИШЕМ ТУТ
    }

    @SuppressWarnings("unchecked")
    public IReadAggregateHandler<DSInput, DSOutput> ecfConverterRecordDSHandler() {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(this.getName()) {
            @Override protected DSOutput execute(final DSInput input, final ExecutionContext context) {
                final IEcfConverterDao<IEntity> dao = EcfConverterConfig.this.getConverterDao();
                final IEcfConverter<IEntity> converter = dao.getConverter();

                final List<IEntity> entityList = EcfConverterConfig.this.filterConverterRecordDS(converter.getEntityList(), context);
                final DSOutput output = ListOutputBuilder.get(input, entityList).pageable(true).build();
                final List<DataWrapper> wrappers = DataWrapper.wrap(output);
                for (final DataWrapper w: wrappers) {
                    final EcfCatalogItem ecfCatalogItem = converter.getEcfCatalogItem(w.<IEntity>getWrapped(), false);
                    w.setProperty(PROPERTY_ECF_VALUE, ecfCatalogItem);
                }

                return output;
            }
        }.setPageable(true);
    }

    @SuppressWarnings("unchecked")
    public IReadAggregateHandler<DSInput, DSOutput> ecfConverterValueDSHandler() {
        return new ItemListDataHandler(this.getName()) {
            @Override protected void applyWhereConditions(final String alias, final DQLSelectBuilder dql, final ExecutionContext context) {
                super.applyWhereConditions(alias, dql, context);
                final IEcfConverterDao<IEntity> dao = EcfConverterConfig.this.getConverterDao();
                dql.where(eq(property(EcfCatalogItem.fisCatalogCode().fromAlias(alias)), value(dao.getEcfCatalogCode())));
                dql.order(property(EcfCatalogItem.fisItemCode().fromAlias(alias)));
            }
        }.order(EcfCatalogItem.fisCatalogCode()).pageable(true);
    }

}
