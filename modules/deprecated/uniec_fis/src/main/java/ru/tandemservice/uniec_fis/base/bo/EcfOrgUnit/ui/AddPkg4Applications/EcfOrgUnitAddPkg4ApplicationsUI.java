/* $Id: DppOrgUnitAddEditUI.java 22487 2012-04-04 13:16:00Z vzhukov $ */
package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.ui.AddPkg4Applications;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.process.*;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.hibsupport.transaction.sync.RuntimeTimeoutException;
import ru.tandemservice.uni.util.BackgroundProcessHolder;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaignPeriod;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.EcfOrgUnitManager;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.logic.EcfOrgUnitPackageDao;

import java.util.Date;
import java.util.List;

@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "holder.id")
})
public class EcfOrgUnitAddPkg4ApplicationsUI extends UIPresenter
{
    private final EntityHolder<EcfOrgUnit> holder = new EntityHolder<EcfOrgUnit>();

    public EntityHolder<EcfOrgUnit> getHolder() { return this.holder; }
    public EcfOrgUnit getEcfOrgUnit() { return this.getHolder().getValue(); }

    private List<EnrollmentCampaign> enrollmentCampaignList;
    public List<EnrollmentCampaign> getEnrollmentCampaignList() { return this.enrollmentCampaignList; }
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList) { this.enrollmentCampaignList = enrollmentCampaignList; }

    private EnrollmentCampaign enrollmentCampaign;
    public EnrollmentCampaign getEnrollmentCampaign() { return this.enrollmentCampaign; }
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign) { this.enrollmentCampaign = enrollmentCampaign; }

    private Date _regDateFrom;
    public Date getRegDateFrom() { return _regDateFrom; }
    public void setRegDateFrom(Date regDateFrom) { _regDateFrom = regDateFrom; }

    private Date _regDateTo;
    public Date getRegDateTo() { return _regDateTo; }
    public void setRegDateTo(Date regDateTo) { _regDateTo = regDateTo; }

    private List<EcfCatalogItem> _applicationsStatusList;
    private EcfCatalogItem _applicationsStatus;


    @Override
    public void onComponentRefresh() {
        final EcfOrgUnit ecfOrgUnit = this.getHolder().refresh();

        final List<EnrollmentCampaign> enrollmentCampaignList = EcfOrgUnitManager.instance().dao().getEnrollmentCampaignList(ecfOrgUnit);
        setEnrollmentCampaignList(enrollmentCampaignList);
        if (null == getEnrollmentCampaign() && !enrollmentCampaignList.isEmpty()) {
            //            setEnrollmentCampaign(
            //                CollectionUtils.find(enrollmentCampaignList, new Predicate<EnrollmentCampaign>() {
            //                    @Override public boolean evaluate(EnrollmentCampaign ec) {
            //                        return Boolean.TRUE.equals(ec.getEducationYear().getCurrent());
            //                    }
            //                })
            //            );
            setEnrollmentCampaign(enrollmentCampaignList.get(enrollmentCampaignList.size()-1));
        }

        setApplicationsStatusList(DataAccessServices.dao().getList(EcfCatalogItem.class, EcfCatalogItem.fisCatalogCode(), "4"));

        prepareReqRegDate();
    }

    public void prepareReqRegDate()
    {
        final List<EnrollmentCampaignPeriod> periodList = enrollmentCampaign.getPeriodList();
        final Date now = CoreDateUtils.getDayFirstTimeMoment(new Date());
        if (!periodList.isEmpty() && now.getTime() >= periodList.get(0).getDateFrom().getTime())
            _regDateFrom = now;
        if (!periodList.isEmpty() && now.getTime() <= periodList.get(0).getDateTo().getTime())
            _regDateTo = now;
    }

    public void onClickApply()
    {
        if (_regDateFrom != null && _regDateTo != null && _regDateFrom.getTime() > _regDateTo.getTime())
        {
            ContextLocal.getErrorCollector().add("Дата «Заявления с» не может быть больше чем дата «Заявления по».", "regDateFrom", "regDateTo");
            return;
        }

        final IBackgroundProcess process = new BackgroundProcessBase()
        {
            @Override
            public ProcessResult run(ProcessState state)
            {
                state.setCurrentValue(1);

                try
                {
                    EcfOrgUnitPackageDao.setupProcessState(state);
                    EcfOrgUnitManager.instance().pkgApplicationsDao()
                            .doCreatePackages(getEcfOrgUnit(),
                                              DataAccessServices.dao().get(EcfCatalogItem.class, getApplicationsStatus().getId()),
                                              DataAccessServices.dao().get(EnrollmentCampaign.class, getEnrollmentCampaign().getId()),
                                              _regDateFrom, _regDateTo);
                }
                catch (RuntimeTimeoutException e)
                {
                    throw new ApplicationException("Формирование пакетов уже запущено.");
                }
                catch (Throwable e)
                {
                    //TODO когда будет логирование ошибок, нужно выводить общие слова
                    if (e instanceof ApplicationException)
                        return new ProcessResult(e);
                    else
                        throw CoreExceptionUtils.getRuntimeException(e);
                }
                if (UserContext.getInstance().getErrorCollector().hasErrors())
                {
                    return new ProcessResult("Пакет «Заявления абитуриентов» не удалось сформировать.", true);
                }
                state.setCurrentValue(100);

                return new ProcessResult("Пакет «Заявления абитуриентов» успешно сформирован.");
            }
        };

        new BackgroundProcessHolder().start("Формирование пакета «Заявления абитуриентов»", process, ProcessDisplayMode.percent);
    }

    public EcfCatalogItem getApplicationsStatus()
    {
        return _applicationsStatus;
    }

    public void setApplicationsStatus(EcfCatalogItem applicationsStatus)
    {
        _applicationsStatus = applicationsStatus;
    }

    public List<EcfCatalogItem> getApplicationsStatusList()
    {
        return _applicationsStatusList;
    }

    public void setApplicationsStatusList(List<EcfCatalogItem> applicationsStatusList)
    {
        _applicationsStatusList = applicationsStatusList;
    }
}
