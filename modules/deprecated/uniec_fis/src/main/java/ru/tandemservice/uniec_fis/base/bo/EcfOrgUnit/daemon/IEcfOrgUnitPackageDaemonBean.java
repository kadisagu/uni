package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.daemon;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;

/**
 * @author vdanilov
 */
public interface IEcfOrgUnitPackageDaemonBean {

    public static final String GLOBAL_DAEMON_LOCK = "ecfOrgUnitPackageDao.lock";

    public static final SpringBeanCache<IEcfOrgUnitPackageDaemonBean> instance = new SpringBeanCache<IEcfOrgUnitPackageDaemonBean>(IEcfOrgUnitPackageDaemonBean.class.getName());


    public static interface IEcfOrgUnitEnrollmentCampaign {
        Long getEcfOrgUnitId();
        Long getEnrollmentCampaignId();
    }


    /**
     * @return uniq (p.ecfOrgUnit.id, p.enrollmentCampaign.id) | p ∊ ecfOrgUnitPackage
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    List<IEcfOrgUnitEnrollmentCampaign> getEcfOrgUnitEnrollmentCampaignList();

    /**
     * @param ecfOuEc
     * @return  p.id | p ∊ ecfOrgUnitPackage, p.ou=ecfOuEc.ou, p.ec=ecfOuEc.ec, p первый не отправленный пакет
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    Long getNextSendQueuePackageId(IEcfOrgUnitEnrollmentCampaign ecfOuEc);

    /**
     * Пробует отправить пакет в ФИС (на время сохранеия объектов открывает транзакции)
     * @param packageId
     * @return true, если операция выполнена успешно
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    public boolean tryToSendPackage(final Long packageId);

    /**
     * @param ecfOuEc
     * @return p.id | p ∊ ecfOrgUnitPackage, p.ou=ecfOuEc.ou, p.ec=ecfOuEc.ec, для p не получен ответ из ФИС
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    List<Long> getResultQueuePackageIds(IEcfOrgUnitEnrollmentCampaign ecfOuEc);

    /**
     * Пробует получить ответ из ФИС (на время сохранеия объектов открывает транзакции)
     * @param packageId
     * @return true, если операциявыполнена успешно
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    public boolean tryToGetResult(final Long packageId);


    /**
     * Выставляет признак архивности для пакетов, у которых есть ответ
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doArchivePackages();

}
