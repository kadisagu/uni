package ru.tandemservice.uniec_fis.base.bo.EcfConverter.ui.Config4EntranceDiscipline;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineKind;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline;
import ru.tandemservice.uniec.entity.settings.SetDiscipline;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.EcfConverterManager;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.IEcfConverterDao;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.ui.EcfConverterConfig;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author vdanilov
 */
@Configuration
public class EcfConverterConfig4EntranceDiscipline extends EcfConverterConfig
{

    public static final String ENROLLMENT_CAMPAIGN_DS = "enrollmentCampaignDS";

    @Override
    protected IEcfConverterDao getConverterDao()
    {
        return EcfConverterManager.instance().converterDao4EntranceDiscipline();
    }

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return super.presenterExtPoint(presenterExtPointBuilder().addDataSource(selectDS(ENROLLMENT_CAMPAIGN_DS, enrollmentCampaignDSHandler())));
    }

    protected static final Map<String, Object> briefInfoParams = new HashMap<String, Object>()
    {{
        put(PublisherActivator.PUBLISHER_ID_KEY, EnrollmentDirection.id());
    }};

    @Bean
    @Override
    public ColumnListExtPoint ecfConverterRecordDS()
    {
        return this.columnListExtPointBuilder(ECF_CONVERTER_RECORD_DS)
                .addColumn(textColumn("disciplineTitle", EntranceDiscipline.discipline().s()).formatter(source -> ((SetDiscipline) source).getFullTitle()).create())
                .addColumn(publisherColumn("directionTitle",EntranceDiscipline.enrollmentDirection().s())
                                   .formatter(source -> ((EnrollmentDirection) source).getTitle())
                                   .publisherLinkResolver(new IPublisherLinkResolver()
                                   {
                                       @Override
                                       public Object getParameters(IEntity iEntity)
                                       {
                                           EntranceDiscipline discipline = ((DataWrapper) iEntity).<EntranceDiscipline>getWrapped();
                                           return ParametersMap.createWith(PublisherActivator.PUBLISHER_ID_KEY, discipline.getEnrollmentDirection().getId());
                                       }

                                       @Override
                                       public String getComponentName(IEntity iEntity)
                                       {
                                           return ru.tandemservice.uniec.component.entrant.EnrollmentDirectionPub.Controller.class.getPackage().getName();
                                       }
                                   })
                                   .create())
                .addColumn(textColumn("studentCategory", EntranceDiscipline.studentCategory().s()).formatter(source -> ((StudentCategory) source).getTitle()).create())
                .addColumn(textColumn("disciplineKind", EntranceDiscipline.kind().s()).formatter(source -> ((EntranceDisciplineKind) source).getTitle()).create())
                .addColumn(blockColumn("value", "valueBlock").width("200px").create())
                .addColumn(blockColumn("action", "actionBlock").width("1px").hasBlockHeader(true).create())
                .create();
    }

    @Bean
    @Override
    public IReadAggregateHandler<DSInput, DSOutput> ecfConverterRecordDSHandler()
    {
        return super.ecfConverterRecordDSHandler();
    }

    @Override
    protected List<IEntity> filterConverterRecordDS(List<IEntity> entityList, ExecutionContext context)
    {
        final EnrollmentCampaign enrollmentCampaign = context.get(EcfConverterConfig4EntranceDisciplineUI.PARAM_ENROLLMENT_CAMPAIGN);
        final String disciplineFilter = context.get(EcfConverterConfig4EntranceDisciplineUI.PARAM_DISCIPLINE_FILTERE);
        final String directionFilter = context.get(EcfConverterConfig4EntranceDisciplineUI.PARAM_DIRECTION_FILTER);

        if (enrollmentCampaign == null)
            return new ArrayList<>();

        return entityList.stream()
                .filter(dis -> ((EntranceDiscipline) dis).getEnrollmentDirection().getEnrollmentCampaign().equals(enrollmentCampaign))
                .filter(dis -> StringUtils.trimToNull(disciplineFilter) == null
                        || StringUtils.containsIgnoreCase(((EntranceDiscipline) dis).getDiscipline().getFullTitle(), disciplineFilter))
                .filter(dis -> StringUtils.trimToNull(directionFilter) == null
                        || StringUtils.containsIgnoreCase(((EntranceDiscipline) dis).getEnrollmentDirection().getTitle(), (directionFilter)))
                .collect(Collectors.toList());
    }

    @Bean
    @Override
    public IReadAggregateHandler<DSInput, DSOutput> ecfConverterValueDSHandler()
    {
        return super.ecfConverterValueDSHandler();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> enrollmentCampaignDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrollmentCampaign.class)
                .order(EnrollmentCampaign.educationYear().intValue(), OrderDirection.desc)
                .filter(EnrollmentCampaign.title())
                .pageable(true);
    }
}
