package ru.tandemservice.uniec_fis.base.bo.EcfSettings.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodItem;
import ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodKey;

import java.util.Collection;
import java.util.List;

/**
 * @author vdanilov
 */
public interface IEcfSettingsDao extends INeedPersistenceSupport {

    /**
     * По направлениям приема ПК создает недостающие ключи
     * @param ec
     * @param directions
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    List<EcfSettingsECPeriodKey> doCreateEcfSettingsECPeriodKeys(EnrollmentCampaign ec, Collection<EnrollmentDirection> directions);


    /**
     * @param key
     * @param itemList
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doSaveEcfSettingsECPeriodKey(EcfSettingsECPeriodKey key, List<EcfSettingsECPeriodItem> itemList);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    ErrorCollector doValidateEcfSettingsECPeriodKey(EcfSettingsECPeriodKey key, List<EcfSettingsECPeriodItem> itemList);


}
