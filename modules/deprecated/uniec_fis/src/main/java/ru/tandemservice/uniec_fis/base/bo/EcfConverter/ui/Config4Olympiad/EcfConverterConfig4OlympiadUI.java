/**
 *$Id$
 */
package ru.tandemservice.uniec_fis.base.bo.EcfConverter.ui.Config4Olympiad;

import ru.tandemservice.uniec_fis.base.bo.EcfConverter.EcfConverterManager;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.ui.EcfConverterConfigUI;

/**
 * @author Alexander Shaburov
 * @since 10.01.13
 */
public class EcfConverterConfig4OlympiadUI extends EcfConverterConfigUI
{
    /**
     * @return true, если элеметнов в списке нет, иначе false
     */
    public boolean isRecordDSEmpty()
    {
        return getConfig().getDataSource(EcfConverterConfig4Olympiad.ECF_CONVERTER_RECORD_DS).getRecords().isEmpty();
    }

    public void onClickAutoSync()
    {
        EcfConverterManager.instance().olympiad().doAutoSync();
    }
}
