package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity;

import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.gen.EcfOrgUnitPackage4CampaignInfoGen;

/**
 * Пакет: Приемная кампания
 *
 * Root / PackageData / CampaignInfo / Campaigns / Campaign
 */
public class EcfOrgUnitPackage4CampaignInfo extends EcfOrgUnitPackage4CampaignInfoGen
{
    @Override
    public String getTitle() {
        if (getEnrollmentCampaign() == null) {
            return this.getClass().getSimpleName();
        }
        return "Пакет 1: Приемная кампания «" + getEnrollmentCampaign().getTitle() + "»: " + getStatus().getTitle();
    }
}