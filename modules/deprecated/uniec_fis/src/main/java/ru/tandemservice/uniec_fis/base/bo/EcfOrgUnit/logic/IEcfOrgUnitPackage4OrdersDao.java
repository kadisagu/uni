/**
 *$Id$
 */
package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage4AdmissionOrders;

/**
 * @author Alexander Shaburov
 * @since 15.01.13
 */
public interface IEcfOrgUnitPackage4OrdersDao extends IEcfOrgUnitPackageDao
{
    /**
     * Создает пакет (пакет 4: Списки заявлений абитуриентов, включенных в приказ)
     * @param ecfOrgUnit
     * @param enrollmentCampaign
     */
    @Transactional(propagation= Propagation.REQUIRED, readOnly=false)
    EcfOrgUnitPackage4AdmissionOrders doCreatePackage(EcfOrgUnit ecfOrgUnit, EnrollmentCampaign enrollmentCampaign);
}
