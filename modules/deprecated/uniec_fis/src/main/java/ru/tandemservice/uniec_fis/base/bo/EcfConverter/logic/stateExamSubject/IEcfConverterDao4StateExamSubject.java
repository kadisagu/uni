/**
 *$Id$
 */
package ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.stateExamSubject;

import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.IEcfConverterStaticDao;

/**
 * @author Alexander Shaburov
 * @since 11.01.13
 */
public interface IEcfConverterDao4StateExamSubject extends IEcfConverterStaticDao<StateExamSubject>
{
}
