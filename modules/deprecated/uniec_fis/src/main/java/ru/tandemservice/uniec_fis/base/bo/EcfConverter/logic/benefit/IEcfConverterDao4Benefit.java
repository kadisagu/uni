/* $Id$ */
package ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.benefit;

import org.tandemframework.shared.person.catalog.entity.Benefit;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.IEcfConverterDao;

/**
 * @author Andrey Andreev
 * @since 02.08.2016
 */
public interface IEcfConverterDao4Benefit extends IEcfConverterDao<Benefit>
{
//    @Transactional(propagation= Propagation.REQUIRED, readOnly=false)
//    void doAutoSync();
}
