package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.ui.PackageIOPub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author vdanilov
 */
@Configuration
public class EcfOrgUnitPackageIOPub extends BusinessComponentManager {

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
        .create();
    }

}
