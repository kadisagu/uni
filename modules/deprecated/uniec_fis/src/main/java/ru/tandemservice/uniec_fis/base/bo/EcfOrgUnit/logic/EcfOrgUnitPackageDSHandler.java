package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackageIOLog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class EcfOrgUnitPackageDSHandler extends AbstractReadAggregateHandler<DSInput, DSOutput> {

    public EcfOrgUnitPackageDSHandler(final String ownerId) {
        super(ownerId);
    }


    @Override
    @SuppressWarnings("unchecked")
    protected DSOutput execute(final DSInput input, final ExecutionContext context) {

        final DQLSelectBuilder dql = new DQLSelectBuilder()
        .fromEntity(EcfOrgUnitPackage.class, "e").column(property("e"))
        .order(property(EcfOrgUnitPackage.creationDate().fromAlias("e")), OrderDirection.desc)
        .where(eq(property(EcfOrgUnitPackage.enrollmentCampaign().fromAlias("e")), commonValue(context.get("enrollmentCampaign"))))
        .where(eq(property(EcfOrgUnitPackage.ecfOrgUnit().fromAlias("e")), commonValue(context.get("ecfOrgUnit"))));
        final DSOutput output = DQLSelectOutputBuilder.get(input, dql, context.getSession()).pageable(true).build();

        final List<DataWrapper> wrappers = DataWrapper.wrap(output);

        List<EcfOrgUnitPackageIOLog> rows = new DQLSelectBuilder()
        .fromEntity(EcfOrgUnitPackageIOLog.class, "x").column("x")
        .order(property(EcfOrgUnitPackageIOLog.operationDate().fromAlias("x")), OrderDirection.desc)
        .order(property(EcfOrgUnitPackageIOLog.id().fromAlias("x")), OrderDirection.desc)
        .where(in(property(EcfOrgUnitPackageIOLog.owner().id().fromAlias("x")), CommonDAO.ids(wrappers)))
        .createStatement(context.getSession()).list();

        Map<Long, DataWrapper> map = CommonDAO.map(wrappers);
        Map<Long, EcfOrgUnitPackageIOLog> lastPackageIOLogMap = new HashMap<>();
        for (EcfOrgUnitPackageIOLog row: rows) {
            DataWrapper wrapper = map.get(row.getOwner().getId());
            // log
            List<String> log = (List<String>)wrapper.getProperty("log");
            if (null == log) { wrapper.setProperty("log", log = new ArrayList<>()); }
            log.add(row.getDescription());

            // lastPackageIOLogMap
            final EcfOrgUnitPackageIOLog pkgIOLog = lastPackageIOLogMap.get(row.getOwner().getId());
            if (pkgIOLog != null)
            {
                if (row.getOperationDate().getTime() > pkgIOLog.getOperationDate().getTime())
                    lastPackageIOLogMap.put(row.getOwner().getId(), row);
            }
            else
            {
                lastPackageIOLogMap.put(row.getOwner().getId(), row);
            }
        }

        for (DataWrapper wrapper: wrappers) {
            List<String> log = (List<String>)wrapper.getProperty("log");
            if (null == log) { continue; }

            wrapper.setProperty("logSize", log.size());

            // log
            if (log.size() > 4) {
                wrapper.setProperty("vPropLog", StringUtils.join(log.subList(0, 4), "\n")+"\n... еще записей " + (log.size()-4));
            } else {
                wrapper.setProperty("vPropLog", StringUtils.join(log, "\n"));
            }

            // stateTitle
            wrapper.setProperty("vPropStateTitle", getPkgStatus(lastPackageIOLogMap.get(wrapper.getId())));
        }

        return output;
    }

    /**
     * "Данные переданы в ФИС" – если последняя операции и ее состояние import.complete, import.fis-error, result.fis-error;
     * "Получен результат обработки данных в ФИС" – если последняя операции и ее состояние result.complete;
     * <пусто> – в иных случаях;
     */
    protected String getPkgStatus(EcfOrgUnitPackageIOLog pkgIOLog)
    {
        if (pkgIOLog == null)
            return "";

        final String status = pkgIOLog.getFullStatus();
        if (status.equalsIgnoreCase("result." + EcfOrgUnitPackageIOLog.IO_STATUS_COMPLETE))
            return "Получен результат обработки данных в ФИС";
        else if (status.equalsIgnoreCase("import." + EcfOrgUnitPackageIOLog.IO_STATUS_COMPLETE) || status.equalsIgnoreCase("import." + EcfOrgUnitPackageIOLog.IO_STATUS_FIS_ERROR) || status.equalsIgnoreCase("result." + EcfOrgUnitPackageIOLog.IO_STATUS_FIS_ERROR))
            return  "Данные переданы в ФИС";
        else
            return "";
    }
}
