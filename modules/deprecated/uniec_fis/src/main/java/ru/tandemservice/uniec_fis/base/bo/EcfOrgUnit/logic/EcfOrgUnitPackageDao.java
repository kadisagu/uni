package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.logic;

import fis.schema.pkg.send.req.PackageData;
import fis.schema.pkg.send.req.Root;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.process.ProcessState;
import org.tandemframework.core.tool.IdGen;
import org.tandemframework.core.view.formatter.StringLimitFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.DaoCache;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.codes.StudentCategoryCodes;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.NotUsedTargetAdmissionKind;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.ecg.IEcfEcgDao.IEcfEcgKey;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.EcfOrgUnitManager;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage;

import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public abstract class EcfOrgUnitPackageDao extends UniBaseDao implements IEcfOrgUnitPackageDao
{

    private static final ThreadLocal<Reference<ProcessState>> processStates = new ThreadLocal<>();

    public static void setupProcessState(ProcessState state)
    {
        processStates.set(new WeakReference<>(state));
    }

    /**
     * Вычисляет и выводит процент выполненной работы процесса.
     * Используется в циклах.
     *
     * @param percent  процентная часть от всей работы процесса у данного цикла
     * @param item     элемент цикла
     * @param itemList список по которому бежит цикл
     */
    protected <T> void setProcessState(int percent, T item, List<T> itemList)
    {
        setProcessState(percent, itemList.indexOf(item), itemList.size());
    }


    /**
     * Вычисляет и выводит процент выполненной работы процесса.
     * Используется в циклах.
     *
     * @param percent процентная часть от всей работы процесса у данного цикла
     * @param index   текущий индекс
     * @param length  количество индексов
     */
    protected void setProcessState(int percent, int index, int length)
    {
        Reference<ProcessState> ref = processStates.get();
        if (null == ref)
        {
            return;
        }

        ProcessState state = ref.get();
        if (null == state)
        {
            return;
        }

        double q = (index + 1) / (double) length;
        Double currentValue = ((double) (percent - state.getCurrentValue())) * (q * q);
        state.setCurrentValue(currentValue.intValue() + state.getCurrentValue());
    }

    protected void setProcessState(int value)
    {
        Reference<ProcessState> ref = processStates.get();
        if (null == ref)
        {
            return;
        }

        ProcessState state = ref.get();
        if (null == state)
        {
            return;
        }

        state.setCurrentValue(value);
    }

    protected static final DatatypeFactory dataTypeFactory;

    static
    {
        try
        {
            dataTypeFactory = DatatypeFactory.newInstance();
        }
        catch (final Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    /**
     * http://tracker.tandemservice.ru/browse/DEV-478?focusedCommentId=163062&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-163062
     * http://tracker.tandemservice.ru/browse/DEV-478?focusedCommentId=163095&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-163095
     *
     * @param date
     * @return
     */
    public static XMLGregorianCalendar xml(final Date date)
    {
        if (date == null)
        {
            return null;
        }
        else
        {
            final GregorianCalendar gc = new GregorianCalendar();
            gc.setTimeInMillis(date.getTime());
            return dataTypeFactory.newXMLGregorianCalendar(gc);
        }
    }

    /**
     * @return xs:date, example: "2002-09-24"
     */
    public static XMLGregorianCalendar xmlDate(final Date date)
    {
        if (date == null)
        {
            return null;
        }
        else
        {
            final GregorianCalendar gc = new GregorianCalendar();
            gc.setTime(date);
            return dataTypeFactory.newXMLGregorianCalendarDate(gc.get(Calendar.YEAR), gc.get(Calendar.MONTH) + 1, gc.get(Calendar.DATE), DatatypeConstants.FIELD_UNDEFINED);
        }
    }

    /**
     * Формирует uid для Сертификат ЕГЭ Уни (в рамках подразделения КГ ФИС)
     *
     * @return
     */
    public static String uid(final Long ecgUid, final EntrantStateExamCertificate certificate, final String postfix)
    {
        return ecgUid.toString() + ".ec" + getStringId(certificate) + "." + postfix;
    }

    public static String uid(final EcfOrgUnit ecfOrgUnit, final EnrollmentCampaign enrollmentCampaign, final String postfix)
    {
        final String prefix = ecfOrgUnit.getUid() + "." + getStringId(enrollmentCampaign);
        if (null == postfix)
        {
            return EcfOrgUnitManager.instance().uidRegistry().doGetUid(prefix);
        }
        else
        {
            return EcfOrgUnitManager.instance().uidRegistry().doGetUid(prefix + "." + postfix);
        }
    }


    /**
     * Формирует uuid для вида ЦП ФИС (в рамках подразделения КГ ФИС)
     *
     * @param ecgUid
     * @return
     */
    public static String uid4TargetAdmissionKind(final String ecgUid, final TargetAdmissionKind targetAdmissionKind)
    {
        if (null == targetAdmissionKind)
        {
            return null;
        }
        return (ecgUid + ".ta-" + targetAdmissionKind.getCode());
    }

    /**
     * Формирует uuid для вида ЦП ФИС
     *
     * @param enrollmentCampaign  ПК
     * @param targetAdmissionKind Вид целевого приема
     * @return ид вида ЦП в рамках ПК
     */
    public static String uid4TargetAdmissionKind(final EnrollmentCampaign enrollmentCampaign, final TargetAdmissionKind targetAdmissionKind)
    {
        if (null == targetAdmissionKind)
        {
            return null;
        }
        if (null == enrollmentCampaign)
        {
            return null;
        }

        final String idStr = getStringId(enrollmentCampaign) + "." + getStringId(targetAdmissionKind);
        return EcfOrgUnitManager.instance().uidRegistry().doGetUid(idStr);
    }

    /**
     * @param ecgUid
     * @param admissionVolumeItemUid
     * @return
     */
    protected String uid4EcgAdmissionVolumeItem(final String ecgUid, final String admissionVolumeItemUid)
    {
        return (ecgUid + ".it-" + admissionVolumeItemUid);
    }


    /**
     * Формирует uuid для заявления ФИС (в рамках подразделения ФИС)
     *
     * @param ecfOrgUnit         подразделение ФИС
     * @param enrollmentCompaign ПК
     * @param request            заявление UNI
     * @return uid-для-ЗАЯВЛЕНИЕ-ФИС
     */
    public static String uid(final EcfOrgUnit ecfOrgUnit, final EnrollmentCampaign enrollmentCompaign, final EntrantRequest request, final IEcfEcgKey ecgKey)
    {
        return uid(ecfOrgUnit, enrollmentCompaign, request.getEntrant()) + "." + uid(ecfOrgUnit, enrollmentCompaign, getStringId(request)) + "." + uid(ecfOrgUnit, enrollmentCompaign, ecgKey);
    }

    /**
     * Формирует uuid для заявления ФИС (в рамках подразделения ФИС)
     *
     * @param ecfOrgUnit         подразделение ФИС
     * @param enrollmentCompaign ПК
     * @param entrant            абитуриент ФИС
     * @return uid-для-АБИТУРИЕНТ-ФИС
     */
    public static String uid(final EcfOrgUnit ecfOrgUnit, final EnrollmentCampaign enrollmentCompaign, final Entrant entrant)
    {
        return uid(ecfOrgUnit, enrollmentCompaign, getStringId(entrant.getPerson()));
    }

    /**
     * Формирует uuid для КГ ФИС (в рамках подразделения ФИС)
     *
     * @param ecfOrgUnit         подразделение ФИС
     * @param enrollmentCompaign ПК
     * @param key                КГ ФИС
     * @return uid-для-КГ-ФИС
     */
    public static String uid(final EcfOrgUnit ecfOrgUnit, final EnrollmentCampaign enrollmentCompaign, final IEcfEcgKey key)
    {
        return uid(ecfOrgUnit, enrollmentCompaign, key.getKey());
    }

    /**
     * Формирует uuid для ПК (в рамках подразделения ФИС)
     *
     * @param ecfOrgUnit         подразделение ФИС
     * @param enrollmentCampaign ПК
     * @return uid-для-ПК
     */
    public static String uid(final EcfOrgUnit ecfOrgUnit, final EnrollmentCampaign enrollmentCampaign)
    {
        return uid(ecfOrgUnit, enrollmentCampaign, (String) null);
    }

    /**
     * Формирует admissionVolumeItemUid для направления ПК (в рамках подразделения ФИС)
     *
     * @param ecfOrgUnit         подразделение ФИС
     * @param enrollmentCampaign ПК
     * @param fisEduLvlId        уровень обучения ФИС
     * @param fisDirectionId     направление обеучения ФИС
     * @param course             курс приема
     * @return admissionVolumeItemUid (uid-для-направления-фис)
     */
    public static String uid(final EcfOrgUnit ecfOrgUnit, final EnrollmentCampaign enrollmentCampaign, final Long fisEduLvlId, final Long fisDirectionId, final Integer course)
    {
        return uid(ecfOrgUnit, enrollmentCampaign, fisEduLvlId + "." + fisDirectionId + "." + course);
    }

    protected Root newRoot(final EcfOrgUnit ecfOrgUnit)
    {
        final Root root = new fis.schema.pkg.send.req.Root();

        // auth data
        root.setAuthData(new Root.AuthData());
        root.getAuthData().setLogin(ecfOrgUnit.getLogin());
        root.getAuthData().setPass(ecfOrgUnit.getPassword());

        // package data
        root.setPackageData(new PackageData());
        return root;
    }

    @Override
    public void doSwitchArchive(final Long packageId, final boolean archiveFlag)
    {
        final EcfOrgUnitPackage ecfOrgUnitPackage = this.refresh(EcfOrgUnitPackage.class, packageId);
        if (archiveFlag == ecfOrgUnitPackage.isArchived())
        {
            return;
        }

        ecfOrgUnitPackage.setArchiveDate(archiveFlag ? new Date() : null);
        this.saveOrUpdate(ecfOrgUnitPackage);
    }

    @Override
    public void doDeletePackage(final Long packageId)
    {
        final EcfOrgUnitPackage ecfOrgUnitPackage = this.refresh(EcfOrgUnitPackage.class, packageId);
        if (ecfOrgUnitPackage.isAlive())
        {
            throw new ApplicationException("Нельзя удалить пакет «" + ecfOrgUnitPackage.getTitle() + "»: пакет доступен для обработки.");
        }
        if (null != ecfOrgUnitPackage.getPkgResultSuccess())
        {
            throw new ApplicationException("Нельзя удалить пакет «" + ecfOrgUnitPackage.getTitle() + "»: пакет был обработан ФИС.");
        }
        if (null != ecfOrgUnitPackage.getPkgImportSuccess())
        {
            throw new ApplicationException("Нельзя удалить пакет «" + ecfOrgUnitPackage.getTitle() + "»: пакет был отправлен в ФИС.");
        }
        this.delete(ecfOrgUnitPackage);
    }

    @Override
    public boolean isFirstUnqueuedPackage(Long packageId)
    {
        final EcfOrgUnitPackage ecfOrgUnitPackage = this.refresh(EcfOrgUnitPackage.class, packageId);
        if (ecfOrgUnitPackage.isQueued())
        {
            return false;
        }

        final List<Long> ids = getUnqueuedPackagesBefore(ecfOrgUnitPackage);
        return ids.isEmpty();
    }

    // список находящихся в очереди пакетов, созданный после теуйщего
    public List<Long> getQueuedPackagesAfter(EcfOrgUnitPackage ecfOrgUnitPackage)
    {
        return new DQLSelectBuilder()
                .fromEntity(EcfOrgUnitPackage.class, "x").column(property("x.id"))
                .where(eq(property(EcfOrgUnitPackage.ecfOrgUnit().fromAlias("x")), value(ecfOrgUnitPackage.getEcfOrgUnit()))) // на том же подразделении
                .where(eq(property(EcfOrgUnitPackage.enrollmentCampaign().fromAlias("x")), value(ecfOrgUnitPackage.getEnrollmentCampaign()))) // из той же ПК
                .where(ne(property(EcfOrgUnitPackage.id().fromAlias("x")), value(ecfOrgUnitPackage.getId()))) // текущий пакет туда попадать не должен
                .where(isNotNull(property(EcfOrgUnitPackage.queueDate().fromAlias("x")))) // уже в очереди (про архивацию не важно)
                .where(or(
                        gt(property(EcfOrgUnitPackage.creationDate().fromAlias("x")), valueTimestamp(ecfOrgUnitPackage.getCreationDate())),
                        gt(property(EcfOrgUnitPackage.id().fromAlias("x")), value(ecfOrgUnitPackage.getId()))
                )) // создан позже (по дате или по id)
                .createStatement(getSession()).list();
    }

    // список не находящихся в очереди пакетов, созданных до текущего
    public List<Long> getUnqueuedPackagesBefore(EcfOrgUnitPackage ecfOrgUnitPackage)
    {
        return new DQLSelectBuilder()
                .fromEntity(EcfOrgUnitPackage.class, "x").column(property("x.id"))
                .where(eq(property(EcfOrgUnitPackage.ecfOrgUnit().fromAlias("x")), value(ecfOrgUnitPackage.getEcfOrgUnit()))) // на том же подразделении
                .where(eq(property(EcfOrgUnitPackage.enrollmentCampaign().fromAlias("x")), value(ecfOrgUnitPackage.getEnrollmentCampaign()))) // из той же ПК
                .where(ne(property(EcfOrgUnitPackage.id().fromAlias("x")), value(ecfOrgUnitPackage.getId()))) // текущий пакет туда попадать не должен
                .where(isNull(property(EcfOrgUnitPackage.queueDate().fromAlias("x")))) // еще не в очереди
                .where(isNull(property(EcfOrgUnitPackage.archiveDate().fromAlias("x")))) // не исключенные из обработки
                .where(not(or(
                        gt(property(EcfOrgUnitPackage.creationDate().fromAlias("x")), valueTimestamp(ecfOrgUnitPackage.getCreationDate())),
                        gt(property(EcfOrgUnitPackage.id().fromAlias("x")), value(ecfOrgUnitPackage.getId()))
                ))) // создан раньше (отрицание из условия выше)
                .createStatement(getSession()).list();
    }

    /**
     * @param ecfOrgUnit Подразделение в ФИС
     * @param campaign   приемная компания
     * @return мапа UID конкурсных групп для приемной компании без постфиксов фин. источника и Крыма
     */
    public Map<EnrollmentDirection, Map<StudentCategory, String>> getCompetitionGroupUidMap(EcfOrgUnit ecfOrgUnit, EnrollmentCampaign campaign)
    {
        Map<EnrollmentDirection, Map<StudentCategory, List<EntranceDiscipline>>> disciplinesMap = EntrantDataUtil.getDisciplinesByStudentCategoryByDirection(getSession(), campaign);

        Map<EnrollmentDirection, Map<StudentCategory, String>> result = new HashMap<>();

        disciplinesMap.entrySet().forEach(
                dirEntry -> {
                    EnrollmentDirection direction = dirEntry.getKey();
                    Map<StudentCategory, String> uidByStudentCategory = new HashMap<>();
                    result.put(direction, uidByStudentCategory);
                    dirEntry.getValue().entrySet().forEach(
                            scEntry -> {
                                StudentCategory studentCategory = scEntry.getKey();
                                List<EntranceDiscipline> disciplines = scEntry.getValue();
                                uidByStudentCategory.put(studentCategory, getCompetitionGroupUid(ecfOrgUnit, direction, studentCategory.getCode(), disciplines));
                            }
                    );
                }
        );
        return result;
    }

    protected String getCompetitionGroupUid(
            EcfOrgUnit ecfOrgUnit,
            Map<EnrollmentDirection, Map<StudentCategory, String>> competitionGroupUidMap,
            RequestedEnrollmentDirection requestedDirection)
    {
        Map<StudentCategory, String> uidByStudCategory = competitionGroupUidMap.get(requestedDirection.getEnrollmentDirection());
        String uid = null;
        if (uidByStudCategory != null)
            uid = uidByStudCategory.get(requestedDirection.getStudentCategory());
        if (uid == null)
            uid = getCompetitionGroupUid(ecfOrgUnit, requestedDirection.getEnrollmentDirection(), requestedDirection.getStudentCategory().getCode(), null);
        return uid;
    }

    /**
     * @param ecfOrgUnit   Подразделение в ФИС
     * @param direction    Направление приема
     * @param categoryCode категория студента
     * @param disciplines  Вступительные испытания
     * @return UID конкурсной группы без постфиксов фин. источника и Крыма
     */
    public static String getCompetitionGroupUid(EcfOrgUnit ecfOrgUnit, EnrollmentDirection direction, String categoryCode, List<EntranceDiscipline> disciplines)
    {
        StringBuilder postfixBuilder = new StringBuilder(getCompetitionGroupUidPostfix(direction, categoryCode));
        if (!CollectionUtils.isEmpty(disciplines))
            disciplines.forEach(dis -> postfixBuilder.append("=").append(getDisciplineUidPostfix(dis)));

        return uid(ecfOrgUnit, direction.getEnrollmentCampaign(), postfixBuilder.toString());
    }

    public static String getCrimeaPostfix(boolean fromCrimea)
    {
        return fromCrimea ? "-Krym" : "";
    }

    public static String getCompetitionGroupUidPostfix(EnrollmentDirection direction, String categoryCode)
    {
        StringBuilder builder = new StringBuilder();
        if (!direction.getEnrollmentCampaign().isExamSetDiff() ||
                StudentCategoryCodes.STUDENT_CATEGORY_STUDENT.equals(categoryCode) ||
                StudentCategoryCodes.STUDENT_CATEGORY_LISTENER.equals(categoryCode))
        {
            if (direction.getEnrollmentCampaign().isUseCompetitionGroup())
                builder.append(getStringId(direction.getCompetitionGroup()));
            else
                builder.append(getStringId(direction));
        }
        else
            builder.append(getStringId(direction));

        String developTechCode = direction.getEducationOrgUnit().getDevelopTech().getCode();
        String developConditionCode = direction.getEducationOrgUnit().getDevelopCondition().getCode();

        builder
                .append(".").append(categoryCode)
                .append(".").append(developTechCode)
                .append(".").append(developConditionCode);

        return builder.toString();
    }

    protected static String getDisciplineUidPostfix(EntranceDiscipline discipline)
    {
        String uid = Long.toString(discipline.getId() >> IdGen.CODE_BITS, 32);
        return uid + ";"
                + ((discipline.isBudget() ? "1" : "0")
                + (discipline.isContract() ? "1" : "0")) + ";"
                + discipline.getKind().getCode();
    }

    public static String getStringId(IEntity entity)
    {
        return Long.toString(entity.getId(), 32);
    }

    @Override
    public void doQueue(final Long packageId)
    {
        final EcfOrgUnitPackage ecfOrgUnitPackage = this.refresh(EcfOrgUnitPackage.class, packageId);
        if (ecfOrgUnitPackage.isQueued())
        {
            return;
        }

        final Session session = this.lock(ecfOrgUnitPackage.getEcfOrgUnit());

        // проверяем, что нет пакетов в очереди, созданных после текущего
        {
            final List<Long> ids = getQueuedPackagesAfter(ecfOrgUnitPackage);
            if (ids.size() > 0)
            {
                throw new ApplicationException("Нельзя отправить в очередь пакет «" + ecfOrgUnitPackage.getTitle() + "»: в очереди уже есть пакеты, созданные после этого.");
            }
        }

        // все пакеты, созанные до текущего, и не находящиеся в очереди - архивируем
        {
            final List<Long> ids = getUnqueuedPackagesBefore(ecfOrgUnitPackage);
            for (final Long id : ids)
            {
                this.doSwitchArchive(id, true);
            }
        }

        ecfOrgUnitPackage.setQueueDate(new Date());
        session.update(ecfOrgUnitPackage);
        session.flush();
    }

    /**
     * @return TargetAdmissionKind, если в ПК используется один Вид ЦП, иначе null
     */
    protected TargetAdmissionKind getUsedSimpleTargetAdmissionKind(EnrollmentCampaign enrollmentCampaign)
    {
        // инициализируем кэш (при первом вызове метода)
        final String cacheKey = "ecfOrgUnitPackageDao.simpleTargetAdmissionKind.keyCache";
        Map<Long, TargetAdmissionKind> cache = DaoCache.get(cacheKey);
        if (null == cache)
        {
            DaoCache.put(cacheKey, cache = new HashMap<>());
        }

        if (!cache.containsKey(enrollmentCampaign.getId()))
        {
            DQLSelectBuilder notUsed = new DQLSelectBuilder().fromEntity(NotUsedTargetAdmissionKind.class, "nu")
                    .where(eq(property(NotUsedTargetAdmissionKind.enrollmentCampaign().fromAlias("nu")), value(enrollmentCampaign)))
                    .where(eq(property(NotUsedTargetAdmissionKind.targetAdmissionKind().fromAlias("nu")), property("ta")));

            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(TargetAdmissionKind.class, "ta")
                    .where(notExists(notUsed.buildQuery()));

            final List<TargetAdmissionKind> kindList = builder.createStatement(getSession()).list();
            cache.put(enrollmentCampaign.getId(), kindList.size() == 1 ? kindList.get(0) : null);
        }

        return cache.get(enrollmentCampaign.getId());
    }

    /**
     * @param entity
     * @param title
     * @param comment
     * @return UID для сущности
     */
    protected static String getSimpleUID(IEntity entity, String title, String comment)
    {
        return _uid(getStringId(entity), entity.getId(), trimTitle(title), comment);
    }

    private static String trimTitle(String title)
    {
        return StringLimitFormatter.INSTANCE.format(title);
    }

    private static String _uid(final String data, Long relatedEntityId, String relatedEntityTitle, String comment)
    {
        return EcfOrgUnitManager.instance().uidRegistry().doGetUid(data, relatedEntityId, relatedEntityTitle, comment);
    }
}
