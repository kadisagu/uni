/* $Id: CommonCatalogList.java 6 2012-04-04 12:30:37Z vzhukov $ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec_fis.base.bo.EcfCatalog.ui.ItemList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.EcfCatalogManager;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.EcfOrgUnitManager;

public class EcfCatalogItemListUI extends UIPresenter
{
    public static final String PARAM_CATALOG_CODE = "catalogCode";
    public static final String PARAM_CATALOG_ITEM_TITLE = "itemTitle";

    public void onClickSync() {
        EcfCatalogManager.instance().dao().doSyncCatalogs(
            EcfOrgUnitManager.instance().dao().getEcfOrgUnit(
                TopOrgUnit.getInstance()
            )
        );
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(PARAM_CATALOG_CODE, getSettings().get(PARAM_CATALOG_CODE));
        dataSource.put(PARAM_CATALOG_ITEM_TITLE, getSettings().get(PARAM_CATALOG_ITEM_TITLE));
    }
}
