package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage4AdmissionOrders;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Пакет: Списки заявлений абитуриентов, включенных в приказ
 *
 * Root / PackageData / OrdersOfAdmission
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EcfOrgUnitPackage4AdmissionOrdersGen extends EcfOrgUnitPackage
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage4AdmissionOrders";
    public static final String ENTITY_NAME = "ecfOrgUnitPackage4AdmissionOrders";
    public static final int VERSION_HASH = -4903696;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EcfOrgUnitPackage4AdmissionOrdersGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EcfOrgUnitPackage4AdmissionOrdersGen> extends EcfOrgUnitPackage.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EcfOrgUnitPackage4AdmissionOrders.class;
        }

        public T newInstance()
        {
            return (T) new EcfOrgUnitPackage4AdmissionOrders();
        }
    }
    private static final Path<EcfOrgUnitPackage4AdmissionOrders> _dslPath = new Path<EcfOrgUnitPackage4AdmissionOrders>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EcfOrgUnitPackage4AdmissionOrders");
    }
            

    public static class Path<E extends EcfOrgUnitPackage4AdmissionOrders> extends EcfOrgUnitPackage.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return EcfOrgUnitPackage4AdmissionOrders.class;
        }

        public String getEntityName()
        {
            return "ecfOrgUnitPackage4AdmissionOrders";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
