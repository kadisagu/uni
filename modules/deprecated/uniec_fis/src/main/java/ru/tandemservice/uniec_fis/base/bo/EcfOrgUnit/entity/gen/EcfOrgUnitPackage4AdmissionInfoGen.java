package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage4AdmissionInfo;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Пакет: Сведения об объеме и структуре приема
 *
 * Root / PackageData / AdmissionInfo
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EcfOrgUnitPackage4AdmissionInfoGen extends EcfOrgUnitPackage
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage4AdmissionInfo";
    public static final String ENTITY_NAME = "ecfOrgUnitPackage4AdmissionInfo";
    public static final int VERSION_HASH = -1662274348;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EcfOrgUnitPackage4AdmissionInfoGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EcfOrgUnitPackage4AdmissionInfoGen> extends EcfOrgUnitPackage.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EcfOrgUnitPackage4AdmissionInfo.class;
        }

        public T newInstance()
        {
            return (T) new EcfOrgUnitPackage4AdmissionInfo();
        }
    }
    private static final Path<EcfOrgUnitPackage4AdmissionInfo> _dslPath = new Path<EcfOrgUnitPackage4AdmissionInfo>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EcfOrgUnitPackage4AdmissionInfo");
    }
            

    public static class Path<E extends EcfOrgUnitPackage4AdmissionInfo> extends EcfOrgUnitPackage.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return EcfOrgUnitPackage4AdmissionInfo.class;
        }

        public String getEntityName()
        {
            return "ecfOrgUnitPackage4AdmissionInfo";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
