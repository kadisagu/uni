package ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity;

import org.tandemframework.shared.person.catalog.entity.Benefit;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.gen.*;

/** @see ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.gen.EcfConv4BenefitGen */
public class EcfConv4Benefit extends EcfConv4BenefitGen
{

    public EcfConv4Benefit()
    {
    }

    public EcfConv4Benefit(Benefit benefit, EcfCatalogItem value)
    {
        setBenefit(benefit);
        setValue(value);
    }
}