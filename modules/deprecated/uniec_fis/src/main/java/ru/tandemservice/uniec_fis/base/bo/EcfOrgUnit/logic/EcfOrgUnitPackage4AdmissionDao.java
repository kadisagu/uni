package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.logic;

import fis.schema.pkg.send.req.PackageData;
import fis.schema.pkg.send.req.PackageData.AdmissionInfo;
import fis.schema.pkg.send.req.PackageData.AdmissionInfo.*;
import fis.schema.pkg.send.req.PackageData.AdmissionInfo.CompetitiveGroups.CompetitiveGroup;
import fis.schema.pkg.send.req.Root;
import fis.schema.pkg.send.req.TEntranceTestSubject;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.codes.DevelopConditionCodes;
import ru.tandemservice.uni.entity.catalog.codes.DevelopTechCodes;
import ru.tandemservice.uni.entity.catalog.codes.StudentCategoryCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.IndividualProgress;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.entrant.EnrCampaignEntrantIndividualProgress;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline;
import ru.tandemservice.uniec.entity.entrant.gen.EntranceDisciplineGen;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.entity.settings.NotUsedTargetAdmissionKind;
import ru.tandemservice.uniec.entity.settings.TargetAdmissionPlanRelation;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.gen.EcfCatalogItemGen;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.EcfConverterManager;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.IEcfConverter;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.ecg.IEcfEcgDao;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.eduForm.IEcfConverterDao4EduForm;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.eduOu.IEcfConverterDao4EduOu;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.finSources.EcfFinSourceDao;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.finSources.IEcfFinSourceDao;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.EcfOrgUnitManager;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage4AdmissionInfo;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage4CampaignInfo;
import ru.tandemservice.uniec_fis.util.FisServiceImpl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class EcfOrgUnitPackage4AdmissionDao extends EcfOrgUnitPackageDao implements IEcfOrgUnitPackage4AdmissionDao
{

    @Override
    public EcfOrgUnitPackage4AdmissionInfo doCreatePackage(final EcfOrgUnit ecfOrgUnit, final EnrollmentCampaign enrollmentCampaign)
    {
        final EcfOrgUnitPackage4AdmissionInfo dbo = new EcfOrgUnitPackage4AdmissionInfo();
        dbo.setCreationDate(new Date());
        dbo.setEcfOrgUnit(ecfOrgUnit);
        dbo.setEnrollmentCampaign(enrollmentCampaign);
        dbo.setXmlPackageRequest(this.generateXmlPackageRequest(dbo));
        this.saveOrUpdate(dbo);
        return dbo;
    }


    protected byte[] generateXmlPackageRequest(final EcfOrgUnitPackage4AdmissionInfo dbo)
    {
        final EnrollmentCampaign enrollmentCampaign = dbo.getEnrollmentCampaign();
        final EcfOrgUnit ecfOrgUnit = dbo.getEcfOrgUnit();

        // первый пакет должен быть выгружен
        {
            final List<EcfOrgUnitPackage4CampaignInfo> packages1 = new DQLSelectBuilder()
                    .fromEntity(EcfOrgUnitPackage4CampaignInfo.class, "x").column(property("x"))
                    .where(eq(property(EcfOrgUnitPackage4CampaignInfo.enrollmentCampaign().fromAlias("x")), value(enrollmentCampaign)))
                    .where(eq(property(EcfOrgUnitPackage4CampaignInfo.ecfOrgUnit().fromAlias("x")), value(ecfOrgUnit)))
                    .createStatement(this.getSession()).list();
            if (packages1.isEmpty())
            {
                throw new ApplicationException(EcfOrgUnitManager.instance().getProperty("error.admission.no-compaing-info-pkg", enrollmentCampaign.getTitle()));
            }
        }

        // obtain new root
        final Root root = this.newRoot(ecfOrgUnit);
        PackageData packageData = root.getPackageData();

        // AdmissionInfo
        packageData.setAdmissionInfo(getAdmissionInfo(ecfOrgUnit, enrollmentCampaign));
        setProcessState(70);

        // InstitutionAchievements
        List<PackageData.InstitutionAchievements.InstitutionAchievement> listItems = getInstitutionAchievements(ecfOrgUnit, enrollmentCampaign);
        if(!listItems.isEmpty())
        {
            PackageData.InstitutionAchievements achievements = new PackageData.InstitutionAchievements();
            achievements.getInstitutionAchievement().addAll(listItems);
            packageData.setInstitutionAchievements(achievements);
        }
        setProcessState(80);

        // TargetOrganizations
        addTargetOrganizations(packageData, enrollmentCampaign);
        setProcessState(90);

        return FisServiceImpl.toXml(root);
    }

    // AdmissionInfo
    protected AdmissionInfo getAdmissionInfo(EcfOrgUnit ecfOrgUnit, EnrollmentCampaign enrollmentCampaign)
    {
        final AdmissionInfo info = new AdmissionInfo();


        // AdmissionVolume / Item
        final Map<MultiKey, AdmissionVolume.Item> globalItemMap = SafeMap.get(key -> {
            final Long eduLvlId = (Long) key.getKey(0);
            final Long directionId = (Long) key.getKey(1);
            final Integer course = (Integer) key.getKey(2);
            final AdmissionVolume.Item item = new AdmissionVolume.Item();
            item.setCampaignUID(uid(ecfOrgUnit, enrollmentCampaign));
            item.setEducationLevelID(eduLvlId);
            item.setDirectionID(directionId);
            item.setUID(uid(ecfOrgUnit, enrollmentCampaign, eduLvlId, directionId, course));
            return item;
        });

        info.setAdmissionVolume(new AdmissionVolume());
        final Map<EnrollmentDirection, AdmissionVolume.Item> directionItemMap =
                getAdmissionVolumeItemByDirection(enrollmentCampaign, ecfOrgUnit, globalItemMap);
        setProcessState(40);

        final List<AdmissionVolume.Item> items = info.getAdmissionVolume().getItem();
        items.addAll(globalItemMap.values().stream()
                             .filter(i -> (i.getNumberBudgetO() != null && i.getNumberBudgetO() > 0)
                                     || (i.getNumberBudgetOZ() != null && i.getNumberBudgetOZ() > 0)
                                     || (i.getNumberBudgetZ() != null && i.getNumberBudgetZ() > 0)
                                     || (i.getNumberPaidO() != null && i.getNumberPaidO() > 0)
                                     || (i.getNumberPaidOZ() != null && i.getNumberPaidOZ() > 0)
                                     || (i.getNumberPaidZ() != null && i.getNumberPaidZ() > 0)
                                     || (i.getNumberTargetO() != null && i.getNumberTargetO() > 0)
                                     || (i.getNumberTargetOZ() != null && i.getNumberTargetOZ() > 0)
                                     || (i.getNumberTargetZ() != null && i.getNumberTargetZ() > 0)
                                     || (i.getNumberQuotaO() != null && i.getNumberQuotaO() > 0)
                                     || (i.getNumberQuotaOZ() != null && i.getNumberQuotaOZ() > 0)
                                     || (i.getNumberQuotaZ() != null && i.getNumberQuotaZ() > 0)
                             )
                             .collect(Collectors.toList()));

        if (items.isEmpty())
            throw new ApplicationException(EcfOrgUnitManager.instance().getProperty("error.admission.no-items"));
        setProcessState(50);


        // AdmissionVolume / DistributedAdmissionVolume
        info.setDistributedAdmissionVolume(new DistributedAdmissionVolume());
        info.getDistributedAdmissionVolume().getItem().addAll(
                items.stream()
                        .map(this::getDistributedAdmissionVolumeItem)
                        .collect(Collectors.toList())
        );
        setProcessState(60);


        // CompetitiveGroups / CompetitiveGroup
        info.setCompetitiveGroups(new CompetitiveGroups());
        fillCompetitiveGroups(info, enrollmentCampaign, ecfOrgUnit, directionItemMap);

        return info;
    }

    // TargetOrganizations
    protected void addTargetOrganizations(PackageData packageData, EnrollmentCampaign enrollmentCampaign)
    {
        PackageData.TargetOrganizations targetOrganizations = new PackageData.TargetOrganizations();

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(TargetAdmissionKind.class, "ta")
                .column(property("ta"))
                .where(notExists(new DQLSelectBuilder()
                                         .fromEntity(NotUsedTargetAdmissionKind.class, "nu")
                                         .column(value(1))
                                         .where(eq(property("nu", NotUsedTargetAdmissionKind.enrollmentCampaign()), value(enrollmentCampaign)))
                                         .where(eq(property("nu", NotUsedTargetAdmissionKind.targetAdmissionKind()), property("ta")))
                                         .buildQuery()
                ));

        List<TargetAdmissionKind> targetAdmissionKinds = builder.createStatement(getSession()).list();
        if (CollectionUtils.isEmpty(targetAdmissionKinds)) return;

        targetOrganizations.getTargetOrganization().addAll(
                targetAdmissionKinds.stream()
                        .map(tak -> {
                            PackageData.TargetOrganizations.TargetOrganization targetOrganization = new PackageData.TargetOrganizations.TargetOrganization();
                            targetOrganization.setUID(uid4TargetAdmissionKind(enrollmentCampaign, tak));
                            targetOrganization.setName(tak.getTitle());
                            return targetOrganization;
                        })
                        .collect(Collectors.toList())
        );

        packageData.setTargetOrganizations(targetOrganizations);
    }

    protected Map<EnrollmentDirection, AdmissionVolume.Item> getAdmissionVolumeItemByDirection(
            EnrollmentCampaign enrollmentCampaign, EcfOrgUnit ecfOrgUnit, Map<MultiKey, AdmissionVolume.Item> globalItemMap)
    {
        final IEcfConverterDao4EduOu eduOrgUnitDao = EcfConverterManager.instance().educationOrgUnit();
        final IEcfConverter<EducationOrgUnit> eduOrgUnitConverter = eduOrgUnitDao.getConverter();
        final IEcfConverterDao4EduForm educationFormDao = EcfConverterManager.instance().educationForm();

        final List<EnrollmentDirection> directions = EcfOrgUnitManager.instance().dao().getEnrollmentDirections(ecfOrgUnit, enrollmentCampaign);

        return directions.stream()
                .collect(Collectors.toMap(dir -> dir,
                                          dir -> {
                                              EcfCatalogItem fisEduLvl = eduOrgUnitDao.getEcfItem4EducationLevel(dir.getEducationOrgUnit());
                                              if (null == fisEduLvl)
                                                  throw new ApplicationException(EcfOrgUnitManager.instance().getProperty("error.admission.no-fis-level", dir.getEducationOrgUnit().getTitle()));

                                              EcfCatalogItem fisDirection = eduOrgUnitConverter.getEcfCatalogItem(dir.getEducationOrgUnit(), false);
                                              if (null == fisDirection)
                                                  throw new ApplicationException(EcfOrgUnitManager.instance().getProperty("error.admission.no-fis-direction", dir.getEducationOrgUnit().getTitle()));

                                              Integer course = dir.getEntranceCourse().getIntValue();

                                              AdmissionVolume.Item item = globalItemMap.get(new MultiKey(fisEduLvl.getFisID(), fisDirection.getFisID(), course));

                                              addNumbers2Item(item, dir, this.getEduFormPostfix(educationFormDao, dir));

                                              return item;
                                          }
                ));
    }

    protected void addNumbers2Item(AdmissionVolume.Item item, EnrollmentDirection dir, String eduFormPostfix)
    {
        if (EcfFinSourceDao.isBudget(dir))
            this.add(item, "numberBudget" + eduFormPostfix, dir.getMinisterialPlan() - (dir.getTargetAdmissionPlanBudget() != null ? dir.getTargetAdmissionPlanBudget() : 0));

        if (EcfFinSourceDao.isContract(dir))
            this.add(item, "numberPaid" + eduFormPostfix, dir.getContractPlan());

        if (EcfFinSourceDao.isListener(dir))
            this.add(item, "numberPaid" + eduFormPostfix, dir.getListenerPlan()); // второе высшее и слушатели - это только договорники

        if (EcfFinSourceDao.isSecondEdu(dir))
            this.add(item, "numberPaid" + eduFormPostfix, dir.getSecondHighEducationPlan()); // второе высшее и слушатели - это только договорники

        if (EcfFinSourceDao.isTargetAdmission(dir))
            this.add(item, "numberTarget" + eduFormPostfix, dir.getTargetAdmissionPlanBudget());

        if (EcfFinSourceDao.isQuota(dir))
            this.add(item, "numberQuota" + eduFormPostfix, dir.getSpecRightsQuota());
    }

    protected DistributedAdmissionVolume.Item getDistributedAdmissionVolumeItem(AdmissionVolume.Item item)
    {
        DistributedAdmissionVolume.Item disItem = new DistributedAdmissionVolume.Item();

        disItem.setAdmissionVolumeUID(item.getUID());
        disItem.setLevelBudget(1L);

        disItem.setNumberBudgetO(item.getNumberBudgetO());
        disItem.setNumberBudgetZ(item.getNumberBudgetZ());
        disItem.setNumberBudgetOZ(item.getNumberBudgetOZ());

        disItem.setNumberQuotaO(item.getNumberQuotaO());
        disItem.setNumberQuotaZ(item.getNumberQuotaZ());
        disItem.setNumberQuotaOZ(item.getNumberQuotaOZ());

        disItem.setNumberTargetO(item.getNumberTargetO());
        disItem.setNumberTargetZ(item.getNumberTargetZ());
        disItem.setNumberTargetOZ(item.getNumberTargetOZ());

        return disItem;
    }

    // CompetitiveGroups
    protected void fillCompetitiveGroups(AdmissionInfo info, EnrollmentCampaign enrollmentCampaign, EcfOrgUnit ecfOrgUnit,
                                         Map<EnrollmentDirection, AdmissionVolume.Item> directionItemMap)
    {
        final List<CompetitiveGroup> groups = info.getCompetitiveGroups().getCompetitiveGroup();

        final Map<EnrollmentDirection, Map<StudentCategory, List<EntranceDiscipline>>> disciplinesByStudentCategoryByDirectionMap = getDisciplinesByStudentCategoryByDirectionMap(enrollmentCampaign);
        final List<StudentCategory> studentCategoriesList = getCatalogItemList(StudentCategory.class, StudentCategory.code().s(), getPossibleCategories(enrollmentCampaign));

        int[] directionIndex = new int[]{0};
        int[] directionLength = new int[]{directionItemMap.size()};
        directionItemMap.entrySet().forEach(entry -> {
            setProcessState(95, directionIndex[0]++, directionLength[0]);
            EnrollmentDirection direction = entry.getKey();
            AdmissionVolume.Item item = entry.getValue();
            Map<StudentCategory, List<EntranceDiscipline>> disciplinesByStudentCategoryMap = disciplinesByStudentCategoryByDirectionMap.get(direction);
            groups.addAll(getCompetitiveGroup4Direction(ecfOrgUnit, direction, item, studentCategoriesList, disciplinesByStudentCategoryMap));
        });
    }

    protected Map<EnrollmentDirection, Map<StudentCategory, List<EntranceDiscipline>>> getDisciplinesByStudentCategoryByDirectionMap(EnrollmentCampaign enrollmentCampaign)
    {
        return getList(EntranceDiscipline.class, EntranceDiscipline.enrollmentDirection().enrollmentCampaign().s(), enrollmentCampaign)
                .stream()
                .collect(Collectors.groupingBy(
                        EntranceDisciplineGen::getEnrollmentDirection,
                        Collectors.mapping(
                                dis -> dis,
                                Collectors.groupingBy(EntranceDisciplineGen::getStudentCategory,
                                                      Collectors.mapping(dis -> dis, Collectors.toList())))));
    }

    protected List<CompetitiveGroup> getCompetitiveGroup4Direction(EcfOrgUnit ecfOrgUnit,
                                                                   EnrollmentDirection direction,
                                                                   AdmissionVolume.Item item,
                                                                   List<StudentCategory> studentCategoriesList,
                                                                   Map<StudentCategory, List<EntranceDiscipline>> disciplinesByStudentCategoryMap)
    {
        List<CompetitiveGroup> groups = new ArrayList<>();


        List<EcfCatalogItem> sourceList = EcfConverterManager.instance().financingSource().getSourceList(direction);
        EcfCatalogItem sourceContract = this.<EcfCatalogItem>getByNaturalId(new EcfCatalogItemGen.NaturalId(IEcfFinSourceDao.CATALOG_CODE, IEcfFinSourceDao.CONTRACT_CODE));

        studentCategoriesList.forEach(studentCategory -> {
            List<EntranceDiscipline> disciplines = disciplinesByStudentCategoryMap != null ? disciplinesByStudentCategoryMap.get(studentCategory) : null;
            String studentCategoryCode = studentCategory.getCode();

            switch (studentCategory.getCode())
            {
                case StudentCategoryCodes.STUDENT_CATEGORY_SECONDARY:
                {
                    if (EcfFinSourceDao.isSecondEdu(direction))
                        fillCompetitiveGroup(groups, ecfOrgUnit, direction, studentCategoryCode, disciplines, sourceContract, item, false);
                    break;
                }
                case StudentCategoryCodes.STUDENT_CATEGORY_LISTENER:
                {
                    break;
                }
                default:
                    sourceList.forEach(source -> fillCompetitiveGroup(groups, ecfOrgUnit, direction, studentCategoryCode, disciplines, source, item, false));
            }
        });

        if (direction.isCrimea() && direction.getCrimeaQuota() != null && direction.getCrimeaQuota() > 0)
        {
            EcfCatalogItem sourceBudget = this.<EcfCatalogItem>getByNaturalId(new EcfCatalogItemGen.NaturalId(IEcfFinSourceDao.CATALOG_CODE, IEcfFinSourceDao.BUDGET_CODE));
            StudentCategory studentCategory = this.getByCode(StudentCategory.class, StudentCategoryCodes.STUDENT_CATEGORY_STUDENT);

            fillCompetitiveGroup(groups, ecfOrgUnit, direction, studentCategory.getCode(),
                                 disciplinesByStudentCategoryMap.get(studentCategory), sourceBudget, item, true);
        }

        return groups;
    }


    // CompetitiveGroups / CompetitiveGroup
    protected void fillCompetitiveGroup(List<CompetitiveGroup> groups,
                                        EcfOrgUnit ecfOrgUnit,
                                        EnrollmentDirection direction,
                                        String studentCategoryCode,
                                        List<EntranceDiscipline> disciplines,
                                        EcfCatalogItem financeSource,
                                        AdmissionVolume.Item item,
                                        boolean fromCrimea)
    {
        final IEcfConverter<EntranceDiscipline> disciplineSetConverter = EcfConverterManager.instance().converterDao4EntranceDiscipline().getConverter();
        EnrollmentCampaign enrollmentCampaign = direction.getEnrollmentCampaign();
        String developTechCode = direction.getEducationOrgUnit().getDevelopTech().getCode();
        String developConditionCode = direction.getEducationOrgUnit().getDevelopCondition().getCode();

        final String groupUID = getCompetitionGroupUid(ecfOrgUnit, direction, studentCategoryCode, disciplines);
        final CompetitiveGroup group = new CompetitiveGroup();
        group.setUID(groupUID + "-" + financeSource.getFisItemCode() + getCrimeaPostfix(fromCrimea));
        group.setCampaignUID(uid(ecfOrgUnit, enrollmentCampaign));
        group.setName(getCompetitiveGroupName(direction, studentCategoryCode, financeSource, disciplines, fromCrimea));

        // CompetitiveGroup / CommonBenefit
        // только для первого образования (либо если они не различимы)
        // только для полного срока и обычной технологии
        // здесь выдаем все льготы (считается, что по КГ прием возможен по любой олимпиаде, если она является профильной - а признак профильности определить мы не можем - так что выводим все)
        final boolean hasCommonBenefit = DevelopConditionCodes.FULL_PERIOD.equals(developConditionCode)
                && DevelopTechCodes.GENERAL.equals(developTechCode)
                && !StudentCategoryCodes.STUDENT_CATEGORY_SECONDARY.equals(studentCategoryCode);
        if (hasCommonBenefit)
            group.setCommonBenefit(getCommonBenefit(group.getUID(), disciplines, disciplineSetConverter));

        if (EcfFinSourceDao.isTargetAdmission(financeSource))        // CompetitiveGroup / TargetOrganizations - Целевой прием
        {
            if (!studentCategoryCode.equals(StudentCategoryCodes.STUDENT_CATEGORY_SECONDARY))
                addTargetOrganizationList(group, direction);
        }
        else        // CompetitiveGroup / Items - Остальные
        {
            CompetitiveGroup.CompetitiveGroupItem groupItem = new CompetitiveGroup.CompetitiveGroupItem();
            final String eduFormPostfix = this.getEduFormPostfix(EcfConverterManager.instance().educationForm(), direction);

            if (EcfFinSourceDao.isBudget(financeSource))
            {
                int number = fromCrimea ? direction.getCrimeaQuota() : direction.getMinisterialPlan()
                        - (direction.getTargetAdmissionPlanBudget() != null ? direction.getTargetAdmissionPlanBudget() : 0)
                        - (direction.getCrimeaQuota() != null ? direction.getCrimeaQuota() : 0);
                this.add(groupItem, "numberBudget" + eduFormPostfix, number);
            }

            if (EcfFinSourceDao.isQuota(financeSource))
                this.add(groupItem, "numberQuota" + eduFormPostfix, direction.getSpecRightsQuota());

            if (EcfFinSourceDao.isContract(financeSource))
            {
                if (studentCategoryCode.equals(StudentCategoryCodes.STUDENT_CATEGORY_SECONDARY))
                    this.add(groupItem, "numberPaid" + eduFormPostfix, direction.getSecondHighEducationPlan());
                else
                    this.add(groupItem, "numberPaid" + eduFormPostfix, direction.getContractPlan());
            }

            group.setCompetitiveGroupItem(groupItem);
        }

        // Пустые группы не нужны
        if (group.getTargetOrganizations() == null && group.getCompetitiveGroupItem() == null) return;

        // CompetitiveGroup / EntranceTestItems
        addEntranceTestItems(group, enrollmentCampaign, ecfOrgUnit, groupUID, hasCommonBenefit, financeSource, disciplines, disciplineSetConverter, fromCrimea);

        group.setDirectionID(item.getDirectionID());
        IEcfConverter<DevelopForm> developFormEcfConverter = EcfConverterManager.instance().educationForm().getConverter();
        group.setEducationFormID(developFormEcfConverter.getEcfCatalogItem(direction.getEducationOrgUnit().getDevelopForm(), true).getFisID());
        group.setEducationLevelID(item.getEducationLevelID());
        group.setEducationSourceID(financeSource.getFisID());
        group.setIsForKrym(fromCrimea);

        groups.add(group);
    }

    public static List<String> getPossibleCategories(EnrollmentCampaign enrollmentCampaign)
    {
        List<String> possibleCategories = new ArrayList<>(4);
        possibleCategories.add(StudentCategoryCodes.STUDENT_CATEGORY_STUDENT);
        if (enrollmentCampaign.isExamSetDiff())
        {
            // если различается - то добавляем все
            possibleCategories.add(StudentCategoryCodes.STUDENT_CATEGORY_SECONDARY);
            possibleCategories.add(StudentCategoryCodes.STUDENT_CATEGORY_LISTENER);
        }

        return possibleCategories;
    }

    // CompetitiveGroups / CompetitiveGroup / Name
    public static String getCompetitiveGroupName(EnrollmentDirection direction, String categoryCode,
                                                 EcfCatalogItem financeSource, List<EntranceDiscipline> disciplines,
                                                 boolean fromCrimea)
    {
        StringBuilder titleBuilder = new StringBuilder();

        if (!direction.getEnrollmentCampaign().isExamSetDiff() ||
                StudentCategoryCodes.STUDENT_CATEGORY_STUDENT.equals(categoryCode) ||
                StudentCategoryCodes.STUDENT_CATEGORY_LISTENER.equals(categoryCode))
        {
            if (direction.getEnrollmentCampaign().isUseCompetitionGroup())
                titleBuilder.append(direction.getCompetitionGroup().getTitle())
                        .append(", ").append(direction.getEnrollmentCampaign().getTitle());
            else
                titleBuilder = getDirectionName(direction);
        }
        else if (StudentCategoryCodes.STUDENT_CATEGORY_SECONDARY.equals(categoryCode))
            titleBuilder = getDirectionName(direction);

        String developTechTitle = StringUtils.trimToNull(direction.getEducationOrgUnit().getDevelopTech().getShortTitle());
        if (developTechTitle != null)
            titleBuilder.append(", ").append(developTechTitle);

        String developCondTitle = StringUtils.trimToNull(direction.getEducationOrgUnit().getDevelopCondition().getShortTitle());
        if (null != developCondTitle)
            titleBuilder.append(", ").append(developCondTitle);

        if (EcfFinSourceDao.isQuota(financeSource))
            titleBuilder.append(", ").append("Квота");// Слишком длинное название для квот
        else
            titleBuilder.append(", ").append(financeSource.getTitle());

        if (fromCrimea)
            titleBuilder.append("-Крым");

        if (!CollectionUtils.isEmpty(disciplines))
        {
            String examsString = disciplines.stream()
                    .map(dis -> {
                        String kindTitle = StringUtils.trimToNull(dis.getKind().getShortTitle());
                        if (null != kindTitle && dis.getKind().getCode().equals(UniecDefines.ENTRANCE_DISCIPLINE_KIND_PROFILE))
                            return dis.getDiscipline().getShortTitle() + " (" + kindTitle + ")";

                        return dis.getDiscipline().getShortTitle();
                    })
                    .collect(Collectors.joining(", "));

            if (examsString.length() > 0)
                titleBuilder.append(" (").append(examsString).append(")");
        }

        if (categoryCode.equals(StudentCategoryCodes.STUDENT_CATEGORY_SECONDARY))
            titleBuilder.append(", ").append("Вт. высшее");

        return titleBuilder.toString();
    }

    protected static StringBuilder getDirectionName(EnrollmentDirection direction)
    {
        return new StringBuilder().append(direction.getTitle())
                .append(", ").append(direction.getEnrollmentCampaign().getTitle())
                .append(", ").append(direction.getEducationOrgUnit().getFormativeOrgUnit().getShortTitle())
                .append(", ").append(direction.getEducationOrgUnit().getTerritorialOrgUnit().getTerritorialShortTitle())
                .append(", ").append(direction.getEducationOrgUnit().getDevelopForm().getShortTitle())
                .append(", ").append(direction.getEducationOrgUnit().getDevelopPeriod().getTitle());
    }

    // CompetitiveGroup / TargetOrganizations
    protected void addTargetOrganizationList(CompetitiveGroup group, EnrollmentDirection direction)
    {
        CompetitiveGroup.TargetOrganizations targetOrganizations = new CompetitiveGroup.TargetOrganizations();

        final IEcfConverterDao4EduForm educationFormDao = EcfConverterManager.instance().educationForm();
        final String eduFormPostfix = getEduFormPostfix(educationFormDao, direction);

        TargetAdmissionKind targetAdmissionKind = getUsedSimpleTargetAdmissionKind(direction.getEnrollmentCampaign());
        if (targetAdmissionKind != null)            // если в ПК используется один Вид ЦП, то данные по плану приема хранятся в НП
        {
            CompetitiveGroup.TargetOrganizations.TargetOrganization to = getTargetOrganization(
                    direction, targetAdmissionKind, eduFormPostfix, direction.getTargetAdmissionPlanBudget());
            targetOrganizations.getTargetOrganization().add(to);
        }
        else        // если в ПК используется несколько Видов ЦП то данные по планам приема хранятся в релейшенах
        {
            String alias = "rel";
            final List<TargetAdmissionPlanRelation> relations = getList(
                    new DQLSelectBuilder()
                            .fromEntity(TargetAdmissionPlanRelation.class, alias)
                            .column(property(alias))
                            .where(eq(property(alias, TargetAdmissionPlanRelation.entranceEducationOrgUnit()), value(direction)))
                            .where(eq(property(alias, TargetAdmissionPlanRelation.budget()), value(Boolean.TRUE)))
                            .where(gt(property(alias, TargetAdmissionPlanRelation.planValue()), value(0)))
            );

            targetOrganizations.getTargetOrganization().addAll(
                    relations.stream()
                            .map(rel -> getTargetOrganization(direction, rel.getTargetAdmissionKind(), eduFormPostfix, rel.getPlanValue()))
                            .collect(Collectors.toList())
            );
        }


        if (!targetOrganizations.getTargetOrganization().isEmpty())
            group.setTargetOrganizations(targetOrganizations);
    }

    protected CompetitiveGroup.TargetOrganizations.TargetOrganization getTargetOrganization(
            EnrollmentDirection direction, TargetAdmissionKind targetAdmissionKind,
            String eduFormPostfix, int planValue)
    {
        CompetitiveGroup.TargetOrganizations.TargetOrganization targetOrganization = new CompetitiveGroup.TargetOrganizations.TargetOrganization();
        targetOrganization.setUID(uid4TargetAdmissionKind(direction.getEnrollmentCampaign(), targetAdmissionKind));

        CompetitiveGroup.TargetOrganizations.TargetOrganization.CompetitiveGroupTargetItem targetItem =
                new CompetitiveGroup.TargetOrganizations.TargetOrganization.CompetitiveGroupTargetItem();

        this.add(targetItem, "numberTarget" + eduFormPostfix, planValue);

        targetOrganization.setCompetitiveGroupTargetItem(targetItem);

        return targetOrganization;
    }

    // CompetitiveGroup / CommonBenefit
    protected CompetitiveGroup.CommonBenefit getCommonBenefit(String groupUid, List<EntranceDiscipline> disciplines, IEcfConverter<EntranceDiscipline> disciplineConverter)
    {
        // всегда один элемент
        final CompetitiveGroup.CommonBenefit.CommonBenefitItem commonBenefitItem = new CompetitiveGroup.CommonBenefit.CommonBenefitItem();
        commonBenefitItem.setUID(groupUid + ".benefit");
        commonBenefitItem.setBenefitKindID(1L); // всегда вне конкурса
        commonBenefitItem.setIsForAllOlympics(true); // для всех
        commonBenefitItem.setOlympicDiplomTypes(new CompetitiveGroup.CommonBenefit.CommonBenefitItem.OlympicDiplomTypes());
        commonBenefitItem.getOlympicDiplomTypes().getOlympicDiplomTypeID().add(1L); // победитель
        commonBenefitItem.getOlympicDiplomTypes().getOlympicDiplomTypeID().add(2L); // призер

        CompetitiveGroup.CommonBenefit.CommonBenefitItem.MinEgeMarks minMarksList = new CompetitiveGroup.CommonBenefit.CommonBenefitItem.MinEgeMarks();


        if (!CollectionUtils.isEmpty(disciplines))
        {
            for (final EntranceDiscipline discipline : disciplines)
            {
                final EcfCatalogItem setDisciplineTestSubject = disciplineConverter.getEcfCatalogItem(discipline, false);
                if (null != setDisciplineTestSubject)
                {
                    if (discipline.getDiscipline() instanceof Discipline2RealizationWayRelation)
                    {
                        CompetitiveGroup.CommonBenefit.CommonBenefitItem.MinEgeMarks.MinMarks minMarks = new CompetitiveGroup.CommonBenefit.CommonBenefitItem.MinEgeMarks.MinMarks();
                        minMarks.setSubjectID(setDisciplineTestSubject.getFisID());
                        minMarks.setMinMark(75);
                        minMarksList.getMinMarks().add(minMarks);
                    }
                }
            }
        }

        if (minMarksList.getMinMarks().isEmpty())
        {
            CompetitiveGroup.CommonBenefit.CommonBenefitItem.MinEgeMarks.MinMarks minMarks = new CompetitiveGroup.CommonBenefit.CommonBenefitItem.MinEgeMarks.MinMarks();
            minMarks.setSubjectID(1);
            minMarks.setMinMark(75);
            minMarksList.getMinMarks().add(minMarks);
        }

        commonBenefitItem.setMinEgeMarks(minMarksList);

        commonBenefitItem.setLevelForAllOlympics(1L);

        commonBenefitItem.setProfileForAllOlympics(new CompetitiveGroup.CommonBenefit.CommonBenefitItem.ProfileForAllOlympics());
        commonBenefitItem.getProfileForAllOlympics().getProfileID().add(255L);

        commonBenefitItem.setClassForAllOlympics(255L);

        CompetitiveGroup.CommonBenefit commonBenefit = new CompetitiveGroup.CommonBenefit();
        commonBenefit.getCommonBenefitItem().add(commonBenefitItem);

        return commonBenefit;
    }

    // CompetitiveGroup / EntranceTestItems
    protected void addEntranceTestItems(CompetitiveGroup group, EnrollmentCampaign enrollmentCampaign, EcfOrgUnit ecfOrgUnit,
                                        String ecgUid, boolean hasCommonBenefit, EcfCatalogItem financeSource, List<EntranceDiscipline> disciplines,
                                        IEcfConverter<EntranceDiscipline> disciplineSetConverter, boolean fromCrimea)
    {
        CompetitiveGroup.EntranceTestItems entranceTestItems = new CompetitiveGroup.EntranceTestItems();

        final IEcfEcgDao ecgDao = EcfConverterManager.instance().ecg();

        if (!CollectionUtils.isEmpty(disciplines))
        {
            final List<CompetitiveGroup.EntranceTestItems.EntranceTestItem> testItems = entranceTestItems.getEntranceTestItem();

            for (final EntranceDiscipline discipline : disciplines)
            {
                final String testItemUid = ecgUid
                        + ".ex-" + uid(ecfOrgUnit, enrollmentCampaign, getDisciplineUidPostfix(discipline))
                        + "-" + financeSource.getFisItemCode()
                        + getCrimeaPostfix(fromCrimea);
                final CompetitiveGroup.EntranceTestItems.EntranceTestItem testItem = new CompetitiveGroup.EntranceTestItems.EntranceTestItem();
                testItem.setEntranceTestSubject(new TEntranceTestSubject());
                testItem.setUID(testItemUid);
                testItem.setEntranceTestTypeID(ecgDao.getTestTypeId(discipline.getType()));

                final EcfCatalogItem setDisciplineTestSubject = disciplineSetConverter.getEcfCatalogItem(discipline, false);
                if (setDisciplineTestSubject != null)
                    testItem.getEntranceTestSubject().setSubjectID(setDisciplineTestSubject.getFisID());
                else
                    testItem.getEntranceTestSubject().setSubjectName(discipline.getTitle());

                if (hasCommonBenefit)
                {
                    // всегда один элемент
                    final CompetitiveGroup.EntranceTestItems.EntranceTestItem.EntranceTestBenefits.EntranceTestBenefitItem testBenefitItem = new CompetitiveGroup.EntranceTestItems.EntranceTestItem.EntranceTestBenefits.EntranceTestBenefitItem();
                    testBenefitItem.setUID(testItem.getUID() + ".benefit" + getCrimeaPostfix(fromCrimea));
                    testBenefitItem.setBenefitKindID(3L); // всегда Приравнивание к лицам, набравшим максимальное количество баллов по ЕГЭ
                    testBenefitItem.setIsForAllOlympics(true); // для всех
                    testBenefitItem.setMinEgeMark(75); // хак для поддержки новой структуры пакетов
                    testBenefitItem.setOlympicDiplomTypes(new CompetitiveGroup.EntranceTestItems.EntranceTestItem.EntranceTestBenefits.EntranceTestBenefitItem.OlympicDiplomTypes());
                    testBenefitItem.getOlympicDiplomTypes().getOlympicDiplomTypeID().add(1L); // победитель
                    testBenefitItem.getOlympicDiplomTypes().getOlympicDiplomTypeID().add(2L); // призер
                    testItem.setEntranceTestBenefits(new CompetitiveGroup.EntranceTestItems.EntranceTestItem.EntranceTestBenefits());
                    testBenefitItem.setLevelForAllOlympics(1L);

                    testBenefitItem.setProfileForAllOlympics(new CompetitiveGroup.EntranceTestItems.EntranceTestItem.EntranceTestBenefits.EntranceTestBenefitItem.ProfileForAllOlympics());
                    testBenefitItem.getProfileForAllOlympics().getProfileID().add(255L);

                    testBenefitItem.setClassForAllOlympics(255L);

                    testItem.getEntranceTestBenefits().getEntranceTestBenefitItem().add(testBenefitItem);
                }
                if (discipline.getPassMark() != null)
                    testItem.setMinScore(BigDecimal.valueOf(discipline.getPassMark()));
                testItem.setEntranceTestPriority(BigInteger.valueOf(discipline.getPriority() != null ? discipline.getPriority() : 0));

                testItems.add(testItem);
            }
            group.setEntranceTestItems(entranceTestItems);
        }
    }


    private List<PackageData.InstitutionAchievements.InstitutionAchievement> getInstitutionAchievements(EcfOrgUnit ecfOrgUnit, EnrollmentCampaign campaign)
    {
        final IEcfConverter<IndividualProgress> individProgressIEcfConverter = EcfConverterManager.instance().individualAchievement().getConverter();
        List<IndividualProgress> listAchievement = getPropertiesList(EnrCampaignEntrantIndividualProgress.class, EnrCampaignEntrantIndividualProgress.enrollmentCampaign(),
                                                                     campaign, false, EnrCampaignEntrantIndividualProgress.individualProgress());
        List<PackageData.InstitutionAchievements.InstitutionAchievement> resultList = new ArrayList<>(listAchievement.size());
        for (IndividualProgress item : listAchievement)
        {
            PackageData.InstitutionAchievements.InstitutionAchievement newItem = new PackageData.InstitutionAchievements.InstitutionAchievement();
            newItem.setCampaignUID(uid(ecfOrgUnit, campaign));
            newItem.setInstitutionAchievementUID(item.getCode());
            newItem.setName(item.getTitle());
            newItem.setMaxValue(BigDecimal.valueOf(item.getMaxMark()));
            final EcfCatalogItem indProgressCatalogItem = individProgressIEcfConverter.getEcfCatalogItem(item, false);
            if (null != indProgressCatalogItem)
                newItem.setIdCategory(indProgressCatalogItem.getFisID());
            resultList.add(newItem);
        }
        return resultList;
    }


    public String getEduFormPostfix(final IEcfConverterDao4EduForm educationFormDao, final EnrollmentDirection dir)
    {
        final String eduFormPostfix = educationFormDao.getEduFormPostfix(dir.getEducationOrgUnit().getDevelopForm());
        if (null == eduFormPostfix)
        {
            throw new ApplicationException(EcfOrgUnitManager.instance().getProperty("error.admission.no-fis-eduform", dir.getEducationOrgUnit().getTitle()));
        }
        return eduFormPostfix;
    }

    private void add(final Object object, final String property, int i)
    {
        final Long value = (Long) FastBeanUtils.getValue(object, property);
        if (null != value)
        {
            i += value.intValue();
        }
        FastBeanUtils.setValue(object, property, (long) i);
    }
}
