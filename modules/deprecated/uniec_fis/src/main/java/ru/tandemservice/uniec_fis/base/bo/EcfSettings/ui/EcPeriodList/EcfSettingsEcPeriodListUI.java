package ru.tandemservice.uniec_fis.base.bo.EcfSettings.ui.EcPeriodList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec_fis.base.bo.EcfSettings.EcfSettingsManager;
import ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodItem;
import ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodKey;
import ru.tandemservice.uniec_fis.base.bo.EcfSettings.ui.EcPeriodEdit.EcfSettingsEcPeriodEdit;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author vdanilov
 */
public class EcfSettingsEcPeriodListUI extends UIPresenter {

    public List<EnrollmentCampaign> getEnrollmentCampaignList() {
        return DataAccessServices.dao().getList(
                new DQLSelectBuilder().fromEntity(EnrollmentCampaign.class, "c").column(property("c"))
                        .order(property(EnrollmentCampaign.educationYear().intValue().fromAlias("c")), OrderDirection.desc));
    }

    public EnrollmentCampaign getEnrollmentCampaign() {
        return getSettings().get("enrollmentCampaign");
    }

    private Map<EcfSettingsECPeriodKey, List<EcfSettingsECPeriodItem>> dataMap = Collections.emptyMap();
    public void setDataMap(Map<EcfSettingsECPeriodKey, List<EcfSettingsECPeriodItem>> dataMap) { this.dataMap = dataMap; }
    public Map<EcfSettingsECPeriodKey, List<EcfSettingsECPeriodItem>> getDataMap() { return this.dataMap; }

    public List<EcfSettingsECPeriodKey> getKeyList() { return new ArrayList<EcfSettingsECPeriodKey>(getDataMap().keySet()); }

    private EcfSettingsECPeriodKey currentKey;
    public EcfSettingsECPeriodKey getCurrentKey() { return this.currentKey; }
    public void setCurrentKey(EcfSettingsECPeriodKey currentKey) { this.currentKey = currentKey; }

    public List<EcfSettingsECPeriodItem> getItemList() { return getDataMap().get(getCurrentKey()); }

    private EcfSettingsECPeriodItem currentItem;
    public EcfSettingsECPeriodItem getCurrentItem() { return this.currentItem; }
    public void setCurrentItem(EcfSettingsECPeriodItem currentItem) { this.currentItem = currentItem; }

    public void refreshDataMap()
    {
        LinkedHashMap<EcfSettingsECPeriodKey, List<EcfSettingsECPeriodItem>> dataMap = new LinkedHashMap<EcfSettingsECPeriodKey, List<EcfSettingsECPeriodItem>>();
        setDataMap(dataMap);

        EnrollmentCampaign ec = getEnrollmentCampaign();
        if (null == ec) { return; }

        ICommonDAO dao = DataAccessServices.dao();

        // сначала разбираемся с ключами
        List<EcfSettingsECPeriodKey> keyList = dao.getList(EcfSettingsECPeriodKey.class, EcfSettingsECPeriodKey.enrollmentCampaign(), ec);
        Collections.sort(keyList);

        for (EcfSettingsECPeriodKey key: keyList) {
            dataMap.put(key, new ArrayList<EcfSettingsECPeriodItem>(4));
        }

        // затем к каждому ключу добавляем элементы (поскольку в нашем несовершенном мире нет блокировок на чтение, даже если засунуть это в один dao, то может случиться npe)
        List<EcfSettingsECPeriodItem> itemList = dao.getList(EcfSettingsECPeriodItem.class, EcfSettingsECPeriodItem.key(), keyList);
        Collections.sort(itemList);

        for (EcfSettingsECPeriodItem item: itemList) {
            List<EcfSettingsECPeriodItem> list = dataMap.get(item.getKey());
            if (null != list) { list.add(item); }
        }
    }


    @Override
    public void onComponentRefresh() {
        refreshDataMap();
    }

    public void onSelectEnrollmentCampaign() {
        getSettings().save();
        refreshDataMap();
    }

    public void onClickAddKeysFromDirections() {
        EnrollmentCampaign ec = getEnrollmentCampaign();
        if (null == ec) {
            throw new ApplicationException("Требуется выбрать приемную кампанию");
        }

        EcfSettingsManager.instance().dao().doCreateEcfSettingsECPeriodKeys(ec, null);
        getSupport().setRefreshScheduled(true);
    }

    public void onClickEdit() {
        getActivationBuilder().asRegionDialog(EcfSettingsEcPeriodEdit.class).parameter(PUBLISHER_ID, getSupport().getListenerParameterAsLong()).activate();
    }



}
