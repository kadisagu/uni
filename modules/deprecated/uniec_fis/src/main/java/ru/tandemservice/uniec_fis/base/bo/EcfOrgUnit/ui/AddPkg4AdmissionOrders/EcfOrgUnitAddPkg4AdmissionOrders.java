/**
 *$Id$
 */
package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.ui.AddPkg4AdmissionOrders;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author Alexander Shaburov
 * @since 15.01.13
 */
@Configuration
public class EcfOrgUnitAddPkg4AdmissionOrders extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .create();
    }
}
