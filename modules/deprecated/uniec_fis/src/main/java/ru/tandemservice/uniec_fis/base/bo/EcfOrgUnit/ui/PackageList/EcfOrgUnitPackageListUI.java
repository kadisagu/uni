package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.ui.PackageList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.support.IUISupport;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.process.*;
import org.tandemframework.core.tool.IdGen;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.TapSupportUtils;
import org.tandemframework.tapsupport.confirm.ClickButtonAction;
import org.tandemframework.tapsupport.confirm.ConfirmInfo;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uni.util.BackgroundProcessHolder;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.EcfOrgUnitManager;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.logic.EcfOrgUnitPackageDao;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.logic.IEcfOrgUnitPackageDao;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.ui.AddPkg4Admission.EcfOrgUnitAddPkg4Admission;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.ui.AddPkg4AdmissionOrders.EcfOrgUnitAddPkg4AdmissionOrders;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.ui.AddPkg4Applications.EcfOrgUnitAddPkg4Applications;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.ui.AddPkg4Compaign.EcfOrgUnitAddPkg4Compaign;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author vdanilov
 */
@State({
    @Bind(key = "orgUnitId", binding = "holder.id", required=true)
})
public class EcfOrgUnitPackageListUI extends UIPresenter {

    public static final String PARAM_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";

    private final OrgUnitHolder holder = new OrgUnitHolder();
    public OrgUnitHolder getHolder() { return this.holder; }
    public OrgUnit getOrgUnit() { return this.getHolder().getValue(); }

    public CommonPostfixPermissionModelBase getSecModel() { return this.getHolder().getSecModel(); }

    private EcfOrgUnit ecfOrgUnit;
    public EcfOrgUnit getEcfOrgUnit() { return this.ecfOrgUnit; }
    public void setEcfOrgUnit(EcfOrgUnit ecfOrgUnit) { this.ecfOrgUnit = ecfOrgUnit; }

    private IUIDataSource _ecfOrgUnitPackageDS;
    public IUIDataSource getEcfOrgUnitPackageDS() { return _ecfOrgUnitPackageDS; }

    @Override
    public void onComponentRefresh()
    {
        this.setEcfOrgUnit(
            EcfOrgUnitManager.instance().dao().getEcfOrgUnit(
                this.getHolder().refresh(OrgUnit.class)
            )
        );
        if (null == getEcfOrgUnit()) {
            throw new NullPointerException("no-ecf-org-unit: " + this.getOrgUnit().getId());
        }

        if (getSettings().get(PARAM_ENROLLMENT_CAMPAIGN) == null)
        {
            final List<Object> ecList = DataAccessServices.dao().getList(
                new DQLSelectBuilder().fromEntity(EnrollmentCampaign.class, "c").column(property("c"))
                .order(property(EnrollmentCampaign.id().fromAlias("c")), OrderDirection.desc)
            );

            getSettings().set(PARAM_ENROLLMENT_CAMPAIGN, !ecList.isEmpty() ? ecList.get(0) : null);
        }

        _ecfOrgUnitPackageDS = getConfig().getDataSource(EcfOrgUnitPackageList.ECF_ORG_UNIT_PACKAGE_DS);

    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        dataSource.put("ecfOrgUnit", getEcfOrgUnit());
        dataSource.put(PARAM_ENROLLMENT_CAMPAIGN, getSettings().get(PARAM_ENROLLMENT_CAMPAIGN));
    }

    public String getSubmitId(String action, Long id) {
        return "submit_"+action+"_"+id;
    }

    public void onClickDownload() {
        EcfOrgUnitPackage ecfOrgUnitPackage = DataAccessServices.dao().get(EcfOrgUnitPackage.class, getSupport().getListenerParameterAsLong());
        String filename = ecfOrgUnitPackage.getEnrollmentCampaign().getEducationYear().getIntValue()+"-"+Long.toString(ecfOrgUnitPackage.getId() >> IdGen.CODE_BITS, 32)+".xml";
        byte[] xmlPackageRequest = ecfOrgUnitPackage.getXmlPackageRequest();
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().document(xmlPackageRequest).xml().fileName(filename), true);
    }

    public void onClickQueue() {
        IUISupport support = getSupport();
        EcfOrgUnitPackage ecfOrgUnitPackage = DataAccessServices.dao().get(EcfOrgUnitPackage.class, support.getListenerParameterAsLong());
        IEcfOrgUnitPackageDao pkgDao = EcfOrgUnitManager.pkgDao(ecfOrgUnitPackage);

        if (!pkgDao.isFirstUnqueuedPackage(ecfOrgUnitPackage.getId())) {
            if (!"ok".equals(support.getClientParameter())) {
                TapSupportUtils.displayConfirm(
                    new ConfirmInfo(
                        "В списке пакетов есть необработанные записи, созданные раньше текущей. Помещение текущего пакета в очедерь приведет к безвозвратному исключению их из обработки. Поместить текущий пакет в очедерь?",
                        new ClickButtonAction(getSubmitId("queue", ecfOrgUnitPackage.getId()), "ok", false)
                    )
                );
                return;
            }
        }

        pkgDao.doQueue(ecfOrgUnitPackage.getId());
    }

    public void onClickArchive() {
        EcfOrgUnitPackage ecfOrgUnitPackage = DataAccessServices.dao().get(EcfOrgUnitPackage.class, getSupport().getListenerParameterAsLong());
        EcfOrgUnitManager.pkgDao(ecfOrgUnitPackage).doSwitchArchive(ecfOrgUnitPackage.getId(), true /*!ecfOrgUnitPackage.isArchived()*/);
    }

    public void onClickDelete() {
        EcfOrgUnitPackage ecfOrgUnitPackage = DataAccessServices.dao().get(EcfOrgUnitPackage.class, getSupport().getListenerParameterAsLong());
        EcfOrgUnitManager.pkgDao(ecfOrgUnitPackage).doDeletePackage(ecfOrgUnitPackage.getId());
    }


    public void onClickAddPkg4Compaign() {
        getActivationBuilder().asRegion(EcfOrgUnitAddPkg4Compaign.class).parameter(UIPresenter.PUBLISHER_ID, getEcfOrgUnit().getId()).activate();
    }

    public void onClickAddPkg4Admission() {
        getActivationBuilder().asRegion(EcfOrgUnitAddPkg4Admission.class).parameter(UIPresenter.PUBLISHER_ID, getEcfOrgUnit().getId()).activate();
    }

    public void onClickAddPkg4Applications() {
        getActivationBuilder().asRegion(EcfOrgUnitAddPkg4Applications.class).parameter(UIPresenter.PUBLISHER_ID, getEcfOrgUnit().getId()).activate();
    }

    public void onClickAddPkg4AdmissionOrders() {
        getActivationBuilder().asRegion(EcfOrgUnitAddPkg4AdmissionOrders.class).parameter(UIPresenter.PUBLISHER_ID, getEcfOrgUnit().getId()).activate();
    }

    public void onClickAddPkg4DelApplications() {
        final IBackgroundProcess process = new BackgroundProcessBase()
        {
            @Override
            public ProcessResult run(ProcessState state)
            {
                state.setCurrentValue(1);

                try
                {
                    EcfOrgUnitPackageDao.setupProcessState(state);
                    EcfOrgUnitManager.instance().pkgDelApplicationsDao().doCreatePackage(getEcfOrgUnit(), getSettings().<EnrollmentCampaign>get(PARAM_ENROLLMENT_CAMPAIGN));
                }
                catch (Exception e)
                {
                    //TODO когда будет логирование ошибок, нужно выводить общие слова
                    if (e instanceof ApplicationException)
                        return new ProcessResult(e);
                    else
                        throw e;
                }

                if (UserContext.getInstance().getErrorCollector().hasErrors())
                {
                    return new ProcessResult("Пакет на удаление всех заявлений не удалось сформировать.", true);
                }
                state.setCurrentValue(100);

                return new ProcessResult("Пакет на удаление всех заявлений успешно сформирован.");
            }
        };

        new BackgroundProcessHolder().start("Формирование пакета на удаление всех заявлений", process, ProcessDisplayMode.percent);
    }

    public void onClickAddPkg4DelAdmissionOrders() {
        final IBackgroundProcess process = new BackgroundProcessBase()
        {
            @Override
            public ProcessResult run(ProcessState state)
            {
                state.setCurrentValue(1);

                try
                {
                    EcfOrgUnitPackageDao.setupProcessState(state);
                    EcfOrgUnitManager.instance().pkgDelAdmissionOrdersDao().doCreatePackage(getEcfOrgUnit(), getSettings().<EnrollmentCampaign>get(PARAM_ENROLLMENT_CAMPAIGN));
                }
                catch (Exception e)
                {
                    //TODO когда будет логирование ошибок, нужно выводить общие слова
                    if (e instanceof ApplicationException)
                        return new ProcessResult(e);
                    else
                        throw e;
                }

                if (UserContext.getInstance().getErrorCollector().hasErrors())
                {
                    return new ProcessResult("Пакет на удаление всех заявлений из приказов не удалось сформировать.", true);
                }
                state.setCurrentValue(100);

                return new ProcessResult("Пакет на удаление всех заявлений из приказов успешно сформирован.");
            }
        };

        new BackgroundProcessHolder().start("Формирование пакета на удаление всех заявлений из приказов", process, ProcessDisplayMode.percent);
    }
}
