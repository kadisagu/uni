package ru.tandemservice.uniec_fis.base.bo.EcfConverter;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.fias.base.entity.AddressType;
import org.tandemframework.shared.fias.base.entity.codes.AddressLevelCodes;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.IEcfConverterDao;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.IEcfConverterStaticDao;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.benefit.EcfBenefitDao;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.benefit.EcfConverterDao4Benefit;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.benefit.IEcfBenefitDao;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.country.EcfConverterDao4AddressCountry;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.discipline.EcfConverterDao4EntranceDiscipline;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.discipline.EcfConverterDao4SetDiscipline;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.discipline.IEcfConverterDao4EntranceDiscipline;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.discipline.IEcfConverterDao4SetDiscipline;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.disciplineType.EcfConverterDao4DisciplineType;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.disciplineType.IEcfConverterDao4DisciplineType;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.ecg.EcfEcgDao;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.ecg.IEcfEcgDao;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.eduForm.EcfConverterDao4EduForm;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.eduForm.IEcfConverterDao4EduForm;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.eduOu.EcfConverterDao4EduOu;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.eduOu.IEcfConverterDao4EduOu;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.finSources.EcfFinSourceDao;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.finSources.IEcfFinSourceDao;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.indAchievement.EcfConverterDao4IndAchievement;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.indAchievement.IEcfConverterDao4IndAchievement;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.olympiad.EcfConverterDao4Olympiad;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.olympiad.IEcfConverterDao4Olympiad;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.olympiadType.EcfConverterDao4OlympiadType;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.olympiadType.IEcfConverterDao4OlympiadType;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.region.EcfConverterDao4Region;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.region.IEcfConverterDao4Region;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.stateExamSubject.EcfConverterDao4StateExamSubject;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.stateExamSubject.IEcfConverterDao4StateExamSubject;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.benefit.IEcfConverterDao4Benefit;

import java.util.Objects;

/**
 * @author vdanilov
 */
@Configuration
public class EcfConverterManager extends BusinessObjectManager {

    public static EcfConverterManager instance() {
        return instance(EcfConverterManager.class);
    }

    @Bean
    public IEcfConverterDao4EduOu educationOrgUnit() {
        return new EcfConverterDao4EduOu();
    }

    @Bean
    public IEcfConverterDao4SetDiscipline setDiscipline() {
        return new EcfConverterDao4SetDiscipline();
    }

    @Bean
    public IEcfConverterDao4EntranceDiscipline converterDao4EntranceDiscipline() {
        return new EcfConverterDao4EntranceDiscipline();
    }

    @Bean
    public IEcfConverterDao4Region converterDao4Region() {
        return new EcfConverterDao4Region();
    }

    @Bean
    public IEcfConverterStaticDao<AddressCountry> addressCountry() {
        return new EcfConverterDao4AddressCountry();
    }

    @Bean
    public IEcfConverterDao4EduForm educationForm() {
        return new EcfConverterDao4EduForm();
    }

    @Bean
    public IEcfFinSourceDao financingSource() {
        return new EcfFinSourceDao();
    }

    @Bean
    public IEcfEcgDao ecg() {
        return new EcfEcgDao();
    }

    @Bean
    public IEcfBenefitDao benefit()
    {
        return new EcfBenefitDao();
    }

    @Bean
    public IEcfConverterDao4DisciplineType disciplineType()
    {
        return new EcfConverterDao4DisciplineType();
    }

    @Bean
    public IEcfConverterDao4OlympiadType olympiadType()
    {
        return new EcfConverterDao4OlympiadType();
    }

    @Bean
    public IEcfConverterDao4Olympiad olympiad()
    {
        return new EcfConverterDao4Olympiad();
    }

    @Bean
    public IEcfConverterDao4IndAchievement individualAchievement()
    {
        return new EcfConverterDao4IndAchievement();
    }

    @Bean
    public IEcfConverterDao4StateExamSubject stateExamSubject()
    {
        return new EcfConverterDao4StateExamSubject();
    }

    @Bean
    public IEcfConverterDao4Benefit converterDao4Benefit() {
        return new EcfConverterDao4Benefit();
    }

    @SuppressWarnings("unchecked")
    public static <T extends IEntity> IEcfConverterDao<T> getConvetertDao(String converterName) {
        if (null == (converterName = StringUtils.trimToNull(converterName))) {
            throw new IllegalArgumentException("converterName is empty");
        }
        return instance().getMeta().getConfigContext().getBean("EcfConverter."+converterName, IEcfConverterDao.class);
    }

    /**
     * Справочник 41 - "Тип населенного пункта"
     *
     * @param addressItem адресс
     * @return ID типа населенного пункта в ФИС
     */
    public static Long getTownId(AddressItem addressItem)
    {
        if(addressItem == null)
        {
            return null;
        }

        AddressType addressType = addressItem.getAddressType();

        if(Objects.equals(AddressLevelCodes.REGION, addressType.getAddressLevel().getCode())
                && Objects.equals(103, addressType.getCode()))
        {
            return 1L;
        }
        else if(Objects.equals(AddressLevelCodes.SETTLEMENT, addressType.getAddressLevel().getCode())
                && Objects.equals(301, addressType.getCode()))
        {
            return 2L;
        }
        else if(!addressType.isCountryside())
        {
            return 3L;
        }
        else if(addressType.isCountryside())
        {
            return 4L;
        }

        return null;
    }
}
