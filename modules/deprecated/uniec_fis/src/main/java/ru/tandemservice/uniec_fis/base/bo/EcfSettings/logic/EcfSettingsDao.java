package ru.tandemservice.uniec_fis.base.bo.EcfSettings.logic;

import org.tandemframework.core.common.INaturalId;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dao.MergeAction;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.EcfConverterManager;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.IEcfConverter;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.eduOu.IEcfConverterDao4EduOu;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.finSources.IEcfFinSourceDao;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.EcfOrgUnitManager;
import ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodItem;
import ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.EcfSettingsECPeriodKey;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author vdanilov
 */
public class EcfSettingsDao extends UniBaseDao implements IEcfSettingsDao {


    @Override
    public List<EcfSettingsECPeriodKey> doCreateEcfSettingsECPeriodKeys(final EnrollmentCampaign ec, Collection<EnrollmentDirection> directions) {

        // check 1
        if (null == directions) {
            directions = EcfOrgUnitManager.instance().dao().getEnrollmentDirections(null, ec);
        }

        // check 2
        for (EnrollmentDirection d: directions) {
            if (!ec.equals(d.getEnrollmentCampaign())) {
                throw new IllegalStateException("∃ d ∊ directions | d.enrollmentCampaign ≠ ec");
            }
        }

        final IEcfConverterDao4EduOu eduOuDao = EcfConverterManager.instance().educationOrgUnit();
        final IEcfFinSourceDao finSourceDao = EcfConverterManager.instance().financingSource();
        final IEcfConverter<DevelopForm> eduFormConverter = EcfConverterManager.instance().educationForm().getConverter();

        final Map<INaturalId, EcfSettingsECPeriodKey> target = new HashMap<INaturalId, EcfSettingsECPeriodKey>();
        for (final EnrollmentDirection dir : directions) {

            final EducationOrgUnit eduOu = dir.getEducationOrgUnit();

            if (eduOu.getDevelopForm().getCode().equals(DevelopFormCodes.EXTERNAL_FORM) || eduOu.getDevelopForm().getCode().equals(DevelopFormCodes.APPLICANT_FORM)) {
                ContextLocal.getInfoCollector().add("Направление приема «" + dir.getTitle() + "» не было обработано (форма освоения " + eduOu.getDevelopForm().getTitle() + " не может быть передана в ФИС).");
                continue;
            }

            final EcfCatalogItem fisEduLvl = eduOuDao.getEcfItem4EducationLevel(eduOu);
            if (null == fisEduLvl) {
                ContextLocal.getInfoCollector().add("Направление приема «"+dir.getTitle()+"» не было обработано (не удалось определить уровень образования ФИС)");
                continue;
            }

            final EcfCatalogItem fisEduForm = eduFormConverter.getEcfCatalogItem(eduOu.getDevelopForm(), false);
            if (null == fisEduForm) {
                ContextLocal.getInfoCollector().add("Направление приема «"+dir.getTitle()+"» не было обработано (не удалось определить форму обучения ФИС)");
                continue;
            }

            final List<EcfCatalogItem> educationSourceList = finSourceDao.getSourceList(dir);
            if (educationSourceList.isEmpty()) {
                ContextLocal.getInfoCollector().add("Направление приема «"+dir.getTitle()+"» не было обработано (не удалось определить источники финансирования ФИС)");
                continue;
            }

            final Course course = dir.getEntranceCourse();

            for (final EcfCatalogItem educationSource: educationSourceList) {
                final EcfSettingsECPeriodKey key = new EcfSettingsECPeriodKey();
                key.setEnrollmentCampaign(ec);
                key.setEducationLevel(fisEduLvl);
                key.setEducationForm(fisEduForm);
                key.setEducationSource(educationSource);
                key.setCourse(course);
                target.put(key.getNaturalId(), key);
            }
        }

        return new MergeAction.SessionMergeAction<INaturalId, EcfSettingsECPeriodKey>() {
            @Override protected INaturalId key(final EcfSettingsECPeriodKey source) { return source.getNaturalId(); }
            @Override protected EcfSettingsECPeriodKey buildRow(final EcfSettingsECPeriodKey source) {
                final EcfSettingsECPeriodKey key = new EcfSettingsECPeriodKey(source);
                key.setUid(EcfOrgUnitManager.instance().uidRegistry().doGetUid(
                    key.getEnrollmentCampaign().getId()+"."+
                    key.getEducationForm().getFisItemCode()+"."+
                    key.getEducationLevel().getFisItemCode()+"."+
                    key.getEducationSource().getFisItemCode()+"."+
                    key.getCourse().getIntValue()
                ));
                return key;
            }
            @Override protected void fill(final EcfSettingsECPeriodKey target, final EcfSettingsECPeriodKey source) { /**/ }
            @Override protected boolean isDeleteable(EcfSettingsECPeriodKey target) { return false; }
        }.merge(this.getList(EcfSettingsECPeriodKey.class, EcfSettingsECPeriodKey.enrollmentCampaign(), ec), target.values());

    }


    @Override
    public void doSaveEcfSettingsECPeriodKey(EcfSettingsECPeriodKey key, List<EcfSettingsECPeriodItem> itemList)
    {
        if (key.getSize() != itemList.size()) {
            throw new IllegalStateException("key.size ≠ itemList.size");
        }

        for (int i=0; i<key.getSize(); i++) {
            EcfSettingsECPeriodItem item = itemList.get(i);
            if (!key.equals(item.getKey())) {
                throw new IllegalStateException("∃ i ∊ itemList | i.key ≠ key");
            }
            if (item.getStage() != (1+i)) {
                throw new IllegalStateException("∃ i ∊ N | itemList[i].stage ≠ 1+i");
            }
        }

        new MergeAction.SessionMergeAction<INaturalId, EcfSettingsECPeriodItem>() {
            @Override protected INaturalId key(final EcfSettingsECPeriodItem source) { return source.getNaturalId(); }
            @Override protected EcfSettingsECPeriodItem buildRow(final EcfSettingsECPeriodItem source) { return new EcfSettingsECPeriodItem(source); }
            @Override protected void fill(final EcfSettingsECPeriodItem target, final EcfSettingsECPeriodItem source) { target.update(source, false); }
        }.merge(this.getList(EcfSettingsECPeriodItem.class, EcfSettingsECPeriodItem.key(), key), itemList);

    }

    @Override
    public ErrorCollector doValidateEcfSettingsECPeriodKey(EcfSettingsECPeriodKey key, List<EcfSettingsECPeriodItem> itemList)
    {
        ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();

        for (EcfSettingsECPeriodItem item : itemList)
        {
            if (item.getDateEnd().getTime() < item.getDateStart().getTime())
                errorCollector.add("«Окончание приема» должна быть больше либо равна дате «Начало приема».", "date1_" + item.getId(), "date2_" + item.getId());
        }

        return errorCollector;
    }

}
