package ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.util.NumberAsStringComparator;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import ru.tandemservice.uniec_fis.base.bo.EcfSettings.entity.gen.EcfSettingsECPeriodKeyGen;

/**
 * Даты приемной кампании ФИС (ключ)
 */
public class EcfSettingsECPeriodKey extends EcfSettingsECPeriodKeyGen implements Comparable<EcfSettingsECPeriodKey>, ITitled
{

    public EcfSettingsECPeriodKey() {
    }

    public EcfSettingsECPeriodKey(EcfSettingsECPeriodKey source) {
        this.update(source, true);
    }

    @Override
    public int compareTo(EcfSettingsECPeriodKey o)
    {
        // если это одно и то же, то 0
        if (this.equals(o)) {
            return 0;
        }

        int result;

        // по курсу
        if (0 != (result = (this.getCourse().getIntValue() - o.getCourse().getIntValue()))) {
            return result;
        }

        // по коду ФИС формы обучения
        if (0 != (result = NumberAsStringComparator.INSTANCE.compare(this.getEducationForm().getFisItemCode(), o.getEducationForm().getFisItemCode()))) {
            return result;
        }

        // по коду ФИС уровня образования
        if (0 != (result = NumberAsStringComparator.INSTANCE.compare(this.getEducationLevel().getFisItemCode(), o.getEducationLevel().getFisItemCode()))) {
            return result;
        }

        // по коду ФИС источника финансирования
        if (0 != (result = NumberAsStringComparator.INSTANCE.compare(this.getEducationSource().getFisItemCode(), o.getEducationSource().getFisItemCode()))) {
            return result;
        }

        // по id
        return this.getId().compareTo(o.getId());
    }

    public static String getSizeTitle(int sz) {
        if (sz <= 0) { return "Нет приема"; }
        if (1 == sz) { return "Прием без этапов"; }
        return CommonBaseStringUtil.numberWithPostfixCase(sz, "этпа", "этапа", "этапов");
    }

    @Override
    @EntityDSLSupport
    public String getSizeTitle() {
        return getSizeTitle(getSize());
    }

    @Override
    @EntityDSLSupport
    public String getTitle() {
        if (getCourse() == null) {
            return this.getClass().getSimpleName();
        }
        return new StringBuilder()
        .append(getCourse().getTitle()).append(" курс, ")
        .append(getEducationForm().getTitle()).append(", ")
        .append(getEducationLevel().getTitle()).append(", ")
        .append(getEducationSource().getTitle())
        .toString();
    }


}