package ru.tandemservice.uniec_fis.util;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.mvel2.MVEL;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Date;

/**
 * @author vdanilov
 */
public class FisServiceImpl implements IFisService {

    public static byte[] toXml(final Object root) {
        try {
            final JAXBContext jc = JAXBContext.newInstance(root.getClass());
            final Marshaller m = jc.createMarshaller();
            m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            final ByteArrayOutputStream ba = new ByteArrayOutputStream(8192);
            m.marshal(root, ba);
            ba.flush();

            return ba.toByteArray();
        } catch (Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    public static <T> T fromXml(final Class<T> clazz, final byte[] xml) {
        try {
            final JAXBContext jc = JAXBContext.newInstance(clazz.getPackage().getName());
            final Unmarshaller u = jc.createUnmarshaller();
            final Object object = u.unmarshal(new ByteArrayInputStream(xml));

            // если это нужный класс, то его и возвращаем
            if (clazz.isInstance(object)) {
                return clazz.cast(object);
            }

            // если корневой элемент <error>
            // либо не декларирован в схеме (как это было в старом протоколе)
            // либо является ссылкой на тип TError (как сделано сейчас)
            if (object instanceof JAXBElement)
            {
                final JAXBElement element = ((JAXBElement)object);
                if ("error".equalsIgnoreCase(element.getName().getLocalPart()))
                {
                    Object value = element.getValue();

                    // пробуем распарсить terror внутри error
                    if ("TError".equalsIgnoreCase(value.getClass().getSimpleName())) {
                        throw new FisError(
                            value,
                            String.valueOf(MVEL.getProperty("errorCode", value)),
                            String.valueOf(MVEL.getProperty("errorText", value))
                        );
                    }
                }
            }

            // пришло хз что O_o
            throw new IllegalStateException(new String(xml));
        } catch (Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }


    private static final Logger log = Logger.getLogger(FisServiceImpl.class);

    private final String proxyHost = StringUtils.trimToNull(ApplicationRuntime.getProperty("uni.ec.fis.proxy.host"));
    public String getProxyHost() { return this.proxyHost; }

    private final String proxyPort = StringUtils.trimToNull(ApplicationRuntime.getProperty("uni.ec.fis.proxy.port"));
    public String getProxyPort() { return this.proxyPort; }

    private final String baseUrl = ApplicationRuntime.getProperty("uni.ec.fis.connection.url");
    public String getBaseUrl() { return this.baseUrl; }

    private final int timeout = parseTimeout();
    public int getTimeout() { return this.timeout; }

    public int parseTimeout() {
        final String timeoutStr = StringUtils.trimToNull(ApplicationRuntime.getProperty("uni.ec.fis.connection.timeout"));
        if (null == timeoutStr) { return 0; }
        return Math.max(Integer.parseInt(timeoutStr), 0);
    }

    public static String prettyFormat(final String xml) {
        try {
            final Document doc = DocumentHelper.parseText(xml);
            final StringWriter sw = new StringWriter();
            final OutputFormat format = OutputFormat.createPrettyPrint();
            final XMLWriter xw = new XMLWriter(sw, format);
            xw.write(doc);
            return sw.toString();
        } catch (final Throwable t) {
            // throw CoreExceptionUtils.getRuntimeException(t);
            // возвращать что-то надо, даже если были ошибки
            log.error(t.getMessage(), t);
        }

        // возвращать что-то надо, даже если были ошибки
        return xml;
    }

    public static String prettyFormat(final byte[] bytes) {
        return prettyFormat(new String(bytes));
    }

    @Override
    public FisConnection connect(final String urlPostfix) {
        return new FisConnection() {

            private String request_raw;
            @Override public String getRequestRaw() {
                return this.request_raw;
            }
            @Override public String getRequest() {
                return prettyFormat(this.getRequestRaw());
            }

            private String response_raw;
            @Override public String getResponseRaw() {
                return this.response_raw;
            }
            @Override public String getResponse() {
                return prettyFormat(this.getResponseRaw());
            }

            private URL url;
            @Override public URL getUrl() {
                return this.url;
            }

            private void write(final OutputStream os, byte[] byteArray) throws IOException
            {
                this.request_raw = new String(byteArray, "UTF-8");
                IOUtils.write(byteArray, os);
            }

            @SuppressWarnings("unchecked")
            private byte[] read(final InputStream is) throws IOException
            {
                return (this.response_raw = IOUtils.toString(is, "UTF-8")).getBytes();
            }

            private HttpURLConnection getConnection(final String urlPostfix) throws IOException
            {
                if (StringUtils.isEmpty(FisServiceImpl.this.getBaseUrl()))
                    throw new NotSpecifiedFisConnectionUrlException("Property 'uni.ec.fis.connection.url' is not specified.");

                this.url = new URL(FisServiceImpl.this.getBaseUrl()+"/"+urlPostfix);
                final HttpURLConnection conn = openConnection(this.url);
                int timeout = getTimeout();
                if (timeout > 0) {
                    conn.setConnectTimeout(timeout);
                    conn.setReadTimeout(10*timeout);
                }
                conn.setRequestMethod("POST");
                conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "text/xml");

                conn.connect();
                return conn;
            }

            protected HttpURLConnection openConnection(URL url) throws IOException
            {
                String proxyHost = getProxyHost();
                if (null != proxyHost) {
                    int proxyPort = Integer.parseInt(getProxyPort());
                    final Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyHost, proxyPort));
                    return (HttpURLConnection) url.openConnection(proxy);
                } else {
                    return (HttpURLConnection) url.openConnection();
                }
            }

            @Override
            public byte[] call(byte[] request) {
                try {
                    final HttpURLConnection conn = this.getConnection(urlPostfix);
                    try {

                        // отправляем туда данные (надо делать buffered - т.к. пихать по одному байту в сокет - зло)
                        final BufferedOutputStream bs = new BufferedOutputStream(conn.getOutputStream());
                        this.write(bs, request);
                        bs.flush();

                        // принимаем данные обратно
                        return this.read(conn.getInputStream());

                    } catch (Exception t) {

                        // формируем человекочитаемое сообщение об ошибке
                        Debug.exception(t.getMessage()+"\nrequest="+this.getRequest()+"\nresponse="+this.getResponse(), t);

                        // отправляем ошибку дальше
                        throw t;

                    } finally {

                        // соединение надо закрыть
                        conn.disconnect();
                    }
                } catch (final Throwable t) {
                    throw CoreExceptionUtils.getRuntimeException(t);
                }
            }

            @Override
            public <T> T call(Class<T> responseKlass, byte[] request) {
                return fromXml(responseKlass, call(request));
            }

            @Override public <T> T call(final Class<T> responseKlass, final Object root) {
                return call(responseKlass, toXml(root));
            }

            @Override
            public void writeDebugToLogFile(final String postfix) {
                try {
                    final String timestamp = new DateFormatter("yyyyMMdd-HHmmss").format(new Date());
                    final File file = new File(Paths.get(System.getProperty("catalina.base"), "logs", "debug-"+timestamp+"-FisServiceImpl-"+postfix+".io.xml").toString());
                    if (!file.createNewFile()) {
                        throw new IllegalStateException();
                    }

                    final FileOutputStream ofs = new FileOutputStream(file);
                    final BufferedOutputStream bfs = new BufferedOutputStream(ofs);
                    IOUtils.write(this.getRequestRaw(), bfs);
                    IOUtils.write("\n", bfs);
                    IOUtils.write(this.getResponseRaw(), bfs);
                    IOUtils.write("\n", bfs);
                    bfs.close();

                } catch (final Throwable t) {
                    throw CoreExceptionUtils.getRuntimeException(t);
                }
            }

        };
    }


}
