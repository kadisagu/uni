package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage4ApplicationsInfo;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Пакет: Заявления абитуриентов
 *
 * Root / PackageData / Applications
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EcfOrgUnitPackage4ApplicationsInfoGen extends EcfOrgUnitPackage
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage4ApplicationsInfo";
    public static final String ENTITY_NAME = "ecfOrgUnitPackage4ApplicationsInfo";
    public static final int VERSION_HASH = 2090922432;
    private static IEntityMeta ENTITY_META;

    public static final String P_ENTRANT_REQUEST_REG_DATE = "entrantRequestRegDate";
    public static final String L_STATUS = "status";

    private Date _entrantRequestRegDate;     // Дата добавления заявлений Uni
    private EcfCatalogItem _status;     // Статус Заявлений ФИС

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата добавления заявлений Uni. Свойство не может быть null.
     */
    @NotNull
    public Date getEntrantRequestRegDate()
    {
        return _entrantRequestRegDate;
    }

    /**
     * @param entrantRequestRegDate Дата добавления заявлений Uni. Свойство не может быть null.
     */
    public void setEntrantRequestRegDate(Date entrantRequestRegDate)
    {
        dirty(_entrantRequestRegDate, entrantRequestRegDate);
        _entrantRequestRegDate = entrantRequestRegDate;
    }

    /**
     * Статус Заявлений в ФИС, определяется пользователем в момент выгрузки
     *
     * @return Статус Заявлений ФИС. Свойство не может быть null.
     */
    @NotNull
    public EcfCatalogItem getStatus()
    {
        return _status;
    }

    /**
     * @param status Статус Заявлений ФИС. Свойство не может быть null.
     */
    public void setStatus(EcfCatalogItem status)
    {
        dirty(_status, status);
        _status = status;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EcfOrgUnitPackage4ApplicationsInfoGen)
        {
            setEntrantRequestRegDate(((EcfOrgUnitPackage4ApplicationsInfo)another).getEntrantRequestRegDate());
            setStatus(((EcfOrgUnitPackage4ApplicationsInfo)another).getStatus());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EcfOrgUnitPackage4ApplicationsInfoGen> extends EcfOrgUnitPackage.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EcfOrgUnitPackage4ApplicationsInfo.class;
        }

        public T newInstance()
        {
            return (T) new EcfOrgUnitPackage4ApplicationsInfo();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "entrantRequestRegDate":
                    return obj.getEntrantRequestRegDate();
                case "status":
                    return obj.getStatus();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "entrantRequestRegDate":
                    obj.setEntrantRequestRegDate((Date) value);
                    return;
                case "status":
                    obj.setStatus((EcfCatalogItem) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "entrantRequestRegDate":
                        return true;
                case "status":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "entrantRequestRegDate":
                    return true;
                case "status":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "entrantRequestRegDate":
                    return Date.class;
                case "status":
                    return EcfCatalogItem.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EcfOrgUnitPackage4ApplicationsInfo> _dslPath = new Path<EcfOrgUnitPackage4ApplicationsInfo>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EcfOrgUnitPackage4ApplicationsInfo");
    }
            

    /**
     * @return Дата добавления заявлений Uni. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage4ApplicationsInfo#getEntrantRequestRegDate()
     */
    public static PropertyPath<Date> entrantRequestRegDate()
    {
        return _dslPath.entrantRequestRegDate();
    }

    /**
     * Статус Заявлений в ФИС, определяется пользователем в момент выгрузки
     *
     * @return Статус Заявлений ФИС. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage4ApplicationsInfo#getStatus()
     */
    public static EcfCatalogItem.Path<EcfCatalogItem> status()
    {
        return _dslPath.status();
    }

    public static class Path<E extends EcfOrgUnitPackage4ApplicationsInfo> extends EcfOrgUnitPackage.Path<E>
    {
        private PropertyPath<Date> _entrantRequestRegDate;
        private EcfCatalogItem.Path<EcfCatalogItem> _status;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата добавления заявлений Uni. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage4ApplicationsInfo#getEntrantRequestRegDate()
     */
        public PropertyPath<Date> entrantRequestRegDate()
        {
            if(_entrantRequestRegDate == null )
                _entrantRequestRegDate = new PropertyPath<Date>(EcfOrgUnitPackage4ApplicationsInfoGen.P_ENTRANT_REQUEST_REG_DATE, this);
            return _entrantRequestRegDate;
        }

    /**
     * Статус Заявлений в ФИС, определяется пользователем в момент выгрузки
     *
     * @return Статус Заявлений ФИС. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage4ApplicationsInfo#getStatus()
     */
        public EcfCatalogItem.Path<EcfCatalogItem> status()
        {
            if(_status == null )
                _status = new EcfCatalogItem.Path<EcfCatalogItem>(L_STATUS, this);
            return _status;
        }

        public Class getEntityClass()
        {
            return EcfOrgUnitPackage4ApplicationsInfo.class;
        }

        public String getEntityName()
        {
            return "ecfOrgUnitPackage4ApplicationsInfo";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
