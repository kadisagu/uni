package ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Запись справочника ФИС
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EcfCatalogItemGen extends EntityBase
 implements INaturalIdentifiable<EcfCatalogItemGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem";
    public static final String ENTITY_NAME = "ecfCatalogItem";
    public static final int VERSION_HASH = -411708795;
    private static IEntityMeta ENTITY_META;

    public static final String P_FIS_CATALOG_CODE = "fisCatalogCode";
    public static final String P_FIS_ITEM_CODE = "fisItemCode";
    public static final String P_REMOVAL_DATE = "removalDate";
    public static final String P_TITLE = "title";

    private String _fisCatalogCode;     // Код справочника ФИС
    private String _fisItemCode;     // Код элемента справочника ФИС
    private Date _removalDate;     // Дата утраты актуальности
    private String _title;     // Название элемента

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Код справочника ФИС. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFisCatalogCode()
    {
        return _fisCatalogCode;
    }

    /**
     * @param fisCatalogCode Код справочника ФИС. Свойство не может быть null.
     */
    public void setFisCatalogCode(String fisCatalogCode)
    {
        dirty(_fisCatalogCode, fisCatalogCode);
        _fisCatalogCode = fisCatalogCode;
    }

    /**
     * @return Код элемента справочника ФИС. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFisItemCode()
    {
        return _fisItemCode;
    }

    /**
     * @param fisItemCode Код элемента справочника ФИС. Свойство не может быть null.
     */
    public void setFisItemCode(String fisItemCode)
    {
        dirty(_fisItemCode, fisItemCode);
        _fisItemCode = fisItemCode;
    }

    /**
     * @return Дата утраты актуальности.
     */
    public Date getRemovalDate()
    {
        return _removalDate;
    }

    /**
     * @param removalDate Дата утраты актуальности.
     */
    public void setRemovalDate(Date removalDate)
    {
        dirty(_removalDate, removalDate);
        _removalDate = removalDate;
    }

    /**
     * @return Название элемента. Свойство не может быть null.
     */
    @NotNull
    @Length(max=1023)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название элемента. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EcfCatalogItemGen)
        {
            if (withNaturalIdProperties)
            {
                setFisCatalogCode(((EcfCatalogItem)another).getFisCatalogCode());
                setFisItemCode(((EcfCatalogItem)another).getFisItemCode());
            }
            setRemovalDate(((EcfCatalogItem)another).getRemovalDate());
            setTitle(((EcfCatalogItem)another).getTitle());
        }
    }

    public INaturalId<EcfCatalogItemGen> getNaturalId()
    {
        return new NaturalId(getFisCatalogCode(), getFisItemCode());
    }

    public static class NaturalId extends NaturalIdBase<EcfCatalogItemGen>
    {
        private static final String PROXY_NAME = "EcfCatalogItemNaturalProxy";

        private String _fisCatalogCode;
        private String _fisItemCode;

        public NaturalId()
        {}

        public NaturalId(String fisCatalogCode, String fisItemCode)
        {
            _fisCatalogCode = fisCatalogCode;
            _fisItemCode = fisItemCode;
        }

        public String getFisCatalogCode()
        {
            return _fisCatalogCode;
        }

        public void setFisCatalogCode(String fisCatalogCode)
        {
            _fisCatalogCode = fisCatalogCode;
        }

        public String getFisItemCode()
        {
            return _fisItemCode;
        }

        public void setFisItemCode(String fisItemCode)
        {
            _fisItemCode = fisItemCode;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EcfCatalogItemGen.NaturalId) ) return false;

            EcfCatalogItemGen.NaturalId that = (NaturalId) o;

            if( !equals(getFisCatalogCode(), that.getFisCatalogCode()) ) return false;
            if( !equals(getFisItemCode(), that.getFisItemCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getFisCatalogCode());
            result = hashCode(result, getFisItemCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getFisCatalogCode());
            sb.append("/");
            sb.append(getFisItemCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EcfCatalogItemGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EcfCatalogItem.class;
        }

        public T newInstance()
        {
            return (T) new EcfCatalogItem();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "fisCatalogCode":
                    return obj.getFisCatalogCode();
                case "fisItemCode":
                    return obj.getFisItemCode();
                case "removalDate":
                    return obj.getRemovalDate();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "fisCatalogCode":
                    obj.setFisCatalogCode((String) value);
                    return;
                case "fisItemCode":
                    obj.setFisItemCode((String) value);
                    return;
                case "removalDate":
                    obj.setRemovalDate((Date) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "fisCatalogCode":
                        return true;
                case "fisItemCode":
                        return true;
                case "removalDate":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "fisCatalogCode":
                    return true;
                case "fisItemCode":
                    return true;
                case "removalDate":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "fisCatalogCode":
                    return String.class;
                case "fisItemCode":
                    return String.class;
                case "removalDate":
                    return Date.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EcfCatalogItem> _dslPath = new Path<EcfCatalogItem>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EcfCatalogItem");
    }
            

    /**
     * @return Код справочника ФИС. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem#getFisCatalogCode()
     */
    public static PropertyPath<String> fisCatalogCode()
    {
        return _dslPath.fisCatalogCode();
    }

    /**
     * @return Код элемента справочника ФИС. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem#getFisItemCode()
     */
    public static PropertyPath<String> fisItemCode()
    {
        return _dslPath.fisItemCode();
    }

    /**
     * @return Дата утраты актуальности.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem#getRemovalDate()
     */
    public static PropertyPath<Date> removalDate()
    {
        return _dslPath.removalDate();
    }

    /**
     * @return Название элемента. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EcfCatalogItem> extends EntityPath<E>
    {
        private PropertyPath<String> _fisCatalogCode;
        private PropertyPath<String> _fisItemCode;
        private PropertyPath<Date> _removalDate;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Код справочника ФИС. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem#getFisCatalogCode()
     */
        public PropertyPath<String> fisCatalogCode()
        {
            if(_fisCatalogCode == null )
                _fisCatalogCode = new PropertyPath<String>(EcfCatalogItemGen.P_FIS_CATALOG_CODE, this);
            return _fisCatalogCode;
        }

    /**
     * @return Код элемента справочника ФИС. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem#getFisItemCode()
     */
        public PropertyPath<String> fisItemCode()
        {
            if(_fisItemCode == null )
                _fisItemCode = new PropertyPath<String>(EcfCatalogItemGen.P_FIS_ITEM_CODE, this);
            return _fisItemCode;
        }

    /**
     * @return Дата утраты актуальности.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem#getRemovalDate()
     */
        public PropertyPath<Date> removalDate()
        {
            if(_removalDate == null )
                _removalDate = new PropertyPath<Date>(EcfCatalogItemGen.P_REMOVAL_DATE, this);
            return _removalDate;
        }

    /**
     * @return Название элемента. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EcfCatalogItemGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EcfCatalogItem.class;
        }

        public String getEntityName()
        {
            return "ecfCatalogItem";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
