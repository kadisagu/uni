package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackageIOLog;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Пакет
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EcfOrgUnitPackageGen extends EntityBase
 implements ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage";
    public static final String ENTITY_NAME = "ecfOrgUnitPackage";
    public static final int VERSION_HASH = -282657235;
    private static IEntityMeta ENTITY_META;

    public static final String L_ECF_ORG_UNIT = "ecfOrgUnit";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_COMMENT = "comment";
    public static final String P_CREATION_DATE = "creationDate";
    public static final String P_QUEUE_DATE = "queueDate";
    public static final String P_ARCHIVE_DATE = "archiveDate";
    public static final String P_XML_PACKAGE = "xmlPackage";
    public static final String P_ECF_PACKAGE_I_D = "ecfPackageID";
    public static final String L_PKG_IMPORT_SUCCESS = "pkgImportSuccess";
    public static final String L_PKG_RESULT_SUCCESS = "pkgResultSuccess";
    public static final String P_ALIVE = "alive";
    public static final String P_ARCHIVE_DISABLED = "archiveDisabled";
    public static final String P_ARCHIVED = "archived";
    public static final String P_DELETE_DISABLED = "deleteDisabled";
    public static final String P_PACKAGE_REQUEST_DATE = "packageRequestDate";
    public static final String P_PACKAGE_RESULT_DATE = "packageResultDate";
    public static final String P_QUEUE_DISABLED = "queueDisabled";
    public static final String P_QUEUED = "queued";
    public static final String P_TITLE = "title";

    private EcfOrgUnit _ecfOrgUnit;     // Подразделение
    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private String _comment;     // Комментарий
    private Date _creationDate;     // Дата формирования пакета
    private Date _queueDate;     // Дата постановки пакета в очередь
    private Date _archiveDate;     // Дата помещения пакета в архив
    private byte[] _xmlPackage;     // XML: Пакет
    private Long _ecfPackageID;     // Идентификатор пакета в ФИС
    private EcfOrgUnitPackageIOLog _pkgImportSuccess;     // Успешная операция передачи данных пакета в ФИС
    private EcfOrgUnitPackageIOLog _pkgResultSuccess;     // Успешная операция получения результатов обработки из ФИС

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * Подразделение ФИС для которого формируется пакет
     *
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public EcfOrgUnit getEcfOrgUnit()
    {
        return _ecfOrgUnit;
    }

    /**
     * @param ecfOrgUnit Подразделение. Свойство не может быть null.
     */
    public void setEcfOrgUnit(EcfOrgUnit ecfOrgUnit)
    {
        dirty(_ecfOrgUnit, ecfOrgUnit);
        _ecfOrgUnit = ecfOrgUnit;
    }

    /**
     * Приемная кампания для которой требуется выгружается пакет
     *
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * Описание пакета для отображения в интерфейсе (генерируется при создании)
     *
     * @return Комментарий.
     */
    public String getComment()
    {
        initLazyForGet("comment");
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        initLazyForSet("comment");
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * Показывает, когда пакет был сформирован в системе
     *
     * @return Дата формирования пакета. Свойство не может быть null.
     */
    @NotNull
    public Date getCreationDate()
    {
        return _creationDate;
    }

    /**
     * @param creationDate Дата формирования пакета. Свойство не может быть null.
     */
    public void setCreationDate(Date creationDate)
    {
        dirty(_creationDate, creationDate);
        _creationDate = creationDate;
    }

    /**
     * Показывает, когда пакет был добавлен в очередь (с этого момента начинаются операции с пакетом)
     *
     * @return Дата постановки пакета в очередь.
     */
    public Date getQueueDate()
    {
        return _queueDate;
    }

    /**
     * @param queueDate Дата постановки пакета в очередь.
     */
    public void setQueueDate(Date queueDate)
    {
        dirty(_queueDate, queueDate);
        _queueDate = queueDate;
    }

    /**
     * Показывает, когда пакет был перемещен в архив (остановлены операции с пакетом)
     *
     * @return Дата помещения пакета в архив.
     */
    public Date getArchiveDate()
    {
        return _archiveDate;
    }

    /**
     * @param archiveDate Дата помещения пакета в архив.
     */
    public void setArchiveDate(Date archiveDate)
    {
        dirty(_archiveDate, archiveDate);
        _archiveDate = archiveDate;
    }

    /**
     * Содержимое пакета
     *
     * @return XML: Пакет. Свойство не может быть null.
     */
    @NotNull
    public byte[] getXmlPackage()
    {
        initLazyForGet("xmlPackage");
        return _xmlPackage;
    }

    /**
     * @param xmlPackage XML: Пакет. Свойство не может быть null.
     */
    public void setXmlPackage(byte[] xmlPackage)
    {
        initLazyForSet("xmlPackage");
        dirty(_xmlPackage, xmlPackage);
        _xmlPackage = xmlPackage;
    }

    /**
     * Идентификатор, который выдается ФИС для загруженного пакета
     * (заполняется на основе данных в pkgImportSuccess)
     *
     * @return Идентификатор пакета в ФИС.
     */
    public Long getEcfPackageID()
    {
        return _ecfPackageID;
    }

    /**
     * @param ecfPackageID Идентификатор пакета в ФИС.
     */
    public void setEcfPackageID(Long ecfPackageID)
    {
        dirty(_ecfPackageID, ecfPackageID);
        _ecfPackageID = ecfPackageID;
    }

    /**
     * Ссылка на удачную операцию отправки данных в фИС
     * (наличине этого признака указывает на факт передачи данных в ФИС)
     *
     * @return Успешная операция передачи данных пакета в ФИС.
     */
    public EcfOrgUnitPackageIOLog getPkgImportSuccess()
    {
        return _pkgImportSuccess;
    }

    /**
     * @param pkgImportSuccess Успешная операция передачи данных пакета в ФИС.
     */
    public void setPkgImportSuccess(EcfOrgUnitPackageIOLog pkgImportSuccess)
    {
        dirty(_pkgImportSuccess, pkgImportSuccess);
        _pkgImportSuccess = pkgImportSuccess;
    }

    /**
     * Ссылка на удачную операцию отправки данных в фИС
     * (наличие этого признака указывает на факт получения резулльтатов обработки пакета в ФИС)
     *
     * @return Успешная операция получения результатов обработки из ФИС.
     */
    public EcfOrgUnitPackageIOLog getPkgResultSuccess()
    {
        return _pkgResultSuccess;
    }

    /**
     * @param pkgResultSuccess Успешная операция получения результатов обработки из ФИС.
     */
    public void setPkgResultSuccess(EcfOrgUnitPackageIOLog pkgResultSuccess)
    {
        dirty(_pkgResultSuccess, pkgResultSuccess);
        _pkgResultSuccess = pkgResultSuccess;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EcfOrgUnitPackageGen)
        {
            setEcfOrgUnit(((EcfOrgUnitPackage)another).getEcfOrgUnit());
            setEnrollmentCampaign(((EcfOrgUnitPackage)another).getEnrollmentCampaign());
            setComment(((EcfOrgUnitPackage)another).getComment());
            setCreationDate(((EcfOrgUnitPackage)another).getCreationDate());
            setQueueDate(((EcfOrgUnitPackage)another).getQueueDate());
            setArchiveDate(((EcfOrgUnitPackage)another).getArchiveDate());
            setXmlPackage(((EcfOrgUnitPackage)another).getXmlPackage());
            setEcfPackageID(((EcfOrgUnitPackage)another).getEcfPackageID());
            setPkgImportSuccess(((EcfOrgUnitPackage)another).getPkgImportSuccess());
            setPkgResultSuccess(((EcfOrgUnitPackage)another).getPkgResultSuccess());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EcfOrgUnitPackageGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EcfOrgUnitPackage.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EcfOrgUnitPackage is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "ecfOrgUnit":
                    return obj.getEcfOrgUnit();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "comment":
                    return obj.getComment();
                case "creationDate":
                    return obj.getCreationDate();
                case "queueDate":
                    return obj.getQueueDate();
                case "archiveDate":
                    return obj.getArchiveDate();
                case "xmlPackage":
                    return obj.getXmlPackage();
                case "ecfPackageID":
                    return obj.getEcfPackageID();
                case "pkgImportSuccess":
                    return obj.getPkgImportSuccess();
                case "pkgResultSuccess":
                    return obj.getPkgResultSuccess();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "ecfOrgUnit":
                    obj.setEcfOrgUnit((EcfOrgUnit) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "creationDate":
                    obj.setCreationDate((Date) value);
                    return;
                case "queueDate":
                    obj.setQueueDate((Date) value);
                    return;
                case "archiveDate":
                    obj.setArchiveDate((Date) value);
                    return;
                case "xmlPackage":
                    obj.setXmlPackage((byte[]) value);
                    return;
                case "ecfPackageID":
                    obj.setEcfPackageID((Long) value);
                    return;
                case "pkgImportSuccess":
                    obj.setPkgImportSuccess((EcfOrgUnitPackageIOLog) value);
                    return;
                case "pkgResultSuccess":
                    obj.setPkgResultSuccess((EcfOrgUnitPackageIOLog) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "ecfOrgUnit":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "comment":
                        return true;
                case "creationDate":
                        return true;
                case "queueDate":
                        return true;
                case "archiveDate":
                        return true;
                case "xmlPackage":
                        return true;
                case "ecfPackageID":
                        return true;
                case "pkgImportSuccess":
                        return true;
                case "pkgResultSuccess":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "ecfOrgUnit":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "comment":
                    return true;
                case "creationDate":
                    return true;
                case "queueDate":
                    return true;
                case "archiveDate":
                    return true;
                case "xmlPackage":
                    return true;
                case "ecfPackageID":
                    return true;
                case "pkgImportSuccess":
                    return true;
                case "pkgResultSuccess":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "ecfOrgUnit":
                    return EcfOrgUnit.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "comment":
                    return String.class;
                case "creationDate":
                    return Date.class;
                case "queueDate":
                    return Date.class;
                case "archiveDate":
                    return Date.class;
                case "xmlPackage":
                    return byte[].class;
                case "ecfPackageID":
                    return Long.class;
                case "pkgImportSuccess":
                    return EcfOrgUnitPackageIOLog.class;
                case "pkgResultSuccess":
                    return EcfOrgUnitPackageIOLog.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EcfOrgUnitPackage> _dslPath = new Path<EcfOrgUnitPackage>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EcfOrgUnitPackage");
    }
            

    /**
     * Подразделение ФИС для которого формируется пакет
     *
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#getEcfOrgUnit()
     */
    public static EcfOrgUnit.Path<EcfOrgUnit> ecfOrgUnit()
    {
        return _dslPath.ecfOrgUnit();
    }

    /**
     * Приемная кампания для которой требуется выгружается пакет
     *
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * Описание пакета для отображения в интерфейсе (генерируется при создании)
     *
     * @return Комментарий.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * Показывает, когда пакет был сформирован в системе
     *
     * @return Дата формирования пакета. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#getCreationDate()
     */
    public static PropertyPath<Date> creationDate()
    {
        return _dslPath.creationDate();
    }

    /**
     * Показывает, когда пакет был добавлен в очередь (с этого момента начинаются операции с пакетом)
     *
     * @return Дата постановки пакета в очередь.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#getQueueDate()
     */
    public static PropertyPath<Date> queueDate()
    {
        return _dslPath.queueDate();
    }

    /**
     * Показывает, когда пакет был перемещен в архив (остановлены операции с пакетом)
     *
     * @return Дата помещения пакета в архив.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#getArchiveDate()
     */
    public static PropertyPath<Date> archiveDate()
    {
        return _dslPath.archiveDate();
    }

    /**
     * Содержимое пакета
     *
     * @return XML: Пакет. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#getXmlPackage()
     */
    public static PropertyPath<byte[]> xmlPackage()
    {
        return _dslPath.xmlPackage();
    }

    /**
     * Идентификатор, который выдается ФИС для загруженного пакета
     * (заполняется на основе данных в pkgImportSuccess)
     *
     * @return Идентификатор пакета в ФИС.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#getEcfPackageID()
     */
    public static PropertyPath<Long> ecfPackageID()
    {
        return _dslPath.ecfPackageID();
    }

    /**
     * Ссылка на удачную операцию отправки данных в фИС
     * (наличине этого признака указывает на факт передачи данных в ФИС)
     *
     * @return Успешная операция передачи данных пакета в ФИС.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#getPkgImportSuccess()
     */
    public static EcfOrgUnitPackageIOLog.Path<EcfOrgUnitPackageIOLog> pkgImportSuccess()
    {
        return _dslPath.pkgImportSuccess();
    }

    /**
     * Ссылка на удачную операцию отправки данных в фИС
     * (наличие этого признака указывает на факт получения резулльтатов обработки пакета в ФИС)
     *
     * @return Успешная операция получения результатов обработки из ФИС.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#getPkgResultSuccess()
     */
    public static EcfOrgUnitPackageIOLog.Path<EcfOrgUnitPackageIOLog> pkgResultSuccess()
    {
        return _dslPath.pkgResultSuccess();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#isAlive()
     */
    public static SupportedPropertyPath<Boolean> alive()
    {
        return _dslPath.alive();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#isArchiveDisabled()
     */
    public static SupportedPropertyPath<Boolean> archiveDisabled()
    {
        return _dslPath.archiveDisabled();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#isArchived()
     */
    public static SupportedPropertyPath<Boolean> archived()
    {
        return _dslPath.archived();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#isDeleteDisabled()
     */
    public static SupportedPropertyPath<Boolean> deleteDisabled()
    {
        return _dslPath.deleteDisabled();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#getPackageRequestDate()
     */
    public static SupportedPropertyPath<Date> packageRequestDate()
    {
        return _dslPath.packageRequestDate();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#getPackageResultDate()
     */
    public static SupportedPropertyPath<Date> packageResultDate()
    {
        return _dslPath.packageResultDate();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#isQueueDisabled()
     */
    public static SupportedPropertyPath<Boolean> queueDisabled()
    {
        return _dslPath.queueDisabled();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#isQueued()
     */
    public static SupportedPropertyPath<Boolean> queued()
    {
        return _dslPath.queued();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EcfOrgUnitPackage> extends EntityPath<E>
    {
        private EcfOrgUnit.Path<EcfOrgUnit> _ecfOrgUnit;
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<String> _comment;
        private PropertyPath<Date> _creationDate;
        private PropertyPath<Date> _queueDate;
        private PropertyPath<Date> _archiveDate;
        private PropertyPath<byte[]> _xmlPackage;
        private PropertyPath<Long> _ecfPackageID;
        private EcfOrgUnitPackageIOLog.Path<EcfOrgUnitPackageIOLog> _pkgImportSuccess;
        private EcfOrgUnitPackageIOLog.Path<EcfOrgUnitPackageIOLog> _pkgResultSuccess;
        private SupportedPropertyPath<Boolean> _alive;
        private SupportedPropertyPath<Boolean> _archiveDisabled;
        private SupportedPropertyPath<Boolean> _archived;
        private SupportedPropertyPath<Boolean> _deleteDisabled;
        private SupportedPropertyPath<Date> _packageRequestDate;
        private SupportedPropertyPath<Date> _packageResultDate;
        private SupportedPropertyPath<Boolean> _queueDisabled;
        private SupportedPropertyPath<Boolean> _queued;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * Подразделение ФИС для которого формируется пакет
     *
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#getEcfOrgUnit()
     */
        public EcfOrgUnit.Path<EcfOrgUnit> ecfOrgUnit()
        {
            if(_ecfOrgUnit == null )
                _ecfOrgUnit = new EcfOrgUnit.Path<EcfOrgUnit>(L_ECF_ORG_UNIT, this);
            return _ecfOrgUnit;
        }

    /**
     * Приемная кампания для которой требуется выгружается пакет
     *
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * Описание пакета для отображения в интерфейсе (генерируется при создании)
     *
     * @return Комментарий.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(EcfOrgUnitPackageGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * Показывает, когда пакет был сформирован в системе
     *
     * @return Дата формирования пакета. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#getCreationDate()
     */
        public PropertyPath<Date> creationDate()
        {
            if(_creationDate == null )
                _creationDate = new PropertyPath<Date>(EcfOrgUnitPackageGen.P_CREATION_DATE, this);
            return _creationDate;
        }

    /**
     * Показывает, когда пакет был добавлен в очередь (с этого момента начинаются операции с пакетом)
     *
     * @return Дата постановки пакета в очередь.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#getQueueDate()
     */
        public PropertyPath<Date> queueDate()
        {
            if(_queueDate == null )
                _queueDate = new PropertyPath<Date>(EcfOrgUnitPackageGen.P_QUEUE_DATE, this);
            return _queueDate;
        }

    /**
     * Показывает, когда пакет был перемещен в архив (остановлены операции с пакетом)
     *
     * @return Дата помещения пакета в архив.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#getArchiveDate()
     */
        public PropertyPath<Date> archiveDate()
        {
            if(_archiveDate == null )
                _archiveDate = new PropertyPath<Date>(EcfOrgUnitPackageGen.P_ARCHIVE_DATE, this);
            return _archiveDate;
        }

    /**
     * Содержимое пакета
     *
     * @return XML: Пакет. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#getXmlPackage()
     */
        public PropertyPath<byte[]> xmlPackage()
        {
            if(_xmlPackage == null )
                _xmlPackage = new PropertyPath<byte[]>(EcfOrgUnitPackageGen.P_XML_PACKAGE, this);
            return _xmlPackage;
        }

    /**
     * Идентификатор, который выдается ФИС для загруженного пакета
     * (заполняется на основе данных в pkgImportSuccess)
     *
     * @return Идентификатор пакета в ФИС.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#getEcfPackageID()
     */
        public PropertyPath<Long> ecfPackageID()
        {
            if(_ecfPackageID == null )
                _ecfPackageID = new PropertyPath<Long>(EcfOrgUnitPackageGen.P_ECF_PACKAGE_I_D, this);
            return _ecfPackageID;
        }

    /**
     * Ссылка на удачную операцию отправки данных в фИС
     * (наличине этого признака указывает на факт передачи данных в ФИС)
     *
     * @return Успешная операция передачи данных пакета в ФИС.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#getPkgImportSuccess()
     */
        public EcfOrgUnitPackageIOLog.Path<EcfOrgUnitPackageIOLog> pkgImportSuccess()
        {
            if(_pkgImportSuccess == null )
                _pkgImportSuccess = new EcfOrgUnitPackageIOLog.Path<EcfOrgUnitPackageIOLog>(L_PKG_IMPORT_SUCCESS, this);
            return _pkgImportSuccess;
        }

    /**
     * Ссылка на удачную операцию отправки данных в фИС
     * (наличие этого признака указывает на факт получения резулльтатов обработки пакета в ФИС)
     *
     * @return Успешная операция получения результатов обработки из ФИС.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#getPkgResultSuccess()
     */
        public EcfOrgUnitPackageIOLog.Path<EcfOrgUnitPackageIOLog> pkgResultSuccess()
        {
            if(_pkgResultSuccess == null )
                _pkgResultSuccess = new EcfOrgUnitPackageIOLog.Path<EcfOrgUnitPackageIOLog>(L_PKG_RESULT_SUCCESS, this);
            return _pkgResultSuccess;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#isAlive()
     */
        public SupportedPropertyPath<Boolean> alive()
        {
            if(_alive == null )
                _alive = new SupportedPropertyPath<Boolean>(EcfOrgUnitPackageGen.P_ALIVE, this);
            return _alive;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#isArchiveDisabled()
     */
        public SupportedPropertyPath<Boolean> archiveDisabled()
        {
            if(_archiveDisabled == null )
                _archiveDisabled = new SupportedPropertyPath<Boolean>(EcfOrgUnitPackageGen.P_ARCHIVE_DISABLED, this);
            return _archiveDisabled;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#isArchived()
     */
        public SupportedPropertyPath<Boolean> archived()
        {
            if(_archived == null )
                _archived = new SupportedPropertyPath<Boolean>(EcfOrgUnitPackageGen.P_ARCHIVED, this);
            return _archived;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#isDeleteDisabled()
     */
        public SupportedPropertyPath<Boolean> deleteDisabled()
        {
            if(_deleteDisabled == null )
                _deleteDisabled = new SupportedPropertyPath<Boolean>(EcfOrgUnitPackageGen.P_DELETE_DISABLED, this);
            return _deleteDisabled;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#getPackageRequestDate()
     */
        public SupportedPropertyPath<Date> packageRequestDate()
        {
            if(_packageRequestDate == null )
                _packageRequestDate = new SupportedPropertyPath<Date>(EcfOrgUnitPackageGen.P_PACKAGE_REQUEST_DATE, this);
            return _packageRequestDate;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#getPackageResultDate()
     */
        public SupportedPropertyPath<Date> packageResultDate()
        {
            if(_packageResultDate == null )
                _packageResultDate = new SupportedPropertyPath<Date>(EcfOrgUnitPackageGen.P_PACKAGE_RESULT_DATE, this);
            return _packageResultDate;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#isQueueDisabled()
     */
        public SupportedPropertyPath<Boolean> queueDisabled()
        {
            if(_queueDisabled == null )
                _queueDisabled = new SupportedPropertyPath<Boolean>(EcfOrgUnitPackageGen.P_QUEUE_DISABLED, this);
            return _queueDisabled;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#isQueued()
     */
        public SupportedPropertyPath<Boolean> queued()
        {
            if(_queued == null )
                _queued = new SupportedPropertyPath<Boolean>(EcfOrgUnitPackageGen.P_QUEUED, this);
            return _queued;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(EcfOrgUnitPackageGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EcfOrgUnitPackage.class;
        }

        public String getEntityName()
        {
            return "ecfOrgUnitPackage";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract boolean isAlive();

    public abstract boolean isArchiveDisabled();

    public abstract boolean isArchived();

    public abstract boolean isDeleteDisabled();

    public abstract Date getPackageRequestDate();

    public abstract Date getPackageResultDate();

    public abstract boolean isQueueDisabled();

    public abstract boolean isQueued();

    public abstract String getTitle();
}
