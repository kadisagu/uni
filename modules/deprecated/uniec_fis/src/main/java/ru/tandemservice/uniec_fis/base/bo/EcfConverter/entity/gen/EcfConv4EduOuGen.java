package ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4EduOu;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * ФИС: Сопоставление: Направление подготовки подразделения
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EcfConv4EduOuGen extends EntityBase
 implements INaturalIdentifiable<EcfConv4EduOuGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4EduOu";
    public static final String ENTITY_NAME = "ecfConv4EduOu";
    public static final int VERSION_HASH = 782468515;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDUCATION_ORG_UNIT = "educationOrgUnit";
    public static final String L_VALUE = "value";

    private EducationOrgUnit _educationOrgUnit;     // Направление подготовки подразделения
    private EcfCatalogItem _value;     // Значение (элемент справочника ФИС)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Направление подготовки подразделения. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EducationOrgUnit getEducationOrgUnit()
    {
        return _educationOrgUnit;
    }

    /**
     * @param educationOrgUnit Направление подготовки подразделения. Свойство не может быть null и должно быть уникальным.
     */
    public void setEducationOrgUnit(EducationOrgUnit educationOrgUnit)
    {
        dirty(_educationOrgUnit, educationOrgUnit);
        _educationOrgUnit = educationOrgUnit;
    }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     */
    @NotNull
    public EcfCatalogItem getValue()
    {
        return _value;
    }

    /**
     * @param value Значение (элемент справочника ФИС). Свойство не может быть null.
     */
    public void setValue(EcfCatalogItem value)
    {
        dirty(_value, value);
        _value = value;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EcfConv4EduOuGen)
        {
            if (withNaturalIdProperties)
            {
                setEducationOrgUnit(((EcfConv4EduOu)another).getEducationOrgUnit());
            }
            setValue(((EcfConv4EduOu)another).getValue());
        }
    }

    public INaturalId<EcfConv4EduOuGen> getNaturalId()
    {
        return new NaturalId(getEducationOrgUnit());
    }

    public static class NaturalId extends NaturalIdBase<EcfConv4EduOuGen>
    {
        private static final String PROXY_NAME = "EcfConv4EduOuNaturalProxy";

        private Long _educationOrgUnit;

        public NaturalId()
        {}

        public NaturalId(EducationOrgUnit educationOrgUnit)
        {
            _educationOrgUnit = ((IEntity) educationOrgUnit).getId();
        }

        public Long getEducationOrgUnit()
        {
            return _educationOrgUnit;
        }

        public void setEducationOrgUnit(Long educationOrgUnit)
        {
            _educationOrgUnit = educationOrgUnit;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EcfConv4EduOuGen.NaturalId) ) return false;

            EcfConv4EduOuGen.NaturalId that = (NaturalId) o;

            if( !equals(getEducationOrgUnit(), that.getEducationOrgUnit()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEducationOrgUnit());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEducationOrgUnit());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EcfConv4EduOuGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EcfConv4EduOu.class;
        }

        public T newInstance()
        {
            return (T) new EcfConv4EduOu();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "educationOrgUnit":
                    return obj.getEducationOrgUnit();
                case "value":
                    return obj.getValue();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "educationOrgUnit":
                    obj.setEducationOrgUnit((EducationOrgUnit) value);
                    return;
                case "value":
                    obj.setValue((EcfCatalogItem) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "educationOrgUnit":
                        return true;
                case "value":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "educationOrgUnit":
                    return true;
                case "value":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "educationOrgUnit":
                    return EducationOrgUnit.class;
                case "value":
                    return EcfCatalogItem.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EcfConv4EduOu> _dslPath = new Path<EcfConv4EduOu>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EcfConv4EduOu");
    }
            

    /**
     * @return Направление подготовки подразделения. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4EduOu#getEducationOrgUnit()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
    {
        return _dslPath.educationOrgUnit();
    }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4EduOu#getValue()
     */
    public static EcfCatalogItem.Path<EcfCatalogItem> value()
    {
        return _dslPath.value();
    }

    public static class Path<E extends EcfConv4EduOu> extends EntityPath<E>
    {
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnit;
        private EcfCatalogItem.Path<EcfCatalogItem> _value;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Направление подготовки подразделения. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4EduOu#getEducationOrgUnit()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
        {
            if(_educationOrgUnit == null )
                _educationOrgUnit = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT, this);
            return _educationOrgUnit;
        }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4EduOu#getValue()
     */
        public EcfCatalogItem.Path<EcfCatalogItem> value()
        {
            if(_value == null )
                _value = new EcfCatalogItem.Path<EcfCatalogItem>(L_VALUE, this);
            return _value;
        }

        public Class getEntityClass()
        {
            return EcfConv4EduOu.class;
        }

        public String getEntityName()
        {
            return "ecfConv4EduOu";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
