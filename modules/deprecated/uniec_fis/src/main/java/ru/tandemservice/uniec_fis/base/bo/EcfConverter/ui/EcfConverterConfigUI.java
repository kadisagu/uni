package ru.tandemservice.uniec_fis.base.bo.EcfConverter.ui;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.entity.IEntity;

import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.IEcfConverterDao;

/**
 * @author vdanilov
 */
public abstract class EcfConverterConfigUI extends UIPresenter {

    private IUIDataSource ecfConverterRecordDS;
    public IUIDataSource getEcfConverterRecordDS() { return this.ecfConverterRecordDS; }
    protected void setEcfConverterRecordDS(final IUIDataSource ecfConverterRecordDS) { this.ecfConverterRecordDS = ecfConverterRecordDS; }

    @Override
    public void onComponentRefresh() {
        this.setEcfConverterRecordDS(this.getConfig().getDataSource(EcfConverterConfig.ECF_CONVERTER_RECORD_DS));
    }

    public DataWrapper getCurrentRow() {
        return this.getEcfConverterRecordDS().<DataWrapper>getCurrent();
    }

    public Long getCurrentRowId() {
        final DataWrapper currentRow = this.getCurrentRow();
        return (null == currentRow ? null : currentRow.getId());
    }

    public IEntity getCurrentRowValue() {
        return this.getCurrentRow().get(EcfConverterConfig.PROPERTY_ECF_VALUE);
    }

    private DataWrapper currentEditRow;
    public DataWrapper getCurrentEditRow() { return this.currentEditRow; }
    protected void setCurrentEditRow(final DataWrapper currentEditRow) { this.currentEditRow = currentEditRow; }

    public boolean isComponentInEditMode() {
        return null != this.getCurrentEditRow();
    }

    public IEntity getCurrentEditRowValue() {
        if (!this.isComponentInEditMode()) { throw new IllegalStateException(); }
        return this.getCurrentEditRow().get(EcfConverterConfig.PROPERTY_ECF_VALUE);
    }

    public void setCurrentEditRowValue(final IEntity value) {
        if (!this.isComponentInEditMode()) { throw new IllegalStateException(); }
        this.getCurrentEditRow().put(EcfConverterConfig.PROPERTY_ECF_VALUE, value);
    }



    public boolean isCurrentRowInEditMode() {
        final DataWrapper editRow = this.getCurrentEditRow();
        if (null == editRow) { return false; }

        final DataWrapper currentRow = this.getCurrentRow();
        if (null == currentRow) { return false; }

        return editRow.getId().equals(currentRow.getId());
    }

    public void onClickEditRow()
    {
        final Long id = this.getListenerParameterAsLong();
        if (null == id) { return; }

        final IUIDataSource ds = this.getEcfConverterRecordDS();
        final DataWrapper current = ds.<DataWrapper>getRecordById(id);
        if (null == current) { return; }

        final DataWrapper wrapper = this.buildEditRowDataWrapper(current);
        if (null == wrapper) { return; }

        this.setCurrentEditRow(wrapper);
    }

    public void onClickSaveRow()
    {
        final Long id = this.getListenerParameterAsLong();
        if (null == id) { return; }

        final DataWrapper editRow = this.getCurrentEditRow();
        if (null == editRow) { return; }

        if (!id.equals(editRow.getId())) { return; }

        try {
            if (this.doSaveRow(editRow)) {
                this.setCurrentEditRow(null);
            }
        } catch (final Throwable t) {
            this.getSupport().setRefreshScheduled(true);
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    protected DataWrapper buildEditRowDataWrapper(final DataWrapper current) {
        final DataWrapper editWrapper = new DataWrapper(current.getWrapped());
        editWrapper.put(EcfConverterConfig.PROPERTY_ECF_VALUE, current.getProperty(EcfConverterConfig.PROPERTY_ECF_VALUE));
        return editWrapper;
    }

    @SuppressWarnings("unchecked")
    protected boolean doSaveRow(final DataWrapper editRow) {
        final EcfConverterConfig manager = this.getConfig().<EcfConverterConfig>getManager();
        final IEcfConverterDao<IEntity> converterDao = manager.getConverterDao();
        final EcfCatalogItem value = editRow.<EcfCatalogItem>get(EcfConverterConfig.PROPERTY_ECF_VALUE);
        converterDao.update(editRow.<IEntity>getWrapped(), value);
        return true;
    }

}
