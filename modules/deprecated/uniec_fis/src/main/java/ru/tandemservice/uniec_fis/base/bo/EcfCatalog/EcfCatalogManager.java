package ru.tandemservice.uniec_fis.base.bo.EcfCatalog;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.logic.EcfCatalogDao;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.logic.IEcfCatalogDao;

/**
 * @author vdanilov
 */
@Configuration
public class EcfCatalogManager extends BusinessObjectManager {

    public static EcfCatalogManager instance() {
        return instance(EcfCatalogManager.class);
    }

    @Bean
    public IEcfCatalogDao dao() {
        return new EcfCatalogDao();
    }

}
