/**
 *$Id$
 */
package ru.tandemservice.uniec_fis.base.bo.EcfConverter.ui.Config4Olympiad;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.EcfConverterManager;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.IEcfConverterDao;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.ui.EcfConverterConfig;

/**
 * @author Alexander Shaburov
 * @since 10.01.13
 */
@Configuration
public class EcfConverterConfig4Olympiad extends EcfConverterConfig
{
    @Override
    protected IEcfConverterDao getConverterDao()
    {
        return EcfConverterManager.instance().olympiad();
    }

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return super.presenterExtPoint(presenterExtPointBuilder());
    }

    @Bean
    @Override
    public ColumnListExtPoint ecfConverterRecordDS()
    {
        return this.columnListExtPointBuilder(ECF_CONVERTER_RECORD_DS)
                .addColumn(textColumn("title", "title").create())
                .addColumn(blockColumn("value", "valueBlock").width("460px").create())
                .addColumn(blockColumn("action", "actionBlock").width("1px").hasBlockHeader(true).create())
                .create();
    }

    @Bean
    @Override
    public IReadAggregateHandler<DSInput, DSOutput> ecfConverterRecordDSHandler()
    {
        return super.ecfConverterRecordDSHandler();
    }

    @Bean
    @Override
    public IReadAggregateHandler<DSInput, DSOutput> ecfConverterValueDSHandler()
    {
        return super.ecfConverterValueDSHandler();
    }
}
