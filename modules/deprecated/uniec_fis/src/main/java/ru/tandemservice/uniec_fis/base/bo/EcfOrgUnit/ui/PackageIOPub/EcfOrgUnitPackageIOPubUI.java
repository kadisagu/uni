package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.ui.PackageIOPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;

import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackageIOLog;

/**
 * @author vdanilov
 */
@State({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "holder.id", required=true)
})
public class EcfOrgUnitPackageIOPubUI extends UIPresenter {

    private final EntityHolder<EcfOrgUnitPackageIOLog> holder = new EntityHolder<EcfOrgUnitPackageIOLog>();
    public EntityHolder<EcfOrgUnitPackageIOLog> getHolder() { return this.holder; }
    public EcfOrgUnitPackageIOLog getOrgUnitPackageIOLog() { return getHolder().getValue(); }
    public EcfOrgUnitPackage getOrgUnitPackage() { return getOrgUnitPackageIOLog().getOwner(); }

    private OrgUnitSecModel _secModel;
    public  OrgUnitSecModel getSecModel() { return _secModel; }

    @Override
    public void onComponentRefresh() {
        getHolder().refresh();
        _secModel = new OrgUnitSecModel(getOrgUnitPackage().getEcfOrgUnit().getOrgUnit());
    }

}
