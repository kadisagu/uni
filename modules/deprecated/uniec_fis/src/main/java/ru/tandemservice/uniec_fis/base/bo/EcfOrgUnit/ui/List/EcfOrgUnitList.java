package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import org.tandemframework.shared.commonbase.tapestry.formatters.NobrCommaFormatter;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitViewUI;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit;
import ru.tandemservice.uniec_fis.base.ext.OrgUnit.ui.View.OrgUnitViewExt;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
@Configuration
public class EcfOrgUnitList extends BusinessComponentManager {

    private static final String VIEW_PROPERTY_HEAD_TITLE = "headTitle";
    public static final String ECF_ORG_UNIT_DS = "ecfOrgUnitDS";

    @Bean
    public ColumnListExtPoint ecfOrgUnitListDS() {
        final IPublisherLinkResolver publisherLinkResolver = new SimplePublisherLinkResolver(EcfOrgUnit.orgUnit().id()).param(OrgUnitViewUI.SELECTED_PAGE, OrgUnitViewExt.TAB_ECF_ORG_UNIT);
        return this.columnListExtPointBuilder(ECF_ORG_UNIT_DS)
        .addColumn(bcLinkColumn("title", EcfOrgUnit.orgUnit().fullTitle()).linkResolver(publisherLinkResolver).order().create())
        .addColumn(textColumn("phone", EcfOrgUnit.orgUnit().phone()).create())
        .addColumn(textColumn("internalPhone", EcfOrgUnit.orgUnit().internalPhone()).create())
        .addColumn(textColumn("headTitle", VIEW_PROPERTY_HEAD_TITLE).formatter(NobrCommaFormatter.INSTANCE).create())
        .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, "onClickEditEcfOrgUnit").permissionKey("editEcfOrgUnit").create())
        .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, "onClickDeleteEcfOrgUnit", alert("ecfOrgUnitDS.delete.alert", EcfOrgUnit.orgUnit().fullTitle())).disabled(EcfOrgUnit.P_TOP_ORG_UNIT).permissionKey("deleteEcfOrgUnit").create())
        .create();
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
        .addDataSource(this.searchListDS(ECF_ORG_UNIT_DS, this.ecfOrgUnitListDS()).handler(this.ecfOrgUnitDSHandler()))
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> ecfOrgUnitDSHandler() {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(this.getName()) {
            private final DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(EcfOrgUnit.class, "e");
            @Override protected DSOutput execute(final DSInput input, final ExecutionContext context) {
                final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EcfOrgUnit.class, "e").column("e");
                final DSOutput output = DQLSelectOutputBuilder.get(input, builder, context.getSession()).order(this.orderRegistry).build();
                final List<DataWrapper> wrappers = DataWrapper.wrap(output);

                BatchUtils.execute(wrappers, 128, new BatchUtils.Action<DataWrapper>() {
                    @Override public void execute(final Collection<DataWrapper> wrappers) {

                        // orgUnit.id -> headTitle(s)
                        final Map<Long, StringBuilder> map = new HashMap<Long, StringBuilder>(wrappers.size());

                        // заполняем id по EcfOrgUnit (через врапперы)
                        for (final DataWrapper wrapper: wrappers) {
                            final EcfOrgUnit wrapped = wrapper.<EcfOrgUnit>getWrapped();
                            map.put(wrapped.getOrgUnit().getId(), new StringBuilder());
                        }

                        // запрос на руководителей и его обработка
                        final Iterable<Object[]> rows = UniBaseDao.scrollRows(
                            new DQLSelectBuilder()
                                .fromEntity(EmployeePost.class, "p")
                                .column(property(EmployeePost.orgUnit().id().fromAlias("p")))
                                .column(property(EmployeePost.person().identityCard().fullFio().fromAlias("p")))
                                .where(in(EmployeePost.orgUnit().id().fromAlias("p"), map.keySet()))
                                .where(eq(property(EmployeePost.postRelation().headerPost().fromAlias("p")), value(Boolean.TRUE)))
                                .order(property(EmployeePost.person().identityCard().fullFio().fromAlias("p")))
                                .createStatement(context.getSession())
                        );
                        for (final Object[] row: rows) {
                            final StringBuilder sb = map.get(row[0]);
                            if (sb.length() > 0) { sb.append(", "); }
                            sb.append(row[1]);
                        }

                        // заполняем результатами
                        for (final DataWrapper wrapper: wrappers) {
                            final EcfOrgUnit wrapped = wrapper.<EcfOrgUnit>getWrapped();
                            wrapper.setProperty(VIEW_PROPERTY_HEAD_TITLE, map.get(wrapped.getOrgUnit().getId()).toString());
                        }
                    }
                });

                return output;
            }
        };
    }

}
