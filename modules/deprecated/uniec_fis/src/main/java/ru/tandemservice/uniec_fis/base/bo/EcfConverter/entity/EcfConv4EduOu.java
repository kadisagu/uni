package ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity;

import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.gen.EcfConv4EduOuGen;

/**
 * ФИС: Сопоставление: направление подготовки (специальность)
 */
public class EcfConv4EduOu extends EcfConv4EduOuGen
{

    public EcfConv4EduOu() {}

    public EcfConv4EduOu(EducationOrgUnit educationOrgUnit, EcfCatalogItem value) {
        this.setEducationOrgUnit(educationOrgUnit);
        this.setValue(value);
    }
}