package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage4CampaignInfo;

/**
 * @author vdanilov
 */
public interface IEcfOrgUnitPackage4CompaignDao extends IEcfOrgUnitPackageDao {

    /**
     * Создает пакет (пакет 1: Информация о приемных кампаниях)
     * @param ecfOrgUnit
     * @param enrollmentCampaign
     * @param status
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    EcfOrgUnitPackage4CampaignInfo doCreatePackage(EcfOrgUnit ecfOrgUnit, EnrollmentCampaign enrollmentCampaign, EcfCatalogItem status);

}
