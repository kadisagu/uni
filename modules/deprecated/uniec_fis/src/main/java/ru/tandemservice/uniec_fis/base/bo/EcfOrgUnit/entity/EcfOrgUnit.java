package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;

import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.gen.EcfOrgUnitGen;
import ru.tandemservice.uniec_fis.util.IFisAuthentication;

/**
 * Подразделение, имеющее самостоятельный статус в ФИС
 */
public class EcfOrgUnit extends EcfOrgUnitGen implements IFisAuthentication
{
    public EcfOrgUnit() {}

    public EcfOrgUnit(final OrgUnit orgUnit) {
        this.setOrgUnit(orgUnit);
    }


    @Override
    @EntityDSLSupport
    public boolean isTopOrgUnit() {
        final OrgUnit orgUnit = this.getOrgUnit();
        return (null != orgUnit) && TopOrgUnit.class.isInstance(orgUnit);
    }
}