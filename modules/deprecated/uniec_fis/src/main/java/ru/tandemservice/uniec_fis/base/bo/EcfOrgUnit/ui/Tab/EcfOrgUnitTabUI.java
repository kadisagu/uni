package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.ui.Tab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.State;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.EcfOrgUnitManager;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key = "orgUnitId", binding = "holder.id", required=true)
})
@State({
    @Bind(key = "selectedTab", binding = "selectedTab")
})
public class EcfOrgUnitTabUI extends UIPresenter {

    private final OrgUnitHolder holder = new OrgUnitHolder();
    public OrgUnitHolder getHolder() { return this.holder; }
    public OrgUnit getOrgUnit() { return this.getHolder().getValue(); }

    public CommonPostfixPermissionModelBase getSecModel() { return this.getHolder().getSecModel(); }

    private String _selectedTab;
    public String getSelectedTab() { return this._selectedTab; }
    public void setSelectedTab(final String selectedTab) { this._selectedTab = selectedTab; }

    private Long ecfOrgUnitId;
    public Long getEcfOrgUnitId() { return this.ecfOrgUnitId; }

    @Override
    public void onComponentRefresh() {
        final EcfOrgUnit ecfOrgUnit = EcfOrgUnitManager.instance().dao().getEcfOrgUnit(
            this.getHolder().refresh(OrgUnit.class)
        );
        if (null == ecfOrgUnit) {
            throw new NullPointerException("no-ecf-org-unit: " + this.getOrgUnit().getId());
        }
        this.ecfOrgUnitId = ecfOrgUnit.getId();
    }

}
