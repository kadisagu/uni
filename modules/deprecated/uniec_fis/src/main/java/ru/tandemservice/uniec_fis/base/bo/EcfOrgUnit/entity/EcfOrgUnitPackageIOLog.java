package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.base.util.zip.GZipCompressionInterface;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.EcfOrgUnitManager;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.gen.EcfOrgUnitPackageIOLogGen;
import ru.tandemservice.uniec_fis.util.FisServiceImpl;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * Лог взаимодействия с сервером
 */
public class EcfOrgUnitPackageIOLog extends EcfOrgUnitPackageIOLogGen implements ITitled
{
    public static final String IO_STATUS_COMPLETE = "complete";
    public static final String IO_STATUS_FIS_ERROR = "fis-error";
    public static final String IO_STATUS_FATAL_ERROR = "fatal-error";

    public static final DateFormatter DATE_FORMATTER = new DateFormatter("dd.MM.yyyy HH:mm:ss");
    public static final GZipCompressionInterface zip = GZipCompressionInterface.INSTANCE;

    public EcfOrgUnitPackageIOLog() {
    }
    public EcfOrgUnitPackageIOLog(EcfOrgUnitPackage owner) {
        setOwner(owner);
    }

    public byte[] getXmlRequest() {
        return zip.decompress(this.getZipXmlRequest());
    }
    public void setXmlRequest(byte[] xmlRequest) {
        this.setZipXmlRequest(zip.compress(xmlRequest));
    }

    public String getXmlRequestString() {
        final byte[] xmlRequest = getXmlRequest();
        return (null == xmlRequest ? null : new String(xmlRequest));
    }
    public void setXmlRequestString(String xmlRequest) {
        this.setXmlRequest(xmlRequest.getBytes());
    }

    public String getFormattedXmlRequest() {
        return FisServiceImpl.prettyFormat(getXmlRequestString());
    }


    public byte[] getXmlResponse() {
        return zip.decompress(this.getZipXmlResponse());
    }
    public void setXmlResponse(byte[] xmlResponse) {
        this.setZipXmlResponse(zip.compress(xmlResponse));
    }

    public String getXmlResponseString() {
        final byte[] xmlResponse = getXmlResponse();
        return (null == xmlResponse ? null : new String(xmlResponse));
    }
    public void setXmlResponseString(String xmlResponse) {
        this.setXmlResponse(xmlResponse.getBytes());
    }

    public String getFormattedXmlResponse() {
        return FisServiceImpl.prettyFormat(getXmlResponseString());
    }


    public void registerError(Throwable t) {
        ByteArrayOutputStream buff = new ByteArrayOutputStream();
        PrintStream print = new PrintStream(buff);
        while (null != t) {
            for (int i = 0; i < 30; i++)
                print.println(t.getStackTrace()[i].toString());
            t = t.getCause();
        }
        print.flush();
        this.setZipErrorLog(zip.compress(buff.toByteArray()));
    }

    public void registerError(String error) {
        this.setZipErrorLog(zip.compress(error.getBytes()));
    }

    public String getErrorString() {
        if (getStatus().equalsIgnoreCase(IO_STATUS_FIS_ERROR))
        {
            if (getFisErrorCode() != null)
                return "(" + getFisErrorCode() + ")" + getFisErrorText();
            else
                return StringUtils.trimToEmpty(getFisErrorText());
        }

        final byte[] bytes = zip.decompress(getZipErrorLog());
        return (null == bytes ? null : new String(bytes));
    }

    public String getDescription() {
        return DATE_FORMATTER.format(getOperationDate()) +" " + getOperationTextStatus();
    }

    public String getOprationName() {
        return StringUtils.substringAfterLast(getOperationUrl(), "/");
    }

    public String getOperationTextStatus()
    {
        final String key = getFullStatus();

        if (getStatus().equalsIgnoreCase(IO_STATUS_FIS_ERROR))
        {
            // до первой точки, исключая точку
            final String errorText = getFisErrorText() == null ? "" : getFisErrorText().split("[.]")[0];

            // если не указан код ошибки, то значит от фис пришел ответ не соответствующий xsd, тогда в fisErrorText будет просто содержимое ответа
            final String text = getFisErrorCode() != null ? "(" + getFisErrorCode() + ") " + errorText : (StringUtils.trimToEmpty(getFisErrorText()).length() > 50 ? StringUtils.trimToEmpty(getFisErrorText()).substring(0, 50) : StringUtils.trimToEmpty(getFisErrorText()));

            if (StringUtils.isEmpty(text))
                return EcfOrgUnitManager.instance().getProperty(key, text).replace(": ", "");
            return EcfOrgUnitManager.instance().getProperty(key, text);
        }
        else
            return EcfOrgUnitManager.instance().getProperty(key);
    }

    public String getFullStatus() {
        return getOprationName() + "." + getStatus();
    }

    @Override
    @EntityDSLSupport
    public String getTitle() {
        return getDescription();
    }

}