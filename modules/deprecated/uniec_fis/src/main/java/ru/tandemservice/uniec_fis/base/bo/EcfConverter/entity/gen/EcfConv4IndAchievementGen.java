package ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniec.entity.catalog.IndividualProgress;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4IndAchievement;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * ФИС: Сопоставление: Индивидуальные достижения ФИС
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EcfConv4IndAchievementGen extends EntityBase
 implements INaturalIdentifiable<EcfConv4IndAchievementGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4IndAchievement";
    public static final String ENTITY_NAME = "ecfConv4IndAchievement";
    public static final int VERSION_HASH = 1440113669;
    private static IEntityMeta ENTITY_META;

    public static final String L_INDIVIDUAL_ACHIEVEMENT = "individualAchievement";
    public static final String L_VALUE = "value";

    private IndividualProgress _individualAchievement;     // Индивидуальное достижение
    private EcfCatalogItem _value;     // Значение (элемент справочника ФИС)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Индивидуальное достижение. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public IndividualProgress getIndividualAchievement()
    {
        return _individualAchievement;
    }

    /**
     * @param individualAchievement Индивидуальное достижение. Свойство не может быть null и должно быть уникальным.
     */
    public void setIndividualAchievement(IndividualProgress individualAchievement)
    {
        dirty(_individualAchievement, individualAchievement);
        _individualAchievement = individualAchievement;
    }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     */
    @NotNull
    public EcfCatalogItem getValue()
    {
        return _value;
    }

    /**
     * @param value Значение (элемент справочника ФИС). Свойство не может быть null.
     */
    public void setValue(EcfCatalogItem value)
    {
        dirty(_value, value);
        _value = value;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EcfConv4IndAchievementGen)
        {
            if (withNaturalIdProperties)
            {
                setIndividualAchievement(((EcfConv4IndAchievement)another).getIndividualAchievement());
            }
            setValue(((EcfConv4IndAchievement)another).getValue());
        }
    }

    public INaturalId<EcfConv4IndAchievementGen> getNaturalId()
    {
        return new NaturalId(getIndividualAchievement());
    }

    public static class NaturalId extends NaturalIdBase<EcfConv4IndAchievementGen>
    {
        private static final String PROXY_NAME = "EcfConv4IndAchievementNaturalProxy";

        private Long _individualAchievement;

        public NaturalId()
        {}

        public NaturalId(IndividualProgress individualAchievement)
        {
            _individualAchievement = ((IEntity) individualAchievement).getId();
        }

        public Long getIndividualAchievement()
        {
            return _individualAchievement;
        }

        public void setIndividualAchievement(Long individualAchievement)
        {
            _individualAchievement = individualAchievement;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EcfConv4IndAchievementGen.NaturalId) ) return false;

            EcfConv4IndAchievementGen.NaturalId that = (NaturalId) o;

            if( !equals(getIndividualAchievement(), that.getIndividualAchievement()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getIndividualAchievement());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getIndividualAchievement());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EcfConv4IndAchievementGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EcfConv4IndAchievement.class;
        }

        public T newInstance()
        {
            return (T) new EcfConv4IndAchievement();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "individualAchievement":
                    return obj.getIndividualAchievement();
                case "value":
                    return obj.getValue();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "individualAchievement":
                    obj.setIndividualAchievement((IndividualProgress) value);
                    return;
                case "value":
                    obj.setValue((EcfCatalogItem) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "individualAchievement":
                        return true;
                case "value":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "individualAchievement":
                    return true;
                case "value":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "individualAchievement":
                    return IndividualProgress.class;
                case "value":
                    return EcfCatalogItem.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EcfConv4IndAchievement> _dslPath = new Path<EcfConv4IndAchievement>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EcfConv4IndAchievement");
    }
            

    /**
     * @return Индивидуальное достижение. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4IndAchievement#getIndividualAchievement()
     */
    public static IndividualProgress.Path<IndividualProgress> individualAchievement()
    {
        return _dslPath.individualAchievement();
    }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4IndAchievement#getValue()
     */
    public static EcfCatalogItem.Path<EcfCatalogItem> value()
    {
        return _dslPath.value();
    }

    public static class Path<E extends EcfConv4IndAchievement> extends EntityPath<E>
    {
        private IndividualProgress.Path<IndividualProgress> _individualAchievement;
        private EcfCatalogItem.Path<EcfCatalogItem> _value;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Индивидуальное достижение. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4IndAchievement#getIndividualAchievement()
     */
        public IndividualProgress.Path<IndividualProgress> individualAchievement()
        {
            if(_individualAchievement == null )
                _individualAchievement = new IndividualProgress.Path<IndividualProgress>(L_INDIVIDUAL_ACHIEVEMENT, this);
            return _individualAchievement;
        }

    /**
     * @return Значение (элемент справочника ФИС). Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4IndAchievement#getValue()
     */
        public EcfCatalogItem.Path<EcfCatalogItem> value()
        {
            if(_value == null )
                _value = new EcfCatalogItem.Path<EcfCatalogItem>(L_VALUE, this);
            return _value;
        }

        public Class getEntityClass()
        {
            return EcfConv4IndAchievement.class;
        }

        public String getEntityName()
        {
            return "ecfConv4IndAchievement";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
