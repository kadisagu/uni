/**
 *$Id$
 */
package ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.olympiad;

import org.tandemframework.core.entity.IdentifiableWrapper;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.IEcfConverterDao;

/**
 * @author Alexander Shaburov
 * @since 10.01.13
 */
public interface IEcfConverterDao4Olympiad extends IEcfConverterDao<IdentifiableWrapper>
{
    /**
     * Производится автозаполнение настройки для еще не настроенных элементов.
     * Проверяется на совпадение названий олимпиад УНИ и ФИС
     */
    void doAutoSync();
}
