package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage4CampaignInfo;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Пакет: Приемная кампания
 *
 * Root / PackageData / CampaignInfo / Campaigns / Campaign
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EcfOrgUnitPackage4CampaignInfoGen extends EcfOrgUnitPackage
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage4CampaignInfo";
    public static final String ENTITY_NAME = "ecfOrgUnitPackage4CampaignInfo";
    public static final int VERSION_HASH = 1420043424;
    private static IEntityMeta ENTITY_META;

    public static final String L_STATUS = "status";

    private EcfCatalogItem _status;     // Статус ПК ФИС

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * Статус ПК в ФИС, определяется пользователем в момент выгрузки
     *
     * @return Статус ПК ФИС. Свойство не может быть null.
     */
    @NotNull
    public EcfCatalogItem getStatus()
    {
        return _status;
    }

    /**
     * @param status Статус ПК ФИС. Свойство не может быть null.
     */
    public void setStatus(EcfCatalogItem status)
    {
        dirty(_status, status);
        _status = status;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EcfOrgUnitPackage4CampaignInfoGen)
        {
            setStatus(((EcfOrgUnitPackage4CampaignInfo)another).getStatus());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EcfOrgUnitPackage4CampaignInfoGen> extends EcfOrgUnitPackage.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EcfOrgUnitPackage4CampaignInfo.class;
        }

        public T newInstance()
        {
            return (T) new EcfOrgUnitPackage4CampaignInfo();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "status":
                    return obj.getStatus();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "status":
                    obj.setStatus((EcfCatalogItem) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "status":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "status":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "status":
                    return EcfCatalogItem.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EcfOrgUnitPackage4CampaignInfo> _dslPath = new Path<EcfOrgUnitPackage4CampaignInfo>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EcfOrgUnitPackage4CampaignInfo");
    }
            

    /**
     * Статус ПК в ФИС, определяется пользователем в момент выгрузки
     *
     * @return Статус ПК ФИС. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage4CampaignInfo#getStatus()
     */
    public static EcfCatalogItem.Path<EcfCatalogItem> status()
    {
        return _dslPath.status();
    }

    public static class Path<E extends EcfOrgUnitPackage4CampaignInfo> extends EcfOrgUnitPackage.Path<E>
    {
        private EcfCatalogItem.Path<EcfCatalogItem> _status;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * Статус ПК в ФИС, определяется пользователем в момент выгрузки
     *
     * @return Статус ПК ФИС. Свойство не может быть null.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage4CampaignInfo#getStatus()
     */
        public EcfCatalogItem.Path<EcfCatalogItem> status()
        {
            if(_status == null )
                _status = new EcfCatalogItem.Path<EcfCatalogItem>(L_STATUS, this);
            return _status;
        }

        public Class getEntityClass()
        {
            return EcfOrgUnitPackage4CampaignInfo.class;
        }

        public String getEntityName()
        {
            return "ecfOrgUnitPackage4CampaignInfo";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
