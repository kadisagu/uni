/* $Id: OrgUnitViewExt.java 22487 2012-04-04 13:16:00Z vzhukov $ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec_fis.base.ext.OrgUnit.ui.View;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitView;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.EcfOrgUnitManager;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.ui.Tab.EcfOrgUnitTab;

@Configuration
public class OrgUnitViewExt extends BusinessComponentExtensionManager
{
    public static final String TAB_ECF_ORG_UNIT = "ecfOrgUnitTab";

    @Autowired
    public OrgUnitView _orgUnitView;

    @Bean
    public TabPanelExtension tabPanelExtension() {
        return this.tabPanelExtensionBuilder(this._orgUnitView.orgUnitTabPanelExtPoint())
        .addTab(
            componentTab(TAB_ECF_ORG_UNIT, EcfOrgUnitTab.class)
            .permissionKey("ui:secModel.orgUnit_viewEcfTab")
            .parameters("mvel:['orgUnitId':presenter.orgUnit.id]")
            .visible("ognl:@ru.tandemservice.uniec_fis.base.ext.OrgUnit.ui.View.OrgUnitViewExt@showEcfOrgUnitTab(presenter.orgUnit)")
        )
        .create();
    }

    public static boolean showEcfOrgUnitTab(final OrgUnit orgUnit) {
        return (null != EcfOrgUnitManager.instance().dao().getEcfOrgUnit(orgUnit));
    }

}
