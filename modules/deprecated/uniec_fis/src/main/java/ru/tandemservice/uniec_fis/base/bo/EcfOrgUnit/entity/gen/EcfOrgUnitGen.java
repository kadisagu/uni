package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Подразделение, имеющее самостоятельный статус в ФИС
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EcfOrgUnitGen extends EntityBase
 implements INaturalIdentifiable<EcfOrgUnitGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit";
    public static final String ENTITY_NAME = "ecfOrgUnit";
    public static final int VERSION_HASH = 816488950;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORG_UNIT = "orgUnit";
    public static final String P_UID = "uid";
    public static final String P_REMOVAL_DATE = "removalDate";
    public static final String P_LOGIN = "login";
    public static final String P_PASSWORD = "password";
    public static final String P_TOP_ORG_UNIT = "topOrgUnit";

    private OrgUnit _orgUnit;     // Подразделение
    private long _uid;     // Уникальный идентификационный номер
    private Date _removalDate;     // Дата утраты актуальности
    private String _login;     // Логин
    private String _password;     // Пароль

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Подразделение. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null и должно быть уникальным.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * Уникальный номер (генерируется при сохранении)
     *
     * @return Уникальный идентификационный номер. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public long getUid()
    {
        return _uid;
    }

    /**
     * @param uid Уникальный идентификационный номер. Свойство не может быть null и должно быть уникальным.
     */
    public void setUid(long uid)
    {
        dirty(_uid, uid);
        _uid = uid;
    }

    /**
     * Показывает, когда подразделение утратило статус аккредитованного ФИС
     *
     * @return Дата утраты актуальности.
     */
    public Date getRemovalDate()
    {
        return _removalDate;
    }

    /**
     * @param removalDate Дата утраты актуальности.
     */
    public void setRemovalDate(Date removalDate)
    {
        dirty(_removalDate, removalDate);
        _removalDate = removalDate;
    }

    /**
     * @return Логин.
     */
    @Length(max=255)
    public String getLogin()
    {
        return _login;
    }

    /**
     * @param login Логин.
     */
    public void setLogin(String login)
    {
        dirty(_login, login);
        _login = login;
    }

    /**
     * @return Пароль.
     */
    @Length(max=255)
    public String getPassword()
    {
        return _password;
    }

    /**
     * @param password Пароль.
     */
    public void setPassword(String password)
    {
        dirty(_password, password);
        _password = password;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EcfOrgUnitGen)
        {
            if (withNaturalIdProperties)
            {
                setOrgUnit(((EcfOrgUnit)another).getOrgUnit());
            }
            setUid(((EcfOrgUnit)another).getUid());
            setRemovalDate(((EcfOrgUnit)another).getRemovalDate());
            setLogin(((EcfOrgUnit)another).getLogin());
            setPassword(((EcfOrgUnit)another).getPassword());
        }
    }

    public INaturalId<EcfOrgUnitGen> getNaturalId()
    {
        return new NaturalId(getOrgUnit());
    }

    public static class NaturalId extends NaturalIdBase<EcfOrgUnitGen>
    {
        private static final String PROXY_NAME = "EcfOrgUnitNaturalProxy";

        private Long _orgUnit;

        public NaturalId()
        {}

        public NaturalId(OrgUnit orgUnit)
        {
            _orgUnit = ((IEntity) orgUnit).getId();
        }

        public Long getOrgUnit()
        {
            return _orgUnit;
        }

        public void setOrgUnit(Long orgUnit)
        {
            _orgUnit = orgUnit;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EcfOrgUnitGen.NaturalId) ) return false;

            EcfOrgUnitGen.NaturalId that = (NaturalId) o;

            if( !equals(getOrgUnit(), that.getOrgUnit()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getOrgUnit());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getOrgUnit());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EcfOrgUnitGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EcfOrgUnit.class;
        }

        public T newInstance()
        {
            return (T) new EcfOrgUnit();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "uid":
                    return obj.getUid();
                case "removalDate":
                    return obj.getRemovalDate();
                case "login":
                    return obj.getLogin();
                case "password":
                    return obj.getPassword();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "uid":
                    obj.setUid((Long) value);
                    return;
                case "removalDate":
                    obj.setRemovalDate((Date) value);
                    return;
                case "login":
                    obj.setLogin((String) value);
                    return;
                case "password":
                    obj.setPassword((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "orgUnit":
                        return true;
                case "uid":
                        return true;
                case "removalDate":
                        return true;
                case "login":
                        return true;
                case "password":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "orgUnit":
                    return true;
                case "uid":
                    return true;
                case "removalDate":
                    return true;
                case "login":
                    return true;
                case "password":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "uid":
                    return Long.class;
                case "removalDate":
                    return Date.class;
                case "login":
                    return String.class;
                case "password":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EcfOrgUnit> _dslPath = new Path<EcfOrgUnit>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EcfOrgUnit");
    }
            

    /**
     * @return Подразделение. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * Уникальный номер (генерируется при сохранении)
     *
     * @return Уникальный идентификационный номер. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit#getUid()
     */
    public static PropertyPath<Long> uid()
    {
        return _dslPath.uid();
    }

    /**
     * Показывает, когда подразделение утратило статус аккредитованного ФИС
     *
     * @return Дата утраты актуальности.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit#getRemovalDate()
     */
    public static PropertyPath<Date> removalDate()
    {
        return _dslPath.removalDate();
    }

    /**
     * @return Логин.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit#getLogin()
     */
    public static PropertyPath<String> login()
    {
        return _dslPath.login();
    }

    /**
     * @return Пароль.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit#getPassword()
     */
    public static PropertyPath<String> password()
    {
        return _dslPath.password();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit#isTopOrgUnit()
     */
    public static SupportedPropertyPath<Boolean> topOrgUnit()
    {
        return _dslPath.topOrgUnit();
    }

    public static class Path<E extends EcfOrgUnit> extends EntityPath<E>
    {
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PropertyPath<Long> _uid;
        private PropertyPath<Date> _removalDate;
        private PropertyPath<String> _login;
        private PropertyPath<String> _password;
        private SupportedPropertyPath<Boolean> _topOrgUnit;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Подразделение. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * Уникальный номер (генерируется при сохранении)
     *
     * @return Уникальный идентификационный номер. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit#getUid()
     */
        public PropertyPath<Long> uid()
        {
            if(_uid == null )
                _uid = new PropertyPath<Long>(EcfOrgUnitGen.P_UID, this);
            return _uid;
        }

    /**
     * Показывает, когда подразделение утратило статус аккредитованного ФИС
     *
     * @return Дата утраты актуальности.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit#getRemovalDate()
     */
        public PropertyPath<Date> removalDate()
        {
            if(_removalDate == null )
                _removalDate = new PropertyPath<Date>(EcfOrgUnitGen.P_REMOVAL_DATE, this);
            return _removalDate;
        }

    /**
     * @return Логин.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit#getLogin()
     */
        public PropertyPath<String> login()
        {
            if(_login == null )
                _login = new PropertyPath<String>(EcfOrgUnitGen.P_LOGIN, this);
            return _login;
        }

    /**
     * @return Пароль.
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit#getPassword()
     */
        public PropertyPath<String> password()
        {
            if(_password == null )
                _password = new PropertyPath<String>(EcfOrgUnitGen.P_PASSWORD, this);
            return _password;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit#isTopOrgUnit()
     */
        public SupportedPropertyPath<Boolean> topOrgUnit()
        {
            if(_topOrgUnit == null )
                _topOrgUnit = new SupportedPropertyPath<Boolean>(EcfOrgUnitGen.P_TOP_ORG_UNIT, this);
            return _topOrgUnit;
        }

        public Class getEntityClass()
        {
            return EcfOrgUnit.class;
        }

        public String getEntityName()
        {
            return "ecfOrgUnit";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract boolean isTopOrgUnit();
}
