/* $Id: DppOrgUnitAddEditUI.java 22487 2012-04-04 13:16:00Z vzhukov $ */
package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.hibsupport.entity.EntityHolder;

import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.EcfOrgUnitManager;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit;

@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "holder.id")
})
public class EcfOrgUnitAddEditUI extends UIPresenter
{
    private final EntityHolder<EcfOrgUnit> holder = new EntityHolder<EcfOrgUnit>();

    public EntityHolder<EcfOrgUnit> getHolder() { return this.holder; }
    public EcfOrgUnit getEcfOrgUnit() { return this.getHolder().getValue(); }

    @Override
    public void onComponentRefresh()
    {
        final EntityHolder<EcfOrgUnit> holder = this.getHolder();

        // инициализация формы
        if (null == holder.getValue()) {
            holder.setValue(new EcfOrgUnit());
        }

        // обновление объекта
        holder.refresh();
    }

    @Override
    public void onComponentRender() {
        final EntityHolder<EcfOrgUnit> holder = this.getHolder();
        ContextLocal.beginPageTitlePart(this._uiConfig.getProperty(null == holder.getId() ? "ui.sticker.add" : "ui.sticker.edit"));
    }

    public void onClickApply() {
        EcfOrgUnitManager.instance().dao().save(this.getEcfOrgUnit());
        this.deactivate();
    }
}
