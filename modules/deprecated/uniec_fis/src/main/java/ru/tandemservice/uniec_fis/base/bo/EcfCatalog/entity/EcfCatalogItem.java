package ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity;

import org.tandemframework.core.CoreExceptionUtils;

import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.gen.EcfCatalogItemGen;

/**
 * Запись справочника ФИС
 */
public class EcfCatalogItem extends EcfCatalogItemGen
{
    @Override
    public EcfCatalogItem clone() {
        try {
            final EcfCatalogItem clone = this.getClass().newInstance();
            clone.update(this);
            return clone;
        } catch (final Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    public Long getFisID() {
        return Long.valueOf(getFisItemCode());
    }

}