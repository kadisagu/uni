package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.hibsupport.DataAccessServices;

import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.EcfOrgUnitManager;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.ui.AddEdit.EcfOrgUnitAddEdit;

/**
 * @author vdanilov
 */
public class EcfOrgUnitListUI extends UIPresenter {

    public void onClickAddEcfOrgUnit() {
        this._uiActivation.asRegionDialog(EcfOrgUnitAddEdit.class).parameter(UIPresenter.PUBLISHER_ID, null).activate();
    }

    public void onClickEditEcfOrgUnit() {
        this._uiActivation.asRegionDialog(EcfOrgUnitAddEdit.class).parameter(UIPresenter.PUBLISHER_ID, this.getListenerParameterAsLong()).activate();
    }

    public void onClickDeleteEcfOrgUnit() {
        final EcfOrgUnit ecfOrgUnit = DataAccessServices.dao().get(EcfOrgUnit.class, this.getListenerParameterAsLong());
        EcfOrgUnitManager.instance().dao().delete(ecfOrgUnit);
    }


}
