package ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.finSources;

import org.tandemframework.shared.commonbase.base.util.Wiki;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.gen.EcfCatalogItemGen;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.EcfConverterManager;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author vdanilov
 */
public class EcfFinSourceDao extends UniBaseDao implements IEcfFinSourceDao {

    public static boolean isBudget(EnrollmentDirection dir) {
        return dir.isBudget() && null != dir.getMinisterialPlan() && dir.getMinisterialPlan() > 0;
    }

    public static boolean isContract(EnrollmentDirection dir) {
        return dir.isContract() && null != dir.getContractPlan() && dir.getContractPlan() > 0;
    }

    public static boolean isTargetAdmission(EnrollmentDirection dir) {
        return isBudget(dir) && null != dir.getTargetAdmissionPlanBudget() && dir.getTargetAdmissionPlanBudget() > 0;
    }

    public static boolean isListener(EnrollmentDirection dir) {
        return dir.isContract() && (null != dir.getListenerPlan() && dir.getListenerPlan() > 0);
    }

    public static boolean isSecondEdu(EnrollmentDirection dir) {
        return dir.isContract() && (null != dir.getSecondHighEducationPlan() && dir.getSecondHighEducationPlan() > 0);
    }

    public static boolean isQuota(EnrollmentDirection dir) {
        return dir.isBudget() && null != dir.getSpecRightsQuota() && dir.getSpecRightsQuota() > 0;
    }

    @Override
    public List<EcfCatalogItem> getSourceList(EnrollmentDirection dir) {
        boolean isBudget = isBudget(dir);
        boolean isContract = isContract(dir);
        boolean isTargetAdmission = isTargetAdmission(dir);
        boolean isQuota = isQuota(dir);
        return getSourceList(isBudget, isContract, isTargetAdmission, isQuota);
    }

    @Override
    public List<EcfCatalogItem> getSourceList(RequestedEnrollmentDirection reqDir) {
        boolean isBudget = reqDir.getCompensationType().isBudget();
        boolean isContract = !isBudget;
        boolean selectTargetAdmission = null != reqDir.getTargetAdmissionKind() || reqDir.isTargetAdmission(); // здесь ингорируем план приема по ЦП (если косячат, то ФИС пакет отвергнет)
        Integer benefit = EcfConverterManager.instance().benefit().getBenefitCode4RequestedEnrollmentDirection(reqDir);
        boolean isQuota = benefit != null && benefit.equals(2);
        if (isQuota) {
            isBudget = false;
            isContract = false;
        }
        return getSourceList(isBudget && !selectTargetAdmission, isContract, isBudget && selectTargetAdmission, isQuota);
    }

    @Override
    public EcfCatalogItem getSource(RequestedEnrollmentDirection reqDir)
    {
        List<EcfCatalogItem> sourceList = getSourceList(reqDir);
        if (sourceList.size() != 1)
            throw new IllegalStateException("FinanceSourceID is ambiguous: " + sourceList.stream().map(EcfCatalogItemGen::getTitle).collect(Collectors.joining("; ")));
        return sourceList.get(0);
    }

    @Override
    public EcfCatalogItem getSource(PreliminaryEnrollmentStudent preEnrStudent)
    {
        boolean isBudget = preEnrStudent.getCompensationType().isBudget();
        boolean selectTargetAdmission = preEnrStudent.isTargetAdmission(); // здесь ингорируем план приема по ЦП (если косячат, то ФИС пакет отвергнет)

        final List<EcfCatalogItem> sourceList = getSourceList(isBudget && !selectTargetAdmission, !isBudget, isBudget && selectTargetAdmission, false);

        if (sourceList.size() != 1)
            throw new IllegalStateException("FinanceSourceID is ambiguous");
        return sourceList.get(0);
    }


    /** сопоставление по кодам */
    @Wiki(url="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=9864761")
    public List<EcfCatalogItem> getSourceList(boolean isBudget, boolean isContract, boolean isTargetAdmission, boolean isQuota) {
        final List<EcfCatalogItem> result = new ArrayList<EcfCatalogItem>(4);
        if (isBudget)
            result.add(this.<EcfCatalogItem>getByNaturalId(new EcfCatalogItemGen.NaturalId(CATALOG_CODE, BUDGET_CODE)));

        if (isContract)
            result.add(this.<EcfCatalogItem>getByNaturalId(new EcfCatalogItemGen.NaturalId(CATALOG_CODE, CONTRACT_CODE)));

        if (isTargetAdmission)
            result.add(this.<EcfCatalogItem>getByNaturalId(new EcfCatalogItemGen.NaturalId(CATALOG_CODE, TARGET_CODE)));

        if (isQuota)
            result.add(this.<EcfCatalogItem>getByNaturalId(new EcfCatalogItemGen.NaturalId(CATALOG_CODE, QUOTE_CODE)));

        return result;
    }


    /**
     * Источник финансирования - Бюджетные места
     *
     * @param source элемент справочника
     */
    public static boolean isBudget(EcfCatalogItem source)
    {
        return source.getFisCatalogCode().equals(CATALOG_CODE) && source.getFisItemCode().equals(BUDGET_CODE);
    }

    /**
     * Источник финансирования - С оплатой обучения
     *
     * @param source элемент справочника
     */
    public static boolean isContract(EcfCatalogItem source)
    {
        return source.getFisCatalogCode().equals(CATALOG_CODE) && source.getFisItemCode().equals(CONTRACT_CODE);
    }

    /**
     * Источник финансирования - Целевой прием
     *
     * @param source элемент справочника
     */
    public static boolean isTargetAdmission(EcfCatalogItem source)
    {
        return source.getFisCatalogCode().equals(CATALOG_CODE) && source.getFisItemCode().equals(TARGET_CODE);
    }

    /**
     * Источник финансирования - Квота приема лиц, имеющих особое право
     *
     * @param source элемент справочника
     */
    public static boolean isQuota(EcfCatalogItem source)
    {
        return source.getFisCatalogCode().equals(CATALOG_CODE) && source.getFisItemCode().equals(QUOTE_CODE);
    }
}
