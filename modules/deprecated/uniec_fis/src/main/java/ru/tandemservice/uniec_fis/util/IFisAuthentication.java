package ru.tandemservice.uniec_fis.util;

/**
 * @author vdanilov
 */
public interface IFisAuthentication {

    String getLogin();

    String getPassword();

}
