/**
 *$Id$
 */
package ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.benefit;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import ru.tandemservice.uniec.entity.entrant.Discipline2OlympiadDiplomaRelation;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.OlympiadDiploma;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;

import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 28.12.12
 */
public interface IEcfBenefitDao extends INeedPersistenceSupport
{
    /**
     * По коду можно получить ID льготы ФИС.
     * Чем меньше тем лучше для абитуриента.
     * @return код льготы;
     * null, если для ВНП нельзя вычислить код льготы
     */
    @Wiki(url="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=9864778")
    Integer getBenefitCode4RequestedEnrollmentDirection(RequestedEnrollmentDirection direction);

    /**
     * @return элемента справочника ФИС №30. Вид льготы.
     * null, если для ВНП нельзя вычислить код льготы
     */
    @Wiki(url="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=9864778")
    EcfCatalogItem getEcfItem4RequestedEnrollmentDirection(RequestedEnrollmentDirection direction);

    /**
     * По коду получаем элемента справочника ФИС №30. Вид льготы.
     * fisCatalogCode = 30
     * @param code 1..3
     * @return элемента справочника ФИС.
     * Eсли выходит за интервал, то null
     */
    @Wiki(url="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=9864778")
    EcfCatalogItem getEcfItem4BenefitCode(int code);

    /**
     * @return Передаваемое название льготы.
     * null, если для ВНП нельзя вычислить код льготы
     */
    @Wiki(url="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=9864778")
    String getDocumentTypeNameText(RequestedEnrollmentDirection direction);

    /**
     * @return список зачтений олимпиад абитуриента из кеша
     */
    List<Discipline2OlympiadDiplomaRelation> getDiscipline2OlympiadDiplomaRelations(Entrant entrant);

    /**
     * @return список дипломов олимпиад абитуриента из кеша
     */
    List<OlympiadDiploma> getOlympiadDiplomas(Entrant entrant);

    /**
     * @return Льгота абитуриента из кеша
     */
    PersonBenefit getPersonBenefit(Entrant entrant);
}
