/* $Id$ */
package ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.region;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.IEcfConverterDao;

/**
 * @author Andrey Andreev
 * @since 12.05.2016
 */
public interface IEcfConverterDao4Region extends IEcfConverterDao<AddressItem>
{
    @Transactional(propagation= Propagation.REQUIRED, readOnly=false)
    void doAutoSync();
}
