package ru.tandemservice.uniec_fis.base.bo.EcfConverter.ui.Config4SetDiscipline;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.EcfConverterManager;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.ui.EcfConverterConfigUI;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author vdanilov
 */
public class EcfConverterConfig4SetDisciplineUI extends EcfConverterConfigUI {

    public static final String PARAM_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();

        if (getSettings().get(PARAM_ENROLLMENT_CAMPAIGN) == null)
        {
            final List<Object> ecList = DataAccessServices.dao().getList(
                    new DQLSelectBuilder().fromEntity(EnrollmentCampaign.class, "c").column(property("c"))
                            .order(property(EnrollmentCampaign.id().fromAlias("c")), OrderDirection.desc)
            );

            getSettings().set(PARAM_ENROLLMENT_CAMPAIGN, !ecList.isEmpty() ? ecList.get(0) : null);
        }
    }

    public void onClickAutoSync() {
        EcfConverterManager.instance().setDiscipline().doAutoSync();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(PARAM_ENROLLMENT_CAMPAIGN, getSettings().get(PARAM_ENROLLMENT_CAMPAIGN));
    }
}
