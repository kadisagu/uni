package ru.tandemservice.uniec_fis.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author Andrey Andreev
 * @since 08.08.2016
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniec_fis_2x10x6_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.6")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность ecfOrgUnitPackage4ApplicationsInfo

		// создано обязательное свойство status
		{
			// создать колонку
			tool.createColumn("ecf_oupkg_applications_t", new DBColumn("status_id", DBType.LONG));

			// задать значение по умолчанию из справочника №4 код 2 - "Новое"
			tool.executeUpdate("update ecf_oupkg_applications_t" +
									   " set status_id=(" +
									   " select item.id from ecf_catalog_item_t item" +
									   " where item.fiscatalogcode_p = '4'" +
									   " and item.fisitemcode_p = '2'" +
									   " )" +
									   " where status_id is null");

			// сделать колонку NOT NULL
			tool.setColumnNullable("ecf_oupkg_applications_t", "status_id", false);
		}


    }
}