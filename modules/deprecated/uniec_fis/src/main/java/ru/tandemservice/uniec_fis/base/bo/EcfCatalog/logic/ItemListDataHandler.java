package ru.tandemservice.uniec_fis.base.bo.EcfCatalog.logic;

import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.util.NumberAsStringComparator;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.ui.ItemList.EcfCatalogItemListUI;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class ItemListDataHandler extends EntityComboDataSourceHandler {

    public ItemListDataHandler(String ownerId) {
        super(ownerId, EcfCatalogItem.class);
        this.filter(EcfCatalogItem.title());
    }

    @Override
    protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
    {
        super.applyWhereConditions(alias, dql, context);

        DataWrapper code = context.get(EcfCatalogItemListUI.PARAM_CATALOG_CODE);
        String title = context.get(EcfCatalogItemListUI.PARAM_CATALOG_ITEM_TITLE);

        if (code != null)
            dql.where(eq(property(EcfCatalogItem.fisCatalogCode().fromAlias(alias)), value(code.getTitle())));

        if (title != null)
            dql.where(like(alias, EcfCatalogItem.title(), title));
    }

    @Override
    protected void execute(DQLSelectBuilder builder, String alias, DSOutput output, ExecutionContext context)
    {
        // находим все id
        final DQLExecutionContext qdlContext = new DQLExecutionContext(context.getSession());
        final Iterable<Object[]> rows = CommonDAO.scrollRows(
            builder
            .column(property(alias, "id"))
            .column(property(alias, EcfCatalogItem.fisCatalogCode()))
            .column(property(alias, EcfCatalogItem.fisItemCode()))
            .createStatement(qdlContext)
        );

        // сортируем id
        final List<Long> ids = UniBaseDao.getSortedIds(rows, NumberAsStringComparator.INSTANCE, NumberAsStringComparator.INSTANCE);
        output.setTotalSize(ids.size());

        // выбираем область
        final List<Long> subIds = ids.subList(output.getStartRecord(), Math.min(output.getStartRecord() + output.getCountRecord(), ids.size()));
        output.addRecords(CommonDAO.sort(IUniBaseDao.instance.get().getList(EcfCatalogItem.class, "id", subIds), subIds));
    }

}
