package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.ui.Tab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;

import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.ui.PackageList.EcfOrgUnitPackageList;

/**
 * @author vdanilov
 */
@Configuration
public class EcfOrgUnitTab extends BusinessComponentManager {

    // tab panel
    public static final String TAB_PANEL = "tabPanel";

    @Bean
    public TabPanelExtPoint tabPanelExtPoint() {
        return this.tabPanelExtPointBuilder(TAB_PANEL)
        .addTab(componentTab("packageList", EcfOrgUnitPackageList.class).permissionKey("ui:holder.secModel.orgUnit_viewEcfPackagesTab"))
        .create();
    }
}
