package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnit;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage4AdmissionInfo;

/**
 * @author vdanilov
 */
public interface IEcfOrgUnitPackage4AdmissionDao extends IEcfOrgUnitPackageDao {

    /**
     * Создает пакет (пакет 2: Сведения об объеме и структуре приема)
     * @param ecfOrgUnit
     * @param enrollmentCampaign
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    EcfOrgUnitPackage4AdmissionInfo doCreatePackage(EcfOrgUnit ecfOrgUnit, EnrollmentCampaign enrollmentCampaign);

}
