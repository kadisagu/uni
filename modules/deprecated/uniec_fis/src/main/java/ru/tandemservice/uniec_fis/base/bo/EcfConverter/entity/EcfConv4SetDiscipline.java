package ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity;

import ru.tandemservice.uniec.entity.settings.SetDiscipline;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.gen.EcfConv4SetDisciplineGen;

/**
 * ФИС: Сопоставление: Элемент набора вступительных испытаний
 */
public class EcfConv4SetDiscipline extends EcfConv4SetDisciplineGen
{

    public EcfConv4SetDiscipline() {}
    public EcfConv4SetDiscipline(SetDiscipline discipline, EcfCatalogItem value) {
        setDiscipline(discipline);
        setValue(value);
    }
}