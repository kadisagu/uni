package ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity;

import ru.tandemservice.uniec.entity.catalog.IndividualProgress;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.gen.*;

/** @see ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.gen.EcfConv4IndAchievementGen */
public class EcfConv4IndAchievement extends EcfConv4IndAchievementGen
{

    public EcfConv4IndAchievement() {}
    public EcfConv4IndAchievement(IndividualProgress achievement, EcfCatalogItem value) {
        setIndividualAchievement(achievement);
        setValue(value);
    }
}