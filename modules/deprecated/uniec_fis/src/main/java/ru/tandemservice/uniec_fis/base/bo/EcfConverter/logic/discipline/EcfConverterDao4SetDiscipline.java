package ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.discipline;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniec.entity.settings.ConversionScale;
import ru.tandemservice.uniec.entity.settings.SetDiscipline;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.gen.EcfCatalogItemGen;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4SetDiscipline;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.gen.EcfConv4SetDisciplineGen;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.EcfConverterBaseDao;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.isNotNull;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author vdanilov
 */
public class EcfConverterDao4SetDiscipline extends EcfConverterBaseDao<SetDiscipline, Long> implements IEcfConverterDao4SetDiscipline {

    @Override
    public String getEcfCatalogCode() {
        return "1";
    }

    @Override
    public List<SetDiscipline> getEntityList() {
        final List<SetDiscipline> list = new DQLSelectBuilder()
        .fromEntity(SetDiscipline.class, "x").column(property("x"))
        .order(property(SetDiscipline.enrollmentCampaign().educationYear().intValue().fromAlias("x")))
        .order(property(SetDiscipline.enrollmentCampaign().title().fromAlias("x")))
        .createStatement(this.getSession()).list();
        Collections.sort(list, SetDiscipline.COMPARATOR);
        return list;
    }

    @Override
    protected Long getItemMapKey(final SetDiscipline entity) {
        return entity.getId();
    }

    @Override
    protected Map<Long, Long> getItemMap() {
        return EcfConverterBaseDao.<Long, Long>map(scrollRows(
            new DQLSelectBuilder()
            .fromEntity(EcfConv4SetDiscipline.class, "x")
            .column(property(EcfConv4SetDiscipline.discipline().id().fromAlias("x")), "dsc_id")
            .column(property(EcfConv4SetDiscipline.value().id().fromAlias("x")), "value_id")
            .createStatement(this.getSession())
        ));
    }

    @Override
    public void update(final Map<SetDiscipline, EcfCatalogItem> values, final boolean clearNulls) {

        // формируем перечень требуемых строк
        final List<EcfConv4SetDiscipline> targetRecords = new ArrayList<EcfConv4SetDiscipline>(values.size());
        for (final Map.Entry<SetDiscipline, EcfCatalogItem> e: values.entrySet()) {
            if (null != e.getValue()) {
                targetRecords.add(new EcfConv4SetDiscipline(e.getKey(), e.getValue()));
            }
        }

        // обновляем данные в базе
        new MergeAction.SessionMergeAction<EcfConv4SetDisciplineGen.NaturalId, EcfConv4SetDiscipline>() {
            @Override protected EcfConv4SetDisciplineGen.NaturalId key(final EcfConv4SetDiscipline source) {
                return (EcfConv4SetDisciplineGen.NaturalId)source.getNaturalId();
            }
            @Override protected EcfConv4SetDiscipline buildRow(final EcfConv4SetDiscipline source) {
                return new EcfConv4SetDiscipline(source.getDiscipline(), source.getValue());
            }
            @Override protected void fill(final EcfConv4SetDiscipline target, final EcfConv4SetDiscipline source) {
                target.update(source, false);
            }
            @Override protected void doDeleteRecord(final EcfConv4SetDiscipline databaseRecord) {
                if (clearNulls) { super.doDeleteRecord(databaseRecord); }
            }
        }.merge(this.getList(EcfConv4SetDiscipline.class), targetRecords);

    }

    @Override
    protected List<EcfCatalogItem> getUsedFisItemList()
    {
        return new DQLSelectBuilder().fromEntity(EcfConv4SetDiscipline.class, "b").column(property(EcfConv4SetDiscipline.value().fromAlias("b")))
                .where(isNotNull(property(EcfConv4SetDiscipline.value().fromAlias("b"))))
                .createStatement(getSession()).list();
    }

    @Override
    public void doAutoSync()
    {
        final Map<SetDiscipline, EcfCatalogItem> values = new HashMap<SetDiscipline, EcfCatalogItem>();
        for (ConversionScale scale: getList(ConversionScale.class)) {
            String fisItemCode = StringUtils.trimToNull(scale.getSubject().getSubjectFISCode());
            if (null == fisItemCode) { continue; }

            EcfCatalogItem item = getByNaturalId(new EcfCatalogItemGen.NaturalId(getEcfCatalogCode(), fisItemCode));
            if (null == item) { continue; }

            values.put(scale.getDiscipline(), item);
        }

        // для которых еще не выбран элемент ФИС
        for (EcfConv4SetDiscipline discipline : getList(EcfConv4SetDiscipline.class))
        {
            if (discipline.getValue() != null)
                values.put(discipline.getDiscipline(), discipline.getValue());
        }

        this.update(values, false);
    }


}
