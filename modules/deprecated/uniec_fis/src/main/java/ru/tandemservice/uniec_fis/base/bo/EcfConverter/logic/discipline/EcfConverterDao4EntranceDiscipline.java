package ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.discipline;

import com.google.common.collect.ComparisonChain;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline;
import ru.tandemservice.uniec.entity.settings.ConversionScale;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.gen.EcfCatalogItemGen;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4EntranceDiscipline;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.EcfConv4SetDiscipline;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.entity.gen.EcfConv4EntranceDisciplineGen;
import ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic.EcfConverterBaseDao;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.isNotNull;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author vdanilov
 */
public class EcfConverterDao4EntranceDiscipline extends EcfConverterBaseDao<EntranceDiscipline, Long> implements IEcfConverterDao4EntranceDiscipline
{

    @Override
    public String getEcfCatalogCode()
    {
        return "1";
    }

    @Override
    public List<EntranceDiscipline> getEntityList()
    {
        List<EntranceDiscipline> disciplines = new DQLSelectBuilder()
                .fromEntity(EntranceDiscipline.class, "x")
                .column(property("x"))
                .createStatement(this.getSession()).list();

        disciplines.sort((d1, d2) -> ComparisonChain.start()
                .compare(d1.getEnrollmentDirection().getEnrollmentCampaign(), d2.getEnrollmentDirection().getEnrollmentCampaign(), EnrollmentCampaign.COMPARATOR)
                .compare(d1, d2, Comparator.comparing(d -> d.getDiscipline().getFullTitle()))
                .compare(d1, d2, Comparator.comparing(d -> d.getEnrollmentDirection().getTitle()))
                .result());

        return disciplines;
    }

    @Override
    protected Long getItemMapKey(final EntranceDiscipline entity)
    {
        return entity.getId();
    }

    @Override
    protected Map<Long, Long> getItemMap()
    {
        return EcfConverterBaseDao.<Long, Long>map(scrollRows(
                new DQLSelectBuilder()
                        .fromEntity(EcfConv4EntranceDiscipline.class, "x")
                        .column(property("x", EcfConv4EntranceDiscipline.discipline().id()), "dsc_id")
                        .column(property("x", EcfConv4EntranceDiscipline.value().id()), "value_id")
                        .createStatement(this.getSession())
        ));
    }

    @Override
    public void update(final Map<EntranceDiscipline, EcfCatalogItem> values, final boolean clearNulls)
    {

        // формируем перечень требуемых строк
        final List<EcfConv4EntranceDiscipline> targetRecords = values.entrySet().stream()
                .filter(e -> e.getValue() != null)
                .map(e -> new EcfConv4EntranceDiscipline(e.getKey(), e.getValue()))
                .collect(Collectors.toList());

        // обновляем данные в базе
        new MergeAction.SessionMergeAction<EcfConv4EntranceDisciplineGen.NaturalId, EcfConv4EntranceDiscipline>()
        {
            @Override
            protected EcfConv4EntranceDisciplineGen.NaturalId key(final EcfConv4EntranceDiscipline source)
            {
                return (EcfConv4EntranceDisciplineGen.NaturalId) source.getNaturalId();
            }

            @Override
            protected EcfConv4EntranceDiscipline buildRow(final EcfConv4EntranceDiscipline source)
            {
                return new EcfConv4EntranceDiscipline(source.getDiscipline(), source.getValue());
            }

            @Override
            protected void fill(final EcfConv4EntranceDiscipline target, final EcfConv4EntranceDiscipline source)
            {
                target.update(source, false);
            }

            @Override
            protected void doDeleteRecord(final EcfConv4EntranceDiscipline databaseRecord)
            {
                if (clearNulls)
                {
                    super.doDeleteRecord(databaseRecord);
                }
            }
        }.merge(this.getList(EcfConv4EntranceDiscipline.class), targetRecords);

    }

    @Override
    protected List<EcfCatalogItem> getUsedFisItemList()
    {
        return new DQLSelectBuilder()
                .fromEntity(EcfConv4EntranceDiscipline.class, "b")
                .column(property("b", EcfConv4EntranceDiscipline.value()))
                .where(isNotNull(property("b", EcfConv4SetDiscipline.value())))
                .createStatement(getSession()).list();
    }

    @Override
    public void doAutoSync()
    {
        final Map<EntranceDiscipline, EcfCatalogItem> values = new HashMap<>();

        final Map<Long, List<EntranceDiscipline>> entranceDisBySetDis = getList(EntranceDiscipline.class).stream()
                .collect(Collectors.groupingBy(dis -> dis.getDiscipline().getId(), Collectors.mapping(dis -> dis, Collectors.toList())));

        getList(ConversionScale.class)
                .forEach(scale -> {
                    List<EntranceDiscipline> entranceDisciplines = entranceDisBySetDis.get(scale.getDiscipline().getId());
                    if (CollectionUtils.isEmpty(entranceDisciplines)) return;

                    String fisItemCode = StringUtils.trimToNull(scale.getSubject().getSubjectFISCode());
                    if (fisItemCode == null) return;

                    final EcfCatalogItem item = getByNaturalId(new EcfCatalogItemGen.NaturalId(getEcfCatalogCode(), fisItemCode));
                    if (null == item) return;

                    entranceDisciplines.forEach(dis -> values.put(dis, item));
                });

        // для которых еще не выбран элемент ФИС
        getList(EcfConv4EntranceDiscipline.class).stream()
                .filter(discipline -> discipline.getValue() != null)
                .forEach(discipline -> values.put(discipline.getDiscipline(), discipline.getValue()));

        this.update(values, false);
    }
}
