package ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.daemon;

import org.mvel2.MVEL;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage4DelAdmissionOrders;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackage4DelApplications;
import ru.tandemservice.uniec_fis.base.bo.EcfOrgUnit.entity.EcfOrgUnitPackageIOLog;
import ru.tandemservice.uniec_fis.util.FisServiceImpl;
import ru.tandemservice.uniec_fis.util.IFisService;
import ru.tandemservice.uniec_fis.util.IFisService.FisConnection;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class EcfOrgUnitPackageDaemonBean extends UniBaseDao implements IEcfOrgUnitPackageDaemonBean {

    public static boolean active = true;
    public static int ITERATIONS = 4;

    public static final SyncDaemon DAEMON = new SyncDaemon(EcfOrgUnitPackageDaemonBean.class.getName(), 15, IEcfOrgUnitPackageDaemonBean.GLOBAL_DAEMON_LOCK) {
        @Override protected void main()
        {
            if (!active) { return; }
            final IEcfOrgUnitPackageDaemonBean dao = IEcfOrgUnitPackageDaemonBean.instance.get();

            // сначала пробуем передать данные в ФИС (по каждому подразделению и ПК отдельно, до тех пор пока там эти данные есть или не произошла ошибка)
            Debug.begin("send-pkg");
            try {
                int iterations = ITERATIONS;
                final List<IEcfOrgUnitEnrollmentCampaign> ecfOuEcList = dao.getEcfOrgUnitEnrollmentCampaignList(); // фиксируем список на момент запуска
                while ((ecfOuEcList.size() > 0) && (iterations --> 0)) {
                    for (final Iterator<IEcfOrgUnitEnrollmentCampaign> it = ecfOuEcList.iterator(); it.hasNext(); ) {
                        if (!this.sendPackage(it.next())) { it.remove(); }
                    }
                }
            } catch (final Exception t) {
                Debug.exception(t.getMessage(), t);
            } finally {
                Debug.end();
            }

            // затем пробуем получить ответ (по каждому подразделению и ПК отдельно, до тех пор пока там эти данные есть или не произошла ошибка)
            Debug.begin("get-result");
            try {
                int iterations = ITERATIONS;
                final List<IEcfOrgUnitEnrollmentCampaign> ecfOuEcList = dao.getEcfOrgUnitEnrollmentCampaignList(); // фиксируем список на момент запуска
                while ((ecfOuEcList.size() > 0) && (iterations --> 0)) {
                    for (final Iterator<IEcfOrgUnitEnrollmentCampaign> it = ecfOuEcList.iterator(); it.hasNext(); ) {
                        if (!this.getResult(it.next())) { it.remove(); }
                    }
                }
            } catch (final Exception t) {
                Debug.exception(t.getMessage(), t);
            } finally {
                Debug.end();
            }

            // сбрасываем пакеты, которые уже прошли обработку
            Debug.begin("do-archive");
            try {
                dao.doArchivePackages();
            } catch (final Exception t) {
                Debug.exception(t.getMessage(), t);
            } finally {
                Debug.end();
            }
        }

        // пробуем послать пакет в фис, если не получилось или ошибка - то false
        private boolean sendPackage(final IEcfOrgUnitEnrollmentCampaign ecfOuEc) {
            final IEcfOrgUnitPackageDaemonBean dao = IEcfOrgUnitPackageDaemonBean.instance.get();
            try
            {
                final Long nextPackageId = dao.getNextSendQueuePackageId(ecfOuEc);
                if (null == nextPackageId) { return false; } // нечего отправлять

                try {
                    if (!dao.tryToSendPackage(nextPackageId)) {
                        return false; // ошибка
                    }
                } catch (final Exception t) {
                    final String message = t.getMessage()+": package-id="+nextPackageId;
                    this.logger.error(message, t);
                    Debug.exception(message, t);
                    return false; // ошибка
                }

                return true; // все, что хотели передать - передали
            }
            catch (final Exception t)
            {
                Debug.exception(t.getMessage(), t);
                return false; // ошибка
            }
        }

        // пробуем получить данные о результатах из фис, если не получилось или ошибка - то false
        private boolean getResult(final IEcfOrgUnitEnrollmentCampaign ecfOuEc) {
            final IEcfOrgUnitPackageDaemonBean dao = IEcfOrgUnitPackageDaemonBean.instance.get();
            try
            {
                final List<Long> packageIds = dao.getResultQueuePackageIds(ecfOuEc);
                if (packageIds.isEmpty()) { return false; } // нечего запрашивать

                for (final Long packageId: packageIds) {
                    try {
                        if (!dao.tryToGetResult(packageId)) {
                            return false; // произошла обишка (прекращаем сразу)
                        }
                    } catch (final Exception t) {
                        final String message = t.getMessage()+": package-id="+packageIds;
                        this.logger.error(message, t);
                        Debug.exception(message, t);
                        return false; // ошибка
                    }
                }

                return true; // все, что хотели передать - передали
            }
            catch (final Exception t)
            {
                Debug.exception(t.getMessage(), t);
                return false; // ошибка
            }
        }
    };

    @Override
    protected void initDao()
    {
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, EcfOrgUnitPackage.class, DAEMON.getAfterCompleteWakeUpListener());
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EcfOrgUnitPackage.class, DAEMON.getAfterCompleteWakeUpListener());
    }


    protected interface CallResult<T> {
        Long getLogEntryId();
        T getResult();
    }

    /* фис-о-вызыватель */
    protected <T> CallResult<T> call(final Class<T> responseClass, final String urlPostfix, final Object root, final Long packageId)
    {
        Debug.begin("EcfOrgUnitPackageDaemonBean.call");
        try {

            final T result;
            final EcfOrgUnitPackageIOLog io = new EcfOrgUnitPackageIOLog();
            io.setOperationDate(new Date());

            final FisConnection connection = IFisService.instance.get().connect(urlPostfix);
            try
            {
                // формируем данные для отправки
                final byte[] xmlRequest = FisServiceImpl.toXml(root);
                io.setXmlRequest(xmlRequest);

                // еще один try-catch для того что бы отловить ошибки отрпавки
                final byte[] xmlResponse;
                try
                {
                    // сохранем результат вызова
                    xmlResponse = connection.call(xmlRequest);
                    io.setXmlResponse(xmlResponse);
                }
                catch (final Throwable t)
                {
                    io.setStatus(EcfOrgUnitPackageIOLog.IO_STATUS_FATAL_ERROR);
                    throw t;
                }

                // расшифровываем результат вызова
                result = FisServiceImpl.fromXml(responseClass, xmlResponse);

                io.setStatus(EcfOrgUnitPackageIOLog.IO_STATUS_COMPLETE);
            }
            catch (final IFisService.FisError e)
            {
                // ошибка ФИС
                io.registerError(e.getMessage());
                io.setStatus(EcfOrgUnitPackageIOLog.IO_STATUS_FIS_ERROR);
                // если есть код, то ошибка по шаблону xsd ФИС
                if (e.getFisRespErrorCode() != null)
                {
                    io.setFisErrorText(e.getFisRespErrorText());
                    io.setFisErrorCode(e.getFisRespErrorCode());
                }
                return null;
            }
            catch (final Exception t)
            {
                // иная ошибка - придется сохранять stackTrace
                io.registerError(t);
                if (io.getStatus() == null)
                {
                    io.setStatus(EcfOrgUnitPackageIOLog.IO_STATUS_FIS_ERROR);
                    io.setFisErrorText(t.getMessage());
                }
                if (this.logger.isInfoEnabled()) {
                    final EcfOrgUnitPackage pkg = this.get(EcfOrgUnitPackage.class, packageId);
                    final StringBuilder sb = new StringBuilder();
                    sb.append("\nou=").append(pkg.getEcfOrgUnit().getOrgUnit().getTitle()).append(", id=").append(pkg.getEcfOrgUnit().getId());
                    sb.append("\nec=").append(pkg.getEnrollmentCampaign().getTitle()).append(", id=").append(pkg.getEnrollmentCampaign().getId());
                    sb.append("\nurl=").append(urlPostfix);
                    sb.append("\nrequest=").append(connection.getRequest());
                    sb.append("\nresponse=").append(connection.getResponse());
                    sb.append("\nerror-class=").append(t.getClass());
                    sb.append("\nerror-message=").append(t.getMessage());
                    final String message = sb.toString();
                    Debug.exception(message, t);
                    this.logger.error(message, t);
                }
                return null;
            }
            finally
            {
                // после того, как произошло обращение к ФИС, сохраняем в базу и возвращаем объект
                // в отдельной транзакции (сохраняем операцию в базу), ВСЕГДА
                io.setOperationUrl(connection.getUrl().toString());
                IUniBaseDao.instance.get().doInTransaction(session -> {
                    io.setOwner((EcfOrgUnitPackage)session.load(EcfOrgUnitPackage.class, packageId));
                    session.save(io);
                    return null;
                });
            }

            final Long id = io.getId();
            return new CallResult<T>() {
                @Override public Long getLogEntryId() { return id; }
                @Override public T getResult() { return result; }
            };

        } finally {
            Debug.end();
        }
    }


    ///////////////////////
    // служебные функции //
    ///////////////////////

    @Override
    public List<IEcfOrgUnitEnrollmentCampaign> getEcfOrgUnitEnrollmentCampaignList() {
        Debug.begin("EcfOrgUnitPackageDaemonBean.getEcfOrgUnitEnrollmentCampaignList");
        try {
            final List<Object[]> rows = new DQLSelectBuilder().predicate(DQLPredicateType.distinct)
            .fromEntity(EcfOrgUnitPackage.class, "p")
            .column(property(EcfOrgUnitPackage.ecfOrgUnit().id().fromAlias("p")))
            .column(property(EcfOrgUnitPackage.enrollmentCampaign().id().fromAlias("p")))
            .where(isNull(property(EcfOrgUnitPackage.archiveDate().fromAlias("p")))) /* пакет не архивный */
            .where(isNotNull(property(EcfOrgUnitPackage.queueDate().fromAlias("p")))) /* пакет в очереди */
            .createStatement(this.getSession()).list();

            final List<IEcfOrgUnitEnrollmentCampaign> result = new ArrayList<>(rows.size());
            for (final Object[] row: rows) {
                final Long ecfOuId = (Long)row[0];
                final Long ecfEcId = (Long)row[1];
                result.add(new IEcfOrgUnitEnrollmentCampaign() {
                    @Override public Long getEcfOrgUnitId() { return ecfOuId; }
                    @Override public Long getEnrollmentCampaignId() { return ecfEcId; }
                });
            }

            return result;
        } finally {
            Debug.end();
        }
    }


    @Override
    public void doArchivePackages() {

        // если в пакете все хорошо - отправляем в архив
        this.executeAndClear(
            new DQLUpdateBuilder(EcfOrgUnitPackage.class)
            .set(EcfOrgUnitPackage.P_ARCHIVE_DATE, value(new Date(), PropertyType.TIMESTAMP))
            .where(isNull(property(EcfOrgUnitPackage.P_ARCHIVE_DATE)))
            .where(isNotNull(property(EcfOrgUnitPackage.pkgImportSuccess())))
            .where(isNotNull(property(EcfOrgUnitPackage.pkgResultSuccess())))
        );

    }


    ///////////////////////////
    // отправка пакета в ФИС //
    ///////////////////////////

    @Override
    public Long getNextSendQueuePackageId(final IEcfOrgUnitEnrollmentCampaign ecfOuEc) {
        return new DQLSelectBuilder()
        .fromEntity(EcfOrgUnitPackage.class, "p").column(property("p.id"))
        .where(eq(property(EcfOrgUnitPackage.ecfOrgUnit().id().fromAlias("p")), value(ecfOuEc.getEcfOrgUnitId())))
        .where(eq(property(EcfOrgUnitPackage.enrollmentCampaign().id().fromAlias("p")), value(ecfOuEc.getEnrollmentCampaignId())))
        .order(property(EcfOrgUnitPackage.creationDate().fromAlias("p"))) /* сортируем по дате формирования (надо взять самый первый) */
        .where(isNull(property(EcfOrgUnitPackage.pkgImportSuccess().fromAlias("p")))) /* пакет не передавался */
        .where(isNull(property(EcfOrgUnitPackage.archiveDate().fromAlias("p")))) /* пакет не архивный */
        .where(isNotNull(property(EcfOrgUnitPackage.queueDate().fromAlias("p")))) /* пакет в очереди */
        .createStatement(this.getSession())
        .setMaxResults(1).<Long>uniqueResult();
    }

    /**
     * Пробует отправить пакет в ФИС (на время сохранеия объектов открывает транзакции)
     */
    @Override
    public boolean tryToSendPackage(final Long packageId)
    {
        Debug.begin("EcfOrgUnitPackageDaemonBean.tryToSendPackage");
        try {
            final EcfOrgUnitPackage pkg = this.get(EcfOrgUnitPackage.class, packageId);
            if (null != pkg.getPkgImportSuccess()) { return false; } // oops (пакет уже удачно передан)

            // запрос в ФИС
            final CallResult result = tryCall(pkg);
            if (result == null) return false; /* ошибка */

            final Long id = (Long) MVEL.getProperty("packageID", result.getResult());
            // (транзакцию открывать надо именно здесь)
            // если все ок, пробуем сохранить
            return IUniBaseDao.instance.get().doInTransaction(session -> {
                final EcfOrgUnitPackage pkg1 = (EcfOrgUnitPackage)session.get(EcfOrgUnitPackage.class, packageId);
                pkg1.setEcfPackageID(id);
                pkg1.setPkgImportSuccess((EcfOrgUnitPackageIOLog) session.load(EcfOrgUnitPackageIOLog.class, result.getLogEntryId()));
                session.saveOrUpdate(pkg1);
                return true;
            });

        } finally {
            Debug.end();
        }
    }

    // Если пакет на Удаление, то для него другая команда и другой ответ.
    protected CallResult tryCall(final EcfOrgUnitPackage pkg)
    {
        if (pkg instanceof EcfOrgUnitPackage4DelAdmissionOrders || pkg instanceof EcfOrgUnitPackage4DelApplications)
        {
            final CallResult<fis.schema.pkg.del.resp.DeletePackageInfo> result;
            try {
                // формируем запрос в ФИС (обновляем реквизиты доступа)
                final fis.schema.pkg.del.req.Root root = FisServiceImpl.fromXml(fis.schema.pkg.del.req.Root.class, pkg.getXmlPackageRequest());
                root.getAuthData().setLogin(pkg.getEcfOrgUnit().getLogin());
                root.getAuthData().setPass(pkg.getEcfOrgUnit().getPassword());

                // отправляем пакет в ФИС
                result = this.call(fis.schema.pkg.del.resp.DeletePackageInfo.class, "delete", root, pkg.getId());
                if (null == result) { return null; /* ошибка */ }
            } catch (final Throwable t) {
                throw CoreExceptionUtils.getRuntimeException(t);
            }

            return result;
        }
        else
        {
            final CallResult<fis.schema.pkg.send.resp.ImportPackageInfo> result;
            try {
                // формируем запрос в ФИС (обновляем реквизиты доступа)
                final fis.schema.pkg.send.req.Root root = FisServiceImpl.fromXml(fis.schema.pkg.send.req.Root.class, pkg.getXmlPackageRequest());
                root.getAuthData().setLogin(pkg.getEcfOrgUnit().getLogin());
                root.getAuthData().setPass(pkg.getEcfOrgUnit().getPassword());

                // отправляем пакет в ФИС
                result = this.call(fis.schema.pkg.send.resp.ImportPackageInfo.class, "import", root, pkg.getId());
                if (null == result) { return null; /* ошибка */ }
            } catch (final Throwable t) {
                throw CoreExceptionUtils.getRuntimeException(t);
            }

            // если мы смогли прочитать id-пакета, сохраняем его (если в результате нет id-пакета, то загрузка не считается успешной)
            final Long id = result.getResult().getPackageID();
            if (null == id) { return null; } /* ошибка */

            return result;
        }
    }

    /////////////////////////////////////
    // получение результатов обработки //
    /////////////////////////////////////

    @Override
    public List<Long> getResultQueuePackageIds(final IEcfOrgUnitEnrollmentCampaign ecfOuEc) {
        return new DQLSelectBuilder()
        .fromEntity(EcfOrgUnitPackage.class, "p").column(property("p.id"))
        .where(eq(property(EcfOrgUnitPackage.ecfOrgUnit().id().fromAlias("p")), value(ecfOuEc.getEcfOrgUnitId())))
        .where(eq(property(EcfOrgUnitPackage.enrollmentCampaign().id().fromAlias("p")), value(ecfOuEc.getEnrollmentCampaignId())))
        .order(property(EcfOrgUnitPackage.pkgImportSuccess().operationDate().fromAlias("p"))) /* сначало сортируем по дате отправки пакета */
        .order(property(EcfOrgUnitPackage.creationDate().fromAlias("p"))) /* затем, сортируем по дате формирования (надо взять первые) */
        .where(isNotNull(property(EcfOrgUnitPackage.ecfPackageID().fromAlias("p")))) /* у пакета должен быть id из ФИС */
        .where(isNull(property(EcfOrgUnitPackage.pkgResultSuccess().fromAlias("p")))) /* ответ не получен */
        .where(isNull(property(EcfOrgUnitPackage.archiveDate().fromAlias("p")))) /* пакет не архивный */
        .where(isNotNull(property(EcfOrgUnitPackage.queueDate().fromAlias("p")))) /* пакет в очереди */
        .createStatement(this.getSession())
        .setMaxResults(5).<Long>list();
    }

    /**
     * Пробует получить ответ из ФИС (на время сохранеия объектов открывает транзакции)
     */
    @Override
    public boolean tryToGetResult(final Long packageId)
    {
        Debug.begin("EcfOrgUnitPackageDaemonBean.tryToGetResult");
        try {
            final EcfOrgUnitPackage pkg = this.get(EcfOrgUnitPackage.class, packageId);
            if (null == pkg.getEcfPackageID()) { return false; } // oops (нет номера пакета)

            // запрос в ФИС
            final CallResult result = tryCallResult(pkg);
            if (null == result) { return false; /* ошибка */ }

            // (транзакцию открывать надо именно здесь)
            // пробуем сохранить в базу
            return IUniBaseDao.instance.get().doInTransaction(session -> {
                final EcfOrgUnitPackage pkg1 = (EcfOrgUnitPackage)session.get(EcfOrgUnitPackage.class, packageId);
                pkg1.setPkgResultSuccess((EcfOrgUnitPackageIOLog) session.load(EcfOrgUnitPackageIOLog.class, result.getLogEntryId()));
                session.saveOrUpdate(pkg1);
                return true;
            });

        } finally {
            Debug.end();
        }
    }

    // Если пакет на Удаление, то для него другая команда и другой ответ.
    protected CallResult tryCallResult(final EcfOrgUnitPackage pkg)
    {
        if (pkg instanceof EcfOrgUnitPackage4DelAdmissionOrders || pkg instanceof EcfOrgUnitPackage4DelApplications)
        {
            final CallResult<fis.schema.pkg.del.result.resp.DeleteResultPackage> result;
            try {
                // формируем запрос в ФИС
                final fis.schema.pkg.del.result.req.Root root = new fis.schema.pkg.del.result.req.Root();
                root.setAuthData(new fis.schema.pkg.del.result.req.AuthData());
                root.getAuthData().setLogin(pkg.getEcfOrgUnit().getLogin());
                root.getAuthData().setPass(pkg.getEcfOrgUnit().getPassword());
                root.setGetResultDeleteApplication(new fis.schema.pkg.del.result.req.GetResultDeleteApplication());
                root.getGetResultDeleteApplication().setPackageID(pkg.getEcfPackageID());

                // отправляем запрос на результат в ФИС
                result = this.call(fis.schema.pkg.del.result.resp.DeleteResultPackage.class, "delete/result", root, pkg.getId());
                if (null == result) { return null; /* ошибка */ }
            } catch (final Throwable t) {
                throw CoreExceptionUtils.getRuntimeException(t);
            }

            return result;
        }
        else
        {
            final CallResult<fis.schema.pkg.result.resp.ImportResultPackage> result;
            try {
                // формируем запрос в ФИС
                final fis.schema.pkg.result.req.Root root = new fis.schema.pkg.result.req.Root();
                root.setAuthData(new fis.schema.pkg.result.req.AuthData());
                root.getAuthData().setLogin(pkg.getEcfOrgUnit().getLogin());
                root.getAuthData().setPass(pkg.getEcfOrgUnit().getPassword());
                root.setGetResultImportApplication(new fis.schema.pkg.result.req.GetResultImportApplication());
                root.getGetResultImportApplication().setPackageID(pkg.getEcfPackageID());

                // отправляем запрос на результат в ФИС
                result = this.call(fis.schema.pkg.result.resp.ImportResultPackage.class, "import/result", root, pkg.getId());
                if (null == result) { return null; /* ошибка */ }
            } catch (final Throwable t) {
                throw CoreExceptionUtils.getRuntimeException(t);
            }

            // если смогли получить данные (и данные те самые), то сохраняем все в базу
            if (!pkg.getEcfPackageID().equals(result.getResult().getPackageID())) { return null; }

            return result;
        }
    }
}
