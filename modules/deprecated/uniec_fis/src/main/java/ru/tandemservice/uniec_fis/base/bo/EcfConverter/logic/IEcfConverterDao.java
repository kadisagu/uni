package ru.tandemservice.uniec_fis.base.bo.EcfConverter.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.uniec_fis.base.bo.EcfCatalog.entity.EcfCatalogItem;

import java.util.Map;

/**
 * @author vdanilov
 */
public interface IEcfConverterDao<T extends IEntity> extends INeedPersistenceSupport {

    /** @return converter.ecfCatalogCode: код справочника ФИС (из которого будут выбираться значения для сопоставления) */
    String getEcfCatalogCode();

    /** @return IEcfConverter, который хранит в себе данные сопоставления */
    IEcfConverter<T> getConverter();

    /** сохраняет сопоставление (полностью) в базу */
    @Transactional(readOnly=false, propagation=Propagation.REQUIRED)
    void update(Map<T, EcfCatalogItem> values, boolean clearNulls);

    /** сохраняет сопоставление (только указанный ключ) в базу */
    @Transactional(readOnly=false, propagation=Propagation.REQUIRED)
    void update(T key, EcfCatalogItem value);


    /**
     * Проверяет на использование элементов справочника в пользовательских конвертерах.
     * Если элемент справочника используется и он изменился или удален, то падает ошибка.
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    void validateUsedInUserConverter(Map<Long, String> itemMap);
}
