#!/bin/bash

if true; then

XJC="/opt/java/jdk170/bin/xjc -enableIntrospection -contentForWildcard"

# справочники
$XJC -p fis.schema.catalog_list.req catalog-list-req.xsd 
$XJC -p fis.schema.catalog_list.resp catalog-list-resp.xsd 
$XJC -p fis.schema.catalog_items.req catalog-items-req.xsd 
$XJC -p fis.schema.catalog_items.resp catalog-items-resp.xsd 
$XJC -p fis.schema.catalog_items.resp.olymp catalog-items-resp-olymp.xsd 
$XJC -p fis.schema.catalog_items.resp.spec catalog-items-resp-spec.xsd 

# пакет (отправка)
$XJC -p fis.schema.pkg.send.req pkg-send-req.xsd 
$XJC -p fis.schema.pkg.send.resp pkg-send-resp.xsd 

# пакет (ответ)
$XJC -p fis.schema.pkg.result.req pkg-result-req.xsd 
$XJC -p fis.schema.pkg.result.resp pkg-result-resp.xsd 

fi

# убираем огромные комменты в классах (чтобы ide не снесло бошку)

cd fis
find -type f | grep -E "[.]java$" | while read f; do
 cp "$f" "$f.orig" && cat "$f.orig" | sed '/<pre>/,/<[/]pre>/d' > "$f"
done