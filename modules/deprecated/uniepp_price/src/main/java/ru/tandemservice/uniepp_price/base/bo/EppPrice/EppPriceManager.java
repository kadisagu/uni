/* $Id$ */
package ru.tandemservice.uniepp_price.base.bo.EppPrice;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

import ru.tandemservice.uniepp_price.base.bo.EppPrice.logic.EppPriceDao;
import ru.tandemservice.uniepp_price.base.bo.EppPrice.logic.IEppPriceDao;

/**
 * @author Vasily Zhukov
 * @since 30.08.2011
 */
@Configuration
public class EppPriceManager extends BusinessObjectManager
{
    public static EppPriceManager instance()
    {
        return instance(EppPriceManager.class);
    }
    
    @Bean
    public IEppPriceDao dao()
    {
        return new EppPriceDao();
    }
}
