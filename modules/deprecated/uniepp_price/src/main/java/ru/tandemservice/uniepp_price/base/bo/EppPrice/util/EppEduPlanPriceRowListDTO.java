/* $Id$ */
// Copyright 2006-2012 Tandem Service Software
package ru.tandemservice.uniepp_price.base.bo.EppPrice.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author Vasily Zhukov
 * @since 07.02.2012
 */
public class EppEduPlanPriceRowListDTO
{
    private EppEduPlanPriceRowDTO _rootBlockDTO;         // root-блок
    private List<EppEduPlanPriceRowDTO> _rootRowsDTO;    // вложенные IEppEpvRegistryRow в root-блок 

    private List<EppEduPlanPriceRowDTO> _listBlockDTO;                // обычные блоки
    private Map<Long, List<EppEduPlanPriceRowDTO>> _block2rowsDTOMap; // обычный блок -> вложенные в него IEppEpvRegistryRow

    /**
     * итоговый список, сформированный так:
     * |_root-блок
     * | |
     * | |_список вложенных IEppEpvRegistryRow в root-блок
     * |
     * |_обычный блок
     * | |
     * | |_список вложенных IEppEpvRegistryRow в обычный блок
     * ...
     * |_обычный блок
     * |
     * |_список вложенных IEppEpvRegistryRow в обычный блок
     */
    private List<EppEduPlanPriceRowDTO> _listDTO;

    public EppEduPlanPriceRowListDTO(EppEduPlanPriceRowDTO rootBlockDTO, List<EppEduPlanPriceRowDTO> rootRowsDTO, List<EppEduPlanPriceRowDTO> listBlockDTO, Map<Long, List<EppEduPlanPriceRowDTO>> block2rowsDTOMap)
    {
        _rootBlockDTO = rootBlockDTO;
        _rootRowsDTO = Collections.unmodifiableList(rootRowsDTO);
        _listBlockDTO = Collections.unmodifiableList(listBlockDTO);
        _block2rowsDTOMap = Collections.unmodifiableMap(block2rowsDTOMap);

        _listDTO = new ArrayList<EppEduPlanPriceRowDTO>();
        _listDTO.add(rootBlockDTO);
        _listDTO.addAll(_rootRowsDTO);
        for (EppEduPlanPriceRowDTO block : listBlockDTO)
        {
            _listDTO.add(block);
            _listDTO.addAll(_block2rowsDTOMap.get(block.getId()));
        }
        _listDTO = Collections.unmodifiableList(_listDTO);
    }

    public EppEduPlanPriceRowDTO findBlockDTO(Long versionBlockId)
    {
        // проверяем root-блок
        if (_rootBlockDTO.getId().equals(versionBlockId))
            return _rootBlockDTO;

        // проверяем остальные блоки
        for (EppEduPlanPriceRowDTO row : _listBlockDTO)
            if (row.getId().equals(versionBlockId))
                return row;

        // ничего не нашли
        return null;
    }

    // Getters

    public EppEduPlanPriceRowDTO getRootBlockDTO()
    {
        return _rootBlockDTO;
    }

    public List<EppEduPlanPriceRowDTO> getRootRowsDTO()
    {
        return _rootRowsDTO;
    }

    public List<EppEduPlanPriceRowDTO> getListBlockDTO()
    {
        return _listBlockDTO;
    }

    public Map<Long, List<EppEduPlanPriceRowDTO>> getBlock2rowsDTOMap()
    {
        return _block2rowsDTOMap;
    }

    public List<EppEduPlanPriceRowDTO> getListDTO()
    {
        return _listDTO;
    }
}
