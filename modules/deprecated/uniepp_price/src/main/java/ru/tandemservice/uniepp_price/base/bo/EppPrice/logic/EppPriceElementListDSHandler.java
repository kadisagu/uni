/* $Id$ */
package ru.tandemservice.uniepp_price.base.bo.EppPrice.logic;

import java.util.List;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;

/**
 * @author Vasily Zhukov
 * @since 30.08.2011
 */
public class EppPriceElementListDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String LIST = "list";

    public EppPriceElementListDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        return ListOutputBuilder.get(input, context.<List>get(LIST)).pageable(false).build();
    }
}
