/* $Id$ */
package ru.tandemservice.uniepp_price.base.bo.EppPrice.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.commonbase.catalog.entity.Currency;
import org.tandemframework.shared.ctr.catalog.entity.CtrPriceCategory;
import org.tandemframework.shared.ctr.catalog.entity.CtrPricePaymentGrid;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp_price.base.bo.EppPrice.util.EppEduPlanPriceRowDTO;

import java.util.Date;
import java.util.List;

/**
 * @author Vasily Zhukov
 * @since 30.08.2011
 */
public interface IEppPriceDao extends INeedPersistenceSupport
{
    List<CtrPricePaymentGrid> getUsedPaymentGridList(EppEduPlanVersion eduPlanVersion);

    List<CtrPriceCategory> getUsedPriceCategoryList(EppEduPlanVersion eduPlanVersion);

    List<EppEduPlanPriceRowDTO> getPriceDTOList(EppEduPlanVersion eduPlanVersion);

    boolean isEppEduPlanVersionBlockHasPriceElementCost(EppEduPlanVersionBlock block, Currency currency, Date periodDate, List<CtrPricePaymentGrid> pricePaymentGridList, List<CtrPriceCategory> priceCategoryList);

    boolean isEppEduPlanVersionBlockHasIEppEpvRegistryRowWithoutPriceElementCost(EppEduPlanVersionBlock block, Currency currency, Date periodDate, List<CtrPricePaymentGrid> pricePaymentGridList, List<CtrPriceCategory> priceCategoryList);
    
    void doUpdateCost(EppEduPlanVersionBlock block, Currency currency, Date periodDate, List<CtrPricePaymentGrid> pricePaymentGridList, List<CtrPriceCategory> priceCategoryList);
}
