/* $Id$ */
package ru.tandemservice.uniepp_price.base.bo.EppPrice.logic;

import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.ctr.base.bo.CtrPrice.CtrPriceManager;
import org.tandemframework.shared.ctr.base.bo.CtrPrice.logic.ICtrPriceDao;
import org.tandemframework.shared.ctr.base.bo.CtrPrice.util.CtrPriceElementCostKey;
import org.tandemframework.shared.ctr.base.bo.CtrPrice.util.CtrPriceElementCostStageDTO;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCost;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementPeriod;
import org.tandemframework.shared.ctr.catalog.entity.CtrPriceCategory;
import org.tandemframework.shared.ctr.catalog.entity.CtrPricePaymentGrid;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp_price.base.bo.EppPrice.util.EppEduPlanPriceRowDTO;
import ru.tandemservice.uniepp_price.base.bo.EppPrice.util.EppEduPlanPriceRowListDTO;

import java.util.*;

/**
 * @author Vasily Zhukov
 * @since 30.08.2011
 */
public class EppPriceDao extends CommonDAO implements IEppPriceDao
{
    @Override
    public List<CtrPricePaymentGrid> getUsedPaymentGridList(EppEduPlanVersion eduPlanVersion)
    {
        DQLSelectBuilder eduPlanVersionBlockDQL = getEduPlanVersionBlockDQL(eduPlanVersion);
        DQLSelectBuilder eduPlanVersionBlockDisciplineDQL = getEduPlanVersionPriceElementDQL(eduPlanVersionBlockDQL);

        return new DQLSelectBuilder().fromEntity(CtrPricePaymentGrid.class, "e")
        .where(DQLExpressions.in(DQLExpressions.property(CtrPricePaymentGrid.id().fromAlias("e")), new DQLSelectBuilder().fromEntity(CtrPriceElementCost.class, "s")
            .column(DQLExpressions.property(CtrPriceElementCost.paymentGrid().id().fromAlias("s")))
            .where(DQLExpressions.or(
                DQLExpressions.in(DQLExpressions.property(CtrPriceElementCost.period().element().id().fromAlias("s")), eduPlanVersionBlockDQL.buildQuery()),
                DQLExpressions.in(DQLExpressions.property(CtrPriceElementCost.period().element().id().fromAlias("s")), eduPlanVersionBlockDisciplineDQL.buildQuery())
            ))
            .buildQuery()
        ))
        .order(DQLExpressions.property(CtrPricePaymentGrid.title().fromAlias("e")))
        .createStatement(getSession()).list();
    }

    @Override
    public List<CtrPriceCategory> getUsedPriceCategoryList(EppEduPlanVersion eduPlanVersion)
    {
        DQLSelectBuilder eduPlanVersionBlockDQL = getEduPlanVersionBlockDQL(eduPlanVersion);
        DQLSelectBuilder eduPlanVersionBlockDisciplineDQL = getEduPlanVersionPriceElementDQL(eduPlanVersionBlockDQL);

        return new DQLSelectBuilder().fromEntity(CtrPriceCategory.class, "e")
        .where(DQLExpressions.in(DQLExpressions.property(CtrPriceCategory.id().fromAlias("e")), new DQLSelectBuilder().fromEntity(CtrPriceElementCost.class, "s")
            .column(DQLExpressions.property(CtrPriceElementCost.category().id().fromAlias("s")))
            .where(DQLExpressions.or(
                DQLExpressions.in(DQLExpressions.property(CtrPriceElementCost.period().element().id().fromAlias("s")), eduPlanVersionBlockDQL.buildQuery()),
                DQLExpressions.in(DQLExpressions.property(CtrPriceElementCost.period().element().id().fromAlias("s")), eduPlanVersionBlockDisciplineDQL.buildQuery())
            ))
            .buildQuery()
        ))
        .order(DQLExpressions.property(CtrPriceCategory.shortTitle().fromAlias("e")))
        .createStatement(getSession()).list();
    }

    private EppEduPlanPriceRowListDTO getPriceRowListDTO(EppEduPlanVersion eduPlanVersion)
    {
        DQLSelectBuilder eduPlanVersionBlockDQL = getEduPlanVersionBlockDQL(eduPlanVersion);
        DQLSelectBuilder eduPlanVersionBlockDisciplineDQL = getEduPlanVersionPriceElementDQL(eduPlanVersionBlockDQL);

        List<Long> blockIdsList = eduPlanVersionBlockDQL.createStatement(getSession()).list();
        List<Long> rowIdsList = eduPlanVersionBlockDisciplineDQL.createStatement(getSession()).list();

        List<EppEduPlanVersionBlock> blockList = new DQLSelectBuilder().fromEntity(EppEduPlanVersionBlock.class, "e")
        .where(DQLExpressions.in(DQLExpressions.property(EppEduPlanVersionBlock.id().fromAlias("e")), blockIdsList))
        .createStatement(getSession()).list();

        List<EppEpvRegistryRow> rowList = new DQLSelectBuilder().fromEntity(EppEpvRegistryRow.class, "e")
        .where(DQLExpressions.eq(DQLExpressions.property(EppEpvRegistryRow.owner().eduPlanVersion().fromAlias("e")), DQLExpressions.value(eduPlanVersion)))
        .where(DQLExpressions.in(DQLExpressions.property(EppEpvRegistryRow.registryElement().id().fromAlias("e")), rowIdsList))
        .createStatement(getSession()).list();

        Collections.sort(blockList);
        Collections.sort(rowList, new Comparator<EppEpvRegistryRow>()
            {
            @Override
            public int compare(EppEpvRegistryRow o1, EppEpvRegistryRow o2)
            {
                final EppRegistryStructure r1 = o1.getType().getRoot();
                final EppRegistryStructure r2 = o2.getType().getRoot();
                int i = r1.getCode().compareTo(r2.getCode());
                if (0 != i)
                {
                    return i;
                }
                return o1.getTitle().compareTo(o2.getTitle());
            }
            });

        Map<EppEduPlanVersionBlock, List<EppEpvRegistryRow>> block2disciplineMap = new HashMap<>();
        for (EppEpvRegistryRow row : rowList)
        {
            List<EppEpvRegistryRow> list = block2disciplineMap.get(row.getOwner());
            if (list == null)
            {
                block2disciplineMap.put(row.getOwner(), list = new ArrayList<>());
            }
            list.add(row);
        }

        int i = 0;
        while (i < blockList.size() && !blockList.get(i).isRootBlock()) i++;
        EppEduPlanVersionBlock rootVersionBlock = blockList.get(i);
        List<EppEduPlanVersionBlock> versionBlockList = blockList;
        versionBlockList.remove(rootVersionBlock);

        EppEduPlanPriceRowDTO rootBlockDTO = new EppEduPlanPriceRowDTO(rootVersionBlock);
        List<EppEduPlanPriceRowDTO> rootRowsDTO = new ArrayList<>();
        List<EppEpvRegistryRow> rootRows = block2disciplineMap.get(rootVersionBlock);
        if (rootRows != null)
            for (EppEpvRegistryRow row : rootRows)
                rootRowsDTO.add(new EppEduPlanPriceRowDTO(row));

        List<EppEduPlanPriceRowDTO> listBlockDTO = new ArrayList<>();
        Map<Long, List<EppEduPlanPriceRowDTO>> block2rowsDTOMap = new HashMap<>();
        for (EppEduPlanVersionBlock versionBlock : versionBlockList)
        {
            List<EppEduPlanPriceRowDTO> blockRowsDTO = new ArrayList<>();
            List<EppEpvRegistryRow> blockRows = block2disciplineMap.get(versionBlock);
            if (blockRows != null)
                for (EppEpvRegistryRow row : blockRows)
                    blockRowsDTO.add(new EppEduPlanPriceRowDTO(row));

            listBlockDTO.add(new EppEduPlanPriceRowDTO(versionBlock));
            block2rowsDTOMap.put(versionBlock.getId(), Collections.unmodifiableList(blockRowsDTO));
        }

        return new EppEduPlanPriceRowListDTO(rootBlockDTO, rootRowsDTO, listBlockDTO, block2rowsDTOMap);
    }

    @Override
    public List<EppEduPlanPriceRowDTO> getPriceDTOList(EppEduPlanVersion eduPlanVersion)
    {
        return getPriceRowListDTO(eduPlanVersion).getListDTO();
    }

    @Override
    public boolean isEppEduPlanVersionBlockHasPriceElementCost(EppEduPlanVersionBlock block, org.tandemframework.shared.commonbase.catalog.entity.Currency currency, Date periodDate, List<CtrPricePaymentGrid> pricePaymentGridList, List<CtrPriceCategory> priceCategoryList)
    {
        EppEduPlanVersion eduPlanVersion = block.getEduPlanVersion();
        EppEduPlanPriceRowListDTO listDTO = getPriceRowListDTO(eduPlanVersion);
        EppEduPlanPriceRowDTO rowDTO = listDTO.findBlockDTO(block.getId());
        if (rowDTO == null) return false; // блок уже удалился

        if (pricePaymentGridList == null || pricePaymentGridList.isEmpty())
            pricePaymentGridList = getUsedPaymentGridList(eduPlanVersion);

        if (priceCategoryList == null || priceCategoryList.isEmpty())
            priceCategoryList = getUsedPriceCategoryList(eduPlanVersion);

        List<CtrPriceElementPeriod> periodList = CtrPriceManager.instance().dao().getUsedPeriodList(rowDTO.getPriceElement(), periodDate);
        if (periodList.isEmpty()) return false; // нет периода - нет цены

        CtrPriceElementPeriod period = periodList.get(0);
        Map<CtrPriceElementCostKey, CtrPriceElementCost> costMap = CtrPriceManager.instance().dao().getCostMap(rowDTO.getPriceElement(), currency);

        for (CtrPricePaymentGrid paymentGrid : pricePaymentGridList)
            for (CtrPriceCategory category : priceCategoryList)
                if (costMap.containsKey(new CtrPriceElementCostKey(period, paymentGrid, category, currency)))
                    return true; // найдена цена

        // цена не найдена
        return false;
    }

    @Override
    public boolean isEppEduPlanVersionBlockHasIEppEpvRegistryRowWithoutPriceElementCost(EppEduPlanVersionBlock block, org.tandemframework.shared.commonbase.catalog.entity.Currency currency, Date periodDate, List<CtrPricePaymentGrid> pricePaymentGridList, List<CtrPriceCategory> priceCategoryList)
    {
        EppEduPlanVersion eduPlanVersion = block.getEduPlanVersion();
        EppEduPlanPriceRowListDTO listDTO = getPriceRowListDTO(eduPlanVersion);
        EppEduPlanPriceRowDTO rowDTO = listDTO.findBlockDTO(block.getId());
        if (rowDTO == null) return false; // блок уже удалился

        if (pricePaymentGridList == null || pricePaymentGridList.isEmpty())
            pricePaymentGridList = getUsedPaymentGridList(eduPlanVersion);

        if (priceCategoryList == null || priceCategoryList.isEmpty())
            priceCategoryList = getUsedPriceCategoryList(eduPlanVersion);

        // список IEppEpvRegistryRow, в которых будем искать "пустые ячейки"
        List<EppEduPlanPriceRowDTO> forCheck = new ArrayList<>(listDTO.getRootRowsDTO());
        List<EppEduPlanPriceRowDTO> additionalBlockList = listDTO.getBlock2rowsDTOMap().get(rowDTO.getId());
        if (additionalBlockList != null)
            forCheck.addAll(additionalBlockList);

        for (EppEduPlanPriceRowDTO row : forCheck)
        {
            List<CtrPriceElementPeriod> periodList = CtrPriceManager.instance().dao().getUsedPeriodList(row.getPriceElement(), periodDate);
            if (periodList.isEmpty()) return true; // нет периода - нет цены, нашли элемент

            CtrPriceElementPeriod period = periodList.get(0);
            Map<CtrPriceElementCostKey, CtrPriceElementCost> costMap = CtrPriceManager.instance().dao().getCostMap(row.getPriceElement(), currency);

            for (CtrPricePaymentGrid paymentGrid : pricePaymentGridList)
                for (CtrPriceCategory category : priceCategoryList)
                    if (!costMap.containsKey(new CtrPriceElementCostKey(period, paymentGrid, category, currency)))
                        return true; // нет цены, нашли элемент
        }

        // у всех цена указана
        return false;
    }

    @Override
    public void doUpdateCost(EppEduPlanVersionBlock block, org.tandemframework.shared.commonbase.catalog.entity.Currency currency, Date periodDate, List<CtrPricePaymentGrid> pricePaymentGridList, List<CtrPriceCategory> priceCategoryList)
    {
        EppEduPlanVersion eduPlanVersion = block.getEduPlanVersion();
        EppEduPlanPriceRowListDTO listDTO = getPriceRowListDTO(eduPlanVersion);
        EppEduPlanPriceRowDTO blockDTO = listDTO.findBlockDTO(block.getId());
        if (blockDTO == null) return; // блок уже удалился

        if (pricePaymentGridList == null || pricePaymentGridList.isEmpty())
            pricePaymentGridList = getUsedPaymentGridList(eduPlanVersion);

        if (priceCategoryList == null || priceCategoryList.isEmpty())
            priceCategoryList = getUsedPriceCategoryList(eduPlanVersion);

        // список IEppEpvRegistryRow, цены из которых будут слагаемыми
        List<EppEduPlanPriceRowDTO> forSum = new ArrayList<>(listDTO.getRootRowsDTO());
        List<EppEduPlanPriceRowDTO> additionalBlockList = listDTO.getBlock2rowsDTOMap().get(blockDTO.getId());
        if (additionalBlockList != null)
            forSum.addAll(additionalBlockList);

        // инициализируем сумматор цен
        Map<PairKey<CtrPricePaymentGrid, CtrPriceCategory>, List<CtrPriceElementCostStageDTO>> sumCostStageMap = new HashMap<>();
        ICtrPriceDao priceDao = CtrPriceManager.instance().dao();
        Map<CtrPriceElementCostKey, CtrPriceElementCost> blockCostMap = priceDao.getCostMap(block, currency);

        Date minDurationBegin = null;
        Date maxDurationEnd = null;
        for (EppEduPlanPriceRowDTO row : forSum)
        {
            List<CtrPriceElementPeriod> periodList = priceDao.getUsedPeriodList(row.getPriceElement(), periodDate);
            if (!periodList.isEmpty())
            {
                CtrPriceElementPeriod period = periodList.get(0);
                if (minDurationBegin == null || period.getDurationBegin().before(minDurationBegin))
                    minDurationBegin = period.getDurationBegin();
                if (maxDurationEnd == null || (period.getDurationEnd() != null && period.getDurationEnd().after(maxDurationEnd)))
                    maxDurationEnd = period.getDurationEnd();
                Map<CtrPriceElementCostKey, CtrPriceElementCost> costMap = priceDao.getCostMap(row.getPriceElement(), currency);

                for (CtrPricePaymentGrid paymentGrid : pricePaymentGridList)
                {
                    for (CtrPriceCategory category : priceCategoryList)
                    {
                        CtrPriceElementCost elementCost = costMap.get(new CtrPriceElementCostKey(period, paymentGrid, category, currency));
                        if (elementCost != null)
                        {
                            List<CtrPriceElementCostStageDTO> costStageDTOList = priceDao.getPriceElementCostStageDTOList(elementCost);
                            if (costStageDTOList != null)
                            {
                                List<CtrPriceElementCostStageDTO> sumCostStageDTOList = sumCostStageMap.get(PairKey.create(paymentGrid, category));
                                if (sumCostStageDTOList == null)
                                {
                                    sumCostStageMap.put(PairKey.create(paymentGrid, category), costStageDTOList);
                                } else
                                {
                                    if (sumCostStageDTOList.size() != costStageDTOList.size())
                                        throw new RuntimeException("Different stage list count found for paymentGrid (" + paymentGrid.getTitle() + "), priceCategory (" + category.getTitle() + "), currency (" + currency.getTitle() + "), IEppEpvRegistryRow (" + row.getId() + ", " + row.getTitle() + ")");

                                    for (CtrPriceElementCostStageDTO costStageDTO : costStageDTOList)
                                    {
                                        // search in sumCostStageDTOList
                                        int i = 0;
                                        while (i < sumCostStageDTOList.size() && !sumCostStageDTOList.get(i).equals(costStageDTO))
                                            i++;
                                        if (i < sumCostStageDTOList.size())
                                        {
                                            // ok
                                            CtrPriceElementCostStageDTO sumCostStageDTO = sumCostStageDTOList.get(i);
                                            sumCostStageDTO.setCost(sumCostStageDTO.getCost() + costStageDTO.getCost());
                                        } else
                                            throw new RuntimeException("Different stageUniqueCode found for paymentGrid (" + paymentGrid.getTitle() + "), priceCategory (" + category.getTitle() + "), currency (" + currency.getTitle() + "), IEppEpvRegistryRow (" + row.getId() + ", " + row.getTitle() + ")");
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        // находим подходящий период для блока УП(в), если его нет, то создаем
        List<CtrPriceElementPeriod> blockPeriodList = priceDao.getUsedPeriodList(block, periodDate);
        CtrPriceElementPeriod blockPeriod;
        if (blockPeriodList.isEmpty())
        {
            if (minDurationBegin == null) return; // невозможно подобрать период для блока УП(в)
            blockPeriod = priceDao.savePriceElementPeriod(block, minDurationBegin, maxDurationEnd, null);
        } else
            blockPeriod = blockPeriodList.get(0);

        // сохраняем цены в блоке УП(в)
        for (CtrPricePaymentGrid paymentGrid : pricePaymentGridList)
        {
            for (CtrPriceCategory category : priceCategoryList)
            {
                PairKey<CtrPricePaymentGrid, CtrPriceCategory> pairKey = PairKey.create(paymentGrid, category);
                List<CtrPriceElementCostStageDTO> sumCostStageDTOList = sumCostStageMap.get(pairKey);
                CtrPriceElementCostKey blockCostKey = new CtrPriceElementCostKey(blockPeriod, paymentGrid, category, currency);
                CtrPriceElementCost blockElementCost = blockCostMap.get(blockCostKey);

                if (sumCostStageDTOList == null || sumCostStageDTOList.isEmpty())
                {
                    if (blockElementCost != null)
                    {
                        // если нет суммы и есть куда сохранять, то во все поля запишем нули
                        List<CtrPriceElementCostStageDTO> costStageDTOList = priceDao.getPriceElementCostStageDTOList(blockElementCost);
                        for (CtrPriceElementCostStageDTO costStageDTO : costStageDTOList) costStageDTO.setCost(0.0);
                        priceDao.saveOrUpdatePriceElementCost(currency, blockPeriod, paymentGrid, category, null, costStageDTOList);
                    } else
                    {
                        // разве нужно создавать стадии, если их нет и нечего суммировать?
                    }
                } else
                {
                    if (blockElementCost != null)
                    {
                        // если есть сумма и есть куда сохранять, то изменим цены
                        Map<String, CtrPriceElementCostStageDTO> id2costStageDTO = new HashMap<>();
                        for (CtrPriceElementCostStageDTO sumCostStageDTO : sumCostStageDTOList)
                            id2costStageDTO.put(sumCostStageDTO.getStageUniqueCode(), sumCostStageDTO);
                        List<CtrPriceElementCostStageDTO> costStageDTOList = priceDao.getPriceElementCostStageDTOList(blockElementCost);
                        for (CtrPriceElementCostStageDTO costStageDTO : costStageDTOList)
                        {
                            CtrPriceElementCostStageDTO sumCostStageDTO = id2costStageDTO.get(costStageDTO.getStageUniqueCode());
                            costStageDTO.setCost(sumCostStageDTO.getCost());
                        }
                        priceDao.saveOrUpdatePriceElementCost(currency, blockPeriod, paymentGrid, category, null, costStageDTOList);
                    } else
                    {
                        // если есть сумма, но некуда сохранять, то создадим бланки CtrPriceElementCostStageDTO
                        List<CtrPriceElementCostStageDTO> forSaveList = new ArrayList<>();
                        for (CtrPriceElementCostStageDTO sumCostStageDTO : sumCostStageDTOList)
                            forSaveList.add(new CtrPriceElementCostStageDTO(sumCostStageDTO.getId(), sumCostStageDTO.getTitle(), null, sumCostStageDTO.getCost(), null, sumCostStageDTO.getLevel(), sumCostStageDTO.getParent(), sumCostStageDTO.getStageUniqueCode(), sumCostStageDTO.getCostTitle(), false));
                        priceDao.saveOrUpdatePriceElementCost(currency, blockPeriod, paymentGrid, category, null, sumCostStageDTOList);
                    }
                }
            }
        }
    }

    private DQLSelectBuilder getEduPlanVersionBlockDQL(EppEduPlanVersion eduPlanVersion)
    {
        return new DQLSelectBuilder().fromEntity(EppEduPlanVersionBlock.class, "b1")
        .column(DQLExpressions.property(EppEduPlanVersionBlock.id().fromAlias("b1")))
        .where(DQLExpressions.eq(DQLExpressions.property(EppEduPlanVersionBlock.eduPlanVersion().fromAlias("b1")), DQLExpressions.value(eduPlanVersion)));
    }

    private DQLSelectBuilder getEduPlanVersionPriceElementDQL(DQLSelectBuilder eduPlanVersionBlockDQL)
    {
        return new DQLSelectBuilder().fromEntity(EppEpvRegistryRow.class, "b2")
        .column(DQLExpressions.property(EppEpvRegistryRow.registryElement().id().fromAlias("b2")))
        .where(DQLExpressions.isNotNull(EppEpvRegistryRow.registryElement().id().fromAlias("b2")))
        .where(DQLExpressions.in(DQLExpressions.property(EppEpvRegistryRow.owner().id().fromAlias("b2")), eduPlanVersionBlockDQL.buildQuery()));
    }
}
