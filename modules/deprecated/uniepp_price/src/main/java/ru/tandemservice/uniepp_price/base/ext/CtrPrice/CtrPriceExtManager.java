/* $Id$ */
package ru.tandemservice.uniepp_price.base.ext.CtrPrice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;

import org.tandemframework.shared.ctr.base.bo.CtrPrice.CtrPriceManager;
import org.tandemframework.shared.ctr.base.bo.CtrPrice.util.ICtrPriceElementCostStageFactory;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrPricePaymentGridCodes;
import ru.tandemservice.uniepp_price.base.ext.CtrPrice.logic.EppPriceElementCostStageFactoryHalfTerm;
import ru.tandemservice.uniepp_price.base.ext.CtrPrice.logic.EppPriceElementCostStageFactoryTerm;
import ru.tandemservice.uniepp_price.base.ext.CtrPrice.logic.EppPriceElementCostStageFactoryYear;

/**
 * @author Vasily Zhukov
 * @since 25.08.2011
 */
@Configuration
public class CtrPriceExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private CtrPriceManager _ctrPriceManager;

    @Bean
    public ItemListExtension<ICtrPriceElementCostStageFactory> stageFactoryItemListExt()
    {
        return itemListExtension(_ctrPriceManager.stageFactoryItemList())
                .replace(CtrPricePaymentGridCodes.EPP_YEAR, new EppPriceElementCostStageFactoryYear())
                .replace(CtrPricePaymentGridCodes.EPP_SEMESTER, new EppPriceElementCostStageFactoryTerm())
                .replace(CtrPricePaymentGridCodes.EPP_HALF_SEMESTER, new EppPriceElementCostStageFactoryHalfTerm())
                .create();
    }
}