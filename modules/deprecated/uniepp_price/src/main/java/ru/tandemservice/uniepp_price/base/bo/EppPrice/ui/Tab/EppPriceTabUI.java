/* $Id$ */
package ru.tandemservice.uniepp_price.base.bo.EppPrice.ui.Tab;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.HeadColumn;
import org.tandemframework.core.view.list.source.AbstractListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.catalog.entity.codes.CurrencyCodes;
import org.tandemframework.shared.ctr.base.bo.CtrPrice.CtrPriceManager;
import org.tandemframework.shared.ctr.base.bo.CtrPrice.ui.AddEdit.CtrPriceAddEdit;
import org.tandemframework.shared.ctr.base.bo.CtrPrice.ui.ElementCostPub.CtrPriceElementCostPub;
import org.tandemframework.shared.ctr.base.bo.CtrPrice.ui.ElementCostPub.CtrPriceElementCostPubUI;
import org.tandemframework.shared.ctr.base.bo.CtrPrice.ui.Tab.CtrPriceTab;
import org.tandemframework.shared.ctr.base.bo.CtrPrice.ui.Tab.CtrPriceTabUI;
import org.tandemframework.shared.ctr.base.bo.CtrPrice.util.CtrPriceElementCostKey;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCost;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementPeriod;
import org.tandemframework.shared.ctr.catalog.entity.CtrPriceCategory;
import org.tandemframework.shared.ctr.catalog.entity.CtrPricePaymentGrid;
import org.tandemframework.tapsupport.TapSupportUtils;
import org.tandemframework.tapsupport.confirm.ClickButtonAction;
import org.tandemframework.tapsupport.confirm.ConfirmInfo;

import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp_price.base.bo.EppPrice.EppPriceManager;
import ru.tandemservice.uniepp_price.base.bo.EppPrice.logic.EppPriceElementListDSHandler;
import ru.tandemservice.uniepp_price.base.bo.EppPrice.logic.IEppPriceDao;
import ru.tandemservice.uniepp_price.base.bo.EppPrice.util.EppEduPlanPriceRowDTO;

/**
 * @author Vasily Zhukov
 * @since 30.08.2011
 */
@Input({@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "eduPlanVersionId")})
public class EppPriceTabUI extends UIPresenter
{
    private EppEduPlanVersion _eduPlanVersion;
    private Long _eduPlanVersionId;

    private org.tandemframework.shared.commonbase.catalog.entity.Currency _currency;
    private Date _periodDate;
    private List<CtrPricePaymentGrid> _pricePaymentGridList;
    private List<CtrPriceCategory> _priceCategoryList;
    private List<EppEduPlanPriceRowDTO> _rowDTOList;

    @Override
    public void onComponentRefresh()
    {
        _eduPlanVersion = DataAccessServices.dao().getNotNull(_eduPlanVersionId);
        onClickClear();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        String name = dataSource.getName();

        if (name.equals(EppPriceTab.PRICE_PAYMENT_GRID_DS))
        {
            dataSource.put(UIDefines.COMBO_OBJECT_LIST, EppPriceManager.instance().dao().getUsedPaymentGridList(_eduPlanVersion));
        } else if (name.equals(EppPriceTab.PRICE_CATEGORY_DS))
        {
            dataSource.put(UIDefines.COMBO_OBJECT_LIST, EppPriceManager.instance().dao().getUsedPriceCategoryList(_eduPlanVersion));
        } else if (name.equals(EppPriceTab.LIST_DS))
        {
            dataSource.put(EppPriceElementListDSHandler.LIST, _rowDTOList);
        }
    }

    // Getters & Setters

    public EppEduPlanVersion getEduPlanVersion()
    {
        return _eduPlanVersion;
    }

    public void setEduPlanVersion(EppEduPlanVersion eduPlanVersion)
    {
        _eduPlanVersion = eduPlanVersion;
    }

    public Long getEduPlanVersionId()
    {
        return _eduPlanVersionId;
    }

    public void setEduPlanVersionId(Long eduPlanVersionId)
    {
        _eduPlanVersionId = eduPlanVersionId;
    }

    public org.tandemframework.shared.commonbase.catalog.entity.Currency getCurrency()
    {
        return _currency;
    }

    public void setCurrency(org.tandemframework.shared.commonbase.catalog.entity.Currency currency)
    {
        _currency = currency;
    }

    public Date getPeriodDate()
    {
        return _periodDate;
    }

    public void setPeriodDate(Date periodDate)
    {
        _periodDate = periodDate;
    }

    public List<CtrPricePaymentGrid> getPricePaymentGridList()
    {
        return _pricePaymentGridList;
    }

    public void setPricePaymentGridList(List<CtrPricePaymentGrid> pricePaymentGridList)
    {
        _pricePaymentGridList = pricePaymentGridList;
    }

    public List<CtrPriceCategory> getPriceCategoryList()
    {
        return _priceCategoryList;
    }

    public void setPriceCategoryList(List<CtrPriceCategory> priceCategoryList)
    {
        _priceCategoryList = priceCategoryList;
    }

    // Listeners

    public void onClickClear()
    {
        _currency = DataAccessServices.dao().getByCode(org.tandemframework.shared.commonbase.catalog.entity.Currency.class, CurrencyCodes.RUB);
        _periodDate = new Date();
        _pricePaymentGridList = Collections.emptyList();
        _priceCategoryList = Collections.emptyList();
        onClickSearch();
    }

    public void onClickSearch()
    {
        _rowDTOList = EppPriceManager.instance().dao().getPriceDTOList(_eduPlanVersion);

        IUIDataSource dataSource = _uiConfig.getDataSource(EppPriceTab.LIST_DS);
        dataSource.doCleanupDataSource();
        AbstractListDataSource<CtrPriceElementPeriod> legacyDataSource = ((BaseSearchListDataSource) dataSource).getLegacyDataSource();
        List<AbstractColumn> columns = legacyDataSource.getColumns();

        // загружаем данные
        IEppPriceDao dao = EppPriceManager.instance().dao();
        List<CtrPricePaymentGrid> pricePaymentGridList = _pricePaymentGridList.isEmpty() ? dao.getUsedPaymentGridList(_eduPlanVersion) : _pricePaymentGridList;
        List<CtrPriceCategory> priceCategoryList = _priceCategoryList.isEmpty() ? dao.getUsedPriceCategoryList(_eduPlanVersion) : _priceCategoryList;

        // создаем нужные колонки
        List<HeadColumn> headColumns = new ArrayList<HeadColumn>();
        for (final CtrPricePaymentGrid paymentGrid : pricePaymentGridList)
        {
            HeadColumn headColumn = new HeadColumn("paymentGrid" + paymentGrid.getCode(), paymentGrid.getTitle());

            for (final CtrPriceCategory category : priceCategoryList)
            {
                String blockColumnName = "block-" + paymentGrid.getCode() + "-" + category.getCode();
                String blockColumnTitle = category.getTitle();
                BlockColumn<CtrPriceElementCost> blockColumn = new BlockColumn<CtrPriceElementCost>(blockColumnName, blockColumnTitle, "block" ) {
                    private CtrPricePaymentGrid _paymentGrid = paymentGrid;
                    private CtrPriceCategory _category = category;

                    @SuppressWarnings("unused")
                    public CtrPricePaymentGrid getPaymentGrid() {
                        return _paymentGrid;
                    }

                    @SuppressWarnings("unused")
                    public CtrPriceCategory getCategory() {
                        return _category;
                    }
                };

                for (EppEduPlanPriceRowDTO row : _rowDTOList)
                {
                    CtrPriceElementPeriod period = null;
                    final List<CtrPriceElementPeriod> periodList = CtrPriceManager.instance().dao().getUsedPeriodList(row.getPriceElement(), _periodDate);
                    if (!periodList.isEmpty())
                    {
                        period = periodList.get(0);
                        final Map<CtrPriceElementCostKey, CtrPriceElementCost> costMap = CtrPriceManager.instance().dao().getCostMap(row.getPriceElement(), _currency);
                        blockColumn.getValueMap().put(row.getId(), costMap.get(new CtrPriceElementCostKey(period, paymentGrid, category, _currency)));
                    }
                    row.setPeriod(period);
                }

                headColumn.addColumn(blockColumn);
            }

            headColumns.add(headColumn);
        }

        columns.addAll(1, headColumns);
    }

    public void onClickOpenCtrPriceTab()
    {
        Map<String, Object> params = new HashMap<String, Object>(this.<Map<String, Object>>getListenerParameter());
        params.put(CtrPriceTabUI.SECURITY_OBJECT_ID, _eduPlanVersionId);
        params.put(CtrPriceTabUI.PERMISSION_KEY_POSTFIX, "eppEduPlanVersion");
        _uiActivation.asDesktopRoot(CtrPriceTab.class).parameters(params).activate();
    }

    public void onClickAddPriceFromCell()
    {
        _uiActivation.asRegion(CtrPriceAddEdit.class).parameters(this.<Map<String, Object>>getListenerParameter()).activate();
    }

    public void onClickViewCostPubFromCell()
    {
        Map<String, Object> params = new HashMap<String, Object>(this.<Map<String, Object>>getListenerParameter());
        params.put(CtrPriceElementCostPubUI.SECURITY_OBJECT_ID, _eduPlanVersionId);
        params.put(CtrPriceElementCostPubUI.PERMISSION_KEY_POSTFIX, "eppEduPlanVersion");
        _uiActivation.asDesktopRoot(CtrPriceElementCostPub.class).parameters(params).activate();
    }

    @SuppressWarnings("unchecked")
    public void onClickUpdateCost()
    {
        Long blockId = (Long) ((Map<String, Object>) getListenerParameter()).get("priceElementId");
        EppEduPlanVersionBlock block = DataAccessServices.dao().getNotNull(blockId);
        IEppPriceDao dao = EppPriceManager.instance().dao();

        if (null == getClientParameter())
        {
            List<String> alertList = new ArrayList<String>();
            if (dao.isEppEduPlanVersionBlockHasPriceElementCost(block, _currency, _periodDate, _pricePaymentGridList, _priceCategoryList))
                alertList.add(_uiConfig.getProperty("ui.eppEduPlanVersionBlockHasPriceElementCost"));
            if (dao.isEppEduPlanVersionBlockHasIEppEpvRegistryRowWithoutPriceElementCost(block, _currency, _periodDate, _pricePaymentGridList, _priceCategoryList))
                alertList.add(_uiConfig.getProperty("ui.eppEduPlanVersionBlockHasIEppEpvRegistryRowWithoutPriceElementCost"));

            if (!alertList.isEmpty())
            {
                ConfirmInfo confirm = new ConfirmInfo(StringUtils.join(alertList, " "), new ClickButtonAction("calc" + blockId, "ok", true));
                TapSupportUtils.displayConfirm(confirm);
                return;
            }
        }

        dao.doUpdateCost(block, _currency, _periodDate, _pricePaymentGridList, _priceCategoryList);
        onClickSearch();
    }
}
