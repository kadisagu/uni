/* $Id$ */
package ru.tandemservice.uniepp_price.base.bo.EppPrice.util;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.entity.IEntity;

import org.tandemframework.shared.ctr.base.entity.ICtrPriceElement;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementPeriod;

import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;

/**
 * @author Vasily Zhukov
 * @since 30.08.2011
 */
public class EppEduPlanPriceRowDTO extends EntityBase implements IEntity, ITitled
{
    private Long _id;
    private String _title;
    private ICtrPriceElement _priceElement;
    private CtrPriceElementPeriod _period;
    private boolean _subRow;

    public EppEduPlanPriceRowDTO(EppEduPlanVersionBlock block)
    {
        _id = block.getId();
        _title = block.getEducationElementTitle();
        _priceElement = block;
        _subRow = false;
    }

    public EppEduPlanPriceRowDTO(EppEpvRegistryRow row)
    {
        _id = row.getId();
        _title = row.getTitle() + " (" + row.getRegistryElement().getNumberWithAbbreviation() + ")";
        _priceElement = row.getRegistryElement();
        _subRow = true;
    }

    // Getters

    @Override
    public Long getId()
    {
        return _id;
    }

    @Override
    public String getTitle()
    {
        return _title;
    }

    public ICtrPriceElement getPriceElement()
    {
        return _priceElement;
    }

    public boolean isSubRow()
    {
        return _subRow;
    }

    // Getters & Setters

    public CtrPriceElementPeriod getPeriod()
    {
        return _period;
    }

    public void setPeriod(final CtrPriceElementPeriod period)
    {
        _period = period;
    }
}
