/* $Id$ */
package ru.tandemservice.uniepp_price.base.ext.CtrPrice.logic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.tandemframework.shared.ctr.base.entity.ICtrPriceElement;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCostStage;

import ru.tandemservice.uni.dao.grid.IDevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.unieductr.base.entity.EduProgramPrice;
import ru.tandemservice.unieductr.base.ext.CtrPrice.logic.EduProgramElementCostStageFactoryHalfTerm;
import ru.tandemservice.uniepp.base.bo.EppPrice.IEppPriceElementCostStageFactory;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

/**
 * @author Dmitry Seleznev
 * @since 21.10.2011
 */
public class EppPriceElementCostStageFactoryHalfTerm extends EduProgramElementCostStageFactoryHalfTerm implements IEppPriceElementCostStageFactory
{
    @Override
    public List<CtrPriceElementCostStage> getStageList(ICtrPriceElement priceElement)
    {
        if (priceElement instanceof EppEduPlanVersionBlock) {
            final Map<Integer, Integer> term2courseMap = getStageCodeSource((EppEduPlanVersionBlock) priceElement);

            List<CtrPriceElementCostStage> stagesList = new ArrayList<CtrPriceElementCostStage>();
            for (Map.Entry<Integer, Integer> term2courseEntry : term2courseMap.entrySet())
            {
                for (int halfTerm : half_terms)
                {
                    CtrPriceElementCostStage stage = new CtrPriceElementCostStage();
                    stage.setTitle("За " + term2courseEntry.getKey() + " семестр, " + halfTerm + " часть (" + term2courseEntry.getValue() + " курс)");
                    stage.setStageUniqueCode(getStageUniqueCode(term2courseEntry, halfTerm));
                    stagesList.add(stage);
                }
            }

            return stagesList;
        }

        return super.getStageList(priceElement);
    }

    @Override
    public List<CtrPriceElementCostStage> filterStageList(ICtrPriceElement priceElement, List<CtrPriceElementCostStage> stageList, Map<String, Object> filter)
    {
        if (priceElement instanceof EduProgramPrice)
        {
            return super.filterStageList(priceElement, stageList, filter);
        }
        else if (priceElement instanceof EppEduPlanVersionBlock)
        {
            Object courseV = filter.get(IEppPriceElementCostStageFactory.FILTER_KEY_COURSE);
            Object termV = filter.get(IEppPriceElementCostStageFactory.FILTER_KEY_TERM);
            Integer course = (courseV instanceof Integer) ? (Integer) courseV : null;
            Integer term = (termV instanceof Integer) ? (Integer) termV : null;

            if (course == null && term == null)
            {
                return stageList;
            }

            final Map<Integer, Integer> stageCodeSource = getStageCodeSource((EppEduPlanVersionBlock) priceElement);
            Set<String> keys = new HashSet<String>();
            for (Map.Entry<Integer, Integer> stageCodeEntry : stageCodeSource.entrySet())
            {
                if (checkStageCodeForTerm(term, stageCodeEntry) && checkStageCodeForCourse(course, stageCodeEntry))
                {
                    for (int halfTerm : half_terms)
                        keys.add(getStageUniqueCode(stageCodeEntry, halfTerm));
                }
            }

            List<CtrPriceElementCostStage> result = new ArrayList<CtrPriceElementCostStage>();
            for (CtrPriceElementCostStage stage : stageList)
                if (keys.contains(stage.getStageUniqueCode()))
                    result.add(stage);

            return result;
        }
        else return stageList;
    }

    private Map<Integer, Integer> getStageCodeSource(EppEduPlanVersionBlock priceElement)
    {
        // делаем мап семестр -> курс
        DevelopGrid grid = priceElement.getEduPlanVersion().getEduPlan().getDevelopGrid();
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        for (Map.Entry<Course, Integer[]> entry : IDevelopGridDAO.instance.get().getDevelopGridDetail(grid).entrySet())
            for (Integer term : entry.getValue())
                if (null != term)
                    map.put(term, entry.getKey().getIntValue());
        return map;
    }

    private String getStageUniqueCode(Map.Entry<Integer, Integer> stageCodeEntry, int halfTermNum)
    {
        return String.valueOf(stageCodeEntry.getKey()) + "-" + String.valueOf(halfTermNum);
    }

    private boolean checkStageCodeForCourse(Integer course, Map.Entry<Integer, Integer> stageCodeEntry)
    {
        return (course == null || course.equals(stageCodeEntry.getValue()));
    }

    private boolean checkStageCodeForTerm(Integer term, Map.Entry<Integer, Integer> stageCodeEntry)
    {
        return (term == null || term.equals(stageCodeEntry.getKey()));
    }

}