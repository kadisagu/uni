/* $Id$ */
package ru.tandemservice.uniepp_price.base.ext.CtrPrice.logic;

import org.tandemframework.shared.ctr.base.entity.ICtrPriceElement;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCostStage;
import ru.tandemservice.uni.dao.grid.IDevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.gen.CourseGen;
import ru.tandemservice.unieductr.base.entity.EduProgramPrice;
import ru.tandemservice.unieductr.base.ext.CtrPrice.logic.EduProgramElementCostStageFactoryYear;
import ru.tandemservice.uniepp.base.bo.EppPrice.IEppPriceElementCostStageFactory;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Vasily Zhukov
 * @since 26.08.2011
 */
public class EppPriceElementCostStageFactoryYear extends EduProgramElementCostStageFactoryYear implements IEppPriceElementCostStageFactory
{
    @Override
    public List<CtrPriceElementCostStage> getStageList(ICtrPriceElement priceElement)
    {
        if(priceElement instanceof EduProgramPrice)
        {
            return super.getStageList(priceElement);
        }
        else if (priceElement instanceof EppEduPlanVersionBlock)
        {
            final List<Integer> stageCodeSource = getStageCodeSource((EppEduPlanVersionBlock) priceElement);

            List<CtrPriceElementCostStage> stagesList = new ArrayList<>();
            for (Integer stageCodeEntry : stageCodeSource)
            {
                CtrPriceElementCostStage stage = new CtrPriceElementCostStage();
                stage.setTitle("За " + stageCodeEntry + " курс");
                stage.setStageUniqueCode(String.valueOf(stageCodeEntry));
                stagesList.add(stage);
            }

            return stagesList;
        }
        else
            return new ArrayList<>();
    }

    @Override
    public List<CtrPriceElementCostStage> filterStageList(ICtrPriceElement priceElement, List<CtrPriceElementCostStage> stageList, Map<String, Object> filter)
    {
        if(priceElement instanceof EduProgramPrice)
        {
            return super.filterStageList(priceElement, stageList, filter);
        }
        else if (priceElement instanceof EppEduPlanVersionBlock)
        {
            Object courseV = filter.get(IEppPriceElementCostStageFactory.FILTER_KEY_COURSE);
            Integer course = (courseV instanceof Integer) ? (Integer) courseV : null;

            if (course == null)
            {
                return stageList;
            }

            final List<Integer> stageCodeSource = getStageCodeSource((EppEduPlanVersionBlock) priceElement);
            Set<String> keys = new HashSet<>();
            for (Integer stageCodeEntry : stageCodeSource)
            {
                if (checkStageCodeForCourse(course, stageCodeEntry))
                {
                    keys.add(String.valueOf(stageCodeEntry));
                }
            }

            List<CtrPriceElementCostStage> result = new ArrayList<>();
            for (CtrPriceElementCostStage stage : stageList)
                if (keys.contains(stage.getStageUniqueCode()))
                    result.add(stage);

            return result;
        }
        else return stageList;
    }

    private List<Integer> getStageCodeSource(EppEduPlanVersionBlock priceElement)
    {
        // делаем список курсов
        final DevelopGrid grid = priceElement.getEduPlanVersion().getEduPlan().getDevelopGrid();
        return IDevelopGridDAO.instance.get().getDevelopGridCourseSet(grid).stream()
                .map(CourseGen::getIntValue)
                .collect(Collectors.toList());
    }


    private boolean checkStageCodeForCourse(Integer course, Integer stageCodeEntry)
    {
        return (course == null || course.equals(stageCodeEntry));
    }
}