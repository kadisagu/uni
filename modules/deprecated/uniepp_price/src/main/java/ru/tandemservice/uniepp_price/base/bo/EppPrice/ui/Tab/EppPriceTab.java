/* $Id$ */
package ru.tandemservice.uniepp_price.base.bo.EppPrice.ui.Tab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.catalog.entity.Currency;
import ru.tandemservice.uniepp_price.base.bo.EppPrice.logic.EppPriceElementListDSHandler;

/**
 * @author Vasily Zhukov
 * @since 30.08.2011
 */
@Configuration
public class EppPriceTab extends BusinessComponentManager
{
    public static final String CURRENCY_DS = "currencyDS";
    public static final String PRICE_PAYMENT_GRID_DS = "pricePaymentGridDS";
    public static final String PRICE_CATEGORY_DS = "priceCategoryDS";
    public static final String LIST_DS = "listDS";

    @Bean
    public IDefaultComboDataSourceHandler currencyComboDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), Currency.class);
    }

    @Bean
    public IDefaultComboDataSourceHandler pricePaymentGridComboDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName()).filtered(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler priceCategoryComboDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName()).filtered(true);
    }

    @Bean
    public ColumnListExtPoint listDS()
    {
        return columnListExtPointBuilder(LIST_DS)
                .addColumn(blockColumn("title", "title"))
                .addColumn(blockColumn("updateCost", "updateCost").width("1"))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> listDSHandler()
    {
        return new EppPriceElementListDSHandler(getName());
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(CURRENCY_DS, currencyComboDSHandler()))
                .addDataSource(selectDS(PRICE_PAYMENT_GRID_DS, pricePaymentGridComboDSHandler()))
                .addDataSource(selectDS(PRICE_CATEGORY_DS, priceCategoryComboDSHandler()))
                .addDataSource(searchListDS(LIST_DS, listDS(), listDSHandler()))
                .create();
    }
}
