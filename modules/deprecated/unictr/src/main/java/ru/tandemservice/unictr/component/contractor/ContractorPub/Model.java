/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unictr.component.contractor.ContractorPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unictr.entity.contractor.ContactPerson;
import ru.tandemservice.unictr.entity.contractor.Contractor;

/**
 * @author OKhmelevskaya
 * Created on: 26.01.2008
 */
@SuppressWarnings("deprecation")
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "contractor.id"),
        @Bind(key = "selectedPage", binding = "selectedPage")
})
@Output(keys = {"contractorId", "contactPersonId"}, bindings = {"contractor.id", "contactPersonId"})
public class Model
{
    private String _selectedPage;
    private Contractor _contractor = new Contractor();
    
    private DynamicListDataSource<ContactPerson> _contactPersonDataSource;
    private Long _contactPersonId;

    public String getSelectedPage()
    {
        return _selectedPage;
    }

    public void setSelectedPage(String selectedPage)
    {
        _selectedPage = selectedPage;
    }
    
    public void setContractor(Contractor contractor)
    {
        _contractor = contractor;
    }

    public Contractor getContractor()
    {
        return _contractor;
    }

    public void setContactPersonDataSource(DynamicListDataSource<ContactPerson> contactPersonDataSource)
    {
        _contactPersonDataSource = contactPersonDataSource;
    }

    public DynamicListDataSource<ContactPerson> getContactPersonDataSource()
    {
        return _contactPersonDataSource;
    }

    public void setContactPersonId(Long contactPersonId)
    {
        _contactPersonId = contactPersonId;
    }

    public Long getContactPersonId()
    {
        return _contactPersonId;
    }
}
