/* $Id:Controller.java 2645 2008-04-29 07:54:04Z okhmelevskaya $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unictr.component.contactperson.ContactPersonList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.formatter.EmailFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import ru.tandemservice.unictr.entity.contractor.ContactPerson;
import ru.tandemservice.unictr.entity.contractor.Contractor;

/**
 * @author OKhmelevskaya
 * Created on: 31.01.2008
 */
@SuppressWarnings({"deprecation"})
public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate
{
    @Override
    public void onRefreshComponent(IBusinessComponent context)
    {
        getDao().prepare(getModel(context));
        prepareDataSource(context);
    }

    protected void refresh(IBusinessComponent context)
    {
        getModel(context).getDataSource().refresh();
    }
    
    private void prepareDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<ContactPerson> dataSource = new DynamicListDataSource<>(component, this);
        dataSource.addColumn(new SimpleColumn("ФИО", ContactPerson.P_FULLFIO));
        dataSource.addColumn(new SimpleColumn("Должность", ContactPerson.post().title().s()).setClickable(false));
        dataSource.addColumn(new PublisherLinkColumn("Контрагент", Contractor.shortTitle().s(),  ContactPerson.contractor().s()));
        dataSource.addColumn(new SimpleColumn("ОПФ", ContactPerson.contractor().legalForm().shortTitle().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Головная организация", ContactPerson.contractor().headOrganization().shortTitle().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Телефон", ContactPerson.P_ALL_PHONES).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Email", ContactPerson.P_EMAIL, EmailFormatter.INSTANCE).setOrderable(false).setClickable(false));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить контактное лицо?"));
        model.setDataSource(dataSource);
    }

    @Override
    public void updateListDataSource(IBusinessComponent context)
    {
        getDao().prepareListDataSource(getModel(context));
    }
    
    public void onClickSearch(IBusinessComponent context)
    {
        getModel(context).getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent context)
    {
        getModel(context).setContactPerson(new ContactPerson());
        getModel(context).setContractor(new Contractor());
        getModel(context).setContractorTitle(null);
        getModel(context).setHeadOrgTitle(null);
        getModel(context).setPostTitle(null);
        onClickSearch(context);
    }

    public void onClickDelete(IBusinessComponent context)
    {
        getDao().deleteRow(context);
        refresh(context);
    }
}
