/* $Id:Controller.java 2645 2008-04-29 07:54:04Z okhmelevskaya $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unictr.component.contactperson.ContactPersonPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import ru.tandemservice.unictr.IUnictrComponents;

/**
 * @author OKhmelevskaya
 * Created on: 30.01.2008
 */

public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent context)
    {
        getDao().prepare(getModel(context));
    }

    public void onClickEditContractPerson(IBusinessComponent context)
    {
        context.createChildRegion("contactPersonPub", new ComponentActivator(IUnictrComponents.CONTACT_PERSON_ADD_EDIT));
    }

    public void onClickEditContractPersonPost(IBusinessComponent context)
    {
        context.createChildRegion("contactPersonPub", new ComponentActivator(IUnictrComponents.CONTACT_PERSON_EDIT_POST));
    }

    public void onClickEditContractPersonContactInfo(IBusinessComponent context)
    {
        context.createChildRegion("contactPersonPub", new ComponentActivator(IUnictrComponents.CONTACT_PERSON_EDIT_CONTACT_INFO));
    }
}
