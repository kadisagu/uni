/* $Id:Model.java 2645 2008-04-29 07:54:04Z okhmelevskaya $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unictr.component.contactperson.ContactPersonList;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.ctr.catalog.entity.LegalForm;
import ru.tandemservice.unictr.entity.contractor.ContactPerson;
import ru.tandemservice.unictr.entity.contractor.Contractor;

import java.util.List;

/**
 * @author OKhmelevskaya
 * @since 31.01.2008
 */

@SuppressWarnings({"deprecation"})
public class Model
{
    private ContactPerson _contactPerson = new ContactPerson();
    private Contractor _contractor = new Contractor();
    private DynamicListDataSource<ContactPerson> _dataSource;
    private List<LegalForm> _legalFormList;
    private String _postTitle;
    private String _contractorTitle;
    private String _headOrgTitle;

    public void setContactPerson(ContactPerson contactPerson)
    {
        _contactPerson = contactPerson;
    }

    public ContactPerson getContactPerson()
    {
        return _contactPerson;
    }

    public DynamicListDataSource<ContactPerson> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<ContactPerson> dataSource)
    {
        _dataSource = dataSource;
    }

    public void setLegalFormList(List<LegalForm> legalFormList)
    {
        _legalFormList = legalFormList;
    }

    public List<LegalForm> getLegalFormList()
    {
        return _legalFormList;
    }

    public void setContractor(Contractor contractor)
    {
        _contractor = contractor;
    }

    public Contractor getContractor()
    {
        return _contractor;
    }

    public void setPostTitle(String postTitle)
    {
        _postTitle = postTitle;
    }

    public String getPostTitle()
    {
        return _postTitle;
    }

    public void setContractorTitle(String contractorTitle)
    {
        _contractorTitle = contractorTitle;
    }

    public String getContractorTitle()
    {
        return _contractorTitle;
    }

    public void setHeadOrgTitle(String headOrgTitle)
    {
        _headOrgTitle = headOrgTitle;
    }

    public String getHeadOrgTitle()
    {
        return _headOrgTitle;
    }
}
