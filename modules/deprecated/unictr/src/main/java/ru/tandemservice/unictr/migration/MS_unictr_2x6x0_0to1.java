package ru.tandemservice.unictr.migration;

import org.apache.commons.lang.mutable.MutableInt;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Убиение всех КЛ-студентов
 * Если что-то будет падать, то писать коммент в SH-1299
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unictr_2x6x0_0to1 extends IndependentMigrationScript
{
    private static final String[][] COLUMNS = {
        {"ctr_contractor_t", "contactor_id"},
        {"ctr_res_payment_t", "src_id"},
        {"ctr_res_payment_t", "dst_id"},
        {"ctr_res_payorder_t", "dst_id"},
        {"ctr_res_payorder_t", "src_id"},
        {"dpp_request", "requestor_id"},
        {"dpp_demand_extract_start", "listener_id"},
        {"dpp_demand_extract_reserve", "listener_id"},
    };

    private static final String[][] COLUMNS2 = {
        {"ctr_contractor_role_t", "contactor_id"},
        {"ctr_promice_t", "src_id"},
        {"ctr_promice_t", "dst_id"},
    };

    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
            new ScriptDependency("org.tandemframework", "1.6.15"),
            new ScriptDependency("org.tandemframework.shared", "1.6.0")
        };
    }

    private static Long getLong(ResultSet rs, int column) throws SQLException {
        Number value = (Number) rs.getObject(column);
        return null == value ? null : value.longValue();
    }

    @Override
    public void run(final DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность studentContactor

        // сущность была удалена
        {
            // нужно для всех КА-студентов создать КА-ФЛ (либо найти существующих)
            final short studentContactorEntityCode = tool.entityCodes().get("studentContactor");
            final short physicalContactorEntityCode = tool.entityCodes().get("physicalContactor");

            // studentcontactor.id -> per
            Map<Long, Long> sc2personMap = tool.executeQuery(
                    (ctool, rs) -> {
                        final Map<Long, Long> result = new HashMap<>(1024);
                        while (rs.next()) {
                            Long scId = getLong(rs, 1);
                            Long personId = getLong(rs, 2);
                            result.put(scId, personId);
                        }
                        return result;
                    },
                "select sc_pr.id, sc_pr.person_id " +
                " from personrole_t sc_pr" +
                " where sc_pr.discriminator=?",
                studentContactorEntityCode
            );

            // person.id -> physicalcontactor.id
            final Map<Long, Long> person2pcMap = SafeMap.get((Long personId) -> {
                try {
                    Long principalId = tool.executeQuery(
                            (ctool, rs) -> {
                                while (rs.next()) { return getLong(rs, 1); } // берем первого
                                throw new IllegalStateException("No principal, personId="+personId);
                            },
                            "select principal_id from person2principalrelation_t where person_id=?",
                            personId
                    );

                    Long id = EntityIDGenerator.generateNewId(physicalContactorEntityCode);
                    tool.executeUpdate("insert into personrole_t (id,discriminator,version,person_id,principal_id) values (?,?,?,?,?)", id, physicalContactorEntityCode, 1, personId, principalId);
                    tool.executeUpdate("insert into contactorperson_t (id,active_p) values (?,?)", id, Boolean.TRUE);
                    return id;
                } catch (Exception t) {
                    throw CoreExceptionUtils.getRuntimeException(t);
                }
            });

            // заполняем его существующими в базе значениями
            tool.executeQuery(
                    (ctool, rs) -> {
                        while (rs.next()) {
                            Long pcId = getLong(rs, 1);
                            Long personId = getLong(rs, 2);
                            person2pcMap.put(personId, pcId);
                        }
                        return null;
                    },
                "select pc_pr.id, pc_pr.person_id " +
                " from personrole_t pc_pr" +
                " where pc_pr.discriminator=?",
                physicalContactorEntityCode
            );

            Map<String, MutableInt> countMap = SafeMap.get(MutableInt.class);
            for (Map.Entry<Long, Long> e: sc2personMap.entrySet()) {
                Long scId = e.getKey();
                Long pcId = person2pcMap.get(e.getValue());
                System.out.println(scId+" -> "+pcId);

                for (String[] column: COLUMNS) {

                    if ("ctr_contractor_t".equals(column[0]) && "contactor_id".equals(column[1])) {
                        // здесь нужно делать мерж (набор полей owner_id, contactor_id - уникальный) и существуют договоры, в которых один и тот же человек одновременно есть как ФЛ и студент

                        // сначала изменяем все, что изменяемо (где нет дублей)
                        int i = tool.executeUpdate("update ctr_contractor_t set contactor_id=? where contactor_id=? and not exists(select x.id from ctr_contractor_t x where x.owner_id=owner_id and x.contactor_id=?)", pcId, scId, pcId);
                        if (i > 0) { countMap.get(column[0]+"."+column[1]).add(i); }

                        // затем выгружаем все, что дубли имеет и перебрасываем связи
                        Map<Long, Long> scContactor2pcContactorMap = tool.executeQuery(
                                (ctool, rs) -> {
                                    final Map<Long, Long> result = new HashMap<>(1024);
                                    while (rs.next()) {
                                        Long scCId = getLong(rs, 1);
                                        Long pcCId = getLong(rs, 2);
                                        if (null != result.put(scCId, pcCId)) { throw new IllegalStateException(/*здесь должны быть уникальные записи*/); }
                                    }
                                    return result;
                                },
                            "select sc_c.id, pc_c.id " +
                            " from ctr_contractor_t sc_c, ctr_contractor_t pc_c" +
                            " where sc_c.owner_id=pc_c.owner_id and sc_c.contactor_id=? and pc_c.contactor_id=?",
                            scId, pcId
                        );

                        for (Map.Entry<Long, Long> ec: scContactor2pcContactorMap.entrySet()) {
                            // перебрасываем связи
                            for (String[] column2: COLUMNS2) {
                                moveReferences(tool, column2[0], column2[1], ec.getKey(), ec.getValue(), countMap);
                            }
                            // удаляем
                            tool.executeUpdate("delete from ctr_contractor_t where id=?", ec.getKey());
                        }

                        // вызываем еще раз (если все выше сделано верно - здесь должно быть 0 записей)
                        moveReferences(tool, column[0], column[1], scId, pcId, countMap);
                    }
                    else
                    {
                        moveReferences(tool, column[0], column[1], scId, pcId, countMap);
                    }
                }
            }

            for (Map.Entry<String, MutableInt> e: countMap.entrySet()) {
                System.out.println(e.getKey()+" -> "+e.getValue());
            }

            // удаляем данные
            tool.deleteRowsByEntityCode(studentContactorEntityCode, "studentcontactor_t", "contactorperson_t", "personrole_t");
            // удалить таблицу
            tool.dropTable("studentcontactor_t", true);
            // удалить код сущности
            tool.entityCodes().delete("studentContactor");

        }


    }

    private void moveReferences(final DBTool tool, String tableName, String columnName, Long idFrom, Long idTo, Map<String, MutableInt> countMap) throws SQLException {
        if (tool.tableExists(tableName) && tool.columnExists(tableName, columnName)) {
            int i = tool.executeUpdate("update "+tableName+" set "+columnName+"=? where "+columnName+"=?", idTo, idFrom);
            if (i > 0) { countMap.get(tableName+"."+columnName).add(i); }
        }
    }
}