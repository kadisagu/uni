/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unictr.component.contactperson.ContactPersonAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Return;
import ru.tandemservice.unictr.entity.catalog.ContractorPost;
import ru.tandemservice.unictr.entity.contractor.ContactPerson;

import java.util.List;

/**
 * @author OKhmelevskaya
 * @since 30.01.2008
 */

@Input({
	@Bind(key="contactPersonId", binding="contactPerson.id"),
	@Bind(key="createdCatalogItemId", binding="createdCatalogItemId"),
	@Bind(key="contractorId", binding="contractorId")
})
@Return(@Bind(key="contactPersonId", binding="contactPerson.id"))
@SuppressWarnings({"deprecation"})
public class Model
{
    private Long _contractorId;
    private ContactPerson _contactPerson = new ContactPerson();
    private List<ContractorPost> _postList;
    private boolean leader;
    private Long _createdCatalogItemId;
    
    public Long getCreatedCatalogItemId() { return _createdCatalogItemId; }
    public void setCreatedCatalogItemId(Long createdCatalogItemId) { this._createdCatalogItemId = createdCatalogItemId; }

    public void setContractorId(Long contractorId) { _contractorId = contractorId; }
    public Long getContractorId() { return _contractorId; }

    public void setContactPerson(ContactPerson contactPerson) { _contactPerson = contactPerson; }
    public ContactPerson getContactPerson() { return _contactPerson; }

    public void setPostList(List<ContractorPost> postList) {  _postList = postList; }
    public List<ContractorPost> getPostList() { return _postList; }
    
    public boolean isLeader() { return leader; }
    public void setLeader(boolean leader) { this.leader = leader; }
}
