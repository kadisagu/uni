/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unictr.component.contractor.ContractorList;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.ctr.catalog.entity.LegalForm;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unictr.entity.contractor.ContactPerson;
import ru.tandemservice.unictr.entity.contractor.Contractor;

import java.util.List;

/**
 * @author OKhmelevskaya
 * Created on: 25.01.2008
 */
@SuppressWarnings("deprecation")
public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("e");

    @Override
    public void prepare(Model model)
    {
        model.setLegalFormList(getCatalogItemList(LegalForm.class));
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(Contractor.ENTITY_CLASS, "e");
        filterByTitle(model, builder);
        filterByHeadOrganization(model, builder);
        filterByLegalForm(model, builder);

        _orderSettings.applyOrder(builder, model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }

    private void filterByTitle(Model model, MQBuilder builder)
    {
        String title = model.getContractor().getTitle();
        if (!StringUtils.isEmpty(title) && !title.equals("*"))
        {
            AbstractExpression expr1 = MQExpression.like("e", Contractor.P_TITLE, "%" + title);
            AbstractExpression expr2 = MQExpression.like("e", Contractor.P_SHORT_TITLE, "%" + title);
            builder.add(MQExpression.or(expr1, expr2));
        }
    }

    private void filterByHeadOrganization(Model model, MQBuilder builder)
    {
        if (!StringUtils.isEmpty(model.getHeadOrgTitle()) && !model.getHeadOrgTitle().equals("*"))
        {
            builder.addJoin("e", Contractor.L_HEAD_ORGANIZATION, "headOrg");
            builder.add(MQExpression.like("headOrg", Contractor.P_TITLE, "%" + model.getHeadOrgTitle()));
        }
    }

    private void filterByLegalForm(Model model, MQBuilder builder)
    {
        ICatalogItem legalForm = model.getContractor().getLegalForm();
        if (legalForm != null)
        {
            builder.addJoin("e", Contractor.L_LEGAL_FORM, "legalForm");
            builder.add(MQExpression.eq("legalForm", "id", legalForm.getId()));
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void deleteContractor(Long contractorId)
    {
        Contractor contractor = get(Contractor.class, contractorId);
        List<Contractor> contractorList = getSession().createCriteria(Contractor.class).add(Restrictions.eq(Contractor.L_HEAD_ORGANIZATION, contractor)).list();
        for (Contractor c : contractorList)
        {
            c.setHeadOrganization(null);
            getSession().update(c);
        }
        List<ContactPerson> contactPersonList = getSession().createCriteria(ContactPerson.class).add(Restrictions.eq(ContactPerson.L_CONTRACTOR, contractor)).list();
        for (ContactPerson cp : contactPersonList)
        {
            getSession().delete(cp);
        }
        getSession().delete(contractor);
    }
}
