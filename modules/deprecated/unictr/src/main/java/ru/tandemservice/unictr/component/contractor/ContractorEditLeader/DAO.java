/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unictr.component.contractor.ContractorEditLeader;

import org.hibernate.criterion.Restrictions;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unictr.entity.catalog.ContractorPost;
import ru.tandemservice.unictr.entity.contractor.ContactPerson;
import ru.tandemservice.unictr.entity.contractor.Contractor;

/**
 * @author OKhmelevskaya
 * Created on: 31.01.2008
 */
@SuppressWarnings("deprecation")
public class DAO extends UniDao<Model> implements IDAO
{
    @SuppressWarnings("unchecked")
    @Override
    public void prepare(Model model)
    {
        Contractor contractor = get(Contractor.class, model.getContractorId());
        model.setLeader(contractor.getLeader());
        if(contractor.getLeader() != null)
            model.setPost(contractor.getLeader().getPost());
        model.setContactPersonList(getSession().createCriteria(ContactPerson.class).add(Restrictions.eq(ContactPerson.L_CONTRACTOR, contractor)).list());
        model.setPostList(getCatalogItemList(ContractorPost.class));
    }

    @Override
    public void update(Model model)
    {
        ContactPerson leader =  model.getLeader();
        leader.setPost(model.getPost());
        update(leader);
        
        Contractor contractor = get(Contractor.class, model.getContractorId());
        contractor.setLeader(leader);
        update(contractor);
    }
}
