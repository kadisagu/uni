/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unictr.component.contractor.ContractorList;

import org.tandemframework.core.component.Output;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.ctr.catalog.entity.LegalForm;
import ru.tandemservice.unictr.entity.contractor.Contractor;

import java.util.List;

/**
 * @author OKhmelevskaya
 * @since 25.01.2008
 */
@SuppressWarnings("deprecation")
@Output(keys = "contractorId", bindings = "rowId")
public class Model
{
    private Contractor _contractor = new Contractor();
    private DynamicListDataSource<Contractor> _dataSource;
    private List<LegalForm> _legalFormList;
    private String _headOrgTitle;
    private Long _rowId;

    public Contractor getContractor()
    {
        return _contractor;
    }

    public void setContractor(Contractor contractor)
    {
        _contractor = contractor;
    }

    public DynamicListDataSource<Contractor> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<Contractor> dataSource)
    {
        _dataSource = dataSource;
    }

    public Long getRowId()
    {
        return _rowId;
    }

    public void setRowId(Long rowId)
    {
        _rowId = rowId;
    }

    public void setLegalFormList(List<LegalForm> legalFormList)
    {
        _legalFormList = legalFormList;
    }

    public List<LegalForm> getLegalFormList()
    {
        return _legalFormList;
    }

    public void setHeadOrgTitle(String headOrgTitle)
    {
        _headOrgTitle = headOrgTitle;
    }

    public String getHeadOrgTitle()
    {
        return _headOrgTitle;
    }
}
