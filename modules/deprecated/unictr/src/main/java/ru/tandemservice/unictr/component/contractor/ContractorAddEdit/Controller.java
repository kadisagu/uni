/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unictr.component.contractor.ContractorAddEdit;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import org.tandemframework.shared.ctr.catalog.entity.ScopeOfActivity;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInline;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInlineConfig;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInlineUI;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import ru.tandemservice.unictr.entity.contractor.gen.ContractorGen;

import java.util.Collections;

/**
 * @author OKhmelevskaya
 *         Created on: 26.01.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    public static final String REGION_LEGAL_ADDRESS = "legalAddressRegion";

    @Override
    public void onRefreshComponent(IBusinessComponent context)
    {
        Model model = getModel(context);
        getDao().prepare(model);

        AddressBaseEditInlineConfig addressConfig = new AddressBaseEditInlineConfig();
        addressConfig.setEntityId(model.getContractor().getId() == null ? null : model.getContractor().getId());
        addressConfig.setDetailedOnly(true);
        addressConfig.setDetailLevel(4);
        addressConfig.setAreaVisible(true);
        addressConfig.setInline(true);
        addressConfig.setSettlementRequired(true);
        addressConfig.setFieldSetTitle("Юридический адрес");
        addressConfig.setAddressProperty(ContractorGen.L_LEGAL_ADDRESS);
        context.createChildRegion(REGION_LEGAL_ADDRESS, new ComponentActivator(AddressBaseEditInline.class.getSimpleName(), new ParametersMap().add(AddressBaseEditInlineUI.BIND_CONFIG, addressConfig)));
    }

    public void onClickAddScopeOfActivity(IBusinessComponent context)
    {
        context.createDefaultChildRegion(new ComponentActivator(
                DefaultCatalogAddEditModel.class.getPackage().getName(),
                Collections.singletonMap("catalogCode", (Object) ScopeOfActivity.ENTITY_NAME)
        ));
    }

    public void onClickApply(IBusinessComponent context)
    {
        AddressBaseEditInlineUI addressBaseEditInlineUI = (AddressBaseEditInlineUI) context.getChildRegion(REGION_LEGAL_ADDRESS).getActiveComponent().getPresenter();

        if(addressBaseEditInlineUI.getResult() == null) throw new RuntimeException("Адрес обязателен");
        if(!(addressBaseEditInlineUI.getResult() instanceof AddressDetailed)) throw new RuntimeException("Некорректный адрес");
        AddressDetailed legalAddress = (AddressDetailed) addressBaseEditInlineUI.getResult();

        Model model = getModel(context);
        model.setLegalAddress(legalAddress);

        getDao().update(model);
        deactivate(context);
    }




}
