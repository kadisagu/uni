package ru.tandemservice.unictr.entity.contractor.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.ctr.catalog.entity.LegalForm;
import org.tandemframework.shared.ctr.catalog.entity.ScopeOfActivity;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import ru.tandemservice.unictr.entity.contractor.ContactPerson;
import ru.tandemservice.unictr.entity.contractor.Contractor;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Контрагент
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ContractorGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unictr.entity.contractor.Contractor";
    public static final String ENTITY_NAME = "contractor";
    public static final int VERSION_HASH = -24300386;
    private static IEntityMeta ENTITY_META;

    public static final String P_TITLE = "title";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String L_LEGAL_FORM = "legalForm";
    public static final String P_SUBDIVISION = "subdivision";
    public static final String P_GROUND = "ground";
    public static final String L_HEAD_ORGANIZATION = "headOrganization";
    public static final String L_SCOPE_OF_ACTIVITY = "scopeOfActivity";
    public static final String P_STUFF_QUANTITY = "stuffQuantity";
    public static final String L_LEADER = "leader";
    public static final String P_PAYEE = "payee";
    public static final String P_INN = "inn";
    public static final String P_BANK = "bank";
    public static final String P_CUR_ACCOUNT = "curAccount";
    public static final String P_COR_ACCOUNT = "corAccount";
    public static final String P_BIC = "bic";
    public static final String P_KPP = "kpp";
    public static final String P_OKATO = "okato";
    public static final String P_OGRN = "ogrn";
    public static final String P_OKONH = "okonh";
    public static final String P_OKPO = "okpo";
    public static final String P_INCOME_CODE = "incomeCode";
    public static final String L_ADDRESS = "address";
    public static final String L_LEGAL_ADDRESS = "legalAddress";
    public static final String P_EMAIL = "email";
    public static final String P_ICQ = "icq";
    public static final String P_WEBPAGE = "webpage";
    public static final String P_PHONE = "phone";
    public static final String P_FAX = "fax";
    public static final String P_REGIONAL_INFO = "regionalInfo";
    public static final String P_COMMENTS = "comments";

    private String _title;     // Название
    private String _shortTitle;     // Сокращенное название
    private LegalForm _legalForm;     // Организационно-правовая форма
    private String _subdivision;     // Подразделение
    private String _ground;     // Основания
    private Contractor _headOrganization;     // Головная организация
    private ScopeOfActivity _scopeOfActivity;     // Сфера деятельности
    private Integer _stuffQuantity;     // Общая численность штата
    private ContactPerson _leader;     // Руководитель
    private String _payee;     // Получатель платежа
    private String _inn;     // ИНН
    private String _bank;     // Название банка
    private String _curAccount;     // Расчетный счет
    private String _corAccount;     // Корр. счет
    private String _bic;     // БИК
    private String _kpp;     // КПП
    private String _okato;     // ОКАТО
    private String _ogrn;     // ОГРН
    private String _okonh;     // ОКОНХ
    private String _okpo;     // ОКПО
    private String _incomeCode;     // Код видов доходов
    private AddressDetailed _address;     // Адрес местоположения
    private AddressDetailed _legalAddress;     // Адрес юридический
    private String _email;     // E-mail
    private String _icq;     // ICQ
    private String _webpage;     // Веб-страница
    private String _phone;     // Телефон
    private String _fax;     // Факс
    private String _regionalInfo;     // Региональные характеристики
    private String _comments;     // Примечания

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название. Свойство не может быть null.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Организационно-правовая форма. Свойство не может быть null.
     */
    @NotNull
    public LegalForm getLegalForm()
    {
        return _legalForm;
    }

    /**
     * @param legalForm Организационно-правовая форма. Свойство не может быть null.
     */
    public void setLegalForm(LegalForm legalForm)
    {
        dirty(_legalForm, legalForm);
        _legalForm = legalForm;
    }

    /**
     * @return Подразделение.
     */
    @Length(max=255)
    public String getSubdivision()
    {
        return _subdivision;
    }

    /**
     * @param subdivision Подразделение.
     */
    public void setSubdivision(String subdivision)
    {
        dirty(_subdivision, subdivision);
        _subdivision = subdivision;
    }

    /**
     * @return Основания.
     */
    @Length(max=255)
    public String getGround()
    {
        return _ground;
    }

    /**
     * @param ground Основания.
     */
    public void setGround(String ground)
    {
        dirty(_ground, ground);
        _ground = ground;
    }

    /**
     * @return Головная организация.
     */
    public Contractor getHeadOrganization()
    {
        return _headOrganization;
    }

    /**
     * @param headOrganization Головная организация.
     */
    public void setHeadOrganization(Contractor headOrganization)
    {
        dirty(_headOrganization, headOrganization);
        _headOrganization = headOrganization;
    }

    /**
     * @return Сфера деятельности.
     */
    public ScopeOfActivity getScopeOfActivity()
    {
        return _scopeOfActivity;
    }

    /**
     * @param scopeOfActivity Сфера деятельности.
     */
    public void setScopeOfActivity(ScopeOfActivity scopeOfActivity)
    {
        dirty(_scopeOfActivity, scopeOfActivity);
        _scopeOfActivity = scopeOfActivity;
    }

    /**
     * @return Общая численность штата.
     */
    public Integer getStuffQuantity()
    {
        return _stuffQuantity;
    }

    /**
     * @param stuffQuantity Общая численность штата.
     */
    public void setStuffQuantity(Integer stuffQuantity)
    {
        dirty(_stuffQuantity, stuffQuantity);
        _stuffQuantity = stuffQuantity;
    }

    /**
     * @return Руководитель.
     */
    public ContactPerson getLeader()
    {
        return _leader;
    }

    /**
     * @param leader Руководитель.
     */
    public void setLeader(ContactPerson leader)
    {
        dirty(_leader, leader);
        _leader = leader;
    }

    /**
     * @return Получатель платежа.
     */
    @Length(max=255)
    public String getPayee()
    {
        return _payee;
    }

    /**
     * @param payee Получатель платежа.
     */
    public void setPayee(String payee)
    {
        dirty(_payee, payee);
        _payee = payee;
    }

    /**
     * @return ИНН.
     */
    @Length(max=255)
    public String getInn()
    {
        return _inn;
    }

    /**
     * @param inn ИНН.
     */
    public void setInn(String inn)
    {
        dirty(_inn, inn);
        _inn = inn;
    }

    /**
     * @return Название банка.
     */
    @Length(max=255)
    public String getBank()
    {
        return _bank;
    }

    /**
     * @param bank Название банка.
     */
    public void setBank(String bank)
    {
        dirty(_bank, bank);
        _bank = bank;
    }

    /**
     * @return Расчетный счет.
     */
    @Length(max=255)
    public String getCurAccount()
    {
        return _curAccount;
    }

    /**
     * @param curAccount Расчетный счет.
     */
    public void setCurAccount(String curAccount)
    {
        dirty(_curAccount, curAccount);
        _curAccount = curAccount;
    }

    /**
     * @return Корр. счет.
     */
    @Length(max=255)
    public String getCorAccount()
    {
        return _corAccount;
    }

    /**
     * @param corAccount Корр. счет.
     */
    public void setCorAccount(String corAccount)
    {
        dirty(_corAccount, corAccount);
        _corAccount = corAccount;
    }

    /**
     * @return БИК.
     */
    @Length(max=255)
    public String getBic()
    {
        return _bic;
    }

    /**
     * @param bic БИК.
     */
    public void setBic(String bic)
    {
        dirty(_bic, bic);
        _bic = bic;
    }

    /**
     * @return КПП.
     */
    @Length(max=255)
    public String getKpp()
    {
        return _kpp;
    }

    /**
     * @param kpp КПП.
     */
    public void setKpp(String kpp)
    {
        dirty(_kpp, kpp);
        _kpp = kpp;
    }

    /**
     * @return ОКАТО.
     */
    @Length(max=255)
    public String getOkato()
    {
        return _okato;
    }

    /**
     * @param okato ОКАТО.
     */
    public void setOkato(String okato)
    {
        dirty(_okato, okato);
        _okato = okato;
    }

    /**
     * @return ОГРН.
     */
    @Length(max=255)
    public String getOgrn()
    {
        return _ogrn;
    }

    /**
     * @param ogrn ОГРН.
     */
    public void setOgrn(String ogrn)
    {
        dirty(_ogrn, ogrn);
        _ogrn = ogrn;
    }

    /**
     * @return ОКОНХ.
     */
    @Length(max=255)
    public String getOkonh()
    {
        return _okonh;
    }

    /**
     * @param okonh ОКОНХ.
     */
    public void setOkonh(String okonh)
    {
        dirty(_okonh, okonh);
        _okonh = okonh;
    }

    /**
     * @return ОКПО.
     */
    @Length(max=255)
    public String getOkpo()
    {
        return _okpo;
    }

    /**
     * @param okpo ОКПО.
     */
    public void setOkpo(String okpo)
    {
        dirty(_okpo, okpo);
        _okpo = okpo;
    }

    /**
     * @return Код видов доходов.
     */
    @Length(max=255)
    public String getIncomeCode()
    {
        return _incomeCode;
    }

    /**
     * @param incomeCode Код видов доходов.
     */
    public void setIncomeCode(String incomeCode)
    {
        dirty(_incomeCode, incomeCode);
        _incomeCode = incomeCode;
    }

    /**
     * @return Адрес местоположения.
     */
    public AddressDetailed getAddress()
    {
        return _address;
    }

    /**
     * @param address Адрес местоположения.
     */
    public void setAddress(AddressDetailed address)
    {
        dirty(_address, address);
        _address = address;
    }

    /**
     * @return Адрес юридический.
     */
    public AddressDetailed getLegalAddress()
    {
        return _legalAddress;
    }

    /**
     * @param legalAddress Адрес юридический.
     */
    public void setLegalAddress(AddressDetailed legalAddress)
    {
        dirty(_legalAddress, legalAddress);
        _legalAddress = legalAddress;
    }

    /**
     * @return E-mail.
     */
    @Length(max=255)
    public String getEmail()
    {
        return _email;
    }

    /**
     * @param email E-mail.
     */
    public void setEmail(String email)
    {
        dirty(_email, email);
        _email = email;
    }

    /**
     * @return ICQ.
     */
    @Length(max=255)
    public String getIcq()
    {
        return _icq;
    }

    /**
     * @param icq ICQ.
     */
    public void setIcq(String icq)
    {
        dirty(_icq, icq);
        _icq = icq;
    }

    /**
     * @return Веб-страница.
     */
    @Length(max=255)
    public String getWebpage()
    {
        return _webpage;
    }

    /**
     * @param webpage Веб-страница.
     */
    public void setWebpage(String webpage)
    {
        dirty(_webpage, webpage);
        _webpage = webpage;
    }

    /**
     * @return Телефон.
     */
    @Length(max=255)
    public String getPhone()
    {
        return _phone;
    }

    /**
     * @param phone Телефон.
     */
    public void setPhone(String phone)
    {
        dirty(_phone, phone);
        _phone = phone;
    }

    /**
     * @return Факс.
     */
    @Length(max=255)
    public String getFax()
    {
        return _fax;
    }

    /**
     * @param fax Факс.
     */
    public void setFax(String fax)
    {
        dirty(_fax, fax);
        _fax = fax;
    }

    /**
     * @return Региональные характеристики.
     */
    @Length(max=255)
    public String getRegionalInfo()
    {
        return _regionalInfo;
    }

    /**
     * @param regionalInfo Региональные характеристики.
     */
    public void setRegionalInfo(String regionalInfo)
    {
        dirty(_regionalInfo, regionalInfo);
        _regionalInfo = regionalInfo;
    }

    /**
     * @return Примечания.
     */
    @Length(max=255)
    public String getComments()
    {
        return _comments;
    }

    /**
     * @param comments Примечания.
     */
    public void setComments(String comments)
    {
        dirty(_comments, comments);
        _comments = comments;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ContractorGen)
        {
            setTitle(((Contractor)another).getTitle());
            setShortTitle(((Contractor)another).getShortTitle());
            setLegalForm(((Contractor)another).getLegalForm());
            setSubdivision(((Contractor)another).getSubdivision());
            setGround(((Contractor)another).getGround());
            setHeadOrganization(((Contractor)another).getHeadOrganization());
            setScopeOfActivity(((Contractor)another).getScopeOfActivity());
            setStuffQuantity(((Contractor)another).getStuffQuantity());
            setLeader(((Contractor)another).getLeader());
            setPayee(((Contractor)another).getPayee());
            setInn(((Contractor)another).getInn());
            setBank(((Contractor)another).getBank());
            setCurAccount(((Contractor)another).getCurAccount());
            setCorAccount(((Contractor)another).getCorAccount());
            setBic(((Contractor)another).getBic());
            setKpp(((Contractor)another).getKpp());
            setOkato(((Contractor)another).getOkato());
            setOgrn(((Contractor)another).getOgrn());
            setOkonh(((Contractor)another).getOkonh());
            setOkpo(((Contractor)another).getOkpo());
            setIncomeCode(((Contractor)another).getIncomeCode());
            setAddress(((Contractor)another).getAddress());
            setLegalAddress(((Contractor)another).getLegalAddress());
            setEmail(((Contractor)another).getEmail());
            setIcq(((Contractor)another).getIcq());
            setWebpage(((Contractor)another).getWebpage());
            setPhone(((Contractor)another).getPhone());
            setFax(((Contractor)another).getFax());
            setRegionalInfo(((Contractor)another).getRegionalInfo());
            setComments(((Contractor)another).getComments());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ContractorGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) Contractor.class;
        }

        public T newInstance()
        {
            return (T) new Contractor();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "title":
                    return obj.getTitle();
                case "shortTitle":
                    return obj.getShortTitle();
                case "legalForm":
                    return obj.getLegalForm();
                case "subdivision":
                    return obj.getSubdivision();
                case "ground":
                    return obj.getGround();
                case "headOrganization":
                    return obj.getHeadOrganization();
                case "scopeOfActivity":
                    return obj.getScopeOfActivity();
                case "stuffQuantity":
                    return obj.getStuffQuantity();
                case "leader":
                    return obj.getLeader();
                case "payee":
                    return obj.getPayee();
                case "inn":
                    return obj.getInn();
                case "bank":
                    return obj.getBank();
                case "curAccount":
                    return obj.getCurAccount();
                case "corAccount":
                    return obj.getCorAccount();
                case "bic":
                    return obj.getBic();
                case "kpp":
                    return obj.getKpp();
                case "okato":
                    return obj.getOkato();
                case "ogrn":
                    return obj.getOgrn();
                case "okonh":
                    return obj.getOkonh();
                case "okpo":
                    return obj.getOkpo();
                case "incomeCode":
                    return obj.getIncomeCode();
                case "address":
                    return obj.getAddress();
                case "legalAddress":
                    return obj.getLegalAddress();
                case "email":
                    return obj.getEmail();
                case "icq":
                    return obj.getIcq();
                case "webpage":
                    return obj.getWebpage();
                case "phone":
                    return obj.getPhone();
                case "fax":
                    return obj.getFax();
                case "regionalInfo":
                    return obj.getRegionalInfo();
                case "comments":
                    return obj.getComments();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "legalForm":
                    obj.setLegalForm((LegalForm) value);
                    return;
                case "subdivision":
                    obj.setSubdivision((String) value);
                    return;
                case "ground":
                    obj.setGround((String) value);
                    return;
                case "headOrganization":
                    obj.setHeadOrganization((Contractor) value);
                    return;
                case "scopeOfActivity":
                    obj.setScopeOfActivity((ScopeOfActivity) value);
                    return;
                case "stuffQuantity":
                    obj.setStuffQuantity((Integer) value);
                    return;
                case "leader":
                    obj.setLeader((ContactPerson) value);
                    return;
                case "payee":
                    obj.setPayee((String) value);
                    return;
                case "inn":
                    obj.setInn((String) value);
                    return;
                case "bank":
                    obj.setBank((String) value);
                    return;
                case "curAccount":
                    obj.setCurAccount((String) value);
                    return;
                case "corAccount":
                    obj.setCorAccount((String) value);
                    return;
                case "bic":
                    obj.setBic((String) value);
                    return;
                case "kpp":
                    obj.setKpp((String) value);
                    return;
                case "okato":
                    obj.setOkato((String) value);
                    return;
                case "ogrn":
                    obj.setOgrn((String) value);
                    return;
                case "okonh":
                    obj.setOkonh((String) value);
                    return;
                case "okpo":
                    obj.setOkpo((String) value);
                    return;
                case "incomeCode":
                    obj.setIncomeCode((String) value);
                    return;
                case "address":
                    obj.setAddress((AddressDetailed) value);
                    return;
                case "legalAddress":
                    obj.setLegalAddress((AddressDetailed) value);
                    return;
                case "email":
                    obj.setEmail((String) value);
                    return;
                case "icq":
                    obj.setIcq((String) value);
                    return;
                case "webpage":
                    obj.setWebpage((String) value);
                    return;
                case "phone":
                    obj.setPhone((String) value);
                    return;
                case "fax":
                    obj.setFax((String) value);
                    return;
                case "regionalInfo":
                    obj.setRegionalInfo((String) value);
                    return;
                case "comments":
                    obj.setComments((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "title":
                        return true;
                case "shortTitle":
                        return true;
                case "legalForm":
                        return true;
                case "subdivision":
                        return true;
                case "ground":
                        return true;
                case "headOrganization":
                        return true;
                case "scopeOfActivity":
                        return true;
                case "stuffQuantity":
                        return true;
                case "leader":
                        return true;
                case "payee":
                        return true;
                case "inn":
                        return true;
                case "bank":
                        return true;
                case "curAccount":
                        return true;
                case "corAccount":
                        return true;
                case "bic":
                        return true;
                case "kpp":
                        return true;
                case "okato":
                        return true;
                case "ogrn":
                        return true;
                case "okonh":
                        return true;
                case "okpo":
                        return true;
                case "incomeCode":
                        return true;
                case "address":
                        return true;
                case "legalAddress":
                        return true;
                case "email":
                        return true;
                case "icq":
                        return true;
                case "webpage":
                        return true;
                case "phone":
                        return true;
                case "fax":
                        return true;
                case "regionalInfo":
                        return true;
                case "comments":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "title":
                    return true;
                case "shortTitle":
                    return true;
                case "legalForm":
                    return true;
                case "subdivision":
                    return true;
                case "ground":
                    return true;
                case "headOrganization":
                    return true;
                case "scopeOfActivity":
                    return true;
                case "stuffQuantity":
                    return true;
                case "leader":
                    return true;
                case "payee":
                    return true;
                case "inn":
                    return true;
                case "bank":
                    return true;
                case "curAccount":
                    return true;
                case "corAccount":
                    return true;
                case "bic":
                    return true;
                case "kpp":
                    return true;
                case "okato":
                    return true;
                case "ogrn":
                    return true;
                case "okonh":
                    return true;
                case "okpo":
                    return true;
                case "incomeCode":
                    return true;
                case "address":
                    return true;
                case "legalAddress":
                    return true;
                case "email":
                    return true;
                case "icq":
                    return true;
                case "webpage":
                    return true;
                case "phone":
                    return true;
                case "fax":
                    return true;
                case "regionalInfo":
                    return true;
                case "comments":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "title":
                    return String.class;
                case "shortTitle":
                    return String.class;
                case "legalForm":
                    return LegalForm.class;
                case "subdivision":
                    return String.class;
                case "ground":
                    return String.class;
                case "headOrganization":
                    return Contractor.class;
                case "scopeOfActivity":
                    return ScopeOfActivity.class;
                case "stuffQuantity":
                    return Integer.class;
                case "leader":
                    return ContactPerson.class;
                case "payee":
                    return String.class;
                case "inn":
                    return String.class;
                case "bank":
                    return String.class;
                case "curAccount":
                    return String.class;
                case "corAccount":
                    return String.class;
                case "bic":
                    return String.class;
                case "kpp":
                    return String.class;
                case "okato":
                    return String.class;
                case "ogrn":
                    return String.class;
                case "okonh":
                    return String.class;
                case "okpo":
                    return String.class;
                case "incomeCode":
                    return String.class;
                case "address":
                    return AddressDetailed.class;
                case "legalAddress":
                    return AddressDetailed.class;
                case "email":
                    return String.class;
                case "icq":
                    return String.class;
                case "webpage":
                    return String.class;
                case "phone":
                    return String.class;
                case "fax":
                    return String.class;
                case "regionalInfo":
                    return String.class;
                case "comments":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<Contractor> _dslPath = new Path<Contractor>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "Contractor");
    }
            

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Организационно-правовая форма. Свойство не может быть null.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getLegalForm()
     */
    public static LegalForm.Path<LegalForm> legalForm()
    {
        return _dslPath.legalForm();
    }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getSubdivision()
     */
    public static PropertyPath<String> subdivision()
    {
        return _dslPath.subdivision();
    }

    /**
     * @return Основания.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getGround()
     */
    public static PropertyPath<String> ground()
    {
        return _dslPath.ground();
    }

    /**
     * @return Головная организация.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getHeadOrganization()
     */
    public static Contractor.Path<Contractor> headOrganization()
    {
        return _dslPath.headOrganization();
    }

    /**
     * @return Сфера деятельности.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getScopeOfActivity()
     */
    public static ScopeOfActivity.Path<ScopeOfActivity> scopeOfActivity()
    {
        return _dslPath.scopeOfActivity();
    }

    /**
     * @return Общая численность штата.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getStuffQuantity()
     */
    public static PropertyPath<Integer> stuffQuantity()
    {
        return _dslPath.stuffQuantity();
    }

    /**
     * @return Руководитель.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getLeader()
     */
    public static ContactPerson.Path<ContactPerson> leader()
    {
        return _dslPath.leader();
    }

    /**
     * @return Получатель платежа.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getPayee()
     */
    public static PropertyPath<String> payee()
    {
        return _dslPath.payee();
    }

    /**
     * @return ИНН.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getInn()
     */
    public static PropertyPath<String> inn()
    {
        return _dslPath.inn();
    }

    /**
     * @return Название банка.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getBank()
     */
    public static PropertyPath<String> bank()
    {
        return _dslPath.bank();
    }

    /**
     * @return Расчетный счет.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getCurAccount()
     */
    public static PropertyPath<String> curAccount()
    {
        return _dslPath.curAccount();
    }

    /**
     * @return Корр. счет.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getCorAccount()
     */
    public static PropertyPath<String> corAccount()
    {
        return _dslPath.corAccount();
    }

    /**
     * @return БИК.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getBic()
     */
    public static PropertyPath<String> bic()
    {
        return _dslPath.bic();
    }

    /**
     * @return КПП.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getKpp()
     */
    public static PropertyPath<String> kpp()
    {
        return _dslPath.kpp();
    }

    /**
     * @return ОКАТО.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getOkato()
     */
    public static PropertyPath<String> okato()
    {
        return _dslPath.okato();
    }

    /**
     * @return ОГРН.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getOgrn()
     */
    public static PropertyPath<String> ogrn()
    {
        return _dslPath.ogrn();
    }

    /**
     * @return ОКОНХ.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getOkonh()
     */
    public static PropertyPath<String> okonh()
    {
        return _dslPath.okonh();
    }

    /**
     * @return ОКПО.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getOkpo()
     */
    public static PropertyPath<String> okpo()
    {
        return _dslPath.okpo();
    }

    /**
     * @return Код видов доходов.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getIncomeCode()
     */
    public static PropertyPath<String> incomeCode()
    {
        return _dslPath.incomeCode();
    }

    /**
     * @return Адрес местоположения.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getAddress()
     */
    public static AddressDetailed.Path<AddressDetailed> address()
    {
        return _dslPath.address();
    }

    /**
     * @return Адрес юридический.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getLegalAddress()
     */
    public static AddressDetailed.Path<AddressDetailed> legalAddress()
    {
        return _dslPath.legalAddress();
    }

    /**
     * @return E-mail.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getEmail()
     */
    public static PropertyPath<String> email()
    {
        return _dslPath.email();
    }

    /**
     * @return ICQ.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getIcq()
     */
    public static PropertyPath<String> icq()
    {
        return _dslPath.icq();
    }

    /**
     * @return Веб-страница.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getWebpage()
     */
    public static PropertyPath<String> webpage()
    {
        return _dslPath.webpage();
    }

    /**
     * @return Телефон.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getPhone()
     */
    public static PropertyPath<String> phone()
    {
        return _dslPath.phone();
    }

    /**
     * @return Факс.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getFax()
     */
    public static PropertyPath<String> fax()
    {
        return _dslPath.fax();
    }

    /**
     * @return Региональные характеристики.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getRegionalInfo()
     */
    public static PropertyPath<String> regionalInfo()
    {
        return _dslPath.regionalInfo();
    }

    /**
     * @return Примечания.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getComments()
     */
    public static PropertyPath<String> comments()
    {
        return _dslPath.comments();
    }

    public static class Path<E extends Contractor> extends EntityPath<E>
    {
        private PropertyPath<String> _title;
        private PropertyPath<String> _shortTitle;
        private LegalForm.Path<LegalForm> _legalForm;
        private PropertyPath<String> _subdivision;
        private PropertyPath<String> _ground;
        private Contractor.Path<Contractor> _headOrganization;
        private ScopeOfActivity.Path<ScopeOfActivity> _scopeOfActivity;
        private PropertyPath<Integer> _stuffQuantity;
        private ContactPerson.Path<ContactPerson> _leader;
        private PropertyPath<String> _payee;
        private PropertyPath<String> _inn;
        private PropertyPath<String> _bank;
        private PropertyPath<String> _curAccount;
        private PropertyPath<String> _corAccount;
        private PropertyPath<String> _bic;
        private PropertyPath<String> _kpp;
        private PropertyPath<String> _okato;
        private PropertyPath<String> _ogrn;
        private PropertyPath<String> _okonh;
        private PropertyPath<String> _okpo;
        private PropertyPath<String> _incomeCode;
        private AddressDetailed.Path<AddressDetailed> _address;
        private AddressDetailed.Path<AddressDetailed> _legalAddress;
        private PropertyPath<String> _email;
        private PropertyPath<String> _icq;
        private PropertyPath<String> _webpage;
        private PropertyPath<String> _phone;
        private PropertyPath<String> _fax;
        private PropertyPath<String> _regionalInfo;
        private PropertyPath<String> _comments;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(ContractorGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(ContractorGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Организационно-правовая форма. Свойство не может быть null.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getLegalForm()
     */
        public LegalForm.Path<LegalForm> legalForm()
        {
            if(_legalForm == null )
                _legalForm = new LegalForm.Path<LegalForm>(L_LEGAL_FORM, this);
            return _legalForm;
        }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getSubdivision()
     */
        public PropertyPath<String> subdivision()
        {
            if(_subdivision == null )
                _subdivision = new PropertyPath<String>(ContractorGen.P_SUBDIVISION, this);
            return _subdivision;
        }

    /**
     * @return Основания.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getGround()
     */
        public PropertyPath<String> ground()
        {
            if(_ground == null )
                _ground = new PropertyPath<String>(ContractorGen.P_GROUND, this);
            return _ground;
        }

    /**
     * @return Головная организация.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getHeadOrganization()
     */
        public Contractor.Path<Contractor> headOrganization()
        {
            if(_headOrganization == null )
                _headOrganization = new Contractor.Path<Contractor>(L_HEAD_ORGANIZATION, this);
            return _headOrganization;
        }

    /**
     * @return Сфера деятельности.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getScopeOfActivity()
     */
        public ScopeOfActivity.Path<ScopeOfActivity> scopeOfActivity()
        {
            if(_scopeOfActivity == null )
                _scopeOfActivity = new ScopeOfActivity.Path<ScopeOfActivity>(L_SCOPE_OF_ACTIVITY, this);
            return _scopeOfActivity;
        }

    /**
     * @return Общая численность штата.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getStuffQuantity()
     */
        public PropertyPath<Integer> stuffQuantity()
        {
            if(_stuffQuantity == null )
                _stuffQuantity = new PropertyPath<Integer>(ContractorGen.P_STUFF_QUANTITY, this);
            return _stuffQuantity;
        }

    /**
     * @return Руководитель.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getLeader()
     */
        public ContactPerson.Path<ContactPerson> leader()
        {
            if(_leader == null )
                _leader = new ContactPerson.Path<ContactPerson>(L_LEADER, this);
            return _leader;
        }

    /**
     * @return Получатель платежа.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getPayee()
     */
        public PropertyPath<String> payee()
        {
            if(_payee == null )
                _payee = new PropertyPath<String>(ContractorGen.P_PAYEE, this);
            return _payee;
        }

    /**
     * @return ИНН.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getInn()
     */
        public PropertyPath<String> inn()
        {
            if(_inn == null )
                _inn = new PropertyPath<String>(ContractorGen.P_INN, this);
            return _inn;
        }

    /**
     * @return Название банка.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getBank()
     */
        public PropertyPath<String> bank()
        {
            if(_bank == null )
                _bank = new PropertyPath<String>(ContractorGen.P_BANK, this);
            return _bank;
        }

    /**
     * @return Расчетный счет.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getCurAccount()
     */
        public PropertyPath<String> curAccount()
        {
            if(_curAccount == null )
                _curAccount = new PropertyPath<String>(ContractorGen.P_CUR_ACCOUNT, this);
            return _curAccount;
        }

    /**
     * @return Корр. счет.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getCorAccount()
     */
        public PropertyPath<String> corAccount()
        {
            if(_corAccount == null )
                _corAccount = new PropertyPath<String>(ContractorGen.P_COR_ACCOUNT, this);
            return _corAccount;
        }

    /**
     * @return БИК.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getBic()
     */
        public PropertyPath<String> bic()
        {
            if(_bic == null )
                _bic = new PropertyPath<String>(ContractorGen.P_BIC, this);
            return _bic;
        }

    /**
     * @return КПП.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getKpp()
     */
        public PropertyPath<String> kpp()
        {
            if(_kpp == null )
                _kpp = new PropertyPath<String>(ContractorGen.P_KPP, this);
            return _kpp;
        }

    /**
     * @return ОКАТО.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getOkato()
     */
        public PropertyPath<String> okato()
        {
            if(_okato == null )
                _okato = new PropertyPath<String>(ContractorGen.P_OKATO, this);
            return _okato;
        }

    /**
     * @return ОГРН.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getOgrn()
     */
        public PropertyPath<String> ogrn()
        {
            if(_ogrn == null )
                _ogrn = new PropertyPath<String>(ContractorGen.P_OGRN, this);
            return _ogrn;
        }

    /**
     * @return ОКОНХ.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getOkonh()
     */
        public PropertyPath<String> okonh()
        {
            if(_okonh == null )
                _okonh = new PropertyPath<String>(ContractorGen.P_OKONH, this);
            return _okonh;
        }

    /**
     * @return ОКПО.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getOkpo()
     */
        public PropertyPath<String> okpo()
        {
            if(_okpo == null )
                _okpo = new PropertyPath<String>(ContractorGen.P_OKPO, this);
            return _okpo;
        }

    /**
     * @return Код видов доходов.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getIncomeCode()
     */
        public PropertyPath<String> incomeCode()
        {
            if(_incomeCode == null )
                _incomeCode = new PropertyPath<String>(ContractorGen.P_INCOME_CODE, this);
            return _incomeCode;
        }

    /**
     * @return Адрес местоположения.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getAddress()
     */
        public AddressDetailed.Path<AddressDetailed> address()
        {
            if(_address == null )
                _address = new AddressDetailed.Path<AddressDetailed>(L_ADDRESS, this);
            return _address;
        }

    /**
     * @return Адрес юридический.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getLegalAddress()
     */
        public AddressDetailed.Path<AddressDetailed> legalAddress()
        {
            if(_legalAddress == null )
                _legalAddress = new AddressDetailed.Path<AddressDetailed>(L_LEGAL_ADDRESS, this);
            return _legalAddress;
        }

    /**
     * @return E-mail.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getEmail()
     */
        public PropertyPath<String> email()
        {
            if(_email == null )
                _email = new PropertyPath<String>(ContractorGen.P_EMAIL, this);
            return _email;
        }

    /**
     * @return ICQ.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getIcq()
     */
        public PropertyPath<String> icq()
        {
            if(_icq == null )
                _icq = new PropertyPath<String>(ContractorGen.P_ICQ, this);
            return _icq;
        }

    /**
     * @return Веб-страница.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getWebpage()
     */
        public PropertyPath<String> webpage()
        {
            if(_webpage == null )
                _webpage = new PropertyPath<String>(ContractorGen.P_WEBPAGE, this);
            return _webpage;
        }

    /**
     * @return Телефон.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getPhone()
     */
        public PropertyPath<String> phone()
        {
            if(_phone == null )
                _phone = new PropertyPath<String>(ContractorGen.P_PHONE, this);
            return _phone;
        }

    /**
     * @return Факс.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getFax()
     */
        public PropertyPath<String> fax()
        {
            if(_fax == null )
                _fax = new PropertyPath<String>(ContractorGen.P_FAX, this);
            return _fax;
        }

    /**
     * @return Региональные характеристики.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getRegionalInfo()
     */
        public PropertyPath<String> regionalInfo()
        {
            if(_regionalInfo == null )
                _regionalInfo = new PropertyPath<String>(ContractorGen.P_REGIONAL_INFO, this);
            return _regionalInfo;
        }

    /**
     * @return Примечания.
     * @see ru.tandemservice.unictr.entity.contractor.Contractor#getComments()
     */
        public PropertyPath<String> comments()
        {
            if(_comments == null )
                _comments = new PropertyPath<String>(ContractorGen.P_COMMENTS, this);
            return _comments;
        }

        public Class getEntityClass()
        {
            return Contractor.class;
        }

        public String getEntityName()
        {
            return "contractor";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
