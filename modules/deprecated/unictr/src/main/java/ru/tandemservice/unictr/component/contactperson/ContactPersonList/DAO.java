/* $Id:DAO.java 2645 2008-04-29 07:54:04Z okhmelevskaya $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unictr.component.contactperson.ContactPersonList;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.ctr.catalog.entity.LegalForm;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unictr.entity.contractor.ContactPerson;
import ru.tandemservice.unictr.entity.contractor.Contractor;

/**
 * @author OKhmelevskaya
 * Created on: 31.01.2008
 */
@SuppressWarnings({"deprecation"})
public class DAO extends UniDao<Model> implements IDAO
{
    protected static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("e");
    static
    {
        _orderSettings.setOrders(ContactPerson.P_FULLFIO, new OrderDescription("e", ContactPerson.P_LAST_NAME), new OrderDescription("e", ContactPerson.P_FIRST_NAME), new OrderDescription("e", ContactPerson.P_MIDDLE_NAME));
    }
    
    @Override
    public void prepare(Model model)
    {
        model.setLegalFormList(getCatalogItemList(LegalForm.class));
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(ContactPerson.ENTITY_CLASS, "e");
        filterByLastName(model, builder);
        filterByPostName(model, builder);
        filterByContractor(model, builder);
        filterByHeadOrganization(model, builder);
        filterByLegalForm(model, builder);
        _orderSettings.applyOrder(builder, model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }

    private void filterByLastName(Model model, MQBuilder builder)
    {
        String lastName = model.getContactPerson().getLastName();
        if (!StringUtils.isEmpty(lastName) && !lastName.equals("*"))
        {
            builder.add(MQExpression.like("e", ContactPerson.P_LAST_NAME, "%" + lastName));
        }
    }
    
    private void filterByPostName(Model model, MQBuilder builder)
    {
        String title = model.getPostTitle();
        if (!StringUtils.isEmpty(title) && !title.equals("*"))
        {
            builder.add(MQExpression.like("e", ContactPerson.L_POST + ".title", "%" + title));
        }
    }

    private void filterByContractor(Model model, MQBuilder builder)
    {
        String title = model.getContractorTitle();
        if (!StringUtils.isEmpty(title) && !title.equals("*"))
        {
            builder.addJoin("e", ContactPerson.L_CONTRACTOR, "c");
            AbstractExpression expr1 = MQExpression.like("c", Contractor.P_TITLE, "%" + title);
            AbstractExpression expr2 = MQExpression.like("c", Contractor.P_SHORT_TITLE, "%" + title);
            builder.add(MQExpression.or(expr1, expr2));
        }
    }

    private void filterByHeadOrganization(Model model, MQBuilder builder)
    {
        String title = model.getHeadOrgTitle();
        if (!StringUtils.isEmpty(title) && !title.equals("*"))
        {
            builder.addJoin("e", ContactPerson.L_CONTRACTOR, "c");
            builder.addJoin("c", Contractor.L_HEAD_ORGANIZATION, "headOrg");
            AbstractExpression expr1 = MQExpression.like("headOrg", Contractor.P_TITLE, "%" + title);
            AbstractExpression expr2 = MQExpression.like("headOrg", Contractor.P_SHORT_TITLE, "%" + title);
            builder.add(MQExpression.or(expr1, expr2));
        }
    }

    private void filterByLegalForm(Model model, MQBuilder builder)
    {
        Contractor contractor = model.getContractor();
        if (null != contractor && null != contractor.getLegalForm())
            builder.add(MQExpression.eq("e", ContactPerson.L_CONTRACTOR + "." + Contractor.L_LEGAL_FORM, contractor.getLegalForm()));
    }
}
