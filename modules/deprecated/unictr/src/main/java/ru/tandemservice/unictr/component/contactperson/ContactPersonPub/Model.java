/* $Id:Model.java 2645 2008-04-29 07:54:04Z okhmelevskaya $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unictr.component.contactperson.ContactPersonPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import ru.tandemservice.unictr.entity.contractor.ContactPerson;

/**
 * @author OKhmelevskaya
 *         Created on: 30.01.2008
 */
@SuppressWarnings("deprecation")
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "contactPerson.id"),
        @Bind(key = "selectedPage", binding = "selectedPage")
})
@Output(keys = {"contactPersonId", "contractorId"}, bindings = {"contactPerson.id", "contractorId"})
public class Model
{
    private String _selectedPage;
    private ContactPerson _contactPerson = new ContactPerson();
    private Long _contractorId;

    public void setSelectedPage(String selectedPage)
    {
        _selectedPage = selectedPage;
    }

    public String getSelectedPage()
    {
        return _selectedPage;
    }

    public void setContactPerson(ContactPerson contactPerson)
    {
        _contactPerson = contactPerson;
    }

    public ContactPerson getContactPerson()
    {
        return _contactPerson;
    }

    public void setContractorId(Long contractorId)
    {
        _contractorId = contractorId;
    }

    public Long getContractorId()
    {
        return _contractorId;
    }
}
