/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unictr.entity.contractor;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import ru.tandemservice.unictr.entity.contractor.gen.ContactPersonGen;

@Deprecated
public class ContactPerson extends ContactPersonGen
{
    public static final String P_FULLFIO = "fullFio";

    public String getFullFio()
    {
        return getLastName() + " " + getFirstName() + (getMiddleName() == null ? "" : " " + getMiddleName());
    }

    public static final String P_LEADER = "leader";

    public boolean isLeader()
    {
        return equals(getContractor().getLeader());
    }

    public static final String P_ALL_PHONES = "allPhones";

    public String getAllPhones()
    {
        List<String> phoneArray = new ArrayList<String>();

        if (!StringUtils.isEmpty(getWorkPhone())) phoneArray.add(getWorkPhone());
        if (!StringUtils.isEmpty(getMobilePhone())) phoneArray.add(getMobilePhone());
        if (!StringUtils.isEmpty(getPhone())) phoneArray.add(getPhone());

        return StringUtils.join(phoneArray.iterator(), ", ");
    }
}
