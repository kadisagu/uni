/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unictr.component.contractor.ContractorAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Return;
import org.tandemframework.shared.ctr.catalog.entity.LegalForm;
import org.tandemframework.shared.ctr.catalog.entity.ScopeOfActivity;
import org.tandemframework.shared.fias.base.entity.Address;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import ru.tandemservice.unictr.entity.contractor.Contractor;

import java.util.List;

/**
 * @author OKhmelevskaya
 * @since 26.01.2008
 */
@SuppressWarnings("deprecation")
@Input({
    @Bind(key="contractorId", binding="contractor.id"),
    @Bind(key="createdCatalogItemId", binding="createdCatalogItemId")
})
@Return(@Bind(key="contractorId", binding="contractor.id"))
public class Model
{
    private Contractor _contractor = new Contractor();
    private List<LegalForm> _legalFormList;
    private List<Contractor> _headOrganizationList;
    private List<ScopeOfActivity> _scopeOfActivityList;
    private Long _createdCatalogItemId;
    private AddressDetailed _legalAddress;

    public Long getCreatedCatalogItemId() { return _createdCatalogItemId; }
    public void setCreatedCatalogItemId(Long createdCatalogItemId) { this._createdCatalogItemId = createdCatalogItemId; }

    public void setContractor(Contractor contractor) { _contractor = contractor; }
    public Contractor getContractor() { return _contractor; }

    public void setLegalFormList(List<LegalForm> legalFormList) { _legalFormList = legalFormList; }
    public List<LegalForm> getLegalFormList() { return _legalFormList; }

    public void setHeadOrganizationList(List<Contractor> headOrganizationList) { _headOrganizationList = headOrganizationList; }
    public List<Contractor> getHeadOrganizationList() { return _headOrganizationList; }

    public void setScopeOfActivityList(List<ScopeOfActivity> scopeOfActivityList) { _scopeOfActivityList = scopeOfActivityList; }
    public List<ScopeOfActivity> getScopeOfActivityList() { return _scopeOfActivityList; }


    public AddressDetailed getLegalAddress()
    {
        return _legalAddress;
    }

    public void setLegalAddress(AddressDetailed legalAddress)
    {
        _legalAddress = legalAddress;
    }
}
