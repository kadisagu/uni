/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unictr.component.contractor.ContractorPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;

/**
 * @author OKhmelevskaya
 * Created on: 30.01.2008
 */
@SuppressWarnings("deprecation")
public class ContactPersonDataSourceDelegate implements IListDataSourceDelegate
{
    private Controller _controller;

    public ContactPersonDataSourceDelegate(Controller controller)
    {
        _controller = controller;
    }

    @Override
    public void updateListDataSource(IBusinessComponent context)
    {
       _controller.getDao().prepareListDataSource(_controller.getModel(context));
    }
}
