/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unictr.component.contractor.ContractorAddEdit;

import java.util.ArrayList;
import java.util.List;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.fias.base.bo.AddressBase.AddressBaseManager;

import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import org.tandemframework.shared.ctr.catalog.entity.LegalForm;
import org.tandemframework.shared.ctr.catalog.entity.ScopeOfActivity;
import ru.tandemservice.unictr.entity.contractor.Contractor;

/**
 * @author OKhmelevskaya
 * Created on: 26.01.2008
 */
@Deprecated
@SuppressWarnings("deprecation")
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        if (null != model.getContractor().getId()) {
            model.setContractor(get(Contractor.class, model.getContractor().getId()));
        }

        model.setLegalFormList(getCatalogItemList(LegalForm.class));
        model.setHeadOrganizationList(getAvailableHeadOrgs(model));
        model.setScopeOfActivityList(getCatalogItemList(ScopeOfActivity.class));

        if (null != model.getCreatedCatalogItemId()) {
            final ICatalogItem item = get(model.getCreatedCatalogItemId());
            if (item instanceof ScopeOfActivity) {
                model.getContractor().setScopeOfActivity((ScopeOfActivity) item);
            }
        }
    }

    @SuppressWarnings("unchecked")
    private List<Contractor> getAvailableHeadOrgs(Model model)
    {
        if (model.getContractor().getId() != null)
        {
            MQBuilder builder = new MQBuilder(Contractor.ENTITY_CLASS, "e");
            builder.add(MQExpression.notIn("e", "id", getDescendantContractorIds(model.getContractor())));
            builder.add(MQExpression.notEq("e", "id", model.getContractor().getId()));
            return builder.getResultList(getSession());
        }
        else
            return getSession().createCriteria(Contractor.class).list();
    }

    private List<Long> getDescendantContractorIds(Contractor contractor)
    {
        List<Long> result = new ArrayList<Long>();
        List<Long> childIds = new ArrayList<Long>();
        childIds.add(contractor.getId());
        while (!childIds.isEmpty())
        {
            result.addAll(childIds);
            childIds = getContractorIdsByParent(childIds);
        }
        return result;
    }

    private List<Long> getContractorIdsByParent(List<Long> IdList)
    {
        MQBuilder builder = new MQBuilder(Contractor.ENTITY_CLASS, "e", new String[] { "id" });
        builder.add(MQExpression.in("e", Contractor.L_HEAD_ORGANIZATION + ".id", IdList));
        return builder.getResultList(getSession());
    }

    @Override
    public void update(Model model)
    {
        Contractor contractor = model.getContractor();
        AddressDetailed legalAddress = model.getLegalAddress();
        AddressBaseManager.instance().dao().saveOrUpdateAddressWithoutEntityUpdate(contractor, legalAddress, Contractor.L_ADDRESS);

        if (null != contractor.getAddress()) {
            getSession().saveOrUpdate(contractor.getAddress());
        }

        if (null != contractor.getLegalAddress()) {
            getSession().saveOrUpdate(contractor.getLegalAddress());
        }

        UniBaseUtils.checkUnique(getSession(), Contractor.ENTITY_CLASS, Contractor.P_TITLE, contractor);
        getSession().saveOrUpdate(contractor);
    }
}
