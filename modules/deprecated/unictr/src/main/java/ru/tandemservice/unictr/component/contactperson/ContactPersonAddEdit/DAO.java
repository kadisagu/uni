/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unictr.component.contactperson.ContactPersonAddEdit;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unictr.entity.catalog.ContractorPost;
import ru.tandemservice.unictr.entity.contractor.ContactPerson;
import ru.tandemservice.unictr.entity.contractor.Contractor;

/**
 * @author OKhmelevskaya
 * @since 30.01.2008
 */
@SuppressWarnings({"deprecation"})
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        if (model.getContactPerson().getId() != null) {
            model.setContactPerson(get(ContactPerson.class, model.getContactPerson().getId()));
            model.setContractorId(model.getContactPerson().getContractor().getId());
        } else {
        	model.getContactPerson().setContractor(get(Contractor.class, model.getContractorId()));
        }
        model.setPostList(getCatalogItemList(ContractorPost.class));
        model.setLeader(model.getContactPerson().isLeader());
        
        if (null != model.getCreatedCatalogItemId()) {
        	final ICatalogItem item = get(model.getCreatedCatalogItemId());
        	if (item instanceof ContractorPost) {
        		model.getContactPerson().setPost((ContractorPost) item);
        	}
        }
        
    }

    @Override
    public void update(Model model)
    {
    	ContactPerson contactPerson = model.getContactPerson();
		if (null == contactPerson.getId()) {
    		contactPerson.setContractor(get(Contractor.class, model.getContractorId()));
    	}
    	
    	getSession().saveOrUpdate(contactPerson);
    	
    	/* invalidate leader */ {
    		Contractor contractor = contactPerson.getContractor();
    		ContactPerson oldLeader = contractor.getLeader();
    		ContactPerson leader = (model.isLeader() ? contactPerson : contactPerson.equals(oldLeader) ? null : oldLeader);
    		contractor.setLeader(leader);
    		getSession().saveOrUpdate(contractor);
    	}
    }
}
