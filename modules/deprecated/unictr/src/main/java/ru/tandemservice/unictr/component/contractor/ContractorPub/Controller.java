/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unictr.component.contractor.ContractorPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInline;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInlineConfig;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInlineUI;
import ru.tandemservice.unictr.IUnictrComponents;
import ru.tandemservice.unictr.entity.contractor.ContactPerson;

/**
 * @author OKhmelevskaya
 * @since 26.01.2008
 */
@SuppressWarnings("deprecation")
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent context)
    {
        getDao().prepare(getModel(context));
        prepareContactPersonDataSource(context);
    }

    public void prepareContactPersonDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getContactPersonDataSource() != null) return;

        ContactPersonDataSourceDelegate delegate = new ContactPersonDataSourceDelegate(this);
        DynamicListDataSource<ContactPerson> dataSource = new DynamicListDataSource<>(component, delegate, 10);
        dataSource.addColumn(new SimpleColumn("ФИО", ContactPerson.P_FULLFIO));
        dataSource.addColumn(new SimpleColumn("Должность", ContactPerson.post().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Телефон", ContactPerson.P_ALL_PHONES).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Email", ContactPerson.P_EMAIL, org.tandemframework.core.view.formatter.EmailFormatter.INSTANCE).setOrderable(false).setClickable(false));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteContactPerson", "Удалить контактное лицо?"));
        model.setContactPersonDataSource(dataSource);
    }

    public void onClickEditContractor(IBusinessComponent context)
    {
        context.createChildRegion("contractorPub", new ComponentActivator(IUnictrComponents.CONTRACTOR_ADD_EDIT));
    }

    public void onClickEditContractorLeader(IBusinessComponent context)
    {
        context.createChildRegion("contractorPub", new ComponentActivator(IUnictrComponents.CONTRACTOR_EDIT_LEADER));
    }

    public void onClickEditContractorBankProps(IBusinessComponent context)
    {
        context.createChildRegion("contractorPub", new ComponentActivator(IUnictrComponents.CONTRACTOR_EDIT_BANK_PROPS));
    }

    public void onClickEditContractorContactInfo(IBusinessComponent context)
    {
        context.createChildRegion("contractorPub", new ComponentActivator(IUnictrComponents.CONTRACTOR_EDIT_CONTACT_INFO));
    }

    public void onClickEditContractorAdditionalInfo(IBusinessComponent context)
    {
        context.createChildRegion("contractorPub", new ComponentActivator(IUnictrComponents.CONTRACTOR_EDIT_ADDITIONAL_INFO));
    }

    public void onClickAddressEdit(IBusinessComponent context)
    {
        Model model = getModel(context);

        AddressBaseEditInlineConfig addressConfig = new AddressBaseEditInlineConfig();
        addressConfig.setEntityId(model.getContractor().getId());
        addressConfig.setDetailedOnly(true);
        addressConfig.setDetailLevel(4);
        addressConfig.setAreaVisible(true);
        addressConfig.setOffice(true);
        addressConfig.setInline(false);
        addressConfig.setSettlementRequired(true);
        addressConfig.setAddressProperty((String) context.getListenerParameter());

        context.createChildRegion("contractorPub", new ComponentActivator(AddressBaseEditInline.class.getSimpleName(), new ParametersMap().add(AddressBaseEditInlineUI.BIND_CONFIG, addressConfig)));
    }

    public void onClickDelete(IBusinessComponent context)
    {
        getDao().deleteRow(context);
        onRefreshComponent(context);
    }

    public void onClickAddContactPerson(IBusinessComponent context)
    {
        getModel(context).setContactPersonId(null);
        context.createChildRegion("contractorPub", new ComponentActivator(IUnictrComponents.CONTACT_PERSON_ADD_EDIT));
    }

    public void onClickDeleteContactPerson(IBusinessComponent context)
    {
        getDao().deleteRow(context);
        getModel(context).getContactPersonDataSource().refresh();
    }
}
