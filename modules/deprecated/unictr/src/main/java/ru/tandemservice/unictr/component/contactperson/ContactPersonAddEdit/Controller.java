/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unictr.component.contactperson.ContactPersonAddEdit;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import ru.tandemservice.unictr.entity.catalog.ContractorPost;

import java.util.Collections;

/**
 * @author OKhmelevskaya
 *         Created on: 30.01.2008
 */
@SuppressWarnings("deprecation")
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent context)
    {
        getDao().prepare(getModel(context));
    }

    public void onClickApply(IBusinessComponent context)
    {
        getDao().update(getModel(context));
        deactivate(context);
    }

    public void onClickAddPost(IBusinessComponent context)
    {
        context.createDefaultChildRegion(new ComponentActivator(
                DefaultCatalogAddEditModel.class.getPackage().getName(),
                Collections.singletonMap("catalogCode", (Object) ContractorPost.ENTITY_NAME)
        ));
    }
}
