package ru.tandemservice.unictr.entity.contractor.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unictr.entity.catalog.ContractorPost;
import ru.tandemservice.unictr.entity.contractor.ContactPerson;
import ru.tandemservice.unictr.entity.contractor.Contractor;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Контактное лицо
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ContactPersonGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unictr.entity.contractor.ContactPerson";
    public static final String ENTITY_NAME = "contactPerson";
    public static final int VERSION_HASH = -721381758;
    private static IEntityMeta ENTITY_META;

    public static final String P_FIRST_NAME = "firstName";
    public static final String P_LAST_NAME = "lastName";
    public static final String P_MIDDLE_NAME = "middleName";
    public static final String P_BIRTH_DATE = "birthDate";
    public static final String L_CONTRACTOR = "contractor";
    public static final String L_POST = "post";
    public static final String P_SUBDIVISION = "subdivision";
    public static final String P_WORK_PHONE = "workPhone";
    public static final String P_MOBILE_PHONE = "mobilePhone";
    public static final String P_PHONE = "phone";
    public static final String P_EMAIL = "email";
    public static final String P_ICQ = "icq";

    private String _firstName;     // Имя
    private String _lastName;     // Фамилия
    private String _middleName;     // Отчество
    private Date _birthDate;     // Дата рождения
    private Contractor _contractor;     // Контрагент
    private ContractorPost _post;     // Должность контрагента
    private String _subdivision;     // Подразделение/отдел
    private String _workPhone;     // Рабочий телефон
    private String _mobilePhone;     // Мобильный телефон
    private String _phone;     // Телефон
    private String _email;     // E-mail
    private String _icq;     // ICQ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Имя. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFirstName()
    {
        return _firstName;
    }

    /**
     * @param firstName Имя. Свойство не может быть null.
     */
    public void setFirstName(String firstName)
    {
        dirty(_firstName, firstName);
        _firstName = firstName;
    }

    /**
     * @return Фамилия. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getLastName()
    {
        return _lastName;
    }

    /**
     * @param lastName Фамилия. Свойство не может быть null.
     */
    public void setLastName(String lastName)
    {
        dirty(_lastName, lastName);
        _lastName = lastName;
    }

    /**
     * @return Отчество.
     */
    @Length(max=255)
    public String getMiddleName()
    {
        return _middleName;
    }

    /**
     * @param middleName Отчество.
     */
    public void setMiddleName(String middleName)
    {
        dirty(_middleName, middleName);
        _middleName = middleName;
    }

    /**
     * @return Дата рождения.
     */
    public Date getBirthDate()
    {
        return _birthDate;
    }

    /**
     * @param birthDate Дата рождения.
     */
    public void setBirthDate(Date birthDate)
    {
        dirty(_birthDate, birthDate);
        _birthDate = birthDate;
    }

    /**
     * @return Контрагент. Свойство не может быть null.
     */
    @NotNull
    public Contractor getContractor()
    {
        return _contractor;
    }

    /**
     * @param contractor Контрагент. Свойство не может быть null.
     */
    public void setContractor(Contractor contractor)
    {
        dirty(_contractor, contractor);
        _contractor = contractor;
    }

    /**
     * @return Должность контрагента.
     */
    public ContractorPost getPost()
    {
        return _post;
    }

    /**
     * @param post Должность контрагента.
     */
    public void setPost(ContractorPost post)
    {
        dirty(_post, post);
        _post = post;
    }

    /**
     * @return Подразделение/отдел.
     */
    @Length(max=255)
    public String getSubdivision()
    {
        return _subdivision;
    }

    /**
     * @param subdivision Подразделение/отдел.
     */
    public void setSubdivision(String subdivision)
    {
        dirty(_subdivision, subdivision);
        _subdivision = subdivision;
    }

    /**
     * @return Рабочий телефон.
     */
    @Length(max=255)
    public String getWorkPhone()
    {
        return _workPhone;
    }

    /**
     * @param workPhone Рабочий телефон.
     */
    public void setWorkPhone(String workPhone)
    {
        dirty(_workPhone, workPhone);
        _workPhone = workPhone;
    }

    /**
     * @return Мобильный телефон.
     */
    @Length(max=255)
    public String getMobilePhone()
    {
        return _mobilePhone;
    }

    /**
     * @param mobilePhone Мобильный телефон.
     */
    public void setMobilePhone(String mobilePhone)
    {
        dirty(_mobilePhone, mobilePhone);
        _mobilePhone = mobilePhone;
    }

    /**
     * @return Телефон.
     */
    @Length(max=255)
    public String getPhone()
    {
        return _phone;
    }

    /**
     * @param phone Телефон.
     */
    public void setPhone(String phone)
    {
        dirty(_phone, phone);
        _phone = phone;
    }

    /**
     * @return E-mail.
     */
    @Length(max=255)
    public String getEmail()
    {
        return _email;
    }

    /**
     * @param email E-mail.
     */
    public void setEmail(String email)
    {
        dirty(_email, email);
        _email = email;
    }

    /**
     * @return ICQ.
     */
    @Length(max=255)
    public String getIcq()
    {
        return _icq;
    }

    /**
     * @param icq ICQ.
     */
    public void setIcq(String icq)
    {
        dirty(_icq, icq);
        _icq = icq;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ContactPersonGen)
        {
            setFirstName(((ContactPerson)another).getFirstName());
            setLastName(((ContactPerson)another).getLastName());
            setMiddleName(((ContactPerson)another).getMiddleName());
            setBirthDate(((ContactPerson)another).getBirthDate());
            setContractor(((ContactPerson)another).getContractor());
            setPost(((ContactPerson)another).getPost());
            setSubdivision(((ContactPerson)another).getSubdivision());
            setWorkPhone(((ContactPerson)another).getWorkPhone());
            setMobilePhone(((ContactPerson)another).getMobilePhone());
            setPhone(((ContactPerson)another).getPhone());
            setEmail(((ContactPerson)another).getEmail());
            setIcq(((ContactPerson)another).getIcq());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ContactPersonGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ContactPerson.class;
        }

        public T newInstance()
        {
            return (T) new ContactPerson();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "firstName":
                    return obj.getFirstName();
                case "lastName":
                    return obj.getLastName();
                case "middleName":
                    return obj.getMiddleName();
                case "birthDate":
                    return obj.getBirthDate();
                case "contractor":
                    return obj.getContractor();
                case "post":
                    return obj.getPost();
                case "subdivision":
                    return obj.getSubdivision();
                case "workPhone":
                    return obj.getWorkPhone();
                case "mobilePhone":
                    return obj.getMobilePhone();
                case "phone":
                    return obj.getPhone();
                case "email":
                    return obj.getEmail();
                case "icq":
                    return obj.getIcq();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "firstName":
                    obj.setFirstName((String) value);
                    return;
                case "lastName":
                    obj.setLastName((String) value);
                    return;
                case "middleName":
                    obj.setMiddleName((String) value);
                    return;
                case "birthDate":
                    obj.setBirthDate((Date) value);
                    return;
                case "contractor":
                    obj.setContractor((Contractor) value);
                    return;
                case "post":
                    obj.setPost((ContractorPost) value);
                    return;
                case "subdivision":
                    obj.setSubdivision((String) value);
                    return;
                case "workPhone":
                    obj.setWorkPhone((String) value);
                    return;
                case "mobilePhone":
                    obj.setMobilePhone((String) value);
                    return;
                case "phone":
                    obj.setPhone((String) value);
                    return;
                case "email":
                    obj.setEmail((String) value);
                    return;
                case "icq":
                    obj.setIcq((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "firstName":
                        return true;
                case "lastName":
                        return true;
                case "middleName":
                        return true;
                case "birthDate":
                        return true;
                case "contractor":
                        return true;
                case "post":
                        return true;
                case "subdivision":
                        return true;
                case "workPhone":
                        return true;
                case "mobilePhone":
                        return true;
                case "phone":
                        return true;
                case "email":
                        return true;
                case "icq":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "firstName":
                    return true;
                case "lastName":
                    return true;
                case "middleName":
                    return true;
                case "birthDate":
                    return true;
                case "contractor":
                    return true;
                case "post":
                    return true;
                case "subdivision":
                    return true;
                case "workPhone":
                    return true;
                case "mobilePhone":
                    return true;
                case "phone":
                    return true;
                case "email":
                    return true;
                case "icq":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "firstName":
                    return String.class;
                case "lastName":
                    return String.class;
                case "middleName":
                    return String.class;
                case "birthDate":
                    return Date.class;
                case "contractor":
                    return Contractor.class;
                case "post":
                    return ContractorPost.class;
                case "subdivision":
                    return String.class;
                case "workPhone":
                    return String.class;
                case "mobilePhone":
                    return String.class;
                case "phone":
                    return String.class;
                case "email":
                    return String.class;
                case "icq":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ContactPerson> _dslPath = new Path<ContactPerson>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ContactPerson");
    }
            

    /**
     * @return Имя. Свойство не может быть null.
     * @see ru.tandemservice.unictr.entity.contractor.ContactPerson#getFirstName()
     */
    public static PropertyPath<String> firstName()
    {
        return _dslPath.firstName();
    }

    /**
     * @return Фамилия. Свойство не может быть null.
     * @see ru.tandemservice.unictr.entity.contractor.ContactPerson#getLastName()
     */
    public static PropertyPath<String> lastName()
    {
        return _dslPath.lastName();
    }

    /**
     * @return Отчество.
     * @see ru.tandemservice.unictr.entity.contractor.ContactPerson#getMiddleName()
     */
    public static PropertyPath<String> middleName()
    {
        return _dslPath.middleName();
    }

    /**
     * @return Дата рождения.
     * @see ru.tandemservice.unictr.entity.contractor.ContactPerson#getBirthDate()
     */
    public static PropertyPath<Date> birthDate()
    {
        return _dslPath.birthDate();
    }

    /**
     * @return Контрагент. Свойство не может быть null.
     * @see ru.tandemservice.unictr.entity.contractor.ContactPerson#getContractor()
     */
    public static Contractor.Path<Contractor> contractor()
    {
        return _dslPath.contractor();
    }

    /**
     * @return Должность контрагента.
     * @see ru.tandemservice.unictr.entity.contractor.ContactPerson#getPost()
     */
    public static ContractorPost.Path<ContractorPost> post()
    {
        return _dslPath.post();
    }

    /**
     * @return Подразделение/отдел.
     * @see ru.tandemservice.unictr.entity.contractor.ContactPerson#getSubdivision()
     */
    public static PropertyPath<String> subdivision()
    {
        return _dslPath.subdivision();
    }

    /**
     * @return Рабочий телефон.
     * @see ru.tandemservice.unictr.entity.contractor.ContactPerson#getWorkPhone()
     */
    public static PropertyPath<String> workPhone()
    {
        return _dslPath.workPhone();
    }

    /**
     * @return Мобильный телефон.
     * @see ru.tandemservice.unictr.entity.contractor.ContactPerson#getMobilePhone()
     */
    public static PropertyPath<String> mobilePhone()
    {
        return _dslPath.mobilePhone();
    }

    /**
     * @return Телефон.
     * @see ru.tandemservice.unictr.entity.contractor.ContactPerson#getPhone()
     */
    public static PropertyPath<String> phone()
    {
        return _dslPath.phone();
    }

    /**
     * @return E-mail.
     * @see ru.tandemservice.unictr.entity.contractor.ContactPerson#getEmail()
     */
    public static PropertyPath<String> email()
    {
        return _dslPath.email();
    }

    /**
     * @return ICQ.
     * @see ru.tandemservice.unictr.entity.contractor.ContactPerson#getIcq()
     */
    public static PropertyPath<String> icq()
    {
        return _dslPath.icq();
    }

    public static class Path<E extends ContactPerson> extends EntityPath<E>
    {
        private PropertyPath<String> _firstName;
        private PropertyPath<String> _lastName;
        private PropertyPath<String> _middleName;
        private PropertyPath<Date> _birthDate;
        private Contractor.Path<Contractor> _contractor;
        private ContractorPost.Path<ContractorPost> _post;
        private PropertyPath<String> _subdivision;
        private PropertyPath<String> _workPhone;
        private PropertyPath<String> _mobilePhone;
        private PropertyPath<String> _phone;
        private PropertyPath<String> _email;
        private PropertyPath<String> _icq;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Имя. Свойство не может быть null.
     * @see ru.tandemservice.unictr.entity.contractor.ContactPerson#getFirstName()
     */
        public PropertyPath<String> firstName()
        {
            if(_firstName == null )
                _firstName = new PropertyPath<String>(ContactPersonGen.P_FIRST_NAME, this);
            return _firstName;
        }

    /**
     * @return Фамилия. Свойство не может быть null.
     * @see ru.tandemservice.unictr.entity.contractor.ContactPerson#getLastName()
     */
        public PropertyPath<String> lastName()
        {
            if(_lastName == null )
                _lastName = new PropertyPath<String>(ContactPersonGen.P_LAST_NAME, this);
            return _lastName;
        }

    /**
     * @return Отчество.
     * @see ru.tandemservice.unictr.entity.contractor.ContactPerson#getMiddleName()
     */
        public PropertyPath<String> middleName()
        {
            if(_middleName == null )
                _middleName = new PropertyPath<String>(ContactPersonGen.P_MIDDLE_NAME, this);
            return _middleName;
        }

    /**
     * @return Дата рождения.
     * @see ru.tandemservice.unictr.entity.contractor.ContactPerson#getBirthDate()
     */
        public PropertyPath<Date> birthDate()
        {
            if(_birthDate == null )
                _birthDate = new PropertyPath<Date>(ContactPersonGen.P_BIRTH_DATE, this);
            return _birthDate;
        }

    /**
     * @return Контрагент. Свойство не может быть null.
     * @see ru.tandemservice.unictr.entity.contractor.ContactPerson#getContractor()
     */
        public Contractor.Path<Contractor> contractor()
        {
            if(_contractor == null )
                _contractor = new Contractor.Path<Contractor>(L_CONTRACTOR, this);
            return _contractor;
        }

    /**
     * @return Должность контрагента.
     * @see ru.tandemservice.unictr.entity.contractor.ContactPerson#getPost()
     */
        public ContractorPost.Path<ContractorPost> post()
        {
            if(_post == null )
                _post = new ContractorPost.Path<ContractorPost>(L_POST, this);
            return _post;
        }

    /**
     * @return Подразделение/отдел.
     * @see ru.tandemservice.unictr.entity.contractor.ContactPerson#getSubdivision()
     */
        public PropertyPath<String> subdivision()
        {
            if(_subdivision == null )
                _subdivision = new PropertyPath<String>(ContactPersonGen.P_SUBDIVISION, this);
            return _subdivision;
        }

    /**
     * @return Рабочий телефон.
     * @see ru.tandemservice.unictr.entity.contractor.ContactPerson#getWorkPhone()
     */
        public PropertyPath<String> workPhone()
        {
            if(_workPhone == null )
                _workPhone = new PropertyPath<String>(ContactPersonGen.P_WORK_PHONE, this);
            return _workPhone;
        }

    /**
     * @return Мобильный телефон.
     * @see ru.tandemservice.unictr.entity.contractor.ContactPerson#getMobilePhone()
     */
        public PropertyPath<String> mobilePhone()
        {
            if(_mobilePhone == null )
                _mobilePhone = new PropertyPath<String>(ContactPersonGen.P_MOBILE_PHONE, this);
            return _mobilePhone;
        }

    /**
     * @return Телефон.
     * @see ru.tandemservice.unictr.entity.contractor.ContactPerson#getPhone()
     */
        public PropertyPath<String> phone()
        {
            if(_phone == null )
                _phone = new PropertyPath<String>(ContactPersonGen.P_PHONE, this);
            return _phone;
        }

    /**
     * @return E-mail.
     * @see ru.tandemservice.unictr.entity.contractor.ContactPerson#getEmail()
     */
        public PropertyPath<String> email()
        {
            if(_email == null )
                _email = new PropertyPath<String>(ContactPersonGen.P_EMAIL, this);
            return _email;
        }

    /**
     * @return ICQ.
     * @see ru.tandemservice.unictr.entity.contractor.ContactPerson#getIcq()
     */
        public PropertyPath<String> icq()
        {
            if(_icq == null )
                _icq = new PropertyPath<String>(ContactPersonGen.P_ICQ, this);
            return _icq;
        }

        public Class getEntityClass()
        {
            return ContactPerson.class;
        }

        public String getEntityName()
        {
            return "contactPerson";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
