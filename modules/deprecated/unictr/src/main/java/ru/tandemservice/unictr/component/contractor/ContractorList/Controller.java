/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unictr.component.contractor.ContractorList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import ru.tandemservice.unictr.IUnictrComponents;
import ru.tandemservice.unictr.entity.contractor.Contractor;

/**
 * @author OKhmelevskaya
 * Created on: 25.01.2008
 */
@SuppressWarnings("deprecation")
public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));

        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<Contractor> dataSource = new DynamicListDataSource<>(component, this);
        dataSource.addColumn(new SimpleColumn("Название", Contractor.P_SHORT_TITLE));
        dataSource.addColumn(new SimpleColumn("ОПФ", Contractor.legalForm().shortTitle().s()).setClickable(false));
        dataSource.addColumn(new PublisherLinkColumn("Головная организация", Contractor.P_SHORT_TITLE, Contractor.L_HEAD_ORGANIZATION));
        dataSource.addColumn(new SimpleColumn("Телефон", Contractor.phone().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить контрагента?"));
        model.setDataSource(dataSource);
    }

    protected void refresh(IBusinessComponent context)
    {
        getModel(context).getDataSource().refresh();
    }

    @Override
    public void updateListDataSource(IBusinessComponent context)
    {
        getDao().prepareListDataSource(getModel(context));
    }

    public void onClickSearch(IBusinessComponent context)
    {
        getModel(context).getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent context)
    {
        getModel(context).setContractor(new Contractor());
        getModel(context).setHeadOrgTitle(null);
        onClickSearch(context);
    }

    public void onClickAdd(IBusinessComponent context)
    {
        context.createDefaultChildRegion(new ComponentActivator(IUnictrComponents.CONTRACTOR_ADD_EDIT));
    }

    public void onClickDelete(IBusinessComponent context)
    {
        getDao().deleteContractor((Long)context.getListenerParameter());
        refresh(context);
    }
}
