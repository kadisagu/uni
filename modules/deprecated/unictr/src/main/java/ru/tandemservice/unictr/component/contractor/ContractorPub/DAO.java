/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unictr.component.contractor.ContractorPub;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unictr.entity.contractor.ContactPerson;
import ru.tandemservice.unictr.entity.contractor.Contractor;

/**
 * @author OKhmelevskaya
 * @since 26.01.2008
 */
@SuppressWarnings("deprecation")
public class DAO extends UniDao<Model> implements IDAO
{
    protected static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("cp");

    static
    {
        _orderSettings.setOrders(ContactPerson.P_FULLFIO, new OrderDescription("cp", ContactPerson.P_LAST_NAME), new OrderDescription("cp", ContactPerson.P_FIRST_NAME), new OrderDescription("cp", ContactPerson.P_MIDDLE_NAME));
    }

    @Override
    public void prepare(Model model)
    {
        model.setContractor(get(Contractor.class, model.getContractor().getId()));
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        DynamicListDataSource<ContactPerson> dataSource = model.getContactPersonDataSource();
        MQBuilder builder = new MQBuilder(ContactPerson.ENTITY_CLASS, "cp");
        builder.add(MQExpression.eq("cp", "contractor", model.getContractor()));
        _orderSettings.applyOrder(builder, model.getContactPersonDataSource().getEntityOrder());
        UniBaseUtils.createPage(dataSource, builder, getSession());
    }
}
