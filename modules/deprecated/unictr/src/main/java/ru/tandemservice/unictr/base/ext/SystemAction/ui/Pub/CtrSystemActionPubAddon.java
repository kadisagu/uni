/**
 *$Id$
 */
package ru.tandemservice.unictr.base.ext.SystemAction.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.shared.ctr.base.bo.Contactor.logic.ICtrSyncDao;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;

/**
 * @author Alexander Shaburov
 * @since 13.02.13
 */
public class CtrSystemActionPubAddon extends UIAddon
{
    public CtrSystemActionPubAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public void onClickUpdateContactorPersonSearchIndex()
    {
        for (IEntityMeta meta : EntityRuntime.getMeta(ContactorPerson.class).getChildren())
        {
            @SuppressWarnings("unchecked")
            Class<? extends ContactorPerson> contactorClass = meta.getEntityClass();
            ICtrSyncDao.instance.get().doUpdateContactorSearchIndex(contactorClass);
        }
    }
}
