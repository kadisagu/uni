/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unictr.component.contractor.ContractorEditLeader;

import org.tandemframework.core.component.Input;
import ru.tandemservice.unictr.entity.catalog.ContractorPost;
import ru.tandemservice.unictr.entity.contractor.ContactPerson;

import java.util.List;

/**
 * @author OKhmelevskaya
 * @since 31.01.2008
 */
@SuppressWarnings("deprecation")
@Input(keys = {"contractorId", "contactPersonId"}, bindings = {"contractorId", "leader.id"})
public class Model
{
    private Long _contractorId;
    private ContactPerson _leader = new ContactPerson();
    private ContractorPost _post = new ContractorPost();
    private List<ContactPerson> _contactPersonList;
    private List<ContractorPost> _postList;

    public void setContractorId(Long contractorId)
    {
        _contractorId = contractorId;
    }

    public Long getContractorId()
    {
        return _contractorId;
    }

    public void setContactPersonList(List<ContactPerson> contactPersonList)
    {
        _contactPersonList = contactPersonList;
    }

    public List<ContactPerson> getContactPersonList()
    {
        return _contactPersonList;
    }

    public void setLeader(ContactPerson leader)
    {
        _leader = leader;
    }

    public ContactPerson getLeader()
    {
        return _leader;
    }

    public void setPostList(List<ContractorPost> postList)
    {
        _postList = postList;
    }

    public List<ContractorPost> getPostList()
    {
        return _postList;
    }

    public void setPost(ContractorPost post)
    {
        _post = post;
    }

    public ContractorPost getPost()
    {
        return _post;
    }
}
