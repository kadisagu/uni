/* $Id:DAO.java 2645 2008-04-29 07:54:04Z okhmelevskaya $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unictr.component.contactperson.ContactPersonPub;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unictr.entity.contractor.ContactPerson;

/**
 * @author OKhmelevskaya
 * Created on: 30.01.2008
 */
@SuppressWarnings("deprecation")
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        ContactPerson contactPerson = get(ContactPerson.class, model.getContactPerson().getId()); 
        model.setContactPerson(contactPerson);
        model.setContractorId(contactPerson.getContractor().getId());
    }
}
