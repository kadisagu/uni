/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unictr;

/**
 * @author OKhmelevskaya
 * @since 04.05.2008	
 */
public interface IUnictrComponents
{
  //список в алфавитном порядке
    String CONTACT_PERSON_ADD_EDIT = "ru.tandemservice.unictr.component.contactperson.ContactPersonAddEdit";
    String CONTACT_PERSON_EDIT_CONTACT_INFO = "ru.tandemservice.unictr.component.contactperson.ContactPersonEditContactInfo";
    String CONTACT_PERSON_EDIT_POST = "ru.tandemservice.unictr.component.contactperson.ContactPersonEditPost";
    
    String CONTRACTOR_ADD_EDIT = "ru.tandemservice.unictr.component.contractor.ContractorAddEdit";
    String CONTRACTOR_EDIT_ADDITIONAL_INFO = "ru.tandemservice.unictr.component.contractor.ContractorEditAdditionalInfo";
    String CONTRACTOR_EDIT_BANK_PROPS = "ru.tandemservice.unictr.component.contractor.ContractorEditBankProps";
    String CONTRACTOR_EDIT_CONTACT_INFO = "ru.tandemservice.unictr.component.contractor.ContractorEditContactInfo";
    String CONTRACTOR_EDIT_LEADER = "ru.tandemservice.unictr.component.contractor.ContractorEditLeader";
}
