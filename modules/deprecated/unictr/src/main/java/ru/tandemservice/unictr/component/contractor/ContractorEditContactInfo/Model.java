/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unictr.component.contractor.ContractorEditContactInfo;

import org.tandemframework.core.component.Input;
import ru.tandemservice.unictr.entity.contractor.Contractor;

/**
 * @author OKhmelevskaya
 * Created on: 29.01.2008
 */
@SuppressWarnings("deprecation")
@Input(keys = "contractorId", bindings = "contractor.id")
public class Model
{
    private Contractor _contractor = new Contractor();

    public void setContractor(Contractor contractor)
    {
        _contractor = contractor;
    }

    public Contractor getContractor()
    {
        return _contractor;
    }
}
