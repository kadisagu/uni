package ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage;

import org.hibernate.Session;

import ru.tandemservice.uni.dao.UniBaseDao;

/**
 * @author vdanilov
 */
@SuppressWarnings("deprecation")
public abstract class AbstractStageDAO<M extends AbstractStageModel> extends UniBaseDao implements IAbstractStageDAO<M> {
    @Override public boolean skip(final M model) { return false; }

    protected IStageResult getResult(final M model) {
        return IStageResult.NONE;
    }

    @Override
    public IStageResult save(final M model) {
        return getResult(model);
    };

    @Override
    public void prepare(final M model) {
        Session session = getSession();
        if (null != model.getAgreement().getId()) { session.update(model.getAgreement()); }
        reassociateEntityProperties(session, model.getAgreement());
    };
}
