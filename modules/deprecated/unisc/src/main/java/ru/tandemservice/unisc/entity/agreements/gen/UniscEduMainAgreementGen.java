package ru.tandemservice.unisc.entity.agreements.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;
import ru.tandemservice.unisc.entity.agreements.UniscEduMainAgreement;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Договор
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniscEduMainAgreementGen extends UniscEduAgreementBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisc.entity.agreements.UniscEduMainAgreement";
    public static final String ENTITY_NAME = "uniscEduMainAgreement";
    public static final int VERSION_HASH = 1817641472;
    private static IEntityMeta ENTITY_META;

    public static final String P_UUID = "uuid";

    private String _uuid;     // Идентификатор (для связи с внешней системой)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Идентификатор (для связи с внешней системой).
     */
    @Length(max=255)
    public String getUuid()
    {
        return _uuid;
    }

    /**
     * @param uuid Идентификатор (для связи с внешней системой).
     */
    public void setUuid(String uuid)
    {
        dirty(_uuid, uuid);
        _uuid = uuid;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UniscEduMainAgreementGen)
        {
            setUuid(((UniscEduMainAgreement)another).getUuid());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniscEduMainAgreementGen> extends UniscEduAgreementBase.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniscEduMainAgreement.class;
        }

        public T newInstance()
        {
            return (T) new UniscEduMainAgreement();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "uuid":
                    return obj.getUuid();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "uuid":
                    obj.setUuid((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "uuid":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "uuid":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "uuid":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniscEduMainAgreement> _dslPath = new Path<UniscEduMainAgreement>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniscEduMainAgreement");
    }
            

    /**
     * @return Идентификатор (для связи с внешней системой).
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduMainAgreement#getUuid()
     */
    public static PropertyPath<String> uuid()
    {
        return _dslPath.uuid();
    }

    public static class Path<E extends UniscEduMainAgreement> extends UniscEduAgreementBase.Path<E>
    {
        private PropertyPath<String> _uuid;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Идентификатор (для связи с внешней системой).
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduMainAgreement#getUuid()
     */
        public PropertyPath<String> uuid()
        {
            if(_uuid == null )
                _uuid = new PropertyPath<String>(UniscEduMainAgreementGen.P_UUID, this);
            return _uuid;
        }

        public Class getEntityClass()
        {
            return UniscEduMainAgreement.class;
        }

        public String getEntityName()
        {
            return "uniscEduMainAgreement";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
