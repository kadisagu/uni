package ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage;

import ru.tandemservice.uni.dao.IPrepareable;

/**
 * @author vdanilov
 */
@SuppressWarnings("deprecation")
public interface IAbstractStageDAO<M extends AbstractStageModel> extends IPrepareable<M> {
    boolean skip(M model);
    IStageResult save(M model);
}
