// Copyright 2006-2012 Tandem Service Software
// Proprietary Licence for Tandem University

package ru.tandemservice.unisc.ws.cost;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Информация о стоимости обучения по направлениям подготовки подразделения
 *
 * @author Vasily Zhukov
 * @since 17.02.2011
 */
@XmlRootElement
public class EducationOrgUnitCostEnvironment
{
    @XmlAttribute(required = true)
    public String educationYearTitle;

    @XmlAttribute(required = true)
    public String payPeriodCode;

    @XmlAttribute(required = true)
    public int stagePeriod;

    public List<EducationOrgUnitCostNode> row = new ArrayList<EducationOrgUnitCostNode>();

    public static class EducationOrgUnitCostNode
    {
        public EducationOrgUnitCostNode()
        {
        }

        public EducationOrgUnitCostNode(long cost, String cipher, String educationOrgUnit)
        {
            this.cost = cost;
            this.cipher = cipher;
            this.educationOrgUnit = educationOrgUnit;
        }

        /**
         * Стоимость обучения по указанному направлению подготовки подразделения
         * в указанном учебном году, за указанный период опланы, в указанный номер периода
         */
        @XmlAttribute(required = true)
        public long cost;

        /**
         * Шифр
         */
        @XmlAttribute
        public String cipher;

        /**
         * Идентификатор уровня образования подразделения.
         */
        @XmlAttribute(required = true)
        public String educationOrgUnit;
    }
}
