package ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage2;

import ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage.AbstractStageModel;
import ru.tandemservice.unisc.component.agreement.AgreementPayPlanEdit.AgreementPayPlanModel;

/**
 * @author vdanilov
 */
@SuppressWarnings("deprecation")
public class Model extends AbstractStageModel {

    private final AgreementPayPlanModel payPlanModel = new AgreementPayPlanModel();
    public AgreementPayPlanModel getPayPlanModel() { return this.payPlanModel; }

    public String getFirstStageTitle() { return this.getAgreement().getFirstStageTitle(); }
    public String getLastStageTitle() { return this.getAgreement().getLastStageTitle(); }


}
