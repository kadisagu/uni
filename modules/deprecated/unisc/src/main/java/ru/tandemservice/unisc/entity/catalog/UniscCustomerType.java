package ru.tandemservice.unisc.entity.catalog;

import ru.tandemservice.unisc.UniscDefines;
import ru.tandemservice.unisc.entity.catalog.gen.UniscCustomerTypeGen;

/**
 * Тип заказчика
 */
public class UniscCustomerType extends UniscCustomerTypeGen
{
    public boolean hasJuridicalPart()
    {
        return (UniscDefines.CUSTOMER_TYPE_JURIDICAL_PERSON.equals(getCode()) ||
            	UniscDefines.CUSTOMER_TYPE_COMPLEX_PERSON.equals(getCode()));
    }

    public boolean hasNaturalPart()
    {
        return (UniscDefines.CUSTOMER_TYPE_NATURAL_PERSON.equals(getCode()) ||
            	UniscDefines.CUSTOMER_TYPE_COMPLEX_PERSON.equals(getCode()));
    }
}
