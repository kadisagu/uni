package ru.tandemservice.unisc.entity.agreements.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementFinesState;
import ru.tandemservice.unisc.entity.agreements.UniscEduMainAgreement;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Начисление пеней по договору
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniscEduAgreementFinesStateGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisc.entity.agreements.UniscEduAgreementFinesState";
    public static final String ENTITY_NAME = "uniscEduAgreementFinesState";
    public static final int VERSION_HASH = -67964559;
    private static IEntityMeta ENTITY_META;

    public static final String L_AGREEMENT = "agreement";
    public static final String P_DATE = "date";
    public static final String P_FINES_ACTIVE = "finesActive";

    private UniscEduMainAgreement _agreement;     // договор
    private Date _date;     // Дата
    private boolean _finesActive;     // Начислять пени

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return договор. Свойство не может быть null.
     */
    @NotNull
    public UniscEduMainAgreement getAgreement()
    {
        return _agreement;
    }

    /**
     * @param agreement договор. Свойство не может быть null.
     */
    public void setAgreement(UniscEduMainAgreement agreement)
    {
        dirty(_agreement, agreement);
        _agreement = agreement;
    }

    /**
     * @return Дата. Свойство не может быть null.
     */
    @NotNull
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date Дата. Свойство не может быть null.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return Начислять пени. Свойство не может быть null.
     */
    @NotNull
    public boolean isFinesActive()
    {
        return _finesActive;
    }

    /**
     * @param finesActive Начислять пени. Свойство не может быть null.
     */
    public void setFinesActive(boolean finesActive)
    {
        dirty(_finesActive, finesActive);
        _finesActive = finesActive;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UniscEduAgreementFinesStateGen)
        {
            setAgreement(((UniscEduAgreementFinesState)another).getAgreement());
            setDate(((UniscEduAgreementFinesState)another).getDate());
            setFinesActive(((UniscEduAgreementFinesState)another).isFinesActive());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniscEduAgreementFinesStateGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniscEduAgreementFinesState.class;
        }

        public T newInstance()
        {
            return (T) new UniscEduAgreementFinesState();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "agreement":
                    return obj.getAgreement();
                case "date":
                    return obj.getDate();
                case "finesActive":
                    return obj.isFinesActive();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "agreement":
                    obj.setAgreement((UniscEduMainAgreement) value);
                    return;
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "finesActive":
                    obj.setFinesActive((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "agreement":
                        return true;
                case "date":
                        return true;
                case "finesActive":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "agreement":
                    return true;
                case "date":
                    return true;
                case "finesActive":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "agreement":
                    return UniscEduMainAgreement.class;
                case "date":
                    return Date.class;
                case "finesActive":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniscEduAgreementFinesState> _dslPath = new Path<UniscEduAgreementFinesState>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniscEduAgreementFinesState");
    }
            

    /**
     * @return договор. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementFinesState#getAgreement()
     */
    public static UniscEduMainAgreement.Path<UniscEduMainAgreement> agreement()
    {
        return _dslPath.agreement();
    }

    /**
     * @return Дата. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementFinesState#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return Начислять пени. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementFinesState#isFinesActive()
     */
    public static PropertyPath<Boolean> finesActive()
    {
        return _dslPath.finesActive();
    }

    public static class Path<E extends UniscEduAgreementFinesState> extends EntityPath<E>
    {
        private UniscEduMainAgreement.Path<UniscEduMainAgreement> _agreement;
        private PropertyPath<Date> _date;
        private PropertyPath<Boolean> _finesActive;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return договор. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementFinesState#getAgreement()
     */
        public UniscEduMainAgreement.Path<UniscEduMainAgreement> agreement()
        {
            if(_agreement == null )
                _agreement = new UniscEduMainAgreement.Path<UniscEduMainAgreement>(L_AGREEMENT, this);
            return _agreement;
        }

    /**
     * @return Дата. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementFinesState#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(UniscEduAgreementFinesStateGen.P_DATE, this);
            return _date;
        }

    /**
     * @return Начислять пени. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementFinesState#isFinesActive()
     */
        public PropertyPath<Boolean> finesActive()
        {
            if(_finesActive == null )
                _finesActive = new PropertyPath<Boolean>(UniscEduAgreementFinesStateGen.P_FINES_ACTIVE, this);
            return _finesActive;
        }

        public Class getEntityClass()
        {
            return UniscEduAgreementFinesState.class;
        }

        public String getEntityName()
        {
            return "uniscEduAgreementFinesState";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
