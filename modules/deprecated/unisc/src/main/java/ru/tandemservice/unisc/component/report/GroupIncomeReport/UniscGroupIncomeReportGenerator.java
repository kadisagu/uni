// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisc.component.report.GroupIncomeReport;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import jxl.write.Number;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.report.xls.CellFormatFactory;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.unisc.entity.agreements.UniscEduAdditAgreement;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayFactRow;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayPlanRow;
import ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student;

import java.io.ByteArrayOutputStream;
import java.util.*;

/**
 * @author oleyba
 * @since 16.04.2010
 */
public class UniscGroupIncomeReportGenerator
{
    private Session session;
    private Group group;
    private Date dateFrom;
    private Date dateTo;

    private CellFormatFactory _cellFormatFactory = new CellFormatFactory();

    public UniscGroupIncomeReportGenerator(Session session, Group group, Date dateFrom, Date dateTo)
    {
        this.session = session;
        this.group = group;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
    }

    public DatabaseFile generateReportContent() throws Exception
    {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        WritableWorkbook workbook = Workbook.createWorkbook(out, ws);

        WritableSheet sheet = workbook.createSheet("По группам", 0);
        sheet.getSettings().setCopies(1);

        // ширина колонок в пикселях
        int[] columnWidth = new int[]{91, 91, 105, 105, 105, 105, 105, 91, 84, 105, 84, 91, 105};
        for (int i = 0; i < columnWidth.length; i++)
            sheet.setColumnView(i, columnWidth[i] / 7);
        sheet.setRowView(6, 840);

        WritableCellFormat header = getCellFormat(Bold.YES, Alignment.CENTRE, Color.NONE, Border.NONE);
        header.setFont(new WritableFont(WritableFont.ARIAL, 12, WritableFont.BOLD));
        sheet.addCell(new Label(0, 0, "Приход средств (по группе)", header));
        sheet.mergeCells(0, 0, 12, 0);

        sheet.addCell(new Label(0, 1, "за " + DateFormatter.DEFAULT_DATE_FORMATTER.format(dateFrom) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(dateTo), header));
        sheet.mergeCells(0, 1, 12, 1);

        sheet.addCell(new Label(0, 3, "Группа", getCellFormat(Bold.YES, Alignment.LEFT, Color.NONE, Border.NONE)));
        sheet.addCell(new Label(1, 3, group.getTitle(), getCellFormat(Bold.NO, Alignment.LEFT, Color.NONE, Border.NONE)));
        sheet.addCell(new Label(2, 3, "Курс", getCellFormat(Bold.YES, Alignment.LEFT, Color.NONE, Border.NONE)));
        sheet.addCell(new Label(3, 3, group.getCourse().getTitle(), getCellFormat(Bold.NO, Alignment.LEFT, Color.NONE, Border.NONE)));
        sheet.addCell(new Label(0, 4, "Направление подготовки (специальность)", getCellFormat(Bold.YES, Alignment.LEFT, Color.NONE, Border.NONE)));
        sheet.mergeCells(0, 4, 2, 4);
        sheet.addCell(new Label(3, 4, group.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getDisplayableTitle(), getCellFormat(Bold.NO, Alignment.LEFT, Color.NONE, Border.NONE)));
        sheet.mergeCells(3, 4, 12, 4);

        WritableCellFormat tableHeaderFormat = getCellFormat(Bold.YES, Alignment.LEFT, Color.BLUE, Border.ALL);
        tableHeaderFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        String[] columnTitles = new String[]{"Фамилия", "Имя", "Отчество", "Номер приказа (зачисление)", "Дата приказа (зачисление)", "Номер приказа (отчисление)", "Дата приказа (отчисление)", "Наличие льготы по оплате", "Тип документа об оплате", "Номер документа об оплате", "Дата оплаты", "Сумма"};
        for (int i = 0; i < columnTitles.length; i++)
            sheet.addCell(new Label(i, 6, columnTitles[i], tableHeaderFormat));
        WritableCellFormat totalHeaderCellFormat = getCellFormat(Bold.YES, Alignment.LEFT, Color.GREEN, Border.ALL);
        totalHeaderCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        sheet.addCell(new Label(columnTitles.length, 6, "ИТОГО", totalHeaderCellFormat));

        int row = 7;

        Map<Student, List<UniscEduAgreementPayFactRow>> paymentMap = getPayments();
        Map<Student, OrderData> orderDataMap = getOrderData();
        Map<Student, String> discount = getDiscount();
        double total = 0;
        for (Student student : paymentMap.keySet())
        {
            List<UniscEduAgreementPayFactRow> payments = paymentMap.get(student);
            boolean bottom = payments.size() == 1;
            sheet.addCell(new Label(0, row, student.getPerson().getIdentityCard().getLastName(), getCellFormat(Bold.NO, Alignment.LEFT, Color.NONE, getBorders(true, bottom))));
            sheet.addCell(new Label(1, row, student.getPerson().getIdentityCard().getFirstName(), getCellFormat(Bold.NO, Alignment.LEFT, Color.NONE, getBorders(true, bottom))));
            sheet.addCell(new Label(2, row, StringUtils.trimToEmpty(student.getPerson().getIdentityCard().getMiddleName()), getCellFormat(Bold.NO, Alignment.LEFT, Color.NONE, getBorders(true, bottom))));
            printOrderData(sheet, row, bottom, orderDataMap.get(student));
            sheet.addCell(new Label(7, row, StringUtils.trimToEmpty(discount.get(student)), getCellFormat(Bold.NO, Alignment.RIGHT, Color.NONE, getBorders(true, bottom))));
            boolean top = true;
            double sum = 0; int mergeStart = row;
            for (UniscEduAgreementPayFactRow payment : payments)
            {
                bottom = (row == mergeStart + payments.size() - 1);
                if (!top)
                    for (int col = 0; col <=7; col++)
                        sheet.addCell(new Label(col, row, "", getCellFormat(Bold.NO, Alignment.LEFT, Color.NONE, getBorders(false, bottom))));                        
                sheet.addCell(new Label(8, row, payment.getType() == null ? "" : payment.getType().getShortTitle(), getCellFormat(Bold.NO, Alignment.LEFT, Color.NONE, getBorders(top, bottom))));
                sheet.addCell(new Label(9, row, StringUtils.trimToEmpty(payment.getNumber()), getCellFormat(Bold.NO, Alignment.RIGHT, Color.NONE, getBorders(top, bottom))));
                sheet.addCell(new Label(10, row, DateFormatter.DEFAULT_DATE_FORMATTER.format(payment.getDate()), getCellFormat(Bold.NO, Alignment.RIGHT, Color.NONE, getBorders(top, bottom))));
                sheet.addCell(new Number(11, row, payment.getCostAsDouble(), getCellFormat(true, Bold.NO, Alignment.RIGHT, Color.NONE, getBorders(top, bottom))));
                row++; top = false; sum = sum + payment.getCostAsDouble();
            }
            sheet.addCell(new Number(12, mergeStart, sum, getCellFormat(true, Bold.YES, Alignment.RIGHT, Color.GREEN, getBorders(true, bottom))));
            sheet.mergeCells(12, mergeStart, 12, row-1);
            total = total + sum;

        }
        sheet.addCell(new Label(0, row, "ИТОГО по группе:", getCellFormat(true, Bold.YES, Alignment.RIGHT, Color.BLUE, getBorders(true, true))));
        sheet.mergeCells(0, row, 11, row);
        sheet.addCell(new Number(12, row, total, getCellFormat(true, Bold.YES, Alignment.RIGHT, Color.GREEN, getBorders(true, true))));

        workbook.write();
        workbook.close();

        DatabaseFile content = new DatabaseFile();
        content.setContent(out.toByteArray());
        return content;
    }



    private void printOrderData(WritableSheet sheet, int row, boolean bottom, OrderData data)
            throws Exception
    {
        if (data == null)
        {
            for (int col = 3; col <= 9; col++)
                sheet.addCell(new Label(col, row, "", getCellFormat(Bold.NO, Alignment.RIGHT, Color.NONE, getBorders(true, bottom))));
            return;
        }
        Date enrOrderDate = data.getEduEnrollmentOrderDate();
        String enrOrderNumber = data.getEduEnrollmentOrderNumber();
        // если восстановление или перевод были позже - печатаем их туда
        if (null == enrOrderDate || (null != data.getRestorationOrderDate() && data.getRestorationOrderDate().after(data.getEduEnrollmentOrderDate())))
        {
            enrOrderDate = data.getRestorationOrderDate();
            if (null != data.getRestorationOrderDate()) enrOrderNumber = "восс. " + StringUtils.trimToEmpty(data.getRestorationOrderNumber());
        }
        if (null == enrOrderDate || (null != data.getTransferOrderDate() && data.getTransferOrderDate().after(data.getEduEnrollmentOrderDate())))
        {
            enrOrderDate = data.getTransferOrderDate();
            if (null != data.getTransferOrderDate()) enrOrderNumber = "перев. " + StringUtils.trimToEmpty(data.getTransferOrderNumber());
        }
        sheet.addCell(new Label(3, row, StringUtils.trimToEmpty(enrOrderNumber), getCellFormat(Bold.NO, Alignment.RIGHT, Color.NONE, getBorders(true, bottom))));
        sheet.addCell(new Label(4, row, DateFormatter.DEFAULT_DATE_FORMATTER.format(enrOrderDate), getCellFormat(Bold.NO, Alignment.RIGHT, Color.NONE, getBorders(true, bottom))));
        sheet.addCell(new Label(5, row, DateFormatter.DEFAULT_DATE_FORMATTER.format(data.getExcludeOrderDate()), getCellFormat(Bold.NO, Alignment.RIGHT, Color.NONE, getBorders(true, bottom))));
        sheet.addCell(new Label(6, row, StringUtils.trimToEmpty(data.getExcludeOrderNumber()), getCellFormat(Bold.NO, Alignment.RIGHT, Color.NONE, getBorders(true, bottom))));
    }

    private Map<Student, OrderData> getOrderData()
    {
        HashMap<Student, OrderData> map = new HashMap<>();
        MQBuilder builder = new MQBuilder(OrderData.ENTITY_CLASS, "d");
        builder.add(MQExpression.eq("d", OrderData.student().group().s(), group));
        for (OrderData orderData : builder.<OrderData>getResultList(session))
            map.put(orderData.getStudent(), orderData);
        return map;
     }

    private Map<Student, String> getDiscount()
    {
        HashMap<Student, String> map = new HashMap<>();

        MQBuilder builder = new MQBuilder(UniscEduAgreementPayPlanRow.ENTITY_CLASS, "pay");
        builder.addDomain("rel", UniscEduAgreement2Student.ENTITY_CLASS);
        builder.addJoin("rel", UniscEduAgreement2Student.student().s(), "student");
        builder.addSelect("student");

        MQBuilder add = new MQBuilder(UniscEduAdditAgreement.ENTITY_CLASS, "add");
        add.add(MQExpression.eqProperty("pay", UniscEduAgreementPayPlanRow.owner().id().s(), "add", "id"));
        add.add(MQExpression.eqProperty("add", UniscEduAdditAgreement.agreement().id().s(), "rel", UniscEduAgreement2Student.agreement().id().s()));
        builder.add(MQExpression.or(
                MQExpression.eqProperty("pay", UniscEduAgreementPayPlanRow.owner().id().s(), "rel", UniscEduAgreement2Student.agreement().id().s()),
                MQExpression.exists(add)));

        builder.add(MQExpression.isNotNull("pay", UniscEduAgreementPayPlanRow.discount().s()));
        builder.add(MQExpression.eq("student", Student.group().s(), group));
        builder.add(UniMQExpression.betweenDate("pay", UniscEduAgreementPayPlanRow.P_DATE, dateFrom, dateTo));
        builder.add(MQExpression.isNull("pay", UniscEduAgreementPayPlanRow.L_REPLACED_BY));

        builder.addOrder("student", Student.id().s());
        builder.addOrder("pay", UniscEduAgreementPayPlanRow.date().s());

        for (Object[] row : builder.<Object[]>getResultList(session))
        {
            UniscEduAgreementPayPlanRow pay = (UniscEduAgreementPayPlanRow) row[0];
            if (pay.getDiscount() == 0) continue;
            Student student = (Student) row[1];
            String discount = String.valueOf(-1 * pay.getDiscount()) + "% (по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(pay.getDate())+")";
            String prev = map.get(student);
            if (null != prev) discount = prev + "; " + discount;
            map.put(student, discount);
        }

        return map;
    }    

    private Map<Student, List<UniscEduAgreementPayFactRow>> getPayments()
    {
        LinkedHashMap<Student, List<UniscEduAgreementPayFactRow>> map = new LinkedHashMap<>();

        MQBuilder builder = new MQBuilder(UniscEduAgreementPayFactRow.ENTITY_CLASS, "pay");
        builder.addDomain("rel", UniscEduAgreement2Student.ENTITY_CLASS);
        builder.addJoin("rel", UniscEduAgreement2Student.student().s(), "student");
        builder.addSelect("student");

        builder.addLeftJoinFetch("student", Student.L_GROUP, "group_fetch");
        builder.addLeftJoinFetch("student", Student.L_COURSE, "course_fetch");
        builder.addLeftJoinFetch("student", Student.L_PERSON, "person_fetch");
        builder.addLeftJoinFetch("person_fetch", Person.L_IDENTITY_CARD, "idc_fetch");

        builder.add(MQExpression.eqProperty("pay", UniscEduAgreementPayFactRow.L_AGREEMENT + ".id", "rel", UniscEduAgreement2Student.L_AGREEMENT + ".id"));
        builder.add(MQExpression.eq("student", Student.group().s(), group));
        builder.add(UniMQExpression.betweenDate("pay", UniscEduAgreementPayFactRow.P_DATE, dateFrom, dateTo));

        builder.addOrder("student", Student.person().identityCard().lastName().s());
        builder.addOrder("student", Student.person().identityCard().firstName().s());
        builder.addOrder("student", Student.person().identityCard().middleName().s());
        builder.addOrder("pay", UniscEduAgreementPayFactRow.date().s());

        for (Object[] row : builder.<Object[]>getResultList(session))
        {
            UniscEduAgreementPayFactRow pay = (UniscEduAgreementPayFactRow) row[0];
            Student student = (Student) row[1];
            List<UniscEduAgreementPayFactRow> payments = map.get(student);
            if (null == payments)
                map.put(student, payments = new ArrayList<>());
            payments.add(pay);
        }

        return map;
    }


    private Border[] getBorders(boolean top, boolean bottom)
    {
        List<Border> result = new ArrayList<>();
        if (top) result.add(Border.TOP);
        if (bottom) result.add(Border.BOTTOM);
        result.add(Border.LEFT);
        result.add(Border.RIGHT);
        if (result.size() == 4) return new Border[] {Border.ALL};
        return result.toArray(new Border[result.size()]);
    }

    //sheet.addCell(new jxl.write.Number(column, row, value, getCellFormat(bold, Alignment.RIGHT, color, borders)));

    private WritableCellFormat getCellFormat(Bold bold, Alignment alignment, Color backgroundColor, jxl.format.Border... borders) throws Exception
    {
        return getCellFormat(false, bold, alignment, backgroundColor, borders);
    }

    /**
     * Возвращает формат ячейки по заданным параметрам
     * Если еще не существует, то создает и регистрирует
     *
     * @param bold            жирный
     * @param alignment       выравнивание
     * @param backgroundColor цвет фона
     * @param borders         границы
     * @return формат ячейки по заданным параметрам
     * @throws Exception если что-нибудь сломалось в jxl
     */
    private WritableCellFormat getCellFormat(boolean number, Bold bold, Alignment alignment, Color backgroundColor, jxl.format.Border... borders) throws Exception
    {
        String cellFormatName = getCellFormatName(number, bold, alignment, backgroundColor, borders);
        WritableCellFormat result = _cellFormatFactory.getFormat(cellFormatName);
        if (result == null)
        {
            WritableFont font = new WritableFont(_cellFormatFactory.getDefaultCellFormat().getFont());
            font.setBoldStyle((bold == Bold.YES) ? WritableFont.BOLD : WritableFont.NO_BOLD);
            _cellFormatFactory.registerFont(font);

            result =  number ? new WritableCellFormat(font, NumberFormats.FLOAT) : new WritableCellFormat(font);
            result.setWrap(true);
            if (alignment != null)
                result.setAlignment(alignment);
            switch (backgroundColor)
            {
                case BLUE:
                    result.setBackground(Colour.ICE_BLUE);
                    break;
                case GREEN:
                    result.setBackground(Colour.LIGHT_GREEN);
                    break;
            }
            for (jxl.format.Border border : borders)
                result.setBorder(border, BorderLineStyle.DOTTED);
            _cellFormatFactory.registerCellFormat(getCellFormatName(number, bold, alignment, backgroundColor, borders), result);
        }
        return result;
    }

    /**
     * @param bold            жирный
     * @param alignment       выравнивание
     * @param backgroundColor цвет фона
     * @param borders         границы
     * @return название формата ячейки по заданным параметрам
     */
    private String getCellFormatName(boolean number, Bold bold, Alignment alignment, Color backgroundColor, Border... borders)
    {
        StringBuilder sb = new StringBuilder();
        sb.append(bold.toString());
        sb.append(java.lang.Boolean.toString(number));
        sb.append(alignment.getDescription());
        sb.append(backgroundColor.toString());
        for (jxl.format.Border border : borders)
            sb.append(border.getDescription());
        return sb.toString();
    }

    private enum Color
    {
        NONE, BLUE, GREEN
    }

    private enum Bold
    {
        YES, NO
    }
}
