package ru.tandemservice.unisc.component.settings.CostSettings.Edit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.Return;

import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;

@Input({
	@Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="eduOrgUnit.id")
})
@Return({
	@Bind(key="close", binding="close")
})
public class Model {
    private boolean close = true;

    public boolean isClose() { return close; }
    public void setClose(boolean close) { this.close = close; }

    private UniscEduOrgUnit eduOrgUnit = new UniscEduOrgUnit();
	public UniscEduOrgUnit getEduOrgUnit() { return this.eduOrgUnit; }
	public void setEduOrgUnit(final UniscEduOrgUnit eduOrgUnit) { this.eduOrgUnit = eduOrgUnit; }

	public EducationOrgUnit getEducationOrgUnit() { return this.getEduOrgUnit().getEducationOrgUnit(); }

}
