package ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage3;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.IComponentRegion;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.SimpleListDataSource;
import org.tandemframework.shared.person.base.bo.Person.ui.NextOfKinAddEdit.PersonNextOfKinAddEdit;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage.AbstractStageController;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson;
import ru.tandemservice.unisc.entity.agreements.gen.UniscEduAgreementNaturalPersonGen;

import java.util.Collections;

/**
 * @author vdanilov
 */
@SuppressWarnings("deprecation")
public class Controller extends AbstractStageController<IDAO, Model> {

    @Override
    public void onRefreshComponent(final IBusinessComponent component) {
        super.onRefreshComponent(component);
        final Model model = this.getModel(component);

        fillDataSource(model);
    }

    private void fillDataSource(final Model model) {

        final SimpleListDataSource<UniscEduAgreementNaturalPerson> dataSource = model.getNextOfKinDataSource();
        if (!dataSource.getColumns().isEmpty()) {
            dataSource.getColumns().clear();
        }

        dataSource.addColumn(new SimpleColumn("", UniscEduAgreementNaturalPersonGen.L_RELATION_DEGREE+".title").setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("ФИО", UniscEduAgreementNaturalPerson.P_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Паспорт", UniscEduAgreementNaturalPerson.P_SIMPLE_PASPORT_TITLE).setClickable(false).setOrderable(false));
        if ((null != model.getNextOfKinList()) && (model.getNextOfKinList().size() > 1)) {
            dataSource.addColumn(new ToggleColumn("Оплачивает", UniscEduAgreementNaturalPersonGen.P_USED_IN_AGREEMENT).setListener("onClickUsedNextOfKin"));
            dataSource.addColumn(new ToggleColumn("Подписывает", UniscEduAgreementNaturalPersonGen.P_SIGN_IN_AGREEMENT).setListener("onClickSignNextOfKin"));
        }
        dataSource.addColumn(new ActionColumn("", ActionColumn.EDIT, "onClickEditNextOfKin"));


    }

    public void onClickAddContractor(final IBusinessComponent component) {
        component.createDefaultChildRegion(new ComponentActivator(
                ru.tandemservice.unictr.component.contractor.ContractorAddEdit.Model.class.getPackage().getName()
        ));
    }

    public void onClickAddContactPerson(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        component.createDefaultChildRegion(new ComponentActivator(
                ru.tandemservice.unictr.component.contactperson.ContactPersonAddEdit.Model.class.getPackage().getName(),
                new ParametersMap()
                        .add("contractorId", model.getContractor().getId())
                        .add(PublisherActivator.PUBLISHER_ID_KEY, null)
        ));
    }

    public void onClickSignNextOfKin(final IBusinessComponent component) {
        final Long id = (Long)component.getListenerParameter();
        if (null == id) { return; }
        final Model model = this.getModel(component);
        for (final UniscEduAgreementNaturalPerson np: model.getNextOfKinList()) {
            np.setSignInAgreement(id.equals(np.getId()));
        }
    }

    public void onClickUsedNextOfKin(final IBusinessComponent component) {
        final Long id = (Long)component.getListenerParameter();
        if (null == id) { return; }
        final Model model = this.getModel(component);
        for (final UniscEduAgreementNaturalPerson np: model.getNextOfKinList()) {
            if (id.equals(np.getId())) {
                np.setUsedInAgreement(!np.isUsedInAgreement());
            }
        }
    }

    public void onClickEditNextOfKin(final IBusinessComponent component) {
        final Long id = (Long) component.getListenerParameter();
        if (null == id) { return; }
        final Model model = this.getModel(component);
        for (final UniscEduAgreementNaturalPerson np: model.getNextOfKinList()) {
            if (id.equals(np.getId())) {
                component.createChildRegion("nperson", new ComponentActivator(
                        ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage3.NaturalPersonEdit.Model.class.getPackage().getName(),
                        Collections.<String, Object>singletonMap("naturalPerson", np)
                ));


                return;
            }
        }

        //final Long id = (Long) component.getListenerParameter();
        //final IEntityMeta meta = EntityRuntime.getMeta(id);
        //if (PersonNextOfKin.class.isAssignableFrom(meta.getEntityClass())) {
        //	final Model model = this.getModel(component);
        //	final PersonRole role = model.getPersonRole();
        //	component.createDefaultChildRegion(new ComponentActivator(
        //			IUniComponents.EMPLOYEE_NEXT_OF_KIN_ADD_EDIT,
        //			new ParametersMap()
        //			.add("nextOfKinId", id)
        //			.add("personId", role.getPerson().getId())
        //			.add("personRoleName", role.getClass().getSimpleName()))
        //	);
        //} else {
        //	// inline edit
        //}
    }

    public void onClickAddNextOfKin(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        final PersonRole role = model.getPersonRole();
        component.createDefaultChildRegion(new ComponentActivator(
                                                   PersonNextOfKinAddEdit.class.getSimpleName(),
                                                   new ParametersMap()
                                                           .add("personId", role.getPerson().getId())
                                                           .add("personRoleName", EntityRuntime.getMeta(role.getId()).getEntityClass().getSimpleName()))
        );
    }

    public void onChangeNextOfKinList(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        this.getDao().prepareNaturalPersonsList(model);
        fillDataSource(model);

        final IComponentRegion region = component.getChildRegion("nperson");
        if (null != region) { region.deactivateComponent(); }
    }


}
