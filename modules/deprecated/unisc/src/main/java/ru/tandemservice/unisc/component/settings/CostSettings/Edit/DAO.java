package ru.tandemservice.unisc.component.settings.CostSettings.Edit;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;

public class DAO extends UniDao<Model> implements IDAO {

	@Override
	public void prepare(final Model model) {
		model.setEduOrgUnit(this.get(UniscEduOrgUnit.class, model.getEduOrgUnit().getId()));
	}

	@Override
	public void save(final Model model) {
		this.getSession().saveOrUpdate(model.getEduOrgUnit());
	}
}
