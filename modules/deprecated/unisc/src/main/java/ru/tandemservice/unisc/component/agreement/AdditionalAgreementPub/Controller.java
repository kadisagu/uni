package ru.tandemservice.unisc.component.agreement.AdditionalAgreementPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.unisc.UniscComponents;

import java.util.Collections;

public class Controller extends AbstractBusinessController<IDAO, Model> {
    @Override
    public void onRefreshComponent(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
        component.createChildRegion("agreement", new ComponentActivator(
                UniscComponents.AGREEMENT_PUB,
                Collections.<String, Object>singletonMap(PublisherActivator.PUBLISHER_ID_KEY, model.getAgreement().getId())
        ));
    }

    public void onClickEdit(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                UniscComponents.STUDENT_ADDITIONAL_AGREMENT_ADD_EDIT,
                Collections.<String, Object>singletonMap(PublisherActivator.PUBLISHER_ID_KEY, model.getAgreement().getId())
        ));
        //this.activate(component, new ComponentActivator(
        //		UniscComponents.STUDENT_ADDITIONAL_AGREMENT_ADD_EDIT,
        //		Collections.<String, Object>singletonMap(PublisherActivator.PUBLISHER_ID_KEY, model.getAgreement().getId())
        //));
    }

    public void onClickPrint(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        this.activateInRoot(component, new ComponentActivator(
                UniscComponents.STUDENT_ADDITIONAL_AGREMENT_PRINT,
                Collections.<String, Object>singletonMap(PublisherActivator.PUBLISHER_ID_KEY, model.getAgreement().getId())
        ));
    }

}
