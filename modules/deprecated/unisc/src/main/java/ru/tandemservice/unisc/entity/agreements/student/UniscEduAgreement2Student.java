package ru.tandemservice.unisc.entity.agreements.student;

import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.formatters.StudentNumberFormatter;
import ru.tandemservice.unisc.entity.agreements.IUniscEduAgreement2PersonRole;
import ru.tandemservice.unisc.entity.agreements.student.gen.UniscEduAgreement2StudentGen;

/**
 * Договор со студентом
 */
public class UniscEduAgreement2Student extends UniscEduAgreement2StudentGen
implements IUniscEduAgreement2PersonRole<Student> {
    @Override public String getPermissionContext() { return "Student"; }
    @Override public Student getTarget() { return getStudent(); }
    @Override public String getTitle() {
        return "Договор со студентом №" + getAgreement().getNumber() + " " +  (getStudent().getPersonalNumber()) + " " + getStudent().getPerson().getFullFio();
    }
}
