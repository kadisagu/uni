package ru.tandemservice.unisc.component.agreement.AgreementPayPlanEdit;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.tapestry.form.IFormComponent;
import org.apache.tapestry.form.ValidationMessages;
import org.apache.tapestry.form.translator.NumberTranslator;
import org.apache.tapestry.form.translator.Translator;
import org.apache.tapestry.form.validator.BaseValidator;
import org.apache.tapestry.form.validator.Validator;
import org.apache.tapestry.valid.ValidatorException;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayPlanRow;
import ru.tandemservice.unisc.entity.catalog.UniscCustomerType;

import java.util.*;

/**
 * @author vdanilov
 */
public class AgreementPayPlanModel {

    private boolean disabled;
    public boolean isDisabled() { return this.disabled; }
    public void setDisabled(final boolean disabled) { this.disabled = disabled; }

    public AgreementPayPlanModel() {
        this(false);
    }

    public AgreementPayPlanModel(final boolean disabled) {
        this.disabled = disabled;
    }


    private Double _nCostAsDouble = 0d;
    public Double getNCostAsDouble() { return this._nCostAsDouble; }
    public void setNCostAsDouble(final Double costAsDouble) { this._nCostAsDouble = costAsDouble; }

    private Double _jCostAsDouble = 0d;
    public Double getJCostAsDouble() { return this._jCostAsDouble; }
    public void setJCostAsDouble(final Double costAsDouble) { this._jCostAsDouble = costAsDouble; }


    public static class Period {
        private final UniscEduAgreementBase owner;
        public UniscEduAgreementBase getOwner() { return this.owner; }

        private final int period;
        public int getPeriod() { return this.period; }

        private final String title;
        public String getTitle() { return this.title; }


        private List<UniscEduAgreementPayPlanRow> rows = new ArrayList<UniscEduAgreementPayPlanRow>();
        public List<UniscEduAgreementPayPlanRow> getRows() { return this.rows; }
        protected Period(final UniscEduAgreementBase owner, final int period, final String title) {
            this.owner = owner;
            this.period = period;
            this.title = title;
        }
    }

    private List<Period> periods = Collections.emptyList();
    public List<Period> getPeriods() { return this.periods; }
    public void setPeriods(final List<Period> periods) { this.periods = periods; }

    transient private Period period;
    public Period getPeriod() { return this.period; }
    public void setPeriod(final Period period) { this.period = period; }

    transient private UniscEduAgreementPayPlanRow row;
    public UniscEduAgreementPayPlanRow getRow() { return this.row; }
    public void setRow(final UniscEduAgreementPayPlanRow row) { this.row = row; }

    public boolean isPeriodFirstRow() { return ((null != period) && (period.getRows().size() > 0) && (row == period.getRows().get(0))); }
    public int getPeriodSize() { return getPeriod().getRows().size(); }

    public Validator getRowDateValidator() {
        return new BaseValidator() {
            @Override public void validate(final IFormComponent field, final ValidationMessages messages, final Object object) throws ValidatorException {
                // TODO: validate dates
            }
        };
    }

    private UniscCustomerType currentCustomerType() { return this.getRow().getOwner().getCustomerType(); }
    public boolean isHasNaturalPart() {
        final UniscCustomerType currentCustomerType = this.currentCustomerType();
        return ((null == currentCustomerType) || currentCustomerType.hasNaturalPart());
    }
    public boolean isHasJuridicalPart() {
        final UniscCustomerType currentCustomerType = this.currentCustomerType();
        return ((null == currentCustomerType) || currentCustomerType.hasJuridicalPart());
    }

    private double d(final Double d) {
        if (null == d) { return 0; }
        return d;
    }

    /**
     * Устанавливает стоимость для графика из модели
     */
    public void applyCost() {
        this.refreshCost(this.d(this.getNCostAsDouble()), this.d(this.getJCostAsDouble()));
    }

    /**
     * Перераспределяет стоимость между физ и юр лицом, устанавливает указанную стоимость
     */
    public void refreshCost(final double nCost, final double jCost) {
        this.refreshCost();

        final boolean hasNCost = this.d(this.getNCostAsDouble()) > 0;
        final boolean hasJCost = this.d(this.getJCostAsDouble()) > 0;

        final double nRatio = hasNCost ? nCost / this.d(this.getNCostAsDouble()) : nCost;
        final double jRatio = hasJCost ? jCost / this.d(this.getJCostAsDouble()) : jCost;

        for (final Period p: this.getPeriods()) {
            for (final UniscEduAgreementPayPlanRow row: p.getRows()) {
                if (hasNCost) {
                    row.setNCostAsDouble(nRatio * this.d(row.getNCostAsDouble()));
                } else {
                    row.setNCostAsDouble(nRatio / (p.getRows().size() * this.getPeriods().size()));
                }

                if (hasJCost) {
                    row.setJCostAsDouble(jRatio * this.d(row.getJCostAsDouble()));
                } else {
                    row.setJCostAsDouble(jRatio / (p.getRows().size() * this.getPeriods().size()));
                }
            }
        }

        this.refreshCost();
    }

    /**
     * Перераспределяет стоимость между физ и юр лицом в зависимости от настройки
     */
    public void refreshCost() {
        double nCost = 0, jCost = 0;

        for (final Period p: this.getPeriods()) {
            for (final UniscEduAgreementPayPlanRow row: p.getRows()) {
                final UniscCustomerType customerType = row.getOwner().getCustomerType();
                if (null != customerType) {
                    final double rowNCost = this.d(row.getNCostAsDouble());
                    final double rowJCost = this.d(row.getJCostAsDouble());
                    final boolean hasNPart = customerType.hasNaturalPart();
                    final boolean hasJPart = customerType.hasJuridicalPart();
                    final boolean error = ((rowNCost > 0) && !hasNPart) || ((rowJCost > 0) && !hasJPart);
                    if (error) {
                        final double cost = rowNCost + rowJCost;
                        row.setNCostAsDouble(0d);
                        row.setJCostAsDouble(0d);

                        if (hasNPart && hasJPart) {
                            row.setNCostAsDouble(0.5 * cost);
                            row.setJCostAsDouble(0.5 * cost);
                        } else {
                            if (hasNPart) { row.setNCostAsDouble(cost); }
                            if (hasJPart) { row.setJCostAsDouble(cost); }
                        }

                    }
                }
                nCost += this.d(row.getNCostAsDouble());
                jCost += this.d(row.getJCostAsDouble());
            }
        }

        this.setNCostAsDouble(nCost);
        this.setJCostAsDouble(jCost);
    }

    public void prepare(final List<UniscEduAgreementPayPlanRow> agreementPlanRows) {
        UniscEduAgreementBase last = null;
        Map<Integer, Period> stage2period = null;

        final List<Period> periods = new ArrayList<Period>();
        for (final UniscEduAgreementPayPlanRow row: agreementPlanRows) {
            final UniscEduAgreementBase owner = row.getOwner();
            if (!owner.equals(last)) {
                last = owner;
                stage2period = new HashMap<Integer, Period>();
            }

            if (null != stage2period) {
                Period p = stage2period.get(row.getStage());
                if (null == p) {
                    stage2period.put(row.getStage(), p = new Period(owner, row.getStage(), owner.getPayPlanFreq().toString(owner.getConfig(), row.getStage())));
                    periods.add(p);
                }
                p.getRows().add(row);
            }
        }
        this.setPeriods(periods);
    }

    public List<UniscEduAgreementPayPlanRow> getRows() {
        final List<UniscEduAgreementPayPlanRow> result = new ArrayList<UniscEduAgreementPayPlanRow>();
        for (final Period p: this.getPeriods()) {
            for (final UniscEduAgreementPayPlanRow row: p.getRows()) {
                result.add(row);
            }
        }
        return result;
    }


    public boolean split(final Long id) {
        if (null == id) { return false; }
        for (final Period p: this.getPeriods()) {
            final ListIterator<UniscEduAgreementPayPlanRow> it = p.getRows().listIterator();
            while (it.hasNext()) {
                final UniscEduAgreementPayPlanRow r = it.next();
                if (id.equals(r.getId())) {
                    final UniscEduAgreementPayPlanRow clone = new UniscEduAgreementPayPlanRow();
                    clone.update(r);
                    clone.setDate(DateUtils.addDays(r.getDate(), 1));
                    clone.setNCost(r.getNCost()/2);
                    clone.setJCost(r.getJCost()/2);
                    r.setNCost(r.getNCost() - clone.getNCost());
                    r.setJCost(r.getJCost() - clone.getJCost());
                    it.add(clone);
                    return true;
                }
            }
        }
        return false;
    }

    public boolean merge(final Long id) {
        if (null == id) { return false; }
        for (final Period p: this.getPeriods()) {
            final ListIterator<UniscEduAgreementPayPlanRow> it = p.getRows().listIterator();
            UniscEduAgreementPayPlanRow prev = null;
            while (it.hasNext()) {
                final UniscEduAgreementPayPlanRow r = it.next();
                if (id.equals(r.getId())) {
                    if (null != prev) {
                        prev.setNCost(prev.getNCost() + r.getNCost());
                        prev.setJCost(prev.getJCost() + r.getJCost());
                        it.remove();
                        return true;

                    } else {
                        return false;
                    }
                }
                prev = r;
            }
        }
        return false;
    }

    public Translator getTranslator()
    {
        return new NumberTranslator()
        {
            @Override
            protected String formatObject(IFormComponent field, Locale locale, Object object)
            {
                return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(object).replace("-", "+");
            }

            @Override
            protected Object parseText(IFormComponent field, ValidationMessages messages, String text) throws ValidatorException
            {
                text = StringUtils.trimToNull(text);
                if (null == text)
                    return null;
                if (text.charAt(0) == '+') text = text.replaceFirst("\\+", "-");
                return super.parseText(field, messages, text);
            }
        };
    }

    public String getCostAsStringId()
    {
        if (null == period || null == row || null == period.getRows())
            return "_null";
        int i = 0;
        while (i < periods.size() && !period.equals(periods.get(i)))
            i++;
        int j = 0;
        while (j < period.getRows().size() && !row.equals(period.getRows().get(j)))
            j++;
        return "costAsString_" + i + "_" + j;
    }
}
