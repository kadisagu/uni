package ru.tandemservice.unisc.component.settings.CostSettings.EditPlan;

import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {
	void save(Model model);
	void prepareDataSource(Model model);

	void doCloneRow(Model model, Long rowId);
	void doDeleteRow(Model model, Long rowId);
}
