package ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage3;

import org.apache.tapestry.form.IFormComponent;
import org.apache.tapestry.form.ValidationMessages;
import org.apache.tapestry.form.validator.Required;
import org.apache.tapestry.form.validator.Validator;
import org.apache.tapestry.valid.ValidatorException;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.view.list.source.SimpleListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unictr.entity.contractor.ContactPerson;
import ru.tandemservice.unictr.entity.contractor.Contractor;
import ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage.AbstractStageModel;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson;

import java.util.Collections;
import java.util.List;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key="contractorId", binding="createdItemId"),
    @Bind(key="contactPersonId", binding="createdItemId"),
    @Bind(key="createdNextOfKinId", binding="createdItemId"),
    @Bind(key="defaultNaturalPersonList", binding="defaultNextOfKinList")
})
@SuppressWarnings("deprecation")
public class Model extends AbstractStageModel {

    private Long createdItemId;
    public Long getCreatedItemId() { return this.createdItemId; }
    public void setCreatedItemId(final Long createdItemId) { this.createdItemId = createdItemId; }


    // j-person

    private Contractor contractor;
    public Contractor getContractor() { return this.contractor; }
    public void setContractor(final Contractor contractor) { this.contractor = contractor; }

    public ContactPerson getContactPerson() { return this.getAgreement().getJuridicalPerson(); }
    public void setContactPerson(final ContactPerson contactPerson) { this.getAgreement().setJuridicalPerson(contactPerson); }

    private ISelectModel contractorModel;
    public ISelectModel getContractorModel() { return this.contractorModel; }
    protected void setContractorModel(final ISelectModel contractorModel) { this.contractorModel = contractorModel; }

    private ISelectModel contactPersonModel;
    public ISelectModel getContactPersonModel() { return this.contactPersonModel; }
    protected void setContactPersonModel(final ISelectModel contactPersonModel) { this.contactPersonModel = contactPersonModel; }


    // n-person

    public Validator getNextOfKinValidator() {
        return new Required() {
            @SuppressWarnings("unchecked")
            @Override public void validate(final IFormComponent field, final ValidationMessages messages, final Object object) throws ValidatorException {
                super.validate(field, messages, object);
                final List<UniscEduAgreementNaturalPerson> nextOfKinList = (List<UniscEduAgreementNaturalPerson>) object;

                // если всего один товарищь, то увы.... он и подписывеет и оплачивает (не разделяем мы их)
                if (nextOfKinList.size()==1) {
                    final UniscEduAgreementNaturalPerson np = nextOfKinList.get(0);
                    np.setUsedInAgreement(true);
                    np.setSignInAgreement(true);
                }

                boolean hasSigners = false;
                boolean hasPayers = false;
                for (final UniscEduAgreementNaturalPerson np: nextOfKinList) {
                    hasPayers |= np.isUsedInAgreement();
                    hasSigners |= np.isSignInAgreement();
                }

                if (!hasPayers) { throw new ValidatorException("Необходимо указать, кто из перечисленных лиц вносит оплату"); }
                if (!hasSigners) { throw new ValidatorException("Необходимо указать, кто из перечисленных лиц подписывает документ"); }

            }
        };
    }

    private ISelectModel nextOfKinModel;
    public ISelectModel getNextOfKinModel() { return this.nextOfKinModel; }
    protected void setNextOfKinModel(final ISelectModel nextOfKinModel) { this.nextOfKinModel = nextOfKinModel; }

    private List<UniscEduAgreementNaturalPerson> defaultNextOfKinList = null;
    public List<UniscEduAgreementNaturalPerson> getDefaultNextOfKinList() { return this.defaultNextOfKinList; }
    public void setDefaultNextOfKinList(final List<UniscEduAgreementNaturalPerson> defaultNextOfKinList) { this.defaultNextOfKinList = defaultNextOfKinList; }

    private List<UniscEduAgreementNaturalPerson> nextOfKinList = null;
    public List<UniscEduAgreementNaturalPerson> getNextOfKinList() { return this.nextOfKinList; }
    public void setNextOfKinList(final List<UniscEduAgreementNaturalPerson> nextOfKinList) { this.nextOfKinList = nextOfKinList; }

    private final SimpleListDataSource<UniscEduAgreementNaturalPerson> nextOfKinDataSource = new SimpleListDataSource<UniscEduAgreementNaturalPerson>(Collections.<UniscEduAgreementNaturalPerson>emptyList()) {
        @Override public List<UniscEduAgreementNaturalPerson> getEntityList() { return (null == getNextOfKinList() ? super.getEntityList() : getNextOfKinList()); }
        @Override public long getTotalSize() { return getEntityList().size(); }
        @Override public long getCountRow() { return getTotalSize(); }
    };
    public SimpleListDataSource<UniscEduAgreementNaturalPerson> getNextOfKinDataSource() { return this.nextOfKinDataSource; }
}
