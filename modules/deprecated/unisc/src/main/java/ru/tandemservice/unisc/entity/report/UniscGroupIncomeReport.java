package ru.tandemservice.unisc.entity.report;

import org.tandemframework.core.view.formatter.DateFormatter;

import ru.tandemservice.uni.IStorableReport;
import ru.tandemservice.unisc.entity.report.gen.UniscGroupIncomeReportGen;

/**
 * Отчет «Приход средств (по группе)»
 */
public class UniscGroupIncomeReport extends UniscGroupIncomeReportGen implements IStorableReport
{
    public static final String P_PERIOD_TITLE = "periodTitle";    

    public String getPeriodTitle()
    {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }
}