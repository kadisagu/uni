/* $Id: Controller.java 8816 2009-07-02 07:31:01Z ekachanova $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisc.component.agreement.AgreementLogTab;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.shared.commonbase.logging.bo.EventLog.ui.View.RowItem;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.StaticSelectModel;
import ru.tandemservice.unisc.entity.agreements.gen.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author ekachanova
 */
public class Controller extends org.tandemframework.shared.commonbase.logging.bo.EventLog.ui.View.Controller
{

    private static final List<String> _entityClassNames = Arrays.asList(
            UniscEduMainAgreementGen.ENTITY_CLASS,
            UniscEduAdditAgreementGen.ENTITY_CLASS,
            UniscEduAgreementNaturalPersonGen.ENTITY_CLASS,
            UniscEduAgreementPayPlanRowGen.ENTITY_CLASS,
            UniscEduAgreementPayFactRowGen.ENTITY_CLASS,
            UniscEduAgreementFinesHistoryGen.ENTITY_CLASS
    );

    @SuppressWarnings("unchecked")
    @Override
    public void onRefreshComponent(final IBusinessComponent component) {
        final Model model = (Model)component.getModel();
        ((IDAO)getDao()).prepare(model);
        super.onRefreshComponent(component);

        /* patch */ {
            final IMergeRowIdResolver transactionRowIdResolver = entity -> String.valueOf(((RowItem)entity).getTransactionNumber());
            final IMergeRowIdResolver logEventRowIdResolver = entity -> String.valueOf(((RowItem)entity).getEvent().getId());
            for (final AbstractColumn column : model.getDataSource().getColumns()) {
                if ("propertyTitle".equals(column.getKey())) { break; }
                if ("transactionNumber".equals(column.getKey()) || "date".equals(column.getKey())) {
                    column.setMergeRows(true);
                    column.setMergeRowIdResolver(transactionRowIdResolver);
                } else {
                    column.setMergeRows(true);
                    column.setMergeRowIdResolver(logEventRowIdResolver);
                }
            }
        }
    }

    @Override
    public ISelectModel getEntityTypeListModel()
    {
        List<IEntityMeta> types = new ArrayList<>();
        for(IEntityMeta entityType : EntityRuntime.getInstance().getEntities()) {
            if(_entityClassNames.contains(entityType.getClassName())) {
                types.add(entityType);
            }
        }
        return new StaticSelectModel("name", "title", types);
    }
}
