package ru.tandemservice.unisc.entity.agreements;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.entity.IEntity;

public interface IUniscEduAgreement2PersonRole<T extends EntityBase> extends IEntity, ITitled {
    String getPermissionContext();
    T getTarget();
    UniscEduMainAgreement getAgreement();
}
