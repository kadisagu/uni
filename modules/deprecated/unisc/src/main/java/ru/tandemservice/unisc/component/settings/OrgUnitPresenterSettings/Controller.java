package ru.tandemservice.unisc.component.settings.OrgUnitPresenterSettings;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unisc.entity.config.UniscOrgUnitPresenter;

import java.util.Collections;


public class Controller extends AbstractBusinessController<IDAO, Model> {

	@Override
	public void onRefreshComponent(IBusinessComponent component) {
		final Model model = getModel(component);
		model.setSettings(UniBaseUtils.getDataSettings(component, "OrgUnitPresenterSettings.filter"));
		getDao().prepare(model);
		
		if (null == model.getDataSource()) {
			DynamicListDataSource<Model.Wrapper> ds = new DynamicListDataSource<>(component, component1 -> {
                getDao().refreshDataSource(model);
            });
			ds.addColumn(new SimpleColumn("Название", OrgUnit.P_TITLE).setOrderable(true).setClickable(false));
			ds.addColumn(new SimpleColumn("В лице", "presenter."+UniscOrgUnitPresenter.P_PERSON).setOrderable(false).setClickable(false));
			ds.addColumn(new SimpleColumn("На основании", "presenter."+UniscOrgUnitPresenter.P_GROUNDS).setOrderable(false).setClickable(false));
			ds.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit"));
			model.setDataSource(ds);
		}
	}
	
	public void onClickEdit(IBusinessComponent component) {
        activateInRoot(component, new ComponentActivator(Controller.class.getPackage().getName()+".Edit", Collections.singletonMap(PublisherActivator.PUBLISHER_ID_KEY, component.getListenerParameter())));
	}
	
	public void onClickSearch(IBusinessComponent component) {
        Model model = getModel(component);
        model.getDataSource().refresh();
        DataSettingsFacade.saveSettings(model.getSettings());
    }

    public void onClickClear(IBusinessComponent component) {
        getModel(component).getSettings().clear();
        onClickSearch(component);
    }
}
