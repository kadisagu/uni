package ru.tandemservice.unisc.entity.config.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Настройка оплаты (по направлению подготовки)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniscEduOrgUnitGen extends EntityBase
 implements INaturalIdentifiable<UniscEduOrgUnitGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisc.entity.config.UniscEduOrgUnit";
    public static final String ENTITY_NAME = "uniscEduOrgUnit";
    public static final int VERSION_HASH = 919696853;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDUCATION_YEAR = "educationYear";
    public static final String L_EDUCATION_ORG_UNIT = "educationOrgUnit";
    public static final String P_CIPHER = "cipher";
    public static final String P_COUNT_TERM = "countTerm";
    public static final String P_DESCRIPTION = "description";
    public static final String P_CONDITIONS = "conditions";
    public static final String P_DEFAULT_FORMING_DATE = "defaultFormingDate";
    public static final String P_DEFAULT_DEADLINE_DATE = "defaultDeadlineDate";

    private EducationYear _educationYear;     // год
    private EducationOrgUnit _educationOrgUnit;     // направление подготовки
    private String _cipher;     // шифр
    private int _countTerm;     // число семестров в году
    private String _description;     // описание
    private String _conditions;     // условия
    private Date _defaultFormingDate;     // Дата «От» договора (по умолчанию)
    private Date _defaultDeadlineDate;     // Дата «До» договора (по умолчанию)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return год. Свойство не может быть null.
     */
    @NotNull
    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    /**
     * @param educationYear год. Свойство не может быть null.
     */
    public void setEducationYear(EducationYear educationYear)
    {
        dirty(_educationYear, educationYear);
        _educationYear = educationYear;
    }

    /**
     * @return направление подготовки. Свойство не может быть null.
     */
    @NotNull
    public EducationOrgUnit getEducationOrgUnit()
    {
        return _educationOrgUnit;
    }

    /**
     * @param educationOrgUnit направление подготовки. Свойство не может быть null.
     */
    public void setEducationOrgUnit(EducationOrgUnit educationOrgUnit)
    {
        dirty(_educationOrgUnit, educationOrgUnit);
        _educationOrgUnit = educationOrgUnit;
    }

    /**
     * @return шифр.
     */
    @Length(max=255)
    public String getCipher()
    {
        return _cipher;
    }

    /**
     * @param cipher шифр.
     */
    public void setCipher(String cipher)
    {
        dirty(_cipher, cipher);
        _cipher = cipher;
    }

    /**
     * @return число семестров в году. Свойство не может быть null.
     */
    @NotNull
    public int getCountTerm()
    {
        return _countTerm;
    }

    /**
     * @param countTerm число семестров в году. Свойство не может быть null.
     */
    public void setCountTerm(int countTerm)
    {
        dirty(_countTerm, countTerm);
        _countTerm = countTerm;
    }

    /**
     * @return описание.
     */
    @Length(max=2048)
    public String getDescription()
    {
        return _description;
    }

    /**
     * @param description описание.
     */
    public void setDescription(String description)
    {
        dirty(_description, description);
        _description = description;
    }

    /**
     * @return условия.
     */
    @Length(max=2048)
    public String getConditions()
    {
        return _conditions;
    }

    /**
     * @param conditions условия.
     */
    public void setConditions(String conditions)
    {
        dirty(_conditions, conditions);
        _conditions = conditions;
    }

    /**
     * @return Дата «От» договора (по умолчанию).
     */
    public Date getDefaultFormingDate()
    {
        return _defaultFormingDate;
    }

    /**
     * @param defaultFormingDate Дата «От» договора (по умолчанию).
     */
    public void setDefaultFormingDate(Date defaultFormingDate)
    {
        dirty(_defaultFormingDate, defaultFormingDate);
        _defaultFormingDate = defaultFormingDate;
    }

    /**
     * @return Дата «До» договора (по умолчанию).
     */
    public Date getDefaultDeadlineDate()
    {
        return _defaultDeadlineDate;
    }

    /**
     * @param defaultDeadlineDate Дата «До» договора (по умолчанию).
     */
    public void setDefaultDeadlineDate(Date defaultDeadlineDate)
    {
        dirty(_defaultDeadlineDate, defaultDeadlineDate);
        _defaultDeadlineDate = defaultDeadlineDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UniscEduOrgUnitGen)
        {
            if (withNaturalIdProperties)
            {
                setEducationYear(((UniscEduOrgUnit)another).getEducationYear());
                setEducationOrgUnit(((UniscEduOrgUnit)another).getEducationOrgUnit());
                setCipher(((UniscEduOrgUnit)another).getCipher());
            }
            setCountTerm(((UniscEduOrgUnit)another).getCountTerm());
            setDescription(((UniscEduOrgUnit)another).getDescription());
            setConditions(((UniscEduOrgUnit)another).getConditions());
            setDefaultFormingDate(((UniscEduOrgUnit)another).getDefaultFormingDate());
            setDefaultDeadlineDate(((UniscEduOrgUnit)another).getDefaultDeadlineDate());
        }
    }

    public INaturalId<UniscEduOrgUnitGen> getNaturalId()
    {
        return new NaturalId(getEducationYear(), getEducationOrgUnit(), getCipher());
    }

    public static class NaturalId extends NaturalIdBase<UniscEduOrgUnitGen>
    {
        private static final String PROXY_NAME = "UniscEduOrgUnitNaturalProxy";

        private Long _educationYear;
        private Long _educationOrgUnit;
        private String _cipher;

        public NaturalId()
        {}

        public NaturalId(EducationYear educationYear, EducationOrgUnit educationOrgUnit, String cipher)
        {
            _educationYear = ((IEntity) educationYear).getId();
            _educationOrgUnit = ((IEntity) educationOrgUnit).getId();
            _cipher = cipher;
        }

        public Long getEducationYear()
        {
            return _educationYear;
        }

        public void setEducationYear(Long educationYear)
        {
            _educationYear = educationYear;
        }

        public Long getEducationOrgUnit()
        {
            return _educationOrgUnit;
        }

        public void setEducationOrgUnit(Long educationOrgUnit)
        {
            _educationOrgUnit = educationOrgUnit;
        }

        public String getCipher()
        {
            return _cipher;
        }

        public void setCipher(String cipher)
        {
            _cipher = cipher;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof UniscEduOrgUnitGen.NaturalId) ) return false;

            UniscEduOrgUnitGen.NaturalId that = (NaturalId) o;

            if( !equals(getEducationYear(), that.getEducationYear()) ) return false;
            if( !equals(getEducationOrgUnit(), that.getEducationOrgUnit()) ) return false;
            if( !equals(getCipher(), that.getCipher()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEducationYear());
            result = hashCode(result, getEducationOrgUnit());
            result = hashCode(result, getCipher());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEducationYear());
            sb.append("/");
            sb.append(getEducationOrgUnit());
            sb.append("/");
            sb.append(getCipher());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniscEduOrgUnitGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniscEduOrgUnit.class;
        }

        public T newInstance()
        {
            return (T) new UniscEduOrgUnit();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "educationYear":
                    return obj.getEducationYear();
                case "educationOrgUnit":
                    return obj.getEducationOrgUnit();
                case "cipher":
                    return obj.getCipher();
                case "countTerm":
                    return obj.getCountTerm();
                case "description":
                    return obj.getDescription();
                case "conditions":
                    return obj.getConditions();
                case "defaultFormingDate":
                    return obj.getDefaultFormingDate();
                case "defaultDeadlineDate":
                    return obj.getDefaultDeadlineDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "educationYear":
                    obj.setEducationYear((EducationYear) value);
                    return;
                case "educationOrgUnit":
                    obj.setEducationOrgUnit((EducationOrgUnit) value);
                    return;
                case "cipher":
                    obj.setCipher((String) value);
                    return;
                case "countTerm":
                    obj.setCountTerm((Integer) value);
                    return;
                case "description":
                    obj.setDescription((String) value);
                    return;
                case "conditions":
                    obj.setConditions((String) value);
                    return;
                case "defaultFormingDate":
                    obj.setDefaultFormingDate((Date) value);
                    return;
                case "defaultDeadlineDate":
                    obj.setDefaultDeadlineDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "educationYear":
                        return true;
                case "educationOrgUnit":
                        return true;
                case "cipher":
                        return true;
                case "countTerm":
                        return true;
                case "description":
                        return true;
                case "conditions":
                        return true;
                case "defaultFormingDate":
                        return true;
                case "defaultDeadlineDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "educationYear":
                    return true;
                case "educationOrgUnit":
                    return true;
                case "cipher":
                    return true;
                case "countTerm":
                    return true;
                case "description":
                    return true;
                case "conditions":
                    return true;
                case "defaultFormingDate":
                    return true;
                case "defaultDeadlineDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "educationYear":
                    return EducationYear.class;
                case "educationOrgUnit":
                    return EducationOrgUnit.class;
                case "cipher":
                    return String.class;
                case "countTerm":
                    return Integer.class;
                case "description":
                    return String.class;
                case "conditions":
                    return String.class;
                case "defaultFormingDate":
                    return Date.class;
                case "defaultDeadlineDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniscEduOrgUnit> _dslPath = new Path<UniscEduOrgUnit>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniscEduOrgUnit");
    }
            

    /**
     * @return год. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.config.UniscEduOrgUnit#getEducationYear()
     */
    public static EducationYear.Path<EducationYear> educationYear()
    {
        return _dslPath.educationYear();
    }

    /**
     * @return направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.config.UniscEduOrgUnit#getEducationOrgUnit()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
    {
        return _dslPath.educationOrgUnit();
    }

    /**
     * @return шифр.
     * @see ru.tandemservice.unisc.entity.config.UniscEduOrgUnit#getCipher()
     */
    public static PropertyPath<String> cipher()
    {
        return _dslPath.cipher();
    }

    /**
     * @return число семестров в году. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.config.UniscEduOrgUnit#getCountTerm()
     */
    public static PropertyPath<Integer> countTerm()
    {
        return _dslPath.countTerm();
    }

    /**
     * @return описание.
     * @see ru.tandemservice.unisc.entity.config.UniscEduOrgUnit#getDescription()
     */
    public static PropertyPath<String> description()
    {
        return _dslPath.description();
    }

    /**
     * @return условия.
     * @see ru.tandemservice.unisc.entity.config.UniscEduOrgUnit#getConditions()
     */
    public static PropertyPath<String> conditions()
    {
        return _dslPath.conditions();
    }

    /**
     * @return Дата «От» договора (по умолчанию).
     * @see ru.tandemservice.unisc.entity.config.UniscEduOrgUnit#getDefaultFormingDate()
     */
    public static PropertyPath<Date> defaultFormingDate()
    {
        return _dslPath.defaultFormingDate();
    }

    /**
     * @return Дата «До» договора (по умолчанию).
     * @see ru.tandemservice.unisc.entity.config.UniscEduOrgUnit#getDefaultDeadlineDate()
     */
    public static PropertyPath<Date> defaultDeadlineDate()
    {
        return _dslPath.defaultDeadlineDate();
    }

    public static class Path<E extends UniscEduOrgUnit> extends EntityPath<E>
    {
        private EducationYear.Path<EducationYear> _educationYear;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnit;
        private PropertyPath<String> _cipher;
        private PropertyPath<Integer> _countTerm;
        private PropertyPath<String> _description;
        private PropertyPath<String> _conditions;
        private PropertyPath<Date> _defaultFormingDate;
        private PropertyPath<Date> _defaultDeadlineDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return год. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.config.UniscEduOrgUnit#getEducationYear()
     */
        public EducationYear.Path<EducationYear> educationYear()
        {
            if(_educationYear == null )
                _educationYear = new EducationYear.Path<EducationYear>(L_EDUCATION_YEAR, this);
            return _educationYear;
        }

    /**
     * @return направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.config.UniscEduOrgUnit#getEducationOrgUnit()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
        {
            if(_educationOrgUnit == null )
                _educationOrgUnit = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT, this);
            return _educationOrgUnit;
        }

    /**
     * @return шифр.
     * @see ru.tandemservice.unisc.entity.config.UniscEduOrgUnit#getCipher()
     */
        public PropertyPath<String> cipher()
        {
            if(_cipher == null )
                _cipher = new PropertyPath<String>(UniscEduOrgUnitGen.P_CIPHER, this);
            return _cipher;
        }

    /**
     * @return число семестров в году. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.config.UniscEduOrgUnit#getCountTerm()
     */
        public PropertyPath<Integer> countTerm()
        {
            if(_countTerm == null )
                _countTerm = new PropertyPath<Integer>(UniscEduOrgUnitGen.P_COUNT_TERM, this);
            return _countTerm;
        }

    /**
     * @return описание.
     * @see ru.tandemservice.unisc.entity.config.UniscEduOrgUnit#getDescription()
     */
        public PropertyPath<String> description()
        {
            if(_description == null )
                _description = new PropertyPath<String>(UniscEduOrgUnitGen.P_DESCRIPTION, this);
            return _description;
        }

    /**
     * @return условия.
     * @see ru.tandemservice.unisc.entity.config.UniscEduOrgUnit#getConditions()
     */
        public PropertyPath<String> conditions()
        {
            if(_conditions == null )
                _conditions = new PropertyPath<String>(UniscEduOrgUnitGen.P_CONDITIONS, this);
            return _conditions;
        }

    /**
     * @return Дата «От» договора (по умолчанию).
     * @see ru.tandemservice.unisc.entity.config.UniscEduOrgUnit#getDefaultFormingDate()
     */
        public PropertyPath<Date> defaultFormingDate()
        {
            if(_defaultFormingDate == null )
                _defaultFormingDate = new PropertyPath<Date>(UniscEduOrgUnitGen.P_DEFAULT_FORMING_DATE, this);
            return _defaultFormingDate;
        }

    /**
     * @return Дата «До» договора (по умолчанию).
     * @see ru.tandemservice.unisc.entity.config.UniscEduOrgUnit#getDefaultDeadlineDate()
     */
        public PropertyPath<Date> defaultDeadlineDate()
        {
            if(_defaultDeadlineDate == null )
                _defaultDeadlineDate = new PropertyPath<Date>(UniscEduOrgUnitGen.P_DEFAULT_DEADLINE_DATE, this);
            return _defaultDeadlineDate;
        }

        public Class getEntityClass()
        {
            return UniscEduOrgUnit.class;
        }

        public String getEntityName()
        {
            return "uniscEduOrgUnit";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
