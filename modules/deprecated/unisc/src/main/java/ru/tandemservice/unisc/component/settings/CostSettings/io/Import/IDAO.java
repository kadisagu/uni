package ru.tandemservice.unisc.component.settings.CostSettings.io.Import;

import org.tandemframework.core.info.ErrorCollector;

import ru.tandemservice.uni.dao.IUniDao;

import java.io.IOException;

/**
 * @author vdanilov
 */
public interface IDAO extends IUniDao<Model> {
	void save(Model model, ErrorCollector errorCollector) throws IOException;
}
