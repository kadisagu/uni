/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisc.component.report.StudentsWithoutContractsReport;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.Collections;
import java.util.List;

/**
 * @author agolubenko
 * @since May 21, 2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        if (OrgUnitManager.instance().dao().getTopOrgUnitId().equals(model.getOrgUnitId()))
        {
            model.setOrgUnitId(null);
        }

        if (model.getOrgUnitId() != null)
        {
            model.setOrgUnit(getNotNull(model.getOrgUnitId()));
            model.setSecModel(new OrgUnitSecModel(model.getOrgUnit()));
        }

        EducationYear currentEducationYear = get(EducationYear.class, EducationYear.current().s(), true);
        model.setCurrentEducationYear(currentEducationYear);

        model.setEducationYear(currentEducationYear);
        model.setEducationYearList(getEducationYearList(currentEducationYear));
        model.setFormativeOrgUnitModel(getFormativeOrgUnitModel(model.getOrgUnit()));
        model.setTerritorialOrgUnitModel(getTerritorialOrgUnitModel(model.getOrgUnit()));
        model.setDevelopFormModel(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setCourseModel(new LazySimpleSelectModel<>(DevelopGridDAO.getCourseList()));
        model.setStudentCategoryModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(StudentCategory.class)));
        model.setStudentStatusModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(StudentStatus.class)));
    }

    @Override
    public void createReport(Model model)
    {
        model.setContent(new StudentsWithoutContractsReportGenerator(getSession(), model).generateReportContent());
    }

    @SuppressWarnings("unchecked")
    private List<EducationYear> getEducationYearList(EducationYear currentEducationYear)
    {
        Criteria criteria = getSession().createCriteria(EducationYear.class);
        criteria.add(Restrictions.ge(EducationYear.P_INT_VALUE, currentEducationYear.getIntValue()));
        criteria.addOrder(Order.asc(EducationYear.intValue().s()));
        return criteria.list();
    }

    private ISelectModel getFormativeOrgUnitModel(final OrgUnit orgUnit)
    {
        if (orgUnit != null)
        {
            return new FullCheckSelectModel(OrgUnit.P_TITLE)
            {
                @SuppressWarnings("unchecked")
                @Override
                public ListResult<OrgUnit> findValues(String filter)
                {
                    Criteria criteria = getSession().createCriteria(Student.class);
                    criteria.createAlias(Student.educationOrgUnit().s(), "educationOrgUnit");
                    criteria.createAlias(EducationOrgUnit.formativeOrgUnit().fromAlias("educationOrgUnit").s(), "formativeOrgUnit");

                    Criterion eqFormative = Restrictions.eq(EducationOrgUnit.formativeOrgUnit().fromAlias("educationOrgUnit").s(), orgUnit);
                    Criterion eqTerritorial = Restrictions.eq(EducationOrgUnit.territorialOrgUnit().fromAlias("educationOrgUnit").s(), orgUnit);
                    criteria.add(Restrictions.or(eqFormative, eqTerritorial));
                    if (StringUtils.isNotEmpty(filter))
                    {
                        criteria.add(Restrictions.like(OrgUnit.title().fromAlias("formativeOrgUnit").s(), filter));
                    }
                    criteria.setProjection(Projections.distinct(Projections.property(EducationOrgUnit.formativeOrgUnit().fromAlias("educationOrgUnit").s())));
                    List<OrgUnit> result = criteria.list();

                    Collections.sort(result, ITitled.TITLED_COMPARATOR);
                    return new ListResult<>(result);
                }
            };
        }

        return new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING);
    }

    private ISelectModel getTerritorialOrgUnitModel(final OrgUnit orgUnit)
    {
        if (orgUnit != null)
        {
            return new FullCheckSelectModel(OrgUnit.P_TERRITORIAL_TITLE)
            {
                @SuppressWarnings("unchecked")
                @Override
                public ListResult<OrgUnit> findValues(String filter)
                {
                    Criteria criteria = getSession().createCriteria(Student.class);
                    criteria.createAlias(Student.educationOrgUnit().s(), "educationOrgUnit");
                    criteria.createAlias(EducationOrgUnit.territorialOrgUnit().fromAlias("educationOrgUnit").s(), "territorialOrgUnit");

                    Criterion eqFormative = Restrictions.eq(EducationOrgUnit.formativeOrgUnit().fromAlias("educationOrgUnit").s(), orgUnit);
                    Criterion eqTerritorial = Restrictions.eq(EducationOrgUnit.territorialOrgUnit().fromAlias("educationOrgUnit").s(), orgUnit);
                    criteria.add(Restrictions.or(eqFormative, eqTerritorial));
                    criteria.add(Restrictions.like(OrgUnit.title().fromAlias("territorialOrgUnit").s(), filter));
                    if (StringUtils.isNotEmpty(filter))
                    {
                        criteria.setProjection(Projections.distinct(Projections.property(EducationOrgUnit.territorialOrgUnit().fromAlias("educationOrgUnit").s())));
                    }
                    List<OrgUnit> result = criteria.list();

                    Collections.sort(result, ITitled.TITLED_COMPARATOR);
                    return new ListResult<>(result);
                }
            };
        }

        return new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL);
    }
}
