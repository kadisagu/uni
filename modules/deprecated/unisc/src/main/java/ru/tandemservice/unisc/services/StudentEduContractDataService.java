/* $Id$ */
package ru.tandemservice.unisc.services;

import ru.tandemservice.uni.services.ContractAttributes;
import ru.tandemservice.uni.services.IStudentEduContractDataService;
import ru.tandemservice.unisc.dao.agreement.IUniscEduAgreementDAO;
import ru.tandemservice.unisc.entity.agreements.UniscEduMainAgreement;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 15.08.2012
 */
public class StudentEduContractDataService implements IStudentEduContractDataService
{
    @Override
    public ContractAttributes getStudentContractAttributes(Long studentId)
    {
        UniscEduMainAgreement contract = IUniscEduAgreementDAO.INSTANCE.get().getStudentContract(studentId);
        if (null != contract) return new ContractAttributes(contract.getNumber(), contract.getFormingDate());
        return null;
    }

    @Override
    public Map<Long, ContractAttributes> getStudentContractAttributes(Collection<Long> studentIds)
    {
        Map<Long, ContractAttributes> result = new HashMap<Long, ContractAttributes>();

        for (Map.Entry<Long, UniscEduMainAgreement> entry : IUniscEduAgreementDAO.INSTANCE.get().getStudentContracts(studentIds).entrySet())
        {
            result.put(entry.getKey(), new ContractAttributes(entry.getValue().getNumber(), entry.getValue().getFormingDate()));
        }
        return result;
    }
}