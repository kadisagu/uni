// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisc.component.report.PayDebtorsReport;

import org.hibernate.Session;
import org.tandemframework.core.info.ErrorCollector;

import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unisc.component.report.UniscReportAddDaoUtil;

/**
 * @author oleyba
 * @since 26.08.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        final Session session = getSession();

        if (OrgUnitManager.instance().dao().getTopOrgUnitId().equals(model.getOrgUnitId()))
            model.setOrgUnitId(null);

        if (null != model.getOrgUnitId())
        {
            model.setOrgUnit((OrgUnit) getNotNull(model.getOrgUnitId()));
            model.setSecModel(new OrgUnitSecModel(model.getOrgUnit()));
        }        

        UniscReportAddDaoUtil.initSelectModels(model, this, session);
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        if (model.getDateFrom().after(model.getDateTo()))
            errors.add("Дата начала периода должна быть не позже даты окончания периода.", "dateFrom", "dateTo");
    }

    @Override
    public void createReport(Model model)
    {
        model.setContent(new PayDebtorsReportGenerator(getSession(), UniscReportAddDaoUtil.getAgreementBuilder(model), model.getOrgUnitId(), model.getDateTo(), model.getDateFrom()).generateReportContent());
    }
}
