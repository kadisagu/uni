package ru.tandemservice.unisc.component.settings.OrgUnitPresenterSettings;

import java.util.List;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.IOrgUnitTypeModel;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.unisc.entity.config.UniscOrgUnitPresenter;

public class Model implements IOrgUnitTypeModel
{
	public static class Wrapper extends IdentifiableWrapper<OrgUnit> {
		private static final long serialVersionUID = 1L;
		
		private final UniscOrgUnitPresenter presenter;
		public UniscOrgUnitPresenter getPresenter() { return presenter; }
		
		public Wrapper(Long id, String title, UniscOrgUnitPresenter uniscOrgUnitPresenter) {
			super(id, title);
			presenter = uniscOrgUnitPresenter;
		}
	}
	
	private DynamicListDataSource<Wrapper> dataSource;
	public DynamicListDataSource<Wrapper> getDataSource() { return dataSource; }
	public void setDataSource(DynamicListDataSource<Wrapper> dataSource) { this.dataSource = dataSource; }
	
	private IDataSettings _settings;
	public IDataSettings getSettings() { return _settings; }
	public void setSettings(IDataSettings settings) { _settings = settings; }
	
	@Override
    public OrgUnitType getOrgUnitType() {
        return (OrgUnitType) _settings.get("orgUnitType");
    }

	private List<OrgUnitType> _orgUnitTypeList;
	public List<OrgUnitType> getOrgUnitTypeList() { return _orgUnitTypeList; }
	public void setOrgUnitTypeList(List<OrgUnitType> orgUnitTypeList) { _orgUnitTypeList = orgUnitTypeList; }

	private ISelectModel _orgUnitListModel;
	public ISelectModel getOrgUnitListModel() { return _orgUnitListModel; }
	public void setOrgUnitListModel(ISelectModel orgUnitListModel) { _orgUnitListModel = orgUnitListModel; }
	
}
