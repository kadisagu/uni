package ru.tandemservice.unisc.entity.config;

import ru.tandemservice.unisc.dao.config.UniscEduOrgUnitConfigDAO;
import ru.tandemservice.unisc.entity.config.gen.UniscEduOrgUnitPayPlanRowGen;

/**
 * Строка графика оплат (настройка)
 */
public class UniscEduOrgUnitPayPlanRow extends UniscEduOrgUnitPayPlanRowGen
{
    public double getCostAsDouble() { return UniscEduOrgUnitConfigDAO.wrap(super.getCost()); }
    public void setCostAsDouble(double d) { setCost(UniscEduOrgUnitConfigDAO.unwrap(d)); }
}
