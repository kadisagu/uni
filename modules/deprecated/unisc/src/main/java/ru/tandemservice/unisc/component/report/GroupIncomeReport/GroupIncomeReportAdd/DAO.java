// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisc.component.report.GroupIncomeReport.GroupIncomeReportAdd;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unisc.component.report.GroupIncomeReport.UniscGroupIncomeReportGenerator;
import ru.tandemservice.unisc.component.report.PaymentReport.MQBuilderFactory;
import ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student;
import ru.tandemservice.unisc.entity.report.UniscGroupIncomeReport;

import java.util.Date;

/**
 * @author oleyba
 * @since 16.04.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        final Session session = getSession();

        if (OrgUnitManager.instance().dao().getTopOrgUnitId().equals(model.getOrgUnitId()))
            model.setOrgUnitId(null);

        model.setFormativeOrgUnitModel(createOrgUnitAutocompleteModel(model, OrgUnit.P_FULL_TITLE, session, EducationOrgUnit.formativeOrgUnit().s()));
        model.setTerritorialOrgUnitModel(createOrgUnitAutocompleteModel(model, OrgUnit.P_TERRITORIAL_FULL_TITLE, session, EducationOrgUnit.territorialOrgUnit().s()));
        model.setEducationLevelHighSchoolModel(createEducationLevelsHighSchoolModel(model, session));
        model.setDevelopFormModel(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setCourseModel(new LazySimpleSelectModel<>(DevelopGridDAO.getCourseList()));
        model.setGroupModel(getGroupModel(model, session));
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        if (model.getReport().getDateFrom().after(model.getReport().getDateTo()))
            errors.add("Дата начала периода должна быть не позже даты окончания периода.", "dateFrom", "dateTo");
    }

    @Override
    public void update(Model model)
    {
        final Session session = getSession();

        UniscGroupIncomeReport report = model.getReport();
        report.setFormingDate(new Date());


        if (model.getOrgUnitId() != null)
            report.setOrgUnit(UniDaoFacade.getCoreDao().get(OrgUnit.class, model.getOrgUnitId()));
        report.setGroup(model.getGroup().getTitle());

        try
        {
            DatabaseFile databaseFile = new UniscGroupIncomeReportGenerator(getSession(), model.getGroup(), report.getDateFrom(), report.getDateTo()).generateReportContent();
            session.save(databaseFile);

            report.setContent(databaseFile);
            session.save(report);
        }
        catch (Throwable e)
        {
            throw new RuntimeException(e);
        }
    }

    private ISelectModel createOrgUnitAutocompleteModel(final Model model, final String titleProperty, final Session session, final String path)
    {
        return new FullCheckSelectModel(titleProperty)
        {
            @Override
            public ListResult<OrgUnit> findValues(String filter)
            {
                Session session = DataAccessServices.dao().getComponentSession();
                MQBuilder ids = getIdsBuilder(path, model);
                if (null != model.getFormativeOrgUnit() && !EducationOrgUnit.formativeOrgUnit().s().equals(path))
                    ids.add(MQExpression.eq("e", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnit()));
                return new ListResult<>(getBuilder(OrgUnit.ENTITY_CLASS, ids, filter).<OrgUnit>getResultList(session));
            }
        };
    }

    private ISelectModel createEducationLevelsHighSchoolModel(final Model model, final Session session)
    {
        return new FullCheckSelectModel(EducationLevelsHighSchool.P_DISPLAYABLE_TITLE)
        {
            @Override
            public ListResult<EducationLevelsHighSchool> findValues(String filter)
            {
                Session session = DataAccessServices.dao().getComponentSession();
                MQBuilder ids = getIdsBuilder(EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, model);
                if (null != model.getFormativeOrgUnit())
                    ids.add(MQExpression.eq("e", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnit()));
                if (null != model.getTerritorialOrgUnit())
                    ids.add(MQExpression.eq("e", EducationOrgUnit.territorialOrgUnit().s(), model.getTerritorialOrgUnit()));
                return new ListResult<>(getBuilder(EducationLevelsHighSchool.ENTITY_CLASS, ids, filter).<EducationLevelsHighSchool>getResultList(session));
            }
        };
    }

    private MQBuilder getIdsBuilder(String selectProperty, Model model)
    {
        MQBuilder ids = new MQBuilder(EducationOrgUnit.ENTITY_CLASS, "e", new String[]{selectProperty + ".id"});
        ids.addDomain("g", Group.ENTITY_CLASS);
        ids.add(MQExpression.eqProperty("g", Group.educationOrgUnit().id().s(), "e", "id"));
        if (null == model.getOrgUnitId())
            return ids;
        ids.addLeftJoin("e", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, "terr_ou");
        ids.add(MQExpression.or(
            MQExpression.eq("e", EducationOrgUnit.L_FORMATIVE_ORG_UNIT + ".id", model.getOrgUnitId()),
            MQExpression.eq("terr_ou", "id", model.getOrgUnitId())
        ));
        return ids;
    }

    private MQBuilder getBuilder(String entityClass, MQBuilder ids, String filter)
    {
        MQBuilder builder = new MQBuilder(entityClass, "s");
        builder.addOrder("s", "title");
        if (StringUtils.isNotBlank(filter))
            builder.add(MQExpression.like("s", "title", "%" + filter));
        builder.add(MQExpression.in("s", "id", ids));
        return builder;
    }

    private MQBuilderFactory.AutocompleteModel getGroupModel(final Model model, final Session session)
    {
        return new MQBuilderFactory.AutocompleteModel(session)
        {
            @Override
            public MQBuilderFactory getFactory()
            {
                final MQBuilder groupBuilder = new MQBuilder(Group.ENTITY_CLASS, "g");
                groupBuilder.setNeedDistinct(true);
                groupBuilder.addOrder("g", Group.title().s());
                groupBuilder.addDomain("r", UniscEduAgreement2Student.ENTITY_CLASS);
                groupBuilder.add(MQExpression.eqProperty("r", UniscEduAgreement2Student.student().group().id().s(), "g", "id"));
                groupBuilder.addJoin("g", Group.educationOrgUnit().s(), "ou");
                if (model.getOrgUnitId() != null)
                {
                    groupBuilder.addLeftJoin("ou", EducationOrgUnit.territorialOrgUnit().s(), "terr_ou");
                    groupBuilder.add(MQExpression.or(
                            MQExpression.eq("ou", EducationOrgUnit.formativeOrgUnit().s() + ".id", model.getOrgUnitId()),
                            MQExpression.eq("terr_ou", "id", model.getOrgUnitId())
                    ));
                }
                if (model.getFormativeOrgUnit() != null)
                    groupBuilder.add(MQExpression.eq("ou", EducationOrgUnit.formativeOrgUnit().s(), model.getFormativeOrgUnit()));
                if (model.getTerritorialOrgUnit() != null)
                    groupBuilder.add(MQExpression.eq("ou", EducationOrgUnit.territorialOrgUnit().s(), model.getTerritorialOrgUnit()));
                if (model.getEducationLevelHighSchool() != null)
                    groupBuilder.add(MQExpression.eq("ou", EducationOrgUnit.educationLevelHighSchool().s(), model.getEducationLevelHighSchool()));
                if (model.getDevelopForm() != null)
                    groupBuilder.add(MQExpression.eq("ou", EducationOrgUnit.developForm().s(), model.getDevelopForm()));
                if (model.getCourse() != null)
                    groupBuilder.add(MQExpression.eq("g", Group.course().s(), model.getCourse()));
                return MQBuilderFactory.create(groupBuilder, "g");
            }
        };
    }
}

