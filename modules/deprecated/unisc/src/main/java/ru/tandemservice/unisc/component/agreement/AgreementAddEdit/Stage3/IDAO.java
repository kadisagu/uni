package ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage3;

import ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage.IAbstractStageDAO;

/**
 * @author vdanilov
 */
@SuppressWarnings("deprecation")
public interface IDAO extends IAbstractStageDAO<Model> {

    void prepareNaturalPersonsList(Model model);

}
