package ru.tandemservice.unisc.component.agreement.AgreementAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage.AbstractStageModel;
import ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage.IStageResult;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;

import java.util.Arrays;
import java.util.ListIterator;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key=AbstractStageModel.RESULT, binding="result")
})
@SuppressWarnings("deprecation")
public abstract class Model {

    public Boolean creationFormFlag;
    public boolean isCreationFormFlag() { return Boolean.TRUE.equals(creationFormFlag); }

    public void changeCreationFormFlag(final Boolean flag) {
        if ((null == creationFormFlag) || (null == flag)) {
            creationFormFlag = flag;
        }
    }

    private static final String[] STAGES = {
        ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage0.Model.class.getPackage().getName(),
        ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage1.Model.class.getPackage().getName(),
        ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage2.Model.class.getPackage().getName(),
        ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage3.Model.class.getPackage().getName(),
    };

    protected ListIterator<String> prepareStageIterator() { return Arrays.asList(Model.STAGES).listIterator(); }

    private ListIterator<String> stageIterator = prepareStageIterator();
    ListIterator<String> getStageIterator() { return this.stageIterator; }

    private IStageResult result = null;
    public IStageResult getResult() { return this.result; }
    public void setResult(final IStageResult result) { this.result = result; }

    public abstract UniscEduAgreementBase getAgreement();
    public abstract PersonRole getPersonRole();

}
