package ru.tandemservice.unisc.entity.agreements;

import org.tandemframework.core.common.ITitled;

import ru.tandemservice.unisc.entity.agreements.gen.UniscEduAgreementBaseGen;

/**
 * Соглашение
 */
public abstract class UniscEduAgreementBase extends UniscEduAgreementBaseGen implements ITitled {
	public abstract UniscEduMainAgreement getAgreement();

	public int getLastStage() {
		try {
			return this.getPayPlanFreq().getLastStage(this.getConfig(), this.getPayPlanDuration(), this.getFirstStage());
		} catch (final NullPointerException e) {
			return this.getFirstStage();
		}
	}

	public String getLastStageTitle() {
		try {
			final int lastStage = this.getLastStage();
			return this.getPayPlanFreq().toString(this.getConfig(), lastStage);
		} catch (final NullPointerException e) {
			return "";
		}
	}

	public String getFirstStageTitle() {
		try {
			final int firstStage = this.getFirstStage();
			return this.getPayPlanFreq().toString(this.getConfig(), firstStage);
		} catch (final NullPointerException e) {
			return "";
		}
	}

}
