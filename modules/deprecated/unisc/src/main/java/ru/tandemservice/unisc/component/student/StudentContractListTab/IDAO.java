/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisc.component.student.StudentContractListTab;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author vdanilov
 */
public interface IDAO extends IUniDao<Model>
{

    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void updateCheckByLawer(Model model, Long id);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void updateCheckByAccounting(Model model, Long id);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void updateCheckBySignature(Model model, Long id);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void updateActivateState(Model model, Long id);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void deleteAgreement(Model model, Long id);

    @Transactional(propagation= Propagation.SUPPORTS)
    void refreshDataSource(Model model);

    boolean isHasRelation(Student student);

}
