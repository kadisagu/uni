package ru.tandemservice.unisc.component.orgunit.OrgUnitStudentsTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;

import org.tandemframework.shared.organization.base.entity.OrgUnit;

/**
 * 
 * @author nkokorina
 * Created on: 03.02.2010
 */

@Input( { @Bind(key = "orgUnitId", binding = "orgUnitId") })
public class Model
{
    private Long _orgUnitId;
    private OrgUnit _orgUnit;

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }
    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }
    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

}
