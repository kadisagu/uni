/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisc.component.report.StudentsWithoutContractsReport;

import java.util.List;

import org.tandemframework.core.component.State;
import org.tandemframework.sec.runtime.SecurityRuntime;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.StudentStatus;

/**
 * @author agolubenko
 * @since May 21, 2010
 */
@State(keys = "orgUnitId", bindings = "orgUnitId")
public class Model
{
    private static final String VIEW_REPORT_PERMISSION_KEY = "viewStudentsWithoutContractsReport";

    private Long _orgUnitId;
    private OrgUnit _orgUnit;
    private EducationYear _currentEducationYear;

    private byte[] _content;
    private CommonPostfixPermissionModel _secModel;

    private boolean _educationYearActive;
    private EducationYear _educationYear;
    private List<EducationYear> _educationYearList;

    private boolean _formativeOrgUnitActive;
    private List<OrgUnit> _formativeOrgUnitList;
    private ISelectModel _formativeOrgUnitModel;

    private boolean _territorialOrgUnitActive;
    private List<OrgUnit> _territorialOrgUnitList;
    private ISelectModel _territorialOrgUnitModel;

    private boolean _developFormActive;
    private List<DevelopForm> _developFormList;
    private ISelectModel _developFormModel;

    private boolean _courseActive;
    private List<Course> _courseList;
    private ISelectModel _courseModel;

    private boolean _studentCategoryActive;
    private List<StudentCategory> _studentCategoryList;
    private ISelectModel _studentCategoryModel;

    private boolean _studentStatusActive;
    private List<StudentStatus> _studentStatusList;
    private ISelectModel _studentStatusModel;

    public Object getSecuredObject()
    {
        return (getOrgUnit() != null) ? getOrgUnit() : SecurityRuntime.getInstance().getCommonSecurityObject();
    }

    public String getViewKey()
    {
        return (getOrgUnit() != null) ? getSecModel().getPermission(VIEW_REPORT_PERMISSION_KEY) : "scStudentsWithoutContractsReport";
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    public EducationYear getCurrentEducationYear()
    {
        return _currentEducationYear;
    }

    public void setCurrentEducationYear(EducationYear currentEducationYear)
    {
        _currentEducationYear = currentEducationYear;
    }

    public byte[] getContent()
    {
        return _content;
    }

    public void setContent(byte[] content)
    {
        _content = content;
    }

    public boolean isEducationYearActive()
    {
        return _educationYearActive;
    }

    public void setEducationYearActive(boolean educationYearActive)
    {
        _educationYearActive = educationYearActive;
    }

    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    public void setEducationYear(EducationYear educationYear)
    {
        _educationYear = educationYear;
    }

    public List<EducationYear> getEducationYearList()
    {
        return _educationYearList;
    }

    public void setEducationYearList(List<EducationYear> educationYearList)
    {
        _educationYearList = educationYearList;
    }

    public boolean isFormativeOrgUnitActive()
    {
        return _formativeOrgUnitActive;
    }

    public void setFormativeOrgUnitActive(boolean formativeOrgUnitActive)
    {
        _formativeOrgUnitActive = formativeOrgUnitActive;
    }

    public List<OrgUnit> getFormativeOrgUnitList()
    {
        return _formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(List<OrgUnit> formativeOrgUnitList)
    {
        _formativeOrgUnitList = formativeOrgUnitList;
    }

    public ISelectModel getFormativeOrgUnitModel()
    {
        return _formativeOrgUnitModel;
    }

    public void setFormativeOrgUnitModel(ISelectModel formativeOrgUnitModel)
    {
        _formativeOrgUnitModel = formativeOrgUnitModel;
    }

    public boolean isTerritorialOrgUnitActive()
    {
        return _territorialOrgUnitActive;
    }

    public void setTerritorialOrgUnitActive(boolean territorialOrgUnitActive)
    {
        _territorialOrgUnitActive = territorialOrgUnitActive;
    }

    public List<OrgUnit> getTerritorialOrgUnitList()
    {
        return _territorialOrgUnitList;
    }

    public void setTerritorialOrgUnitList(List<OrgUnit> territorialOrgUnitList)
    {
        _territorialOrgUnitList = territorialOrgUnitList;
    }

    public ISelectModel getTerritorialOrgUnitModel()
    {
        return _territorialOrgUnitModel;
    }

    public void setTerritorialOrgUnitModel(ISelectModel territorialOrgUnitModel)
    {
        _territorialOrgUnitModel = territorialOrgUnitModel;
    }

    public boolean isDevelopFormActive()
    {
        return _developFormActive;
    }

    public void setDevelopFormActive(boolean developFormActive)
    {
        _developFormActive = developFormActive;
    }

    public List<DevelopForm> getDevelopFormList()
    {
        return _developFormList;
    }

    public void setDevelopFormList(List<DevelopForm> developFormList)
    {
        _developFormList = developFormList;
    }

    public ISelectModel getDevelopFormModel()
    {
        return _developFormModel;
    }

    public void setDevelopFormModel(ISelectModel developFormModel)
    {
        _developFormModel = developFormModel;
    }

    public boolean isCourseActive()
    {
        return _courseActive;
    }

    public void setCourseActive(boolean courseActive)
    {
        _courseActive = courseActive;
    }

    public List<Course> getCourseList()
    {
        return _courseList;
    }

    public void setCourseList(List<Course> courseList)
    {
        _courseList = courseList;
    }

    public ISelectModel getCourseModel()
    {
        return _courseModel;
    }

    public void setCourseModel(ISelectModel courseModel)
    {
        _courseModel = courseModel;
    }

    public boolean isStudentCategoryActive()
    {
        return _studentCategoryActive;
    }

    public void setStudentCategoryActive(boolean studentCategoryActive)
    {
        _studentCategoryActive = studentCategoryActive;
    }

    public List<StudentCategory> getStudentCategoryList()
    {
        return _studentCategoryList;
    }

    public void setStudentCategoryList(List<StudentCategory> studentCategoryList)
    {
        _studentCategoryList = studentCategoryList;
    }

    public ISelectModel getStudentCategoryModel()
    {
        return _studentCategoryModel;
    }

    public void setStudentCategoryModel(ISelectModel studentCategoryModel)
    {
        _studentCategoryModel = studentCategoryModel;
    }

    public boolean isStudentStatusActive()
    {
        return _studentStatusActive;
    }

    public void setStudentStatusActive(boolean studentStatusActive)
    {
        _studentStatusActive = studentStatusActive;
    }

    public List<StudentStatus> getStudentStatusList()
    {
        return _studentStatusList;
    }

    public void setStudentStatusList(List<StudentStatus> studentStatusList)
    {
        _studentStatusList = studentStatusList;
    }

    public ISelectModel getStudentStatusModel()
    {
        return _studentStatusModel;
    }

    public void setStudentStatusModel(ISelectModel studentStatusModel)
    {
        _studentStatusModel = studentStatusModel;
    }
}
