package ru.tandemservice.unisc.component.report;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unisc.component.report.PaymentReport.MQBuilderFactory;
import ru.tandemservice.unisc.entity.agreements.UniscEduMainAgreement;
import ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;

import java.util.List;

/**
 * @author oleyba
 * @since 14.08.2009
 */
public class UniscReportAddDaoUtil
{

    public static void initSelectModels(final UniscReportAddModel model, IUniBaseDao dao, Session session)
    {
        model.setFormativeOrgUnitModel(UniscReportAddDaoUtil.createFormativeOrgUnitAutocompleteModel(model, session));
        model.setTerritorialOrgUnitModel(UniscReportAddDaoUtil.createTerritorialOrgUnitAutocompleteModel(model, session));
        model.setEducationLevelHighSchoolModel(UniscReportAddDaoUtil.createEducationLevelsHighSchoolModel(model, session));
        model.setDevelopFormModel(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setDevelopConditionModel(EducationCatalogsManager.getDevelopConditionSelectModel());
        model.setDevelopTechModel(EducationCatalogsManager.getDevelopTechSelectModel());
        model.setDevelopPeriodModel(EducationCatalogsManager.getDevelopPeriodSelectModel());

        model.setGroupModel(new MQBuilderFactory.AutocompleteModel(session)
        {
            @Override
            public MQBuilderFactory getFactory()
            {
                return getGroupBuilder(model);
            }
        });

        model.setCourseModel(new LazySimpleSelectModel<>(DevelopGridDAO.getCourseList()));
    }

    public static MQBuilderFactory getAgreementBuilder(final UniscReportAddModel model)
    {
        return new MQBuilderFactory("a")
        {
            @Override
            public MQBuilder createBuilder()
            {
                MQBuilder builder = new MQBuilder(UniscEduMainAgreement.ENTITY_CLASS, "a");
                builder.addJoin("a", UniscEduMainAgreement.L_CONFIG, "config");
                builder.addJoin("config", UniscEduOrgUnit.L_EDUCATION_ORG_UNIT, "ou");
                UniscReportAddDaoUtil.appendEduOuConditions(builder, model, "ou");
                builder.addDomain("rel", UniscEduAgreement2Student.ENTITY_CLASS);
                builder.add(MQExpression.eqProperty("a", "id", "rel", UniscEduAgreement2Student.L_AGREEMENT + ".id"));
                if (model.isCourseActive())
                    builder.add(MQExpression.eq("rel", UniscEduAgreement2Student.L_STUDENT + "." + Student.L_COURSE, model.getCourse()));
                if (model.isGroupActive())
                    builder.add(MQExpression.eq("rel", UniscEduAgreement2Student.L_STUDENT + "." + Student.L_GROUP, model.getGroup()));
                return builder;
            }
        };
    }

    private static void appendEduOuConditions(MQBuilder builder, UniscReportAddModel model, String alias)
    {
        if (model.getOrgUnitId() != null)
        {
            builder.addLeftJoin(alias, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, "terr_ou");
            builder.add(MQExpression.or(
                MQExpression.eq(alias, EducationOrgUnit.L_FORMATIVE_ORG_UNIT + ".id", model.getOrgUnitId()),
                MQExpression.eq("terr_ou", "id", model.getOrgUnitId())
            ));
        }
        if (model.isFormativeOrgUnitActive())
            builder.add(MQExpression.in(alias, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnitList()));
        if (model.isTerritorialOrgUnitActive())
        {
            if (!model.getTerritorialOrgUnitList().isEmpty())
                builder.add(MQExpression.in(alias, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnitList()));
            else
                builder.add(MQExpression.isNull(alias, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT));
        }
        if (model.isEducationLevelHighSchoolActive())
            builder.add(MQExpression.in(alias, EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, model.getEducationLevelHighSchoolList()));
        if (model.isDevelopFormActive())
            builder.add(MQExpression.in(alias, EducationOrgUnit.L_DEVELOP_FORM, model.getDevelopFormList()));
        if (model.isDevelopConditionActive())
            builder.add(MQExpression.in(alias, EducationOrgUnit.L_DEVELOP_CONDITION, model.getDevelopConditionList()));
        if (model.isDevelopTechActive())
            builder.add(MQExpression.in(alias, EducationOrgUnit.L_DEVELOP_TECH, model.getDevelopTechList()));
        if (model.isDevelopPeriodActive())
            builder.add(MQExpression.in(alias, EducationOrgUnit.L_DEVELOP_PERIOD, model.getDevelopPeriodList()));
    }

    private static ISelectModel createFormativeOrgUnitAutocompleteModel(final UniscReportAddModel model, final Session session)
    {
        return new FullCheckSelectModel(OrgUnit.P_FULL_TITLE)
        {
            @Override
            public ListResult<OrgUnit> findValues(String filter)
            {
                Session session = DataAccessServices.dao().getComponentSession();
                MQBuilder ids = getIdsBuilder(EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model);
                return new ListResult<>(getBuilder(OrgUnit.ENTITY_CLASS, ids, filter).<OrgUnit>getResultList(session));
            }
        };
    }

    private static ISelectModel createTerritorialOrgUnitAutocompleteModel(final UniscReportAddModel model, final Session session)
    {
        return new FullCheckSelectModel(OrgUnit.P_FULL_TITLE)
        {
            @Override
            public ListResult<OrgUnit> findValues(String filter)
            {
                Session session = DataAccessServices.dao().getComponentSession();
                MQBuilder ids = getIdsBuilder(EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model);
                filter(ids, model.getSelectedFormativeOrgUnitList(), EducationOrgUnit.L_FORMATIVE_ORG_UNIT);
                return new ListResult<>(getBuilder(OrgUnit.ENTITY_CLASS, ids, filter).<OrgUnit>getResultList(session));
            }
        };
    }

    private static ISelectModel createEducationLevelsHighSchoolModel(final UniscReportAddModel model, final Session session)
    {
        return new FullCheckSelectModel(EducationLevelsHighSchool.P_FULL_TITLE)
        {
            @Override
            public ListResult<EducationLevelsHighSchool> findValues(String filter)
            {
                Session session = DataAccessServices.dao().getComponentSession();
                MQBuilder ids = getIdsBuilder(EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, model);
                filter(ids, model.getSelectedFormativeOrgUnitList(), EducationOrgUnit.L_FORMATIVE_ORG_UNIT);
                filter(ids, model.getSelectedTerritorialOrgUnitList(), EducationOrgUnit.L_TERRITORIAL_ORG_UNIT);
                return new ListResult<>(getBuilder(EducationLevelsHighSchool.ENTITY_CLASS, ids, filter).<EducationLevelsHighSchool>getResultList(session));
            }
        };
    }

    private static MQBuilderFactory getGroupBuilder(UniscReportAddModel model)
    {
        final MQBuilder groupBuilder = new MQBuilder(Group.ENTITY_CLASS, "g");
        groupBuilder.setNeedDistinct(true);
        groupBuilder.addOrder("g", Group.P_TITLE);
        groupBuilder.addDomain("r", UniscEduAgreement2Student.ENTITY_CLASS);
        groupBuilder.add(MQExpression.eqProperty("r", UniscEduAgreement2Student.L_STUDENT + "." + Student.L_GROUP + ".id", "g", "id"));
        groupBuilder.addJoin("g", Group.L_EDUCATION_ORG_UNIT, "ou");
        UniscReportAddDaoUtil.appendEduOuConditions(groupBuilder, model, "ou");
        if (model.isCourseActive())
            groupBuilder.add(MQExpression.eq("g", Group.L_COURSE, model.getCourse()));
        return MQBuilderFactory.create(groupBuilder, "g");
    }

    private static final String EDU_OU_ALIAS = "e";

    private static MQBuilder getIdsBuilder(String selectProperty, UniscReportAddModel model)
    {
        MQBuilder ids = new MQBuilder(EducationOrgUnit.ENTITY_CLASS, EDU_OU_ALIAS, new String[]{selectProperty + ".id"});
        ids.addDomain("a", UniscEduMainAgreement.ENTITY_CLASS);
        ids.add(MQExpression.eqProperty("a", UniscEduMainAgreement.L_CONFIG + "." + UniscEduOrgUnit.L_EDUCATION_ORG_UNIT + ".id", EDU_OU_ALIAS, "id"));
        if (null == model.getOrgUnitId())
            return ids;
        ids.addLeftJoin(EDU_OU_ALIAS, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, "terr_ou");
        ids.add(MQExpression.or(
            MQExpression.eq(EDU_OU_ALIAS, EducationOrgUnit.L_FORMATIVE_ORG_UNIT + ".id", model.getOrgUnitId()),
            MQExpression.eq("terr_ou", "id", model.getOrgUnitId())
        ));
        return ids;
    }

    private static void filter(MQBuilder builder, List selected, String property)
    {
        if (null == selected)
            return;
        if (selected.isEmpty())
            builder.add(MQExpression.isNull(EDU_OU_ALIAS, property));
        else
            builder.add(MQExpression.in(EDU_OU_ALIAS, property, selected));
    }

    private static MQBuilder getBuilder(String entityClass, MQBuilder ids, String filter)
    {
        MQBuilder builder = new MQBuilder(entityClass, "s");
        builder.addOrder("s", "title");
        if (StringUtils.isNotBlank(filter))
            builder.add(MQExpression.like("s", "title", "%" + filter));
        builder.add(MQExpression.in("s", "id", ids));
        return builder;
    }
}
