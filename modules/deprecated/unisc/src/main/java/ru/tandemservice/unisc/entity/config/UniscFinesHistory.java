package ru.tandemservice.unisc.entity.config;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.unisc.dao.payment.IUniscEduAgreementPaymentEvent;
import ru.tandemservice.unisc.entity.config.gen.UniscFinesHistoryGen;

import java.util.Date;

/**
 * Размер начисления штрафных пеней
 */
public class UniscFinesHistory extends UniscFinesHistoryGen implements ITitled, IUniscEduAgreementPaymentEvent.FinesTax {
    public static final DoubleFormatter TAX_FORMATTER = new DoubleFormatter(4);

    @Override
    @EntityDSLSupport(parts={UniscFinesHistoryGen.P_TAX})
    public String getTaxAsString() {
        return UniscFinesHistory.TAX_FORMATTER.format(getTax())+" %";
    }

    @Override
    @EntityDSLSupport(parts={UniscFinesHistoryGen.P_DATE})
    public String getDateAsString() {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(getDate());
    }

    @Override
    public String getTitle() {
        return "Начисление пеней от " + getDateAsString() + " в размере " + getTaxAsString() + " в день";
    }


    @Override public Date getEventDate() { return getDate(); }
    @Override public double getFinesTax() { return getTax(); }

}
