// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisc.component.report.GroupIncomeReport.GroupIncomeReportAdd;

import org.tandemframework.core.component.Input;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unisc.entity.report.UniscGroupIncomeReport;

/**
 * @author oleyba
 * @since 16.04.2010
 */
@Input(keys = "orgUnitId", bindings = "orgUnitId")
public class Model
{
    private UniscGroupIncomeReport _report = new UniscGroupIncomeReport();

    private Long orgUnitId;

    private ISelectModel _formativeOrgUnitModel;
    private ISelectModel _territorialOrgUnitModel;
    private ISelectModel _educationLevelHighSchoolModel;
    private ISelectModel _developFormModel;
    private ISelectModel _courseModel;
    private ISelectModel _groupModel;

    private OrgUnit _formativeOrgUnit;
    private OrgUnit _territorialOrgUnit;
    private EducationLevelsHighSchool _educationLevelHighSchool;
    private DevelopForm _developForm;
    private Course course;
    private Group group;

    public UniscGroupIncomeReport getReport()
    {
        return _report;
    }

    public void setReport(UniscGroupIncomeReport report)
    {
        _report = report;
    }

    public Long getOrgUnitId()
    {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        this.orgUnitId = orgUnitId;
    }

    public ISelectModel getFormativeOrgUnitModel()
    {
        return _formativeOrgUnitModel;
    }

    public void setFormativeOrgUnitModel(ISelectModel formativeOrgUnitModel)
    {
        _formativeOrgUnitModel = formativeOrgUnitModel;
    }

    public ISelectModel getTerritorialOrgUnitModel()
    {
        return _territorialOrgUnitModel;
    }

    public void setTerritorialOrgUnitModel(ISelectModel territorialOrgUnitModel)
    {
        _territorialOrgUnitModel = territorialOrgUnitModel;
    }

    public ISelectModel getEducationLevelHighSchoolModel()
    {
        return _educationLevelHighSchoolModel;
    }

    public void setEducationLevelHighSchoolModel(ISelectModel educationLevelHighSchoolModel)
    {
        _educationLevelHighSchoolModel = educationLevelHighSchoolModel;
    }

    public ISelectModel getDevelopFormModel()
    {
        return _developFormModel;
    }

    public void setDevelopFormModel(ISelectModel developFormModel)
    {
        _developFormModel = developFormModel;
    }

    public ISelectModel getCourseModel()
    {
        return _courseModel;
    }

    public void setCourseModel(ISelectModel courseModel)
    {
        _courseModel = courseModel;
    }

    public ISelectModel getGroupModel()
    {
        return _groupModel;
    }

    public void setGroupModel(ISelectModel groupModel)
    {
        _groupModel = groupModel;
    }

    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        _formativeOrgUnit = formativeOrgUnit;
    }

    public OrgUnit getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    public void setTerritorialOrgUnit(OrgUnit territorialOrgUnit)
    {
        _territorialOrgUnit = territorialOrgUnit;
    }

    public EducationLevelsHighSchool getEducationLevelHighSchool()
    {
        return _educationLevelHighSchool;
    }

    public void setEducationLevelHighSchool(EducationLevelsHighSchool educationLevelHighSchool)
    {
        _educationLevelHighSchool = educationLevelHighSchool;
    }

    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public void setDevelopForm(DevelopForm developForm)
    {
        _developForm = developForm;
    }

    public Course getCourse()
    {
        return course;
    }

    public void setCourse(Course course)
    {
        this.course = course;
    }

    public Group getGroup()
    {
        return group;
    }

    public void setGroup(Group group)
    {
        this.group = group;
    }
}
