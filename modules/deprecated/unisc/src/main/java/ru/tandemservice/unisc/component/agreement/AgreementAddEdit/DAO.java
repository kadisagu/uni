package ru.tandemservice.unisc.component.agreement.AgreementAddEdit;

import java.util.List;

import org.hibernate.Session;
import org.tandemframework.core.CoreExceptionUtils;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage.IAgreementOperation;
import ru.tandemservice.unisc.dao.agreement.IUniscEduAgreementDAO;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;

/**
 * @author vdanilov
 */
@SuppressWarnings("deprecation")
public abstract class DAO<M extends Model> extends UniBaseDao implements IDAO<M> {

    @Override
    public void prepare(final M model) {
        reassociate(model);
        model.changeCreationFormFlag(null == model.getAgreement().getId());
    }

    @Override
    public void execute(final M model) {
        if (null != model.getResult()) {
            try {
                model.getResult().execute(new IAgreementOperation() {
                    @Override public UniscEduAgreementBase update(final UniscEduAgreementBase agreement, final boolean save) {
                        model.getAgreement().update(agreement);
                        if (save) { save(model); }
                        else { reassociate(model); }
                        return model.getAgreement();
                    }
                });
            } finally {
                model.setResult(null);
                this.refreshAgreementAfterStageExecution(model);
            }
        }
    }

    @Override
    public void save(final M model) {
        reassociate(model);
        try {
            IUniscEduAgreementDAO.INSTANCE.get().doSaveAgreementWithNumberCheck(model.getAgreement());
        } catch (Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    protected void reassociate(final M model) {
        final Session session = getSession();
        if (null != model.getAgreement().getId()) { session.update(model.getAgreement()); }
        reassociateEntityProperties(session, model.getAgreement());
    };


    protected void refreshAgreementAfterStageExecution(final M model) {
        final UniscEduAgreementBase agreement = model.getAgreement();
        final UniscEduOrgUnit config = agreement.getConfig();

        /* сроки освоения */ {
            if (null == agreement.getDevelopPeriodEduPlan()) {
                agreement.setDevelopPeriodEduPlan(null == config ? null : config.getEducationOrgUnit().getDevelopPeriod());
            }
            if (null == agreement.getDevelopPeriodGos()) {
                agreement.setDevelopPeriodGos(null == config ? null : config.getEducationOrgUnit().getDevelopPeriod());
            }
        }
    }

    @Override
    public List<UniscEduAgreementNaturalPerson> getDefaultNaturalPersonList(final M model) {
        return null;
    };
}
