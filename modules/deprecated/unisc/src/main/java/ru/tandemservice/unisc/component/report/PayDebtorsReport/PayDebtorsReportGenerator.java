package ru.tandemservice.unisc.component.report.PayDebtorsReport;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.hibernate.Session;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unisc.UniscDefines;
import ru.tandemservice.unisc.component.report.PaymentReport.MQBuilderFactory;
import ru.tandemservice.unisc.component.report.UniscRtfUtils;
import ru.tandemservice.unisc.dao.payment.IUniscEduAgreementPaymentDAO;
import ru.tandemservice.unisc.dao.payment.IUniscEduAgreementPaymentState;
import ru.tandemservice.unisc.entity.agreements.UniscEduMainAgreement;
import ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student;
import ru.tandemservice.unisc.entity.catalog.UniscTemplateDocument;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;

import java.util.*;

/**
 * @author oleyba
 * @since 26.08.2009
 */
public class PayDebtorsReportGenerator
{
    private Session session;
    private MQBuilderFactory agreementBuilder;
    private Long orgUnitId;
    protected Date dateFrom;
    private Date dateTo;

    Map<Long, IUniscEduAgreementPaymentState> paymentStateMap;
    private List<UniscEduMainAgreement> agreementList = new ArrayList<>();
    private HashMap<Long, Student> studentMap = new HashMap<>();
    protected List<OrgUnit> formativeOrgUnitList = new ArrayList<>();
    protected List<OrgUnit> territorialOrgUnitList = new ArrayList<>();
    private Map<MultiKey, ReportRow> rowMap = new HashMap<>();
    private Map<MultiKey, List<ReportRow>> reportMap = new HashMap<MultiKey, List<ReportRow>>()
    {
        private static final long serialVersionUID = 1L;

        @Override
        public List<ReportRow> get(Object key)
        {
            List<ReportRow> rows = super.get(key);
            if (rows == null)
                put((MultiKey) key, rows = new ArrayList<>());
            return rows;
        }
    };

    public PayDebtorsReportGenerator(Session session, MQBuilderFactory agreementBuilder, Long orgUnitId, Date dateTo, Date dateFrom)
    {
        this.session = session;
        this.agreementBuilder = agreementBuilder;
        this.orgUnitId = orgUnitId;

        Calendar beginDay = Calendar.getInstance();
        beginDay.setTime(dateFrom);
        beginDay.set(Calendar.HOUR_OF_DAY, 0);
        beginDay.set(Calendar.MINUTE, 0);
        beginDay.set(Calendar.SECOND, 0);
        beginDay.set(Calendar.MILLISECOND, 0);

        Calendar endDay = Calendar.getInstance();
        endDay.setTime(dateTo);
        endDay.set(Calendar.HOUR_OF_DAY, 23);
        endDay.set(Calendar.MINUTE, 59);
        endDay.set(Calendar.SECOND, 59);
        endDay.set(Calendar.MILLISECOND, 999);

        this.dateFrom = beginDay.getTime();
        this.dateTo = endDay.getTime();
    }

    public byte[] generateReportContent()
    {
        ITemplateDocument templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniscTemplateDocument.class, UniscDefines.TEMPLATE_PAY_DEBTORS_REPORT);
        RtfDocument document = new RtfReader().read(templateDocument.getContent());

        init();
        processAgreements();

        final List<String[]> reportTable = new ArrayList<>();
        double debt = 0;
        double prevDebt = 0;
        for (OrgUnit formativeOrgUnit : formativeOrgUnitList)
            for (OrgUnit territorialOrgUnit : territorialOrgUnitList)
            {
                MultiKey key = new MultiKey(formativeOrgUnit.getId(), territorialOrgUnit == null ? null : territorialOrgUnit.getId());
                List<ReportRow> rowList = reportMap.get(key);
                if (rowList == null)
                    continue;
                String caption = getGroupCaption(formativeOrgUnit, territorialOrgUnit);
                reportTable.add(new String[]{caption});
                double groupDebt = 0;
                double groupPrevDebt = 0;
                for (ReportRow row : rowList)
                {
                    row.append(reportTable);
                    groupDebt = groupDebt + row.getDebt();
                    groupPrevDebt = groupPrevDebt + row.getPrevDebt();
                }
                reportTable.add(new String[]{"ИТОГО", "", "", "", "", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(groupDebt), DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(groupPrevDebt)});
                debt = debt + groupDebt;
                prevDebt = prevDebt + groupPrevDebt;
            }

        modify(document, reportTable, debt, prevDebt);

        return RtfUtil.toByteArray(document);
    }

    private void init()
    {
        MQBuilder builder = new MQBuilder(UniscEduAgreement2Student.ENTITY_CLASS, "rel");
        builder.addLeftJoinFetch("rel", UniscEduAgreement2Student.L_STUDENT, "student_fetch");
        builder.addLeftJoinFetch("student_fetch", Student.L_GROUP, "group_fetch");
        builder.addLeftJoinFetch("student_fetch", Student.L_COURSE, "course_fetch");
        builder.addLeftJoinFetch("student_fetch", Student.L_PERSON, "person_fetch");
        builder.addLeftJoinFetch("person_fetch", Person.L_IDENTITY_CARD, "idc_fetch");
        builder.addLeftJoinFetch("rel", UniscEduAgreement2Student.L_AGREEMENT, "agr_fetch");
        builder.addLeftJoinFetch("agr_fetch", UniscEduMainAgreement.L_CONFIG, "conf_fetch");
        builder.addLeftJoinFetch("conf_fetch", UniscEduOrgUnit.L_EDUCATION_ORG_UNIT, "agr_edu_ou_fetch");
        builder.addLeftJoinFetch("agr_edu_ou_fetch", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "agr_hs_fetch");
        builder.addLeftJoinFetch("agr_hs_fetch", EducationLevelsHighSchool.L_EDUCATION_LEVEL, "agr_lev_fetch");
        builder.add(MQExpression.in("rel", UniscEduAgreement2Student.L_AGREEMENT + ".id", agreementBuilder.createIdBuilder()));


        // убрали условие о том, что должен быть долг в период построения отчета
        /*
        builder.addDomain("add", UniscEduAdditAgreement.ENTITY_CLASS);
        builder.addDomain("pay", UniscEduAgreementPayPlanRow.ENTITY_CLASS);
        builder.add(MQExpression.or(
                MQExpression.eqProperty("pay", UniscEduAgreementPayPlanRow.L_OWNER + ".id", "rel", UniscEduAgreement2Student.L_AGREEMENT + ".id"),
                MQExpression.and(
                        MQExpression.eqProperty("pay", UniscEduAgreementPayPlanRow.L_OWNER + ".id", "add", "id"),
                        MQExpression.eqProperty("add", UniscEduAdditAgreement.L_AGREEMENT + ".id", "rel", UniscEduAgreement2Student.L_AGREEMENT + ".id"))));

        builder.add(UniMQExpression.betweenDate("pay", UniscEduAgreementPayPlanRow.P_DATE, dateFrom, dateTo));
        builder.add(MQExpression.isNull("pay", UniscEduAgreementPayPlanRow.L_REPLACED_BY));
         */

        // добавили условие о том, что студенты и группы неархивные, чтобы не считать графики по всем договорам с сотворения мира
        builder.add(MQExpression.eq("student_fetch", Student.P_ARCHIVAL, Boolean.FALSE));
        builder.add(MQExpression.or(MQExpression.isNull("student_fetch", Student.L_GROUP),
                MQExpression.eq("group_fetch", Group.P_ARCHIVAL, Boolean.FALSE)));


        for (UniscEduAgreement2Student rel : builder.<UniscEduAgreement2Student>getResultList(session))
        {
            studentMap.put(rel.getAgreement().getId(), rel.getStudent());
            agreementList.add(rel.getAgreement());
        }

        MQBuilder builder1 = agreementBuilder.createBuilder();
        builder1.getSelectAliasList().clear();
        builder1.addSelect(agreementBuilder.getAlias(), new Object[]{"id"});
        paymentStateMap = IUniscEduAgreementPaymentDAO.INSTANCE.get().getStateMap(builder1.<Long>getResultList(session), dateTo);
    }

    private void processAgreements()
    {
        Set<OrgUnit> formativeOrgUnits = new HashSet<>();
        Set<OrgUnit> territorialOrgUnits = new HashSet<>();
        for (UniscEduMainAgreement agreement: agreementList)
        {
            Student student = studentMap.get(agreement.getId());
            EducationOrgUnit eduOu = agreement.getConfig().getEducationOrgUnit();
            Long formativeId = eduOu.getFormativeOrgUnit().getId();
            Long territorialId = eduOu.getTerritorialOrgUnit().getId();
            MultiKey rowKey = new MultiKey(student.getId(), formativeId, territorialId);
            ReportRow row = rowMap.get(rowKey);
            if (row == null)
                rowMap.put(rowKey, row = new ReportRow(student));
            IUniscEduAgreementPaymentState paymentState = paymentStateMap.get(agreement.getId());
            if (paymentState != null)
                for (IUniscEduAgreementPaymentState.StatePlanRow planRow : paymentState.getPlanRows())
                    row.process(planRow);
            if (row.getDebt() + row.getPrevDebt() > 0)
            {
                reportMap.get(new MultiKey(formativeId, territorialId)).add(row);
                formativeOrgUnits.add(eduOu.getFormativeOrgUnit());
                territorialOrgUnits.add(eduOu.getTerritorialOrgUnit());
            }
        }

        formativeOrgUnitList = new ArrayList<>(formativeOrgUnits);
        formativeOrgUnitList.sort(CommonCollator.TITLED_WITH_ID_COMPARATOR);
        territorialOrgUnitList = new ArrayList<>(territorialOrgUnits);
        territorialOrgUnitList.sort(CommonCollator.TITLED_WITH_ID_COMPARATOR);

        for (List<ReportRow> rowList : reportMap.values())
            Collections.sort(rowList);
    }

    private void modify(RtfDocument document, final List<String[]> reportTable, double debt, double prevDebt)
    {
        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T", reportTable.toArray(new String[reportTable.size()][]));
        tableModifier.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                UniscRtfUtils.afterModify(newRowList, startIndex, reportTable);
            }
        }
        );
        tableModifier.modify(document);

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("periodReport", DateFormatter.DEFAULT_DATE_FORMATTER.format(dateFrom) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(dateTo));
        OrgUnit orgUnit = orgUnitId == null ? null : UniDaoFacade.getCoreDao().get(OrgUnit.class, orgUnitId);
        injectModifier.put("scope", orgUnit == null ? "ИТОГО по всему ОУ" : "ИТОГО по поздразделению (" + (orgUnit.getNominativeCaseTitle() == null ? orgUnit.getTitle() : orgUnit.getNominativeCaseTitle()) + ")");
        injectModifier.put("total", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(debt));
        injectModifier.put("totalDebt", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(prevDebt));
        injectModifier.modify(document);
    }

    private String getGroupCaption(OrgUnit formativeOrgUnit, OrgUnit territorialOrgUnit)
    {
        String caption = "Подразделение: " + (formativeOrgUnit.getNominativeCaseTitle() == null ? formativeOrgUnit.getTitle() : formativeOrgUnit.getNominativeCaseTitle());
        if (null != territorialOrgUnit)
            caption = caption + " (" + (territorialOrgUnit.getNominativeCaseTitle() == null ? territorialOrgUnit.getTitle() : territorialOrgUnit.getNominativeCaseTitle()) + ")";
        return caption;
    }

    @SuppressWarnings({"unchecked", "unused"})
    private class ReportRow implements Comparable
    {
        private Student student;
        private double sum;
        private double debt;
        private double prevDebt;

        private ReportRow(Student student)
        {
            this.student = student;
        }

        public void process(IUniscEduAgreementPaymentState.StatePlanRow planRow)
        {
            if (planRow.getDate().after(dateTo))
                return;
            if (planRow.getDate().before(dateFrom))
            {
                prevDebt = prevDebt + planRow.getDebt()[0];
                return;
            }
            sum = sum + planRow.getCost();
            debt = debt + planRow.getDebt()[0];
        }

        public void append(List<String[]> reportTable)
        {
            reportTable.add(new String[]{
                    student.getPerson().getFullFio(),
                    student.getCourse().getTitle(),
                    student.getGroup() == null ? "" : student.getGroup().getTitle(),
                            DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(sum),
                            DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(sum - debt),
                            DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(debt),
                            DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(prevDebt)
            });
        }

        @Override
        public int compareTo(Object o)
        {
            if (this == o) { return 0; }
            final ReportRow other = (ReportRow) o;
            int i;
            if (0 != (i = Course.COURSE_COMPARATOR.compare(student.getCourse(), other.student.getCourse()))) { return i; }
            if (0 != (i = CommonCollator.TITLED_COMPARATOR.compare(student.getGroup(), other.student.getGroup()))) { return i; }
            return Student.FULL_FIO_AND_ID_COMPARATOR.compare(student, other.student);
        }

        public Student getStudent() { return student; }
        public double getDebt() { return debt; }
        public double getPrevDebt() { return prevDebt; }
    }
}
