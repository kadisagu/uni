package ru.tandemservice.unisc.component.settings.CostSettings;

import com.google.common.collect.ImmutableList;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.IEducationLevelModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;

import java.util.List;

public class Model implements IEducationLevelModel {


	private IDataSettings _settings;
	public IDataSettings getSettings() { return this._settings; }
	public void setSettings(final IDataSettings settings) { this._settings = settings; }

	private ISelectModel years = null;
	public ISelectModel getYears() { return this.years; }
	public void setYears(final ISelectModel years) { this.years = years; }

	private ISelectModel cipherModel;
	public ISelectModel getCipherModel() { return this.cipherModel; }
	public void setCipherModel(final ISelectModel cipherModel) { this.cipherModel = cipherModel; }

	private ISelectModel _formativeOrgUnitModel;
	private ISelectModel _territorialOrgUnitModel;
	private ISelectModel _educationLevelsHighSchoolModel;
	private ISelectModel _developFormModel;
	private ISelectModel _developTechModel;
	private ISelectModel _developConditionModel;
	private ISelectModel _developPeriodModel;

	public ISelectModel getFormativeOrgUnitModel() { return this._formativeOrgUnitModel; }
	public void setFormativeOrgUnitModel(final ISelectModel formativeOrgUnitModel) { this._formativeOrgUnitModel = formativeOrgUnitModel; }

	public ISelectModel getTerritorialOrgUnitModel() { return this._territorialOrgUnitModel; }
	public void setTerritorialOrgUnitModel(final ISelectModel territorialOrgUnitModel) { this._territorialOrgUnitModel = territorialOrgUnitModel; }

	public ISelectModel getEducationLevelsHighSchoolModel() { return this._educationLevelsHighSchoolModel; }
	public void setEducationLevelsHighSchoolModel(final ISelectModel educationLevelsHighSchoolModel) { this._educationLevelsHighSchoolModel = educationLevelsHighSchoolModel; }

	public ISelectModel getDevelopFormModel() { return this._developFormModel; }
	public void setDevelopFormModel(final ISelectModel developFormModel) { this._developFormModel = developFormModel; }

	public ISelectModel getDevelopTechModel() { return this._developTechModel; }
	public void setDevelopTechModel(final ISelectModel developTechModel) { this._developTechModel = developTechModel; }

	public ISelectModel getDevelopConditionModel() { return this._developConditionModel; }
	public void setDevelopConditionModel(final ISelectModel developConditionModel) { this._developConditionModel = developConditionModel; }

	public ISelectModel getDevelopPeriodModel() { return this._developPeriodModel; }
	public void setDevelopPeriodModel(final ISelectModel developPeriodModel) { this._developPeriodModel = developPeriodModel; }

	private static final List<IdentifiableWrapper<IEntity>> stateList;
	public List<IdentifiableWrapper<IEntity>> getStateList() { return Model.stateList; }

	public static final long STATE_NOT_CONFIGURE = 1L;
	public static final long STATE_CONFIGURE = 2L;

	static {
		stateList = ImmutableList.of(new IdentifiableWrapper<>(Model.STATE_NOT_CONFIGURE, "Не заполнены"),
                                     new IdentifiableWrapper<>(Model.STATE_CONFIGURE, "Заполнены"));
	}


	private DynamicListDataSource<UniscEduOrgUnit> dataSource;
	public DynamicListDataSource<UniscEduOrgUnit> getDataSource() { return this.dataSource; }
	public void setDataSource(final DynamicListDataSource<UniscEduOrgUnit> dataSource) { this.dataSource = dataSource; }


	public EducationYear getEducationYear() { return (EducationYear) this.getSettings().get("educationYear"); }
	protected void setEducationYear(final EducationYear year) { this.getSettings().set("educationYear", year); }

	public UniscEduOrgUnit getCipher() { return (UniscEduOrgUnit) this.getSettings().get("cipher"); }
	protected void setCipher(final UniscEduOrgUnit cipher) { this.getSettings().set("cipher", cipher); }

	@Override public OrgUnit getFormativeOrgUnit() { return (OrgUnit) this.getSettings().get("formativeOrgUnit"); }
	@Override public OrgUnit getTerritorialOrgUnit() { return (OrgUnit) this.getSettings().get("territorialOrgUnit"); }
	@Override public EducationLevelsHighSchool getEducationLevelsHighSchool() { return (EducationLevelsHighSchool) this.getSettings().get("eduLevelHighSchool"); }
	@Override public DevelopCondition getDevelopCondition() { return (DevelopCondition) this.getSettings().get("developCondition"); }
	@Override public DevelopForm getDevelopForm() { return (DevelopForm) this.getSettings().get("developForm"); }
	@Override public DevelopPeriod getDevelopPeriod() { return (DevelopPeriod) this.getSettings().get("developPeriod"); }
	@Override public DevelopTech getDevelopTech() { return (DevelopTech) this.getSettings().get("developTech"); }


}
