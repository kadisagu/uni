package ru.tandemservice.unisc.entity.catalog;

import java.util.HashMap;
import java.util.Map;

import ru.tandemservice.unisc.UniscDefines;
import ru.tandemservice.unisc.entity.catalog.gen.UniscPayPeriodGen;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;

/**
 * Период оплаты
 */
public class UniscPayPeriod extends UniscPayPeriodGen {
    protected static final Map<String, UniscPayPeriod.Freq> CODE_2_PERIOD = new HashMap<String, UniscPayPeriod.Freq>();
    static {
        UniscPayPeriod.CODE_2_PERIOD.put(UniscDefines.PAY_PERIOD_TOTAL, UniscPayPeriod.Freq.TOTAL);
        UniscPayPeriod.CODE_2_PERIOD.put(UniscDefines.PAY_PERIOD_YEAR, UniscPayPeriod.Freq.YEAR);
        UniscPayPeriod.CODE_2_PERIOD.put(UniscDefines.PAY_PERIOD_TERM, UniscPayPeriod.Freq.TERM);
        UniscPayPeriod.CODE_2_PERIOD.put(UniscDefines.PAY_PERIOD_HALF_TERM, UniscPayPeriod.Freq.HALF_TERM);
    }

    private transient UniscPayPeriod.Freq _cache;
    public UniscPayPeriod.Freq freq() {
        if (null != this._cache) { return this._cache; }
        return (this._cache = UniscPayPeriod.CODE_2_PERIOD.get(this.getCode()));
    }


    public int getStagesCount(final UniscEduOrgUnit eduOrgUnit) { return this.freq().count(eduOrgUnit); }
    public int getLastStage(final UniscEduOrgUnit eduOrgUnit, final UniscPayPeriod duration, final int stage) { return this.freq().lastStage(eduOrgUnit, duration.freq(), stage); }

    public int getCourseNumber(final UniscEduOrgUnit eduOrgUnit, final int stage) { return this.freq().courseNumber(eduOrgUnit, stage); }
    public int getTermNumber(final UniscEduOrgUnit eduOrgUnit, final int stage) { return this.freq().termNumber(eduOrgUnit, stage); }
    public int getPartNumber(final UniscEduOrgUnit eduOrgUnit, final int stage) { return this.freq().partNumber(eduOrgUnit, stage); }

    public String toString(final UniscEduOrgUnit eduOrgUnit, final int stage) { return this.freq().format(eduOrgUnit, stage); }


    public static enum Freq {
        TOTAL {
            @Override public int courseNumber(final UniscEduOrgUnit eduOrgUnit, final int stage) { return 1; }
            @Override public int termNumber(final UniscEduOrgUnit eduOrgUnit, final int stage) { return 1; }
            @Override public int partNumber(final UniscEduOrgUnit eduOrgUnit, final int stage) { return 1; }

            @Override public int count(final UniscEduOrgUnit eduOrgUnit) {
                return 1;
            }
            @Override public String format(final UniscEduOrgUnit eduOrgUnit, final int stage) {
                return "весь срок";
            }
        },
        YEAR {
            @Override public int courseNumber(final UniscEduOrgUnit eduOrgUnit, final int stage) { return (1+stage); }
            @Override public int termNumber(final UniscEduOrgUnit eduOrgUnit, final int stage) { return 1; }
            @Override public int partNumber(final UniscEduOrgUnit eduOrgUnit, final int stage) { return 1; }

            @Override public int count(final UniscEduOrgUnit eduOrgUnit) {
                return eduOrgUnit.getEducationOrgUnit().getDevelopPeriod().getLastCourse();
            }
            @Override public String format(final UniscEduOrgUnit eduOrgUnit, final int stage) {
                return (this.courseNumber(eduOrgUnit, stage) + " курс");
            }
        },
        TERM {
            @Override public int courseNumber(final UniscEduOrgUnit eduOrgUnit, final int stage) {
                final int countTerm = eduOrgUnit.getCountTerm();
                return (1 + (stage / countTerm));
            }
            @Override public int termNumber(final UniscEduOrgUnit eduOrgUnit, final int stage) {
                final int countTerm = eduOrgUnit.getCountTerm();
                return (1 + (stage % countTerm));
            }
            @Override public int partNumber(final UniscEduOrgUnit eduOrgUnit, final int stage) { return 1; }

            @Override public int count(final UniscEduOrgUnit eduOrgUnit) {
                return eduOrgUnit.getEducationOrgUnit().getDevelopPeriod().getLastCourse() * (eduOrgUnit.getCountTerm());
            }
            @Override public String format(final UniscEduOrgUnit eduOrgUnit, final int stage) {
                return (this.termNumber(eduOrgUnit, stage) + " семестр " + this.courseNumber(eduOrgUnit, stage) + " курса");
            }
        },
        HALF_TERM {
            @Override public int courseNumber(final UniscEduOrgUnit eduOrgUnit, final int stage) {
                final int countTerm = eduOrgUnit.getCountTerm();
                return (1 + (stage / (2*countTerm)));
            }
            @Override public int termNumber(final UniscEduOrgUnit eduOrgUnit, final int stage) {
                final int countTerm = eduOrgUnit.getCountTerm();
                return (1 + ((stage/2) % countTerm));
            }
            @Override public int partNumber(final UniscEduOrgUnit eduOrgUnit, final int stage) {
                return (1 + (stage%2));
            }

            @Override public int count(final UniscEduOrgUnit eduOrgUnit) {
                return eduOrgUnit.getEducationOrgUnit().getDevelopPeriod().getLastCourse() * (2*eduOrgUnit.getCountTerm());
            }
            @Override public String format(final UniscEduOrgUnit eduOrgUnit, final int stage) {
                return (this.partNumber(eduOrgUnit, stage) + " часть " + this.termNumber(eduOrgUnit, stage) + " семестра " + this.courseNumber(eduOrgUnit, stage) + " курса");
            }
        };

        public abstract int count(UniscEduOrgUnit eduOrgUnit);
        public int lastStage(final UniscEduOrgUnit eduOrgUnit, final Freq duration, final int stage) {
            if (TOTAL.equals(duration)) {
                return (this.count(eduOrgUnit)-1);
            } else if (YEAR.equals(duration)) {
                final int c = this.courseNumber(eduOrgUnit, stage);
                int s = stage;
                while (c == this.courseNumber(eduOrgUnit, s)) { s++; }
                return (s-1);
            } else if (TERM.equals(duration)) {
                final int c = this.courseNumber(eduOrgUnit, stage);
                final int t = this.termNumber(eduOrgUnit, stage);
                int s = stage;
                while ((c == this.courseNumber(eduOrgUnit, s)) && (t == this.termNumber(eduOrgUnit, s))) { s++; }
                return (s-1);
            } else if (HALF_TERM.equals(duration)) {
                final int c = this.courseNumber(eduOrgUnit, stage);
                final int t = this.termNumber(eduOrgUnit, stage);
                final int p = this.partNumber(eduOrgUnit, stage);
                int s = stage;
                while ((c == this.courseNumber(eduOrgUnit, s)) && (t == this.termNumber(eduOrgUnit, s)) && (p == this.partNumber(eduOrgUnit, s))) { s++; }
                return (s-1);
            }
            return stage;
        }

        public abstract String format(UniscEduOrgUnit eduOrgUnit, int stage);

        public abstract int courseNumber(UniscEduOrgUnit eduOrgUnit, int stage);
        public abstract int termNumber(UniscEduOrgUnit eduOrgUnit, int stage);
        public abstract int partNumber(UniscEduOrgUnit eduOrgUnit, int stage);

        public int index(final UniscEduOrgUnit eduOrgUnit, final int stage) {
            final int c = courseNumber(eduOrgUnit, stage);
            final int t = termNumber(eduOrgUnit, stage);
            final int p = partNumber(eduOrgUnit, stage);
            return p + 2*(t + 3*c);
        }


    }


}
