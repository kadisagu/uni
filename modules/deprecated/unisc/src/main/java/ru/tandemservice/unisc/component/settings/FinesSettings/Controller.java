package ru.tandemservice.unisc.component.settings.FinesSettings;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unisc.entity.config.UniscFinesHistory;

import java.util.Collections;


public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(final IBusinessComponent component) {
        final Model model = getModel(component);
        model.setSettings(UniBaseUtils.getDataSettings(component, "FinesSettings.filter"));
        getDao().prepare(model);

        if (null == model.getDataSource()) {
            final DynamicListDataSource<UniscFinesHistory> ds = new DynamicListDataSource<>(component, component1 -> {
                getDao().refreshDataSource(model);
            });
            ds.addColumn(new SimpleColumn("Дата изменения размера ставки", UniscFinesHistory.P_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setWidth(20).setOrderable(true).setClickable(false));
            ds.addColumn(new SimpleColumn("Процент для расчета пеней (в день)", UniscFinesHistory.P_TAX, UniscFinesHistory.TAX_FORMATTER).setOrderable(false).setClickable(false));
            ds.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit"));
            ds.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "В результате удаления настройки по всем договорам от {0} произойдет перерасчет задолжности! Вы уверены, что хотите удалить настройку?", "dateAsString"));
            model.setDataSource(ds);
        }
    }

    public void onClickEdit(final IBusinessComponent component) {
        component.createDefaultChildRegion(new ComponentActivator(Controller.class.getPackage().getName() + ".AddEdit",
                                                         Collections.singletonMap(PublisherActivator.PUBLISHER_ID_KEY, component.getListenerParameter())
        ));
    }

    public void onClickAdd(final IBusinessComponent component) {
        component.createDefaultChildRegion(new ComponentActivator(Controller.class.getPackage().getName() + ".AddEdit",
                                                         Collections.singletonMap(PublisherActivator.PUBLISHER_ID_KEY, null)
        ));
    }

    public void onClickDelete(final IBusinessComponent component) {
        getDao().delete(getModel(component), (Long)component.getListenerParameter());
    }

    public void onClickSearch(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        getDao().prepare(model);
        model.getDataSource().refresh();
        DataSettingsFacade.saveSettings(model.getSettings());
    }

    public void onClickClear(final IBusinessComponent component) {
        this.getModel(component).getSettings().clear();
        this.onClickSearch(component);
    }
}
