package ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage0;

import org.apache.tapestry.form.validator.Required;
import org.apache.tapestry.form.validator.Validator;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.uni.dao.EducationLevelModel;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.ui.SelectModel;
import ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage.AbstractStageModel;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;
import ru.tandemservice.unisc.utils.UniscPropertyUtils;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key="defaultEducationYearId", binding="defaultEducationYearId"),
    @Bind(key="defaultUniscEduOrgUnitId", binding="defaultUniscEduOrgUnitId"),
    @Bind(key="defaultEducationOrgUnitId", binding="defaultEducationOrgUnitId")
})
@SuppressWarnings("deprecation")
public class Model extends AbstractStageModel {

    public boolean isCipherRequired() {
        return UniscPropertyUtils.STRICT_CIPHER;
    }
    public Validator getCipherValidator() {
        if (isCipherRequired()) {
            return new Required() {
                // нужно, чтобы я мог выбирать пустые значения
                @Override public boolean isRequired() { return false; }
            };
        }
        return null;
    }

    private final SelectModel<EducationYear> educationYearModel = new SelectModel<EducationYear>();
    public SelectModel<EducationYear> getEducationYearModel() { return this.educationYearModel; }
    public void setDefaultEducationYearId(final Long id) {
        getEducationYearModel().setValueId(id);
    }

    private final SelectModel<UniscEduOrgUnit> cipherModel = new SelectModel<UniscEduOrgUnit>();
    public SelectModel<UniscEduOrgUnit> getCipherModel() { return this.cipherModel; }
    public void setDefaultUniscEduOrgUnitId(final Long id) {
        getCipherModel().setValueId(id);
    }

    private final EducationLevelModel educationLevelModel = new EducationLevelModel();
    public EducationLevelModel getEducationLevelModel() { return this.educationLevelModel; }
    public void setDefaultEducationOrgUnitId(final Long id) {
        if (null != id) {
            final EducationOrgUnit edu = UniDaoFacade.getCoreDao().get(EducationOrgUnit.class, id);
            this.educationLevelModel.fromEducationOrgUnit(edu, true);
        }
    }
}
