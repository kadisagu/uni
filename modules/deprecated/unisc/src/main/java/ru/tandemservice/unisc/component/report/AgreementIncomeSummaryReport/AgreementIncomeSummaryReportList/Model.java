// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisc.component.report.AgreementIncomeSummaryReport.AgreementIncomeSummaryReportList;

import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unisc.component.report.UniscReportSecModel;
import ru.tandemservice.unisc.entity.report.UniscAgreementIncomeSummaryReport;

/**
 * @author oleyba
 * @since 07.09.2009
 */
@State(keys = "orgUnitId", bindings = "orgUnitId")
public class Model extends UniscReportSecModel
{
    private Long orgUnitId;
    private OrgUnit orgUnit;
    private DynamicListDataSource<UniscAgreementIncomeSummaryReport> _dataSource;

    @Override
    public String getViewKey()
    {
        return null == getOrgUnit() ? "scAgreementIncomeSummaryReport" : getSecModel().getPermission("viewAgreementIncomeSummaryReport");
    }

    public Long getOrgUnitId()
    {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        this.orgUnitId = orgUnitId;
    }

    public DynamicListDataSource<UniscAgreementIncomeSummaryReport> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<UniscAgreementIncomeSummaryReport> dataSource)
    {
        _dataSource = dataSource;
    }

    @Override
    public OrgUnit getOrgUnit()
    {
        return orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        this.orgUnit = orgUnit;
    }
}
