// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisc.component.report.AgreementIncomeSummaryReport.AgreementIncomeSummaryReportPub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.sec.runtime.SecurityRuntime;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unisc.component.report.UniscReportSecModel;
import ru.tandemservice.unisc.entity.report.UniscAgreementIncomeSummaryReport;

/**
 * @author oleyba
 * @since 07.09.2009
 */
@State(keys = PublisherActivator.PUBLISHER_ID_KEY, bindings = "report.id")
public class Model extends UniscReportSecModel
{
    private UniscAgreementIncomeSummaryReport _report = new UniscAgreementIncomeSummaryReport();

    @Override
    public OrgUnit getOrgUnit()
    {
        return getReport().getOrgUnit();
    }

    @Override
    public String getViewKey()
    {
        return null == getOrgUnit() ? "scAgreementIncomeSummaryReport" : getSecModel().getPermission("viewAgreementIncomeSummaryReport");
    }

    public UniscAgreementIncomeSummaryReport getReport()
    {
        return _report;
    }

    public void setReport(UniscAgreementIncomeSummaryReport report)
    {
        _report = report;
    }

    public boolean isNotEmpty(String property)
    {
        return StringUtils.isNotEmpty(property);
    }

    @Override
    public Object getSecuredObject()
    {
        return getReport().getOrgUnit() == null ? SecurityRuntime.getInstance().getCommonSecurityObject() : getReport().getOrgUnit();
    }
}
