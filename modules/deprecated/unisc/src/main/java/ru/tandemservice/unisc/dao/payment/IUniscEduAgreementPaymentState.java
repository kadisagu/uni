package ru.tandemservice.unisc.dao.payment;

import java.util.Collection;
import java.util.Date;

/**
 * @author vdanilov
 */
public interface IUniscEduAgreementPaymentState {

    /** описание пеней */
    interface StateFinesRow {

        /** ставка */
        double getFinesTax();

        /** число дней */
        int getDays();

        /** задолжность */
        double getDebt();

        /** пеней */
        double getFines();

    }

    /** описание платежа */
    interface StateFactRow {

        /** идентификатор платежа (раельного) */
        Long getId();

        /** дата поступления денежных средств */
        Date getDate();

        /** сумма, взятая с платежа в счет списания долга */
        double getPayment();

        /** задолженность после совершения платежа (null - для переплат) */
        double[] getDebt();

        /** список данных по расчету пеней (null - для переплат) */
        Collection<StateFinesRow> getFinesRows();

    }

    /** описание строки графика оплат */
    interface StatePlanRow {

        /** идентификатор строки графика оплат */
        Long getId();

        /** сумма по строке */
        double getCost();

        /** задолжность (обучение, пеней) */
        double[] getDebt();

        /** дата, к которой необходимо внести сумму */
        Date getDate();

        /** список платежей из которых производилось списение долга по строке */
        Collection<StateFactRow> getFactRows();
    }

    /** создает копию */
    IUniscEduAgreementPaymentState clone();

    /** пересчитывает состояние (по новому событию) */
    void process(IUniscEduAgreementPaymentEvent event);

    /** @return задолжность по оплатам (обучение, пеней) по строкам плана до указанной даты */
    double[] getDebt(Date now);

    /** @return список строк из графика оплат */
    Collection<StatePlanRow> getPlanRows();

    /** @return переплата (только за обучение) */
    Collection<StateFactRow> getOverpayRows();

}
