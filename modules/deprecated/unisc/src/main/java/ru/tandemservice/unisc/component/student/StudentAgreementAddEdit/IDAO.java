package ru.tandemservice.unisc.component.student.StudentAgreementAddEdit;

import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author vdanilov
 */
public interface IDAO extends ru.tandemservice.unisc.component.agreement.AgreementRelationAddEdit.IDAO<Student> {

}
