package ru.tandemservice.unisc.component.agreement.AgreementRelationAddEdit;

import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.unisc.dao.agreement.IUniscEduAgreementDAO;
import ru.tandemservice.unisc.entity.agreements.IUniscEduAgreement2PersonRole;


/**
 * @author vdanilov
 */
@SuppressWarnings("deprecation")
public abstract class DAO<T extends PersonRole> extends ru.tandemservice.unisc.component.agreement.AgreementAddEdit.DAO<Model<T>> implements IDAO<T> {

    protected abstract IUniscEduAgreement2PersonRole<T> createRelation(T personRole);

    @SuppressWarnings("unchecked")
    @Override public void prepare(final Model<T> model) {
        if (null == model.getRelation()) {
            final IEntityMeta meta = EntityRuntime.getMeta(model.getId());
            if (null == meta) {
                throw new IllegalStateException("null-value model.id");
            }

            final Class entityClass = meta.getEntityClass();
            if (IUniscEduAgreement2PersonRole.class.isAssignableFrom(entityClass)) {
                model.setRelation((IUniscEduAgreement2PersonRole<T>) this.get(entityClass, model.getId()));

            } else if (PersonRole.class.isAssignableFrom(entityClass)) {
                model.setRelation(this.createRelation((T) this.get(entityClass, model.getId())));

            } else {
                throw new IllegalStateException(entityClass.getName());
            }
        } else {
            this.reassociate(model);
        }

        model.setId(model.getRelation().getId());
        super.prepare(model);
    }

    @Override public void save(final Model<T> model) {
        super.save(model);
        model.setRelation(IUniscEduAgreementDAO.INSTANCE.get().doConnect(model.getRelation()));
    }

    @Override
    protected void reassociate(final Model<T> model) {
        super.reassociate(model);
        if (null != model.getRelation().getId()) {
            this.getSession().update(model.getRelation());
            this.reassociateEntityProperties(getSession(), (EntityBase) model.getRelation());
        }
    }
}
