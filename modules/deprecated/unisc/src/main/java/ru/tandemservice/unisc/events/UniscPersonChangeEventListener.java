package ru.tandemservice.unisc.events;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.hibernate.Session;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.event.single.ISingleEntityEvent;
import org.tandemframework.hibsupport.event.single.listener.IHibernateEventListener;
import org.tandemframework.hibsupport.transaction.sync.ParamTransactionCompleteAction;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonRole;
import org.tandemframework.shared.person.base.entity.gen.PersonRoleGen;

import ru.tandemservice.unisc.dao.agreement.IUniscEduAgreementDAO;
import ru.tandemservice.unisc.entity.agreements.IUniscEduAgreement2PersonRole;

/**
 * @author root
 */
public class UniscPersonChangeEventListener implements IHibernateEventListener<ISingleEntityEvent>
{

    private static final ParamTransactionCompleteAction<Long, Void> action = new ParamTransactionCompleteAction<Long, Void>() {
        @Override public Void beforeCompletion(final Session session, final Collection<Long> params) {
            BatchUtils.execute(params, 100, new BatchUtils.Action<Long>() {
                @Override public void execute(final Collection<Long> params) {
                    final MQBuilder builder = new MQBuilder(PersonRoleGen.ENTITY_CLASS, "pr", new String[] { "id" });
                    builder.add(MQExpression.in("pr", "person.id", params));
                    final Set<Long> personRoleIds = new HashSet<Long>(builder.<Long>getResultList(session));

                    final Date now = new Date();
                    final Map<Long, Set<IUniscEduAgreement2PersonRole<PersonRole>>> map = IUniscEduAgreementDAO.INSTANCE.get().getRelationMap4PersonRoles(personRoleIds);
                    for (final Set<IUniscEduAgreement2PersonRole<PersonRole>> relationSet: map.values()) {
                        for (final IUniscEduAgreement2PersonRole<PersonRole> rel: relationSet) {
                            rel.getAgreement().setLastModificationDate(now);
                            session.update(rel.getAgreement());
                        }
                    }
                }
            });
            session.flush();
            return null;
        }
    };

    @Override
    public void onEvent(final ISingleEntityEvent event) {
        final IEntity entity = event.getEntity();
        if (entity instanceof IdentityCard) {
            final Person person = ((IdentityCard)entity).getPerson();
            if (null != person) {
                UniscPersonChangeEventListener.action.register(event.getSession(), person.getId());
            }
        } else if (entity instanceof Person) {
            UniscPersonChangeEventListener.action.register(event.getSession(), entity.getId());
        }
    }

}
