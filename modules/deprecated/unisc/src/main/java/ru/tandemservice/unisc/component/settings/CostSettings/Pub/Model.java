package ru.tandemservice.unisc.component.settings.CostSettings.Pub;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.AbstractListDataSource;

import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unisc.entity.catalog.UniscPayPeriod;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlan;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlanRow;

@State({
	@Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="id")
})
public class Model {

	/* интерфейсный адаптер для  UniscEduOrgUnitPayPlan */
	public static class Block {

		protected Block(final UniscEduOrgUnitPayPlan plan) { this.plan = plan; }
		protected Block(final UniscEduOrgUnit uniscEduOrgUnit, final UniscPayPeriod uniscPayFreq) {
			this.plan = new UniscEduOrgUnitPayPlan();
			this.plan.setUniscEduOrgUnit(uniscEduOrgUnit);
			this.plan.setUniscPayFreq(uniscPayFreq);
		}

		private final UniscEduOrgUnitPayPlan plan;
		public UniscEduOrgUnitPayPlan getPlan() { return this.plan; }

		private final List<UniscEduOrgUnitPayPlanRow> rows = new ArrayList<UniscEduOrgUnitPayPlanRow>();
		protected List<UniscEduOrgUnitPayPlanRow> getRows() { return this.rows; }

		private final AbstractListDataSource<UniscEduOrgUnitPayPlanRow> dataSource = new AbstractListDataSource<UniscEduOrgUnitPayPlanRow>() {
			@Override public void onChangeOrder() {}
			@Override public void onRefresh() {}
			@Override public long getTotalSize() { return Block.this.rows.size(); }
			@Override public long getStartRow() { return 0; }
			@Override public long getCountRow() { return this.getTotalSize(); }
			@Override public List<UniscEduOrgUnitPayPlanRow> getEntityList() { return Block.this.rows; }
            @Override public AbstractListDataSource<UniscEduOrgUnitPayPlanRow> getCopy() { return this; }
		};
		public AbstractListDataSource<UniscEduOrgUnitPayPlanRow> getDataSource() { return this.dataSource; }

	}

	private Long id;
	public Long getId() { return this.id; }
	public void setId(final Long id) { this.id = id; }

	private UniscEduOrgUnit eduOrgUnit;
	public UniscEduOrgUnit getEduOrgUnit() { return this.eduOrgUnit; }
	public void setEduOrgUnit(final UniscEduOrgUnit eduOrgUnit) { this.eduOrgUnit = eduOrgUnit; }

	public EducationOrgUnit getEducationOrgUnit() { return this.getEduOrgUnit().getEducationOrgUnit(); }

	private List<Block> blockList = Collections.emptyList();
	public List<Block> getBlockList() { return this.blockList; }
	protected void setBlockList(final List<Block> blockList) { this.blockList = blockList; }

	private transient Block currentBlock;
	public Block getCurrentBlock() { return this.currentBlock; }
	public void setCurrentBlock(final Block currentBlock) { this.currentBlock = currentBlock; }

}
