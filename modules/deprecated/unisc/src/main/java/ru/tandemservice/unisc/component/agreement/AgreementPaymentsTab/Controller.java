/* $Id: Controller.java 8816 2009-07-02 07:31:01Z ekachanova $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisc.component.agreement.AgreementPaymentsTab;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayFactRow;
import ru.tandemservice.unisc.entity.agreements.gen.UniscEduAgreementPayFactRowGen;

import java.util.Collections;

/**
 * @author vdanilov
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = getModel(component);
        getDao().prepare(model);

        final DynamicListDataSource<UniscEduAgreementPayFactRow> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareDataSource(model);
        });

        dataSource.addColumn(new SimpleColumn("Дата платежа", UniscEduAgreementPayFactRowGen.P_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Сумма платежа", UniscEduAgreementPayFactRow.VIEW_P_COST_AS_STRING).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Назначение платежа", UniscEduAgreementPayFactRowGen.P_FINES_PAYMENT, value -> Boolean.TRUE.equals(value) ? "Списание пени" : "За обучение").setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Тип документа", UniscEduAgreementPayFactRowGen.L_TYPE + ".title").setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("№ документа", UniscEduAgreementPayFactRowGen.P_NUMBER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Плательщик", UniscEduAgreementPayFactRow.VIEW_P_PAYER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Комментарий", UniscEduAgreementPayFactRowGen.P_COMMENT).setClickable(false).setOrderable(false));

        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit").setPermissionKey(model.getSecModel().getPermission("editRow")));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить платеж на сумму {0} от {1}?", UniscEduAgreementPayFactRow.VIEW_P_COST_AS_STRING, UniscEduAgreementPayFactRow.VIEW_P_DATE_STR).setPermissionKey(model.getSecModel().getPermission("deleteRow")));

        model.setDataSource(dataSource);
    }

    public void onClickAdd(final IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(ru.tandemservice.unisc.component.agreement.AgreementPaymentAddEdit.Model.class.getPackage().getName(),
                                                         Collections.<String, Object>singletonMap(PublisherActivator.PUBLISHER_ID_KEY, getModel(component).getId())
        ));
    }

    public void onClickEdit(final IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(ru.tandemservice.unisc.component.agreement.AgreementPaymentAddEdit.Model.class.getPackage().getName(),
                                                         Collections.singletonMap(PublisherActivator.PUBLISHER_ID_KEY, component.getListenerParameter())
        ));
    }

    public void onClickDelete(final IBusinessComponent component)
    {
        getDao().delete(getModel(component), (Long) component.getListenerParameter());
    }
}
