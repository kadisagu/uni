package ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage2;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage.AbstractStageController;

/**
 * @author vdanilov
 */
@SuppressWarnings("deprecation")
public class Controller extends AbstractStageController<IDAO, Model> {

    public void onClickApplyCost(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        model.getPayPlanModel().applyCost();
    }

    public void onClickPayPlanModelSplitRow(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        final Long id = (Long) component.getListenerParameter();
        if (model.getPayPlanModel().split(id)) {
            getDao().prepare(model);
        }
    }

    public void onClickPayPlanModelMergeDownRow(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        final Long id = (Long) component.getListenerParameter();
        if (model.getPayPlanModel().merge(id)) {
            getDao().prepare(model);
        }
    }


}
