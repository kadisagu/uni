package ru.tandemservice.unisc.dao.agreement;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisc.entity.agreements.*;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Содержит общие (заменяемые) методы работы с контрактами
 * @author vdanilov
 */
public interface IUniscEduAgreementDAO {
    public static final SpringBeanCache<IUniscEduAgreementDAO> INSTANCE = new SpringBeanCache<IUniscEduAgreementDAO>(IUniscEduAgreementDAO.class.getName());

    /**
     * генерирует номер для договора
     * @param agreement договор, для которого нужно сгенерировать номер
     * @param force в не зависимости от настроек
     **/
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    void assignNewAgreementNumber(UniscEduAgreementBase agreement, boolean force);

    /**
     * сохраняет договор (и проверяет дублирование номера)
     * @param agreement договор, который нужно сохранить
     * @return true, если объект успешно сохранен; false - если номер занят
     **/
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    boolean doSaveAgreementWithNumberCheck(UniscEduAgreementBase agreement);


    /**
     * устанавливает связь между договором
     * @param agreement
     * @param studentId
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    <T extends EntityBase, U extends IUniscEduAgreement2PersonRole<T>> U doConnect(U relation);

    /**
     * возвращает обект-связь по договору (в порядке приоритетов)
     * @param agreement
     * @return
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    IUniscEduAgreement2PersonRole<PersonRole> getRelation(UniscEduMainAgreement agreement);

    /**
     * возвращает id обектов-связей по договорам (в порядке приоритетов)
     * @param mainAgreementIds
     * @return { mainAgreement.id -> IUniscEduAgreement2PersonRole }
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    Map<Long, IUniscEduAgreement2PersonRole<PersonRole>> getRelationMap4Agreements(Collection<Long> mainAgreementIds);

    /**
     * возвращает id обектов-связей по персон-ролям
     * @param personRoleIds
     * @return { mainAgreement.id -> { IUniscEduAgreement2PersonRole } }
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    Map<Long, Set<IUniscEduAgreement2PersonRole<PersonRole>>> getRelationMap4PersonRoles(Collection<Long> personRoleIds);


    /**
     * для вычисления permissionKeys для работы с договором
     * @param agreement договор
     * @return Student\Entrant в зависимости от того, с кем заключен этот договор.
     * Если и с тем, и с тем - то Student.
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    String getPermissionContext(UniscEduAgreementBase agreement);


    /**
     * проверяет, что договор можно изменять
     * @param agreement
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    boolean isMutable(UniscEduAgreementBase agreement);


    /** возвращает список физлиц, прикрепленных к договору (или доп соглашению) */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    List<UniscEduAgreementNaturalPerson> getNaturalPersons(UniscEduAgreementBase agreement);

    /** проверяет список физлиц, прикрепленных к договору (или доп соглашению) */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    void checkNaturalPersons(UniscEduAgreementBase agreement, List<UniscEduAgreementNaturalPerson> naturalPersons);

    /** обновляет список физлиц, привязанных к договору (или доп соглашению) */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doSyncNaturalPersons(UniscEduAgreementBase agreement, List<UniscEduAgreementNaturalPerson> naturalPersons);



    /** возвращает график оплат по договору (или доп соглашению) */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    List<UniscEduAgreementPayPlanRow> getAgreementPayPlanRows(UniscEduAgreementBase agreement, boolean createEmptyRowsFromConfig);

    //    /** возвращает ИТОГОВЫЙ график оплат по договору (или доп соглашению) */
    //    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    //    List<UniscEduAgreementPayPlanRow> getAgreementTotalPayPlanRows(UniscEduAgreementBase agreement);

    /** обновляет график оплат по договору (или доп соглашению) */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doSyncAgreementPayPlanRows(UniscEduAgreementBase agreement, List<UniscEduAgreementPayPlanRow> rows);


    /** проверяет видимость таба на студенте */
    boolean isTabVisible(Student student);

    /** проверяет видимость кнопки редактирования на карточку доп. соглашения */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    public boolean isEditBottonVisible(UniscEduMainAgreement agreement);

    /**
     * Возвращает последний из студенческих договоров
     *
     * @param studentId - идентификатор студента
     * @return - последний из основных договоров со студентом
     */
    UniscEduMainAgreement getStudentContract(Long studentId);

    /**
     * Возвращает мап последних из студенческих договоров по каждому идентификатору студента из передаваемой коллекции
     *
     * @param studentIds - коллекция идентификаторов студентов
     * @return - мап последних из основных договоров по каждому студенту
     *         ключ = идентификатор студента
     *         значение - последний из основных договоров со студентом
     */
    Map<Long, UniscEduMainAgreement> getStudentContracts(Collection<Long> studentIds);
}