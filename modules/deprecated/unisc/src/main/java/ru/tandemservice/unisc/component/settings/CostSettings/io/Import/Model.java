package ru.tandemservice.unisc.component.settings.CostSettings.io.Import;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

/**
 * @author vdanilov
 */
public class Model {
	private IUploadFile uploadFile;
	public IUploadFile getUploadFile() { return this.uploadFile; }
	public void setUploadFile(final IUploadFile uploadFile) { this.uploadFile = uploadFile; }

	private EducationYear year;
	public EducationYear getYear() { return this.year; }
	public void setYear(final EducationYear year) { this.year = year; }

	private ISelectModel years = null;
	public ISelectModel getYears() { return this.years; }
	public void setYears(final ISelectModel years) { this.years = years; }
}
