package ru.tandemservice.unisc.component.agreement.AgreementPrintVersionsTab;

import java.util.List;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unisc.entity.agreements.IUniscEduAgreement2PersonRole;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPrintVersion;
import ru.tandemservice.unisc.entity.agreements.gen.UniscEduAgreementPrintVersionGen;

/**
 * @author vdanilov
 */
public class DAO extends UniBaseDao implements IDAO {

    @Override
    @SuppressWarnings("unchecked")
    public void prepare(final Model model) {
        final IEntity entity = get(model.getId());
        if (entity instanceof UniscEduAgreementBase) {
            model.setAgreement((UniscEduAgreementBase)entity);
        } else if (entity instanceof IUniscEduAgreement2PersonRole) {
            model.setAgreement(((IUniscEduAgreement2PersonRole)entity).getAgreement());
        } else {
            if (null == entity) { throw new NullPointerException(String.valueOf(model.getId())); }
            throw new IllegalStateException(entity.getClass().getName());
        }

        model.setId(model.getAgreement().getId());
    }

    @Override
    public void refreshDataSource(final Model model) {
        final MQBuilder builder = new MQBuilder(UniscEduAgreementPrintVersionGen.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", UniscEduAgreementPrintVersionGen.L_OWNER+".id", model.getAgreement().getId()));
        builder.addOrder("e", UniscEduAgreementPrintVersionGen.P_TIMESTAMP);
        final DynamicListDataSource<UniscEduAgreementPrintVersion> dataSource = model.getDataSource();

        final List<UniscEduAgreementPrintVersion> resultList = builder.<UniscEduAgreementPrintVersion>getResultList(getSession());
        dataSource.createPage(resultList);
        dataSource.setTotalSize(resultList.size());
        dataSource.setCountRow(resultList.size());
    }

    @Override
    public void delete(final Model model, final Long id) {
        if ((null != model.getAgreement().getPrintVersion()) && model.getAgreement().getPrintVersion().getId().equals(id)) { return; }

        final UniscEduAgreementPrintVersion pv = get(UniscEduAgreementPrintVersion.class, id);
        if (!model.getAgreement().equals(pv.getOwner())) { return; }

        getSession().delete(pv);
    }


}
