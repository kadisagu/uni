package ru.tandemservice.unisc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип заказчика"
 * Имя сущности : uniscCustomerType
 * Файл data.xml : unisc.data.xml
 */
public interface UniscCustomerTypeCodes
{
    /** Константа кода (code) элемента : физическое лицо (title) */
    String FIZICHESKOE_LITSO = "1";
    /** Константа кода (code) элемента : юридическое лицо (title) */
    String YURIDICHESKOE_LITSO = "2";
    /** Константа кода (code) элемента : юридическое лицо с частичной оплатой (title) */
    String YURIDICHESKOE_LITSO_S_CHASTICHNOY_OPLATOY = "3";

    Set<String> CODES = ImmutableSet.of(FIZICHESKOE_LITSO, YURIDICHESKOE_LITSO, YURIDICHESKOE_LITSO_S_CHASTICHNOY_OPLATOY);
}
