package ru.tandemservice.unisc.dao.payment;

import java.util.Date;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;

/**
 * @author vdanilov
 */
public interface IUniscEduAgreementPaymentEvent extends IEntity, ITitled {

    /* дата события */
    Date getEventDate();


    /* строка графика оплат */
    public static interface PlanRow extends IUniscEduAgreementPaymentEvent {
        Long getOwnerId();
        int getStage();
        double getJCostAsDoubleDiscounted();
        double getNCostAsDoubleDiscounted();

    }

    /* оплата */
    public static interface FactRow extends IUniscEduAgreementPaymentEvent {
        boolean isFinesPayment();
        double getJCostAsDouble();
        double getNCostAsDouble();

    }

    /* изменение процента начисления пеней */
    public static interface FinesTax extends IUniscEduAgreementPaymentEvent {
        double getFinesTax();
    }

    /* изменения факта начисления пеней */
    public static interface FinesState extends IUniscEduAgreementPaymentEvent {
        boolean isFinesActive();
    }

}
