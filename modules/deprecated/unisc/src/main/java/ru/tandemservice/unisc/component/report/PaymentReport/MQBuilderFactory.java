package ru.tandemservice.unisc.component.report.PaymentReport;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;

import java.util.List;
import java.util.Set;

/**
 * Фабрика для удобной передачи метода, который конструирует билдер.
 * На совести использующего остается проследить за тем, чтобы alias, idProperty
 * и titleProperty были указаны верно.
 * @author oleyba
 * @since 14.08.2009
 */
public abstract class MQBuilderFactory
{
    private String alias;
    private String idProperty = "id";
    private String titleProperty = "title";

    public abstract MQBuilder createBuilder();

    protected MQBuilderFactory(String alias)
    {
        this.alias = alias;
    }

    /**
     * Простейший вариант фабрики - клонирует и возвращает билдер, переданный в качестве образца.
     * При использовании стоит иметь в виду код клонирующего конструктора MQBuilder'а -
     * возможно, в конкретном случае лучше написать метод, который каждый раз
     * создает билдер полностью заново.
     * @param template билдер-образец
     * @param alias альяс
     * @return фабрика
     */
    public static MQBuilderFactory create(final MQBuilder template, final String alias)
    {
        return new MQBuilderFactory(alias)
        {
            @Override
            public MQBuilder createBuilder()
            {
                return new MQBuilder(template);
            }
        };
    }

    public abstract static class AutocompleteModel extends BaseSingleSelectModel implements IMultiSelectModel
    {
        public abstract MQBuilderFactory getFactory();

        public AutocompleteModel(Session session)
        {
        }

        @Override
        public Object getValue(Object primaryKey)
        {
            MQBuilder builder = getFactory().createBuilder();
            builder.add(MQExpression.eq(getFactory().getAlias(), getFactory().getIdProperty(), primaryKey));
            return builder.uniqueResult(DataAccessServices.dao().getComponentSession());
        }

        @Override
        public List getValues(Set primaryKeys)
        {
            MQBuilder builder = getFactory().createBuilder();
            builder.add(MQExpression.in(getFactory().getAlias(), getFactory().getIdProperty(), primaryKeys));
            return builder.getResultList(DataAccessServices.dao().getComponentSession());
        }

        @Override
        public ListResult findValues(String filter)
        {
            MQBuilder builder = getFactory().createBuilder();
            if (StringUtils.isNotBlank(filter))
                builder.add(MQExpression.like(getFactory().getAlias(), getFactory().getTitleProperty(), "%" + filter));
            return new ListResult<>(builder.getResultList(DataAccessServices.dao().getComponentSession(), 0, 20), (int) builder.getResultCount(DataAccessServices.dao().getComponentSession()));
        }
    }

    public MQBuilder createIdBuilder()
    {
        return createBuilder().addSelect(getAlias(), new String[]{getIdProperty()});
    }

    public MQBuilderFactory setIdProperty(String idProperty)
    {
        this.idProperty = idProperty;
        return this;
    }

    public MQBuilderFactory setTitleProperty(String titleProperty)
    {
        this.titleProperty = titleProperty;
        return this;
    }

    // getters

    public String getAlias()
    {
        return alias;
    }

    public String getIdProperty()
    {
        return idProperty;
    }

    public String getTitleProperty()
    {
        return titleProperty;
    }
}
