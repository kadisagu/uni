package ru.tandemservice.unisc.component.settings.CostSettings.io.Import;

import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.event.IEventServiceLock;

import java.io.IOException;

/**
 * @author vdanilov
 */
public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(final IBusinessComponent component) {
        final Model model = getModel(component);
        getDao().prepare(model);
    }

    public synchronized void onClickApply(final IBusinessComponent component) throws IOException
    {
        final Model model = getModel(component);
        IEventServiceLock eventLock = CoreServices.eventService().lock();
        try {
            getDao().save(model, component.getUserContext().getErrorCollector());
        } finally {
            eventLock.release();
        }
        // System.gc();
        deactivate(component);
    }

}
