// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisc.component.orgunit.OrgUnitContractList;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.EducationLevelsHighSchoolAutocompleteModel;
import ru.tandemservice.unisc.component.report.PaymentReport.MQBuilderFactory;

import java.util.List;

/**
 * @author oleyba
 * @since 02.09.2009
 */
public class DAO extends ru.tandemservice.unisc.component.list.StudentAgreementListBase.DAO<Model>
{
    @Override
    @SuppressWarnings({"unchecked", "deprecation"})
    public void prepare(final Model model)
    {
        super.prepare(model);
        //final Session session = getSession();
        model.setOrgUnit((OrgUnit) getNotNull(model.getOrgUnitId()));
        model.setSecModel(new CommonPostfixPermissionModel(model.getOrgUnit().getOrgUnitType().getCode()));
        model.setFormativeOrgUnitModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder builder = getFormativeOrgUnitBulder(model).createBuilder();
                if (StringUtils.isNotBlank(filter))
                {
                    builder.add(MQExpression.like("ou", OrgUnit.P_TITLE, "%" + filter));
                }
                List<OrgUnit> list = builder.getResultList(getSession());
                return new ListResult<>(list);
            }
        });
        model.setTerritorialOrgUnitModel(new FullCheckSelectModel(OrgUnit.P_TERRITORIAL_FULL_TITLE)
        {
            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder builder = getTerritorialOrgUnitBulder(model).createBuilder();
                if (StringUtils.isNotBlank(filter))
                {
                    builder.add(MQExpression.like("ou", OrgUnit.P_TITLE, "%" + filter));
                }
                List<OrgUnit> list = builder.getResultList(getSession());
                return new ListResult<>(list);
            }
        });
        model.setEducationLevelsHighSchoolModel(new EducationLevelsHighSchoolAutocompleteModel()
        {
            @Override
            protected List<EducationLevelsHighSchool> getFilteredList()
            {
                MQBuilder builder = new MQBuilder(EducationLevelsHighSchool.ENTITY_CLASS, "h");
                builder.setNeedDistinct(true);

                builder.addDomain("student", Student.ENTITY_CLASS);
                builder.addJoin("student", Student.L_EDUCATION_ORG_UNIT, "e");
                builder.addLeftJoin("e",  EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, "terr");

                builder.add(MQExpression.eqProperty("student", Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL + ".id", "h", "id"));

                builder.add(MQExpression.or(
                        MQExpression.eq("student", Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT + ".id", model.getOrgUnitId()),
                        MQExpression.eq("terr", "id", model.getOrgUnitId())
                ));

                if ((null != model.getFormativeOrgUnitList()) && !model.getFormativeOrgUnitList().isEmpty())
                {
                    builder.add(MQExpression.in("e", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnitList()));
                }
                if ((null != model.getTerritorialOrgUnitList()) && !model.getTerritorialOrgUnitList().isEmpty()) {
                    builder.add(MQExpression.in("e", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnitList()));
                }

                builder.addOrder("h", EducationLevelsHighSchool.P_TITLE, OrderDirection.asc);
                return builder.getResultList(getSession());
            }
        });
    }

    @Override
    protected MQBuilder getStudentBuilder(final Model model) {
        final MQBuilder studentBuilder = super.getStudentBuilder(model);
        studentBuilder.addLeftJoin("student",  Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, "terr");
        studentBuilder.add(MQExpression.or(
                MQExpression.eq("student", Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT + ".id", model.getOrgUnitId()),
                MQExpression.eq("terr", "id", model.getOrgUnitId())
        ));
        return studentBuilder;
    }

    private static MQBuilderFactory getFormativeOrgUnitBulder(final Model model)
    {
        return new MQBuilderFactory("ou")
        {
            @Override
            public MQBuilder createBuilder()
            {
                MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "ou");
                builder.add(MQExpression.eq("ou", OrgUnit.P_ARCHIVAL, Boolean.FALSE));
                builder.setNeedDistinct(true);
                builder.addDomain("student", Student.ENTITY_CLASS);
                builder.add(MQExpression.eqProperty("student", Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT + ".id", "ou", "id"));
                builder.addLeftJoin("student",  Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, "terr");
                builder.add(MQExpression.or(
                        MQExpression.eq("student", Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT + ".id", model.getOrgUnitId()),
                        MQExpression.eq("terr", "id", model.getOrgUnitId())
                ));
                return builder;
            }
        };
    }

    private static MQBuilderFactory getTerritorialOrgUnitBulder(final Model model)
    {
        return new MQBuilderFactory("ou")
        {
            @Override
            public MQBuilder createBuilder()
            {
                MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "ou");
                builder.add(MQExpression.eq("ou", OrgUnit.P_ARCHIVAL, Boolean.FALSE));
                builder.setNeedDistinct(true);
                builder.addDomain("student", Student.ENTITY_CLASS);
                builder.add(MQExpression.eqProperty("student", Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT + ".id", "ou", "id"));
                builder.addLeftJoin("student",  Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, "terr");
                builder.add(MQExpression.or(
                        MQExpression.eq("student", Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT + ".id", model.getOrgUnitId()),
                        MQExpression.eq("terr", "id", model.getOrgUnitId())
                ));
                return builder;
            }
        };
    }
}
