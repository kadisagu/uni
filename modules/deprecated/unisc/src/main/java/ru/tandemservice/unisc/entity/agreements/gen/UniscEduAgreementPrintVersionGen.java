package ru.tandemservice.unisc.entity.agreements.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPrintVersion;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Печатная версия документа
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniscEduAgreementPrintVersionGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPrintVersion";
    public static final String ENTITY_NAME = "uniscEduAgreementPrintVersion";
    public static final int VERSION_HASH = 1285264373;
    private static IEntityMeta ENTITY_META;

    public static final String L_OWNER = "owner";
    public static final String L_FILE = "file";
    public static final String P_UPLOADED = "uploaded";
    public static final String P_TIMESTAMP = "timestamp";
    public static final String P_TITLE = "title";
    public static final String P_COMMENT = "comment";
    public static final String P_FILE_NAME = "fileName";
    public static final String P_CONTENT_TYPE = "contentType";
    public static final String P_USER = "user";

    private UniscEduAgreementBase _owner;     // Договор
    private DatabaseFile _file;     // Файл с текстом документа
    private boolean _uploaded;     // Загружен из файла
    private Date _timestamp;     // Дата формирования
    private String _title;     // Название версии
    private String _comment;     // Комментарий
    private String _fileName;     // Название файла
    private String _contentType;     // Тип
    private String _user;     // Пользователь

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Договор. Свойство не может быть null.
     */
    @NotNull
    public UniscEduAgreementBase getOwner()
    {
        return _owner;
    }

    /**
     * @param owner Договор. Свойство не может быть null.
     */
    public void setOwner(UniscEduAgreementBase owner)
    {
        dirty(_owner, owner);
        _owner = owner;
    }

    /**
     * @return Файл с текстом документа. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getFile()
    {
        return _file;
    }

    /**
     * @param file Файл с текстом документа. Свойство не может быть null.
     */
    public void setFile(DatabaseFile file)
    {
        dirty(_file, file);
        _file = file;
    }

    /**
     * @return Загружен из файла. Свойство не может быть null.
     */
    @NotNull
    public boolean isUploaded()
    {
        return _uploaded;
    }

    /**
     * @param uploaded Загружен из файла. Свойство не может быть null.
     */
    public void setUploaded(boolean uploaded)
    {
        dirty(_uploaded, uploaded);
        _uploaded = uploaded;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getTimestamp()
    {
        return _timestamp;
    }

    /**
     * @param timestamp Дата формирования. Свойство не может быть null.
     */
    public void setTimestamp(Date timestamp)
    {
        dirty(_timestamp, timestamp);
        _timestamp = timestamp;
    }

    /**
     * @return Название версии.
     */
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название версии.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Комментарий.
     */
    @Length(max=255)
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * @return Название файла.
     */
    @Length(max=255)
    public String getFileName()
    {
        return _fileName;
    }

    /**
     * @param fileName Название файла.
     */
    public void setFileName(String fileName)
    {
        dirty(_fileName, fileName);
        _fileName = fileName;
    }

    /**
     * @return Тип.
     */
    @Length(max=255)
    public String getContentType()
    {
        return _contentType;
    }

    /**
     * @param contentType Тип.
     */
    public void setContentType(String contentType)
    {
        dirty(_contentType, contentType);
        _contentType = contentType;
    }

    /**
     * @return Пользователь.
     */
    @Length(max=255)
    public String getUser()
    {
        return _user;
    }

    /**
     * @param user Пользователь.
     */
    public void setUser(String user)
    {
        dirty(_user, user);
        _user = user;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UniscEduAgreementPrintVersionGen)
        {
            setOwner(((UniscEduAgreementPrintVersion)another).getOwner());
            setFile(((UniscEduAgreementPrintVersion)another).getFile());
            setUploaded(((UniscEduAgreementPrintVersion)another).isUploaded());
            setTimestamp(((UniscEduAgreementPrintVersion)another).getTimestamp());
            setTitle(((UniscEduAgreementPrintVersion)another).getTitle());
            setComment(((UniscEduAgreementPrintVersion)another).getComment());
            setFileName(((UniscEduAgreementPrintVersion)another).getFileName());
            setContentType(((UniscEduAgreementPrintVersion)another).getContentType());
            setUser(((UniscEduAgreementPrintVersion)another).getUser());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniscEduAgreementPrintVersionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniscEduAgreementPrintVersion.class;
        }

        public T newInstance()
        {
            return (T) new UniscEduAgreementPrintVersion();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "owner":
                    return obj.getOwner();
                case "file":
                    return obj.getFile();
                case "uploaded":
                    return obj.isUploaded();
                case "timestamp":
                    return obj.getTimestamp();
                case "title":
                    return obj.getTitle();
                case "comment":
                    return obj.getComment();
                case "fileName":
                    return obj.getFileName();
                case "contentType":
                    return obj.getContentType();
                case "user":
                    return obj.getUser();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "owner":
                    obj.setOwner((UniscEduAgreementBase) value);
                    return;
                case "file":
                    obj.setFile((DatabaseFile) value);
                    return;
                case "uploaded":
                    obj.setUploaded((Boolean) value);
                    return;
                case "timestamp":
                    obj.setTimestamp((Date) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "fileName":
                    obj.setFileName((String) value);
                    return;
                case "contentType":
                    obj.setContentType((String) value);
                    return;
                case "user":
                    obj.setUser((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "owner":
                        return true;
                case "file":
                        return true;
                case "uploaded":
                        return true;
                case "timestamp":
                        return true;
                case "title":
                        return true;
                case "comment":
                        return true;
                case "fileName":
                        return true;
                case "contentType":
                        return true;
                case "user":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "owner":
                    return true;
                case "file":
                    return true;
                case "uploaded":
                    return true;
                case "timestamp":
                    return true;
                case "title":
                    return true;
                case "comment":
                    return true;
                case "fileName":
                    return true;
                case "contentType":
                    return true;
                case "user":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "owner":
                    return UniscEduAgreementBase.class;
                case "file":
                    return DatabaseFile.class;
                case "uploaded":
                    return Boolean.class;
                case "timestamp":
                    return Date.class;
                case "title":
                    return String.class;
                case "comment":
                    return String.class;
                case "fileName":
                    return String.class;
                case "contentType":
                    return String.class;
                case "user":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniscEduAgreementPrintVersion> _dslPath = new Path<UniscEduAgreementPrintVersion>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniscEduAgreementPrintVersion");
    }
            

    /**
     * @return Договор. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPrintVersion#getOwner()
     */
    public static UniscEduAgreementBase.Path<UniscEduAgreementBase> owner()
    {
        return _dslPath.owner();
    }

    /**
     * @return Файл с текстом документа. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPrintVersion#getFile()
     */
    public static DatabaseFile.Path<DatabaseFile> file()
    {
        return _dslPath.file();
    }

    /**
     * @return Загружен из файла. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPrintVersion#isUploaded()
     */
    public static PropertyPath<Boolean> uploaded()
    {
        return _dslPath.uploaded();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPrintVersion#getTimestamp()
     */
    public static PropertyPath<Date> timestamp()
    {
        return _dslPath.timestamp();
    }

    /**
     * @return Название версии.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPrintVersion#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPrintVersion#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @return Название файла.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPrintVersion#getFileName()
     */
    public static PropertyPath<String> fileName()
    {
        return _dslPath.fileName();
    }

    /**
     * @return Тип.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPrintVersion#getContentType()
     */
    public static PropertyPath<String> contentType()
    {
        return _dslPath.contentType();
    }

    /**
     * @return Пользователь.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPrintVersion#getUser()
     */
    public static PropertyPath<String> user()
    {
        return _dslPath.user();
    }

    public static class Path<E extends UniscEduAgreementPrintVersion> extends EntityPath<E>
    {
        private UniscEduAgreementBase.Path<UniscEduAgreementBase> _owner;
        private DatabaseFile.Path<DatabaseFile> _file;
        private PropertyPath<Boolean> _uploaded;
        private PropertyPath<Date> _timestamp;
        private PropertyPath<String> _title;
        private PropertyPath<String> _comment;
        private PropertyPath<String> _fileName;
        private PropertyPath<String> _contentType;
        private PropertyPath<String> _user;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Договор. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPrintVersion#getOwner()
     */
        public UniscEduAgreementBase.Path<UniscEduAgreementBase> owner()
        {
            if(_owner == null )
                _owner = new UniscEduAgreementBase.Path<UniscEduAgreementBase>(L_OWNER, this);
            return _owner;
        }

    /**
     * @return Файл с текстом документа. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPrintVersion#getFile()
     */
        public DatabaseFile.Path<DatabaseFile> file()
        {
            if(_file == null )
                _file = new DatabaseFile.Path<DatabaseFile>(L_FILE, this);
            return _file;
        }

    /**
     * @return Загружен из файла. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPrintVersion#isUploaded()
     */
        public PropertyPath<Boolean> uploaded()
        {
            if(_uploaded == null )
                _uploaded = new PropertyPath<Boolean>(UniscEduAgreementPrintVersionGen.P_UPLOADED, this);
            return _uploaded;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPrintVersion#getTimestamp()
     */
        public PropertyPath<Date> timestamp()
        {
            if(_timestamp == null )
                _timestamp = new PropertyPath<Date>(UniscEduAgreementPrintVersionGen.P_TIMESTAMP, this);
            return _timestamp;
        }

    /**
     * @return Название версии.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPrintVersion#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(UniscEduAgreementPrintVersionGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPrintVersion#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(UniscEduAgreementPrintVersionGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @return Название файла.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPrintVersion#getFileName()
     */
        public PropertyPath<String> fileName()
        {
            if(_fileName == null )
                _fileName = new PropertyPath<String>(UniscEduAgreementPrintVersionGen.P_FILE_NAME, this);
            return _fileName;
        }

    /**
     * @return Тип.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPrintVersion#getContentType()
     */
        public PropertyPath<String> contentType()
        {
            if(_contentType == null )
                _contentType = new PropertyPath<String>(UniscEduAgreementPrintVersionGen.P_CONTENT_TYPE, this);
            return _contentType;
        }

    /**
     * @return Пользователь.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPrintVersion#getUser()
     */
        public PropertyPath<String> user()
        {
            if(_user == null )
                _user = new PropertyPath<String>(UniscEduAgreementPrintVersionGen.P_USER, this);
            return _user;
        }

        public Class getEntityClass()
        {
            return UniscEduAgreementPrintVersion.class;
        }

        public String getEntityName()
        {
            return "uniscEduAgreementPrintVersion";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
