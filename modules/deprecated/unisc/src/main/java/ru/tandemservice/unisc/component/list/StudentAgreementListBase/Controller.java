/* $Id: Controller.java 8435 2009-06-11 07:08:32Z vdanilov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unisc.component.list.StudentAgreementListBase;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.gen.PersonRoleGen;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.gen.StudentGen;
import ru.tandemservice.unisc.component.list.StudentAgreementListBase.Model.Row;
import ru.tandemservice.unisc.entity.agreements.UniscEduMainAgreement;
import ru.tandemservice.unisc.entity.agreements.gen.UniscEduAgreementBaseGen;
import ru.tandemservice.unisc.entity.config.gen.UniscEduOrgUnitGen;

import java.util.*;

/**
 * @author vdanilov
 */
@SuppressWarnings({"unchecked"})
public class Controller<I extends IDAO, M extends Model> extends AbstractBusinessController<I, M>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final M model = this.getModel(component);
        final I dao = this.getDao();
        model.setSettings(component.getSettings());
        dao.prepare(model);

        final DynamicListDataSource<Model.Row> dataSource = new DynamicListDataSource<Model.Row>(component, component1 -> dao.refreshStudentContractsDataSource(model, false)) {
            @Override public List<Long> getCountRowValueList() {
                List<Long> result = new ArrayList<>();
                result.add(10L);
                result.add(50L);
                result.add(100L);
                return result;
            }
        };

        final IMergeRowIdResolver studentIdResolver = e -> String.valueOf(((Row)e).getStudent().getId());
        final IMergeRowIdResolver mainAgreementIdResolver = e -> String.valueOf(((Row)e).getMainAgreement().getId());

        //dataSource.addColumn(UniListController.getIcoColumn("student", "Студент").setMergeRowIdResolver(studentIdResolver).setMergeRows(true));

        //dataSource.addColumn(new SimpleColumn("ФИО", "student."+PersonRoleGen.L_PERSON+"."+Person.P_FIO).setOrderable(false).setClickable(false).setMergeRowIdResolver(studentIdResolver).setMergeRows(true));

        final PublisherLinkColumn studentColumn = new PublisherLinkColumn("Студент", PersonRoleGen.L_PERSON + "." + Person.P_FIO, "student");
        studentColumn.setResolver(new IPublisherLinkResolver() {
            @Override
            public Object getParameters(final IEntity entity)
            {
                final Map<String, Object> parameters = new HashMap<>();
                parameters.put(PublisherActivator.PUBLISHER_ID_KEY, ((Student) entity).getId());
                parameters.put(ru.tandemservice.uni.component.student.StudentPub.Model.SELECTED_TAB_BIND_KEY, "contractsTab");
                return parameters;
            }

            @Override
            public String getComponentName(final IEntity entity) { return null; }
        });
        dataSource.addColumn(studentColumn.setOrderable(false).setMergeRowIdResolver(studentIdResolver).setMergeRows(true).setRequired(true));

        dataSource.addColumn(new SimpleColumn("Состояние", "student."+StudentGen.L_STATUS+".title").setOrderable(false).setClickable(false).setMergeRowIdResolver(studentIdResolver).setMergeRows(true));
        dataSource.addColumn(new SimpleColumn("Категория", "student."+StudentGen.L_STUDENT_CATEGORY+".title").setOrderable(false).setClickable(false).setMergeRowIdResolver(studentIdResolver).setMergeRows(true));
        dataSource.addColumn(new SimpleColumn("Курс", "student."+StudentGen.L_COURSE+".title").setOrderable(false).setClickable(false).setMergeRowIdResolver(studentIdResolver).setMergeRows(true));

        dataSource.addColumn(new SimpleColumn("№ договора", "mainAgreement."+UniscEduAgreementBaseGen.P_NUMBER).setOrderable(false).setClickable(false).setMergeRowIdResolver(mainAgreementIdResolver).setMergeRows(true));

        dataSource.addColumn(new BlockColumn<String>("payPlanDebt", "Задолженность (пени)").setOrderable(false).setClickable(false).setMergeRowIdResolver(mainAgreementIdResolver).setMergeRows(true));
        dataSource.addColumn(new BlockColumn<String>("payPlanStatus", "Оплачено этапов").setOrderable(false).setClickable(false).setMergeRowIdResolver(mainAgreementIdResolver).setMergeRows(true));


        dataSource.addColumn(new PublisherLinkColumn("№ документа", "documentNumber").setResolver(new DefaultPublisherLinkResolver() {
            @Override public Object getParameters(final IEntity e) {
                final Row row = ((Model.Row)e);
                return ((row.getAgreement() instanceof UniscEduMainAgreement) ? row.getRelation().getId() : row.getAgreement().getId());
            }
        }).setFormatter(NewLineFormatter.SIMPLE).setOrderable(false).setRequired(true));
        dataSource.addColumn(new SimpleColumn("Шифр", "agreement."+UniscEduAgreementBaseGen.L_CONFIG+"."+UniscEduOrgUnitGen.P_CIPHER,NewLineFormatter.SIMPLE).setOrderable(false).setClickable(false));

        dataSource.addColumn(new SimpleColumn("Формирующее подр.", "agreement."+UniscEduAgreementBaseGen.L_CONFIG+"."+UniscEduOrgUnitGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnit.L_FORMATIVE_ORG_UNIT + "." + OrgUnit.P_FULL_TITLE).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Территориальное подр.", "agreement."+UniscEduAgreementBaseGen.L_CONFIG+"."+UniscEduOrgUnitGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnit.L_TERRITORIAL_ORG_UNIT + "." + OrgUnit.P_TERRITORIAL_FULL_TITLE).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", "agreement."+UniscEduAgreementBaseGen.L_CONFIG+"."+UniscEduOrgUnitGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnit.P_TITLE_WITH_FORM_AND_CONDITION).setOrderable(false).setClickable(false));

        dataSource.addColumn(new SimpleColumn("Описание", "agreement."+UniscEduAgreementBaseGen.L_CONFIG+"."+UniscEduOrgUnitGen.P_DESCRIPTION).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Условия", "agreement."+UniscEduAgreementBaseGen.L_CONFIG+"."+UniscEduOrgUnitGen.P_CONDITIONS).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Способ оплаты", "agreement."+UniscEduAgreementBaseGen.L_PAY_PLAN_FREQ+".title").setOrderable(false).setClickable(false));

        dataSource.addColumn(new SimpleColumn("Тип заказчика", "agreement."+UniscEduAgreementBaseGen.L_CUSTOMER_TYPE+".title").setOrderable(false).setClickable(false));
        dataSource.addColumn(new BlockColumn<StringBuilder>("agreementCustomer", "Заказчик").setOrderable(false).setClickable(false));

        dataSource.addColumn(new SimpleColumn("Действует с", "agreement."+UniscEduAgreementBaseGen.P_FORMING_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Действует до", "agreement."+UniscEduAgreementBaseGen.P_DEADLINE_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Изменен", "agreement."+UniscEduAgreementBaseGen.P_LAST_MODIFICATION_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false));
        dataSource.addColumn(new ToggleColumn("Проверен", "agreement."+UniscEduAgreementBaseGen.P_CHECKED_BY_SIGNATURE).toggleOffListener("onClickCheckBySignature").toggleOnListener("onClickCheckBySignature").setPermissionKey(model.getCheckedBySignaturePermissionKey()));
        dataSource.addColumn(CommonBaseUtil.getPrintColumn("onClickPrintStudentContract", "Печать").setPermissionKey(model.getPrintPermissionKey()));

        this.getModel(component).setStudentContractsDataSource(dataSource);
    }



    public void onClickPrintStudentContract(final IBusinessComponent component) {
        final Long rowId = (Long) component.getListenerParameter();
        this.activateInRoot(component, new ComponentActivator(
                getDao().getPrintComponentName(rowId),
                Collections.singletonMap(PublisherActivator.PUBLISHER_ID_KEY, (Object) getDao().getPrintObjectId(rowId))
        ));
    }

    public void onClickCheckBySignature(final IBusinessComponent component) {
        this.getDao().updateCheckBySignature(this.getModel(component), (Long)component.getListenerParameter());
    }

    public void onClickSearch(final IBusinessComponent component) {
        final M model = this.getModel(component);
        this.getDao().prepare(model);
        model.getStudentContractsDataSource().refresh();
        component.saveSettings();
    }

    public void onClickClear(final IBusinessComponent component) {
        final M model = getModel(component);
        model.getSettings().clear();
        getDao().restoreDefaults(model);
        this.onClickSearch(component);
    }

    public void onClickPrint(final IBusinessComponent component) {
        final Model model = getModel(component);
        final byte[] context = getDao().prepareReport(model, component.getUserContext().getPrincipal().getId());
        final Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(context, "StudentContracts.rtf");
        activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new ParametersMap().add("id", id)));
    }
}
