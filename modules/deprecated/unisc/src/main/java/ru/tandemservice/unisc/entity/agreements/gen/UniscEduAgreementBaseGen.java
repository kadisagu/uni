package ru.tandemservice.unisc.entity.agreements.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.unictr.entity.contractor.ContactPerson;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPrintVersion;
import ru.tandemservice.unisc.entity.catalog.UniscCustomerType;
import ru.tandemservice.unisc.entity.catalog.UniscPayPeriod;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Базовое соглашение
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniscEduAgreementBaseGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase";
    public static final String ENTITY_NAME = "uniscEduAgreementBase";
    public static final int VERSION_HASH = -860275161;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONFIG = "config";
    public static final String P_NUMBER = "number";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String P_DEADLINE_DATE = "deadlineDate";
    public static final String P_LAST_MODIFICATION_DATE = "lastModificationDate";
    public static final String L_DEVELOP_PERIOD_GOS = "developPeriodGos";
    public static final String L_DEVELOP_PERIOD_EDU_PLAN = "developPeriodEduPlan";
    public static final String L_PAY_PLAN_DURATION = "payPlanDuration";
    public static final String L_PAY_PLAN_FREQ = "payPlanFreq";
    public static final String P_FIRST_STAGE = "firstStage";
    public static final String L_CUSTOMER_TYPE = "customerType";
    public static final String L_JURIDICAL_PERSON = "juridicalPerson";
    public static final String P_CHECKED_BY_LAWYER = "checkedByLawyer";
    public static final String P_CHECKED_BY_ACCOUNTING = "checkedByAccounting";
    public static final String P_CHECKED_BY_SIGNATURE = "checkedBySignature";
    public static final String L_PRINT_VERSION = "printVersion";

    private UniscEduOrgUnit _config;     // направление подготовки год / год / шифр
    private String _number;     // Номер
    private Date _formingDate;     // Дата начала действия
    private Date _deadlineDate;     // Дата окончания действия
    private Date _lastModificationDate;     // Дата последнего изменения
    private DevelopPeriod _developPeriodGos;     // срок обучения по ГОС
    private DevelopPeriod _developPeriodEduPlan;     // срок обучения по УП
    private UniscPayPeriod _payPlanDuration;     // срок действия
    private UniscPayPeriod _payPlanFreq;     // способ оплаты
    private int _firstStage;     // первый этап
    private UniscCustomerType _customerType;     // Тип заказчика
    private ContactPerson _juridicalPerson;     // Юридическое лицо
    private boolean _checkedByLawyer;     // Проверен юристами
    private boolean _checkedByAccounting;     // Проверен бухгалтерией
    private boolean _checkedBySignature;     // Подписан
    private UniscEduAgreementPrintVersion _printVersion;     // Текущая версия для печати

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return направление подготовки год / год / шифр. Свойство не может быть null.
     */
    @NotNull
    public UniscEduOrgUnit getConfig()
    {
        return _config;
    }

    /**
     * @param config направление подготовки год / год / шифр. Свойство не может быть null.
     */
    public void setConfig(UniscEduOrgUnit config)
    {
        dirty(_config, config);
        _config = config;
    }

    /**
     * @return Номер. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер. Свойство не может быть null.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Дата начала действия. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата начала действия. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Дата окончания действия.
     */
    public Date getDeadlineDate()
    {
        return _deadlineDate;
    }

    /**
     * @param deadlineDate Дата окончания действия.
     */
    public void setDeadlineDate(Date deadlineDate)
    {
        dirty(_deadlineDate, deadlineDate);
        _deadlineDate = deadlineDate;
    }

    /**
     * @return Дата последнего изменения. Свойство не может быть null.
     */
    @NotNull
    public Date getLastModificationDate()
    {
        return _lastModificationDate;
    }

    /**
     * @param lastModificationDate Дата последнего изменения. Свойство не может быть null.
     */
    public void setLastModificationDate(Date lastModificationDate)
    {
        dirty(_lastModificationDate, lastModificationDate);
        _lastModificationDate = lastModificationDate;
    }

    /**
     * @return срок обучения по ГОС.
     */
    public DevelopPeriod getDevelopPeriodGos()
    {
        return _developPeriodGos;
    }

    /**
     * @param developPeriodGos срок обучения по ГОС.
     */
    public void setDevelopPeriodGos(DevelopPeriod developPeriodGos)
    {
        dirty(_developPeriodGos, developPeriodGos);
        _developPeriodGos = developPeriodGos;
    }

    /**
     * @return срок обучения по УП.
     */
    public DevelopPeriod getDevelopPeriodEduPlan()
    {
        return _developPeriodEduPlan;
    }

    /**
     * @param developPeriodEduPlan срок обучения по УП.
     */
    public void setDevelopPeriodEduPlan(DevelopPeriod developPeriodEduPlan)
    {
        dirty(_developPeriodEduPlan, developPeriodEduPlan);
        _developPeriodEduPlan = developPeriodEduPlan;
    }

    /**
     * @return срок действия. Свойство не может быть null.
     */
    @NotNull
    public UniscPayPeriod getPayPlanDuration()
    {
        return _payPlanDuration;
    }

    /**
     * @param payPlanDuration срок действия. Свойство не может быть null.
     */
    public void setPayPlanDuration(UniscPayPeriod payPlanDuration)
    {
        dirty(_payPlanDuration, payPlanDuration);
        _payPlanDuration = payPlanDuration;
    }

    /**
     * @return способ оплаты. Свойство не может быть null.
     */
    @NotNull
    public UniscPayPeriod getPayPlanFreq()
    {
        return _payPlanFreq;
    }

    /**
     * @param payPlanFreq способ оплаты. Свойство не может быть null.
     */
    public void setPayPlanFreq(UniscPayPeriod payPlanFreq)
    {
        dirty(_payPlanFreq, payPlanFreq);
        _payPlanFreq = payPlanFreq;
    }

    /**
     * @return первый этап. Свойство не может быть null.
     */
    @NotNull
    public int getFirstStage()
    {
        return _firstStage;
    }

    /**
     * @param firstStage первый этап. Свойство не может быть null.
     */
    public void setFirstStage(int firstStage)
    {
        dirty(_firstStage, firstStage);
        _firstStage = firstStage;
    }

    /**
     * @return Тип заказчика. Свойство не может быть null.
     */
    @NotNull
    public UniscCustomerType getCustomerType()
    {
        return _customerType;
    }

    /**
     * @param customerType Тип заказчика. Свойство не может быть null.
     */
    public void setCustomerType(UniscCustomerType customerType)
    {
        dirty(_customerType, customerType);
        _customerType = customerType;
    }

    /**
     * @return Юридическое лицо.
     */
    public ContactPerson getJuridicalPerson()
    {
        return _juridicalPerson;
    }

    /**
     * @param juridicalPerson Юридическое лицо.
     */
    public void setJuridicalPerson(ContactPerson juridicalPerson)
    {
        dirty(_juridicalPerson, juridicalPerson);
        _juridicalPerson = juridicalPerson;
    }

    /**
     * @return Проверен юристами. Свойство не может быть null.
     */
    @NotNull
    public boolean isCheckedByLawyer()
    {
        return _checkedByLawyer;
    }

    /**
     * @param checkedByLawyer Проверен юристами. Свойство не может быть null.
     */
    public void setCheckedByLawyer(boolean checkedByLawyer)
    {
        dirty(_checkedByLawyer, checkedByLawyer);
        _checkedByLawyer = checkedByLawyer;
    }

    /**
     * @return Проверен бухгалтерией. Свойство не может быть null.
     */
    @NotNull
    public boolean isCheckedByAccounting()
    {
        return _checkedByAccounting;
    }

    /**
     * @param checkedByAccounting Проверен бухгалтерией. Свойство не может быть null.
     */
    public void setCheckedByAccounting(boolean checkedByAccounting)
    {
        dirty(_checkedByAccounting, checkedByAccounting);
        _checkedByAccounting = checkedByAccounting;
    }

    /**
     * @return Подписан. Свойство не может быть null.
     */
    @NotNull
    public boolean isCheckedBySignature()
    {
        return _checkedBySignature;
    }

    /**
     * @param checkedBySignature Подписан. Свойство не может быть null.
     */
    public void setCheckedBySignature(boolean checkedBySignature)
    {
        dirty(_checkedBySignature, checkedBySignature);
        _checkedBySignature = checkedBySignature;
    }

    /**
     * @return Текущая версия для печати.
     */
    public UniscEduAgreementPrintVersion getPrintVersion()
    {
        return _printVersion;
    }

    /**
     * @param printVersion Текущая версия для печати.
     */
    public void setPrintVersion(UniscEduAgreementPrintVersion printVersion)
    {
        dirty(_printVersion, printVersion);
        _printVersion = printVersion;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UniscEduAgreementBaseGen)
        {
            setConfig(((UniscEduAgreementBase)another).getConfig());
            setNumber(((UniscEduAgreementBase)another).getNumber());
            setFormingDate(((UniscEduAgreementBase)another).getFormingDate());
            setDeadlineDate(((UniscEduAgreementBase)another).getDeadlineDate());
            setLastModificationDate(((UniscEduAgreementBase)another).getLastModificationDate());
            setDevelopPeriodGos(((UniscEduAgreementBase)another).getDevelopPeriodGos());
            setDevelopPeriodEduPlan(((UniscEduAgreementBase)another).getDevelopPeriodEduPlan());
            setPayPlanDuration(((UniscEduAgreementBase)another).getPayPlanDuration());
            setPayPlanFreq(((UniscEduAgreementBase)another).getPayPlanFreq());
            setFirstStage(((UniscEduAgreementBase)another).getFirstStage());
            setCustomerType(((UniscEduAgreementBase)another).getCustomerType());
            setJuridicalPerson(((UniscEduAgreementBase)another).getJuridicalPerson());
            setCheckedByLawyer(((UniscEduAgreementBase)another).isCheckedByLawyer());
            setCheckedByAccounting(((UniscEduAgreementBase)another).isCheckedByAccounting());
            setCheckedBySignature(((UniscEduAgreementBase)another).isCheckedBySignature());
            setPrintVersion(((UniscEduAgreementBase)another).getPrintVersion());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniscEduAgreementBaseGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniscEduAgreementBase.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("UniscEduAgreementBase is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "config":
                    return obj.getConfig();
                case "number":
                    return obj.getNumber();
                case "formingDate":
                    return obj.getFormingDate();
                case "deadlineDate":
                    return obj.getDeadlineDate();
                case "lastModificationDate":
                    return obj.getLastModificationDate();
                case "developPeriodGos":
                    return obj.getDevelopPeriodGos();
                case "developPeriodEduPlan":
                    return obj.getDevelopPeriodEduPlan();
                case "payPlanDuration":
                    return obj.getPayPlanDuration();
                case "payPlanFreq":
                    return obj.getPayPlanFreq();
                case "firstStage":
                    return obj.getFirstStage();
                case "customerType":
                    return obj.getCustomerType();
                case "juridicalPerson":
                    return obj.getJuridicalPerson();
                case "checkedByLawyer":
                    return obj.isCheckedByLawyer();
                case "checkedByAccounting":
                    return obj.isCheckedByAccounting();
                case "checkedBySignature":
                    return obj.isCheckedBySignature();
                case "printVersion":
                    return obj.getPrintVersion();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "config":
                    obj.setConfig((UniscEduOrgUnit) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "deadlineDate":
                    obj.setDeadlineDate((Date) value);
                    return;
                case "lastModificationDate":
                    obj.setLastModificationDate((Date) value);
                    return;
                case "developPeriodGos":
                    obj.setDevelopPeriodGos((DevelopPeriod) value);
                    return;
                case "developPeriodEduPlan":
                    obj.setDevelopPeriodEduPlan((DevelopPeriod) value);
                    return;
                case "payPlanDuration":
                    obj.setPayPlanDuration((UniscPayPeriod) value);
                    return;
                case "payPlanFreq":
                    obj.setPayPlanFreq((UniscPayPeriod) value);
                    return;
                case "firstStage":
                    obj.setFirstStage((Integer) value);
                    return;
                case "customerType":
                    obj.setCustomerType((UniscCustomerType) value);
                    return;
                case "juridicalPerson":
                    obj.setJuridicalPerson((ContactPerson) value);
                    return;
                case "checkedByLawyer":
                    obj.setCheckedByLawyer((Boolean) value);
                    return;
                case "checkedByAccounting":
                    obj.setCheckedByAccounting((Boolean) value);
                    return;
                case "checkedBySignature":
                    obj.setCheckedBySignature((Boolean) value);
                    return;
                case "printVersion":
                    obj.setPrintVersion((UniscEduAgreementPrintVersion) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "config":
                        return true;
                case "number":
                        return true;
                case "formingDate":
                        return true;
                case "deadlineDate":
                        return true;
                case "lastModificationDate":
                        return true;
                case "developPeriodGos":
                        return true;
                case "developPeriodEduPlan":
                        return true;
                case "payPlanDuration":
                        return true;
                case "payPlanFreq":
                        return true;
                case "firstStage":
                        return true;
                case "customerType":
                        return true;
                case "juridicalPerson":
                        return true;
                case "checkedByLawyer":
                        return true;
                case "checkedByAccounting":
                        return true;
                case "checkedBySignature":
                        return true;
                case "printVersion":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "config":
                    return true;
                case "number":
                    return true;
                case "formingDate":
                    return true;
                case "deadlineDate":
                    return true;
                case "lastModificationDate":
                    return true;
                case "developPeriodGos":
                    return true;
                case "developPeriodEduPlan":
                    return true;
                case "payPlanDuration":
                    return true;
                case "payPlanFreq":
                    return true;
                case "firstStage":
                    return true;
                case "customerType":
                    return true;
                case "juridicalPerson":
                    return true;
                case "checkedByLawyer":
                    return true;
                case "checkedByAccounting":
                    return true;
                case "checkedBySignature":
                    return true;
                case "printVersion":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "config":
                    return UniscEduOrgUnit.class;
                case "number":
                    return String.class;
                case "formingDate":
                    return Date.class;
                case "deadlineDate":
                    return Date.class;
                case "lastModificationDate":
                    return Date.class;
                case "developPeriodGos":
                    return DevelopPeriod.class;
                case "developPeriodEduPlan":
                    return DevelopPeriod.class;
                case "payPlanDuration":
                    return UniscPayPeriod.class;
                case "payPlanFreq":
                    return UniscPayPeriod.class;
                case "firstStage":
                    return Integer.class;
                case "customerType":
                    return UniscCustomerType.class;
                case "juridicalPerson":
                    return ContactPerson.class;
                case "checkedByLawyer":
                    return Boolean.class;
                case "checkedByAccounting":
                    return Boolean.class;
                case "checkedBySignature":
                    return Boolean.class;
                case "printVersion":
                    return UniscEduAgreementPrintVersion.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniscEduAgreementBase> _dslPath = new Path<UniscEduAgreementBase>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniscEduAgreementBase");
    }
            

    /**
     * @return направление подготовки год / год / шифр. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase#getConfig()
     */
    public static UniscEduOrgUnit.Path<UniscEduOrgUnit> config()
    {
        return _dslPath.config();
    }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Дата начала действия. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Дата окончания действия.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase#getDeadlineDate()
     */
    public static PropertyPath<Date> deadlineDate()
    {
        return _dslPath.deadlineDate();
    }

    /**
     * @return Дата последнего изменения. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase#getLastModificationDate()
     */
    public static PropertyPath<Date> lastModificationDate()
    {
        return _dslPath.lastModificationDate();
    }

    /**
     * @return срок обучения по ГОС.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase#getDevelopPeriodGos()
     */
    public static DevelopPeriod.Path<DevelopPeriod> developPeriodGos()
    {
        return _dslPath.developPeriodGos();
    }

    /**
     * @return срок обучения по УП.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase#getDevelopPeriodEduPlan()
     */
    public static DevelopPeriod.Path<DevelopPeriod> developPeriodEduPlan()
    {
        return _dslPath.developPeriodEduPlan();
    }

    /**
     * @return срок действия. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase#getPayPlanDuration()
     */
    public static UniscPayPeriod.Path<UniscPayPeriod> payPlanDuration()
    {
        return _dslPath.payPlanDuration();
    }

    /**
     * @return способ оплаты. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase#getPayPlanFreq()
     */
    public static UniscPayPeriod.Path<UniscPayPeriod> payPlanFreq()
    {
        return _dslPath.payPlanFreq();
    }

    /**
     * @return первый этап. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase#getFirstStage()
     */
    public static PropertyPath<Integer> firstStage()
    {
        return _dslPath.firstStage();
    }

    /**
     * @return Тип заказчика. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase#getCustomerType()
     */
    public static UniscCustomerType.Path<UniscCustomerType> customerType()
    {
        return _dslPath.customerType();
    }

    /**
     * @return Юридическое лицо.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase#getJuridicalPerson()
     */
    public static ContactPerson.Path<ContactPerson> juridicalPerson()
    {
        return _dslPath.juridicalPerson();
    }

    /**
     * @return Проверен юристами. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase#isCheckedByLawyer()
     */
    public static PropertyPath<Boolean> checkedByLawyer()
    {
        return _dslPath.checkedByLawyer();
    }

    /**
     * @return Проверен бухгалтерией. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase#isCheckedByAccounting()
     */
    public static PropertyPath<Boolean> checkedByAccounting()
    {
        return _dslPath.checkedByAccounting();
    }

    /**
     * @return Подписан. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase#isCheckedBySignature()
     */
    public static PropertyPath<Boolean> checkedBySignature()
    {
        return _dslPath.checkedBySignature();
    }

    /**
     * @return Текущая версия для печати.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase#getPrintVersion()
     */
    public static UniscEduAgreementPrintVersion.Path<UniscEduAgreementPrintVersion> printVersion()
    {
        return _dslPath.printVersion();
    }

    public static class Path<E extends UniscEduAgreementBase> extends EntityPath<E>
    {
        private UniscEduOrgUnit.Path<UniscEduOrgUnit> _config;
        private PropertyPath<String> _number;
        private PropertyPath<Date> _formingDate;
        private PropertyPath<Date> _deadlineDate;
        private PropertyPath<Date> _lastModificationDate;
        private DevelopPeriod.Path<DevelopPeriod> _developPeriodGos;
        private DevelopPeriod.Path<DevelopPeriod> _developPeriodEduPlan;
        private UniscPayPeriod.Path<UniscPayPeriod> _payPlanDuration;
        private UniscPayPeriod.Path<UniscPayPeriod> _payPlanFreq;
        private PropertyPath<Integer> _firstStage;
        private UniscCustomerType.Path<UniscCustomerType> _customerType;
        private ContactPerson.Path<ContactPerson> _juridicalPerson;
        private PropertyPath<Boolean> _checkedByLawyer;
        private PropertyPath<Boolean> _checkedByAccounting;
        private PropertyPath<Boolean> _checkedBySignature;
        private UniscEduAgreementPrintVersion.Path<UniscEduAgreementPrintVersion> _printVersion;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return направление подготовки год / год / шифр. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase#getConfig()
     */
        public UniscEduOrgUnit.Path<UniscEduOrgUnit> config()
        {
            if(_config == null )
                _config = new UniscEduOrgUnit.Path<UniscEduOrgUnit>(L_CONFIG, this);
            return _config;
        }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(UniscEduAgreementBaseGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Дата начала действия. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(UniscEduAgreementBaseGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Дата окончания действия.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase#getDeadlineDate()
     */
        public PropertyPath<Date> deadlineDate()
        {
            if(_deadlineDate == null )
                _deadlineDate = new PropertyPath<Date>(UniscEduAgreementBaseGen.P_DEADLINE_DATE, this);
            return _deadlineDate;
        }

    /**
     * @return Дата последнего изменения. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase#getLastModificationDate()
     */
        public PropertyPath<Date> lastModificationDate()
        {
            if(_lastModificationDate == null )
                _lastModificationDate = new PropertyPath<Date>(UniscEduAgreementBaseGen.P_LAST_MODIFICATION_DATE, this);
            return _lastModificationDate;
        }

    /**
     * @return срок обучения по ГОС.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase#getDevelopPeriodGos()
     */
        public DevelopPeriod.Path<DevelopPeriod> developPeriodGos()
        {
            if(_developPeriodGos == null )
                _developPeriodGos = new DevelopPeriod.Path<DevelopPeriod>(L_DEVELOP_PERIOD_GOS, this);
            return _developPeriodGos;
        }

    /**
     * @return срок обучения по УП.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase#getDevelopPeriodEduPlan()
     */
        public DevelopPeriod.Path<DevelopPeriod> developPeriodEduPlan()
        {
            if(_developPeriodEduPlan == null )
                _developPeriodEduPlan = new DevelopPeriod.Path<DevelopPeriod>(L_DEVELOP_PERIOD_EDU_PLAN, this);
            return _developPeriodEduPlan;
        }

    /**
     * @return срок действия. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase#getPayPlanDuration()
     */
        public UniscPayPeriod.Path<UniscPayPeriod> payPlanDuration()
        {
            if(_payPlanDuration == null )
                _payPlanDuration = new UniscPayPeriod.Path<UniscPayPeriod>(L_PAY_PLAN_DURATION, this);
            return _payPlanDuration;
        }

    /**
     * @return способ оплаты. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase#getPayPlanFreq()
     */
        public UniscPayPeriod.Path<UniscPayPeriod> payPlanFreq()
        {
            if(_payPlanFreq == null )
                _payPlanFreq = new UniscPayPeriod.Path<UniscPayPeriod>(L_PAY_PLAN_FREQ, this);
            return _payPlanFreq;
        }

    /**
     * @return первый этап. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase#getFirstStage()
     */
        public PropertyPath<Integer> firstStage()
        {
            if(_firstStage == null )
                _firstStage = new PropertyPath<Integer>(UniscEduAgreementBaseGen.P_FIRST_STAGE, this);
            return _firstStage;
        }

    /**
     * @return Тип заказчика. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase#getCustomerType()
     */
        public UniscCustomerType.Path<UniscCustomerType> customerType()
        {
            if(_customerType == null )
                _customerType = new UniscCustomerType.Path<UniscCustomerType>(L_CUSTOMER_TYPE, this);
            return _customerType;
        }

    /**
     * @return Юридическое лицо.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase#getJuridicalPerson()
     */
        public ContactPerson.Path<ContactPerson> juridicalPerson()
        {
            if(_juridicalPerson == null )
                _juridicalPerson = new ContactPerson.Path<ContactPerson>(L_JURIDICAL_PERSON, this);
            return _juridicalPerson;
        }

    /**
     * @return Проверен юристами. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase#isCheckedByLawyer()
     */
        public PropertyPath<Boolean> checkedByLawyer()
        {
            if(_checkedByLawyer == null )
                _checkedByLawyer = new PropertyPath<Boolean>(UniscEduAgreementBaseGen.P_CHECKED_BY_LAWYER, this);
            return _checkedByLawyer;
        }

    /**
     * @return Проверен бухгалтерией. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase#isCheckedByAccounting()
     */
        public PropertyPath<Boolean> checkedByAccounting()
        {
            if(_checkedByAccounting == null )
                _checkedByAccounting = new PropertyPath<Boolean>(UniscEduAgreementBaseGen.P_CHECKED_BY_ACCOUNTING, this);
            return _checkedByAccounting;
        }

    /**
     * @return Подписан. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase#isCheckedBySignature()
     */
        public PropertyPath<Boolean> checkedBySignature()
        {
            if(_checkedBySignature == null )
                _checkedBySignature = new PropertyPath<Boolean>(UniscEduAgreementBaseGen.P_CHECKED_BY_SIGNATURE, this);
            return _checkedBySignature;
        }

    /**
     * @return Текущая версия для печати.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase#getPrintVersion()
     */
        public UniscEduAgreementPrintVersion.Path<UniscEduAgreementPrintVersion> printVersion()
        {
            if(_printVersion == null )
                _printVersion = new UniscEduAgreementPrintVersion.Path<UniscEduAgreementPrintVersion>(L_PRINT_VERSION, this);
            return _printVersion;
        }

        public Class getEntityClass()
        {
            return UniscEduAgreementBase.class;
        }

        public String getEntityName()
        {
            return "uniscEduAgreementBase";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
