package ru.tandemservice.unisc.component.agreement.AdditionalAgreementAddEdit;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressRu;
import org.tandemframework.shared.fias.utils.AddressBaseUtils;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisc.dao.agreement.IUniscEduAgreementDAO;
import ru.tandemservice.unisc.entity.agreements.IUniscEduAgreement2PersonRole;
import ru.tandemservice.unisc.entity.agreements.UniscEduAdditAgreement;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson;
import ru.tandemservice.unisc.entity.agreements.UniscEduMainAgreement;
import ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student;
import ru.tandemservice.unisc.entity.agreements.student.gen.UniscEduAgreement2StudentGen;

import java.util.Iterator;
import java.util.List;


/**
 * @author vdanilov
 */
public class DAO extends ru.tandemservice.unisc.component.agreement.AgreementAddEdit.DAO<Model> implements IDAO {

    @SuppressWarnings("unchecked")
    @Override public void prepare(final Model model) {
        if (null == model.getAgreement()) {
            final IEntityMeta meta = EntityRuntime.getMeta(model.getId());
            if (null == meta) { throw new IllegalStateException(String.valueOf(model.getId())); }

            final Class entityClass = meta.getEntityClass();
            if (UniscEduAdditAgreement.class.isAssignableFrom(entityClass)) {
                model.setAgreement(this.get(UniscEduAdditAgreement.class, model.getId()));

            } else if (UniscEduAgreement2Student.class.isAssignableFrom(entityClass)) {
                final UniscEduMainAgreement parent = this.get(UniscEduAgreement2Student.class, model.getId()).getAgreement();
                this.prepareAdditionalAgreement(model, parent);

            } else if (Student.class.isAssignableFrom(entityClass)) {
                final UniscEduAgreement2Student uniscEduAgreement2Student = (UniscEduAgreement2Student)this.getSession().createCriteria(UniscEduAgreement2StudentGen.ENTITY_CLASS)
                .add(Restrictions.eq(UniscEduAgreement2StudentGen.L_STUDENT, this.get(model.getId())))
                .add(Restrictions.eq(UniscEduAgreement2StudentGen.P_ACTIVE, Boolean.TRUE))
                .uniqueResult();
                if (null == uniscEduAgreement2Student) {
                    throw new ApplicationException("Вы не можете зарегистрировать дополнительное соглашение, так как у студента нет действующих договоров.");
                }

                final UniscEduMainAgreement parent = uniscEduAgreement2Student.getAgreement();
                this.prepareAdditionalAgreement(model, parent);

            } else {
                throw new IllegalStateException(entityClass.getName());
            }
        } else {
            this.reassociate(model);
        }

        model.setId(model.getAgreement().getId());

        /* invalidate role */ {
            final IUniscEduAgreement2PersonRole<PersonRole> rel = IUniscEduAgreementDAO.INSTANCE.get().getRelation(model.getAgreement().getAgreement());
            if (null != rel) {
                model.setPersonRole(rel.getTarget());
            }
        }

        super.prepare(model);
    }

    protected void prepareAdditionalAgreement(final Model model, final UniscEduMainAgreement parent) {
        final UniscEduAdditAgreement aa = new UniscEduAdditAgreement();
        aa.setAgreement(parent);
        aa.setConfig(aa.getAgreement().getConfig());
        aa.setCustomerType(aa.getAgreement().getCustomerType());
        aa.setPayPlanFreq(aa.getAgreement().getPayPlanFreq());
        aa.setPayPlanDuration(aa.getAgreement().getPayPlanDuration());
        aa.setFirstStage(1+aa.getAgreement().getLastStage());
        aa.setJuridicalPerson(aa.getJuridicalPerson());

        model.setAgreement(aa);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<UniscEduAgreementNaturalPerson> getDefaultNaturalPersonList(final Model model) {
        final Iterator<Long> it = new Iterator<Long>() {
            long i = 0;
            @Override public boolean hasNext() { return true; }
            @Override public Long next() { return this.i++; }
            @Override public void remove() {}
        };
        return (List<UniscEduAgreementNaturalPerson>) CollectionUtils.collect(IUniscEduAgreementDAO.INSTANCE.get().getNaturalPersons(model.getAgreement().getAgreement()), input -> {
            final UniscEduAgreementNaturalPerson src = (UniscEduAgreementNaturalPerson)input;
            final UniscEduAgreementNaturalPerson result = new UniscEduAgreementNaturalPerson();
            result.update(src);
            result.setId(it.next());
            result.setOwner(null);
            if (null != src.getAddress())
            {
                AddressDetailed newAddress = (AddressDetailed)AddressBaseUtils.getSameAddress(src.getAddress());
                result.setAddress(newAddress);
            }
            else
            {
                AddressRu newAddress = new AddressRu();
                newAddress.setCountry(get(AddressCountry.class, AddressCountry.code(), IKladrDefines.RUSSIA_COUNTRY_CODE));
                result.setAddress(newAddress);
            }

            return result;
        });
    }

}
