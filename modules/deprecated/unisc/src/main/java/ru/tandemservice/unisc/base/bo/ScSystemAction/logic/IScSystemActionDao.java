/**
 *$Id$
 */
package ru.tandemservice.unisc.base.bo.ScSystemAction.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

/**
 * @author Alexander Shaburov
 * @since 13.02.13
 */
public interface IScSystemActionDao extends INeedPersistenceSupport
{
    public void deleteContractWithoutLinks();

    /** @param agreement2StudentId */
    @Transactional(propagation=Propagation.REQUIRED)
    public void doPrintRelation(Long agreement2StudentId);
}
