package ru.tandemservice.unisc.component.report.AgreementIncomeSummaryReport;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.unisc.component.report.PaymentReport.MQBuilderFactory;
import ru.tandemservice.unisc.dao.payment.IUniscEduAgreementPaymentDAO;
import ru.tandemservice.unisc.dao.payment.IUniscEduAgreementPaymentState;
import ru.tandemservice.unisc.entity.agreements.UniscEduAdditAgreement;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayPlanRow;
import ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.Boolean;
import java.util.*;

/**
 * @author oleyba
 * @since 07.09.2009
 */
public class AgreementIncomeSummaryReportGenerator
{
    protected Session session;
    protected Long orgUnitId;
    protected MQBuilderFactory agreementBuilder;
    protected Date dateFrom;
    protected Date dateTo;

    protected Map<Long, StudentWrapper> studentByAgreementId = new HashMap<>();
    protected Map<Long, StudentRow> reportRowByStudentId = new HashMap<>();
    protected Map<Long, GroupRow> reportRowByGroupId = new HashMap<>();
    protected Map<MultiKey, Set<GroupRow>> reportTable = new HashMap<MultiKey, Set<GroupRow>>()
    {
        private static final long serialVersionUID = 1L;

        @Override
        public Set<GroupRow> get(Object key)
        {
            Set<GroupRow> result = super.get(key);
            if (result == null)
                put((MultiKey) key, result = new HashSet<>());
            return result;
        }
    };

    protected List<OrgUnit> formativeOrgUnitList = new ArrayList<>();
    protected List<OrgUnit> territorialOrgUnitList = new ArrayList<>();

    public AgreementIncomeSummaryReportGenerator(Session session, Long orgUnitId, MQBuilderFactory agreementBuilder, Date dateFrom, Date dateTo)
    {
        this.session = session;
        this.orgUnitId = orgUnitId;
        this.agreementBuilder = agreementBuilder;

        Calendar beginDay = Calendar.getInstance();
        beginDay.setTime(dateFrom);
        beginDay.set(Calendar.HOUR_OF_DAY, 0);
        beginDay.set(Calendar.MINUTE, 0);
        beginDay.set(Calendar.SECOND, 0);
        beginDay.set(Calendar.MILLISECOND, 0);

        Calendar endDay = Calendar.getInstance();
        endDay.setTime(dateTo);
        endDay.set(Calendar.HOUR_OF_DAY, 23);
        endDay.set(Calendar.MINUTE, 59);
        endDay.set(Calendar.SECOND, 59);
        endDay.set(Calendar.MILLISECOND, 999);

        this.dateFrom = beginDay.getTime();
        this.dateTo = endDay.getTime();
    }

    @SuppressWarnings({"unchecked"})
    public DatabaseFile generateReportContent() throws WriteException, IOException
    {
        init();
        processAgreements();

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        WritableWorkbook workbook = Workbook.createWorkbook(out, ws);

        // create fonts
        WritableFont arial12bold = new WritableFont(WritableFont.ARIAL, 12, WritableFont.BOLD);
        WritableFont arial10 = new WritableFont(WritableFont.ARIAL, 10);
        WritableFont arial10bold = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);

        // create cell formats
        WritableCellFormat headerFormat = new WritableCellFormat(arial12bold);
        headerFormat.setAlignment(Alignment.CENTRE);

        WritableCellFormat subHeaderFormat = new WritableCellFormat(arial10bold);
        subHeaderFormat.setAlignment(Alignment.CENTRE);

        WritableCellFormat tableHeaderFormat = new WritableCellFormat(arial10bold);
        tableHeaderFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.MEDIUM, Colour.BLACK);
        tableHeaderFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        tableHeaderFormat.setAlignment(Alignment.CENTRE);
        tableHeaderFormat.setWrap(true);

        WritableCellFormat rowFormat = new WritableCellFormat(arial10);
        rowFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        rowFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        rowFormat.setWrap(true);

        WritableCellFormat integerFormat = new WritableCellFormat(arial10, NumberFormats.INTEGER);
        integerFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        integerFormat.setAlignment(Alignment.CENTRE);
        integerFormat.setVerticalAlignment(VerticalAlignment.CENTRE);

        WritableCellFormat doubleFormat = new WritableCellFormat(arial10, NumberFormats.FLOAT);
        doubleFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        doubleFormat.setAlignment(Alignment.CENTRE);
        doubleFormat.setVerticalAlignment(VerticalAlignment.CENTRE);

        WritableCellFormat totalsUnborderedFormat = new WritableCellFormat(arial10bold);
        totalsUnborderedFormat.setAlignment(Alignment.LEFT);
        totalsUnborderedFormat.setVerticalAlignment(VerticalAlignment.CENTRE);

        WritableCellFormat totalsFormat = new WritableCellFormat(arial10bold);
        totalsFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.MEDIUM, Colour.BLACK);
        totalsFormat.setAlignment(Alignment.LEFT);
        totalsFormat.setVerticalAlignment(VerticalAlignment.CENTRE);

        WritableCellFormat totalsDoubleFormat = new WritableCellFormat(arial10, NumberFormats.FLOAT);
        totalsDoubleFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.MEDIUM, Colour.BLACK);
        totalsDoubleFormat.setAlignment(Alignment.CENTRE);
        totalsDoubleFormat.setVerticalAlignment(VerticalAlignment.CENTRE);

        WritableCellFormat totalsIntegerFormat = new WritableCellFormat(arial10, NumberFormats.INTEGER);
        totalsIntegerFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.MEDIUM, Colour.BLACK);
        totalsIntegerFormat.setAlignment(Alignment.CENTRE);
        totalsIntegerFormat.setVerticalAlignment(VerticalAlignment.CENTRE);

        {
            // create list
            WritableSheet groupSheet = workbook.createSheet("По группам", 0);
            groupSheet.getSettings().setCopies(1);

            // ширина колонок в пикселях
            int[] columnWidth = new int[]{63, 77, 98, 77, 70, 84};
            for (int i = 0; i < columnWidth.length; i++)
                groupSheet.setColumnView(i, columnWidth[i] / 7);
            groupSheet.setRowView(1, 115);
            groupSheet.setRowView(3, 560);

            groupSheet.addCell(new Label(0, 0, "Сводка поступлений по договорам", headerFormat));
            groupSheet.mergeCells(0, 0, 5, 0);

            groupSheet.addCell(new Label(0, 2, "ЗА ПЕРИОД " + DateFormatter.DEFAULT_DATE_FORMATTER.format(dateFrom) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(dateTo), subHeaderFormat));
            groupSheet.mergeCells(0, 2, 5, 2);

            String[] columnTitles = new String[]{"Группа", "Кол-во студентов", "Сумма к оплате", "Оплачено", "Долг", "Остаток после даты"};
            for (int i = 0; i < columnTitles.length; i++)
                groupSheet.addCell(new Label(i, 3, columnTitles[i], tableHeaderFormat));
            int row = 4;
            int studentCount = 0;
            double sum = 0;
            double debt = 0;
            double overpay = 0;
            for (OrgUnit formative : formativeOrgUnitList)
                for (OrgUnit territorial : territorialOrgUnitList)
                {
                    Long formId = formative.getId();
                    Long terrId = territorial == null ? null : territorial.getId();
                    MultiKey eduOuKey = new MultiKey(formId, terrId);
                    List<GroupRow> groupList = new ArrayList<>(reportTable.get(eduOuKey));
                    if (groupList.isEmpty()) continue;
                    String title = formative.getShortTitle() + (territorial == null ? "" : " (" + (territorial.getNominativeCaseTitle() == null ? territorial.getTitle() : territorial.getNominativeCaseTitle()) + ")");
                    groupSheet.addCell(new Label(0, row, title, rowFormat));
                    groupSheet.mergeCells(0, row, 5, row++);
                    Collections.sort(groupList);
                    for (GroupRow groupRow : groupList)
                    {
                        groupSheet.addCell(new Label(0, row, groupRow.getGroup().title, rowFormat));
                        groupSheet.addCell(new jxl.write.Number(1, row, groupRow.studentRows.size(), integerFormat));
                        groupSheet.addCell(new jxl.write.Number(2, row, groupRow.sum, doubleFormat));
                        groupSheet.addCell(new jxl.write.Number(3, row, groupRow.sum - groupRow.debt + groupRow.overpay, doubleFormat));
                        groupSheet.addCell(new jxl.write.Number(4, row, groupRow.debt, doubleFormat));
                        groupSheet.addCell(new jxl.write.Number(5, row++, -groupRow.overpay, doubleFormat));
                        studentCount = studentCount + groupRow.studentRows.size();
                        sum = sum + groupRow.sum;
                        debt = debt + groupRow.debt;
                        overpay = overpay + groupRow.overpay;
                    }
                }
            groupSheet.addCell(new Label(0, row, "Итого:", totalsUnborderedFormat));
            groupSheet.addCell(new jxl.write.Number(1, row, studentCount, totalsIntegerFormat));
            groupSheet.addCell(new jxl.write.Number(2, row, sum, totalsDoubleFormat));
            groupSheet.addCell(new jxl.write.Number(3, row, sum - debt + overpay, totalsDoubleFormat));
            groupSheet.addCell(new jxl.write.Number(4, row, debt, totalsDoubleFormat));
            groupSheet.addCell(new jxl.write.Number(5, row, -overpay, totalsDoubleFormat));
        }

        {
            // create list
            WritableSheet studentSheet = workbook.createSheet("По студентам", 1);
            studentSheet.getSettings().setCopies(1);

            // ширина колонок в пикселях
            int[] columnWidth = new int[]{224, 91, 84, 98, 63, 63, 91, 77};
            for (int i = 0; i < columnWidth.length; i++)
                studentSheet.setColumnView(i, columnWidth[i] / 7);
            studentSheet.setRowView(1, 115);
            studentSheet.setRowView(3, 560);

            studentSheet.addCell(new Label(0, 0, "Сводная ведомость студентов", headerFormat));
            studentSheet.mergeCells(0, 0, 7, 0);

            studentSheet.addCell(new Label(0, 2, "ЗА ПЕРИОД " + DateFormatter.DEFAULT_DATE_FORMATTER.format(dateFrom) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(dateTo), subHeaderFormat));
            studentSheet.mergeCells(0, 2, 7, 2);

            String[] columnTitles = new String[]{"Студент", "Формирующее подр.", "Территориальное подр.", "Направление подготовки", "Курс", "Группа", "Плановая сумма", "Оплачено"};
            for (int i = 0; i < columnTitles.length; i++)
                studentSheet.addCell(new Label(i, 3, columnTitles[i], tableHeaderFormat));
            int row = 4;
            double sum = 0;
            double payed = 0;
            for (OrgUnit formative : formativeOrgUnitList)
                for (OrgUnit territorial : territorialOrgUnitList)
                {
                    Long formId = formative.getId();
                    Long terrId = territorial == null ? null : territorial.getId();
                    MultiKey eduOuKey = new MultiKey(formId, terrId);
                    List<GroupRow> groupList = new ArrayList<>(reportTable.get(eduOuKey));
                    Collections.sort(groupList);
                    for (GroupRow groupRow : groupList)
                    {
                        List<StudentRow> studentList = groupRow.studentRows;
                        Collections.sort(studentList);
                        for (StudentRow studentRow : studentList)
                        {
                            studentSheet.addCell(new Label(0, row, studentRow.student.getFullFio(), rowFormat));
                            studentSheet.addCell(new Label(1, row, studentRow.student.eduOu.getFormativeOrgUnit().getShortTitle(), rowFormat));
                            OrgUnit studentTerritorial = studentRow.student.eduOu.getTerritorialOrgUnit();
                            studentSheet.addCell(new Label(2, row, (studentTerritorial.getNominativeCaseTitle() == null ? studentTerritorial.getTerritorialTitle() : studentTerritorial.getNominativeCaseTitle()), rowFormat));
                            studentSheet.addCell(new Label(3, row, studentRow.student.eduOu.getEducationLevelHighSchool().getEducationLevel().getShortTitle(), rowFormat));
                            studentSheet.addCell(new Label(4, row, studentRow.student.course, rowFormat));
                            studentSheet.addCell(new Label(5, row, studentRow.student.group.title, rowFormat));
                            studentSheet.addCell(new jxl.write.Number(6, row, studentRow.sum, doubleFormat));
                            studentSheet.addCell(new jxl.write.Number(7, row++, studentRow.sum - studentRow.debt + studentRow.overpay, doubleFormat));
                        }
                        studentSheet.addCell(new Label(0, row, "Итого по группе " + groupRow.getGroup().title + ":", totalsFormat));
                        studentSheet.addCell(new Label(1, row, "", rowFormat));
                        studentSheet.addCell(new Label(2, row, "", rowFormat));
                        studentSheet.addCell(new Label(3, row, "", rowFormat));
                        studentSheet.addCell(new Label(4, row, "", rowFormat));
                        studentSheet.addCell(new Label(5, row, "", rowFormat));
                        studentSheet.addCell(new jxl.write.Number(6, row, groupRow.sum, totalsDoubleFormat));
                        studentSheet.addCell(new jxl.write.Number(7, row++, groupRow.sum - groupRow.debt + groupRow.overpay, totalsDoubleFormat));

                        sum = sum + groupRow.sum;
                        payed = payed + groupRow.sum - groupRow.debt + groupRow.overpay;
                    }
                }
            studentSheet.addCell(new Label(0, row, "Итого:", totalsFormat));
            studentSheet.addCell(new Label(1, row, "", totalsFormat));
            studentSheet.addCell(new Label(2, row, "", totalsFormat));
            studentSheet.addCell(new Label(3, row, "", totalsFormat));
            studentSheet.addCell(new Label(4, row, "", totalsFormat));
            studentSheet.addCell(new Label(5, row, "", totalsFormat));
            studentSheet.addCell(new jxl.write.Number(6, row, sum, totalsDoubleFormat));
            studentSheet.addCell(new jxl.write.Number(7, row, payed, totalsDoubleFormat));
        }

        workbook.write();
        workbook.close();

        DatabaseFile content = new DatabaseFile();
        content.setContent(out.toByteArray());
        return content;
    }

    private List<Long> agreementList = new ArrayList<>();

    private void init()
    {
        MQBuilder builder = new MQBuilder(UniscEduAgreement2Student.ENTITY_CLASS, "rel", new String[] {
                /* 0 */ "id",
                /* 1 */ UniscEduAgreement2Student.agreement().id().s()});
        builder.add(MQExpression.in("rel", UniscEduAgreement2Student.L_AGREEMENT + ".id", agreementBuilder.createIdBuilder()));
        builder.addJoin("rel", UniscEduAgreement2Student.L_STUDENT, "student");
        builder.addSelect("student", new Object[] {
                /* 2 */ Student.id().s(),
                /* 3 */ Student.person().identityCard().lastName().s(),
                /* 4 */ Student.person().identityCard().firstName().s(),
                /* 5 */ Student.person().identityCard().middleName().s(),
                /* 6 */ Student.course().title().s(),
                /* 7 */ Student.educationOrgUnit().id().s()});
        builder.addJoin("student", Student.L_GROUP, "grp");
        builder.addSelect("grp", new Object[] {
                /* 8 */ Group.id().s(),
                /* 9 */ Group.title().s(),
                /* 10 */ Group.course().title().s(),
                /* 11 */ Group.educationOrgUnit().id().s()});

        builder.add(MQExpression.eq("grp", Group.P_ARCHIVAL, Boolean.FALSE));
        
        builder.addDomain("pay", UniscEduAgreementPayPlanRow.ENTITY_CLASS);
        MQBuilder add = new MQBuilder(UniscEduAdditAgreement.ENTITY_CLASS, "add");
        add.add(MQExpression.eqProperty("pay", UniscEduAgreementPayPlanRow.owner().id().s(), "add", "id"));
        add.add(MQExpression.eqProperty("add", UniscEduAdditAgreement.agreement().id().s(), "rel", UniscEduAgreement2Student.agreement().id().s()));
        builder.add(MQExpression.or(
                MQExpression.eqProperty("pay", UniscEduAgreementPayPlanRow.owner().id().s(), "rel", UniscEduAgreement2Student.agreement().id().s()),
                MQExpression.exists(add)));

        builder.add(UniMQExpression.betweenDate("pay", UniscEduAgreementPayPlanRow.P_DATE, dateFrom, dateTo));
        builder.add(MQExpression.isNull("pay", UniscEduAgreementPayPlanRow.L_REPLACED_BY));

        List<Object[]> rows = builder.<Object[]>getResultList(session);
        Set<Long> eduOuIds = new HashSet<>();
        for (Object[] row : rows)
        {
            eduOuIds.add((Long) row [7]);
            eduOuIds.add((Long) row [11]);
        }

        MQBuilder eduOrgUnits = new MQBuilder(EducationOrgUnit.ENTITY_CLASS, "ou").add(MQExpression.in("ou", "id", eduOuIds));
        Map<Long, EducationOrgUnit> eduOuMap = new HashMap<>();
        for (EducationOrgUnit ou : eduOrgUnits.<EducationOrgUnit>getResultList(session))
            eduOuMap.put(ou.getId(), ou);

        for (Object[] row : rows)
        {
            Long agreementId = (Long) row[1];
            GroupWrapper group = new GroupWrapper((Long) row[8], (String) row[9], (String) row[10], eduOuMap.get((Long) row[11]));
            studentByAgreementId.put(agreementId, new StudentWrapper((Long) row[2], group, (String) row[3], (String) row[4], (String) row[5], (String) row[6], eduOuMap.get((Long) row[7])));
            agreementList.add(agreementId);
        }
    }

    private void processAgreements()
    {
        Map<Long, IUniscEduAgreementPaymentState> paymentStateMap = IUniscEduAgreementPaymentDAO.INSTANCE.get().getStateMap(agreementList, dateTo);

        Set<OrgUnit> formativeOrgUnits = new HashSet<>();
        Set<OrgUnit> territorialOrgUnits = new HashSet<>();
        for (Long agreement: agreementList)
        {
            IUniscEduAgreementPaymentState state = paymentStateMap.get(agreement);
            StudentWrapper student = studentByAgreementId.get(agreement);
            GroupWrapper group = student.group;
            EducationOrgUnit eduOu = group.eduOu;
            Long formId = eduOu.getFormativeOrgUnit().getId();
            Long terrId = eduOu.getTerritorialOrgUnit().getId();
            MultiKey eduOuKey = new MultiKey(formId, terrId);
            GroupRow groupRow = reportRowByGroupId.get(group.id);
            if (groupRow == null)
                reportRowByGroupId.put(group.id, groupRow = new GroupRow(student.group));
            groupRow.process(state);
            StudentRow studentRow = reportRowByStudentId.get(student.id);
            if (studentRow == null)
                reportRowByStudentId.put(student.id, studentRow = groupRow.addStudent(student));
            studentRow.process(state);
            formativeOrgUnits.add(eduOu.getFormativeOrgUnit());
            territorialOrgUnits.add(eduOu.getTerritorialOrgUnit());
            reportTable.get(eduOuKey).add(groupRow);
        }

        formativeOrgUnitList = new ArrayList<>(formativeOrgUnits);
        Collections.sort(formativeOrgUnitList, ITitled.TITLED_COMPARATOR);
        territorialOrgUnitList = new ArrayList<>(territorialOrgUnits);
        Collections.sort(territorialOrgUnitList, ITitled.TITLED_COMPARATOR);
    }

    protected class GroupRow extends RowBase implements Comparable
    {
        private GroupWrapper group;
        private List<StudentRow> studentRows = new ArrayList<>();

        public GroupRow(GroupWrapper group)
        {
            this.group = group;
        }

        @Override
        public int compareTo(Object o)
        {
            if (group == null) return 1;
            GroupWrapper other = ((GroupRow) o).getGroup();
            if (other == null) return -1;
            int result = group.course.compareTo(other.course);
            if (result == 0)
                result = group.title.compareTo(other.title);
            return result;
        }

        public StudentRow addStudent(StudentWrapper student)
        {
            StudentRow studentRow = new StudentRow(student);
            studentRows.add(studentRow);
            return studentRow;
        }

        public GroupWrapper getGroup() { return group; }
    }

    protected class StudentRow extends RowBase implements Comparable
    {
        private StudentWrapper student;

        public StudentRow(StudentWrapper student)
        {
            this.student = student;
        }

        public void append(List<String[]> reportTable)
        {
            reportTable.add(new String[]{
                    student.getFullFio(),
                    student.course,
                    student.group == null ? "" : student.group.title,
                            DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(sum),
                            DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(sum - debt),
                            DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(debt)
            });
        }

        @Override
        public int compareTo(Object o)
        {
            StudentRow other = (StudentRow) o;
            int order = CommonCollator.RUSSIAN_COLLATOR.compare(student.lastName, other.student.lastName);
            if (order == 0) order = CommonCollator.RUSSIAN_COLLATOR.compare(student.firstName, other.student.firstName);
            if (order == 0) order = CommonCollator.RUSSIAN_COLLATOR.compare(student.middleName, other.student.middleName);
            if (order == 0) order = Long.compare(student.id, other.student.id);
            return order;
        }

        public StudentWrapper getStudent() { return student; }
        @Override
        public double getDebt() { return debt; }
        @Override
        public double getOverpay() { return overpay; }
    }

    protected abstract class RowBase
    {
        protected double sum;
        protected double debt;
        protected double overpay;

        public void process(IUniscEduAgreementPaymentState state)
        {
            for (IUniscEduAgreementPaymentState.StatePlanRow planRow : state.getPlanRows())
                process(planRow);
            for (IUniscEduAgreementPaymentState.StateFactRow factRow : state.getOverpayRows())
                overpay = overpay + factRow.getPayment();
        }

        protected void process(IUniscEduAgreementPaymentState.StatePlanRow planRow)
        {
            if (planRow.getDate().after(dateTo) || planRow.getDate().before(dateFrom))
                return;
            sum = sum + planRow.getCost();
            debt = debt + planRow.getDebt()[0];
        }

        public double getDebt() { return debt; }
        public double getOverpay() { return overpay; }
    }


    private static class GroupWrapper
    {
        GroupWrapper(Long id, String title, String course, EducationOrgUnit eduOu)
        {
            this.id = id;
            this.title = title;
            this.course = course;
            this.eduOu = eduOu;
}

        Long id;
        String title;
        String course;
        EducationOrgUnit eduOu;
    }

    private static class StudentWrapper
    {
        StudentWrapper(Long id, GroupWrapper group, String lastName, String firstName, String middleName, String course, EducationOrgUnit eduOu)
        {
            this.id = id;
            this.group = group;
            this.lastName = lastName;
            this.firstName = firstName;
            this.middleName = middleName;
            this.course = course;
            this.eduOu = eduOu;
        }

        Long id;
        GroupWrapper group;
        String lastName;
        String firstName;
        String middleName;
        String course;
        EducationOrgUnit eduOu;

        String getFullFio()
        {
            StringBuilder str = new StringBuilder().append(lastName);
            if (StringUtils.isNotEmpty(firstName))
                str.append(" ").append(firstName);
            if (StringUtils.isNotEmpty(middleName))
                str.append(" ").append(middleName);
            return str.toString();
        }
    }
}
