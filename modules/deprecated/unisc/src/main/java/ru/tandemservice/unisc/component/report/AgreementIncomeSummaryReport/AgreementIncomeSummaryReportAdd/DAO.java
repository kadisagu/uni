// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisc.component.report.AgreementIncomeSummaryReport.AgreementIncomeSummaryReportAdd;

import org.hibernate.Session;
import org.tandemframework.core.info.ErrorCollector;

import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unisc.component.report.UniscReportAddDaoUtil;
import ru.tandemservice.unisc.component.report.AgreementIncomeSummaryReport.AgreementIncomeSummaryReportGenerator;
import ru.tandemservice.unisc.entity.report.UniscAgreementIncomeSummaryReport;

/**
 * @author oleyba
 * @since 07.09.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        final Session session = getSession();

        if (OrgUnitManager.instance().dao().getTopOrgUnitId().equals(model.getOrgUnitId()))
            model.setOrgUnitId(null);

        UniscReportAddDaoUtil.initSelectModels(model, this, session);
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        if (model.getReport().getDateFrom().after(model.getReport().getDateTo()))
            errors.add("Дата начала периода должна быть не позже даты окончания периода.", "dateFrom", "dateTo");
        if (model.getReport().getDateTo().getTime() - model.getReport().getDateFrom().getTime() >= 31622400000L)
            errors.add("Отчетный период не должен превышать один год.");
    }

    @Override
    public void update(Model model)
    {
        final Session session = getSession();

        UniscAgreementIncomeSummaryReport report = model.getReport();
        model.fillReportParams(report);

        try
        {
            DatabaseFile databaseFile = new AgreementIncomeSummaryReportGenerator(getSession(), model.getOrgUnitId(), UniscReportAddDaoUtil.getAgreementBuilder(model), report.getDateFrom(), report.getDateTo()).generateReportContent();
            session.save(databaseFile);

            report.setContent(databaseFile);
            session.save(report);
        }
        catch (Throwable e)
        {
            throw new RuntimeException(e);
        }
    }
}
