package ru.tandemservice.unisc.component.settings.CostSettings.EditPlan;

import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.hibernate.Session;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlan;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlanRow;
import ru.tandemservice.unisc.entity.config.gen.UniscEduOrgUnitPayPlanRowGen;

public class DAO extends UniDao<Model> implements IDAO {

	@Override
	public void prepare(final Model model) {
		model.setEduOrgUnitPayPlan(this.get(UniscEduOrgUnitPayPlan.class, model.getEduOrgUnitPayPlan().getId()));
	}

	@Override
	public void save(final Model model) {
		final Session session = getSession();
		for (final UniscEduOrgUnitPayPlanRow row: model.getDataSource().getEntityList()) {
			session.saveOrUpdate(row);
		}
	}

	@Override
	public void prepareDataSource(final Model model) {
		final MQBuilder builder = new MQBuilder(UniscEduOrgUnitPayPlanRowGen.ENTITY_CLASS, "row");
		builder.add(MQExpression.eq("row", UniscEduOrgUnitPayPlanRowGen.L_PLAN+".id", model.getEduOrgUnitPayPlan().getId()));
		builder.addOrder("row", UniscEduOrgUnitPayPlanRowGen.P_STAGE);
		builder.addOrder("row", UniscEduOrgUnitPayPlanRowGen.P_DATE);
		final List<UniscEduOrgUnitPayPlanRow> result = builder.getResultList(getSession());
		final DynamicListDataSource<UniscEduOrgUnitPayPlanRow> dataSource = model.getDataSource();
		dataSource.setCountRow(result.size());
		dataSource.setTotalSize(result.size());
		dataSource.createPage(result);
	}

	@Override
	public void doCloneRow(final Model model, final Long rowId) {
		final UniscEduOrgUnitPayPlanRow row = get(UniscEduOrgUnitPayPlanRow.class, rowId);
		if (model.getEduOrgUnitPayPlan().equals(row.getPlan())) {
			final UniscEduOrgUnitPayPlanRow clone = new UniscEduOrgUnitPayPlanRow();
			clone.update(row);
			clone.setDate(DateUtils.addDays(row.getDate(), 1));
			clone.setCost(row.getCost()/2);
			row.setCost(row.getCost() - clone.getCost());
			save(clone);
		}
	}

	@Override
	public void doDeleteRow(final Model model, final Long rowId) {
		final UniscEduOrgUnitPayPlanRow row = get(UniscEduOrgUnitPayPlanRow.class, rowId);
		if (model.getEduOrgUnitPayPlan().equals(row.getPlan())) {
			final Session session = getSession();

			final MQBuilder builder = new MQBuilder(UniscEduOrgUnitPayPlanRowGen.ENTITY_CLASS, "row");
			builder.add(MQExpression.eq("row", UniscEduOrgUnitPayPlanRowGen.L_PLAN+".id", model.getEduOrgUnitPayPlan().getId()));
			builder.add(MQExpression.eq("row", UniscEduOrgUnitPayPlanRowGen.P_STAGE, row.getStage()));
			builder.addOrder("row", UniscEduOrgUnitPayPlanRowGen.P_STAGE);
			builder.addOrder("row", UniscEduOrgUnitPayPlanRowGen.P_DATE);

			final List<UniscEduOrgUnitPayPlanRow> rows = builder.getResultList(session);
			final int i = rows.indexOf(row);
			if (i > 0 /* есть и не первый */) {
				final UniscEduOrgUnitPayPlanRow rc = rows.get(i-1);
				rc.setCost(rc.getCost() + row.getCost());
				session.saveOrUpdate(rc);
				session.delete(row);
			}
		}
	}
}
