/* $Id$ */
package ru.tandemservice.unisc.base.ext.Depersonalization;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.Depersonalization.DepersonalizationManager;
import org.tandemframework.shared.commonbase.base.bo.Depersonalization.logic.IDepersonalizationObject;
import org.tandemframework.shared.commonbase.base.bo.Depersonalization.logic.TemplateDocumentResetToDefaultObject;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson;
import ru.tandemservice.unisc.entity.catalog.UniscTemplateDocument;

/**
 * @author Nikolay Fedorovskih
 * @since 13.11.2014
 */
@Configuration
public class DepersonalizationExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private DepersonalizationManager _depersonalizationManager;

    @Bean
    public ItemListExtension<IDepersonalizationObject> depersonalizationObjectListExtension()
    {
        return _depersonalizationManager.extensionBuilder()
                .add(_depersonalizationManager.updateBuilder(UniscEduAgreementNaturalPerson.class)
                             .autoReplaceAllStringFields()
                             .autoCleanSomeFields())
                .add(new TemplateDocumentResetToDefaultObject(UniscTemplateDocument.class))
                .build();
    }
}