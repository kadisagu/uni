/**
 *$Id$
 */
package ru.tandemservice.unisc.base.bo.ScSystemAction;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

import ru.tandemservice.unisc.base.bo.ScSystemAction.logic.IScSystemActionDao;
import ru.tandemservice.unisc.base.bo.ScSystemAction.logic.ScSystemActionDao;

/**
 * @author Alexander Shaburov
 * @since 13.02.13
 */
@Configuration
public class ScSystemActionManager extends BusinessObjectManager
{
    public static ScSystemActionManager instance()
    {
        return instance(ScSystemActionManager.class);
    }

    @Bean
    public IScSystemActionDao dao()
    {
        return new ScSystemActionDao();
    }

}
