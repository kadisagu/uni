package ru.tandemservice.unisc.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unisc.entity.report.UniscGroupIncomeReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Отчет «Приход средств (по группе)»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniscGroupIncomeReportGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisc.entity.report.UniscGroupIncomeReport";
    public static final String ENTITY_NAME = "uniscGroupIncomeReport";
    public static final int VERSION_HASH = 2056927338;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONTENT = "content";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String P_GROUP = "group";
    public static final String P_DATE_FROM = "dateFrom";
    public static final String P_DATE_TO = "dateTo";

    private DatabaseFile _content;     // Печатная форма
    private Date _formingDate;     // Дата формирования
    private OrgUnit _orgUnit;     // Подразделение
    private String _group;     // Группа
    private Date _dateFrom;     // Начало периода
    private Date _dateTo;     // Конец периода

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Подразделение.
     */
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Группа.
     */
    @Length(max=255)
    public String getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа.
     */
    public void setGroup(String group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Начало периода. Свойство не может быть null.
     */
    @NotNull
    public Date getDateFrom()
    {
        return _dateFrom;
    }

    /**
     * @param dateFrom Начало периода. Свойство не может быть null.
     */
    public void setDateFrom(Date dateFrom)
    {
        dirty(_dateFrom, dateFrom);
        _dateFrom = dateFrom;
    }

    /**
     * @return Конец периода. Свойство не может быть null.
     */
    @NotNull
    public Date getDateTo()
    {
        return _dateTo;
    }

    /**
     * @param dateTo Конец периода. Свойство не может быть null.
     */
    public void setDateTo(Date dateTo)
    {
        dirty(_dateTo, dateTo);
        _dateTo = dateTo;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UniscGroupIncomeReportGen)
        {
            setContent(((UniscGroupIncomeReport)another).getContent());
            setFormingDate(((UniscGroupIncomeReport)another).getFormingDate());
            setOrgUnit(((UniscGroupIncomeReport)another).getOrgUnit());
            setGroup(((UniscGroupIncomeReport)another).getGroup());
            setDateFrom(((UniscGroupIncomeReport)another).getDateFrom());
            setDateTo(((UniscGroupIncomeReport)another).getDateTo());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniscGroupIncomeReportGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniscGroupIncomeReport.class;
        }

        public T newInstance()
        {
            return (T) new UniscGroupIncomeReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "content":
                    return obj.getContent();
                case "formingDate":
                    return obj.getFormingDate();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "group":
                    return obj.getGroup();
                case "dateFrom":
                    return obj.getDateFrom();
                case "dateTo":
                    return obj.getDateTo();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "group":
                    obj.setGroup((String) value);
                    return;
                case "dateFrom":
                    obj.setDateFrom((Date) value);
                    return;
                case "dateTo":
                    obj.setDateTo((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "content":
                        return true;
                case "formingDate":
                        return true;
                case "orgUnit":
                        return true;
                case "group":
                        return true;
                case "dateFrom":
                        return true;
                case "dateTo":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "content":
                    return true;
                case "formingDate":
                    return true;
                case "orgUnit":
                    return true;
                case "group":
                    return true;
                case "dateFrom":
                    return true;
                case "dateTo":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "content":
                    return DatabaseFile.class;
                case "formingDate":
                    return Date.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "group":
                    return String.class;
                case "dateFrom":
                    return Date.class;
                case "dateTo":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniscGroupIncomeReport> _dslPath = new Path<UniscGroupIncomeReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniscGroupIncomeReport");
    }
            

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.report.UniscGroupIncomeReport#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.report.UniscGroupIncomeReport#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.unisc.entity.report.UniscGroupIncomeReport#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Группа.
     * @see ru.tandemservice.unisc.entity.report.UniscGroupIncomeReport#getGroup()
     */
    public static PropertyPath<String> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Начало периода. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.report.UniscGroupIncomeReport#getDateFrom()
     */
    public static PropertyPath<Date> dateFrom()
    {
        return _dslPath.dateFrom();
    }

    /**
     * @return Конец периода. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.report.UniscGroupIncomeReport#getDateTo()
     */
    public static PropertyPath<Date> dateTo()
    {
        return _dslPath.dateTo();
    }

    public static class Path<E extends UniscGroupIncomeReport> extends EntityPath<E>
    {
        private DatabaseFile.Path<DatabaseFile> _content;
        private PropertyPath<Date> _formingDate;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PropertyPath<String> _group;
        private PropertyPath<Date> _dateFrom;
        private PropertyPath<Date> _dateTo;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.report.UniscGroupIncomeReport#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.report.UniscGroupIncomeReport#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(UniscGroupIncomeReportGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.unisc.entity.report.UniscGroupIncomeReport#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Группа.
     * @see ru.tandemservice.unisc.entity.report.UniscGroupIncomeReport#getGroup()
     */
        public PropertyPath<String> group()
        {
            if(_group == null )
                _group = new PropertyPath<String>(UniscGroupIncomeReportGen.P_GROUP, this);
            return _group;
        }

    /**
     * @return Начало периода. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.report.UniscGroupIncomeReport#getDateFrom()
     */
        public PropertyPath<Date> dateFrom()
        {
            if(_dateFrom == null )
                _dateFrom = new PropertyPath<Date>(UniscGroupIncomeReportGen.P_DATE_FROM, this);
            return _dateFrom;
        }

    /**
     * @return Конец периода. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.report.UniscGroupIncomeReport#getDateTo()
     */
        public PropertyPath<Date> dateTo()
        {
            if(_dateTo == null )
                _dateTo = new PropertyPath<Date>(UniscGroupIncomeReportGen.P_DATE_TO, this);
            return _dateTo;
        }

        public Class getEntityClass()
        {
            return UniscGroupIncomeReport.class;
        }

        public String getEntityName()
        {
            return "uniscGroupIncomeReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
