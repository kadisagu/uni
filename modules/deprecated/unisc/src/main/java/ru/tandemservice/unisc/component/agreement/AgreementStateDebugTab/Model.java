/* $Id: Model.java 8754 2009-06-29 09:27:43Z ekachanova $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisc.component.agreement.AgreementStateDebugTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.list.source.AbstractListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import ru.tandemservice.uni.ui.formatters.RuMoneyFormatter;
import ru.tandemservice.unisc.dao.payment.IUniscEduAgreementPaymentEvent;
import ru.tandemservice.unisc.dao.payment.IUniscEduAgreementPaymentState;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayPlanRow;
import ru.tandemservice.unisc.entity.config.UniscFinesHistory;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author vdanilov
 */
@State({@Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="id")})
public class Model {
    private Long id;
    public void setId(final Long id) { this.id = id; }
    public Long getId() { return this.id; }



    @SuppressWarnings("serial")
    public static abstract class PayPlanRow extends IdentifiableWrapper<IEntity> {
        public abstract UniscEduAgreementPayPlanRow getPlanRow();
        public abstract IUniscEduAgreementPaymentState.StateFactRow getFactRow();

        public String getFactPaymentAsString() {
            return (null == getFactRow() ? "" : RuMoneyFormatter.INSTANCE.format(getFactRow().getPayment()));
        }

        public String getFactRowDebtAsString() {
            if (null == getFactRow()) { return ""; }
            final double[] debt = getFactRow().getDebt();
            if (null == debt) { return ""; }
            return (
                    RuMoneyFormatter.INSTANCE.format(debt[0]) +
                    (debt[1] > 0d ? ("\n("+RuMoneyFormatter.INSTANCE.format(debt[1])+")") : "")
            );
        }


        public String getFactRowFinesInfoAsString() {
            if (null == getFactRow()) { return ""; }
            final Collection<IUniscEduAgreementPaymentState.StateFinesRow> fines = getFactRow().getFinesRows();
            if ((null == fines) || fines.isEmpty()) { return ""; }

            final StringBuilder sb = new StringBuilder();
            for (final IUniscEduAgreementPaymentState.StateFinesRow row: fines) {
                if (sb.length() > 0) { sb.append(",\n"); }
                sb
                .append(UniscFinesHistory.TAX_FORMATTER.format(row.getFinesTax()/* уже в процентах *100.0d*0.01d */))
                .append("% (")
                .append(CommonBaseDateUtil.getDaysCountWithName(row.getDays()))
                .append(") от ")
                .append(RuMoneyFormatter.INSTANCE.format(row.getDebt()));
            }
            return sb.toString();
        }


        public String getNothing() { return ""; }
        public PayPlanRow(final Long id, final String title) { super(id, title); }
    }

    private List<PayPlanRow> payPlanRows = Collections.emptyList();
    public List<PayPlanRow> getPayPlanRows() { return this.payPlanRows; }
    public void setPayPlanRows(final List<PayPlanRow> payPlanRows) { this.payPlanRows = payPlanRows; }

    private AbstractListDataSource<PayPlanRow> payPlanDataSource = new AbstractListDataSource<PayPlanRow>() {
        @Override public void onChangeOrder() {}
        @Override public void onRefresh() {}
        @Override public long getTotalSize() { return getEntityList().size(); }
        @Override public long getStartRow() { return 0; }
        @Override public long getCountRow() { return this.getTotalSize(); }
        @Override public List<PayPlanRow> getEntityList() { return payPlanRows; }
        @Override public AbstractListDataSource<PayPlanRow> getCopy() { return this; }
    };

    public AbstractListDataSource<PayPlanRow> getPayPlanDataSource() { return this.payPlanDataSource; }







    @SuppressWarnings("serial")
    public static class Row extends IdentifiableWrapper<IUniscEduAgreementPaymentEvent> {

        private final String stateString;
        public String getStateString() { return this.stateString; }

        private final IUniscEduAgreementPaymentEvent event;
        public IUniscEduAgreementPaymentEvent getEvent() { return this.event; }

        public Date getEventDate() { return null == getEvent() ? null : getEvent().getEventDate(); }

        public Row(final IUniscEduAgreementPaymentEvent e, final IUniscEduAgreementPaymentState agreementState) {
            super(e.getId(), e.getTitle());
            this.stateString = (null == agreementState ? "" : agreementState.toString());
            this.event = e;
        }

        public Row() {
            super(0L, "");
            this.event = null;
            this.stateString = null;
        }

    }

    private List<Row> rows = Collections.emptyList();
    public List<Row> getRows() { return this.rows; }
    public void setRows(final List<Row> rows) { this.rows = rows; }

    private AbstractListDataSource<Row> dataSource = new AbstractListDataSource<Row>() {
        @Override public void onChangeOrder() {}
        @Override public void onRefresh() {}
        @Override public long getTotalSize() { return getEntityList().size(); }
        @Override public long getStartRow() { return 0; }
        @Override public long getCountRow() { return this.getTotalSize(); }
        @Override public List<Row> getEntityList() { return rows; }
        @Override public AbstractListDataSource<Row> getCopy() { return this; }
    };

    public AbstractListDataSource<Row> getDataSource() { return this.dataSource; }


}
