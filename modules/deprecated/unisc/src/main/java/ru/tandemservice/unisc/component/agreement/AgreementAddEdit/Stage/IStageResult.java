package ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage;

/**
 * @author vdanilov
 */
@SuppressWarnings("deprecation")
public interface IStageResult {
    IStageResult CLOSE = m -> {};
    IStageResult NONE = m -> {};

    void execute(IAgreementOperation m);
}
