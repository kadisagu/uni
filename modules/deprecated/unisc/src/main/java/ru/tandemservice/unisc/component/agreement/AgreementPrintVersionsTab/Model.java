package ru.tandemservice.unisc.component.agreement.AgreementPrintVersionsTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPrintVersion;

/**
 * @author vdanilov
 */
@State({@Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="id")})
public class Model {

	private Long id;
	public Long getId() { return this.id; }
	public void setId(final Long id) { this.id = id; }

	private UniscEduAgreementBase agreement;
	public UniscEduAgreementBase getAgreement() { return this.agreement; }
	public void setAgreement(final UniscEduAgreementBase agreement) { this.agreement = agreement; }

	private DynamicListDataSource<UniscEduAgreementPrintVersion> dataSource;
	public DynamicListDataSource<UniscEduAgreementPrintVersion> getDataSource() { return this.dataSource; }
	public void setDataSource(final DynamicListDataSource<UniscEduAgreementPrintVersion> dataSource) { this.dataSource = dataSource; }


}
