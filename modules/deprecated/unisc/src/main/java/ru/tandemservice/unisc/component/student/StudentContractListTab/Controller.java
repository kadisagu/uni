/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unisc.component.student.StudentContractListTab;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.component.impl.ComponentRegion;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import org.tandemframework.tapsupport.TapSupportDefines;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisc.UniscComponents;
import ru.tandemservice.unisc.dao.agreement.IUniscEduAgreementDAO;
import ru.tandemservice.unisc.entity.agreements.UniscEduAdditAgreement;
import ru.tandemservice.unisc.entity.agreements.gen.UniscEduAgreementBaseGen;

import java.util.Collections;

/**
 * @author vdanilov
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        if (component.getName().equals(this.getClass().getPackage().getName()))
        {
            // на самом компоненте - подготавливаем данные для таба
            this.getDao().prepare(model);
            this.prepareAgreementsDataSource(component);
        }
        else
        {
            // на нем, как на точке расширения - проверяем видимость
            final Student student = this.getDao().get(Student.class, model.getId());

            boolean isVisible = UniDefines.COMPENSATION_TYPE_CONTRACT.equals(student.getCompensationType().getCode()) || this.getDao().isHasRelation(student);
            model.setTabVisible(isVisible && IUniscEduAgreementDAO.INSTANCE.get().isTabVisible(student));
        }
    }

    private void prepareAgreementsDataSource(final IBusinessComponent component)
    {
        final String p = "agreement.";
        final Model model = this.getModel(component);
        final Student student = this.getDao().get(Student.class, model.getId());
        final DynamicListDataSource<Model.AgreementWrapper> dataSource = new DynamicListDataSource<>(component, component1 -> {
            Controller.this.getDao().refreshDataSource(model);
        });

        dataSource.addColumn(new PublisherLinkColumn("№ документа", "title").setDisableHandler(entity -> {
            return false; // !(((Model.AgreementWrapper)entity).getAgreement() instanceof UniscEduMainAgreement);
        }).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Действует с", p+UniscEduAgreementBaseGen.P_FORMING_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Действует до", p+UniscEduAgreementBaseGen.P_DEADLINE_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата последнего изменения", p+UniscEduAgreementBaseGen.P_LAST_MODIFICATION_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Тип заказчика", p+UniscEduAgreementBaseGen.L_CUSTOMER_TYPE+".title").setOrderable(false).setClickable(false));

        final IEntityHandler activateDisabler = entity -> {
            final Model.AgreementWrapper w = ((Model.AgreementWrapper)entity);
            return !((w.getAgreement().getId().equals(model.getLastAgreement().getAgreement().getId())));
        };
        dataSource.addColumn(new ToggleColumn("Действующий", "active").setListener("onClickActivateStudentContract").setDisableHandler(activateDisabler).setPermissionKey("editStudentContract"));

        final IEntityHandler disabler = entity -> {
            if (student.isArchival())
                return true;
            final Model.AgreementWrapper w = ((Model.AgreementWrapper)entity);
            return !(w.getAgreement().getId().equals(model.getLastAgreement().getId()) && w.isActive());
        };
        final IEntityHandler editDisabler = entity -> {
            if (UniDefines.COMPENSATION_TYPE_BUDGET.equals(student.getCompensationType().getCode()))
                return true;
            if (student.isArchival())
                return true;
            final Model.AgreementWrapper w = ((Model.AgreementWrapper)entity);
            return w.getAgreement().isCheckedBySignature() || !(w.getAgreement().getId().equals(model.getLastAgreement().getId()) && w.isActive());
        };
        //dataSource.addColumn(new ToggleColumn("Проверен юристами", p+UniscEduAgreementBaseGen.P_CHECKED_BY_LAWYER).setListener("onClickCheckByLawer").setDisableHandler(disabler).setPermissionKey("editCheckedByLawyer_studentList"));
        //dataSource.addColumn(new ToggleColumn("Проверен бухгалтерией", p+UniscEduAgreementBaseGen.P_CHECKED_BY_ACCOUNTING).setListener("onClickCheckByAccounting").setDisableHandler(disabler).setPermissionKey("editCheckedByAccounting_studentList"));
        dataSource.addColumn(new ToggleColumn("Проверен", p+UniscEduAgreementBaseGen.P_CHECKED_BY_SIGNATURE).setListener("onClickCheckBySignature").setDisableHandler(disabler).setPermissionKey("editCheckedBySignature_studentList"));

        dataSource.addColumn(CommonBaseUtil.getPrintColumn("onClickPrintStudentContract", "Печать").setPermissionKey("printStudentContract"));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditStudentContract").setPermissionKey("editStudentContract").setDisableHandler(editDisabler));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteStudentContract", "Удалить документ?").setPermissionKey("deleteStudentContract").setDisableHandler(disabler));
        this.getModel(component).setAgreementsDataSource(dataSource);
    }

    public void onClickActivateStudentContract(final IBusinessComponent component) {
        this.getDao().updateActivateState(this.getModel(component), (Long)component.getListenerParameter());
        ContextLocal.setRequestAttribute(UIDefines.UPDATE_COMPONENTS, Collections.singleton("overall"));
    }

    public void onClickCheckByLawer(final IBusinessComponent component) {
        this.getDao().updateCheckByLawer(this.getModel(component), (Long)component.getListenerParameter());
    }

    public void onClickCheckByAccounting(final IBusinessComponent component) {
        this.getDao().updateCheckByAccounting(this.getModel(component), (Long)component.getListenerParameter());
    }

    public void onClickCheckBySignature(final IBusinessComponent component) {
        this.getDao().updateCheckBySignature(this.getModel(component), (Long)component.getListenerParameter());
        onRefreshComponent(component);
    }


    public void onClickAddStudentContract(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                UniscComponents.STUDENT_CONTRACT_ADD_EDIT,
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, model.getId())
        ));
        //component.createDefaultChildRegion(new ComponentActivator(
        //		UniscComponents.STUDENT_CONTRACT_ADD_EDIT,
        //		new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, model.getId())
        //));
    }


    public void onClickForceAddStudentContract(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                UniscComponents.STUDENT_CONTRACT_ADD_EDIT,
                new ParametersMap()
                .add(PublisherActivator.PUBLISHER_ID_KEY, model.getId())
                .add("force", Boolean.TRUE)
        ));
        //component.createDefaultChildRegion(new ComponentActivator(
        //		UniscComponents.STUDENT_CONTRACT_ADD_EDIT,
        //		new ParametersMap()
        //		.add(PublisherActivator.PUBLISHER_ID_KEY, model.getId())
        //		.add("force", Boolean.TRUE)
        //));
    }




    @SuppressWarnings("unchecked")
    public void onClickEditStudentContract(final IBusinessComponent component)
    {
        final Long id = (Long)component.getListenerParameter();
        final IEntityMeta meta = EntityRuntime.getMeta(id);
        if (null == meta) { return; }
        final Class entityClass = meta.getEntityClass();
        if (UniscEduAdditAgreement.class.isAssignableFrom(entityClass)) {
            ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                    UniscComponents.STUDENT_ADDITIONAL_AGREMENT_ADD_EDIT,
                    new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, id)
            ));
            //component.createDefaultChildRegion(new ComponentActivator(
            //        UniscComponents.STUDENT_ADDITIONAL_AGREMENT_ADD_EDIT,
            //        new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, id)
            //));
        } else {
            ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                    UniscComponents.STUDENT_CONTRACT_ADD_EDIT,
                    new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, id)
            ));
            //component.createDefaultChildRegion(new ComponentActivator(
            //        UniscComponents.STUDENT_CONTRACT_ADD_EDIT,
            //        new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, id)
            //));
        }
    }

    @SuppressWarnings("unchecked")
    public void onClickPrintStudentContract(final IBusinessComponent component)
    {
        final Long id = (Long)component.getListenerParameter();
        final IEntityMeta meta = EntityRuntime.getMeta(id);
        if (null == meta) { return; }
        final Class entityClass = meta.getEntityClass();
        if (UniscEduAdditAgreement.class.isAssignableFrom(entityClass)) {
            this.activateInRoot(component, new ComponentActivator(
                    UniscComponents.STUDENT_ADDITIONAL_AGREMENT_PRINT,
                    Collections.<String, Object>singletonMap(PublisherActivator.PUBLISHER_ID_KEY, id)
            ));
        } else {
            this.activateInRoot(component, new ComponentActivator(
                    UniscComponents.STUDENT_CONTRACT_PRINT,
                    Collections.<String, Object>singletonMap(PublisherActivator.PUBLISHER_ID_KEY, id)
            ));
        }
    }


    public void onClickAddStudentAdditionalAgreement(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                UniscComponents.STUDENT_ADDITIONAL_AGREMENT_ADD_EDIT,
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, model.getId())
        ));
        //component.createDefaultChildRegion(new ComponentActivator(
        //        UniscComponents.STUDENT_ADDITIONAL_AGREMENT_ADD_EDIT,
        //        new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, model.getId())
        //));
    }


    public synchronized void onClickDeleteStudentContract(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        try {
            this.getDao().deleteAgreement(model, (Long) component.getListenerParameter());
            ((IBusinessComponent) ((ComponentRegion) component.getParentRegion()).getOwner()).refresh(); // обновляем номер контракта на странице
        } finally {
            this.getDao().prepare(model);
        }
    }
}
