package ru.tandemservice.unisc.component.settings.CostSettings.Add;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;

import ru.tandemservice.uni.dao.EducationLevelModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uni.ui.SelectModel;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key="defaultEducationYearId", binding="educationYearModel.valueId"),
    @Bind(key="close", binding="close")
})
public class Model {

    private boolean close;

    public Boolean getClose() { return close; }
    public void setClose(Boolean close) { this.close = Boolean.TRUE.equals(close); }

    private final SelectModel<EducationYear> educationYearModel = new SelectModel<EducationYear>();
    public SelectModel<EducationYear> getEducationYearModel() { return this.educationYearModel; }

    private final EducationLevelModel educationLevelModel = new EducationLevelModel();
    public EducationLevelModel getEducationLevelModel() { return this.educationLevelModel; }

    private String cipher;
    public String getCipher() { return this.cipher; }
    public void setCipher(final String cipher) { this.cipher = cipher; }


}
