/**
 *$Id$
 */
package ru.tandemservice.unisc.base.ext.SystemAction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.IItemListExtensionBuilder;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.SystemActionManager;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.util.SystemActionDefinition;

import ru.tandemservice.unisc.base.ext.SystemAction.ui.Pub.SystemActionPubExt;

/**
 * @author Alexander Shaburov
 * @since 13.02.13
 */
@Configuration
public class SystemActionExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private SystemActionManager _systemActionManager;

    @Bean
    public ItemListExtension<SystemActionDefinition> buttonListExtension()
    {
        IItemListExtensionBuilder<SystemActionDefinition> extBuilder = itemListExtension(_systemActionManager.buttonListExtPoint())
        .add("uniscontract_deleteContractWithoutLinks", new SystemActionDefinition("unisc", "deleteContractWithoutLinks", "onClickDeleteContractWithoutLinks", SystemActionPubExt.SC_SYSTEM_ACTION_PUB_ADDON_NAME))
        .add("uniscontract_eduorgunitcostenv", new SystemActionDefinition("unisc", "eduorgunitcostenv", "onClickGetEducationOrgUnitCostEnvironmentXml", SystemActionPubExt.SC_SYSTEM_ACTION_PUB_ADDON_NAME));

        if (Debug.isDisplay()) {
            extBuilder.add("uniscontract_printAll", new SystemActionDefinition("unisc", "printAll", "onClickPrintAll", SystemActionPubExt.SC_SYSTEM_ACTION_PUB_ADDON_NAME));
        }

        return extBuilder.create();
    }
}
