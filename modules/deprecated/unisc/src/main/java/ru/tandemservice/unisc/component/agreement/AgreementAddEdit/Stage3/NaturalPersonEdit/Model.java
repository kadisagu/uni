package ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage3.NaturalPersonEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.fias.base.entity.Address;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson;

/**
 * @author vdanilov
 */
@Input({@Bind(key="naturalPerson", binding="naturalPerson")})
@SuppressWarnings("deprecation")
public class Model {

    private UniscEduAgreementNaturalPerson naturalPerson;
    private ISelectModel _issuancePlaceModel;
    private AddressDetailed _address;

    public UniscEduAgreementNaturalPerson getNaturalPerson() { return this.naturalPerson; }
    public void setNaturalPerson(final UniscEduAgreementNaturalPerson naturalPerson) {
        this.naturalPerson = naturalPerson;
    }

    public ISelectModel getIssuancePlaceModel()
    {
        return _issuancePlaceModel;
    }

    public void setIssuancePlaceModel(ISelectModel issuancePlaceModel)
    {
        _issuancePlaceModel = issuancePlaceModel;
    }

    public AddressDetailed getAddress()
    {
        return _address;
    }

    public void setAddress(AddressDetailed address)
    {
        _address = address;
    }
}
