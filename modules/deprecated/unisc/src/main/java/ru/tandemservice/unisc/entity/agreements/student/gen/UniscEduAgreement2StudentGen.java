package ru.tandemservice.unisc.entity.agreements.student.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisc.entity.agreements.UniscEduMainAgreement;
import ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Договор со студентом
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniscEduAgreement2StudentGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student";
    public static final String ENTITY_NAME = "uniscEduAgreement2Student";
    public static final int VERSION_HASH = -1038892842;
    private static IEntityMeta ENTITY_META;

    public static final String L_AGREEMENT = "agreement";
    public static final String L_STUDENT = "student";
    public static final String P_ACTIVE = "active";

    private UniscEduMainAgreement _agreement;     // Договор
    private Student _student;     // Студент
    private boolean _active;     // Действующий

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Договор. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public UniscEduMainAgreement getAgreement()
    {
        return _agreement;
    }

    /**
     * @param agreement Договор. Свойство не может быть null и должно быть уникальным.
     */
    public void setAgreement(UniscEduMainAgreement agreement)
    {
        dirty(_agreement, agreement);
        _agreement = agreement;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Действующий. Свойство не может быть null.
     */
    @NotNull
    public boolean isActive()
    {
        return _active;
    }

    /**
     * @param active Действующий. Свойство не может быть null.
     */
    public void setActive(boolean active)
    {
        dirty(_active, active);
        _active = active;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UniscEduAgreement2StudentGen)
        {
            setAgreement(((UniscEduAgreement2Student)another).getAgreement());
            setStudent(((UniscEduAgreement2Student)another).getStudent());
            setActive(((UniscEduAgreement2Student)another).isActive());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniscEduAgreement2StudentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniscEduAgreement2Student.class;
        }

        public T newInstance()
        {
            return (T) new UniscEduAgreement2Student();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "agreement":
                    return obj.getAgreement();
                case "student":
                    return obj.getStudent();
                case "active":
                    return obj.isActive();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "agreement":
                    obj.setAgreement((UniscEduMainAgreement) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "active":
                    obj.setActive((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "agreement":
                        return true;
                case "student":
                        return true;
                case "active":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "agreement":
                    return true;
                case "student":
                    return true;
                case "active":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "agreement":
                    return UniscEduMainAgreement.class;
                case "student":
                    return Student.class;
                case "active":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniscEduAgreement2Student> _dslPath = new Path<UniscEduAgreement2Student>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniscEduAgreement2Student");
    }
            

    /**
     * @return Договор. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student#getAgreement()
     */
    public static UniscEduMainAgreement.Path<UniscEduMainAgreement> agreement()
    {
        return _dslPath.agreement();
    }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Действующий. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student#isActive()
     */
    public static PropertyPath<Boolean> active()
    {
        return _dslPath.active();
    }

    public static class Path<E extends UniscEduAgreement2Student> extends EntityPath<E>
    {
        private UniscEduMainAgreement.Path<UniscEduMainAgreement> _agreement;
        private Student.Path<Student> _student;
        private PropertyPath<Boolean> _active;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Договор. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student#getAgreement()
     */
        public UniscEduMainAgreement.Path<UniscEduMainAgreement> agreement()
        {
            if(_agreement == null )
                _agreement = new UniscEduMainAgreement.Path<UniscEduMainAgreement>(L_AGREEMENT, this);
            return _agreement;
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Действующий. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student#isActive()
     */
        public PropertyPath<Boolean> active()
        {
            if(_active == null )
                _active = new PropertyPath<Boolean>(UniscEduAgreement2StudentGen.P_ACTIVE, this);
            return _active;
        }

        public Class getEntityClass()
        {
            return UniscEduAgreement2Student.class;
        }

        public String getEntityName()
        {
            return "uniscEduAgreement2Student";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
