package ru.tandemservice.unisc.dao.agreement;

import org.apache.commons.lang.StringUtils;
import org.hibernate.FlushMode;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.util.ClassUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisc.dao.config.IUniscEduOrgUnitConfigDAO;
import ru.tandemservice.unisc.dao.print.IUniscPrintDAO;
import ru.tandemservice.unisc.entity.agreements.*;
import ru.tandemservice.unisc.entity.agreements.gen.*;
import ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student;
import ru.tandemservice.unisc.entity.agreements.student.gen.UniscEduAgreement2StudentGen;
import ru.tandemservice.unisc.entity.catalog.UniscPayPeriod;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlanRow;
import ru.tandemservice.unisc.utils.UniscPropertyUtils;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Стандартная реализация
 *
 * @author vdanilov
 */
public class UniscEduAgreementDAO extends UniBaseDao implements IUniscEduAgreementDAO, ApplicationContextAware
{
    private final List<IUniscEduAgreement2PersonRoleResolver> revertedResolverList = new ArrayList<>();

    @SuppressWarnings("unchecked")
    @Override
    public void setApplicationContext(final ApplicationContext applicationContext) throws BeansException
    {
        this.revertedResolverList.addAll(applicationContext.getBeansOfType(IUniscEduAgreement2PersonRoleResolver.class).values());
        Collections.sort(this.revertedResolverList, (o1, o2) -> - (o1.priority() - o2.priority()));
    }

    public static final Object lock = new Object();

    @Override
    public void assignNewAgreementNumber(final UniscEduAgreementBase agreement, final boolean force)
    {
        if (StringUtils.isEmpty(agreement.getNumber()))
        {
            if (agreement instanceof UniscEduMainAgreement)
            {
                final int year = agreement.getConfig().getEducationYear().getIntValue();
                final String key = UniscEduMainAgreement.class.getSimpleName() + ".number." + year;

                if (force || UniscPropertyUtils.GENERATE_NUMBER)
                {

                    // выделяем номер
                    final Session session = this.getSession();
                    UniDaoFacade.getNumberDao().execute(key, value -> {
                        while (true)
                        {
                            value++;
                            final DecimalFormat f2 = (DecimalFormat) NumberFormat.getInstance();
                            f2.setMinimumIntegerDigits(2);
                            f2.setGroupingUsed(false);

                            final DecimalFormat f4 = (DecimalFormat) NumberFormat.getInstance();
                            f4.setMinimumIntegerDigits(4);
                            f4.setGroupingUsed(false);

                            final String number = f2.format(year % 100) + f4.format(value);
                            agreement.setNumber(number);
                            if (UniscEduAgreementDAO.this.isFreeNumber(agreement, session))
                            {
                                return value;
                            }

                        }
                    });
                } else
                {

                    // не выделяем номер
                    final DecimalFormat f2 = (DecimalFormat) NumberFormat.getInstance();
                    f2.setMinimumIntegerDigits(2);
                    f2.setGroupingUsed(false);
                    agreement.setNumber(f2.format(year % 100) + UniscPropertyUtils.RESERVED_POSTFIX);
                }

            } else if (agreement instanceof UniscEduAdditAgreement)
            {
                final String key = UniscEduAdditAgreement.class.getSimpleName() + ".number." + agreement.getAgreement().getNumber();
                UniDaoFacade.getNumberDao().execute(key, value -> {
                    agreement.setNumber(String.valueOf(1 + value));
                    return (1 + value);
                });

            }
        }
    }

    @Override
    /* НЕ ПЕРЕОПРЕДЕЛЯЙТЕ ЭТОТ МЕТОД: getAgreementIdsWithNumber - то, что вы ищете */
    public boolean doSaveAgreementWithNumberCheck(final UniscEduAgreementBase agreement)
    {
        final Session session = this.getSession();
        synchronized (UniscEduAgreementDAO.lock)
        {
            final Set<Long> ids = this.getAgreementIdsWithSameNumber(agreement, session);
            if (ids.size() > 0)
            {
                if (UniscPropertyUtils.STUPID_MODE)
                {
                    if (agreement instanceof UniscEduMainAgreement)
                    {
                        // по просьбе наших замечательных друзей из УрГПУ,
                        // где не в состоянии воспользоваться фильтром по номеру
                        // пишем человекочитаемый список того, чем же именно занят номер договора!
                        // руки в список договоров зайти отваливаются, видимо

                        final StringBuilder sb = new StringBuilder();
                        final Collection<IUniscEduAgreement2PersonRole<PersonRole>> relations = getRelationMap4Agreements(ids).values();
                        for (IUniscEduAgreement2PersonRole<PersonRole> relation : relations)
                        {
                            if (sb.length() > 0)
                            {
                                sb.append(",\n");
                            }
                            sb.append(relation.getTitle());
                        }

                        UserContext.getInstance().getErrorCollector().add("Указанный номер уже используется:\n " + sb.toString() + ".", "number");
                        throw new ApplicationException("Указанный номер уже используется:\n " + sb.toString() + ".");
                    }
                }
                UserContext.getInstance().getErrorCollector().add("Указанный номер уже используется.", "number");
                throw new ApplicationException("Указанный номер уже используется.");
            }

            agreement.setLastModificationDate(new Date());
            session.saveOrUpdate(agreement);
            session.flush(); // нужно, чтобы данные не попали одновременно из нескольких соединений
        }
        return true;
    }

    protected boolean isFreeNumber(final UniscEduAgreementBase agreement, final Session session)
    {
        return this.getAgreementIdsWithSameNumber(agreement, session).isEmpty();
    }

    /**
     * Возвращает идентификаторы договоров (отличных от самого документа), которые имеют тот же номер (в рамках схему нумерации)
     * может быть переопределен в проектах
     * !!!ВАЖНО!!! метод не должен возварщать значение agreement.id
     *
     * @param agreement договор
     * @param session   сессия
     * @return коллекцию идентификаторов
     */
    protected Set<Long> getAgreementIdsWithSameNumber(final UniscEduAgreementBase agreement, final Session session)
    {
        final String num = StringUtils.trimToEmpty(agreement.getNumber());
        if (num.endsWith(UniscPropertyUtils.RESERVED_POSTFIX))
        {
            return Collections.emptySet(); /* резервирование номеров */
        }


        final FlushMode flushMode = session.getFlushMode();
        try
        {
            // здесь важно брать состояние именно в базе на данный момент
            // поэтому flush перед запросом здесь делать не нужно
            session.setFlushMode(FlushMode.MANUAL);

            if (agreement instanceof UniscEduMainAgreement)
            {
                final MQBuilder notActive = new MQBuilder(UniscEduAgreement2StudentGen.ENTITY_CLASS, "rel", new String[]{UniscEduAgreement2StudentGen.L_AGREEMENT + ".id"});
                notActive.add(MQExpression.eq("rel", UniscEduAgreement2StudentGen.P_ACTIVE, Boolean.FALSE));

                final MQBuilder builder = new MQBuilder(UniscEduMainAgreementGen.ENTITY_CLASS, "a", new String[]{"id"});
                builder.add(MQExpression.notIn("a", "id", notActive));
                builder.add(MQExpression.eq("a", UniscEduAgreementBaseGen.P_NUMBER, num));

                final HashSet<Long> ids = new HashSet<>(builder.<Long>getResultList(session));
                if (null != agreement.getId())
                {
                    ids.remove(agreement.getId());
                }
                return ids;
            }

            if (agreement instanceof UniscEduAdditAgreement)
            {
                final MQBuilder builder = new MQBuilder(UniscEduAdditAgreementGen.ENTITY_CLASS, "a", new String[]{"id"});
                builder.add(MQExpression.eq("a", UniscEduAdditAgreementGen.L_AGREEMENT + ".id", agreement.getAgreement().getId()));
                builder.add(MQExpression.eq("a", UniscEduAgreementBaseGen.P_NUMBER, num));

                final HashSet<Long> ids = new HashSet<>(builder.<Long>getResultList(session));
                if (null != agreement.getId())
                {
                    ids.remove(agreement.getId());
                }
                return ids;
            }

            return Collections.emptySet();

        } finally
        {
            session.setFlushMode(flushMode);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<UniscEduAgreementNaturalPerson> getNaturalPersons(final UniscEduAgreementBase agreement)
    {
        synchronized (String.valueOf(agreement.getId()).intern())
        {
            return this.getSession().createCriteria(UniscEduAgreementNaturalPerson.class).add(Restrictions.eq(UniscEduAgreementNaturalPersonGen.L_OWNER, agreement)).addOrder(Order.asc(IEntity.P_ID)).list();
        }
    }

    @Override
    public void checkNaturalPersons(final UniscEduAgreementBase agreement, final List<UniscEduAgreementNaturalPerson> naturalPersons)
    {
        if (agreement.getCustomerType().hasNaturalPart())
        {
            // additional checks
            if ((null == naturalPersons) || naturalPersons.isEmpty())
            {
                throw new ApplicationException("Для документа необходимо указать список физических лиц (заказчиков).");
            }

            // если всего один товарищь, то увы.... он и подписывеет и оплачивает (не разделяем мы их)
            if (naturalPersons.size() == 1)
            {
                final UniscEduAgreementNaturalPerson np = naturalPersons.get(0);
                np.setUsedInAgreement(true);
                np.setSignInAgreement(true);
            }

            boolean hasSigners = false;
            boolean hasPayers = false;
            for (final UniscEduAgreementNaturalPerson np : naturalPersons)
            {
                hasPayers |= np.isUsedInAgreement();
                hasSigners |= np.isSignInAgreement();
            }

            if (!hasPayers)
            {
                throw new ApplicationException("Необходимо указать, кто из перечисленных лиц вносит оплату.");
            }
            if (!hasSigners)
            {
                throw new ApplicationException("Необходимо указать, кто из перечисленных лиц подписывает документ.");
            }
        }
    }

    @Override
    public void doSyncNaturalPersons(final UniscEduAgreementBase agreement, final List<UniscEduAgreementNaturalPerson> naturalPersons)
    {
        this.checkNaturalPersons(agreement, naturalPersons);
        synchronized (String.valueOf(agreement.getId()).intern())
        {
            final Session s = this.getSession();
            for (final UniscEduAgreementNaturalPerson p : naturalPersons)
            {
                if ((null == p.getId()) || p.isTransient())
                {
                    continue;
                }
                s.update(p); // reassociate instances before loading
            }

            final List<UniscEduAgreementNaturalPerson> current = this.getNaturalPersons(agreement);
            final Map<Long, UniscEduAgreementNaturalPerson> currentMap = new HashMap<>();
            for (final UniscEduAgreementNaturalPerson c : current)
            {
                currentMap.put(c.getId(), c);
            }

            for (final UniscEduAgreementNaturalPerson p : naturalPersons)
            {
                if ((null == p.getId()) || p.isTransient())
                {
                    // новый объект - сохраняем
                    p.setId(null);
                    this._save_natural_person(agreement, s, p);

                } else if (null != currentMap.remove(p.getId()))
                {
                    // старый объект (и он уже есть)
                    this._save_natural_person(agreement, s, p);

                } else if (agreement.getId().equals(p.getOwner().getId()))
                {
                    // объект старый, но кто-то его уже удалил - гы! сохраним заново
                    if (!s.contains(p))
                    {
                        p.setId(null);
                    }
                    this._save_natural_person(agreement, s, p);

                } //else
                {
                    // wtf ? объект вообще левый, ничего делать не буду
                }
            }

            for (final UniscEduAgreementNaturalPerson p : currentMap.values())
            {
                // удаляем всех, кого нет
                s.delete(p);
                if (null != p.getAddress())
                {
                    s.delete(p.getAddress());
                }
            }
        }
    }

    private void _save_natural_person(final UniscEduAgreementBase agreement, final Session s, final UniscEduAgreementNaturalPerson p)
    {
        p.setOwner(agreement);
        if (null != p.getAddress())
        {
            s.saveOrUpdate(p.getAddress());
        }
        s.saveOrUpdate(p);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T extends EntityBase, U extends IUniscEduAgreement2PersonRole<T>> U doConnect(final U relation)
    {
        final Session session = this.getSession();
        synchronized (UniscEduAgreementDAO.lock)
        {
            try
            {
                final U rel = (U) session.createCriteria(Hibernate.getClass(relation)).add(Restrictions.eq("agreement", relation.getAgreement())).uniqueResult();
                if (null == rel)
                {
                    session.save(relation);
                    return relation;
                }

                if (!rel.getTarget().getId().equals(relation.getTarget().getId()))
                {
                    throw new IllegalStateException();
                }
                return rel;
            } finally
            {
                // call flush manually BEFORE exit from critical section
                session.flush();
            }
        }
    }

    @Override
    public Map<Long, IUniscEduAgreement2PersonRole<PersonRole>> getRelationMap4Agreements(final Collection<Long> mainAgreementIds)
    {
        final Map<Long, IUniscEduAgreement2PersonRole<PersonRole>> result = new HashMap<>(mainAgreementIds.size());
        for (final IUniscEduAgreement2PersonRoleResolver resolver : this.revertedResolverList)
        {
            final Collection<IUniscEduAgreement2PersonRole<PersonRole>> resolved = resolver.getRelations4Agreements(mainAgreementIds);
            for (final IUniscEduAgreement2PersonRole<PersonRole> rel : resolved)
            {
                result.put(rel.getAgreement().getId(), rel);
            }
        }
        return result;
    }

    @Override
    public Map<Long, Set<IUniscEduAgreement2PersonRole<PersonRole>>> getRelationMap4PersonRoles(final Collection<Long> personRoleIds)
    {
        final Map<Long, Set<IUniscEduAgreement2PersonRole<PersonRole>>> result = new HashMap<>(personRoleIds.size());
        for (final IUniscEduAgreement2PersonRoleResolver resolver : this.revertedResolverList)
        {
            final Collection<IUniscEduAgreement2PersonRole<PersonRole>> resolved = resolver.getRelations4PersonRole(personRoleIds);
            for (final IUniscEduAgreement2PersonRole<PersonRole> rel : resolved)
            {
                SafeMap.safeGet(result, rel.getTarget().getId(), HashSet.class).add(rel);
            }
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    public IUniscEduAgreement2PersonRole<PersonRole> getRelation(final UniscEduMainAgreement agreement)
    {
        final IUniscEduAgreement2PersonRole<PersonRole> relation = this.getRelationMap4Agreements(Collections.singleton(agreement.getId())).get(agreement.getId());
        return (IUniscEduAgreement2PersonRole<PersonRole>) (null == relation ? null : get((Class<IEntity>) ClassUtils.getUserClass(relation), relation.getId()));
    }

    @Override
    public String getPermissionContext(final UniscEduAgreementBase agreement)
    {
        final IUniscEduAgreement2PersonRole<PersonRole> relation = this.getRelation(agreement.getAgreement());
        return relation == null ? null : relation.getPermissionContext();
    }

    @Override
    public List<UniscEduAgreementPayPlanRow> getAgreementPayPlanRows(final UniscEduAgreementBase agreement, final boolean createEmptyRowsFromConfig)
    {
        if (createEmptyRowsFromConfig)
        {
            final int firstStage = agreement.getFirstStage();
            final int lastStage = agreement.getLastStage();

            final UniscPayPeriod payPlanFreq = agreement.getPayPlanFreq();
            final UniscEduOrgUnit config = agreement.getConfig();
            final int firstCourse = payPlanFreq.getCourseNumber(config, firstStage);

            final List<UniscEduOrgUnitPayPlanRow> rows = IUniscEduOrgUnitConfigDAO.INSTANCE.get().getPlanRows(config, payPlanFreq, firstStage, lastStage, true);
            final List<UniscEduAgreementPayPlanRow> agreementRows = new ArrayList<>();
            for (final UniscEduOrgUnitPayPlanRow row : rows)
            {
                final UniscEduAgreementPayPlanRow agreementRow = new UniscEduAgreementPayPlanRow();
                if ((firstStage <= row.getStage()) && (row.getStage() <= lastStage))
                {
                    agreementRow.setOwner(agreement);
                    agreementRow.setStage(row.getStage());
                    agreementRow.setNCost(Math.round(0.5 * row.getCost()));
                    agreementRow.setJCost(Math.round(0.5 * row.getCost()));

                    final Calendar cal = Calendar.getInstance();
                    cal.setTime(row.getDate());
                    cal.add(Calendar.YEAR, (payPlanFreq.getCourseNumber(config, row.getStage()) - firstCourse));
                    agreementRow.setDate(cal.getTime());

                    agreementRows.add(agreementRow);

                }
            }
            return agreementRows;
        }

        synchronized (String.valueOf(agreement.getId()).intern())
        {
            final MQBuilder oldRowsBuilder = new MQBuilder(UniscEduAgreementPayPlanRowGen.ENTITY_CLASS, "r");
            // oldRowsBuilder.add(MQExpression.isNull("r", UniscEduAgreementPayPlanRowGen.L_REPLACED_BY));
            oldRowsBuilder.add(MQExpression.eq("r", UniscEduAgreementPayPlanRowGen.L_OWNER + ".id", agreement.getId()));
            oldRowsBuilder.addOrder("r", UniscEduAgreementPayPlanRowGen.P_STAGE);
            oldRowsBuilder.addOrder("r", UniscEduAgreementPayPlanRowGen.P_DATE);
            return oldRowsBuilder.getResultList(this.getSession());
        }
    }

    //    @Override
    //    @SuppressWarnings("unchecked")
    //    public List<UniscEduAgreementPayPlanRow> getAgreementTotalPayPlanRows(final UniscEduAgreementBase agreement) {
    //        final Long agreementId = agreement.getId();
    //        if (agreement instanceof UniscEduMainAgreement) {
    //            // для договора - берем все доп соглашения и данные самого договора
    //            final IUniscEduAgreementPaymentState state = IUniscEduAgreementPaymentDAO.NOBR_IN_LINES.get().getStateMap(Collections.singleton(agreementId), null).get(agreementId);
    //            final Session session = getSession();
    //            if ((null == state) || (null == state.getPlanRows())) {
    //                return Collections.emptyList();
    //            }
    //            return (List<UniscEduAgreementPayPlanRow>) CollectionUtils.collect(state.getPlanRows(), new Transformer() {
    //                @Override public Object transform(final Object input) {
    //                    return session.get(UniscEduAgreementPayPlanRow.class, ((IUniscEduAgreementPaymentState.StatePlanRow)input).getId()); /* уже в кэшк сессии */
    //                }
    //            });
    //        } else {
    //            // иначе - берем только то, что в объекте
    //            final MQBuilder oldRowsBuilder = new MQBuilder(UniscEduAgreementPayPlanRowGen.ENTITY_CLASS, "r");
    //            oldRowsBuilder.add(MQExpression.isNull("r", UniscEduAgreementPayPlanRowGen.L_REPLACED_BY));
    //            oldRowsBuilder.add(MQExpression.eq("r", UniscEduAgreementPayPlanRowGen.L_OWNER+".id", agreementId));
    //            oldRowsBuilder.addOrder("r", UniscEduAgreementPayPlanRowGen.P_STAGE);
    //            oldRowsBuilder.addOrder("r", UniscEduAgreementPayPlanRowGen.P_DATE);
    //            return new ArrayList<UniscEduAgreementPayPlanRow>(oldRowsBuilder.<UniscEduAgreementPayPlanRow>getResultList(this.getSession()));
    //        }
    //    }

    protected Map<Long, Map<Integer, Map<Long, UniscEduAgreementPayPlanRow>>> getPayRowsDataMap(final List<UniscEduAgreementPayPlanRow> dbRows)
    {
        final Map<Long, Map<Integer, Map<Long, UniscEduAgreementPayPlanRow>>> dbData = SafeMap.get(key -> SafeMap.get(key1 -> new HashMap<>()));
        for (final UniscEduAgreementPayPlanRow dbRow : dbRows) {
            dbData.get(dbRow.getOwner().getId())
                    .get(dbRow.getStage())
                    .put(dbRow.getId(), dbRow);
        }
        return dbData;
    }

    @Override
    public void doSyncAgreementPayPlanRows(final UniscEduAgreementBase agreement, final List<UniscEduAgreementPayPlanRow> rows)
    {
        synchronized (String.valueOf(agreement.getId()).intern())
        {
            final List<UniscEduAgreementPayPlanRow> dbRows = this.getAgreementPayPlanRows(agreement, false);
            final Map<Long, Map<Integer, Map<Long, UniscEduAgreementPayPlanRow>>> dbData = this.getPayRowsDataMap(dbRows);

            final Session session = this.getSession();
            final Map<Integer, Map<Long, UniscEduAgreementPayPlanRow>> stage2data = dbData.get(agreement.getId());

            /* только текущее соглашение */
            {
                for (final UniscEduAgreementPayPlanRow row : rows)
                {
                    if (agreement.equals(row.getOwner()))
                    {
                        final Map<Long, UniscEduAgreementPayPlanRow> id2data = stage2data.get(row.getStage());
                        if (!row.isTransient())
                        {
                            final UniscEduAgreementPayPlanRow obj = id2data.remove(row.getId());
                            if (null != obj)
                            {
                                obj.update(row);
                                obj.setOwner(agreement);
                                session.saveOrUpdate(obj);
                            } else
                            {
                                session.saveOrUpdate(row);
                            }
                        } else
                        {
                            row.setId(null);
                            row.setOwner(agreement);
                            session.saveOrUpdate(row);
                        }
                    }
                }

                for (final Map<Long, UniscEduAgreementPayPlanRow> maps : stage2data.values())
                {
                    for (final UniscEduAgreementPayPlanRow row : maps.values())
                    {
                        session.delete(row);
                    }
                }
            }

            session.flush();
        }
    }

    @Override
    public boolean isMutable(final UniscEduAgreementBase agreement)
    {

        /* проверка по загруженной версии */
        {

            // 2768: Если текущей версией для печати является файл, загруженный пользователем (признак загружен = "true"), то кнопка "Редактировать доп. соглашение" дизаблится.
            final UniscEduAgreementPrintVersion lastPrintVersion = (null == agreement.getId()) ? null : IUniscPrintDAO.INSTANCE.get().getLastPrintVersion(agreement);
            if (null != lastPrintVersion)
            {
                return !lastPrintVersion.isUploaded();
            }
        }

        return true;
    }

    @Override
    public boolean isTabVisible(final Student student)
    {
        return true;
    }

    @Override
    @SuppressWarnings("unchecked")
    public boolean isEditBottonVisible(final UniscEduMainAgreement agreement)
    {
        final IUniscEduAgreement2PersonRole rel = IUniscEduAgreementDAO.INSTANCE.get().getRelation(agreement);
        if (rel instanceof UniscEduAgreement2Student)
        {
            final Student student = ((UniscEduAgreement2Student) rel).getStudent();
            return (UniDefines.COMPENSATION_TYPE_BUDGET.equals(student.getCompensationType().getCode()));

        }
        return false;
    }

    public UniscEduMainAgreement getStudentContract(Long studentId)
    {
        List<UniscEduMainAgreement> studentContractsList = new DQLSelectBuilder()
                .fromEntity(UniscEduAgreement2Student.class, "c")
                .column(property(UniscEduAgreement2Student.agreement().fromAlias("c")))
                .where(eq(property(UniscEduAgreement2Student.student().id().fromAlias("c")), value(studentId)))
                .order(property(UniscEduAgreement2Student.agreement().formingDate()), OrderDirection.desc)
                .createStatement(getSession()).list();
        if (!studentContractsList.isEmpty()) return studentContractsList.get(0);
        return null;
    }

    public Map<Long, UniscEduMainAgreement> getStudentContracts(Collection<Long> studentIds)
    {
        final Map<Long, UniscEduMainAgreement> result = new HashMap<>();

        BatchUtils.execute(studentIds, DQL.MAX_VALUES_ROW_NUMBER, ids -> {
            List<UniscEduAgreement2Student> studentContractsList = new DQLSelectBuilder()
                    .fromEntity(UniscEduAgreement2Student.class, "c").column("c")
                    .where(in(property(UniscEduAgreement2Student.student().id().fromAlias("c")), ids))
                    .order(property(UniscEduAgreement2Student.agreement().formingDate().fromAlias("c")), OrderDirection.desc)
                    .createStatement(getSession()).list();

            for (UniscEduAgreement2Student contract : studentContractsList) {
                if (!result.containsKey(contract.getStudent().getId())) {
                    result.put(contract.getStudent().getId(), contract.getAgreement());
                }
            }
        });

        return result;
    }
}