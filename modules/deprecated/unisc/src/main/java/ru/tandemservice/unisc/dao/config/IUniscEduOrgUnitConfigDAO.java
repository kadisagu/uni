package ru.tandemservice.unisc.dao.config;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.unisc.entity.catalog.UniscPayPeriod;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlan;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlanRow;
import ru.tandemservice.unisc.ws.cost.EducationOrgUnitCostEnvironment;

/**
 * Методы работы с настройками направления подготовки
 * @author vdanilov
 */
public interface IUniscEduOrgUnitConfigDAO {
	public static final SpringBeanCache<IUniscEduOrgUnitConfigDAO> INSTANCE = new SpringBeanCache<IUniscEduOrgUnitConfigDAO>(IUniscEduOrgUnitConfigDAO.class.getName());

	@Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
	UniscEduOrgUnit getUniscEduOrgUnit(EducationYear year, EducationOrgUnit eduOrgUnit, String cipher);

	@Transactional(propagation=Propagation.REQUIRED, readOnly=false)
	UniscEduOrgUnit doSaveUniscEduOrgUnit(UniscEduOrgUnit orgUnit);

	@Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
	List<UniscEduOrgUnitPayPlan> listPayPlans4EducationOrgUnit(UniscEduOrgUnit uniscEduOrgUnit);

	@Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
	List<UniscEduOrgUnitPayPlanRow> getPlanRows(UniscEduOrgUnit uniscEduOrgUnit, UniscPayPeriod payFreq, int firstStage, int lastStage, boolean createIfEmpty);

    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    EducationOrgUnitCostEnvironment getEducationOrgUnitCostEnvironment(String educationYearTitle, String payPeriodCode, int stagePeriod);
}
