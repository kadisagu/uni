package ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage;

import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

/**
 * @author vdanilov
 */
public abstract class AbstractStageController<D extends IAbstractStageDAO<M>, M extends AbstractStageModel> extends AbstractBusinessController<D, M> {
    @Override public void onRefreshComponent(final IBusinessComponent component) {
        final M model = this.getModel(component);

        if (model.first) {
            model.first = false;
            if (this.getDao().skip(model)) {
                model.setResult(IStageResult.NONE);
                this.deactivate(component);
                return;
            }
        }

        this.getDao().prepare(model);

    }

    public void onClickApply(final IBusinessComponent component) {
        try {
            final M model = this.getModel(component);
            final IStageResult result = this.getDao().save(model);
            if (null == result) {
                // что-то не выбрано :( //
                throw new IllegalStateException();
            }
            model.setResult(result);
            this.deactivate(component);
        } catch (Throwable t) {
            component.getDesktop().setRefreshScheduled(true);
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }
}
