package ru.tandemservice.unisc.entity.config.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnitRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Настройка оплаты (по направлению подготовки для слушателей на базе студентов)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniscEduOrgUnitRelationGen extends EntityBase
 implements INaturalIdentifiable<UniscEduOrgUnitRelationGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisc.entity.config.UniscEduOrgUnitRelation";
    public static final String ENTITY_NAME = "uniscEduOrgUnitRelation";
    public static final int VERSION_HASH = -1289946074;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONFIG = "config";
    public static final String L_BASED_ON = "basedOn";

    private UniscEduOrgUnit _config;     // настройка
    private UniscEduOrgUnit _basedOn;     // на базе

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return настройка. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public UniscEduOrgUnit getConfig()
    {
        return _config;
    }

    /**
     * @param config настройка. Свойство не может быть null и должно быть уникальным.
     */
    public void setConfig(UniscEduOrgUnit config)
    {
        dirty(_config, config);
        _config = config;
    }

    /**
     * @return на базе. Свойство не может быть null.
     */
    @NotNull
    public UniscEduOrgUnit getBasedOn()
    {
        return _basedOn;
    }

    /**
     * @param basedOn на базе. Свойство не может быть null.
     */
    public void setBasedOn(UniscEduOrgUnit basedOn)
    {
        dirty(_basedOn, basedOn);
        _basedOn = basedOn;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UniscEduOrgUnitRelationGen)
        {
            if (withNaturalIdProperties)
            {
                setConfig(((UniscEduOrgUnitRelation)another).getConfig());
            }
            setBasedOn(((UniscEduOrgUnitRelation)another).getBasedOn());
        }
    }

    public INaturalId<UniscEduOrgUnitRelationGen> getNaturalId()
    {
        return new NaturalId(getConfig());
    }

    public static class NaturalId extends NaturalIdBase<UniscEduOrgUnitRelationGen>
    {
        private static final String PROXY_NAME = "UniscEduOrgUnitRelationNaturalProxy";

        private Long _config;

        public NaturalId()
        {}

        public NaturalId(UniscEduOrgUnit config)
        {
            _config = ((IEntity) config).getId();
        }

        public Long getConfig()
        {
            return _config;
        }

        public void setConfig(Long config)
        {
            _config = config;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof UniscEduOrgUnitRelationGen.NaturalId) ) return false;

            UniscEduOrgUnitRelationGen.NaturalId that = (NaturalId) o;

            if( !equals(getConfig(), that.getConfig()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getConfig());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getConfig());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniscEduOrgUnitRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniscEduOrgUnitRelation.class;
        }

        public T newInstance()
        {
            return (T) new UniscEduOrgUnitRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "config":
                    return obj.getConfig();
                case "basedOn":
                    return obj.getBasedOn();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "config":
                    obj.setConfig((UniscEduOrgUnit) value);
                    return;
                case "basedOn":
                    obj.setBasedOn((UniscEduOrgUnit) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "config":
                        return true;
                case "basedOn":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "config":
                    return true;
                case "basedOn":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "config":
                    return UniscEduOrgUnit.class;
                case "basedOn":
                    return UniscEduOrgUnit.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniscEduOrgUnitRelation> _dslPath = new Path<UniscEduOrgUnitRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniscEduOrgUnitRelation");
    }
            

    /**
     * @return настройка. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisc.entity.config.UniscEduOrgUnitRelation#getConfig()
     */
    public static UniscEduOrgUnit.Path<UniscEduOrgUnit> config()
    {
        return _dslPath.config();
    }

    /**
     * @return на базе. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.config.UniscEduOrgUnitRelation#getBasedOn()
     */
    public static UniscEduOrgUnit.Path<UniscEduOrgUnit> basedOn()
    {
        return _dslPath.basedOn();
    }

    public static class Path<E extends UniscEduOrgUnitRelation> extends EntityPath<E>
    {
        private UniscEduOrgUnit.Path<UniscEduOrgUnit> _config;
        private UniscEduOrgUnit.Path<UniscEduOrgUnit> _basedOn;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return настройка. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisc.entity.config.UniscEduOrgUnitRelation#getConfig()
     */
        public UniscEduOrgUnit.Path<UniscEduOrgUnit> config()
        {
            if(_config == null )
                _config = new UniscEduOrgUnit.Path<UniscEduOrgUnit>(L_CONFIG, this);
            return _config;
        }

    /**
     * @return на базе. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.config.UniscEduOrgUnitRelation#getBasedOn()
     */
        public UniscEduOrgUnit.Path<UniscEduOrgUnit> basedOn()
        {
            if(_basedOn == null )
                _basedOn = new UniscEduOrgUnit.Path<UniscEduOrgUnit>(L_BASED_ON, this);
            return _basedOn;
        }

        public Class getEntityClass()
        {
            return UniscEduOrgUnitRelation.class;
        }

        public String getEntityName()
        {
            return "uniscEduOrgUnitRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
