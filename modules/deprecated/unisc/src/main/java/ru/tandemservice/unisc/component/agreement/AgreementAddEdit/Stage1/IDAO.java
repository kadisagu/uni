package ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage1;

import ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage.IAbstractStageDAO;

/**
 * @author vdanilov
 */
@SuppressWarnings("deprecation")
public interface IDAO extends IAbstractStageDAO<Model> {

}
