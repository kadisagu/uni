package ru.tandemservice.unisc.component.orgunit.OrgUnitStudentsTab;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniDao;

/**
 * 
 * @author nkokorina
 * Created on: 03.02.2010
 */

public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        if (null != model.getOrgUnitId())
        {
            model.setOrgUnit((OrgUnit)get(model.getOrgUnitId()));
        }
    }
}
