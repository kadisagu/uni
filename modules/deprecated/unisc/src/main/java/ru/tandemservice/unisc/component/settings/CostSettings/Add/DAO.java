package ru.tandemservice.unisc.component.settings.CostSettings.Add;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.tapsupport.component.selection.StaticSelectModel;
import ru.tandemservice.uni.dao.EducationLevelModel;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.basic.gen.EducationYearGen;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.ui.SelectModel;
import ru.tandemservice.uni.util.EducationLevelUtil;
import ru.tandemservice.unisc.dao.config.IUniscEduOrgUnitConfigDAO;

/**
 * @author vdanilov
 */
@SuppressWarnings("deprecation")
public class DAO extends UniBaseDao implements IDAO {


    @Override
    public Long save(final Model model) {
        final EducationYear educationYear = model.getEducationYearModel().getValue();
        if (null == educationYear) {
            throw new ApplicationException("Не выбран учебный год.");
        }

        final EducationOrgUnit educationOrgUnit = UniDaoFacade.getEducationLevelDao().getEducationOrgUnit(model.getEducationLevelModel());
        if (null == educationOrgUnit) {
            throw new ApplicationException("Не указано направление подготовки.");
        }

        final String cipher = StringUtils.trimToNull(model.getCipher());
        return IUniscEduOrgUnitConfigDAO.INSTANCE.get().getUniscEduOrgUnit(educationYear, educationOrgUnit, cipher).getId();
    }


    @Override
    public void prepare(final Model model) {
        prepareEducationYearModel(model);
        prepareEducationOrgUnitModel(model);
    }

    protected void prepareEducationYearModel(final Model model) {
        final SelectModel<EducationYear> educationYearModel = model.getEducationYearModel();
        if (null == educationYearModel.getSource()) {
            educationYearModel.setSource(new StaticSelectModel("id", "title", getCatalogItemList(EducationYear.class)));
        }

        if (null != educationYearModel.getValue()) {
            educationYearModel.setValue(reassociateEntity(getSession(), educationYearModel.getValue()));
        } else {
            educationYearModel.setValue(get(EducationYear.class, EducationYearGen.P_CURRENT, Boolean.TRUE));
        }

    }

    protected void prepareEducationOrgUnitModel(final Model model) {
        final EducationLevelModel educationLevelModel = model.getEducationLevelModel();
        EducationLevelUtil.prepareEducationLevelModel(educationLevelModel, false);
    }

}
