package ru.tandemservice.unisc.component.settings.CostSettings.EditPlan;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlan;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlanRow;

@Input({
	@Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="eduOrgUnitPayPlan.id")
})
public class Model {
	private UniscEduOrgUnitPayPlan eduOrgUnitPayPlan = new UniscEduOrgUnitPayPlan();;
	public UniscEduOrgUnitPayPlan getEduOrgUnitPayPlan() { return this.eduOrgUnitPayPlan; }
	public void setEduOrgUnitPayPlan(final UniscEduOrgUnitPayPlan eduOrgUnitPayPlan) { this.eduOrgUnitPayPlan = eduOrgUnitPayPlan; }

	public UniscEduOrgUnit getEduOrgUnit() { return this.getEduOrgUnitPayPlan().getUniscEduOrgUnit(); }
	public EducationOrgUnit getEducationOrgUnit() { return this.getEduOrgUnit().getEducationOrgUnit(); }

	private DynamicListDataSource<UniscEduOrgUnitPayPlanRow> dataSource;
	public DynamicListDataSource<UniscEduOrgUnitPayPlanRow> getDataSource() { return this.dataSource; }
	public void setDataSource(final DynamicListDataSource<UniscEduOrgUnitPayPlanRow> dataSource) { this.dataSource = dataSource; }

}
