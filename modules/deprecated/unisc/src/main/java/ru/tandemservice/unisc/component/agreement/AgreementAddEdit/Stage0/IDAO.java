package ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage0;

import ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage.IAbstractStageDAO;

/**
 * @author vdanilov
 */
@SuppressWarnings("deprecation")
public interface IDAO extends IAbstractStageDAO<Model> {

}
