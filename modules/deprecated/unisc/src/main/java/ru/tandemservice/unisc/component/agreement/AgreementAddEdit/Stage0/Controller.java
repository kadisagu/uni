package ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage0;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage.AbstractStageController;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;

/**
 * @author vdanilov
 */
@SuppressWarnings("deprecation")
public class Controller extends AbstractStageController<IDAO, Model> {

    public void onChangeCipher(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        /* по шифру ставим год и направление */ {
            final UniscEduOrgUnit value = model.getCipherModel().getValue();

            if (null == value) {
                //model.getEducationYearModel().setValue(null);
                model.getEducationLevelModel().clear();
            } else {
                model.getEducationYearModel().setValue(value.getEducationYear());
                model.getEducationLevelModel().fromEducationOrgUnit(value.getEducationOrgUnit(), true);
            }
        }
        this.getDao().prepare(model);
    }


}
