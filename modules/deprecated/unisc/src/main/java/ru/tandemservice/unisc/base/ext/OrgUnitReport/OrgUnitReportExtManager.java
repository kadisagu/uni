/* $Id: $ */
package ru.tandemservice.unisc.base.ext.OrgUnitReport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.IItemListExtensionBuilder;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.OrgUnitReportManager;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.logic.OrgUnitReportBlockDefinition;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.logic.OrgUnitReportDefinition;
import ru.tandemservice.uni.component.reports.FormativeAndTerritorialOrgUnitReportVisibleResolver;


/**
 * @author Andrey Andreev
 * @since 13.04.2017
 */
@Configuration
public class OrgUnitReportExtManager extends BusinessObjectExtensionManager
{


    public static final String UNISC_ORGUNIT_REPORT_BLOCK = "uniscOrgUnitReportBlock";


    @Autowired
    private OrgUnitReportManager _orgUnitReportManager;


    @Bean
    public ItemListExtension<OrgUnitReportBlockDefinition> blockListExtension()
    {

        IItemListExtensionBuilder<OrgUnitReportBlockDefinition> builder = itemListExtension(_orgUnitReportManager.blockListExtPoint());

        return builder
                .add(UNISC_ORGUNIT_REPORT_BLOCK, new OrgUnitReportBlockDefinition("Отчеты модуля «(Старый) Договоры и оплаты»", UNISC_ORGUNIT_REPORT_BLOCK, new FormativeAndTerritorialOrgUnitReportVisibleResolver()))
                .create();
    }

    @Bean
    public ItemListExtension<OrgUnitReportDefinition> reportListExtension()
    {

        return itemListExtension(_orgUnitReportManager.reportListExtPoint())
                .add("orgUnitPaymentReport", new OrgUnitReportDefinition("(Старый) Отчет по оплатам", "orgUnitPaymentReport", UNISC_ORGUNIT_REPORT_BLOCK, "ru.tandemservice.unisc.component.report.PaymentReport.PaymentReportList", "viewPaymentReportList"))
                .add("orgUnitAgreementIncomeReport", new OrgUnitReportDefinition("(Старый) Поступление средств по договорам", "orgUnitAgreementIncomeReport", UNISC_ORGUNIT_REPORT_BLOCK, "ru.tandemservice.unisc.component.report.AgreementIncomeReport.AgreementIncomeReportList", "viewAgreementIncomeReportList"))
                .add("orgUnitPayDebtorsReport", new OrgUnitReportDefinition("(Старый) Список должников по оплате", "orgUnitPayDebtorsReport", UNISC_ORGUNIT_REPORT_BLOCK, "ru.tandemservice.unisc.component.report.PayDebtorsReport", "viewPayDebtorsReport"))
                .add("orgUnitAgreementIncomeSummaryReport", new OrgUnitReportDefinition("(Старый) Сводка поступлений по договорам", "orgUnitAgreementIncomeSummaryReport", UNISC_ORGUNIT_REPORT_BLOCK, "ru.tandemservice.unisc.component.report.AgreementIncomeSummaryReport.AgreementIncomeSummaryReportList", "viewAgreementIncomeSummaryReport"))
                .add("orgUnitGroupIncomeReport", new OrgUnitReportDefinition("(Старый) Приход средств (по группе)", "orgUnitGroupIncomeReport", UNISC_ORGUNIT_REPORT_BLOCK, "ru.tandemservice.unisc.component.report.GroupIncomeReport.GroupIncomeReportList", "viewGroupIncomeReport"))
                .add("orgUnitStudentsWithoutContractsReport", new OrgUnitReportDefinition("(Старый) Перечень студентов без договоров", "orgUnitStudentsWithoutContractsReport", UNISC_ORGUNIT_REPORT_BLOCK, "ru.tandemservice.unisc.component.report.StudentsWithoutContractsReport", "viewStudentsWithoutContractsReport"))
                .create();
    }
}
