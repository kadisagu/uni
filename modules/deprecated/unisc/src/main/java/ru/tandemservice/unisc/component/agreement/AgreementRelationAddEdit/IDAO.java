package ru.tandemservice.unisc.component.agreement.AgreementRelationAddEdit;

import org.tandemframework.shared.person.base.entity.PersonRole;

/**
 * @author vdanilov
 */
@SuppressWarnings("deprecation")
public interface IDAO<T extends PersonRole> extends ru.tandemservice.unisc.component.agreement.AgreementAddEdit.IDAO<Model<T>> {

}
