package ru.tandemservice.unisc.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.ui.formatters.RuMoneyFormatter;
import ru.tandemservice.uni.ws.debts.IStudentDebtResolver;
import ru.tandemservice.uni.ws.debts.StudentDebtsService.StudentDebtInfo;
import ru.tandemservice.unisc.dao.payment.IUniscEduAgreementPaymentDAO;
import ru.tandemservice.unisc.dao.payment.IUniscEduAgreementPaymentState;
import ru.tandemservice.unisc.entity.agreements.student.gen.UniscEduAgreement2StudentGen;

/**
 * @author root
 */
public class UniscStudentDebtResolver extends UniBaseDao implements IStudentDebtResolver {

    @Override
    public Map<Long, Collection<StudentDebtInfo>> getDebtMap(final Collection<Long> ids) {
        final Date now = new Date();
        final Map<Long, Collection<StudentDebtInfo>> result = SafeMap.get(ArrayList.class);

        BatchUtils.execute(ids, 200, new BatchUtils.Action<Long>() {
            @Override public void execute(final Collection<Long> ids) {
                final Map<Long, Long> a2s = this.getAgreement2StudentMap(ids);
                final Map<Long, IUniscEduAgreementPaymentState> debtMap = IUniscEduAgreementPaymentDAO.INSTANCE.get().getStateMap(a2s.keySet(), now);

                for (final Map.Entry<Long, IUniscEduAgreementPaymentState> debtEntry: debtMap.entrySet()) {
                    final double[] debt = debtEntry.getValue().getDebt(null);
                    if ((debt[0] > 0) || (debt[1] > 0)) {
                        final Collection<StudentDebtInfo> studentInfoList = result.get(a2s.get(debtEntry.getKey()));
                        if (debt[0] > 0) {
                            final StudentDebtInfo info = new StudentDebtInfo();
                            info.module= "unisc";
                            info.key = "ag.debt."+debtEntry.getKey();
                            info.keyTitle = "Долг по договору (основная сумма)";
                            info.value = String.valueOf(debt[0]);
                            info.valueTitle = RuMoneyFormatter.INSTANCE.format(debt[0]);
                            studentInfoList.add(info);
                        }
                        if (debt[1] > 0) {
                            final StudentDebtInfo info = new StudentDebtInfo();
                            info.module= "unisc";
                            info.key = "ag.fines"+debtEntry.getKey();
                            info.keyTitle = "Долг по договору (пеней)";
                            info.value = String.valueOf(debt[1]);
                            info.valueTitle = RuMoneyFormatter.INSTANCE.format(debt[1]);
                            studentInfoList.add(info);
                        }
                    }
                }

            }

            private Map<Long, Long> getAgreement2StudentMap(final Collection<Long> ids) {
                final Map<Long, Long> a2s = new HashMap<Long, Long>();
                final MQBuilder b = new MQBuilder(UniscEduAgreement2StudentGen.ENTITY_CLASS, "rel", new String[] {
                        UniscEduAgreement2StudentGen.agreement().id().s(),
                        UniscEduAgreement2StudentGen.student().id().s()
                });
                b.add(MQExpression.eq("rel", UniscEduAgreement2StudentGen.active().toString(), Boolean.TRUE));
                b.add(MQExpression.in("rel", UniscEduAgreement2StudentGen.student().id().s(), ids));

                final List<Object[]> rows = b.getResultList(UniscStudentDebtResolver.this.getSession());
                for (final Object[] row: rows) {
                    a2s.put((Long)row[0], (Long)row[1]);
                }
                return a2s;
            }
        });



        return result;
    }

}
