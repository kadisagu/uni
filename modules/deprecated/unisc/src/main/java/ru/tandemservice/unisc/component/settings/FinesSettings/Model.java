package ru.tandemservice.unisc.component.settings.FinesSettings;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisc.entity.config.UniscFinesHistory;

public class Model {

	private IDataSettings _settings;
	public IDataSettings getSettings() { return this._settings; }
	public void setSettings(final IDataSettings settings) { this._settings = settings; }

	public EducationYear getEducationYear() { return (EducationYear) getSettings().getEntry("educationYear").getValue(); }

	private ISelectModel years = null;
	public ISelectModel getYears() { return this.years; }
	public void setYears(final ISelectModel years) { this.years = years; }

	private DynamicListDataSource<UniscFinesHistory> dataSource;
	public DynamicListDataSource<UniscFinesHistory> getDataSource() { return dataSource; }
	public void setDataSource(final DynamicListDataSource<UniscFinesHistory> dataSource) { this.dataSource = dataSource; }

}
