package ru.tandemservice.unisc.component.settings.CostSettings;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {

	@Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
	void refreshDataSource(Model model);
	
}
