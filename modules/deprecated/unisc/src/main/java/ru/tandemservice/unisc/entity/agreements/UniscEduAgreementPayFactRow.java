package ru.tandemservice.unisc.entity.agreements;

import java.util.Date;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.view.formatter.DateFormatter;

import ru.tandemservice.uni.ui.formatters.MoneyFormatter;
import ru.tandemservice.unisc.dao.config.UniscEduOrgUnitConfigDAO;
import ru.tandemservice.unisc.dao.payment.IUniscEduAgreementPaymentEvent;
import ru.tandemservice.unisc.entity.agreements.gen.UniscEduAgreementPayFactRowGen;

/**
 * Договор
 */
public class UniscEduAgreementPayFactRow extends UniscEduAgreementPayFactRowGen implements ITitled, IUniscEduAgreementPaymentEvent.FactRow
{
    public static final String VIEW_P_J_COST_AS_DOUBLE = "jCostAsDouble";
    public static final String VIEW_P_J_COST_AS_STRING = "jCostAsString";
    public static final String VIEW_P_N_COST_AS_DOUBLE = "nCostAsDouble";
    public static final String VIEW_P_N_COST_AS_STRING = "nCostAsString";
    public static final String VIEW_P_COST_AS_DOUBLE = "costAsDouble";
    public static final String VIEW_P_COST_AS_STRING = "costAsString";
    public static final String VIEW_P_DATE_STR = "dateAsString";
    public static final String VIEW_P_PAYER = "payerTitle";

	private static final MoneyFormatter mf = MoneyFormatter.ruMoneyFormatter(3);
	@Override
	public double getJCostAsDouble() { return UniscEduOrgUnitConfigDAO.wrap(super.getJCost()); }
	public void setJCostAsDouble(final double d) { setJCost(UniscEduOrgUnitConfigDAO.unwrap(d)); }
	public String getJCostAsString() { return UniscEduAgreementPayFactRow.mf.format(this.getJCost()); }

	@Override
	public double getNCostAsDouble() { return UniscEduOrgUnitConfigDAO.wrap(super.getNCost()); }
	public void setNCostAsDouble(final double d) { setNCost(UniscEduOrgUnitConfigDAO.unwrap(d)); }
	public String getNCostAsString() { return UniscEduAgreementPayFactRow.mf.format(this.getNCost()); }


	public double getCostAsDouble() { return (getJCostAsDouble() + getNCostAsDouble()); }
	public String getCostAsString() { return UniscEduAgreementPayFactRow.mf.format(this.getJCost() + this.getNCost()); }


	@Override
	public Date getEventDate() { return getDate(); }
	public String getDateAsString() { return DateFormatter.DEFAULT_DATE_FORMATTER.format(super.getDate()); }

	@Override
	public String getTitle() {
		return ((isFinesPayment() ? "Погашение пеней " : "Платеж ") + getDateAsString() + ": фл=" + getNCostAsString() + ", юл=" + getJCostAsString());
	}

    public String getPayerTitle(){
        String payerType = getJCost() == 0L ? "физическое лицо" : "юридическое лицо";
        return getPayer() == null ? payerType : getPayer() + " (" + payerType + ")"; 
    }
}
