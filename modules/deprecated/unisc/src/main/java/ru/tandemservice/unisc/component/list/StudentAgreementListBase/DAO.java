/* $Id: DAO.java 8433 2009-06-11 06:38:32Z vdanilov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unisc.component.list.StudentAgreementListBase;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.settings.IDataSettingsManager;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonRole;
import org.tandemframework.shared.person.base.entity.gen.IdentityCardGen;
import org.tandemframework.shared.person.base.entity.gen.PersonGen;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.education.gen.EducationOrgUnitGen;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.gen.StudentGen;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;
import ru.tandemservice.uni.ui.formatters.RuMoneyFormatter;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisc.UniscComponents;
import ru.tandemservice.unisc.dao.agreement.IUniscEduAgreementDAO;
import ru.tandemservice.unisc.dao.payment.IUniscEduAgreementPaymentDAO;
import ru.tandemservice.unisc.dao.payment.IUniscEduAgreementPaymentState;
import ru.tandemservice.unisc.entity.agreements.UniscEduAdditAgreement;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayPlanRow;
import ru.tandemservice.unisc.entity.agreements.gen.UniscEduAdditAgreementGen;
import ru.tandemservice.unisc.entity.agreements.gen.UniscEduAgreementBaseGen;
import ru.tandemservice.unisc.entity.agreements.gen.UniscEduAgreementNaturalPersonGen;
import ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student;
import ru.tandemservice.unisc.entity.agreements.student.gen.UniscEduAgreement2StudentGen;
import ru.tandemservice.unisc.entity.catalog.UniscCustomerType;
import ru.tandemservice.unisc.entity.catalog.UniscPayPeriod;
import ru.tandemservice.unisc.entity.catalog.UniscPayPeriod.Freq;
import ru.tandemservice.unisc.entity.catalog.UniscTemplateDocument;
import ru.tandemservice.unisc.entity.catalog.gen.UniscCustomerTypeGen;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;
import ru.tandemservice.unisc.entity.config.gen.UniscEduOrgUnitGen;

import java.util.*;

/**
 * @author vdanilov
 */
public class DAO<M extends Model> extends UniDao<M> implements IDAO<M>
{
    @Override
    public void prepare(final M model)
    {
        model.setCourseListModel(new LazySimpleSelectModel<>(DevelopGridDAO.getCourseList()));
        model.setCustomerTypeList(this.getList(UniscCustomerType.class, UniscCustomerTypeGen.P_HIDDEN, Boolean.FALSE, ICatalogItem.CATALOG_ITEM_CODE));
        model.setCipherModel(new UniQueryFullCheckSelectModel(UniscEduOrgUnit.P_CIPHER_WITH_TITLE)
        {
            @Override
            protected MQBuilder query(final String alias, final String filter)
            {
                final MQBuilder builder = new MQBuilder(UniscEduOrgUnitGen.ENTITY_CLASS, alias);
                if (model.getEduYear() != null) {
                    builder.add(MQExpression.eq(alias, UniscEduOrgUnitGen.L_EDUCATION_YEAR, model.getEduYear()));
                }
                builder.addOrder(alias, UniscEduOrgUnitGen.L_EDUCATION_YEAR + ".code");
                builder.addOrder(alias, UniscEduOrgUnitGen.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnitGen.L_EDUCATION_LEVEL_HIGH_SCHOOL + ".code");
                builder.addOrder(alias, UniscEduOrgUnitGen.L_EDUCATION_ORG_UNIT + ".id");
                builder.addOrder(alias, UniscEduOrgUnitGen.P_CIPHER);
                if (null != filter) {
                    builder.add(MQExpression.like(alias, UniscEduOrgUnitGen.P_CIPHER, filter));
                }
                return builder;
            }
        });
        model.setFormativeOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING));
        model.setTerritorialOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL));
        model.setEducationLevelsHighSchoolModel(new FullCheckSelectModel(EducationLevelsHighSchool.P_DISPLAYABLE_TITLE)
        {
            @Override
            @SuppressWarnings({"unchecked"})
            public ListResult findValues(final String filter)
            {
                final List<OrgUnit> formativeOrgUnitList = (List<OrgUnit>) model.getSettings().get("formativeOrgUnitList");
                final List<OrgUnit> territorialOrgUnitList = (List<OrgUnit>) model.getSettings().get("territorialOrgUnitList");
                final List<EducationLevelsHighSchool> list = UniDaoFacade.getEducationLevelDao().getEducationLevelsHighSchoolList(formativeOrgUnitList, territorialOrgUnitList, filter);
                return new ListResult<>(list);
            }
        });
        model.setDevelopFormModel(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setDevelopConditionModel(EducationCatalogsManager.getDevelopConditionSelectModel());
        model.setDevelopTechModel(EducationCatalogsManager.getDevelopTechSelectModel());
        model.setDevelopPeriodModel(EducationCatalogsManager.getDevelopPeriodSelectModel());

        model.setStudentStatusList(this.getList(StudentStatus.class, StudentStatus.P_USED_IN_SYSTEM, Boolean.TRUE, StudentStatus.P_PRIORITY));
        model.setStudentCategoryList(this.getCatalogItemListOrderByCode(StudentCategory.class));
        model.setEduYearList(new EducationYearModel());
        if (model.getContractActive() == null) {
            this.restoreDefaults(model);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void refreshStudentContractsDataSource(final M model, final boolean forPrint)
    {
        final List<Model.Row> result = this.refreshStudentContractsDataSourceData(model, forPrint);

        Debug.begin("refreshStudentContractsDataSource(result="+result.size()+")");
        try {
            final DynamicListDataSource<Model.Row> ds = model.getStudentContractsDataSource();
            /* заказчики */
            {
                final Map<Long, StringBuilder> agreementCustomerMap = new HashMap<>();
                final Collection<Long> agreementIds = new ArrayList<>();
                for (final Model.Row row : result)
                {
                    final UniscEduAgreementBase a = row.getAgreement();
                    if (a.getCustomerType().hasJuridicalPart() && (null != a.getJuridicalPerson()))
                    {
                        final StringBuilder list = new StringBuilder();
                        list.append(a.getJuridicalPerson().getContractor().getTitle())
                        .append(" (")
                        .append(this.fio(a.getJuridicalPerson().getFirstName(), a.getJuridicalPerson().getLastName(), a.getJuridicalPerson().getMiddleName()))
                        .append(")");
                        agreementCustomerMap.put(a.getId(), list);
                    }
                    if (a.getCustomerType().hasNaturalPart()) {
                        agreementIds.add(a.getId());
                    }
                }

                BatchUtils.execute(agreementIds, 200, new BatchUtils.Action<Long>() {
                    @Override
                    public void execute(final Collection<Long> ids)
                    {
                        final MQBuilder nPersonsBuilder = new MQBuilder(UniscEduAgreementNaturalPersonGen.ENTITY_CLASS, "np");
                        nPersonsBuilder.add(MQExpression.in("np", UniscEduAgreementNaturalPersonGen.L_OWNER + ".id", ids));
                        for (final UniscEduAgreementNaturalPerson np : nPersonsBuilder.<UniscEduAgreementNaturalPerson>getResultList(DAO.this.getSession()))
                        {
                            StringBuilder list = agreementCustomerMap.get(np.getOwner().getId());
                            if (null == list) {
                                agreementCustomerMap.put(np.getOwner().getId(), list = new StringBuilder());
                            }
                            if (list.length() > 0) {
                                list.append(",\n");
                            }
                            list.append(DAO.this.fio(np.getLastName(), np.getFirstName(), np.getMiddleName()));
                        }
                    }
                });

                ((BlockColumn<StringBuilder>) ds.getColumn("agreementCustomer")).setValueMap(agreementCustomerMap);
            }

            /* задолжность */
            {
                final Map<Long, UniscEduAgreement2Student> agreement2StudentMap = new HashMap<>(result.size());
                for (final Model.Row row: result) {
                    agreement2StudentMap.put(row.getRelation().getAgreement().getId(), row.getRelation());
                }

                final Date now = new Date();
                final Map<Long, String> payPlanStatusMap = new HashMap<>();
                final Map<Long, String> payPlanDebtMap = new HashMap<>();
                final Map<Long, IUniscEduAgreementPaymentState> stateMap = IUniscEduAgreementPaymentDAO.INSTANCE.get().getStateMap(agreement2StudentMap.keySet(), null);
                final Session session = this.getSession();
                for (final Map.Entry<Long, IUniscEduAgreementPaymentState> e : stateMap.entrySet())
                {
                    final IUniscEduAgreementPaymentState state = e.getValue();

                    /* долг (на текущий момент) */ {
                        final double[] d = state.getDebt(now);
                        final String debt = (((d[0] > 0) || (d[1] > 0)) ? (RuMoneyFormatter.INSTANCE.format(d[0]) + "\n(" + RuMoneyFormatter.INSTANCE.format(d[1]) + ")") : null);
                        payPlanDebtMap.put(e.getKey(), debt);
                    }

                    final UniscEduAgreement2Student a2s = agreement2StudentMap.get(e.getKey());
                    final int studentCourse = a2s.getStudent().getCourse().getIntValue();


                    int plan_p=0, fact_p=0;
                    double plan_c=0, fact_c=0;
                    for (final IUniscEduAgreementPaymentState.StatePlanRow planRow: state.getPlanRows()) {
                        final UniscEduAgreementPayPlanRow row = ((UniscEduAgreementPayPlanRow)session.get(UniscEduAgreementPayPlanRow.class, planRow.getId())); /* object is alredy in session cache */
                        final UniscPayPeriod payPlanFreq = row.getOwner().getPayPlanFreq();
                        final boolean isCurrentCourse = (Freq.TOTAL.equals(payPlanFreq.freq()) ? (studentCourse == 1) : (studentCourse == payPlanFreq.getCourseNumber(row.getOwner().getConfig(), row.getStage())));
                        if (isCurrentCourse) {
                            final double cost = planRow.getCost();
                            final double pays = planRow.getDebt()[0];
                            plan_p ++;
                            if (pays <= 0 /* полностью оплачен */) { fact_p ++; }

                            plan_c += cost;
                            fact_c += (cost - pays); /* оплачено - разница между стоимостьюэтапа и его долгом (можно, конечно, взять все его внутренние платежи и сложить их... но зжачем!) */
                        }
                    }
                    if (plan_p > 0) {
                        final String status = ((fact_p+"/"+plan_p) + " ("+(plan_c > 0 ? (int)(fact_c*100.0/plan_c) : 100)+"%)");
                        payPlanStatusMap.put(e.getKey(), status);
                    }
                }
                ((BlockColumn<String>) ds.getColumn("payPlanDebt")).setValueMap(payPlanDebtMap);
                ((BlockColumn<String>)ds.getColumn("payPlanStatus")).setValueMap(payPlanStatusMap);


            }
        } finally {
            Debug.end();
        }
    }

    private List<Model.Row> refreshStudentContractsDataSourceData(final M model, final boolean forPrint) {
        Debug.begin("refreshStudentContractsDataSourceData(forPrint="+forPrint+")");
        try {
            final Map<UniscEduAgreement2Student, List<UniscEduAgreementBase>> student2agreementListMap = new LinkedHashMap<>();
            final DynamicListDataSource<Model.Row> ds = model.getStudentContractsDataSource();

            final MQBuilder studentBuilder = this.getStudentBuilder(model);
            final MQBuilder agreementBuilder = this.getAgreementBuilder(model);

            /* relations (all) */
            {
                final MQBuilder builder = new MQBuilder(UniscEduAgreement2StudentGen.ENTITY_CLASS, "e");
                builder.addJoin("e", UniscEduAgreement2StudentGen.L_AGREEMENT, "mainAgreement");
                builder.addJoin("e", UniscEduAgreement2StudentGen.L_STUDENT, "student");
                builder.add(MQExpression.in("student", "id", studentBuilder));

                builder.addJoin("student", PersonRole.L_PERSON, "person");
                builder.addJoin("person", Person.L_IDENTITY_CARD, "identityCard");

                builder.addOrder("identityCard", IdentityCardGen.P_LAST_NAME);
                builder.addOrder("identityCard", IdentityCardGen.P_FIRST_NAME);
                builder.addOrder("identityCard", IdentityCardGen.P_MIDDLE_NAME);
                builder.addOrder("person", "id");
                builder.addOrder("student", StudentGen.L_STUDENT_CATEGORY + "." + ICatalogItem.CATALOG_ITEM_CODE);

                // условия фильтров
                {
                    if (model.getContractActive() != null)
                        builder.add(MQExpression.eq("e", UniscEduAgreement2StudentGen.P_ACTIVE, model.getContractActive()));
                    if (model.getEduYearWithoutAdditAgreement() != null)
                    {
                        final MQBuilder notExistsAdditAgreement = new MQBuilder(UniscEduAdditAgreementGen.ENTITY_CLASS, "addit_ne", new String[]{UniscEduAdditAgreementGen.L_AGREEMENT + ".id"});
                        notExistsAdditAgreement.addJoin("addit_ne", UniscEduAgreementBaseGen.L_CONFIG, "config_ne");
                        notExistsAdditAgreement.add(MQExpression.eq("config_ne", UniscEduOrgUnitGen.L_EDUCATION_YEAR, model.getEduYearWithoutAdditAgreement()));
                        notExistsAdditAgreement.add(MQExpression.eqProperty("addit_ne", UniscEduAdditAgreementGen.L_AGREEMENT + ".id", "mainAgreement", "id"));
                        builder.add(MQExpression.notExists(notExistsAdditAgreement));
                        builder.add(MQExpression.less("mainAgreement", UniscEduAgreementBase.config().educationYear().intValue().s(), model.getEduYearWithoutAdditAgreement().getIntValue()));
                    }
                }

                if (model.isShowAdditional()) {
                    final MQBuilder mainAgreementBuilder = new MQBuilder(UniscEduAdditAgreementGen.ENTITY_CLASS, "addit", new String[] {UniscEduAdditAgreementGen.L_AGREEMENT + ".id"});
                    mainAgreementBuilder.add(MQExpression.in("addit", "id", agreementBuilder));
                    builder.add(MQExpression.or(
                        MQExpression.in("mainAgreement", "id", agreementBuilder),
                        MQExpression.in("mainAgreement", "id", mainAgreementBuilder)
                    ));
                } else {
                    builder.add(MQExpression.in("mainAgreement", "id", agreementBuilder));
                }

                this.orderAgreementProperties(builder, "mainAgreement");

                final List<UniscEduAgreement2Student> relations = builder.getResultList(this.getSession());
                for (final UniscEduAgreement2Student rel : relations)
                {
                    final ArrayList<UniscEduAgreementBase> agreements = new ArrayList<>();
                    agreements.add(rel.getAgreement());
                    student2agreementListMap.put(rel, agreements);
                }
            }

            /* additional agreement */
            if (model.isShowAdditional())
            {

                final MQBuilder builder = new MQBuilder(UniscEduAgreement2StudentGen.ENTITY_CLASS, "e");
                builder.addJoin("e", UniscEduAgreement2StudentGen.L_AGREEMENT, "mainAgreement");
                builder.addDomain("additAgreement", UniscEduAdditAgreementGen.ENTITY_CLASS).addSelect("additAgreement");
                builder.add(MQExpression.eqProperty("mainAgreement", "id", "additAgreement", UniscEduAdditAgreementGen.L_AGREEMENT + ".id"));
                builder.addJoin("e", UniscEduAgreement2StudentGen.L_STUDENT, "student");
                builder.add(MQExpression.in("student", "id", studentBuilder));
                builder.add(MQExpression.in("additAgreement", "id", agreementBuilder));

                this.orderAgreementProperties(builder, "additAgreement");
                final List<Object[]> rows = builder.getResultList(this.getSession());

                for (final Object[] row : rows)
                {
                    final List<UniscEduAgreementBase> list = student2agreementListMap.get(row[0]);
                    if (null != list) {
                        list.add((UniscEduAgreementBase) row[1]);
                    }
                }
            }

            return this.setupDataSource(ds, student2agreementListMap, forPrint);
        } finally {
            Debug.end();
        }
    }

    private List<Model.Row> setupDataSource(final DynamicListDataSource<Model.Row> ds, final Map<UniscEduAgreement2Student, List<UniscEduAgreementBase>> student2agreementListMap, final boolean forPrint) {
        Debug.begin("setupDataSource(forPrint="+forPrint+", size="+student2agreementListMap.size()+")");
        try {
            List<Model.Row> result = new ArrayList<>();
            for (final Map.Entry<UniscEduAgreement2Student, List<UniscEduAgreementBase>> e : student2agreementListMap.entrySet()) {
                for (final UniscEduAgreementBase a : e.getValue()) {
                    result.add(new Model.Row(e.getKey(), a));
                }
            }

            if (forPrint) {
                ds.setMustFindNearestCountRowValue(false);
                ds.setTotalSize(result.size());
                ds.setCountRow(result.size());
                ds.createPage(result);
            } else {
                ds.setMustFindNearestCountRowValue(true);
                UniBaseUtils.createPage(ds, result);
            }

            // только те, которые показываем
            result = ds.getEntityList();


            // fetch student data
            final Session session = this.getSession();
            BatchUtils.execute(result, 100, new BatchUtils.Action<Model.Row>() {
                @Override public void execute(final Collection<Model.Row> rows) {
                    // fetch agreement data
                    {
                        final Set<Long> ids = new HashSet<>();
                        for (final Model.Row row: rows) { ids.add(row.getAgreement().getId()); }

                        final MQBuilder builder = new MQBuilder(UniscEduAgreementBase.ENTITY_CLASS, "a");
                        builder.add(MQExpression.in("a", "id", ids));
                        builder.addJoinFetch("a", UniscEduAgreementBaseGen.L_CONFIG, "config");
                        builder.addJoinFetch("config", UniscEduOrgUnitGen.L_EDUCATION_ORG_UNIT, "edu");
                        builder.getResultList(session);
                    }

                    // fetch student data
                    {
                        final Set<Long> ids = new HashSet<>();
                        for (final Model.Row row: rows) { ids.add(row.getStudent().getId()); }

                        final MQBuilder builder = new MQBuilder(Student.ENTITY_CLASS, "student");
                        builder.add(MQExpression.in("student", "id", ids));
                        builder.addJoinFetch("student", PersonRole.L_PERSON, "person");
                        builder.addJoinFetch("person", Person.L_IDENTITY_CARD, "identityCard");
                        builder.getResultList(session);
                    }
                }
            });


            return result;
        } finally {
            Debug.end();
        }
    }

    @Override
    public void updateCheckBySignature(final M model, final Long id)
    {
        final UniscEduAgreementBase a = this.get(UniscEduAgreementBase.class, id);
        if (null == a) {
            return;
        }
        a.setCheckedBySignature(!a.isCheckedBySignature());
        this.getSession().saveOrUpdate(a);
    }

    @Override
    public String getPrintComponentName(final Long documentId)
    {
        final UniscEduAgreementBase agreement = (UniscEduAgreementBase) this.get(documentId);
        if (agreement instanceof UniscEduAdditAgreement) {
            return UniscComponents.STUDENT_ADDITIONAL_AGREMENT_PRINT;
        }
        return UniscComponents.STUDENT_CONTRACT_PRINT;
    }

    @Override
    public Long getPrintObjectId(final Long documentId)
    {
        final UniscEduAgreementBase agreement = (UniscEduAgreementBase) this.get(documentId);
        if (agreement instanceof UniscEduAdditAgreement) {
            return agreement.getId();
        }
        return IUniscEduAgreementDAO.INSTANCE.get().getRelation(agreement.getAgreement()).getId();
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public void restoreDefaults(final M model)
    {
        final IDataSettings settings = model.getSettings();
        settings.clear();
        settings.set("eduYear", this.get(EducationYear.class, EducationYear.P_CURRENT, Boolean.TRUE));
        settings.set("contractActive", new IdentifiableWrapper(Model.TRUE_OPTION_ID, "Действующий"));
        settings.set("showAdditional", Boolean.TRUE);
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public byte[] prepareReport(final M model, final Long principalId)
    {
        final DynamicListDataSource<Model.Row> dataSource = model.getStudentContractsDataSource();
        final long dsCountRow = dataSource.getCountRow();
        final long dsTotalSize = dataSource.getTotalSize();

        try {
            this.refreshStudentContractsDataSource(model, true);

            final ITemplateDocument templateDocument = this.getCatalogItem(UniscTemplateDocument.class, "studentContractList");
            final RtfDocument document = new RtfReader().read(templateDocument.getContent());

            // шапка отчета
            final RtfInjectModifier injectModifier = new RtfInjectModifier();
            injectModifier.put("formationDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));
            injectModifier.modify(document);
            // значения фильтров в шапке
            final List<String[]> filterRows = new ArrayList<>();
            {
                if (model.getEduYear() != null) {
                    filterRows.add(new String[]{"Учебный год:", model.getEduYear().getTitle()});
                }
                if (model.getContractActive() != null) {
                    filterRows.add(new String[]{"Состояние договора:", model.getContractActive() ? "Действующий" : "Недействующий"});
                }

                final IDataSettings settings = model.getSettings();
                final String lastName = (String) settings.get("lastNameFilter");
                final String firstName = (String) settings.get("firstNameFilter");
                final String agreementNumber = (String) settings.get("agreementNumberFilter");
                Date changeDate = (Date) settings.get("changeDate");

                if (null != changeDate)
                    filterRows.add(new String[]{"Изменен с:", DateFormatter.DEFAULT_DATE_FORMATTER.format(changeDate)});
                if (StringUtils.isNotEmpty(agreementNumber)) {
                    filterRows.add(new String[]{"Номер договора:", agreementNumber});
                }
                if (StringUtils.isNotEmpty(lastName)) {
                    filterRows.add(new String[]{"Фамилия:", lastName});
                }
                if (StringUtils.isNotEmpty(firstName)) {
                    filterRows.add(new String[]{"Имя:", firstName});
                }
                if (model.getStatusActive() != null) {
                    filterRows.add(new String[]{"Статус студента:", model.getStatusActive() ? "Активные студенты" : "Неактивные студенты"});
                }
                if (model.getStudentStatus() != null) {
                    filterRows.add(new String[]{"Состояние студента:", model.getStudentStatus().getTitle()});
                }
                if (model.getStudentCategory() != null) {
                    filterRows.add(new String[]{"Категория обучаемого:", model.getStudentCategory().getTitle()});
                }
                if ((model.getCourseList() != null) && !model.getCourseList().isEmpty()) {
                    filterRows.add(new String[]{"Курс:", UniStringUtils.join(model.getCourseList(), Course.P_TITLE, ", ")});
                }
                if (model.getCustomerType() != null) {
                    filterRows.add(new String[]{"Тип заказчика:", model.getCustomerType().getTitle()});
                }

                final UniscEduOrgUnit cipher = model.getCipher();
                if (null != cipher) {
                    filterRows.add(new String[]{"Шифр:", cipher.getTitle()});
                } else
                {
                    if ((null != model.getFormativeOrgUnitList()) && !model.getFormativeOrgUnitList().isEmpty()) {
                        filterRows.add(new String[]{"Формирующее подр.:", UniStringUtils.join(model.getFormativeOrgUnitList(), OrgUnit.P_TITLE, ", ")});
                    }
                    if ((null != model.getTerritorialOrgUnitList()) && !model.getTerritorialOrgUnitList().isEmpty()) {
                        filterRows.add(new String[]{"Территориальное подр.:", UniStringUtils.join(model.getTerritorialOrgUnitList(), OrgUnit.P_TITLE, ", ")});
                    }
                    if (null != model.getEducationLevelsHighSchool()) {
                        filterRows.add(new String[]{"Направление подготовки (специальность):", model.getEducationLevelsHighSchool().getTitle()});
                    }
                    if ((null != model.getDevelopFormList()) && !model.getDevelopFormList().isEmpty()) {
                        filterRows.add(new String[]{"Форма освоения:", UniStringUtils.join(model.getDevelopFormList(), ICatalogItem.CATALOG_ITEM_TITLE, ", ")});
                    }
                    if ((null != model.getDevelopConditionList()) && !model.getDevelopConditionList().isEmpty()) {
                        filterRows.add(new String[]{"Условие освоения:", UniStringUtils.join(model.getDevelopConditionList(), ICatalogItem.CATALOG_ITEM_TITLE, ", ")});
                    }
                    if ((null != model.getDevelopTechList()) && !model.getDevelopTechList().isEmpty()) {
                        filterRows.add(new String[]{"Технология освоения:", UniStringUtils.join(model.getDevelopTechList(), ICatalogItem.CATALOG_ITEM_TITLE, ", ")});
                    }
                    if ((null != model.getDevelopPeriodList()) && !model.getDevelopPeriodList().isEmpty()) {
                        filterRows.add(new String[]{"Срок освоения:", UniStringUtils.join(model.getDevelopPeriodList(), ICatalogItem.CATALOG_ITEM_TITLE, ", ")});
                    }
                }
                if (model.getEduYearWithoutAdditAgreement() != null) {
                    filterRows.add(new String[]{"Нет доп. соглашения по договору на учебный год:", model.getEduYearWithoutAdditAgreement().getTitle()});
                }
                if (model.getChecked() != null) {
                    filterRows.add(new String[]{"Проверен:", model.getChecked() ? "Да" : "Нет"});
                }
            }
            new RtfTableModifier().put("N", filterRows.toArray(new String[filterRows.size()][])).modify(document);

            // колонки таблицы договоров
            final Map<String, Integer> columnsWidth = new HashMap<>();
            columnsWidth.put("n", 1);
            columnsWidth.put("agreement.config.educationOrgUnit.territorialOrgUnit.territorialFullTitle", 197);
            columnsWidth.put("agreement.config.educationOrgUnit.formativeOrgUnit.fullTitle", 235);
            columnsWidth.put("null.documentNumber", 163);
            columnsWidth.put("mainAgreement.number", 100);
            final String fioColumnKey = "student.person.fio";
            columnsWidth.put(fioColumnKey, 79 * 3);
            columnsWidth.put("student.status.title", 25 * 6);
            columnsWidth.put("student.course.title", 85);
            columnsWidth.put("student.studentCategory.title", 45 * 3);
            columnsWidth.put("agreement.config.educationOrgUnit.titleWithFormAndCondition", 115 * 2);
            columnsWidth.put("agreement.config.description", 137 * 2);
            columnsWidth.put("agreement.config.conditions", 19 * 6);
            columnsWidth.put("agreement.payPlanFreq.title", 137);
            columnsWidth.put("agreement.customerType.title", 175);
            columnsWidth.put("agreementCustomer", 200);
            columnsWidth.put("agreement.formingDate", 25 * 6);
            columnsWidth.put("agreement.deadlineDate", 25 * 6);
            columnsWidth.put("agreement.checkedBySignature", 127);
            columnsWidth.put("payPlanDebt", 200);
            columnsWidth.put("payPlanStatus", 100);
            columnsWidth.put("agreement.config.cipher", 80);
            columnsWidth.put("agreement.lastModificationDate", 25*6);

            final List<AbstractColumn> listColumns = this.getColumns(model, principalId);
            if (listColumns.isEmpty()) {
                throw new IllegalStateException();
            }

            final List<String[]> reportRows = new ArrayList<>();
            int columnCount = 1; // число колонок таблицы договоров
            // заголовок таблицы договоров
            {
                final List<String> headerRow = new ArrayList<>();
                headerRow.add("№");
                for (final AbstractColumn c : listColumns) {
                    if (columnsWidth.containsKey(c.getName()))
                    {
                        headerRow.add(c.getCaption());
                        columnCount++;
                    }
                }
                reportRows.add(headerRow.toArray(new String[headerRow.size()]));
            }

            /* строки отчета */
            {
                int rowNumber = 1;
                final List<Model.Row> agreementList = dataSource.getEntityList();
                for (final Model.Row listRow : agreementList)
                {
                    final List<String> reportRow = new ArrayList<>();
                    reportRow.add(String.valueOf(rowNumber++));
                    for (final AbstractColumn listColumn : listColumns)
                    {
                        if (!columnsWidth.containsKey(listColumn.getName())) {
                            continue;
                        }
                        if (listColumn instanceof IMultiValuesColumn)
                        {
                            final StringBuilder sb = new StringBuilder();
                            for (final Object entity : ((IMultiValuesColumn) listColumn).getEntityList(listRow))
                            {
                                if (sb.length() > 0) {
                                    sb.append(", ");
                                }
                                Object content = listColumn.getContent((IEntity) entity);
                                if ((content instanceof String) && (StringUtils.isNotBlank((String) content))) {
                                    content = ((String) content).replace("<br/>", " ");
                                }
                                sb.append(content);
                            }
                            reportRow.add(sb.toString());
                        } else {
                            reportRow.add(this.getColumnContent(listRow, listColumn));
                        }
                    }
                    reportRows.add(reportRow.toArray(new String[reportRow.size()]));
                }
            }

            final int[] parts = new int[columnCount];
            /* ширина колонок */
            {
                final int totalWidth = 3353;//2802;
                int i = 0;
                parts[i] = columnsWidth.get("n");
                int used = parts[i++];
                int fioPosition = 0;
                for (final AbstractColumn c : listColumns)
                {
                    if (!columnsWidth.containsKey(c.getName())) {
                        continue;
                    }
                    if (fioColumnKey.equals(c.getName()))
                    {
                        fioPosition = i;
                        parts[i++] = 0;
                        continue;
                    }
                    parts[i] = columnsWidth.get(c.getName());
                    used = used + parts[i++];
                }
                parts[fioPosition] = totalWidth - used;
            }

            final RtfTableModifier tableModifier = new RtfTableModifier().put("T", reportRows.toArray(new String[reportRows.size()][]));
            tableModifier.put("T", new RtfRowIntercepterBase()
            {
                @Override
                public void beforeModify(final RtfTable table, final int currentRowIndex)
                {
                    RtfUtil.splitRow(table.getRowList().get(currentRowIndex), 0, null, parts);
                }
            });
            tableModifier.modify(document);

            return RtfUtil.toByteArray(document);
        } finally {
            dataSource.setTotalSize(dsTotalSize);
            dataSource.setCountRow(dsCountRow);
            this.refreshStudentContractsDataSource(model, false);
        }
    }

    @SuppressWarnings({"unchecked"})
    private String getColumnContent(final Model.Row listRow, final AbstractColumn listColumn)
    {
        final Object content = listColumn.getContent(listRow);
        if (content instanceof String) {
            return (String) content;
        }
        if (content instanceof StringBuilder) {
            return content.toString();
        }
        return "";
    }

    @SuppressWarnings("unchecked")
    private List<AbstractColumn> getColumns(final Model model, final Long principalId)
    {
        final List<AbstractColumn> columns = new ArrayList<>();
        final IDataSettingsManager dataSettingsManager = (IDataSettingsManager) ApplicationRuntime.getBean("dataSettingsManager");
        final IDataSettings dataSettings = (null == dataSettingsManager ? null : dataSettingsManager.getSettings(String.valueOf(principalId), model.getListSettingsKey() + "..columns"));
        final DynamicListDataSource<Model.Row> ds = model.getStudentContractsDataSource();
        for (final AbstractColumn c : ds.getColumns())
        {
            if ((null == dataSettings) || c.isVisible(dataSettings))
            {
                if (c instanceof FormatterColumn) {
                    columns.add((FormatterColumn) c);
                } else if ((c instanceof ToggleColumn) || (c instanceof BooleanColumn)) {
                    columns.add(new SimpleColumn(c.getCaption(), c.getKey(), YesNoFormatter.INSTANCE));
                } else if (c instanceof BlockColumn) {
                    columns.add(c);
                }
            }
        }
        return columns;
    }

    protected Object fio(final String lastName, final String firstName, final String middleName)
    {
        final StringBuilder str = new StringBuilder().append(lastName).append(" ");
        if (StringUtils.isNotEmpty(firstName)) {
            str.append(firstName.substring(0, 1).toUpperCase()).append(".");
        }
        if (StringUtils.isNotEmpty(middleName)) {
            str.append(middleName.substring(0, 1).toUpperCase()).append(".");
        }
        return str.toString();
    }

    private void orderAgreementProperties(final MQBuilder builder, final String agreementAlias)
    {
        builder.addJoin(agreementAlias, UniscEduAgreementBaseGen.L_CONFIG, "config");
        builder.addJoin("config", UniscEduOrgUnitGen.L_EDUCATION_ORG_UNIT, "edu");

        builder.addOrder("edu", EducationOrgUnitGen.L_EDUCATION_LEVEL_HIGH_SCHOOL + ".code");
        builder.addOrder("edu", "id");
        builder.addOrder(agreementAlias, UniscEduAgreementBaseGen.P_FORMING_DATE);
        builder.addOrder(agreementAlias, "id");
    }

    protected MQBuilder getStudentBuilder(final M model)
    {
        final MQBuilder studentBuilder = new MQBuilder(StudentGen.ENTITY_CLASS, "student", new String[]{"id"});

        // условия фильтров
        {
            final IDataSettings settings = model.getSettings();
            final String lastName = (String) settings.get("lastNameFilter");
            final String firstName = (String) settings.get("firstNameFilter");

            if (StringUtils.isNotEmpty(lastName)) {
                studentBuilder.add(MQExpression.like("student", Student.L_PERSON + "." + PersonGen.L_IDENTITY_CARD + "." + IdentityCardGen.P_LAST_NAME, CoreStringUtils.escapeLike(lastName)));
            }
            if (StringUtils.isNotEmpty(firstName)) {
                studentBuilder.add(MQExpression.like("student", Student.L_PERSON + "." + PersonGen.L_IDENTITY_CARD + "." + IdentityCardGen.P_FIRST_NAME, CoreStringUtils.escapeLike(firstName)));
            }
            if (model.getStatusActive() != null) {
                studentBuilder.add(MQExpression.eq("student", Student.L_STATUS + "." + StudentStatus.P_ACTIVE, model.getStatusActive()));
            }
            if (model.getStudentStatus() != null) {
                studentBuilder.add(MQExpression.eq("student", Student.L_STATUS, model.getStudentStatus()));
            }
            if (model.getStudentCategory() != null) {
                studentBuilder.add(MQExpression.eq("student", Student.L_STUDENT_CATEGORY, model.getStudentCategory()));
            }
            if ((model.getCourseList() != null) && !model.getCourseList().isEmpty()) {
                studentBuilder.add(MQExpression.in("student", Student.L_COURSE, model.getCourseList()));
            }
        }
        return studentBuilder;
    }

    protected MQBuilder getAgreementBuilder(final M model)
    {
        final MQBuilder agreementBuilder = new MQBuilder(UniscEduAgreementBaseGen.ENTITY_CLASS, "agreement", new String[]{"id"});

        // условия фильтров
        {
            final IDataSettings settings = model.getSettings();

            agreementBuilder.addJoin("agreement", UniscEduAgreementBaseGen.L_CONFIG, "config");
            agreementBuilder.addJoin("config", UniscEduOrgUnitGen.L_EDUCATION_ORG_UNIT, "edu");

            Date changeDate = (Date) model.getSettings().get("changeDate");
            if (null != changeDate)
                agreementBuilder.add(UniMQExpression.betweenDate("agreement", UniscEduAgreementBaseGen.P_LAST_MODIFICATION_DATE, changeDate, new Date()));
            if (model.getEduYear() != null) {
                agreementBuilder.add(MQExpression.eq("config", UniscEduOrgUnitGen.L_EDUCATION_YEAR, model.getEduYear()));
            }
            final String agreementNumber = (String) settings.get("agreementNumberFilter");
            if (StringUtils.isNotEmpty(agreementNumber)) {
                agreementBuilder.add(MQExpression.eq("agreement", UniscEduAgreementBaseGen.P_NUMBER, agreementNumber));
            }
            if (model.getCustomerType() != null) {
                agreementBuilder.add(MQExpression.eq("agreement", UniscEduAgreementBaseGen.L_CUSTOMER_TYPE, model.getCustomerType()));
            }
            if (null != model.getCipher()) {
                agreementBuilder.add(MQExpression.eq("e", UniscEduAgreement2Student.L_AGREEMENT + "." + UniscEduAgreementBaseGen.L_CONFIG + ".id", model.getCipher().getId()));
            } else
            {
                if ((null != model.getFormativeOrgUnitList()) && !model.getFormativeOrgUnitList().isEmpty()) {
                    agreementBuilder.add(MQExpression.in("edu", EducationOrgUnitGen.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnitList()));
                }
                if ((null != model.getTerritorialOrgUnitList()) && !model.getTerritorialOrgUnitList().isEmpty()) {
                    agreementBuilder.add(MQExpression.in("edu", EducationOrgUnitGen.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnitList()));
                }
                if (null != model.getEducationLevelsHighSchool()) {
                    agreementBuilder.add(MQExpression.eq("edu", EducationOrgUnitGen.L_EDUCATION_LEVEL_HIGH_SCHOOL + ".id", model.getEducationLevelsHighSchool().getId()));
                }
                if ((null != model.getDevelopFormList()) && !model.getDevelopFormList().isEmpty()) {
                    agreementBuilder.add(MQExpression.in("edu", EducationOrgUnitGen.L_DEVELOP_FORM, model.getDevelopFormList()));
                }
                if ((null != model.getDevelopConditionList()) && !model.getDevelopConditionList().isEmpty()) {
                    agreementBuilder.add(MQExpression.in("edu", EducationOrgUnitGen.L_DEVELOP_CONDITION, model.getDevelopConditionList()));
                }
                if ((null != model.getDevelopTechList()) && !model.getDevelopTechList().isEmpty()) {
                    agreementBuilder.add(MQExpression.in("edu", EducationOrgUnitGen.L_DEVELOP_TECH, model.getDevelopTechList()));
                }
                if ((null != model.getDevelopPeriodList()) && !model.getDevelopPeriodList().isEmpty()) {
                    agreementBuilder.add(MQExpression.in("edu", EducationOrgUnitGen.L_DEVELOP_PERIOD, model.getDevelopPeriodList()));
                }
            }
            if (model.getChecked() != null) {
                agreementBuilder.add(MQExpression.eq("agreement", UniscEduAgreementBaseGen.P_CHECKED_BY_SIGNATURE, model.getChecked()));
            }
        }

        return agreementBuilder;
    }
}
