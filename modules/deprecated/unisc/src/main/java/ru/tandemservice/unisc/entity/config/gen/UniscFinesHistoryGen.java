package ru.tandemservice.unisc.entity.config.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unisc.entity.config.UniscFinesHistory;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Размер начисления штрафных пеней
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniscFinesHistoryGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisc.entity.config.UniscFinesHistory";
    public static final String ENTITY_NAME = "uniscFinesHistory";
    public static final int VERSION_HASH = -1328010519;
    private static IEntityMeta ENTITY_META;

    public static final String P_DATE = "date";
    public static final String P_TAX = "tax";
    public static final String P_DATE_AS_STRING = "dateAsString";
    public static final String P_TAX_AS_STRING = "taxAsString";

    private Date _date;     // дата изменения
    private double _tax;     // процент

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return дата изменения. Свойство не может быть null.
     */
    @NotNull
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date дата изменения. Свойство не может быть null.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return процент. Свойство не может быть null.
     */
    @NotNull
    public double getTax()
    {
        return _tax;
    }

    /**
     * @param tax процент. Свойство не может быть null.
     */
    public void setTax(double tax)
    {
        dirty(_tax, tax);
        _tax = tax;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UniscFinesHistoryGen)
        {
            setDate(((UniscFinesHistory)another).getDate());
            setTax(((UniscFinesHistory)another).getTax());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniscFinesHistoryGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniscFinesHistory.class;
        }

        public T newInstance()
        {
            return (T) new UniscFinesHistory();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "date":
                    return obj.getDate();
                case "tax":
                    return obj.getTax();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "tax":
                    obj.setTax((Double) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "date":
                        return true;
                case "tax":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "date":
                    return true;
                case "tax":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "date":
                    return Date.class;
                case "tax":
                    return Double.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniscFinesHistory> _dslPath = new Path<UniscFinesHistory>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniscFinesHistory");
    }
            

    /**
     * @return дата изменения. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.config.UniscFinesHistory#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return процент. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.config.UniscFinesHistory#getTax()
     */
    public static PropertyPath<Double> tax()
    {
        return _dslPath.tax();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisc.entity.config.UniscFinesHistory#getDateAsString()
     */
    public static SupportedPropertyPath<String> dateAsString()
    {
        return _dslPath.dateAsString();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisc.entity.config.UniscFinesHistory#getTaxAsString()
     */
    public static SupportedPropertyPath<String> taxAsString()
    {
        return _dslPath.taxAsString();
    }

    public static class Path<E extends UniscFinesHistory> extends EntityPath<E>
    {
        private PropertyPath<Date> _date;
        private PropertyPath<Double> _tax;
        private SupportedPropertyPath<String> _dateAsString;
        private SupportedPropertyPath<String> _taxAsString;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return дата изменения. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.config.UniscFinesHistory#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(UniscFinesHistoryGen.P_DATE, this);
            return _date;
        }

    /**
     * @return процент. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.config.UniscFinesHistory#getTax()
     */
        public PropertyPath<Double> tax()
        {
            if(_tax == null )
                _tax = new PropertyPath<Double>(UniscFinesHistoryGen.P_TAX, this);
            return _tax;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisc.entity.config.UniscFinesHistory#getDateAsString()
     */
        public SupportedPropertyPath<String> dateAsString()
        {
            if(_dateAsString == null )
                _dateAsString = new SupportedPropertyPath<String>(UniscFinesHistoryGen.P_DATE_AS_STRING, this);
            return _dateAsString;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisc.entity.config.UniscFinesHistory#getTaxAsString()
     */
        public SupportedPropertyPath<String> taxAsString()
        {
            if(_taxAsString == null )
                _taxAsString = new SupportedPropertyPath<String>(UniscFinesHistoryGen.P_TAX_AS_STRING, this);
            return _taxAsString;
        }

        public Class getEntityClass()
        {
            return UniscFinesHistory.class;
        }

        public String getEntityName()
        {
            return "uniscFinesHistory";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getDateAsString();

    public abstract String getTaxAsString();
}
