package ru.tandemservice.unisc.component.settings.CostSettings.Copy;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.hibernate.Session;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.tapsupport.component.selection.StaticSelectModel;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.ui.SelectModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.basic.gen.EducationYearGen;
import ru.tandemservice.unisc.dao.config.IUniscEduOrgUnitConfigDAO;
import ru.tandemservice.unisc.entity.catalog.UniscPayPeriod;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlan;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlanRow;
import ru.tandemservice.unisc.entity.config.gen.UniscEduOrgUnitGen;
import ru.tandemservice.unisc.entity.config.gen.UniscEduOrgUnitPayPlanGen;
import ru.tandemservice.unisc.entity.config.gen.UniscEduOrgUnitPayPlanRowGen;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author vdanilov
 */
@SuppressWarnings("deprecation")
public class DAO extends UniBaseDao implements IDAO
{

    @Override
    public void save(final Model model)
    {
        final EducationYear year = model.getEducationYearModel().getValue();
        if (null == year)
        {
            throw new IllegalArgumentException();
        }

        final int diffY = year.getIntValue() - model.getSource().getIntValue();
        if (diffY <= 0)
        {
            throw new IllegalArgumentException();
        }


        final Session session = getSession();
        final IUniscEduOrgUnitConfigDAO dao = IUniscEduOrgUnitConfigDAO.INSTANCE.get();

        final Map<UniscEduOrgUnit, Map<UniscPayPeriod, UniscEduOrgUnitPayPlan>> cfg2pay2plan = new HashMap<>();
        /* берем все, что уже есть */
        {
            final MQBuilder builder = new MQBuilder(UniscEduOrgUnitPayPlanGen.ENTITY_CLASS, "e");
            builder.add(MQExpression.eq("e", UniscEduOrgUnitPayPlanGen.L_UNISC_EDU_ORG_UNIT + "." + UniscEduOrgUnitGen.L_EDUCATION_YEAR + ".id", year.getId()));
            final List<UniscEduOrgUnitPayPlan> plans = builder.getResultList(session);
            for (final UniscEduOrgUnitPayPlan plan : plans)
            {
                Map<UniscPayPeriod, UniscEduOrgUnitPayPlan> pay2plan = cfg2pay2plan.get(plan.getUniscEduOrgUnit());
                if (null == pay2plan)
                {
                    cfg2pay2plan.put(plan.getUniscEduOrgUnit(), pay2plan = new HashMap<>(4));
                }
                pay2plan.put(plan.getUniscPayFreq(), plan);
            }
        }

        final Map<UniscEduOrgUnit, UniscEduOrgUnit> ou_old2new = new HashMap<>();
        /* копируем направления */
        {
            final MQBuilder builder = new MQBuilder(UniscEduOrgUnitGen.ENTITY_CLASS, "eduou");
            builder.setReadOnly(true);
            builder.add(MQExpression.eq("eduou", UniscEduOrgUnitGen.L_EDUCATION_YEAR, model.getSource()));
            final List<UniscEduOrgUnit> list = builder.getResultList(session);
            for (final UniscEduOrgUnit edu : list)
            {
                final UniscEduOrgUnit uniscEduOrgUnit = dao.getUniscEduOrgUnit(year, edu.getEducationOrgUnit(), edu.getCipher());
                uniscEduOrgUnit.setConditions(edu.getConditions());
                uniscEduOrgUnit.setDescription(edu.getDescription());
                uniscEduOrgUnit.setDefaultDeadlineDate(null == edu.getDefaultDeadlineDate() ? null : DateUtils.addYears(edu.getDefaultDeadlineDate(), diffY));
                uniscEduOrgUnit.setDefaultFormingDate(null == edu.getDefaultFormingDate() ? null : DateUtils.addYears(edu.getDefaultFormingDate(), diffY));
                ou_old2new.put(edu, uniscEduOrgUnit);
            }
        }

        /* копируем способы оплаты */
        {
            final MQBuilder builder = new MQBuilder(UniscEduOrgUnitPayPlanGen.ENTITY_CLASS, "e");
            builder.setReadOnly(true);
            builder.addOrder("e", UniscEduOrgUnitPayPlanGen.L_UNISC_PAY_FREQ + "." + ICatalogItem.CATALOG_ITEM_CODE);
            builder.add(MQExpression.eq("e", UniscEduOrgUnitPayPlanGen.L_UNISC_EDU_ORG_UNIT + "." + UniscEduOrgUnitGen.L_EDUCATION_YEAR, model.getSource()));
            final List<UniscEduOrgUnitPayPlan> list = builder.getResultList(session);
            for (final UniscEduOrgUnitPayPlan plan : list)
            {
                final UniscEduOrgUnit uniscEduOrgUnit = ou_old2new.get(plan.getUniscEduOrgUnit());
                Map<UniscPayPeriod, UniscEduOrgUnitPayPlan> pm = cfg2pay2plan.get(uniscEduOrgUnit);
                if (null == pm)
                {
                    cfg2pay2plan.put(uniscEduOrgUnit, pm = new HashMap<>());
                }

                /* убираем все нах */
                {
                    final UniscEduOrgUnitPayPlan p = pm.get(plan.getUniscPayFreq());
                    if (null != p)
                    {
                        session.delete(p);
                    }
                }

                final UniscEduOrgUnitPayPlan value = new UniscEduOrgUnitPayPlan();
                value.update(plan);
                value.setUniscEduOrgUnit(uniscEduOrgUnit);
                session.save(value);
                pm.put(value.getUniscPayFreq(), value);
            }
        }

        /* копируем оплату */
        {
            final MQBuilder builder = new MQBuilder(UniscEduOrgUnitPayPlanRowGen.ENTITY_CLASS, "e");
            builder.add(MQExpression.eq("e", UniscEduOrgUnitPayPlanRowGen.L_PLAN + "." + UniscEduOrgUnitPayPlanGen.L_UNISC_EDU_ORG_UNIT + "." + UniscEduOrgUnitGen.L_EDUCATION_YEAR, model.getSource()));
            final List<UniscEduOrgUnitPayPlanRow> rows = builder.getResultList(getSession());
            for (final UniscEduOrgUnitPayPlanRow row : rows)
            {
                final UniscEduOrgUnitPayPlanRow r = new UniscEduOrgUnitPayPlanRow();
                r.update(row);

                final UniscEduOrgUnit key = ou_old2new.get(row.getPlan().getUniscEduOrgUnit());
                r.setPlan(cfg2pay2plan.get(key).get(row.getPlan().getUniscPayFreq()));
                r.setDate(DateUtils.addYears(r.getDate(), diffY));

                session.save(r);
            }
        }
    }

    @Override
    public void prepare(final Model model)
    {
        model.setSource(get(EducationYear.class, model.getSource().getId()));
        this.prepareEducationYearModel(model);
    }

    protected void prepareEducationYearModel(final Model model)
    {
        final List<EducationYear> years = this.getList(EducationYear.class, EducationYearGen.P_INT_VALUE);

        final int year = model.getSource().getIntValue();
        CollectionUtils.filter(years, object -> ((EducationYear) object).getIntValue() > year);

        final SelectModel<EducationYear> educationYearModel = model.getEducationYearModel();

        if (null == educationYearModel.getSource())
        {
            educationYearModel.setSource(new StaticSelectModel("id", "title", years));
        }

        if (null != educationYearModel.getValue())
        {
            educationYearModel.setValue(reassociateEntity(getSession(), educationYearModel.getValue()));
        } else
        {
            final int v = model.getSource().getIntValue();
            for (final EducationYear y : years)
            {
                if (y.getIntValue() > v)
                {
                    educationYearModel.setValue(y);
                    break;
                }
            }
        }

    }
}
