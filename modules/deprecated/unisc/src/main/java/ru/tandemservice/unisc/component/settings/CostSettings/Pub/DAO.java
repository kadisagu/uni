package ru.tandemservice.unisc.component.settings.CostSettings.Pub;

import org.apache.commons.lang.mutable.MutableDouble;
import org.hibernate.Session;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unisc.UniscDefines;
import ru.tandemservice.unisc.component.settings.CostSettings.Pub.Model.Block;
import ru.tandemservice.unisc.dao.config.UniscEduOrgUnitConfigDAO;
import ru.tandemservice.unisc.dao.io.IUniscIODao;
import ru.tandemservice.unisc.entity.catalog.UniscPayPeriod;
import ru.tandemservice.unisc.entity.catalog.UniscPayPeriod.Freq;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlan;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlanRow;
import ru.tandemservice.unisc.entity.config.gen.UniscEduOrgUnitPayPlanGen;
import ru.tandemservice.unisc.entity.config.gen.UniscEduOrgUnitPayPlanRowGen;

import java.util.*;

public class DAO extends UniDao<Model> implements IDAO {

	@Override
	public void prepare(final Model model) {
		final IEntity entity = this.get(model.getId());
		if (entity instanceof UniscEduOrgUnit) {
			model.setEduOrgUnit((UniscEduOrgUnit) entity);
		} else {
			throw new IllegalArgumentException(entity.getClass().getName());
		}

		/* prepare block list */ {
			final Map<UniscPayPeriod, Block> blockMap = this.getBlockMap(model);
			final List<Block> blockList = new ArrayList<>(blockMap.values());
			Collections.sort(blockList, (o1, o2) -> CommonCatalogUtil.CATALOG_CODE_COMPARATOR.compare(o1.getPlan().getUniscPayFreq(), o2.getPlan().getUniscPayFreq()));
			model.setBlockList(blockList);
		}
	}

	private Map<UniscPayPeriod, Block> getBlockMap(final Model model) {
		final Map<UniscPayPeriod, Block> blockMap = new HashMap<>();

		/* fill block by plans */ {
			final MQBuilder builder = new MQBuilder(UniscEduOrgUnitPayPlanGen.ENTITY_CLASS, "e");
			builder.add(MQExpression.eq("e", UniscEduOrgUnitPayPlanGen.L_UNISC_EDU_ORG_UNIT+".id", model.getEduOrgUnit().getId()));
			final List<UniscEduOrgUnitPayPlan> plans = builder.getResultList(this.getSession());
			for (final UniscEduOrgUnitPayPlan plan: plans) {
				blockMap.put(plan.getUniscPayFreq(), new Block(plan));
			}
		}

		/* fill block by rows */ {
			final MQBuilder builder = new MQBuilder(UniscEduOrgUnitPayPlanRowGen.ENTITY_CLASS, "e");
			builder.add(MQExpression.eq("e", UniscEduOrgUnitPayPlanRowGen.L_PLAN+"."+UniscEduOrgUnitPayPlanGen.L_UNISC_EDU_ORG_UNIT+".id", model.getEduOrgUnit().getId()));
			builder.addOrder("e", UniscEduOrgUnitPayPlanRowGen.P_STAGE);
			builder.addOrder("e", UniscEduOrgUnitPayPlanRowGen.P_DATE);
			final List<UniscEduOrgUnitPayPlanRow> planRows = builder.getResultList(this.getSession());
			for (final UniscEduOrgUnitPayPlanRow planRow: planRows) {
				blockMap.get(planRow.getPlan().getUniscPayFreq()).getRows().add(planRow);
			}
		}

		/* fill default valus */ {
			for (final UniscPayPeriod period: this.getCatalogItemList(UniscPayPeriod.class)) {
				if (null == blockMap.get(period)) {
					blockMap.put(period, new Block(model.getEduOrgUnit(), period));
				}
			}
		}
		return blockMap;
	}

	@Override
	public void doRecalculate(final Model model) {
		final Map<Integer, MutableDouble> course2cost = SafeMap.get(MutableDouble.class);
		final Session session = getSession();

		final UniscPayPeriod yearFreq = getCatalogItem(UniscPayPeriod.class, UniscDefines.PAY_PERIOD_YEAR);
		/* collect */ {
			final UniscEduOrgUnitPayPlan yearPlan = IUniscIODao.INSTANCE.get().doGetPayPlan(model.getEduOrgUnit(), yearFreq);
			final MQBuilder builder = new MQBuilder(UniscEduOrgUnitPayPlanRowGen.ENTITY_CLASS, "e");
			builder.add(MQExpression.eq("e", UniscEduOrgUnitPayPlanRowGen.L_PLAN, yearPlan));
			builder.addOrder("e", UniscEduOrgUnitPayPlanRowGen.P_STAGE);
			builder.addOrder("e", UniscEduOrgUnitPayPlanRowGen.P_DATE);
			for (final UniscEduOrgUnitPayPlanRow row: builder.<UniscEduOrgUnitPayPlanRow>getResultList(session)) {
				course2cost.get(yearFreq.getCourseNumber(model.getEduOrgUnit(), row.getStage())).add(row.getCostAsDouble());
				course2cost.get(0).add(row.getCostAsDouble());
			}
		}

		for (final UniscPayPeriod freq: this.getCatalogItemList(UniscPayPeriod.class)) {
			if (!yearFreq.equals(freq)) {
				final UniscEduOrgUnitPayPlan plan = IUniscIODao.INSTANCE.get().doGetPayPlan(model.getEduOrgUnit(), freq);
				final MQBuilder builder = new MQBuilder(UniscEduOrgUnitPayPlanRowGen.ENTITY_CLASS, "e");
				builder.add(MQExpression.eq("e", UniscEduOrgUnitPayPlanRowGen.L_PLAN, plan));
				builder.addOrder("e", UniscEduOrgUnitPayPlanRowGen.P_STAGE);
				builder.addOrder("e", UniscEduOrgUnitPayPlanRowGen.P_DATE);

				final Map<Integer, List<UniscEduOrgUnitPayPlanRow>> course2rows = SafeMap.get(key -> new ArrayList<>());

				for (final UniscEduOrgUnitPayPlanRow row: builder.<UniscEduOrgUnitPayPlanRow>getResultList(session)) {
					course2rows.get(freq.getCourseNumber(model.getEduOrgUnit(), row.getStage())).add(row);
				}
				for (final Map.Entry<Integer, List<UniscEduOrgUnitPayPlanRow>> e: course2rows.entrySet()) {
					final double cost = course2cost.get((Freq.TOTAL.equals(freq.freq()) ? 0 : e.getKey())).doubleValue();
					long lc = UniscEduOrgUnitConfigDAO.unwrap(cost);

					final double dd = ((int)(100.0*cost / e.getValue().size()))*0.01; // с точностью до копеек (бить копейки между строками не надо)
					for (final UniscEduOrgUnitPayPlanRow r : e.getValue()) {
						r.setCostAsDouble(dd);
						lc -= r.getCost();
						session.saveOrUpdate(r);
					}

					/* patch */ {
						if (lc != 0) {
							final UniscEduOrgUnitPayPlanRow row = e.getValue().iterator().next();
							row.setCost(row.getCost() + lc);
							session.saveOrUpdate(row);
						}
					}
				}
			}
		}
	}

}
