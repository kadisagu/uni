package ru.tandemservice.unisc.component.report.AgreementIncomeReport;

import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayFactRow;

import java.util.*;

/**
 * @author oleyba
 * @since 21.08.2009
 */
public abstract class AgreementIncomeReportRow
{
    private Map<Integer, Long> sumMap = new HashMap<>();

    public abstract List<String> getTitles();

    public void addPayment(UniscEduAgreementPayFactRow row)
    {
        int month = CommonBaseDateUtil.getMonthStartingWithOne(row.getDate());
        Long value = sumMap.get(month);
        if (null == value) value = 0L;
        sumMap.put(month, value + row.getJCost() + row.getNCost());
    }

    public String getSum(Integer month)
    {
        if (month == null)
            return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(0.001d * getTotal());
        Long value = sumMap.get(month);
        if (null == value)
            return "";
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(0.001d * value);
    }

    public Long getTotal()
    {
        Long value = 0L;
        for (Long month : sumMap.values()) value = value + month;
        return value;
    }

    public static class RowGroup
    {
        private OrgUnit formative;
        private OrgUnit territorial;
        private Set<AgreementIncomeReportRow> reportRows = new HashSet<>();

        public RowGroup(EducationOrgUnit eduOu)
        {
            formative = eduOu.getFormativeOrgUnit();
            territorial = eduOu.getTerritorialOrgUnit();
        }

        public void addRow(AgreementIncomeReportRow row)
        {
            reportRows.add(row);
        }

        public static final Comparator<RowGroup> comparator = (o1, o2) -> {
            int result = o1.formative.getTitle().compareToIgnoreCase(o2.formative.getTitle());
            if (result == 0)
            {
                if (o1.territorial == null) return -1;
                if (o2.territorial == null) return 1;
                result = o1.territorial.getTitle().compareToIgnoreCase(o2.territorial.getTitle());
            }
            return result;
        };

        private String getTitle()
        {
            String title = "Подразделение: " + (formative.getNominativeCaseTitle() == null ? formative.getTitle() : formative.getNominativeCaseTitle());
            if (null != territorial)
                title = title + " (" +  (territorial.getNominativeCaseTitle() == null ? territorial.getTitle() : territorial.getNominativeCaseTitle()) + ")";
            return title;
        }

        public long appendRows(List<String[]> reportRowList, List<Integer> months, Comparator<AgreementIncomeReportRow> rowComparator, int titlesLength)
        {
            reportRowList.add(new String[]{getTitle()});
            ArrayList<AgreementIncomeReportRow> rowList = new ArrayList<>(reportRows);
            Collections.sort(rowList, rowComparator);
            long total = 0L;
            for (AgreementIncomeReportRow row : rowList)
            {
                List<String> result = new ArrayList<>();
                result.addAll(row.getTitles());
                for (int month : months)
                    result.add(row.getSum(month));
                result.add(row.getSum(null));
                reportRowList.add(result.toArray(new String[result.size()]));
                total = total + row.getTotal();
            }
            List<String> result = new ArrayList<>();
            result.add("Итого");
            for (int month = 0; month < months.size() + titlesLength - 1; month++)
                result.add("");
            result.add(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(0.001d * total));
            reportRowList.add(result.toArray(new String[result.size()]));
            return total;
        }
    }
}