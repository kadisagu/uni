/* $Id: DAO.java 8754 2009-06-29 09:27:43Z ekachanova $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisc.component.agreement.AgreementPrintVersionsTab.AddEdit;

import org.apache.commons.io.IOUtils;
import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.sec.IPrincipal;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfWriter;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.unisc.component.agreement.AgreementRelationPrint.UniscEduAgreementPrintDAOBase;
import ru.tandemservice.unisc.dao.agreement.IUniscEduAgreementDAO;
import ru.tandemservice.unisc.dao.print.IUniscPrintDAO;
import ru.tandemservice.unisc.entity.agreements.IUniscEduAgreement2PersonRole;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPrintVersion;

import java.io.ByteArrayOutputStream;
import java.util.Date;

/**
 * @author vdanilov
 */
public class DAO extends UniscEduAgreementPrintDAOBase implements IDAO
{
    @Override
    @SuppressWarnings("unchecked")
    public void prepare(final Model model)
    {
        final IEntity e = get(model.getId());
        if (e instanceof UniscEduAgreementPrintVersion)
        {
            model.setVersion((UniscEduAgreementPrintVersion)e);
        }
        else if (e instanceof UniscEduAgreementBase)
        {
            model.setVersion(new UniscEduAgreementPrintVersion());
            model.getVersion().setOwner((UniscEduAgreementBase) e);
            model.getVersion().setTimestamp(new Date());
            model.getVersion().setTitle("Версия от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getVersion().getTimestamp()));
        }
        else if (e instanceof IUniscEduAgreement2PersonRole)
        {
            model.setVersion(new UniscEduAgreementPrintVersion());
            model.getVersion().setOwner(((IUniscEduAgreement2PersonRole) e).getAgreement());
            model.getVersion().setTimestamp(new Date());
            model.getVersion().setTitle("Версия от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getVersion().getTimestamp()));
        }
        else
        {
            if (null == e) {
                throw new NullPointerException();
            }
            throw new IllegalStateException(String.valueOf(e.getClass()));
        }

    }

    @Override
    public void save(final Model model)
    {
        final UniscEduAgreementPrintVersion v = model.getVersion();

        final IPrincipal principal = UserContext.getInstance().getPrincipal();
        v.setUser(null == principal ? null : principal.getTitle());

        final DatabaseFile dbFile = new DatabaseFile();
        final IUploadFile uploadFile = model.getUploadFile();
        if ((uploadFile == null) || (uploadFile.getSize() <= 0)) {
            try {
                final UniscEduAgreementBase agreement = v.getOwner();
                final IUniscEduAgreement2PersonRole<PersonRole> rel = IUniscEduAgreementDAO.INSTANCE.get().getRelation(agreement.getAgreement());
                final RtfDocument document = IUniscPrintDAO.INSTANCE.get().getDocumentTemplate(agreement);
                IUniscPrintDAO.INSTANCE.get().getRtfInjectorModifier(agreement, (null == rel ? null : rel.getTarget().getPerson())).modify(document);
                this.applyBarcode(document, IUniscPrintDAO.INSTANCE.get().getBarcodeString(agreement));

                final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                RtfWriter.write(document, outputStream);
                outputStream.close();
                dbFile.setContent(outputStream.toByteArray());
                v.setFileName("Contract-"+DateFormatter.DEFAULT_DATE_FORMATTER.format(v.getTimestamp())+".rtf");
                v.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
                v.setUploaded(false);

            } catch (final Throwable e) {
                throw new RuntimeException(e.getMessage(), e);
            }

        } else {
            if (uploadFile.getSize() > 512 * 1024) {
                throw new ApplicationException("Размер загружаемого файла не должен превышать 512Kb.");
            }

            try {
                final byte[] content = IOUtils.toByteArray(uploadFile.getStream());
                if ((content != null) && (content.length > 0)) {
                    dbFile.setContent(content);
                } else {
                    dbFile.setContent(new byte[] {});
                }
                v.setFileName(uploadFile.getFileName());
                v.setContentType(CommonBaseUtil.getContentType(uploadFile));
                v.setUploaded(true);
            } catch (final Throwable e) {
                throw new RuntimeException(e.getMessage(), e);
            }
        }
        save(dbFile);
        v.setFile(dbFile);
        save(v);
    }

}
