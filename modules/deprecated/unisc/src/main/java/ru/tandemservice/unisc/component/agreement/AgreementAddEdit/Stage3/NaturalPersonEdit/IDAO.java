package ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage3.NaturalPersonEdit;

import ru.tandemservice.uni.dao.IPrepareable;

/**
 * @author vdanilov
 */
@SuppressWarnings("deprecation")
public interface IDAO extends IPrepareable<Model> {
    void save(Model model);
}
