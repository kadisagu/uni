/* $Id: Model.java 8355 2009-06-08 08:23:06Z vdanilov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unisc.component.list.StudentAgreementListBase;

import java.util.Arrays;
import java.util.List;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisc.entity.agreements.UniscEduAdditAgreement;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;
import ru.tandemservice.unisc.entity.agreements.UniscEduMainAgreement;
import ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student;
import ru.tandemservice.unisc.entity.catalog.UniscCustomerType;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;

/**
 * @author vdanilov
 */
@SuppressWarnings("unchecked")
public class Model
{
    public static final long TRUE_OPTION_ID = 0L;
    public static final long FALSE_OPTION_ID = 1L;

    @SuppressWarnings("serial")
    public static class Row extends IdentifiableWrapper<UniscEduAgreementBase> {
        private final UniscEduAgreement2Student relation;
        public UniscEduAgreement2Student getRelation() { return this.relation; }
        public UniscEduMainAgreement getMainAgreement() { return relation.getAgreement(); }
        public Student getStudent() { return relation.getStudent(); }

        private final UniscEduAgreementBase agreement;
        public UniscEduAgreementBase getAgreement() { return this.agreement; }
        public String getDocumentNumber() {
            final UniscEduAgreementBase a = getAgreement();
            return (
            (a instanceof UniscEduAdditAgreement ? (((UniscEduAdditAgreement)a).getAgreement().getNumber() + ".") : "") +
            a.getNumber() +
            ("\n(" + a.getConfig().getEducationYear().getTitle()+")")
            );
        }


        protected Row(final UniscEduAgreement2Student relation, final UniscEduAgreementBase agreement) {
            super(agreement.getId(), "");
            this.relation = relation;
            this.agreement = agreement;
        }

        protected Row(final UniscEduAgreement2Student relation) {
            this(relation, relation.getAgreement());
        }
    }

    private DynamicListDataSource<Row> studentContractsDataSource;
    public DynamicListDataSource<Row> getStudentContractsDataSource() { return this.studentContractsDataSource; }
    public void setStudentContractsDataSource(final DynamicListDataSource<Row> studentContractsDataSource) { this.studentContractsDataSource = studentContractsDataSource; }

    public String getCurrentColumnSplittedRawValue() {
        final Object value = studentContractsDataSource.getColumnValue();
        if (null == value) { return ""; }
        return NewLineFormatter.NOBR_IN_LINES.format(String.valueOf(value));
    }

    private IDataSettings _settings;
    public void setSettings(final IDataSettings settings) { _settings = settings; }
    public IDataSettings getSettings() { return _settings; }

    private ISelectModel eduYearList;
    public ISelectModel getEduYearList() { return eduYearList; }
    public void setEduYearList(ISelectModel eduYearList) { this.eduYearList = eduYearList; }
    public EducationYear getEduYear() { return (EducationYear) this.getSettings().get("eduYear"); }
    public EducationYear getEduYearWithoutAdditAgreement() { return (EducationYear) this.getSettings().get("eduYearWithoutAdditAgreement"); }

    private List<IdentifiableWrapper> _contractActiveList = Arrays.asList(new IdentifiableWrapper(TRUE_OPTION_ID, "Действующий"), new IdentifiableWrapper(FALSE_OPTION_ID, "Недействующий"));
    public List<IdentifiableWrapper> getContractActiveList() { return _contractActiveList; }
    public Boolean getContractActive() {
        final IdentifiableWrapper wrapper = (IdentifiableWrapper) getSettings().get("contractActive");
        if (wrapper == null) return null;
        return TRUE_OPTION_ID == wrapper.getId();
    }

    public boolean isShowAdditional() { return getSettings().get("showAdditional") == null ? true : (Boolean) getSettings().get("showAdditional"); }
    public void setShowAdditional(boolean showAdditional) { getSettings().set("showAdditional", showAdditional); }

    private List<IdentifiableWrapper> _statusActiveList = Arrays.asList(new IdentifiableWrapper(TRUE_OPTION_ID, "Активные студенты"), new IdentifiableWrapper(FALSE_OPTION_ID, "Неактивные студенты"));
    public List<IdentifiableWrapper> getStatusActiveList() { return _statusActiveList; }
    public Boolean getStatusActive() {
        final IdentifiableWrapper wrapper = (IdentifiableWrapper) getSettings().get("statusActive");
        if (wrapper == null) return null;
        return TRUE_OPTION_ID == wrapper.getId();
    }

    private List<StudentStatus> _studentStatusList;
    public List<StudentStatus> getStudentStatusList() { return _studentStatusList; }
    public void setStudentStatusList(List<StudentStatus> studentStatusList) { _studentStatusList = studentStatusList; }
    public StudentStatus getStudentStatus() { return (StudentStatus) this.getSettings().get("studentStatus"); }

    private List<StudentCategory> _studentCategoryList;
    public List<StudentCategory> getStudentCategoryList() { return _studentCategoryList; }
    public void setStudentCategoryList(List<StudentCategory> studentCategoryList) { _studentCategoryList = studentCategoryList; }
    public StudentCategory getStudentCategory() { return (StudentCategory) this.getSettings().get("studentCategory"); }

    private ISelectModel courseListModel;
    public ISelectModel getCourseListModel() { return courseListModel; }
    public void setCourseListModel(ISelectModel courseListModel) { this.courseListModel = courseListModel; }
    public List<Course> getCourseList() { return (List<Course>) this.getSettings().get("courseList"); }

    private List<UniscCustomerType> _customerTypeList;
    public List<UniscCustomerType> getCustomerTypeList() { return _customerTypeList; }
    public void setCustomerTypeList(List<UniscCustomerType> customerTypeList) {  _customerTypeList = customerTypeList; }
    public UniscCustomerType getCustomerType() { return (UniscCustomerType) this.getSettings().get("customerType"); }

    private ISelectModel cipherModel;
    public ISelectModel getCipherModel() { return this.cipherModel; }
    public void setCipherModel(final ISelectModel cipherModel) { this.cipherModel = cipherModel; }
    public UniscEduOrgUnit getCipher() { return (UniscEduOrgUnit) this.getSettings().get("cipher"); }

    private ISelectModel _formativeOrgUnitModel;
    public ISelectModel getFormativeOrgUnitModel() { return this._formativeOrgUnitModel; }
    public void setFormativeOrgUnitModel(final ISelectModel formativeOrgUnitModel) { this._formativeOrgUnitModel = formativeOrgUnitModel; }
    public List<OrgUnit> getFormativeOrgUnitList() { return (List<OrgUnit>) this.getSettings().get("formativeOrgUnitList"); }

    private ISelectModel _territorialOrgUnitModel;
    public ISelectModel getTerritorialOrgUnitModel() { return this._territorialOrgUnitModel; }
    public void setTerritorialOrgUnitModel(final ISelectModel territorialOrgUnitModel) { this._territorialOrgUnitModel = territorialOrgUnitModel; }
    public List<OrgUnit> getTerritorialOrgUnitList() { return (List<OrgUnit>) this.getSettings().get("territorialOrgUnitList"); }

    private ISelectModel _educationLevelsHighSchoolModel;
    public ISelectModel getEducationLevelsHighSchoolModel() { return this._educationLevelsHighSchoolModel; }
    public void setEducationLevelsHighSchoolModel(final ISelectModel educationLevelsHighSchoolModel) { this._educationLevelsHighSchoolModel = educationLevelsHighSchoolModel; }
    public EducationLevelsHighSchool getEducationLevelsHighSchool() { return (EducationLevelsHighSchool) this.getSettings().get("eduLevelHighSchool"); }

    private ISelectModel _developFormModel;
    public ISelectModel getDevelopFormModel() { return this._developFormModel; }
    public void setDevelopFormModel(final ISelectModel developFormModel) { this._developFormModel = developFormModel; }
    public List<DevelopForm> getDevelopFormList() { return (List<DevelopForm>) this.getSettings().get("developFormList"); }

    private ISelectModel _developTechModel;
    public ISelectModel getDevelopTechModel() { return this._developTechModel; }
    public void setDevelopTechModel(final ISelectModel developTechModel) { this._developTechModel = developTechModel; }
    public List<DevelopTech> getDevelopTechList() { return (List<DevelopTech>) this.getSettings().get("developTechList"); }

    private ISelectModel _developConditionModel;
    public ISelectModel getDevelopConditionModel() { return this._developConditionModel; }
    public void setDevelopConditionModel(final ISelectModel developConditionModel) { this._developConditionModel = developConditionModel; }
    public List<DevelopCondition> getDevelopConditionList() { return (List<DevelopCondition>) this.getSettings().get("developConditionList"); }

    private ISelectModel _developPeriodModel;
    public ISelectModel getDevelopPeriodModel() { return this._developPeriodModel; }
    public void setDevelopPeriodModel(final ISelectModel developPeriodModel) { this._developPeriodModel = developPeriodModel; }
    public List<DevelopPeriod> getDevelopPeriodList() { return (List<DevelopPeriod>) this.getSettings().get("developPeriodList"); }

    private List<IdentifiableWrapper> _checkedList = Arrays.asList(new IdentifiableWrapper(TRUE_OPTION_ID, "Да"), new IdentifiableWrapper(FALSE_OPTION_ID, "Нет"));
    public List<IdentifiableWrapper> getCheckedList() { return _checkedList; }
    public Boolean getChecked() {
        final IdentifiableWrapper wrapper = (IdentifiableWrapper) getSettings().get("checked");
        if (wrapper == null) return null;
        return TRUE_OPTION_ID == wrapper.getId(); }

    public String getPrintPermissionKey() { return "printContract_list"; }
    public String getCheckedBySignaturePermissionKey() { return "editCheckedBySignature_list"; }
    public String getPrintAllPermissionKey() { return "printAllContracts_list"; }

    public String getListSettingsKey()
    {
        return "StudentContractList.contracts";
    }
}
