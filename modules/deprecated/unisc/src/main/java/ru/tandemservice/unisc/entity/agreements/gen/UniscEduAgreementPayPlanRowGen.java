package ru.tandemservice.unisc.entity.agreements.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unisc.entity.agreements.UniscEduAdditAgreement;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayPlanRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Строка графика оплат
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniscEduAgreementPayPlanRowGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayPlanRow";
    public static final String ENTITY_NAME = "uniscEduAgreementPayPlanRow";
    public static final int VERSION_HASH = -605501964;
    private static IEntityMeta ENTITY_META;

    public static final String L_OWNER = "owner";
    public static final String L_REPLACED_BY = "replacedBy";
    public static final String P_STAGE = "stage";
    public static final String P_DATE = "date";
    public static final String P_J_COST = "JCost";
    public static final String P_N_COST = "NCost";
    public static final String P_COMMENT = "comment";
    public static final String P_DISCOUNT = "discount";
    public static final String P_CHANGED = "changed";

    private UniscEduAgreementBase _owner;     // Договор
    private UniscEduAdditAgreement _replacedBy;     // Изменен в
    private int _stage;     // Номер периода
    private Date _date;     // Дата
    private long _JCost;     // Стоимость (юр. лицо)
    private long _NCost;     // Стоимость (физ. лицо)
    private String _comment;     // Комментарий
    private Double _discount;     // Скидка (в процентах)
    private Date _changed;     // Дата последнего изменения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Договор. Свойство не может быть null.
     */
    @NotNull
    public UniscEduAgreementBase getOwner()
    {
        return _owner;
    }

    /**
     * @param owner Договор. Свойство не может быть null.
     */
    public void setOwner(UniscEduAgreementBase owner)
    {
        dirty(_owner, owner);
        _owner = owner;
    }

    /**
     * @return Изменен в.
     */
    public UniscEduAdditAgreement getReplacedBy()
    {
        return _replacedBy;
    }

    /**
     * @param replacedBy Изменен в.
     */
    public void setReplacedBy(UniscEduAdditAgreement replacedBy)
    {
        dirty(_replacedBy, replacedBy);
        _replacedBy = replacedBy;
    }

    /**
     * @return Номер периода. Свойство не может быть null.
     */
    @NotNull
    public int getStage()
    {
        return _stage;
    }

    /**
     * @param stage Номер периода. Свойство не может быть null.
     */
    public void setStage(int stage)
    {
        dirty(_stage, stage);
        _stage = stage;
    }

    /**
     * @return Дата. Свойство не может быть null.
     */
    @NotNull
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date Дата. Свойство не может быть null.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return Стоимость (юр. лицо). Свойство не может быть null.
     */
    @NotNull
    public long getJCost()
    {
        return _JCost;
    }

    /**
     * @param JCost Стоимость (юр. лицо). Свойство не может быть null.
     */
    public void setJCost(long JCost)
    {
        dirty(_JCost, JCost);
        _JCost = JCost;
    }

    /**
     * @return Стоимость (физ. лицо). Свойство не может быть null.
     */
    @NotNull
    public long getNCost()
    {
        return _NCost;
    }

    /**
     * @param NCost Стоимость (физ. лицо). Свойство не может быть null.
     */
    public void setNCost(long NCost)
    {
        dirty(_NCost, NCost);
        _NCost = NCost;
    }

    /**
     * @return Комментарий.
     */
    @Length(max=255)
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * @return Скидка (в процентах).
     */
    public Double getDiscount()
    {
        return _discount;
    }

    /**
     * @param discount Скидка (в процентах).
     */
    public void setDiscount(Double discount)
    {
        dirty(_discount, discount);
        _discount = discount;
    }

    /**
     * @return Дата последнего изменения.
     */
    public Date getChanged()
    {
        return _changed;
    }

    /**
     * @param changed Дата последнего изменения.
     */
    public void setChanged(Date changed)
    {
        dirty(_changed, changed);
        _changed = changed;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UniscEduAgreementPayPlanRowGen)
        {
            setOwner(((UniscEduAgreementPayPlanRow)another).getOwner());
            setReplacedBy(((UniscEduAgreementPayPlanRow)another).getReplacedBy());
            setStage(((UniscEduAgreementPayPlanRow)another).getStage());
            setDate(((UniscEduAgreementPayPlanRow)another).getDate());
            setJCost(((UniscEduAgreementPayPlanRow)another).getJCost());
            setNCost(((UniscEduAgreementPayPlanRow)another).getNCost());
            setComment(((UniscEduAgreementPayPlanRow)another).getComment());
            setDiscount(((UniscEduAgreementPayPlanRow)another).getDiscount());
            setChanged(((UniscEduAgreementPayPlanRow)another).getChanged());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniscEduAgreementPayPlanRowGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniscEduAgreementPayPlanRow.class;
        }

        public T newInstance()
        {
            return (T) new UniscEduAgreementPayPlanRow();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "owner":
                    return obj.getOwner();
                case "replacedBy":
                    return obj.getReplacedBy();
                case "stage":
                    return obj.getStage();
                case "date":
                    return obj.getDate();
                case "JCost":
                    return obj.getJCost();
                case "NCost":
                    return obj.getNCost();
                case "comment":
                    return obj.getComment();
                case "discount":
                    return obj.getDiscount();
                case "changed":
                    return obj.getChanged();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "owner":
                    obj.setOwner((UniscEduAgreementBase) value);
                    return;
                case "replacedBy":
                    obj.setReplacedBy((UniscEduAdditAgreement) value);
                    return;
                case "stage":
                    obj.setStage((Integer) value);
                    return;
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "JCost":
                    obj.setJCost((Long) value);
                    return;
                case "NCost":
                    obj.setNCost((Long) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "discount":
                    obj.setDiscount((Double) value);
                    return;
                case "changed":
                    obj.setChanged((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "owner":
                        return true;
                case "replacedBy":
                        return true;
                case "stage":
                        return true;
                case "date":
                        return true;
                case "JCost":
                        return true;
                case "NCost":
                        return true;
                case "comment":
                        return true;
                case "discount":
                        return true;
                case "changed":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "owner":
                    return true;
                case "replacedBy":
                    return true;
                case "stage":
                    return true;
                case "date":
                    return true;
                case "JCost":
                    return true;
                case "NCost":
                    return true;
                case "comment":
                    return true;
                case "discount":
                    return true;
                case "changed":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "owner":
                    return UniscEduAgreementBase.class;
                case "replacedBy":
                    return UniscEduAdditAgreement.class;
                case "stage":
                    return Integer.class;
                case "date":
                    return Date.class;
                case "JCost":
                    return Long.class;
                case "NCost":
                    return Long.class;
                case "comment":
                    return String.class;
                case "discount":
                    return Double.class;
                case "changed":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniscEduAgreementPayPlanRow> _dslPath = new Path<UniscEduAgreementPayPlanRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniscEduAgreementPayPlanRow");
    }
            

    /**
     * @return Договор. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayPlanRow#getOwner()
     */
    public static UniscEduAgreementBase.Path<UniscEduAgreementBase> owner()
    {
        return _dslPath.owner();
    }

    /**
     * @return Изменен в.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayPlanRow#getReplacedBy()
     */
    public static UniscEduAdditAgreement.Path<UniscEduAdditAgreement> replacedBy()
    {
        return _dslPath.replacedBy();
    }

    /**
     * @return Номер периода. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayPlanRow#getStage()
     */
    public static PropertyPath<Integer> stage()
    {
        return _dslPath.stage();
    }

    /**
     * @return Дата. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayPlanRow#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return Стоимость (юр. лицо). Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayPlanRow#getJCost()
     */
    public static PropertyPath<Long> JCost()
    {
        return _dslPath.JCost();
    }

    /**
     * @return Стоимость (физ. лицо). Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayPlanRow#getNCost()
     */
    public static PropertyPath<Long> NCost()
    {
        return _dslPath.NCost();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayPlanRow#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @return Скидка (в процентах).
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayPlanRow#getDiscount()
     */
    public static PropertyPath<Double> discount()
    {
        return _dslPath.discount();
    }

    /**
     * @return Дата последнего изменения.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayPlanRow#getChanged()
     */
    public static PropertyPath<Date> changed()
    {
        return _dslPath.changed();
    }

    public static class Path<E extends UniscEduAgreementPayPlanRow> extends EntityPath<E>
    {
        private UniscEduAgreementBase.Path<UniscEduAgreementBase> _owner;
        private UniscEduAdditAgreement.Path<UniscEduAdditAgreement> _replacedBy;
        private PropertyPath<Integer> _stage;
        private PropertyPath<Date> _date;
        private PropertyPath<Long> _JCost;
        private PropertyPath<Long> _NCost;
        private PropertyPath<String> _comment;
        private PropertyPath<Double> _discount;
        private PropertyPath<Date> _changed;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Договор. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayPlanRow#getOwner()
     */
        public UniscEduAgreementBase.Path<UniscEduAgreementBase> owner()
        {
            if(_owner == null )
                _owner = new UniscEduAgreementBase.Path<UniscEduAgreementBase>(L_OWNER, this);
            return _owner;
        }

    /**
     * @return Изменен в.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayPlanRow#getReplacedBy()
     */
        public UniscEduAdditAgreement.Path<UniscEduAdditAgreement> replacedBy()
        {
            if(_replacedBy == null )
                _replacedBy = new UniscEduAdditAgreement.Path<UniscEduAdditAgreement>(L_REPLACED_BY, this);
            return _replacedBy;
        }

    /**
     * @return Номер периода. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayPlanRow#getStage()
     */
        public PropertyPath<Integer> stage()
        {
            if(_stage == null )
                _stage = new PropertyPath<Integer>(UniscEduAgreementPayPlanRowGen.P_STAGE, this);
            return _stage;
        }

    /**
     * @return Дата. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayPlanRow#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(UniscEduAgreementPayPlanRowGen.P_DATE, this);
            return _date;
        }

    /**
     * @return Стоимость (юр. лицо). Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayPlanRow#getJCost()
     */
        public PropertyPath<Long> JCost()
        {
            if(_JCost == null )
                _JCost = new PropertyPath<Long>(UniscEduAgreementPayPlanRowGen.P_J_COST, this);
            return _JCost;
        }

    /**
     * @return Стоимость (физ. лицо). Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayPlanRow#getNCost()
     */
        public PropertyPath<Long> NCost()
        {
            if(_NCost == null )
                _NCost = new PropertyPath<Long>(UniscEduAgreementPayPlanRowGen.P_N_COST, this);
            return _NCost;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayPlanRow#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(UniscEduAgreementPayPlanRowGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @return Скидка (в процентах).
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayPlanRow#getDiscount()
     */
        public PropertyPath<Double> discount()
        {
            if(_discount == null )
                _discount = new PropertyPath<Double>(UniscEduAgreementPayPlanRowGen.P_DISCOUNT, this);
            return _discount;
        }

    /**
     * @return Дата последнего изменения.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayPlanRow#getChanged()
     */
        public PropertyPath<Date> changed()
        {
            if(_changed == null )
                _changed = new PropertyPath<Date>(UniscEduAgreementPayPlanRowGen.P_CHANGED, this);
            return _changed;
        }

        public Class getEntityClass()
        {
            return UniscEduAgreementPayPlanRow.class;
        }

        public String getEntityName()
        {
            return "uniscEduAgreementPayPlanRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
