/* $Id: DAO.java 8754 2009-06-29 09:27:43Z ekachanova $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisc.component.agreement.AgreementLogTab;

import org.tandemframework.core.entity.IEntity;

import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unisc.entity.agreements.IUniscEduAgreement2PersonRole;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;

/**
 * @author ekachanova
 */
public class DAO extends org.tandemframework.shared.commonbase.logging.bo.EventLog.ui.View.DAO implements IDAO
{
	@SuppressWarnings("unchecked")
	@Override
	public void prepare(final Model model)
	{
		final IEntity entity = UniDaoFacade.getCoreDao().get(model.getEntityId());
		if (entity instanceof UniscEduAgreementBase) {
			model.setEntityId(entity.getId());
		} else if (entity instanceof IUniscEduAgreement2PersonRole) {
			model.setEntityId(((IUniscEduAgreement2PersonRole)entity).getAgreement().getId());
		} else {
			if (null == entity) { throw new NullPointerException(String.valueOf(model.getEntityId())); }
			throw new IllegalStateException(entity.getClass().getName());
		}
	}

}
