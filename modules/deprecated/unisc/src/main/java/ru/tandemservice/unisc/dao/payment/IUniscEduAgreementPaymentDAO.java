package ru.tandemservice.unisc.dao.payment;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.tandemframework.core.util.cache.SpringBeanCache;

/**
 * Обрабатывает платежи, считает задолжность
 * @author vdanilov
 */
public interface IUniscEduAgreementPaymentDAO {
    public static final SpringBeanCache<IUniscEduAgreementPaymentDAO> INSTANCE = new SpringBeanCache<IUniscEduAgreementPaymentDAO>(IUniscEduAgreementPaymentDAO.class.getName());

    /**
     * @return состояние для обработки событий (по договору)
     **/
    IUniscEduAgreementPaymentState getNewState();

    /**
     * @param agreementIds - идентификаторы договоров (UniscEduMainAgreement)
     * @param date - дата, на момент которой необходимо получить состояние договоров (null - далекое будущее)
     * @return { mainAgreement.id ->  { список событий, связаных с договором } }
     **/
    Map<Long, List<IUniscEduAgreementPaymentEvent>> getEventsMap(Collection<Long> agreementIds, Date date);

    /**
     * @param agreementIds - идентификаторы договоров (UniscEduMainAgreement)
     * @param date - дата, на момент которой необходимо получить состояние договоров (null - далекое будущее)
     * @return { mainAgreement.id -> state (nullable) }
     */
    Map<Long, IUniscEduAgreementPaymentState> getStateMap(Collection<Long> agreementIds, Date date);

}
