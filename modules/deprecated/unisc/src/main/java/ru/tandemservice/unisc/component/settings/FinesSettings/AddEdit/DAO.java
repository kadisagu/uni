package ru.tandemservice.unisc.component.settings.FinesSettings.AddEdit;

import org.hibernate.Session;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unisc.entity.config.UniscFinesHistory;

public class DAO extends UniDao<Model> implements IDAO {
	@Override public void prepare(final Model model) {
		if (null != model.getFines().getId()) {
			model.setFines(get(UniscFinesHistory.class, model.getFines().getId()));
		}
	}

	@Override
	public void save(final Model model) {
		final Session session = getSession();
		session.saveOrUpdate(model.getFines());
	}

}
