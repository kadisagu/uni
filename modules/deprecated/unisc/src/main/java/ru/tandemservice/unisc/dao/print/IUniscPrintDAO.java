package ru.tandemservice.unisc.dao.print;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;

import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayPlanRow;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPrintVersion;

/**
 * @author vdanilov
 */
public interface IUniscPrintDAO {
    public static final SpringBeanCache<IUniscPrintDAO> INSTANCE = new SpringBeanCache<IUniscPrintDAO>(IUniscPrintDAO.class.getName());

    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    RtfInjectModifier getRtfInjectorModifier(UniscEduAgreementBase agreementBase, Person person);

    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    RtfDocument getDocumentTemplate(UniscEduAgreementBase agreementBase);

    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    RtfInjectModifier getRtfInjectorModifier4Quittance(UniscEduAgreementPayPlanRow row, Person person);

    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    RtfDocument getDocumentTemplate4Quittance(UniscEduAgreementPayPlanRow row);

    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    String getBarcodeString(UniscEduAgreementBase agreement);

    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    UniscEduAgreementPrintVersion getLastPrintVersion(UniscEduAgreementBase agreementBase);

}
