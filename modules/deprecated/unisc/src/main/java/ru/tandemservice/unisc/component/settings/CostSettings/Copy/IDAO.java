package ru.tandemservice.unisc.component.settings.CostSettings.Copy;

import ru.tandemservice.uni.dao.IPrepareable;

/**
 * @author vdanilov
 */
public interface IDAO extends IPrepareable<Model> {

	void save(Model model);

}
