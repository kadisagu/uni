package ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage;

import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;

/**
 * @author vdanilov
 */
@SuppressWarnings("deprecation")
public interface IAgreementOperation {
    UniscEduAgreementBase update(UniscEduAgreementBase agreement, boolean save);
}
