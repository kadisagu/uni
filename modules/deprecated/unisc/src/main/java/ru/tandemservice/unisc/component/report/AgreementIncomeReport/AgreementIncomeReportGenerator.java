package ru.tandemservice.unisc.component.report.AgreementIncomeReport;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.unisc.UniscDefines;
import ru.tandemservice.unisc.component.report.PaymentReport.MQBuilderFactory;
import ru.tandemservice.unisc.component.report.UniscRtfUtils;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayFactRow;
import ru.tandemservice.unisc.entity.agreements.UniscEduMainAgreement;
import ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student;
import ru.tandemservice.unisc.entity.catalog.UniscTemplateDocument;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;

import java.util.*;

/**
 * @author oleyba
 * @since 19.08.2009
 */
public class AgreementIncomeReportGenerator
{
    protected Session session;
    protected Long orgUnitId;
    protected MQBuilderFactory agreementBuilder;
    protected Date dateFrom;
    protected Date dateTo;

    protected Map<Long, Student> studentByAgreementId = new HashMap<>();
    protected Map<Long, ReportRow> reportRowByStudentId = new HashMap<>();
    protected Map<Long, ReportRow> reportRowByAgreementId = new HashMap<Long, ReportRow>()
    {
        private static final long serialVersionUID = 1L;

        @Override
        public ReportRow get(Object key)
        {
            Student student = studentByAgreementId.get(key);
            ReportRow row = reportRowByStudentId.get(student.getId());
            if (row == null) reportRowByStudentId.put(student.getId(), row = new ReportRow(student));
            return row;
        }
    };
    protected Map<MultiKey, AgreementIncomeReportRow.RowGroup> groupMap = new HashMap<>();
    protected List<Integer> months = new ArrayList<>();

    public AgreementIncomeReportGenerator(Session session, Long orgUnitId, MQBuilderFactory agreementBuilder, Date dateFrom, Date dateTo)
    {
        this.session = session;
        this.orgUnitId = orgUnitId;
        this.agreementBuilder = agreementBuilder;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        for (int month = CommonBaseDateUtil.getMonthStartingWithOne(dateFrom); month <= CommonBaseDateUtil.getMonthStartingWithOne(dateTo); month++)
            months.add(month);
    }

    public DatabaseFile generateReportContent()
    {
        ITemplateDocument templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniscTemplateDocument.class, UniscDefines.TEMPLATE_AGREEMENT_INCOME_REPORT);
        RtfDocument document = new RtfReader().read(templateDocument.getContent());

        fillStudentMap();
        fillPaymentMap();

        final List<String[]> reportRowList = new ArrayList<>();
        List<AgreementIncomeReportRow.RowGroup> groups = new ArrayList<>(groupMap.values());
        Collections.sort(groups, AgreementIncomeReportRow.RowGroup.comparator);
        long total = 0L;
        for (AgreementIncomeReportRow.RowGroup group : groups)
            total = total + group.appendRows(reportRowList, months, ReportRow.getComparator(), 4);

        modify(document, reportRowList, total);

        byte[] data = RtfUtil.toByteArray(document);
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    protected void modify(RtfDocument document, final List<String[]> reportRowList, long total)
    {
        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T", reportRowList.toArray(new String[reportRowList.size()][]));
        final int[] parts = new int[months.size()];
        Arrays.fill(parts, 1);

        tableModifier.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public void beforeModify(RtfTable table, int currentRowIndex)
            {
                RtfUtil.splitRow(table.getRowList().get(currentRowIndex - 1), 4, (newCell, index) -> {
                    String title = StringUtils.capitalize(RussianDateFormatUtils.getMonthName(months.get(index), true));
                    newCell.setElementList(Collections.singletonList((IRtfElement) RtfBean.getElementFactory().createRtfText(title)));
                }, parts);
                RtfUtil.splitRow(table.getRowList().get(currentRowIndex), 4, null, parts);
            }

            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                UniscRtfUtils.afterModify(newRowList, startIndex, reportRowList);
            }
        });
        tableModifier.modify(document);

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("periodReport", DateFormatter.DEFAULT_DATE_FORMATTER.format(dateFrom) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(dateTo));
        OrgUnit orgUnit = orgUnitId == null ? null : UniDaoFacade.getCoreDao().get(OrgUnit.class, orgUnitId);
        injectModifier.put("scope", orgUnit == null ? "ИТОГО по всему ОУ" : "ИТОГО по поздразделению (" + (orgUnit.getNominativeCaseTitle() == null ? orgUnit.getTitle() : orgUnit.getNominativeCaseTitle()) + ")");
        injectModifier.put("total", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(0.001d * total));
        injectModifier.modify(document);
    }

    protected void fillPaymentMap()
    {
        MQBuilder builder = new MQBuilder(UniscEduAgreementPayFactRow.ENTITY_CLASS, "p");
        builder.add(MQExpression.in("p", UniscEduAgreementPayFactRow.L_AGREEMENT + ".id", agreementBuilder.createIdBuilder()));
        builder.add(UniMQExpression.betweenDate("p", UniscEduAgreementPayFactRow.P_DATE, dateFrom, dateTo));
        builder.addOrder("p", UniscEduAgreementPayFactRow.P_DATE);
        builder.addLeftJoinFetch("p", UniscEduAgreementPayFactRow.L_TYPE, "type_fetch");
        builder.addLeftJoinFetch("p", UniscEduAgreementPayFactRow.L_AGREEMENT, "agr_fetch");
        builder.addLeftJoinFetch("agr_fetch", UniscEduMainAgreement.L_CONFIG, "conf_fetch");
        builder.addLeftJoinFetch("conf_fetch", UniscEduOrgUnit.L_EDUCATION_ORG_UNIT, "agr_edu_ou_fetch");
        builder.addLeftJoinFetch("agr_edu_ou_fetch", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, "agr_form_fetch");
        builder.addLeftJoinFetch("agr_edu_ou_fetch", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, "agr_terr_fetch");
        builder.addLeftJoinFetch("agr_edu_ou_fetch", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "agr_hs_fetch");
        builder.addLeftJoinFetch("agr_hs_fetch", EducationLevelsHighSchool.L_EDUCATION_LEVEL, "agr_lev_fetch");
        builder.addDomain("rel_g", UniscEduAgreement2Student.ENTITY_CLASS);
        builder.add(MQExpression.eqProperty("rel_g", UniscEduAgreement2Student.L_AGREEMENT, "p", UniscEduAgreementPayFactRow.L_AGREEMENT));
        builder.addLeftJoinFetch("rel_g", UniscEduAgreement2Student.L_STUDENT, "student_fetch");
        builder.addLeftJoinFetch("student_fetch", Student.L_GROUP, "group_fetch");
        builder.add(MQExpression.eq("group_fetch", Group.P_ARCHIVAL, Boolean.FALSE));

        for (UniscEduAgreementPayFactRow payment : builder.<UniscEduAgreementPayFactRow>getResultList(session))
        {
            ReportRow reportRow = reportRowByAgreementId.get(payment.getAgreement().getId());
            reportRow.addPayment(payment);
            getRowGroup(payment).addRow(reportRow);
        }
    }

    private AgreementIncomeReportRow.RowGroup getRowGroup(UniscEduAgreementPayFactRow payment)
    {
        EducationOrgUnit eduOu = payment.getAgreement().getConfig().getEducationOrgUnit();
        MultiKey key = new MultiKey(eduOu.getFormativeOrgUnit().getId(), eduOu.getTerritorialOrgUnit().getId());
        AgreementIncomeReportRow.RowGroup rowGroup = groupMap.get(key);
        if (rowGroup == null)
            groupMap.put(key, rowGroup = new AgreementIncomeReportRow.RowGroup(eduOu));
        return rowGroup;
    }

    protected void fillStudentMap()
    {
        MQBuilder builder = new MQBuilder(UniscEduAgreement2Student.ENTITY_CLASS, "rel");
        builder.addLeftJoinFetch("rel", UniscEduAgreement2Student.L_STUDENT, "student_fetch");
        builder.addLeftJoinFetch("student_fetch", Student.L_GROUP, "group_fetch");
        builder.add(MQExpression.eq("group_fetch", Group.P_ARCHIVAL, Boolean.FALSE));
        builder.addLeftJoinFetch("student_fetch", Student.L_COURSE, "course_fetch");
        builder.addLeftJoinFetch("student_fetch", Student.L_PERSON, "person_fetch");
        builder.addLeftJoinFetch("person_fetch", Person.L_IDENTITY_CARD, "idc_fetch");
        builder.addLeftJoinFetch("student_fetch", Student.L_EDUCATION_ORG_UNIT, "edu_ou_fetch");
        builder.addLeftJoinFetch("edu_ou_fetch", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "hs_fetch");
        builder.addLeftJoinFetch("hs_fetch", EducationLevelsHighSchool.L_EDUCATION_LEVEL, "lev_fetch");

        builder.addDomain("pay", UniscEduAgreementPayFactRow.ENTITY_CLASS);
        builder.add(MQExpression.eqProperty("pay", UniscEduAgreementPayFactRow.L_AGREEMENT + ".id", "rel", UniscEduAgreement2Student.L_AGREEMENT + ".id"));
        builder.add(MQExpression.in("pay", UniscEduAgreementPayFactRow.L_AGREEMENT + ".id", agreementBuilder.createIdBuilder()));
        builder.add(UniMQExpression.betweenDate("pay", UniscEduAgreementPayFactRow.P_DATE, dateFrom, dateTo));

        for (UniscEduAgreement2Student rel : builder.<UniscEduAgreement2Student>getResultList(session))
            studentByAgreementId.put(rel.getAgreement().getId(), rel.getStudent());
    }

    protected static class ReportRow extends AgreementIncomeReportRow
    {
        private Student student;

        public ReportRow(Student student)
        {
            this.student = student;
        }

        public static Comparator<AgreementIncomeReportRow> getComparator()
        {
            return (o1, o2) -> {
                if (o1 == o2) { return 0; }
                ReportRow r1 = (ReportRow) o1;
                ReportRow r2 = (ReportRow) o2;
                int i;
                if (0 != (i = Course.COURSE_COMPARATOR.compare(r1.student.getCourse(), r2.student.getCourse()))) { return i; }
                if (0 != (i = CommonCollator.TITLED_COMPARATOR.compare(r1.student.getGroup(), r2.student.getGroup()))) { return i; }
                return Student.FULL_FIO_AND_ID_COMPARATOR.compare(r1.student, r2.student);
            };
        }

        @Override
        public List<String> getTitles()
        {
            return Arrays.asList(
                    student.getPerson().getFullFio(),
                    student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getShortTitle(),
                    student.getCourse().getTitle(),
                    student.getGroup() == null ? "" : student.getGroup().getTitle());
        }
    }
}
