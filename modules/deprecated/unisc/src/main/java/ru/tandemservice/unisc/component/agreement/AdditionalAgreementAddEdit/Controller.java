package ru.tandemservice.unisc.component.agreement.AdditionalAgreementAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.ContextLocal;

/**
 * @author vdanilov
 */
public class Controller extends ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Controller<IDAO, Model> {

    @Override
    public void onRenderComponent(IBusinessComponent component)
    {
        ContextLocal.beginPageTitlePart(getModel(component).isCreationFormFlag() ? "Добавление соглашения" : "Редактирование соглашения");   
    }
}
