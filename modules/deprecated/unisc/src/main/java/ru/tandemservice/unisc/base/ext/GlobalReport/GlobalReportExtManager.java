/* $Id$ */
package ru.tandemservice.unisc.base.ext.GlobalReport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.GlobalReportManager;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.logic.GlobalReportDefinition;
import ru.tandemservice.unisc.base.ext.GlobalReport.ui.List.UniscGlobalReportListAddon;

/**
 * @author azhebko
 * @since 31.03.2014
 */
@Configuration
public class GlobalReportExtManager extends BusinessObjectExtensionManager
{
    public static final String SC_PAYMENT_REPORT = "scPaymentReport";
    public static final String SC_AGREEMENT_INCOME_REPORT = "scAgreementIncomeReport";

    @Autowired
    private GlobalReportManager _globalReportManager;

    @Bean
    public ItemListExtension<GlobalReportDefinition> reportListExtension()
    {
        return itemListExtension(_globalReportManager.reportListExtPoint())
                .add(SC_PAYMENT_REPORT, new GlobalReportDefinition("sc", "scPayment", ru.tandemservice.unisc.component.report.PaymentReport.PaymentReportList.Model.class.getPackage().getName()))
                .add(SC_AGREEMENT_INCOME_REPORT, new GlobalReportDefinition("sc", "scAgreementIncome", ru.tandemservice.unisc.component.report.AgreementIncomeReport.AgreementIncomeReportList.Model.class.getPackage().getName()))
                .add("scPayDebtorsReport", new GlobalReportDefinition("sc", "scPayDebtors", ru.tandemservice.unisc.component.report.PayDebtorsReport.Model.class.getPackage().getName()))
                .add("scAgreementIncomeSummaryReport", new GlobalReportDefinition("sc", "scAgreementIncomeSummary", ru.tandemservice.unisc.component.report.AgreementIncomeSummaryReport.AgreementIncomeSummaryReportList.Model.class.getPackage().getName()))
                .add("scGroupIncomeReport", new GlobalReportDefinition("sc", "scGroupIncome", ru.tandemservice.unisc.component.report.GroupIncomeReport.GroupIncomeReportList.Model.class.getPackage().getName()))
                .add("scStudentsWithoutContractsReport", new GlobalReportDefinition("sc", "scStudentsWithoutContracts", ru.tandemservice.unisc.component.report.StudentsWithoutContractsReport.Model.class.getPackage().getName()))
                .create();
    }
}