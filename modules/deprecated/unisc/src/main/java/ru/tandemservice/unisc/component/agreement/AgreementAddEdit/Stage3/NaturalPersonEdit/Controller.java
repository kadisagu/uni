package ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage3.NaturalPersonEdit;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInline;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInlineConfig;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInlineUI;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson;

/**
 * @author vdanilov
 */
@SuppressWarnings("deprecation")
public class Controller extends AbstractBusinessController<IDAO, Model> {

    public static final String REGION_ADDRESS = "addressRegion";

    @Override
    public void onRefreshComponent(final IBusinessComponent component) {

        Model model = getModel(component);

        getDao().prepare(model);

        AddressBaseEditInlineConfig addressConfig = new AddressBaseEditInlineConfig();
        addressConfig.setEntityId(model.getNaturalPerson().getId() == null ? null : model.getNaturalPerson().getId());
        addressConfig.setDetailedOnly(true);
        addressConfig.setDetailLevel(4);
        addressConfig.setAreaVisible(true);
        addressConfig.setInline(true);
        addressConfig.setFieldSetTitle("Адрес");
        addressConfig.setAddressProperty(UniscEduAgreementNaturalPerson.L_ADDRESS);
        addressConfig.setAddressBase(model.getNaturalPerson().getAddress());
        component.createChildRegion(REGION_ADDRESS, new ComponentActivator(AddressBaseEditInline.class.getSimpleName(), new ParametersMap().add(AddressBaseEditInlineUI.BIND_CONFIG, addressConfig)));
    }

    public void onClickApply(final IBusinessComponent component) {
        Model model = getModel(component);
        AddressBaseEditInlineUI addressBaseEditInlineUI = (AddressBaseEditInlineUI) component.getChildRegion(REGION_ADDRESS).getActiveComponent().getPresenter();
        if(addressBaseEditInlineUI.getResult() == null) throw new RuntimeException("Адрес обязателен");
        if(!(addressBaseEditInlineUI.getResult() instanceof AddressDetailed)) throw new RuntimeException("Некорректный адрес");
        AddressDetailed address = (AddressDetailed) addressBaseEditInlineUI.getResult();

        model.setAddress(address);

        getDao().save(model);
        deactivate(component);
    }

}
