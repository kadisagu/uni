package ru.tandemservice.unisc.component.settings.CostSettings;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.gen.OrgUnitGen;
import ru.tandemservice.uni.entity.education.gen.EducationOrgUnitGen;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;
import ru.tandemservice.unisc.entity.config.gen.UniscEduOrgUnitGen;

import java.util.Collections;
import java.util.List;


public class Controller extends AbstractBusinessController<IDAO, Model> {

    public void onClickExport(final IBusinessComponent component) {
        this.activateInRoot(component, new ComponentActivator(
                ru.tandemservice.unisc.component.settings.CostSettings.io.Export.Model.class.getPackage().getName()
        ));
    }

    public void onClickImport(final IBusinessComponent component) {
        this.activateInRoot(component, new ComponentActivator(
                ru.tandemservice.unisc.component.settings.CostSettings.io.Import.Model.class.getPackage().getName()
        ));
    }


    @Override
    public void onRefreshComponent(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        model.setSettings(UniBaseUtils.getDataSettings(component, "CostSettings.filter"));
        this.getDao().prepare(model);

        if (null == model.getDataSource()) {
            final DynamicListDataSource<UniscEduOrgUnit> ds = new DynamicListDataSource<>(component, component1 -> {
                Controller.this.getDao().refreshDataSource(model);
            });


            ds.addColumn(new PublisherLinkColumn("Направление подготовки (специальность)", UniscEduOrgUnitGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnitGen.L_EDUCATION_LEVEL_HIGH_SCHOOL+".fullTitle") {
                @Override public List<IEntity> getEntityList(final IEntity entity) { return Collections.singletonList(entity); }
            }.setResolver(new DefaultPublisherLinkResolver() {
                @Override public String getComponentName(final IEntity entity) { return Controller.class.getPackage().getName()+".Pub"; }
            }).setOrderable(true));
            ds.addColumn(new SimpleColumn("Шифр", UniscEduOrgUnitGen.P_CIPHER).setClickable(false).setOrderable(true));
            ds.addColumn(new SimpleColumn("Формирующее подр.", UniscEduOrgUnitGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnitGen.L_FORMATIVE_ORG_UNIT+".title").setClickable(false).setOrderable(false));
            ds.addColumn(new SimpleColumn("Территориальное подр.", UniscEduOrgUnitGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnitGen.L_TERRITORIAL_ORG_UNIT+"."+ OrgUnitGen.P_TERRITORIAL_TITLE).setClickable(false).setOrderable(false));

            ds.addColumn(new SimpleColumn("Форма освоения", UniscEduOrgUnitGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnitGen.L_DEVELOP_FORM+".title").setClickable(false).setOrderable(false));
            ds.addColumn(new SimpleColumn("Условие освоения", UniscEduOrgUnitGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnitGen.L_DEVELOP_CONDITION+".title").setClickable(false).setOrderable(false));
            ds.addColumn(new SimpleColumn("Технология освоения", UniscEduOrgUnitGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnitGen.L_DEVELOP_TECH+".title").setClickable(false).setOrderable(false));
            ds.addColumn(new SimpleColumn("Срок освоения", UniscEduOrgUnitGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnitGen.L_DEVELOP_PERIOD+".title").setClickable(false).setOrderable(false));

            ds.addColumn(new SimpleColumn("Количество частей (семестров) в учебном году", UniscEduOrgUnitGen.P_COUNT_TERM).setClickable(false));
            ds.addColumn(new SimpleColumn("Описание", UniscEduOrgUnitGen.P_DESCRIPTION).setClickable(false).setOrderable(false));
            ds.addColumn(new SimpleColumn("Условия", UniscEduOrgUnitGen.P_CONDITIONS).setClickable(false).setOrderable(false));

            ds.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit"));
            ds.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить направление подготовки из настройки вместе с заданными графиками оплат?"));
            model.setDataSource(ds);
        }
    }

    public void onClickDelete(final IBusinessComponent component) {
        getDao().delete((Long) component.getListenerParameter());
    }

    public void onClickEdit(final IBusinessComponent component) {
        activate(component, new ComponentActivator(
                Controller.class.getPackage().getName()+".Edit",
                Collections.singletonMap(PublisherActivator.PUBLISHER_ID_KEY, component.getListenerParameter())
        ));
    }

    public void onClickAdd(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        activate(component, new ComponentActivator(
                Controller.class.getPackage().getName()+".Add",
                Collections.<String, Object>singletonMap("defaultEducationYearId", model.getEducationYear().getId())
        ));
    }

    public void onClickCopy(final IBusinessComponent component) {
        final EducationYear educationYear = getModel(component).getEducationYear();
        if (null == educationYear) { return; }

        activate(component, new ComponentActivator(
                Controller.class.getPackage().getName()+".Copy",
                Collections.<String, Object>singletonMap("yearId", educationYear.getId())
        ));
    }

    public void onClickSearch(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        getDao().prepare(model);
        model.getDataSource().refresh();
        DataSettingsFacade.saveSettings(model.getSettings());
    }

    public void onClickClear(final IBusinessComponent component) {
        this.getModel(component).getSettings().clear();
        this.onClickSearch(component);
    }
}
