package ru.tandemservice.unisc.component.settings.OrgUnitPresenterSettings.Edit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;

import ru.tandemservice.unisc.entity.config.UniscOrgUnitPresenter;

@Input({
	@Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="id")
})
public class Model {
	private Long id;
	public Long getId() { return id; }
	public void setId(Long id) { this.id = id; }
	
	private UniscOrgUnitPresenter presenter;
	public UniscOrgUnitPresenter getPresenter() { return presenter; }
	public void setPresenter(UniscOrgUnitPresenter presenter) { this.presenter = presenter; }
	
}
