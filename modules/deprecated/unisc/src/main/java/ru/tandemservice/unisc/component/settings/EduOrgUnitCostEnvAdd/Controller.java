package ru.tandemservice.unisc.component.settings.EduOrgUnitCostEnvAdd;

import java.io.ByteArrayOutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.unisc.dao.config.IUniscEduOrgUnitConfigDAO;
import ru.tandemservice.unisc.ws.cost.EducationOrgUnitCostEnvironment;

/**
 * @author Vasily Zhukov
 * @since 17.02.2011
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);
    }

    public void onClickApply(IBusinessComponent component) throws Exception
    {
        Model model = getModel(component);

        ByteArrayOutputStream result = new ByteArrayOutputStream();
        JAXBContext context = JAXBContext.newInstance(EducationOrgUnitCostEnvironment.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(IUniscEduOrgUnitConfigDAO.INSTANCE.get().getEducationOrgUnitCostEnvironment(model.getEducationYear().getTitle(), model.getPayPeriod().getCode(), model.getStage()), result);

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().xml().fileName("educationOrgUnitCostEnvironment.soap.xml").document(result), true);
    }
}

