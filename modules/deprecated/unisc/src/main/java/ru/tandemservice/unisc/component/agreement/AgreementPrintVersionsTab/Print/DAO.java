/* $Id: DAO.java 6933 2009-03-07 05:41:27Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unisc.component.agreement.AgreementPrintVersionsTab.Print;

import org.tandemframework.core.document.IDocumentRenderer;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPrintVersion;

/**
 * @author vdanilov
 */
public class DAO extends UniBaseDao implements IDAO {

    @Override public IDocumentRenderer getDocumentRenderer(final Model model) {
        final UniscEduAgreementPrintVersion pv = get(UniscEduAgreementPrintVersion.class, model.getId());
        return pv.getRenderer();
    }
}
