package ru.tandemservice.unisc.sec;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.ClassGroupMeta;
import org.tandemframework.sec.meta.ModuleLocalGroupMeta;
import org.tandemframework.sec.meta.PermissionGroupMeta;
import org.tandemframework.sec.meta.SecurityConfigMeta;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.organization.base.util.IOrgUnitLPGVisibilityResolver;
import org.tandemframework.shared.organization.base.util.IOrgUnitPermissionCustomizer;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.uni.dao.IOrgstructDAO;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author nkokorina
 * Created on: 03.02.2010
 */

public class UniStudentContractOrgstructPermissionModifier implements ISecurityConfigMetaMapModifier, IOrgUnitPermissionCustomizer, IOrgUnitLPGVisibilityResolver
{

    @Override
    public void modify(Map<String, SecurityConfigMeta> securityConfigMetaMap)
    {
        SecurityConfigMeta config = new SecurityConfigMeta();
        config.setModuleName("uni");
        config.setName("uni-sc-sec-config");
        config.setTitle("");

        ModuleLocalGroupMeta moduleLocalGroupMeta = PermissionMetaUtil.createModuleLocalGroup(config, "uniscLocal", "Модуль «(Старый) Договоры и оплаты»");
        ClassGroupMeta localClassGroupAgreement = PermissionMetaUtil.createClassGroup(moduleLocalGroupMeta, "mainEduAgreementStudentLocalClass", "Объект «Договор со студентом»", "ru.tandemservice.unisc.entity.agreements.UniscEduMainAgreement");
        ClassGroupMeta localClassGroupAdditAgreement = PermissionMetaUtil.createClassGroup(moduleLocalGroupMeta, "additAgreementStudentLocalClass", "Объект «Дополнительное соглашение»", "ru.tandemservice.unisc.entity.agreements.UniscEduAdditAgreement");

        PermissionMetaUtil.createGroupRelation(config, "uniscMainEduAgreementStudentPG", localClassGroupAgreement.getName());
        PermissionMetaUtil.createGroupRelation(config, "uniscAdditionalAgreementPG", localClassGroupAdditAgreement.getName());

        for (OrgUnitType description : DataAccessServices.dao().getList(OrgUnitType.class, OrgUnitType.P_TITLE))
        {
            String code = description.getCode();

            PermissionGroupMeta permissionGroupStudentsTab = PermissionMetaUtil.createPermissionGroup(config, code + "StudentsPermissionGroup", "Вкладка «Студенты»");

            PermissionGroupMeta permissionGroupTab = PermissionMetaUtil.createPermissionGroup(permissionGroupStudentsTab, code + "ContractsTabPermissionGroup", "Вкладка «(Старый) Договоры»");
            PermissionMetaUtil.createPermission(permissionGroupTab, "orgUnit_viewContractTab_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(permissionGroupTab, "editCheckedBySignature_list_" + code, "Изменение флага «Проверен»");
            PermissionMetaUtil.createPermission(permissionGroupTab, "printContract_list_" + code, "Печать договора");
            PermissionMetaUtil.createPermission(permissionGroupTab, "printAllContracts_list_" + code, "Печать списка договоров");

            PermissionGroupMeta permissionGroupReportsAll = PermissionMetaUtil.createPermissionGroup(config, code + "ReportsTabPermissionGroup", "Вкладка «Отчеты»");

            PermissionGroupMeta permissionGroupReports = PermissionMetaUtil.createPermissionGroup(permissionGroupReportsAll, code + "UniscReportPermissionGroup", "Отчеты модуля «Договоры и оплаты»");
            PermissionMetaUtil.createPermission(permissionGroupReports, "viewPaymentReportList_" + code, "Просмотр отчета «Отчет по оплатам»");
            PermissionMetaUtil.createPermission(permissionGroupReports, "viewAgreementIncomeReportList_" + code, "Просмотр отчета «Поступление средств по договорам»");
            PermissionMetaUtil.createPermission(permissionGroupReports, "viewPayDebtorsReport_" + code, "Просмотр отчета «Список должников по оплате»");
            PermissionMetaUtil.createPermission(permissionGroupReports, "viewAgreementIncomeSummaryReport_" + code, "Просмотр отчета «Сводка поступлений по договорам»");
            PermissionMetaUtil.createPermission(permissionGroupReports, "viewGroupIncomeReport_" + code, "Просмотр отчета «Приход средств (по группе)»");
            PermissionMetaUtil.createPermission(permissionGroupReports, "viewStudentsWithoutContractsReport_" + code, "Просмотр отчета «Перечень студентов без договоров»");
        }

        securityConfigMetaMap.put(config.getName(), config);
    }

    @Override
    public void customize(OrgUnitType orgUnitType, Map<String, PermissionGroupMeta> permissionGroupMap)
    {
        boolean hidden = IOrgstructDAO.instance.get().hidePermissionsBasedOnFormingAndTerritorialKind(orgUnitType);
        String code = orgUnitType.getCode();

        permissionGroupMap.get(code + "ContractsTabPermissionGroup").setHidden(hidden);
        permissionGroupMap.get(code + "UniscReportPermissionGroup").setHidden(hidden);
    }

    @Override
    public List<ILPGVisibilityResolver> getResolverList()
    {
        return Arrays.<ILPGVisibilityResolver>asList(new ILPGVisibilityResolver()
        {
            @Override
            public String getGroupName()
            {
                return "uniscLocal";
            }

            @Override
            public boolean isHidden(OrgUnitType orgUnitType)
            {
                return IOrgstructDAO.instance.get().hidePermissionsBasedOnFormingAndTerritorialKind(orgUnitType);
            }
        });
    }
}
