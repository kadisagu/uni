package ru.tandemservice.unisc.dao.io;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.gen.EducationLevelsGen;
import ru.tandemservice.uni.entity.catalog.gen.EducationLevelsHighSchoolGen;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.education.gen.EducationOrgUnitGen;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisc.dao.config.IUniscEduOrgUnitConfigDAO;
import ru.tandemservice.unisc.dao.config.UniscEduOrgUnitConfigDAO;
import ru.tandemservice.unisc.entity.catalog.UniscPayPeriod;
import ru.tandemservice.unisc.entity.catalog.UniscPayPeriod.Freq;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlan;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlanRow;
import ru.tandemservice.unisc.entity.config.gen.UniscEduOrgUnitGen;
import ru.tandemservice.unisc.entity.config.gen.UniscEduOrgUnitPayPlanGen;
import ru.tandemservice.unisc.entity.config.gen.UniscEduOrgUnitPayPlanRowGen;

import java.util.*;

public class UniscIODao extends UniBaseDao implements IUniscIODao
{
    private static final Object[] MARKER = new Object[]{};
    public static final String BLOCK_NAME = "default";

    /* REIMPLEMENT IT */
    protected Block createBlock(final String identifier)
    {
        final String[] COLUMNS = {"1 курс", "2 курс", "3 курс", "4 курс", "5 курс", "6 курс"};
        final String title = "Стоимость обучения за год";
        return this.block(identifier, COLUMNS, title);
    }

    /* REIMPLEMENT IT */
    protected String getBlockIdentifier(final EducationOrgUnit edu)
    {
        return UniscIODao.BLOCK_NAME;
    }

    /* REIMPLEMENT IT */
    protected long cost(final Block block, final UniscPayPeriod p, final UniscEduOrgUnit eduOrgUnit, final int stage, final Double[] value)
    {
        final Freq ap = p.freq();
        if (null == value)
        {
            return 0;
        }
        if (Freq.TOTAL == ap)
        {
            double result = 0;
            final int c = eduOrgUnit.getEducationOrgUnit().getDevelopPeriod().getLastCourse();
            for (int i = 0; i < c; i++)
            {
                result += UniscIODao.safe(value, i);
            }
            return UniscEduOrgUnitConfigDAO.unwrap(result);
        } else if (Freq.YEAR == ap)
        {
            return UniscEduOrgUnitConfigDAO.unwrap(UniscIODao.safe(value, stage));
        } else if (Freq.TERM == ap)
        {
            final int countTerm = eduOrgUnit.getCountTerm();
            if (countTerm <= 0)
            {
                return 0;
            }
            final int course = ((stage / countTerm));
            return UniscEduOrgUnitConfigDAO.unwrap(UniscIODao.safe(value, course) / countTerm);
        } else if (Freq.HALF_TERM == ap)
        {
            final int countTerm = eduOrgUnit.getCountTerm();
            if (countTerm <= 0)
            {
                return 0;
            }
            final int course = ((stage / (2 * countTerm)));
            return UniscEduOrgUnitConfigDAO.unwrap(UniscIODao.safe(value, course) / (2 * countTerm));
        }
        return 0;
    }

    /* REIMPLEMENT IT */
    protected Date date(final Block block, final UniscPayPeriod p, final UniscEduOrgUnit eduOrgUnit, final int stage)
    {
        final Freq ap = p.freq();
        final int year = eduOrgUnit.getEducationYear().getIntValue();
        if (Freq.TOTAL == ap)
        {
            final Calendar c = Calendar.getInstance();
            c.set(year, Calendar.SEPTEMBER, 1);
            return c.getTime();
        } else if (Freq.YEAR == ap)
        {
            final Calendar c = Calendar.getInstance();
            c.set(year, Calendar.SEPTEMBER, 1);
            return c.getTime();
        } else if (Freq.TERM == ap)
        {
            final Calendar c = Calendar.getInstance();
            c.set(year, Calendar.SEPTEMBER, 1);
            c.add(Calendar.MONTH, ((ap.termNumber(eduOrgUnit, stage) - 1) * 10) / eduOrgUnit.getCountTerm());
            return c.getTime();
        } else if (Freq.HALF_TERM == ap)
        {
            final Calendar c = Calendar.getInstance();
            c.set(year, Calendar.SEPTEMBER, 1);
            c.add(Calendar.MONTH, ((ap.termNumber(eduOrgUnit, stage) - 1) * 10) / eduOrgUnit.getCountTerm());
            c.add(Calendar.MONTH, ((ap.partNumber(eduOrgUnit, stage) - 1) * 10) / (2 * eduOrgUnit.getCountTerm()));
            return c.getTime();
        }
        final Calendar c = Calendar.getInstance();
        c.set(year, Calendar.SEPTEMBER, 1);
        return c.getTime();
    }


    @Override
    public Date getDate(final UniscEduOrgUnit uniscEduOrgUnit, final UniscPayPeriod p, final int stage)
    {
        final String key = this.getBlockIdentifier(uniscEduOrgUnit.getEducationOrgUnit());
        final Block block = UniscIODao.this.createBlock(key);
        return this.date(block, p, uniscEduOrgUnit, stage);
    }

    protected static double safe(final Double[] d, final int i)
    {
        if ((null != d) && (i < d.length))
        {
            final Double dd = d[i];
            if (null == dd)
            {
                return 0;
            }
            return dd;
        }
        return 0;
    }

    protected static Double nextDouble(final Iterator<String> data)
    {
        if (!data.hasNext())
        {
            return null;
        }
        final String text = data.next();
        if (text.isEmpty())
        {
            return null;
        }
        return Double.valueOf(text);
    }

    protected Block block(final String identifier, final String[] COLUMNS, final String title)
    {
        return new Block()
        {
            private final Map<EducationOrgUnit, Map<String, Object[]>> rows = new LinkedHashMap<>();

            @Override
            public String getBlockIdentifier()
            {
                return identifier;
            }

            @Override
            public String getBlockTitle()
            {
                return title;
            }

            @Override
            public String[] getValueCaptions()
            {
                return COLUMNS;
            }

            @Override
            public Map<EducationOrgUnit, Map<String, Object[]>> getRows()
            {
                return this.rows;
            }
        };
    }

    protected Map<String, Block> getBlockMap()
    {
        return SafeMap.get(UniscIODao.this::createBlock);
    }


    @Override
    public List<Block> getExportData(final EducationYear year)
    {
        final Map<String, Block> blockMap = this.getBlockMap();
        final List<EducationOrgUnit> result = this.getSortedEducationOrgUnitList();
        for (final EducationOrgUnit edu : result)
        {
            final String blockIdentifier = this.getBlockIdentifier(edu);
            final Map<EducationOrgUnit, Map<String, Object[]>> rows = blockMap.get(blockIdentifier).getRows();

            // TODO: load current settings (depend on year)

            rows.put(edu, Collections.singletonMap("", UniscIODao.MARKER));
        }
        return new ArrayList<>(blockMap.values());
    }

    private List<EducationOrgUnit> getSortedEducationOrgUnitList()
    {
        final MQBuilder builder = new MQBuilder(EducationOrgUnitGen.ENTITY_CLASS, "edu");
        builder.addOrder("edu", EducationOrgUnitGen.L_EDUCATION_LEVEL_HIGH_SCHOOL + "." + ICatalogItem.CATALOG_ITEM_TITLE);
        builder.addOrder("edu", EducationOrgUnitGen.L_DEVELOP_FORM + ".code");
        builder.addOrder("edu", EducationOrgUnitGen.L_DEVELOP_CONDITION + ".code");
        builder.addOrder("edu", EducationOrgUnitGen.L_DEVELOP_TECH + ".code");
        builder.addOrder("edu", EducationOrgUnitGen.L_DEVELOP_PERIOD + ".code");
        builder.addOrder("edu", EducationOrgUnitGen.formativeOrgUnit().title());
        builder.addOrder("edu", EducationOrgUnitGen.territorialOrgUnit().territorialTitle());
        return builder.getResultList(this.getSession());
    }

    @Override
    public UniscEduOrgUnitPayPlan doGetPayPlan(final UniscEduOrgUnit ou, final UniscPayPeriod freq)
    {
        synchronized (UniscIODao.MARKER)
        {
            UniscEduOrgUnitPayPlan p = (UniscEduOrgUnitPayPlan) getSession().createCriteria(UniscEduOrgUnitPayPlan.class)
                    .add(Restrictions.eq(UniscEduOrgUnitPayPlanGen.L_UNISC_EDU_ORG_UNIT, ou))
                    .add(Restrictions.eq(UniscEduOrgUnitPayPlanGen.L_UNISC_PAY_FREQ, freq))
                    .uniqueResult();
            if (null != p)
            {
                return p;
            }

            final Map<String, Block> blockMap = this.getBlockMap();
            final String blockIdentifier = UniscIODao.this.getBlockIdentifier(ou.getEducationOrgUnit());
            final Block block = blockMap.get(blockIdentifier);

            p = new UniscEduOrgUnitPayPlan();
            p.setUniscEduOrgUnit(ou);
            p.setUniscPayFreq(freq);
            save(p);

            final int count = freq.getStagesCount(ou);
            for (int i = 0; i < count; i++)
            {
                final UniscEduOrgUnitPayPlanRow row = new UniscEduOrgUnitPayPlanRow();
                row.setPlan(p);
                row.setStage(i);
                row.setDate(UniscIODao.this.date(block, freq, ou, row.getStage()));
                row.setCost(0);
                save(row);
            }

            return p;
        }
    }


    protected Object[] getInternalValue(final Block block, final Iterator<String> data)
    {
        boolean filled = false;
        final List<Double> values = new ArrayList<>();
        for (int i = 0; i < block.getValueCaptions().length; i++)
        {
            final Double value = UniscIODao.nextDouble(data);
            values.add(value);
            filled |= (null != value);
        }

        if (filled)
        {
            return new Object[]{values.toArray(new Double[values.size()])};
        }
        return null;
    }

    protected Double[] getCostFromInternalValue(final Object[] internalValue)
    {
        return (Double[]) internalValue[0];
    }

    protected Object[] patchAndApplyInternalValue(final UniscEduOrgUnit cfg, final Object[] internalValue)
    {
        return internalValue;
    }


    @Override
    public ImportFacade getImportFacade(final EducationYear year)
    {
        final Map<String, Block> blockMap = this.getBlockMap();
        final List<Object[]> eduOrgUnitHierarchy = this.getEduOrgUnitHierarchy();

        return new ImportFacade()
        {
            @Override
            public void add(final EducationOrgUnit edu, String cipher, final Iterator<String> data)
            {
                cipher = StringUtils.trimToEmpty(cipher);

                final String blockIdentifier = UniscIODao.this.getBlockIdentifier(edu);
                final Block block = blockMap.get(blockIdentifier);

                final Object[] value = getInternalValue(block, data);

                if (null != value)
                {
                    final Map<EducationOrgUnit, Map<String, Object[]>> rows = block.getRows();
                    Map<String, Object[]> ciphers = rows.get(edu);
                    if (null == ciphers)
                    {
                        rows.put(edu, ciphers = new HashMap<>());
                    }
                    ciphers.put(cipher, value);
                }
            }


            @Override
            public void doImport()
            {
                synchronized (UniscIODao.MARKER)
                {

                    final List<UniscPayPeriod> payPeriodList = UniscIODao.this.getCatalogItemList(UniscPayPeriod.class); /* before object loading (avoid proxy) */
                    final Map<UniscEduOrgUnit, Map<UniscPayPeriod, UniscEduOrgUnitPayPlan>> cfg2pay2plan = new HashMap<>();
                    /* берем все, что уже есть */
                    {
                        final MQBuilder builder = new MQBuilder(UniscEduOrgUnitPayPlanGen.ENTITY_CLASS, "e");
                        builder.add(MQExpression.eq("e", UniscEduOrgUnitPayPlanGen.L_UNISC_EDU_ORG_UNIT + "." + UniscEduOrgUnitGen.L_EDUCATION_YEAR + ".id", year.getId()));
                        final List<UniscEduOrgUnitPayPlan> plans = builder.getResultList(UniscIODao.this.getSession());
                        for (final UniscEduOrgUnitPayPlan plan : plans)
                        {
                            Map<UniscPayPeriod, UniscEduOrgUnitPayPlan> pay2plan = cfg2pay2plan.get(plan.getUniscEduOrgUnit());
                            if (null == pay2plan)
                            {
                                cfg2pay2plan.put(plan.getUniscEduOrgUnit(), pay2plan = new HashMap<>(4));
                            }
                            pay2plan.put(plan.getUniscPayFreq(), plan);
                        }
                    }

                    final Map<UniscEduOrgUnitPayPlan, Collection<UniscEduOrgUnitPayPlanRow>> plan2rows = new HashMap<>();
                    /* берем из базы все, что есть */
                    {
                        final MQBuilder builder = new MQBuilder(UniscEduOrgUnitPayPlanRowGen.ENTITY_CLASS, "e");
                        builder.add(MQExpression.eq("e", UniscEduOrgUnitPayPlanRowGen.L_PLAN + "." + UniscEduOrgUnitPayPlanGen.L_UNISC_EDU_ORG_UNIT + "." + UniscEduOrgUnitGen.L_EDUCATION_YEAR + ".id", year.getId()));
                        final List<UniscEduOrgUnitPayPlanRow> rows = builder.getResultList(getSession());
                        for (final UniscEduOrgUnitPayPlanRow row : rows)
                        {
                            Collection<UniscEduOrgUnitPayPlanRow> plans = plan2rows.get(row.getPlan());
                            if (null == plans)
                            {
                                plan2rows.put(row.getPlan(), plans = new ArrayList<>());
                            }
                            plans.add(row);
                        }
                    }


                    for (final Block block : blockMap.values())
                    {
                        final Map<EducationOrgUnit, Map<String, Object[]>> config = block.getRows();

                        /* за 5 проходов гарантированно заполнит. т.к. структура дерева не более 5и */
                        {
                            for (int i = 0; i < 5; i++)
                            {
                                for (final Object[] row : eduOrgUnitHierarchy)
                                {
                                    final EducationOrgUnit p = UniscIODao.this.get(EducationOrgUnit.class, (Long) row[0]);
                                    if (block.getBlockIdentifier().equals(UniscIODao.this.getBlockIdentifier(p)))
                                    {
                                        continue;
                                    }

                                    final EducationOrgUnit c = UniscIODao.this.get(EducationOrgUnit.class, (Long) row[1]);
                                    if (block.getBlockIdentifier().equals(UniscIODao.this.getBlockIdentifier(c)))
                                    {
                                        continue;
                                    }
                                    if (!p.getTerritorialOrgUnit().equals(c.getTerritorialOrgUnit()))
                                    {
                                        continue;
                                    }
                                    if (null == config.get(c))
                                    {
                                        config.put(c, config.get(p));
                                    }
                                }
                            }
                        }


                        for (final Map.Entry<EducationOrgUnit, Map<String, Object[]>> e0 : config.entrySet())
                        {
                            for (final Map.Entry<String, Object[]> e1 : e0.getValue().entrySet())
                            {
                                final UniscEduOrgUnit cfg = IUniscEduOrgUnitConfigDAO.INSTANCE.get().getUniscEduOrgUnit(year, e0.getKey(), e1.getKey());
                                final Object[] internalValue = patchAndApplyInternalValue(cfg, e1.getValue());

                                Map<UniscPayPeriod, UniscEduOrgUnitPayPlan> pay2plan = cfg2pay2plan.get(cfg);
                                if (null == pay2plan)
                                {
                                    cfg2pay2plan.put(cfg, pay2plan = new HashMap<>(4));
                                }

                                for (final UniscPayPeriod period : payPeriodList)
                                {
                                    if (null == pay2plan.get(period))
                                    {
                                        final UniscEduOrgUnitPayPlan p = new UniscEduOrgUnitPayPlan();
                                        p.setUniscEduOrgUnit(cfg);
                                        p.setUniscPayFreq(period);
                                        pay2plan.put(period, p);
                                    }
                                }

                                final Double[] cost = getCostFromInternalValue(internalValue);
                                for (final Map.Entry<UniscPayPeriod, UniscEduOrgUnitPayPlan> p : pay2plan.entrySet())
                                {

                                    if (null == p.getValue().getId())
                                    {
                                        UniscIODao.this.save(p.getValue());
                                        final int count = p.getKey().getStagesCount(cfg);
                                        for (int i = 0; i < count; i++)
                                        {
                                            final UniscEduOrgUnitPayPlanRow row = new UniscEduOrgUnitPayPlanRow();
                                            row.setPlan(p.getValue());
                                            row.setStage(i);
                                            row.setDate(UniscIODao.this.date(block, p.getKey(), cfg, row.getStage()));
                                            row.setCost(UniscIODao.this.cost(block, p.getKey(), cfg, row.getStage(), cost));
                                            UniscIODao.this.save(row);
                                        }

                                    } else
                                    {
                                        final Collection<UniscEduOrgUnitPayPlanRow> planRows = plan2rows.get(p.getValue());
                                        if (null == planRows)
                                        {
                                            final int count = p.getKey().getStagesCount(cfg);
                                            for (int i = 0; i < count; i++)
                                            {
                                                final UniscEduOrgUnitPayPlanRow row = new UniscEduOrgUnitPayPlanRow();
                                                row.setPlan(p.getValue());
                                                row.setStage(i);
                                                row.setDate(UniscIODao.this.date(block, p.getKey(), cfg, row.getStage()));
                                                row.setCost(UniscIODao.this.cost(block, p.getKey(), cfg, row.getStage(), cost));
                                                UniscIODao.this.save(row);
                                            }

                                        } else
                                        {
                                            final int count = p.getKey().getStagesCount(cfg);
                                            if (count <= 0)
                                            {
                                                for (final UniscEduOrgUnitPayPlanRow row : planRows)
                                                {
                                                    UniscIODao.this.getSession().delete(row);
                                                }

                                            } else
                                            {

                                                // рассчитываем суммы из настроек
                                                final long[] stage2cost_fl = new long[count];
                                                for (int i = 0; i < count; i++)
                                                {
                                                    stage2cost_fl[i] = UniscIODao.this.cost(block, p.getKey(), cfg, i, cost);
                                                }

                                                // рассчитываем суммы из базы
                                                final double[] stage2factor = new double[count];
                                                for (final UniscEduOrgUnitPayPlanRow row : planRows)
                                                {
                                                    final int stage = row.getStage();
                                                    if ((stage < 0) && (stage >= count))
                                                    {
                                                        continue;
                                                    }

                                                    stage2factor[stage] += row.getCost();
                                                }

                                                // рассчитываем множители (то, что требуется получить, делим на то, что было)
                                                for (int i = 0; i < count; i++)
                                                {
                                                    stage2factor[i] = stage2factor[i] > 0 ? stage2cost_fl[i] / stage2factor[i] : 0;
                                                }

                                                // меняем стоимость в базе (пропорционально стоимости каждого этапа)
                                                for (final UniscEduOrgUnitPayPlanRow row : planRows)
                                                {
                                                    final int stage = row.getStage();
                                                    if ((stage < 0) && (stage >= count))
                                                    {
                                                        continue;
                                                    }

                                                    if (stage2cost_fl[stage] > 0)
                                                    {
                                                        row.setCost(Math.round(row.getCost() * stage2factor[stage]));
                                                        stage2cost_fl[stage] -= row.getCost();
                                                    } else
                                                    {
                                                        row.setCost(0);
                                                    }
                                                    UniscIODao.this.getSession().saveOrUpdate(row);
                                                }

                                                // распределяем остатки
                                                for (final UniscEduOrgUnitPayPlanRow row : planRows)
                                                {
                                                    final int stage = row.getStage();
                                                    if ((stage < 0) && (stage >= count))
                                                    {
                                                        continue;
                                                    }

                                                    if (stage2cost_fl[stage] > 0)
                                                    {
                                                        row.setCost(row.getCost() + stage2cost_fl[stage]);
                                                        stage2cost_fl[stage] = 0;
                                                    }
                                                    UniscIODao.this.getSession().saveOrUpdate(row);
                                                }

                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        };
    }


    /* List { pid, cid } */
    private List<Object[]> getEduOrgUnitHierarchy()
    {
        final String[] properties = {
                EducationOrgUnitGen.L_FORMATIVE_ORG_UNIT,
                EducationOrgUnitGen.L_DEVELOP_FORM,
                EducationOrgUnitGen.L_DEVELOP_CONDITION,
                EducationOrgUnitGen.L_DEVELOP_TECH,
                EducationOrgUnitGen.L_DEVELOP_PERIOD
        };
        final MQBuilder builder = new MQBuilder(EducationOrgUnitGen.ENTITY_CLASS, "p", new String[]{"id"});
        builder.addOrder("p", "id");

        builder.addDomain("c", EducationOrgUnitGen.ENTITY_CLASS).addSelect("c", new String[]{"id"});
        builder.addOrder("c", "id");

        /* совпадение свйоств */
        {
            for (final String p : properties)
            {
                builder.add(MQExpression.eqProperty("p", p, "c", p));
            }
        }

        /* иерархия направлений ВПО */
        {
            final String prefix = EducationOrgUnitGen.L_EDUCATION_LEVEL_HIGH_SCHOOL + "." + EducationLevelsHighSchoolGen.L_EDUCATION_LEVEL;
            builder.add(MQExpression.eqProperty("c", prefix + "." + EducationLevelsGen.L_PARENT_LEVEL, "p", prefix));
        }

        return builder.getResultList(this.getSession());
    }


}
