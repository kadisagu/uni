package ru.tandemservice.unisc.component.settings.FinesSettings.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;


public class Controller extends AbstractBusinessController<IDAO, Model> {

	@Override
	public void onRefreshComponent(IBusinessComponent component) {
		final Model model = getModel(component);
		getDao().prepare(model);
	}
	
	public void onClickApply(IBusinessComponent component) {
		final Model model = getModel(component);
		getDao().save(model);
		deactivate(component);
	}

	
}
