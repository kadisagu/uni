package ru.tandemservice.unisc.component.agreement.AgreementPaymentAddEdit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IdentifiableWrapper;

import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayFactRow;
import ru.tandemservice.unisc.entity.catalog.UniscPaymentDocumentType;

/**
 * @author vdanilov
 */
@Input({@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "id")})
@SuppressWarnings("unchecked")
public class Model
{
    public static final Long J_PAYER_ID = 0L;
    public static final Long N_PAYER_ID = 1L;

    private Long id;

    private UniscEduAgreementPayFactRow row;
    private double sum;
    private IdentifiableWrapper payerType;
    private IdentifiableWrapper payer;

    private List<UniscPaymentDocumentType> types = Collections.emptyList();
    private List<IdentifiableWrapper> payerTypeList = new ArrayList<IdentifiableWrapper>();
    private List<IdentifiableWrapper> jPayerList = new ArrayList<IdentifiableWrapper>();
    private List<IdentifiableWrapper> nPayerList = new ArrayList<IdentifiableWrapper>();

    public boolean isAddForm()
    {
        return row.getId() == null;
    }

    public List<IdentifiableWrapper> getPayerList()
    {
        if (null == getPayerType())
            return new ArrayList<IdentifiableWrapper>();
        if (J_PAYER_ID.equals(getPayerType().getId()))
            return getJPayerList();
        if (N_PAYER_ID.equals(getPayerType().getId()))
            return getNPayerList();
        return new ArrayList<IdentifiableWrapper>();
    }

    // accessors

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public List<UniscPaymentDocumentType> getTypes()
    {
        return types;
    }

    public void setTypes(List<UniscPaymentDocumentType> types)
    {
        this.types = types;
    }

    public UniscEduAgreementPayFactRow getRow()
    {
        return row;
    }

    public void setRow(UniscEduAgreementPayFactRow row)
    {
        this.row = row;
    }

    public double getSum()
    {
        return sum;
    }

    public void setSum(double sum)
    {
        this.sum = sum;
    }

    public IdentifiableWrapper getPayerType()
    {
        return payerType;
    }

    public void setPayerType(IdentifiableWrapper payerType)
    {
        this.payerType = payerType;
    }

    public IdentifiableWrapper getPayer()
    {
        return payer;
    }

    public void setPayer(IdentifiableWrapper payer)
    {
        this.payer = payer;
    }

    public List<IdentifiableWrapper> getPayerTypeList()
    {
        return payerTypeList;
    }

    public void setPayerTypeList(List<IdentifiableWrapper> payerTypeList)
    {
        this.payerTypeList = payerTypeList;
    }

    public List<IdentifiableWrapper> getJPayerList()
    {
        return jPayerList;
    }

    public void setJPayerList(List<IdentifiableWrapper> jPayerList)
    {
        this.jPayerList = jPayerList;
    }

    public List<IdentifiableWrapper> getNPayerList()
    {
        return nPayerList;
    }

    public void setNPayerList(List<IdentifiableWrapper> nPayerList)
    {
        this.nPayerList = nPayerList;
    }

    public String getAlert()
    {
        preparePayment();

        // todo

        return null;
    }

    public void preparePayment()
    {
        if (getPayerType() == null)
            return;

        if (J_PAYER_ID.equals(getPayerType().getId()))
            getRow().setJCostAsDouble(getSum());
        else if (Model.N_PAYER_ID.equals(getPayerType().getId()))
            getRow().setNCostAsDouble(getSum());

        if (getPayer() == null)
            return;

        getRow().setPayer(getPayer().getTitle());
    }
}
