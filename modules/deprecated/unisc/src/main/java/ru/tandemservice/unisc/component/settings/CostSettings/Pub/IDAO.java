package ru.tandemservice.unisc.component.settings.CostSettings.Pub;

import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {
	void doRecalculate(Model model);
}
