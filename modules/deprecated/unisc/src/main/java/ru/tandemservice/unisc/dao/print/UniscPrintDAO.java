package ru.tandemservice.unisc.dao.print;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.jfree.util.Log;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.util.SafeSimpleDateFormat;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.NumberSpellingUtil;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.codes.RelationDegreeCodes;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.AcademyData;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unictr.entity.contractor.ContactPerson;
import ru.tandemservice.unictr.entity.contractor.Contractor;
import ru.tandemservice.unisc.UniscDefines;
import ru.tandemservice.unisc.dao.agreement.IUniscEduAgreementDAO;
import ru.tandemservice.unisc.entity.agreements.*;
import ru.tandemservice.unisc.entity.catalog.UniscCustomerType;
import ru.tandemservice.unisc.entity.catalog.UniscPayPeriod.Freq;
import ru.tandemservice.unisc.entity.catalog.UniscTemplateDocument;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;
import ru.tandemservice.unisc.utils.UniscPropertyUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * @author vdanilov
 */
@SuppressWarnings("deprecation")
public class UniscPrintDAO extends UniBaseDao implements IUniscPrintDAO {

    protected static final SafeSimpleDateFormat DD_DATE = new SafeSimpleDateFormat("dd");
    protected static final SafeSimpleDateFormat MM_DATE = new SafeSimpleDateFormat("yy");


    @Override
    public UniscEduAgreementPrintVersion getLastPrintVersion(final UniscEduAgreementBase agreementBase) {
        if (null == agreementBase.getId()) { return null; }
        return (UniscEduAgreementPrintVersion) getSession()
        .createCriteria(UniscEduAgreementPrintVersion.class)
        .add(Restrictions.eq(UniscEduAgreementPrintVersion.L_OWNER, agreementBase))
        .addOrder(Order.desc(UniscEduAgreementPrintVersion.P_TIMESTAMP))
        .setMaxResults(1)
        .uniqueResult();
    }

    @Override
    public RtfInjectModifier getRtfInjectorModifier(final UniscEduAgreementBase agreementBase, final Person person) {
        final DateFormatter defaultDateFormatter = DateFormatter.DEFAULT_DATE_FORMATTER;
        final RtfInjectModifier modifier = new RtfInjectModifier();

        final TopOrgUnit academy = TopOrgUnit.getInstance(true);
        EducationOrgUnit educationOrgUnit = agreementBase.getConfig().getEducationOrgUnit();
        OrgUnit formativeOrgUnit = educationOrgUnit.getFormativeOrgUnit();
        OrgUnit territorialOrgUnit = educationOrgUnit.getTerritorialOrgUnit();

        StringBuilder formativeOrgUnitStr = new StringBuilder();
        formativeOrgUnitStr.append(formativeOrgUnit.getNominativeCaseTitle() != null ? formativeOrgUnit.getNominativeCaseTitle() : formativeOrgUnit.getTitle());
        formativeOrgUnitStr.append(" (");
        if (territorialOrgUnit instanceof TopOrgUnit)
            formativeOrgUnitStr.append(territorialOrgUnit.getTerritorialTitle());
        else
            formativeOrgUnitStr.append(territorialOrgUnit.getNominativeCaseTitle() != null ? territorialOrgUnit.getNominativeCaseTitle() : territorialOrgUnit.getTitle());
        formativeOrgUnitStr.append(")");

        modifier.put("academyTitle", StringUtils.trimToEmpty(academy.getTitle()));
        modifier.put("academyShortTitle", StringUtils.trimToEmpty(academy.getShortTitle()));
        modifier.put("academyFullTitle", StringUtils.trimToEmpty(academy.getFullTitle()));
        modifier.put("academyCertificateTitle", StringUtils.trimToEmpty(AcademyData.getInstance().getCertificateTitle()));
        modifier.put("academyBankTitle", StringUtils.trimToEmpty(academy.getBank()));
        modifier.put("academyINN", StringUtils.trimToEmpty(academy.getInn()));
        modifier.put("academyKPP", StringUtils.trimToEmpty(academy.getKpp()));
        modifier.put("academyBIC", StringUtils.trimToEmpty(academy.getBic()));
        modifier.put("academyOGRN", StringUtils.trimToEmpty(academy.getOgrn()));
        modifier.put("academyOKATO", StringUtils.trimToEmpty(academy.getOkato()));
        modifier.put("academyCurAcc", StringUtils.trimToEmpty(academy.getCurAccount()));
        modifier.put("academyCorAcc", StringUtils.trimToEmpty(academy.getCorAccount()));
        modifier.put("academyPayee", StringUtils.trimToEmpty(academy.getPayee()));
        modifier.put("academyPhones", StringUtils.trimToEmpty(academy.getPhone()));
        modifier.put("academyAddress", StringUtils.trimToEmpty((null != academy.getLegalAddress() ? getAddressTitle(academy.getLegalAddress(), false) : getAddressTitle(academy.getAddress(), false))));
        modifier.put("formativeOrgUnitStr", formativeOrgUnitStr.toString());

        patchAgreementNumber(agreementBase, modifier);
        patchAgreementDates(agreementBase, modifier);
        patchAgreementEducationOrgUnit(agreementBase, modifier);
        patchAgreementGosUPDuration(agreementBase, modifier);

        modifier.put("agreementConfigDescription", StringUtils.trimToEmpty(agreementBase.getConfig().getDescription()));
        modifier.put("agreementConfigConditions", StringUtils.trimToEmpty(agreementBase.getConfig().getConditions()));

        patchCustomer(agreementBase, modifier);
        patchStudentData(person, modifier);

        final UniscEduOrgUnit uniscEduOrgUnit = agreementBase.getConfig();

        final Freq ap = agreementBase.getPayPlanFreq().freq();
        modifier.put("course", String.valueOf(ap.courseNumber(uniscEduOrgUnit, agreementBase.getFirstStage())));
        modifier.put("startEducationCourse", String.valueOf(ap.courseNumber(uniscEduOrgUnit, agreementBase.getFirstStage())));

        final int eduYear = uniscEduOrgUnit.getEducationYear().getIntValue();
        modifier.put("educationYear", String.valueOf(eduYear) + "/" + String.valueOf(1+eduYear));

        final List<UniscEduAgreementPayPlanRow> rows = IUniscEduAgreementDAO.INSTANCE.get().getAgreementPayPlanRows(agreementBase, false);

        modifier.put("chargeFirstPeriodDate", "");

        modifier.put("chargeFirstPeriodRate", "");
        modifier.put("chargeFirstPeriodRateKopeeks", "");
        modifier.put("chargeFirstPeriodRateInWords", "");

        modifier.put("chargeFirstPeriodNRate", "");
        modifier.put("chargeFirstPeriodNRateKopeeks", "");
        modifier.put("chargeFirstPeriodNRateInWords", "");

        modifier.put("chargeFirstPeriodJRate", "");
        modifier.put("chargeFirstPeriodJRateKopeeks", "");
        modifier.put("chargeFirstPeriodJRateInWords", "");

        if ((null != rows) && !rows.isEmpty())
        {
            int stagesToInculde = 0;
            if(UniscDefines.PAY_PERIOD_YEAR.equals(agreementBase.getAgreement().getPayPlanFreq().getCode()))
                stagesToInculde = 1;
            else if(UniscDefines.PAY_PERIOD_TERM.equals(agreementBase.getAgreement().getPayPlanFreq().getCode()))
                stagesToInculde = 2;
            else if(UniscDefines.PAY_PERIOD_HALF_TERM.equals(agreementBase.getAgreement().getPayPlanFreq().getCode()))
                stagesToInculde = 4;
            else if(UniscDefines.PAY_PERIOD_TOTAL.equals(agreementBase.getAgreement().getPayPlanFreq().getCode()))
                stagesToInculde = rows.size();

            int includedPayStages = 0;
            Double chargeFirstYearPeriodNRate = 0d;
            Double chargeFirstYearPeriodJRate = 0d;
            for(UniscEduAgreementPayPlanRow payRow : rows)
            {
                if(includedPayStages++ < stagesToInculde)
                {
                    chargeFirstYearPeriodNRate += payRow.getNCostAsDoubleDiscounted();
                    chargeFirstYearPeriodJRate += payRow.getJCostAsDoubleDiscounted();
                }
            }
            patchRate("chargeFirstYearNRate", chargeFirstYearPeriodNRate, modifier);
            patchRate("chargeFirstYearJRate", chargeFirstYearPeriodJRate, modifier);
            patchRate("chargeFirstYearRate", (chargeFirstYearPeriodNRate + chargeFirstYearPeriodJRate), modifier);
            modifier.put("chargeFirstYearNRateInWords", NumberSpellingUtil.spellNumberMasculineGender(chargeFirstYearPeriodNRate.longValue()));
            modifier.put("chargeFirstYearJRateInWords", NumberSpellingUtil.spellNumberMasculineGender(chargeFirstYearPeriodJRate.longValue()));
            modifier.put("chargeFirstYearRateInWords",  NumberSpellingUtil.spellNumberMasculineGender(Double.valueOf(chargeFirstYearPeriodNRate + chargeFirstYearPeriodJRate).longValue()));

            final UniscEduAgreementPayPlanRow row = rows.get(0);
            modifier.put("chargeFirstPeriodDate", defaultDateFormatter.format(row.getDate()));

            if (row.getNCostAsDouble() > 0) { patchRate("chargeFirstPeriodNRate", row.getNCostAsDoubleDiscounted(), modifier); }
            if (row.getJCostAsDouble() > 0) { patchRate("chargeFirstPeriodJRate", row.getJCostAsDoubleDiscounted(), modifier); }
            patchRate("chargeFirstPeriodRate", (row.getNCostAsDoubleDiscounted() + row.getJCostAsDoubleDiscounted()), modifier);
        }

        patchRate("chargeRate", this.getCost(agreementBase, rows), modifier);
        patchRate("chargeYearRate", this.getCourseCost(agreementBase, rows), modifier);
        patchRate("chargeTotalRate", this.getTotalCost(agreementBase, rows), modifier);

        return modifier;
    }

    protected String getAddressTitle(final AddressBase address, final boolean withFlat) {
        return (null == address ? "" : (withFlat ? address.getTitleWithFlat() : address.getTitle()));
    }

    protected void patchRate(final String prefix, final double cost_d, final RtfInjectModifier modifier) {
        modifier.put(prefix, Integer.toString((int)cost_d));
        modifier.put(prefix+"Kopeeks", StringUtils.leftPad(Integer.toString((int)(100.0d*cost_d - 100.0d*((int)cost_d))), 2, '0'));
        modifier.put(prefix+"InWords", NumberSpellingUtil.spellNumberMasculineGender((long) cost_d));
    }

    protected void patchAgreementEducationOrgUnit(final UniscEduAgreementBase agreementBase, final RtfInjectModifier modifier) {
        final UniscEduOrgUnit uniscEduOrgUnit = agreementBase.getConfig();
        final EducationOrgUnit educationOrgUnit = uniscEduOrgUnit.getEducationOrgUnit();
        final OrgUnit formativeOrgUnit = educationOrgUnit.getFormativeOrgUnit();
        final EducationLevelsHighSchool educationLevelHighSchool = educationOrgUnit.getEducationLevelHighSchool();
        final String educationLevelHighSchoolTitle = educationLevelHighSchool.getPrintTitle();

        modifier.put("studentInfo", UniStringUtils.join(educationLevelHighSchoolTitle, formativeOrgUnit.getTitle()));
        modifier.put("studentSpecialityType", educationLevelHighSchool.getEducationLevel().getLevelType().getDativeCaseShortTitle());
        modifier.put("studentSpeciality", educationLevelHighSchoolTitle);
        modifier.put("studentSpecialityCipher", uniscEduOrgUnit.getCipher());
        modifier.put("studentSpecialityLevelTypeTitle", educationLevelHighSchool.getTitleWithProfile());

        fillQualificationRelated( modifier, educationLevelHighSchool.getEducationLevel().getQualification());

        DevelopForm developForm = educationOrgUnit.getDevelopForm();
        modifier.put("studentEducationForm", getDevelopForm(developForm, InflectorVariantCodes.RU_DATIVE));
        modifier.put("studentEducationFormIm", getDevelopForm(developForm, InflectorVariantCodes.RU_NOMINATIVE));
        modifier.put("studentEducationFormP", getDevelopForm(developForm, InflectorVariantCodes.RU_PREPOSITION));

        modifier.put("studentSubdivision", (!StringUtils.isEmpty(formativeOrgUnit.getNominativeCaseTitle())) ? formativeOrgUnit.getNominativeCaseTitle() : formativeOrgUnit.getFullTitle());
        modifier.put("studentSubdivision_G", (!StringUtils.isEmpty(formativeOrgUnit.getGenitiveCaseTitle())) ? formativeOrgUnit.getGenitiveCaseTitle() : formativeOrgUnit.getFullTitle());

        modifier.put("studentEducationConditionIm", educationOrgUnit.getDevelopCondition().getTitle().toLowerCase());

        if(educationLevelHighSchool.getEducationLevel().getLevelType().isHigh())
        {
            modifier.put("eduType_G", "высшего");
            modifier.put("eduType_P", "высшем");
        }
        else if(educationLevelHighSchool.getEducationLevel().getLevelType().isMiddle())
        {
            modifier.put("eduType_G", "среднего");
            modifier.put("eduType_P", "среднем");
        }
        else
        {
            modifier.put("eduType_G", "");
            modifier.put("eduType_P", "");
        }
    }

    protected void fillQualificationRelated(RtfInjectModifier modifier, Qualifications qualification)
    {
        String studentQualification = "";
        String studentQualificationWord = "";
        String eduProgramKind = "";

        if(qualification != null)
        {
            switch (qualification.getCode()){
                case QualificationsCodes.BAKALAVR:
                    studentQualification = "бакалавриат";
                    studentQualificationWord = "бакалавра";
                    eduProgramKind = "бакалавриата";
                    break;
                case QualificationsCodes.SPETSIALIST:
                    studentQualification = "специалитет";
                    studentQualificationWord = "специалиста";
                    eduProgramKind = "высшего образования";
                    break;
                case QualificationsCodes.BAZOVYY_UROVEN_S_P_O:
                case QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O:
                    studentQualification = "специалист среднего звена";
                    studentQualificationWord = "специалиста среднего звена";
                    eduProgramKind = "среднего профессионального образования";
                    break;
                case QualificationsCodes.MAGISTR:
                    studentQualification = "магистратура";
                    studentQualificationWord = "магистра";
                    eduProgramKind = "магистратуры";
                    break;
                case QualificationsCodes.ASPIRANTURA:
                    studentQualification = "аспирантура";
                    studentQualificationWord = "аспиранта";
                    eduProgramKind = "подготовки научно-педагогических кадров в аспирантуре";
                    break;
            }
        }

        modifier.put("studentQualification", studentQualification);
        modifier.put("studentQualificationWord", studentQualificationWord);
        modifier.put("eduProgramKind", eduProgramKind);
    }

    protected String getDevelopForm(DevelopForm developForm, String inflectorVariantCode)
    {
        switch (inflectorVariantCode)
        {
            case InflectorVariantCodes.RU_DATIVE:
            {
                switch (developForm.getCode())
                {
                    case DevelopFormCodes.FULL_TIME_FORM:
                        return "очной";
                    case DevelopFormCodes.CORESP_FORM:
                        return "заочной";
                    case DevelopFormCodes.PART_TIME_FORM:
                        return "очно-заочной";
                }
                break;
            }

            case InflectorVariantCodes.RU_PREPOSITION:
            {
                switch (developForm.getCode())
                {
                    case DevelopFormCodes.FULL_TIME_FORM:
                        return "очном";
                    case DevelopFormCodes.CORESP_FORM:
                        return "заочном";
                    case DevelopFormCodes.PART_TIME_FORM:
                        return "очно-заочном";
                }
                break;
            }

            case InflectorVariantCodes.RU_NOMINATIVE:
            {
                switch (developForm.getCode())
                {
                    case DevelopFormCodes.FULL_TIME_FORM:
                        return "очная";
                    case DevelopFormCodes.CORESP_FORM:
                        return "заочная";
                    case DevelopFormCodes.PART_TIME_FORM:
                        return "очно-заочная";
                    default: return developForm.getTitle().toLowerCase();
                }
            }
        }

        return developForm.getTitle();
    }

    protected void patchAgreementGosUPDuration(final UniscEduAgreementBase agreementBase, final RtfInjectModifier modifier) {
        modifier.put("apprenticeshipGOS", agreementBase.getDevelopPeriodGos().getTitle());
        modifier.put("apprenticeshipUP", agreementBase.getDevelopPeriodEduPlan().getTitle());
    }

    protected String commaIfNotEmpty(String value) {
        value = StringUtils.trimToEmpty(value);
        if (value.length() > 0) { value += ", "; }
        return value;
    }

    protected void patchCustomer(final UniscEduAgreementBase agreementBase, final RtfInjectModifier modifier) {
        modifier.put("customerInfoCaption", "");
        modifier.put("customerPhones", "");
        modifier.put("customerShortInfo", "");
        modifier.put("customerInfo", "");
        modifier.put("customerPlenipotentiaryPost", "");
        modifier.put("customerPlenipotentiaryFio", "");
        modifier.put("customerActivityGround", "личной инициативы");
        modifier.put("customerTitle", "");
        modifier.put("customerRequisitesBank", "");
        modifier.put("customerINNKPP", "");
        modifier.put("customerBIK", "");
        modifier.put("customerBankTitle", "");
        modifier.put("customerCorAcc", "");
        modifier.put("customerCurAcc", "");
        modifier.put("customerAddress", "");
        modifier.put("customerFax", "");

        final UniscCustomerType tp = agreementBase.getCustomerType();
        if (tp.hasJuridicalPart()) {
            modifier.put("customerInfoCaption", "Банковские реквизиты");

            final ContactPerson jp = agreementBase.getJuridicalPerson();
            if (null != jp) {
                final Contractor contractor = jp.getContractor();

                modifier.put("customerShortInfo",  commaIfNotEmpty(contractor.getTitle()));
                modifier.put("customerInfo",  commaIfNotEmpty(contractor.getTitle()));

                modifier.put("customerPlenipotentiaryPost", commaIfNotEmpty((null == jp.getPost() ? "" : jp.getPost().getTitle())));
                modifier.put("customerPlenipotentiaryFio", commaIfNotEmpty(jp.getFullFio()));

                modifier.put("customerActivityGround",  StringUtils.trimToEmpty(contractor.getGround()));
                modifier.put("customerTitle",  StringUtils.trimToEmpty(contractor.getTitle()));

                modifier.put("customerINNKPP",
                    "ИНН "+ StringUtils.leftPad(StringUtils.trimToEmpty(contractor.getInn()), 12) +
                    "/КПП "+ StringUtils.leftPad(StringUtils.trimToEmpty(contractor.getKpp()), 12)
                );
                modifier.put("customerBIK", "БИК " + StringUtils.leftPad(StringUtils.trimToEmpty(contractor.getBic()), 12));
                modifier.put("customerBankTitle",  StringUtils.trimToEmpty(contractor.getBank()));
                modifier.put("customerCorAcc", "к/с " + StringUtils.leftPad(StringUtils.trimToEmpty(contractor.getCorAccount()), 12) + ",");
                modifier.put("customerCurAcc", "р/с " + StringUtils.leftPad(StringUtils.trimToEmpty(contractor.getCurAccount()), 12));
                modifier.put("customerAddress", StringUtils.trimToEmpty((null != contractor.getLegalAddress() ? getAddressTitle(contractor.getLegalAddress(), false) : getAddressTitle(contractor.getAddress(), false))));
                modifier.put("customerPhones", StringUtils.trimToEmpty(jp.getAllPhones()));
                modifier.put("customerFax", StringUtils.trimToEmpty(contractor.getFax()));
            }

        } else if (tp.hasNaturalPart()) {
            modifier.put("customerInfoCaption", "Паспорт");

            final List<UniscEduAgreementNaturalPerson> persons = IUniscEduAgreementDAO.INSTANCE.get().getNaturalPersons(agreementBase);
            if (!persons.isEmpty()) {
                UniscEduAgreementNaturalPerson np = persons.get(0); // взяли первого

                // если есть подписывающий - то берем его (иначе - первого)
                for (final UniscEduAgreementNaturalPerson t: persons) {
                    if (t.isSignInAgreement()) { np = t; break; }
                }

                patchCustomerNaturalPersonSigner(np, modifier);
            }
        }
    }



    protected void patchCustomerNaturalPersonSigner(final UniscEduAgreementNaturalPerson np, final RtfInjectModifier modifier) {
        final DateFormatter defaultDateFormatter = DateFormatter.DEFAULT_DATE_FORMATTER;
        modifier.put("customerShortInfo", StringUtils.trimToEmpty(np.getTitle()));
        final String passportTitle = StringUtils.trimToEmpty(np.getPasportTitle());

        modifier.put("customerInfo", StringUtils.trimToEmpty(np.getTitle()) + (passportTitle.isEmpty() ? "" : (" (" + passportTitle + ")")));
        modifier.put("customerTitle", StringUtils.trimToEmpty(np.getTitle()));
        modifier.put("customerAddress", StringUtils.trimToEmpty(getAddressTitle(np.getAddress(), true)));
        modifier.put("customerINNKPP", passportTitle);
        if (!np.getRelationDegree().getCode().equals(RelationDegreeCodes.ANOTHER))
            modifier.put("customerRelation", np.getRelationDegree().getTitle());
        modifier.put("customerPassportSeria", StringUtils.trimToEmpty(np.getPassportSeria()));
        modifier.put("customerPassportNumber", StringUtils.trimToEmpty(np.getPassportNumber()));
        modifier.put("customerPassportInfo", StringUtils.trimToEmpty(np.getPassportIssuancePlace()));
        modifier.put("customerPassportDate", defaultDateFormatter.format(np.getPassportIssuanceDate()));

        final Set<String> phones = new LinkedHashSet<String>();
        if (null != np.getAddress()) {
            phones.add(StringUtils.trimToEmpty(np.getMobilePhoneNumber()));
            phones.add(StringUtils.trimToEmpty(np.getHomePhoneNumber()));
        }
        phones.remove("");
        modifier.put("customerPhones", StringUtils.join(phones.iterator(), ", "));
    }


    protected void patchAgreementDates(final UniscEduAgreementBase agreementBase, final RtfInjectModifier modifier) {
        final SimpleDateFormat DD_DATE = new SimpleDateFormat("dd");
        final SimpleDateFormat MM_DATE = new SimpleDateFormat("yy");

        /* contract */ {
            final UniscEduMainAgreement c = agreementBase.getAgreement();
            final Date formingDate = c.getFormingDate();
            modifier.put("contractFormingDay", DD_DATE.format(formingDate));
            modifier.put("contractFormingMonth", RussianDateFormatUtils.getMonthName(formingDate, false));
            modifier.put("contractFormingYear", MM_DATE.format(formingDate));

            final Date deadlineDate = c.getDeadlineDate();
            if (null != deadlineDate) {
                modifier.put("contractPeriodValidityDate", DD_DATE.format(deadlineDate));
                modifier.put("contractPeriodValidityMonth", RussianDateFormatUtils.getMonthName(deadlineDate, false));
                modifier.put("contractPeriodValidityYear", MM_DATE.format(deadlineDate));
            } else {
                modifier.put("contractPeriodValidityDate", "");
                modifier.put("contractPeriodValidityMonth", "");
                modifier.put("contractPeriodValidityYear", "");
            }
        }

        final Date formingDate = agreementBase.getFormingDate();
        modifier.put("formingDay", DD_DATE.format(formingDate));
        modifier.put("formingMonth", RussianDateFormatUtils.getMonthName(formingDate, false));
        modifier.put("formingYear", MM_DATE.format(formingDate));

        final Date deadlineDate = agreementBase.getDeadlineDate();
        if (null != deadlineDate) {
            modifier.put("periodValidityDate", DD_DATE.format(deadlineDate));
            modifier.put("periodValidityMonth", RussianDateFormatUtils.getMonthName(deadlineDate, false));
            modifier.put("periodValidityYear", MM_DATE.format(deadlineDate));
        } else {
            modifier.put("periodValidityDate", "");
            modifier.put("periodValidityMonth", "");
            modifier.put("periodValidityYear", "");
        }
    }


    protected void patchStudentData(final Person person, final RtfInjectModifier modifier) {
        if (null != person) {
            DateFormatter defaultDateFormatter = DateFormatter.DEFAULT_DATE_FORMATTER;
            final IdentityCard card = person.getIdentityCard();
            modifier.put("studentFIO", card.getFullFio());

            {
                final GrammaCase rusCase = GrammaCase.GENITIVE;
                final boolean isMaleSex = card.getSex().isMale();
                final StringBuilder fio = new StringBuilder(PersonManager.instance().declinationDao().getDeclinationLastName(card.getLastName(), rusCase, isMaleSex));
                fio.append(" ").append(PersonManager.instance().declinationDao().getDeclinationFirstName(card.getFirstName(), rusCase, isMaleSex));
                if (StringUtils.isNotEmpty(card.getMiddleName())) {
                    fio.append(" ").append(PersonManager.instance().declinationDao().getDeclinationMiddleName(card.getMiddleName(), rusCase, isMaleSex));
                }
                modifier.put("studentGenitiveFIO", fio.toString());
            }

            modifier.put("studentPassport",
                UniStringUtils.join(card.getCardType().getShortTitle().toLowerCase(), card.getSeria(), card.getNumber()) +
                (((null == card.getIssuanceDate()) && (null == card.getIssuancePlace())) ? "" : UniStringUtils.join(" выдан", defaultDateFormatter.format(card.getIssuanceDate()), card.getIssuanceCode(), card.getIssuancePlace()))
            );

            modifier.put("studentRegaddress", StringUtils.trimToEmpty(null != person.getAddressRegistration() ? getAddressTitle(person.getAddressRegistration(), true) : (
            null != person.getAddress() ? getAddressTitle(person.getAddress(), true) : ""
            )));
            modifier.put("studentFactaddress", StringUtils.trimToEmpty(getAddressTitle(person.getAddress(), true)));

            modifier.put("studentBirthday", (null != card.getBirthDate() ? defaultDateFormatter.format(card.getBirthDate()) : ""));
            modifier.put("studentCitizen", (null != card.getCitizenship() ? card.getCitizenship().getFullTitle() : ""));
            modifier.put("studentCitizenGenitive", (null != card.getCitizenship() ? (UniDefines.CITIZENSHIP_NO.equals(card.getCitizenship()) ? "без гражданства" : getGenitiveCaseCountryStr(card.getCitizenship().getFullTitle())) : ""));

            modifier.put("studentSurname", StringUtils.trimToEmpty(card.getLastName()));
            modifier.put("studentName", StringUtils.trimToEmpty(card.getFirstName()));
            modifier.put("studentPatronymic", StringUtils.trimToEmpty(card.getMiddleName()));
            modifier.put("studentPassportType", StringUtils.trimToEmpty(card.getCardType().getTitle()));
            modifier.put("studentPassportSeria", StringUtils.trimToEmpty(card.getSeria()));
            modifier.put("studentPassportNumber", StringUtils.trimToEmpty(card.getNumber()));
            modifier.put("studentPassportInfo", StringUtils.trimToEmpty(card.getIssuancePlace()));
            modifier.put("studentPassportCode", StringUtils.trimToEmpty(card.getIssuanceCode()));
            modifier.put("studentPassportDate", (null != card.getIssuanceDate() ? defaultDateFormatter.format(card.getIssuanceDate()) : ""));

            modifier.put("studentNationality", null == card.getNationality() ? "" : StringUtils.trimToEmpty(card.getNationality().getTitle()));
        }
    }

    protected String getGenitiveCaseCountryStr(final String country) {
        if (country.equalsIgnoreCase("Республика Беларусь")) { return "Республики Беларусь"; }
        if (country.equalsIgnoreCase("Российская Федерация")) { return "Российской Федерации"; }
        return PersonManager.instance().declinationDao().getDeclinationFirstName(country, GrammaCase.GENITIVE, true);
    }

    protected void patchAgreementNumber(final UniscEduAgreementBase agreementBase, final RtfInjectModifier modifier) {
        modifier.put("agreementNumber", StringUtils.stripEnd(agreementBase.getNumber(), UniscPropertyUtils.RESERVED_POSTFIX));
        modifier.put("contractNumber", StringUtils.stripEnd(agreementBase.getAgreement().getNumber(), UniscPropertyUtils.RESERVED_POSTFIX));
    }

    // платеж за первый этап (целиком)
    protected double getCost(final UniscEduAgreementBase agreementBase, final List<UniscEduAgreementPayPlanRow> rows) {
        int stage = Integer.MAX_VALUE;
        double cost = 0;
        for (final UniscEduAgreementPayPlanRow row: rows) {
            if (stage > row.getStage()) {
                stage = row.getStage();
                cost = 0;
            }
            if (stage == row.getStage()) {
                cost += row.getCostAsDouble();
            }
        }
        return cost;
    }

    // платеж за первый год (целиком)
    protected double getCourseCost(final UniscEduAgreementBase agreementBase, final List<UniscEduAgreementPayPlanRow> rows) {
        int course = Integer.MAX_VALUE;
        double cost = 0;
        for (final UniscEduAgreementPayPlanRow row: rows) {
            final int c = agreementBase.getPayPlanFreq().getCourseNumber(agreementBase.getConfig(), row.getStage());
            if (course > c) {
                course = c;
                cost = 0;
            }
            if (course == c) {
                cost += row.getCostAsDouble();
            }
        }
        return cost;
    }

    // платеж за все этапы этап (целиком)
    protected double getTotalCost(final UniscEduAgreementBase agreementBase, final List<UniscEduAgreementPayPlanRow> rows) {
        double cost = 0;
        for (final UniscEduAgreementPayPlanRow row: rows) {
            cost += row.getCostAsDouble();
        }
        return cost;
    }


    @Override
    public RtfInjectModifier getRtfInjectorModifier4Quittance(final UniscEduAgreementPayPlanRow row, final Person person) {
        final RtfInjectModifier modifier = new RtfInjectModifier();
        final UniscEduAgreementBase agreementBase = row.getOwner();

        patchAgreementNumber(agreementBase, modifier);
        patchAgreementEducationOrgUnit(agreementBase, modifier);
        patchStudentData(person, modifier);

        final UniscEduOrgUnit uniscEduOrgUnit = agreementBase.getConfig();

        final Freq ap = agreementBase.getPayPlanFreq().freq();
        modifier.put("course", String.valueOf(ap.courseNumber(uniscEduOrgUnit, row.getStage())));
        modifier.put("startEducationCourse", String.valueOf(ap.courseNumber(uniscEduOrgUnit, agreementBase.getFirstStage())));

        patchRate("chargeNRate", row.getNCost() > 0 ? row.getNCostAsDoubleDiscounted() : 0d, modifier);
        patchRate("chargeJRate", row.getJCost() > 0 ? row.getJCostAsDoubleDiscounted() : 0d, modifier);
        patchRate("chargeRate", row.getCostAsDouble(), modifier);

        return modifier;
    }


    protected String getDocumentTemplateName4MainAgreement(final UniscEduMainAgreement agreementBase) {
        return "studentContract";
    }

    protected String getDocumentTemplateName4AdditAgreement(final UniscEduAdditAgreement agreementBase) {
        return "studentAdditionalAgreement";
    }

    protected String getDocumentTemplateName4Quittance(final UniscEduAgreementPayPlanRow row) {
        final UniscCustomerType tp = row.getOwner().getCustomerType();
        if (tp.hasNaturalPart() && tp.hasJuridicalPart()) {
            return "quittance-nj";
        }
        return "quittance";
    }

    protected String getDocumentTemplateName(final UniscEduAgreementBase agreementBase) {
        if (agreementBase instanceof UniscEduMainAgreement) {
            return getDocumentTemplateName4MainAgreement((UniscEduMainAgreement) agreementBase);
        } else if (agreementBase instanceof UniscEduAdditAgreement) {
            return getDocumentTemplateName4AdditAgreement((UniscEduAdditAgreement) agreementBase);
        }
        return "studentContract";
    }

    @Override
    public RtfDocument getDocumentTemplate(final UniscEduAgreementBase agreementBase) {
        final String name = getDocumentTemplateName(agreementBase);
        final UniscTemplateDocument catalogItem = getCatalogItem(agreementBase, name);
        return new RtfReader().read(catalogItem.getContent());
    }

    private UniscTemplateDocument getCatalogItem(final UniscEduAgreementBase agreementBase, final String name) {
        UniscTemplateDocument catalogItem = null;

        // год-курс
        try {
            final int year = agreementBase.getConfig().getEducationYear().getIntValue();
            final int course = agreementBase.getPayPlanFreq().getCourseNumber(agreementBase.getConfig(), agreementBase.getFirstStage());
            catalogItem = this.getCatalogItem(UniscTemplateDocument.class, name+"__"+year+"_"+course);
        } catch (final Throwable t) {
            Log.error(t);
        }
        if (null != catalogItem) { return catalogItem; }

        //        // год
        //        try {
        //            final int year = agreementBase.getConfig().getEducationYear().getIntValue();
        //            catalogItem = this.getCatalogItem(UniscTemplateDocument.class, name+"__"+year);
        //        } catch (final Throwable t) {
        //            Log.error(t);
        //        }
        //        if (null != catalogItem) { return catalogItem; }

        // стандартный шаблон
        return this.getCatalogItem(UniscTemplateDocument.class, name);
    }

    @Override
    public RtfDocument getDocumentTemplate4Quittance(final UniscEduAgreementPayPlanRow row) {
        final String name = getDocumentTemplateName4Quittance(row);
        return new RtfReader().read(this.getCatalogItem(UniscTemplateDocument.class, name).getContent());
    }

    @Override
    public String getBarcodeString(final UniscEduAgreementBase a) {
        final UniscEduMainAgreement agreement = a.getAgreement();
        final StringBuilder sb = new StringBuilder();
        sb.append("XXX");
        sb.append(StringUtils.leftPad(String.valueOf(agreement.getConfig().getEducationYear().getIntValue()), 4, "0"));
        sb.append(StringUtils.leftPad(agreement.getNumber(), 6, "0"));
        return sb.toString();
    }
}
