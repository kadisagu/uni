package ru.tandemservice.unisc.component.settings.EduOrgUnitCostEnvAdd;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisc.entity.catalog.UniscPayPeriod;

/**
 * @author Vasily Zhukov
 * @since 17.02.2011
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setEducationYearList(getList(EducationYear.class, EducationYear.P_TITLE));

        for (EducationYear educationYear : model.getEducationYearList())
            if (Boolean.TRUE.equals(educationYear.getCurrent()))
                model.setEducationYear(educationYear);

        model.setPayPeriodList(getList(UniscPayPeriod.class, UniscPayPeriod.P_CODE));

        model.setPayPeriod(model.getPayPeriodList().get(0));

        model.setStage(0);
    }
}
