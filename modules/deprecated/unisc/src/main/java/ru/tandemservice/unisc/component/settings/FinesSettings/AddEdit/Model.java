package ru.tandemservice.unisc.component.settings.FinesSettings.AddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;

import ru.tandemservice.unisc.entity.config.UniscFinesHistory;

@Input({
	@Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="fines.id")
})
public class Model {
	private UniscFinesHistory fines = new UniscFinesHistory();
	public UniscFinesHistory getFines() { return this.fines; }
	public void setFines(final UniscFinesHistory fines) { this.fines = fines; }

}
