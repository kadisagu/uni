/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisc;

/**
 * @author vdanilov
 */
public interface UniscDefines
{
	final String CATALOG_CUSTOMER_TYPE = "uniscCustomerType";
	final String CUSTOMER_TYPE_NATURAL_PERSON = "1";
	final String CUSTOMER_TYPE_JURIDICAL_PERSON = "2";
	final String CUSTOMER_TYPE_COMPLEX_PERSON = "3";

	final String CATALOG_UNISC_DOCUMENT_TEMPLATE = "uniscDocumentTemplate";

	final String CATALOG_PAY_PERIOD = "UniscPayPeriods";
	final String PAY_PERIOD_TOTAL = "0";
	final String PAY_PERIOD_YEAR = "1";
	final String PAY_PERIOD_TERM = "2";
	final String PAY_PERIOD_HALF_TERM = "3";

    final String TEMPLATE_PAYMENT_REPORT = "paymentReport";
    final String TEMPLATE_AGREEMENT_INCOME_REPORT = "agreementIncomeReport";
    final String TEMPLATE_AGREEMENT_INCOME_REPORT_BY_GROUP = "agreementIncomeReport_byGroup";
    final String TEMPLATE_PAY_DEBTORS_REPORT = "payDebtorsReport";
    final String TEMPLATE_STUDENTS_WITHOUT_CONTRACTS_REPORT = "studentsWithoutContractsReport";
}
