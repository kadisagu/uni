package ru.tandemservice.unisc.entity.agreements;

import java.util.ArrayList;
import java.util.Collection;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.sec.ISecLocalEntityOwner;

import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisc.dao.agreement.IUniscEduAgreementDAO;
import ru.tandemservice.unisc.entity.agreements.gen.UniscEduMainAgreementGen;

/**
 * Договор
 */
public class UniscEduMainAgreement extends UniscEduMainAgreementGen
     implements ISecLocalEntityOwner
{
	@Override public UniscEduMainAgreement getAgreement() { return this; }

	@Override public String getTitle() {
		return "Договор № " + this.getNumber();
	}

    @Override
    public Collection<IEntity> getSecLocalEntities()
    {   IUniscEduAgreement2PersonRole<PersonRole> relation = IUniscEduAgreementDAO.INSTANCE.get().getRelation(this);
        if (null == relation || !(relation.getTarget() instanceof Student)) return new ArrayList<IEntity>();
        return ((Student) relation.getTarget()).getSecLocalEntities();
    }
}
