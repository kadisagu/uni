package ru.tandemservice.unisc.utils;

/**
 * @author oleyba
 * @since 24.08.2009
 */
public class UniscOrgstructPermissionGenerator
{
    public static void main(String[] args)
    {
        //System.out.println("unisc.orgstruct.sec.xml");
        //System.out.println("\n\n\n");
        //List<OrgUnitDefines.IOrgUnitDescription> list = new ArrayList<OrgUnitDefines.IOrgUnitDescription>(OrgUnitDefines.getCode2description().values());
        //Collections.sort(list, new Comparator<OrgUnitDefines.IOrgUnitDescription>()
        //{
        //    @Override
        //    public int compare(OrgUnitDefines.IOrgUnitDescription o1, OrgUnitDefines.IOrgUnitDescription o2)
        //    {
        //        return o1.getTitle(0).compareTo(o2.getTitle(0));
        //    }
        //});
        //
        //for (OrgUnitDefines.IOrgUnitDescription description : list)
        //{
        //    String code = description.getCode();
        //
        //    System.out.println(
        //    "   <permission-group name=\""+code+"UniscReportPermissionGroup\" title=\"Отчеты модуля «Договоры и оплаты»\">\n" +
        //    "        <permission name=\"viewPaymentReportList_"+code+"\" title=\"Просмотр отчета «Отчет по оплатам»\"/>\n" +
        //    "        <permission name=\"viewAgreementIncomeReportList_"+code+"\" title=\"Просмотр отчета «Поступление средств по договорам»\"/>\n" +
        //    "        <permission name=\"viewPayDebtorsReport_"+code+"\" title=\"Просмотр отчета «Список должников по оплате»\"/>\n" +
        //    "        <permission name=\"viewAgreementIncomeSummaryReport_"+code+"\" title=\"Просмотр отчета «Сводка поступлений по договорам»\"/>\n" +        
        //    "    </permission-group>\n" +
        //    "    <group-relation permission-group-name=\""+code+"UniscReportPermissionGroup\" group-name=\""+code+"PermissionClassGroup\"/>\n" +
        //    "    <group-relation permission-group-name=\""+code+"UniscReportPermissionGroup\" group-name=\""+code+"LocalClassGroup\"/>\n" +
        //    "    <permission-group name=\"" + code + "ContractsTabPermissionGroup\" title=\"Вкладка «Договоры»\">\n" +
        //    "        <permission name=\"orgUnit_viewContractTab_" + code + "\" title=\"Просмотр\"/>\n" +
        //    "        <permission name=\"editCheckedBySignature_list_" + code + "\" title=\"Изменение флага «Проверен»\"/>\n" +
        //    "        <permission name=\"printContract_list_" + code + "\" title=\"Печать договора\"/>\n" +
        //    "        <permission name=\"printAllContracts_list_" + code + "\" title=\"Печать списка договоров\"/>\n" +
        //    "    </permission-group>\n" +
        //    "    <group-relation permission-group-name=\"" + code + "ContractsTabPermissionGroup\" group-name=\"" + code + "PermissionClassGroup\"/>\n" +
        //    "    <group-relation permission-group-name=\"" + code + "ContractsTabPermissionGroup\" group-name=\"" + code + "LocalClassGroup\"/>\n"
        //    );
        //}
    }
}

