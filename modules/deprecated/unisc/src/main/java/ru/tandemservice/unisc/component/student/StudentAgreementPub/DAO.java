package ru.tandemservice.unisc.component.student.StudentAgreementPub;

import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student;

public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setRelation(getNotNull(UniscEduAgreement2Student.class, model.getRelation().getId()));
        model.setSecModel(new CommonPostfixPermissionModel(model.getRelation().getPermissionContext() + "AgreementInfoTab"));
    }
}
