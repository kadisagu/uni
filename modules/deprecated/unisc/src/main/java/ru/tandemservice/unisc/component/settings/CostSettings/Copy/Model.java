package ru.tandemservice.unisc.component.settings.CostSettings.Copy;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;

import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uni.ui.SelectModel;

/**
 * @author vdanilov
 */
@State({
	@Bind(key="yearId", binding="source.id")
})
public class Model {

	private EducationYear source = new EducationYear();
	public EducationYear getSource() { return this.source; }
	public void setSource(final EducationYear source) { this.source = source; }

	private final SelectModel<EducationYear> educationYearModel = new SelectModel<EducationYear>();
	public SelectModel<EducationYear> getEducationYearModel() { return this.educationYearModel; }

}
