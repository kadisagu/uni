package ru.tandemservice.unisc.component.settings.CostSettings.EditPlan;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlanRow;

public class Controller extends AbstractBusinessController<IDAO, Model> {

	@Override
	public void onRefreshComponent(final IBusinessComponent component) {
		final Model model = getModel(component);
		getDao().prepare(model);

		if (null == model.getDataSource()) {
			final DynamicListDataSource<UniscEduOrgUnitPayPlanRow> ds = new DynamicListDataSource<>(component, component1 -> getDao().prepareDataSource(model));
			ds.addColumn(new SimpleColumn("Этап", "") {
				@Override public String getContent(final IEntity entity) {
					return model.getEduOrgUnitPayPlan().getUniscPayFreq().toString(model.getEduOrgUnit(), ((UniscEduOrgUnitPayPlanRow)entity).getStage());
				}
			}.setClickable(false).setOrderable(false).setMergeRowIdResolver(entity -> String.valueOf(((UniscEduOrgUnitPayPlanRow)entity).getStage())).setMergeRows(true));

			ds.addColumn(new BlockColumn<Void>("date", "Дата").setWidth(1).setClickable(false).setOrderable(false));
			ds.addColumn(new BlockColumn<Void>("cost", "Стоимость").setWidth(1).setClickable(false).setOrderable(false));

			ds.addColumn(new ActionColumn("Копировать", "clone", "onClickCloneRow"));
			ds.addColumn(new ActionColumn("Удалить", "delete", "onClickDeleteRow"));

			model.setDataSource(ds);
		}
	}

	public void onClickCloneRow(final IBusinessComponent component) {
		getDao().doCloneRow(getModel(component), (Long)component.getListenerParameter());
	}

	public void onClickDeleteRow(final IBusinessComponent component) {
		getDao().doDeleteRow(getModel(component), (Long)component.getListenerParameter());
	}

	public void onClickApply(final IBusinessComponent component) {
		final Model model = getModel(component);
		getDao().save(model);
		deactivate(component);
	}

}
