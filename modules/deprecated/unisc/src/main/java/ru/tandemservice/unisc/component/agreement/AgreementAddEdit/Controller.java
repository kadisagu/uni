package ru.tandemservice.unisc.component.agreement.AgreementAddEdit;

import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage.IStageResult;
import ru.tandemservice.unisc.dao.agreement.IUniscEduAgreementDAO;

/**
 * @author vdanilov
 */
@SuppressWarnings("deprecation")
public class Controller<D extends IDAO<M>, M extends Model> extends AbstractBusinessController<D, M> {

    public static String SCOPE = "stage";

    @Override
    public void onRefreshComponent(final IBusinessComponent component) {
        final M model = this.getModel(component);
        if (IStageResult.CLOSE == model.getResult()) {
            this.deactivate(component);
            return;
        }

        final boolean childComponentExists = ((null != component.getChildRegion(Controller.SCOPE)) && (null != component.getChildRegion(Controller.SCOPE).getActiveComponent()));

        if (!childComponentExists) {
            try {
                this.getDao().execute(model);
            } catch (final Throwable t) {
                model.getStageIterator().previous();
                //this.getDao().prepare(model);
                //createStageScope(component, model, model.getStageIterator().next());
                component.getDesktop().setRefreshScheduled(true);
                throw CoreExceptionUtils.getRuntimeException(t);
            }
        }


        this.getDao().prepare(model);
        if (!IUniscEduAgreementDAO.INSTANCE.get().isMutable(model.getAgreement())) {
            throw new ApplicationException("Изменение договора невозможно.");
        }


        if (!childComponentExists) {
            if (model.getStageIterator().hasNext()) {
                final String nextComponentName = model.getStageIterator().next();
                createStageScope(component, model, nextComponentName);
            } else {
                this.getDao().save(model);
                this.deactivate(component);
            }
        }
    }

    protected void createStageScope(final IBusinessComponent component, final M model, final String nextComponentName) {
        final ComponentActivator componentActivator = new ComponentActivator(
            nextComponentName, new ParametersMap()
            .add("agreement", model.getAgreement())
            .add("personRole", model.getPersonRole())
            .add("defaultNaturalPersonList", getDao().getDefaultNaturalPersonList(model))
        );
        componentActivator.setNewSession(false);
        componentActivator.setState(false);
        component.createChildRegion(Controller.SCOPE, componentActivator);
    }

}
