package ru.tandemservice.unisc.dao.agreement;

import java.util.Collection;

import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.unisc.entity.agreements.IUniscEduAgreement2PersonRole;

/**
 * позволяет определить personRole по договору (регистритуется в spring.xml)
 * @author vdanilov
 */
public interface IUniscEduAgreement2PersonRoleResolver {
    int priority();
    Collection<IUniscEduAgreement2PersonRole<PersonRole>> getRelations4Agreements(Collection<Long> mainAgreementIds);
    Collection<IUniscEduAgreement2PersonRole<PersonRole>> getRelations4PersonRole(Collection<Long> personRoleIds);
}