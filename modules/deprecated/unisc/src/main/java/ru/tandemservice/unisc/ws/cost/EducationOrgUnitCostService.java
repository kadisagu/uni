package ru.tandemservice.unisc.ws.cost;

import javax.jws.WebParam;
import javax.jws.WebService;

import ru.tandemservice.unisc.dao.config.IUniscEduOrgUnitConfigDAO;

/**
 * @author Vasily Zhukov
 * @since 17.02.2011
 */
@WebService
public class EducationOrgUnitCostService
{
    /**
     * @param educationYearTitle Название учебного года (ru.tandemservice.uniedu.catalog.entity.basic.EducationYear.title)
     * @param payPeriodCode      Код периода оплаты (ru.tandemservice.unisc.entity.catalog.UniscPayPeriod.code)
     * @param stagePeriod        Номер периода оплаты
     * @return информация о стоимостях обучения по направлениям подготовки подразделения с шифром и
     */
    public EducationOrgUnitCostEnvironment getEducationOrgUnitCost(
            @WebParam(name = "educationYearTitle") String educationYearTitle,
            @WebParam(name = "payPeriodCode") String payPeriodCode,
            @WebParam(name = "stagePeriod") int stagePeriod)
    {
        return IUniscEduOrgUnitConfigDAO.INSTANCE.get().getEducationOrgUnitCostEnvironment(educationYearTitle, payPeriodCode, stagePeriod);
    }
}
