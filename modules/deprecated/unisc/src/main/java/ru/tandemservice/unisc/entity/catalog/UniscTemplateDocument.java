package ru.tandemservice.unisc.entity.catalog;

import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import ru.tandemservice.unisc.entity.catalog.gen.UniscTemplateDocumentGen;

@SuppressWarnings("serial")
public class UniscTemplateDocument extends UniscTemplateDocumentGen implements ITemplateDocument
{
	@Override
	public byte[] getContent()
	{
		return CommonBaseUtil.getTemplateContent(this);
	}
}
