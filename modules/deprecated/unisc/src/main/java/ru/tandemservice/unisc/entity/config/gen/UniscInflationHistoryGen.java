package ru.tandemservice.unisc.entity.config.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unisc.entity.config.UniscInflationHistory;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Ставка инфляции
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniscInflationHistoryGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisc.entity.config.UniscInflationHistory";
    public static final String ENTITY_NAME = "uniscInflationHistory";
    public static final int VERSION_HASH = 257351738;
    private static IEntityMeta ENTITY_META;

    public static final String P_DATE = "date";
    public static final String P_TAX = "tax";

    private Date _date;     // дата изменения
    private double _tax;     // процент

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return дата изменения. Свойство не может быть null.
     */
    @NotNull
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date дата изменения. Свойство не может быть null.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return процент. Свойство не может быть null.
     */
    @NotNull
    public double getTax()
    {
        return _tax;
    }

    /**
     * @param tax процент. Свойство не может быть null.
     */
    public void setTax(double tax)
    {
        dirty(_tax, tax);
        _tax = tax;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UniscInflationHistoryGen)
        {
            setDate(((UniscInflationHistory)another).getDate());
            setTax(((UniscInflationHistory)another).getTax());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniscInflationHistoryGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniscInflationHistory.class;
        }

        public T newInstance()
        {
            return (T) new UniscInflationHistory();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "date":
                    return obj.getDate();
                case "tax":
                    return obj.getTax();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "tax":
                    obj.setTax((Double) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "date":
                        return true;
                case "tax":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "date":
                    return true;
                case "tax":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "date":
                    return Date.class;
                case "tax":
                    return Double.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniscInflationHistory> _dslPath = new Path<UniscInflationHistory>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniscInflationHistory");
    }
            

    /**
     * @return дата изменения. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.config.UniscInflationHistory#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return процент. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.config.UniscInflationHistory#getTax()
     */
    public static PropertyPath<Double> tax()
    {
        return _dslPath.tax();
    }

    public static class Path<E extends UniscInflationHistory> extends EntityPath<E>
    {
        private PropertyPath<Date> _date;
        private PropertyPath<Double> _tax;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return дата изменения. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.config.UniscInflationHistory#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(UniscInflationHistoryGen.P_DATE, this);
            return _date;
        }

    /**
     * @return процент. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.config.UniscInflationHistory#getTax()
     */
        public PropertyPath<Double> tax()
        {
            if(_tax == null )
                _tax = new PropertyPath<Double>(UniscInflationHistoryGen.P_TAX, this);
            return _tax;
        }

        public Class getEntityClass()
        {
            return UniscInflationHistory.class;
        }

        public String getEntityName()
        {
            return "uniscInflationHistory";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
