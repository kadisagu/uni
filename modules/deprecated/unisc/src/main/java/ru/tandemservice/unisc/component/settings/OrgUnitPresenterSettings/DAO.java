package ru.tandemservice.unisc.component.settings.OrgUnitPresenterSettings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitByTypeAutocompleteModel;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unisc.entity.config.UniscOrgUnitPresenter;

public class DAO extends UniDao<Model> implements IDAO {
    private final static OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("ou");

    @Override
    public void prepare(final Model model) {
        model.setOrgUnitTypeList(OrgUnitManager.instance().dao().getOrgUnitActiveTypeList());
        model.setOrgUnitListModel(new OrgUnitByTypeAutocompleteModel(model));
    }

    @Override public void refreshDataSource(final Model model) {
        final Session session = getSession();
        final Map<Long, UniscOrgUnitPresenter> presenters = getPresenters();

        final DynamicListDataSource<Model.Wrapper> dataSource = model.getDataSource();
        final OrgUnitType orgUnitType = (OrgUnitType) model.getSettings().get("orgUnitType");
        final OrgUnit orgUnit = (OrgUnit) model.getSettings().get("orgUnit");

        final MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "ou", new String[] { "id", "title", OrgUnit.L_ORG_UNIT_TYPE+".title" });
        builder.setReadOnly(true);

        if (orgUnitType != null)
        {
            builder.add(MQExpression.eq("ou", OrgUnit.L_ORG_UNIT_TYPE, orgUnitType));
        }

        if (orgUnit != null)
        {
            builder.add(MQExpression.eq("ou", OrgUnit.P_ID, orgUnit.getId()));
        }

        _orderSettings.applyOrder(builder, dataSource.getEntityOrder());

        dataSource.setTotalSize(builder.getResultCount(session));

        final int startRow = (int) dataSource.getStartRow();
        final int countRow = (int) dataSource.getCountRow();
        final List<Model.Wrapper> wrappers = new ArrayList<Model.Wrapper>();
        final List<Object[]> rows = builder.getResultList(session, startRow, countRow);
        for (final Object[] row: rows) {
            wrappers.add(new Model.Wrapper((Long)row[0], ((String)row[1] + " ("+(String)row[2] +")"), presenters.get(row[0])));
        }

        dataSource.createPage(wrappers);
    }

    private Map<Long, UniscOrgUnitPresenter> getPresenters() {
        final MQBuilder builder = new MQBuilder(UniscOrgUnitPresenter.ENTITY_CLASS, "p");
        builder.setReadOnly(true);
        final Map<Long, UniscOrgUnitPresenter> presenters = new HashMap<Long, UniscOrgUnitPresenter>();
        final List<UniscOrgUnitPresenter> list = builder.getResultList(getSession());
        for (final UniscOrgUnitPresenter presenter: list) {
            presenters.put(presenter.getOrgUnit().getId(), presenter);
        }
        return presenters;
    }

}
