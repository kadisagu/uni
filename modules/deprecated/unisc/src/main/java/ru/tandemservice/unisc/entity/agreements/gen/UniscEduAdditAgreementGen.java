package ru.tandemservice.unisc.entity.agreements.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unisc.entity.agreements.UniscEduAdditAgreement;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;
import ru.tandemservice.unisc.entity.agreements.UniscEduMainAgreement;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Доп. соглашение
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniscEduAdditAgreementGen extends UniscEduAgreementBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisc.entity.agreements.UniscEduAdditAgreement";
    public static final String ENTITY_NAME = "uniscEduAdditAgreement";
    public static final int VERSION_HASH = 112378461;
    private static IEntityMeta ENTITY_META;

    public static final String L_AGREEMENT = "agreement";

    private UniscEduMainAgreement _agreement;     // Договор

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Договор.
     */
    public UniscEduMainAgreement getAgreement()
    {
        return _agreement;
    }

    /**
     * @param agreement Договор.
     */
    public void setAgreement(UniscEduMainAgreement agreement)
    {
        dirty(_agreement, agreement);
        _agreement = agreement;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UniscEduAdditAgreementGen)
        {
            setAgreement(((UniscEduAdditAgreement)another).getAgreement());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniscEduAdditAgreementGen> extends UniscEduAgreementBase.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniscEduAdditAgreement.class;
        }

        public T newInstance()
        {
            return (T) new UniscEduAdditAgreement();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "agreement":
                    return obj.getAgreement();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "agreement":
                    obj.setAgreement((UniscEduMainAgreement) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "agreement":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "agreement":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "agreement":
                    return UniscEduMainAgreement.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniscEduAdditAgreement> _dslPath = new Path<UniscEduAdditAgreement>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniscEduAdditAgreement");
    }
            

    /**
     * @return Договор.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAdditAgreement#getAgreement()
     */
    public static UniscEduMainAgreement.Path<UniscEduMainAgreement> agreement()
    {
        return _dslPath.agreement();
    }

    public static class Path<E extends UniscEduAdditAgreement> extends UniscEduAgreementBase.Path<E>
    {
        private UniscEduMainAgreement.Path<UniscEduMainAgreement> _agreement;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Договор.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAdditAgreement#getAgreement()
     */
        public UniscEduMainAgreement.Path<UniscEduMainAgreement> agreement()
        {
            if(_agreement == null )
                _agreement = new UniscEduMainAgreement.Path<UniscEduMainAgreement>(L_AGREEMENT, this);
            return _agreement;
        }

        public Class getEntityClass()
        {
            return UniscEduAdditAgreement.class;
        }

        public String getEntityName()
        {
            return "uniscEduAdditAgreement";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
