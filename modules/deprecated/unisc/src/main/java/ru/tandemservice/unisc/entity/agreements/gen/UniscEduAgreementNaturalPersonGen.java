package ru.tandemservice.unisc.entity.agreements.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.person.catalog.entity.RelationDegree;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Физическое лицо в договоре
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniscEduAgreementNaturalPersonGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson";
    public static final String ENTITY_NAME = "uniscEduAgreementNaturalPerson";
    public static final int VERSION_HASH = 543080165;
    private static IEntityMeta ENTITY_META;

    public static final String L_OWNER = "owner";
    public static final String L_RELATION_DEGREE = "relationDegree";
    public static final String L_ADDRESS = "address";
    public static final String P_LAST_NAME = "lastName";
    public static final String P_FIRST_NAME = "firstName";
    public static final String P_MIDDLE_NAME = "middleName";
    public static final String P_PASSPORT_SERIA = "passportSeria";
    public static final String P_PASSPORT_NUMBER = "passportNumber";
    public static final String P_PASSPORT_ISSUANCE_DATE = "passportIssuanceDate";
    public static final String P_PASSPORT_ISSUANCE_PLACE = "passportIssuancePlace";
    public static final String P_PASSPORT_ISSUANCE_CODE = "passportIssuanceCode";
    public static final String P_HOME_PHONE_NUMBER = "homePhoneNumber";
    public static final String P_MOBILE_PHONE_NUMBER = "mobilePhoneNumber";
    public static final String P_SIGN_IN_AGREEMENT = "signInAgreement";
    public static final String P_USED_IN_AGREEMENT = "usedInAgreement";

    private UniscEduAgreementBase _owner;     // Договор
    private RelationDegree _relationDegree;     // Степень родства
    private AddressDetailed _address;     // Адрес (детальный)
    private String _lastName;     // Фамилия
    private String _firstName;     // Имя
    private String _middleName;     // Отчество
    private String _passportSeria;     // Серия паспорта
    private String _passportNumber;     // Номер паспорта
    private Date _passportIssuanceDate;     // Дата выдачи паспорта
    private String _passportIssuancePlace;     // Кем выдан паспорт
    private String _passportIssuanceCode;     // Код подразделения
    private String _homePhoneNumber;     // Домашний телефон
    private String _mobilePhoneNumber;     // Мобильный телефон
    private boolean _signInAgreement;     // Подписывает договор
    private boolean _usedInAgreement;     // Оплачивает

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Договор. Свойство не может быть null.
     */
    @NotNull
    public UniscEduAgreementBase getOwner()
    {
        return _owner;
    }

    /**
     * @param owner Договор. Свойство не может быть null.
     */
    public void setOwner(UniscEduAgreementBase owner)
    {
        dirty(_owner, owner);
        _owner = owner;
    }

    /**
     * @return Степень родства. Свойство не может быть null.
     */
    @NotNull
    public RelationDegree getRelationDegree()
    {
        return _relationDegree;
    }

    /**
     * @param relationDegree Степень родства. Свойство не может быть null.
     */
    public void setRelationDegree(RelationDegree relationDegree)
    {
        dirty(_relationDegree, relationDegree);
        _relationDegree = relationDegree;
    }

    /**
     * @return Адрес (детальный). Свойство не может быть null.
     */
    @NotNull
    public AddressDetailed getAddress()
    {
        return _address;
    }

    /**
     * @param address Адрес (детальный). Свойство не может быть null.
     */
    public void setAddress(AddressDetailed address)
    {
        dirty(_address, address);
        _address = address;
    }

    /**
     * @return Фамилия. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getLastName()
    {
        return _lastName;
    }

    /**
     * @param lastName Фамилия. Свойство не может быть null.
     */
    public void setLastName(String lastName)
    {
        dirty(_lastName, lastName);
        _lastName = lastName;
    }

    /**
     * @return Имя. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFirstName()
    {
        return _firstName;
    }

    /**
     * @param firstName Имя. Свойство не может быть null.
     */
    public void setFirstName(String firstName)
    {
        dirty(_firstName, firstName);
        _firstName = firstName;
    }

    /**
     * @return Отчество.
     */
    @Length(max=255)
    public String getMiddleName()
    {
        return _middleName;
    }

    /**
     * @param middleName Отчество.
     */
    public void setMiddleName(String middleName)
    {
        dirty(_middleName, middleName);
        _middleName = middleName;
    }

    /**
     * @return Серия паспорта.
     */
    @Length(max=255)
    public String getPassportSeria()
    {
        return _passportSeria;
    }

    /**
     * @param passportSeria Серия паспорта.
     */
    public void setPassportSeria(String passportSeria)
    {
        dirty(_passportSeria, passportSeria);
        _passportSeria = passportSeria;
    }

    /**
     * @return Номер паспорта.
     */
    @Length(max=255)
    public String getPassportNumber()
    {
        return _passportNumber;
    }

    /**
     * @param passportNumber Номер паспорта.
     */
    public void setPassportNumber(String passportNumber)
    {
        dirty(_passportNumber, passportNumber);
        _passportNumber = passportNumber;
    }

    /**
     * @return Дата выдачи паспорта.
     */
    public Date getPassportIssuanceDate()
    {
        return _passportIssuanceDate;
    }

    /**
     * @param passportIssuanceDate Дата выдачи паспорта.
     */
    public void setPassportIssuanceDate(Date passportIssuanceDate)
    {
        dirty(_passportIssuanceDate, passportIssuanceDate);
        _passportIssuanceDate = passportIssuanceDate;
    }

    /**
     * @return Кем выдан паспорт.
     */
    @Length(max=255)
    public String getPassportIssuancePlace()
    {
        return _passportIssuancePlace;
    }

    /**
     * @param passportIssuancePlace Кем выдан паспорт.
     */
    public void setPassportIssuancePlace(String passportIssuancePlace)
    {
        dirty(_passportIssuancePlace, passportIssuancePlace);
        _passportIssuancePlace = passportIssuancePlace;
    }

    /**
     * @return Код подразделения.
     */
    @Length(max=255)
    public String getPassportIssuanceCode()
    {
        return _passportIssuanceCode;
    }

    /**
     * @param passportIssuanceCode Код подразделения.
     */
    public void setPassportIssuanceCode(String passportIssuanceCode)
    {
        dirty(_passportIssuanceCode, passportIssuanceCode);
        _passportIssuanceCode = passportIssuanceCode;
    }

    /**
     * @return Домашний телефон.
     */
    @Length(max=255)
    public String getHomePhoneNumber()
    {
        return _homePhoneNumber;
    }

    /**
     * @param homePhoneNumber Домашний телефон.
     */
    public void setHomePhoneNumber(String homePhoneNumber)
    {
        dirty(_homePhoneNumber, homePhoneNumber);
        _homePhoneNumber = homePhoneNumber;
    }

    /**
     * @return Мобильный телефон.
     */
    @Length(max=255)
    public String getMobilePhoneNumber()
    {
        return _mobilePhoneNumber;
    }

    /**
     * @param mobilePhoneNumber Мобильный телефон.
     */
    public void setMobilePhoneNumber(String mobilePhoneNumber)
    {
        dirty(_mobilePhoneNumber, mobilePhoneNumber);
        _mobilePhoneNumber = mobilePhoneNumber;
    }

    /**
     * @return Подписывает договор. Свойство не может быть null.
     */
    @NotNull
    public boolean isSignInAgreement()
    {
        return _signInAgreement;
    }

    /**
     * @param signInAgreement Подписывает договор. Свойство не может быть null.
     */
    public void setSignInAgreement(boolean signInAgreement)
    {
        dirty(_signInAgreement, signInAgreement);
        _signInAgreement = signInAgreement;
    }

    /**
     * @return Оплачивает. Свойство не может быть null.
     */
    @NotNull
    public boolean isUsedInAgreement()
    {
        return _usedInAgreement;
    }

    /**
     * @param usedInAgreement Оплачивает. Свойство не может быть null.
     */
    public void setUsedInAgreement(boolean usedInAgreement)
    {
        dirty(_usedInAgreement, usedInAgreement);
        _usedInAgreement = usedInAgreement;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UniscEduAgreementNaturalPersonGen)
        {
            setOwner(((UniscEduAgreementNaturalPerson)another).getOwner());
            setRelationDegree(((UniscEduAgreementNaturalPerson)another).getRelationDegree());
            setAddress(((UniscEduAgreementNaturalPerson)another).getAddress());
            setLastName(((UniscEduAgreementNaturalPerson)another).getLastName());
            setFirstName(((UniscEduAgreementNaturalPerson)another).getFirstName());
            setMiddleName(((UniscEduAgreementNaturalPerson)another).getMiddleName());
            setPassportSeria(((UniscEduAgreementNaturalPerson)another).getPassportSeria());
            setPassportNumber(((UniscEduAgreementNaturalPerson)another).getPassportNumber());
            setPassportIssuanceDate(((UniscEduAgreementNaturalPerson)another).getPassportIssuanceDate());
            setPassportIssuancePlace(((UniscEduAgreementNaturalPerson)another).getPassportIssuancePlace());
            setPassportIssuanceCode(((UniscEduAgreementNaturalPerson)another).getPassportIssuanceCode());
            setHomePhoneNumber(((UniscEduAgreementNaturalPerson)another).getHomePhoneNumber());
            setMobilePhoneNumber(((UniscEduAgreementNaturalPerson)another).getMobilePhoneNumber());
            setSignInAgreement(((UniscEduAgreementNaturalPerson)another).isSignInAgreement());
            setUsedInAgreement(((UniscEduAgreementNaturalPerson)another).isUsedInAgreement());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniscEduAgreementNaturalPersonGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniscEduAgreementNaturalPerson.class;
        }

        public T newInstance()
        {
            return (T) new UniscEduAgreementNaturalPerson();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "owner":
                    return obj.getOwner();
                case "relationDegree":
                    return obj.getRelationDegree();
                case "address":
                    return obj.getAddress();
                case "lastName":
                    return obj.getLastName();
                case "firstName":
                    return obj.getFirstName();
                case "middleName":
                    return obj.getMiddleName();
                case "passportSeria":
                    return obj.getPassportSeria();
                case "passportNumber":
                    return obj.getPassportNumber();
                case "passportIssuanceDate":
                    return obj.getPassportIssuanceDate();
                case "passportIssuancePlace":
                    return obj.getPassportIssuancePlace();
                case "passportIssuanceCode":
                    return obj.getPassportIssuanceCode();
                case "homePhoneNumber":
                    return obj.getHomePhoneNumber();
                case "mobilePhoneNumber":
                    return obj.getMobilePhoneNumber();
                case "signInAgreement":
                    return obj.isSignInAgreement();
                case "usedInAgreement":
                    return obj.isUsedInAgreement();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "owner":
                    obj.setOwner((UniscEduAgreementBase) value);
                    return;
                case "relationDegree":
                    obj.setRelationDegree((RelationDegree) value);
                    return;
                case "address":
                    obj.setAddress((AddressDetailed) value);
                    return;
                case "lastName":
                    obj.setLastName((String) value);
                    return;
                case "firstName":
                    obj.setFirstName((String) value);
                    return;
                case "middleName":
                    obj.setMiddleName((String) value);
                    return;
                case "passportSeria":
                    obj.setPassportSeria((String) value);
                    return;
                case "passportNumber":
                    obj.setPassportNumber((String) value);
                    return;
                case "passportIssuanceDate":
                    obj.setPassportIssuanceDate((Date) value);
                    return;
                case "passportIssuancePlace":
                    obj.setPassportIssuancePlace((String) value);
                    return;
                case "passportIssuanceCode":
                    obj.setPassportIssuanceCode((String) value);
                    return;
                case "homePhoneNumber":
                    obj.setHomePhoneNumber((String) value);
                    return;
                case "mobilePhoneNumber":
                    obj.setMobilePhoneNumber((String) value);
                    return;
                case "signInAgreement":
                    obj.setSignInAgreement((Boolean) value);
                    return;
                case "usedInAgreement":
                    obj.setUsedInAgreement((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "owner":
                        return true;
                case "relationDegree":
                        return true;
                case "address":
                        return true;
                case "lastName":
                        return true;
                case "firstName":
                        return true;
                case "middleName":
                        return true;
                case "passportSeria":
                        return true;
                case "passportNumber":
                        return true;
                case "passportIssuanceDate":
                        return true;
                case "passportIssuancePlace":
                        return true;
                case "passportIssuanceCode":
                        return true;
                case "homePhoneNumber":
                        return true;
                case "mobilePhoneNumber":
                        return true;
                case "signInAgreement":
                        return true;
                case "usedInAgreement":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "owner":
                    return true;
                case "relationDegree":
                    return true;
                case "address":
                    return true;
                case "lastName":
                    return true;
                case "firstName":
                    return true;
                case "middleName":
                    return true;
                case "passportSeria":
                    return true;
                case "passportNumber":
                    return true;
                case "passportIssuanceDate":
                    return true;
                case "passportIssuancePlace":
                    return true;
                case "passportIssuanceCode":
                    return true;
                case "homePhoneNumber":
                    return true;
                case "mobilePhoneNumber":
                    return true;
                case "signInAgreement":
                    return true;
                case "usedInAgreement":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "owner":
                    return UniscEduAgreementBase.class;
                case "relationDegree":
                    return RelationDegree.class;
                case "address":
                    return AddressDetailed.class;
                case "lastName":
                    return String.class;
                case "firstName":
                    return String.class;
                case "middleName":
                    return String.class;
                case "passportSeria":
                    return String.class;
                case "passportNumber":
                    return String.class;
                case "passportIssuanceDate":
                    return Date.class;
                case "passportIssuancePlace":
                    return String.class;
                case "passportIssuanceCode":
                    return String.class;
                case "homePhoneNumber":
                    return String.class;
                case "mobilePhoneNumber":
                    return String.class;
                case "signInAgreement":
                    return Boolean.class;
                case "usedInAgreement":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniscEduAgreementNaturalPerson> _dslPath = new Path<UniscEduAgreementNaturalPerson>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniscEduAgreementNaturalPerson");
    }
            

    /**
     * @return Договор. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson#getOwner()
     */
    public static UniscEduAgreementBase.Path<UniscEduAgreementBase> owner()
    {
        return _dslPath.owner();
    }

    /**
     * @return Степень родства. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson#getRelationDegree()
     */
    public static RelationDegree.Path<RelationDegree> relationDegree()
    {
        return _dslPath.relationDegree();
    }

    /**
     * @return Адрес (детальный). Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson#getAddress()
     */
    public static AddressDetailed.Path<AddressDetailed> address()
    {
        return _dslPath.address();
    }

    /**
     * @return Фамилия. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson#getLastName()
     */
    public static PropertyPath<String> lastName()
    {
        return _dslPath.lastName();
    }

    /**
     * @return Имя. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson#getFirstName()
     */
    public static PropertyPath<String> firstName()
    {
        return _dslPath.firstName();
    }

    /**
     * @return Отчество.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson#getMiddleName()
     */
    public static PropertyPath<String> middleName()
    {
        return _dslPath.middleName();
    }

    /**
     * @return Серия паспорта.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson#getPassportSeria()
     */
    public static PropertyPath<String> passportSeria()
    {
        return _dslPath.passportSeria();
    }

    /**
     * @return Номер паспорта.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson#getPassportNumber()
     */
    public static PropertyPath<String> passportNumber()
    {
        return _dslPath.passportNumber();
    }

    /**
     * @return Дата выдачи паспорта.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson#getPassportIssuanceDate()
     */
    public static PropertyPath<Date> passportIssuanceDate()
    {
        return _dslPath.passportIssuanceDate();
    }

    /**
     * @return Кем выдан паспорт.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson#getPassportIssuancePlace()
     */
    public static PropertyPath<String> passportIssuancePlace()
    {
        return _dslPath.passportIssuancePlace();
    }

    /**
     * @return Код подразделения.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson#getPassportIssuanceCode()
     */
    public static PropertyPath<String> passportIssuanceCode()
    {
        return _dslPath.passportIssuanceCode();
    }

    /**
     * @return Домашний телефон.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson#getHomePhoneNumber()
     */
    public static PropertyPath<String> homePhoneNumber()
    {
        return _dslPath.homePhoneNumber();
    }

    /**
     * @return Мобильный телефон.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson#getMobilePhoneNumber()
     */
    public static PropertyPath<String> mobilePhoneNumber()
    {
        return _dslPath.mobilePhoneNumber();
    }

    /**
     * @return Подписывает договор. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson#isSignInAgreement()
     */
    public static PropertyPath<Boolean> signInAgreement()
    {
        return _dslPath.signInAgreement();
    }

    /**
     * @return Оплачивает. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson#isUsedInAgreement()
     */
    public static PropertyPath<Boolean> usedInAgreement()
    {
        return _dslPath.usedInAgreement();
    }

    public static class Path<E extends UniscEduAgreementNaturalPerson> extends EntityPath<E>
    {
        private UniscEduAgreementBase.Path<UniscEduAgreementBase> _owner;
        private RelationDegree.Path<RelationDegree> _relationDegree;
        private AddressDetailed.Path<AddressDetailed> _address;
        private PropertyPath<String> _lastName;
        private PropertyPath<String> _firstName;
        private PropertyPath<String> _middleName;
        private PropertyPath<String> _passportSeria;
        private PropertyPath<String> _passportNumber;
        private PropertyPath<Date> _passportIssuanceDate;
        private PropertyPath<String> _passportIssuancePlace;
        private PropertyPath<String> _passportIssuanceCode;
        private PropertyPath<String> _homePhoneNumber;
        private PropertyPath<String> _mobilePhoneNumber;
        private PropertyPath<Boolean> _signInAgreement;
        private PropertyPath<Boolean> _usedInAgreement;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Договор. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson#getOwner()
     */
        public UniscEduAgreementBase.Path<UniscEduAgreementBase> owner()
        {
            if(_owner == null )
                _owner = new UniscEduAgreementBase.Path<UniscEduAgreementBase>(L_OWNER, this);
            return _owner;
        }

    /**
     * @return Степень родства. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson#getRelationDegree()
     */
        public RelationDegree.Path<RelationDegree> relationDegree()
        {
            if(_relationDegree == null )
                _relationDegree = new RelationDegree.Path<RelationDegree>(L_RELATION_DEGREE, this);
            return _relationDegree;
        }

    /**
     * @return Адрес (детальный). Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson#getAddress()
     */
        public AddressDetailed.Path<AddressDetailed> address()
        {
            if(_address == null )
                _address = new AddressDetailed.Path<AddressDetailed>(L_ADDRESS, this);
            return _address;
        }

    /**
     * @return Фамилия. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson#getLastName()
     */
        public PropertyPath<String> lastName()
        {
            if(_lastName == null )
                _lastName = new PropertyPath<String>(UniscEduAgreementNaturalPersonGen.P_LAST_NAME, this);
            return _lastName;
        }

    /**
     * @return Имя. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson#getFirstName()
     */
        public PropertyPath<String> firstName()
        {
            if(_firstName == null )
                _firstName = new PropertyPath<String>(UniscEduAgreementNaturalPersonGen.P_FIRST_NAME, this);
            return _firstName;
        }

    /**
     * @return Отчество.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson#getMiddleName()
     */
        public PropertyPath<String> middleName()
        {
            if(_middleName == null )
                _middleName = new PropertyPath<String>(UniscEduAgreementNaturalPersonGen.P_MIDDLE_NAME, this);
            return _middleName;
        }

    /**
     * @return Серия паспорта.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson#getPassportSeria()
     */
        public PropertyPath<String> passportSeria()
        {
            if(_passportSeria == null )
                _passportSeria = new PropertyPath<String>(UniscEduAgreementNaturalPersonGen.P_PASSPORT_SERIA, this);
            return _passportSeria;
        }

    /**
     * @return Номер паспорта.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson#getPassportNumber()
     */
        public PropertyPath<String> passportNumber()
        {
            if(_passportNumber == null )
                _passportNumber = new PropertyPath<String>(UniscEduAgreementNaturalPersonGen.P_PASSPORT_NUMBER, this);
            return _passportNumber;
        }

    /**
     * @return Дата выдачи паспорта.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson#getPassportIssuanceDate()
     */
        public PropertyPath<Date> passportIssuanceDate()
        {
            if(_passportIssuanceDate == null )
                _passportIssuanceDate = new PropertyPath<Date>(UniscEduAgreementNaturalPersonGen.P_PASSPORT_ISSUANCE_DATE, this);
            return _passportIssuanceDate;
        }

    /**
     * @return Кем выдан паспорт.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson#getPassportIssuancePlace()
     */
        public PropertyPath<String> passportIssuancePlace()
        {
            if(_passportIssuancePlace == null )
                _passportIssuancePlace = new PropertyPath<String>(UniscEduAgreementNaturalPersonGen.P_PASSPORT_ISSUANCE_PLACE, this);
            return _passportIssuancePlace;
        }

    /**
     * @return Код подразделения.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson#getPassportIssuanceCode()
     */
        public PropertyPath<String> passportIssuanceCode()
        {
            if(_passportIssuanceCode == null )
                _passportIssuanceCode = new PropertyPath<String>(UniscEduAgreementNaturalPersonGen.P_PASSPORT_ISSUANCE_CODE, this);
            return _passportIssuanceCode;
        }

    /**
     * @return Домашний телефон.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson#getHomePhoneNumber()
     */
        public PropertyPath<String> homePhoneNumber()
        {
            if(_homePhoneNumber == null )
                _homePhoneNumber = new PropertyPath<String>(UniscEduAgreementNaturalPersonGen.P_HOME_PHONE_NUMBER, this);
            return _homePhoneNumber;
        }

    /**
     * @return Мобильный телефон.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson#getMobilePhoneNumber()
     */
        public PropertyPath<String> mobilePhoneNumber()
        {
            if(_mobilePhoneNumber == null )
                _mobilePhoneNumber = new PropertyPath<String>(UniscEduAgreementNaturalPersonGen.P_MOBILE_PHONE_NUMBER, this);
            return _mobilePhoneNumber;
        }

    /**
     * @return Подписывает договор. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson#isSignInAgreement()
     */
        public PropertyPath<Boolean> signInAgreement()
        {
            if(_signInAgreement == null )
                _signInAgreement = new PropertyPath<Boolean>(UniscEduAgreementNaturalPersonGen.P_SIGN_IN_AGREEMENT, this);
            return _signInAgreement;
        }

    /**
     * @return Оплачивает. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson#isUsedInAgreement()
     */
        public PropertyPath<Boolean> usedInAgreement()
        {
            if(_usedInAgreement == null )
                _usedInAgreement = new PropertyPath<Boolean>(UniscEduAgreementNaturalPersonGen.P_USED_IN_AGREEMENT, this);
            return _usedInAgreement;
        }

        public Class getEntityClass()
        {
            return UniscEduAgreementNaturalPerson.class;
        }

        public String getEntityName()
        {
            return "uniscEduAgreementNaturalPerson";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
