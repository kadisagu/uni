package ru.tandemservice.unisc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Период оплаты за обучение"
 * Имя сущности : uniscPayPeriod
 * Файл data.xml : unisc.data.xml
 */
public interface UniscPayPeriodCodes
{
    /** Константа кода (code) элемента : за весь период обучения (title) */
    String ZA_VES_PERIOD_OBUCHENIYA = "0";
    /** Константа кода (code) элемента : за год (title) */
    String ZA_GOD = "1";
    /** Константа кода (code) элемента : за семестр (title) */
    String ZA_SEMESTR = "2";
    /** Константа кода (code) элемента : за половину семестра (title) */
    String ZA_POLOVINU_SEMESTRA = "3";

    Set<String> CODES = ImmutableSet.of(ZA_VES_PERIOD_OBUCHENIYA, ZA_GOD, ZA_SEMESTR, ZA_POLOVINU_SEMESTRA);
}
