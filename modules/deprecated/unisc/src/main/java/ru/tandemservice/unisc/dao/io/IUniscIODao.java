package ru.tandemservice.unisc.dao.io;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.unisc.entity.catalog.UniscPayPeriod;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlan;

public interface IUniscIODao {
	public static final SpringBeanCache<IUniscIODao> INSTANCE = new SpringBeanCache<IUniscIODao>(IUniscIODao.class.getName());

	public interface Block {
		String getBlockIdentifier();
		String getBlockTitle();
		String[] getValueCaptions();
		Map<EducationOrgUnit, Map<String, Object[]>> getRows();
	}

	public interface ImportFacade {
		void add(EducationOrgUnit edu, String cipher, Iterator<String> data);
		void doImport();
	}

	List<Block> getExportData(EducationYear year);
	ImportFacade getImportFacade(EducationYear year);
	Date getDate(UniscEduOrgUnit uniscEduOrgUnit, UniscPayPeriod p, int stage);


	UniscEduOrgUnitPayPlan doGetPayPlan(UniscEduOrgUnit ou, UniscPayPeriod freq);

}
