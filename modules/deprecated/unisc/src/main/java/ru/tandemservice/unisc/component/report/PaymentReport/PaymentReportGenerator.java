package ru.tandemservice.unisc.component.report.PaymentReport;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.hibernate.Session;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.unisc.UniscDefines;
import ru.tandemservice.unisc.component.report.UniscRtfUtils;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayFactRow;
import ru.tandemservice.unisc.entity.agreements.UniscEduMainAgreement;
import ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student;
import ru.tandemservice.unisc.entity.catalog.UniscTemplateDocument;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;

import java.util.*;

/**
 * @author oleyba
 * @since 17.08.2009
 */
public class PaymentReportGenerator
{
    protected Session session;
    protected Long orgUnitId;
    protected MQBuilderFactory agreementBuilder;
    protected Date dateFrom;
    protected Date dateTo;

    protected Map<MultiKey, List<UniscEduAgreementPayFactRow>> paymentMap = new HashMap<>();
    protected HashMap<Long, Student> studentMap = new HashMap<>();
    protected List<OrgUnit> formativeOrgUnitList = new ArrayList<>();
    protected List<OrgUnit> territorialOrgUnitList = new ArrayList<>();

    public PaymentReportGenerator(Session session, Long orgUnitId, MQBuilderFactory agreementBuilder, Date dateFrom, Date dateTo)
    {
        this.session = session;
        this.orgUnitId = orgUnitId;
        this.agreementBuilder = agreementBuilder;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
    }

    public DatabaseFile generateReportContent()
    {
        ITemplateDocument templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniscTemplateDocument.class, UniscDefines.TEMPLATE_PAYMENT_REPORT);
        RtfDocument document = new RtfReader().read(templateDocument.getContent());

        initData();

        final List<String[]> reportRowList = new ArrayList<>();

        long total = 0L;
        for (OrgUnit formativeOrgUnit : formativeOrgUnitList)
            for (OrgUnit territorialOrgUnit : territorialOrgUnitList)
                total = total + appendOrgUnitRows(reportRowList, formativeOrgUnit, territorialOrgUnit);

        modify(document, reportRowList, total);

        byte[] data = RtfUtil.toByteArray(document);
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    protected void initData()
    {
        fillStudentMap();
        fillPaymentMap();
    }

    protected void modify(RtfDocument document, final List<String[]> reportRowList, long total)
    {
        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T", reportRowList.toArray(new String[reportRowList.size()][]));
        tableModifier.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                UniscRtfUtils.afterModify(newRowList, startIndex, reportRowList);
            }
        }
        );
        tableModifier.modify(document);

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("periodReport", DateFormatter.DEFAULT_DATE_FORMATTER.format(dateFrom) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(dateTo));
        OrgUnit orgUnit = orgUnitId == null ? null : UniDaoFacade.getCoreDao().get(OrgUnit.class, orgUnitId);
        injectModifier.put("scope", orgUnit == null ? "ИТОГО по всему ОУ" : "ИТОГО по поздразделению (" + (orgUnit.getNominativeCaseTitle() == null ? orgUnit.getTitle() : orgUnit.getNominativeCaseTitle()) + ")");
        injectModifier.put("total", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(0.001d * total));
        injectModifier.modify(document);
    }

    protected long appendOrgUnitRows(List<String[]> reportRowList, OrgUnit formativeOrgUnit, OrgUnit territorialOrgUnit)
    {
        MultiKey key = new MultiKey(formativeOrgUnit.getId(), territorialOrgUnit.getId());
        List<UniscEduAgreementPayFactRow> paymentList = paymentMap.get(key);
        if (paymentList == null)
            return 0L;
        String caption = "Подразделение: " + (formativeOrgUnit.getNominativeCaseTitle() == null ? formativeOrgUnit.getTitle() : formativeOrgUnit.getNominativeCaseTitle());
        caption = caption + " (" +  (territorialOrgUnit.getNominativeCaseTitle() == null ? territorialOrgUnit.getTerritorialTitle() : territorialOrgUnit.getNominativeCaseTitle()) + ")";
        reportRowList.add(new String[] {caption});
        long sum = 0L;
        for (UniscEduAgreementPayFactRow payment : paymentList)
        {
            StringBuilder title = new StringBuilder();
            if (payment.getType() != null)
                title.append(payment.getType().getShortTitle()).append(" ");
            if (payment.getNumber() != null)
                title.append("№ ").append(payment.getNumber()).append(" ");
            title.append("от ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(payment.getDate()));

            appendPayment(reportRowList, payment, title);

            sum = sum + payment.getJCost() + payment.getNCost();
        }
        reportRowList.add(new String[]{"ИТОГО", "", "", "", "", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(0.001d * sum)});
        return sum;
    }

    protected void appendPayment(List<String[]> reportRowList, UniscEduAgreementPayFactRow payment, StringBuilder title)
    {
        Student student = studentMap.get(payment.getAgreement().getId());
        reportRowList.add(new String[]
            {
                    title.toString(),
                    student.getPerson().getFullFio(),
                    payment.getAgreement().getConfig().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getShortTitle(),
                    student.getCourse().getTitle(),
                    student.getGroup() == null ? "" : student.getGroup().getTitle(),
                    payment.getCostAsString()
            });
    }

    protected void fillPaymentMap()
    {
        MQBuilder builder = new MQBuilder(UniscEduAgreementPayFactRow.ENTITY_CLASS, "p");
        builder.add(MQExpression.in("p", UniscEduAgreementPayFactRow.L_AGREEMENT + ".id", agreementBuilder.createIdBuilder()));
        builder.add(UniMQExpression.betweenDate("p", UniscEduAgreementPayFactRow.P_DATE, dateFrom, dateTo));
        builder.addOrder("p", UniscEduAgreementPayFactRow.P_DATE);
        builder.addLeftJoinFetch("p", UniscEduAgreementPayFactRow.L_TYPE, "type_fetch");
        builder.addLeftJoinFetch("p", UniscEduAgreementPayFactRow.L_AGREEMENT, "agr_fetch");
        builder.addLeftJoinFetch("agr_fetch", UniscEduMainAgreement.L_CONFIG, "conf_fetch");
        builder.addLeftJoinFetch("conf_fetch", UniscEduOrgUnit.L_EDUCATION_ORG_UNIT, "agr_edu_ou_fetch");
        builder.addLeftJoinFetch("agr_edu_ou_fetch", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "agr_hs_fetch");
        builder.addLeftJoinFetch("agr_hs_fetch", EducationLevelsHighSchool.L_EDUCATION_LEVEL, "agr_lev_fetch");

        Set<OrgUnit> formativeOrgUnits = new HashSet<>();
        Set<OrgUnit> territorialOrgUnits = new HashSet<>();

        for (UniscEduAgreementPayFactRow payment : builder.<UniscEduAgreementPayFactRow>getResultList(session))
        {
            EducationOrgUnit eduOu = payment.getAgreement().getConfig().getEducationOrgUnit();
            MultiKey key = new MultiKey(eduOu.getFormativeOrgUnit().getId(), eduOu.getTerritorialOrgUnit().getId());
            List<UniscEduAgreementPayFactRow> paymentList = paymentMap.get(key);
            if (paymentList == null)
                paymentMap.put(key, paymentList = new ArrayList<>());
            paymentList.add(payment);
            formativeOrgUnits.add(eduOu.getFormativeOrgUnit());
            territorialOrgUnits.add(eduOu.getTerritorialOrgUnit());
        }

        formativeOrgUnitList = new ArrayList<>(formativeOrgUnits);
        Collections.sort(formativeOrgUnitList, TITLED_COMPARATOR);
        territorialOrgUnitList = new ArrayList<>(territorialOrgUnits);
        Collections.sort(territorialOrgUnitList, TITLED_COMPARATOR);

        for (List<UniscEduAgreementPayFactRow> paymentList : paymentMap.values())
            Collections.sort(paymentList, getPaymentComparator());
    }

    protected Comparator<UniscEduAgreementPayFactRow> getPaymentComparator()
    {
        return Comparator.comparing(UniscEduAgreementPayFactRow::getDate)
                .thenComparing(CommonCollator.comparing(r -> studentMap.get(r.getAgreement().getId()).getFullFio(), true));
    }

    protected void fillStudentMap()
    {
        MQBuilder builder = new MQBuilder(UniscEduAgreement2Student.ENTITY_CLASS, "rel");
        builder.addLeftJoinFetch("rel", UniscEduAgreement2Student.L_STUDENT, "student_fetch");
        builder.addLeftJoinFetch("student_fetch", Student.L_GROUP, "group_fetch");
        builder.addLeftJoinFetch("student_fetch", Student.L_COURSE, "course_fetch");
        builder.addLeftJoinFetch("student_fetch", Student.L_PERSON, "person_fetch");
        builder.addLeftJoinFetch("person_fetch", Person.L_IDENTITY_CARD, "idc_fetch");
        builder.addDomain("pay", UniscEduAgreementPayFactRow.ENTITY_CLASS);
        builder.add(MQExpression.eqProperty("pay", UniscEduAgreementPayFactRow.L_AGREEMENT + ".id", "rel", UniscEduAgreement2Student.L_AGREEMENT + ".id"));
        builder.add(MQExpression.in("pay", UniscEduAgreementPayFactRow.L_AGREEMENT + ".id", agreementBuilder.createIdBuilder()));
        builder.add(UniMQExpression.betweenDate("pay", UniscEduAgreementPayFactRow.P_DATE, dateFrom, dateTo));

        for (UniscEduAgreement2Student rel : builder.<UniscEduAgreement2Student>getResultList(session))
            studentMap.put(rel.getAgreement().getId(), rel.getStudent());
    }

    private static Comparator<ITitled> TITLED_COMPARATOR = new Comparator<ITitled>()
    {
        @Override
        public int compare(ITitled o1, ITitled o2)
        {
            String s1 = o1 == null ? "" : (o1.getTitle() == null ? "" : o1.getTitle());
            String s2 = o2 == null ? "" : (o2.getTitle() == null ? "" : o2.getTitle());
            return s1.compareTo(s2);
        }
    };
}
