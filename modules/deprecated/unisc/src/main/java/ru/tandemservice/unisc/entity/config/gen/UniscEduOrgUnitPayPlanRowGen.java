package ru.tandemservice.unisc.entity.config.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlan;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlanRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Строка графика оплат (настройка)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniscEduOrgUnitPayPlanRowGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlanRow";
    public static final String ENTITY_NAME = "uniscEduOrgUnitPayPlanRow";
    public static final int VERSION_HASH = -996438109;
    private static IEntityMeta ENTITY_META;

    public static final String L_PLAN = "plan";
    public static final String P_STAGE = "stage";
    public static final String P_DATE = "date";
    public static final String P_COST = "cost";

    private UniscEduOrgUnitPayPlan _plan;     // Период
    private int _stage;     // Номер периода
    private Date _date;     // Дата
    private long _cost;     // Стоимость

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Период. Свойство не может быть null.
     */
    @NotNull
    public UniscEduOrgUnitPayPlan getPlan()
    {
        return _plan;
    }

    /**
     * @param plan Период. Свойство не может быть null.
     */
    public void setPlan(UniscEduOrgUnitPayPlan plan)
    {
        dirty(_plan, plan);
        _plan = plan;
    }

    /**
     * @return Номер периода. Свойство не может быть null.
     */
    @NotNull
    public int getStage()
    {
        return _stage;
    }

    /**
     * @param stage Номер периода. Свойство не может быть null.
     */
    public void setStage(int stage)
    {
        dirty(_stage, stage);
        _stage = stage;
    }

    /**
     * @return Дата. Свойство не может быть null.
     */
    @NotNull
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date Дата. Свойство не может быть null.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return Стоимость. Свойство не может быть null.
     */
    @NotNull
    public long getCost()
    {
        return _cost;
    }

    /**
     * @param cost Стоимость. Свойство не может быть null.
     */
    public void setCost(long cost)
    {
        dirty(_cost, cost);
        _cost = cost;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UniscEduOrgUnitPayPlanRowGen)
        {
            setPlan(((UniscEduOrgUnitPayPlanRow)another).getPlan());
            setStage(((UniscEduOrgUnitPayPlanRow)another).getStage());
            setDate(((UniscEduOrgUnitPayPlanRow)another).getDate());
            setCost(((UniscEduOrgUnitPayPlanRow)another).getCost());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniscEduOrgUnitPayPlanRowGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniscEduOrgUnitPayPlanRow.class;
        }

        public T newInstance()
        {
            return (T) new UniscEduOrgUnitPayPlanRow();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "plan":
                    return obj.getPlan();
                case "stage":
                    return obj.getStage();
                case "date":
                    return obj.getDate();
                case "cost":
                    return obj.getCost();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "plan":
                    obj.setPlan((UniscEduOrgUnitPayPlan) value);
                    return;
                case "stage":
                    obj.setStage((Integer) value);
                    return;
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "cost":
                    obj.setCost((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "plan":
                        return true;
                case "stage":
                        return true;
                case "date":
                        return true;
                case "cost":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "plan":
                    return true;
                case "stage":
                    return true;
                case "date":
                    return true;
                case "cost":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "plan":
                    return UniscEduOrgUnitPayPlan.class;
                case "stage":
                    return Integer.class;
                case "date":
                    return Date.class;
                case "cost":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniscEduOrgUnitPayPlanRow> _dslPath = new Path<UniscEduOrgUnitPayPlanRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniscEduOrgUnitPayPlanRow");
    }
            

    /**
     * @return Период. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlanRow#getPlan()
     */
    public static UniscEduOrgUnitPayPlan.Path<UniscEduOrgUnitPayPlan> plan()
    {
        return _dslPath.plan();
    }

    /**
     * @return Номер периода. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlanRow#getStage()
     */
    public static PropertyPath<Integer> stage()
    {
        return _dslPath.stage();
    }

    /**
     * @return Дата. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlanRow#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return Стоимость. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlanRow#getCost()
     */
    public static PropertyPath<Long> cost()
    {
        return _dslPath.cost();
    }

    public static class Path<E extends UniscEduOrgUnitPayPlanRow> extends EntityPath<E>
    {
        private UniscEduOrgUnitPayPlan.Path<UniscEduOrgUnitPayPlan> _plan;
        private PropertyPath<Integer> _stage;
        private PropertyPath<Date> _date;
        private PropertyPath<Long> _cost;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Период. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlanRow#getPlan()
     */
        public UniscEduOrgUnitPayPlan.Path<UniscEduOrgUnitPayPlan> plan()
        {
            if(_plan == null )
                _plan = new UniscEduOrgUnitPayPlan.Path<UniscEduOrgUnitPayPlan>(L_PLAN, this);
            return _plan;
        }

    /**
     * @return Номер периода. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlanRow#getStage()
     */
        public PropertyPath<Integer> stage()
        {
            if(_stage == null )
                _stage = new PropertyPath<Integer>(UniscEduOrgUnitPayPlanRowGen.P_STAGE, this);
            return _stage;
        }

    /**
     * @return Дата. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlanRow#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(UniscEduOrgUnitPayPlanRowGen.P_DATE, this);
            return _date;
        }

    /**
     * @return Стоимость. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlanRow#getCost()
     */
        public PropertyPath<Long> cost()
        {
            if(_cost == null )
                _cost = new PropertyPath<Long>(UniscEduOrgUnitPayPlanRowGen.P_COST, this);
            return _cost;
        }

        public Class getEntityClass()
        {
            return UniscEduOrgUnitPayPlanRow.class;
        }

        public String getEntityName()
        {
            return "uniscEduOrgUnitPayPlanRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
