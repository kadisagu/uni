package ru.tandemservice.unisc.component.settings.EduOrgUnitCostEnvAdd;

import java.util.List;

import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisc.entity.catalog.UniscPayPeriod;

/**
 * @author Vasily Zhukov
 * @since 17.02.2011
 */
public class Model
{
    private List<EducationYear> _educationYearList;

    private List<UniscPayPeriod> _payPeriodList;

    private EducationYear _educationYear;

    private UniscPayPeriod _payPeriod;

    private int _stage;

    // Getters & Setters

    public List<EducationYear> getEducationYearList()
    {
        return _educationYearList;
    }

    public void setEducationYearList(List<EducationYear> educationYearList)
    {
        _educationYearList = educationYearList;
    }

    public List<UniscPayPeriod> getPayPeriodList()
    {
        return _payPeriodList;
    }

    public void setPayPeriodList(List<UniscPayPeriod> payPeriodList)
    {
        _payPeriodList = payPeriodList;
    }

    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    public void setEducationYear(EducationYear educationYear)
    {
        _educationYear = educationYear;
    }

    public UniscPayPeriod getPayPeriod()
    {
        return _payPeriod;
    }

    public void setPayPeriod(UniscPayPeriod payPeriod)
    {
        _payPeriod = payPeriod;
    }

    public int getStage()
    {
        return _stage;
    }

    public void setStage(int stage)
    {
        _stage = stage;
    }
}
