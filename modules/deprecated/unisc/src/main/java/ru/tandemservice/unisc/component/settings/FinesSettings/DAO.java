package ru.tandemservice.unisc.component.settings.FinesSettings;

import org.hibernate.Session;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisc.entity.config.UniscFinesHistory;
import ru.tandemservice.unisc.entity.config.gen.UniscFinesHistoryGen;

import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(final Model model) {
        model.setYears(new EducationYearModel());
    }

    @Override
    public void delete(final Model model, final Long id) {
        delete(id);
    }

    @Override public void refreshDataSource(final Model model) {
        final Session session = getSession();
        final DynamicListDataSource<UniscFinesHistory> dataSource = model.getDataSource();

        final MQBuilder builder = new MQBuilder(UniscFinesHistoryGen.ENTITY_CLASS, "f");
        final EducationYear educationYear = model.getEducationYear();
        if (null != educationYear) {
            builder.add(new AbstractExpression() {
                private static final long serialVersionUID = 5185994287688837201L;
                @SuppressWarnings("unchecked")
                @Override public String getHQL(final List bindingList) {
                    bindingList.add(educationYear.getIntValue());
                    return ("year(f."+UniscFinesHistoryGen.P_DATE+")=:mq"+(bindingList.size()));
                }
            });
        }

        builder.applyOrder(model.getDataSource().getEntityOrder());

        final List<UniscFinesHistory> list = builder.getResultList(session);
        dataSource.setTotalSize(list.size());
        dataSource.setCountRow(list.size());
        dataSource.createPage(list);
    }
}
