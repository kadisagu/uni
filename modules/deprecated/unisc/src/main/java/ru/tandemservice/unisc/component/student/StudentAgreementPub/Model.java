package ru.tandemservice.unisc.component.student.StudentAgreementPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;

import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisc.dao.agreement.IUniscEduAgreementDAO;
import ru.tandemservice.unisc.entity.agreements.UniscEduMainAgreement;
import ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student;

@State( { @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "relation.id"), @Bind(key = "selectedTab", binding = "selectedTab") })
public class Model
{
    private String selectedTab;
    private UniscEduAgreement2Student relation = new UniscEduAgreement2Student();
    private CommonPostfixPermissionModel secModel;

    public boolean isReadOnly()
    {
        return (!IUniscEduAgreementDAO.INSTANCE.get().isMutable(getRelation().getAgreement()));
    }

    public Student getStudent()
    {
        return getRelation().getStudent();
    }

    public UniscEduMainAgreement getAgreement()
    {
        return getRelation().getAgreement();
    }

    public String getSelectedTab()
    {
        return this.selectedTab;
    }

    public void setSelectedTab(final String selectedTab)
    {
        this.selectedTab = selectedTab;
    }

    public UniscEduAgreement2Student getRelation()
    {
        return relation;
    }

    public void setRelation(final UniscEduAgreement2Student relation)
    {
        this.relation = relation;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        this.secModel = secModel;
    }

    public boolean isEditAgreementDisabled()
    {
        if (UniDefines.COMPENSATION_TYPE_BUDGET.equals(getStudent().getCompensationType().getCode()))
            return true;
        if (getStudent().isArchival())
            return true;
        if (getAgreement().isCheckedBySignature())
            return true;
        return  isReadOnly() || !getRelation().isActive();
    }
}
