package ru.tandemservice.unisc.component.agreement.AgreementPrintVersionsTab;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPrintVersion;
import ru.tandemservice.unisc.entity.agreements.gen.UniscEduAgreementPrintVersionGen;

import java.util.Collections;

/**
 * @author vdanilov
 */
public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(final IBusinessComponent component) {
        final Model model = getModel(component);
        getDao().prepare(model);

        final DynamicListDataSource<UniscEduAgreementPrintVersion> ds = new DynamicListDataSource<>(component, component1 -> {
            getDao().refreshDataSource(getModel(component1));
        });
        ds.addColumn(new SimpleColumn("№ версии", UniscEduAgreementPrintVersionGen.P_TITLE).setOrderable(false).setClickable(false));
        ds.addColumn(new SimpleColumn("Дата", UniscEduAgreementPrintVersionGen.P_TIMESTAMP, DateFormatter.DATE_FORMATTER_WITH_TIME).setOrderable(false).setClickable(false));
        ds.addColumn(new SimpleColumn("Пользователь", UniscEduAgreementPrintVersionGen.P_USER).setOrderable(false).setClickable(false));
        ds.addColumn(new SimpleColumn("Комментарий", UniscEduAgreementPrintVersionGen.P_COMMENT).setOrderable(false).setClickable(false));

        //ds.addColumn(new SimpleColumn("Файл", UniscEduAgreementPrintVersionGen.P_FILE_NAME).setOrderable(false).setClickable(false));
        //ds.addColumn(new SimpleColumn("Загружен", UniscEduAgreementPrintVersionGen.P_UPLOADED, YesNoFormatter.INSTANCE).setOrderable(false).setClickable(false));

        ds.addColumn(CommonBaseUtil.getPrintColumn("onClickPrint", "Печать").setPermissionKey("printVersion_StudentAdditAgreementPaymentsTab"));
        ds.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить печатную версию?").setPermissionKey("deleteVersion_StudentAdditAgreementPaymentsTab"));

        model.setDataSource(ds);
    }

    public void onClickPrint(final IBusinessComponent component) {
        activateInRoot(component, new ComponentActivator(
                ru.tandemservice.unisc.component.agreement.AgreementPrintVersionsTab.Print.Model.class.getPackage().getName(),
                Collections.singletonMap(PublisherActivator.PUBLISHER_ID_KEY, component.getListenerParameter())
        ));
    }

    public void onClickAdd(final IBusinessComponent component) {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.unisc.component.agreement.AgreementPrintVersionsTab.AddEdit.Model.class.getPackage().getName(),
                Collections.<String, Object>singletonMap(PublisherActivator.PUBLISHER_ID_KEY, getModel(component).getId())
        ));
    }
    
    public void onClickDelete(final IBusinessComponent component) {
        getDao().delete(getModel(component), (Long)component.getListenerParameter());
    }
}
