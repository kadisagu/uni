package ru.tandemservice.unisc.component.agreement.AgreementAddEdit;

import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson;

import java.util.List;


/**
 * @author vdanilov
 */
@SuppressWarnings("deprecation")
public interface IDAO<M extends Model> {
    void prepare(M model);
    void execute(M model);
    void save(M model);

    List<UniscEduAgreementNaturalPerson> getDefaultNaturalPersonList(M model);
}
