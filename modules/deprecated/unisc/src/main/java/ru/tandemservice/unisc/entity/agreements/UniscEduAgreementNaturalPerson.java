package ru.tandemservice.unisc.entity.agreements;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.fias.base.entity.Address;

import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.utils.AddressBaseUtils;
import org.tandemframework.shared.person.base.entity.PersonNextOfKin;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.RelationDegree;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unisc.entity.agreements.gen.UniscEduAgreementNaturalPersonGen;

/**
 * Физическое лицо в договоре
 */
public class UniscEduAgreementNaturalPerson extends UniscEduAgreementNaturalPersonGen implements ITitled
{
	public static final String P_TITLE = "title";
	public static final String P_PASPORT_TITLE = "pasportTitle";
	public static final String P_SIMPLE_PASPORT_TITLE = "simplePasportTitle";

	public UniscEduAgreementNaturalPerson() {
		setUsedInAgreement(true); // initial state
	}


	public String getFullFio() {
		return UniStringUtils.join(this.getLastName(), this.getFirstName(), this.getMiddleName());
	}

	@Override
    public String getTitle() {
		return getFullFio();
	}

	public String getPasportTitle() {
        if (org.apache.commons.lang.StringUtils.isBlank(this.getPassportSeria()) && org.apache.commons.lang.StringUtils.isBlank(this.getPassportNumber())) { return ""; }
		return UniStringUtils.join("паспорт", this.getPassportSeria(), this.getPassportNumber()) +
		(((null == this.getPassportIssuanceDate()) && (null == this.getPassportIssuancePlace())) ? "" : UniStringUtils.join(" выдан", DateFormatter.DEFAULT_DATE_FORMATTER.format(this.getPassportIssuanceDate()), this.getPassportIssuancePlace()));
	}

	public String getSimplePasportTitle() {
		if (org.apache.commons.lang.StringUtils.isBlank(this.getPassportSeria()) && org.apache.commons.lang.StringUtils.isBlank(this.getPassportNumber())) { return ""; }
		return UniStringUtils.join(this.getPassportSeria(), this.getPassportNumber());
	}


	@Override
	public boolean isUsedInAgreement() {
		// если он не подписывает, то обязательно оплачивает
		return (!isSignInAgreement()) || super.isUsedInAgreement();
	}



	public boolean isTransient() {
		if (null == this.getId()) { return true; }

		final IEntityMeta meta = EntityRuntime.getMeta(this.getId());
		if (null == meta) { return true; }

		return !UniscEduAgreementNaturalPerson.class.isAssignableFrom(meta.getEntityClass());
	}

	public UniscEduAgreementNaturalPerson from(final PersonNextOfKin k) {
		this.setId(k.getId());

		this.setRelationDegree(k.getRelationDegree());
		this.setFirstName(k.getFirstName());
		this.setLastName(k.getLastName());
		this.setMiddleName(k.getMiddleName());
		this.setPassportNumber(k.getPassportNumber());
		this.setPassportSeria(k.getPassportSeria());
		this.setPassportIssuancePlace(k.getPassportIssuancePlace());
		this.setPassportIssuanceDate(k.getPassportIssuanceDate());
		this.setPassportIssuanceCode(k.getPassportIssuanceCode());
		this.setAddress(null != k.getAddress() &&
                k.getAddress() instanceof AddressDetailed ?
                (AddressDetailed)AddressBaseUtils.getSameAddress(k.getAddress()) :
                null);
		if (null == getHomePhoneNumber()) {
			this.setHomePhoneNumber(k.getPhones());
		}

		return this;
	}


	public UniscEduAgreementNaturalPerson from(final Person person, final RelationDegree degree) {
		this.setId(person.getId());
		this.setRelationDegree(degree);

		final IdentityCard identityCard = person.getIdentityCard();
		this.setFirstName(identityCard.getFirstName());
		this.setLastName(identityCard.getLastName());
		this.setMiddleName(identityCard.getMiddleName());
		this.setPassportNumber(identityCard.getNumber());
		this.setPassportSeria(identityCard.getSeria());
		this.setPassportIssuancePlace(identityCard.getIssuancePlace());
		this.setPassportIssuanceDate(identityCard.getIssuanceDate());
		this.setPassportIssuanceCode(identityCard.getIssuanceCode());

		if (null != person.getAddressRegistration() && person.getAddressRegistration() instanceof AddressDetailed) { this.setAddress((AddressDetailed)AddressBaseUtils.getSameAddress(person.getAddressRegistration())); }
		else if (null != person.getAddress() && person.getAddress() instanceof AddressDetailed) { this.setAddress((AddressDetailed)AddressBaseUtils.getSameAddress(person.getAddress())); }

        this.setHomePhoneNumber(person.getContactData().getPhoneFact());
        this.setMobilePhoneNumber(person.getContactData().getPhoneMobile());

		return this;
	}
}
