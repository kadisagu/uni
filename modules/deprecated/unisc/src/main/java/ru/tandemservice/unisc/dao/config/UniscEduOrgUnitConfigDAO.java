package ru.tandemservice.unisc.dao.config;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unisc.dao.io.IUniscIODao;
import ru.tandemservice.unisc.entity.catalog.UniscPayPeriod;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlan;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlanRow;
import ru.tandemservice.unisc.entity.config.gen.UniscEduOrgUnitGen;
import ru.tandemservice.unisc.entity.config.gen.UniscEduOrgUnitPayPlanGen;
import ru.tandemservice.unisc.entity.config.gen.UniscEduOrgUnitPayPlanRowGen;
import ru.tandemservice.unisc.ws.cost.EducationOrgUnitCostEnvironment;

public class UniscEduOrgUnitConfigDAO extends UniBaseDao implements IUniscEduOrgUnitConfigDAO
{

    public static double wrap(final long value)
    {
        return (0.001d * value);
    }

    public static long unwrap(final double value)
    {
        return (long) (value * 1000.0d);
    }


    private static final Object MUTEX = new Object();

    protected UniscEduOrgUnit find(final EducationYear year, final EducationOrgUnit eduOu, String cipher)
    {
        return getByNaturalId(new UniscEduOrgUnitGen.NaturalId(year, eduOu, StringUtils.trimToNull(cipher)));
    }

    @Override
    public UniscEduOrgUnit doSaveUniscEduOrgUnit(final UniscEduOrgUnit entity)
    {
        synchronized (UniscEduOrgUnitConfigDAO.MUTEX)
        {
            /* double check to avoid errors */
            final UniscEduOrgUnit result = this.find(entity.getEducationYear(), entity.getEducationOrgUnit(), entity.getCipher());
            if (null != result)
            {
                return result;
            }

            this.save(entity);
            this.executeFlush();
            return entity;
        }
    }

    @Override
    public UniscEduOrgUnit getUniscEduOrgUnit(final EducationYear year, final EducationOrgUnit eduOrgUnit, String cipher)
    {
        cipher = StringUtils.trimToEmpty(cipher);

        synchronized (UniscEduOrgUnitConfigDAO.MUTEX)
        {
            final UniscEduOrgUnit result = this.find(year, eduOrgUnit, cipher);
            if (null != result)
            {
                return result;
            }

            /* save it */
            {
                final UniscEduOrgUnit e = new UniscEduOrgUnit();
                e.setCountTerm(2);
                e.setEducationOrgUnit(eduOrgUnit);
                e.setEducationYear(year);
                e.setCipher(cipher);

                /* do not use 'this.' here (i want new transaction) */
                return IUniscEduOrgUnitConfigDAO.INSTANCE.get().doSaveUniscEduOrgUnit(e);
            }
        }
    }


    @Override
    public List<UniscEduOrgUnitPayPlan> listPayPlans4EducationOrgUnit(final UniscEduOrgUnit unscEduOrgUnit)
    {
        final MQBuilder builder = new MQBuilder(UniscEduOrgUnitPayPlanGen.ENTITY_CLASS, "e");
        builder.addOrder("e", UniscEduOrgUnitPayPlanGen.L_UNISC_PAY_FREQ + "." + ICatalogItem.CATALOG_ITEM_CODE);
        builder.add(MQExpression.eq("e", UniscEduOrgUnitPayPlanGen.L_UNISC_EDU_ORG_UNIT + ".id", unscEduOrgUnit.getId()));
        return builder.<UniscEduOrgUnitPayPlan>getResultList(this.getSession());
    }

    @Override
    public List<UniscEduOrgUnitPayPlanRow> getPlanRows(
        final UniscEduOrgUnit uniscEduOrgUnit,
        final UniscPayPeriod payFreq,
        final int firstStage, final int lastStage,
        final boolean createIfEmpty
    )
    {
        final MQBuilder builder = new MQBuilder(UniscEduOrgUnitPayPlanRowGen.ENTITY_CLASS, "e");
        builder.addJoin("e", UniscEduOrgUnitPayPlanRowGen.L_PLAN, "plan");
        builder.add(MQExpression.eq("plan", UniscEduOrgUnitPayPlanGen.L_UNISC_EDU_ORG_UNIT + ".id", uniscEduOrgUnit.getId()));
        builder.add(MQExpression.eq("plan", UniscEduOrgUnitPayPlanGen.L_UNISC_PAY_FREQ + ".id", payFreq.getId()));
        builder.add(MQExpression.greatOrEq("e", UniscEduOrgUnitPayPlanRowGen.P_STAGE, firstStage));
        builder.add(MQExpression.lessOrEq("e", UniscEduOrgUnitPayPlanRowGen.P_STAGE, lastStage));
        builder.addOrder("e", UniscEduOrgUnitPayPlanRowGen.P_STAGE);
        builder.addOrder("e", UniscEduOrgUnitPayPlanRowGen.P_DATE);
        final List<UniscEduOrgUnitPayPlanRow> result = builder.<UniscEduOrgUnitPayPlanRow>getResultList(this.getSession());
        if (result.isEmpty() && createIfEmpty)
        {
            final IUniscIODao uniscIODao = IUniscIODao.INSTANCE.get();
            for (int i = firstStage; i <= lastStage; i++)
            {
                final UniscEduOrgUnitPayPlanRow row = new UniscEduOrgUnitPayPlanRow();
                row.setStage(i);
                row.setDate(uniscIODao.getDate(uniscEduOrgUnit, payFreq, i));
                row.setCost(0);
                row.setPlan(null);
                result.add(row);
            }
        }
        return result;
    }

    @Override
    public EducationOrgUnitCostEnvironment getEducationOrgUnitCostEnvironment(String educationYearTitle, String payPeriodCode, int stagePeriod)
    {
        List<Object[]> list = new DQLSelectBuilder().fromEntity(UniscEduOrgUnitPayPlanRow.class, "r")
        .column(DQLExpressions.property(UniscEduOrgUnitPayPlanRow.plan().uniscEduOrgUnit().educationOrgUnit().id().fromAlias("r")))
        .column(DQLExpressions.property(UniscEduOrgUnitPayPlanRow.plan().uniscEduOrgUnit().cipher().fromAlias("r")))
        .column(DQLFunctions.sum(DQLExpressions.property(UniscEduOrgUnitPayPlanRow.cost().fromAlias("r"))))
        .where(DQLExpressions.eq(DQLExpressions.property(UniscEduOrgUnitPayPlanRow.plan().uniscEduOrgUnit().educationYear().title().fromAlias("r")), DQLExpressions.value(educationYearTitle)))
        .where(DQLExpressions.eq(DQLExpressions.property(UniscEduOrgUnitPayPlanRow.plan().uniscPayFreq().code().fromAlias("r")), DQLExpressions.value(payPeriodCode)))
        .where(DQLExpressions.eq(DQLExpressions.property(UniscEduOrgUnitPayPlanRow.stage().fromAlias("r")), DQLExpressions.value(stagePeriod)))
        .group(DQLExpressions.property(UniscEduOrgUnitPayPlanRow.plan().uniscEduOrgUnit().educationOrgUnit().id().fromAlias("r")))
        .group(DQLExpressions.property(UniscEduOrgUnitPayPlanRow.plan().uniscEduOrgUnit().cipher().fromAlias("r")))
        .order(DQLExpressions.property(UniscEduOrgUnitPayPlanRow.plan().uniscEduOrgUnit().educationOrgUnit().id().fromAlias("r")))
        .order(DQLExpressions.property(UniscEduOrgUnitPayPlanRow.plan().uniscEduOrgUnit().cipher().fromAlias("r")))
        .createStatement(new DQLExecutionContext(getSession())).list();

        EducationOrgUnitCostEnvironment envNode = new EducationOrgUnitCostEnvironment();

        envNode.educationYearTitle = educationYearTitle;
        envNode.payPeriodCode = payPeriodCode;
        envNode.stagePeriod = stagePeriod;

        for (Object[] row : list)
            envNode.row.add(new EducationOrgUnitCostEnvironment.EducationOrgUnitCostNode(((Number) row[2]).longValue(), (String) row[1], row[0].toString()));

        return envNode;
    }
}

