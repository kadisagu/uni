package ru.tandemservice.unisc.component.student.StudentAgreementAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Output;

import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.basic.gen.EducationYearGen;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key="force", binding="force")
})
@Output({
    @Bind(key="defaultEducationOrgUnitId", binding="relation.target.educationOrgUnit.id"),
    @Bind(key="defaultEducationYearId", binding="year.id")
})
public class Model extends ru.tandemservice.unisc.component.agreement.AgreementRelationAddEdit.Model<Student> {

    private Boolean force = Boolean.FALSE;
    public Boolean getForce() { return this.force; }
    public void setForce(final Boolean force) { this.force = force; }

    public EducationYear getYear() {
        return UniDaoFacade.getCoreDao().get(EducationYear.class, EducationYearGen.P_INT_VALUE, this.getPersonRole().getEntranceYear());
    }


}
