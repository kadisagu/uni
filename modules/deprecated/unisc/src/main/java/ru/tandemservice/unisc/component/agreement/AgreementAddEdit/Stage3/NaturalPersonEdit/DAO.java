package ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage3.NaturalPersonEdit;

import org.tandemframework.shared.fias.base.bo.AddressBase.AddressBaseManager;
import org.tandemframework.shared.person.base.bo.Person.util.IssuancePlaceSelectModel;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson;

/**
 * @author vdanilov
 */
@SuppressWarnings("deprecation")
public class DAO extends UniBaseDao implements IDAO {

    @Override
    public void prepare(final Model model) {
        final UniscEduAgreementNaturalPerson naturalPerson = model.getNaturalPerson();
        if (null != naturalPerson) {
            if (!naturalPerson.isTransient()) {
                getSession().update(naturalPerson);
                if ((null != naturalPerson.getAddress()) && (null != naturalPerson.getAddress().getId())) {
                    getSession().update(naturalPerson.getAddress());
                }
            }
            reassociateEntityProperties(getSession(), naturalPerson);
        }
        model.setIssuancePlaceModel(new IssuancePlaceSelectModel());
    }

    @Override
    public void save(final Model model) {
        prepare(model);

        final UniscEduAgreementNaturalPerson naturalPerson = model.getNaturalPerson();
        if (null != naturalPerson) {
            naturalPerson.setAddress(model.getAddress());
            if (!naturalPerson.isTransient()) {
                AddressBaseManager.instance().dao().saveOrUpdateAddressWithoutEntityUpdate(naturalPerson, model.getAddress(), UniscEduAgreementNaturalPerson.L_ADDRESS);
                getSession().saveOrUpdate(naturalPerson);
//                if (null != naturalPerson.getAddress()) {
//                    getSession().saveOrUpdate(naturalPerson.getAddress());
//                }
            }
            reassociateEntityProperties(getSession(), naturalPerson);
        }


    }

}
