// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisc.component.orgunit.OrgUnitContractList;

import org.tandemframework.core.component.State;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

/**
 * @author oleyba
 * @since 02.09.2009
 */
@State(keys = "orgUnitId", bindings = "orgUnitId")
public class Model extends ru.tandemservice.unisc.component.list.StudentAgreementListBase.Model
{
    private long _orgUnitId;
    private OrgUnit _orgUnit;
    private CommonPostfixPermissionModel _secModel;

    public long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(long orgUnitId)
    {
        this._orgUnitId = orgUnitId;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }

    @Override
    public String getPrintPermissionKey()
    {
        return getSecModel().getPermission("printContract_list");
    }

    @Override
    public String getCheckedBySignaturePermissionKey()
    {
        return getSecModel().getPermission("editCheckedBySignature_list");
    }

    @Override
    public String getPrintAllPermissionKey()
    {
        return getSecModel().getPermission("printAllContracts_list");
    }

    @Override
    public String getListSettingsKey()
    {
        return getSecModel().getPermission("StudentContractList.contracts");
    }
}