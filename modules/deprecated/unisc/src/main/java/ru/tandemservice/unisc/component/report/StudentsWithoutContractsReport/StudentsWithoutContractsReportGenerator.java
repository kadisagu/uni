/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisc.component.report.StudentsWithoutContractsReport;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unisc.UniscDefines;
import ru.tandemservice.unisc.entity.agreements.UniscEduAdditAgreement;
import ru.tandemservice.unisc.entity.agreements.UniscEduMainAgreement;
import ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student;
import ru.tandemservice.unisc.entity.catalog.UniscTemplateDocument;

import java.util.*;

/**
 * @author agolubenko
 * @since May 24, 2010
 */
public class StudentsWithoutContractsReportGenerator
{
    private Session _session;
    private Model _model;

    public StudentsWithoutContractsReportGenerator(Session session, Model model)
    {
        _session = session;
        _model = model;
    }

    public byte[] generateReportContent()
    {
        ITemplateDocument templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniscTemplateDocument.class, UniscDefines.TEMPLATE_STUDENTS_WITHOUT_CONTRACTS_REPORT);
        RtfDocument document = new RtfReader().read(templateDocument.getContent());

        new RtfInjectModifier().put("formationDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date())).modify(document);

        RtfTableModifier rtfTableModifier = new RtfTableModifier();
        rtfTableModifier.put("N", getReportParameters());
        rtfTableModifier.put("T", getTable(getStudents()));
        rtfTableModifier.modify(document);

        return RtfUtil.toByteArray(document);
    }

    private String[][] getReportParameters()
    {
        List<String[]> result = new ArrayList<String[]>();
        if (_model.isEducationYearActive())
        {
            result.add(getParametersRow("Отсутствует действующий договор", Collections.singletonList(_model.getEducationYear())));
        }
        if (_model.isFormativeOrgUnitActive())
        {
            result.add(getParametersRow("Формирующее подр.", _model.getFormativeOrgUnitList()));
        }
        if (_model.isTerritorialOrgUnitActive())
        {
            String title = "Территориальное подр.";
            List<OrgUnit> territorialOrgUnitList = _model.getTerritorialOrgUnitList();

            result.add((!territorialOrgUnitList.isEmpty()) ? getParametersRow(title, territorialOrgUnitList) : new String[] { title, "Не указано" });
        }
        if (_model.isDevelopFormActive())
        {
            result.add(getParametersRow("Форма освоения", _model.getDevelopFormList()));
        }
        if (_model.isCourseActive())
        {
            result.add(getParametersRow("Курс", _model.getCourseList()));
        }
        if (_model.isStudentCategoryActive())
        {
            result.add(getParametersRow("Категория обучаемого", _model.getStudentCategoryList()));
        }
        if (_model.isStudentStatusActive())
        {
            result.add(getParametersRow("Состояние студента", _model.getStudentStatusList()));
        }
        return result.toArray(new String[result.size()][2]);
    }

    private <T extends IEntity> String[] getParametersRow(String title, List<T> values)
    {
        return new String[] { title, CollectionFormatter.COLLECTION_FORMATTER.format(values) };
    }

    @SuppressWarnings("unchecked")
    private List<Student> getStudents()
    {
        MQBuilder builder = new MQBuilder(Student.ENTITY_CLASS, "student");
        builder.addJoinFetch("student", Student.educationOrgUnit().s(), "educationOrgUnit");
        builder.addJoinFetch("educationOrgUnit", EducationOrgUnit.formativeOrgUnit().s(), "formativeOrgUnit");
        builder.addLeftJoinFetch("educationOrgUnit", EducationOrgUnit.territorialOrgUnit().s(), "territorialOrgUnit");
        builder.addLeftJoinFetch("student", Student.group().s(), "group_");
        builder.addJoinFetch("student", Student.person().s(), "person");
        builder.addJoinFetch("person", Person.identityCard().s(), "identityCard");
        builder.add(MQExpression.eq("student", Student.compensationType().code().s(), UniDefines.COMPENSATION_TYPE_CONTRACT));
        builder.add(MQExpression.eq("student", Student.archival().s(), false));

        // проверяем отсутствие договоров
        MQBuilder agreementsBuilder = new MQBuilder(UniscEduAgreement2Student.ENTITY_CLASS, "relation");
        agreementsBuilder.addJoin("relation", UniscEduAgreement2Student.agreement().s(), "agreement");
        agreementsBuilder.add(MQExpression.eqProperty("student", Student.id().s(), "relation", UniscEduAgreement2Student.student().id().s()));
        agreementsBuilder.add(MQExpression.eq("relation", UniscEduAgreement2Student.active().s(), true));
        if (_model.isEducationYearActive())
        {
            agreementsBuilder.add(MQExpression.eq("agreement", UniscEduMainAgreement.config().educationYear().s(), _model.getEducationYear()));
        }
        else
        {
            agreementsBuilder.add(MQExpression.greatOrEq("agreement", UniscEduMainAgreement.config().educationYear().intValue().s(), _model.getCurrentEducationYear().getIntValue()));
        }
        builder.add(MQExpression.notExists(agreementsBuilder));

        // если выбран год, то надо проверить и отсутствие доп.соглашений
        if (_model.isEducationYearActive())
        {
            MQBuilder additionalAgreementsBuilder = new MQBuilder(UniscEduAdditAgreement.ENTITY_CLASS, "additionalAgreement");
            additionalAgreementsBuilder.addJoin("additionalAgreement", UniscEduAdditAgreement.agreement().s(), "mainAgreement");
            additionalAgreementsBuilder.add(MQExpression.eq("additionalAgreement", UniscEduAdditAgreement.config().educationYear().s(), _model.getEducationYear()));

            additionalAgreementsBuilder.addDomain("additionalRelation", UniscEduAgreement2Student.ENTITY_CLASS);
            additionalAgreementsBuilder.add(MQExpression.eqProperty("student", Student.id().s(), "additionalRelation", UniscEduAgreement2Student.student().id().s()));
            additionalAgreementsBuilder.add(MQExpression.eqProperty("additionalAgreement", UniscEduAdditAgreement.agreement().id().s(), "additionalRelation", UniscEduAgreement2Student.agreement().id().s()));

            builder.add(MQExpression.notExists(additionalAgreementsBuilder));
        }

        // применяем фильтры
        builder.add(MQExpression.in("educationOrgUnit", EducationOrgUnit.formativeOrgUnit().s(), (_model.isFormativeOrgUnitActive()) ? _model.getFormativeOrgUnitList() : _model.getFormativeOrgUnitModel().findValues("").getObjects()));

        List<OrgUnit> territorialOrgUnitList = (_model.isTerritorialOrgUnitActive()) ? _model.getTerritorialOrgUnitList() : _model.getTerritorialOrgUnitModel().findValues("").getObjects();
        if (_model.isTerritorialOrgUnitActive())
        {
            if (!territorialOrgUnitList.isEmpty())
            {
                builder.add(MQExpression.in("educationOrgUnit", EducationOrgUnit.territorialOrgUnit().s(), territorialOrgUnitList));
            }
            else
            {
                builder.add(MQExpression.isNull("educationOrgUnit", EducationOrgUnit.territorialOrgUnit().s()));
            }            
        }
        if (_model.isDevelopFormActive())
        {
            builder.add(MQExpression.in("educationOrgUnit", EducationOrgUnit.developForm().s(), _model.getDevelopFormList()));
        }
        if (_model.isCourseActive())
        {
            builder.add(MQExpression.in("student", Student.course().s(), _model.getCourseList()));
        }
        if (_model.isStudentCategoryActive())
        {
            builder.add(MQExpression.in("student", Student.studentCategory().s(), _model.getStudentCategoryList()));
        }
        if (_model.isStudentStatusActive())
        {
            builder.add(MQExpression.in("student", Student.status().s(), _model.getStudentStatusList()));
        }

        List<Student> result = builder.<Student> getResultList(_session);
        Collections.sort(result, new Comparator<Student>()
        {
            @Override
            public int compare(Student o1, Student o2)
            {
                int result = UniBaseUtils.compare(getFormativeOrgUnitTitle(o1), getFormativeOrgUnitTitle(o2), false);
                if (result == 0)
                {
                    result = UniBaseUtils.compare(getTerritorialOrgUnitTitle(o1), getTerritorialOrgUnitTitle(o2), false);
                }
                if (result == 0)
                {
                    result = Long.compare((long) getCourse(o1), (long) getCourse(o2));
                }
                if (result == 0)
                {
                    result = UniBaseUtils.compare(getGroupTitle(o1), getGroupTitle(o2), false);
                }
                if (result == 0)
                {
                    result = UniBaseUtils.compare(getFullFio(o1), getFullFio(o2), false);
                }
                return result;
            }
        });
        return result;
    }

    private String[][] getTable(List<Student> students)
    {
        List<String[]> result = new ArrayList<String[]>();
        for (int i = 0; i < students.size(); i++)
        {
            Student student = students.get(i);

            String number = Integer.toString(i + 1);
            String fullFio = getFullFio(student);
            String formativeOrgUnitTitle = getFormativeOrgUnitTitle(student);
            String territorialOrgUnitTitle = getTerritorialOrgUnitTitle(student);
            String course = Integer.toString(getCourse(student));
            String groupTitle = getGroupTitle(student);
            String statusTitle = getStatusTitle(student);

            result.add(new String[] { number, fullFio, formativeOrgUnitTitle, territorialOrgUnitTitle, course, groupTitle, statusTitle });
        }
        return result.toArray(new String[result.size()][7]);
    }

    private String getFormativeOrgUnitTitle(Student student)
    {
        OrgUnit formativeOrgUnit = student.getEducationOrgUnit().getFormativeOrgUnit();
        return StringUtils.defaultIfEmpty(formativeOrgUnit.getNominativeCaseTitle(), formativeOrgUnit.getTypeTitle());
    }

    private String getTerritorialOrgUnitTitle(Student student)
    {
        OrgUnit territorialOrgUnit = student.getEducationOrgUnit().getTerritorialOrgUnit();
        return StringUtils.defaultIfEmpty(territorialOrgUnit.getNominativeCaseTitle(), territorialOrgUnit.getTypeTitle());
    }

    private int getCourse(Student student)
    {
        return student.getCourse().getIntValue();
    }

    private String getGroupTitle(Student student)
    {
        Group group = student.getGroup();
        return (group != null) ? group.getTitle() : null;
    }

    private String getFullFio(Student student)
    {
        return student.getPerson().getFullFio();
    }

    private String getStatusTitle(Student student)
    {
        return student.getStatus().getTitle();
    }
}
