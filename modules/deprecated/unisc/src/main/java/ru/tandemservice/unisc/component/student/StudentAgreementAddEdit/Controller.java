package ru.tandemservice.unisc.component.student.StudentAgreementAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.ContextLocal;

import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author vdanilov
 */
public class Controller extends ru.tandemservice.unisc.component.agreement.AgreementRelationAddEdit.Controller<Student> {
    @Override
    public void onRenderComponent(IBusinessComponent component)
    {
        ContextLocal.beginPageTitlePart(getModel(component).isCreationFormFlag() ? "Добавление договора" : "Редактирование договора");
    }
}
