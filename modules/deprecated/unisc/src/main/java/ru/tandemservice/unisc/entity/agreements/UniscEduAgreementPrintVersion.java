package ru.tandemservice.unisc.entity.agreements;

import java.io.OutputStream;

import org.tandemframework.core.document.CacheInfo;
import org.tandemframework.core.document.IDocumentRenderer;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.unisc.entity.agreements.gen.UniscEduAgreementPrintVersionGen;

/**
 * Печатная версия документа
 */
public class UniscEduAgreementPrintVersion extends UniscEduAgreementPrintVersionGen
{
    public IDocumentRenderer getRenderer() {
        final byte[] content = getFile().getContent();
        if (content == null)
            throw new ApplicationException("Файл документа пуст.");

        return new CommonBaseRenderer()
            .contentType(UniscEduAgreementPrintVersion.this.getContentType())
            .fileName(UniscEduAgreementPrintVersion.this.getFileName())
            .document(content);
    }
}