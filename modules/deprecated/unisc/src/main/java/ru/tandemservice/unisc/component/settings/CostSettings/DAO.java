package ru.tandemservice.unisc.component.settings.CostSettings;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.education.gen.EducationOrgUnitGen;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;
import ru.tandemservice.uni.util.EducationLevelUtil;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.basic.gen.EducationYearGen;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;
import ru.tandemservice.unisc.entity.config.gen.UniscEduOrgUnitGen;
import ru.tandemservice.unisc.entity.config.gen.UniscEduOrgUnitPayPlanGen;
import ru.tandemservice.unisc.entity.config.gen.UniscEduOrgUnitPayPlanRowGen;

import java.util.Collections;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    @SuppressWarnings("deprecation")
    public void prepare(final Model model) {
        model.setYears(new EducationYearModel());
        if (null == model.getEducationYear()) { model.setEducationYear(get(EducationYear.class, EducationYearGen.P_CURRENT,Boolean.TRUE)); }

        model.setCipherModel(new UniQueryFullCheckSelectModel(UniscEduOrgUnit.P_CIPHER_WITH_TITLE) {
            @Override protected MQBuilder query(final String alias, final String filter) {
                final MQBuilder builder = new MQBuilder(UniscEduOrgUnitGen.ENTITY_CLASS, alias);
                builder.addOrder(alias, UniscEduOrgUnitGen.L_EDUCATION_YEAR+".code");
                builder.addOrder(alias, UniscEduOrgUnitGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnitGen.L_EDUCATION_LEVEL_HIGH_SCHOOL+".code");
                builder.addOrder(alias, UniscEduOrgUnitGen.P_CIPHER);

                final EducationYear educationYear = model.getEducationYear();
                if (null != educationYear) {
                    builder.add(MQExpression.eq(alias, UniscEduOrgUnitGen.L_EDUCATION_YEAR+".id", educationYear.getId()));
                }
                if (null != filter) {
                    builder.add(MQExpression.like(alias, UniscEduOrgUnitGen.P_CIPHER, filter));
                }
                return builder;
            }
        });

        model.setFormativeOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING));
        model.setTerritorialOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL));
        model.setEducationLevelsHighSchoolModel(EducationLevelUtil.createEducationLevelsHighSchoolModel(model));
        model.setDevelopFormModel(EducationLevelUtil.createDevelopFormModel(model, true));
        model.setDevelopConditionModel(EducationLevelUtil.createDevelopConditionModel(model, true));
        model.setDevelopTechModel(EducationLevelUtil.createDevelopTechModel(model, true));
        model.setDevelopPeriodModel(EducationLevelUtil.createDevelopPeriodModel(model, true));

        super.prepare(model);
    }


    @Override
    public void refreshDataSource(final Model model) {
        final EducationYear educationYear = getEducationalYear(model);

        final UniscEduOrgUnit cipher = model.getCipher();
        if ((null != cipher) && ((null == educationYear) || cipher.getEducationYear().equals(educationYear))) {
            final DynamicListDataSource<UniscEduOrgUnit> dataSource = model.getDataSource();
            dataSource.setTotalSize(1);
            dataSource.createPage(Collections.singletonList(cipher));
            return;
        }

        final MQBuilder builder = new MQBuilder(UniscEduOrgUnitGen.ENTITY_CLASS, "eou");
        builder.add(MQExpression.eq("eou", UniscEduOrgUnitGen.L_EDUCATION_YEAR+".id", educationYear.getId()));
        builder.addJoin("eou", UniscEduOrgUnitGen.L_EDUCATION_ORG_UNIT, "edu");

        if (null != model.getFormativeOrgUnit()) {
            builder.add(MQExpression.eq("edu", EducationOrgUnitGen.L_FORMATIVE_ORG_UNIT+".id", model.getFormativeOrgUnit().getId()));
        }
        if (null != model.getTerritorialOrgUnit()) {
            builder.add(MQExpression.eq("edu", EducationOrgUnitGen.L_TERRITORIAL_ORG_UNIT+".id", model.getTerritorialOrgUnit().getId()));
        }

        if (null != model.getEducationLevelsHighSchool()) {
            builder.add(MQExpression.eq("edu", EducationOrgUnitGen.L_EDUCATION_LEVEL_HIGH_SCHOOL+".id", model.getEducationLevelsHighSchool().getId()));
        }

        if (null != model.getDevelopForm()) {
            builder.add(MQExpression.eq("edu", EducationOrgUnitGen.L_DEVELOP_FORM+".id", model.getDevelopForm().getId()));
        }
        if (null != model.getDevelopCondition()) {
            builder.add(MQExpression.eq("edu", EducationOrgUnitGen.L_DEVELOP_CONDITION+".id", model.getDevelopCondition().getId()));
        }
        if (null != model.getDevelopTech()) {
            builder.add(MQExpression.eq("edu", EducationOrgUnitGen.L_DEVELOP_TECH+".id", model.getDevelopTech().getId()));
        }
        if (null != model.getDevelopPeriod()) {
            builder.add(MQExpression.eq("edu", EducationOrgUnitGen.L_DEVELOP_PERIOD+".id", model.getDevelopPeriod().getId()));
        }

        final IdentifiableWrapper<IEntity> state = model.getSettings().get("state");
        if (null != state) {
            final MQBuilder configuredBuilder = new MQBuilder(UniscEduOrgUnitPayPlanRowGen.ENTITY_CLASS, "r", new String[] { UniscEduOrgUnitPayPlanRowGen.L_PLAN+"."+UniscEduOrgUnitPayPlanGen.L_UNISC_EDU_ORG_UNIT+".id" });
            configuredBuilder.add(MQExpression.eq("r", UniscEduOrgUnitPayPlanRowGen.L_PLAN+"."+UniscEduOrgUnitPayPlanGen.L_UNISC_EDU_ORG_UNIT+"."+UniscEduOrgUnitGen.L_EDUCATION_YEAR+".id", educationYear.getId()));
            configuredBuilder.setNeedDistinct(true);
            if (Model.STATE_CONFIGURE == state.getId()) {
                builder.add(MQExpression.in("eou", "id", configuredBuilder));
            } else if (Model.STATE_NOT_CONFIGURE == state.getId()) {
                builder.add(MQExpression.notIn("eou", "id", configuredBuilder));
            }
        }

        new OrderDescriptionRegistry("eou").applyOrder(builder, model.getDataSource().getEntityOrder());

        final DynamicListDataSource<UniscEduOrgUnit> dataSource = model.getDataSource();
        UniBaseUtils.createPage(dataSource, builder, getSession());
    }


    private EducationYear getEducationalYear(final Model model) {
        final EducationYear educationYear = model.getEducationYear();
        if (null != educationYear) { return educationYear; }
        return get(EducationYear.class, EducationYearGen.P_CURRENT, Boolean.TRUE);
    }

}
