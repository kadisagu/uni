package ru.tandemservice.unisc.component.agreement.AgreementPrintVersionsTab.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;


public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = getModel(component);
        getDao().prepare(model);
    }

    public void onClickApply(final IBusinessComponent component)
    {
        getDao().save(getModel(component));
        deactivate(component);
    }
}
