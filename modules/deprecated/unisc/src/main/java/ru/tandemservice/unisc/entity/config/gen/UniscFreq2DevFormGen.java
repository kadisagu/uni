package ru.tandemservice.unisc.entity.config.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.unisc.entity.catalog.UniscPayPeriod;
import ru.tandemservice.unisc.entity.config.UniscFreq2DevForm;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Зависимость способа оплаты от формы освоения
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniscFreq2DevFormGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisc.entity.config.UniscFreq2DevForm";
    public static final String ENTITY_NAME = "uniscFreq2DevForm";
    public static final int VERSION_HASH = 408297166;
    private static IEntityMeta ENTITY_META;

    public static final String L_FREQ = "freq";
    public static final String L_DEVELOP_FORM = "developForm";

    private UniscPayPeriod _freq;     // Сповоб оплат
    private DevelopForm _developForm;     // Форма освоения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сповоб оплат. Свойство не может быть null.
     */
    @NotNull
    public UniscPayPeriod getFreq()
    {
        return _freq;
    }

    /**
     * @param freq Сповоб оплат. Свойство не может быть null.
     */
    public void setFreq(UniscPayPeriod freq)
    {
        dirty(_freq, freq);
        _freq = freq;
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения. Свойство не может быть null.
     */
    public void setDevelopForm(DevelopForm developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UniscFreq2DevFormGen)
        {
            setFreq(((UniscFreq2DevForm)another).getFreq());
            setDevelopForm(((UniscFreq2DevForm)another).getDevelopForm());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniscFreq2DevFormGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniscFreq2DevForm.class;
        }

        public T newInstance()
        {
            return (T) new UniscFreq2DevForm();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "freq":
                    return obj.getFreq();
                case "developForm":
                    return obj.getDevelopForm();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "freq":
                    obj.setFreq((UniscPayPeriod) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((DevelopForm) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "freq":
                        return true;
                case "developForm":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "freq":
                    return true;
                case "developForm":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "freq":
                    return UniscPayPeriod.class;
                case "developForm":
                    return DevelopForm.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniscFreq2DevForm> _dslPath = new Path<UniscFreq2DevForm>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniscFreq2DevForm");
    }
            

    /**
     * @return Сповоб оплат. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.config.UniscFreq2DevForm#getFreq()
     */
    public static UniscPayPeriod.Path<UniscPayPeriod> freq()
    {
        return _dslPath.freq();
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.config.UniscFreq2DevForm#getDevelopForm()
     */
    public static DevelopForm.Path<DevelopForm> developForm()
    {
        return _dslPath.developForm();
    }

    public static class Path<E extends UniscFreq2DevForm> extends EntityPath<E>
    {
        private UniscPayPeriod.Path<UniscPayPeriod> _freq;
        private DevelopForm.Path<DevelopForm> _developForm;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сповоб оплат. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.config.UniscFreq2DevForm#getFreq()
     */
        public UniscPayPeriod.Path<UniscPayPeriod> freq()
        {
            if(_freq == null )
                _freq = new UniscPayPeriod.Path<UniscPayPeriod>(L_FREQ, this);
            return _freq;
        }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.config.UniscFreq2DevForm#getDevelopForm()
     */
        public DevelopForm.Path<DevelopForm> developForm()
        {
            if(_developForm == null )
                _developForm = new DevelopForm.Path<DevelopForm>(L_DEVELOP_FORM, this);
            return _developForm;
        }

        public Class getEntityClass()
        {
            return UniscFreq2DevForm.class;
        }

        public String getEntityName()
        {
            return "uniscFreq2DevForm";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
