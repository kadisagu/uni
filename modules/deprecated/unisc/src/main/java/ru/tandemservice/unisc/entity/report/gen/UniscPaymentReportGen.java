package ru.tandemservice.unisc.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unisc.entity.report.UniscPaymentReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Отчет по оплатам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniscPaymentReportGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisc.entity.report.UniscPaymentReport";
    public static final String ENTITY_NAME = "uniscPaymentReport";
    public static final int VERSION_HASH = -1915514152;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONTENT = "content";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String P_FORMATIVE_ORG_UNIT_TITLE = "formativeOrgUnitTitle";
    public static final String P_TERRITORIAL_ORG_UNIT_TITLE = "territorialOrgUnitTitle";
    public static final String P_EDU_LEVEL_TITLE = "eduLevelTitle";
    public static final String P_DEVELOP_FORM_TITLE = "developFormTitle";
    public static final String P_DEVELOP_CONDITION_TITLE = "developConditionTitle";
    public static final String P_DEVELOP_TECH_TITLE = "developTechTitle";
    public static final String P_DEVELOP_PERIOD_TITLE = "developPeriodTitle";
    public static final String P_SELECTION = "selection";
    public static final String P_COURSE = "course";
    public static final String P_GROUP = "group";
    public static final String P_DATE_FROM = "dateFrom";
    public static final String P_DATE_TO = "dateTo";

    private DatabaseFile _content;     // Печатная форма
    private Date _formingDate;     // Дата формирования
    private OrgUnit _orgUnit;     // Подразделение
    private String _formativeOrgUnitTitle;     // Формирующее подр.
    private String _territorialOrgUnitTitle;     // Территориальное подр.
    private String _eduLevelTitle;     // Направление подготовки
    private String _developFormTitle;     // Форма освоения
    private String _developConditionTitle;     // Условие освоения
    private String _developTechTitle;     // Технология освоения
    private String _developPeriodTitle;     // Период освоения
    private String _selection;     // Студенты\абитуриенты
    private String _course;     // Курс
    private String _group;     // Группа
    private Date _dateFrom;     // Начало периода
    private Date _dateTo;     // Конец периода

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Подразделение.
     */
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Формирующее подр..
     */
    @Length(max=255)
    public String getFormativeOrgUnitTitle()
    {
        return _formativeOrgUnitTitle;
    }

    /**
     * @param formativeOrgUnitTitle Формирующее подр..
     */
    public void setFormativeOrgUnitTitle(String formativeOrgUnitTitle)
    {
        dirty(_formativeOrgUnitTitle, formativeOrgUnitTitle);
        _formativeOrgUnitTitle = formativeOrgUnitTitle;
    }

    /**
     * @return Территориальное подр..
     */
    @Length(max=255)
    public String getTerritorialOrgUnitTitle()
    {
        return _territorialOrgUnitTitle;
    }

    /**
     * @param territorialOrgUnitTitle Территориальное подр..
     */
    public void setTerritorialOrgUnitTitle(String territorialOrgUnitTitle)
    {
        dirty(_territorialOrgUnitTitle, territorialOrgUnitTitle);
        _territorialOrgUnitTitle = territorialOrgUnitTitle;
    }

    /**
     * @return Направление подготовки.
     */
    public String getEduLevelTitle()
    {
        return _eduLevelTitle;
    }

    /**
     * @param eduLevelTitle Направление подготовки.
     */
    public void setEduLevelTitle(String eduLevelTitle)
    {
        dirty(_eduLevelTitle, eduLevelTitle);
        _eduLevelTitle = eduLevelTitle;
    }

    /**
     * @return Форма освоения.
     */
    @Length(max=255)
    public String getDevelopFormTitle()
    {
        return _developFormTitle;
    }

    /**
     * @param developFormTitle Форма освоения.
     */
    public void setDevelopFormTitle(String developFormTitle)
    {
        dirty(_developFormTitle, developFormTitle);
        _developFormTitle = developFormTitle;
    }

    /**
     * @return Условие освоения.
     */
    @Length(max=255)
    public String getDevelopConditionTitle()
    {
        return _developConditionTitle;
    }

    /**
     * @param developConditionTitle Условие освоения.
     */
    public void setDevelopConditionTitle(String developConditionTitle)
    {
        dirty(_developConditionTitle, developConditionTitle);
        _developConditionTitle = developConditionTitle;
    }

    /**
     * @return Технология освоения.
     */
    @Length(max=255)
    public String getDevelopTechTitle()
    {
        return _developTechTitle;
    }

    /**
     * @param developTechTitle Технология освоения.
     */
    public void setDevelopTechTitle(String developTechTitle)
    {
        dirty(_developTechTitle, developTechTitle);
        _developTechTitle = developTechTitle;
    }

    /**
     * @return Период освоения.
     */
    @Length(max=255)
    public String getDevelopPeriodTitle()
    {
        return _developPeriodTitle;
    }

    /**
     * @param developPeriodTitle Период освоения.
     */
    public void setDevelopPeriodTitle(String developPeriodTitle)
    {
        dirty(_developPeriodTitle, developPeriodTitle);
        _developPeriodTitle = developPeriodTitle;
    }

    /**
     * @return Студенты\абитуриенты.
     */
    @Length(max=255)
    public String getSelection()
    {
        return _selection;
    }

    /**
     * @param selection Студенты\абитуриенты.
     */
    public void setSelection(String selection)
    {
        dirty(_selection, selection);
        _selection = selection;
    }

    /**
     * @return Курс.
     */
    @Length(max=255)
    public String getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс.
     */
    public void setCourse(String course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Группа.
     */
    @Length(max=255)
    public String getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа.
     */
    public void setGroup(String group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Начало периода. Свойство не может быть null.
     */
    @NotNull
    public Date getDateFrom()
    {
        return _dateFrom;
    }

    /**
     * @param dateFrom Начало периода. Свойство не может быть null.
     */
    public void setDateFrom(Date dateFrom)
    {
        dirty(_dateFrom, dateFrom);
        _dateFrom = dateFrom;
    }

    /**
     * @return Конец периода. Свойство не может быть null.
     */
    @NotNull
    public Date getDateTo()
    {
        return _dateTo;
    }

    /**
     * @param dateTo Конец периода. Свойство не может быть null.
     */
    public void setDateTo(Date dateTo)
    {
        dirty(_dateTo, dateTo);
        _dateTo = dateTo;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UniscPaymentReportGen)
        {
            setContent(((UniscPaymentReport)another).getContent());
            setFormingDate(((UniscPaymentReport)another).getFormingDate());
            setOrgUnit(((UniscPaymentReport)another).getOrgUnit());
            setFormativeOrgUnitTitle(((UniscPaymentReport)another).getFormativeOrgUnitTitle());
            setTerritorialOrgUnitTitle(((UniscPaymentReport)another).getTerritorialOrgUnitTitle());
            setEduLevelTitle(((UniscPaymentReport)another).getEduLevelTitle());
            setDevelopFormTitle(((UniscPaymentReport)another).getDevelopFormTitle());
            setDevelopConditionTitle(((UniscPaymentReport)another).getDevelopConditionTitle());
            setDevelopTechTitle(((UniscPaymentReport)another).getDevelopTechTitle());
            setDevelopPeriodTitle(((UniscPaymentReport)another).getDevelopPeriodTitle());
            setSelection(((UniscPaymentReport)another).getSelection());
            setCourse(((UniscPaymentReport)another).getCourse());
            setGroup(((UniscPaymentReport)another).getGroup());
            setDateFrom(((UniscPaymentReport)another).getDateFrom());
            setDateTo(((UniscPaymentReport)another).getDateTo());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniscPaymentReportGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniscPaymentReport.class;
        }

        public T newInstance()
        {
            return (T) new UniscPaymentReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "content":
                    return obj.getContent();
                case "formingDate":
                    return obj.getFormingDate();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "formativeOrgUnitTitle":
                    return obj.getFormativeOrgUnitTitle();
                case "territorialOrgUnitTitle":
                    return obj.getTerritorialOrgUnitTitle();
                case "eduLevelTitle":
                    return obj.getEduLevelTitle();
                case "developFormTitle":
                    return obj.getDevelopFormTitle();
                case "developConditionTitle":
                    return obj.getDevelopConditionTitle();
                case "developTechTitle":
                    return obj.getDevelopTechTitle();
                case "developPeriodTitle":
                    return obj.getDevelopPeriodTitle();
                case "selection":
                    return obj.getSelection();
                case "course":
                    return obj.getCourse();
                case "group":
                    return obj.getGroup();
                case "dateFrom":
                    return obj.getDateFrom();
                case "dateTo":
                    return obj.getDateTo();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "formativeOrgUnitTitle":
                    obj.setFormativeOrgUnitTitle((String) value);
                    return;
                case "territorialOrgUnitTitle":
                    obj.setTerritorialOrgUnitTitle((String) value);
                    return;
                case "eduLevelTitle":
                    obj.setEduLevelTitle((String) value);
                    return;
                case "developFormTitle":
                    obj.setDevelopFormTitle((String) value);
                    return;
                case "developConditionTitle":
                    obj.setDevelopConditionTitle((String) value);
                    return;
                case "developTechTitle":
                    obj.setDevelopTechTitle((String) value);
                    return;
                case "developPeriodTitle":
                    obj.setDevelopPeriodTitle((String) value);
                    return;
                case "selection":
                    obj.setSelection((String) value);
                    return;
                case "course":
                    obj.setCourse((String) value);
                    return;
                case "group":
                    obj.setGroup((String) value);
                    return;
                case "dateFrom":
                    obj.setDateFrom((Date) value);
                    return;
                case "dateTo":
                    obj.setDateTo((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "content":
                        return true;
                case "formingDate":
                        return true;
                case "orgUnit":
                        return true;
                case "formativeOrgUnitTitle":
                        return true;
                case "territorialOrgUnitTitle":
                        return true;
                case "eduLevelTitle":
                        return true;
                case "developFormTitle":
                        return true;
                case "developConditionTitle":
                        return true;
                case "developTechTitle":
                        return true;
                case "developPeriodTitle":
                        return true;
                case "selection":
                        return true;
                case "course":
                        return true;
                case "group":
                        return true;
                case "dateFrom":
                        return true;
                case "dateTo":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "content":
                    return true;
                case "formingDate":
                    return true;
                case "orgUnit":
                    return true;
                case "formativeOrgUnitTitle":
                    return true;
                case "territorialOrgUnitTitle":
                    return true;
                case "eduLevelTitle":
                    return true;
                case "developFormTitle":
                    return true;
                case "developConditionTitle":
                    return true;
                case "developTechTitle":
                    return true;
                case "developPeriodTitle":
                    return true;
                case "selection":
                    return true;
                case "course":
                    return true;
                case "group":
                    return true;
                case "dateFrom":
                    return true;
                case "dateTo":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "content":
                    return DatabaseFile.class;
                case "formingDate":
                    return Date.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "formativeOrgUnitTitle":
                    return String.class;
                case "territorialOrgUnitTitle":
                    return String.class;
                case "eduLevelTitle":
                    return String.class;
                case "developFormTitle":
                    return String.class;
                case "developConditionTitle":
                    return String.class;
                case "developTechTitle":
                    return String.class;
                case "developPeriodTitle":
                    return String.class;
                case "selection":
                    return String.class;
                case "course":
                    return String.class;
                case "group":
                    return String.class;
                case "dateFrom":
                    return Date.class;
                case "dateTo":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniscPaymentReport> _dslPath = new Path<UniscPaymentReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniscPaymentReport");
    }
            

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.report.UniscPaymentReport#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.report.UniscPaymentReport#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.unisc.entity.report.UniscPaymentReport#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.unisc.entity.report.UniscPaymentReport#getFormativeOrgUnitTitle()
     */
    public static PropertyPath<String> formativeOrgUnitTitle()
    {
        return _dslPath.formativeOrgUnitTitle();
    }

    /**
     * @return Территориальное подр..
     * @see ru.tandemservice.unisc.entity.report.UniscPaymentReport#getTerritorialOrgUnitTitle()
     */
    public static PropertyPath<String> territorialOrgUnitTitle()
    {
        return _dslPath.territorialOrgUnitTitle();
    }

    /**
     * @return Направление подготовки.
     * @see ru.tandemservice.unisc.entity.report.UniscPaymentReport#getEduLevelTitle()
     */
    public static PropertyPath<String> eduLevelTitle()
    {
        return _dslPath.eduLevelTitle();
    }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.unisc.entity.report.UniscPaymentReport#getDevelopFormTitle()
     */
    public static PropertyPath<String> developFormTitle()
    {
        return _dslPath.developFormTitle();
    }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.unisc.entity.report.UniscPaymentReport#getDevelopConditionTitle()
     */
    public static PropertyPath<String> developConditionTitle()
    {
        return _dslPath.developConditionTitle();
    }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.unisc.entity.report.UniscPaymentReport#getDevelopTechTitle()
     */
    public static PropertyPath<String> developTechTitle()
    {
        return _dslPath.developTechTitle();
    }

    /**
     * @return Период освоения.
     * @see ru.tandemservice.unisc.entity.report.UniscPaymentReport#getDevelopPeriodTitle()
     */
    public static PropertyPath<String> developPeriodTitle()
    {
        return _dslPath.developPeriodTitle();
    }

    /**
     * @return Студенты\абитуриенты.
     * @see ru.tandemservice.unisc.entity.report.UniscPaymentReport#getSelection()
     */
    public static PropertyPath<String> selection()
    {
        return _dslPath.selection();
    }

    /**
     * @return Курс.
     * @see ru.tandemservice.unisc.entity.report.UniscPaymentReport#getCourse()
     */
    public static PropertyPath<String> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Группа.
     * @see ru.tandemservice.unisc.entity.report.UniscPaymentReport#getGroup()
     */
    public static PropertyPath<String> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Начало периода. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.report.UniscPaymentReport#getDateFrom()
     */
    public static PropertyPath<Date> dateFrom()
    {
        return _dslPath.dateFrom();
    }

    /**
     * @return Конец периода. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.report.UniscPaymentReport#getDateTo()
     */
    public static PropertyPath<Date> dateTo()
    {
        return _dslPath.dateTo();
    }

    public static class Path<E extends UniscPaymentReport> extends EntityPath<E>
    {
        private DatabaseFile.Path<DatabaseFile> _content;
        private PropertyPath<Date> _formingDate;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PropertyPath<String> _formativeOrgUnitTitle;
        private PropertyPath<String> _territorialOrgUnitTitle;
        private PropertyPath<String> _eduLevelTitle;
        private PropertyPath<String> _developFormTitle;
        private PropertyPath<String> _developConditionTitle;
        private PropertyPath<String> _developTechTitle;
        private PropertyPath<String> _developPeriodTitle;
        private PropertyPath<String> _selection;
        private PropertyPath<String> _course;
        private PropertyPath<String> _group;
        private PropertyPath<Date> _dateFrom;
        private PropertyPath<Date> _dateTo;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.report.UniscPaymentReport#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.report.UniscPaymentReport#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(UniscPaymentReportGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.unisc.entity.report.UniscPaymentReport#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.unisc.entity.report.UniscPaymentReport#getFormativeOrgUnitTitle()
     */
        public PropertyPath<String> formativeOrgUnitTitle()
        {
            if(_formativeOrgUnitTitle == null )
                _formativeOrgUnitTitle = new PropertyPath<String>(UniscPaymentReportGen.P_FORMATIVE_ORG_UNIT_TITLE, this);
            return _formativeOrgUnitTitle;
        }

    /**
     * @return Территориальное подр..
     * @see ru.tandemservice.unisc.entity.report.UniscPaymentReport#getTerritorialOrgUnitTitle()
     */
        public PropertyPath<String> territorialOrgUnitTitle()
        {
            if(_territorialOrgUnitTitle == null )
                _territorialOrgUnitTitle = new PropertyPath<String>(UniscPaymentReportGen.P_TERRITORIAL_ORG_UNIT_TITLE, this);
            return _territorialOrgUnitTitle;
        }

    /**
     * @return Направление подготовки.
     * @see ru.tandemservice.unisc.entity.report.UniscPaymentReport#getEduLevelTitle()
     */
        public PropertyPath<String> eduLevelTitle()
        {
            if(_eduLevelTitle == null )
                _eduLevelTitle = new PropertyPath<String>(UniscPaymentReportGen.P_EDU_LEVEL_TITLE, this);
            return _eduLevelTitle;
        }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.unisc.entity.report.UniscPaymentReport#getDevelopFormTitle()
     */
        public PropertyPath<String> developFormTitle()
        {
            if(_developFormTitle == null )
                _developFormTitle = new PropertyPath<String>(UniscPaymentReportGen.P_DEVELOP_FORM_TITLE, this);
            return _developFormTitle;
        }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.unisc.entity.report.UniscPaymentReport#getDevelopConditionTitle()
     */
        public PropertyPath<String> developConditionTitle()
        {
            if(_developConditionTitle == null )
                _developConditionTitle = new PropertyPath<String>(UniscPaymentReportGen.P_DEVELOP_CONDITION_TITLE, this);
            return _developConditionTitle;
        }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.unisc.entity.report.UniscPaymentReport#getDevelopTechTitle()
     */
        public PropertyPath<String> developTechTitle()
        {
            if(_developTechTitle == null )
                _developTechTitle = new PropertyPath<String>(UniscPaymentReportGen.P_DEVELOP_TECH_TITLE, this);
            return _developTechTitle;
        }

    /**
     * @return Период освоения.
     * @see ru.tandemservice.unisc.entity.report.UniscPaymentReport#getDevelopPeriodTitle()
     */
        public PropertyPath<String> developPeriodTitle()
        {
            if(_developPeriodTitle == null )
                _developPeriodTitle = new PropertyPath<String>(UniscPaymentReportGen.P_DEVELOP_PERIOD_TITLE, this);
            return _developPeriodTitle;
        }

    /**
     * @return Студенты\абитуриенты.
     * @see ru.tandemservice.unisc.entity.report.UniscPaymentReport#getSelection()
     */
        public PropertyPath<String> selection()
        {
            if(_selection == null )
                _selection = new PropertyPath<String>(UniscPaymentReportGen.P_SELECTION, this);
            return _selection;
        }

    /**
     * @return Курс.
     * @see ru.tandemservice.unisc.entity.report.UniscPaymentReport#getCourse()
     */
        public PropertyPath<String> course()
        {
            if(_course == null )
                _course = new PropertyPath<String>(UniscPaymentReportGen.P_COURSE, this);
            return _course;
        }

    /**
     * @return Группа.
     * @see ru.tandemservice.unisc.entity.report.UniscPaymentReport#getGroup()
     */
        public PropertyPath<String> group()
        {
            if(_group == null )
                _group = new PropertyPath<String>(UniscPaymentReportGen.P_GROUP, this);
            return _group;
        }

    /**
     * @return Начало периода. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.report.UniscPaymentReport#getDateFrom()
     */
        public PropertyPath<Date> dateFrom()
        {
            if(_dateFrom == null )
                _dateFrom = new PropertyPath<Date>(UniscPaymentReportGen.P_DATE_FROM, this);
            return _dateFrom;
        }

    /**
     * @return Конец периода. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.report.UniscPaymentReport#getDateTo()
     */
        public PropertyPath<Date> dateTo()
        {
            if(_dateTo == null )
                _dateTo = new PropertyPath<Date>(UniscPaymentReportGen.P_DATE_TO, this);
            return _dateTo;
        }

        public Class getEntityClass()
        {
            return UniscPaymentReport.class;
        }

        public String getEntityName()
        {
            return "uniscPaymentReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
