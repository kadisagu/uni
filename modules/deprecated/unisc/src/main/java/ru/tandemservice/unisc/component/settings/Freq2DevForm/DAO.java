/* $Id: DAO.java 6933 2009-03-07 05:41:27Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unisc.component.settings.Freq2DevForm;

import java.util.List;
import java.util.Map;

import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.tapsupport.component.matrix.CellCoordinates;

import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.unisc.entity.catalog.UniscPayPeriod;
import ru.tandemservice.unisc.entity.config.UniscFreq2DevForm;

/**
 * @author vdanilov
 */
public class DAO extends org.tandemframework.shared.commonbase.base.util.AbstractMatrixComponent.DAO<Model, DevelopForm, UniscPayPeriod, Boolean, UniscFreq2DevForm> implements IDAO
{
	@Override
	public void update(final Model model) {
		final Map<CellCoordinates, UniscFreq2DevForm> ids = fillMatrixData(model);
		for (final CellCoordinates pair : model.getMatrixData().keySet()) {
			if (model.getMatrixData().get(pair)) {
				if (!ids.containsKey(pair)) {
					final UniscFreq2DevForm relation = new UniscFreq2DevForm();
					relation.setDevelopForm(get(DevelopForm.class, pair.getRowId()));
					relation.setFreq(get(UniscPayPeriod.class, pair.getColumnId()));
					getSession().save(relation);
				}
			} else {
				if (ids.containsKey(pair)) {
					getSession().delete(ids.get(pair));
				}
			}
		}
	}


	@Override
	protected List<DevelopForm> getVerticalObjects(final Model model) {
		return CatalogManager.instance().dao().getCatalogItemListOrderByCode(DevelopForm.class);
	}

	@Override
	protected List<UniscPayPeriod> getHorizontalObjects(final Model model) {
		return CatalogManager.instance().dao().getCatalogItemListOrderByCode(UniscPayPeriod.class);
	}

	@Override
	protected Long getLeftId(final UniscFreq2DevForm object) {
		return object.getDevelopForm().getId();
	}

	@Override
	protected Long getRightId(final UniscFreq2DevForm object) {
		return object.getFreq().getId();
	}

	@Override
	protected Boolean getValue(final UniscFreq2DevForm object) {
		return null != object.getId();
	}

	@Override
	protected List<UniscFreq2DevForm> getObjects(final Model model) {
		return getList(UniscFreq2DevForm.class);
	}

}
