package ru.tandemservice.unisc.entity.config;

import ru.tandemservice.unisc.entity.catalog.UniscPayPeriod.Freq;
import ru.tandemservice.unisc.entity.config.gen.UniscEduOrgUnitPayPlanGen;

/**
 * Строка графика оплат (настройка)
 */
public class UniscEduOrgUnitPayPlan extends UniscEduOrgUnitPayPlanGen
{
	public boolean isYear() {
		return Freq.YEAR.equals(getUniscPayFreq().freq());
	}
}
