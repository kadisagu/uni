/* $Id: DAO.java 6933 2009-03-07 05:41:27Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unisc.component.settings.CostSettings.io.Export;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unisc.dao.io.IUniscIODao;
import ru.tandemservice.unisc.dao.io.IUniscIODao.Block;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

/**
 * @author vdanilov
 */
public class DAO extends UniBaseDao implements IDAO {

	@Override public void render(final Model model, final OutputStream stream) throws IOException
	{
		final List<Block> blockList = IUniscIODao.INSTANCE.get().getExportData(null);
		for (final Block block: blockList) {
			{
				stream.write(("\"#\"," + "\"" + block.getBlockTitle() + "\"" + "\n").getBytes("UTF-8"));
			}
			{
				final StringBuilder sb = new StringBuilder();
				sb.append("\"#\",");
				sb.append("\"Формирующее подр.\",");
				sb.append("\"Территориальное подр.\",");
				sb.append("\"Направление подготовки (cпециальность)\",");
				sb.append("\"Форма освоения\",");
				sb.append("\"Условия освоения\",");
				sb.append("\"Технология освоения\",");
				sb.append("\"Срок освоения\",");
				sb.append("\"Шифр\",");
				for (final String title: block.getValueCaptions()) {
					sb.append("\"").append(title).append("\",");
				}
				sb.append("\"\"\n");
				stream.write(sb.toString().getBytes("UTF-8"));
			}

			for (final Map.Entry<EducationOrgUnit, Map<String, Object[]>> e0: block.getRows().entrySet()) {
				final EducationOrgUnit edu = e0.getKey();
				for (final Map.Entry<String, Object[]> e1: e0.getValue().entrySet()) {
					final String cipher = e1.getKey();
					final StringBuilder sb = new StringBuilder();
					sb.append("\"").append(edu.getId()).append("\",");
					sb.append("\"").append((null == edu.getFormativeOrgUnit() ? "" : edu.getFormativeOrgUnit().getTypeTitle())).append("\",");
					sb.append("\"").append((null == edu.getTerritorialOrgUnit() ? "" : edu.getTerritorialOrgUnit().getTypeTitle())).append("\",");
					sb.append("\"").append(edu.getTitle()).append("\",");
					sb.append("\"").append(edu.getDevelopForm().getTitle()).append("\",");
					sb.append("\"").append(edu.getDevelopCondition().getTitle()).append("\",");
					sb.append("\"").append(edu.getDevelopTech().getTitle()).append("\",");
					sb.append("\"").append(edu.getDevelopPeriod().getTitle()).append("\",");
					sb.append("\"").append(cipher).append("\"");
					sb.append("\n");
					stream.write(sb.toString().getBytes("UTF-8"));
				}
			}
		}
	}
}
