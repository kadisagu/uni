package ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage0;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.tapsupport.component.selection.StaticSelectModel;
import ru.tandemservice.uni.dao.EducationLevelModel;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.gen.EducationLevelsHighSchoolGen;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.education.gen.EducationOrgUnitGen;
import ru.tandemservice.uni.ui.SelectModel;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;
import ru.tandemservice.uni.util.EducationLevelUtil;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.basic.gen.EducationYearGen;
import ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage.AbstractStageDAO;
import ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage.IStageResult;
import ru.tandemservice.unisc.dao.config.IUniscEduOrgUnitConfigDAO;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;
import ru.tandemservice.unisc.entity.config.gen.UniscEduOrgUnitGen;
import ru.tandemservice.unisc.utils.UniscPropertyUtils;

import java.util.Collections;
import java.util.Date;
import java.util.Map;

/**
 * @author vdanilov
 */
@SuppressWarnings("deprecation")
public class DAO extends AbstractStageDAO<Model> implements IDAO {

    @Override public boolean skip(final Model model) {
        return (model.getAgreement().getId() != null);
    }

    @Override
    protected IStageResult getResult(final Model model) {
        final UniscEduOrgUnit result = getUniscEduOrgUnit(model, true);
        assert result != null;
        final Date formingDate = result.getDefaultFormingDate();
        final Date deadlineDate = result.getDefaultDeadlineDate();
        final DevelopPeriod period = result.getEducationOrgUnit().getDevelopPeriod();

        return m -> {
            model.getAgreement().setConfig(result);
            model.getAgreement().setDevelopPeriodEduPlan(period);
            model.getAgreement().setDevelopPeriodGos(period);
            if (null != formingDate) { model.getAgreement().setFormingDate(formingDate); }
            if (null != deadlineDate) { model.getAgreement().setDeadlineDate(deadlineDate); }
            m.update(model.getAgreement(), false);
        };
    }

    private UniscEduOrgUnit getUniscEduOrgUnit(final Model model, final boolean check) {
        /* если что-то выбрано, то юзаем его */ {
            final UniscEduOrgUnit result = model.getCipherModel().getValue();
            if (null != result) { return result; }
        }

        final EducationYear educationYear = model.getEducationYearModel().getValue();
        if (null == educationYear) {
            if (check) { throw new ApplicationException("Не выбран учебный год."); }
            return null;
        }

        final EducationOrgUnit educationOrgUnit = UniDaoFacade.getEducationLevelDao().getEducationOrgUnit(model.getEducationLevelModel());
        if (null == educationOrgUnit) {
            if (check) { throw new ApplicationException("Не указано направление подготовки."); }
            return null;
        }

        if (UniscPropertyUtils.STRICT_CIPHER) {
            final UniscEduOrgUnit result = model.getCipherModel().getValue();
            if (null == result) {
                if (check) { throw new ApplicationException("Не указан шифр."); }
                return null;
            }
        }

        return IUniscEduOrgUnitConfigDAO.INSTANCE.get().getUniscEduOrgUnit(educationYear, educationOrgUnit, null);
    }

    @Override
    public void prepare(final Model model) {
        super.prepare(model);

        model.getEducationYearModel().setValue(reassociateEntity(getSession(), model.getEducationYearModel().getValue()));
        model.getCipherModel().setValue(reassociateEntity(getSession(), model.getCipherModel().getValue()));

        /* fill it */ {
            final UniscEduOrgUnit value = model.getCipherModel().getValue();
            if (null == value) {
                final UniscEduOrgUnit cfg = model.getAgreement().getConfig();
                if (null != cfg) {
                    final EducationYear selectedYear = model.getEducationYearModel().getValue();
                    if (null == selectedYear || selectedYear.equals(cfg.getEducationYear())) {
                        model.setDefaultEducationOrgUnitId(cfg.getEducationOrgUnit().getId());
                        model.setDefaultEducationYearId(cfg.getEducationYear().getId());
                        model.setDefaultUniscEduOrgUnitId(cfg.getId());
                    }
                }
            }
        }

        final boolean isReadOnly = (null != model.getAgreement().getId());
        prepareChipherModel(model, isReadOnly);
        prepareEducationYearModel(model, isReadOnly);
        prepareEducationOrgUnitModel(model, isReadOnly);
    }


    protected void prepareChipherModel(final Model model, final boolean isReadOnly) {
        final SelectModel<UniscEduOrgUnit> cipherModel = model.getCipherModel();
        if (isReadOnly) {
            cipherModel.setSource(new StaticSelectModel("id", UniscEduOrgUnit.P_CIPHER_WITH_TITLE, Collections.singletonList(cipherModel.getValue())));
        } else {
            if (null == cipherModel.getSource()) {
                cipherModel.setSource(new UniQueryFullCheckSelectModel(UniscEduOrgUnit.P_CIPHER_WITH_TITLE) {
                    @Override protected MQBuilder query(final String alias, final String filter) {
                        final MQBuilder builder = fillUniScEduOrgUnitBuilder(model, alias);
                        builder.addOrder(alias, UniscEduOrgUnitGen.L_EDUCATION_YEAR+".code");
                        builder.addOrder(alias, UniscEduOrgUnitGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnitGen.L_EDUCATION_LEVEL_HIGH_SCHOOL+".code");
                        builder.addOrder(alias, UniscEduOrgUnitGen.P_CIPHER);
                        if (null != filter) {
                            builder.add(MQExpression.or(
                                    MQExpression.like(alias, UniscEduOrgUnitGen.P_CIPHER, CoreStringUtils.escapeLike(filter)),
                                    MQExpression.like(alias, UniscEduOrgUnitGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnitGen.P_SHORT_TITLE, CoreStringUtils.escapeLike(filter)),
                                    MQExpression.like(alias, UniscEduOrgUnitGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnitGen.L_EDUCATION_LEVEL_HIGH_SCHOOL+".title", CoreStringUtils.escapeLike(filter)),
                                    MQExpression.like(alias, UniscEduOrgUnitGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnitGen.L_EDUCATION_LEVEL_HIGH_SCHOOL+"."+EducationLevelsHighSchoolGen.P_SHORT_TITLE, CoreStringUtils.escapeLike(filter))
                            ));
                        }
                        return builder;
                    }
                });
            }
        }
    }

    protected void prepareEducationYearModel(final Model model, final boolean isReadOnly) {
        final SelectModel<EducationYear> educationYearModel = model.getEducationYearModel();
        final UniscEduOrgUnit value = model.getCipherModel().getValue();
        if (isReadOnly) {
            if (null != value) {
                educationYearModel.setValue(value.getEducationYear());
                educationYearModel.setSource(new StaticSelectModel("id", "title", Collections.singletonList(educationYearModel.getValue())));
            } else {
                throw new IllegalStateException("wtf");
            }
        } else {
            if (null == educationYearModel.getSource()) {
                educationYearModel.setSource(new StaticSelectModel("id", "title", getCatalogItemList(EducationYear.class)));
            }
            if (null == educationYearModel.getValue()) {

                if (null == value) {
                    educationYearModel.setValue(get(EducationYear.class, EducationYearGen.P_CURRENT, Boolean.TRUE));
                } else {
                    educationYearModel.setValue(value.getEducationYear());
                }
            }
        }
    }

    protected void prepareEducationOrgUnitModel(final Model model, final boolean isReadOnly) {
        if (isReadOnly) {
            final UniscEduOrgUnit value = model.getCipherModel().getValue();
            if (null != value) {
                final EducationLevelModel educationLevelModel = model.getEducationLevelModel();
                educationLevelModel.fromEducationOrgUnit(value.getEducationOrgUnit(), true);
            } else {
                throw new IllegalStateException("wtf");
            }
        } else {
            final EducationLevelModel educationLevelModel = model.getEducationLevelModel();
            final UniscEduOrgUnit value = getUniscEduOrgUnit(model, false);
            if (null == value) {
                fillEducationOrgUnitModel(model, educationLevelModel);
            } else {
                educationLevelModel.fromEducationOrgUnit(value.getEducationOrgUnit(), true);
            }
        }
    }


    protected void fillEducationOrgUnitModel(final Model model, final EducationLevelModel educationLevelModel) {
        EducationLevelUtil.prepareEducationLevelModel(educationLevelModel, false);
    }

    protected MQBuilder fillUniScEduOrgUnitBuilder(final Model model, final String domainName) {
        final MQBuilder builder = new MQBuilder(UniscEduOrgUnitGen.ENTITY_CLASS, domainName);
        final EducationYear educationYear = model.getEducationYearModel().getValue();
        if (null != educationYear) {
            builder.add(MQExpression.eq(domainName, UniscEduOrgUnitGen.L_EDUCATION_YEAR+".id", educationYear.getId()));
        }

        if (model.getEducationLevelModel().isFilled()) {
            final String joinAlias = domainName+"_edu";
            builder.addJoin(domainName, UniscEduOrgUnitGen.L_EDUCATION_ORG_UNIT, joinAlias);
            for (final Map.Entry<String, String> e: EducationLevelModel.TRANSLATIONS.entrySet()) {
                final Object v = FastBeanUtils.getValue(model.getEducationLevelModel(), e.getValue());
                builder.add(null == v ? MQExpression.isNull(joinAlias, e.getKey()) : MQExpression.eq(joinAlias, e.getKey(), v));
            }
        }
        return builder;
    }


}
