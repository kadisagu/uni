// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisc.component.report.PayDebtorsReport;

import java.util.Date;

import org.tandemframework.core.component.State;
import org.tandemframework.sec.runtime.SecurityRuntime;

import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unisc.component.report.UniscReportAddModel;

/**
 * @author oleyba
 * @since 26.08.2009
 */
@State(keys = "orgUnitId", bindings = "orgUnitId")
public class Model extends UniscReportAddModel
{
    private OrgUnit orgUnit;
    private byte[] _content;
    private Date _dateFrom;
    private Date _dateTo;

    private CommonPostfixPermissionModel secModel;

    public Object getSecuredObject()
    {
        return null == getOrgUnit() ? SecurityRuntime.getInstance().getCommonSecurityObject() : getOrgUnit();
    }    

    public String getViewKey()
    {
        return null == getOrgUnit() ? "scPayDebtorsReport" : getSecModel().getPermission("viewPayDebtorsReport");
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        this.secModel = secModel;
    }

    public OrgUnit getOrgUnit()
    {
        return orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        this.orgUnit = orgUnit;
    }

    public byte[] getContent()
    {
        return _content;
    }

    public void setContent(byte[] content)
    {
        _content = content;
    }

    public Date getDateTo()
    {
        return _dateTo;
    }

    public void setDateTo(Date dateTo)
    {
        _dateTo = dateTo;
    }

    public Date getDateFrom()
    {
        return _dateFrom;
    }

    public void setDateFrom(Date dateFrom)
    {
        _dateFrom = dateFrom;
    }
}
