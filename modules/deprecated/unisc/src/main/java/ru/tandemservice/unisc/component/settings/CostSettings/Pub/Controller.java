package ru.tandemservice.unisc.component.settings.CostSettings.Pub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.AbstractListDataSource;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.ui.formatters.MoneyFormatter;
import ru.tandemservice.unisc.dao.io.IUniscIODao;
import ru.tandemservice.unisc.entity.catalog.UniscPayPeriod;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlan;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlanRow;
import ru.tandemservice.unisc.entity.config.gen.UniscEduOrgUnitPayPlanRowGen;

import java.util.Collections;
import java.util.Date;

public class Controller extends AbstractBusinessController<IDAO, Model> {

	private static final IFormatter<Date> dateFormatter = new DateFormatter("dd.MM.yy");

	@Override
	public void onRefreshComponent(final IBusinessComponent component) {
		final Model model = this.getModel(component);
		this.getDao().prepare(model);

		for (final Model.Block block: model.getBlockList()) {
			final AbstractListDataSource<UniscEduOrgUnitPayPlanRow> dataSource = block.getDataSource();
			if (0 == dataSource.getColumns().size()) {
				final UniscPayPeriod freq = block.getPlan().getUniscPayFreq();
				dataSource.addColumn(new SimpleColumn("Этап оплаты", UniscEduOrgUnitPayPlanRowGen.P_STAGE) {
					@Override public String getContent(final IEntity entity) {
						return freq.toString(block.getPlan().getUniscEduOrgUnit(), ((UniscEduOrgUnitPayPlanRow)entity).getStage());
					}
				}.setClickable(false).setOrderable(false).setMergeRowIdResolver(entity -> String.valueOf(((UniscEduOrgUnitPayPlanRow) entity).getStage())).setMergeRows(true).setClickable(false).setOrderable(false));
				dataSource.addColumn(new SimpleColumn("Дата оплаты", UniscEduOrgUnitPayPlanRowGen.P_DATE, Controller.dateFormatter).setClickable(false).setOrderable(false));
				dataSource.addColumn(new SimpleColumn("Сумма", UniscEduOrgUnitPayPlanRowGen.P_COST, MoneyFormatter.ruMoneyFormatter(3)).setClickable(false).setOrderable(false));
			}
		}
	}

	public void onClickReinit(final IBusinessComponent component) {
		//final Model model = this.getModel(component);
		//final IUniscIODao.ImportFacade facade = IUniscIODao.NOBR_IN_LINES.get().getImportFacade(model.getEduOrgUnit().getEducationYear());
		//facade.add(model.getEduOrgUnit().getEducationOrgUnit(), model.getEduOrgUnit().getCipher(), new ArrayList<String>().iterator());
		//facade.doImport();
	}

	public void onClickRecalculate(final IBusinessComponent component) {
		final Model model = getModel(component);
		this.getDao().doRecalculate(model);
		onRefreshComponent(component);
	}

	public void onClickEdit(final IBusinessComponent component) {
		final Model model = this.getModel(component);
		this.activate(component, new ComponentActivator(
				ru.tandemservice.unisc.component.settings.CostSettings.Edit.Controller.class.getPackage().getName(),
				Collections.<String, Object>singletonMap(PublisherActivator.PUBLISHER_ID_KEY, model.getId())
		));
	}

	public void onClickAddBlock(final IBusinessComponent component) {
		if (component.getListenerParameter() != null) {
			final Model model = this.getModel(component);
			final UniscEduOrgUnitPayPlan plan = IUniscIODao.INSTANCE.get().doGetPayPlan(model.getEduOrgUnit(), (UniscPayPeriod) UniDaoFacade.getCoreDao().get((Long)component.getListenerParameter()));
			this.activate(component, new ComponentActivator(
					ru.tandemservice.unisc.component.settings.CostSettings.EditPlan.Controller.class.getPackage().getName(),
					Collections.<String, Object>singletonMap(PublisherActivator.PUBLISHER_ID_KEY, plan.getId())
			));
		}
	}

	public void onClickDeleteBlock(final IBusinessComponent component) {
		if (component.getListenerParameter() != null) {
			final UniscEduOrgUnitPayPlan plan = UniDaoFacade.getCoreDao().get(UniscEduOrgUnitPayPlan.class, (Long)component.getListenerParameter());
			getDao().delete(plan);
			onRefreshComponent(component);
		}
	}


	public void onClickEditBlock(final IBusinessComponent component) {
		if (component.getListenerParameter() != null) {
			this.activate(component, new ComponentActivator(
					ru.tandemservice.unisc.component.settings.CostSettings.EditPlan.Controller.class.getPackage().getName(),
					Collections.singletonMap(PublisherActivator.PUBLISHER_ID_KEY, component.getListenerParameter())
			));
		}
	}

}
