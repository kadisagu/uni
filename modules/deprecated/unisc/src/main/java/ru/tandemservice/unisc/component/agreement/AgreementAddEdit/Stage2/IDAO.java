package ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage2;

import ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage.IAbstractStageDAO;

/**
 * @author vdanilov
 */
@SuppressWarnings("deprecation")
public interface IDAO extends IAbstractStageDAO<Model> {

}
