package ru.tandemservice.unisc.component.agreement.AgreementPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.AbstractListDataSource;
import org.tandemframework.core.view.list.source.IRowCustomizer;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;

import java.util.Collections;

public class Controller extends AbstractBusinessController<IDAO, Model> {
    @Override
    public void onRefreshComponent(final IBusinessComponent component) {
        final Model model = getModel(component);
        getDao().prepare(model);

        {
            final AbstractListDataSource<Model.PayPlanRow> ds = model.getPayPlanDataSource();
            ds.setRowCustomizer(new IRowCustomizer<Model.PayPlanRow>() {
                @Override public boolean isClickable(final IPublisherLinkColumn column, final Model.PayPlanRow rowEntity, final IEntity itemEntity) {
                    return false;
                }
                @Override public boolean isClickable(final AbstractColumn<?> column, final Model.PayPlanRow rowEntity) {
                    return false;
                }
                @Override public String getRowStyle(final Model.PayPlanRow entity) {
                    return null;
                }
                @Override public String getHeadRowStyle() {
                    return null;
                }
            });
            ds.getColumns().clear();

            final IMergeRowIdResolver payPlanRowDocumentIdResolver = entity -> {
                final Model.PayPlanRow row = (Model.PayPlanRow)entity;
                return ("row-"+String.valueOf(row.getPlanRow().getOwner().getId()));
            };

            final IMergeRowIdResolver payPlanRowIdResolver = entity -> {
                final Model.PayPlanRow row = (Model.PayPlanRow)entity;
                return ("row-"+String.valueOf(row.getPlanRow().getId()));
            };

            ds.addColumn(new SimpleColumn("Документ", "planRow.owner.title", NewLineFormatter.NOBR_IN_LINES).setClickable(false).setOrderable(false).setMergeRows(true).setMergeRowIdResolver(payPlanRowDocumentIdResolver));

            ds.addColumn(new SimpleColumn("Этап", "title").setClickable(false).setOrderable(false).setMergeRows(true).setMergeRowIdResolver(payPlanRowIdResolver));
            ds.addColumn(new SimpleColumn("Оплатить до", "planRow.date", DateFormatter.DEFAULT_DATE_FORMATTER).setWidth(1).setClickable(false).setOrderable(false).setMergeRows(true).setMergeRowIdResolver(payPlanRowIdResolver));

            final HeadColumn consts = new HeadColumn("costs", "Сумма оплаты");
            consts.addColumn(new SimpleColumn("физ. лицо", "planRow.NCostAsString", NewLineFormatter.NOBR_IN_LINES).setClickable(false).setOrderable(false).setMergeRows(true).setMergeRowIdResolver(payPlanRowIdResolver));
            consts.addColumn(new SimpleColumn("юр. лицо", "planRow.JCostAsString", NewLineFormatter.NOBR_IN_LINES).setClickable(false).setOrderable(false).setMergeRows(true).setMergeRowIdResolver(payPlanRowIdResolver));
            ds.addColumn(consts);

            ds.addColumn(new SimpleColumn("% Льготы", "planRow.discountAsString", NewLineFormatter.NOBR_IN_LINES).setClickable(false).setOrderable(false).setMergeRows(true).setMergeRowIdResolver(payPlanRowIdResolver));
            ds.addColumn(new SimpleColumn("Итого к оплате", "planRow.costAsString", NewLineFormatter.NOBR_IN_LINES).setClickable(false).setOrderable(false).setMergeRows(true).setMergeRowIdResolver(payPlanRowIdResolver));
            ds.addColumn(new SimpleColumn("Комментарий", "planRow.comment", NewLineFormatter.NOBR_IN_LINES).setClickable(false).setOrderable(false).setMergeRows(true).setMergeRowIdResolver(payPlanRowIdResolver));

            ds.addColumn(new SimpleColumn("Дата платежа", "factRow.date", DateFormatter.DEFAULT_DATE_FORMATTER).setWidth(1).setClickable(false).setOrderable(false));
            ds.addColumn(new SimpleColumn("Сумма платежа", "factPaymentAsString", NewLineFormatter.NOBR_IN_LINES).setClickable(false).setOrderable(false));

            ds.addColumn(new SimpleColumn("Долг за этап (пени)", "factRowDebtAsString", NewLineFormatter.NOBR_IN_LINES).setClickable(false).setOrderable(false));
            ds.addColumn(new SimpleColumn("Начисление пеней", "factRowFinesInfoAsString", NewLineFormatter.NOBR_IN_LINES).setClickable(false).setOrderable(false));

            final IListenerParametersResolver payPlanRowListenerParameterResolver = (entity, valueEntity) -> {
                final Model.PayPlanRow row = (Model.PayPlanRow)entity;
                return row.getPlanRow().getId();
            };

            final IndicatorColumn printColumn = CommonBaseUtil.getPrintColumn("onClickPayPlanModelPrintRow", "");
            printColumn.setParametersResolver(payPlanRowListenerParameterResolver);
            ds.addColumn(printColumn.setMergeRows(true).setMergeRowIdResolver(payPlanRowIdResolver).setPermissionKey(model.getSecModel().getPermission("printRow")));
        }
    }

    public void onClickPayPlanModelPrintRow(final IBusinessComponent component) {
        this.activateInRoot(component, new ComponentActivator(
                ru.tandemservice.unisc.component.agreement.AgreementPayPlanRowPrint.Model.class.getPackage().getName(),
                Collections.singletonMap(PublisherActivator.PUBLISHER_ID_KEY, component.getListenerParameter())
        ));

    }
}
