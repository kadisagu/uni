package ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage1;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry.form.validator.Validator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.ui.SelectModel;
import ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage.AbstractStageModel;
import ru.tandemservice.unisc.entity.catalog.UniscCustomerType;
import ru.tandemservice.unisc.entity.catalog.UniscPayPeriod;
import ru.tandemservice.unisc.utils.UniscPropertyUtils;

import java.util.Collections;
import java.util.List;

/**
 * @author vdanilov
 */
@SuppressWarnings("deprecation")
public class Model extends AbstractStageModel {

    public boolean isNumberDisabled() {
        return (UniscPropertyUtils.STRICT_NUMBER ? !(
                (null == getAgreement().getId()) || (StringUtils.trimToEmpty(getAgreement().getNumber()).endsWith(UniscPropertyUtils.RESERVED_POSTFIX))
        ) : false);
    }

    public boolean isNumberGenerated() {
        return (UniscPropertyUtils.GENERATE_NUMBER);
    }

    public Validator getFormingDateValidator() {
        return UniscPropertyUtils.FORMING_DATE_VALIDATOR_FACTORY.buildValidator(this);
    }

    public Validator getDeadlineDateValidator() {
        return UniscPropertyUtils.DEADLINE_DATE_VALIDATOR_FACTORY.buildValidator(this);
    }

    public boolean isShowCipher()
    {
        return !StringUtils.isEmpty(getAgreement().getConfig().getCipher());
    }


    private ISelectModel developPeriods;
    public ISelectModel getDevelopPeriods() { return this.developPeriods; }
    protected void setDevelopPeriods(final ISelectModel developPeriods) { this.developPeriods = developPeriods; }

    private List<UniscCustomerType> customerTypes = Collections.emptyList();
    public List<UniscCustomerType> getCustomerTypes() { return this.customerTypes; }
    protected void setCustomerTypes(final List<UniscCustomerType> customerTypes) { this.customerTypes = customerTypes; }

    private List<UniscPayPeriod> payPlanFreqList = Collections.emptyList();
    public List<UniscPayPeriod> getPayPlanFreqList() { return this.payPlanFreqList; }
    public void setPayPlanFreqList(final List<UniscPayPeriod> payPlanFreqList) { this.payPlanFreqList = payPlanFreqList; }

    private List<UniscPayPeriod> payPlanDurationList = Collections.emptyList();
    public List<UniscPayPeriod> getPayPlanDurationList() { return this.payPlanDurationList; }
    public void setPayPlanDurationList(final List<UniscPayPeriod> payPlanDurationList) { this.payPlanDurationList = payPlanDurationList; }

    private final SelectModel<IdentifiableWrapper<IEntity>> firstStageModel = new SelectModel<IdentifiableWrapper<IEntity>>();
    public SelectModel<IdentifiableWrapper<IEntity>> getFirstStageModel() { return this.firstStageModel; }

    public String getFirstStageTitle() { return this.getAgreement().getFirstStageTitle(); }
    public String getLastStageTitle() { return this.getAgreement().getLastStageTitle(); }


}
