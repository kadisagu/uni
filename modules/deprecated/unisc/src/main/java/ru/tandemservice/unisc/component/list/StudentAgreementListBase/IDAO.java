/* $Id: IDAO.java 8433 2009-06-11 06:38:32Z vdanilov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisc.component.list.StudentAgreementListBase;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author vdanilov
 */
public interface IDAO<M extends Model> extends IUniDao<M>
{
	@Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
	void refreshStudentContractsDataSource(M model, boolean forPrint);

	@Transactional(propagation=Propagation.REQUIRED, readOnly=false)
	void updateCheckBySignature(M model, Long id);

    Long getPrintObjectId(Long documentId);

    String getPrintComponentName(Long documentId);

    void restoreDefaults(M model);

    byte[] prepareReport(final M model, final Long principalId);
}
