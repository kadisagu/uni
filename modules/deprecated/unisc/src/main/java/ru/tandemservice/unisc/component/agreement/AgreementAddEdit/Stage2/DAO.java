package ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage2;

import org.hibernate.Session;
import ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage.AbstractStageDAO;
import ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage.IStageResult;
import ru.tandemservice.unisc.component.agreement.AgreementPayPlanEdit.AgreementPayPlanModel;
import ru.tandemservice.unisc.dao.agreement.IUniscEduAgreementDAO;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayPlanRow;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author vdanilov
 */
@SuppressWarnings("deprecation")
public class DAO extends AbstractStageDAO<Model> implements IDAO {

    @Override
    protected IStageResult getResult(final Model model) {
        final List<UniscEduAgreementPayPlanRow> rows = model.getPayPlanModel().getRows();
        return m -> {
            final UniscEduAgreementBase agreement = m.update(model.getAgreement(), true);
            IUniscEduAgreementDAO.INSTANCE.get().doSyncAgreementPayPlanRows(agreement, rows);
        };
    }

    @Override
    public void prepare(final Model model) {
        super.prepare(model);
        this.prepareCost(model);
    }


    protected void prepareCost(final Model model) {
        if (model.getPayPlanModel().getPeriods().isEmpty()) {
            model.getPayPlanModel().prepare(IUniscEduAgreementDAO.INSTANCE.get().getAgreementPayPlanRows(model.getAgreement(), false));
            if (model.getPayPlanModel().getPeriods().isEmpty()) {
                model.getPayPlanModel().prepare(IUniscEduAgreementDAO.INSTANCE.get().getAgreementPayPlanRows(model.getAgreement(), true));
            }
        }
        /* reassociate rows & assign ids\numbers */ {
            final Session session = getSession();
            final List<AgreementPayPlanModel.Period> periods = model.getPayPlanModel().getPeriods();

            final Set<Long> ids = new HashSet<>();
            for (final AgreementPayPlanModel.Period p: periods) {
                for (final UniscEduAgreementPayPlanRow row: p.getRows()) {
                    ids.add(row.getId());

                    if (model.getAgreement().equals(row.getOwner())) {
                        if (!row.isTransient()) { session.update(row); }
                        row.setOwner(model.getAgreement());
                    }
                }
            }


            for (final AgreementPayPlanModel.Period p: periods) {
                for (final UniscEduAgreementPayPlanRow row: p.getRows()) {
                    if (null == row.getId()) {
                        long id = ids.size();
                        while (!ids.add(id)) { id++; }
                        row.setId(id);
                    }
                }
            }
        }

        model.getPayPlanModel().refreshCost();
    }

}
