package ru.tandemservice.unisc.entity.config.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unisc.entity.config.UniscOrgUnitPresenter;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Уполномоченные лица со стороны ОУ для официальных документов
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniscOrgUnitPresenterGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisc.entity.config.UniscOrgUnitPresenter";
    public static final String ENTITY_NAME = "uniscOrgUnitPresenter";
    public static final int VERSION_HASH = 1468097783;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORG_UNIT = "orgUnit";
    public static final String P_PERSON = "person";
    public static final String P_GROUNDS = "grounds";

    private OrgUnit _orgUnit;     // Подразделение
    private String _person;     // В лице
    private String _grounds;     // Действующий на основании

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Подразделение. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null и должно быть уникальным.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return В лице. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPerson()
    {
        return _person;
    }

    /**
     * @param person В лице. Свойство не может быть null.
     */
    public void setPerson(String person)
    {
        dirty(_person, person);
        _person = person;
    }

    /**
     * @return Действующий на основании. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getGrounds()
    {
        return _grounds;
    }

    /**
     * @param grounds Действующий на основании. Свойство не может быть null.
     */
    public void setGrounds(String grounds)
    {
        dirty(_grounds, grounds);
        _grounds = grounds;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UniscOrgUnitPresenterGen)
        {
            setOrgUnit(((UniscOrgUnitPresenter)another).getOrgUnit());
            setPerson(((UniscOrgUnitPresenter)another).getPerson());
            setGrounds(((UniscOrgUnitPresenter)another).getGrounds());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniscOrgUnitPresenterGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniscOrgUnitPresenter.class;
        }

        public T newInstance()
        {
            return (T) new UniscOrgUnitPresenter();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "person":
                    return obj.getPerson();
                case "grounds":
                    return obj.getGrounds();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "person":
                    obj.setPerson((String) value);
                    return;
                case "grounds":
                    obj.setGrounds((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "orgUnit":
                        return true;
                case "person":
                        return true;
                case "grounds":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "orgUnit":
                    return true;
                case "person":
                    return true;
                case "grounds":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "person":
                    return String.class;
                case "grounds":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniscOrgUnitPresenter> _dslPath = new Path<UniscOrgUnitPresenter>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniscOrgUnitPresenter");
    }
            

    /**
     * @return Подразделение. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisc.entity.config.UniscOrgUnitPresenter#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return В лице. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.config.UniscOrgUnitPresenter#getPerson()
     */
    public static PropertyPath<String> person()
    {
        return _dslPath.person();
    }

    /**
     * @return Действующий на основании. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.config.UniscOrgUnitPresenter#getGrounds()
     */
    public static PropertyPath<String> grounds()
    {
        return _dslPath.grounds();
    }

    public static class Path<E extends UniscOrgUnitPresenter> extends EntityPath<E>
    {
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PropertyPath<String> _person;
        private PropertyPath<String> _grounds;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Подразделение. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisc.entity.config.UniscOrgUnitPresenter#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return В лице. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.config.UniscOrgUnitPresenter#getPerson()
     */
        public PropertyPath<String> person()
        {
            if(_person == null )
                _person = new PropertyPath<String>(UniscOrgUnitPresenterGen.P_PERSON, this);
            return _person;
        }

    /**
     * @return Действующий на основании. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.config.UniscOrgUnitPresenter#getGrounds()
     */
        public PropertyPath<String> grounds()
        {
            if(_grounds == null )
                _grounds = new PropertyPath<String>(UniscOrgUnitPresenterGen.P_GROUNDS, this);
            return _grounds;
        }

        public Class getEntityClass()
        {
            return UniscOrgUnitPresenter.class;
        }

        public String getEntityName()
        {
            return "uniscOrgUnitPresenter";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
