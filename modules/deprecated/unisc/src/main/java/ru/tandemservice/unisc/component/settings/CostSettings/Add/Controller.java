package ru.tandemservice.unisc.component.settings.CostSettings.Add;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;

import java.util.Collections;

/**
 * @author vdanilov
 */
public class Controller extends AbstractBusinessController<IDAO, Model> {
    @Override
    public void onRefreshComponent(final IBusinessComponent component) {
        Model model = getModel(component);
        if (Boolean.TRUE.equals(model.getClose())) { deactivate(component); }
        getDao().prepare(model);
    }

    public void onClickApply(final IBusinessComponent component) {
        final Long id = getDao().save(getModel(component));

        component.createDefaultChildRegion(new ComponentActivator(
                ru.tandemservice.unisc.component.settings.CostSettings.Edit.Model.class.getPackage().getName(),
                Collections.<String, Object>singletonMap(PublisherActivator.PUBLISHER_ID_KEY, id)
        ));

    }
}
