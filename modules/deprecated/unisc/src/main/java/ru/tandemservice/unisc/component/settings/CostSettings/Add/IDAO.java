package ru.tandemservice.unisc.component.settings.CostSettings.Add;

import ru.tandemservice.uni.dao.IPrepareable;

/**
 * @author vdanilov
 */
public interface IDAO extends IPrepareable<Model> {

	Long save(Model model);

}
