package ru.tandemservice.unisc.component.agreement.AgreementPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.list.source.AbstractListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.ui.formatters.RuMoneyFormatter;
import ru.tandemservice.unisc.dao.agreement.IUniscEduAgreementDAO;
import ru.tandemservice.unisc.dao.payment.IUniscEduAgreementPaymentState;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayPlanRow;
import ru.tandemservice.unisc.entity.config.UniscFinesHistory;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Input({
    /* это не публикатор (сам по себе), поэтому @input */
    @Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="id")
})
public class Model {
    private Long id;
    public Long getId() { return this.id; }
    public void setId(final Long id) { this.id = id; }

    private UniscEduAgreementBase agreement;
    public UniscEduAgreementBase getAgreement() { return this.agreement; }
    public void setAgreement(final UniscEduAgreementBase agreement) { this.agreement = agreement; }

    public boolean isReadOnly() { return (!IUniscEduAgreementDAO.INSTANCE.get().isMutable(getAgreement())); }

    public EducationOrgUnit getEducationOrgUnit() { return this.getAgreement().getConfig().getEducationOrgUnit(); }
    public String getFirstStageTitle() { return this.getAgreement().getFirstStageTitle(); }
    public String getLastStageTitle() { return this.getAgreement().getLastStageTitle(); }

    private List<UniscEduAgreementNaturalPerson> naturalPersons;
    public List<UniscEduAgreementNaturalPerson> getNaturalPersons() { return this.naturalPersons; }
    public void setNaturalPersons(final List<UniscEduAgreementNaturalPerson> naturalPersons) { this.naturalPersons = naturalPersons; }

    private UniscEduAgreementNaturalPerson naturalPerson;
    public UniscEduAgreementNaturalPerson getNaturalPerson() { return this.naturalPerson; }
    public void setNaturalPerson(final UniscEduAgreementNaturalPerson naturalPerson) { this.naturalPerson = naturalPerson; }


    @SuppressWarnings("serial")
    public static abstract class PayPlanRow extends IdentifiableWrapper<IEntity> {
        public PayPlanRow(final Long id, final String title) { super(id, title); }

        public abstract UniscEduAgreementPayPlanRow getPlanRow();
        public abstract IUniscEduAgreementPaymentState.StateFactRow getFactRow();

        public String getFactPaymentAsString() {
            return (null == getFactRow() ? "" : RuMoneyFormatter.INSTANCE.format(getFactRow().getPayment()));
        }

        public String getFactRowDebtAsString() {
            if (null == getFactRow()) { return ""; }
            final double[] debt = getFactRow().getDebt();
            if (null == debt) { return ""; }
            return (
                    RuMoneyFormatter.INSTANCE.format(debt[0]) +
                    (debt[1] > 0d ? ("\n("+RuMoneyFormatter.INSTANCE.format(debt[1])+")") : "")
            );
        }


        public String getFactRowFinesInfoAsString() {
            if (null == getFactRow()) { return ""; }
            final Collection<IUniscEduAgreementPaymentState.StateFinesRow> fines = getFactRow().getFinesRows();
            if ((null == fines) || fines.isEmpty()) { return ""; }

            final StringBuilder sb = new StringBuilder();
            for (final IUniscEduAgreementPaymentState.StateFinesRow row: fines) {
                if (sb.length() > 0) { sb.append(",\n"); }
                sb
                .append(UniscFinesHistory.TAX_FORMATTER.format(row.getFinesTax()/* уже в процентах *100.0d*0.01d */))
                .append("% (")
                .append(CommonBaseDateUtil.getDaysCountWithName(row.getDays()))
                .append(") от ")
                .append(RuMoneyFormatter.INSTANCE.format(row.getDebt()));
            }
            return sb.toString();
        }
    }

    private List<PayPlanRow> payPlanRows = Collections.emptyList();
    public List<PayPlanRow> getPayPlanRows() { return this.payPlanRows; }
    public void setPayPlanRows(final List<PayPlanRow> payPlanRows) { this.payPlanRows = payPlanRows; }

    private AbstractListDataSource<PayPlanRow> payPlanDataSource = new AbstractListDataSource<PayPlanRow>() {
        @Override public void onChangeOrder() {}
        @Override public void onRefresh() {}
        @Override public long getTotalSize() { return getEntityList().size(); }
        @Override public long getStartRow() { return 0; }
        @Override public long getCountRow() { return this.getTotalSize(); }
        @Override public List<PayPlanRow> getEntityList() { return payPlanRows; }
        @Override public AbstractListDataSource<PayPlanRow> getCopy() { return this; }
    };

    public AbstractListDataSource<PayPlanRow> getPayPlanDataSource() { return this.payPlanDataSource; }

    private CommonPostfixPermissionModel secModel;
    public CommonPostfixPermissionModel getSecModel() { return secModel; }
    public void setSecModel(CommonPostfixPermissionModel secModel) { this.secModel = secModel; }
}
