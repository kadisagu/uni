package ru.tandemservice.unisc.component.report;

import java.util.Date;

import org.tandemframework.shared.organization.base.entity.OrgUnit;

/**
 * @author oleyba
 * @since 20.08.2009
 */
public interface IUniscReport
{
    OrgUnit getOrgUnit();

    void setFormingDate(Date date);
    void setOrgUnit(OrgUnit ou);
    void setFormativeOrgUnitTitle(String title);
    void setTerritorialOrgUnitTitle(String title);
    void setEduLevelTitle(String title);
    void setDevelopFormTitle(String title);
    void setDevelopConditionTitle(String title);
    void setDevelopTechTitle(String title);
    void setDevelopPeriodTitle(String title);
    void setCourse(String title);
    void setGroup(String title);
    void setSelection(String title);
}
