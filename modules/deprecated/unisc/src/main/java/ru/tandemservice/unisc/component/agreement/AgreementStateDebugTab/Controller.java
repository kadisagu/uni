/* $Id: Controller.java 8816 2009-07-02 07:31:01Z ekachanova $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisc.component.agreement.AgreementStateDebugTab;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.AbstractListDataSource;
import org.tandemframework.core.view.list.source.IRowCustomizer;
import ru.tandemservice.unisc.component.agreement.AgreementStateDebugTab.Model.PayPlanRow;

/**
 * @author vdanilov
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component) {
        final Model model = getModel(component);
        getDao().prepare(model);

        {
            final AbstractListDataSource<Model.Row> ds = model.getDataSource();
            ds.getColumns().clear();
            ds.addColumn(new SimpleColumn("Дата", "eventDate", DateFormatter.DEFAULT_DATE_FORMATTER).setWidth(5).setClickable(false).setOrderable(false));
            ds.addColumn(new SimpleColumn("Событие", "title").setClickable(false).setOrderable(false));
            ds.addColumn(new SimpleColumn("Состояние", "stateString").setClickable(false).setOrderable(false));
        }

        {
            final AbstractListDataSource<Model.PayPlanRow> ds = model.getPayPlanDataSource();
            ds.setRowCustomizer(new IRowCustomizer<PayPlanRow>() {
                @Override public boolean isClickable(final IPublisherLinkColumn column, final PayPlanRow rowEntity, final IEntity itemEntity) {
                    return false;
                }
                @Override public boolean isClickable(final AbstractColumn<?> column, final PayPlanRow rowEntity) {
                    return false;
                }
                @Override public String getRowStyle(final PayPlanRow entity) {
                    return null;
                }
                @Override public String getHeadRowStyle() {
                    return null;
                }
            });
            ds.getColumns().clear();

            final IMergeRowIdResolver payPlanRowDocumentIdResolver = entity -> {
                final PayPlanRow row = (PayPlanRow)entity;
                return ("row-"+String.valueOf(row.getPlanRow().getOwner().getId()));
            };

            final IMergeRowIdResolver payPlanRowIdResolver = entity -> {
                final PayPlanRow row = (PayPlanRow)entity;
                return ("row-"+String.valueOf(row.getPlanRow().getId()));
            };

            ds.addColumn(new SimpleColumn("Документ", "planRow.owner.title", NewLineFormatter.NOBR_IN_LINES).setClickable(false).setOrderable(false).setMergeRows(true).setMergeRowIdResolver(payPlanRowDocumentIdResolver));

            ds.addColumn(new SimpleColumn("Этап", "title").setClickable(false).setOrderable(false).setMergeRows(true).setMergeRowIdResolver(payPlanRowIdResolver));
            ds.addColumn(new SimpleColumn("Оплатить до", "planRow.date", DateFormatter.DEFAULT_DATE_FORMATTER).setWidth(1).setClickable(false).setOrderable(false).setMergeRows(true).setMergeRowIdResolver(payPlanRowIdResolver));

            final HeadColumn consts = new HeadColumn("costs", "Сумма оплаты");
            consts.addColumn(new SimpleColumn("физ. лицо", "planRow.NCostAsString", NewLineFormatter.NOBR_IN_LINES).setClickable(false).setOrderable(false).setMergeRows(true).setMergeRowIdResolver(payPlanRowIdResolver));
            consts.addColumn(new SimpleColumn("юр. лицо", "planRow.JCostAsString", NewLineFormatter.NOBR_IN_LINES).setClickable(false).setOrderable(false).setMergeRows(true).setMergeRowIdResolver(payPlanRowIdResolver));
            ds.addColumn(consts);

            ds.addColumn(new SimpleColumn("% Льготы", "planRow.discountAsString", NewLineFormatter.NOBR_IN_LINES).setClickable(false).setOrderable(false).setMergeRows(true).setMergeRowIdResolver(payPlanRowIdResolver));
            ds.addColumn(new SimpleColumn("Итого к оплате", "planRow.costAsString", NewLineFormatter.NOBR_IN_LINES).setClickable(false).setOrderable(false).setMergeRows(true).setMergeRowIdResolver(payPlanRowIdResolver));
            ds.addColumn(new SimpleColumn("Комментарий", "planRow.comment", NewLineFormatter.NOBR_IN_LINES).setClickable(false).setOrderable(false).setMergeRows(true).setMergeRowIdResolver(payPlanRowIdResolver));

            ds.addColumn(new SimpleColumn("Дата платежа", "factRow.date", DateFormatter.DEFAULT_DATE_FORMATTER).setWidth(1).setClickable(false).setOrderable(false));
            ds.addColumn(new SimpleColumn("Сумма платежа", "factPaymentAsString", NewLineFormatter.NOBR_IN_LINES).setClickable(false).setOrderable(false));

            ds.addColumn(new SimpleColumn("Долг за этап (пени)", "factRowDebtAsString", NewLineFormatter.NOBR_IN_LINES).setClickable(false).setOrderable(false));
            ds.addColumn(new SimpleColumn("Начисление пеней", "factRowFinesInfoAsString", NewLineFormatter.NOBR_IN_LINES).setClickable(false).setOrderable(false));
        }



    }


}
