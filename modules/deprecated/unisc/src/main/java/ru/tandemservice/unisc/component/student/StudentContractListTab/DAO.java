/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unisc.component.student.StudentContractListTab;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisc.entity.agreements.UniscEduAdditAgreement;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;
import ru.tandemservice.unisc.entity.agreements.gen.UniscEduAdditAgreementGen;
import ru.tandemservice.unisc.entity.agreements.gen.UniscEduAgreementBaseGen;
import ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student;
import ru.tandemservice.unisc.entity.agreements.student.gen.UniscEduAgreement2StudentGen;

/**
 * @author vdanilov
 */
public class DAO extends UniDao<Model> implements IDAO
{

    @Override
    public void prepare(final Model model)
    {
        final List<Model.AgreementWrapper> result = this.getAgreements(model);
        model.setLastAgreement(result.isEmpty() ? null : result.get(result.size() - 1).getAgreement());
    }

    @Override
    public void refreshDataSource(final Model model)
    {
        final List<Model.AgreementWrapper> result = this.getAgreements(model);
        model.setLastAgreement(result.isEmpty() ? null : result.get(result.size() - 1).getAgreement());

        final DynamicListDataSource<Model.AgreementWrapper> ds = model.getAgreementsDataSource();
        ds.setCountRow(result.size() + 1);
        UniBaseUtils.createPage(ds, result);
    }

    private List<Model.AgreementWrapper> getAgreements(final Model model)
    {
        final MQBuilder builder = new MQBuilder(UniscEduAgreement2StudentGen.ENTITY_CLASS, "a2s");
        builder.add(MQExpression.eq("a2s", UniscEduAgreement2StudentGen.L_STUDENT + ".id", model.getId()));
        builder.addJoinFetch("a2s", UniscEduAgreement2StudentGen.L_AGREEMENT, "agreement");
        builder.addOrder("a2s", UniscEduAgreement2StudentGen.P_ACTIVE);
        builder.addOrder("agreement", UniscEduAgreementBaseGen.P_FORMING_DATE);
        builder.addOrder("agreement", "id");

        final Session session = this.getSession();
        final List<Model.AgreementWrapper> result = new ArrayList<Model.AgreementWrapper>();

        final List<UniscEduAgreement2Student> relations = builder.getResultList(session);
        for (final UniscEduAgreement2Student a2s : relations)
        {
            result.add(new Model.AgreementWrapper(a2s, a2s.isActive()));
            final MQBuilder b = new MQBuilder(UniscEduAdditAgreementGen.ENTITY_CLASS, "agreement");
            b.add(MQExpression.eq("agreement", UniscEduAdditAgreementGen.L_AGREEMENT + ".id", a2s.getAgreement().getId()));
            b.addOrder("agreement", UniscEduAgreementBaseGen.P_FORMING_DATE);
            b.addOrder("agreement", "id");

            for (final UniscEduAdditAgreement a : b.<UniscEduAdditAgreement> getResultList(session))
            {
                result.add(new Model.AgreementWrapper(a, a2s.isActive()));
            }
        }

        return result;
    }

    @Override
    public void updateCheckByAccounting(final Model model, final Long id)
    {
        this.changeBooleanField(model, id, UniscEduAgreementBaseGen.P_CHECKED_BY_ACCOUNTING);
    }

    @Override
    public void updateCheckByLawer(final Model model, final Long id)
    {
        this.changeBooleanField(model, id, UniscEduAgreementBaseGen.P_CHECKED_BY_LAWYER);
    }

    @Override
    public void updateCheckBySignature(final Model model, final Long id)
    {
        this.changeBooleanField(model, id, UniscEduAgreementBaseGen.P_CHECKED_BY_SIGNATURE);
    }

    @Override
    public void updateActivateState(final Model model, final Long id)
    {
        UniscEduAgreement2Student a2s = get(UniscEduAgreement2Student.class, id);
        if (null != a2s)
        {
            // выключаем все другие договоры на всякий случай - активным может быть только последний
            MQBuilder builder = new MQBuilder(UniscEduAgreement2StudentGen.ENTITY_CLASS, "rel");
            builder.add(MQExpression.eq("rel", UniscEduAgreement2StudentGen.student().id().s(), model.getId()));
            builder.add(MQExpression.notEq("rel", "id", id));
            for (UniscEduAgreement2Student rel : builder.<UniscEduAgreement2Student> getResultList(getSession()))
            {
                rel.setActive(rel.getId().equals(a2s.getId()));
                getSession().saveOrUpdate(rel);
            }
            // меняем состояние текущему
            a2s.setActive(!a2s.isActive());
        }
    }

    @Override
    public void deleteAgreement(final Model model, final Long id)
    {
        final UniscEduAgreementBase last = this.check(model, id);
        if (null != last)
        {
            this.getSession().delete(last);
        }
    }

    protected void changeBooleanField(final Model model, final Long id, final String name)
    {
        final UniscEduAgreementBase last = this.check(model, id);
        if (null != last)
        {
            last.setProperty(name, Boolean.FALSE.equals(last.getProperty(name)));
            this.getSession().saveOrUpdate(last);
        }
    }

    protected UniscEduAgreementBase check(final Model model, final Long id)
    {
        final List<Model.AgreementWrapper> result = this.getAgreements(model);
        if (result.isEmpty())
        {
            return null;
        }

        final Model.AgreementWrapper lastWrapper = result.get(result.size() - 1);
        if (!lastWrapper.getId().equals(id))
        {
            return null;
        }

        final UniscEduAgreementBase last = lastWrapper.getAgreement();
        return last;
    }

    @Override
    public boolean isHasRelation(Student student)
    {
        final Session session = this.getSession();

        MQBuilder builder = new MQBuilder(UniscEduAgreement2Student.ENTITY_NAME, "rel", new String[] { UniscEduAgreement2Student.id().s() });
        builder.add(MQExpression.eq("rel", UniscEduAgreement2Student.student().s(), student));

        return !builder.getResultList(session).isEmpty();
    }
}
