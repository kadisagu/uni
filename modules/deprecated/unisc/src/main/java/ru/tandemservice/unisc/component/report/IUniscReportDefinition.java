package ru.tandemservice.unisc.component.report;

/**
 * @author oleyba
 * @since 14.08.2009
 */
public interface IUniscReportDefinition
{
    String getId();
    String getTitle();
    String getPermissionKey();
    String getComponentName();
}
