package ru.tandemservice.unisc.component.settings.CostSettings.io.Import;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.basic.gen.EducationYearGen;
import ru.tandemservice.unisc.dao.io.IUniscIODao;
import ru.tandemservice.unisc.dao.io.IUniscIODao.ImportFacade;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * @author vdanilov
 */
public class DAO extends UniDao<Model> implements IDAO {

	@Override
	public void prepare(final Model model) {
		model.setYears(new EducationYearModel());
		if (null == model.getYear()) {
			model.setYear(this.get(EducationYear.class, EducationYearGen.P_CURRENT, Boolean.TRUE));
		}
	}

	private Collection<String> split(final String row) {
		final List<String> result = new ArrayList<>();
		boolean quote = false;
		int i=0;
		StringBuilder sb = new StringBuilder();
		while (i<row.length()) {
			final char c = row.charAt(i++);
			if (!quote) {
				if (','==c) {
					result.add(sb.toString());
					sb = new StringBuilder();
					continue;
				}
			}

			if ('"'==c) {
				if (quote && ((i<row.length()) && ('"'==row.charAt(i)))) {
					sb.append(c);
					i++;
					continue;
				} else {
					quote = !quote;
					continue;
				}
			}
			sb.append(c);
		}

		if (quote) {
			throw new IllegalStateException("quote error");
		} else {
			result.add(sb.toString());
		}

		return result;
	}

	private void assertEquals(final String title, String data, String mustBe) {
		data = StringUtils.trimToEmpty(data);
		mustBe = StringUtils.trimToEmpty(mustBe);
		if (!data.equals(mustBe)) {
			throw new IllegalStateException("не совпадает `" +title+"` (`" + data + "` != `" + mustBe + "`)");
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void save(final Model model, final ErrorCollector errorCollector) throws IOException
	{
		final InputStream stream = model.getUploadFile().getStream();
		final List<String> lines = IOUtils.readLines(stream);
		final Iterator<String> iterator = lines.iterator();
		final ImportFacade importFacade = IUniscIODao.INSTANCE.get().getImportFacade(model.getYear());


		while(iterator.hasNext()) {
			final String src = iterator.next();
			if (src.startsWith("\"#\"") || src.startsWith("#")) { continue; }

			final Collection<String> row = this.split(src);
			final Iterator<String> data = row.iterator();

			final Long id = Long.valueOf(data.next());
			final EducationOrgUnit edu = this.get(EducationOrgUnit.class, id);
			try {
				try {
					if (null == edu) { continue; }
					assertEquals("формирующее подр.", data.next(), (null == edu.getFormativeOrgUnit() ? "" : edu.getFormativeOrgUnit().getTypeTitle()));
					assertEquals("территориальное подр.", data.next(), (null == edu.getTerritorialOrgUnit() ? "" : edu.getTerritorialOrgUnit().getTypeTitle()));
					assertEquals("направление подготовки", data.next(), edu.getTitle());
					assertEquals("форма", data.next(), edu.getDevelopForm().getTitle());
					assertEquals("условие", data.next(), edu.getDevelopCondition().getTitle());
					assertEquals("технология", data.next(), edu.getDevelopTech().getTitle());
					assertEquals("срок", data.next(), edu.getDevelopPeriod().getTitle());
				} catch (final Throwable t) {
					errorCollector.add("Ошибка в строке `"+src+"`:" + t.getMessage());
					logger.error("src=`"+src+"`\nrow=`"+row.toString()+"`");
					logger.error(t.getMessage(), t);
					continue;
				}
				importFacade.add(edu, data.next(), data);

			} catch (final Throwable t) {
				logger.error("src=`"+src+"`\nrow=`"+row.toString()+"`");
				logger.error(t.getMessage(), t);
				errorCollector.add(t.getMessage());
			}
		}
		importFacade.doImport();
	}

}
