/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unisc.component.student.StudentContractListTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisc.entity.agreements.UniscEduAdditAgreement;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;
import ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student;

/**
 * @author vdanilov
 */
@Input(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "id"))
public class Model
{
    private Long id;

    public Long getId()
    {
        return this.id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    private boolean _tabVisible;

    public boolean isTabVisible()
    {
        return this._tabVisible;
    }

    protected void setTabVisible(final boolean tabVisible)
    {
        this._tabVisible = tabVisible;
    }

    public static class AgreementWrapper extends IdentifiableWrapper<UniscEduAgreementBase>
    {
        private static final long serialVersionUID = 1L;

        private final UniscEduAgreementBase agreement;

        public UniscEduAgreementBase getAgreement()
        {
            return this.agreement;
        }

        private final boolean active;

        public boolean isActive()
        {
            return active;
        }

        public AgreementWrapper(final UniscEduAdditAgreement agreement, final boolean active)
        {
            super(agreement.getId(), agreement.getAgreement().getNumber() + "." + agreement.getNumber());
            this.agreement = agreement;
            this.active = active;
        }

        public AgreementWrapper(final UniscEduAgreement2Student a2s, final boolean active)
        {
            super(a2s.getId(), a2s.getAgreement().getNumber());
            this.agreement = a2s.getAgreement();
            this.active = active;
        }

    }

    private DynamicListDataSource<AgreementWrapper> agreementsDataSource;

    public DynamicListDataSource<AgreementWrapper> getAgreementsDataSource()
    {
        return this.agreementsDataSource;
    }

    protected void setAgreementsDataSource(final DynamicListDataSource<AgreementWrapper> agreementsDataSource)
    {
        this.agreementsDataSource = agreementsDataSource;
    }

    private UniscEduAgreementBase lastAgreement;

    public UniscEduAgreementBase getLastAgreement()
    {
        return this.lastAgreement;
    }

    protected void setLastAgreement(final UniscEduAgreementBase lastAgreement)
    {
        this.lastAgreement = lastAgreement;
    }

    public boolean isAddAdditionalDisabled()
    {
        Student student = UniDaoFacade.getCoreDao().get(Student.class, getId());
        //        List<UniscEduAgreement2Student> rels = UniDaoFacade.getCoreDao().getList(UniscEduAgreement2Student.class, UniscEduAgreement2Student.student().id().s(), getId());
        //        for (UniscEduAgreement2Student rel : rels)
        //            if (rel.isActive() && !rel.getAgreement().isCheckedBySignature())
        //                return true;

        return UniDefines.COMPENSATION_TYPE_BUDGET.equals(student.getCompensationType().getCode()) || student.isArchival();

    }

    public boolean isAddAgreementDisabled()
    {
        Student student = UniDaoFacade.getCoreDao().get(Student.class, getId());
        return UniDefines.COMPENSATION_TYPE_BUDGET.equals(student.getCompensationType().getCode()) || student.isArchival();

    }
}
