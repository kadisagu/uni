package ru.tandemservice.unisc.entity.agreements;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.sec.ISecLocalEntityOwner;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisc.dao.agreement.IUniscEduAgreementDAO;
import ru.tandemservice.unisc.entity.agreements.gen.UniscEduAdditAgreementGen;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Доп. соглашение
 */
public class UniscEduAdditAgreement extends UniscEduAdditAgreementGen implements ISecLocalEntityOwner
{
	@Override
    public String getTitle()
    {
		return "Доп. соглашение № " + this.getNumber();
	}

    @Override
    public Collection<IEntity> getSecLocalEntities()
    {   IUniscEduAgreement2PersonRole<PersonRole> relation = IUniscEduAgreementDAO.INSTANCE.get().getRelation(this.getAgreement());
        if (null == relation || !(relation.getTarget() instanceof Student)) return new ArrayList<IEntity>();
        return ((Student) relation.getTarget()).getSecLocalEntities();
    }
}