/* $Id: DAO.java 8754 2009-06-29 09:27:43Z ekachanova $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisc.component.agreement.AgreementStateDebugTab;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tandemframework.core.entity.IEntity;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unisc.dao.payment.IUniscEduAgreementPaymentDAO;
import ru.tandemservice.unisc.dao.payment.IUniscEduAgreementPaymentEvent;
import ru.tandemservice.unisc.dao.payment.IUniscEduAgreementPaymentState;
import ru.tandemservice.unisc.dao.payment.IUniscEduAgreementPaymentState.StateFactRow;
import ru.tandemservice.unisc.entity.agreements.IUniscEduAgreement2PersonRole;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayPlanRow;

/**
 * @author vdanilov
 */
public class DAO extends UniBaseDao implements IDAO
{

    @SuppressWarnings("unchecked")
    private List<IUniscEduAgreementPaymentEvent> getEventList(final Model model) {
        IEntity e = get(model.getId());
        if (e instanceof UniscEduAgreementBase) {
            e = ((UniscEduAgreementBase)e).getAgreement();
        } else if (e instanceof IUniscEduAgreement2PersonRole) {
            e = ((IUniscEduAgreement2PersonRole)e).getAgreement();
        } else {
            if (null == e) { throw new NullPointerException(); }
            throw new IllegalStateException(String.valueOf(e.getClass()));
        }

        return IUniscEduAgreementPaymentDAO.INSTANCE.get().getEventsMap(Collections.singleton(e.getId()), null).get(e.getId());
    }



    @Override
    public void prepare(final Model model) {
        final List<IUniscEduAgreementPaymentEvent> eventList = getEventList(model);
        final Map<Long, IUniscEduAgreementPaymentEvent> eventMap = new HashMap<Long, IUniscEduAgreementPaymentEvent>();
        for (final IUniscEduAgreementPaymentEvent event: eventList) {
            eventMap.put(event.getId(), event);
        }

        final List<Model.Row> rows = new ArrayList<Model.Row>();
        final IUniscEduAgreementPaymentState agreementState = IUniscEduAgreementPaymentDAO.INSTANCE.get().getNewState();
        for (final IUniscEduAgreementPaymentEvent event: eventList) {
            agreementState.process(event);
            rows.add(new Model.Row(event, agreementState));
        }
        model.setRows(rows);


        final List<Model.PayPlanRow> payPlanRows = new ArrayList<Model.PayPlanRow>();
        for (final IUniscEduAgreementPaymentState.StatePlanRow planRow: agreementState.getPlanRows()) {
            final UniscEduAgreementPayPlanRow realPlanRow = (UniscEduAgreementPayPlanRow) eventMap.get(planRow.getId());
            final String title = realPlanRow.getOwner().getPayPlanFreq().toString(realPlanRow.getOwner().getConfig(), realPlanRow.getStage());
            final Collection<StateFactRow> factRows = planRow.getFactRows();
            if (factRows.isEmpty()) {
                payPlanRows.add(new Model.PayPlanRow(planRow.getId(), title) {
                    /* для строки без платежей */
                    private static final long serialVersionUID = 1L;
                    @Override public UniscEduAgreementPayPlanRow getPlanRow() { return realPlanRow; }
                    @Override public IUniscEduAgreementPaymentState.StateFactRow getFactRow() { return null; }
                });
            } else {
                for (final IUniscEduAgreementPaymentState.StateFactRow factRow: factRows) {
                    payPlanRows.add(new Model.PayPlanRow(factRow.getId(), title) {
                        /* строка с платежом */
                        private static final long serialVersionUID = 1L;
                        @Override public UniscEduAgreementPayPlanRow getPlanRow() { return realPlanRow; }
                        @Override public IUniscEduAgreementPaymentState.StateFactRow getFactRow() { return factRow; }
                    });
                }
            }
        }

        model.setPayPlanRows(payPlanRows);

    }
}
