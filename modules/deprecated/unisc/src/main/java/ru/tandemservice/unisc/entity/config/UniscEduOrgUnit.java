package ru.tandemservice.unisc.entity.config;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.ITitled;

import ru.tandemservice.unisc.entity.config.gen.UniscEduOrgUnitGen;

/**
 * Настройка оплаты (по направлению подготовки)
 */
public class UniscEduOrgUnit extends UniscEduOrgUnitGen implements ITitled {
    public static final String P_CIPHER_WITH_TITLE = "cipherWithTitle";

    public String getCipherWithTitle() {
        String dsc = StringUtils.trimToNull(getDescription());
        if (null == dsc) {
            dsc = this.getEducationOrgUnit().getTitleWithFormAndCondition();
            dsc = dsc +  " " + this.getEducationOrgUnit().getTerritorialOrgUnit().getTerritorialTitle();
        }

        return (StringUtils.trimToEmpty(this.getCipher()) + " (" + this.getEducationYear().getTitle() + " " + dsc +")");
    }

    @Override
    public String getTitle() {
        return this.getEducationYear().getTitle() + ": " + this.getEducationOrgUnit().getEducationLevelHighSchool().getDisplayableTitle() + (StringUtils.isEmpty(this.getCipher()) ? "" : " (" + this.getCipher() + ")");
    }
}
