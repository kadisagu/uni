/* $Id: DAO.java 6933 2009-03-07 05:41:27Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unisc.component.agreement.AgreementRelationPrint;

import java.util.List;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.field.RtfField;
import org.tandemframework.rtf.document.text.picture.RtfPicture;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.util.rtf.UniRtfPictureUtil;

/**
 * @author vdanilov
 */
public class UniscEduAgreementPrintDAOBase extends UniBaseDao
{

    protected void applyBarcode(final RtfDocument document, String barcode) throws Exception {
        try {
            barcode = CoreStringUtils.transliterate(barcode);
            this.modify(document.getElementList(), "barcode", UniRtfPictureUtil.createRtfPicture(barcode, 9.6, 150));
        } catch (final Throwable t) {
            logger.error(t.getMessage());
        }
    }



    private void modify(final List<IRtfElement> elementList, final String fieldName, final RtfPicture picture)
    {
        if (elementList == null) {
            return;
        }
        for (int i = 0; i < elementList.size(); i++)
        {
            final IRtfElement element = elementList.get(i);
            if (element instanceof IRtfGroup)
            {
                this.modify(((IRtfGroup) element).getElementList(), fieldName, picture);
            }
            else if (element instanceof RtfTable)
            {
                final RtfTable table = (RtfTable) element;
                for (final RtfRow row : table.getRowList()) {
                    for (final RtfCell cell : row.getCellList()) {
                        this.modify(cell.getElementList(), fieldName, picture);
                    }
                }
            }
            else if (element instanceof RtfField)
            {
                final RtfField field = (RtfField) element;
                if (field.isMark() && field.getFieldName().equals(fieldName))
                {
                    final IRtfGroup group = RtfBean.getElementFactory().createRtfGroup();
                    group.getElementList().addAll(field.getFormatList());
                    group.getElementList().add(picture);
                    elementList.set(i, group);
                }
            }
        }
    }
}
