package ru.tandemservice.unisc.component.agreement.AgreementRelationAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.unisc.entity.agreements.IUniscEduAgreement2PersonRole;
import ru.tandemservice.unisc.entity.agreements.UniscEduMainAgreement;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="id"),
    @Bind(key="agreementNumber", binding="agreement.number")
})
@SuppressWarnings("deprecation")
public class Model<T extends PersonRole> extends ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Model {

    private Long id;
    public Long getId() { return this.id; }
    public void setId(final Long id) { this.id = id; }

    private IUniscEduAgreement2PersonRole<T> relation;
    public IUniscEduAgreement2PersonRole<T> getRelation() { return this.relation; }
    public void setRelation(final IUniscEduAgreement2PersonRole<T> relation) { this.relation = relation; }

    @Override public UniscEduMainAgreement getAgreement() { return this.getRelation().getAgreement(); }
    @Override public T getPersonRole() { return this.getRelation().getTarget(); }

}
