package ru.tandemservice.unisc.entity.config.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unisc.entity.catalog.UniscPayPeriod;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlan;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * График оплат (настройка)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniscEduOrgUnitPayPlanGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlan";
    public static final String ENTITY_NAME = "uniscEduOrgUnitPayPlan";
    public static final int VERSION_HASH = -1461824757;
    private static IEntityMeta ENTITY_META;

    public static final String L_UNISC_EDU_ORG_UNIT = "uniscEduOrgUnit";
    public static final String L_UNISC_PAY_FREQ = "uniscPayFreq";

    private UniscEduOrgUnit _uniscEduOrgUnit;     // Настройка направления подготовки
    private UniscPayPeriod _uniscPayFreq;     // Период оплаты

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Настройка направления подготовки. Свойство не может быть null.
     */
    @NotNull
    public UniscEduOrgUnit getUniscEduOrgUnit()
    {
        return _uniscEduOrgUnit;
    }

    /**
     * @param uniscEduOrgUnit Настройка направления подготовки. Свойство не может быть null.
     */
    public void setUniscEduOrgUnit(UniscEduOrgUnit uniscEduOrgUnit)
    {
        dirty(_uniscEduOrgUnit, uniscEduOrgUnit);
        _uniscEduOrgUnit = uniscEduOrgUnit;
    }

    /**
     * @return Период оплаты. Свойство не может быть null.
     */
    @NotNull
    public UniscPayPeriod getUniscPayFreq()
    {
        return _uniscPayFreq;
    }

    /**
     * @param uniscPayFreq Период оплаты. Свойство не может быть null.
     */
    public void setUniscPayFreq(UniscPayPeriod uniscPayFreq)
    {
        dirty(_uniscPayFreq, uniscPayFreq);
        _uniscPayFreq = uniscPayFreq;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UniscEduOrgUnitPayPlanGen)
        {
            setUniscEduOrgUnit(((UniscEduOrgUnitPayPlan)another).getUniscEduOrgUnit());
            setUniscPayFreq(((UniscEduOrgUnitPayPlan)another).getUniscPayFreq());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniscEduOrgUnitPayPlanGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniscEduOrgUnitPayPlan.class;
        }

        public T newInstance()
        {
            return (T) new UniscEduOrgUnitPayPlan();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "uniscEduOrgUnit":
                    return obj.getUniscEduOrgUnit();
                case "uniscPayFreq":
                    return obj.getUniscPayFreq();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "uniscEduOrgUnit":
                    obj.setUniscEduOrgUnit((UniscEduOrgUnit) value);
                    return;
                case "uniscPayFreq":
                    obj.setUniscPayFreq((UniscPayPeriod) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "uniscEduOrgUnit":
                        return true;
                case "uniscPayFreq":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "uniscEduOrgUnit":
                    return true;
                case "uniscPayFreq":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "uniscEduOrgUnit":
                    return UniscEduOrgUnit.class;
                case "uniscPayFreq":
                    return UniscPayPeriod.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniscEduOrgUnitPayPlan> _dslPath = new Path<UniscEduOrgUnitPayPlan>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniscEduOrgUnitPayPlan");
    }
            

    /**
     * @return Настройка направления подготовки. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlan#getUniscEduOrgUnit()
     */
    public static UniscEduOrgUnit.Path<UniscEduOrgUnit> uniscEduOrgUnit()
    {
        return _dslPath.uniscEduOrgUnit();
    }

    /**
     * @return Период оплаты. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlan#getUniscPayFreq()
     */
    public static UniscPayPeriod.Path<UniscPayPeriod> uniscPayFreq()
    {
        return _dslPath.uniscPayFreq();
    }

    public static class Path<E extends UniscEduOrgUnitPayPlan> extends EntityPath<E>
    {
        private UniscEduOrgUnit.Path<UniscEduOrgUnit> _uniscEduOrgUnit;
        private UniscPayPeriod.Path<UniscPayPeriod> _uniscPayFreq;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Настройка направления подготовки. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlan#getUniscEduOrgUnit()
     */
        public UniscEduOrgUnit.Path<UniscEduOrgUnit> uniscEduOrgUnit()
        {
            if(_uniscEduOrgUnit == null )
                _uniscEduOrgUnit = new UniscEduOrgUnit.Path<UniscEduOrgUnit>(L_UNISC_EDU_ORG_UNIT, this);
            return _uniscEduOrgUnit;
        }

    /**
     * @return Период оплаты. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlan#getUniscPayFreq()
     */
        public UniscPayPeriod.Path<UniscPayPeriod> uniscPayFreq()
        {
            if(_uniscPayFreq == null )
                _uniscPayFreq = new UniscPayPeriod.Path<UniscPayPeriod>(L_UNISC_PAY_FREQ, this);
            return _uniscPayFreq;
        }

        public Class getEntityClass()
        {
            return UniscEduOrgUnitPayPlan.class;
        }

        public String getEntityName()
        {
            return "uniscEduOrgUnitPayPlan";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
