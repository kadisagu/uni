package ru.tandemservice.unisc.component.agreement.AgreementPub;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.hibernate.Session;

import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unisc.dao.agreement.IUniscEduAgreementDAO;
import ru.tandemservice.unisc.dao.payment.IUniscEduAgreementPaymentDAO;
import ru.tandemservice.unisc.dao.payment.IUniscEduAgreementPaymentState;
import ru.tandemservice.unisc.dao.payment.IUniscEduAgreementPaymentState.StateFactRow;
import ru.tandemservice.unisc.entity.agreements.UniscEduAdditAgreement;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayPlanRow;

public class DAO extends UniDao<Model> implements IDAO {
    @Override
    public void prepare(final Model model) {
        final Session session = getSession();
        if (null != model.getId()) {
            model.setAgreement((UniscEduAgreementBase) session.get(UniscEduAgreementBase.class, model.getId()));
            model.setSecModel(new CommonPostfixPermissionModel(
                    IUniscEduAgreementDAO.INSTANCE.get().getPermissionContext(model.getAgreement()) +
                    (UniscEduAdditAgreement.class.isInstance(model.getAgreement()) ? "Addit" : "") +
            "AgreementInfoTab"));
        }
        model.setNaturalPersons(IUniscEduAgreementDAO.INSTANCE.get().getNaturalPersons(model.getAgreement()));

        final Long agreementId = model.getAgreement().getAgreement().getId();
        final IUniscEduAgreementPaymentState agreementState = IUniscEduAgreementPaymentDAO.INSTANCE.get().getStateMap(Collections.singleton(agreementId), null).get(agreementId);

        final List<Model.PayPlanRow> payPlanRows = new ArrayList<Model.PayPlanRow>();
        if (null != agreementState) {
            for (final IUniscEduAgreementPaymentState.StatePlanRow planRow: agreementState.getPlanRows()) {
                final UniscEduAgreementPayPlanRow realPlanRow = (UniscEduAgreementPayPlanRow) session.get(UniscEduAgreementPayPlanRow.class, planRow.getId()) /* from cache */;
                final String title = realPlanRow.getOwner().getPayPlanFreq().toString(realPlanRow.getOwner().getConfig(), realPlanRow.getStage());
                final Collection<StateFactRow> factRows = planRow.getFactRows();
                if (factRows.isEmpty()) {
                    payPlanRows.add(new Model.PayPlanRow(planRow.getId(), title) {
                        /* для строки без платежей */
                        private static final long serialVersionUID = 1L;
                        @Override public UniscEduAgreementPayPlanRow getPlanRow() { return realPlanRow; }
                        @Override public IUniscEduAgreementPaymentState.StateFactRow getFactRow() { return null; }
                    });
                } else {
                    for (final IUniscEduAgreementPaymentState.StateFactRow factRow: factRows) {
                        payPlanRows.add(new Model.PayPlanRow(factRow.getId(), title) {
                            /* строка с платежом */
                            private static final long serialVersionUID = 1L;
                            @Override public UniscEduAgreementPayPlanRow getPlanRow() { return realPlanRow; }
                            @Override public IUniscEduAgreementPaymentState.StateFactRow getFactRow() { return factRow; }
                        });
                    }
                }
            }
        }
        model.setPayPlanRows(payPlanRows);


    }
}
