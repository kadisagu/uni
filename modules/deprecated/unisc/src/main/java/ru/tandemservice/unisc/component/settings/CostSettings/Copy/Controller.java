package ru.tandemservice.unisc.component.settings.CostSettings.Copy;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

/**
 * @author vdanilov
 */
public class Controller extends AbstractBusinessController<IDAO, Model> {
	@Override
	public void onRefreshComponent(final IBusinessComponent component) {
		getDao().prepare(getModel(component));
	}

	public void onClickApply(final IBusinessComponent component) {
		getDao().save(getModel(component));
		deactivate(component);
	}
}
