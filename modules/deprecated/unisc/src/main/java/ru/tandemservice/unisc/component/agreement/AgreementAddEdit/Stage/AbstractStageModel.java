package ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Return;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key="agreement", binding="agreement"),
    @Bind(key="personRole", binding="personRole")
})
@Return({ @Bind(key=AbstractStageModel.RESULT, binding="result") })
@SuppressWarnings("deprecation")
public class AbstractStageModel {
    public static final String RESULT="agreementAddEditStageResult";

    boolean first = true;

    private UniscEduAgreementBase agreement;
    public UniscEduAgreementBase getAgreement() { return this.agreement; }
    public void setAgreement(final UniscEduAgreementBase agreement) { this.agreement = agreement; }

    private PersonRole personRole;
    public PersonRole getPersonRole() { return this.personRole; }
    public void setPersonRole(final PersonRole personRole) { this.personRole = personRole; }


    private IStageResult result = IStageResult.CLOSE;
    public IStageResult getResult() { return this.result; }
    void setResult(final IStageResult result) { this.result = result; }
}
