/* $Id: DAO.java 8754 2009-06-29 09:27:43Z ekachanova $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisc.component.agreement.AgreementPaymentsTab;

import java.util.List;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unisc.dao.agreement.IUniscEduAgreementDAO;
import ru.tandemservice.unisc.entity.agreements.IUniscEduAgreement2PersonRole;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayFactRow;
import ru.tandemservice.unisc.entity.agreements.UniscEduMainAgreement;
import ru.tandemservice.unisc.entity.agreements.gen.UniscEduAgreementPayFactRowGen;

/**
 * @author vdanilov
 */
public class DAO extends UniBaseDao implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        IEntity e = get(model.getId());
        if (e instanceof UniscEduAgreementBase)
        {
            UniscEduMainAgreement agreement = ((UniscEduAgreementBase) e).getAgreement();
            model.setAgreement(agreement);
            model.setSecModel(new CommonPostfixPermissionModel(IUniscEduAgreementDAO.INSTANCE.get().getPermissionContext(agreement) + "AgreementPaymentsTab"));
        } else if (e instanceof IUniscEduAgreement2PersonRole)
        {
            IUniscEduAgreement2PersonRole relation = (IUniscEduAgreement2PersonRole) e;
            model.setAgreement(relation.getAgreement());
            model.setSecModel(new CommonPostfixPermissionModel(relation.getPermissionContext() + "AgreementPaymentsTab"));
        } else
        {
            if (null == e)
                throw new NullPointerException();
            throw new IllegalStateException(String.valueOf(e.getClass()));
        }
    }

    @Override
    public void prepareDataSource(final Model model)
    {
        final MQBuilder builder = new MQBuilder(UniscEduAgreementPayFactRowGen.ENTITY_CLASS, "row");
        builder.add(MQExpression.eq("row", UniscEduAgreementPayFactRowGen.L_AGREEMENT, model.getAgreement()));
        builder.addOrder("row", UniscEduAgreementPayFactRowGen.P_DATE);

        final List<UniscEduAgreementPayFactRow> resultList = builder.<UniscEduAgreementPayFactRow>getResultList(getSession());
        model.getDataSource().setTotalSize(resultList.size());
        model.getDataSource().setCountRow(resultList.size());
        model.getDataSource().createPage(resultList);
    }

    @Override
    public void delete(final Model model, final Long id)
    {
        delete(id);
    }
}
