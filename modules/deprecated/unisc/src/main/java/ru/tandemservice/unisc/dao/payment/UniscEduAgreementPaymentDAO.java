package ru.tandemservice.unisc.dao.payment;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.hibernate.Session;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQL;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.ui.formatters.RuMoneyFormatter;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayFactRow;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayPlanRow;
import ru.tandemservice.unisc.entity.agreements.UniscEduMainAgreement;
import ru.tandemservice.unisc.entity.agreements.gen.UniscEduAdditAgreementGen;
import ru.tandemservice.unisc.entity.agreements.gen.UniscEduAgreementPayFactRowGen;
import ru.tandemservice.unisc.entity.agreements.gen.UniscEduAgreementPayPlanRowGen;
import ru.tandemservice.unisc.entity.catalog.UniscPayPeriod.Freq;
import ru.tandemservice.unisc.entity.config.UniscFinesHistory;
import ru.tandemservice.unisc.entity.config.gen.UniscFinesHistoryGen;

import java.util.*;

/**
 * @author vdanilov
 */
public class UniscEduAgreementPaymentDAO extends UniBaseDao implements IUniscEduAgreementPaymentDAO {

    /** состояние */
    @SuppressWarnings("unchecked")
    protected static class State implements IUniscEduAgreementPaymentState, Cloneable {
        static final long ONE_HOUR = 60 * 60 * 1000L;
        static final long ONE_DAY = 24 * ONE_HOUR;


        private long lastTime = 0;
        public long getLastTime() { return this.lastTime; }

        private double fines_tax = 0;
        public double getFinesTax() { return this.fines_tax; }

        private boolean fines_active = true;
        public boolean isFinesActive() { return this.fines_active; }

        private final List<PlanRowStateImpl> planRows = new ArrayList<>(4);
        private final Queue<FactRowOverpayStateImpl> factQueues[] = new Queue[] { new ArrayDeque<FactRowOverpayStateImpl>(4), new ArrayDeque<FactRowOverpayStateImpl>(4) };

        @Override public Collection<StatePlanRow> getPlanRows() {
            return Collections.<StatePlanRow>unmodifiableList(planRows);
        }
        @Override public Collection<StateFactRow> getOverpayRows() {
            return Collections.<StateFactRow>unmodifiableCollection(factQueues[0]);
        }

        @Override public double[] getDebt(final Date date) {
            final double debt[] = new double[] { 0d, 0d };
            for (final PlanRowStateImpl s: planRows) {
                if ((null == date) || date.after(s.getDate())) {
                    debt[0] += s.debt[0];
                    debt[1] += s.debt[1];
                }
            }
            return debt;
        }

        @Override
        public State clone() {
            try { return (State) super.clone(); }
            catch (final CloneNotSupportedException e) { throw new RuntimeException(e); }
        }

        private void process_fact_row(final IUniscEduAgreementPaymentEvent.FactRow event) {
            final double cost = (event.getJCostAsDouble() + event.getNCostAsDouble());
            factQueues[(event.isFinesPayment() ? 1 : 0)].add(new FactRowOverpayStateImpl(event.getId(), event.getEventDate(), cost));
        }

        private void process_plan_row(final IUniscEduAgreementPaymentEvent.PlanRow event) {
            final double cost = event.getJCostAsDoubleDiscounted() + event.getNCostAsDoubleDiscounted();
            planRows.add(new PlanRowStateImpl(event.getId(), event.getEventDate(), cost));
        }

        private void process_fines_state(final IUniscEduAgreementPaymentEvent.FinesState event) {
            fines_active = event.isFinesActive();
        }

        private void process_fines_tax(final IUniscEduAgreementPaymentEvent.FinesTax event) {
            fines_tax = Math.max(0, event.getFinesTax());
        }

        @Override public void process(final IUniscEduAgreementPaymentEvent event) {
            final long time = (DateUtils.truncate(event.getEventDate(), Calendar.DATE).getTime() + ONE_HOUR) / State.ONE_DAY;
            if (time < lastTime) { throw new IllegalStateException(); }

            final int days = ((lastTime > 0) ? ((int)(time-lastTime)) : 0);

            process__pre_process_event(event, days);
            process__process_event(event, days);
            process__post_process_event(event, days);

            lastTime = time;
        }

        /* pre-process event handle stage (1) */
        protected void process__pre_process_event(final IUniscEduAgreementPaymentEvent event, final int days) {
            if (fines_active && (days > 0) && (fines_tax > 0d)) {
                for (final PlanRowStateImpl s: planRows) {
                    final FinesRowStateImpl finesRow = calculate_fines(fines_tax, days, s);
                    if (null != finesRow) { s.register_fines(finesRow); }
                }
            }
        }

        /* process event handle stage (2) */
        protected void process__process_event(final IUniscEduAgreementPaymentEvent event, final int days) {
            if (event instanceof IUniscEduAgreementPaymentEvent.PlanRow) {
                process_plan_row((IUniscEduAgreementPaymentEvent.PlanRow)event);

            } else if (event instanceof IUniscEduAgreementPaymentEvent.FactRow) {
                process_fact_row((IUniscEduAgreementPaymentEvent.FactRow)event);

            } else if (event instanceof IUniscEduAgreementPaymentEvent.FinesState) {
                process_fines_state((IUniscEduAgreementPaymentEvent.FinesState)event);

            } else if (event instanceof IUniscEduAgreementPaymentEvent.FinesTax) {
                process_fines_tax((IUniscEduAgreementPaymentEvent.FinesTax)event);

            } else {
                throw new IllegalArgumentException(event.getClass().getName());

            }
        }

        /* post-process event handle stage (3) */
        protected void process__post_process_event(final IUniscEduAgreementPaymentEvent event, final int days) {
            for (final PlanRowStateImpl s: planRows) {
                if (factQueues[0].isEmpty() && factQueues[1].isEmpty() /* а нет больше платежей */) { break; }
                s.eat(factQueues); /* откусить от платежей кусочек */
            }
        }

        /* fact-row (overpay, mutable) */
        protected static class FactRowOverpayStateImpl implements StateFactRow {
            @Override public Long getId() { return id; }
            @Override public Date getDate() { return date; }
            @Override public double getPayment() { return payment; }
            @Override public double[] getDebt() { return null; }
            @Override public Collection<StateFinesRow> getFinesRows() { return null; }

            private final Long id;
            private final Date date;
            private double payment = 0d;

            private FactRowOverpayStateImpl(final Long id, final Date date, final double payment) {
                this.id = id;
                this.date = date;
                this.payment = payment;
            }
        }

        /* fact-row (payment, immutable) */
        protected static class FactRowStateImpl implements StateFactRow {
            @Override public Long getId() { return id; }
            @Override public Date getDate() { return date; }
            @Override public double getPayment() { return payment; }
            @Override public double[] getDebt() { return debt; }
            @Override public Collection<StateFinesRow> getFinesRows() { return Arrays.<StateFinesRow>asList(this.fines); }

            private final Long id;
            private final Date date;
            private final double payment;
            private final double debt[];
            private final FinesRowStateImpl fines[];

            private FactRowStateImpl(final Long id, final Date date, final double payment, final double debt[], final FinesRowStateImpl fines[]) {
                this.id = id;
                this.date = date;
                this.payment = payment;
                this.debt = debt;
                this.fines = fines;
            }
        }

        /* plan-row */
        protected static class PlanRowStateImpl implements StatePlanRow {
            @Override public Long getId() { return id; }
            @Override public Date getDate() { return date; }
            @Override public double getCost() { return cost; }
            @Override public double[] getDebt() { return new double[] { this.debt[0], this.debt[1] }; /* copy */ }
            @Override public Collection<StateFactRow> getFactRows() { return Collections.unmodifiableCollection(payments); }

            private final Long id;
            private final Date date;
            private final double cost;
            private final double debt[];
            private final List<StateFactRow> payments = new ArrayList<>(2);
            private final List<FinesRowStateImpl> fines = new ArrayList<>(2);

            private FinesRowStateImpl[] finesToArray() {
                if (fines.isEmpty()) { return FinesRowStateImpl.EMPTY_FINES_ARRAY; }
                try { return fines.toArray(new FinesRowStateImpl[fines.size()]); }
                finally { fines.clear(); }
            }

            private PlanRowStateImpl(final Long id, final Date date, final double debt) {
                this.id = id;
                this.date = date;
                this.debt = new double[] { this.cost = debt, 0d };
            }

            public void eat(final Queue<FactRowOverpayStateImpl>[] queues) {
                while (eat(queues, 0 /* основные платежы */)) {}
                while (eat(queues, 1 /* пеней */)) {}
            }

            private boolean eat(final Queue<FactRowOverpayStateImpl>[] queues, final int i) {
                final double d = this.debt[i];
                if (d <= 0.0d) { return false /* если долга нет - то ничего не надо больше делать */; }

                final Queue<FactRowOverpayStateImpl> queue = queues[i];
                final FactRowOverpayStateImpl top = queue.peek();
                if (null == top) { return false /* если нет платежей - то тоже ничего не делаем */; }

                final double diff = Math.min(top.payment, d);
                if (diff <= 0.0d) { return false /* если ничего не нужно погашать - не делаем ничего */; }

                top.payment -= diff;
                if (top.payment <= 0.0d) { queue.remove(); }

                this.debt[i] -= diff;
                final double[] debtAfterPayment = this.getDebt();
                final FinesRowStateImpl[] currentFinesRowArray = finesToArray();
                payments.add(new FactRowStateImpl(top.id, top.date, diff, debtAfterPayment, currentFinesRowArray));

                return true /* удачно */;
            }

            private void register_fines(final FinesRowStateImpl finesRow) {
                this.fines.add(finesRow);
                this.debt[1] += finesRow.fines;
            }
        }

        /* fines-row */
        protected static class FinesRowStateImpl implements StateFinesRow {
            public static final FinesRowStateImpl[] EMPTY_FINES_ARRAY = new FinesRowStateImpl[] {};

            final double fines_tax;
            final int days;
            final double debt;
            final double fines;

            @Override public double getFinesTax() { return fines_tax; }
            @Override public int getDays() { return days; }
            @Override public double getDebt() { return debt; }
            @Override public double getFines() { return fines; }

            public FinesRowStateImpl(final double finesTax, final int days, final double debt, final double fines) {
                this.fines_tax = finesTax;
                this.days = days;
                this.debt = debt;
                this.fines = fines;
            }
        }

        protected FinesRowStateImpl calculate_fines(final double fines_tax, final int days, final PlanRowStateImpl s) {
            final double debt = s.debt[0];
            if (debt <= 0.0d) { return null; }
            return new FinesRowStateImpl(fines_tax, days, debt, debt * (fines_tax * 0.01d * days));
        }

        @Override
        public String toString() {
            final int a = CollectionUtils.countMatches(planRows, object -> (((PlanRowStateImpl)object).debt[0] <= 0d));

            final double[] debt = getDebt(null);
            return (
                    (DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date(getLastTime()*State.ONE_DAY))+ ": ") +
                    ("оплачено = " + a + "/" + planRows.size()) + ", " +
                    (factQueues[0].isEmpty() ? ("долг = " + RuMoneyFormatter.INSTANCE.format(debt[0]) + ", ") : ("")) +
                    (factQueues[1].isEmpty() ? ("пеней = " + RuMoneyFormatter.INSTANCE.format(debt[1]) + ", ") : ("")) +
                    (" ("+(fines_active ? "начислять пеней, "+UniscFinesHistory.TAX_FORMATTER.format(fines_tax)+"% в день": "не начислять пеней")+")")
            );
        }
    }

    @Override public IUniscEduAgreementPaymentState getNewState() {
        return new State();
    }

    protected int priority(final IUniscEduAgreementPaymentEvent e) {
        if (e instanceof IUniscEduAgreementPaymentEvent.PlanRow) { return 1; }
        if (e instanceof IUniscEduAgreementPaymentEvent.FactRow) { return 2; }
        return 3;
    }

    private List<IUniscEduAgreementPaymentEvent> getGlobalEvents() {
        final MQBuilder builder = new MQBuilder(UniscFinesHistoryGen.ENTITY_CLASS, "f");
        return builder.<IUniscEduAgreementPaymentEvent>getResultList(getSession());
    }

    /* отбрасывает перекрытые элементы (перекрытие строк графика оплат) */
    private void filter(final List<IUniscEduAgreementPaymentEvent> events) {

        /* сортируем (важно, чтобы договор шел первым, затем допсоглашения в порядке регистрации) */
        Collections.sort(events, (o1, o2) -> {
            if (!(o1 instanceof UniscEduAgreementPayPlanRow)) { return 0; }
            if (!(o2 instanceof UniscEduAgreementPayPlanRow)) { return 0; }

            final UniscEduAgreementBase a1 = ((UniscEduAgreementPayPlanRow)o1).getOwner();
            if (a1 instanceof UniscEduMainAgreement) { return -1; }

            final UniscEduAgreementBase a2 = ((UniscEduAgreementPayPlanRow)o2).getOwner();
            if (a2 instanceof UniscEduMainAgreement) { return 1; }

            final int formingDateDiff = a1.getFormingDate().compareTo(a2.getFormingDate());
            if (0 != formingDateDiff) { return formingDateDiff; }

            final int idDiff = a1.getId().compareTo(a2.getId());
            if (0 != idDiff) { return idDiff; }

            return o1.getEventDate().compareTo(o2.getEventDate());
        });

        /* вспомогательная структура (хранит этап и соглашение, в котором этот этап находится) */
        class Record {
            final UniscEduAgreementBase owner;
            final int stage;
            public Record(final UniscEduAgreementBase owner, final int stage) {
                this.owner = owner;
                this.stage = stage;
            }

            @Override public int hashCode() { return owner.hashCode() ^ stage; }
            @Override public boolean equals(final Object obj) {
                if (obj instanceof Record) {
                    final Record rec = (Record) obj;
                    return (owner.equals(rec.owner) && (stage == rec.stage));
                }
                return false;
            }
        }

        final Record[] bitmap = new Record[64]; /* должно хватить, даже если у нас 10 курсов по 6 полусеместров */
        final Set<Record> coveredRecords = new HashSet<>();

        for(final IUniscEduAgreementPaymentEvent e: events) {
            if (e instanceof UniscEduAgreementPayPlanRow) {
                final UniscEduAgreementPayPlanRow row = (UniscEduAgreementPayPlanRow) e;
                final UniscEduAgreementBase owner = row.getOwner();
                final Freq freq = owner.getPayPlanFreq().freq();

                final int i0 = freq.index(owner.getConfig(), row.getStage());
                final int i1 = freq.index(owner.getConfig(), 1+row.getStage());
                for (int i=i0;i<i1;i++) {
                    final Record record = bitmap[i];
                    if ((null != record) && !record.owner.equals(owner)) {
                        // если мы нашли что-то, что мы перекрываем - запоминаем
                        coveredRecords.add(record);
                    }
                    bitmap[i] = new Record(owner, row.getStage());
                }

            }
        }

        // теперь надо тпочистить то, что мы нащли (важно помнить, что строк может быть несколько на один этап)
        final Iterator<IUniscEduAgreementPaymentEvent> it = events.iterator();
        while (it.hasNext()) {
            final IUniscEduAgreementPaymentEvent e = it.next();
            if (e instanceof UniscEduAgreementPayPlanRow) {
                final UniscEduAgreementPayPlanRow planRow = ((UniscEduAgreementPayPlanRow) e);
                if (coveredRecords.contains(new Record(planRow.getOwner(), planRow.getStage()))) {
                    it.remove(); // перекрывается чем-то
                }
            }
        }
    }

    /* сортирует события по дате и приоритету */
    private void sortEvents(final List<IUniscEduAgreementPaymentEvent> events) {
        Collections.sort(events, (o1, o2) -> {
            final int result = o1.getEventDate().compareTo(o2.getEventDate());
            if (0 != result) { return result; }

            final int p1 = priority(o1), p2 = priority(o2);
            return (p1-p2);
        });
    }

    @Override
    public Map<Long, List<IUniscEduAgreementPaymentEvent>> getEventsMap(final Collection<Long> agreementIds, final Date date) {
        Debug.begin("UniscEduAgreementPaymentDAO.getEventsMap(sz="+agreementIds.size()+")");
        final List<IUniscEduAgreementPaymentEvent> global = getGlobalEvents();
        final Map<Long, List<IUniscEduAgreementPaymentEvent>> listMap = SafeMap.get(key -> new ArrayList<>(global));

        final Session session = getSession();
        BatchUtils.execute(agreementIds, DQL.MAX_VALUES_ROW_NUMBER, ids -> {

            /* строки договора */ {
                final MQBuilder b = new MQBuilder(UniscEduAgreementPayPlanRowGen.ENTITY_CLASS, "r");
                b.addJoinFetch("r", UniscEduAgreementPayPlanRowGen.L_OWNER, "owner");

                if (null != date) { b.add(MQExpression.lessOrEq("r", UniscEduAgreementPayPlanRowGen.P_DATE, date)); }
                b.add(MQExpression.isNull("r", UniscEduAgreementPayPlanRowGen.L_REPLACED_BY));

                b.add(MQExpression.in("r", UniscEduAgreementPayPlanRowGen.L_OWNER+".id", ids));

                final List<UniscEduAgreementPayPlanRow> agreementRows = b.<UniscEduAgreementPayPlanRow>getResultList(session);
                for (final UniscEduAgreementPayPlanRow row: agreementRows) {
                    listMap.get(row.getOwner().getAgreement().getId()).add(row);
                }
            }

            /* строки доп соглашения */ {
                final MQBuilder b = new MQBuilder(UniscEduAgreementPayPlanRowGen.ENTITY_CLASS, "r");
                b.addJoinFetch("r", UniscEduAgreementPayPlanRowGen.L_OWNER, "owner");

                if (null != date) { b.add(MQExpression.lessOrEq("r", UniscEduAgreementPayPlanRowGen.P_DATE, date)); }
                b.add(MQExpression.isNull("r", UniscEduAgreementPayPlanRowGen.L_REPLACED_BY));

                b.addDomain("a", UniscEduAdditAgreementGen.ENTITY_CLASS);
                b.add(MQExpression.in("a", UniscEduAdditAgreementGen.L_AGREEMENT+".id", ids));
                b.add(MQExpression.eqProperty("r", UniscEduAgreementPayPlanRowGen.L_OWNER+".id", "a", "id"));

                final List<UniscEduAgreementPayPlanRow> agreementRows = b.<UniscEduAgreementPayPlanRow>getResultList(session);
                for (final UniscEduAgreementPayPlanRow row: agreementRows) {
                    listMap.get(row.getOwner().getAgreement().getId()).add(row);
                }
            }

            /* платежи */ {
                final MQBuilder b = new MQBuilder(UniscEduAgreementPayFactRowGen.ENTITY_CLASS, "r");
                if (null != date) { b.add(MQExpression.lessOrEq("r", UniscEduAgreementPayPlanRowGen.P_DATE, date)); }
                b.add(MQExpression.in("r", UniscEduAgreementPayFactRowGen.L_AGREEMENT+".id", ids));

                final List<UniscEduAgreementPayFactRow> agreementRows = b.<UniscEduAgreementPayFactRow>getResultList(session);
                for (final UniscEduAgreementPayFactRow row: agreementRows) {
                    listMap.get(row.getAgreement().getId()).add(row);
                }
            }

        });

        /* intersect & sort elements */ {
            Debug.begin("UniscEduAgreementPaymentDAO.getEventsMap.sort(size="+listMap.size()+")");
            for (final Map.Entry<Long, List<IUniscEduAgreementPaymentEvent>> e: listMap.entrySet()) {
                final List<IUniscEduAgreementPaymentEvent> events = e.getValue();
                filter(events);
                sortEvents(events);
            }
            Debug.end();
        }

        Debug.end();
        return listMap;
    }


    @Override
    public Map<Long, IUniscEduAgreementPaymentState> getStateMap(final Collection<Long> agreementIds, final Date date) {
        Debug.begin("UniscEduAgreementPaymentDAO.getStateMap");
        final Map<Long, List<IUniscEduAgreementPaymentEvent>> listMap = getEventsMap(agreementIds, date);
        final Map<Long, IUniscEduAgreementPaymentState> result = new HashMap<>(listMap.size());
        for (final Map.Entry<Long, List<IUniscEduAgreementPaymentEvent>> e: listMap.entrySet()) {
            final IUniscEduAgreementPaymentState state = getNewState();
            for (final IUniscEduAgreementPaymentEvent event: e.getValue()) {
                state.process(event);
            }
            result.put(e.getKey(), state);
        }
        Debug.end();
        return result;
    }

}
