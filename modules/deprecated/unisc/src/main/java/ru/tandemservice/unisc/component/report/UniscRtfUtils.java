package ru.tandemservice.unisc.component.report;

import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.util.RtfUtil;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.List;

@SuppressWarnings("deprecation")
public class UniscRtfUtils
{
    public static void afterModify(List<RtfRow> newRowList, int startIndex, List<String[]> reportRowList)
    {
        for (int i = 0; i < reportRowList.size(); i++)
        {
            if (reportRowList.get(i).length == 1)
            {
                RtfRow row = newRowList.get(startIndex + i);

                // объединяем все ячейки в строке и в получившуюся строку вставляем текст из ячейки 0
                RtfUtil.unitAllCell(row, 0);

                // выделяем жирным
                UniRtfUtil.setRowBold(row);

                // меняем теги выравнивания на тег выравнивания по центру
                SharedRtfUtil.setCellAlignment(row.getCellList().get(0), IRtfData.QJ, IRtfData.QC);
                SharedRtfUtil.setCellAlignment(row.getCellList().get(0), IRtfData.QL, IRtfData.QC);
                SharedRtfUtil.setCellAlignment(row.getCellList().get(0), IRtfData.QR, IRtfData.QC);

                // берем строку "Итого" предыдущей группы
                if (i == 0) continue;
                RtfRow prev = newRowList.get(startIndex + i - 1);

                // выделяем жирным
                UniRtfUtil.setCellBold(prev.getCellList().get(0));
            }
        }
        // берем строку "Итого" последней группы
        UniRtfUtil.setCellBold(newRowList.get(reportRowList.size()).getCellList().get(0));
    }
}