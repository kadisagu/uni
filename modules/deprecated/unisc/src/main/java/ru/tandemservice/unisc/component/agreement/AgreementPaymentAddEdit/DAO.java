/* $Id: DAO.java 8754 2009-06-29 09:27:43Z ekachanova $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisc.component.agreement.AgreementPaymentAddEdit;

import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unisc.UniscDefines;
import ru.tandemservice.unisc.dao.payment.IUniscEduAgreementPaymentDAO;
import ru.tandemservice.unisc.dao.payment.IUniscEduAgreementPaymentState;
import ru.tandemservice.unisc.entity.agreements.*;
import ru.tandemservice.unisc.entity.catalog.UniscPaymentDocumentType;

import java.util.*;

/**
 * @author vdanilov
 */
public class DAO extends UniBaseDao implements IDAO
{
    @Override
    @SuppressWarnings("unchecked")
    public void prepare(final Model model)
    {
        final IEntity e = get(model.getId());
        if (e instanceof UniscEduAgreementPayFactRow)
        {
            model.setRow((UniscEduAgreementPayFactRow) e);
        } else if (e instanceof UniscEduAgreementBase)
        {
            model.setRow(new UniscEduAgreementPayFactRow());
            model.getRow().setAgreement(((UniscEduAgreementBase) e).getAgreement());
        } else if (e instanceof IUniscEduAgreement2PersonRole)
        {
            model.setRow(new UniscEduAgreementPayFactRow());
            model.getRow().setAgreement(((IUniscEduAgreement2PersonRole) e).getAgreement());
        } else
        {
            if (null == e) {
                throw new NullPointerException();
            }
            throw new IllegalStateException(String.valueOf(e.getClass()));
        }

        final UniscEduMainAgreement agreement = model.getRow().getAgreement();

        model.setTypes(getCatalogItemList(UniscPaymentDocumentType.class));

        final List<UniscEduAdditAgreement> additAgreementList = getList(UniscEduAdditAgreement.class, UniscEduAdditAgreement.L_AGREEMENT, agreement);

        final Set<String> customerTypeCodes = new HashSet<String>((List) CommonBaseUtil.getPropertiesList(additAgreementList, UniscEduAdditAgreement.L_CUSTOMER_TYPE + ".code"));
        customerTypeCodes.add(agreement.getCustomerType().getCode());

        model.getPayerTypeList().clear();
        model.getNPayerList().clear();
        model.getJPayerList().clear();

        final IdentifiableWrapper nPayerType = new IdentifiableWrapper(Model.N_PAYER_ID, "Физическое лицо");
        if (customerTypeCodes.contains(UniscDefines.CUSTOMER_TYPE_NATURAL_PERSON) || customerTypeCodes.contains(UniscDefines.CUSTOMER_TYPE_COMPLEX_PERSON)) {
            model.getPayerTypeList().add(nPayerType);
            final MQBuilder builder = new MQBuilder(UniscEduAgreementNaturalPerson.ENTITY_CLASS, "a");
            builder.add(MQExpression.eq("a", UniscEduAgreementNaturalPerson.L_OWNER, agreement));
            builder.addOrder("a", UniscEduAgreementNaturalPerson.P_LAST_NAME);
            builder.addOrder("a", UniscEduAgreementNaturalPerson.P_FIRST_NAME);
            final List<UniscEduAgreementNaturalPerson> naturalPersons = builder.getResultList(getSession());
            long counter = 0L;
            for (final UniscEduAgreementNaturalPerson person : naturalPersons) {
                model.getNPayerList().add(new IdentifiableWrapper(counter++, person.getFullFio()));
            }
        }

        final IdentifiableWrapper jPayerType = new IdentifiableWrapper(Model.J_PAYER_ID, "Юридическое лицо");
        if (customerTypeCodes.contains(UniscDefines.CUSTOMER_TYPE_JURIDICAL_PERSON) || customerTypeCodes.contains(UniscDefines.CUSTOMER_TYPE_COMPLEX_PERSON)) {
            model.getPayerTypeList().add(jPayerType);
            long counter = 0L;
            if (agreement.getJuridicalPerson() != null) {
                model.getJPayerList().add(new IdentifiableWrapper(counter++, agreement.getJuridicalPerson().getContractor().getTitle()));
            }
            for (final UniscEduAdditAgreement additAgreement : additAgreementList) {
                if (additAgreement.getJuridicalPerson() != null) {
                    model.getJPayerList().add(new IdentifiableWrapper(counter++, additAgreement.getJuridicalPerson().getContractor().getTitle()));
                }
            }
        }

        model.setSum(model.getRow().getCostAsDouble());
        if (model.getRow().getJCostAsDouble() > 0) {
            model.setPayerType(jPayerType);
        }

        if (model.getRow().getNCostAsDouble() > 0) {
            model.setPayerType(nPayerType);
        }

        if ((model.getPayerType() != null) && !model.getPayerTypeList().contains(model.getPayerType())) {
            model.getPayerTypeList().add(model.getPayerType());
            Collections.sort(model.getPayerTypeList(), ITitled.TITLED_COMPARATOR);
        }
    }

    @Override
    public void save(final Model model)
    {
        final UniscEduAgreementPayFactRow payment = getPaymentWithCost(model);

        if (model.getPayer() != null) {
            payment.setPayer(model.getPayer().getTitle());
        }

        getSession().saveOrUpdate(payment);
    }

    private UniscEduAgreementPayFactRow getPaymentWithCost(final Model model) {
        final UniscEduAgreementPayFactRow payment = model.getRow();
        if (Model.J_PAYER_ID.equals(model.getPayerType().getId())) {
            payment.setJCostAsDouble(model.getSum());
        } else if (Model.N_PAYER_ID.equals(model.getPayerType().getId())) {
            payment.setNCostAsDouble(model.getSum());
        } else {
            throw new ApplicationException(ApplicationRuntime.getProperty(CoreExceptionUtils.APPLICATION_ERROR_MESSAGE_PROPETY));
        }
        return payment;
    }

    @Override
    public String getConfirmMessage(final Model model)
    {
        if (model.getRow().isFinesPayment()) {
            return null;
        }
        final UniscEduMainAgreement agreement = model.getRow().getAgreement();
        final Long agreementId = agreement.getId();
        final Map<Long, IUniscEduAgreementPaymentState> stateMap = IUniscEduAgreementPaymentDAO.INSTANCE.get().getStateMap(Arrays.asList(agreementId), null);
        final IUniscEduAgreementPaymentState state = stateMap.get(agreementId);
        double currentDebt = 0;
        double totalDebt = 0;
        long planRowId = 0;
        for (final IUniscEduAgreementPaymentState.StatePlanRow planRow : state.getPlanRows()) {
            final double debt = planRow.getDebt()[0];
            totalDebt += debt;
            if (currentDebt == 0)
            {
                planRowId = planRow.getId();
                currentDebt = debt;
            }
        }
        if (totalDebt < model.getSum()) {
            return "Общая сумма по всем зарегистрированным платежам превышает сумму, указанную в договоре. Добавить платеж?";
        }
        if (currentDebt < model.getSum()) {
            return "Сумма регистрируемого платежа превышает сумму оплаты за этап «"+agreement.getPayPlanFreq().toString(agreement.getConfig(), get(UniscEduAgreementPayPlanRow.class, planRowId).getStage())+"» и будет засчитана за следующий этап(ы).";
        }
        return null;
    }
}
