package ru.tandemservice.unisc.component.agreement.AdditionalAgreementPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;

import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.unisc.dao.agreement.IUniscEduAgreementDAO;
import ru.tandemservice.unisc.entity.agreements.UniscEduAdditAgreement;

@State( { @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "agreement.id"), @Bind(key = "selectedTab", binding = "selectedTab") })
public class Model
{

    private String selectedTab;

    public String getSelectedTab()
    {
        return this.selectedTab;
    }

    public void setSelectedTab(final String selectedTab)
    {
        this.selectedTab = selectedTab;
    }

    private UniscEduAdditAgreement agreement = new UniscEduAdditAgreement();

    public UniscEduAdditAgreement getAgreement()
    {
        return this.agreement;
    }

    public void setAgreement(final UniscEduAdditAgreement agreement)
    {
        this.agreement = agreement;
    }

    public boolean isReadOnly()
    {
        if (IUniscEduAgreementDAO.INSTANCE.get().isEditBottonVisible(getAgreement().getAgreement()))
        {
            return true;
        }
        return (!IUniscEduAgreementDAO.INSTANCE.get().isMutable(getAgreement()));
    }

    private PersonRole role;

    public PersonRole getPersonRole()
    {
        return this.role;
    }

    protected void setPersonRole(final PersonRole role)
    {
        this.role = role;
    }

}
