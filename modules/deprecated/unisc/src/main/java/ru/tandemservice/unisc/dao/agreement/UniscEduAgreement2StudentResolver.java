package ru.tandemservice.unisc.dao.agreement;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hibernate.Session;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unisc.entity.agreements.IUniscEduAgreement2PersonRole;
import ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student;

/**
 * @author vdanilov
 */
@SuppressWarnings("unchecked")
public class UniscEduAgreement2StudentResolver extends UniBaseDao implements IUniscEduAgreement2PersonRoleResolver {

    public static Collection<IUniscEduAgreement2PersonRole<PersonRole>> resolveRelations(final Class<? extends IUniscEduAgreement2PersonRole> klass, final String path, final Collection<Long> valueIds, final Session session) {
        final List<IUniscEduAgreement2PersonRole<PersonRole>> result = new ArrayList<IUniscEduAgreement2PersonRole<PersonRole>>(valueIds.size());
        BatchUtils.execute(valueIds, 200, new BatchUtils.Action<Long>() {
            @Override public void execute(Collection<Long> elements) {
                MQBuilder builder = new MQBuilder(klass.getName(), "e");
                builder.add(MQExpression.in("e", path+".id", elements));
                for (IUniscEduAgreement2PersonRole<PersonRole> rel: builder.<IUniscEduAgreement2PersonRole<PersonRole>>getResultList(session)) {
                    result.add(rel);
                }
            }
        });
        return result;
    }



    @Override public int priority() { return 0; }
    @Override public Collection<IUniscEduAgreement2PersonRole<PersonRole>> getRelations4Agreements(Collection<Long> mainAgreementIds) {
        return (Collection)resolveRelations(UniscEduAgreement2Student.class, UniscEduAgreement2Student.agreement().s(), mainAgreementIds, getSession());
    }
    @Override public Collection<IUniscEduAgreement2PersonRole<PersonRole>> getRelations4PersonRole(Collection<Long> personRoleIds) {
        return (Collection)resolveRelations(UniscEduAgreement2Student.class, UniscEduAgreement2Student.student().s(), personRoleIds, getSession());
    }
}
