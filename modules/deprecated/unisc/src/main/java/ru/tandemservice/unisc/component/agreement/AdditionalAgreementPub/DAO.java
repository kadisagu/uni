package ru.tandemservice.unisc.component.agreement.AdditionalAgreementPub;

import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unisc.dao.agreement.IUniscEduAgreementDAO;
import ru.tandemservice.unisc.entity.agreements.IUniscEduAgreement2PersonRole;
import ru.tandemservice.unisc.entity.agreements.UniscEduAdditAgreement;


public class DAO extends UniDao<Model> implements IDAO {
	@Override
	public void prepare(final Model model) {
		model.setAgreement(get(UniscEduAdditAgreement.class, model.getAgreement().getId()));

		/* invalidate role */ {
			final IUniscEduAgreement2PersonRole<PersonRole> rel = IUniscEduAgreementDAO.INSTANCE.get().getRelation(model.getAgreement().getAgreement());
			if (null != rel) {
				model.setPersonRole(rel.getTarget());
			}
		}
	}
}
