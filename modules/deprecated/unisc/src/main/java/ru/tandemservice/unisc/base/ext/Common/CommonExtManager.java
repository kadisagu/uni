/* $Id:$ */
package ru.tandemservice.unisc.base.ext.Common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.util.IModuleStatusReporter;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student;

import java.util.Collections;

/**
 * @author oleyba
 * @since 2/11/14
 */
@Configuration
public class CommonExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private CommonManager _commonManager;

    @Bean
    public ItemListExtension<IModuleStatusReporter> moduleStatusExtPoint()
    {
        return itemListExtension(_commonManager.moduleStatusExtPoint())
            .add("unisc", () -> Collections.singletonList("Договоров: " + IUniBaseDao.instance.get().getCount(UniscEduAgreement2Student.class, UniscEduAgreement2Student.agreement().config().educationYear().current().s(), Boolean.TRUE) + " (тек. уч. год), " + IUniBaseDao.instance.get().getCount(UniscEduAgreement2Student.class) + " (всего)"))
            .create();
    }
}
