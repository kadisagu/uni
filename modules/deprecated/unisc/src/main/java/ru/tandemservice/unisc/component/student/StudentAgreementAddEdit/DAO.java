package ru.tandemservice.unisc.component.student.StudentAgreementAddEdit;

import java.util.List;

import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisc.entity.agreements.IUniscEduAgreement2PersonRole;
import ru.tandemservice.unisc.entity.agreements.UniscEduMainAgreement;
import ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student;
import ru.tandemservice.unisc.entity.agreements.student.gen.UniscEduAgreement2StudentGen;

/**
 * @author vdanilov
 */
public class DAO extends ru.tandemservice.unisc.component.agreement.AgreementRelationAddEdit.DAO<Student> {

	@Override
	public void prepare(final ru.tandemservice.unisc.component.agreement.AgreementRelationAddEdit.Model<Student> model) {
		super.prepare(model);
		final UniscEduAgreement2Student rel = (UniscEduAgreement2Student)model.getRelation();
		if (!rel.isActive()) {
			throw new ApplicationException("Нельзя изменять недействующий договор.");
		}
        if (rel.getStudent().isArchival())
            throw new ApplicationException("Нельзя изменять договор архивного студента.");
        if (rel.getAgreement().isCheckedBySignature())
            throw new ApplicationException("Нельзя изменять проверенный договор.");
	}

	@Override
	protected IUniscEduAgreement2PersonRole<Student> createRelation(final Student personRole) {
		final UniscEduAgreement2Student relation = new UniscEduAgreement2Student();
		relation.setActive(true);
		relation.setAgreement(new UniscEduMainAgreement());
		relation.setStudent(personRole);
		return relation;
	}

	@Override
	public void save(final ru.tandemservice.unisc.component.agreement.AgreementRelationAddEdit.Model<Student> model) {
		if ((null != model.getAgreement().getConfig()) && (null == model.getAgreement().getId())) {
			final Model m = (Model) model;
			if (Boolean.TRUE.equals(m.getForce())) {
				reassociate(model);

				final Session session = this.getSession();
				final MQBuilder builder = new MQBuilder(UniscEduAgreement2StudentGen.ENTITY_CLASS, "e");
				builder.add(MQExpression.eq("e", UniscEduAgreement2StudentGen.L_STUDENT+".id", model.getRelation().getTarget().getId()));
				builder.add(MQExpression.eq("e", UniscEduAgreement2StudentGen.P_ACTIVE, Boolean.TRUE));
				//builder.addDescOrder("e", UniscEduAgreement2StudentGen.L_AGREEMENT+"."+UniscEduAgreementBaseGen.P_FORMING_DATE);
				builder.addDescOrder("e", "id");

				final List<UniscEduAgreement2Student> result = builder.getResultList(session);
				for (final UniscEduAgreement2Student rel: result) {
					rel.setActive(false);
					getSession().saveOrUpdate(rel);
				}


				session.flush();

				((UniscEduAgreement2Student)model.getRelation()).setActive(true);
				super.save(model);
				return;
			}
		}

		((UniscEduAgreement2Student)model.getRelation()).setActive(true);
		super.save(model);
	}

	@Override
	protected void refreshAgreementAfterStageExecution(final ru.tandemservice.unisc.component.agreement.AgreementRelationAddEdit.Model<Student> model) {
		if ((null != model.getAgreement().getConfig()) && (null == model.getAgreement().getId())) {
			final Model m = (Model) model;
			final MQBuilder builder = new MQBuilder(UniscEduAgreement2StudentGen.ENTITY_CLASS, "e");
			builder.add(MQExpression.eq("e", UniscEduAgreement2StudentGen.L_STUDENT+".id", model.getRelation().getTarget().getId()));
			builder.add(MQExpression.eq("e", UniscEduAgreement2StudentGen.P_ACTIVE, Boolean.TRUE));
			// builder.addDescOrder("e", UniscEduAgreement2StudentGen.L_AGREEMENT+"."+UniscEduAgreementBaseGen.P_FORMING_DATE);
			builder.addDescOrder("e", "id");

			final List<UniscEduAgreement2Student> result = builder.getResultList(this.getSession());
			if (!result.isEmpty()) {

				if (Boolean.TRUE.equals(m.getForce())) {
					if (null == model.getAgreement().getNumber()) {
						model.getAgreement().setNumber(result.get(0).getAgreement().getNumber());
					}

				} else {

					// редактирование
					model.setId(result.get(0).getId());
					model.changeCreationFormFlag(null);
					this.prepare(model);
					return;

				}
			}
		}
		super.refreshAgreementAfterStageExecution(model);
	}
}
