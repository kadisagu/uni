/* $Id: DAO.java 6933 2009-03-07 05:41:27Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unisc.component.agreement.AdditionalAgreementPrint;

import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.rtf.document.RtfDocument;

import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.unisc.component.agreement.AgreementRelationPrint.UniscEduAgreementPrintDAOBase;
import ru.tandemservice.unisc.dao.agreement.IUniscEduAgreementDAO;
import ru.tandemservice.unisc.dao.print.IUniscPrintDAO;
import ru.tandemservice.unisc.entity.agreements.IUniscEduAgreement2PersonRole;
import ru.tandemservice.unisc.entity.agreements.UniscEduAdditAgreement;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPrintVersion;

/**
 * @author vdanilov
 */
public class DAO extends UniscEduAgreementPrintDAOBase implements IDAO {

    @Override
    public IDocumentRenderer createDocument(final Model model) throws Exception {
        final UniscEduAdditAgreement studentAdditionalAgreement = this.get(UniscEduAdditAgreement.class, model.getId());
        final UniscEduAgreementPrintVersion printVersion = IUniscPrintDAO.INSTANCE.get().getLastPrintVersion(studentAdditionalAgreement);
        if (null != printVersion) { return printVersion.getRenderer(); }

        final IUniscEduAgreement2PersonRole<PersonRole> rel = IUniscEduAgreementDAO.INSTANCE.get().getRelation(studentAdditionalAgreement.getAgreement());
        final RtfDocument document = IUniscPrintDAO.INSTANCE.get().getDocumentTemplate(studentAdditionalAgreement);
        IUniscPrintDAO.INSTANCE.get().getRtfInjectorModifier(studentAdditionalAgreement, (null == rel ? null : rel.getTarget().getPerson())).modify(document);
        this.applyBarcode(document, IUniscPrintDAO.INSTANCE.get().getBarcodeString(studentAdditionalAgreement));

        return new CommonBaseRenderer().rtf().fileName("StudentAdditionalContract.rtf").document(document);
    }
}
