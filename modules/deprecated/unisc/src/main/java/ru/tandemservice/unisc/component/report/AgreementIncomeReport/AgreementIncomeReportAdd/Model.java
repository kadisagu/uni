// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisc.component.report.AgreementIncomeReport.AgreementIncomeReportAdd;

import org.tandemframework.core.component.Input;

import ru.tandemservice.unisc.component.report.UniscReportAddModel;
import ru.tandemservice.unisc.entity.report.UniscAgreementIncomeReport;

/**
 * @author oleyba
 * @since 19.08.2009
 */
@Input(keys = "orgUnitId", bindings = "orgUnitId")
public class Model extends UniscReportAddModel
{
    private UniscAgreementIncomeReport _report = new UniscAgreementIncomeReport();
    private boolean byGroup;

    public UniscAgreementIncomeReport getReport()
    {
        return _report;
    }

    public void setReport(UniscAgreementIncomeReport report)
    {
        _report = report;
    }

    public boolean isByGroup()
    {
        return byGroup;
    }

    public void setByGroup(boolean byGroup)
    {
        this.byGroup = byGroup;
    }
}
