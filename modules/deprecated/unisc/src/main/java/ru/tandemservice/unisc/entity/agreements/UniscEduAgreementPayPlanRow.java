package ru.tandemservice.unisc.entity.agreements;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.uni.ui.formatters.MoneyFormatter;
import ru.tandemservice.uni.ui.formatters.RuMoneyFormatter;
import ru.tandemservice.unisc.dao.config.UniscEduOrgUnitConfigDAO;
import ru.tandemservice.unisc.dao.payment.IUniscEduAgreementPaymentEvent;
import ru.tandemservice.unisc.entity.agreements.gen.UniscEduAgreementPayPlanRowGen;

import java.util.Date;

/**
 * Строка графика оплат
 */
public class UniscEduAgreementPayPlanRow extends UniscEduAgreementPayPlanRowGen implements ITitled, IUniscEduAgreementPaymentEvent.PlanRow
{
    private static final RuMoneyFormatter mf = new RuMoneyFormatter();
    @Override
    public Long getOwnerId() { return getOwner().getId(); }

    @Override
    public double getJCostAsDoubleDiscounted() { return UniscEduOrgUnitConfigDAO.wrap(super.getJCost()) * (1 - (getDiscount() == null ? 0 : getDiscount() * 0.01)); }
    public double getJCostAsDouble() { return UniscEduOrgUnitConfigDAO.wrap(super.getJCost()); }
    public void setJCostAsDouble(final double d) { this.setJCost(UniscEduOrgUnitConfigDAO.unwrap(d)); }
    public String getJCostAsString() { return MoneyFormatter.ruMoneyFormatter(3).format(this.getJCost()); }

    @Override
    public double getNCostAsDoubleDiscounted() { return UniscEduOrgUnitConfigDAO.wrap(super.getNCost()) * (1 - (getDiscount() == null ? 0 : getDiscount() * 0.01)); }
    public double getNCostAsDouble() { return UniscEduOrgUnitConfigDAO.wrap(super.getNCost()); }
    public void setNCostAsDouble(final double d) { this.setNCost(UniscEduOrgUnitConfigDAO.unwrap(d)); }
    public String getNCostAsString() { return MoneyFormatter.ruMoneyFormatter(3).format(this.getNCost()); }

    public double getCostAsDouble() { return (getJCostAsDouble() + getNCostAsDouble()) * (1 - (getDiscount() == null ? 0 : getDiscount() * 0.01)); }
    public String getCostAsString() { return UniscEduAgreementPayPlanRow.mf.format(this.getCostAsDouble()); }

    public String getDiscountAsString() { return (this.getDiscount() == null) || (this.getDiscount() == .0) ? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(this.getDiscount()).replace("-", "+"); }

    @Override
    public Date getEventDate() { return getDate(); }
    public String getDateAsString() { return DateFormatter.DEFAULT_DATE_FORMATTER.format(super.getDate()); }


    @Override
    public String getTitle() {
        final String postfix = getDateAsString() + ": фл=" + getNCostAsString() + ", юл=" + getJCostAsString();
        if ((null == getOwner()) || (null == getOwner().getPayPlanFreq())) { return postfix; }
        return getOwner().getPayPlanFreq().toString(getOwner().getConfig(), getStage()) + " " + postfix;
    }


    public boolean isTransient() {
        if (null == this.getId()) { return true; }

        final IEntityMeta meta = EntityRuntime.getMeta(this.getId());
        if (null == meta) { return true; }

        return !UniscEduAgreementPayPlanRow.class.isAssignableFrom(meta.getEntityClass());
    }
}
