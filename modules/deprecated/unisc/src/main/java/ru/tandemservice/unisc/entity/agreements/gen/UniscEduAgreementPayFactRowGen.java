package ru.tandemservice.unisc.entity.agreements.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayFactRow;
import ru.tandemservice.unisc.entity.agreements.UniscEduMainAgreement;
import ru.tandemservice.unisc.entity.catalog.UniscPaymentDocumentType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Платеж
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniscEduAgreementPayFactRowGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayFactRow";
    public static final String ENTITY_NAME = "uniscEduAgreementPayFactRow";
    public static final int VERSION_HASH = -667949266;
    private static IEntityMeta ENTITY_META;

    public static final String L_AGREEMENT = "agreement";
    public static final String P_UUID = "uuid";
    public static final String P_DATE = "date";
    public static final String P_FINES_PAYMENT = "finesPayment";
    public static final String P_J_COST = "JCost";
    public static final String P_N_COST = "NCost";
    public static final String L_TYPE = "type";
    public static final String P_NUMBER = "number";
    public static final String P_PAYER = "payer";
    public static final String P_COMMENT = "comment";

    private UniscEduMainAgreement _agreement;     // Договор
    private String _uuid;     // Идентификатор (для связи с внешней системой)
    private Date _date;     // Дата
    private boolean _finesPayment;     // Платеж в счет погашения пеней
    private long _JCost;     // Стоимость (юр. лицо)
    private long _NCost;     // Стоимость (физ. лицо)
    private UniscPaymentDocumentType _type;     // Тип документа
    private String _number;     // Номер документа
    private String _payer;     // Плательщик
    private String _comment;     // Комментарий

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Договор. Свойство не может быть null.
     */
    @NotNull
    public UniscEduMainAgreement getAgreement()
    {
        return _agreement;
    }

    /**
     * @param agreement Договор. Свойство не может быть null.
     */
    public void setAgreement(UniscEduMainAgreement agreement)
    {
        dirty(_agreement, agreement);
        _agreement = agreement;
    }

    /**
     * @return Идентификатор (для связи с внешней системой).
     */
    @Length(max=255)
    public String getUuid()
    {
        return _uuid;
    }

    /**
     * @param uuid Идентификатор (для связи с внешней системой).
     */
    public void setUuid(String uuid)
    {
        dirty(_uuid, uuid);
        _uuid = uuid;
    }

    /**
     * @return Дата. Свойство не может быть null.
     */
    @NotNull
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date Дата. Свойство не может быть null.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return Платеж в счет погашения пеней. Свойство не может быть null.
     */
    @NotNull
    public boolean isFinesPayment()
    {
        return _finesPayment;
    }

    /**
     * @param finesPayment Платеж в счет погашения пеней. Свойство не может быть null.
     */
    public void setFinesPayment(boolean finesPayment)
    {
        dirty(_finesPayment, finesPayment);
        _finesPayment = finesPayment;
    }

    /**
     * @return Стоимость (юр. лицо). Свойство не может быть null.
     */
    @NotNull
    public long getJCost()
    {
        return _JCost;
    }

    /**
     * @param JCost Стоимость (юр. лицо). Свойство не может быть null.
     */
    public void setJCost(long JCost)
    {
        dirty(_JCost, JCost);
        _JCost = JCost;
    }

    /**
     * @return Стоимость (физ. лицо). Свойство не может быть null.
     */
    @NotNull
    public long getNCost()
    {
        return _NCost;
    }

    /**
     * @param NCost Стоимость (физ. лицо). Свойство не может быть null.
     */
    public void setNCost(long NCost)
    {
        dirty(_NCost, NCost);
        _NCost = NCost;
    }

    /**
     * @return Тип документа.
     */
    public UniscPaymentDocumentType getType()
    {
        return _type;
    }

    /**
     * @param type Тип документа.
     */
    public void setType(UniscPaymentDocumentType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Номер документа.
     */
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер документа.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Плательщик.
     */
    @Length(max=255)
    public String getPayer()
    {
        return _payer;
    }

    /**
     * @param payer Плательщик.
     */
    public void setPayer(String payer)
    {
        dirty(_payer, payer);
        _payer = payer;
    }

    /**
     * @return Комментарий.
     */
    @Length(max=255)
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UniscEduAgreementPayFactRowGen)
        {
            setAgreement(((UniscEduAgreementPayFactRow)another).getAgreement());
            setUuid(((UniscEduAgreementPayFactRow)another).getUuid());
            setDate(((UniscEduAgreementPayFactRow)another).getDate());
            setFinesPayment(((UniscEduAgreementPayFactRow)another).isFinesPayment());
            setJCost(((UniscEduAgreementPayFactRow)another).getJCost());
            setNCost(((UniscEduAgreementPayFactRow)another).getNCost());
            setType(((UniscEduAgreementPayFactRow)another).getType());
            setNumber(((UniscEduAgreementPayFactRow)another).getNumber());
            setPayer(((UniscEduAgreementPayFactRow)another).getPayer());
            setComment(((UniscEduAgreementPayFactRow)another).getComment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniscEduAgreementPayFactRowGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniscEduAgreementPayFactRow.class;
        }

        public T newInstance()
        {
            return (T) new UniscEduAgreementPayFactRow();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "agreement":
                    return obj.getAgreement();
                case "uuid":
                    return obj.getUuid();
                case "date":
                    return obj.getDate();
                case "finesPayment":
                    return obj.isFinesPayment();
                case "JCost":
                    return obj.getJCost();
                case "NCost":
                    return obj.getNCost();
                case "type":
                    return obj.getType();
                case "number":
                    return obj.getNumber();
                case "payer":
                    return obj.getPayer();
                case "comment":
                    return obj.getComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "agreement":
                    obj.setAgreement((UniscEduMainAgreement) value);
                    return;
                case "uuid":
                    obj.setUuid((String) value);
                    return;
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "finesPayment":
                    obj.setFinesPayment((Boolean) value);
                    return;
                case "JCost":
                    obj.setJCost((Long) value);
                    return;
                case "NCost":
                    obj.setNCost((Long) value);
                    return;
                case "type":
                    obj.setType((UniscPaymentDocumentType) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "payer":
                    obj.setPayer((String) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "agreement":
                        return true;
                case "uuid":
                        return true;
                case "date":
                        return true;
                case "finesPayment":
                        return true;
                case "JCost":
                        return true;
                case "NCost":
                        return true;
                case "type":
                        return true;
                case "number":
                        return true;
                case "payer":
                        return true;
                case "comment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "agreement":
                    return true;
                case "uuid":
                    return true;
                case "date":
                    return true;
                case "finesPayment":
                    return true;
                case "JCost":
                    return true;
                case "NCost":
                    return true;
                case "type":
                    return true;
                case "number":
                    return true;
                case "payer":
                    return true;
                case "comment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "agreement":
                    return UniscEduMainAgreement.class;
                case "uuid":
                    return String.class;
                case "date":
                    return Date.class;
                case "finesPayment":
                    return Boolean.class;
                case "JCost":
                    return Long.class;
                case "NCost":
                    return Long.class;
                case "type":
                    return UniscPaymentDocumentType.class;
                case "number":
                    return String.class;
                case "payer":
                    return String.class;
                case "comment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniscEduAgreementPayFactRow> _dslPath = new Path<UniscEduAgreementPayFactRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniscEduAgreementPayFactRow");
    }
            

    /**
     * @return Договор. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayFactRow#getAgreement()
     */
    public static UniscEduMainAgreement.Path<UniscEduMainAgreement> agreement()
    {
        return _dslPath.agreement();
    }

    /**
     * @return Идентификатор (для связи с внешней системой).
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayFactRow#getUuid()
     */
    public static PropertyPath<String> uuid()
    {
        return _dslPath.uuid();
    }

    /**
     * @return Дата. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayFactRow#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return Платеж в счет погашения пеней. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayFactRow#isFinesPayment()
     */
    public static PropertyPath<Boolean> finesPayment()
    {
        return _dslPath.finesPayment();
    }

    /**
     * @return Стоимость (юр. лицо). Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayFactRow#getJCost()
     */
    public static PropertyPath<Long> JCost()
    {
        return _dslPath.JCost();
    }

    /**
     * @return Стоимость (физ. лицо). Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayFactRow#getNCost()
     */
    public static PropertyPath<Long> NCost()
    {
        return _dslPath.NCost();
    }

    /**
     * @return Тип документа.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayFactRow#getType()
     */
    public static UniscPaymentDocumentType.Path<UniscPaymentDocumentType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Номер документа.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayFactRow#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Плательщик.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayFactRow#getPayer()
     */
    public static PropertyPath<String> payer()
    {
        return _dslPath.payer();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayFactRow#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    public static class Path<E extends UniscEduAgreementPayFactRow> extends EntityPath<E>
    {
        private UniscEduMainAgreement.Path<UniscEduMainAgreement> _agreement;
        private PropertyPath<String> _uuid;
        private PropertyPath<Date> _date;
        private PropertyPath<Boolean> _finesPayment;
        private PropertyPath<Long> _JCost;
        private PropertyPath<Long> _NCost;
        private UniscPaymentDocumentType.Path<UniscPaymentDocumentType> _type;
        private PropertyPath<String> _number;
        private PropertyPath<String> _payer;
        private PropertyPath<String> _comment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Договор. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayFactRow#getAgreement()
     */
        public UniscEduMainAgreement.Path<UniscEduMainAgreement> agreement()
        {
            if(_agreement == null )
                _agreement = new UniscEduMainAgreement.Path<UniscEduMainAgreement>(L_AGREEMENT, this);
            return _agreement;
        }

    /**
     * @return Идентификатор (для связи с внешней системой).
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayFactRow#getUuid()
     */
        public PropertyPath<String> uuid()
        {
            if(_uuid == null )
                _uuid = new PropertyPath<String>(UniscEduAgreementPayFactRowGen.P_UUID, this);
            return _uuid;
        }

    /**
     * @return Дата. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayFactRow#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(UniscEduAgreementPayFactRowGen.P_DATE, this);
            return _date;
        }

    /**
     * @return Платеж в счет погашения пеней. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayFactRow#isFinesPayment()
     */
        public PropertyPath<Boolean> finesPayment()
        {
            if(_finesPayment == null )
                _finesPayment = new PropertyPath<Boolean>(UniscEduAgreementPayFactRowGen.P_FINES_PAYMENT, this);
            return _finesPayment;
        }

    /**
     * @return Стоимость (юр. лицо). Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayFactRow#getJCost()
     */
        public PropertyPath<Long> JCost()
        {
            if(_JCost == null )
                _JCost = new PropertyPath<Long>(UniscEduAgreementPayFactRowGen.P_J_COST, this);
            return _JCost;
        }

    /**
     * @return Стоимость (физ. лицо). Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayFactRow#getNCost()
     */
        public PropertyPath<Long> NCost()
        {
            if(_NCost == null )
                _NCost = new PropertyPath<Long>(UniscEduAgreementPayFactRowGen.P_N_COST, this);
            return _NCost;
        }

    /**
     * @return Тип документа.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayFactRow#getType()
     */
        public UniscPaymentDocumentType.Path<UniscPaymentDocumentType> type()
        {
            if(_type == null )
                _type = new UniscPaymentDocumentType.Path<UniscPaymentDocumentType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Номер документа.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayFactRow#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(UniscEduAgreementPayFactRowGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Плательщик.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayFactRow#getPayer()
     */
        public PropertyPath<String> payer()
        {
            if(_payer == null )
                _payer = new PropertyPath<String>(UniscEduAgreementPayFactRowGen.P_PAYER, this);
            return _payer;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayFactRow#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(UniscEduAgreementPayFactRowGen.P_COMMENT, this);
            return _comment;
        }

        public Class getEntityClass()
        {
            return UniscEduAgreementPayFactRow.class;
        }

        public String getEntityName()
        {
            return "uniscEduAgreementPayFactRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
