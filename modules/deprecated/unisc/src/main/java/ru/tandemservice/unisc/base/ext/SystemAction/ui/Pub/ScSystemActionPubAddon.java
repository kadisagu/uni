/**
 *$Id$
 */
package ru.tandemservice.unisc.base.ext.SystemAction.ui.Pub;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.impl.ConversationFacade;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.DaemonThreadFactory;
import org.tandemframework.shared.commonbase.base.util.ValueHolder;

import ru.tandemservice.unisc.base.bo.ScSystemAction.ScSystemActionManager;
import ru.tandemservice.unisc.base.bo.ScSystemAction.logic.IScSystemActionDao;
import ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student;

/**
 * @author Alexander Shaburov
 * @since 13.02.13
 */
public class ScSystemActionPubAddon extends UIAddon
{
    public ScSystemActionPubAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public void onClickDeleteContractWithoutLinks()
    {
        ScSystemActionManager.instance().dao().deleteContractWithoutLinks();
    }

    public void onClickGetEducationOrgUnitCostEnvironmentXml()
    {
        getActivationBuilder().asDesktopRoot("ru.tandemservice.unisc.component.settings.EduOrgUnitCostEnvAdd")
        .activate();
    }


    private static final ValueHolder<ScheduledExecutorService> printServiceHolder = new ValueHolder<>();

    protected final Logger logger = Logger.getLogger(this.getClass());
    private FileAppender initAppender(final Logger logger, final String name) {
        try {
            final FileAppender appender = new FileAppender(new PatternLayout("%d{yyyyMMdd HH:mm:ss,SSS} [%t] %p - %m%n"), Paths.get(System.getProperty("catalina.base"), "logs", "sc-"+name+".log").toString());
            appender.setName("sc-logger-"+name+"-appender");
            appender.setThreshold(Level.INFO);
            logger.addAppender(appender);
            return appender;
        } catch (final Throwable t) {
            logger.error(t.getMessage(), t);
            return null;
        }
    }

    public void onClickPrintAll() {
        synchronized (printServiceHolder) {
            if (null != printServiceHolder.getValue()) { throw new ApplicationException("Процесс печати уже запущен.");  }
            printServiceHolder.setValue(Executors.newScheduledThreadPool(8, new DaemonThreadFactory("sched-pool-sc-print")));
        }

        final IScSystemActionDao dao = ScSystemActionManager.instance().dao();
        final ScheduledExecutorService service = printServiceHolder.getValue();
        final FileAppender appender = this.initAppender(this.logger, "print-all");
        try {

            final List<Long> relationIds = DataAccessServices.dao().getList(
                new DQLSelectBuilder()
                .fromEntity(UniscEduAgreement2Student.class, "x")
                .column(property("x.id"))
            );

            for (final Long relationId: relationIds) {
                service.submit(() -> {
                    ContextLocal.begin(null);
                    ConversationFacade.begin();
                    Debug.begin("id="+relationId);
                    try {
                        final IEventServiceLock eventLock = CoreServices.eventService().lock();
                        try { dao.doPrintRelation(relationId); }
                        catch (Throwable t) { logger.error(t.getMessage(), t); }
                        finally { eventLock.release(); }
                    } finally {
                        Debug.end();
                        ConversationFacade.end();
                        ContextLocal.end();
                    }
                });
            }


        } finally {
            try {
                service.shutdown();
                while (!service.awaitTermination(1, TimeUnit.MINUTES)) { }
            } catch (Throwable t) {
                throw CoreExceptionUtils.getRuntimeException(t);
            } finally {
                synchronized (printServiceHolder) { printServiceHolder.setValue(null); }
            }

            logger.removeAppender(appender);
            appender.close();
        }



    }

}
