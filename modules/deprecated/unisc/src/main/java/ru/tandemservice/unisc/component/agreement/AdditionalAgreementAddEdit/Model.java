package ru.tandemservice.unisc.component.agreement.AdditionalAgreementAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;

import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.unisc.entity.agreements.UniscEduAdditAgreement;

/**
 * @author vdanilov
 */
@State({
    @Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="id"),
    @Bind(key="agreementNumber", binding="agreement.number")
})
@Output({
    @Bind(key="defaultEducationOrgUnitId", binding="personRole.educationOrgUnit.id")
})
public class Model extends ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Model {

    private Long id;
    public Long getId() { return this.id; }
    public void setId(final Long id) { this.id = id; }

    private UniscEduAdditAgreement agreement;
    @Override public UniscEduAdditAgreement getAgreement() { return this.agreement; }
    protected void setAgreement(final UniscEduAdditAgreement agreement) { this.agreement = agreement; }

    private PersonRole role;
    @Override public PersonRole getPersonRole() { return this.role; }
    protected void setPersonRole(final PersonRole role) { this.role = role; }
}
