package ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage1;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.tapsupport.component.selection.StaticSelectModel;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.education.gen.EducationOrgUnitGen;
import ru.tandemservice.uni.ui.SelectModel;
import ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage.AbstractStageDAO;
import ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage.IStageResult;
import ru.tandemservice.unisc.dao.agreement.IUniscEduAgreementDAO;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayPlanRow;
import ru.tandemservice.unisc.entity.agreements.gen.UniscEduAdditAgreementGen;
import ru.tandemservice.unisc.entity.agreements.gen.UniscEduAgreementPayPlanRowGen;
import ru.tandemservice.unisc.entity.agreements.student.gen.UniscEduAgreement2StudentGen;
import ru.tandemservice.unisc.entity.catalog.UniscCustomerType;
import ru.tandemservice.unisc.entity.catalog.UniscPayPeriod;
import ru.tandemservice.unisc.entity.catalog.gen.UniscCustomerTypeGen;
import ru.tandemservice.unisc.entity.catalog.gen.UniscPayPeriodGen;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;
import ru.tandemservice.unisc.entity.config.gen.UniscEduOrgUnitGen;
import ru.tandemservice.unisc.entity.config.gen.UniscFreq2DevFormGen;
import ru.tandemservice.unisc.utils.UniscPropertyUtils;

import java.util.*;

/**
 * @author vdanilov
 */
@SuppressWarnings("deprecation")
public class DAO extends AbstractStageDAO<Model> implements IDAO
{

    private void setupFirstStage(final Model model)
    {
        final IdentifiableWrapper<IEntity> firstStage = model.getFirstStageModel().getValue();
        model.getAgreement().setFirstStage(null == firstStage ? 0 : firstStage.getId().intValue());
    }

    @Override
    protected IStageResult getResult(final Model model)
    {
        return m -> m.update(model.getAgreement(), true);
    }

    @Override
    public IStageResult save(final Model model)
    {
        this.setupFirstStage(model);
        return super.save(model);
    }

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);
        final boolean isReadOnly = (null != model.getAgreement().getId());


        /* develop list independent model */
        {
            model.setDevelopPeriods(EducationCatalogsManager.getDevelopPeriodSelectModel());
            model.setCustomerTypes(getList(UniscCustomerType.class, UniscCustomerTypeGen.P_HIDDEN, Boolean.FALSE, ICatalogItem.CATALOG_ITEM_CODE));
            if ((null == model.getAgreement().getCustomerType()) && (model.getCustomerTypes().size() > 0))
            {
                model.getAgreement().setCustomerType(model.getCustomerTypes().get(0));
            }
        }

        this.prepareAgreementBaseData(model, isReadOnly);
        this.preparePayPeriod(model, isReadOnly);
        this.setupFirstStage(model);
    }

    @SuppressWarnings("unchecked")
    protected void preparePayPeriod(final Model model, final boolean isReadOnly)
    {
        /* pay freq & duration */
        {
            if (isReadOnly)
            {
                model.setPayPlanFreqList(Collections.singletonList(model.getAgreement().getPayPlanFreq()));
                model.setPayPlanDurationList(Collections.singletonList(model.getAgreement().getPayPlanDuration()));
            }
            else
            {
                final List<UniscPayPeriod> payPlanPeriodList = this.getList(UniscPayPeriod.class, UniscPayPeriodGen.P_HIDDEN, Boolean.FALSE, ICatalogItem.CATALOG_ITEM_CODE);

                /* filter (by settings) */
                {
                    final MQBuilder freqBuilder = new MQBuilder(UniscFreq2DevFormGen.ENTITY_CLASS, "e", new String[]{ UniscFreq2DevFormGen.L_FREQ + ".id" });
                    freqBuilder.addDomain("c", UniscEduOrgUnitGen.ENTITY_CLASS);
                    freqBuilder.add(MQExpression.eqProperty("e", UniscFreq2DevFormGen.L_DEVELOP_FORM, "c", UniscEduOrgUnitGen.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnitGen.L_DEVELOP_FORM));
                    freqBuilder.add(MQExpression.eq("c", "id", model.getAgreement().getConfig().getId()));
                    freqBuilder.setNeedDistinct(true);
                    final Set<Long> freqIds = new HashSet<>(freqBuilder.<Long>getResultList(getSession()));

                    CollectionUtils.filter(payPlanPeriodList, object -> freqIds.contains(((UniscPayPeriod) object).getId()));
                }

                /* freq */
                {
                    model.setPayPlanFreqList(payPlanPeriodList);
                    if (null == model.getAgreement().getPayPlanFreq())
                    {
                        prepareDefaultPayFreq(model, payPlanPeriodList);
                    }
                }

                /* duration */
                {
                    final String code = (null == model.getAgreement().getPayPlanFreq() ? null : model.getAgreement().getPayPlanFreq().getCode());
                    final ArrayList<UniscPayPeriod> payPlanDurationList = new ArrayList<>(CollectionUtils.select(
                            payPlanPeriodList, object -> ((null == code) || (((UniscPayPeriod) object).getCode().compareTo(code) <= 0)))
                    );
                    model.setPayPlanDurationList(payPlanDurationList);
                    if (null == model.getAgreement().getPayPlanDuration())
                    {
                        prepareDefaultPayDuration(model, payPlanDurationList);
                    }
                }

            }


        }
        /* first stage selection model */
        {
            final SelectModel<IdentifiableWrapper<IEntity>> firstStageModel = model.getFirstStageModel();
            final UniscPayPeriod payPlanFreq = model.getAgreement().getPayPlanFreq();
            final UniscEduOrgUnit config = model.getAgreement().getConfig();
            if (isReadOnly)
            {
                if (null != payPlanFreq)
                {
                    final int c = model.getAgreement().getFirstStage();
                    firstStageModel.setValue(new IdentifiableWrapper<>((long) c, payPlanFreq.toString(config, c)));
                    firstStageModel.setSource(new StaticSelectModel("id", "title", Collections.singletonList(firstStageModel.getValue())));
                } else
                {
                    throw new IllegalStateException("wtf");
                }
            }
            else
            {
                // TODO: этот большой кусок кода нужно переписать

                final List<IdentifiableWrapper<IEntity>> source = new ArrayList<>();
                if (null != payPlanFreq)
                {

                    final Long personRoleId = model.getPersonRole().getId();
                    final Long agreementId = model.getAgreement().getId();
                    final List<UniscEduAgreementPayPlanRow> rows = getExternalAgreementPayPlanRows(personRoleId, agreementId);

                    int max_nc = -1;
                    int max_nt = -1;
                    int max_np = -1;

                    /* нужно брать только те периоды, которые еще не перекрыты договорами и доп соглашениями */
                    {
                        for (final UniscEduAgreementPayPlanRow row : rows)
                        {
                            final UniscPayPeriod freq = row.getOwner().getPayPlanFreq();

                            final int nc = freq.getCourseNumber(row.getOwner().getConfig(), row.getStage());
                            if (nc > max_nc)
                            {
                                max_nc = nc;
                                max_nt = -1;
                                max_np = -1;
                            }
                            if (nc == max_nc)
                            {

                                final int nt = freq.getTermNumber(row.getOwner().getConfig(), row.getStage());
                                if (nt > max_nt)
                                {
                                    max_nt = nt;
                                    max_np = -1;
                                }
                                if (nt == max_nt)
                                {

                                    final int np = freq.getPartNumber(row.getOwner().getConfig(), row.getStage());
                                    if (np > max_np)
                                    {
                                        max_np = np;
                                    }
                                }
                            }
                        }
                    }

                    final int count = payPlanFreq.getStagesCount(config);
                    for (int c = 0; c < count; c++)
                    {
                        final int nc = payPlanFreq.getCourseNumber(config, c);
                        final int nt = payPlanFreq.getTermNumber(config, c);
                        final int np = payPlanFreq.getPartNumber(config, c);

                        boolean ok = false;
                        ok |= (nc >= max_nc);
                        ok |= (nc == max_nc) && (nt >= max_nt);
                        ok |= (nc == max_nc) && (nt == max_nt) && (np >= max_np);

                        if (ok)
                        {
                            source.add(new IdentifiableWrapper<>((long) c, payPlanFreq.toString(config, c)));
                        }
                    }

                    if ((null == firstStageModel.getValue()) && !source.isEmpty())
                    {
                        // если не заполненно и есть откуда заполнять

                        if ((max_nc > 0) && (max_nt > 0) && (max_np > 0))
                        {
                            // если этап не первый
                            max_nc = -1;
                            max_nt = -1;
                            max_np = -1;

                            /* нужно брать только те периоды, которые еще не перекрыты договорами и доп соглашениями */
                            {
                                for (final UniscEduAgreementPayPlanRow row : rows)
                                {
                                    final UniscPayPeriod freq = row.getOwner().getPayPlanFreq();

                                    final int nc = freq.getCourseNumber(row.getOwner().getConfig(), 1 + row.getStage());
                                    if (nc > max_nc)
                                    {
                                        max_nc = nc;
                                        max_nt = -1;
                                        max_np = -1;
                                    }
                                    if (nc == max_nc)
                                    {

                                        final int nt = freq.getTermNumber(row.getOwner().getConfig(), 1 + row.getStage());
                                        if (nt > max_nt)
                                        {
                                            max_nt = nt;
                                            max_np = -1;
                                        }
                                        if (nt == max_nt)
                                        {

                                            final int np = freq.getPartNumber(row.getOwner().getConfig(), 1 + row.getStage());
                                            if (np > max_np)
                                            {
                                                max_np = np;
                                            }
                                        }
                                    }
                                }
                            }

                            for (final IdentifiableWrapper<IEntity> e : source)
                            {
                                final int c = e.getId().intValue();
                                final int nc = payPlanFreq.getCourseNumber(config, c);
                                final int nt = payPlanFreq.getTermNumber(config, c);
                                final int np = payPlanFreq.getPartNumber(config, c);

                                boolean ok = false;
                                ok |= (nc >= max_nc);
                                ok |= (nc == max_nc) && (nt >= max_nt);
                                ok |= (nc == max_nc) && (nt == max_nt) && (np >= max_np);

                                if (ok)
                                {
                                    firstStageModel.setValue(e);
                                    break;
                                }
                            }


                        }
                    }
                }

                firstStageModel.setSource(new StaticSelectModel("id", "title", source));
                if ((null == firstStageModel.getValue()) && !source.isEmpty())
                {
                    firstStageModel.setValue(source.get(0));
                }
            }
        }
    }

    // возвращает набор сторк оплаты уже сознанных договоров и доп соглашений
    protected List<UniscEduAgreementPayPlanRow> getExternalAgreementPayPlanRows(final Long personRoleId, final Long agreementId)
    {
        final MQBuilder b_a2s = new MQBuilder(UniscEduAgreement2StudentGen.ENTITY_CLASS, "a2s", new String[]{UniscEduAgreement2StudentGen.L_AGREEMENT + ".id"});
        b_a2s.add(MQExpression.eq("a2s", UniscEduAgreement2StudentGen.L_STUDENT + ".id", personRoleId));

        final MQBuilder b_aa = new MQBuilder(UniscEduAdditAgreementGen.ENTITY_CLASS, "aa", new String[]{"id"});
        b_aa.add(MQExpression.in("aa", UniscEduAdditAgreementGen.L_AGREEMENT + ".id", b_a2s));

        final MQBuilder b = new MQBuilder(UniscEduAgreementPayPlanRowGen.ENTITY_CLASS, "ppr");
        b.add(MQExpression.isNull("ppr", UniscEduAgreementPayPlanRowGen.L_REPLACED_BY));
        b.addJoinFetch("ppr", UniscEduAgreementPayPlanRowGen.L_OWNER, "o");
        b.add(MQExpression.or(
                MQExpression.in("o", "id", b_a2s),
                MQExpression.in("o", "id", b_aa)
        ));

        if (null != agreementId)
        {
            // свои периоды не учитываемs
            b.add(MQExpression.notEq("o", "id", agreementId));
        }

        return b.getResultList(this.getSession());
    }


    protected void prepareDefaultPayFreq(final Model model, final Collection<UniscPayPeriod> list)
    {
        for (final UniscPayPeriod p : list)
        {
            if (UniscPropertyUtils.DEFAULT_FREQ.equalsIgnoreCase(p.freq().toString()))
            {
                model.getAgreement().setPayPlanFreq(p);
                break;
            }
        }
    }

    protected void prepareDefaultPayDuration(final Model model, final Collection<UniscPayPeriod> list)
    {
        for (final UniscPayPeriod p : list)
        {
            if (UniscPropertyUtils.DEFAULT_DURATION.equalsIgnoreCase(p.freq().toString()))
            {
                model.getAgreement().setPayPlanDuration(p);
                break;
            }
        }
    }

    protected void prepareAgreementBaseData(final Model model, final boolean isReadOnly)
    {
        if (!isReadOnly)
        {
            if (null == model.getAgreement().getFormingDate())
            {
                model.getAgreement().setFormingDate(new Date());
            }
            if (null == model.getAgreement().getNumber())
            {
                IUniscEduAgreementDAO.INSTANCE.get().assignNewAgreementNumber(model.getAgreement(), false);
            }
        }
    }

}
