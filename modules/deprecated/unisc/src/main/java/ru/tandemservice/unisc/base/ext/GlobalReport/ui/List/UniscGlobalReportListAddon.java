/* $Id$ */
package ru.tandemservice.unisc.base.ext.GlobalReport.ui.List;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.util.ParametersMap;

/**
 * @author azhebko
 * @since 31.03.2014
 */
public class UniscGlobalReportListAddon extends UIAddon
{
    public static final String NAME = "uniscGlobalReportListAddon";

    public UniscGlobalReportListAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }
}