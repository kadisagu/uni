/**
 *$Id$
 */
package ru.tandemservice.unisc.base.bo.ScSystemAction.logic;

import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfWriter;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unisc.dao.print.IUniscPrintDAO;
import ru.tandemservice.unisc.entity.agreements.*;
import ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 13.02.13
 */
public class ScSystemActionDao extends UniBaseDao implements IScSystemActionDao
{
    @Override
    public void deleteContractWithoutLinks()
    {
        final Collection<IEntityMeta> entityList = EntityRuntime.getInstance().getEntities();

        final MQBuilder builder = new MQBuilder(UniscEduMainAgreement.ENTITY_CLASS, "magr", new String[] { "id" });
        for (final IEntityMeta meta : entityList)
        {
            if (IUniscEduAgreement2PersonRole.class.isAssignableFrom(meta.getEntityClass()))
            {
                final MQBuilder subBuilder = new MQBuilder(meta.getEntityClass().getName(), "agr", new String[] { "agreement.id" });
                builder.add(MQExpression.notIn("magr", UniscEduMainAgreement.id().s(), subBuilder));
            }
        }

        final List<Long> withoutLinks = builder.getResultList(this.getSession());
        for (final Long id : withoutLinks)
        {
            this.delete(id);
        }
    }


    @Override
    public void doPrintRelation(final Long agreementId)
    {
        final IUniscPrintDAO printDao = IUniscPrintDAO.INSTANCE.get();
        final UniscEduAgreement2Student a2s = this.get(agreementId);
        final UniscEduMainAgreement mainAgreement = a2s.getAgreement();
        final Person person = a2s.getTarget().getPerson();

        final List<UniscEduAgreementBase> agreementList = new ArrayList<>();
        agreementList.add(mainAgreement);
        agreementList.addAll(this.getList(UniscEduAdditAgreement.class, UniscEduAdditAgreement.agreement(), mainAgreement, UniscEduAdditAgreement.P_FORMING_DATE));

        for (final UniscEduAgreementBase agreementBase: agreementList)
        {
            final byte[] bytes;
            try {
                final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                final RtfDocument document = printDao.getDocumentTemplate(agreementBase);
                printDao.getRtfInjectorModifier(agreementBase, person).modify(document);
                RtfWriter.write(document, outputStream);
                outputStream.close();
                bytes = outputStream.toByteArray();
            } catch (final Throwable t) {
                throw CoreExceptionUtils.getRuntimeException(t);
            }

            final DatabaseFile dbFile = new DatabaseFile();
            dbFile.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
            dbFile.setFilename("contract-"+agreementBase.getNumber()+"-"+ DateFormatter.DEFAULT_DATE_FORMATTER.format(agreementBase.getFormingDate())+".rtf");
            dbFile.setContent(bytes);
            this.save(dbFile);

            final UniscEduAgreementPrintVersion v = new UniscEduAgreementPrintVersion();
            v.setFile(dbFile);
            v.setOwner(agreementBase);
            v.setTimestamp(agreementBase.getFormingDate());
            v.setTitle("Версия от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(agreementBase.getFormingDate()));
            v.setFileName(dbFile.getFilename());
            v.setContentType(dbFile.getContentType());
            v.setUploaded(false);
            this.save(v);
        }


    }

}
