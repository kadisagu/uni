package ru.tandemservice.unisc.component.report;

import ru.tandemservice.unisc.component.report.IUniscReportDefinition;

/**
 * @author oleyba
 * @since 14.08.2009
 */
public class UniscReportDefinition implements IUniscReportDefinition
{
    private String _id;
    private String _title;
    private String _permissionKey;
    private String _componentName;

    @Override
    public String getId()
    {
        return _id;
    }

    public void setId(String id)
    {
        _id = id;
    }

    @Override
    public String getTitle()
    {
        return _title;
    }

    public void setTitle(String title)
    {
        _title = title;
    }

    @Override
    public String getPermissionKey()
    {
        return _permissionKey;
    }

    public void setPermissionKey(String permissionKey)
    {
        _permissionKey = permissionKey;
    }

    @Override
    public String getComponentName()
    {
        return _componentName;
    }

    public void setComponentName(String componentName)
    {
        _componentName = componentName;
    }
}
