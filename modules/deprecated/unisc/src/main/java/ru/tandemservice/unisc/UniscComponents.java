/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisc;

/**
 * @author vdanilov
 */
public interface UniscComponents
{
	String AGREEMENT_PUB = ru.tandemservice.unisc.component.agreement.AgreementPub.Model.class.getPackage().getName();

	String STUDENT_CONTRACT_ADD_EDIT = ru.tandemservice.unisc.component.student.StudentAgreementAddEdit.Model.class.getPackage().getName();
	String STUDENT_CONTRACT_PRINT = ru.tandemservice.unisc.component.agreement.AgreementRelationPrint.Model.class.getPackage().getName();

	String STUDENT_ADDITIONAL_AGREMENT_ADD_EDIT = ru.tandemservice.unisc.component.agreement.AdditionalAgreementAddEdit.Model.class.getPackage().getName();
	String STUDENT_ADDITIONAL_AGREMENT_PRINT = ru.tandemservice.unisc.component.agreement.AdditionalAgreementPrint.Model.class.getPackage().getName();
}
