package ru.tandemservice.unisc.utils;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.tapestry.form.validator.Required;
import org.apache.tapestry.form.validator.Validator;
import org.springframework.core.io.Resource;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.spring.CoreSpringUtils;
import ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage.AbstractStageModel;
import ru.tandemservice.unisc.entity.catalog.UniscPayPeriod;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

/**
 * @author vdanilov
 */
@SuppressWarnings("deprecation")
public class UniscPropertyUtils {

    public static interface ValidatorFactory<Model> {
        Validator buildValidator(Model model);
    }


    private static final Logger log = Logger.getLogger(UniscPropertyUtils.class);
    private static final Properties properties = new Properties();

    static {
        try {
            final Resource[] propertyResources = ApplicationRuntime.getResources("*.unisc.properties");
            UniscPropertyUtils.log.info(Arrays.asList(propertyResources).toString());

            final List<Resource> propertyResourceList = Arrays.asList(propertyResources);
            Collections.sort(propertyResourceList, CoreSpringUtils.MODULE_RESOURCE_COMPARATOR_ASC);
            for (final Resource resource : propertyResourceList) {
                UniscPropertyUtils.properties.load(resource.getInputStream());
            }
        } catch (final Throwable t) {
            UniscPropertyUtils.log.error(t.getMessage(), t);
        }
    }

    public static String get(final String key, final String def) {
        String value;

        value = StringUtils.trimToNull(ApplicationRuntime.getProperty(key));
        if (null != value) { return value; }

        value = StringUtils.trimToNull(UniscPropertyUtils.properties.getProperty(key));
        if (null != value) { return value; }

        return def;
    }


    /*
     * небольшая предыстория появления этого аттрибута
     * у одного нашего клиента появлялось сообщение об ошибках "номер занят", при этом номер указывался
     * они попросили выводить кем же именно номер занят (и готовы были это оплатить), а ситуация была редкая
     * вместо того, чтобы пойти посмотреть в списке договоров по номеру (там есть прямо такой фильтр), кем же он занят то
     * 
     * пришлось делать такой режим, чтобы нормальные клиенты не думали, что система их считает дураками
     */
    /** тупой клиент - везде выводим максимум подсказочной информации */
    public static final boolean STUPID_MODE = Boolean.valueOf(UniscPropertyUtils.get("unisc.stupid-mode", Boolean.FALSE.toString()).toLowerCase()).booleanValue();

    /** способ оплаты по умолчанию */
    public static final String DEFAULT_FREQ = UniscPropertyUtils.get("unisc.default.freq", UniscPayPeriod.Freq.YEAR.toString()).toLowerCase();

    /** продолжительность договора по умолчанию */
    public static final String DEFAULT_DURATION = UniscPropertyUtils.get("unisc.default.duration", UniscPayPeriod.Freq.YEAR.toString()).toLowerCase();

    /** обязательность и неизменность номера */
    public static final boolean STRICT_NUMBER = Boolean.valueOf(UniscPropertyUtils.get("unisc.constraint.strict-number", Boolean.TRUE.toString()).toLowerCase()).booleanValue();

    /** генерация номеров */
    public static final boolean GENERATE_NUMBER = UniscPropertyUtils.STRICT_NUMBER || Boolean.valueOf(UniscPropertyUtils.get("unisc.constraint.generate-number", Boolean.TRUE.toString()).toLowerCase()).booleanValue();

    /** обязательность шифра (true = нельзя автоматически создавать направление при создании соответствующего договора) */
    public static final boolean STRICT_CIPHER = Boolean.valueOf(UniscPropertyUtils.get("unisc.constraint.strict-cipher", Boolean.FALSE.toString()).toLowerCase()).booleanValue();

    /** резервирующий символ */
    public static final String RESERVED_POSTFIX = StringUtils.strip(UniscPropertyUtils.get("unisc.reserver", "$").toLowerCase(), "\"");

    /** валидация даты "от" */
    public static final ValidatorFactory<AbstractStageModel> FORMING_DATE_VALIDATOR_FACTORY = UniscPropertyUtils.validator("unisc.validator.forming-date", new ValidatorFactory<AbstractStageModel>() {
        @Override public Validator buildValidator(final AbstractStageModel model) { return new Required(); }
    });

    /** валидация даты "до" */
    public static final ValidatorFactory<AbstractStageModel> DEADLINE_DATE_VALIDATOR_FACTORY = UniscPropertyUtils.validator("unisc.validator.deadline-date", new ValidatorFactory<AbstractStageModel>() {
        @Override public Validator buildValidator(final AbstractStageModel model) { return null; }
    });


    @SuppressWarnings("unchecked")
    private static <M> ValidatorFactory<M> validator(final String key, final ValidatorFactory<M> def) {
        final String name = StringUtils.trimToNull(UniscPropertyUtils.get(key, ""));
        if (null != name) {
            try { return (ValidatorFactory<M>) Class.forName(name).newInstance(); }
            catch(final Throwable t) { UniscPropertyUtils.log.error(t.getMessage(), t); }
        }
        return def;
    }


}
