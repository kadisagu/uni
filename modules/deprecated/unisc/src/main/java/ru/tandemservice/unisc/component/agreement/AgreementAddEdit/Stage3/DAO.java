package ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage3;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.fias.base.entity.Address;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonNextOfKin;
import org.tandemframework.shared.person.base.entity.gen.PersonNextOfKinGen;
import org.tandemframework.shared.person.catalog.entity.RelationDegree;
import org.tandemframework.shared.person.catalog.entity.codes.RelationDegreeCodes;
import org.tandemframework.tapsupport.component.selection.BaseMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;
import ru.tandemservice.unictr.entity.contractor.ContactPerson;
import ru.tandemservice.unictr.entity.contractor.Contractor;
import ru.tandemservice.unictr.entity.contractor.gen.ContactPersonGen;
import ru.tandemservice.unictr.entity.contractor.gen.ContractorGen;
import ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage.AbstractStageDAO;
import ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage.IStageResult;
import ru.tandemservice.unisc.dao.agreement.IUniscEduAgreementDAO;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson;
import ru.tandemservice.unisc.entity.agreements.gen.UniscEduAgreementNaturalPersonGen;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author vdanilov
 */
@SuppressWarnings({"deprecation", "unchecked"})
public class DAO extends AbstractStageDAO<Model> implements IDAO {

    @Override
    protected IStageResult getResult(final Model model) {
        final List<UniscEduAgreementNaturalPerson> nextOfKinList = model.getNextOfKinList();
        IUniscEduAgreementDAO.INSTANCE.get().checkNaturalPersons(model.getAgreement(), nextOfKinList);

        return m -> {
            final UniscEduAgreementBase agreement = m.update(model.getAgreement(), true);
            IUniscEduAgreementDAO.INSTANCE.get().doSyncNaturalPersons(agreement, nextOfKinList);
        };
    }

    @Override
    public void prepare(final Model model) {
        super.prepare(model);

        /* ДО ТОГО КАК ОБНОВИЛИ */ {
            model.setContractor(null == model.getContactPerson() ? null : model.getContactPerson().getContractor());
        }

        if (null != model.getCreatedItemId()) {
            final IEntity entity = get(model.getCreatedItemId());
            processCreatedItem(model, entity);
            model.setCreatedItemId(null);
        }

        prepareJuricialPerson(model);
        prepareNaturalPersons(model);
    }

    @Override
    public void prepareNaturalPersonsList(final Model model) {
    }

    protected void prepareNaturalPersons(final Model model) {
        if (null == model.getNextOfKinList()) {
            model.setNextOfKinList(IUniscEduAgreementDAO.INSTANCE.get().getNaturalPersons(model.getAgreement()));
            if (model.getNextOfKinList().isEmpty() && (null != model.getDefaultNextOfKinList()) && !model.getDefaultNextOfKinList().isEmpty()) {
                model.setNextOfKinList(model.getDefaultNextOfKinList());
            }
        }

        // обновляем все, что получили
        if (!model.getNextOfKinList().isEmpty()) {
            final Session session = getSession();
            for (final UniscEduAgreementNaturalPerson person: model.getNextOfKinList()) {
                if (!person.isTransient()) { session.update(person); }
                reassociateEntityProperties(session, person);
            }
        }

        model.setNextOfKinModel(new BaseMultiSelectModel("id", new String[] {
            UniscEduAgreementNaturalPersonGen.L_RELATION_DEGREE+".title",
            UniscEduAgreementNaturalPerson.P_TITLE,
            UniscEduAgreementNaturalPerson.P_PASPORT_TITLE,
            UniscEduAgreementNaturalPersonGen.L_ADDRESS+"."+Address.P_TITLE_WITH_FLAT
        }, null) {
            @Override
            public ListResult findValues(String filter) {
                final List<UniscEduAgreementNaturalPerson> result = new ArrayList<>(model.getNextOfKinList());

                final boolean anotherExists = isAnotherExists(result);
                final Person person = get(Person.class, model.getPersonRole().getPerson().getId());

                if (!anotherExists) {
                    final RelationDegree degree = getCatalogItem(RelationDegree.class, RelationDegreeCodes.ANOTHER);
                    result.add(new UniscEduAgreementNaturalPerson().from(person, degree));
                }

                /* next of kins (by person) */ {
                    filter = StringUtils.trimToNull(filter);
                    final MQBuilder builder = new MQBuilder(PersonNextOfKinGen.ENTITY_CLASS, "k");
                    builder.add(MQExpression.eq("k", PersonNextOfKinGen.L_PERSON+".id", person.getId()));
                    if (null != filter) {
                        filter = filter.toLowerCase();
                        builder.add(MQExpression.or(
                            MQExpression.like("k", PersonNextOfKinGen.L_RELATION_DEGREE+".title", filter),
                            MQExpression.like("k", PersonNextOfKinGen.P_LAST_NAME, filter),
                            MQExpression.like("k", PersonNextOfKinGen.P_FIRST_NAME, filter)
                        ));
                    }
                    builder.addOrder("k", PersonNextOfKinGen.L_RELATION_DEGREE+".code");
                    builder.addOrder("k", PersonNextOfKinGen.P_LAST_NAME);
                    builder.addOrder("k", PersonNextOfKinGen.P_FIRST_NAME);
                    builder.addOrder("k", PersonNextOfKinGen.P_MIDDLE_NAME);

                    builder.add(MQExpression.notIn("k", "id", CollectionUtils.collect(result, input -> (null == input ? Long.valueOf(-1) : ((IEntity)input).getId()))));
                    result.addAll(CollectionUtils.collect(builder.getResultList(getSession()), input -> { return new UniscEduAgreementNaturalPerson().from((PersonNextOfKin)input); }));
                }

                return new ListResult<>(result);
            }

            public Object getValue(final Object primaryKey) {
                final Long id = (Long)primaryKey;
                if (null == id) { return null; }

                for (final UniscEduAgreementNaturalPerson np : model.getNextOfKinList()) {
                    if (null != np) {
                        if (id.equals(np.getId())) { return np; }
                    }
                }

                final IEntity entity = get(id);
                if (entity instanceof PersonNextOfKin) {
                    return new UniscEduAgreementNaturalPerson().from((PersonNextOfKin)entity);
                } else if (entity instanceof Person) {
                    final RelationDegree degree = getCatalogItem(RelationDegree.class, RelationDegreeCodes.ANOTHER);
                    return new UniscEduAgreementNaturalPerson().from((Person)entity, degree);
                }
                return null;
            }
            private boolean isAnotherExists(final List<UniscEduAgreementNaturalPerson> result) {
                for (final UniscEduAgreementNaturalPerson np: result) {
                    if (RelationDegreeCodes.ANOTHER.equals(np.getRelationDegree().getCode())) {
                        return true;
                    }
                }
                return false;
            }
            @Override public List getValues(final Set primaryKeys) {
                final List result = new ArrayList();
                for (final Object key: primaryKeys) {
                    result.add(getValue(key));
                }
                return result;
            }
        });
    }

    protected void prepareJuricialPerson(final Model model) {
        model.setContractorModel(new UniQueryFullCheckSelectModel() {
            @Override protected MQBuilder query(final String domainName, String filter) {
                filter = StringUtils.trimToNull(filter);
                final MQBuilder builder = new MQBuilder(ContractorGen.ENTITY_CLASS, domainName);
                if (null != filter) {
                    filter = filter.toLowerCase();
                    builder.add(MQExpression.or(
                        MQExpression.like(domainName, ContractorGen.P_TITLE, filter),
                        MQExpression.like(domainName, ContractorGen.P_SHORT_TITLE, filter),
                        MQExpression.like(domainName, ContractorGen.P_INN, filter)
                    ));

                }
                builder.addOrder(domainName, ContractorGen.P_TITLE);
                return builder;
            }
        });

        model.setContactPersonModel(new UniQueryFullCheckSelectModel(ContactPerson.P_FULLFIO) {
            @Override protected MQBuilder query(final String domainName, String filter) {
                filter = StringUtils.trimToNull(filter);
                final MQBuilder builder = new MQBuilder(ContactPersonGen.ENTITY_CLASS, domainName);
                builder.add(MQExpression.eq(domainName, ContactPersonGen.L_CONTRACTOR+".id", model.getContractor().getId()));
                if (null != filter) {
                    filter = filter.toLowerCase();
                    builder.add(MQExpression.or(
                        MQExpression.like(domainName, ContactPersonGen.P_LAST_NAME, filter),
                        MQExpression.like(domainName, ContactPersonGen.P_FIRST_NAME, filter),
                        MQExpression.like(domainName, ContactPersonGen.P_MIDDLE_NAME, filter)
                    ));

                }
                builder.addOrder(domainName, ContactPersonGen.P_LAST_NAME);
                builder.addOrder(domainName, ContactPersonGen.P_FIRST_NAME);
                builder.addOrder(domainName, ContactPersonGen.P_MIDDLE_NAME);
                return builder;
            }
        });
    }

    protected void processCreatedItem(final Model model, final IEntity entity) {
        if (entity instanceof Contractor) {
            model.setContactPerson(null);
            model.setContractor((Contractor)entity);

        } else if (entity instanceof ContactPerson) {
            model.setContactPerson((ContactPerson) entity);
            model.setContractor(model.getContactPerson().getContractor());

        } else if (entity instanceof PersonNextOfKin) {
            final List<UniscEduAgreementNaturalPerson> list = new ArrayList<>(model.getNextOfKinList());
            boolean processed = false;
            for (final UniscEduAgreementNaturalPerson k: list) {
                if (k.getId().equals(entity.getId())) {
                    processed = true;
                    k.from((PersonNextOfKin)entity);
                }
            }
            if (!processed) {
                list.add(new UniscEduAgreementNaturalPerson().from((PersonNextOfKin)entity));
            }

            model.setNextOfKinList(list);
        } //else {
            // WTF ?
        //}
    }

}
