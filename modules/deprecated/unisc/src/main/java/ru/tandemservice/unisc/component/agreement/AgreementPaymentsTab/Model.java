/* $Id: Model.java 8754 2009-06-29 09:27:43Z ekachanova $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisc.component.agreement.AgreementPaymentsTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayFactRow;
import ru.tandemservice.unisc.entity.agreements.UniscEduMainAgreement;

/**
 * @author vdanilov
 */
@State({@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "id")})
public class Model
{
    private Long id;
    private CommonPostfixPermissionModel secModel;
    private UniscEduMainAgreement agreement;
    private DynamicListDataSource<UniscEduAgreementPayFactRow> dataSource;

    public void setId(final Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return this.id;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        this.secModel = secModel;
    }

    public UniscEduMainAgreement getAgreement()
    {
        return agreement;
    }

    public void setAgreement(UniscEduMainAgreement agreement)
    {
        this.agreement = agreement;
    }

    public DynamicListDataSource<UniscEduAgreementPayFactRow> getDataSource()
    {
        return this.dataSource;
    }

    public void setDataSource(final DynamicListDataSource<UniscEduAgreementPayFactRow> dataSource)
    {
        this.dataSource = dataSource;
    }
}
