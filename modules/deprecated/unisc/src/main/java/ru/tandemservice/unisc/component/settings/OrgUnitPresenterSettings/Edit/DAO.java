package ru.tandemservice.unisc.component.settings.OrgUnitPresenterSettings.Edit;

import org.hibernate.Session;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unisc.entity.config.UniscOrgUnitPresenter;

public class DAO extends UniDao<Model> implements IDAO {
	@Override public void prepare(Model model) {
		final OrgUnit ou = get(OrgUnit.class, model.getId());
		
		UniscOrgUnitPresenter presenter = get(UniscOrgUnitPresenter.class, UniscOrgUnitPresenter.L_ORG_UNIT, ou);
		if (null == presenter) { 
			presenter = new UniscOrgUnitPresenter();
			presenter.setOrgUnit(ou);
		}
		
		model.setPresenter(presenter);
	}
	
	@Override
	public void save(Model model) {
		final Session session = getSession();
		session.setReadOnly(model.getPresenter().getOrgUnit(), true);
		session.saveOrUpdate(model.getPresenter());
	}
	
}
