package ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage1;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage.AbstractStageController;
import ru.tandemservice.unisc.dao.agreement.IUniscEduAgreementDAO;
import ru.tandemservice.unisc.utils.UniscPropertyUtils;

/**
 * @author vdanilov
 */
@SuppressWarnings("deprecation")
public class Controller extends AbstractStageController<IDAO, Model> {


    public void onClickReassignNumber(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        final String number = StringUtils.trimToEmpty(model.getAgreement().getNumber());
        if (number.isEmpty() || number.endsWith(UniscPropertyUtils.RESERVED_POSTFIX)) {
            // обновляем номер договора //
            model.getAgreement().setNumber(null);
            IUniscEduAgreementDAO.INSTANCE.get().assignNewAgreementNumber(model.getAgreement(), true);
        } else {
            throw new ApplicationException("Документу уже присвоен уникальный номер.");
        }
    }

    public void onChangePayFreq(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        model.getFirstStageModel().setValue(null);
        this.getDao().prepare(model);
    }

    public void onChangePayDuration(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
    }

    public void onChangeFirstStage(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
    }

    public void onChangeCustomerType(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
    }


}
