package ru.tandemservice.unisc.entity.agreements.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementFinesHistory;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Изменение начисления пеней по договору
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniscEduAgreementFinesHistoryGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisc.entity.agreements.UniscEduAgreementFinesHistory";
    public static final String ENTITY_NAME = "uniscEduAgreementFinesHistory";
    public static final int VERSION_HASH = 673792913;
    private static IEntityMeta ENTITY_META;

    public static final String L_OWNER = "owner";
    public static final String P_DATE = "date";
    public static final String P_ACTIVE = "active";

    private UniscEduAgreementBase _owner;     // Договор
    private Date _date;     // Дата
    private boolean _active;     // Начислять пени

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Договор. Свойство не может быть null.
     */
    @NotNull
    public UniscEduAgreementBase getOwner()
    {
        return _owner;
    }

    /**
     * @param owner Договор. Свойство не может быть null.
     */
    public void setOwner(UniscEduAgreementBase owner)
    {
        dirty(_owner, owner);
        _owner = owner;
    }

    /**
     * @return Дата. Свойство не может быть null.
     */
    @NotNull
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date Дата. Свойство не может быть null.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return Начислять пени. Свойство не может быть null.
     */
    @NotNull
    public boolean isActive()
    {
        return _active;
    }

    /**
     * @param active Начислять пени. Свойство не может быть null.
     */
    public void setActive(boolean active)
    {
        dirty(_active, active);
        _active = active;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UniscEduAgreementFinesHistoryGen)
        {
            setOwner(((UniscEduAgreementFinesHistory)another).getOwner());
            setDate(((UniscEduAgreementFinesHistory)another).getDate());
            setActive(((UniscEduAgreementFinesHistory)another).isActive());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniscEduAgreementFinesHistoryGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniscEduAgreementFinesHistory.class;
        }

        public T newInstance()
        {
            return (T) new UniscEduAgreementFinesHistory();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "owner":
                    return obj.getOwner();
                case "date":
                    return obj.getDate();
                case "active":
                    return obj.isActive();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "owner":
                    obj.setOwner((UniscEduAgreementBase) value);
                    return;
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "active":
                    obj.setActive((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "owner":
                        return true;
                case "date":
                        return true;
                case "active":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "owner":
                    return true;
                case "date":
                    return true;
                case "active":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "owner":
                    return UniscEduAgreementBase.class;
                case "date":
                    return Date.class;
                case "active":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniscEduAgreementFinesHistory> _dslPath = new Path<UniscEduAgreementFinesHistory>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniscEduAgreementFinesHistory");
    }
            

    /**
     * @return Договор. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementFinesHistory#getOwner()
     */
    public static UniscEduAgreementBase.Path<UniscEduAgreementBase> owner()
    {
        return _dslPath.owner();
    }

    /**
     * @return Дата. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementFinesHistory#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return Начислять пени. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementFinesHistory#isActive()
     */
    public static PropertyPath<Boolean> active()
    {
        return _dslPath.active();
    }

    public static class Path<E extends UniscEduAgreementFinesHistory> extends EntityPath<E>
    {
        private UniscEduAgreementBase.Path<UniscEduAgreementBase> _owner;
        private PropertyPath<Date> _date;
        private PropertyPath<Boolean> _active;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Договор. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementFinesHistory#getOwner()
     */
        public UniscEduAgreementBase.Path<UniscEduAgreementBase> owner()
        {
            if(_owner == null )
                _owner = new UniscEduAgreementBase.Path<UniscEduAgreementBase>(L_OWNER, this);
            return _owner;
        }

    /**
     * @return Дата. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementFinesHistory#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(UniscEduAgreementFinesHistoryGen.P_DATE, this);
            return _date;
        }

    /**
     * @return Начислять пени. Свойство не может быть null.
     * @see ru.tandemservice.unisc.entity.agreements.UniscEduAgreementFinesHistory#isActive()
     */
        public PropertyPath<Boolean> active()
        {
            if(_active == null )
                _active = new PropertyPath<Boolean>(UniscEduAgreementFinesHistoryGen.P_ACTIVE, this);
            return _active;
        }

        public Class getEntityClass()
        {
            return UniscEduAgreementFinesHistory.class;
        }

        public String getEntityName()
        {
            return "uniscEduAgreementFinesHistory";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
