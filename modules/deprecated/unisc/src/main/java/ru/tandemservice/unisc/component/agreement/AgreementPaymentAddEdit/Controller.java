package ru.tandemservice.unisc.component.agreement.AgreementPaymentAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.tapsupport.TapSupportUtils;
import org.tandemframework.tapsupport.confirm.ClickButtonAction;
import org.tandemframework.tapsupport.confirm.ConfirmInfo;


public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        final Model model = getModel(component);
        getDao().prepare(model);
    }

    public void onClickApply(IBusinessComponent component)
    {
        final Model model = getModel(component);
        String confirmMessage = getDao().getConfirmMessage(model);
        if (null == component.getClientParameter() && null != confirmMessage)
        {
            ConfirmInfo confirm = new ConfirmInfo(confirmMessage, new ClickButtonAction("submit_desktop", "ok", true));
            TapSupportUtils.displayConfirm(confirm);
            return;
        }
        getDao().save(model);
        deactivate(component);
    }
}
