/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unimove.dao;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.runtime.EntityRuntime;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.entity.catalog.codes.OrderStatesCodes;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author vip_delete
 * @since 16.10.2008
 */
@SuppressWarnings("unchecked")
public class MoveDao extends UniBaseDao implements IMoveDao
{
    /**
     * Удаляет выписку вместе с параграфом, если в параграфе нет других выписок.
     * Если в параграфе есть другие выписки, система выдает пользователю ошибку.
     * @param extract выписка
     */
    @Override
    public void deleteExtractWithParagraph(IAbstractExtract extract)
    {
        final Session session = getSession();
        extract = refresh(extract);

        if (extract.getParagraph() == null) {
            session.delete(extract);
            return;
        }

        final IAbstractParagraph paragraph = extract.getParagraph();
        final ErrorCollector errCollector = UserContext.getInstance().getErrorCollector();

        if (paragraph.getExtractCount() > 1) {
            errCollector.add("Нельзя удалить выписку вместе с параграфом, поскольку в параграфе имеются другие выписки.");
        }

        if (!errCollector.hasErrors()) {
            session.delete(extract);
            session.delete(paragraph);
        }
    }

    /**
     * Удаляет выписку из параграфа (без проверок), затем перенумеровывает все выписки в этом параграфе: выпискам параграфа, полученным с помощью {@code IAbstractParagraph.getExtractList()} присваиваются последовательно номера, начиная с первого.
     * @param extract выписка
     */
    @Override
    public void deleteExtractFromParagraph(IAbstractExtract extract)
    {
        final Session session = getSession();
        final IAbstractParagraph<IAbstractOrder> paragraph = extract.getParagraph();

        session.delete(extract);

        int i = 1;
        for (IAbstractExtract item : paragraph.getExtractList())
        {
            item.setNumber(i);
            session.update(item);
            i++;
        }
    }

    /**
     * Удаляет приказ и параграфы приказа, полученные с помощью {@code IAbstractOrder.getParagraphList()} (без проверок). Выпискам каждого параграфа, полученным с помощью {@code IAbstractParagraph.getExtractList()} указывается состояние {@code newExtractState}.
     * @param order приказ
     * @param newExtractState новое состояние выписок
     */
    @Override
    public void deleteOrder(IAbstractOrder order, ICatalogItem newExtractState)
    {
        final Session session = getSession();
        for (IAbstractParagraph<IAbstractOrder> paragraph : order.getParagraphList())
        {
            for (IAbstractExtract extract : paragraph.getExtractList())
            {
                extract.setParagraph(null);
                extract.setNumber(null);
                extract.setState(newExtractState);
                session.update(extract);
            }
            session.delete(paragraph);
        }
        session.delete(order);
    }

    /**
     * Удаляет приказ со всеми параграфами, полученными с помощью {@code IAbstractOrder.getParagraphList()}, и выписками параграфов, полученными с помощью {@code IAbstractParagraph.getExtractList()} (без проверок).
     * @param order приказ
     */
    @Override
    public void deleteOrderWithExtracts(IAbstractOrder order)
    {
        final Session session = getSession();
        for (IAbstractParagraph<IAbstractOrder> paragraph : order.getParagraphList())
        {
            paragraph.getExtractList().forEach(session::delete);
            session.delete(paragraph);
        }
        session.delete(order);
    }

    /**
     * Удаляет параграф из приказа (без проверок), затем перенумеровывает все параграфы в этом приказе: параграфам приказа, полученным с помощью {@code IAbstractOrder.getParagraphList()} присваиваются последовательно номера, начиная с первого.
     * Выписки удаляемого параграфа, полученные с помощью {@code IAbstractParagraph.getExtractList()}, удаляются, если новое состояние - null. В противном случае, выпискам указывается новое состояние.
     * @param paragraph параграф
     * @param newExtractState новое состояение выписок, null - выписки удалятся
     */
    @Override
    public void deleteParagraph(IAbstractParagraph<? extends IAbstractOrder> paragraph, ICatalogItem newExtractState)
    {
        final Session session = getSession();

        final IAbstractOrder order = paragraph.getOrder();
        int i = order.getParagraphCount() + 1;

        for (IAbstractExtract extract : paragraph.getExtractList())
        {
            if (newExtractState != null) {
                extract.setState(newExtractState);
                extract.setParagraph(null);
                session.update(extract);
            } else {
                session.delete(extract);
            }
        }

        session.delete(paragraph);
        session.flush();

        for (IAbstractParagraph item : order.getParagraphList())
        {
            item.setNumber(i++);
            session.update(item);
        }
        session.flush();

        i = 1;
        for (IAbstractParagraph item : order.getParagraphList())
        {
            item.setNumber(i++);
            session.update(item);
        }
        session.flush();
    }

    /**
     * Проводит приказ.
     * Выполняется проверка на наличие номера приказа, параграфов в приказе, а также проверка на то, что приказ еще не проведен(поле "Дата проведения" пусто).
     * Параграфы приказа получаются с помощью метода {@code IAbstractOrder.getParagraphList()}, если сортировщик ({@code IOrderParagraphsResorter}) не задан для данного вида приказа; если же сортировщик задан, то параграфы получаются с помощью метода {@code IOrderParagraphsResorter.resortParagraphsBeforeCommit(order)}.
     * Для каждого параграфа выполняется проверка на наличие выписок с помощью метода {@code IAbstractParagraph.getExtractCount()}.
     * Для каждой выписки параграфа, полученной с помощью метода {@code IAbstractParagraph.getExtractList()}, выполняется метод проведения выписки {@code IMoveDao.doCommitExtract(extract, newExtractState, parameters)}.
     * Приказу в качестве даты проведения указывается текущая дата, состояние приказа становится {@code newOrderState}.
     * Если проверки приказа и выписок не были пройдены, система выдает пользователю ошибку, приказ и выписки не проводятся.
     * @param order приказ
     * @param newOrderState новое состояние приказа
     * @param newExtractState новое состояние выписок приказа
     * @param parameters параметры
     */
    @Override
    public void doCommitOrder(IAbstractOrder order, ICatalogItem newOrderState, ICatalogItem newExtractState, Map parameters)
    {
        this.flushClearAndRefresh(); // обновляем все объекты в сессии

        final Session session = lock(String.valueOf(order.getId()));
        order = getNotNull(order.getId());

        try {

            if (order.getNumber() == null)
                addError("Нельзя провести приказ. У приказа не указан номер!");

            if (order.getCommitDateSystem() != null)
                addError("Нельзя провести приказ. Приказ «" + order.getTitle() + "» уже проведен!");

            if (order.getParagraphCount() == 0)
                addError("Нельзя провести приказ. В приказе «" + order.getTitle() + "» нет параграфов!");

            if (!order.getState().getCode().equals(OrderStatesCodes.ACCEPTED))
                addError("Провести можно только согласованный приказ. Текущее состояние приказа: " + order.getState().getTitle() + ".");

            final IOrderParagraphsResorter resorter = getOrderParagraphsResorter(order);
            final List<? extends IAbstractParagraph> paragraphList = null == resorter ? order.getParagraphList() : resorter.resortParagraphsBeforeCommit(order);

            // проводим выписки из приказа
            for (IAbstractParagraph<IAbstractOrder> paragraph : paragraphList)
            {
                final List<? extends IAbstractExtract> extractList = paragraph.getExtractList();
                if (extractList == null || extractList.isEmpty()) {
                    addError("Нельзя провести приказ. Параграф №" + paragraph.getNumber() + " приказа «" + paragraph.getOrder().getTitle() + "» пуст!");
                } else {
                    for (IAbstractExtract extract : extractList) {
                        doCommitExtract(extract, newExtractState, parameters);
                    }
                }
            }

            // сохранение приказа, состояния, созранение в базу
            order.setCommitDateSystem(new Date());
            order.setState(newOrderState);
            session.update(order);
            session.flush();

        } catch (Exception t) {
            session.clear();
            throw CoreExceptionUtils.getRuntimeException(t);
        } finally {
            if (null != UserContext.getInstance() && UserContext.getInstance().getErrorCollector().hasErrors()) {
                session.clear();
            }
        }
    }

    /**
     * Откатывает приказ.
     * Выполняется проверка на то, что приказ проведен (поле "Дата проведения" не пусто).
     * Параграфы приказа получаются с помощью метода {@code IAbstractOrder.getParagraphList()}, если сортировщик ({@code IOrderParagraphsResorter}) не задан для данного вида приказа; если же сортировщик задан, то параграфы получаются с помощью метода {@code IOrderParagraphsResorter.resortParagraphsBeforeRollback(order)}.
     * Для каждой выписки, полученной с помощью метода {@code IAbstractParagraph.getExtractList()}, каждого параграфа, полученного с помощью метода {@code IAbstractOrder.getParagraphList()}, выполняется метод отката выписки {@code IMoveDao.doRollbackExtract(extract, newExtractState, parameters)}.
     * Приказу в качестве даты проведения указывается пустое значение, состояние приказа становится {@code newOrderState}.
     * Если проверки приказа и выписок не были пройдены, система выдает пользователю ошибку, приказ и выписки не откатываются.
     * @param order приказ
     * @param newOrderState новое состояние приказа
     * @param newExtractState новое состояние выписок приказа
     * @param parameters параметры
     */
    @Override
    public void doRollbackOrder(IAbstractOrder order, ICatalogItem newOrderState, ICatalogItem newExtractState, Map parameters)
    {
        this.flushClearAndRefresh(); // обновляем все объекты в сессии

        ErrorCollector errCollector = UserContext.getInstance().getErrorCollector();
        Session session = lock(String.valueOf(order.getId()));
        order = getNotNull(order.getId());

        try {

            if (order.getCommitDateSystem() == null)
                errCollector.add("Нельзя откатить приказ. Приказ «" + order.getTitle() + "» еще не проведен!");

            IOrderParagraphsResorter resorter = getOrderParagraphsResorter(order);
            List<? extends IAbstractParagraph> list = null == resorter ? order.getParagraphList() : resorter.resortParagraphsBeforeRollback(order);

            // откатываем выписки из приказа
            for (IAbstractParagraph<IAbstractOrder> paragraph : list) {
                for (IAbstractExtract extract : paragraph.getExtractList()) {
                    doRollbackExtract(extract, newExtractState, parameters);
                }
            }

            // сохранение приказа, состояния, созранение в базу
            order.setCommitDateSystem(null);
            order.setState(newOrderState);
            session.update(order);
            session.flush();

        } catch (Exception t) {
            session.clear();
            throw CoreExceptionUtils.getRuntimeException(t);
        } finally {
            if (errCollector.hasErrors()) {
                session.clear();
            }
        }
    }

    /**
     * Проводит выписку.
     * Выполняется проверка на то, что выписка еще не проведена(признак "Проведена").
     * Для выписки поднимается указанный DAO - наследник IExtractComponentDao, вызывается метод внесения выпиской изменений в систему IExtractComponentDao.doCommit(extract, parameters).
     * Выписке указывается признак "Проведен" - true, состояние выписки становится {@code newExtractState}.
     * Если проверки выписки не были пройдены, система выдает пользователю ошибку, выписка не проводится.
     * @param extract выписка
     * @param newExtractState новое состояние выписки
     * @param parameters параметры
     */
    @Override
    public void doCommitExtract(IAbstractExtract extract, ICatalogItem newExtractState, Map parameters)
    {
        Session session = getSession();
        session.refresh(extract);

        if (extract.isCommitted()) addError("Нельзя провести уже проведенную выписку.");

        // На случай, если объект изменился с момента создания выписки, делаем его рефреш
        if (null != extract.getEntity()) { session.refresh(extract.getEntity()); }

        //тут берем dao этой выписки
        IExtractComponentDao extractDao = getExtractComponentDao(extract);

        //тут надо сделать проверки

        //тут проводим выписку
        extractDao.doCommit(extract, parameters);

        if (null == UserContext.getInstance() || !UserContext.getInstance().getErrorCollector().hasErrors())
        {
            extract.setCommitted(true);
            extract.setState(newExtractState);

            if (null != extract.getEntity())
                session.update(extract.getEntity());

            session.update(extract);
        }
    }

    /**
     * Откатывает выписку.
     * Выполняется проверка на то, что выписка проведена(признак "Проведена").
     * Для выписки поднимается указанный DAO - наследник IExtractComponentDao, вызывается метод отмены вносимых выпиской изменений в систему IExtractComponentDao.doRollback(extract, parameters).
     * Выписке указывается признак "Проведен" - false, состояние выписки становится {@code newExtractState}.
     * Если проверки выписки не были пройдены, система выдает пользователю ошибку, выписка не откатывается.
     * @param extract выписка
     * @param newExtractState новое состояние выписки
     * @param parameters параметры
     */
    @Override
    public void doRollbackExtract(IAbstractExtract extract, ICatalogItem newExtractState, Map parameters)
    {
        Session session = getSession();
        session.refresh(extract);

        //ErrorCollector errCollector = UserContext.getInstance().getErrorCollector();
        if (!extract.isCommitted()) addError("Нельзя откатить не проведенную выписку.");

        // На случай, если объект изменился с момента создания выписки, делаем его рефреш
        if (null != extract.getEntity()) session.refresh(extract.getEntity());

        //тут берем dao этой выписки
        IExtractComponentDao extractDao = getExtractComponentDao(extract);

        //тут надо сделать проверки

        //тут откатываем выписку
        extractDao.doRollback(extract, parameters);

        if (null == UserContext.getInstance() || !UserContext.getInstance().getErrorCollector().hasErrors())
        {
            extract.setCommitted(false);
            extract.setState(newExtractState);

            if (null != extract.getEntity())
                session.update(extract.getEntity());

            session.update(extract);
        }
    }

    private IExtractComponentDao getExtractComponentDao(IAbstractExtract extract)
    {
        String daoName = EntityRuntime.getMeta(Hibernate.getClass(extract)).getName() + "_extractDao";
        return (IExtractComponentDao) ApplicationRuntime.getBean(daoName);
    }

    private IOrderParagraphsResorter getOrderParagraphsResorter(IAbstractOrder order)
    {
        if(null == order.getType()) return null;
        String resorterName = "orderParagraphsResorter_" + EntityRuntime.getMeta(Hibernate.getClass(order)).getName() + "_" + order.getType().getCode();
        if(ApplicationRuntime.containsBean(resorterName))
            return (IOrderParagraphsResorter) ApplicationRuntime.getBean(resorterName);
        else return null;
    }

    public static void addError(String message)
    {
        ErrorCollector errCollector = null != UserContext.getInstance() ? UserContext.getInstance().getErrorCollector() : null;
        if(null != errCollector) errCollector.add(message);
        else throw new RuntimeException(message);
    }
}