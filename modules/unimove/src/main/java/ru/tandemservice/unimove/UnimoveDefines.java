/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unimove;

/**
 * @author vip_delete
 * @since 15.10.2008
 */
public interface UnimoveDefines
{
    String CATALOG_ORDER_STATE = "OrderState";
    String CATALOG_ORDER_STATE_FORMATIVE = "1";     //Формируется
    String CATALOG_ORDER_STATE_ACCEPTABLE = "2";    //На согласовании
    String CATALOG_ORDER_STATE_ACCEPTED = "3";      //Согласовано
    String CATALOG_ORDER_STATE_REJECTED = "4";      //Отклонено
    String CATALOG_ORDER_STATE_FINISHED = "5";      //Проведено

    String CATALOG_EXTRACT_STATE = "ExtractState";
    String CATALOG_EXTRACT_STATE_FORMATIVE = "1";   //Формируется
    String CATALOG_EXTRACT_STATE_ACCEPTABLE = "2";  //На согласовании
    String CATALOG_EXTRACT_STATE_ACCEPTED = "3";    //Согласовано
    String CATALOG_EXTRACT_STATE_REJECTED = "4";    //Отклонено
    String CATALOG_EXTRACT_STATE_IN_ORDER = "5";    //В приказах
    String CATALOG_EXTRACT_STATE_FINISHED = "6";    //Проведено
}
