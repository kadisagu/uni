/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unimove;

import java.util.Date;
import java.util.List;

import ru.tandemservice.unimv.IAbstractDocument;

/**
 * @author vip_delete
 * @since 15.10.2008
 */
public interface IAbstractOrder extends IAbstractDocument
{
    String P_COMMIT_DATE_SYSTEM = "commitDateSystem"; // системная дата проведения приказа
    String P_NUMBER = "number";                // номер приказа в реестре ОУ

    /**
     * Возвращает дату проведения приказа.
     * @return дата проведения приказа
     */
    Date getCommitDateSystem();

    /**
     * Задает дату проведения приказа.
     * @param commitDate дата проведения приказа
     */
    void setCommitDateSystem(Date commitDate);

    /**
     * Возвращает номер приказа.
     * @return номер приказа
     */
    String getNumber();

    /**
     * Задает номер приказа.
     * @param number номер приказа
     */
    void setNumber(String number);

    // Additional Data

    /**
     * Возвращает список параграфов приказа.
     * @return список параграфов приказа
     */
    List<? extends IAbstractParagraph> getParagraphList();

    /**
     * Возвращает количество параграфов приказа
     * @return количество параграфов приказа
     */
    int getParagraphCount();
}