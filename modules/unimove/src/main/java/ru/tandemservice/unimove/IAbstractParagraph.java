/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unimove;

import java.util.List;

import org.tandemframework.core.entity.IEntity;

/**
 * Параграф приказа Order
 *
 * @author vip_delete
 * @since 15.10.2008
 */
public interface IAbstractParagraph<Order extends IAbstractOrder> extends IEntity
{
    String P_NUMBER = "number"; // номер параграфа в приказе
    String L_ORDER = "order";  // приказ, в котором находится параграф

    /**
     * Возвращает номер параграфа в приказе.
     * @return номер параграфа в приказе
     */
    int getNumber();

    /**
     * Задает номер параграфа в приказе.
     * @param number номер параграфа
     */
    void setNumber(int number);

    /**
     * Возвращает приказ параграфа.
     * @return приказа параграфа
     */
    Order getOrder();

    /**
     * Задает приказ.
     * @param order приказ
     */
    void setOrder(Order order);

    // Additional Data

    /**
     * Возвращает список выписок параграфа.
     * @return список выписок
     */
    List<? extends IAbstractExtract> getExtractList();

    /**
     * Возвращает количество выписок параграфа.
     * @return количество выписок параграфа
     */
    int getExtractCount();
}
