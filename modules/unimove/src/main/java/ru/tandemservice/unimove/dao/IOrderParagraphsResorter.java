/**
 * $Id$
 */
package ru.tandemservice.unimove.dao;

import java.util.List;

import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * @author dseleznev
 * Created on: 31.03.2010
 */
public interface IOrderParagraphsResorter
{
    /**
     * Сортирует параграфы приказа перед проведением.
     * @param order приказ
     * @return список отсортированных параграфов
     */
    List<IAbstractParagraph<IAbstractOrder>> resortParagraphsBeforeCommit(IAbstractOrder order);

    /**
     * Сортирует параграфы приказа перед откатом.
     * @param order приказ
     * @return список отсортированных параграфов
     */
    List<IAbstractParagraph<IAbstractOrder>> resortParagraphsBeforeRollback(IAbstractOrder order);
}