/* $Id$ */
package ru.tandemservice.unimove;

/**
 * Документ, который может быть напечатан
 *
 * @author Nikolay Fedorovskih
 * @since 02.08.2013
 */
public interface IPrintableExtract
{
    /**
     * Определяет, может ли быть распечатан конкретный документ
     *
     * @return да или нет
     */
    boolean canBePrinted();

    /**
     * Печать выписки.
     * Желательно, перед печатью проверять canBePrinted().
     * @param printPdf
     */
    void doExtractPrint(boolean printPdf);

    /**
     * Печать приказа, в котором находится выписка.
     * Желательно, перед печатью проверять canBePrinted().
     * @param printPdf
     */
    void doOrderPrint(boolean printPdf);
}