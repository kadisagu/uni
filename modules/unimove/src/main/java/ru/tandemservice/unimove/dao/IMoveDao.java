/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unimove.dao;

import java.util.Map;

import org.tandemframework.common.catalog.entity.ICatalogItem;

import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * Дефолтная реализация - {@link MoveDao}, читайте там подробности того, как работают методы.
 * @author vip_delete
 * @since 16.10.2008
 */
public interface IMoveDao
{
    String MOVE_DAO_BEAN_NAME = "moveDao";

    /**
     * Удаляет выписку вместе с параграфом, если в параграфе нет других выписок.
     * @param extract выписка
     */
    void deleteExtractWithParagraph(IAbstractExtract extract);

    /**
     * Удаляет выписку из параграфа, перенумеровывает все выписки в этом параграфе.
     * @param extract выписка
     */
    void deleteExtractFromParagraph(IAbstractExtract extract);

    /**
     * Удаляет приказ без удаления выписок, указывается новое состояние для выписок.
     * @param order приказ
     * @param newExtractState новое состояние выписок
     */
    void deleteOrder(IAbstractOrder order, ICatalogItem newExtractState);

    /**
     * Удаляет приказ со всеми выписками и параграфами.
     * @param order приказ
     */
    void deleteOrderWithExtracts(IAbstractOrder order);

    /**
     * Удаляет параграф, перенумеровывает параграфы в приказе. Выписки в параграфе принимают указанное состояние или удаляются, если указан null.
     * @param paragraph параграф
     * @param newExtractState новое состояение выписок, null - выписки удалятся
     */
    void deleteParagraph(IAbstractParagraph<? extends IAbstractOrder> paragraph, ICatalogItem newExtractState);

    /**
     * Проводит приказ.
     * @param order приказ
     * @param newOrderState новое состояние приказа
     * @param newExtractState новое состояние выписок приказа
     * @param parameters параметры
     */
    void doCommitOrder(IAbstractOrder order, ICatalogItem newOrderState, ICatalogItem newExtractState, Map parameters);

    /**
     * Откатывает приказ.
     * @param order приказ
     * @param newOrderState новое состояние приказа
     * @param newExtractState новое состояние выписок приказа
     * @param parameters параметры
     */
    void doRollbackOrder(IAbstractOrder order, ICatalogItem newOrderState, ICatalogItem newExtractState, Map parameters);

    /**
     * Проводит выписку.
     * @param extract выписка
     * @param newExtractState новое состояние выписки
     * @param parameters параметры
     */
    void doCommitExtract(IAbstractExtract extract, ICatalogItem newExtractState, Map parameters);

    /**
     * Откатывает выписку.
     * @param extract выписка
     * @param newExtractState новое состояние выписки
     * @param parameters параметры
     */
    void doRollbackExtract(IAbstractExtract extract, ICatalogItem newExtractState, Map parameters);
}