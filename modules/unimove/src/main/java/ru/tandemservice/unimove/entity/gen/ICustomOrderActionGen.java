package ru.tandemservice.unimove.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unimove.entity.ICustomOrderAction;
import ru.tandemservice.unimove.entity.ICustomOrderExtract;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import ru.tandemservice.unimove.entity.ICustomOrderAction;

/**
 * Действие кастомного приказа
 *
 * Интерфейс для логически обособленных действий, которые можно совершать в рамках проведения пользовательского приказа.
 * Одна выписка кастомного приказа может содержать несколько таких разных действий.
 */
@SuppressWarnings({"all", "unchecked", "unused"})
public abstract class ICustomOrderActionGen extends InterfaceStubBase
 implements ICustomOrderAction{
    public static final int VERSION_HASH = -1744500484;

    public static final String L_CUSTOM_EXTRACT = "customExtract";

    private ICustomOrderExtract _customExtract;

    @NotNull

    public ICustomOrderExtract getCustomExtract()
    {
        return _customExtract;
    }

    public void setCustomExtract(ICustomOrderExtract customExtract)
    {
        _customExtract = customExtract;
    }

    private static final Path<ICustomOrderAction> _dslPath = new Path<ICustomOrderAction>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ru.tandemservice.unimove.entity.ICustomOrderAction");
    }
            

    /**
     * @return Выписка из кастомного приказа, с набором действий, определенных пользователем. Свойство не может быть null.
     * @see ru.tandemservice.unimove.entity.ICustomOrderAction#getCustomExtract()
     */
    public static ICustomOrderExtractGen.Path<ICustomOrderExtract> customExtract()
    {
        return _dslPath.customExtract();
    }

    public static class Path<E extends ICustomOrderAction> extends EntityPath<E>
    {
        private ICustomOrderExtractGen.Path<ICustomOrderExtract> _customExtract;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка из кастомного приказа, с набором действий, определенных пользователем. Свойство не может быть null.
     * @see ru.tandemservice.unimove.entity.ICustomOrderAction#getCustomExtract()
     */
        public ICustomOrderExtractGen.Path<ICustomOrderExtract> customExtract()
        {
            if(_customExtract == null )
                _customExtract = new ICustomOrderExtractGen.Path<ICustomOrderExtract>(L_CUSTOM_EXTRACT, this);
            return _customExtract;
        }

        public Class getEntityClass()
        {
            return ICustomOrderAction.class;
        }

        public String getEntityName()
        {
            return "ru.tandemservice.unimove.entity.ICustomOrderAction";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
