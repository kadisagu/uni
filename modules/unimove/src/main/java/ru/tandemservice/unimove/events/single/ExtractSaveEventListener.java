/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unimove.events.single;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.hibsupport.event.single.FilteredSingleEntityEventListener;
import org.tandemframework.hibsupport.event.single.listener.IHibernateInsertListener;
import org.tandemframework.hibsupport.event.single.type.HibernateSaveEvent;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

/**
 * Обработчик события сохранения выписки.
 * Выписке указывается текущая дата как дата создания и одно из двух состояний: "На формировании", если у выписки нет параграфа, "В приказах", если параграф есть.
 *
 * @author vip_delete
 * @since 21.10.2008
 */
public class ExtractSaveEventListener extends FilteredSingleEntityEventListener<HibernateSaveEvent> implements IHibernateInsertListener
{
    @SuppressWarnings("unchecked")
    @Override
    public void onFilteredEvent(final HibernateSaveEvent event)
    {
        IAbstractExtract extract = (IAbstractExtract) event.getEntity();
        extract.setCreateDate(CoreDateUtils.getDateWithMiliseconds(extract.getCreateDate()));// it is unique field
        extract.setCommitted(false);
        if (extract.getParagraph() != null) // выписка из списочного приказа сразу в приказе создается
            extract.setState(UniDaoFacade.getCoreDao().getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER));
        else // для сборного приказа в состоянии формируется
            extract.setState(UniDaoFacade.getCoreDao().getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_FORMATIVE));
    }
}