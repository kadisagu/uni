package ru.tandemservice.unimove.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Состояние приказа"
 * Имя сущности : orderStates
 * Файл data.xml : unimove.data.xml
 */
public interface OrderStatesCodes
{
    /** Константа кода (code) элемента : Формируется (title) */
    String FORMING = "1";
    /** Константа кода (code) элемента : На согласовании (title) */
    String ACCEPTABLE = "2";
    /** Константа кода (code) элемента : Согласовано (title) */
    String ACCEPTED = "3";
    /** Константа кода (code) элемента : Отклонено (title) */
    String REJECTED = "4";
    /** Константа кода (code) элемента : Проведено (title) */
    String FINISHED = "5";

    Set<String> CODES = ImmutableSet.of(FORMING, ACCEPTABLE, ACCEPTED, REJECTED, FINISHED);
}
