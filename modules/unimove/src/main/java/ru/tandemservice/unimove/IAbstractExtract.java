/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unimove;

import org.tandemframework.common.catalog.entity.ICatalogItem;

import ru.tandemservice.unimv.IAbstractDocument;

/**
 * @author vip_delete
 * @since 15.10.2008
 */
public interface IAbstractExtract<E, P extends IAbstractParagraph> extends IAbstractDocument
{
    String P_NUMBER = "number";          // номер выписки в параграфе
    String P_COMMITTED = "committed";    // true, если выписка проведена
    String L_ENTITY = "entity";         // объект на который создана выписка
    String L_PARAGRAPH = "paragraph";   // параграф выписки

    /**
     * Возвращает сущность, связанную с выпиской и которую выписка меняет.
     * @return связанную сущность
     */
    E getEntity();

    /**
     * Задает связанную сущность.
     * @param entity связанная сущность
     */
    void setEntity(E entity);

    /**
     * Возвращает номер выписки в параграфе.
     * @return номер выписки
     */
    Integer getNumber();

    /**
     * Задает номер выписки в параграфе.
     * @param number номер выписки
     */
    void setNumber(Integer number);

    /**
     * Указывает, проведена ли выписка.
     * @return проведена ли выписка
     */
    boolean isCommitted();

    /**
     * Задает признак проведенности выписки.
     * @param committed признак проведенности выписки
     */
    void setCommitted(boolean committed);

    /**
     * Задает тип выписки.
     * @param type тип выписки
     */
    void setType(ICatalogItem type);

    /**
     * Возвращает параграф выписки.
     * @return параграф выписки
     */
    P getParagraph();

    /**
     * Задает параграф.
     * @param paragraph параграф
     */
    void setParagraph(P paragraph);
}
