package ru.tandemservice.unimove.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unimove.entity.ICustomOrderExtract;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import ru.tandemservice.unimove.entity.ICustomOrderExtract;

/**
 * Выписка из кастомного приказа, с набором действий, определенных пользователем
 *
 * Интерфейс для выписок из приказов, в которых пользователь может сам выбирать набор действий.
 */
@SuppressWarnings({"all", "unchecked", "unused"})
public abstract class ICustomOrderExtractGen extends InterfaceStubBase
 implements ICustomOrderExtract{
    public static final int VERSION_HASH = -2085217788;



    private static final Path<ICustomOrderExtract> _dslPath = new Path<ICustomOrderExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ru.tandemservice.unimove.entity.ICustomOrderExtract");
    }
            

    public static class Path<E extends ICustomOrderExtract> extends EntityPath<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return ICustomOrderExtract.class;
        }

        public String getEntityName()
        {
            return "ru.tandemservice.unimove.entity.ICustomOrderExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
