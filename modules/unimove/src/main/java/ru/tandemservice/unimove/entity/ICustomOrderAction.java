/* $Id$ */
package ru.tandemservice.unimove.entity;

import org.tandemframework.core.entity.IEntity;

/**
 * <p>
 * Действие кастомного приказа. Это некоторая обособленная единица, которая выполняет определенное действие бизнес-логики, связанной с приказами.
 * Действие кастомного приказа ссылается на выписку кастомного приказа {@link ru.tandemservice.unimove.entity.ICustomOrderExtract},
 * т.е. одна выписка может содержать (выполнять) произвольное количество действий.
 * <p>
 * Действие кастомного приказа должно хранить данные, необходимые для отмены (отката) приказа. К примеру, дейтсвие смены статуса студента должно,
 * кроме нового статуса, хранить предыдущий статус студента, который сохраняется в момент выполнения действия. При откате приказа, старый статус студету возвращется.
 *
 * @author Nikolay Fedorovskih
 * @since 28.08.2013
 */
public interface ICustomOrderAction extends IEntity
{
    /**
     * Возвращает выписку из кастомного приказа
     *
     * @return выписка
     */
    ICustomOrderExtract getCustomExtract();

    /**
     * Задает выписку из кастомного приказа
     *
     * @param customExtract выписка
     */
    void setCustomExtract(ICustomOrderExtract customExtract);

    /**
     * Копирование данных из аналогичного объекта
     *
     * @param another того же типа действие кастомного приказа
     */
    void update(IEntity another);
}