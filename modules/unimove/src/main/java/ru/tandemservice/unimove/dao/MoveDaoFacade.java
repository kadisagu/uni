/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unimove.dao;

import org.tandemframework.core.runtime.ApplicationRuntime;

/**
 * @author vip_delete
 * @since 27.10.2008
 */
public final class MoveDaoFacade
{
    private static IMoveDao _moveDao;

    public static IMoveDao getMoveDao()
    {
        if (_moveDao == null)
            _moveDao = (IMoveDao) ApplicationRuntime.getBean(IMoveDao.MOVE_DAO_BEAN_NAME);
        return _moveDao;
    }
}
