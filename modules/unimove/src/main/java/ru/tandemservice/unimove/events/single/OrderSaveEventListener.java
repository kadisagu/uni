/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unimove.events.single;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.hibsupport.event.single.FilteredSingleEntityEventListener;
import org.tandemframework.hibsupport.event.single.listener.IHibernateInsertListener;
import org.tandemframework.hibsupport.event.single.type.HibernateSaveEvent;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.OrderStates;

/**
 * Обработчик события сохранения приказа.
 * Приказу указывается текущая дата как дата создания и состояние "На формировании".
 *
 * @author vip_delete
 * @since 21.10.2008
 */
public class OrderSaveEventListener extends FilteredSingleEntityEventListener<HibernateSaveEvent> implements IHibernateInsertListener
{
    @Override
    public void onFilteredEvent(final HibernateSaveEvent event)
    {
        IAbstractOrder order = (IAbstractOrder) event.getEntity();
        order.setCreateDate(CoreDateUtils.getDateWithMiliseconds(order.getCreateDate()));// it is unique field
        order.setState(UniDaoFacade.getCoreDao().getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE));
    }
}
