/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unimove.service;

import org.tandemframework.core.exception.ApplicationException;

import ru.tandemservice.uni.services.UniService;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.OrderStates;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.services.visatask.ITouchVisaTaskHandler;

/**
 * Стандартный сервис завершения процедуры согласования приказа
 *
 * @author vip_delete
 * @since 20.11.2008
 */
public class AcceptVisaOrderService extends UniService implements ITouchVisaTaskHandler
{
    private IAbstractOrder _order;

    @Override
    public void init(IAbstractDocument document)
    {
        if (_order != null) {
            throw new DoubleInitializeServiceError(this);
        }
        _order = (IAbstractOrder) document;
    }

    @Override
    protected void doValidate()
    {
        if (UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTED.equals(_order.getState().getCode()))
            throw new ApplicationException("Приказ уже в состоянии «" + _order.getState().getTitle() + "».");
    }

    @Override
    protected void doExecute() throws Exception
    {
        _order.setState(getCoreDao().getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTED));
        getCoreDao().update(_order);
    }

    protected IAbstractOrder getOrder() {
        return _order;
    }
}