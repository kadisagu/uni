/**
 *$Id:$
 */
package ru.tandemservice.uniemp.base.bo.EmpSettingsPensionQualificationAge.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.uniemp.entity.employee.PensionQualificationAgeSettings;

/**
 * @author Alexander Shaburov
 * @since 11.09.12
 */
public interface IPensionQualificationAgeDao extends INeedPersistenceSupport
{
    ErrorCollector validate(PensionQualificationAgeSettings entity);
    void saveOrUpdate(PensionQualificationAgeSettings entity);
}
