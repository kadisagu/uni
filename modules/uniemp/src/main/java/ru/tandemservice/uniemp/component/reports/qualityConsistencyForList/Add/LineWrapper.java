/**
 *$Id:$
 */
package ru.tandemservice.uniemp.component.reports.qualityConsistencyForList.Add;

import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.person.catalog.entity.ScienceDegree;
import org.tandemframework.shared.person.catalog.entity.ScienceStatus;

import java.util.Set;

/**
 * @author Alexander Shaburov
 * @since 10.10.12
 */
public class LineWrapper
{
    public LineWrapper(PostBoundedWithQGandQL post, String fio, Set<ScienceDegree> scienceDegreeList, ScienceStatus scienceStatus)
    {
        _post = post;
        _fio = fio;
        _scienceDegreeList = scienceDegreeList;
        _scienceStatus = scienceStatus;
    }

    private PostBoundedWithQGandQL _post;
    private String _fio;
    private Set<ScienceDegree> _scienceDegreeList;
    private ScienceStatus _scienceStatus;

    // Getters & Setters

    public PostBoundedWithQGandQL getPost()
    {
        return _post;
    }

    public void setPost(PostBoundedWithQGandQL post)
    {
        _post = post;
    }

    public String getFio()
    {
        return _fio;
    }

    public void setFio(String fio)
    {
        _fio = fio;
    }

    public Set<ScienceDegree> getScienceDegreeList()
    {
        return _scienceDegreeList;
    }

    public void setScienceDegreeList(Set<ScienceDegree> scienceDegreeList)
    {
        _scienceDegreeList = scienceDegreeList;
    }

    public ScienceStatus getScienceStatus()
    {
        return _scienceStatus;
    }

    public void setScienceStatus(ScienceStatus scienceStatus)
    {
        _scienceStatus = scienceStatus;
    }
}
