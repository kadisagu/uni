/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.EmploymentHistoryExtAddEdit;

import org.tandemframework.core.component.Input;

import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import ru.tandemservice.uniemp.component.employee.EmploymentHistoryAbsAddEdit.EmploymentHistoryAbstractModel;
import ru.tandemservice.uniemp.entity.catalog.ServiceLengthType;
import ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemBase;
import ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemExt;

import java.util.List;

/**
 * @author dseleznev
 * Created on: 23.12.2008
 */
@Input(keys = { "employeeId", "rowId" }, bindings = { "employeeId", "employmentHistoryItem.id" })
public class Model extends EmploymentHistoryAbstractModel
{
    private EmploymentHistoryItemExt _employmentHistoryItem = new EmploymentHistoryItemExt();

    private IMultiSelectModel _serviceLengthTypeModel;
    private List<ServiceLengthType> _serviceLengthTypeList;

    // Getters & Setters

    public IMultiSelectModel getServiceLengthTypeModel()
    {
        return _serviceLengthTypeModel;
    }

    public void setServiceLengthTypeModel(IMultiSelectModel serviceLengthTypeModel)
    {
        _serviceLengthTypeModel = serviceLengthTypeModel;
    }

    public List<ServiceLengthType> getServiceLengthTypeList()
    {
        return _serviceLengthTypeList;
    }

    public void setServiceLengthTypeList(List<ServiceLengthType> serviceLengthTypeList)
    {
        _serviceLengthTypeList = serviceLengthTypeList;
    }

    @Override
    public EmploymentHistoryItemExt getEmploymentHistoryItem()
    {
        return _employmentHistoryItem;
    }

    @Override
    public void setEmploymentHistoryItem(EmploymentHistoryItemBase employmentHistoryItem)
    {
        this._employmentHistoryItem = (EmploymentHistoryItemExt) employmentHistoryItem;
    }

}