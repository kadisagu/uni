/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.qualityQuantityConsistencyFull.Add;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.Post;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeeTypeCodes;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.OrgUnitTypeRestriction;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.ScienceDegreeType;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.IUniempDAO;
import ru.tandemservice.uniemp.entity.catalog.EmployeeTemplateDocument;
import ru.tandemservice.uniemp.util.UniempReportUtil;

import java.util.*;

/**
 * @author dseleznev
 *         Created on: 17.04.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final int FACULTY_FOOTER = 1;
    private static final int TOTAL_FOOTER = 2;

    @Override
    public Integer preparePrintReport(Model model)
    {
        RtfInjectModifier paramModifier = new RtfInjectModifier();
        paramModifier.put("reportDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getReportDate()));

        RtfTableModifier tableModifier = new RtfTableModifier();
        final Map<Integer, Integer> stylesMap = new HashMap<>();
        tableModifier.getName2data().putAll(prepareTablesData(model.getReportDate(), stylesMap));
        tableModifier.put("T", new RtfRowIntercepterBase()
        {
            // Format rows according to template
            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                int cnt = -startIndex;
                for (RtfRow row : newRowList)
                {
                    processRtfRow(row, stylesMap, cnt);
                    cnt++;
                }
            }
        });

        return UniempReportUtil.preparePrintingReport(getCatalogItem(EmployeeTemplateDocument.class, UniempDefines.TEMPLATE_QUALITY_QUANTITY_CONSISTENCY_FULL), paramModifier, tableModifier);
    }

    private void processRtfRow(RtfRow row, Map<Integer, Integer> stylesMap, Integer cnt)
    {
        if (stylesMap.containsKey(cnt))
        {
            if (TOTAL_FOOTER == stylesMap.get(cnt))
            {
                row.getCellList().get(0).getElementList().add(0, RtfBean.getElementFactory().createRtfControl(IRtfData.I));
                IRtfControl contrI = RtfBean.getElementFactory().createRtfControl(IRtfData.I, 0);
//                contrI.setValue(0);
                row.getCellList().get(row.getCellList().size() - 1).getElementList().add(contrI);
            }

            // Устанавливаем жирным любую строчку, которая имеет стиль
            row.getCellList().get(0).getElementList().add(0, RtfBean.getElementFactory().createRtfControl(IRtfData.B));
            IRtfControl contrB = RtfBean.getElementFactory().createRtfControl(IRtfData.B, 0);
//            contrB.setValue(0);
            row.getCellList().get(row.getCellList().size() - 1).getElementList().add(contrB);
        }
    }


    private Map<String, String[][]> prepareTablesData(Date reportDate, Map<Integer, Integer> stylesMap)
    {
        Map<String, String[][]> result = new HashMap<>();

        List<String[]> lines = new ArrayList<>();
        List<OrgUnit> parentOrgUnitsList = getParentOrgUnits();
        Map<OrgUnit, List<OrgUnit>> orgUnitsMap = getOrgUnitsListsMap(parentOrgUnitsList);
        Map<Long, OrgUnitPPSStatistics> cathedraStatistics = prepareCathedraStatistics(getEmployeePostsList(reportDate), reportDate);

        Integer lineNumber = 0;
        DoubleFormatter formatter = DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS;
        OrgUnitPPSStatistics fullStat = new OrgUnitPPSStatistics(null, reportDate);

        for (OrgUnit parentOrgUnit : parentOrgUnitsList)
        {
            OrgUnitPPSStatistics facultyStat = new OrgUnitPPSStatistics(parentOrgUnit, reportDate);
            if (orgUnitsMap.containsKey(parentOrgUnit))
            {
                for (OrgUnit orgUnit : orgUnitsMap.get(parentOrgUnit))
                {
                    OrgUnitPPSStatistics stat = cathedraStatistics.get(orgUnit.getId());
                    if (null == stat) stat = new OrgUnitPPSStatistics(orgUnit, reportDate);
                    lines.add(getSinglePostLine(stat, parentOrgUnit.getFullTitle(), orgUnit.getTitle(), formatter));
                    facultyStat.addValues(stat);
                    lineNumber++;
                }
            }

            lines.add(getSinglePostLine(facultyStat, parentOrgUnit.getTitle(), "По " + parentOrgUnit.getOrgUnitType().getDative(), formatter));
            stylesMap.put(lineNumber++, FACULTY_FOOTER);
            fullStat.addValues(facultyStat);
        }

        lines.add(getSinglePostLine(fullStat, "", "По ОУ", formatter));
        stylesMap.put(lineNumber, TOTAL_FOOTER);

        result.put("T", lines.toArray(new String[][]{}));

        return result;
    }

    private String[] getSinglePostLine(OrgUnitPPSStatistics stat, String title, String secondTitle, DoubleFormatter formatter)
    {
        String[] line = new String[16];
        line[0] = title;
        line[1] = secondTitle;
        line[2] = stat.getPpsMainJobAmount() > 0 ? String.valueOf(stat.getPpsMainJobAmount()) : "-";
        line[3] = stat.getPpsSecondJobAmount() > 0 ? String.valueOf(stat.getPpsSecondJobAmount()) : "-";
        line[4] = stat.getPpsAmount() > 0 ? String.valueOf(stat.getPpsAmount()) : "-";
        line[5] = stat.getMasterMainJobAmount() > 0 ? String.valueOf(stat.getMasterMainJobAmount()) : "-";
        line[6] = stat.getMasterSecondJobAmount() > 0 ? String.valueOf(stat.getMasterSecondJobAmount()) : "-";
        line[7] = stat.getMasterAmount() > 0 ? String.valueOf(stat.getMasterAmount()) : "-";
        line[8] = stat.getDoctorMainJobAmount() > 0 ? String.valueOf(stat.getDoctorMainJobAmount()) : "-";
        line[9] = stat.getDoctorSecondJobAmount() > 0 ? String.valueOf(stat.getDoctorSecondJobAmount()) : "-";
        line[10] = stat.getDoctorAmount() > 0 ? String.valueOf(stat.getDoctorAmount()) : "-";
        line[11] = stat.getDegreesPercent() > 0 ? formatter.format(stat.getDegreesPercent()) : "-";
        line[12] = stat.getDegreesMainPercent() > 0 ? formatter.format(stat.getDegreesMainPercent()) : "-";
        line[13] = stat.getDoctorsPercent() > 0 ? formatter.format(stat.getDoctorsPercent()) : "-";
        line[14] = stat.getDoctorsMainPercent() > 0 ? formatter.format(stat.getDoctorsMainPercent()) : "-";
        line[15] = stat.getAverageAge() > 0 ? formatter.format(stat.getAverageAge()) : "-";
        return line;
    }

    /**
     * Возвращает список подразделений, включающих в себя кафедры
     */
    private List<OrgUnit> getParentOrgUnits()
    {
        MQBuilder sub = new MQBuilder(OrgUnitTypeRestriction.ENTITY_CLASS, "outr", new String[]{OrgUnitTypeRestriction.L_PARENT + "." + OrgUnitType.P_ID});
        sub.add(MQExpression.eq("outr", OrgUnitTypeRestriction.L_CHILD + "." + OrgUnitType.P_CODE, OrgUnitTypeCodes.CATHEDRA));

        MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "ou");
        builder.add(MQExpression.in("ou", OrgUnit.L_ORG_UNIT_TYPE + "." + OrgUnitType.P_ID, sub));
        builder.addOrder("ou", OrgUnit.P_TITLE);

        return builder.getResultList(getSession());
    }

    /**
     * Возвращает Map списков кафедр для каждого подразделения
     */
    private Map<OrgUnit, List<OrgUnit>> getOrgUnitsListsMap(List<OrgUnit> orgUnitsList)
    {
        MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "ou");
        builder.add(MQExpression.in("ou", OrgUnit.L_PARENT, orgUnitsList));
        builder.add(MQExpression.eq("ou", OrgUnit.L_ORG_UNIT_TYPE + "." + OrgUnitType.P_CODE, OrgUnitTypeCodes.CATHEDRA));
        builder.addOrder("ou", OrgUnit.P_TITLE);
        List<OrgUnit> resultList = builder.getResultList(getSession());

        Map<OrgUnit, List<OrgUnit>> result = new HashMap<>();
        for (OrgUnit unit : resultList)
        {
            List<OrgUnit> childsList = result.get(unit.getParent());
            if (null == childsList) childsList = new ArrayList<>();
            childsList.add(unit);
            result.put(unit.getParent(), childsList);
        }
        return result;
    }

    /**
     * Возвращает список сотрудников ППС, приписанных к кафедрам
     */
    private List<EmployeePost> getEmployeePostsList(Date reportDate)
    {
        MQBuilder builder = new MQBuilder(EmployeePost.ENTITY_CLASS, "ep");
        builder.add(MQExpression.in("ep", EmployeePost.L_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_POST + "." + Post.L_EMPLOYEE_TYPE, EmployeeManager.instance().dao().getEmployeeTypeWithAllChilds(getCatalogItem(EmployeeType.class, EmployeeTypeCodes.EDU_STAFF))));
        builder.add(MQExpression.lessOrEq("ep", EmployeePost.P_POST_DATE, reportDate));
        AbstractExpression expr1 = MQExpression.isNull("ep", EmployeePost.P_DISMISSAL_DATE);
        AbstractExpression expr2 = MQExpression.greatOrEq("ep", EmployeePost.P_DISMISSAL_DATE, reportDate);
        builder.add(MQExpression.or(expr1, expr2));
        builder.add(MQExpression.eq("ep", EmployeePost.L_ORG_UNIT + "." + OrgUnit.L_ORG_UNIT_TYPE + "." + OrgUnitType.P_CODE, OrgUnitTypeCodes.CATHEDRA));
        builder.addOrder("ep", EmployeePost.L_ORG_UNIT + "." + OrgUnit.P_TITLE);
        builder.addOrder("ep", EmployeePost.L_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_POST + "." + Post.P_RANK);
        builder.addOrder("ep", EmployeePost.L_EMPLOYEE + "." + Employee.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_LAST_NAME);
        builder.addOrder("ep", EmployeePost.L_EMPLOYEE + "." + Employee.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_FIRST_NAME);
        return builder.getResultList(getSession());
    }

    /**
     * Подготавливает статистику по кафедрам
     */
    private Map<Long, OrgUnitPPSStatistics> prepareCathedraStatistics(List<EmployeePost> employeePostsList, Date reportDate)
    {
        Map<Long, OrgUnitPPSStatistics> result = new HashMap<>();
        Map<Person, ScienceDegreeType> employeeDegreeTypesMap = IUniempDAO.instance.get().getEmployeeDegreesMap(reportDate);

        for (EmployeePost post : employeePostsList)
        {
            OrgUnit orgUnit = post.getOrgUnit();
            OrgUnitPPSStatistics stat = result.get(orgUnit.getId());
            if (null == stat) stat = new OrgUnitPPSStatistics(orgUnit, reportDate);
            ScienceDegreeType degreeType = employeeDegreeTypesMap.get(post.getEmployee().getPerson());

            if (null != post.getPerson().getIdentityCard().getBirthDate())
                stat.addEmployeeBirthDate(post.getPerson().getIdentityCard().getBirthDate());

            if (UniDefines.POST_TYPE_SECOND_JOB.equals(post.getPostType().getCode()) || UniDefines.POST_TYPE_SECOND_JOB_INNER.equals(post.getPostType().getCode()) || UniDefines.POST_TYPE_SECOND_JOB_OUTER.equals(post.getPostType().getCode()))
            {
                stat.increasePpsSecondJobAmount();
                if (null != degreeType)
                {
                    if (UniDefines.SCIENCE_DEGREE_TYPE_DOCTOR.equals(degreeType.getCode()))
                        stat.increaseDoctorSecondJobAmount();
                    else
                        stat.increaseMasterSecondJobAmount();
                }
            } else
            {
                stat.increasePpsMainJobAmount();
                if (null != degreeType)
                {
                    if (UniDefines.SCIENCE_DEGREE_TYPE_DOCTOR.equals(degreeType.getCode()))
                        stat.increaseDoctorMainJobAmount();
                    else
                        stat.increaseMasterMainJobAmount();
                }
            }
            result.put(orgUnit.getId(), stat);
        }
        return result;
    }
}