/**
 *$Id$
 */
package ru.tandemservice.uniemp.component.employee.employmentContracts.EmploymentContractsList;

import ru.tandemservice.uniemp.dao.IUniempDAO;

/**
 * Create by ashaburov
 * Date 27.07.11
 */
public interface IDAO extends IUniempDAO<Model>
{
    @Override
    public void prepareListDataSource(final Model model);
}
