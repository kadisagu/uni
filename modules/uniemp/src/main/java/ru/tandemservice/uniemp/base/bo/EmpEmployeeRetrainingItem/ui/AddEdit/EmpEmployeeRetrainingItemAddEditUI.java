/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.bo.EmpEmployeeRetrainingItem.ui.AddEdit;

import org.apache.commons.io.IOUtils;
import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.component.catalog.CommonBaseCatalogDefine;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.person.catalog.entity.DiplomaQualifications;
import org.tandemframework.shared.person.catalog.entity.EduInstitution;
import org.tandemframework.shared.person.catalog.entity.EducationalInstitutionTypeKind;
import org.tandemframework.shared.person.catalog.entity.EmployeeSpeciality;
import ru.tandemservice.uniemp.base.bo.EmpEmployeeRetrainingItem.EmpEmployeeRetrainingItemManager;
import ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem;

import java.io.IOException;

/**
 * Create by ashaburov
 * Date 28.03.12
 */
@Input({
    @Bind(key = EmpEmployeeRetrainingItemAddEditUI.EMPLOYEE_ID, binding = EmpEmployeeRetrainingItemAddEditUI.EMPLOYEE_ID),
    @Bind(key = EmpEmployeeRetrainingItemAddEditUI.ITEM_ID, binding = EmpEmployeeRetrainingItemAddEditUI.ITEM_ID),
    @Bind(key = EmpEmployeeRetrainingItemAddEditUI.CREATE_EDU_INSTITUTION_ID, binding = EmpEmployeeRetrainingItemAddEditUI.CREATE_EDU_INSTITUTION_ID),
    @Bind(key = EmpEmployeeRetrainingItemAddEditUI.CREATE_CATALOG_ITEM_ID, binding = EmpEmployeeRetrainingItemAddEditUI.CREATE_CATALOG_ITEM_ID)

})
public class EmpEmployeeRetrainingItemAddEditUI extends UIPresenter
{
    //input fields name
    public static final String EMPLOYEE_ID = "employeeId";
    public static final String ITEM_ID = "itemId";
    public static final String CREATE_CATALOG_ITEM_ID = "createdCatalogItemId";
    public static final String CREATE_EDU_INSTITUTION_ID = "createdEduInstitutionId";

    //ds param names
    public static final String SELECTED_EDU_INSTITUTION_TYPE = "eduInstitutionParent";
    public static final String SELECTED_COUNTRY = "country";
    public static final String SELECTED_ADDRESS_ITEM = "addressItem";
    public static final String SELECTED_EDU_INSTITUTION_KIND = "eduInstitutionKind";

    private boolean _addForm;
    private boolean _notRefreshComponent;

    private boolean _fileUploadBlockVisible;
    private IUploadFile _eduDocumentFile;
    private boolean _delEduDocumentFile;

    private Long _employeeId;
    private Long _itemId;
    private Long _createdEduInstitutionId;
    private Long _createdCatalogItemId;

    private Employee _employee;
    private EmployeeRetrainingItem _item;

    private AddressCountry _country;
    private EducationalInstitutionTypeKind _eduInstitutionParent;

    //Presenter methods

    @Override
    public void onComponentRefresh()
    {
        //если заполненно поле id Образовательного учреждения или Ного элемента справочника, значит была открыта форма добавления нового эолемента справочника и id добавленного элемента вернулся
        //нужно просетить этот новый элемент в соответствующее поле и выйти из метода, что бы не затереть значения других полей
        if (getCreatedEduInstitutionId() != null)
        {
            getItem().setEduInstitution(DataAccessServices.dao().getNotNull(EduInstitution.class, getCreatedEduInstitutionId()));
            return;
        }
        else if (getCreatedCatalogItemId() != null)
        {
            IEntity catalogItem = DataAccessServices.dao().getNotNull(getCreatedCatalogItemId());

            if (catalogItem instanceof EmployeeSpeciality)
                getItem().setEmployeeSpeciality((EmployeeSpeciality) catalogItem);
            else if (catalogItem instanceof DiplomaQualifications)
                getItem().setDiplomaQualifications((DiplomaQualifications) catalogItem);

            return;
        }
        //если id элементов не пришли, но стоит метка isNotRefreshComponent, значит все ровно бала открыта форма добавления нового элемента, но была нажата Отмена
        //нужно выйти из метода, что бы не обнулить другие поля и убрать метку isNotRefreshComponent
        else if (isNotRefreshComponent())
        {
            setNotRefreshComponent(false);
            return;
        }


        //заполняем КР
        setEmployee(DataAccessServices.dao().getNotNull(Employee.class, getEmployeeId()));

        //заполняем элемент данных ПК
        if (getItemId() == null)
            setItem(new EmployeeRetrainingItem());
        else
        {
            setItem(DataAccessServices.dao().getNotNull(EmployeeRetrainingItem.class, getItemId()));
            setCountry(getItem().getAddressItem().getCountry());
            setEduInstitutionParent(getItem().getEduInstitutionKind().getParent());
        }

        //выставляем признак добавлене\редактирование
        if (getItemId() == null)
            setAddForm(true);
        else
            setAddForm(false);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
		switch (dataSource.getName())
		{
			case EmpEmployeeRetrainingItemAddEdit.EDU_INSTITUTION_KIND_DS:
				dataSource.put(SELECTED_EDU_INSTITUTION_TYPE, getEduInstitutionParent());
				break;
			case EmpEmployeeRetrainingItemAddEdit.SETTLEMENT_DS:
				dataSource.put(AddressItem.PARAM_COUNTRY, getCountry());
				break;
			case EmpEmployeeRetrainingItemAddEdit.EDU_INSTITUTION_DS:
				dataSource.put(SELECTED_COUNTRY, getCountry());
				dataSource.put(SELECTED_ADDRESS_ITEM, getItem().getAddressItem());
				dataSource.put(SELECTED_EDU_INSTITUTION_TYPE, getEduInstitutionParent());
				dataSource.put(SELECTED_EDU_INSTITUTION_KIND, getItem().getEduInstitutionKind());
				break;
		}
    }

    protected ErrorCollector validate()
    {
        ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();

        if (getItem().getStartDate().getTime() > getItem().getFinishDate().getTime())
            errorCollector.add("Дата начала должна быть не больше даты окончания.", "startDate", "finishDate");

        if (getItem().getEmployeeSpeciality() == null && getItem().getDiplomaQualifications() == null)
            errorCollector.add("Обязательно для заполнения одно из полей «Специальность» или «Квалификация».", "speciality", "qualifications");

        return errorCollector;
    }

    //Listeners

    public void onClickAddEduInstitution()
    {
        setNotRefreshComponent(true);

        _uiActivation.asRegionDialog(CommonManager.catalogComponentProvider().getAddEditComponent(EduInstitution.class))
                .parameter(DefaultCatalogAddEditModel.CATALOG_CODE, EduInstitution.ENTITY_NAME)
                .parameter(org.tandemframework.shared.person.component.catalog.eduInstitution.EduInstitutionAddEdit.Model.EXISTANCE_CHECKING_TYPE_PARAM_NAME, org.tandemframework.shared.person.component.catalog.eduInstitution.EduInstitutionAddEdit.Model.RETURN_EXISTING)
                .parameter("personRoleName", Employee.class.getName())
                .parameter("countryId", getCountry() != null ? getCountry().getId() : null)
                .parameter("addressItemId", getItem().getAddressItem() != null ? getItem().getAddressItem().getId() : null)
                .parameter("eduInstitutionTypeId", getItem().getEduInstitutionKind() != null ? getItem().getEduInstitutionKind().getId() : null)
                .activate();
    }

    public void onClickAddEmployeeSpeciality()
    {
        setNotRefreshComponent(true);

        _uiActivation.asRegionDialog(CommonBaseCatalogDefine.DEFAULT_CATALOG_ADD_EDIT)
                .parameter(DefaultCatalogAddEditModel.CATALOG_CODE, EmployeeSpeciality.ENTITY_NAME)
                .activate();
    }

    public void onClickAddDiplomaQualifications()
    {
        setNotRefreshComponent(true);

        _uiActivation.asRegionDialog(CommonBaseCatalogDefine.DEFAULT_CATALOG_ADD_EDIT)
                .parameter(DefaultCatalogAddEditModel.CATALOG_CODE, DiplomaQualifications.ENTITY_NAME)
                .activate();
    }

    public void onClickAddDocument()
    {
        setFileUploadBlockVisible(true);
    }

    public void onClickDelDocument()
    {
        setFileUploadBlockVisible(false);

        setDelEduDocumentFile(true);
    }

    public void onClickApply()
    {
        //деаем проверки, если они есть то выходим
        if (validate().hasErrors())
            return;

        if (getItem().getEmployee() == null)
            getItem().setEmployee(getEmployee());

        //если стоит метка на удаление файла, то удаляем его и зануляем в соответствующем поле объекта
        if (isDelEduDocumentFile())
        {
            DatabaseFile eduDocumentFile = getItem().getEduDocumentFile();
            getItem().setEduDocumentFile(null);
            DataAccessServices.dao().delete(eduDocumentFile);
        }

        //если указан файл и отображалась форма добавления файла, то создаем DatabaseFile и сохраняем ссылку на него в соответствующем поле объекта
        if (getEduDocumentFile() != null && isFileUploadBlockVisible())
        {
            try
            {
                byte[] content = IOUtils.toByteArray(getEduDocumentFile().getStream());

                if ((content != null) && (content.length > 0))
                {
                    if (getItem().getEduDocumentFile() == null)
                        getItem().setEduDocumentFile(new DatabaseFile());
                    getItem().getEduDocumentFile().setContent(content);
                    getItem().setEduDocumentFileName(getEduDocumentFile().getFileName());
                    getItem().setEduDocumentFileType(CommonBaseUtil.getContentType(getEduDocumentFile()));
                    DataAccessServices.dao().save(getItem().getEduDocumentFile());
                }
            }
            catch (IOException e)
            {
                throw new RuntimeException(e.getMessage(), e);
            }
        }

        EmpEmployeeRetrainingItemManager.instance().dao().saveOrUpdateItem(getItem());

        deactivate();
    }

    //Getters & Setters

    public boolean isNotRefreshComponent()
    {
        return _notRefreshComponent;
    }

    public void setNotRefreshComponent(boolean notRefreshComponent)
    {
        _notRefreshComponent = notRefreshComponent;
    }

    public Long getCreatedCatalogItemId()
    {
        return _createdCatalogItemId;
    }

    public void setCreatedCatalogItemId(Long createdCatalogItemId)
    {
        _createdCatalogItemId = createdCatalogItemId;
    }

    public boolean isAddForm()
    {
        return _addForm;
    }

    public void setAddForm(boolean addForm)
    {
        _addForm = addForm;
    }

    public boolean isFileUploadBlockVisible()
    {
        return _fileUploadBlockVisible;
    }

    public void setFileUploadBlockVisible(boolean fileUploadBlockVisible)
    {
        _fileUploadBlockVisible = fileUploadBlockVisible;
    }

    public IUploadFile getEduDocumentFile()
    {
        return _eduDocumentFile;
    }

    public void setEduDocumentFile(IUploadFile eduDocumentFile)
    {
        _eduDocumentFile = eduDocumentFile;
    }

    public boolean isDelEduDocumentFile()
    {
        return _delEduDocumentFile;
    }

    public void setDelEduDocumentFile(boolean delEduDocumentFile)
    {
        _delEduDocumentFile = delEduDocumentFile;
    }

    public Long getEmployeeId()
    {
        return _employeeId;
    }

    public void setEmployeeId(Long employeeId)
    {
        _employeeId = employeeId;
    }

    public Long getItemId()
    {
        return _itemId;
    }

    public void setItemId(Long itemId)
    {
        _itemId = itemId;
    }

    public Long getCreatedEduInstitutionId()
    {
        return _createdEduInstitutionId;
    }

    public void setCreatedEduInstitutionId(Long createdEduInstitutionId)
    {
        _createdEduInstitutionId = createdEduInstitutionId;
    }

    public Employee getEmployee()
    {
        return _employee;
    }

    public void setEmployee(Employee employee)
    {
        _employee = employee;
    }

    public EmployeeRetrainingItem getItem()
    {
        return _item;
    }

    public void setItem(EmployeeRetrainingItem item)
    {
        _item = item;
    }

    public AddressCountry getCountry()
    {
        return _country;
    }

    public void setCountry(AddressCountry country)
    {
        _country = country;
    }

    public EducationalInstitutionTypeKind getEduInstitutionParent()
    {
        return _eduInstitutionParent;
    }

    public void setEduInstitutionParent(EducationalInstitutionTypeKind eduInstitutionParent)
    {
        _eduInstitutionParent = eduInstitutionParent;
    }
}
