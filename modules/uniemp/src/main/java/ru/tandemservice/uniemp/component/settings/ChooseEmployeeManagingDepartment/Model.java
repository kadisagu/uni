/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.component.settings.ChooseEmployeeManagingDepartment;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

/**
 * @author alikhanov
 * @since 14.09.2010
 */
public class Model
{
    private OrgUnit _selectedOrgUnit;
    private ISelectModel _orgUnitsModel;

    public OrgUnit getSelectedOrgUnit()
    {
        return _selectedOrgUnit;
    }

    public void setSelectedOrgUnit(OrgUnit selectedOrgUnit)
    {
        _selectedOrgUnit = selectedOrgUnit;
    }

    public ISelectModel getOrgUnitsModel()
    {
        return _orgUnitsModel;
    }

    public void setOrgUnitsModel(ISelectModel orgUnitsModel)
    {
        _orgUnitsModel = orgUnitsModel;
    }
}
