/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniemp.base.ext.Employee.ui.PostAddEdit;

import org.apache.hivemind.Location;
import org.apache.hivemind.Resource;
import org.apache.tapestry.IAsset;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Vasily Zhukov
 * @since 02.04.2012
 */
public class EmployeePostAddEditLegacyModel
{
    private List<StaffListAllocationItem> selfAllocItemList = new ArrayList<>();

    private List<EmployeePostStaffRateItem> _oldStaffRateItemList = new ArrayList<>();

    private List<Double> _oldStaffRateList = new ArrayList<>();

    private boolean _thereAnyActiveStaffList = false;
    private DynamicListDataSource<EmployeePostStaffRateItem> _staffRateDataSource;
    private List<FinancingSource> _financingSourceModel;
    private ISelectModel _financingSourceItemModel;
    private IMultiSelectModel _employeeHRModel;
    private List<EmployeePostStaffRateItem> _staffRateItemList = new ArrayList<>();
    private IAsset _asset = new IAsset()
    {
        @Override
        public String buildURL()
        {
            return "img/general/add.png";
        }

        @Override
        public InputStream getResourceAsStream()
        {
            return null;  //Body of implemented method
        }

        @Override
        public Resource getResourceLocation()
        {
            return null;  //Body of implemented method
        }

        @Override
        public Location getLocation()
        {
            return null;  //Body of implemented method
        }
    };

    //Getters & Setters

    public ISelectModel getFinancingSourceItemModel()
    {
        return _financingSourceItemModel;
    }

    public void setFinancingSourceItemModel(ISelectModel financingSourceItemModel)
    {
        _financingSourceItemModel = financingSourceItemModel;
    }

    public IMultiSelectModel getEmployeeHRModel()
    {
        return _employeeHRModel;
    }

    public void setEmployeeHRModel(IMultiSelectModel employeeHRModel)
    {
        _employeeHRModel = employeeHRModel;
    }

    public List<Double> getOldStaffRateList()
    {
        return _oldStaffRateList;
    }

    public void setOldStaffRateList(List<Double> oldStaffRateList)
    {
        _oldStaffRateList = oldStaffRateList;
    }

    public IAsset getAsset()
    {
        return _asset;
    }

    public void setAsset(IAsset asset)
    {
        _asset = asset;
    }

    public String getStaffRateColumnId()
    {
        return "staffRate_id_" + getStaffRateDataSource().getCurrentEntity().getId();
    }

    public String getFinSrcColumnId()
    {
        return "finSrc_id_" + getStaffRateDataSource().getCurrentEntity().getId();
    }

    public String getFinSrcItmColumnId()
    {
        return "finSrcItm_id_" + getStaffRateDataSource().getCurrentEntity().getId();
    }

    public String getEmployeeHRId()
    {
        return "employeeHR_Id_" + getStaffRateDataSource().getCurrentEntity().getId();
    }

    public List<StaffListAllocationItem> getSelfAllocItemList()
    {
        return selfAllocItemList;
    }

    public List<EmployeePostStaffRateItem> getOldStaffRateItemList()
    {
        return _oldStaffRateItemList;
    }

    public void setOldStaffRateItemList(List<EmployeePostStaffRateItem> oldStaffRateItemList)
    {
        _oldStaffRateItemList = oldStaffRateItemList;
    }

    public void setSelfAllocItemList(List<StaffListAllocationItem> selfAllocItemList)
    {
        this.selfAllocItemList = selfAllocItemList;
    }

    public List<EmployeePostStaffRateItem> getStaffRateItemList()
    {
        return _staffRateItemList;
    }

    public void setStaffRateItemList(List<EmployeePostStaffRateItem> staffRateItemList)
    {
        _staffRateItemList = staffRateItemList;
    }

    public List<FinancingSource> getFinancingSourceModel()
    {
        return _financingSourceModel;
    }

    public void setFinancingSourceModel(List<FinancingSource> financingSourceModel)
    {
        _financingSourceModel = financingSourceModel;
    }

    public DynamicListDataSource<EmployeePostStaffRateItem> getStaffRateDataSource()
    {
        return _staffRateDataSource;
    }

    public void setStaffRateDataSource(DynamicListDataSource<EmployeePostStaffRateItem> staffRateDataSource)
    {
        _staffRateDataSource = staffRateDataSource;
    }

    public boolean isThereAnyActiveStaffList()
    {
        return _thereAnyActiveStaffList;
    }

    public void setThereAnyActiveStaffList(boolean thereAnyActiveStaffList)
    {
        this._thereAnyActiveStaffList = thereAnyActiveStaffList;
    }
}
