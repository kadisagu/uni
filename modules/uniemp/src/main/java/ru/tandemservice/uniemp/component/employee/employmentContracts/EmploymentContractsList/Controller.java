/**
 *$Id$
 */
package ru.tandemservice.uniemp.component.employee.employmentContracts.EmploymentContractsList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import ru.tandemservice.uniemp.IUniempComponents;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Create by ashaburov
 * Date 27.07.11
 */
public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<EmployeeLabourContract> dataSource = new DynamicListDataSource<>(component, this);

        dataSource.addColumn(new CheckboxColumn("check"));
        dataSource.addColumn(new PublisherLinkColumn("ФИО", EmployeeLabourContract.PERSON_FIO_KEY)
                .setResolver(new SimplePublisherLinkResolver(EmployeeLabourContract.employeePost().id())
                    .param("selectedTab", "employeePostTab").param("selectedDataTab", "Contract")));
        dataSource.addColumn(new SimpleColumn("Должность", EmployeeLabourContract.POST_TITLE_KEY).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Подразделение", EmployeeLabourContract.ORGUNIT_TITLE_KEY).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Тип назначения", EmployeeLabourContract.POST_TYPE_TITLE_KEY).setClickable(false));

        HeadColumn headColumn = new HeadColumn("contract", "Трудовой договор");
        headColumn.setHeaderAlign("center");
        headColumn.addColumn(new SimpleColumn("Номер", EmployeeLabourContract.NUMBER_KEY).setClickable(false));
        headColumn.addColumn(new SimpleColumn("Дата заключения", EmployeeLabourContract.DATE_KEY, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        headColumn.addColumn(new SimpleColumn("Дата начала", EmployeeLabourContract.BEGIN_DATE_KEY, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        headColumn.addColumn(new SimpleColumn("Дата окончания", EmployeeLabourContract.END_DATE_KEY, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        headColumn.addColumn(new SimpleColumn("Дата приема на работу", EmployeeLabourContract.EMPLOYMENT_DATE_KEY, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        headColumn.addColumn(new SimpleColumn("Тип", EmployeeLabourContract.TYPE_KEY).setClickable(false));

        dataSource.addColumn(headColumn);
        dataSource.addColumn(new ActionColumn("Печать уведомления", "printer", "onClickPrint").setDisabledProperty(Model.DISABLED_PRINT_PROPERTY).setPermissionKey("employmentContractsTerminationNoticePrint"));

        model.setDataSource(dataSource);
    }


    @Override
    public void updateListDataSource(IBusinessComponent component)
    {
        getDao().prepareListDataSource(getModel(component));
    }

    public void onChangeDeadlineFilter(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.setDisableEndDateFilter(model.getDeadlineItemFilter() != null);
    }

    public void onClickSearch(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.getDataSource().showFirstPage();
        model.getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.setDisableEndDateFilter(false);
        model.setContractNumberFilter(null);
        model.setContractTypeItemFilter(null);
        model.setDateFromFilter(null);
        model.setDateToFilter(null);
        model.setDeadlineItemFilter(null);
        model.setDecompositionOrgUnitFilter(false);
        model.setEndDateFromFilter(null);
        model.setEndDateToFilter(null);
        model.setFirstNameFilter(null);
        model.setMiddleNameFilter(null);
        model.setLastNameFilter(null);
        model.setOrgUnitItemFilter(null);
        model.setPostRelationItemFilter(null);
        model.setPostTypeItemFilter(null);

        onClickSearch(component);
    }

    public void onClickPrint(IBusinessComponent component)
    {
        getModel(component);
        Long contractId = component.getListenerParameter();

        activateInRoot(component, IUniempComponents.EMPLOYMENT_CONTRACTS_TERMINATION_NOTICE_PRINT_FORM, new ParametersMap().
                add("contractId", contractId).
                add("contractIdList", null).
                add("massPrint", false));
    }

    public void onClickMassPrint(IBusinessComponent component)
    {
        Model model = getModel(component);
        Collection<IEntity> entityCollection = ((CheckboxColumn) model.getDataSource().getColumn(0)).getSelectedObjects();
        ErrorCollector errorCollector = component.getUserContext().getErrorCollector();

        if (entityCollection.isEmpty())
            errorCollector.add("Необходимо выбрать хотя бы одного сотрудника.");

        if (errorCollector.hasErrors())
            return;

        List<String> invalidEmployeeFioList = entityCollection.stream()
            .filter(entity -> entity.getProperty(EmployeeLabourContract.P_END_DATE) == null)
            .map(entity -> (String)entity.getProperty(EmployeeLabourContract.employeePost().person().fio()))
            .collect(Collectors.toList());

        //если у каких то выбранных ТД нет даты окончания, то подготавливаем текст ошибки и выводим ее
        if (invalidEmployeeFioList.size() != 0)
        {
            String noticeStr = (invalidEmployeeFioList.size() == 1) ? "уведомления" : "уведомлений";
            String employeeStr = (invalidEmployeeFioList.size() == 1) ? "сотрудника" : "сотрудников";
            String fioList = String.join(", ", invalidEmployeeFioList);
            errorCollector.add("Печать " + noticeStr + " " + employeeStr + " " + fioList + " невозможна ввиду отсутствия даты окончания ТД.");
        }

        if (errorCollector.hasErrors())
            return;

        List<Long> contractIdList = entityCollection.stream().map(IEntity::getId).collect(Collectors.toList());

        activate(component, new ComponentActivator(IUniempComponents.EMPLOYMENT_CONTRACTS_TERMINATION_NOTICE_PRINT_FORM, new ParametersMap().
                add("contractId", null).
                add("contractIdList", contractIdList).
                add("massPrint", true)));
    }
}

