/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniemp.component.settings.CompensPaymentsList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.HeadColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import org.tandemframework.shared.commonbase.base.util.CommonBaseDefine;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.catalog.RosobrReports;

/**
 * @author dseleznev
 * Created on: 25.01.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
        prepareListDataSource(component);
    }
    
    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<Payment> dataSource = UniBaseUtils.createDataSource(component, getDao());

        dataSource.addColumn(new SimpleColumn("Название", Payment.P_TITLE).setClickable(false).setOrderable(false));
        
        HeadColumn head1 = new HeadColumn("includeInReportsHead", "Включить в отчет");
        head1.setAlign("center");
        head1.setHeaderAlign("center");
        for(RosobrReports rep : model.getRosobrReports())
        {
            CheckboxColumn col = new CheckboxColumn("include" + rep.getCode(), rep.getNumber(), false);
            col.setSelectedObjects(model.getSelectedIncludeRepPaymentsMap().get(rep));
            col.setClickable(false).setOrderable(false);
            head1.addColumn(col);
        }
        dataSource.addColumn(head1);
        
        HeadColumn head2 = new HeadColumn("separateColumnHead", "Выводить отдельным столбцом");
        head2.setAlign("center");
        head2.setHeaderAlign("center");
        for(RosobrReports rep : model.getRosobrReports())
        {
            CheckboxColumn col = new CheckboxColumn("separateColumn" + rep.getCode(), rep.getNumber(), false);
            col.setSelectedObjects(model.getSelectedSeparateRepPaymentsMap().get(rep));
            col.setClickable(false).setOrderable(false);
            head2.addColumn(col);
        }
        dataSource.addColumn(head2);
        
        CheckboxColumn col = new CheckboxColumn("scDegreeColumn", "Считать выплатой за ученую степень", false);
        col.setSelectedObjects(model.getSelectedScDegreePaymentsList());
        col.setClickable(false).setOrderable(false);
        dataSource.addColumn(col);
        
        dataSource.addColumn(new ActionColumn("Вверх", CommonBaseDefine.ICO_UP, "onClickPaymentUp"));
        dataSource.addColumn(new ActionColumn("Вниз", CommonBaseDefine.ICO_DOWN, "onClickPaymentDown"));

        model.setDataSource(dataSource);
    }
    
    public void onClickApply(IBusinessComponent component)
    {
        getDao().update(getModel(component));
    }
    
    public void onClickPaymentUp(IBusinessComponent component)
    {
        getDao().updatePriority(getModel(component), (Long) component.getListenerParameter(), true);
    }
    
    public void onClickPaymentDown(IBusinessComponent component)
    {
        getDao().updatePriority(getModel(component), (Long) component.getListenerParameter(), false);
    }
}