/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.staffListAllocation.Add;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfBorder;
import org.tandemframework.rtf.document.text.table.RtfBorderStyle;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.*;
import org.tandemframework.shared.organization.base.bo.OrgUnit.logic.OrgUnitRankComparator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.shared.organization.catalog.entity.codes.OrgUnitTypeCodes;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.ui.OrgUnitExtendedAutocompleteModel;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.component.reports.staffList.Add.FiltersPresetModel;
import ru.tandemservice.uniemp.component.reports.staffList.Add.IFiltersPresetManager;
import ru.tandemservice.uniemp.component.reports.staffList.Add.OrgUnitTypeWrapper;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.EmployeeTemplateDocument;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.employee.StaffList;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;
import ru.tandemservice.uniemp.entity.employee.StaffListItem;
import ru.tandemservice.uniemp.entity.employee.StaffListPaymentBase;
import ru.tandemservice.uniemp.entity.report.StaffListAllocationReport;
import ru.tandemservice.uniemp.entity.report.StaffListRepPaymentSettings;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author dseleznev
 * Created on: 11.11.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        if (null != model.getFiltersPresetModel() && null != model.getFiltersPresetModel().getFiltersPreset())
            return;

        model.setReportDate(new Date());
        model.setFormingDate(new Date());
        List<IdentifiableWrapper> activityTypesList = new ArrayList<>();
        activityTypesList.add(new IdentifiableWrapper(StaffListAllocationReportUtil.ACTIVITY_TYPE_BUDGET, "Бюджетные средства"));
        activityTypesList.add(new IdentifiableWrapper(StaffListAllocationReportUtil.ACTIVITY_TYPE_OFF_BUDGET, "Внебюджетные средства"));
        model.setActivityTypesList(activityTypesList);

        model.setOrgUnitList(new OrgUnitExtendedAutocompleteModel());
        model.setEmployeeTypeListModel(new LazySimpleSelectModel<>(EmployeeType.class).setSortProperty(EmployeeType.P_CODE));
        model.setQualificationLevelsList(getList(QualificationLevel.class, QualificationLevel.P_CODE));
        model.setProfQualificationGroupsList(getList(ProfQualificationGroup.class, ProfQualificationGroup.P_CODE));

        model.setEmployeePostList(new BaseSingleSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(OrgUnitTypePostRelation.ENTITY_CLASS, "rel", new String[] { OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L });
                builder.add(MQExpression.like("rel", OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_POST + "." + Post.P_TITLE, CoreStringUtils.escapeLike(filter)));

                if (null != model.getOrgUnit())
                    builder.add(MQExpression.eq("rel", OrgUnitTypePostRelation.L_ORG_UNIT_TYPE, model.getOrgUnit().getOrgUnitType()));

                if (null != model.getEmployeeTypeList() && !model.getEmployeeTypeList().isEmpty())
                    builder.add(MQExpression.in("rel", OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_POST + "." + Post.L_EMPLOYEE_TYPE, model.getEmployeeTypeList()));

                if (null != model.getQualificationLevel())
                    builder.add(MQExpression.eq("rel", OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_QUALIFICATION_LEVEL, model.getQualificationLevel()));

                if (null != model.getProfQualificationGroup())
                    builder.add(MQExpression.eq("rel", OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_PROF_QUALIFICATION_GROUP, model.getProfQualificationGroup()));

                builder.setNeedDistinct(true);
                return new ListResult<>(builder.getResultList(getSession()));
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                IEntity entity = get(PostBoundedWithQGandQL.class, (Long)primaryKey);
                if (findValues("").getObjects().contains(entity)) return entity;
                return null;
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((PostBoundedWithQGandQL)value).getFullTitleWithSalary();
            }
        });

        model.setFiltersPresetModel(new FiltersPresetModel("StaffListAllocationReport", new IFiltersPresetManager()
        {
            @Override
            public void initFilters(IDataSettings dataSettings)
            {
                model.setOrgUnit(dataSettings.<OrgUnit>get("orgUnit"));
                model.setPost(dataSettings.<PostBoundedWithQGandQL>get("post"));
                model.setEmployeeTypeList(dataSettings.<List<EmployeeType>>get("employeeType"));
                model.setQualificationLevel(dataSettings.<QualificationLevel>get("qualificationLevel"));
                model.setProfQualificationGroup(dataSettings.<ProfQualificationGroup>get("profQualificationGroup"));
                model.setActivityType(dataSettings.<IdentifiableWrapper>get("activityType"));
                model.setSelectedOrgUnitTypesList(dataSettings.<List<IEntity>>get("selectedOrgUnitTypes"));
                model.setSelectedShowChildOrgUnitsList(dataSettings.<List<IEntity>>get("selectedShowChildOrgUnitsList"));
                model.setSelectedShowChildOrgUnitsAsOwnList(dataSettings.<List<IEntity>>get("selectedShowChildOrgUnitsAsOwnList"));

                if (null == model.getSelectedOrgUnitTypesList())
                    model.setSelectedOrgUnitTypesList(new ArrayList<>());
                if (null == model.getSelectedShowChildOrgUnitsList())
                    model.setSelectedShowChildOrgUnitsList(new ArrayList<>());
                if (null == model.getSelectedShowChildOrgUnitsAsOwnList())
                    model.setSelectedShowChildOrgUnitsAsOwnList(new ArrayList<>());
            }

            @Override
            public void setFilters(IDataSettings dataSettings)
            {
                dataSettings.set("post", model.getPost());
                dataSettings.set("orgUnit", model.getOrgUnit());
                dataSettings.set("activityType", model.getActivityType());
                dataSettings.set("employeeType", model.getEmployeeTypeList());
                dataSettings.set("qualificationLevel", model.getQualificationLevel());
                dataSettings.set("profQualificationGroup", model.getProfQualificationGroup());
                dataSettings.set("selectedOrgUnitTypes", model.getSelectedOrgUnitTypesList());
                dataSettings.set("selectedShowChildOrgUnitsList", model.getSelectedShowChildOrgUnitsList());
                dataSettings.set("selectedShowChildOrgUnitsAsOwnList", model.getSelectedShowChildOrgUnitsAsOwnList());
            }
        }, new String[] { "presetsBlock", "activityType", "orgUnit", "employeeType", "qualificationLevel", "profQualificationGroup", "post", "orgUnitTypesList" }, getSession()));

        prepareOrgUnitTypesList(model);
        preparePaymentsList(model);
    }

    /**
     * Подготавливает данные для фильтра со списком подразделений ОУ
     */
    private void prepareOrgUnitTypesList(Model model)
    {
        MQBuilder builder = new MQBuilder(OrgUnitType.ENTITY_CLASS, "out");
        builder.add(MQExpression.eq("out", OrgUnitType.P_ENABLED, Boolean.TRUE));
        builder.add(MQExpression.notEq("out", OrgUnitType.P_CODE, OrgUnitTypeCodes.TOP));
        builder.addOrder("out", OrgUnitType.P_PRIORITY);
        List<OrgUnitType> typesList = builder.getResultList(getSession());

        List<OrgUnitTypeWrapper> orgUnitTypesList = new ArrayList<>();
        for (OrgUnitType out : typesList)
        {
            orgUnitTypesList.add(new OrgUnitTypeWrapper(out, true, false, false));
        }
        model.setOrgUnitTypesList(orgUnitTypesList);

        // Устанавливаем значения по умолчанию при загрузке формы
        List<IEntity> selectedTypes = new ArrayList<>();
        selectedTypes.addAll(orgUnitTypesList);

        List<IEntity> selectedShowChilds = new ArrayList<>();
        selectedShowChilds.addAll(orgUnitTypesList);

        model.setSelectedOrgUnitTypesList(selectedTypes);
        model.setSelectedShowChildOrgUnitsList(selectedShowChilds);
    }

    /**
     * Подготавливает данные для фильтра со списком выплат
     */
    private void preparePaymentsList(Model model)
    {
        MQBuilder builder = new MQBuilder(StaffListRepPaymentSettings.ENTITY_CLASS, "s");
        AbstractExpression expr1 = MQExpression.eq("s", StaffListRepPaymentSettings.P_TAKE_INTO_ACCOUNT, Boolean.TRUE);
        AbstractExpression expr2 = MQExpression.isNull("s", StaffListRepPaymentSettings.L_PAYMENT);
        builder.add(MQExpression.or(expr1, expr2));
        builder.addOrder("s", StaffListRepPaymentSettings.P_PRIORITY);
        model.setPaymentsList(builder.<StaffListRepPaymentSettings> getResultList(getSession()));
    }

    @Override
    public void prepareOrgUnitTypesDataSource(Model model)
    {
        UniBaseUtils.createPage(model.getOrgUnitTypesDataSource(), model.getOrgUnitTypesList());
    }

    /**
     * Подготавливает полные списки требуемых из данных из базы:
     * 1. Допустимые подразделения,
     * 2. Должности штатного расписания допустимых подразделений,
     * 3. Должности штатной расстановки допустимых подразделений,
     * 4. Выплаты по должностям штатной расстановки допустимых подразделений.
     */
    private void getDataFromDataBase(Model model)
    {
        if (null == model.getSelectedOrgUnitTypesList() || model.getSelectedOrgUnitTypesList().isEmpty())
            throw new ApplicationException("Необходимо указать хотя бы один тип подразделений.");

        List<OrgUnitType> orgUnitTypesList = new ArrayList<>();
        for (OrgUnitTypeWrapper type : model.getOrgUnitTypesList())
            if (type.isUseOrgUnit())
                orgUnitTypesList.add(type.getOrgUnitType());
        List<OrgUnit> orgUnitsList = UniempDaoFacade.getStaffListReportsDAO().getFilteredOrgUnitslList(model.getOrgUnit(), orgUnitTypesList, model.getReportDate());
        List<StaffList> staffListsList = UniempDaoFacade.getStaffListReportsDAO().getActiveStaffListsList(orgUnitsList, model.getReportDate());

        List<OrgUnit> serializedOrgstructure = getChildOrgUnits(null == model.getOrgUnit() ? TopOrgUnit.getInstance() : model.getOrgUnit(), orgUnitsList, model.getOrgUnitTypesMap(), model.getReportDate());
        if (null != model.getOrgUnit()) serializedOrgstructure.add(0, model.getOrgUnit());
        model.setSerializedOrgstructure(serializedOrgstructure);
        model.setOrgUnitsMap(prepareOrgUnitsMap());

        List<Payment> displayablePaymentsForStaffList = UniempDaoFacade.getStaffListReportsDAO().getDisplayablePaymentsForStaffList();
        List<StaffListItem> staffListItemsList = UniempDaoFacade.getStaffListReportsDAO().getStaffListItemsList(staffListsList, model.getPost(), model.getEmployeeTypeList(), model.getQualificationLevel(), model.getProfQualificationGroup(), null == model.getActivityType() ? null : (model.getActivityType().getId().equals(StaffListAllocationReportUtil.ACTIVITY_TYPE_BUDGET) ? UniempDefines.FINANCING_SOURCE_BUDGET : UniempDefines.FINANCING_SOURCE_OFF_BUDGET), null);
        List<StaffListAllocationItem> allocItemsList = UniempDaoFacade.getStaffListReportsDAO().getStaffListAllocationItemsList(staffListItemsList);
        List<StaffListPaymentBase> staffListPaymentsList = UniempDaoFacade.getStaffListReportsDAO().getStaffListPaymentsList(staffListItemsList, displayablePaymentsForStaffList, null == model.getActivityType() ? null : (model.getActivityType().getId().equals(StaffListAllocationReportUtil.ACTIVITY_TYPE_BUDGET) ? UniempDefines.FINANCING_SOURCE_BUDGET : UniempDefines.FINANCING_SOURCE_OFF_BUDGET), null);
        //List<Payment> allDisplayablePaymentsForStaffList = UniempDaoFacade.getStaffListReportsDAO().getDisplayablePaymentsForStaffList(UniempDaoFacade.getStaffListReportsDAO().getUsedPaymentsSet(staffListPaymentsList));
        List<EmployeePostFakeWrapper> employeePostStaffRatesRelsList = UniempDaoFacade.getStaffListReportsDAO().getEmployeePostStaffRateItemList(staffListItemsList, orgUnitsList, model.getPost(), model.getEmployeeTypeList(), model.getQualificationLevel(), model.getProfQualificationGroup(), model.getReportDate(), null == model.getActivityType() ? null : (model.getActivityType().getId().equals(StaffListAllocationReportUtil.ACTIVITY_TYPE_BUDGET)));

        model.setAccountableOrgUnitsList(orgUnitsList);
        model.setRawStaffListItemsList(staffListItemsList);
        model.setRawStaffListAllocationItemsList(allocItemsList);
        model.setRawEmployeePostStaffRatesList(employeePostStaffRatesRelsList);
        model.setRawStaffListPaymentsList(staffListPaymentsList);
    }

    private RtfDocument getDocument(final Model model)
    {
        // Подготавливаем данные к генерации печатной формы отчета
        getDataFromDataBase(model);
        StaffListAllocationReportUtil.preparePaymentsHeader(model);
        StaffListAllocationReportUtil.prepareDataForPrinting(model); //TODO:
        StaffListAllocationReportUtil.renderTableData(model); //TODO: 

        // получить шаблон отчета
        ITemplateDocument templateDocument = getCatalogItem(EmployeeTemplateDocument.class, UniempDefines.TEMPLATE_PPS_STAFF_LIST_ALLOCATION);
        RtfDocument template = new RtfReader().read(templateDocument.getContent());
        RtfDocument document = template.getClone();

        RtfInjectModifier paramModifier = new RtfInjectModifier();
        paramModifier.put("dateStr", new SimpleDateFormat("«dd» MMMMM yyyy", RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU).format(model.getReportDate()));
        paramModifier.put("employeeType", null != model.getEmployeeTypeList() && model.getEmployeeTypeList().size() == 1 ? ": " + model.getEmployeeTypeList().get(0).getTitle() : "");

        RtfTableModifier tableModifier = new RtfTableModifier();

        tableModifier.put("PH", new String[][] {});
        tableModifier.put("CN", new String[][] { model.getNumberHeaders() });
        tableModifier.put("S", model.getSummaryData());
        tableModifier.put("T", model.getTableData());

        // Устанавливаем модифаер для таблицы заголовков верхнего уровня 
        tableModifier.put("PH", new RtfRowIntercepterBase()
        {
            @Override
            public void beforeModify(RtfTable table, int currentRowIndex)
            {
                if (model.getPaymentColumnWidth().length > 0)
                {
                    RtfUtil.splitRow(table.getRowList().get(1), StaffListAllocationReportUtil.PAYMENTS_COLUMN_IDX - 2, (newCell, index) -> {
                        String content = model.getPaymentHeaders()[index];
                        IRtfElement text = RtfBean.getElementFactory().createRtfText(content);
                        newCell.getElementList().add(text);
                    }, model.getPaymentColumnWidth());
                }
                else
                {
                    RtfUtil.splitRow(table.getRowList().get(1), StaffListAllocationReportUtil.PAYMENTS_COLUMN_IDX - 2, (newCell, index) -> {
                        String content = "Надбавок и доплат нет";
                        IRtfElement text = RtfBean.getElementFactory().createRtfText(content);
                        newCell.getElementList().add(text);
                    }, new int[] { 1 });
                }
            }
        });

        // Устанавливаем модифаер для нумерованного заголовка таблицы
        tableModifier.put("CN", new RtfRowIntercepterBase()
        {
            @Override
            public void beforeModify(RtfTable table, int currentRowIndex)
            {
                if (model.getPaymentColumnWidth().length > 0)
                    for (RtfRow row : table.getRowList())
                        RtfUtil.splitRow(row, StaffListAllocationReportUtil.PAYMENTS_COLUMN_IDX + (table.getRowList().indexOf(row) == table.getRowList().size() - 1 ? -4 : 0), null, model.getPaymentColumnWidth());
            }
        });

        tableModifier.put("T", new RtfRowIntercepterBase()
        {
            // Форматируем строки в соответствии с установленными стилями
            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                int cnt = -startIndex;
                for (RtfRow row : newRowList)
                {
                    if (cnt >= 0 && null != model.getRowStyles()[cnt])
                    {
                        StaffListAllocationReportUtil.applyRtfRowStyles(row, model.getRowStyles()[cnt]);
                        if (null != model.getRowStyles()[cnt].getMergingAllStartCell())
                        {
                            row.getCellList().get(0).getCellBorder().setTop(null);
                            row.getCellList().get(0).getCellBorder().setBottom(null);
                            Integer cellToFill = model.getRowStyles()[cnt].getMergingAllStartCell();
                            row.getCellList().get(null != cellToFill ? cellToFill : 1).setBackgroundColorIndex(model.getRowStyles()[cnt].getBackGroundColor());
                        }
                    }
                    else if (cnt >= 0 && null == model.getRowStyles()[cnt])
                    {
                        row.getCellList().get(0).getCellBorder().setTop(null);
                        row.getCellList().get(0).getCellBorder().setBottom(null);

                        RtfBorder oldRight = row.getCellList().get(0).getCellBorder().getRight();
                        RtfBorder newRight = RtfBorder.getInstance(oldRight.getWidth(), oldRight.getColorIndex(), RtfBorderStyle.DASHED_BORDER_SMALL);
                        row.getCellList().get(0).getCellBorder().setRight(newRight);
//                                setStyle(RtfBorderStyle.DASHED_BORDER_SMALL);
                    }
                    cnt++;
                }
            }
        });

        paramModifier.modify(document);
        tableModifier.modify(document);
        return document;
    }

    /**
     * Генерирует и сохраняет отчет в базу данных
     */
    @Override
    public void update(Model model)
    {
        if (null == model.getSelectedOrgUnitTypesList() || model.getSelectedOrgUnitTypesList().isEmpty())
            throw new ApplicationException("Необходимо указать хотя бы один тип подразделений.");

        StaffListAllocationReport report = model.getReport();

        report.setFormingDate(new Date());
        report.setReportDate(DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getReportDate()));
        report.setActivityType(null != model.getActivityType() ? model.getActivityType().getTitle() : null);
        report.setOrgUnit(null != model.getOrgUnit() ? model.getOrgUnit().getFullTitle() : null);
        report.setQualificationLevel(null != model.getQualificationLevel() ? model.getQualificationLevel().getTitle() : null);
        report.setProfQualificationGroup(null != model.getProfQualificationGroup() ? model.getProfQualificationGroup().getTitle() : null);
        report.setPost(null != model.getPost() ? model.getPost().getFullTitleWithSalary() : null);

        StringBuilder emplTypesBuilder = new StringBuilder();
        for (EmployeeType type : model.getEmployeeTypeList())
            emplTypesBuilder.append(emplTypesBuilder.length() > 0 ? ";" : "").append(type.getShortTitle());
        report.setEmployeePostType(emplTypesBuilder.length() > 0 ? emplTypesBuilder.append(".").toString() : "");

        if (null == model.getSelectedOrgUnitTypesList() || model.getSelectedOrgUnitTypesList().isEmpty())
            report.setOrgUnitTypes(null);
        else if (model.getOrgUnitTypesList().size() == model.getSelectedOrgUnitTypesList().size())
            report.setOrgUnitTypes("Все");
        else
        {
            StringBuilder orgUnitTypesStr = new StringBuilder();
            for (IEntity wrapper : model.getSelectedOrgUnitTypesList())
                orgUnitTypesStr.append(orgUnitTypesStr.length() > 0 ? "; " : "").append(((OrgUnitTypeWrapper)wrapper).getOrgUnitType().getTitle());
            report.setOrgUnitTypes(orgUnitTypesStr.append(".").toString());
        }

        DatabaseFile databaseFile = new DatabaseFile();
        databaseFile.setContent(RtfUtil.toByteArray(getDocument(model)));
        getSession().save(databaseFile);

        report.setContent(databaseFile);
        getSession().save(report);
    }

    /**
     * Возвращает список дочерних подразделений для указанного подразделения,
     * включая дочерние подразделения дочерних подразделений и т.д.
     * Другими словами, возвращает иерархию оргструктуры в последовательном виде
     */
    @Override
    public List<OrgUnit> getChildOrgUnits(OrgUnit orgUnit, List<OrgUnit> orgUnitsToTake, Map<Long, OrgUnitTypeWrapper> orgUnitTypesMap, Date actualDate)
    {
        if (null != orgUnit.getParent() && !orgUnitTypesMap.get(orgUnit.getOrgUnitType().getId()).isShowChildOrgUnits())
            return new ArrayList<>();

        List<OrgUnit> result = new ArrayList<>();

        AbstractExpression expr1 = MQExpression.isNull("ou", OrgUnit.P_ARCHIVING_DATE);
        AbstractExpression expr2 = MQExpression.isNotNull("ou", OrgUnit.P_ARCHIVING_DATE);
        AbstractExpression expr3 = MQExpression.greatOrEq("ou", OrgUnit.P_ARCHIVING_DATE, actualDate);

        MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "ou");
        builder.add(MQExpression.eq("ou", OrgUnit.L_PARENT, orgUnit));
        builder.add(MQExpression.or(expr1, MQExpression.and(expr2, expr3)));
        builder.addOrder("ou", OrgUnit.L_ORG_UNIT_TYPE + "." + OrgUnitType.P_PRIORITY);
        builder.addOrder("ou", OrgUnit.P_NOMINATIVE_CASE_TITLE);
        builder.addOrder("ou", OrgUnit.P_TITLE);
        List<OrgUnit> childsList = builder.getResultList(getSession());

        Collections.sort(childsList, OrgUnitRankComparator.INSTANCE);

        for (OrgUnit child : childsList)
        {
            if (orgUnitsToTake.contains(child))
            {
                result.add(child);
                if (orgUnitTypesMap.get(child.getOrgUnitType().getId()).isShowChildOrgUnits())
                    result.addAll(getChildOrgUnits(child, orgUnitsToTake, orgUnitTypesMap, actualDate));
            }
        }

        return result;
    }

    private Map<Long, OrgUnit> prepareOrgUnitsMap()
    {
        Map<Long, OrgUnit> orgUnitsMap = new HashMap<>();
        MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "ou");
        for (OrgUnit orgUnit : builder.<OrgUnit> getResultList(getSession()))
        {
            orgUnitsMap.put(orgUnit.getId(), orgUnit);
        }
        return orgUnitsMap;
    }
}