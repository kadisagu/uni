package ru.tandemservice.uniemp.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.person.catalog.entity.DiplomaQualifications;
import org.tandemframework.shared.person.catalog.entity.EduInstitution;
import org.tandemframework.shared.person.catalog.entity.EducationDocumentType;
import org.tandemframework.shared.person.catalog.entity.EducationalInstitutionTypeKind;
import org.tandemframework.shared.person.catalog.entity.EmployeeSpeciality;
import ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Факт Профессиональной переподготовки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeRetrainingItemGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem";
    public static final String ENTITY_NAME = "employeeRetrainingItem";
    public static final int VERSION_HASH = -2007896492;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE = "employee";
    public static final String P_START_DATE = "startDate";
    public static final String P_FINISH_DATE = "finishDate";
    public static final String P_AMOUNT_HOURS = "amountHours";
    public static final String L_EMPLOYEE_SPECIALITY = "employeeSpeciality";
    public static final String L_DIPLOMA_QUALIFICATIONS = "diplomaQualifications";
    public static final String L_ADDRESS_ITEM = "addressItem";
    public static final String L_EDU_INSTITUTION_KIND = "eduInstitutionKind";
    public static final String L_EDU_INSTITUTION = "eduInstitution";
    public static final String L_EDU_DOCUMENT_TYPE = "eduDocumentType";
    public static final String P_EDU_DOCUMENT_SERIA = "eduDocumentSeria";
    public static final String P_EDU_DOCUMENT_NUMBER = "eduDocumentNumber";
    public static final String P_EDU_DOCUMENT_DATE = "eduDocumentDate";
    public static final String L_EDU_DOCUMENT_FILE = "eduDocumentFile";
    public static final String P_EDU_DOCUMENT_FILE_NAME = "eduDocumentFileName";
    public static final String P_EDU_DOCUMENT_FILE_TYPE = "eduDocumentFileType";
    public static final String P_ORDER_NUMBER = "orderNumber";
    public static final String P_ORDER_DATE = "orderDate";

    private Employee _employee;     // Кадровый ресурс
    private Date _startDate;     // Дата начала переподготовки
    private Date _finishDate;     // Дата окончания переподготовки
    private Integer _amountHours;     // Кол-во часов
    private EmployeeSpeciality _employeeSpeciality;     // Специальность по диплому
    private DiplomaQualifications _diplomaQualifications;     // Квалификация по диплому
    private AddressItem _addressItem;     // Элемент административно-территориального деления
    private EducationalInstitutionTypeKind _eduInstitutionKind;     // Тип/вид образовательного учреждения
    private EduInstitution _eduInstitution;     // Образовательное учреждение
    private EducationDocumentType _eduDocumentType;     // Тип документа о полученном образовании
    private String _eduDocumentSeria;     // Серия документа о полученном образовании
    private String _eduDocumentNumber;     // Номер документа о полученном образовании
    private Date _eduDocumentDate;     // Дата выдачи документа о полученном образовании
    private DatabaseFile _eduDocumentFile;     // Копия документа о полученном образовании
    private String _eduDocumentFileName;     // Имя файла копии документа о полученном образовании
    private String _eduDocumentFileType;     // Тип файла копии документа о полученном образовании
    private String _orderNumber;     // Номер приказа о направлении сотрудника на профессиональную переподготовку
    private Date _orderDate;     // Дата приказа о направлении сотрудника на профессиональную переподготовку

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Кадровый ресурс. Свойство не может быть null.
     */
    @NotNull
    public Employee getEmployee()
    {
        return _employee;
    }

    /**
     * @param employee Кадровый ресурс. Свойство не может быть null.
     */
    public void setEmployee(Employee employee)
    {
        dirty(_employee, employee);
        _employee = employee;
    }

    /**
     * @return Дата начала переподготовки. Свойство не может быть null.
     */
    @NotNull
    public Date getStartDate()
    {
        return _startDate;
    }

    /**
     * @param startDate Дата начала переподготовки. Свойство не может быть null.
     */
    public void setStartDate(Date startDate)
    {
        dirty(_startDate, startDate);
        _startDate = startDate;
    }

    /**
     * @return Дата окончания переподготовки. Свойство не может быть null.
     */
    @NotNull
    public Date getFinishDate()
    {
        return _finishDate;
    }

    /**
     * @param finishDate Дата окончания переподготовки. Свойство не может быть null.
     */
    public void setFinishDate(Date finishDate)
    {
        dirty(_finishDate, finishDate);
        _finishDate = finishDate;
    }

    /**
     * @return Кол-во часов.
     */
    public Integer getAmountHours()
    {
        return _amountHours;
    }

    /**
     * @param amountHours Кол-во часов.
     */
    public void setAmountHours(Integer amountHours)
    {
        dirty(_amountHours, amountHours);
        _amountHours = amountHours;
    }

    /**
     * @return Специальность по диплому.
     */
    public EmployeeSpeciality getEmployeeSpeciality()
    {
        return _employeeSpeciality;
    }

    /**
     * @param employeeSpeciality Специальность по диплому.
     */
    public void setEmployeeSpeciality(EmployeeSpeciality employeeSpeciality)
    {
        dirty(_employeeSpeciality, employeeSpeciality);
        _employeeSpeciality = employeeSpeciality;
    }

    /**
     * @return Квалификация по диплому.
     */
    public DiplomaQualifications getDiplomaQualifications()
    {
        return _diplomaQualifications;
    }

    /**
     * @param diplomaQualifications Квалификация по диплому.
     */
    public void setDiplomaQualifications(DiplomaQualifications diplomaQualifications)
    {
        dirty(_diplomaQualifications, diplomaQualifications);
        _diplomaQualifications = diplomaQualifications;
    }

    /**
     * @return Элемент административно-территориального деления. Свойство не может быть null.
     */
    @NotNull
    public AddressItem getAddressItem()
    {
        return _addressItem;
    }

    /**
     * @param addressItem Элемент административно-территориального деления. Свойство не может быть null.
     */
    public void setAddressItem(AddressItem addressItem)
    {
        dirty(_addressItem, addressItem);
        _addressItem = addressItem;
    }

    /**
     * @return Тип/вид образовательного учреждения. Свойство не может быть null.
     */
    @NotNull
    public EducationalInstitutionTypeKind getEduInstitutionKind()
    {
        return _eduInstitutionKind;
    }

    /**
     * @param eduInstitutionKind Тип/вид образовательного учреждения. Свойство не может быть null.
     */
    public void setEduInstitutionKind(EducationalInstitutionTypeKind eduInstitutionKind)
    {
        dirty(_eduInstitutionKind, eduInstitutionKind);
        _eduInstitutionKind = eduInstitutionKind;
    }

    /**
     * @return Образовательное учреждение.
     */
    public EduInstitution getEduInstitution()
    {
        return _eduInstitution;
    }

    /**
     * @param eduInstitution Образовательное учреждение.
     */
    public void setEduInstitution(EduInstitution eduInstitution)
    {
        dirty(_eduInstitution, eduInstitution);
        _eduInstitution = eduInstitution;
    }

    /**
     * @return Тип документа о полученном образовании. Свойство не может быть null.
     */
    @NotNull
    public EducationDocumentType getEduDocumentType()
    {
        return _eduDocumentType;
    }

    /**
     * @param eduDocumentType Тип документа о полученном образовании. Свойство не может быть null.
     */
    public void setEduDocumentType(EducationDocumentType eduDocumentType)
    {
        dirty(_eduDocumentType, eduDocumentType);
        _eduDocumentType = eduDocumentType;
    }

    /**
     * @return Серия документа о полученном образовании.
     */
    @Length(max=255)
    public String getEduDocumentSeria()
    {
        return _eduDocumentSeria;
    }

    /**
     * @param eduDocumentSeria Серия документа о полученном образовании.
     */
    public void setEduDocumentSeria(String eduDocumentSeria)
    {
        dirty(_eduDocumentSeria, eduDocumentSeria);
        _eduDocumentSeria = eduDocumentSeria;
    }

    /**
     * @return Номер документа о полученном образовании. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getEduDocumentNumber()
    {
        return _eduDocumentNumber;
    }

    /**
     * @param eduDocumentNumber Номер документа о полученном образовании. Свойство не может быть null.
     */
    public void setEduDocumentNumber(String eduDocumentNumber)
    {
        dirty(_eduDocumentNumber, eduDocumentNumber);
        _eduDocumentNumber = eduDocumentNumber;
    }

    /**
     * @return Дата выдачи документа о полученном образовании.
     */
    public Date getEduDocumentDate()
    {
        return _eduDocumentDate;
    }

    /**
     * @param eduDocumentDate Дата выдачи документа о полученном образовании.
     */
    public void setEduDocumentDate(Date eduDocumentDate)
    {
        dirty(_eduDocumentDate, eduDocumentDate);
        _eduDocumentDate = eduDocumentDate;
    }

    /**
     * @return Копия документа о полученном образовании.
     */
    public DatabaseFile getEduDocumentFile()
    {
        return _eduDocumentFile;
    }

    /**
     * @param eduDocumentFile Копия документа о полученном образовании.
     */
    public void setEduDocumentFile(DatabaseFile eduDocumentFile)
    {
        dirty(_eduDocumentFile, eduDocumentFile);
        _eduDocumentFile = eduDocumentFile;
    }

    /**
     * @return Имя файла копии документа о полученном образовании.
     */
    @Length(max=255)
    public String getEduDocumentFileName()
    {
        return _eduDocumentFileName;
    }

    /**
     * @param eduDocumentFileName Имя файла копии документа о полученном образовании.
     */
    public void setEduDocumentFileName(String eduDocumentFileName)
    {
        dirty(_eduDocumentFileName, eduDocumentFileName);
        _eduDocumentFileName = eduDocumentFileName;
    }

    /**
     * @return Тип файла копии документа о полученном образовании.
     */
    @Length(max=255)
    public String getEduDocumentFileType()
    {
        return _eduDocumentFileType;
    }

    /**
     * @param eduDocumentFileType Тип файла копии документа о полученном образовании.
     */
    public void setEduDocumentFileType(String eduDocumentFileType)
    {
        dirty(_eduDocumentFileType, eduDocumentFileType);
        _eduDocumentFileType = eduDocumentFileType;
    }

    /**
     * @return Номер приказа о направлении сотрудника на профессиональную переподготовку.
     */
    @Length(max=255)
    public String getOrderNumber()
    {
        return _orderNumber;
    }

    /**
     * @param orderNumber Номер приказа о направлении сотрудника на профессиональную переподготовку.
     */
    public void setOrderNumber(String orderNumber)
    {
        dirty(_orderNumber, orderNumber);
        _orderNumber = orderNumber;
    }

    /**
     * @return Дата приказа о направлении сотрудника на профессиональную переподготовку.
     */
    public Date getOrderDate()
    {
        return _orderDate;
    }

    /**
     * @param orderDate Дата приказа о направлении сотрудника на профессиональную переподготовку.
     */
    public void setOrderDate(Date orderDate)
    {
        dirty(_orderDate, orderDate);
        _orderDate = orderDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EmployeeRetrainingItemGen)
        {
            setEmployee(((EmployeeRetrainingItem)another).getEmployee());
            setStartDate(((EmployeeRetrainingItem)another).getStartDate());
            setFinishDate(((EmployeeRetrainingItem)another).getFinishDate());
            setAmountHours(((EmployeeRetrainingItem)another).getAmountHours());
            setEmployeeSpeciality(((EmployeeRetrainingItem)another).getEmployeeSpeciality());
            setDiplomaQualifications(((EmployeeRetrainingItem)another).getDiplomaQualifications());
            setAddressItem(((EmployeeRetrainingItem)another).getAddressItem());
            setEduInstitutionKind(((EmployeeRetrainingItem)another).getEduInstitutionKind());
            setEduInstitution(((EmployeeRetrainingItem)another).getEduInstitution());
            setEduDocumentType(((EmployeeRetrainingItem)another).getEduDocumentType());
            setEduDocumentSeria(((EmployeeRetrainingItem)another).getEduDocumentSeria());
            setEduDocumentNumber(((EmployeeRetrainingItem)another).getEduDocumentNumber());
            setEduDocumentDate(((EmployeeRetrainingItem)another).getEduDocumentDate());
            setEduDocumentFile(((EmployeeRetrainingItem)another).getEduDocumentFile());
            setEduDocumentFileName(((EmployeeRetrainingItem)another).getEduDocumentFileName());
            setEduDocumentFileType(((EmployeeRetrainingItem)another).getEduDocumentFileType());
            setOrderNumber(((EmployeeRetrainingItem)another).getOrderNumber());
            setOrderDate(((EmployeeRetrainingItem)another).getOrderDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeRetrainingItemGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeRetrainingItem.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeRetrainingItem();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "employee":
                    return obj.getEmployee();
                case "startDate":
                    return obj.getStartDate();
                case "finishDate":
                    return obj.getFinishDate();
                case "amountHours":
                    return obj.getAmountHours();
                case "employeeSpeciality":
                    return obj.getEmployeeSpeciality();
                case "diplomaQualifications":
                    return obj.getDiplomaQualifications();
                case "addressItem":
                    return obj.getAddressItem();
                case "eduInstitutionKind":
                    return obj.getEduInstitutionKind();
                case "eduInstitution":
                    return obj.getEduInstitution();
                case "eduDocumentType":
                    return obj.getEduDocumentType();
                case "eduDocumentSeria":
                    return obj.getEduDocumentSeria();
                case "eduDocumentNumber":
                    return obj.getEduDocumentNumber();
                case "eduDocumentDate":
                    return obj.getEduDocumentDate();
                case "eduDocumentFile":
                    return obj.getEduDocumentFile();
                case "eduDocumentFileName":
                    return obj.getEduDocumentFileName();
                case "eduDocumentFileType":
                    return obj.getEduDocumentFileType();
                case "orderNumber":
                    return obj.getOrderNumber();
                case "orderDate":
                    return obj.getOrderDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "employee":
                    obj.setEmployee((Employee) value);
                    return;
                case "startDate":
                    obj.setStartDate((Date) value);
                    return;
                case "finishDate":
                    obj.setFinishDate((Date) value);
                    return;
                case "amountHours":
                    obj.setAmountHours((Integer) value);
                    return;
                case "employeeSpeciality":
                    obj.setEmployeeSpeciality((EmployeeSpeciality) value);
                    return;
                case "diplomaQualifications":
                    obj.setDiplomaQualifications((DiplomaQualifications) value);
                    return;
                case "addressItem":
                    obj.setAddressItem((AddressItem) value);
                    return;
                case "eduInstitutionKind":
                    obj.setEduInstitutionKind((EducationalInstitutionTypeKind) value);
                    return;
                case "eduInstitution":
                    obj.setEduInstitution((EduInstitution) value);
                    return;
                case "eduDocumentType":
                    obj.setEduDocumentType((EducationDocumentType) value);
                    return;
                case "eduDocumentSeria":
                    obj.setEduDocumentSeria((String) value);
                    return;
                case "eduDocumentNumber":
                    obj.setEduDocumentNumber((String) value);
                    return;
                case "eduDocumentDate":
                    obj.setEduDocumentDate((Date) value);
                    return;
                case "eduDocumentFile":
                    obj.setEduDocumentFile((DatabaseFile) value);
                    return;
                case "eduDocumentFileName":
                    obj.setEduDocumentFileName((String) value);
                    return;
                case "eduDocumentFileType":
                    obj.setEduDocumentFileType((String) value);
                    return;
                case "orderNumber":
                    obj.setOrderNumber((String) value);
                    return;
                case "orderDate":
                    obj.setOrderDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "employee":
                        return true;
                case "startDate":
                        return true;
                case "finishDate":
                        return true;
                case "amountHours":
                        return true;
                case "employeeSpeciality":
                        return true;
                case "diplomaQualifications":
                        return true;
                case "addressItem":
                        return true;
                case "eduInstitutionKind":
                        return true;
                case "eduInstitution":
                        return true;
                case "eduDocumentType":
                        return true;
                case "eduDocumentSeria":
                        return true;
                case "eduDocumentNumber":
                        return true;
                case "eduDocumentDate":
                        return true;
                case "eduDocumentFile":
                        return true;
                case "eduDocumentFileName":
                        return true;
                case "eduDocumentFileType":
                        return true;
                case "orderNumber":
                        return true;
                case "orderDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "employee":
                    return true;
                case "startDate":
                    return true;
                case "finishDate":
                    return true;
                case "amountHours":
                    return true;
                case "employeeSpeciality":
                    return true;
                case "diplomaQualifications":
                    return true;
                case "addressItem":
                    return true;
                case "eduInstitutionKind":
                    return true;
                case "eduInstitution":
                    return true;
                case "eduDocumentType":
                    return true;
                case "eduDocumentSeria":
                    return true;
                case "eduDocumentNumber":
                    return true;
                case "eduDocumentDate":
                    return true;
                case "eduDocumentFile":
                    return true;
                case "eduDocumentFileName":
                    return true;
                case "eduDocumentFileType":
                    return true;
                case "orderNumber":
                    return true;
                case "orderDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "employee":
                    return Employee.class;
                case "startDate":
                    return Date.class;
                case "finishDate":
                    return Date.class;
                case "amountHours":
                    return Integer.class;
                case "employeeSpeciality":
                    return EmployeeSpeciality.class;
                case "diplomaQualifications":
                    return DiplomaQualifications.class;
                case "addressItem":
                    return AddressItem.class;
                case "eduInstitutionKind":
                    return EducationalInstitutionTypeKind.class;
                case "eduInstitution":
                    return EduInstitution.class;
                case "eduDocumentType":
                    return EducationDocumentType.class;
                case "eduDocumentSeria":
                    return String.class;
                case "eduDocumentNumber":
                    return String.class;
                case "eduDocumentDate":
                    return Date.class;
                case "eduDocumentFile":
                    return DatabaseFile.class;
                case "eduDocumentFileName":
                    return String.class;
                case "eduDocumentFileType":
                    return String.class;
                case "orderNumber":
                    return String.class;
                case "orderDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeeRetrainingItem> _dslPath = new Path<EmployeeRetrainingItem>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeRetrainingItem");
    }
            

    /**
     * @return Кадровый ресурс. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem#getEmployee()
     */
    public static Employee.Path<Employee> employee()
    {
        return _dslPath.employee();
    }

    /**
     * @return Дата начала переподготовки. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem#getStartDate()
     */
    public static PropertyPath<Date> startDate()
    {
        return _dslPath.startDate();
    }

    /**
     * @return Дата окончания переподготовки. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem#getFinishDate()
     */
    public static PropertyPath<Date> finishDate()
    {
        return _dslPath.finishDate();
    }

    /**
     * @return Кол-во часов.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem#getAmountHours()
     */
    public static PropertyPath<Integer> amountHours()
    {
        return _dslPath.amountHours();
    }

    /**
     * @return Специальность по диплому.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem#getEmployeeSpeciality()
     */
    public static EmployeeSpeciality.Path<EmployeeSpeciality> employeeSpeciality()
    {
        return _dslPath.employeeSpeciality();
    }

    /**
     * @return Квалификация по диплому.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem#getDiplomaQualifications()
     */
    public static DiplomaQualifications.Path<DiplomaQualifications> diplomaQualifications()
    {
        return _dslPath.diplomaQualifications();
    }

    /**
     * @return Элемент административно-территориального деления. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem#getAddressItem()
     */
    public static AddressItem.Path<AddressItem> addressItem()
    {
        return _dslPath.addressItem();
    }

    /**
     * @return Тип/вид образовательного учреждения. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem#getEduInstitutionKind()
     */
    public static EducationalInstitutionTypeKind.Path<EducationalInstitutionTypeKind> eduInstitutionKind()
    {
        return _dslPath.eduInstitutionKind();
    }

    /**
     * @return Образовательное учреждение.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem#getEduInstitution()
     */
    public static EduInstitution.Path<EduInstitution> eduInstitution()
    {
        return _dslPath.eduInstitution();
    }

    /**
     * @return Тип документа о полученном образовании. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem#getEduDocumentType()
     */
    public static EducationDocumentType.Path<EducationDocumentType> eduDocumentType()
    {
        return _dslPath.eduDocumentType();
    }

    /**
     * @return Серия документа о полученном образовании.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem#getEduDocumentSeria()
     */
    public static PropertyPath<String> eduDocumentSeria()
    {
        return _dslPath.eduDocumentSeria();
    }

    /**
     * @return Номер документа о полученном образовании. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem#getEduDocumentNumber()
     */
    public static PropertyPath<String> eduDocumentNumber()
    {
        return _dslPath.eduDocumentNumber();
    }

    /**
     * @return Дата выдачи документа о полученном образовании.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem#getEduDocumentDate()
     */
    public static PropertyPath<Date> eduDocumentDate()
    {
        return _dslPath.eduDocumentDate();
    }

    /**
     * @return Копия документа о полученном образовании.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem#getEduDocumentFile()
     */
    public static DatabaseFile.Path<DatabaseFile> eduDocumentFile()
    {
        return _dslPath.eduDocumentFile();
    }

    /**
     * @return Имя файла копии документа о полученном образовании.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem#getEduDocumentFileName()
     */
    public static PropertyPath<String> eduDocumentFileName()
    {
        return _dslPath.eduDocumentFileName();
    }

    /**
     * @return Тип файла копии документа о полученном образовании.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem#getEduDocumentFileType()
     */
    public static PropertyPath<String> eduDocumentFileType()
    {
        return _dslPath.eduDocumentFileType();
    }

    /**
     * @return Номер приказа о направлении сотрудника на профессиональную переподготовку.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem#getOrderNumber()
     */
    public static PropertyPath<String> orderNumber()
    {
        return _dslPath.orderNumber();
    }

    /**
     * @return Дата приказа о направлении сотрудника на профессиональную переподготовку.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem#getOrderDate()
     */
    public static PropertyPath<Date> orderDate()
    {
        return _dslPath.orderDate();
    }

    public static class Path<E extends EmployeeRetrainingItem> extends EntityPath<E>
    {
        private Employee.Path<Employee> _employee;
        private PropertyPath<Date> _startDate;
        private PropertyPath<Date> _finishDate;
        private PropertyPath<Integer> _amountHours;
        private EmployeeSpeciality.Path<EmployeeSpeciality> _employeeSpeciality;
        private DiplomaQualifications.Path<DiplomaQualifications> _diplomaQualifications;
        private AddressItem.Path<AddressItem> _addressItem;
        private EducationalInstitutionTypeKind.Path<EducationalInstitutionTypeKind> _eduInstitutionKind;
        private EduInstitution.Path<EduInstitution> _eduInstitution;
        private EducationDocumentType.Path<EducationDocumentType> _eduDocumentType;
        private PropertyPath<String> _eduDocumentSeria;
        private PropertyPath<String> _eduDocumentNumber;
        private PropertyPath<Date> _eduDocumentDate;
        private DatabaseFile.Path<DatabaseFile> _eduDocumentFile;
        private PropertyPath<String> _eduDocumentFileName;
        private PropertyPath<String> _eduDocumentFileType;
        private PropertyPath<String> _orderNumber;
        private PropertyPath<Date> _orderDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Кадровый ресурс. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem#getEmployee()
     */
        public Employee.Path<Employee> employee()
        {
            if(_employee == null )
                _employee = new Employee.Path<Employee>(L_EMPLOYEE, this);
            return _employee;
        }

    /**
     * @return Дата начала переподготовки. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem#getStartDate()
     */
        public PropertyPath<Date> startDate()
        {
            if(_startDate == null )
                _startDate = new PropertyPath<Date>(EmployeeRetrainingItemGen.P_START_DATE, this);
            return _startDate;
        }

    /**
     * @return Дата окончания переподготовки. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem#getFinishDate()
     */
        public PropertyPath<Date> finishDate()
        {
            if(_finishDate == null )
                _finishDate = new PropertyPath<Date>(EmployeeRetrainingItemGen.P_FINISH_DATE, this);
            return _finishDate;
        }

    /**
     * @return Кол-во часов.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem#getAmountHours()
     */
        public PropertyPath<Integer> amountHours()
        {
            if(_amountHours == null )
                _amountHours = new PropertyPath<Integer>(EmployeeRetrainingItemGen.P_AMOUNT_HOURS, this);
            return _amountHours;
        }

    /**
     * @return Специальность по диплому.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem#getEmployeeSpeciality()
     */
        public EmployeeSpeciality.Path<EmployeeSpeciality> employeeSpeciality()
        {
            if(_employeeSpeciality == null )
                _employeeSpeciality = new EmployeeSpeciality.Path<EmployeeSpeciality>(L_EMPLOYEE_SPECIALITY, this);
            return _employeeSpeciality;
        }

    /**
     * @return Квалификация по диплому.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem#getDiplomaQualifications()
     */
        public DiplomaQualifications.Path<DiplomaQualifications> diplomaQualifications()
        {
            if(_diplomaQualifications == null )
                _diplomaQualifications = new DiplomaQualifications.Path<DiplomaQualifications>(L_DIPLOMA_QUALIFICATIONS, this);
            return _diplomaQualifications;
        }

    /**
     * @return Элемент административно-территориального деления. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem#getAddressItem()
     */
        public AddressItem.Path<AddressItem> addressItem()
        {
            if(_addressItem == null )
                _addressItem = new AddressItem.Path<AddressItem>(L_ADDRESS_ITEM, this);
            return _addressItem;
        }

    /**
     * @return Тип/вид образовательного учреждения. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem#getEduInstitutionKind()
     */
        public EducationalInstitutionTypeKind.Path<EducationalInstitutionTypeKind> eduInstitutionKind()
        {
            if(_eduInstitutionKind == null )
                _eduInstitutionKind = new EducationalInstitutionTypeKind.Path<EducationalInstitutionTypeKind>(L_EDU_INSTITUTION_KIND, this);
            return _eduInstitutionKind;
        }

    /**
     * @return Образовательное учреждение.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem#getEduInstitution()
     */
        public EduInstitution.Path<EduInstitution> eduInstitution()
        {
            if(_eduInstitution == null )
                _eduInstitution = new EduInstitution.Path<EduInstitution>(L_EDU_INSTITUTION, this);
            return _eduInstitution;
        }

    /**
     * @return Тип документа о полученном образовании. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem#getEduDocumentType()
     */
        public EducationDocumentType.Path<EducationDocumentType> eduDocumentType()
        {
            if(_eduDocumentType == null )
                _eduDocumentType = new EducationDocumentType.Path<EducationDocumentType>(L_EDU_DOCUMENT_TYPE, this);
            return _eduDocumentType;
        }

    /**
     * @return Серия документа о полученном образовании.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem#getEduDocumentSeria()
     */
        public PropertyPath<String> eduDocumentSeria()
        {
            if(_eduDocumentSeria == null )
                _eduDocumentSeria = new PropertyPath<String>(EmployeeRetrainingItemGen.P_EDU_DOCUMENT_SERIA, this);
            return _eduDocumentSeria;
        }

    /**
     * @return Номер документа о полученном образовании. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem#getEduDocumentNumber()
     */
        public PropertyPath<String> eduDocumentNumber()
        {
            if(_eduDocumentNumber == null )
                _eduDocumentNumber = new PropertyPath<String>(EmployeeRetrainingItemGen.P_EDU_DOCUMENT_NUMBER, this);
            return _eduDocumentNumber;
        }

    /**
     * @return Дата выдачи документа о полученном образовании.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem#getEduDocumentDate()
     */
        public PropertyPath<Date> eduDocumentDate()
        {
            if(_eduDocumentDate == null )
                _eduDocumentDate = new PropertyPath<Date>(EmployeeRetrainingItemGen.P_EDU_DOCUMENT_DATE, this);
            return _eduDocumentDate;
        }

    /**
     * @return Копия документа о полученном образовании.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem#getEduDocumentFile()
     */
        public DatabaseFile.Path<DatabaseFile> eduDocumentFile()
        {
            if(_eduDocumentFile == null )
                _eduDocumentFile = new DatabaseFile.Path<DatabaseFile>(L_EDU_DOCUMENT_FILE, this);
            return _eduDocumentFile;
        }

    /**
     * @return Имя файла копии документа о полученном образовании.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem#getEduDocumentFileName()
     */
        public PropertyPath<String> eduDocumentFileName()
        {
            if(_eduDocumentFileName == null )
                _eduDocumentFileName = new PropertyPath<String>(EmployeeRetrainingItemGen.P_EDU_DOCUMENT_FILE_NAME, this);
            return _eduDocumentFileName;
        }

    /**
     * @return Тип файла копии документа о полученном образовании.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem#getEduDocumentFileType()
     */
        public PropertyPath<String> eduDocumentFileType()
        {
            if(_eduDocumentFileType == null )
                _eduDocumentFileType = new PropertyPath<String>(EmployeeRetrainingItemGen.P_EDU_DOCUMENT_FILE_TYPE, this);
            return _eduDocumentFileType;
        }

    /**
     * @return Номер приказа о направлении сотрудника на профессиональную переподготовку.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem#getOrderNumber()
     */
        public PropertyPath<String> orderNumber()
        {
            if(_orderNumber == null )
                _orderNumber = new PropertyPath<String>(EmployeeRetrainingItemGen.P_ORDER_NUMBER, this);
            return _orderNumber;
        }

    /**
     * @return Дата приказа о направлении сотрудника на профессиональную переподготовку.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem#getOrderDate()
     */
        public PropertyPath<Date> orderDate()
        {
            if(_orderDate == null )
                _orderDate = new PropertyPath<Date>(EmployeeRetrainingItemGen.P_ORDER_DATE, this);
            return _orderDate;
        }

        public Class getEntityClass()
        {
            return EmployeeRetrainingItem.class;
        }

        public String getEntityName()
        {
            return "employeeRetrainingItem";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
