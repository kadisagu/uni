/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.component.employee.EmployeeListUniemp;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.employeebase.catalog.entity.EtksLevels;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitAutocompleteModel;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.tapsupport.component.selection.*;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;
import ru.tandemservice.uniemp.entity.catalog.CombinationPostType;
import ru.tandemservice.uniemp.entity.employee.CombinationPost;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author lefay
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final DQLOrderDescriptionRegistry _orderSettings = new DQLOrderDescriptionRegistry(Employee.class, "e");

    static
    {
        _orderSettings.setOrders(EmployeeListWrapper.P_EMPLOYEE_CODE, new OrderDescription("e", Employee.P_EMPLOYEE_CODE));
        _orderSettings.setOrders(EmployeeListWrapper.P_FULL_FIO, new OrderDescription("i", IdentityCard.P_LAST_NAME), new OrderDescription("i", IdentityCard.P_FIRST_NAME), new OrderDescription("i", IdentityCard.P_MIDDLE_NAME));
    }

    @Override
    public void prepare(final Model model)
    {
        model.setOrgUnitList(new OrgUnitAutocompleteModel());
        model.setYesNoSelectModel(TwinComboDataSourceHandler.getYesNoDefaultSelectModel());
        model.setPostStatusList(new BaseMultiSelectModel()
        {
            @Override
            public List getValues(Set primaryKeys) {
                List<EmployeePostStatus> objects = findValues("").getObjects();
                List<EmployeePostStatus> resultList = new ArrayList<>();

                for (EmployeePostStatus postStatus: objects)
                {
                    if (primaryKeys.contains(postStatus.getId()))
                        resultList.add(postStatus);
                }

                return resultList;
            }

            @Override
            public ListResult<EmployeePostStatus> findValues(String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmployeePostStatus.class, "eps");
                Boolean activeStatus = TwinComboDataSourceHandler.getSelectedValue(model.getSettings().get("activeStatus"));
                if (null != activeStatus)
                {
                    builder.where(eq(property("eps", EmployeePostStatus.active()), value(activeStatus)));
                }
                Boolean actualStatus = TwinComboDataSourceHandler.getSelectedValue(model.getSettings().get("actualStatus"));
                if (null != actualStatus)
                {
                    builder.where(eq(property("eps", EmployeePostStatus.active()), value(actualStatus)));
                }
                return new ListResult<>(builder.createStatement(getSession()).list());
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((EmployeePostStatus) value).getTitle();
            }
        });
        model.setPostTypeList(getCatalogItemList(PostType.class));

        model.setEmployeePostList(new UniQueryFullCheckSelectModel(PostBoundedWithQGandQL.fullTitle().s())
        {
            @Override
            protected MQBuilder query(String alias, String filter)
            {
                MQBuilder builder = new MQBuilder(PostBoundedWithQGandQL.ENTITY_CLASS, alias);
                MQBuilder subBuilder = new MQBuilder(OrgUnitTypePostRelation.ENTITY_CLASS, "rel", new String[]{OrgUnitTypePostRelation.postBoundedWithQGandQL().id().s()});
                if (null != filter)
                {
                    subBuilder.add(MQExpression.like("rel", OrgUnitTypePostRelation.postBoundedWithQGandQL().title().s(), CoreStringUtils.escapeLike(filter)));
                }

                Object orgUnitFilter = model.getSettings().get("orgUnitFilter");
                if (null != orgUnitFilter)
                {
                    subBuilder.add(MQExpression.eq("rel", OrgUnitTypePostRelation.orgUnitType().s(), ((OrgUnit) orgUnitFilter).getOrgUnitType()));
                }
                subBuilder.setNeedDistinct(true);
                builder.add(MQExpression.in(alias, "id", subBuilder));
                builder.addOrder(alias, PostBoundedWithQGandQL.title().s());
                return builder;
            }
        });

        final List<IdentifiableWrapper> combinationPostStatusList = new ArrayList<>();
        combinationPostStatusList.add(new IdentifiableWrapper(1L, "Активные"));
        combinationPostStatusList.add(new IdentifiableWrapper(2L, "Неактивные"));
        model.setCombinationPostStatusModel(new BaseSingleSelectModel()
        {
            @Override
            public Object getValue(Object primaryKey)
            {
                if (primaryKey.equals(1L))
                    return new IdentifiableWrapper(1L, "Активные");
                else
                    return new IdentifiableWrapper(2L, "Неактивные");
            }

            @Override
            public ListResult findValues(String filter)
            {
                if (filter == null || StringUtils.isEmpty(filter))
                    return new ListResult<>(combinationPostStatusList);
                else
                {
                    List<IdentifiableWrapper> resultList = new ArrayList<>();
                    for (IdentifiableWrapper wrapper : combinationPostStatusList)
                        if (StringUtils.containsIgnoreCase(wrapper.getTitle(), filter))
                            resultList.add(wrapper);

                    return new ListResult<>(resultList);
                }

            }
        });

        model.setCombinationPostTypeModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(CombinationPostType.class, "c").column("c");
                builder.where(like(DQLFunctions.upper(property(CombinationPostType.title().fromAlias("c"))), value(CoreStringUtils.escapeLike(filter))));
                if (o != null)
                    builder.where(eqValue(property(CombinationPostType.id().fromAlias("c")), o));

                return new DQLListResultBuilder(builder);
            }
        });

        if (model.getSettings().get("combinationPostStatus") == null)
            model.getSettings().set("combinationPostStatus", new IdentifiableWrapper(1L, "Активные"));
    }

    @Override
    public void prepareListDataSource(Model model, boolean archival)
    {
        IDataSettings settings = model.getSettings();

        final Object orgUnitFilter = settings.get("orgUnitFilter");
        final Object postFilter = settings.get("postFilter");
        final Object postType = settings.get("postType");
        final List<EmployeePostStatus> postStatusList = settings.get("postStatusList");
        final Boolean activeStatus = TwinComboDataSourceHandler.getSelectedValue(settings.get("activeStatus"));
        final Boolean actualStatus = TwinComboDataSourceHandler.getSelectedValue(settings.get("actualStatus"));
        final String employeeCode = settings.get("employeeCode");
        final String lastName = settings.get("lastName");
        final String firstName = settings.get("firstName");
        final String middleName = settings.get("middleName");
        final Boolean showCombinationPost = settings.get("showCombinationPost");
        final Object combinationPostStatus = settings.get("combinationPostStatus");
        final Object combinationPostType = settings.get("combinationPostType");

        DQLSelectBuilder employeeBuilder = new DQLSelectBuilder().fromEntity(Employee.class, "e")
                .joinPath(DQLJoinType.inner, Employee.person().identityCard().fromAlias("e"), "i")
                .where(eqValue(property("e", Employee.P_ARCHIVAL), archival));
        if (null != orgUnitFilter || null != postFilter || null != activeStatus || CollectionUtils.isNotEmpty(postStatusList) || null != postType || null != actualStatus)
        {
            IDQLExpression inCombPost = null;
            if (!(showCombinationPost != null && showCombinationPost))
            {
                if (null != orgUnitFilter || null != postFilter || null != activeStatus || CollectionUtils.isNotEmpty(postStatusList) || null != actualStatus)
                {
                    DQLSelectBuilder combPostBuilder = new DQLSelectBuilder().fromEntity(CombinationPost.class, "cp")
                            .column("cp.id")
                            .where(eq(property("cp", CombinationPost.employeePost().employee()), property("e.id")));

                    if (null != orgUnitFilter)
                        combPostBuilder.where(DQLExpressions.eq(DQLExpressions.property("cp", CombinationPost.orgUnit()), DQLExpressions.commonValue(orgUnitFilter)));

                    if (null != postFilter)
                        combPostBuilder.where(DQLExpressions.eq(DQLExpressions.property("cp", CombinationPost.postBoundedWithQGandQL().id()), DQLExpressions.value(((IEntity) postFilter).getId())));

                    if (CollectionUtils.isNotEmpty(postStatusList))
                        combPostBuilder.where(DQLExpressions.in(DQLExpressions.property("cp", CombinationPost.employeePost().postStatus()), postStatusList));

                    else if (null != activeStatus)
                        combPostBuilder.where(DQLExpressions.eq(DQLExpressions.property("cp", CombinationPost.employeePost().postStatus().active()), DQLExpressions.value(activeStatus)));

                    if (null != actualStatus)
                        combPostBuilder.where(DQLExpressions.eq(DQLExpressions.property("cp", CombinationPost.employeePost().postStatus().actual()), DQLExpressions.value(actualStatus)));

                    if (null != combinationPostStatus)
                    {
                        IEntity entity = (IEntity) combinationPostStatus;
                        Date now = Calendar.getInstance().getTime();
                        Date firstMoment = CoreDateUtils.getDayFirstTimeMoment(now);
                        Date lastMoment = CoreDateUtils.getDayFirstTimeMoment(CoreDateUtils.add(now, Calendar.DAY_OF_YEAR, 1));
                        if (entity.getId().equals(1L))
                        {
                            IDQLExpression beginDateLessOrEqual = lt(property("cp", CombinationPost.beginDate()), valueDate(lastMoment));
                            IDQLExpression endDateIsNull = isNull(property("cp", CombinationPost.endDate()));
                            IDQLExpression endDateGreatOrEqual = ge(property("cp", CombinationPost.endDate()), valueDate(firstMoment));
                            IDQLExpression combinationMeets = and(beginDateLessOrEqual, or(endDateIsNull, endDateGreatOrEqual));
                            IDQLExpression active = eq(property("cp", CombinationPost.employeePost().postStatus().active()), value(Boolean.TRUE));

                            // совмещение активно: дата начала уже наступила, дата окончания не наступила/отсутствует, основная должность активна
                            combPostBuilder.where(and(combinationMeets, active));

                        }
                        else
                        {
                            IDQLExpression beginDateGreat = ge(property("cp", CombinationPost.beginDate()), valueDate(lastMoment));
                            IDQLExpression endDateLess = lt(property("cp", CombinationPost.endDate()), valueDate(firstMoment));
                            IDQLExpression notActive = eq(property("cp", CombinationPost.employeePost().postStatus().active()), value(Boolean.FALSE));

                            // совмещение неавктивно: не наступило/закончилось или основная должность неактивна
                            combPostBuilder.where(or(notActive, or(beginDateGreat, endDateLess)));
                        }
                    }

                    if (null != combinationPostType)
                        combPostBuilder.where(eq(property("cp", CombinationPost.combinationPostType()), commonValue(combinationPostType)));

                    inCombPost = exists(combPostBuilder.buildQuery());
                }
            }

            DQLSelectBuilder postBuilder = new DQLSelectBuilder().fromEntity(EmployeePost.class, "ep")
                    .column("ep.id")
                    .where(eq(property("e.id"), property("ep", EmployeePost.L_EMPLOYEE)));

            if (null != orgUnitFilter)
                postBuilder.where(eq(property("ep", EmployeePost.orgUnit()), commonValue(orgUnitFilter)));

            if (null != postFilter)
                postBuilder.where(eq(property("ep", EmployeePost.postRelation().postBoundedWithQGandQL()), value(((IEntity) postFilter).getId())));

            if (null != postType)
                postBuilder.where(eq(property("ep", EmployeePost.postType()), commonValue(postType)));

            if (CollectionUtils.isNotEmpty(postStatusList))
            {
                postBuilder.where(in(property("ep", EmployeePost.postStatus()), postStatusList));
            }
            else if (null != activeStatus)
            {
                postBuilder.where(eq(property("ep", EmployeePost.postStatus().active()), value(activeStatus)));
            }

            if (null != actualStatus)
                postBuilder.where(eq(property("ep", EmployeePost.postStatus().actual()), value(actualStatus)));

            IDQLExpression inEmplPost = exists(postBuilder.buildQuery());

            if (inCombPost != null)
                employeeBuilder.where(or(inCombPost, inEmplPost));
            else
                employeeBuilder.where(inEmplPost);
        }

        if (!StringUtils.isEmpty(employeeCode))
            employeeBuilder.where(eqValue(property("e", Employee.P_EMPLOYEE_CODE), employeeCode));

        if (!StringUtils.isEmpty(lastName))
            employeeBuilder.where(likeUpper(property("i", IdentityCard.P_LAST_NAME), value(CoreStringUtils.escapeLike(lastName))));

        if (!StringUtils.isEmpty(firstName))
            employeeBuilder.where(likeUpper(property("i", IdentityCard.P_FIRST_NAME), value(CoreStringUtils.escapeLike(firstName))));

        if (!StringUtils.isEmpty(middleName))
            employeeBuilder.where(likeUpper(property("i", IdentityCard.P_MIDDLE_NAME), value(CoreStringUtils.escapeLike(middleName))));

        _orderSettings.applyOrder(employeeBuilder, model.getDataSource().getEntityOrder());
        employeeBuilder
                .column("e.id")
                .column(property("e", Employee.P_EMPLOYEE_CODE))
                .column(property("i", IdentityCard.P_FULL_FIO));


        DynamicListDataSource<EmployeeListWrapper> ds = model.getDataSource();
        DQLExecutionContext dqlExecutionContext = new DQLExecutionContext(getSession());
        Number count = employeeBuilder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();
        ds.setTotalSize((null == count ? 0 : count.longValue()));

        long startRow = ds.isPrintEnabled() ? ds.getPrintStartRow() : ds.getStartRow();
        long countRow = ds.isPrintEnabled() ? ds.getPrintCountRow() : ds.getCountRow();
        List<Object[]> list = employeeBuilder.createStatement(dqlExecutionContext).setFirstResult((int) startRow).setMaxResults((int) countRow).list();

        List<EmployeeListWrapper> wrappersList = new ArrayList<>(list.size());
        List<Long> employeeIdsList = new ArrayList<>(list.size());

        for (Object[] item : list)
        {
            Long id = (Long) item[0];
            wrappersList.add(new EmployeeListWrapper(id, ((String) item[2]), (String) item[1]));
            employeeIdsList.add(id);
        }
        ds.createPage(wrappersList);

        final Map<Long, List<EmployeePostListWrapper>> postsMap = new HashMap<>();
        final Set<Long> disableDeletingList = new HashSet<>();

        BatchUtils.execute(employeeIdsList, DQL.MAX_VALUES_ROW_NUMBER, employeeIdsList1 -> {
            DQLSelectBuilder delEmployeeBuilder = new DQLSelectBuilder().fromEntity(EmployeePost.class, "ep").column(property("ep", EmployeePost.employee().id()));
            delEmployeeBuilder.where(in(property("ep", EmployeePost.employee().id()), employeeIdsList1));
            disableDeletingList.addAll(delEmployeeBuilder.createStatement(getSession()).<Long>list());

            DQLSelectBuilder postDataBuilder = new DQLSelectBuilder().fromEntity(EmployeePost.class, "ep").
                    column(property("ep", EmployeePost.id())).
                    column(property("ep", EmployeePost.employee().id())).
                    column(property("ep", EmployeePost.postRelation().postBoundedWithQGandQL().title())).
                    column(property("ep", EmployeePost.orgUnit().title())).
                    column(property("ep", EmployeePost.orgUnit().orgUnitType().title())).
                    column(property("ep", EmployeePost.postRelation().postBoundedWithQGandQL().post().employeeType().shortTitle())).
                    column(property("ep", EmployeePost.postRelation().postBoundedWithQGandQL().qualificationLevel().shortTitle())).
                    column(property("ep", EmployeePost.postRelation().postBoundedWithQGandQL().profQualificationGroup().shortTitle())).
                    column(property("etks", EtksLevels.title())).
                    column(property("ep", EmployeePost.postType().shortTitle())).
                    column(property("ep", EmployeePost.postStatus().shortTitle()));

            postDataBuilder.joinPath(DQLJoinType.left, EmployeePost.etksLevels().fromAlias("ep"), "etks");
            postDataBuilder.where(in(property("ep", EmployeePost.employee().id()), employeeIdsList1));

            List<Object[]> combinationPostList = new ArrayList<>();
            if (!(showCombinationPost != null && showCombinationPost))
            {
                DQLSelectBuilder combDataBuilder = new DQLSelectBuilder().fromEntity(CombinationPost.class, "cp").
                        column(property("cp", CombinationPost.id())).//0
                        column(property("cp", CombinationPost.employeePost().id())).//1
                        column(property("cp", CombinationPost.postBoundedWithQGandQL().title())).//2
                        column(property("cp", CombinationPost.orgUnit().title())).//3
                        column(property("cp", CombinationPost.orgUnit().orgUnitType().title())).//4
                        column(property("cp", CombinationPost.postBoundedWithQGandQL().post().employeeType().shortTitle())).//5
                        column(property("cp", CombinationPost.postBoundedWithQGandQL().qualificationLevel().shortTitle())).//6
                        column(property("cp", CombinationPost.postBoundedWithQGandQL().profQualificationGroup().shortTitle())).//7
                        column(property("cp", CombinationPost.combinationPostType().shortTitle())).//8
                        column(property("cp", CombinationPost.employeePost().employee().id()));//9
                combDataBuilder.where(in(property("cp", CombinationPost.employeePost().employee().id()), employeeIdsList1));

                if (null != orgUnitFilter)
                    combDataBuilder.where(eqValue(property("cp", CombinationPost.orgUnit()), orgUnitFilter));

                if (null != postFilter)
                    combDataBuilder.where(eqValue(property("cp", CombinationPost.postBoundedWithQGandQL().id()), ((IEntity) postFilter).getId()));

                if (CollectionUtils.isNotEmpty(postStatusList))
                    combDataBuilder.where(in(property("cp", CombinationPost.employeePost().postStatus()), postStatusList));

                else if (null != activeStatus)
                    combDataBuilder.where(eqValue(property("cp", CombinationPost.employeePost().postStatus().active()), activeStatus));

                if (null != actualStatus)
                    combDataBuilder.where(eqValue(property("cp", CombinationPost.employeePost().postStatus().actual()), actualStatus));

                if (null != combinationPostStatus)
                {
                    IEntity entity = (IEntity) combinationPostStatus;
                    Date now = Calendar.getInstance().getTime();
                    Date firstMoment = CoreDateUtils.getDayFirstTimeMoment(now);
                    Date lastMoment = CoreDateUtils.getDayFirstTimeMoment(CoreDateUtils.add(now, Calendar.DAY_OF_YEAR, 1));
                    if (entity.getId().equals(1L))
                    {
                        IDQLExpression beginDateLessOrEqual = lt(property("cp", CombinationPost.beginDate()), valueDate(lastMoment));
                        IDQLExpression endDateIsNull = isNull(property("cp", CombinationPost.endDate()));
                        IDQLExpression endDateGreatOrEqual = ge(property("cp", CombinationPost.endDate()), valueDate(firstMoment));
                        IDQLExpression combinationMeets = and(beginDateLessOrEqual, or(endDateIsNull, endDateGreatOrEqual));
                        IDQLExpression active = eq(property("cp", CombinationPost.employeePost().postStatus().active()), value(Boolean.TRUE));

                        // совмещение активно: дата начала уже наступила, дата окончания не наступила/отсутствует, основная должность активна
                        combDataBuilder.where(and(combinationMeets, active));

                    }
                    else
                    {
                        IDQLExpression beginDateGreat = ge(property("cp", CombinationPost.beginDate()), valueDate(lastMoment));
                        IDQLExpression endDateLess = lt(property("cp", CombinationPost.endDate()), valueDate(firstMoment));
                        IDQLExpression notActive = eq(property("cp", CombinationPost.employeePost().postStatus().active()), value(Boolean.FALSE));

                        // совмещение неавктивно: не наступило/закончилось или основная должность неактивна
                        combDataBuilder.where(or(notActive, or(beginDateGreat, endDateLess)));
                    }
                }

                if (null != combinationPostType)
                    combDataBuilder.where(eq(property("cp", CombinationPost.combinationPostType()), commonValue(combinationPostType)));

                combDataBuilder.order(property("cp", CombinationPost.employeePost().employee().id().s()));
                combDataBuilder.order(property("cp", CombinationPost.postBoundedWithQGandQL().title().s()));
                combDataBuilder.order(property("cp", CombinationPost.orgUnit().title().s()));
                combDataBuilder.order(property("cp", CombinationPost.employeePost().postStatus().shortTitle().s()));

                combinationPostList = combDataBuilder.createStatement(getSession()).list();
            }

            if (null != orgUnitFilter)
                postDataBuilder.where(eqValue(property("ep", EmployeePost.orgUnit()), orgUnitFilter));

            if (null != postFilter)
                postDataBuilder.where(eqValue(property("ep", EmployeePost.postRelation().postBoundedWithQGandQL().id()), ((IEntity) postFilter).getId()));

            if (null != postType)
                postDataBuilder.where(eqValue(property("ep", EmployeePost.postType()), postType));

            if (CollectionUtils.isNotEmpty(postStatusList))
                postDataBuilder.where(in(property("ep", EmployeePost.postStatus()), postStatusList));

            else if (null != activeStatus)
                postDataBuilder.where(eqValue(property("ep", EmployeePost.postStatus().active()), activeStatus));

            if (null != actualStatus)
                postDataBuilder.where(eqValue(property("ep", EmployeePost.postStatus().actual()), actualStatus));

            postDataBuilder.order(property("ep", EmployeePost.employee().id().s()));
            postDataBuilder.order(property("ep", EmployeePost.postRelation().postBoundedWithQGandQL().title().s()));
            postDataBuilder.order(property("ep", EmployeePost.orgUnit().title().s()));
            postDataBuilder.order(property("ep", EmployeePost.postType().shortTitle().s()));
            postDataBuilder.order(property("ep", EmployeePost.postStatus().shortTitle().s()));

            Map<Long, List<EmployeePostListWrapper>> emplPostIdcombPostMap = new HashMap<>();
            for (Object[] combinationPost : combinationPostList)
            {
                List<EmployeePostListWrapper> postList = emplPostIdcombPostMap.get(combinationPost[1]);
                if (postList == null)
                    postList = new ArrayList<>();
                postList.add(new EmployeePostListWrapper(
                        (Long) combinationPost[0],
                        (String) combinationPost[2],
                        (String) combinationPost[3],
                        (String) combinationPost[4],
                        (String) combinationPost[5],
                        (String) combinationPost[6],
                        (String) combinationPost[7],
                        null,
                        (String) combinationPost[8],
                        null,
                        true
                ));
                emplPostIdcombPostMap.put((Long) combinationPost[1], postList);
            }

            List<Long> combPostWrapperList = new ArrayList<>();
            Map<Long, List<EmployeePostListWrapper>> emplIdcombPostMap = new HashMap<>();
            for (Object[] combinationPost : combinationPostList)
            {
                List<EmployeePostListWrapper> postList = emplIdcombPostMap.get(combinationPost[9]);
                if (postList == null)
                    postList = new ArrayList<>();
                EmployeePostListWrapper wrapper = new EmployeePostListWrapper(
                        (Long) combinationPost[0],
                        (String) combinationPost[2],
                        (String) combinationPost[3],
                        (String) combinationPost[4],
                        (String) combinationPost[5],
                        (String) combinationPost[6],
                        (String) combinationPost[7],
                        null,
                        (String) combinationPost[8],
                        null,
                        true
                );
                postList.add(wrapper);
                emplIdcombPostMap.put((Long) combinationPost[9], postList);

                combPostWrapperList.add((Long) combinationPost[0]);
            }


            for (Object[] postFields : postDataBuilder.createStatement(getSession()).<Object[]>list())
            {
                Long employeePostId = (Long) postFields[0];
                Long employeeId = (Long) postFields[1];
                String postTitle = (String) postFields[2];
                String orgUnitTitle = (String) postFields[3];
                String orgUnitTypeTitle = (String) postFields[4];
                String employeeTypeShortTitle = (String) postFields[5];
                String qualifLevelShortTitle = (String) postFields[6];
                String profQualifGroupShortTitle = (String) postFields[7];
                String etksLevelTitle = (String) postFields[8];
                String postTypeShortTitle = (String) postFields[9];
                String postStatusShortTitle = (String) postFields[10];

                EmployeePostListWrapper wrapper = new EmployeePostListWrapper(employeePostId, postTitle, orgUnitTitle, orgUnitTypeTitle, employeeTypeShortTitle, qualifLevelShortTitle, profQualifGroupShortTitle, etksLevelTitle, postTypeShortTitle, postStatusShortTitle, false);
                List<EmployeePostListWrapper> postsList = postsMap.get(employeeId);
                if (null == postsList)
                    postsList = new ArrayList<>();
                postsList.add(wrapper);

                List<EmployeePostListWrapper> combWrapperList = emplPostIdcombPostMap.get(employeePostId);
                if (combWrapperList != null && !combWrapperList.isEmpty())
                {
                    postsList.addAll(postsList.indexOf(wrapper) + 1, combWrapperList);
                    combPostWrapperList.removeAll(ids(combWrapperList));
                }

                postsMap.put(employeeId, postsList);
            }

            for (Object[] comb : combinationPostList)
            {
                if (!combPostWrapperList.contains(comb[0]))
                    continue;

                List<EmployeePostListWrapper> postsList = postsMap.get(comb[9]);
                if (null == postsList)
                    postsList = new ArrayList<>();

                EmployeePostListWrapper wrapper = new EmployeePostListWrapper(
                        (Long) comb[0],
                        (String) comb[2],
                        (String) comb[3],
                        (String) comb[4],
                        (String) comb[5],
                        (String) comb[6],
                        (String) comb[7],
                        null,
                        (String) comb[8],
                        null,
                        true
                );

                postsList.add(wrapper);

                postsMap.put((Long) comb[9], postsList);
            }
        });

        for (EmployeeListWrapper wrapper : model.getDataSource().getEntityList())
        {
            List<EmployeePostListWrapper> postsList = postsMap.get(wrapper.getId());
            if (null == postsList) postsList = new ArrayList<>();
            wrapper.setCantDeleteEmployee(disableDeletingList.contains(wrapper.getId()));
            wrapper.setPostsList(postsList);
        }

        flushClearAndRefresh(); // Нужно для того, чтобы выкинуть целую кучу ненужного хлама.
    }

    @Override
    public void deleteRow(IBusinessComponent context)
    {
        PersonManager.instance().dao().deletePersonRole((Long) context.getListenerParameter());
    }
}