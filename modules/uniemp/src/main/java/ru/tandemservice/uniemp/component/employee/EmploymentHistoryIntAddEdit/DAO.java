/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.EmploymentHistoryIntAddEdit;

import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitByTypeAutocompleteModel;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.CommonMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uniemp.component.employee.EmploymentHistoryAbsAddEdit.EmploymentHistoryAbstractDAO;
import ru.tandemservice.uniemp.entity.catalog.ServiceLengthType;
import ru.tandemservice.uniemp.entity.employee.EmpHistoryToServiceLengthTypeRelation;

import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author dseleznev
 * Created on: 24.12.2008
 */
public class DAO extends EmploymentHistoryAbstractDAO<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);
        model.setOrganization(TopOrgUnit.getInstance());

        if (null != model.getEmploymentHistoryItem().getId())
        {
            if (null != model.getEmploymentHistoryItem().getOrgUnit())
            {
                model.setOrgUnitType(model.getEmploymentHistoryItem().getOrgUnit().getOrgUnitType());
            }

            List<ServiceLengthType> serviceLengthTypeList = new DQLSelectBuilder().fromEntity(EmpHistoryToServiceLengthTypeRelation.class, "b").column(property(EmpHistoryToServiceLengthTypeRelation.serviceLengthType().fromAlias("b")))
                    .where(eq(property(EmpHistoryToServiceLengthTypeRelation.employmentHistoryItemBase().fromAlias("b")), value(model.getEmploymentHistoryItem())))
                    .createStatement(getSession()).list();

            model.setServiceLengthTypeList(serviceLengthTypeList);
        }

        model.setOrgUnitTypeList(OrgUnitManager.instance().dao().getListOrgUnitTypeWithEmployee());
        model.setOrgUnitListModel(new OrgUnitByTypeAutocompleteModel(model));
        model.setPostListModel(new BaseSingleSelectModel()
        {
            @Override
            public ListResult findValues(final String filter)
            {
                final MQBuilder builder = new MQBuilder(PostBoundedWithQGandQL.ENTITY_CLASS, "p");
                if (filter != null)
                {
                    builder.add(MQExpression.like("p", PostBoundedWithQGandQL.P_TITLE, "%" + filter));
                }
                builder.addOrder("p", PostBoundedWithQGandQL.P_TITLE);
                return new ListResult<>(builder.getResultList(getSession()));
            }

            @Override
            public Object getValue(final Object primaryKey)
            {
                final IEntity entity = get((Long)primaryKey);
                if (findValues("").getObjects().contains(entity))
                {
                    return entity;
                }
                else
                {
                    return null;
                }
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((PostBoundedWithQGandQL)value).getFullTitle();
            }
        });

        model.setServiceLengthTypeModel(new CommonMultiSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(ServiceLengthType.class, "b").column("b")
                        .where(likeUpper(property(ServiceLengthType.title().fromAlias("b")), value(CoreStringUtils.escapeLike(filter, true))))
                        .order(property(ServiceLengthType.title().fromAlias("b")));

                if (set != null)
                    builder.where(in(property(ServiceLengthType.id().fromAlias("b")), set));

                return new DQLListResultBuilder(builder);
            }
        });
    }

    @Override
    public void update(Model model)
    {
        if (model.getEmploymentHistoryItem().getId() != null)
        {
            new DQLDeleteBuilder(EmpHistoryToServiceLengthTypeRelation.class)
                    .where(eq(property(EmpHistoryToServiceLengthTypeRelation.employmentHistoryItemBase()), value(model.getEmploymentHistoryItem())))
                    .createStatement(getSession()).execute();
        }

        super.update(model);

        if (model.getServiceLengthTypeList() != null && !model.getServiceLengthTypeList().isEmpty())
        {
            for (ServiceLengthType type : model.getServiceLengthTypeList())
            {
                EmpHistoryToServiceLengthTypeRelation relation = new EmpHistoryToServiceLengthTypeRelation();
                relation.setEmploymentHistoryItemBase(model.getEmploymentHistoryItem());
                relation.setServiceLengthType(type);

                save(relation);
            }

            model.getEmploymentHistoryItem().setEmptyServiceLengthType(false);
            saveOrUpdate(model.getEmploymentHistoryItem());
        }
        else
        {
            model.getEmploymentHistoryItem().setEmptyServiceLengthType(true);
            saveOrUpdate(model.getEmploymentHistoryItem());
        }
    }
}