/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.ext.SystemAction.ui.Pub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.ui.Pub.SystemActionPub;

/**
 * @author Alexander Shaburov
 * @since 12.02.13
 */
@Configuration
public class SystemActionPubExt extends BusinessComponentExtensionManager
{
    public static final String EMP_SYSTEM_ACTION_PUB_ADDON_NAME = "empSystemActionPubAddon";

    @Autowired
    private SystemActionPub _systemActionPub;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_systemActionPub.presenterExtPoint())
                .addAddon(uiAddon(EMP_SYSTEM_ACTION_PUB_ADDON_NAME, EmpSystemActionPubAddon.class))
                .create();
    }
}
