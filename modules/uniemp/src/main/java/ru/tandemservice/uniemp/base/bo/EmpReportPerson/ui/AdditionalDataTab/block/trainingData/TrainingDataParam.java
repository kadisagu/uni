/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.AdditionalDataTab.block.trainingData;

import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.base.bo.EmployeeReportPerson.ui.Add.EmployeeReportPersonAddUI;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import ru.tandemservice.uniemp.entity.employee.EmployeeTrainingItem;

import java.util.Date;

/**
 * @author Vasily Zhukov
 * @since 10.04.2012
 */
public class TrainingDataParam implements IReportDQLModifier
{
    private IReportParam<Date> _startDateFrom = new ReportParam<>();
    private IReportParam<Date> _startDateTo = new ReportParam<>();
    private IReportParam<Date> _finishDateFrom = new ReportParam<>();
    private IReportParam<Date> _finishDateTo = new ReportParam<>();
    private IReportParam<Date> _noTrainingFrom = new ReportParam<>();
    private IReportParam<Date> _noTrainingTo = new ReportParam<>();

    private String employeeAlias(ReportDQL dql)
    {
        // соединяем кадрового ресурса
        return dql.innerJoinEntity(Employee.class, Employee.person());
    }

    private String trainingAlias(ReportDQL dql)
    {
        // соединяем кадрового ресурса
        String employeeAlias = employeeAlias(dql);

        // соединяем повышение квалификации
        return dql.innerJoinEntity(employeeAlias, EmployeeTrainingItem.class, EmployeeTrainingItem.employee());
    }

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {

    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (_startDateFrom.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "trainingData.startDateFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(_startDateFrom.getData()));

            dql.builder.where(DQLExpressions.ge(DQLExpressions.property(EmployeeTrainingItem.startDate().fromAlias(trainingAlias(dql))), DQLExpressions.valueDate(_startDateFrom.getData())));
        }

        if (_startDateTo.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "trainingData.startDateTo", DateFormatter.DEFAULT_DATE_FORMATTER.format(_startDateTo.getData()));

            dql.builder.where(DQLExpressions.le(DQLExpressions.property(EmployeeTrainingItem.startDate().fromAlias(trainingAlias(dql))), DQLExpressions.valueDate(_startDateTo.getData())));
        }

        if (_finishDateFrom.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "trainingData.finishDateFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(_finishDateFrom.getData()));

            dql.builder.where(DQLExpressions.ge(DQLExpressions.property(EmployeeTrainingItem.finishDate().fromAlias(trainingAlias(dql))), DQLExpressions.valueDate(_finishDateFrom.getData())));
        }

        if (_finishDateTo.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "trainingData.finishDateTo", DateFormatter.DEFAULT_DATE_FORMATTER.format(_finishDateTo.getData()));

            dql.builder.where(DQLExpressions.le(DQLExpressions.property(EmployeeTrainingItem.finishDate().fromAlias(trainingAlias(dql))), DQLExpressions.valueDate(_finishDateTo.getData())));
        }

        if (_noTrainingFrom.isActive() || _noTrainingTo.isActive())
        {
            String alias = dql.nextAlias();

            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmployeeTrainingItem.class, alias)
                    .where(DQLExpressions.ge(DQLExpressions.property(EmployeeTrainingItem.employee().fromAlias(alias)), DQLExpressions.property(employeeAlias(dql))));

            if (_noTrainingFrom.isActive())
            {
                printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "trainingData.noTrainingFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(_noTrainingFrom.getData()));

                builder.where(DQLExpressions.ge(DQLExpressions.property(EmployeeTrainingItem.finishDate().fromAlias(alias)), DQLExpressions.valueDate(_noTrainingFrom.getData())));
            }

            if (_noTrainingTo.isActive())
            {
                printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "trainingData.noTrainingTo", DateFormatter.DEFAULT_DATE_FORMATTER.format(_noTrainingTo.getData()));

                builder.where(DQLExpressions.le(DQLExpressions.property(EmployeeTrainingItem.startDate().fromAlias(alias)), DQLExpressions.valueDate(_noTrainingTo.getData())));
            }

            dql.builder.where(DQLExpressions.notExists(builder.buildQuery()));
        }
    }

    // Getters

    public IReportParam<Date> getStartDateFrom()
    {
        return _startDateFrom;
    }

    public IReportParam<Date> getStartDateTo()
    {
        return _startDateTo;
    }

    public IReportParam<Date> getFinishDateFrom()
    {
        return _finishDateFrom;
    }

    public IReportParam<Date> getFinishDateTo()
    {
        return _finishDateTo;
    }

    public IReportParam<Date> getNoTrainingFrom()
    {
        return _noTrainingFrom;
    }

    public IReportParam<Date> getNoTrainingTo()
    {
        return _noTrainingTo;
    }
}
