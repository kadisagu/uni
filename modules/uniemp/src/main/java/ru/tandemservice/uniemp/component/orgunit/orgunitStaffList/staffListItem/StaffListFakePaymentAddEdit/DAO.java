/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffListItem.StaffListFakePaymentAddEdit;

import org.apache.cxf.common.util.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.catalog.PaymentType;
import ru.tandemservice.uniemp.entity.employee.StaffListFakePayment;
import ru.tandemservice.uniemp.entity.employee.StaffListItem;
import ru.tandemservice.uniemp.util.StaffListPaymentsUtil;

/**
 * @author dseleznev
 * Created on: 05.05.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        if (null != model.getStaffListItemId())
            model.setStaffListItem(get(StaffListItem.class, model.getStaffListItemId()));

        if (null != model.getStaffListFakePayment().getId())
        {
            model.setStaffListFakePayment(get(StaffListFakePayment.class, model.getStaffListFakePayment().getId()));
            model.setPaymentType(model.getStaffListFakePayment().getPayment().getType());
        }
        else
            model.getStaffListFakePayment().setStaffListItem(model.getStaffListItem());

        //FinancingSource financingSource = model.getStaffListFakePayment().getFinancingSource();

        model.setFinancingSourceItemsListModel( new BaseSingleSelectModel()
        {
            @Override
            public Object getValue(Object primaryKey)
            {
                if(null == model.getStaffListFakePayment().getFinancingSource()) return null;
                MQBuilder builder = new MQBuilder(FinancingSourceItem.ENTITY_CLASS, "fs");
                builder.add(MQExpression.eq("fs", FinancingSourceItem.financingSource(), model.getStaffListFakePayment().getFinancingSource()));
                builder.add(MQExpression.eq("fs", FinancingSourceItem.id().s(), primaryKey));
                if(builder.getResultCount(getSession()) > 0) return get(FinancingSourceItem.class, (Long)primaryKey);
                return null;
            }

            @Override
            public ListResult findValues(String filter)
            {
                if(null == model.getStaffListFakePayment().getFinancingSource()) return ListResult.getEmpty();
                MQBuilder builder = new MQBuilder(FinancingSourceItem.ENTITY_CLASS, "fs");
                builder.add(MQExpression.eq("fs", FinancingSourceItem.financingSource(), model.getStaffListFakePayment().getFinancingSource()));
                if(!StringUtils.isEmpty(filter))
                    builder.add(MQExpression.like("fs", FinancingSourceItem.title().s(), CoreStringUtils.escapeLike(filter)));
                builder.addOrder("fs", FinancingSource.title().s());
                return new ListResult<>(builder.<FinancingSourceItem>getResultList(getSession(), 50, 0), builder.getResultCount(getSession()));
            }
        });


        model.setFinancingSourcesList(getCatalogItemList(FinancingSource.class));
        model.setPaymentTypesList(HierarchyUtil.listHierarchyNodesWithParents(getList(PaymentType.class), true));
        model.setPaymentsListModel(new BaseSingleSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (null == model.getPaymentType()) return ListResult.getEmpty();

                MQBuilder builder = new MQBuilder(Payment.ENTITY_CLASS, "p");
                builder.add(MQExpression.eq("p", Payment.P_ACTIVE, Boolean.TRUE));
                builder.add(MQExpression.eq("p", Payment.L_TYPE, model.getPaymentType()));
                builder.addOrder("p", Payment.P_TITLE);
                return new ListResult<>(builder.getResultList(getSession()));
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                IEntity entity = get(Payment.class, (Long)primaryKey);
                if (findValues("").getObjects().contains(entity)) return entity;
                return null;
            }
        });
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StaffListFakePayment.class, "b").column("b");
        builder.where(DQLExpressions.eqValue(DQLExpressions.property(StaffListFakePayment.staffListItem().id().fromAlias("b")), model.getStaffListItemId()));
        builder.where(DQLExpressions.eqValue(DQLExpressions.property(StaffListFakePayment.payment().fromAlias("b")), model.getStaffListFakePayment().getPayment()));
        builder.where(DQLExpressions.eqValue(DQLExpressions.property(StaffListFakePayment.financingSource().fromAlias("b")), model.getStaffListFakePayment().getFinancingSource()));
        if (model.getStaffListFakePayment().getFinancingSourceItem() == null)
            builder.where(DQLExpressions.isNull(DQLExpressions.property(StaffListFakePayment.financingSourceItem().fromAlias("b"))));
        else
            builder.where(DQLExpressions.eqValue(DQLExpressions.property(StaffListFakePayment.financingSourceItem().fromAlias("b")), model.getStaffListFakePayment().getFinancingSourceItem()));

        if (!builder.createStatement(getSession()).list().isEmpty())
        {
            StringBuilder errorBuilder = new StringBuilder("Невозможно добавить выплату для должности ");
            errorBuilder.append(model.getStaffListItem().getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL().getTitle());
            errorBuilder.append(", поскольку выплата ");
            errorBuilder.append(model.getStaffListFakePayment().getPayment().getTitle()).append(" : ");
            errorBuilder.append(model.getStaffListFakePayment().getFinancingSource().getTitle());
            if (model.getStaffListFakePayment().getFinancingSourceItem() != null)
                errorBuilder.append(" (").append(model.getStaffListFakePayment().getFinancingSourceItem().getTitle()).append(")");
            errorBuilder.append(" уже есть в списке выплат.");

            errors.add(errorBuilder.toString());
        }
    }

    @Override
    public void update(Model model)
    {
        getSession().saveOrUpdate(model.getStaffListFakePayment());
        StaffListPaymentsUtil.recalculateAllPaymentsValueForStaffListItem(model.getStaffListItem(), getSession());
    }
}