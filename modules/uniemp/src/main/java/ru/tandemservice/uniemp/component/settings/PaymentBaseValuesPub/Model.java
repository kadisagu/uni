/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniemp.component.settings.PaymentBaseValuesPub;

import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.employee.PaymentToPostBaseValue;

/**
 * @author dseleznev
 * Created on: 29.09.2009
 */
@State(keys = "paymentId", bindings = "paymentId")
public class Model
{
    private Long _paymentId;
    private Payment _payment;
    private DynamicListDataSource<PaymentToPostBaseValue> _dataSource;

    public Long getPaymentId()
    {
        return _paymentId;
    }

    public void setPaymentId(Long paymentId)
    {
        this._paymentId = paymentId;
    }

    public Payment getPayment()
    {
        return _payment;
    }

    public void setPayment(Payment payment)
    {
        this._payment = payment;
    }

    public DynamicListDataSource<PaymentToPostBaseValue> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<PaymentToPostBaseValue> dataSource)
    {
        this._dataSource = dataSource;
    }
}