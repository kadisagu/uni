/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.component.employee.EmployeeServiceLengthAddEdit;

import org.tandemframework.core.component.Input;
import ru.tandemservice.uniemp.entity.catalog.ServiceLengthType;
import ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemFake;

import java.util.List;

/**
 * @author euroelessar
 * @since 05.04.2010
 */
@Input(keys = { "rowId", "employeeId" }, bindings = { "itemId", "employeeId" } )
public class Model
{
    private Long _employeeId;
    private Long _itemId;
    private EmploymentHistoryItemFake _employmentHistoryItemFake;
    private List<ServiceLengthType> _serviceLengthTypeList;
    private ServiceLengthType _serviceLengthType;

    // Getters & Setters

    public ServiceLengthType getServiceLengthType()
    {
        return _serviceLengthType;
    }

    public void setServiceLengthType(ServiceLengthType serviceLengthType)
    {
        _serviceLengthType = serviceLengthType;
    }

    public Long getItemId()
    {
        return _itemId;
    }

    public void setItemId(Long itemId)
    {
        _itemId = itemId;
    }

    public EmploymentHistoryItemFake getEmploymentHistoryItemFake()
    {
        return _employmentHistoryItemFake;
    }

    public void setEmploymentHistoryItemFake(EmploymentHistoryItemFake employmentHistoryItemFake)
    {
        _employmentHistoryItemFake = employmentHistoryItemFake;
    }

    public Long getEmployeeId()
    {
        return _employeeId;
    }

    public void setEmployeeId(Long employeeId)
    {
        _employeeId = employeeId;
    }

    public List<ServiceLengthType> getServiceLengthTypeList()
    {
        return _serviceLengthTypeList;
    }

    public void setServiceLengthTypeList(List<ServiceLengthType> serviceLengthTypeList)
    {
        _serviceLengthTypeList = serviceLengthTypeList;
    }
}
