/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniemp.component.catalog.financingSourceItem.FinancingSourceItemAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import org.tandemframework.shared.organization.base.util.OrgUnitAutocompleteModel;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;

/**
 * @author AutoGenerator
 * Created on 15.12.2010
 */
public class DAO extends DefaultCatalogAddEditDAO<FinancingSourceItem, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setFinancingSourcesList(getList(FinancingSource.class, FinancingSource.code().s()));
        model.setOrgUnitListModel(new OrgUnitAutocompleteModel());
    }

    @Override
    public void update(Model model)
    {
        super.update(model);
    }
}