/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniemp.component.employee.EmployeeSickListAddEdit;

import java.util.List;

import org.tandemframework.core.component.Input;

import ru.tandemservice.uniemp.entity.catalog.SickListBasics;
import ru.tandemservice.uniemp.entity.employee.EmployeeSickList;

/**
 * @author E. Grigoriev
 * @since 08.07.2008
 */
@Input(keys = {"employeeId", "rowId"}, bindings = {"employeeId", "employeeSickList.id"})
public class Model
{
    private Long _employeeId;
    
    private EmployeeSickList _employeeSickList = new EmployeeSickList();
    private List<SickListBasics> _sickListBasics;
    
    public EmployeeSickList getEmployeeSickList()
    {
        return _employeeSickList;
    }
    public void setEmployeeSickList(EmployeeSickList employeeSickList)
    {
        _employeeSickList = employeeSickList;
    }
    public List<SickListBasics> getSickListBasics()
    {
        return _sickListBasics;
    }
    public void setSickListBasics(List<SickListBasics> sickListBasics)
    {
        _sickListBasics = sickListBasics;
    }
    public Long getEmployeeId()
    {
        return _employeeId;
    }
    public void setEmployeeId(Long employeeId)
    {
        _employeeId = employeeId;
    }
}