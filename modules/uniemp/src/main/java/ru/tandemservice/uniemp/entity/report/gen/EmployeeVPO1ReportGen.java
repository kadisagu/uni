package ru.tandemservice.uniemp.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.report.StorableReport;
import ru.tandemservice.uniemp.entity.report.EmployeeVPO1Report;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Отчет «ВПО-1 - Сведения о персонале учреждения»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeVPO1ReportGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.report.EmployeeVPO1Report";
    public static final String ENTITY_NAME = "employeeVPO1Report";
    public static final int VERSION_HASH = -492573807;
    private static IEntityMeta ENTITY_META;

    public static final String P_REPORT_DATE = "reportDate";
    public static final String P_AGE_DATE = "ageDate";

    private Date _reportDate;     // Дата отчета
    private Date _ageDate;     // Рассчитывать возраст сотрудников на дату

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата отчета. Свойство не может быть null.
     */
    @NotNull
    public Date getReportDate()
    {
        return _reportDate;
    }

    /**
     * @param reportDate Дата отчета. Свойство не может быть null.
     */
    public void setReportDate(Date reportDate)
    {
        dirty(_reportDate, reportDate);
        _reportDate = reportDate;
    }

    /**
     * @return Рассчитывать возраст сотрудников на дату. Свойство не может быть null.
     */
    @NotNull
    public Date getAgeDate()
    {
        return _ageDate;
    }

    /**
     * @param ageDate Рассчитывать возраст сотрудников на дату. Свойство не может быть null.
     */
    public void setAgeDate(Date ageDate)
    {
        dirty(_ageDate, ageDate);
        _ageDate = ageDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EmployeeVPO1ReportGen)
        {
            setReportDate(((EmployeeVPO1Report)another).getReportDate());
            setAgeDate(((EmployeeVPO1Report)another).getAgeDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeVPO1ReportGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeVPO1Report.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeVPO1Report();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "reportDate":
                    return obj.getReportDate();
                case "ageDate":
                    return obj.getAgeDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "reportDate":
                    obj.setReportDate((Date) value);
                    return;
                case "ageDate":
                    obj.setAgeDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "reportDate":
                        return true;
                case "ageDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "reportDate":
                    return true;
                case "ageDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "reportDate":
                    return Date.class;
                case "ageDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeeVPO1Report> _dslPath = new Path<EmployeeVPO1Report>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeVPO1Report");
    }
            

    /**
     * @return Дата отчета. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.report.EmployeeVPO1Report#getReportDate()
     */
    public static PropertyPath<Date> reportDate()
    {
        return _dslPath.reportDate();
    }

    /**
     * @return Рассчитывать возраст сотрудников на дату. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.report.EmployeeVPO1Report#getAgeDate()
     */
    public static PropertyPath<Date> ageDate()
    {
        return _dslPath.ageDate();
    }

    public static class Path<E extends EmployeeVPO1Report> extends StorableReport.Path<E>
    {
        private PropertyPath<Date> _reportDate;
        private PropertyPath<Date> _ageDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата отчета. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.report.EmployeeVPO1Report#getReportDate()
     */
        public PropertyPath<Date> reportDate()
        {
            if(_reportDate == null )
                _reportDate = new PropertyPath<Date>(EmployeeVPO1ReportGen.P_REPORT_DATE, this);
            return _reportDate;
        }

    /**
     * @return Рассчитывать возраст сотрудников на дату. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.report.EmployeeVPO1Report#getAgeDate()
     */
        public PropertyPath<Date> ageDate()
        {
            if(_ageDate == null )
                _ageDate = new PropertyPath<Date>(EmployeeVPO1ReportGen.P_AGE_DATE, this);
            return _ageDate;
        }

        public Class getEntityClass()
        {
            return EmployeeVPO1Report.class;
        }

        public String getEntityName()
        {
            return "employeeVPO1Report";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
