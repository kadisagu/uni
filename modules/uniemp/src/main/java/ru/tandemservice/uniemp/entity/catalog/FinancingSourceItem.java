package ru.tandemservice.uniemp.entity.catalog;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;

import ru.tandemservice.uniemp.entity.catalog.gen.FinancingSourceItemGen;

/**
 * Источник финансирования (детально)
 */
public class FinancingSourceItem extends FinancingSourceItemGen implements ITitledWithCases
{
    public static final String P_FULL_TITLE = "fullTitle";

    public String getFullTitle()
    {
        return getTitle() + " (" + getFinancingSource().getShortTitle() + ")";
    }

    @Override
    public String getCaseTitle(GrammaCase caze)
    {
        return TitledWithCasesUtil.getCaseTitle(this, caze);
    }
}