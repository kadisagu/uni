/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.component.vacationShedule.VacationSchedulePub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.services.UniServiceFacade;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.catalog.StaffListState;
import ru.tandemservice.uniemp.entity.employee.VacationSchedule;
import ru.tandemservice.uniemp.entity.employee.VacationScheduleItem;
import ru.tandemservice.unimv.UnimvDefines;
import ru.tandemservice.unimv.UnimvUtil;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimv.entity.visa.VisaTask;
import ru.tandemservice.unimv.services.visatask.ISendToCoordinationService;
import ru.tandemservice.unimv.services.visatask.ITouchVisaTaskHandler;
import ru.tandemservice.unimv.services.visatask.ITouchVisaTaskService;

/**
 * @author dseleznev
 * Created on: 10.01.2011
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("vsi");

    static
    {
        _orderSettings.setOrders(VacationScheduleItem.employeePost().employee().person().fullFio(), new OrderDescription("idCard", IdentityCard.lastName().s()), new OrderDescription("idCard", IdentityCard.firstName().s()), new OrderDescription("idCard", IdentityCard.middleName().s()));
    }

    @Override
    public void prepare(Model model)
    {
        model.setVacationSchedule(getNotNull(VacationSchedule.class, model.getVacationScheduleId()));
        model.setSecModel(new CommonPostfixPermissionModel("vacationSchedule"));
        model.setVisingStatus(UnimvDaoFacade.getVisaDao().getVisingStatus(model.getVacationSchedule()));
    }

    @Override
    public void doSendToCoordination(Model model, IPersistentPersonable initiator)
    {
       //1. надо проверить, что у приказа сейчас не идет процедура согласования
        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(model.getVacationSchedule());
        if (visaTask != null)
            throw new ApplicationException("Нельзя отправить график отпусков на согласование, так как он уже на согласовании.");

        if (StringUtils.isEmpty(model.getVacationSchedule().getNumber()))
            throw new ApplicationException("Нельзя отправить график отпусков на согласование без номера.");

        //2. надо сохранить печатную форму приказа
        //saveListOrderText(model.getVacationSchedule());

        //3. отправляем на согласование
        ISendToCoordinationService sendToCoordinationService = UniServiceFacade.getService(ISendToCoordinationService.SEND_TO_COORDINATION_SERVICE);
        sendToCoordinationService.init(model.getVacationSchedule(), initiator);
        sendToCoordinationService.execute();
    }

    @Override
    public void doSendToFormative(Model model)
    {
        //1. надо проверить, что у приказа сейчас не идет процедура согласования
        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(model.getVacationSchedule());
        if (visaTask != null)
            throw new ApplicationException("Нельзя отправить график отпусков на формирование, так как он на согласовании.");

        //2. надо сменить состояние на формируется
        MQBuilder ssBuilder = new MQBuilder(StaffListState.ENTITY_CLASS, "ss");
        ssBuilder.add(MQExpression.eq("ss", StaffListState.code().s(), UniempDefines.STAFF_LIST_STATUS_FORMING));
        StaffListState formingStatus = (StaffListState)ssBuilder.uniqueResult(getSession());
        model.getVacationSchedule().setState(formingStatus);
        update(model.getVacationSchedule());

    }

    @Override
    public void doReject(Model model)
    {
        ITouchVisaTaskService rejectService = UniServiceFacade.getService(ITouchVisaTaskService.REJECT_VISA_TASK_SERVICE_NAME);
        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(model.getVacationSchedule());
        if (visaTask != null)
        {
            rejectService.init(visaTask.getId(), "SYSTEM: Отменено пользователем с публикатора");
            rejectService.execute();
        } else
        {
            //задачи нет => документ уже согласован,
            //в историю согласования ничего не попадет. так как нет задачи согласования
            //короче запускаем процедуру отрицательного завершения процедуры согласования
            ITouchVisaTaskHandler touchService = UnimvUtil.findTouchService(model.getVacationSchedule(), UnimvDefines.VISA_REJECT_SERVICE_MAP);
            touchService.init(model.getVacationSchedule());
            touchService.execute();
        }
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(VacationScheduleItem.ENTITY_CLASS, "vsi");
        builder.add(MQExpression.eq("vsi", VacationScheduleItem.vacationSchedule().s(), model.getVacationSchedule()));
        builder.addJoinFetch("vsi", VacationScheduleItem.employeePost().s(), "ep");
        builder.addJoinFetch("ep", EmployeePost.employee().s(), "e");
        builder.addJoinFetch("e", Employee.person().s(), "p");
        builder.addJoinFetch("p", Person.identityCard().s(), "idCard");
        _orderSettings.applyOrder(builder, model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }
}