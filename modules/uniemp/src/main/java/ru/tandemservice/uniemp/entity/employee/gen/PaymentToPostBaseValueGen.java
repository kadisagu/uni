package ru.tandemservice.uniemp.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.employee.PaymentToPostBaseValue;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Базовое значение выплаты для должности
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PaymentToPostBaseValueGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.employee.PaymentToPostBaseValue";
    public static final String ENTITY_NAME = "paymentToPostBaseValue";
    public static final int VERSION_HASH = 1821666425;
    private static IEntityMeta ENTITY_META;

    public static final String L_PAYMENT = "payment";
    public static final String L_POST_BOUNDED_WITH_Q_GAND_Q_L = "postBoundedWithQGandQL";
    public static final String P_VALUE = "value";

    private Payment _payment;     // Выплата
    private PostBoundedWithQGandQL _postBoundedWithQGandQL;     // Должность/профессия отнесенная к ПКГ и КУ
    private double _value;     // Базовое значение выплаты

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выплата. Свойство не может быть null.
     */
    @NotNull
    public Payment getPayment()
    {
        return _payment;
    }

    /**
     * @param payment Выплата. Свойство не может быть null.
     */
    public void setPayment(Payment payment)
    {
        dirty(_payment, payment);
        _payment = payment;
    }

    /**
     * @return Должность/профессия отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    @NotNull
    public PostBoundedWithQGandQL getPostBoundedWithQGandQL()
    {
        return _postBoundedWithQGandQL;
    }

    /**
     * @param postBoundedWithQGandQL Должность/профессия отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    public void setPostBoundedWithQGandQL(PostBoundedWithQGandQL postBoundedWithQGandQL)
    {
        dirty(_postBoundedWithQGandQL, postBoundedWithQGandQL);
        _postBoundedWithQGandQL = postBoundedWithQGandQL;
    }

    /**
     * @return Базовое значение выплаты. Свойство не может быть null.
     */
    @NotNull
    public double getValue()
    {
        return _value;
    }

    /**
     * @param value Базовое значение выплаты. Свойство не может быть null.
     */
    public void setValue(double value)
    {
        dirty(_value, value);
        _value = value;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PaymentToPostBaseValueGen)
        {
            setPayment(((PaymentToPostBaseValue)another).getPayment());
            setPostBoundedWithQGandQL(((PaymentToPostBaseValue)another).getPostBoundedWithQGandQL());
            setValue(((PaymentToPostBaseValue)another).getValue());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PaymentToPostBaseValueGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PaymentToPostBaseValue.class;
        }

        public T newInstance()
        {
            return (T) new PaymentToPostBaseValue();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "payment":
                    return obj.getPayment();
                case "postBoundedWithQGandQL":
                    return obj.getPostBoundedWithQGandQL();
                case "value":
                    return obj.getValue();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "payment":
                    obj.setPayment((Payment) value);
                    return;
                case "postBoundedWithQGandQL":
                    obj.setPostBoundedWithQGandQL((PostBoundedWithQGandQL) value);
                    return;
                case "value":
                    obj.setValue((Double) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "payment":
                        return true;
                case "postBoundedWithQGandQL":
                        return true;
                case "value":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "payment":
                    return true;
                case "postBoundedWithQGandQL":
                    return true;
                case "value":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "payment":
                    return Payment.class;
                case "postBoundedWithQGandQL":
                    return PostBoundedWithQGandQL.class;
                case "value":
                    return Double.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PaymentToPostBaseValue> _dslPath = new Path<PaymentToPostBaseValue>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PaymentToPostBaseValue");
    }
            

    /**
     * @return Выплата. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.PaymentToPostBaseValue#getPayment()
     */
    public static Payment.Path<Payment> payment()
    {
        return _dslPath.payment();
    }

    /**
     * @return Должность/профессия отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.PaymentToPostBaseValue#getPostBoundedWithQGandQL()
     */
    public static PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
    {
        return _dslPath.postBoundedWithQGandQL();
    }

    /**
     * @return Базовое значение выплаты. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.PaymentToPostBaseValue#getValue()
     */
    public static PropertyPath<Double> value()
    {
        return _dslPath.value();
    }

    public static class Path<E extends PaymentToPostBaseValue> extends EntityPath<E>
    {
        private Payment.Path<Payment> _payment;
        private PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> _postBoundedWithQGandQL;
        private PropertyPath<Double> _value;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выплата. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.PaymentToPostBaseValue#getPayment()
     */
        public Payment.Path<Payment> payment()
        {
            if(_payment == null )
                _payment = new Payment.Path<Payment>(L_PAYMENT, this);
            return _payment;
        }

    /**
     * @return Должность/профессия отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.PaymentToPostBaseValue#getPostBoundedWithQGandQL()
     */
        public PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
        {
            if(_postBoundedWithQGandQL == null )
                _postBoundedWithQGandQL = new PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL>(L_POST_BOUNDED_WITH_Q_GAND_Q_L, this);
            return _postBoundedWithQGandQL;
        }

    /**
     * @return Базовое значение выплаты. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.PaymentToPostBaseValue#getValue()
     */
        public PropertyPath<Double> value()
        {
            if(_value == null )
                _value = new PropertyPath<Double>(PaymentToPostBaseValueGen.P_VALUE, this);
            return _value;
        }

        public Class getEntityClass()
        {
            return PaymentToPostBaseValue.class;
        }

        public String getEntityName()
        {
            return "paymentToPostBaseValue";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
