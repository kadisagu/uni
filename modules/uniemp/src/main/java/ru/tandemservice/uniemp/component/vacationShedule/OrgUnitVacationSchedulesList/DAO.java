/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.vacationShedule.OrgUnitVacationSchedulesList;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.entity.employee.VacationSchedule;

/**
 * @author dseleznev
 * Created on: 05.01.2011
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("vs");

    @Override
    public void prepare(final Model model)
    {
        if (null != model.getOrgUnitId())
        {
            model.setOrgUnit((OrgUnit)get(model.getOrgUnitId()));
        }
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        IDataSettings settings = model.getSettings();
        Object year = settings.get("year");
        Object number = settings.get("number");
        Object formationDateFrom = settings.get("formationDateFromFilter");
        Object formationDateTo = settings.get("formationDateToFilter");
        Object acceptionDateFrom = settings.get("acceptionDateFromFilter");
        Object acceptionDateTo = settings.get("acceptionDateToFilter");

        MQBuilder builder = new MQBuilder(VacationSchedule.ENTITY_CLASS, "vs");
        builder.add(MQExpression.eq("vs", VacationSchedule.orgUnit().s(), model.getOrgUnit()));

        if (null != year)
        {
            try { builder.add(MQExpression.eq("vs", VacationSchedule.year().s(), Integer.valueOf((String)year))); }
            catch (Exception e) { /* do nothing */ }
        }

        if (null != number)
        {
            builder.add(MQExpression.like("vs", VacationSchedule.number().s(), CoreStringUtils.escapeLike((String)number)));
        }

        if (null != formationDateFrom)
        {
            builder.add(MQExpression.greatOrEq("vs", VacationSchedule.createDate().s(), formationDateFrom));
        }

        if (null != formationDateTo)
        {
            builder.add(MQExpression.lessOrEq("vs", VacationSchedule.createDate().s(), formationDateTo));
        }

        if (null != acceptionDateFrom)
        {
            builder.add(MQExpression.greatOrEq("vs", VacationSchedule.acceptDate().s(), acceptionDateFrom));
        }

        if (null != acceptionDateTo)
        {
            builder.add(MQExpression.lessOrEq("vs", VacationSchedule.acceptDate().s(), acceptionDateTo));
        }

        _orderSettings.applyOrder(builder, model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }
}