/**
 *$Id$
 */
package ru.tandemservice.uniemp.component.employee.EmployeeCombinationPostAddEdit;

import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.common.component.selection.MQListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.info.InfoCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.EtksLevels;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.SalaryRaisingCoefficient;
import org.tandemframework.shared.organization.base.util.OrgUnitAutocompleteModel;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.tapsupport.component.selection.*;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.dao.UniempDAO;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.CombinationPostType;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.employee.*;
import ru.tandemservice.uniemp.util.StaffListPaymentsUtil;

import java.math.BigDecimal;
import java.util.*;

/**
 * Create by ashaburov
 * Date 08.12.11
 */
public class DAO extends UniempDAO<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        if (model.getEntityId() != null)
            model.setEntity(get(CombinationPost.class, model.getEntityId()));
        else
            model.setEntity(new CombinationPost());

        if (model.getEmployeePostId() != null)
            model.setEmployeePost(get(EmployeePost.class, model.getEmployeePostId()));

        if (model.getEntityId() != null)
        {
            MQBuilder builder = new MQBuilder(CombinationPostStaffRateItem.ENTITY_CLASS, "b");
            builder.add(MQExpression.eq("b", CombinationPostStaffRateItem.combinationPost().id().s(), model.getEntityId()));

            model.setCombinationPostStaffRateItemList(builder.<CombinationPostStaffRateItem>getResultList(getSession()));
        }

        model.setOrgUnitModel(new OrgUnitAutocompleteModel());

        model.setPostModel(new BaseSingleSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (model.getEntity().getOrgUnit() == null)
                {
                    return ListResult.getEmpty();
                }

                if (model.getEntity().isFreelance())
                {
                    return new ListResult<>(UniempDaoFacade.getUniempDAO().getPostRelationList(model.getEntity().getOrgUnit(), model.getEntity().getPostBoundedWithQGandQL(), filter, 50));
                }
                else
                {
                    int maxCount = UniempDaoFacade.getStaffListDAO().getFreePostsListForOrgUnitCount(model.getEntity().getOrgUnit(), model.getEntity().getPostBoundedWithQGandQL() != null ? model.getEntity().getPostBoundedWithQGandQL() : null, filter, null, null);
                    return new ListResult<>(UniempDaoFacade.getStaffListDAO().getFreePostsListForOrgUnit(model.getEntity().getOrgUnit(), model.getEntity().getPostBoundedWithQGandQL() != null ? model.getEntity().getPostBoundedWithQGandQL() : null, filter, null, null, 50), maxCount);
                }
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                Object object = get((Long) primaryKey);

                if (object instanceof OrgUnitTypePostRelation) {
                    return ((OrgUnitTypePostRelation) object).getPostBoundedWithQGandQL();
                }
                if (object instanceof PostBoundedWithQGandQL) {
                    return object;
                }

                return null;
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                if (value instanceof OrgUnitTypePostRelation)
                    return ((OrgUnitTypePostRelation)value).getPostBoundedWithQGandQL().getFullTitleWithSalary();

                if (value instanceof PostBoundedWithQGandQL)
                    return ((PostBoundedWithQGandQL)value).getFullTitleWithSalary();

                return "";
            }
        });

        model.setCombinationPostTypeModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(CombinationPostType.class, "t").column("t");
                builder.where(DQLExpressions.like(DQLExpressions.property("t", CombinationPostType.title()), DQLExpressions.value(CoreStringUtils.escapeLike(filter))));
                if (o != null)
                    builder.where(DQLExpressions.eq(DQLExpressions.property("t", CombinationPostType.id()), DQLExpressions.commonValue(o)));
                builder.order(DQLExpressions.property("t", CombinationPostType.title()));

                return new DQLListResultBuilder(builder);
            }
        });

        model.setMissingEmployeePostModel(new FullCheckSelectModel(EmployeePost.P_TITLE_WITH_POST)
        {
            @Override
            public ListResult findValues(String filter)
            {
                if(null == model.getEntity().getPostBoundedWithQGandQL() || null == model.getEntity().getOrgUnit())
                    return ListResult.getEmpty();

                MQBuilder builder = new MQBuilder(EmployeePost.ENTITY_CLASS, "ep");
                builder.add(MQExpression.eq("ep", EmployeePost.postRelation().postBoundedWithQGandQL(), model.getEntity().getPostBoundedWithQGandQL()));
                builder.add(MQExpression.eq("ep", EmployeePost.L_ORG_UNIT, model.getEntity().getOrgUnit()));
                if (model.getEmployeePost() != null)
                    builder.add(MQExpression.notEq("ep", EmployeePost.P_ID, model.getEmployeePost().getId()));
                builder.addOrder("ep", EmployeePost.L_EMPLOYEE + "." + Employee.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_LAST_NAME);
                builder.addOrder("ep", EmployeePost.L_EMPLOYEE + "." + Employee.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_FIRST_NAME);
                return new ListResult<>(builder.<EmployeePost>getResultList(getSession()));
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((EmployeePost) value).getExtendedPostTitle();
            }
        });

        model.setRaisingCoefficientListModel(new FullCheckSelectModel(SalaryRaisingCoefficient.P_FULL_TITLE)
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (null == model.getEntity().getPostBoundedWithQGandQL())
                {
                    return ListResult.getEmpty();
                }
                PostBoundedWithQGandQL pbw = model.getEntity().getPostBoundedWithQGandQL();
                MQBuilder builder = new MQBuilder(SalaryRaisingCoefficient.ENTITY_CLASS, "rc");
                builder.add(MQExpression.eq("rc", SalaryRaisingCoefficient.L_POST, pbw.getPost()));
                builder.add(MQExpression.eq("rc", SalaryRaisingCoefficient.L_QUALIFICATION_LEVEL, pbw.getQualificationLevel()));
                builder.add(MQExpression.like("rc", SalaryRaisingCoefficient.P_TITLE, CoreStringUtils.escapeLike(filter)));
                builder.addOrder("rc", SalaryRaisingCoefficient.P_RAISING_COEFFICIENT);
                builder.addOrder("rc", SalaryRaisingCoefficient.P_TITLE);
                return new ListResult<>(builder.getResultList(getSession()));
            }
        });

        model.setEtksLevelModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                MQBuilder builder = new MQBuilder(EtksLevels.ENTITY_CLASS, "b");
                builder.add(MQExpression.like("b", EtksLevels.title().s(), CoreStringUtils.escapeLike(filter)));
                builder.addOrder("b", EtksLevels.title().s());
                if (o != null)
                    builder.add(MQExpression.eq("b", EtksLevels.id().s(), o));

                return new MQListResultBuilder(builder);
            }
        });

        model.setFinancingSourceModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                if (model.getEntity().getOrgUnit() == null || model.getEntity().getPostBoundedWithQGandQL() == null)
                    return new SimpleListResultBuilder<>(new ArrayList<>());


                if (model.isThereAnyActiveStaffList())
                {
                    StaffList activeStaffList = UniempDaoFacade.getStaffListDAO().getActiveStaffList(model.getEntity().getOrgUnit());

                    MQBuilder builder = new MQBuilder(StaffListItem.ENTITY_CLASS, "b", new String[]{StaffListItem.financingSource().s()});
                    builder.add(MQExpression.eq("b", StaffListItem.staffList().s(), activeStaffList));
                    builder.add(MQExpression.eq("b", StaffListItem.orgUnitPostRelation().orgUnitTypePostRelation().postBoundedWithQGandQL().s(), model.getEntity().getPostBoundedWithQGandQL()));
                    builder.add(MQExpression.like("b", StaffListItem.financingSource().title().s(), CoreStringUtils.escapeLike(filter)));
                    builder.setNeedDistinct(true);
                    if (o != null)
                        builder.add(MQExpression.eq("b",StaffListItem.financingSource().id().s(), o));

                    return new MQListResultBuilder(builder);
                }
                else
                {
                    MQBuilder builder = new MQBuilder(FinancingSource.ENTITY_CLASS, "b");
                    builder.add(MQExpression.like("b", FinancingSource.title().s(), CoreStringUtils.escapeLike(filter)));
                    if (o != null)
                        builder.add(MQExpression.eq("b", FinancingSource.id().s(), o));

                    return new MQListResultBuilder(builder);
                }
            }
        });


        model.setFinancingSourceItemModel(new CommonSingleSelectModel()
        {
            @Override
            @SuppressWarnings("unchecked")
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                long id = model.getStaffRateDataSource().getCurrentEntity().getId();

                final IValueMapHolder finSrcHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSource");
                final Map<Long, FinancingSource> finSrcMap = (null == finSrcHolder ? Collections.emptyMap() : finSrcHolder.getValueMap());

                FinancingSource financingSource = finSrcMap.get(id);

                if (model.getEntity().getOrgUnit() == null || model.getEntity().getPostBoundedWithQGandQL() == null || financingSource == null)
                    return new SimpleListResultBuilder<>(new ArrayList<>());

                if (model.isThereAnyActiveStaffList())
                {
                    StaffList staffList = UniempDaoFacade.getStaffListDAO().getActiveStaffList(model.getEntity().getOrgUnit());

                    MQBuilder builder = new MQBuilder(StaffListItem.ENTITY_CLASS, "b", new String[] {StaffListItem.L_FINANCING_SOURCE_ITEM});
                    builder.add(MQExpression.eq("b", StaffListItem.staffList().s(), staffList));
                    builder.add(MQExpression.eq("b", StaffListItem.orgUnitPostRelation().orgUnitTypePostRelation().postBoundedWithQGandQL().s(), model.getEntity().getPostBoundedWithQGandQL()));
                    builder.add(MQExpression.eq("b", StaffListItem.financingSource().s(), financingSource));
                    builder.add(MQExpression.isNotNull("b", StaffListItem.financingSourceItem().s()));
                    builder.add(MQExpression.like("b", StaffListItem.financingSourceItem().title().s(), CoreStringUtils.escapeLike(filter)));
                    if (o != null)
                        builder.add(MQExpression.eq("b", StaffListItem.financingSourceItem().id().s(), o));
                    builder.setNeedDistinct(true);

                    return new MQListResultBuilder(builder);
                }
                else
                {
                    MQBuilder builder = new MQBuilder(FinancingSourceItem.ENTITY_CLASS, "b");
                    builder.add(MQExpression.eq("b", FinancingSourceItem.financingSource().id().s(), financingSource.getId()));
                    builder.add(MQExpression.like("b", FinancingSourceItem.P_TITLE, CoreStringUtils.escapeLike(filter)));
                    if (o != null)
                        builder.add(MQExpression.eq("b", FinancingSourceItem.id().s(), o));

                    return new MQListResultBuilder(builder);
                }
            }
        });

        model.setEmployeeHRModel(new BaseMultiSelectModel()
        {
            @Override
            public List getValues(Set primaryKeys)
            {
                List<IdentifiableWrapper> objects = findValues("").getObjects();
                List<IdentifiableWrapper> resultList = new ArrayList<>();

                for (IdentifiableWrapper wrapper : objects)
                {
                    if (primaryKeys.contains(wrapper.getId()))
                        resultList.add(wrapper);
                }

                return resultList;
            }

            @Override
            @SuppressWarnings("unchecked")
            public ListResult<IdentifiableWrapper> findValues(String filter)
            {
                String newStaffRateStr = "<Новая ставка> - ";
                long id = model.getStaffRateDataSource().getCurrentEntity().getId();

                final IValueMapHolder finSrcHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSource");
                final Map<Long, FinancingSource> finSrcMap = (null == finSrcHolder ? Collections.emptyMap() : finSrcHolder.getValueMap());

                final IValueMapHolder finSrcItmHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSourceItem");
                final Map<Long, FinancingSourceItem> finSrcItmMap = (null == finSrcItmHolder ? Collections.emptyMap() : finSrcItmHolder.getValueMap());

                FinancingSource financingSource = finSrcMap.get(id);
                FinancingSourceItem financingSourceItem = finSrcItmMap.get(id);

                if (financingSource == null || model.getEntity().getOrgUnit() == null || model.getEntity().getPostBoundedWithQGandQL() == null)
                    return ListResult.getEmpty();

                Set<IdentifiableWrapper> resultList = new HashSet<>();
                List<StaffListAllocationItem> staffListAllocList = new ArrayList<>();

                StaffList staffList = UniempDaoFacade.getStaffListDAO().getActiveStaffList(model.getEntity().getOrgUnit());
                List<StaffListAllocationItem> allocationItemList = UniempDaoFacade.getStaffListDAO().getStaffListAllocationItemsList(staffList, model.getEntity().getPostBoundedWithQGandQL());
                for (StaffListAllocationItem item : allocationItemList)
                {
                    if (item.getStaffListItem().getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL().equals(model.getEntity().getPostBoundedWithQGandQL()))
                        if (financingSource.equals(item.getFinancingSource()))
                            if ((financingSourceItem == null && item.getFinancingSourceItem() == null) || (financingSourceItem != null && financingSourceItem.equals(item.getFinancingSourceItem())))
                                if ((item.getEmployeePost() != null && !item.getEmployeePost().getPostStatus().isActive()))
                                    staffListAllocList.add(item);
                }
//                if (allocationItemList.size() > 0)
//                    if (allocationItemList.get(0).getStaffListItem().getStaffRate() - allocationItemList.get(0).getStaffListItem().getOccStaffRate() <= 0)
//                        staffListAllocList = new LinkedList<StaffListAllocationItem>();

                Double summStaffRate = 0.0d;
                for (StaffListAllocationItem item : staffListAllocList)
                    summStaffRate += item.getStaffRate();

                if (UniempDaoFacade.getStaffListDAO().isPossibleAddNewStaffRate(model.getEntity().getOrgUnit(), model.getEntity().getPostBoundedWithQGandQL(), financingSource, financingSourceItem))
                {
                    StaffListItem staffListItem = UniempDaoFacade.getStaffListDAO().getStaffListItem(model.getEntity().getOrgUnit(), model.getEntity().getPostBoundedWithQGandQL(), financingSource, financingSourceItem);
                    Double diff = 0.0d;
                    if (staffListItem != null)
                        diff = staffListItem.getStaffRate() - staffListItem.getOccStaffRate() - summStaffRate;
                    if (diff > 0 && newStaffRateStr.contains(filter))
                        resultList.add(new IdentifiableWrapper(0L, newStaffRateStr + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(diff)));
                }

                if (model.getEntityId() != null)
                {
                    for (StaffListAllocationItem allocationItem : UniempDaoFacade.getStaffListDAO().getStaffListAllocationItemList(UniempDaoFacade.getStaffListDAO().getActiveStaffList(model.getEntity().getOrgUnit()), model.getEntity(), model.getEntity().getPostBoundedWithQGandQL(), financingSource, financingSourceItem))
                        if (allocationItem.getCombinationPost() != null && allocationItem.getCombinationPost().equals(model.getEntity()))
                            resultList.add(new IdentifiableWrapper(allocationItem.getId(), allocationItem.getEmployeePost().getPerson().getFio() + ", ставка - " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(allocationItem.getStaffRate()) + " - " + allocationItem.getCombinationPost().getCombinationPostType().getShortTitle() + " " + allocationItem.getEmployeePost().getPostStatus().getShortTitle()));
                }

                for (StaffListAllocationItem item : staffListAllocList)
                {
                    if (item.getEmployeePost().getPerson().getFullFio().contains(filter))
                        resultList.add(new IdentifiableWrapper(item.getId(), item.getEmployeePost().getPerson().getFio() + ", ставка - " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(item.getStaffRate()) + " - " + item.getEmployeePost().getPostType().getShortTitle() + " " + item.getEmployeePost().getPostStatus().getShortTitle()));
                }

                return new ListResult<>(resultList);
            }
        });
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareStaffRateDataSource(Model model)
    {
        UniBaseUtils.createPage(model.getStaffRateDataSource(), model.getCombinationPostStaffRateItemList());
        model.setNeedUpdateDataSource(false);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void validate(Model model, ErrorCollector errors)
    {
        //final IValueMapHolder staffRateHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("staffRate");
        //final Map<Long, Double> staffRateMap = (null == staffRateHolder ? Collections.emptyMap() : staffRateHolder.getValueMap());
        //final IValueMapHolder finSrcHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSource");
        //final Map<Long, FinancingSource> finSrcMap = (null == finSrcHolder ? Collections.emptyMap() : finSrcHolder.getValueMap());
        //final IValueMapHolder finSrcItemHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSourceItem");
        //final Map<Long, FinancingSourceItem> finSrcItemMap = (null == finSrcItemHolder ? Collections.emptyMap() : finSrcItemHolder.getValueMap());
        final IValueMapHolder empHRHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("employeeHR");
        final Map<Long, List<IdentifiableWrapper>> empHRMap = (null == empHRHolder ? Collections.emptyMap() : empHRHolder.getValueMap());

        //если была доступна колонка Кадровой расстановки, достаем из враперов объекты Штатной расстановки
        final Map<Long, List<StaffListAllocationItem>> allocItemsMap = new HashMap<>();
        //Мапа, указывающая на то выбрана ли строчка <Новая ставка> в мультиселекте
        Map<Long, Boolean> hasNewStaffRateMap = new HashMap<>();
        if (model.isThereAnyActiveStaffList())
        {
            for (IEntity entity : model.getCombinationPostStaffRateItemList())
            {
                List<StaffListAllocationItem> allocationItemList = new ArrayList<>();
                for (IdentifiableWrapper wrapper : empHRMap.get(entity.getId()))
                {
                    if (wrapper.getId() != 0L)
                    {
                        StaffListAllocationItem item = IUniBaseDao.instance.get().get(StaffListAllocationItem.class, wrapper.getId());
                        allocationItemList.add(item);
                    }

                }

                allocItemsMap.put(entity.getId(), allocationItemList);

                hasNewStaffRateMap.put(entity.getId(), empHRMap.get(entity.getId()).contains(new IdentifiableWrapper(0L, "")));
            }
        }

        if (model.getCombinationPostStaffRateItemList().isEmpty())
            errors.add("Необходимо указать ставку для должности по совмещению.");

        for (CombinationPostStaffRateItem item : model.getCombinationPostStaffRateItemList())
            for (CombinationPostStaffRateItem item2nd : model.getCombinationPostStaffRateItemList())
                if (!item.equals(item2nd))
                    if (item.getFinancingSource().equals(item2nd.getFinancingSource()))
                        if ((item.getFinancingSourceItem() == null && item2nd.getFinancingSourceItem() == null) ||
                                ((item.getFinancingSourceItem() != null && item2nd.getFinancingSourceItem() != null) && item.getFinancingSourceItem().equals(item2nd.getFinancingSourceItem())))
                            errors.add("Набор полей «Источник финансирования», «Источник финансирования (детально)» должен быть уникальным в рамках должности по совмещению.",
                                    "finSrc_id_" + item.getId(), "finSrcItm_id_" + item.getId());

        if (model.isThereAnyActiveStaffList() && !errors.hasErrors())
            for (CombinationPostStaffRateItem staffRateItem : model.getCombinationPostStaffRateItemList())
            {
                StaffListItem staffListItem = UniempDaoFacade.getStaffListDAO().getStaffListItem(model.getEntity().getOrgUnit(), model.getEntity().getPostBoundedWithQGandQL(), staffRateItem.getFinancingSource(), staffRateItem.getFinancingSourceItem());
                Double diff = 0.0d;
                if (staffListItem != null)
                    diff = staffListItem.getStaffRate() - staffListItem.getOccStaffRate();
                BigDecimal x = new java.math.BigDecimal(diff);
                x = x.setScale(3, BigDecimal.ROUND_HALF_UP);
                diff = x.doubleValue();

                for (StaffListAllocationItem allocationItem : UniempDaoFacade.getStaffListDAO().getStaffListItemAllocations(staffListItem))
                    if (allocationItem.getEmployeePost() != null && !allocationItem.getEmployeePost().getPostStatus().isActive() &&
                            !allocItemsMap.get(staffRateItem.getId()).contains(allocationItem))
                        diff -= allocationItem.getStaffRate();

                Double sumStaffRateAllocItems = 0.0d;
                for (StaffListAllocationItem allocItem : allocItemsMap.get(staffRateItem.getId()))
                {
                    sumStaffRateAllocItems += allocItem.getStaffRate();

                    //если это ставка выбранной должности по совмещению, то прибавляем ее к свободным ставкам, что бы она не учитывалась в проверке
                    if ((allocItem.getCombinationPost() != null && allocItem.getCombinationPost().equals(model.getEntity()) && allocItem.getEmployeePost().equals(model.getEmployeePost()))
                            && allocItem.getEmployeePost().getPostStatus().isActive())
                        diff += allocItem.getStaffRate();
                }

                if ((staffRateItem.getStaffRate() > sumStaffRateAllocItems && !hasNewStaffRateMap.get(staffRateItem.getId())) ||
                        (staffRateItem.getStaffRate() > diff && hasNewStaffRateMap.get(staffRateItem.getId())))
                    errors.add("Размер ставки " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(staffRateItem.getStaffRate()) + " " + staffRateItem.getFinancingSource().getTitle() +
                            " превышает суммарную ставку, выбранную в поле «Кадровая расстановка».",
                            "staffRate_id_" + staffRateItem.getId());

                if (staffRateItem.getStaffRate() <= sumStaffRateAllocItems && hasNewStaffRateMap.get(staffRateItem.getId()))
                    errors.add("Необходимо точнее указать занимаемые ставки в поле «Кадровая расстановка» для "
                            + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(staffRateItem.getStaffRate()) + " " + staffRateItem.getFinancingSource().getTitle() + ".",
                            "employeeHR_Id_" + staffRateItem.getId());
            }

        Double combinationStaffRate = 0d;
        for (CombinationPostStaffRateItem staffRateItem : model.getCombinationPostStaffRateItemList())
            combinationStaffRate += staffRateItem.getStaffRate();

        Double employeeStaffRate = 0d;
        for (EmployeePostStaffRateItem staffRateItem : UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(model.getEmployeePost()))
            employeeStaffRate += staffRateItem.getStaffRate();

        if (combinationStaffRate > employeeStaffRate)
            errors.add("Cтавка должности по совмещению сотрудника превышает суммарный размер ставки основной должности сотрудника.");

        if (model.getEntity().getEndDate() != null)
        {
            if (model.getEntity().getBeginDate().getTime() > model.getEntity().getEndDate().getTime())
                errors.add("Дата начала не может быть больше даты окончания.", "beginDate", "endDate");
        }
    }

    @Override
    public void validateCombinationPeriods(Model model, InfoCollector infoCollector)
    {
        if (model.getEntity().getOrgUnit() == null || model.getEntity().getPostBoundedWithQGandQL() == null || model.getEntity().getBeginDate() == null)
            return;

        MQBuilder builder = new MQBuilder(CombinationPost.ENTITY_CLASS, "b");
        builder.add(MQExpression.eq("b", CombinationPost.employeePost().s(), model.getEmployeePost()));
        builder.add(MQExpression.eq("b", CombinationPost.orgUnit().s(), model.getEntity().getOrgUnit()));
        builder.add(MQExpression.eq("b", CombinationPost.postBoundedWithQGandQL().s(), model.getEntity().getPostBoundedWithQGandQL()));
        if (model.getEntityId() != null)
            builder.add(MQExpression.notEq("b", CombinationPost.id().s(), model.getEntityId()));

        Date beginDate = model.getEntity().getBeginDate();
        Date endDate = model.getEntity().getEndDate();

        for (CombinationPost combinationPost : builder.<CombinationPost>getResultList(getSession()))
        {
            Date begin = combinationPost.getBeginDate();
            Date end = combinationPost.getEndDate();

            if (end != null)
            {
                if (endDate != null)
                {
                    if (CommonBaseDateUtil.isBetween(beginDate, begin, end) || CommonBaseDateUtil.isBetween(endDate, begin, end))
                        infoCollector.add("Должность " + combinationPost.getPostBoundedWithQGandQL().getFullTitle() + " (период работы с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(begin) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(end) + ") уже добавлена в список должностей по совмещению.");
                   else {
                        if (CommonBaseDateUtil.isBetween(begin, beginDate, endDate) || CommonBaseDateUtil.isBetween(end, beginDate, endDate))
                             infoCollector.add("Должность " + combinationPost.getPostBoundedWithQGandQL().getFullTitle() + " (период работы с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(begin) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(end) + ") уже добавлена в список должностей по совмещению.");
                    }
                }
                else
                {
                    if (CommonBaseDateUtil.isBefore(beginDate, end) || beginDate.equals(end))
                        infoCollector.add("Должность " + combinationPost.getPostBoundedWithQGandQL().getFullTitle() + " (период работы с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(begin) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(end) + ") уже добавлена в список должностей по совмещению.");
                }
            }
            else
            {
                if (endDate != null)
                {
                    if (CommonBaseDateUtil.isAfter(endDate, begin) || endDate.equals(begin))
                        infoCollector.add("Должность " + combinationPost.getPostBoundedWithQGandQL().getFullTitle() + " (период работы с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(begin) + ") уже добавлена в список должностей по совмещению.");
                }
                else
                {
                    infoCollector.add("Должность " + combinationPost.getPostBoundedWithQGandQL().getFullTitle() + " (период работы с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(begin) + ") уже добавлена в список должностей по совмещению.");
                }
            }
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void update(Model model)
    {
        model.getEntity().setEmployeePost(model.getEmployeePost());
        saveOrUpdate(model.getEntity());

        /*Сохраняем ставки должности по совмещению*/

        //если редактируем должность, то перед тем как создать новые ставки удаляем старые
        //если редактируем должность, то перед тем как создать новые удаляем старые ставки этого совмещения в ШР
        if (model.getEntityId() != null)
        {
            MQBuilder builder = new MQBuilder(CombinationPostStaffRateItem.ENTITY_CLASS, "b");
            builder.add(MQExpression.eq("b", CombinationPostStaffRateItem.combinationPost().s(), model.getEntity()));

            MQBuilder allocBuilder = new MQBuilder(StaffListAllocationItem.ENTITY_CLASS, "a");
            allocBuilder.add(MQExpression.eq("a", StaffListAllocationItem.staffListItem().staffList().s(), UniempDaoFacade.getStaffListDAO().getActiveStaffList(model.getEntity().getOrgUnit())));
            allocBuilder.add(MQExpression.eq("a", StaffListAllocationItem.employeePost().s(), model.getEmployeePost()));
            allocBuilder.add(MQExpression.eq("a", StaffListAllocationItem.combinationPost().s(), model.getEntity()));
            allocBuilder.add(MQExpression.eq("a", StaffListAllocationItem.combination().s(), true));

            for (CombinationPostStaffRateItem item : builder.<CombinationPostStaffRateItem>getResultList(getSession()))
                delete(item);

            for (StaffListAllocationItem item  : allocBuilder.<StaffListAllocationItem>getResultList(getSession()))
                delete(item);
        }

        //сохраняем список id ставок перед их сохранением, т.к. после сохранения они поменяются
        List<Long> staffRateOldIdList = CommonBaseEntityUtil.getIdList(model.getCombinationPostStaffRateItemList());

        //создаем указанные ставки
        for (CombinationPostStaffRateItem item : model.getCombinationPostStaffRateItemList())
        {
            if (item.getCombinationPost() != null && item.getFinancingSource() != null )
                save(item);
        }

        /*Сохраняем ссылки на сотрудника в ШР*/

        if (model.isThereAnyActiveStaffList())
        {
            //достаем элементы штатной расстановки, которые выбраны в мультиселектах
            final IValueMapHolder empHRHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("employeeHR");
            final Map<Long, List<IdentifiableWrapper>> empHRMap = (null == empHRHolder ? Collections.emptyMap() : empHRHolder.getValueMap());

            //Мапа, id энтини сечлиста на выбранные в мультиселекте объекты штатной расстановки
            final Map<Long, List<StaffListAllocationItem>> allocItemsMap = new HashMap<>();

            //достаем из враперов объекты Штатной расстановки
            for (Long entityId : staffRateOldIdList)
            {
                List<StaffListAllocationItem> allocationItemList = new ArrayList<>();
                for (IdentifiableWrapper wrapper : empHRMap.get(entityId))
                {
                    if (wrapper.getId() != 0L)
                    {
                        StaffListAllocationItem item = IUniBaseDao.instance.get().get(StaffListAllocationItem.class, wrapper.getId());
                        if (item != null)
                            allocationItemList.add(item);
                    }
                }
                allocItemsMap.put(entityId, allocationItemList);
            }

            //удаляем выбранные ставки с неакт. сотрудниками, если они выбранны
            for (Long entityId : staffRateOldIdList)
                for (StaffListAllocationItem allocItem : allocItemsMap.get(entityId))
                    delete(allocItem);

            for (CombinationPostStaffRateItem staffRateItem : model.getCombinationPostStaffRateItemList())
            {
                //достаем должность ШР
                StaffListItem staffListItem = UniempDaoFacade.getStaffListDAO().getStaffListItem(model.getEntity().getOrgUnit(), model.getEntity().getPostBoundedWithQGandQL(), staffRateItem.getFinancingSource(), staffRateItem.getFinancingSourceItem());

                //создаем новую ставку ШР
                StaffListAllocationItem allocationItem = new StaffListAllocationItem();

                //заполняем поля ставки
                allocationItem.setStaffRate(staffRateItem.getStaffRate());
                allocationItem.setFinancingSource(staffRateItem.getFinancingSource());
                allocationItem.setFinancingSourceItem(staffRateItem.getFinancingSourceItem());
                allocationItem.setStaffListItem(staffListItem);
                allocationItem.setEmployeePost(model.getEmployeePost());
                allocationItem.setRaisingCoefficient(model.getEntity().getSalaryRaisingCoefficient());
                if (allocationItem.getRaisingCoefficient() != null)
                    allocationItem.setMonthBaseSalaryFund(allocationItem.getRaisingCoefficient().getRecommendedSalary() * allocationItem.getStaffRate());
                else if (allocationItem.getStaffListItem() != null)
                    allocationItem.setMonthBaseSalaryFund(allocationItem.getStaffListItem().getSalary() * allocationItem.getStaffRate());
                allocationItem.setReserve(false);
                allocationItem.setCombination(true);
                allocationItem.setCombinationPost(model.getEntity());

                saveOrUpdate(allocationItem);

                StaffListPaymentsUtil.recalculateAllPaymentsValue(allocationItem, getSession());
            }
        }
    }
}
