/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffList.StaffListAddEdit;

import org.tandemframework.core.component.Input;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniemp.entity.employee.StaffList;

/**
 * @author dseleznev
 * Created on: 19.09.2008
 */
@Input(keys = { "orgUnitId", "staffListId", "copyStaffList" }, bindings = { "orgUnitId", "staffListId", "copyStaffList" })
public class Model
{
    private Long _orgUnitId;
    private OrgUnit _orgUnit;

    private Long _staffListId;
    private StaffList _staffList = new StaffList();
    
    private Boolean _copyStaffList;
    private StaffList _baseStaffList;
    
    private Boolean _addNewRequiredPayments;
    private Boolean _removeNonActivePayments;
    
    private Boolean _addNewRequiredPaymentsVisible = false;
    private Boolean _removeNonActivePaymentsVisible = false;

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        this._orgUnitId = orgUnitId;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        this._orgUnit = orgUnit;
    }

    public Long getStaffListId()
    {
        return _staffListId;
    }

    public void setStaffListId(Long staffListId)
    {
        this._staffListId = staffListId;
    }

    public StaffList getStaffList()
    {
        return _staffList;
    }

    public void setStaffList(StaffList staffList)
    {
        this._staffList = staffList;
    }

    public Boolean getCopyStaffList()
    {
        return _copyStaffList;
    }

    public void setCopyStaffList(Boolean copyStaffList)
    {
        this._copyStaffList = copyStaffList;
    }

    public StaffList getBaseStaffList()
    {
        return _baseStaffList;
    }

    public void setBaseStaffList(StaffList baseStaffList)
    {
        this._baseStaffList = baseStaffList;
    }

    public Boolean getAddNewRequiredPayments()
    {
        return _addNewRequiredPayments;
    }

    public void setAddNewRequiredPayments(Boolean addNewRequiredPayments)
    {
        this._addNewRequiredPayments = addNewRequiredPayments;
    }

    public Boolean getRemoveNonActivePayments()
    {
        return _removeNonActivePayments;
    }

    public void setRemoveNonActivePayments(Boolean removeNonActivePayments)
    {
        this._removeNonActivePayments = removeNonActivePayments;
    }

    public Boolean getAddNewRequiredPaymentsVisible()
    {
        return _addNewRequiredPaymentsVisible;
    }

    public void setAddNewRequiredPaymentsVisible(Boolean addNewRequiredPaymentsVisible)
    {
        this._addNewRequiredPaymentsVisible = addNewRequiredPaymentsVisible;
    }

    public Boolean getRemoveNonActivePaymentsVisible()
    {
        return _removeNonActivePaymentsVisible;
    }

    public void setRemoveNonActivePaymentsVisible(Boolean removeNonActivePaymentsVisible)
    {
        this._removeNonActivePaymentsVisible = removeNonActivePaymentsVisible;
    }
}