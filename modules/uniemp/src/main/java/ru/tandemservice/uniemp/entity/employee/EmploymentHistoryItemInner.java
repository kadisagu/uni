package ru.tandemservice.uniemp.entity.employee;

import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uniemp.entity.employee.gen.EmploymentHistoryItemInnerGen;

public class EmploymentHistoryItemInner extends EmploymentHistoryItemInnerGen
{
    @Override
    public String getOrganizationTitle()
    {
        return TopOrgUnit.getInstance().getTitle();
    }

    @Override
    public String getOrgUnitTitle()
    {
        return null != getOrgUnit() ? getOrgUnit().getFullTitle() : null;
    }

    @Override
    public String getPostTitle()
    {
        return getPostBoundedWithQGandQL().getTitle();
    }
}