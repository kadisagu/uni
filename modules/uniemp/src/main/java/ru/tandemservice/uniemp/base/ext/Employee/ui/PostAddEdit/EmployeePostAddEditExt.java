/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniemp.base.ext.Employee.ui.PostAddEdit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.block.BlockListExtension;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostAddEdit.EmployeePostAddEdit;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostAddEdit.IOnUpdateEmployeePostExt;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostAddEdit.IOnValidateEmployeePostExt;

/**
 * @author Vasily Zhukov
 * @since 02.04.2012
 */
@Configuration
public class EmployeePostAddEditExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "uniemp" + EmployeePostAddEditExtUI.class.getSimpleName();

    @Autowired
    private EmployeePostAddEdit _employeePostAddEdit;

    public static final String EMPLOYEE_POST_STAFF_RATE = "staffRate";

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_employeePostAddEdit.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, EmployeePostAddEditExtUI.class))
                .create();
    }

    @Bean
    public ItemListExtension<IOnValidateEmployeePostExt> validateExtItemList()
    {
        return itemListExtension(_employeePostAddEdit.validateExtItemList())
                .add(EmployeePostAddEditExtUI.class.getName(), new ValidateExt())
                .create();
    }

    @Bean
    public ItemListExtension<IOnUpdateEmployeePostExt> updateExtItemList()
    {
        return itemListExtension(_employeePostAddEdit.updateExtItemList())
                .add(EmployeePostAddEditExtUI.class.getName(), new UpdateExt())
                .create();
    }

    @Bean
    public BlockListExtension blockListExtension()
    {
        return blockListExtensionBuilder(_employeePostAddEdit.employeePostAddEditBlockListExtPoint())
                .addBlock(htmlBlock(EMPLOYEE_POST_STAFF_RATE, "EmployeePostAddEdit_Data_StaffRate").after(EmployeePostAddEdit.EMPLOYEE_POST_ATTRIBUTES))
                .create();
    }
}
