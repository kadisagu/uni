package ru.tandemservice.uniemp.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.employee.StaffListItem;
import ru.tandemservice.uniemp.entity.employee.StaffListPaymentBase;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Базовая выплата штатного расписания
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StaffListPaymentBaseGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.employee.StaffListPaymentBase";
    public static final String ENTITY_NAME = "staffListPaymentBase";
    public static final int VERSION_HASH = -78622194;
    private static IEntityMeta ENTITY_META;

    public static final String L_STAFF_LIST_ITEM = "staffListItem";
    public static final String L_PAYMENT = "payment";
    public static final String P_AMOUNT = "amount";
    public static final String L_FINANCING_SOURCE = "financingSource";
    public static final String L_FINANCING_SOURCE_ITEM = "financingSourceItem";

    private StaffListItem _staffListItem;     // Должность штатного расписания
    private Payment _payment;     // Выплата
    private double _amount;     // Значение выплаты
    private FinancingSource _financingSource;     // Источник выплаты
    private FinancingSourceItem _financingSourceItem;     // Источник выплаты (детально)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Должность штатного расписания. Свойство не может быть null.
     */
    @NotNull
    public StaffListItem getStaffListItem()
    {
        return _staffListItem;
    }

    /**
     * @param staffListItem Должность штатного расписания. Свойство не может быть null.
     */
    public void setStaffListItem(StaffListItem staffListItem)
    {
        dirty(_staffListItem, staffListItem);
        _staffListItem = staffListItem;
    }

    /**
     * @return Выплата. Свойство не может быть null.
     */
    @NotNull
    public Payment getPayment()
    {
        return _payment;
    }

    /**
     * @param payment Выплата. Свойство не может быть null.
     */
    public void setPayment(Payment payment)
    {
        dirty(_payment, payment);
        _payment = payment;
    }

    /**
     * @return Значение выплаты. Свойство не может быть null.
     */
    @NotNull
    public double getAmount()
    {
        return _amount;
    }

    /**
     * @param amount Значение выплаты. Свойство не может быть null.
     */
    public void setAmount(double amount)
    {
        dirty(_amount, amount);
        _amount = amount;
    }

    /**
     * @return Источник выплаты. Свойство не может быть null.
     */
    @NotNull
    public FinancingSource getFinancingSource()
    {
        return _financingSource;
    }

    /**
     * @param financingSource Источник выплаты. Свойство не может быть null.
     */
    public void setFinancingSource(FinancingSource financingSource)
    {
        dirty(_financingSource, financingSource);
        _financingSource = financingSource;
    }

    /**
     * @return Источник выплаты (детально).
     */
    public FinancingSourceItem getFinancingSourceItem()
    {
        return _financingSourceItem;
    }

    /**
     * @param financingSourceItem Источник выплаты (детально).
     */
    public void setFinancingSourceItem(FinancingSourceItem financingSourceItem)
    {
        dirty(_financingSourceItem, financingSourceItem);
        _financingSourceItem = financingSourceItem;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StaffListPaymentBaseGen)
        {
            setStaffListItem(((StaffListPaymentBase)another).getStaffListItem());
            setPayment(((StaffListPaymentBase)another).getPayment());
            setAmount(((StaffListPaymentBase)another).getAmount());
            setFinancingSource(((StaffListPaymentBase)another).getFinancingSource());
            setFinancingSourceItem(((StaffListPaymentBase)another).getFinancingSourceItem());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StaffListPaymentBaseGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StaffListPaymentBase.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("StaffListPaymentBase is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "staffListItem":
                    return obj.getStaffListItem();
                case "payment":
                    return obj.getPayment();
                case "amount":
                    return obj.getAmount();
                case "financingSource":
                    return obj.getFinancingSource();
                case "financingSourceItem":
                    return obj.getFinancingSourceItem();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "staffListItem":
                    obj.setStaffListItem((StaffListItem) value);
                    return;
                case "payment":
                    obj.setPayment((Payment) value);
                    return;
                case "amount":
                    obj.setAmount((Double) value);
                    return;
                case "financingSource":
                    obj.setFinancingSource((FinancingSource) value);
                    return;
                case "financingSourceItem":
                    obj.setFinancingSourceItem((FinancingSourceItem) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "staffListItem":
                        return true;
                case "payment":
                        return true;
                case "amount":
                        return true;
                case "financingSource":
                        return true;
                case "financingSourceItem":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "staffListItem":
                    return true;
                case "payment":
                    return true;
                case "amount":
                    return true;
                case "financingSource":
                    return true;
                case "financingSourceItem":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "staffListItem":
                    return StaffListItem.class;
                case "payment":
                    return Payment.class;
                case "amount":
                    return Double.class;
                case "financingSource":
                    return FinancingSource.class;
                case "financingSourceItem":
                    return FinancingSourceItem.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StaffListPaymentBase> _dslPath = new Path<StaffListPaymentBase>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StaffListPaymentBase");
    }
            

    /**
     * @return Должность штатного расписания. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListPaymentBase#getStaffListItem()
     */
    public static StaffListItem.Path<StaffListItem> staffListItem()
    {
        return _dslPath.staffListItem();
    }

    /**
     * @return Выплата. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListPaymentBase#getPayment()
     */
    public static Payment.Path<Payment> payment()
    {
        return _dslPath.payment();
    }

    /**
     * @return Значение выплаты. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListPaymentBase#getAmount()
     */
    public static PropertyPath<Double> amount()
    {
        return _dslPath.amount();
    }

    /**
     * @return Источник выплаты. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListPaymentBase#getFinancingSource()
     */
    public static FinancingSource.Path<FinancingSource> financingSource()
    {
        return _dslPath.financingSource();
    }

    /**
     * @return Источник выплаты (детально).
     * @see ru.tandemservice.uniemp.entity.employee.StaffListPaymentBase#getFinancingSourceItem()
     */
    public static FinancingSourceItem.Path<FinancingSourceItem> financingSourceItem()
    {
        return _dslPath.financingSourceItem();
    }

    public static class Path<E extends StaffListPaymentBase> extends EntityPath<E>
    {
        private StaffListItem.Path<StaffListItem> _staffListItem;
        private Payment.Path<Payment> _payment;
        private PropertyPath<Double> _amount;
        private FinancingSource.Path<FinancingSource> _financingSource;
        private FinancingSourceItem.Path<FinancingSourceItem> _financingSourceItem;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Должность штатного расписания. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListPaymentBase#getStaffListItem()
     */
        public StaffListItem.Path<StaffListItem> staffListItem()
        {
            if(_staffListItem == null )
                _staffListItem = new StaffListItem.Path<StaffListItem>(L_STAFF_LIST_ITEM, this);
            return _staffListItem;
        }

    /**
     * @return Выплата. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListPaymentBase#getPayment()
     */
        public Payment.Path<Payment> payment()
        {
            if(_payment == null )
                _payment = new Payment.Path<Payment>(L_PAYMENT, this);
            return _payment;
        }

    /**
     * @return Значение выплаты. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListPaymentBase#getAmount()
     */
        public PropertyPath<Double> amount()
        {
            if(_amount == null )
                _amount = new PropertyPath<Double>(StaffListPaymentBaseGen.P_AMOUNT, this);
            return _amount;
        }

    /**
     * @return Источник выплаты. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListPaymentBase#getFinancingSource()
     */
        public FinancingSource.Path<FinancingSource> financingSource()
        {
            if(_financingSource == null )
                _financingSource = new FinancingSource.Path<FinancingSource>(L_FINANCING_SOURCE, this);
            return _financingSource;
        }

    /**
     * @return Источник выплаты (детально).
     * @see ru.tandemservice.uniemp.entity.employee.StaffListPaymentBase#getFinancingSourceItem()
     */
        public FinancingSourceItem.Path<FinancingSourceItem> financingSourceItem()
        {
            if(_financingSourceItem == null )
                _financingSourceItem = new FinancingSourceItem.Path<FinancingSourceItem>(L_FINANCING_SOURCE_ITEM, this);
            return _financingSourceItem;
        }

        public Class getEntityClass()
        {
            return StaffListPaymentBase.class;
        }

        public String getEntityName()
        {
            return "staffListPaymentBase";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
