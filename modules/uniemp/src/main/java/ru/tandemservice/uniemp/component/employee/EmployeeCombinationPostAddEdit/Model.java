/**
 *$Id$
 */
package ru.tandemservice.uniemp.component.employee.EmployeeCombinationPostAddEdit;

import org.apache.hivemind.Location;
import org.apache.hivemind.Resource;
import org.apache.tapestry.IAsset;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.uniemp.entity.employee.CombinationPost;
import ru.tandemservice.uniemp.entity.employee.CombinationPostStaffRateItem;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Create by ashaburov
 * Date 08.12.11
 */
@Input({
        @Bind(key = "id", binding = "entityId"),
        @Bind(key = "employeePostId", binding = "employeePostId")
})
public class Model
{
    private Long _entityId;
    private CombinationPost _entity;
    private Long _employeePostId;
    private EmployeePost _employeePost;

    private ISingleSelectModel _orgUnitModel;
    private ISingleSelectModel _postModel;
    private ISingleSelectModel _missingEmployeePostModel;
    private ISingleSelectModel _raisingCoefficientListModel;
    private ISingleSelectModel _etksLevelModel;
    private IMultiSelectModel _employeeHRModel;
    private ISingleSelectModel _financingSourceItemModel;
    private ISingleSelectModel _financingSourceModel;
    private ISingleSelectModel _combinationPostTypeModel;

    private DynamicListDataSource _staffRateDataSource;

    private List<CombinationPostStaffRateItem> _combinationPostStaffRateItemList = new ArrayList<>();

    private boolean _needUpdateDataSource = true;
    private boolean _thereAnyActiveStaffList = false;

    private IAsset _asset = new IAsset()
    {
        @Override
        public String buildURL()
        {
            return "img/general/add.png";
        }

        @Override
        public InputStream getResourceAsStream()
        {
            return null;  //Body of implemented method
        }

        @Override
        public Resource getResourceLocation()
        {
            return null;  //Body of implemented method
        }

        @Override
        public Location getLocation()
        {
            return null;  //Body of implemented method
        }
    };


    //Calculate

    public String getStaffRateColumnId()
    {
        return "staffRate_id_" + getStaffRateDataSource().getCurrentEntity().getId();
    }

    public String getFinSrcColumnId()
    {
        return "finSrc_id_" + getStaffRateDataSource().getCurrentEntity().getId();
    }

    public String getFinSrcItmColumnId()
    {
        return "finSrcItm_id_" + getStaffRateDataSource().getCurrentEntity().getId();
    }

    public String getEmployeeHRId()
    {
        return "employeeHR_Id_" + getStaffRateDataSource().getCurrentEntity().getId();
    }

    //Getters & Setters

    public ISingleSelectModel getCombinationPostTypeModel()
    {
        return _combinationPostTypeModel;
    }

    public void setCombinationPostTypeModel(ISingleSelectModel combinationPostTypeModel)
    {
        _combinationPostTypeModel = combinationPostTypeModel;
    }

    public ISingleSelectModel getFinancingSourceItemModel()
    {
        return _financingSourceItemModel;
    }

    public void setFinancingSourceItemModel(ISingleSelectModel financingSourceItemModel)
    {
        _financingSourceItemModel = financingSourceItemModel;
    }

    public IMultiSelectModel getEmployeeHRModel()
    {
        return _employeeHRModel;
    }

    public void setEmployeeHRModel(IMultiSelectModel employeeHRModel)
    {
        _employeeHRModel = employeeHRModel;
    }

    public boolean isThereAnyActiveStaffList()
    {
        return _thereAnyActiveStaffList;
    }

    public void setThereAnyActiveStaffList(boolean thereAnyActiveStaffList)
    {
        _thereAnyActiveStaffList = thereAnyActiveStaffList;
    }

    public List<CombinationPostStaffRateItem> getCombinationPostStaffRateItemList()
    {
        return _combinationPostStaffRateItemList;
    }

    public void setCombinationPostStaffRateItemList(List<CombinationPostStaffRateItem> combinationPostStaffRateItemList)
    {
        _combinationPostStaffRateItemList = combinationPostStaffRateItemList;
    }

    public boolean isNeedUpdateDataSource()
    {
        return _needUpdateDataSource;
    }

    public void setNeedUpdateDataSource(boolean needUpdateDataSource)
    {
        _needUpdateDataSource = needUpdateDataSource;
    }

    public ISingleSelectModel getEtksLevelModel()
    {
        return _etksLevelModel;
    }

    public void setEtksLevelModel(ISingleSelectModel etksLevelModel)
    {
        _etksLevelModel = etksLevelModel;
    }

    public ISingleSelectModel getRaisingCoefficientListModel()
    {
        return _raisingCoefficientListModel;
    }

    public void setRaisingCoefficientListModel(ISingleSelectModel raisingCoefficientListModel)
    {
        _raisingCoefficientListModel = raisingCoefficientListModel;
    }

    public ISingleSelectModel getFinancingSourceModel()
    {
        return _financingSourceModel;
    }

    public void setFinancingSourceModel(ISingleSelectModel financingSourceModel)
    {
        _financingSourceModel = financingSourceModel;
    }

    public ISingleSelectModel getOrgUnitModel()
    {
        return _orgUnitModel;
    }

    public void setOrgUnitModel(ISingleSelectModel orgUnitModel)
    {
        _orgUnitModel = orgUnitModel;
    }

    public ISingleSelectModel getPostModel()
    {
        return _postModel;
    }

    public void setPostModel(ISingleSelectModel postModel)
    {
        _postModel = postModel;
    }

    public ISingleSelectModel getMissingEmployeePostModel()
    {
        return _missingEmployeePostModel;
    }

    public void setMissingEmployeePostModel(ISingleSelectModel missingEmployeePostModel)
    {
        _missingEmployeePostModel = missingEmployeePostModel;
    }

    public DynamicListDataSource getStaffRateDataSource()
    {
        return _staffRateDataSource;
    }

    public void setStaffRateDataSource(DynamicListDataSource staffRateDataSource)
    {
        _staffRateDataSource = staffRateDataSource;
    }

    public IAsset getAsset()
    {
        return _asset;
    }

    public void setAsset(IAsset asset)
    {
        _asset = asset;
    }

    public Long getEntityId()
    {
        return _entityId;
    }

    public void setEntityId(Long entityId)
    {
        _entityId = entityId;
    }

    public CombinationPost getEntity()
    {
        return _entity;
    }

    public void setEntity(CombinationPost entity)
    {
        _entity = entity;
    }

    public Long getEmployeePostId()
    {
        return _employeePostId;
    }

    public void setEmployeePostId(Long employeePostId)
    {
        _employeePostId = employeePostId;
    }

    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    public void setEmployeePost(EmployeePost employeePost)
    {
        _employeePost = employeePost;
    }
}
