/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniemp.component.settings.EmployeeVPO1Settings;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.RowCollectionFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.MultiValuesColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import ru.tandemservice.uniemp.IUniempComponents;
import ru.tandemservice.uniemp.entity.catalog.EmployeeVPO1ReportLines;

/**
 * @author dseleznev
 * Created on: 21.01.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        prepareListDataSource(component);
    }
    
    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (getModel(component).getDataSource() != null) return;
        
        DynamicListDataSource<EmployeeVPO1ReportLines> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareListDataSource(model);
        });
        
        dataSource.addColumn(new SimpleColumn("Строка отчета", EmployeeVPO1ReportLines.P_TITLE).setClickable(false).setOrderable(false).setTreeColumn(true));
        dataSource.addColumn(new SimpleColumn("Тип должности", new String[] {EmployeeVPO1ReportLines.L_POST_TYPE, EmployeeType.P_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new MultiValuesColumn("Должности, включаемые в отчет", EmployeeVPO1ReportLines.P_POSTS_LIST)
                                     .setFormatter(new RowCollectionFormatter(source -> ((PostBoundedWithQGandQL) source).getFullTitleWithSalary()))
                                     .setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit"));
        getModel(component).setDataSource(dataSource);
    }
    
    public void onClickEdit(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IUniempComponents.VPO1_SETTING_EDIT, new ParametersMap().add("employeeVPO1ReportLinesId", (Long) component.getListenerParameter())));
    }
}