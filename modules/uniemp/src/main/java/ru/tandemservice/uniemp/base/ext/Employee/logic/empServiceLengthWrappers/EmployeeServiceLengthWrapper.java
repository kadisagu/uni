/**
 *$Id:$
 */
package ru.tandemservice.uniemp.base.ext.Employee.logic.empServiceLengthWrappers;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.uniemp.entity.catalog.ServiceLengthType;
import ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemBase;
import ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemFake;

import java.util.Date;

/**
 * Враппер Стажа Кадрового ресурса.
 * Используется для агрегации полей связанных со Стажем КР.
 * Данные о стаже берутся из Истории должностей КР.
 *
 * @author Alexander Shaburov
 * @since 24.09.12
 */
public class EmployeeServiceLengthWrapper extends DataWrapper
{
    public EmployeeServiceLengthWrapper(EmploymentHistoryItemBase item)
    {
        super(item);
    }

    private ServiceLengthType _type;     // Вид стажа
    private Date _beginDate;     // Дата начала
    private Date _endDate;     // Дата окончания
    private String _comment;     // Примечание

    public boolean isNotEditable()
    {
        return !(getWrapped() instanceof EmploymentHistoryItemFake);
    }

    public String getTitleWithPeriod()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("Стаж «");
        builder.append(getType().getTitle());
        builder.append("» на период с ");
        builder.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getBeginDate()));
        builder.append(" по ");
        if (null == getEndDate())
            builder.append("настоящее время");
        else
            builder.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getEndDate()));
        return builder.toString();
    }

    // Getters & Setters

    public ServiceLengthType getType()
    {
        return _type;
    }

    public void setType(ServiceLengthType type)
    {
        _type = type;
    }

    public Date getBeginDate()
    {
        return _beginDate;
    }

    public void setBeginDate(Date beginDate)
    {
        _beginDate = beginDate;
    }

    public Date getEndDate()
    {
        return _endDate;
    }

    public void setEndDate(Date endDate)
    {
        _endDate = endDate;
    }

    public String getComment()
    {
        return _comment;
    }

    public void setComment(String comment)
    {
        _comment = comment;
    }
}
