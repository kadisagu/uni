/* $Id$ */
package ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.AdditionalDataTab.block.sickData;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.employeebase.base.bo.EmployeeReportPerson.ui.Add.EmployeeReportPersonAddUI;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import ru.tandemservice.uniemp.entity.catalog.SickListBasics;
import ru.tandemservice.uniemp.entity.employee.EmployeeSickList;

import java.util.Date;
import java.util.List;

/**
 * @author Vasily Zhukov
 * @since 05.08.2011
 */
public class SickDataParam implements IReportDQLModifier
{
    private IReportParam<Date> _sickDateFrom = new ReportParam<>();
    private IReportParam<Date> _sickDateTo = new ReportParam<>();
    private IReportParam<String> _medicalFoundation = new ReportParam<>();
    private IReportParam<List<SickListBasics>> _sickBasic = new ReportParam<>();

    private String alias(ReportDQL dql)
    {
        // соединяем кадрового ресурса
        String alias = dql.innerJoinEntity(Employee.class, Employee.person());

        // соединяем лист нетрудоспособности
        return dql.innerJoinEntity(alias, EmployeeSickList.class, EmployeeSickList.employee());
    }

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {

    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (_sickDateFrom.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "sickData.sickDateFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(_sickDateFrom.getData()));

            dql.builder.where(DQLExpressions.ge(DQLExpressions.property(EmployeeSickList.openDate().fromAlias(alias(dql))), DQLExpressions.valueDate(CoreDateUtils.getDayFirstTimeMoment(_sickDateFrom.getData()))));
        }

        if (_sickDateTo.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "sickData.sickDateTo", DateFormatter.DEFAULT_DATE_FORMATTER.format(_sickDateTo.getData()));

            dql.builder.where(DQLExpressions.lt(DQLExpressions.property(EmployeeSickList.closeDate().fromAlias(alias(dql))), DQLExpressions.valueDate(CoreDateUtils.getNextDayFirstTimeMoment(_sickDateTo.getData(), 1))));
        }

        if (_medicalFoundation.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "sickData.medicalFoundation", _medicalFoundation.getData());

            dql.builder.where(DQLExpressions.likeUpper(EmployeeSickList.medicalFoundation().fromAlias(alias(dql)), DQLExpressions.value(CoreStringUtils.escapeLike(_medicalFoundation.getData()))));
        }

        if (_sickBasic.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "sickData.sickBasic", CommonBaseStringUtil.join(_sickBasic.getData(), SickListBasics.P_TITLE, ", "));

            dql.builder.where(DQLExpressions.in(DQLExpressions.property(EmployeeSickList.basic().fromAlias(alias(dql))), _sickBasic.getData()));
        }
    }

    // Getters

    public IReportParam<Date> getSickDateFrom()
    {
        return _sickDateFrom;
    }

    public IReportParam<Date> getSickDateTo()
    {
        return _sickDateTo;
    }

    public IReportParam<String> getMedicalFoundation()
    {
        return _medicalFoundation;
    }

    public IReportParam<List<SickListBasics>> getSickBasic()
    {
        return _sickBasic;
    }
}
