/**
 *$Id:$
 */
package ru.tandemservice.uniemp.base.bo.EmpEmployee;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniemp.base.bo.EmpEmployee.ui.logic.EmpContractDao;
import ru.tandemservice.uniemp.base.bo.EmpEmployee.ui.logic.IEmpContractDao;

/**
 * @author Alexander Shaburov
 * @since 22.10.12
 */
@Configuration
public class EmpEmployeeManager extends BusinessObjectManager
{
    public static EmpEmployeeManager instance()
    {
        return instance(EmpEmployeeManager.class);
    }

    @Bean
    public IEmpContractDao empContractDao()
    {
        return new EmpContractDao();
    }
}
