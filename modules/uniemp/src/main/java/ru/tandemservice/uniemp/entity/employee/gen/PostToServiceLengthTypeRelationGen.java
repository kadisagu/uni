package ru.tandemservice.uniemp.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import ru.tandemservice.uniemp.entity.catalog.ServiceLengthType;
import ru.tandemservice.uniemp.entity.employee.PostToServiceLengthTypeRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь Должности отнесенной к ПКГ и КУ и Вида стажа
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PostToServiceLengthTypeRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.employee.PostToServiceLengthTypeRelation";
    public static final String ENTITY_NAME = "postToServiceLengthTypeRelation";
    public static final int VERSION_HASH = 2108229237;
    private static IEntityMeta ENTITY_META;

    public static final String L_POST_BOUNDED_WITH_Q_GAND_Q_L = "postBoundedWithQGandQL";
    public static final String L_SERVICE_LENGTH_TYPE = "serviceLengthType";

    private PostBoundedWithQGandQL _postBoundedWithQGandQL;     // Должность/профессия отнесенная к ПКГ и КУ
    private ServiceLengthType _serviceLengthType;     // Виды стажа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Должность/профессия отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    @NotNull
    public PostBoundedWithQGandQL getPostBoundedWithQGandQL()
    {
        return _postBoundedWithQGandQL;
    }

    /**
     * @param postBoundedWithQGandQL Должность/профессия отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    public void setPostBoundedWithQGandQL(PostBoundedWithQGandQL postBoundedWithQGandQL)
    {
        dirty(_postBoundedWithQGandQL, postBoundedWithQGandQL);
        _postBoundedWithQGandQL = postBoundedWithQGandQL;
    }

    /**
     * @return Виды стажа. Свойство не может быть null.
     */
    @NotNull
    public ServiceLengthType getServiceLengthType()
    {
        return _serviceLengthType;
    }

    /**
     * @param serviceLengthType Виды стажа. Свойство не может быть null.
     */
    public void setServiceLengthType(ServiceLengthType serviceLengthType)
    {
        dirty(_serviceLengthType, serviceLengthType);
        _serviceLengthType = serviceLengthType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PostToServiceLengthTypeRelationGen)
        {
            setPostBoundedWithQGandQL(((PostToServiceLengthTypeRelation)another).getPostBoundedWithQGandQL());
            setServiceLengthType(((PostToServiceLengthTypeRelation)another).getServiceLengthType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PostToServiceLengthTypeRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PostToServiceLengthTypeRelation.class;
        }

        public T newInstance()
        {
            return (T) new PostToServiceLengthTypeRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "postBoundedWithQGandQL":
                    return obj.getPostBoundedWithQGandQL();
                case "serviceLengthType":
                    return obj.getServiceLengthType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "postBoundedWithQGandQL":
                    obj.setPostBoundedWithQGandQL((PostBoundedWithQGandQL) value);
                    return;
                case "serviceLengthType":
                    obj.setServiceLengthType((ServiceLengthType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "postBoundedWithQGandQL":
                        return true;
                case "serviceLengthType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "postBoundedWithQGandQL":
                    return true;
                case "serviceLengthType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "postBoundedWithQGandQL":
                    return PostBoundedWithQGandQL.class;
                case "serviceLengthType":
                    return ServiceLengthType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PostToServiceLengthTypeRelation> _dslPath = new Path<PostToServiceLengthTypeRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PostToServiceLengthTypeRelation");
    }
            

    /**
     * @return Должность/профессия отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.PostToServiceLengthTypeRelation#getPostBoundedWithQGandQL()
     */
    public static PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
    {
        return _dslPath.postBoundedWithQGandQL();
    }

    /**
     * @return Виды стажа. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.PostToServiceLengthTypeRelation#getServiceLengthType()
     */
    public static ServiceLengthType.Path<ServiceLengthType> serviceLengthType()
    {
        return _dslPath.serviceLengthType();
    }

    public static class Path<E extends PostToServiceLengthTypeRelation> extends EntityPath<E>
    {
        private PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> _postBoundedWithQGandQL;
        private ServiceLengthType.Path<ServiceLengthType> _serviceLengthType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Должность/профессия отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.PostToServiceLengthTypeRelation#getPostBoundedWithQGandQL()
     */
        public PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
        {
            if(_postBoundedWithQGandQL == null )
                _postBoundedWithQGandQL = new PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL>(L_POST_BOUNDED_WITH_Q_GAND_Q_L, this);
            return _postBoundedWithQGandQL;
        }

    /**
     * @return Виды стажа. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.PostToServiceLengthTypeRelation#getServiceLengthType()
     */
        public ServiceLengthType.Path<ServiceLengthType> serviceLengthType()
        {
            if(_serviceLengthType == null )
                _serviceLengthType = new ServiceLengthType.Path<ServiceLengthType>(L_SERVICE_LENGTH_TYPE, this);
            return _serviceLengthType;
        }

        public Class getEntityClass()
        {
            return PostToServiceLengthTypeRelation.class;
        }

        public String getEntityName()
        {
            return "postToServiceLengthTypeRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
