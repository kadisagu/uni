package ru.tandemservice.uniemp.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.catalog.entity.ScienceStatus;
import ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation;
import ru.tandemservice.uniemp.entity.employee.OrgUnitPostToScStatusRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь должности на подразделении с учеными званиями
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OrgUnitPostToScStatusRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.employee.OrgUnitPostToScStatusRelation";
    public static final String ENTITY_NAME = "orgUnitPostToScStatusRelation";
    public static final int VERSION_HASH = 1700515816;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORG_UNIT_POST_RELATION = "orgUnitPostRelation";
    public static final String L_ACADEMIC_STATUS = "academicStatus";

    private OrgUnitPostRelation _orgUnitPostRelation;     // Должность на подразделении
    private ScienceStatus _academicStatus;     // Ученое звание

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Должность на подразделении. Свойство не может быть null.
     */
    @NotNull
    public OrgUnitPostRelation getOrgUnitPostRelation()
    {
        return _orgUnitPostRelation;
    }

    /**
     * @param orgUnitPostRelation Должность на подразделении. Свойство не может быть null.
     */
    public void setOrgUnitPostRelation(OrgUnitPostRelation orgUnitPostRelation)
    {
        dirty(_orgUnitPostRelation, orgUnitPostRelation);
        _orgUnitPostRelation = orgUnitPostRelation;
    }

    /**
     * @return Ученое звание. Свойство не может быть null.
     */
    @NotNull
    public ScienceStatus getAcademicStatus()
    {
        return _academicStatus;
    }

    /**
     * @param academicStatus Ученое звание. Свойство не может быть null.
     */
    public void setAcademicStatus(ScienceStatus academicStatus)
    {
        dirty(_academicStatus, academicStatus);
        _academicStatus = academicStatus;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof OrgUnitPostToScStatusRelationGen)
        {
            setOrgUnitPostRelation(((OrgUnitPostToScStatusRelation)another).getOrgUnitPostRelation());
            setAcademicStatus(((OrgUnitPostToScStatusRelation)another).getAcademicStatus());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OrgUnitPostToScStatusRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OrgUnitPostToScStatusRelation.class;
        }

        public T newInstance()
        {
            return (T) new OrgUnitPostToScStatusRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "orgUnitPostRelation":
                    return obj.getOrgUnitPostRelation();
                case "academicStatus":
                    return obj.getAcademicStatus();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "orgUnitPostRelation":
                    obj.setOrgUnitPostRelation((OrgUnitPostRelation) value);
                    return;
                case "academicStatus":
                    obj.setAcademicStatus((ScienceStatus) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "orgUnitPostRelation":
                        return true;
                case "academicStatus":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "orgUnitPostRelation":
                    return true;
                case "academicStatus":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "orgUnitPostRelation":
                    return OrgUnitPostRelation.class;
                case "academicStatus":
                    return ScienceStatus.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OrgUnitPostToScStatusRelation> _dslPath = new Path<OrgUnitPostToScStatusRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OrgUnitPostToScStatusRelation");
    }
            

    /**
     * @return Должность на подразделении. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.OrgUnitPostToScStatusRelation#getOrgUnitPostRelation()
     */
    public static OrgUnitPostRelation.Path<OrgUnitPostRelation> orgUnitPostRelation()
    {
        return _dslPath.orgUnitPostRelation();
    }

    /**
     * @return Ученое звание. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.OrgUnitPostToScStatusRelation#getAcademicStatus()
     */
    public static ScienceStatus.Path<ScienceStatus> academicStatus()
    {
        return _dslPath.academicStatus();
    }

    public static class Path<E extends OrgUnitPostToScStatusRelation> extends EntityPath<E>
    {
        private OrgUnitPostRelation.Path<OrgUnitPostRelation> _orgUnitPostRelation;
        private ScienceStatus.Path<ScienceStatus> _academicStatus;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Должность на подразделении. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.OrgUnitPostToScStatusRelation#getOrgUnitPostRelation()
     */
        public OrgUnitPostRelation.Path<OrgUnitPostRelation> orgUnitPostRelation()
        {
            if(_orgUnitPostRelation == null )
                _orgUnitPostRelation = new OrgUnitPostRelation.Path<OrgUnitPostRelation>(L_ORG_UNIT_POST_RELATION, this);
            return _orgUnitPostRelation;
        }

    /**
     * @return Ученое звание. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.OrgUnitPostToScStatusRelation#getAcademicStatus()
     */
        public ScienceStatus.Path<ScienceStatus> academicStatus()
        {
            if(_academicStatus == null )
                _academicStatus = new ScienceStatus.Path<ScienceStatus>(L_ACADEMIC_STATUS, this);
            return _academicStatus;
        }

        public Class getEntityClass()
        {
            return OrgUnitPostToScStatusRelation.class;
        }

        public String getEntityName()
        {
            return "orgUnitPostToScStatusRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
