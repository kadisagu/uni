/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.ppsAllocation.Add;

import java.util.List;

import org.tandemframework.core.entity.IdentifiableWrapper;

/**
 * @author dseleznev
 * Created on: 20.04.2009
 */
public class Model
{
    private IdentifiableWrapper _year;
    private List<IdentifiableWrapper> _yearsList;

    public IdentifiableWrapper getYear()
    {
        return _year;
    }

    public void setYear(IdentifiableWrapper year)
    {
        this._year = year;
    }

    public List<IdentifiableWrapper> getYearsList()
    {
        return _yearsList;
    }

    public void setYearsList(List<IdentifiableWrapper> yearsList)
    {
        this._yearsList = yearsList;
    }

}