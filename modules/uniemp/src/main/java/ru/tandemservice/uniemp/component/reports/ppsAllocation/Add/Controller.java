/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.ppsAllocation.Add;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dseleznev
 * Created on: 20.04.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    @SuppressWarnings("unchecked")
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        int currentYear = UniBaseUtils.getCurrentYear();
        List<IdentifiableWrapper> yearsList = new ArrayList<>();
        for(int i = currentYear; i >= currentYear - 20; i--)
            yearsList.add(new IdentifiableWrapper((long) i, String.valueOf(i)));
        model.setYearsList(yearsList);
        model.setYear(model.getYearsList().get(0));
    }
    
    public void onClickApply(IBusinessComponent component)
    {
        // переходим на универсальный компонент рендера отчета
        activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new ParametersMap()
                .add("id", getDao().preparePrintReport(getModel(component)))
        ));
    }
}