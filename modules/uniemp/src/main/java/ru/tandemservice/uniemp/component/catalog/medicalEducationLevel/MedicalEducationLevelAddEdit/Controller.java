/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniemp.component.catalog.medicalEducationLevel.MedicalEducationLevelAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditController;
import ru.tandemservice.uniemp.entity.catalog.MedicalEducationLevel;

/**
 * @author AutoGenerator
 * Created on 21.09.2010
 */
public class Controller extends DefaultCatalogAddEditController<MedicalEducationLevel, Model, IDAO>
{

    @Override
    public void onRefreshComponent(IBusinessComponent context)
    {
        super.onRefreshComponent(context);
        Model model = getModel(context);
        if (model.isEditForm() && model.getCatalogItem().getProgramSubject() != null) {
            model.setEduProgramSubjectIndex(model.getCatalogItem().getProgramSubject().getSubjectIndex());
        }
    }

}
