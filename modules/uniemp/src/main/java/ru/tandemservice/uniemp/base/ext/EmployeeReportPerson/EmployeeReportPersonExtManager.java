/* $Id$ */
package ru.tandemservice.uniemp.base.ext.EmployeeReportPerson;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author azhebko
 * @since 26.03.2014
 */
@Configuration
public class EmployeeReportPersonExtManager extends BusinessObjectExtensionManager
{
}