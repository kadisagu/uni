/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.OrgUnitStaffListTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniemp.entity.employee.StaffList;

/**
 * @author dseleznev
 * Created on: 16.09.2008
 */
@State( { @Bind(key = "orgUnitId", binding = "orgUnitId") })
@Output(keys = { "orgUnitId", "staffListId" }, bindings = { "orgUnitId", "staffListId" })
public class Model
{
    private Long _orgUnitId;
    private OrgUnit _orgUnit;
    private Long _staffListId;
    private DynamicListDataSource<StaffList> _staffListDataSource;

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        this._orgUnitId = orgUnitId;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        this._orgUnit = orgUnit;
    }

    public Long getStaffListId()
    {
        return _staffListId;
    }

    public void setStaffListId(Long staffListId)
    {
        this._staffListId = staffListId;
    }

    public DynamicListDataSource<StaffList> getStaffListDataSource()
    {
        return _staffListDataSource;
    }

    public void setStaffListDataSource(DynamicListDataSource<StaffList> staffListDataSource)
    {
        this._staffListDataSource = staffListDataSource;
    }

    public String getAddStaffListKey()
    {
        return "orgUnit_addStaffList_" + getOrgUnit().getOrgUnitType().getCode();
    }

}