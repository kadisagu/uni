/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffList.StaffListChangeState;

import java.util.Date;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;

import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.employee.StaffList;

/**
 * @author dseleznev
 * Created on: 20.09.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickApply(IBusinessComponent component)
    {
        getDao().update(getModel(component));
        deactivate(component);
    }

    public void onChangeState(IBusinessComponent component)
    {
        StaffList staffList = getModel(component).getStaffList();
        if (null != staffList.getStaffListState())
        {
            if(UniempDefines.STAFF_LIST_STATUS_ACTIVE.equals(staffList.getStaffListState().getCode()) && null == staffList.getAcceptionDate())
                staffList.setAcceptionDate(new Date());
            if(UniempDefines.STAFF_LIST_STATUS_ARCHIVE.equals(staffList.getStaffListState().getCode()))
                staffList.setArchivingDate(new Date());
            else
                staffList.setArchivingDate(null);
        }
    }
}