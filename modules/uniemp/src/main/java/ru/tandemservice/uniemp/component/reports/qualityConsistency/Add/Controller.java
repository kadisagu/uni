/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.qualityConsistency.Add;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.uni.IUniComponents;

/**
 * @author dseleznev
 * Created on: 15.04.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }
    
    public void onClickApply(IBusinessComponent component)
    {
        // переходим на универсальный компонент рендера отчета
        activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new ParametersMap()
                .add("id", getDao().preparePrintReport(getModel(component)))
        ));
    }
}