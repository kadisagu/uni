/**
 *$Id$
 */
package ru.tandemservice.uniemp.component.settings.PaymentOnFOTToPaymentsSettings.PaymentOnFOTToPaymentsSettingsEdit;

import java.util.List;

import org.tandemframework.core.info.ErrorCollector;

import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.employee.PaymentOnFOTToPaymentsRelation;

/**
 * Create by: ashaburov
 * Date: 26.04.11
 */
public interface IDAO extends IUniDao<Model>
{
    /**
     * Проверяет отсутствие цикличных связей в настройке.<p>
     * Метод идет по связям выплаты (<tt>payment</tt>) в обратном направлении,<p>
     * если оказывается, что одна из выплат, в цепочке связей, входит в состав выбранных выплат на странице редактирования настройки, то добавляется ошибка.
     * @param payment - выплата, относительно которой идет проверка
     * @param relationList - список связей (список ентити настройки)
     * @param model - модель
     * @param errorCollector - клектор ошибок
     */
    public void checkCycle(Payment payment, List<PaymentOnFOTToPaymentsRelation> relationList, Model model, ErrorCollector errorCollector);
}
