/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffListAllocItem.StaffListFakePaymentItemAddEdit;

import java.util.List;

import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;
import ru.tandemservice.uniemp.entity.employee.StaffListFakePayment;

/**
 * @author dseleznev
 * Created on: 11.05.2010
 */
@Input(keys = "staffListAllocationItemId", bindings = "staffListAllocationItemId")
public class Model
{
    private Long _staffListAllocationItemId;

    private List<FinancingSourceItem> _financingSourcesItemList;
    private List<FinancingSource> _financingSourcesList;

    private StaffListAllocationItem _staffListAllocationItem;
    private StaffListFakePayment _staffListFakePayment;

    private ISelectModel _paymentsListModel;

    //Getters & Setters

    public List<FinancingSourceItem> getFinancingSourcesItemList()
    {
        return _financingSourcesItemList;
    }

    public void setFinancingSourcesItemList(List<FinancingSourceItem> financingSourcesItemList)
    {
        _financingSourcesItemList = financingSourcesItemList;
    }

    public List<FinancingSource> getFinancingSourcesList()
    {
        return _financingSourcesList;
    }

    public void setFinancingSourcesList(List<FinancingSource> financingSourcesList)
    {
        _financingSourcesList = financingSourcesList;
    }

    public Long getStaffListAllocationItemId()
    {
        return _staffListAllocationItemId;
    }

    public void setStaffListAllocationItemId(Long staffListAllocationItemId)
    {
        this._staffListAllocationItemId = staffListAllocationItemId;
    }

    public StaffListFakePayment getStaffListFakePayment()
    {
        return _staffListFakePayment;
    }

    public void setStaffListFakePayment(StaffListFakePayment staffListFakePayment)
    {
        this._staffListFakePayment = staffListFakePayment;
    }

    public StaffListAllocationItem getStaffListAllocationItem()
    {
        return _staffListAllocationItem;
    }

    public void setStaffListAllocationItem(StaffListAllocationItem staffListAllocationItem)
    {
        this._staffListAllocationItem = staffListAllocationItem;
    }

    public ISelectModel getPaymentsListModel()
    {
        return _paymentsListModel;
    }

    public void setPaymentsListModel(ISelectModel paymentsListModel)
    {
        this._paymentsListModel = paymentsListModel;
    }
}