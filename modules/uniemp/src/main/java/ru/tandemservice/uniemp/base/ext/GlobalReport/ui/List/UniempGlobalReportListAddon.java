/* $Id$ */
package ru.tandemservice.uniemp.base.ext.GlobalReport.ui.List;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.employeebase.base.bo.EmployeeReportPerson.ui.Add.EmployeeReportPersonAddUI;
import org.tandemframework.shared.employeebase.base.ext.ReportPerson.ui.Add.ReportPersonAddExt;
import org.tandemframework.shared.person.base.bo.ReportPerson.ui.Add.ReportPersonAdd;
import org.tandemframework.shared.person.base.bo.ReportPerson.ui.Add.ReportPersonAddUI;
import ru.tandemservice.uniemp.base.bo.EmpMedicalStuffReport.ui.Add.EmpMedicalStuffReportAdd;
import ru.tandemservice.uniemp.base.bo.EmpServiceLengthReport.ui.Add.EmpServiceLengthReportAdd;
import ru.tandemservice.uniemp.base.bo.EmpSummaryPPSReport.ui.Add.EmpSummaryPPSReportAdd;

/**
 * @author azhebko
 * @since 27.03.2014
 */
public class UniempGlobalReportListAddon extends UIAddon
{
    public static final String NAME = "uniempGlobalReportListAddon";

    public UniempGlobalReportListAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public ParametersMap getEmployeeSampleParamMap()
    {
        return new ParametersMap()
                .add(ReportPersonAddUI.TAB_LIST_VISIBLE, ReportPersonAddExt.EMPLOYEE_TAB)
                .add(ReportPersonAddUI.SCHEET_LIST_VISIBLE, EmployeeReportPersonAddUI.EMPLOYEE_SCHEET)
                .add(ReportPersonAddUI.COMPONENT_TITLE, "Форма добавления отчета «Выборка сотрудников»");
    }
}