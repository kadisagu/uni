/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.component.employee.VacationCertificatePrint;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.Post;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.bo.Person.logic.declination.IDeclinationDao;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.catalog.EmployeeTemplateDocument;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;

/**
 * @author dseleznev
 * Created on: 24.03.2011
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public RtfDocument initRtfDocument(Model model)
    {
        RtfDocument document = new RtfReader().read(getCatalogItem(EmployeeTemplateDocument.class, UniempDefines.TEMPLATE_VACATION_CERTIFICATE).getContent());
        EmployeeHoliday employeeHoliday = get(EmployeeHoliday.class, model.getEmployeeHolidayId());
        EmployeePost employeePost = employeeHoliday.getEmployeePost();
        TopOrgUnit academy = TopOrgUnit.getInstance(true);
        RtfInjectModifier modifier = new RtfInjectModifier();
        modifier
                .put("academyName", academy.getTitle())
                .put("academy", academy.getShortTitle())
                .put("number", model.getPrintNumber())
                .put("day", RussianDateFormatUtils.getDayString(model.getPrintDate(), true))
                .put("monthStr", RussianDateFormatUtils.getMonthName(model.getPrintDate(), false))
                .put("year", RussianDateFormatUtils.getYearString(model.getPrintDate(), false))
                .put("sexTitle", employeePost.getPerson().getIdentityCard().getSex().isMale() ? "он" : "она")
                .put("stDay", RussianDateFormatUtils.getDayString(employeeHoliday.getStartDate(), true))
                .put("stMonthStr", RussianDateFormatUtils.getMonthName(employeeHoliday.getStartDate(), false))
                .put("stYear", RussianDateFormatUtils.getYearString(employeeHoliday.getStartDate(), false))
                .put("enDay", RussianDateFormatUtils.getDayString(employeeHoliday.getFinishDate(), true))
                .put("enMonthStr", RussianDateFormatUtils.getMonthName(employeeHoliday.getFinishDate(), false))
                .put("enYear", RussianDateFormatUtils.getYearString(employeeHoliday.getFinishDate(), false))
                .put("oN", null != model.getOrderNumber() ? model.getOrderNumber() : "")
                .put("oMonthStr", null != model.getOrderDate() ? RussianDateFormatUtils.getMonthName(model.getOrderDate(), false) : "")
                .put("oY", null != model.getOrderDate() ? RussianDateFormatUtils.getYearString(model.getOrderDate(), false) : "")
                .put("oD", null != model.getOrderDate() ? RussianDateFormatUtils.getDayString(model.getOrderDate(), true) : "");

        EmployeePost rector = (EmployeePost) academy.getHead();
        modifier.put("rectorShortFio", rector != null ? rector.getPerson().getIdentityCard().getIof() : "");

        MQBuilder employeeDepartmentBuilder = new MQBuilder(OrgUnit.ENTITY_CLASS, "o");
        employeeDepartmentBuilder.add(MQExpression.eq("o", OrgUnit.P_PERSONNEL_DEPARTMENT, true));
        OrgUnit employeeDepartment = (OrgUnit) employeeDepartmentBuilder.uniqueResult(getSession());
        EmployeePost employeeDepartmentHead = (EmployeePost)(employeeDepartment != null ? employeeDepartment.getHead() : null );
        modifier.put("headShortFio", employeeDepartmentHead != null ? employeeDepartmentHead.getPerson().getIdentityCard().getIof() : "");

        IDeclinationDao declinationDao = PersonManager.instance().declinationDao();
        modifier.put("fio_N", declinationDao.getCalculatedFIODeclination(employeePost.getPerson().getIdentityCard(), InflectorVariantCodes.RU_NOMINATIVE));
        modifier.put("fio_G", declinationDao.getCalculatedFIODeclination(employeePost.getPerson().getIdentityCard(), InflectorVariantCodes.RU_GENITIVE));
        modifier.put("fio_D", declinationDao.getCalculatedFIODeclination(employeePost.getPerson().getIdentityCard(), InflectorVariantCodes.RU_DATIVE));
        modifier.put("fio_A", declinationDao.getCalculatedFIODeclination(employeePost.getPerson().getIdentityCard(), InflectorVariantCodes.RU_ACCUSATIVE));
        modifier.put("fio_I", declinationDao.getCalculatedFIODeclination(employeePost.getPerson().getIdentityCard(), InflectorVariantCodes.RU_INSTRUMENTAL));
        modifier.put("fio_P", declinationDao.getCalculatedFIODeclination(employeePost.getPerson().getIdentityCard(), InflectorVariantCodes.RU_PREPOSITION));

        Post post = employeePost.getPostRelation().getPostBoundedWithQGandQL().getPost();
        String postTitle_N = StringUtils.uncapitalize(null != post.getNominativeCaseTitle() ? post.getNominativeCaseTitle() : post.getTitle());
        modifier.put("post_N", null != post.getNominativeCaseTitle() ? StringUtils.uncapitalize(post.getNominativeCaseTitle()) : postTitle_N);
        modifier.put("post_G", null != post.getGenitiveCaseTitle() ? StringUtils.uncapitalize(post.getGenitiveCaseTitle()) : postTitle_N);
        modifier.put("post_D", null != post.getDativeCaseTitle() ? StringUtils.uncapitalize(post.getDativeCaseTitle()) : postTitle_N);
        modifier.put("post_A", null != post.getAccusativeCaseTitle() ? StringUtils.uncapitalize(post.getAccusativeCaseTitle()) : postTitle_N);
        modifier.put("post_I", null != post.getInstrumentalCaseTitle() ? StringUtils.uncapitalize(post.getInstrumentalCaseTitle()) : postTitle_N);
        modifier.put("post_P", null != post.getPrepositionalCaseTitle() ? StringUtils.uncapitalize(post.getPrepositionalCaseTitle()) : postTitle_N);

        OrgUnit orgUnit = employeePost.getOrgUnit();
        String unitTitle_N = StringUtils.uncapitalize(null != orgUnit.getNominativeCaseTitle() ? orgUnit.getNominativeCaseTitle() : orgUnit.getFullTitle());
        modifier.put("orgUnit_N", null != orgUnit.getNominativeCaseTitle() ? StringUtils.uncapitalize(orgUnit.getNominativeCaseTitle()) : unitTitle_N);
        modifier.put("orgUnit_G", null != orgUnit.getGenitiveCaseTitle() ? StringUtils.uncapitalize(orgUnit.getGenitiveCaseTitle()) : unitTitle_N);
        modifier.put("orgUnit_D", null != orgUnit.getDativeCaseTitle() ? StringUtils.uncapitalize(orgUnit.getDativeCaseTitle()) : unitTitle_N);
        modifier.put("orgUnit_A", null != orgUnit.getAccusativeCaseTitle() ? StringUtils.uncapitalize(orgUnit.getAccusativeCaseTitle()) : unitTitle_N);
        modifier.put("orgUnit_P", null != orgUnit.getPrepositionalCaseTitle() ? StringUtils.uncapitalize(orgUnit.getPrepositionalCaseTitle()) : unitTitle_N);
        modifier.put("orgUnit_I", null != orgUnit.getNominativeCaseTitle() ? StringUtils.uncapitalize(orgUnit.getNominativeCaseTitle()) : unitTitle_N);

        modifier.put("academyAddress", academy.getLegalAddress().getTitle());

        StringBuilder phonesBuffer = new StringBuilder();
        if (academy.getPhone() != null)
        {
            phonesBuffer.append("тел. ").append(academy.getPhone()).append(", ");
        }
        if (academy.getFax() != null)
        {
            phonesBuffer.append("факс. ").append(academy.getFax());
        }
        modifier.put("academyPhones", phonesBuffer.toString());

        modifier.modify(document);
        return document;
    }
}