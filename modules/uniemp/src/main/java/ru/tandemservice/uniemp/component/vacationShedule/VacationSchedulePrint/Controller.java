/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.component.vacationShedule.VacationSchedulePrint;

import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.document.IDocumentRenderer;

import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;

/**
 * @author dseleznev
 * Created on: 26.01.2011
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);

        try { BusinessComponentUtils.downloadDocument(buildDocumentRenderer(component), true); }
        catch (Throwable t) { throw CoreExceptionUtils.getRuntimeException(t); }
    }

    public IDocumentRenderer buildDocumentRenderer(IBusinessComponent component)
    {
        Model model = getModel(component);
        deactivate(component);
        return new CommonBaseRenderer().rtf().fileName("ГрафикОтпусков" + model.getVacationSchedule().getYear() + "_" + model.getVacationSchedule().getOrgUnit().getShortTitle() + ".rtf").document(getDao().print(model));
    }
}