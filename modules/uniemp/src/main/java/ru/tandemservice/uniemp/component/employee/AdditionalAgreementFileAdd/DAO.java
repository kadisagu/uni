/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.AdditionalAgreementFileAdd;

import org.tandemframework.core.CoreUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.MaxUploadFileSizeException;
import ru.tandemservice.uniemp.dao.UniempDAO;
import ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author dseleznev
 * Created on: 12.11.2008
 */
public class DAO extends UniempDAO<Model> implements IDAO
{
    public static final long MAX_FILE_SIZE = 1024 * 1024; //1 Mb

    @Override
    public void prepare(Model model)
    {
        if (null != model.getAgreementId())
            model.setAgreement(get(ContractCollateralAgreement.class, model.getAgreementId()));
    }

    @Override
    public void update(Model model)
    {
        if (model.getFile() != null)
        {
            validateFileSize(model);
            InputStream input = model.getFile().getStream();
            byte[] content;
            try
            {
                content = CoreUtils.getBytes(input);
            }
            catch (IOException e)
            {
                throw new RuntimeException(e);
            }
            
            DatabaseFile file = model.getAgreement().getAgreementFile();
            
            model.getAgreement().setAgreementFile(new DatabaseFile());
            model.getAgreement().setAgreementFileName(model.getFile().getFileName());
            model.getAgreement().setAgreementFileType(CommonBaseUtil.getContentType(model.getFile()));
            model.getAgreement().getAgreementFile().setContent(content);
            getSession().save(model.getAgreement().getAgreementFile());
            getSession().update(model.getAgreement());
            
            if(null != file)
                delete(file);
        }
        else
            throw new ApplicationException("Необходимо указать имя файла доп. соглашения.");
    }

    private void validateFileSize(Model model) throws ApplicationException
    {
        long maxFileSize = MAX_FILE_SIZE;
        String maxImageSize = ApplicationRuntime.getProperty("max.image.size");
        if (maxImageSize != null)
            maxFileSize = Long.parseLong(maxImageSize);
        if (model.getFile().getSize() > maxFileSize)
            throw new MaxUploadFileSizeException(maxFileSize);
    }

}