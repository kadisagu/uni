/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffListAllocItem.StaffListAllocItemPub;

import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;
import ru.tandemservice.uniemp.entity.employee.StaffListPaymentBase;

/**
 * @author dseleznev
 * Created on: 08.04.2009
 */
@State(keys = PublisherActivator.PUBLISHER_ID_KEY, bindings = "publisherId")
@Output(keys = "staffListAllocationItemId", bindings = "staffListAllocationItem.id")
public class Model
{
    private Long _publisherId;
    private StaffListAllocationItem _staffListAllocationItem;
    private DynamicListDataSource<StaffListPaymentBase> _dataSource;
    private boolean _staffListItemHasFakePayments;

    public Long getPublisherId()
    {
        return _publisherId;
    }

    public void setPublisherId(Long publisherId)
    {
        this._publisherId = publisherId;
    }

    public StaffListAllocationItem getStaffListAllocationItem()
    {
        return _staffListAllocationItem;
    }

    public void setStaffListAllocationItem(StaffListAllocationItem staffListAllocationItem)
    {
        this._staffListAllocationItem = staffListAllocationItem;
    }

    public DynamicListDataSource<StaffListPaymentBase> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<StaffListPaymentBase> dataSource)
    {
        this._dataSource = dataSource;
    }

     public boolean isStaffListItemHasFakePayments()
    {
        return _staffListItemHasFakePayments;
    }

    public void setStaffListItemHasFakePayments(boolean staffListItemHasFakePayments)
    {
        this._staffListItemHasFakePayments = staffListItemHasFakePayments;
    }

    public String getAddPaymentKey()
    {
        return "addStaffListAllocPayment";
    }
    
    public String getEditKey()
    {
        return "editStaffListAllocationItem";
    }
    
    public String getDelKey()
    {
        return "deleteStaffListAllocationItem";
    }
}