/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.EmploymentHistoryIntAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.uniemp.component.employee.EmploymentHistoryAbsAddEdit.EmploymentHistoryAbstractController;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.ServiceLengthType;

import java.util.List;

/**
 * @author dseleznev
 * Created on: 24.12.2008
 */
public class Controller extends EmploymentHistoryAbstractController<IDAO, Model>
{
    public void onChangePost(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getEmploymentHistoryItem().getPostBoundedWithQGandQL() != null && (model.getServiceLengthTypeList() == null || model.getServiceLengthTypeList().isEmpty()))
        {
            List<ServiceLengthType> serviceLengthTypeList = UniempDaoFacade.getUniempDAO().getPostServiceLengthTypeList(model.getEmploymentHistoryItem().getPostBoundedWithQGandQL());
            model.setServiceLengthTypeList(serviceLengthTypeList);
        }
    }
}