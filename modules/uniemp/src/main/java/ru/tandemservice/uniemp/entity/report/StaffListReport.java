package ru.tandemservice.uniemp.entity.report;

import ru.tandemservice.uniemp.entity.report.gen.StaffListReportGen;

/**
 * Штатное расписание
 */
public class StaffListReport extends StaffListReportGen
{
    public static final String P_EMPLOYEE_POST_TYPE_WRAPPED = "employeePostTypeWrapped";
    public static final String P_ORG_UNIT_TYPES_WRAPPED = "orgUnitTypesWrapped";
    
    public String getEmployeePostTypeWrapped()
    {
        return null != getEmployeePostType() ? getEmployeePostType().replaceAll("; ", ";<br/>") : null;
    }
    
    public String getOrgUnitTypesWrapped()
    {
        return null != getOrgUnitTypes() ? getOrgUnitTypes().replaceAll("; ", ";<br/>") : null;
    }
}