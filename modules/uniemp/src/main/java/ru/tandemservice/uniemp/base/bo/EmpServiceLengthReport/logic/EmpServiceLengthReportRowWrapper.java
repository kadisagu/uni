/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.bo.EmpServiceLengthReport.logic;

import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.commonbase.base.util.key.TripletKey;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uniemp.base.ext.Employee.logic.empServiceLengthWrappers.EmployeeServiceWrapper;
import ru.tandemservice.uniemp.util.UniempUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Враппер для отчета Стажи сотрудников.
 * Агрегирует данный Кадрового ресурса: список должностей, данные по стажам.
 * Реализует геттеры для данных по колонкам отчета.
 *
 * @author Alexander Shaburov
 * @since 25.01.13
 */
public class EmpServiceLengthReportRowWrapper
{
    public EmpServiceLengthReportRowWrapper(Employee employee)
    {
        _employee = employee;
    }

    private Employee _employee;
    private List<EmployeePost> _employeePostList = new ArrayList<>();
    private List<EmployeeServiceWrapper> _serviceWrapperList = new ArrayList<>();

    // Calculate Getters

    public String getFio()
    {
        return _employee.getPerson().getFullFio();
    }

    public String getEmployeeCode()
    {
        return _employee.getEmployeeCode();
    }

    public String getPostTitle(int i)
    {
        if (i < _employeePostList.size())
            return _employeePostList.get(i).getPostRelation().getPostBoundedWithQGandQL().getTitle();
        return "";
    }

    public String getPostTypeTitle(int i)
    {
        if (i < _employeePostList.size())
            return _employeePostList.get(i).getPostType().getTitle();
        return "";
    }

    public String getPostOrgUnitTitle(int i)
    {
        if (i < _employeePostList.size())
            return _employeePostList.get(i).getOrgUnit().getPrintTitle();
        return "";
    }

    public String getEmployeeServiceTypeTitle(int i)
    {
        if (i < _serviceWrapperList.size())
            return _serviceWrapperList.get(i).getServiceLengthType().getTitle();
        return "";
    }

    public String getEmployeeServicePeriod(int i, Date date)
    {
        if (i < _serviceWrapperList.size())
        {
            final TripletKey<Integer,Integer,Integer> tripletKey = UniempUtil.getServiceLengthTypeDuration(date, _serviceWrapperList.get(i).getPeriodList());

            return CommonBaseDateUtil.getDatePeriodWithNames(tripletKey.getThird(), tripletKey.getSecond(), tripletKey.getFirst());
        }
        return "";
    }

    public int row()
    {
        return Math.max(_employeePostList.size(), _serviceWrapperList.size());
    }

    public boolean isMegre()
    {
        return row() > 1;
    }

    // Getters & Setters

    public Employee getEmployee()
    {
        return _employee;
    }

    public List<EmployeePost> getEmployeePostList()
    {
        return _employeePostList;
    }

    public List<EmployeeServiceWrapper> getServiceWrapperList()
    {
        return _serviceWrapperList;
    }
}
