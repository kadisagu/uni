package ru.tandemservice.uniemp.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import ru.tandemservice.uniemp.entity.catalog.EncouragementType;
import ru.tandemservice.uniemp.entity.employee.EmployeeEncouragement;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Поощрение/награда
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeEncouragementGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.employee.EmployeeEncouragement";
    public static final String ENTITY_NAME = "employeeEncouragement";
    public static final int VERSION_HASH = -436022166;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE = "employee";
    public static final String P_TITLE = "title";
    public static final String L_TYPE = "type";
    public static final String P_ASSIGN_DATE = "assignDate";
    public static final String P_DOCUMENT = "document";
    public static final String P_DOCUMENT_NUMBER = "documentNumber";

    private Employee _employee;     // Кадровый ресурс
    private String _title;     // Название
    private EncouragementType _type;     // Тип поощрения/награды
    private Date _assignDate;     // Дата присуждения
    private String _document;     // Документ
    private String _documentNumber;     // Номер

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Кадровый ресурс. Свойство не может быть null.
     */
    @NotNull
    public Employee getEmployee()
    {
        return _employee;
    }

    /**
     * @param employee Кадровый ресурс. Свойство не может быть null.
     */
    public void setEmployee(Employee employee)
    {
        dirty(_employee, employee);
        _employee = employee;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Тип поощрения/награды. Свойство не может быть null.
     */
    @NotNull
    public EncouragementType getType()
    {
        return _type;
    }

    /**
     * @param type Тип поощрения/награды. Свойство не может быть null.
     */
    public void setType(EncouragementType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Дата присуждения. Свойство не может быть null.
     */
    @NotNull
    public Date getAssignDate()
    {
        return _assignDate;
    }

    /**
     * @param assignDate Дата присуждения. Свойство не может быть null.
     */
    public void setAssignDate(Date assignDate)
    {
        dirty(_assignDate, assignDate);
        _assignDate = assignDate;
    }

    /**
     * @return Документ.
     */
    @Length(max=255)
    public String getDocument()
    {
        return _document;
    }

    /**
     * @param document Документ.
     */
    public void setDocument(String document)
    {
        dirty(_document, document);
        _document = document;
    }

    /**
     * @return Номер.
     */
    @Length(max=255)
    public String getDocumentNumber()
    {
        return _documentNumber;
    }

    /**
     * @param documentNumber Номер.
     */
    public void setDocumentNumber(String documentNumber)
    {
        dirty(_documentNumber, documentNumber);
        _documentNumber = documentNumber;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EmployeeEncouragementGen)
        {
            setEmployee(((EmployeeEncouragement)another).getEmployee());
            setTitle(((EmployeeEncouragement)another).getTitle());
            setType(((EmployeeEncouragement)another).getType());
            setAssignDate(((EmployeeEncouragement)another).getAssignDate());
            setDocument(((EmployeeEncouragement)another).getDocument());
            setDocumentNumber(((EmployeeEncouragement)another).getDocumentNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeEncouragementGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeEncouragement.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeEncouragement();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "employee":
                    return obj.getEmployee();
                case "title":
                    return obj.getTitle();
                case "type":
                    return obj.getType();
                case "assignDate":
                    return obj.getAssignDate();
                case "document":
                    return obj.getDocument();
                case "documentNumber":
                    return obj.getDocumentNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "employee":
                    obj.setEmployee((Employee) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "type":
                    obj.setType((EncouragementType) value);
                    return;
                case "assignDate":
                    obj.setAssignDate((Date) value);
                    return;
                case "document":
                    obj.setDocument((String) value);
                    return;
                case "documentNumber":
                    obj.setDocumentNumber((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "employee":
                        return true;
                case "title":
                        return true;
                case "type":
                        return true;
                case "assignDate":
                        return true;
                case "document":
                        return true;
                case "documentNumber":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "employee":
                    return true;
                case "title":
                    return true;
                case "type":
                    return true;
                case "assignDate":
                    return true;
                case "document":
                    return true;
                case "documentNumber":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "employee":
                    return Employee.class;
                case "title":
                    return String.class;
                case "type":
                    return EncouragementType.class;
                case "assignDate":
                    return Date.class;
                case "document":
                    return String.class;
                case "documentNumber":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeeEncouragement> _dslPath = new Path<EmployeeEncouragement>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeEncouragement");
    }
            

    /**
     * @return Кадровый ресурс. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeEncouragement#getEmployee()
     */
    public static Employee.Path<Employee> employee()
    {
        return _dslPath.employee();
    }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeEncouragement#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Тип поощрения/награды. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeEncouragement#getType()
     */
    public static EncouragementType.Path<EncouragementType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Дата присуждения. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeEncouragement#getAssignDate()
     */
    public static PropertyPath<Date> assignDate()
    {
        return _dslPath.assignDate();
    }

    /**
     * @return Документ.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeEncouragement#getDocument()
     */
    public static PropertyPath<String> document()
    {
        return _dslPath.document();
    }

    /**
     * @return Номер.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeEncouragement#getDocumentNumber()
     */
    public static PropertyPath<String> documentNumber()
    {
        return _dslPath.documentNumber();
    }

    public static class Path<E extends EmployeeEncouragement> extends EntityPath<E>
    {
        private Employee.Path<Employee> _employee;
        private PropertyPath<String> _title;
        private EncouragementType.Path<EncouragementType> _type;
        private PropertyPath<Date> _assignDate;
        private PropertyPath<String> _document;
        private PropertyPath<String> _documentNumber;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Кадровый ресурс. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeEncouragement#getEmployee()
     */
        public Employee.Path<Employee> employee()
        {
            if(_employee == null )
                _employee = new Employee.Path<Employee>(L_EMPLOYEE, this);
            return _employee;
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeEncouragement#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EmployeeEncouragementGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Тип поощрения/награды. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeEncouragement#getType()
     */
        public EncouragementType.Path<EncouragementType> type()
        {
            if(_type == null )
                _type = new EncouragementType.Path<EncouragementType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Дата присуждения. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeEncouragement#getAssignDate()
     */
        public PropertyPath<Date> assignDate()
        {
            if(_assignDate == null )
                _assignDate = new PropertyPath<Date>(EmployeeEncouragementGen.P_ASSIGN_DATE, this);
            return _assignDate;
        }

    /**
     * @return Документ.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeEncouragement#getDocument()
     */
        public PropertyPath<String> document()
        {
            if(_document == null )
                _document = new PropertyPath<String>(EmployeeEncouragementGen.P_DOCUMENT, this);
            return _document;
        }

    /**
     * @return Номер.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeEncouragement#getDocumentNumber()
     */
        public PropertyPath<String> documentNumber()
        {
            if(_documentNumber == null )
                _documentNumber = new PropertyPath<String>(EmployeeEncouragementGen.P_DOCUMENT_NUMBER, this);
            return _documentNumber;
        }

        public Class getEntityClass()
        {
            return EmployeeEncouragement.class;
        }

        public String getEntityName()
        {
            return "employeeEncouragement";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
