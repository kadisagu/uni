/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.bo.EmpServiceLengthReport.logic;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.*;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import org.apache.commons.lang.mutable.MutableInt;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniemp.base.ext.Employee.logic.empServiceLengthWrappers.EmployeeServiceWrapper;
import ru.tandemservice.uniemp.base.ext.Employee.logic.empServiceLengthWrappers.PeriodWrapper;
import ru.tandemservice.uniemp.entity.catalog.ServiceLengthType;
import ru.tandemservice.uniemp.entity.employee.EmpHistoryToServiceLengthTypeRelation;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.diffyears;

/**
 * @author Alexander Shaburov
 * @since 25.01.13
 */
public class EmpServiceLengthReportDao extends UniBaseDao implements IEmpServiceLengthReportDao
{
    @SuppressWarnings("unchecked")
    @Override
    public <T extends EmpServiceLengthReportModel> T prepareModel()
    {
        EmpServiceLengthReportModel model = new EmpServiceLengthReportModel();

        // умолчания Состояние должности сотрудника
        {
            List<EmployeePostStatus> statusList = new DQLSelectBuilder().fromEntity(EmployeePostStatus.class, "s").column(property("s"))
                    .where(eq(property(EmployeePostStatus.active().fromAlias("s")), value(true)))
                    .createStatement(getSession()).list();

            model.setEmpPostStatusList(statusList);
        }

        return (T) model;
    }

    @Override
    public <T extends EmpServiceLengthReportModel> byte[] createReport(T model) throws IOException, WriteException
    {
        if (validate(model).hasErrors())
            return null;
        preparePrintData(model);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        WritableWorkbook workbook = Workbook.createWorkbook(out, ws);
        WritableSheet sheet = workbook.createSheet("Стажи сотрудников", 0);

        prepareFontFormatMap(model);
        writeHeader(sheet, model);
        writeTable(sheet, model);

        workbook.write();
        workbook.close();

        return out.toByteArray();
    }

    /**
     * Подготавливает все используемые в отчете форматы шрифтов.
     * Складывает их по названию в мапу.
     * @return подготовленный мап форматов
     * @throws WriteException
     */
    private <T extends EmpServiceLengthReportModel> Map<String, WritableCellFormat> prepareFontFormatMap(T model) throws WriteException
    {
        final Map<String, WritableCellFormat> map = new HashMap<>();

        // шапка
        {
            WritableFont arial10 = new WritableFont(WritableFont.ARIAL, 10);
            WritableFont arial10Bold = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);

            WritableCellFormat arial10Format = new WritableCellFormat(arial10);
            WritableCellFormat arial10BoldFormat = new WritableCellFormat(arial10Bold);
            map.put(EmpServiceLengthReportModel.ARIAL_10, arial10Format);
            map.put(EmpServiceLengthReportModel.ARIAL_10_BOLD, arial10BoldFormat);
        }

        // шапка таблицы
        {
            WritableFont arial8Bold = new WritableFont(WritableFont.ARIAL, 8, WritableFont.BOLD);

            WritableCellFormat arial8BoldTopBot = new WritableCellFormat(arial8Bold);
            arial8BoldTopBot.setBorder(Border.BOTTOM, BorderLineStyle.MEDIUM);
            arial8BoldTopBot.setBorder(Border.TOP, BorderLineStyle.MEDIUM);
            arial8BoldTopBot.setBorder(Border.RIGHT, BorderLineStyle.THIN);
            arial8BoldTopBot.setVerticalAlignment(VerticalAlignment.CENTRE);
            arial8BoldTopBot.setBackground(Colour.GRAY_25);
            arial8BoldTopBot.setWrap(true);

            WritableCellFormat arial8BoldLeft = new WritableCellFormat(arial8Bold);
            arial8BoldLeft.setBorder(Border.BOTTOM, BorderLineStyle.MEDIUM);
            arial8BoldLeft.setBorder(Border.TOP, BorderLineStyle.MEDIUM);
            arial8BoldLeft.setBorder(Border.LEFT, BorderLineStyle.MEDIUM);
            arial8BoldLeft.setBorder(Border.RIGHT, BorderLineStyle.THIN);
            arial8BoldLeft.setVerticalAlignment(VerticalAlignment.CENTRE);
            arial8BoldLeft.setBackground(Colour.GRAY_25);
            arial8BoldLeft.setWrap(true);

            WritableCellFormat arial8BoldRight = new WritableCellFormat(arial8Bold);
            arial8BoldRight.setBorder(Border.BOTTOM, BorderLineStyle.MEDIUM);
            arial8BoldRight.setBorder(Border.TOP, BorderLineStyle.MEDIUM);
            arial8BoldRight.setBorder(Border.RIGHT, BorderLineStyle.MEDIUM);
            arial8BoldRight.setVerticalAlignment(VerticalAlignment.CENTRE);
            arial8BoldRight.setBackground(Colour.GRAY_25);
            arial8BoldRight.setWrap(true);

            map.put(EmpServiceLengthReportModel.ARIAL_8_BOLD_TOP_BOT_BORDER_GRAY, arial8BoldTopBot);
            map.put(EmpServiceLengthReportModel.ARIAL_8_BOLD_LEFT_BORDER_GRAY, arial8BoldLeft);
            map.put(EmpServiceLengthReportModel.ARIAL_8_BOLD_RIGHT_BORDER_GRAY, arial8BoldRight);
        }

        // основная часть таблицы
        {
            WritableFont arial8 = new WritableFont(WritableFont.ARIAL, 8);

            WritableCellFormat arial8Left = new WritableCellFormat(arial8);
            arial8Left.setBorder(Border.LEFT, BorderLineStyle.MEDIUM);
            arial8Left.setBorder(Border.RIGHT, BorderLineStyle.THIN);
            arial8Left.setBorder(Border.BOTTOM, BorderLineStyle.MEDIUM);
            arial8Left.setVerticalAlignment(VerticalAlignment.TOP);
            arial8Left.setWrap(true);

            WritableCellFormat arial8Right = new WritableCellFormat(arial8);
            arial8Right.setBorder(Border.RIGHT, BorderLineStyle.MEDIUM);
            arial8Right.setVerticalAlignment(VerticalAlignment.TOP);
            arial8Right.setWrap(true);

            WritableCellFormat arial8RightBot = new WritableCellFormat(arial8);
            arial8RightBot.setBorder(Border.RIGHT, BorderLineStyle.MEDIUM);
            arial8RightBot.setBorder(Border.BOTTOM, BorderLineStyle.MEDIUM);
            arial8RightBot.setVerticalAlignment(VerticalAlignment.TOP);
            arial8RightBot.setWrap(true);

            WritableCellFormat arial8MidBot = new WritableCellFormat(arial8);
            arial8MidBot.setBorder(Border.RIGHT, BorderLineStyle.THIN);
            arial8MidBot.setBorder(Border.BOTTOM, BorderLineStyle.MEDIUM);
            arial8MidBot.setVerticalAlignment(VerticalAlignment.TOP);
            arial8MidBot.setWrap(true);

            WritableCellFormat arial8Bot = new WritableCellFormat(arial8);
            arial8Bot.setBorder(Border.BOTTOM, BorderLineStyle.MEDIUM);
            arial8Bot.setVerticalAlignment(VerticalAlignment.TOP);
            arial8Bot.setWrap(true);

            WritableCellFormat arial8Empty = new WritableCellFormat(arial8);
            arial8Empty.setVerticalAlignment(VerticalAlignment.TOP);
            arial8Empty.setWrap(true);

            WritableCellFormat arial8MidRight = new WritableCellFormat(arial8);
            arial8MidRight.setBorder(Border.RIGHT, BorderLineStyle.THIN);
            arial8MidRight.setVerticalAlignment(VerticalAlignment.TOP);
            arial8MidRight.setWrap(true);

            map.put(EmpServiceLengthReportModel.ARIAL_8_LEFT_BORDER, arial8Left);
            map.put(EmpServiceLengthReportModel.ARIAL_8_RIGHT_BORDER, arial8Right);
            map.put(EmpServiceLengthReportModel.ARIAL_8_RIGHT_BOT_BORDER, arial8RightBot);
            map.put(EmpServiceLengthReportModel.ARIAL_8_MID_BOT_BORDER, arial8MidBot);
            map.put(EmpServiceLengthReportModel.ARIAL_8_EMPTY_BORDER, arial8Empty);
            map.put(EmpServiceLengthReportModel.ARIAL_8_MID_RIGHT_BORDER, arial8MidRight);
            map.put(EmpServiceLengthReportModel.ARIAL_8_BOT_BORDER, arial8Bot);
        }

        model.setFontMap(map);
        return map;
    }

    /**
     * Рисует шапку отчета: фильтры.
     * @return номер строки, в которой был последний фильтр
     * @throws WriteException
     */
    protected  <T extends EmpServiceLengthReportModel> int writeHeader(WritableSheet sheet, T model) throws WriteException
    {
        final WritableCellFormat arial10Format = model.getFontMap().get(EmpServiceLengthReportModel.ARIAL_10);
        final WritableCellFormat arial10BoldFormat = model.getFontMap().get(EmpServiceLengthReportModel.ARIAL_10_BOLD);

        writeFilter(sheet, arial10BoldFormat, 0, "Отчет на", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getFormingDate()));
        writeFilter(sheet, arial10BoldFormat, 1, "Расчет стажей сотрудников на", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getServLenghDate()));

        int row = 3;
        if (model.getOrgUnitList() != null && !model.getOrgUnitList().isEmpty())
        {
            writeFilter(sheet, arial10Format, row++, "Подразделение", UniStringUtils.join(model.getOrgUnitList(), OrgUnit.titleWithType().s(), ", "));
        }
        if (model.isOrgUnitChild())
        {
            writeFilter(sheet, arial10Format, row++, "Выводить сотрудников дочерних подразделений", YesNoFormatter.INSTANCE.format(model.isOrgUnitChild()));
        }
        if (model.getEmpTypeList() != null && !model.getEmpTypeList().isEmpty())
        {
            writeFilter(sheet, arial10Format, row++, "Тип должности", UniStringUtils.join(model.getEmpTypeList(), EmployeeType.title().s(), ", "));
        }
        if (model.getPostList() != null && !model.getPostList().isEmpty())
        {
            writeFilter(sheet, arial10Format, row++, "Должность", UniStringUtils.join(model.getPostList(), PostBoundedWithQGandQL.title().s(), ", "));
        }
        if (model.getEmpPostStatusList() != null && !model.getEmpPostStatusList().isEmpty())
        {
            writeFilter(sheet, arial10Format, row++, "Состояние", UniStringUtils.join(model.getEmpPostStatusList(), EmployeePostStatus.title().s(), ", "));
        }
        if (model.getAgeFrom() != null)
        {
            writeFilter(sheet, arial10Format, row++, "Возраст, от", model.getAgeFrom().toString());
        }
        if (model.getAgeTo() != null)
        {
            writeFilter(sheet, arial10Format, row++, "Возраст, до", model.getAgeTo().toString());
        }
        if (model.getSex() != null)
        {
            writeFilter(sheet, arial10Format, row++, "Пол", model.getSex().getTitle());
        }
        if (model.getServLenghTypeList() != null && !model.getServLenghTypeList().isEmpty())
        {
            writeFilter(sheet, arial10Format, row, "Вид стажа", UniStringUtils.join(model.getServLenghTypeList(), ServiceLengthType.title().s(), ", "));
        }

        model.setRow(row);
        return row;
    }

    /**
     * Рисует основную таблицу с сотрудниками и их стажами.
     */
    protected  <T extends EmpServiceLengthReportModel> void writeTable(WritableSheet sheet, T model) throws WriteException
    {
        writeTableHeader(sheet, model);

        MutableInt number = new MutableInt(1);
        for (EmpServiceLengthReportRowWrapper wrapper : model.getRowWrapperList())
        {
            writeEmployeeRow(sheet, model, wrapper, number);
        }
    }

    /**
     * Рисует строки относящиесся к данному врапперу(КРу).
     * @param wrapper враппер КРа
     * @param number порядковый номер КРа
     * @throws WriteException
     */
    private <T extends EmpServiceLengthReportModel> void writeEmployeeRow(WritableSheet sheet, T model, EmpServiceLengthReportRowWrapper wrapper, MutableInt number) throws WriteException
    {
        final WritableCellFormat left = model.getFontMap().get(EmpServiceLengthReportModel.ARIAL_8_LEFT_BORDER);
        final WritableCellFormat right = model.getFontMap().get(EmpServiceLengthReportModel.ARIAL_8_RIGHT_BORDER);
        final WritableCellFormat rightBot = model.getFontMap().get(EmpServiceLengthReportModel.ARIAL_8_RIGHT_BOT_BORDER);
        final WritableCellFormat midRight = model.getFontMap().get(EmpServiceLengthReportModel.ARIAL_8_MID_RIGHT_BORDER);
        final WritableCellFormat midBot = model.getFontMap().get(EmpServiceLengthReportModel.ARIAL_8_MID_BOT_BORDER);
        final WritableCellFormat empty = model.getFontMap().get(EmpServiceLengthReportModel.ARIAL_8_EMPTY_BORDER);
        final WritableCellFormat bot = model.getFontMap().get(EmpServiceLengthReportModel.ARIAL_8_BOT_BORDER);

        int row = model.getRow();
        for (int i = 0; i < wrapper.row(); i++)
        {
            row++;
            int col = 0;

            if (i == 0)
            {
                sheet.addCell(new Label(col++, row, String.valueOf(number.intValue()), left));
                if (wrapper.isMegre())
                    sheet.mergeCells(col - 1, row, col - 1, row + wrapper.row() - 1);
                sheet.addCell(new Label(col++, row, wrapper.getEmployeeCode(), midBot));
                if (wrapper.isMegre())
                    sheet.mergeCells(col - 1, row, col - 1, row + wrapper.row() - 1);
                sheet.addCell(new Label(col++, row, wrapper.getFio(), midBot));
                if (wrapper.isMegre())
                    sheet.mergeCells(col - 1, row, col - 1, row + wrapper.row() - 1);

                number.increment();
            }
            else
            {
                col = 3;
            }

            sheet.addCell(new Label(col++, row, wrapper.getPostTitle(i), i + 1 == wrapper.row() ? bot : empty));
            sheet.addCell(new Label(col++, row, wrapper.getPostTypeTitle(i), i + 1 == wrapper.row() ? bot : empty));
            sheet.addCell(new Label(col++, row, wrapper.getPostOrgUnitTitle(i), i + 1 == wrapper.row() ? midBot : midRight));
            sheet.addCell(new Label(col++, row, wrapper.getEmployeeServiceTypeTitle(i), i + 1 == wrapper.row() ? bot : empty));
            sheet.addCell(new Label(col, row, wrapper.getEmployeeServicePeriod(i, model.getServLenghDate()), i + 1 == wrapper.row() ? rightBot : right));
        }

        model.setRow(row);
    }

    /**
     * Pисует шапку таблицы очета.
     */
    protected <T extends EmpServiceLengthReportModel> void writeTableHeader(WritableSheet sheet, T model) throws WriteException
    {
        prepareColumnList(model);

        final WritableCellFormat topBot = model.getFontMap().get(EmpServiceLengthReportModel.ARIAL_8_BOLD_TOP_BOT_BORDER_GRAY);
        final WritableCellFormat left = model.getFontMap().get(EmpServiceLengthReportModel.ARIAL_8_BOLD_LEFT_BORDER_GRAY);
        final WritableCellFormat right = model.getFontMap().get(EmpServiceLengthReportModel.ARIAL_8_BOLD_RIGHT_BORDER_GRAY);

        int row = model.getRow() + 1;
        int col = 0;
        for (CoreCollectionUtils.Pair<String, Integer> pair : model.getColumnList())
        {
            if (model.getColumnList().indexOf(pair) == 0)
            {
                sheet.setColumnView(col, pair.getY());
                sheet.addCell(new Label(col++, row, pair.getX(), left));
            }
            else if (model.getColumnList().indexOf(pair) + 1 == model.getColumnList().size())
            {
                sheet.setColumnView(col, pair.getY());
                sheet.addCell(new Label(col++, row, pair.getX(), right));
            }
            else
            {
                sheet.setColumnView(col, pair.getY());
                sheet.addCell(new Label(col++, row, pair.getX(), topBot));
            }
        }

        model.setRow(row);
    }

    /**
     * Подготавливает колонки таблицы. Названия и размер.
     * @return список колонок
     */
    private <T extends EmpServiceLengthReportModel> List<CoreCollectionUtils.Pair<String, Integer>> prepareColumnList(T model)
    {
        final List<CoreCollectionUtils.Pair<String, Integer>> pairList = new ArrayList<>();
        pairList.add(new CoreCollectionUtils.Pair<>("№ п/п", calculateColumnWidth(5)));
        pairList.add(new CoreCollectionUtils.Pair<>("Таб. номер", calculateColumnWidth("Таб. номер")));
        pairList.add(new CoreCollectionUtils.Pair<>("ФИО", calculateColumnWidth(45)));
        pairList.add(new CoreCollectionUtils.Pair<>("Должность", calculateColumnWidth(30)));
        pairList.add(new CoreCollectionUtils.Pair<>("Тип назначения на должность", calculateColumnWidth(20)));
        pairList.add(new CoreCollectionUtils.Pair<>("Подразделение", calculateColumnWidth(40)));
        pairList.add(new CoreCollectionUtils.Pair<>("Вид стажа", calculateColumnWidth(15)));
        pairList.add(new CoreCollectionUtils.Pair<>("Количество лет, месяцев, дней", calculateColumnWidth(20)));

        model.setColumnList(pairList);
        return pairList;
    }

    /**
     * Строка фильтра. 3 и 4 колонки.
     * @param format формат шрифта
     * @param row строка, в которой выводится фильтр
     * @param filterName название фильтра
     * @param value значение фильтра
     * @throws WriteException
     */
    protected void writeFilter(WritableSheet sheet, CellFormat format, int row, String filterName, String value) throws WriteException
    {
        sheet.addCell(new Label(2, row, filterName, format));
        sheet.addCell(new Label(3, row, value, format));
    }

    /**
     * Проверки.
     * Вызываются перед всеми действиями.
     */
    protected <T extends EmpServiceLengthReportModel> ErrorCollector validate(T model)
    {
        final ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();
        if (model.getAgeFrom() != null && model.getAgeTo() != null &&
                (model.getAgeFrom() > model.getAgeTo()))
            errorCollector.add("Поле «Возраст, до» должно быть больше либо равен чем «Возраст, от».", "ageFrom", "ageTo");

        return errorCollector;
    }

    /**
     * Поднимает все необходимые данные, учитывая фильтры, и по ним строит список врапперов для строк отчета.
     */
    protected <T extends EmpServiceLengthReportModel> void preparePrintData(T model)
    {
        final Map<Employee, EmpServiceLengthReportRowWrapper> resultWrapperMap = new HashMap<>();
        final Map<Employee, Map<ServiceLengthType, EmployeeServiceWrapper>> employeeServiceWrapperMap = new HashMap<>();

        final List<Long> orgUnitIdList = new ArrayList<>();
        if (model.getOrgUnitList() != null && !model.getOrgUnitList().isEmpty())
        {
            for (OrgUnit orgUnit : model.getOrgUnitList())
            {
                orgUnitIdList.add(orgUnit.getId());
                if (model.isOrgUnitChild())
                    orgUnitIdList.addAll(UniBaseDao.ids(OrgUnitManager.instance().dao().getChildOrgUnits(orgUnit, true)));
            }
        }

        final DQLSelectBuilder employeePostBuilder = new DQLSelectBuilder().fromEntity(EmployeePost.class, "p");
        if (!orgUnitIdList.isEmpty())
            employeePostBuilder.where(in(property(EmployeePost.orgUnit().id().fromAlias("p")), orgUnitIdList));

        if (model.getPostList() != null && !model.getPostList().isEmpty())
            employeePostBuilder.where(in(property(EmployeePost.postRelation().postBoundedWithQGandQL().fromAlias("p")), model.getPostList()));

        if (model.getEmpTypeList() != null && !model.getEmpTypeList().isEmpty())
            employeePostBuilder.where(in(property(EmployeePost.postRelation().postBoundedWithQGandQL().post().employeeType().fromAlias("p")), model.getEmpTypeList()));

        if (model.getEmpPostStatusList() != null && !model.getEmpPostStatusList().isEmpty())
            employeePostBuilder.where(in(property(EmployeePost.postStatus().fromAlias("p")), model.getEmpPostStatusList()));

        if (model.getAgeFrom() != null)
            employeePostBuilder.where(ge(diffyears(value(new Date(), PropertyType.DATE), property(EmployeePost.person().identityCard().birthDate().fromAlias("p"))), value(model.getAgeFrom().intValue())));
        if (model.getAgeTo() != null)
            employeePostBuilder.where(le(diffyears(value(new Date(), PropertyType.DATE), property(EmployeePost.person().identityCard().birthDate().fromAlias("p"))), value(model.getAgeTo().intValue())));

        if (model.getSex() != null)
            employeePostBuilder.where(eq(property(EmployeePost.person().identityCard().sex().fromAlias("p")), value(model.getSex())));

        if (model.getServLenghTypeList() != null && !model.getServLenghTypeList().isEmpty())
        {
            employeePostBuilder.where(in(property(EmployeePost.employee().id().fromAlias("p")),
                    new DQLSelectBuilder().fromEntity(EmpHistoryToServiceLengthTypeRelation.class, "r").column(property(EmpHistoryToServiceLengthTypeRelation.employmentHistoryItemBase().employee().id().fromAlias("r")))
                            .where(in(property(EmpHistoryToServiceLengthTypeRelation.serviceLengthType().fromAlias("r")), model.getServLenghTypeList()))
                            .predicate(DQLPredicateType.distinct)
                            .buildQuery()));
        }

        for (EmployeePost employeePost : employeePostBuilder.createStatement(getSession()).<EmployeePost>list())
        {
            EmpServiceLengthReportRowWrapper wrapper = resultWrapperMap.get(employeePost.getEmployee());
            if (wrapper == null)
                resultWrapperMap.put(employeePost.getEmployee(), wrapper = createRowWrapperInstance(employeePost.getEmployee()));

            wrapper.getEmployeePostList().add(employeePost);
        }

        final DQLSelectBuilder servLengBuilder = new DQLSelectBuilder().fromEntity(EmpHistoryToServiceLengthTypeRelation.class, "rel")
                .column(property(EmpHistoryToServiceLengthTypeRelation.employmentHistoryItemBase().employee().fromAlias("rel")))
                .column(property(EmpHistoryToServiceLengthTypeRelation.serviceLengthType().fromAlias("rel")))
                .column(property(EmpHistoryToServiceLengthTypeRelation.employmentHistoryItemBase().assignDate().fromAlias("rel")))
                .column(property(EmpHistoryToServiceLengthTypeRelation.employmentHistoryItemBase().dismissalDate().fromAlias("rel")))
                .where(in(property(EmpHistoryToServiceLengthTypeRelation.employmentHistoryItemBase().employee().id().fromAlias("rel")), employeePostBuilder.column(property(EmployeePost.employee().id().fromAlias("p"))).buildQuery()));
        if (model.getServLenghTypeList() != null && !model.getServLenghTypeList().isEmpty())
            servLengBuilder.where(in(property(EmpHistoryToServiceLengthTypeRelation.serviceLengthType().fromAlias("rel")), model.getServLenghTypeList()));

        for (Object[] resultSet : servLengBuilder.createStatement(getSession()).<Object[]>list())
        {
            final Employee employee = (Employee) resultSet[0];
            final ServiceLengthType type = (ServiceLengthType) resultSet[1];
            final Date assignDate = (Date) resultSet[2];
            final Date dismissalDate = (Date) resultSet[3];

            Map<ServiceLengthType, EmployeeServiceWrapper> emplSrvWrapperMap = employeeServiceWrapperMap.get(employee);
            if (emplSrvWrapperMap == null)
                employeeServiceWrapperMap.put(employee, emplSrvWrapperMap = new HashMap<>());
            EmployeeServiceWrapper wrapper = emplSrvWrapperMap.get(type);
            if (wrapper == null)
                emplSrvWrapperMap.put(type, wrapper = new EmployeeServiceWrapper(type));

            wrapper.getPeriodList().add(new PeriodWrapper(assignDate, dismissalDate));
        }

        for (Map.Entry<Employee, EmpServiceLengthReportRowWrapper> entry : resultWrapperMap.entrySet())
        {
            final Map<ServiceLengthType, EmployeeServiceWrapper> serviceTypeWrapperMap = employeeServiceWrapperMap.get(entry.getKey());
            if (serviceTypeWrapperMap == null)
                continue;

            final EmpServiceLengthReportRowWrapper rowWrapper = entry.getValue();
            Collections.sort(rowWrapper.getEmployeePostList(), new EntityComparator<>(new EntityOrder(EmployeePost.postRelation().postBoundedWithQGandQL().title().s())));
            for (Map.Entry<ServiceLengthType, EmployeeServiceWrapper> serviceEntry : serviceTypeWrapperMap.entrySet())
            {

                final EmployeeServiceWrapper value = serviceEntry.getValue();
                value.combinationPeriods();

                rowWrapper.getServiceWrapperList().add(value);
            }
        }

        ArrayList<EmpServiceLengthReportRowWrapper> resultList = new ArrayList<>(resultWrapperMap.values());
        resultList.sort(CommonCollator.comparing(EmpServiceLengthReportRowWrapper::getFio, true));
        model.setRowWrapperList(resultList);
    }

    /**
     * Создает экземпляр враппера строки отчета.
     * Тут можно переопределить в анонимном классе методы вывода в проектах.
     * @param employee кадровый ресурс
     * @return врапер сотрудника для отчета
     */
    protected EmpServiceLengthReportRowWrapper createRowWrapperInstance(Employee employee)
    {
        return new EmpServiceLengthReportRowWrapper(employee);
    }

    /**
     * Вычисляет ширину колонки по названию.
     * Максимальный размер - 75 символов.
     * @param string название или количество символов
     * @return ширина
     */
    protected int calculateColumnWidth(Object string)
    {
        int max = 75;
        int size = string instanceof String ? ((String) string).length() + 2 : (Integer) string;
        if (size > max)
            return max;
        return size;
    }
}
