/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.EmployeeVacanciesList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.employee.StaffListItem;

import java.util.ArrayList;

/**
 * @author dseleznev
 * Created on: 29.10.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate
{

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
        prepareDataSource(component);
    }
    
    private void prepareDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<ViewWrapper<StaffListItem>> dataSource = new DynamicListDataSource<>(component, this);
        
        //dataSource.addColumn(UniListController.getIcoColumn("employeePost", "Сотрудник"));
        dataSource.addColumn(new SimpleColumn("Должность", StaffListItem.POST_TITLE_KEY).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Тип должн.", StaffListItem.POST_TYPE_KEY).setClickable(false));
        dataSource.addColumn(new SimpleColumn("ПКГ", StaffListItem.PROF_QUALIFICATION_GROUP_KEY).setClickable(false));
        dataSource.addColumn(new SimpleColumn("КУ", StaffListItem.QUALIFICATION_LEVEL_KEY).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Разряд ЕТКС", StaffListItem.ETKS_KEY).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Подразделение", StaffListItem.ORG_UNIT_TITLE_KEY).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Свободная ставка", "remStaffRate").setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Источник финансирования", new String[] {StaffListItem.L_FINANCING_SOURCE, FinancingSource.P_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Источник финансирования (детально)", new String[] {StaffListItem.L_FINANCING_SOURCE_ITEM, FinancingSourceItem.P_TITLE}).setClickable(false).setOrderable(false));

        model.setDataSource(dataSource);
    }
    
    @Override
    public void updateListDataSource(IBusinessComponent component)
    {
        getDao().prepareListDataSource(getModel(component));
    }

    public void onChangeChildOgrUnitVisible(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (null == model.getOrgUnit())
        {
            model.setChildOgrUnitVisible(false);
        }
    }

    public void onClickSearch(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.getDataSource().showFirstPage();
        model.getDataSource().refresh();
    }
    
    public void onClickClear(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.setOrgUnitType(null);
        model.setOrgUnit(null);
        model.setPostsList(new ArrayList<>());
        model.setEmployeeType(null);
        model.setChildOgrUnitVisible(false);
        onClickSearch(component);
    }

}