/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.staffListAllocation.Add;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.key.TripletKey;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.ProfQualificationGroup;
import org.tandemframework.shared.employeebase.catalog.entity.QualificationLevel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniemp.component.reports.staffList.Add.FiltersPresetModel;
import ru.tandemservice.uniemp.component.reports.staffList.Add.OrgUnitTypeWrapper;
import ru.tandemservice.uniemp.component.reports.staffList.Add.RtfTableRowStyle;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;
import ru.tandemservice.uniemp.entity.employee.StaffListItem;
import ru.tandemservice.uniemp.entity.employee.StaffListPaymentBase;
import ru.tandemservice.uniemp.entity.report.StaffListAllocationReport;
import ru.tandemservice.uniemp.entity.report.StaffListRepPaymentSettings;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author dseleznev
 * Created on: 10.11.2010
 */
public class Model
{
    private StaffListAllocationReport _report = new StaffListAllocationReport();
    
    // Модель, берущая на себя часть функций по работе с шаблонами фильтров
    private FiltersPresetModel _filtersPresetModel;
    
    // --- ФИЛЬТРЫ ОТЧЕТА -- //

    private Date _reportDate;                              // Дата отчета, которая пойдёт в печатную форму
    private Date _formingDate;                             // Дата формирования отчета, неизменяемая
    private IdentifiableWrapper _activityType;             // Вид деятельности (Б / ВБС)
    private List<EmployeeType> _employeeTypeList;          // Набор типов должностей (ППС, РС и тд)
    private OrgUnit _orgUnit;                              // Подразделение
    private QualificationLevel _qualificationLevel;        // Квалификационный уровень
    private ProfQualificationGroup _profQualificationGroup;// Профессионально-квалификационная группа
    private PostBoundedWithQGandQL _post;                  // Должность
    //private boolean _mergeIdenticalPosts;
    
    private List<IdentifiableWrapper> _activityTypesList;  // Список видов деятельности
    private IMultiSelectModel _employeeTypeListModel;      // Список типов должностей
    
    private ISelectModel _orgUnitList;                     // Список подразделений
    private List<QualificationLevel> _qualificationLevelsList; // Список квалификационных уровней
    private List<ProfQualificationGroup> _profQualificationGroupsList; // Список профессионально-квалификационных групп
    private ISelectModel _employeePostList;                // Список должностей
    
    private List<OrgUnitTypeWrapper> _orgUnitTypesList;    // Список типов подразделений для SearchList'а
    private List<IEntity> _selectedOrgUnitTypesList;       // Список выбранных типов подразделений
    private List<IEntity> _selectedShowChildOrgUnitsList;  // Список типов подразделений, для которых будут отображаться дочерние подразделения
    private List<IEntity> _selectedShowChildOrgUnitsAsOwnList; // Список типов подразделений, для которых должности дочерних подразделений будут считаться их собственными

    private List<StaffListRepPaymentSettings> _paymentsList; // Список типов выплат для отчета

    private DynamicListDataSource<OrgUnitTypeWrapper> _orgUnitTypesDataSource;
    
    
    // -- ОСНОВНЫЕ ДАННЫЕ ДЛЯ ПЕЧАТНОЙ ФОРМЫ -- //
    
    // "Сырые" данные из базы данных, используемые для построения данных, подлежащих выводу в отчете 
    private Map<Long, OrgUnit> _orgUnitsMap;
    private List<OrgUnit> _accountableOrgUnitsList;
    private List<StaffListItem> _rawStaffListItemsList;
    private List<EmployeePostFakeWrapper> _rawEmployeePostStaffRatesList;
    private List<StaffListAllocationItem> _rawStaffListAllocationItemsList;
    private List<StaffListPaymentBase> _rawStaffListPaymentsList;
    
    // --- Данные, подготовленные для вывода в печатной форме отчета --- //
    
    // --- Суммарные данные по ОУ --- //
    private Double _vuzBudgetStaffRate; // Суммарная ставка бюджета по всему ОУ
    private Double _vuzOffBudgetStaffRate; // Суммарная ставка внебюджета по всему ОУ
    private Double _vuzEmployeePostBudgetStaffRate; // Реальная суммарная ставка бюджета по всему ОУ
    private Double _vuzEmployeePostOffBudgetStaffRate; // Реальная суммарная ставка внебюджета по всему ОУ
    private CoreCollectionUtils.Pair<Double, Double> _vuzSumSalary; // Суммарный месячный фонд оплаты труда по тарифу для ОУ (бюджет)
    private Map<Long, CoreCollectionUtils.Pair<Double, Double>> _vuzSumPaymentsMap; // Суммарные надбавки по ОУ
    private CoreCollectionUtils.Pair<Double, Double> _vuzSumTotalSalary; // Суммарный фонд оплаты труда с надбавками для ОУ
    
    // --- Данные по подразделениям --- //
    private Map<Long, String[]> _postDataMap; // Название, приказ и КУ
    private Map<CoreCollectionUtils.Pair<Long, Boolean>, String[]> _employeePostDataMap; // ФИО, Тип назначения на должность, ставка, базовый оклад
    private Map<Long, Double> _postSalarysMap; // Должностные оклады для должностей, отнесенных к ПКГ и КУ

    // Вспомогательный мап с выплатами по должностям штатного расписания, начисляемыми на базовый оклад
    Map<EmployeePostFakeWrapper, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> _paymentsRubleValuesMap = new HashMap<>();
    // Вспомогательный мап с выплатами по должностям штатного расписания, начисляемыми на месячный ФОТ
    Map<EmployeePostFakeWrapper, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> _paymentsFullRubleValuesMap = new HashMap<>();
    
    // Мап вложенности должностей (отсортированных по названию) в подразделениях 
    private Map<Long, List<StaffListItem>> _orgUnitPostListsMap;
    private Map<Long, List<StaffListItem>> _orgUnitPostSortedListsMap;
    private Map<Long, StaffListItem> _staffListItemsMap;
    
    // Мап соотнесения должностей штатной расстановки с сотрудними на должности (key = employeePostId)
    private Map<TripletKey<Long, FinancingSource, FinancingSourceItem>, StaffListAllocationItem> _employeePostAllocItemsMap;

    // Мап вложенности ставок сотрудников в подразделениях 
    private Map<Long, Map<Long, List<EmployeePostFakeWrapper>>> _orgUnitEmployeePostStaffRatesListsMap;

    // Мапы со ставками по должностям на подразделениях
    private Map<Long, Map<Long, Double>> _orgUnitPostBudgetStaffRatesMap;
    private Map<Long, Map<Long, Double>> _orgUnitPostOffBudgetStaffRatesMap;

    // Мапы с реальными ставками по сотрудникам на подразделениях
    private Map<Long, Map<Long, Double>> _orgUnitEmployeePostBudgetStaffRatesMap;
    private Map<Long, Map<Long, Double>> _orgUnitEmployeePostOffBudgetStaffRatesMap;
    
    // Мапы с данными по месячному фонду оплаты труда по тарифу по каждой должности, 
    // суммарно по подразделению и суммарно по подразделению, включая дочерние подразделения
    private Map<EmployeePostFakeWrapper, CoreCollectionUtils.Pair<Double, Double>> _orgUnitPostSalarysMap;
    private Map<Long, CoreCollectionUtils.Pair<Double, Double>> _orgUnitWithChildsSalarysMap;
    private Map<Long, CoreCollectionUtils.Pair<Double, Double>> _orgUnitSalarysMap;
    
    // Вакансии по каждой из должностей штатного расписания 
    private Map<CoreCollectionUtils.Pair<Long, Long>, CoreCollectionUtils.Pair<Double, Double>> _vacancyRates; // key = CoreCollectionUtils.Pair<orgUnitId, orgUnitTypePostRelationId>
    private Map<Long, CoreCollectionUtils.Pair<Double, Double>> _orgUnitSumVacancyRates; // key = orgUnitId
    
    // Мапы с данными о выплатах по каждой должности, 
    // суммарно по подразделению и суммарно по подразделению, включая дочерние подразделения
    private Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> _orgUnitSumPaymentsMap;
    private Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> _orgUnitWithChildsSumPaymentsMap;
    private Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> _orgUnitPostPaymentsMap;
    
    // Мапы с данными по месячному фонду оплаты труда с надбавками по каждой должности, 
    // суммарно по подразделению и суммарно по подразделению, включая дочерние подразделения
    private Map<Long, CoreCollectionUtils.Pair<Double, Double>> _orgUnitSumTotalSalaryMap;
    private Map<Long, CoreCollectionUtils.Pair<Double, Double>> _orgUnitWithChildsSumTotalSalaryMap;
    private Map<EmployeePostFakeWrapper, CoreCollectionUtils.Pair<Double, Double>> _orgUnitPostTotalSalaryMap;
    
    
    // -- ВСПОМОГАТЕЛЬНЫЕ ДАННЫЕ ДЛЯ ПЕЧАТНОЙ ФОРМЫ -- //
    
    private int _columnsNumber; // Количество колонок в таблице для нумерации
    private int[] _paymentColumnWidth; // Массив пропорциональных ширин для колонок выплат
    private List<Payment> _paymentHeadersList; // Список заголовков выплат, которые попадут в отчет
    
    private List<Long> _otherPaymentIds; // Список ID выплат, которые должны быть отнесены к колонке "Другие выплаты"
    
    private List<OrgUnit> _serializedOrgstructure; // Сериализованная организационная структура
    private Map<OrgUnit, OrgUnit> _orgUnitMappingsMap; // Мап соответствий подразделений подразделениям, к которым нужно причислять данные
    private Map<Long, OrgUnitTypeWrapper> _orgUnitTypesMap; // Мап, в котором хранится родитель для каждого подразделения 

    
    // -- ГОТОВЫЕ К ВЫВОДУ ДАННЫЕ -- // 

    private String[] _paymentHeaders; // Заголовки выплат для печатной формы
    private String[] _numberHeaders; // Заголовки для строки с нумерацией столбцов
    
    private String[][] _tableData; // Основные данные отчета
    private String[][] _summaryData; // Суммарные данные по ОУ
    
    private RtfTableRowStyle[] _rowStyles; // Стили строк таблицы

    
    // -- ГЕТТЕРЫ И СЕТТЕРЫ -- //
    
    public StaffListAllocationReport getReport()
    {
        return _report;
    }

    public void setReport(StaffListAllocationReport report)
    {
        this._report = report;
    }

    public FiltersPresetModel getFiltersPresetModel()
    {
        return _filtersPresetModel;
    }

    public void setFiltersPresetModel(FiltersPresetModel filtersPresetModel)
    {
        this._filtersPresetModel = filtersPresetModel;
    }

    public Date getReportDate()
    {
        return _reportDate;
    }

    public void setReportDate(Date reportDate)
    {
        this._reportDate = reportDate;
    }

    public Date getFormingDate()
    {
        return _formingDate;
    }

    public void setFormingDate(Date formingDate)
    {
        this._formingDate = formingDate;
    }

    public IdentifiableWrapper getActivityType()
    {
        return _activityType;
    }

    public void setActivityType(IdentifiableWrapper activityType)
    {
        this._activityType = activityType;
    }

    public List<EmployeeType> getEmployeeTypeList()
    {
        return _employeeTypeList;
    }

    public void setEmployeeTypeList(List<EmployeeType> employeeTypeList)
    {
        this._employeeTypeList = employeeTypeList;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        this._orgUnit = orgUnit;
    }

    public QualificationLevel getQualificationLevel()
    {
        return _qualificationLevel;
    }

    public void setQualificationLevel(QualificationLevel qualificationLevel)
    {
        this._qualificationLevel = qualificationLevel;
    }

    public ProfQualificationGroup getProfQualificationGroup()
    {
        return _profQualificationGroup;
    }

    public void setProfQualificationGroup(ProfQualificationGroup profQualificationGroup)
    {
        this._profQualificationGroup = profQualificationGroup;
    }

    public PostBoundedWithQGandQL getPost()
    {
        return _post;
    }

    public void setPost(PostBoundedWithQGandQL post)
    {
        this._post = post;
    }

    public List<IdentifiableWrapper> getActivityTypesList()
    {
        return _activityTypesList;
    }

    public void setActivityTypesList(List<IdentifiableWrapper> activityTypesList)
    {
        this._activityTypesList = activityTypesList;
    }

    public IMultiSelectModel getEmployeeTypeListModel()
    {
        return _employeeTypeListModel;
    }

    public void setEmployeeTypeListModel(IMultiSelectModel employeeTypeListModel)
    {
        this._employeeTypeListModel = employeeTypeListModel;
    }

    public ISelectModel getOrgUnitList()
    {
        return _orgUnitList;
    }

    public void setOrgUnitList(ISelectModel orgUnitList)
    {
        this._orgUnitList = orgUnitList;
    }

    public List<QualificationLevel> getQualificationLevelsList()
    {
        return _qualificationLevelsList;
    }

    public void setQualificationLevelsList(List<QualificationLevel> qualificationLevelsList)
    {
        this._qualificationLevelsList = qualificationLevelsList;
    }

    public List<ProfQualificationGroup> getProfQualificationGroupsList()
    {
        return _profQualificationGroupsList;
    }

    public void setProfQualificationGroupsList(List<ProfQualificationGroup> profQualificationGroupsList)
    {
        this._profQualificationGroupsList = profQualificationGroupsList;
    }

    public ISelectModel getEmployeePostList()
    {
        return _employeePostList;
    }

    public void setEmployeePostList(ISelectModel employeePostList)
    {
        this._employeePostList = employeePostList;
    }

    public List<OrgUnitTypeWrapper> getOrgUnitTypesList()
    {
        return _orgUnitTypesList;
    }

    public void setOrgUnitTypesList(List<OrgUnitTypeWrapper> orgUnitTypesList)
    {
        this._orgUnitTypesList = orgUnitTypesList;
    }

    public List<IEntity> getSelectedOrgUnitTypesList()
    {
        return _selectedOrgUnitTypesList;
    }

    public void setSelectedOrgUnitTypesList(List<IEntity> selectedOrgUnitTypesList)
    {
        this._selectedOrgUnitTypesList = selectedOrgUnitTypesList;
    }

    public List<IEntity> getSelectedShowChildOrgUnitsList()
    {
        return _selectedShowChildOrgUnitsList;
    }

    public void setSelectedShowChildOrgUnitsList(List<IEntity> selectedShowChildOrgUnitsList)
    {
        this._selectedShowChildOrgUnitsList = selectedShowChildOrgUnitsList;
    }

    public List<IEntity> getSelectedShowChildOrgUnitsAsOwnList()
    {
        return _selectedShowChildOrgUnitsAsOwnList;
    }

    public void setSelectedShowChildOrgUnitsAsOwnList(List<IEntity> selectedShowChildOrgUnitsAsOwnList)
    {
        this._selectedShowChildOrgUnitsAsOwnList = selectedShowChildOrgUnitsAsOwnList;
    }

    public List<StaffListRepPaymentSettings> getPaymentsList()
    {
        return _paymentsList;
    }

    public void setPaymentsList(List<StaffListRepPaymentSettings> paymentsList)
    {
        this._paymentsList = paymentsList;
    }

    public DynamicListDataSource<OrgUnitTypeWrapper> getOrgUnitTypesDataSource()
    {
        return _orgUnitTypesDataSource;
    }

    public void setOrgUnitTypesDataSource(DynamicListDataSource<OrgUnitTypeWrapper> orgUnitTypesDataSource)
    {
        this._orgUnitTypesDataSource = orgUnitTypesDataSource;
    }

    public Map<Long, OrgUnit> getOrgUnitsMap()
    {
        return _orgUnitsMap;
    }

    public void setOrgUnitsMap(Map<Long, OrgUnit> orgUnitsMap)
    {
        this._orgUnitsMap = orgUnitsMap;
    }

    public List<OrgUnit> getAccountableOrgUnitsList()
    {
        return _accountableOrgUnitsList;
    }

    public void setAccountableOrgUnitsList(List<OrgUnit> accountableOrgUnitsList)
    {
        this._accountableOrgUnitsList = accountableOrgUnitsList;
    }

    public List<StaffListItem> getRawStaffListItemsList()
    {
        return _rawStaffListItemsList;
    }

    public void setRawStaffListItemsList(List<StaffListItem> rawStaffListItemsList)
    {
        this._rawStaffListItemsList = rawStaffListItemsList;
    }

    public List<EmployeePostFakeWrapper> getRawEmployeePostStaffRatesList()
    {
        return _rawEmployeePostStaffRatesList;
    }

    public void setRawEmployeePostStaffRatesList(List<EmployeePostFakeWrapper> rawEmployeePostStaffRatesList)
    {
        this._rawEmployeePostStaffRatesList = rawEmployeePostStaffRatesList;
    }

    public List<StaffListAllocationItem> getRawStaffListAllocationItemsList()
    {
        return _rawStaffListAllocationItemsList;
    }

    public void setRawStaffListAllocationItemsList(List<StaffListAllocationItem> rawStaffListAllocationItemsList)
    {
        this._rawStaffListAllocationItemsList = rawStaffListAllocationItemsList;
    }

    public List<StaffListPaymentBase> getRawStaffListPaymentsList()
    {
        return _rawStaffListPaymentsList;
    }

    public void setRawStaffListPaymentsList(List<StaffListPaymentBase> rawStaffListPaymentsList)
    {
        this._rawStaffListPaymentsList = rawStaffListPaymentsList;
    }

    public Double getVuzBudgetStaffRate()
    {
        return _vuzBudgetStaffRate;
    }

    public void setVuzBudgetStaffRate(Double vuzBudgetStaffRate)
    {
        this._vuzBudgetStaffRate = vuzBudgetStaffRate;
    }

    public Double getVuzOffBudgetStaffRate()
    {
        return _vuzOffBudgetStaffRate;
    }

    public void setVuzOffBudgetStaffRate(Double vuzOffBudgetStaffRate)
    {
        this._vuzOffBudgetStaffRate = vuzOffBudgetStaffRate;
    }

    public Double getVuzEmployeePostBudgetStaffRate()
    {
        return _vuzEmployeePostBudgetStaffRate;
    }

    public void setVuzEmployeePostBudgetStaffRate(Double vuzEmployeePostBudgetStaffRate)
    {
        this._vuzEmployeePostBudgetStaffRate = vuzEmployeePostBudgetStaffRate;
    }

    public Double getVuzEmployeePostOffBudgetStaffRate()
    {
        return _vuzEmployeePostOffBudgetStaffRate;
    }

    public void setVuzEmployeePostOffBudgetStaffRate(Double vuzEmployeePostOffBudgetStaffRate)
    {
        this._vuzEmployeePostOffBudgetStaffRate = vuzEmployeePostOffBudgetStaffRate;
    }

    public CoreCollectionUtils.Pair<Double, Double> getVuzSumSalary()
    {
        return _vuzSumSalary;
    }

    public void setVuzSumSalary(CoreCollectionUtils.Pair<Double, Double> vuzSumSalary)
    {
        this._vuzSumSalary = vuzSumSalary;
    }

    public Map<Long, CoreCollectionUtils.Pair<Double, Double>> getVuzSumPaymentsMap()
    {
        return _vuzSumPaymentsMap;
    }

    public void setVuzSumPaymentsMap(Map<Long, CoreCollectionUtils.Pair<Double, Double>> vuzSumPaymentsMap)
    {
        this._vuzSumPaymentsMap = vuzSumPaymentsMap;
    }

    public CoreCollectionUtils.Pair<Double, Double> getVuzSumTotalSalary()
    {
        return _vuzSumTotalSalary;
    }

    public void setVuzSumTotalSalary(CoreCollectionUtils.Pair<Double, Double> vuzSumTotalSalary)
    {
        this._vuzSumTotalSalary = vuzSumTotalSalary;
    }

    public Map<Long, String[]> getPostDataMap()
    {
        return _postDataMap;
    }

    public void setPostDataMap(Map<Long, String[]> postDataMap)
    {
        this._postDataMap = postDataMap;
    }

    public Map<CoreCollectionUtils.Pair<Long, Boolean>, String[]> getEmployeePostDataMap()
    {
        return _employeePostDataMap;
    }

    public void setEmployeePostDataMap(Map<CoreCollectionUtils.Pair<Long, Boolean>, String[]> employeePostDataMap)
    {
        this._employeePostDataMap = employeePostDataMap;
    }

    public Map<Long, Double> getPostSalarysMap()
    {
        return _postSalarysMap;
    }

    public void setPostSalarysMap(Map<Long, Double> postSalarysMap)
    {
        this._postSalarysMap = postSalarysMap;
    }

    public Map<EmployeePostFakeWrapper, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> getPaymentsRubleValuesMap()
    {
        return _paymentsRubleValuesMap;
    }

    public void setPaymentsRubleValuesMap(Map<EmployeePostFakeWrapper, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> paymentsRubleValuesMap)
    {
        this._paymentsRubleValuesMap = paymentsRubleValuesMap;
    }

    public Map<EmployeePostFakeWrapper, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> getPaymentsFullRubleValuesMap()
    {
        return _paymentsFullRubleValuesMap;
    }

    public void setPaymentsFullRubleValuesMap(Map<EmployeePostFakeWrapper, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> paymentsFullRubleValuesMap)
    {
        this._paymentsFullRubleValuesMap = paymentsFullRubleValuesMap;
    }

    public Map<Long, List<StaffListItem>> getOrgUnitPostListsMap()
    {
        return _orgUnitPostListsMap;
    }

    public void setOrgUnitPostListsMap(Map<Long, List<StaffListItem>> orgUnitPostListsMap)
    {
        this._orgUnitPostListsMap = orgUnitPostListsMap;
    }

    public Map<Long, List<StaffListItem>> getOrgUnitPostSortedListsMap()
    {
        return _orgUnitPostSortedListsMap;
    }

    public void setOrgUnitPostSortedListsMap(Map<Long, List<StaffListItem>> orgUnitPostSortedListsMap)
    {
        this._orgUnitPostSortedListsMap = orgUnitPostSortedListsMap;
    }

    public Map<Long, StaffListItem> getStaffListItemsMap()
    {
        return _staffListItemsMap;
    }

    public void setStaffListItemsMap(Map<Long, StaffListItem> staffListItemsMap)
    {
        this._staffListItemsMap = staffListItemsMap;
    }

    public Map<TripletKey<Long, FinancingSource, FinancingSourceItem>, StaffListAllocationItem> getEmployeePostAllocItemsMap()
    {
        return _employeePostAllocItemsMap;
    }

    public void setEmployeePostAllocItemsMap(Map<TripletKey<Long, FinancingSource, FinancingSourceItem>, StaffListAllocationItem> employeePostAllocItemsMap)
    {
        _employeePostAllocItemsMap = employeePostAllocItemsMap;
    }

    public Map<Long, Map<Long, List<EmployeePostFakeWrapper>>> getOrgUnitEmployeePostStaffRatesListsMap()
    {
        return _orgUnitEmployeePostStaffRatesListsMap;
    }

    public void setOrgUnitEmployeePostStaffRatesListsMap(Map<Long, Map<Long, List<EmployeePostFakeWrapper>>> orgUnitEmployeePostStaffRatesListsMap)
    {
        this._orgUnitEmployeePostStaffRatesListsMap = orgUnitEmployeePostStaffRatesListsMap;
    }

    public Map<Long, Map<Long, Double>> getOrgUnitPostBudgetStaffRatesMap()
    {
        return _orgUnitPostBudgetStaffRatesMap;
    }

    public void setOrgUnitPostBudgetStaffRatesMap(Map<Long, Map<Long, Double>> orgUnitPostBudgetStaffRatesMap)
    {
        this._orgUnitPostBudgetStaffRatesMap = orgUnitPostBudgetStaffRatesMap;
    }

    public Map<Long, Map<Long, Double>> getOrgUnitPostOffBudgetStaffRatesMap()
    {
        return _orgUnitPostOffBudgetStaffRatesMap;
    }

    public void setOrgUnitPostOffBudgetStaffRatesMap(Map<Long, Map<Long, Double>> orgUnitPostOffBudgetStaffRatesMap)
    {
        this._orgUnitPostOffBudgetStaffRatesMap = orgUnitPostOffBudgetStaffRatesMap;
    }

    public Map<Long, Map<Long, Double>> getOrgUnitEmployeePostBudgetStaffRatesMap()
    {
        return _orgUnitEmployeePostBudgetStaffRatesMap;
    }

    public void setOrgUnitEmployeePostBudgetStaffRatesMap(Map<Long, Map<Long, Double>> orgUnitEmployeePostBudgetStaffRatesMap)
    {
        this._orgUnitEmployeePostBudgetStaffRatesMap = orgUnitEmployeePostBudgetStaffRatesMap;
    }

    public Map<Long, Map<Long, Double>> getOrgUnitEmployeePostOffBudgetStaffRatesMap()
    {
        return _orgUnitEmployeePostOffBudgetStaffRatesMap;
    }

    public void setOrgUnitEmployeePostOffBudgetStaffRatesMap(Map<Long, Map<Long, Double>> orgUnitEmployeePostOffBudgetStaffRatesMap)
    {
        this._orgUnitEmployeePostOffBudgetStaffRatesMap = orgUnitEmployeePostOffBudgetStaffRatesMap;
    }

    public Map<EmployeePostFakeWrapper, CoreCollectionUtils.Pair<Double, Double>> getOrgUnitPostSalarysMap()
    {
        return _orgUnitPostSalarysMap;
    }

    public void setOrgUnitPostSalarysMap(Map<EmployeePostFakeWrapper, CoreCollectionUtils.Pair<Double, Double>> orgUnitPostSalarysMap)
    {
        this._orgUnitPostSalarysMap = orgUnitPostSalarysMap;
    }

    public Map<Long, CoreCollectionUtils.Pair<Double, Double>> getOrgUnitWithChildsSalarysMap()
    {
        return _orgUnitWithChildsSalarysMap;
    }

    public void setOrgUnitWithChildsSalarysMap(Map<Long, CoreCollectionUtils.Pair<Double, Double>> orgUnitWithChildsSalarysMap)
    {
        this._orgUnitWithChildsSalarysMap = orgUnitWithChildsSalarysMap;
    }

    public Map<Long, CoreCollectionUtils.Pair<Double, Double>> getOrgUnitSalarysMap()
    {
        return _orgUnitSalarysMap;
    }

    public void setOrgUnitSalarysMap(Map<Long, CoreCollectionUtils.Pair<Double, Double>> orgUnitSalarysMap)
    {
        this._orgUnitSalarysMap = orgUnitSalarysMap;
    }

    public Map<CoreCollectionUtils.Pair<Long, Long>, CoreCollectionUtils.Pair<Double, Double>> getVacancyRates()
    {
        return _vacancyRates;
    }

    public void setVacancyRates(Map<CoreCollectionUtils.Pair<Long, Long>, CoreCollectionUtils.Pair<Double, Double>> vacancyRates)
    {
        this._vacancyRates = vacancyRates;
    }

    public Map<Long, CoreCollectionUtils.Pair<Double, Double>> getOrgUnitSumVacancyRates()
    {
        return _orgUnitSumVacancyRates;
    }

    public void setOrgUnitSumVacancyRates(Map<Long, CoreCollectionUtils.Pair<Double, Double>> orgUnitSumVacancyRates)
    {
        this._orgUnitSumVacancyRates = orgUnitSumVacancyRates;
    }

    public Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> getOrgUnitSumPaymentsMap()
    {
        return _orgUnitSumPaymentsMap;
    }

    public void setOrgUnitSumPaymentsMap(Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> orgUnitSumPaymentsMap)
    {
        this._orgUnitSumPaymentsMap = orgUnitSumPaymentsMap;
    }

    public Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> getOrgUnitWithChildsSumPaymentsMap()
    {
        return _orgUnitWithChildsSumPaymentsMap;
    }

    public void setOrgUnitWithChildsSumPaymentsMap(Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> orgUnitWithChildsSumPaymentsMap)
    {
        this._orgUnitWithChildsSumPaymentsMap = orgUnitWithChildsSumPaymentsMap;
    }

    public Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> getOrgUnitPostPaymentsMap()
    {
        return _orgUnitPostPaymentsMap;
    }

    public void setOrgUnitPostPaymentsMap(Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> orgUnitPostPaymentsMap)
    {
        this._orgUnitPostPaymentsMap = orgUnitPostPaymentsMap;
    }

    public Map<Long, CoreCollectionUtils.Pair<Double, Double>> getOrgUnitSumTotalSalaryMap()
    {
        return _orgUnitSumTotalSalaryMap;
    }

    public void setOrgUnitSumTotalSalaryMap(Map<Long, CoreCollectionUtils.Pair<Double, Double>> orgUnitSumTotalSalaryMap)
    {
        this._orgUnitSumTotalSalaryMap = orgUnitSumTotalSalaryMap;
    }

    public Map<Long, CoreCollectionUtils.Pair<Double, Double>> getOrgUnitWithChildsSumTotalSalaryMap()
    {
        return _orgUnitWithChildsSumTotalSalaryMap;
    }

    public void setOrgUnitWithChildsSumTotalSalaryMap(Map<Long, CoreCollectionUtils.Pair<Double, Double>> orgUnitWithChildsSumTotalSalaryMap)
    {
        this._orgUnitWithChildsSumTotalSalaryMap = orgUnitWithChildsSumTotalSalaryMap;
    }

    public Map<EmployeePostFakeWrapper, CoreCollectionUtils.Pair<Double, Double>> getOrgUnitPostTotalSalaryMap()
    {
        return _orgUnitPostTotalSalaryMap;
    }

    public void setOrgUnitPostTotalSalaryMap(Map<EmployeePostFakeWrapper, CoreCollectionUtils.Pair<Double, Double>> orgUnitPostTotalSalaryMap)
    {
        this._orgUnitPostTotalSalaryMap = orgUnitPostTotalSalaryMap;
    }

    public int getColumnsNumber()
    {
        return _columnsNumber;
    }

    public void setColumnsNumber(int columnsNumber)
    {
        this._columnsNumber = columnsNumber;
    }

    public int[] getPaymentColumnWidth()
    {
        return _paymentColumnWidth;
    }

    public void setPaymentColumnWidth(int[] paymentColumnWidth)
    {
        this._paymentColumnWidth = paymentColumnWidth;
    }

    public List<Payment> getPaymentHeadersList()
    {
        return _paymentHeadersList;
    }

    public void setPaymentHeadersList(List<Payment> paymentHeadersList)
    {
        this._paymentHeadersList = paymentHeadersList;
    }

    public List<Long> getOtherPaymentIds()
    {
        return _otherPaymentIds;
    }

    public void setOtherPaymentIds(List<Long> otherPaymentIds)
    {
        this._otherPaymentIds = otherPaymentIds;
    }

    public List<OrgUnit> getSerializedOrgstructure()
    {
        return _serializedOrgstructure;
    }

    public void setSerializedOrgstructure(List<OrgUnit> serializedOrgstructure)
    {
        this._serializedOrgstructure = serializedOrgstructure;
    }

    public Map<OrgUnit, OrgUnit> getOrgUnitMappingsMap()
    {
        return _orgUnitMappingsMap;
    }

    public void setOrgUnitMappingsMap(Map<OrgUnit, OrgUnit> orgUnitMappingsMap)
    {
        this._orgUnitMappingsMap = orgUnitMappingsMap;
    }

    public Map<Long, OrgUnitTypeWrapper> getOrgUnitTypesMap()
    {
        return _orgUnitTypesMap;
    }

    public void setOrgUnitTypesMap(Map<Long, OrgUnitTypeWrapper> orgUnitTypesMap)
    {
        this._orgUnitTypesMap = orgUnitTypesMap;
    }

    public String[] getPaymentHeaders()
    {
        return _paymentHeaders;
    }

    public void setPaymentHeaders(String[] paymentHeaders)
    {
        this._paymentHeaders = paymentHeaders;
    }

    public String[] getNumberHeaders()
    {
        return _numberHeaders;
    }

    public void setNumberHeaders(String[] numberHeaders)
    {
        this._numberHeaders = numberHeaders;
    }

    public String[][] getTableData()
    {
        return _tableData;
    }

    public void setTableData(String[][] tableData)
    {
        this._tableData = tableData;
    }

    public String[][] getSummaryData()
    {
        return _summaryData;
    }

    public void setSummaryData(String[][] summaryData)
    {
        this._summaryData = summaryData;
    }

    public RtfTableRowStyle[] getRowStyles()
    {
        return _rowStyles;
    }

    public void setRowStyles(RtfTableRowStyle[] rowStyles)
    {
        this._rowStyles = rowStyles;
    }
}