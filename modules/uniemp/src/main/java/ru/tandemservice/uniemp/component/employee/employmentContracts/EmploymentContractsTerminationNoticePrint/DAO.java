/**
 *$Id$
 */
package ru.tandemservice.uniemp.component.employee.employmentContracts.EmploymentContractsTerminationNoticePrint;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.entity.catalog.EmployeeTemplateDocument;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;

import java.util.Date;

/**
 * Create by ashaburov
 * Date 19.08.11
 */
public class DAO  extends UniDao<Model> implements IDAO
{
    @Override
    public RtfDocument modifyDocument(EmployeeLabourContract contract, String number, Date date, RtfDocument document)
    {
        RtfInjectModifier modifier = new RtfInjectModifier();

        TopOrgUnit academy = TopOrgUnit.getInstance(true);
        modifier.put("academyName", academy.getTitle());
        modifier.put("academyAddress", academy.getAddress() != null ? academy.getAddress().getTitle() : "");
        modifier.put("academyPhones", academy.getPhone());
        modifier.put("fio_D", PersonManager.instance().declinationDao().getCalculatedFIODeclination(contract.getEmployeePost().getPerson().getIdentityCard(), InflectorVariantCodes.RU_DATIVE));
        modifier.put("address", contract.getEmployeePost().getPerson().getIdentityCard().getAddress() != null ? contract.getEmployeePost().getPerson().getIdentityCard().getAddress().getTitleWithFlat() : "");
        modifier.put("notifNumber", number);
        modifier.put("notifDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(date));
        modifier.put("labourContractEndDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(contract.getEndDate()));
        modifier.put("labourContractNumber", contract.getNumber());
        modifier.put("labourContractDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(contract.getDate()));
        boolean postType = contract.getEmployeePost().getPostType().getCode().equals(UniDefines.POST_TYPE_SECOND_JOB_INNER) ||
                contract.getEmployeePost().getPostType().getCode().equals(UniDefines.POST_TYPE_SECOND_JOB_OUTER) ||
                contract.getEmployeePost().getPostType().getCode().equals(UniDefines.POST_TYPE_SECOND_JOB);
        modifier.put("postType", postType ? "по совместительству" : "");
        modifier.put("post_G", contract.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL().getPost().getGenitiveCaseTitle() != null ? contract.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL().getPost().getGenitiveCaseTitle() : contract.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL().getPost().getTitle());
        modifier.put("orgUnit_G", contract.getEmployeePost().getOrgUnit().getGenitiveCaseTitle() != null ? contract.getEmployeePost().getOrgUnit().getGenitiveCaseTitle() : contract.getEmployeePost().getOrgUnit().getTitle());
        MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "ou");
        builder.add(MQExpression.eq("ou", OrgUnit.P_ARCHIVAL, false));
        builder.add(MQExpression.eq("ou", OrgUnit.P_PERSONNEL_DEPARTMENT, true));
        OrgUnit personnelDepartmentsOrgUnit = (OrgUnit) builder.uniqueResult(getSession());
        modifier.put("headShortFio", (personnelDepartmentsOrgUnit != null && personnelDepartmentsOrgUnit.getHead() != null) ? ((EmployeePost)personnelDepartmentsOrgUnit.getHead()).getPerson().getFio() : "");

        modifier.modify(document);
        return document;
    }

    @Override
    public RtfDocument getRtfDocument()
    {
        return new RtfReader().read(getCatalogItem(EmployeeTemplateDocument.class, UniDefines.TEMPLATE_EMPLOYMENT_CONTRACT_TERMINATION_NOTICE).getContent());
    }
}
