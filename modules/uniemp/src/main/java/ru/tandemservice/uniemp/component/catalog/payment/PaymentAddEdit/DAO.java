/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniemp.component.catalog.payment.PaymentAddEdit;

import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.catalog.PaymentType;
import ru.tandemservice.uniemp.entity.catalog.PaymentUnit;

/**
 * @author AutoGenerator
 * Created on 08.12.2008
 */
public class DAO extends DefaultCatalogAddEditDAO<Payment, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setPaymentTypesList(HierarchyUtil.listHierarchyNodesWithParents(getList(PaymentType.class), true));
        model.setFinancingSourcesList(CatalogManager.instance().dao().getCatalogItemListOrderByCode(FinancingSource.class));
        model.setPaymentUnitsList(CatalogManager.instance().dao().getCatalogItemListOrderByCode(PaymentUnit.class));
        if(model.isAddForm())
        {
            model.getCatalogItem().setActive(true);
        }
    }

    @Override
    public void update(Model model)
    {
        super.update(model);
    }
}