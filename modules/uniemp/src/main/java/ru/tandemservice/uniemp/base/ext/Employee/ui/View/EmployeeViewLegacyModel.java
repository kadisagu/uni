/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniemp.base.ext.Employee.ui.View;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.View.EmployeeViewUI;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import org.tandemframework.shared.person.base.entity.PersonAcademicStatus;
import org.tandemframework.tapsupport.component.blob.BlobDescription;
import ru.tandemservice.uniemp.base.ext.Employee.logic.EmploymentDuration;
import ru.tandemservice.uniemp.entity.catalog.ServiceLengthType;
import ru.tandemservice.uniemp.entity.employee.*;
import ru.tandemservice.uniemp.base.ext.Employee.logic.empServiceLengthWrappers.EmployeeServiceLengthWrapper;

import java.util.Date;
import java.util.List;

/**
 * @author Vasily Zhukov
 * @since 30.03.2012
 */
class EmployeeViewLegacyModel
{
    private EmployeeViewUI _baseModel;
    private EmployeeCard _employeeCard = new EmployeeCard();
    private boolean _employeeCardTypePPS;

    private BlobDescription _fileDescription;

    private Long _employeePostId;
    private Long _rowId;

    private DynamicListDataSource<EmployeePost> _employeePostDataSource;
    private DynamicListDataSource<PersonAcademicDegree> _scienceDegreeDataSource;
    private DynamicListDataSource<PersonAcademicStatus> _scienceStatusDataSource;
    private DynamicListDataSource<EmployeeMedicalSpeciality> _medicalSpecDataSource;
    private DynamicListDataSource<EmployeeSickList> _employeeSickListDataSource;
    private DynamicListDataSource<EmploymentHistoryItemBase> _employmentHistoryDataSource;
    private DynamicListDataSource<EmployeeEncouragement> _employeeEncouragementsListDataSource;
    private DynamicListDataSource<EmployeeServiceLengthWrapper> _employeeServiceLengthDataSource;
    private DynamicListDataSource<DataWrapper> _employeeServiceLengthTotalDataSource;
    private DynamicListDataSource<EmployeeTrainingItem> _trainingDataSource;
    private DynamicListDataSource<EmployeeRetrainingItem> _retrainingDataSource;

    private EmploymentDuration _employmentDuration = new EmploymentDuration();
    private List<ServiceLengthType> _serviceLengthTypeList;

    private String _postFilter;
    private Date _assignDateFilter;
    private Date _dismissalDateFilter;
    private boolean _actual;

    private boolean _medicalSpecListVisible = true;

    public boolean isAccessible()
    {
        return getBaseModel().isAccessible();
    }

    public Employee getEmployee()
    {
        return getBaseModel().getEmployee();
    }

    // Getters & Setters

    public DynamicListDataSource<DataWrapper> getEmployeeServiceLengthTotalDataSource()
    {
        return _employeeServiceLengthTotalDataSource;
    }

    public void setEmployeeServiceLengthTotalDataSource(DynamicListDataSource<DataWrapper> employeeServiceLengthTotalDataSource)
    {
        _employeeServiceLengthTotalDataSource = employeeServiceLengthTotalDataSource;
    }

    public DynamicListDataSource<EmployeeTrainingItem> getTrainingDataSource()
    {
        return _trainingDataSource;
    }

    public void setTrainingDataSource(DynamicListDataSource<EmployeeTrainingItem> trainingDataSource)
    {
        _trainingDataSource = trainingDataSource;
    }

    public DynamicListDataSource<EmployeeRetrainingItem> getRetrainingDataSource()
    {
        return _retrainingDataSource;
    }

    public void setRetrainingDataSource(DynamicListDataSource<EmployeeRetrainingItem> retrainingDataSource)
    {
        _retrainingDataSource = retrainingDataSource;
    }

    public boolean isEmployeeCardTypePPS()
    {
        return _employeeCardTypePPS;
    }

    public EmployeeCard getEmployeeCard()
    {
        return _employeeCard;
    }

    public void setEmployeeCard(EmployeeCard employeeCard)
    {
        _employeeCard = employeeCard;
    }

    public void setEmployeeCardTypePPS(boolean employeeCardTypePPS)
    {
        _employeeCardTypePPS = employeeCardTypePPS;
    }

    public Long getEmployeePostId()
    {
        return _employeePostId;
    }

    public void setEmployeePostId(Long employeePostId)
    {
        _employeePostId = employeePostId;
    }

    public DynamicListDataSource<EmployeePost> getEmployeePostDataSource()
    {
        return _employeePostDataSource;
    }

    public void setEmployeePostDataSource(DynamicListDataSource<EmployeePost> employeePostDataSource)
    {
        _employeePostDataSource = employeePostDataSource;
    }

    public DynamicListDataSource<PersonAcademicDegree> getScienceDegreeDataSource()
    {
        return _scienceDegreeDataSource;
    }

    public void setScienceDegreeDataSource(DynamicListDataSource<PersonAcademicDegree> scienceDegreeDataSource)
    {
        _scienceDegreeDataSource = scienceDegreeDataSource;
    }

    public DynamicListDataSource<PersonAcademicStatus> getScienceStatusDataSource()
    {
        return _scienceStatusDataSource;
    }

    public void setScienceStatusDataSource(DynamicListDataSource<PersonAcademicStatus> scienceStatusDataSource)
    {
        _scienceStatusDataSource = scienceStatusDataSource;
    }

    public DynamicListDataSource<EmployeeMedicalSpeciality> getMedicalSpecDataSource()
    {
        return _medicalSpecDataSource;
    }

    public void setMedicalSpecDataSource(DynamicListDataSource<EmployeeMedicalSpeciality> medicalSpecDataSource)
    {
        this._medicalSpecDataSource = medicalSpecDataSource;
    }

    public DynamicListDataSource<EmployeeSickList> getEmployeeSickListDataSource()
    {
        return _employeeSickListDataSource;
    }

    public void setEmployeeSickListDataSource(DynamicListDataSource<EmployeeSickList> employeeSickListDataSource)
    {
        _employeeSickListDataSource = employeeSickListDataSource;
    }

    public DynamicListDataSource<EmploymentHistoryItemBase> getEmploymentHistoryDataSource()
    {
        return _employmentHistoryDataSource;
    }

    public void setEmploymentHistoryDataSource(DynamicListDataSource<EmploymentHistoryItemBase> employmentHistoryDataSource)
    {
        this._employmentHistoryDataSource = employmentHistoryDataSource;
    }

    public DynamicListDataSource<EmployeeEncouragement> getEmployeeEncouragementsListDataSource()
    {
        return _employeeEncouragementsListDataSource;
    }

    public void setEmployeeEncouragementsListDataSource(DynamicListDataSource<EmployeeEncouragement> employeeEncouragementsListDataSource)
    {
        this._employeeEncouragementsListDataSource = employeeEncouragementsListDataSource;
    }

    public Long getRowId()
    {
        return _rowId;
    }

    public void setRowId(Long rowId)
    {
        _rowId = rowId;
    }

    public BlobDescription getFileDescription()
    {
        return _fileDescription;
    }

    public void setFileDescription(BlobDescription fileDescription)
    {
        _fileDescription = fileDescription;
    }

    public EmployeeViewUI getBaseModel()
    {
        return _baseModel;
    }

    public void setBaseModel(EmployeeViewUI baseModel)
    {
        _baseModel = baseModel;
    }

    public EmploymentDuration getEmploymentDuration()
    {
        return _employmentDuration;
    }

    public void setEmploymentDuration(EmploymentDuration employmentDuration)
    {
        this._employmentDuration = employmentDuration;
    }

    public String getPostFilter()
    {
        return _postFilter;
    }

    public void setPostFilter(String postFilter)
    {
        this._postFilter = postFilter;
    }

    public Date getAssignDateFilter()
    {
        return _assignDateFilter;
    }

    public void setAssignDateFilter(Date assignDateFilter)
    {
        this._assignDateFilter = assignDateFilter;
    }

    public Date getDismissalDateFilter()
    {
        return _dismissalDateFilter;
    }

    public void setDismissalDateFilter(Date dismissalDateFilter)
    {
        this._dismissalDateFilter = dismissalDateFilter;
    }

    public DynamicListDataSource<EmployeeServiceLengthWrapper> getEmployeeServiceLengthDataSource()
    {
        return _employeeServiceLengthDataSource;
    }

    public void setEmployeeServiceLengthDataSource(DynamicListDataSource<EmployeeServiceLengthWrapper> employeeServiceLengthDataSource)
    {
        _employeeServiceLengthDataSource = employeeServiceLengthDataSource;
    }

    public List<ServiceLengthType> getServiceLengthTypeList()
    {
        return _serviceLengthTypeList;
    }

    public void setServiceLengthTypeList(List<ServiceLengthType> serviceLengthTypeList)
    {
        _serviceLengthTypeList = serviceLengthTypeList;
    }

    public boolean isMedicalSpecListVisible()
    {
        return _medicalSpecListVisible;
    }

    public void setMedicalSpecListVisible(boolean medicalSpecListVisible)
    {
        this._medicalSpecListVisible = medicalSpecListVisible;
    }

    public boolean isActual() {
        return _actual;
    }

    public void setActual(boolean actual) {
        this._actual = actual;
    }
}
