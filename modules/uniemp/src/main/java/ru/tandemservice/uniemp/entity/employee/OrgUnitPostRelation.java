package ru.tandemservice.uniemp.entity.employee;

import java.util.List;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.entity.IEntity;

import org.tandemframework.shared.employeebase.catalog.entity.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.gen.OrgUnitPostRelationGen;

public class OrgUnitPostRelation extends OrgUnitPostRelationGen
{
    public static final String P_TITLE = "title";
    public static final String P_SIMPLE_TITLE = "simpleTitle";
    public static final String P_TITLE_WITH_SALARY = "titleWithSalary";
    public static final String P_SCIENCE_STATUSES_LIST = "scienceStatusesList";
    public static final String P_SCIENCE_DEGREES_LIST = "scienceDegreesList";
    public static final String P_POST_TYPES_LIST = "postTypesList";

    public static final String P_SCIENCE_STATUSES_LIST_STR = "scienceStatusesListStr";
    public static final String P_SCIENCE_DEGREES_LIST_STR = "scienceDegreesListStr";
    public static final String P_POST_TYPES_LIST_STR = "postTypesListStr";
    
    public static final String P_USED_IN_ANY_STAFF_LIST = "usedInAnyStaffList";
    
    public static final String[] POST_TITLE_KEY = new String[] { OrgUnitPostRelation.P_TITLE };
    public static final String[] ORG_UNIT_TITLE_KEY = new String[] { OrgUnitPostRelation.L_ORG_UNIT, OrgUnit.P_FULL_TITLE };
    public static final String[] POST_SIMPLE_TITLE_KEY = new String[] { OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION, OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L, PostBoundedWithQGandQL.P_TITLE};
    public static final String[] POST_TYPE_KEY = new String[] { OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION, OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L, PostBoundedWithQGandQL.L_POST, Post.L_EMPLOYEE_TYPE, EmployeeType.P_SHORT_TITLE };
    public static final String[] QUALIFICATION_LEVEL_KEY = new String[] { OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION, OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L, PostBoundedWithQGandQL.L_QUALIFICATION_LEVEL, QualificationLevel.P_SHORT_TITLE };
    public static final String[] PROF_QUALIFICATION_GROUP_KEY = new String[] { OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION, OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L, PostBoundedWithQGandQL.L_PROF_QUALIFICATION_GROUP, ProfQualificationGroup.P_SHORT_TITLE };
    public static final String[] BASE_SALARY_KEY = new String[] { OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION, OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L, PostBoundedWithQGandQL.P_SALARY };
    public static final String[] ETKS_KEY = new String[] { OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION, OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L, PostBoundedWithQGandQL.L_ETKS_LEVELS, EtksLevels.P_TITLE };

    public String getTitle()
    {
        return getOrgUnitTypePostRelation().getTitle();
    }
    
    public String getSimpleTitle()
    {
        return getOrgUnitTypePostRelation().getPostBoundedWithQGandQL().getTitle();
    }
    
    public String getTitleWithSalary()
    {
        return getOrgUnitTypePostRelation().getPostBoundedWithQGandQL().getFullTitleWithSalary();
    }

    @SuppressWarnings("unchecked")
    public List<OrgUnitPostToScStatusRelation> getScienceStatusesList()
    {
        return UniempDaoFacade.getUniempDAO().getOrgUnitPostRelations(this, OrgUnitPostToScStatusRelation.class, OrgUnitPostToScStatusRelation.L_ACADEMIC_STATUS);
    }

    @SuppressWarnings("unchecked")
    public List<OrgUnitPostToScDegreeRelation> getScienceDegreesList()
    {
        return UniempDaoFacade.getUniempDAO().getOrgUnitPostRelations(this, OrgUnitPostToScDegreeRelation.class, OrgUnitPostToScDegreeRelation.L_ACADEMIC_DEGREE);
    }

    @SuppressWarnings("unchecked")
    public List<OrgUnitPostToTypeRelation> getPostTypesList()
    {
        return UniempDaoFacade.getUniempDAO().getOrgUnitPostRelations(this, OrgUnitPostToTypeRelation.class, OrgUnitPostToTypeRelation.L_POST_TYPE);
    }

    public String getScienceStatusesListStr()
    {
        return getPropertyListStr(getScienceStatusesList(), OrgUnitPostToScStatusRelation.L_ACADEMIC_STATUS);
    }

    public String getScienceDegreesListStr()
    {
        return getPropertyListStr(getScienceDegreesList(), OrgUnitPostToScDegreeRelation.L_ACADEMIC_DEGREE);
    }

    public String getPostTypesListStr()
    {
        return getPropertyListStr(getPostTypesList(), OrgUnitPostToTypeRelation.L_POST_TYPE);
    }
    
    public boolean isUsedInAnyStaffList()
    {
        return UniempDaoFacade.getStaffListDAO().getUsingInAnyStaffList(this); 
    }

    private String getPropertyListStr(List<? extends IEntity> list, String catalogItemPropertyName)
    {
        if (null != list && list.size() > 0)
        {
            StringBuilder serializedList = new StringBuilder("");
            for (IEntity entity : list)
            {
                if (list.indexOf(entity) > 0)
                    serializedList.append(", ");
                serializedList.append(((IEntity)entity.getProperty(catalogItemPropertyName)).getProperty(ICatalogItem.CATALOG_ITEM_TITLE));
            }
            return serializedList.toString();
        }
        return "";
    }
}