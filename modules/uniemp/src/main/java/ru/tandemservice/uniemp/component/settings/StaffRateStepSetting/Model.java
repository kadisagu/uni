/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.settings.StaffRateStepSetting;


/**
 * @author dseleznev
 * Created on: 07.10.2008
 */
public class Model
{
    private Double _staffRateStep;

    public Double getStaffRateStep()
    {
        return _staffRateStep;
    }

    public void setStaffRateStep(Double staffRateStep)
    {
        this._staffRateStep = staffRateStep;
    }
}
