/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.staffListAllocation.Add;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.uniemp.component.reports.staffList.Add.OrgUnitTypeWrapper;

/**
 * @author dseleznev
 * Created on: 10.11.2010
 */
public interface IDAO extends IUniDao<Model>
{
    void prepareOrgUnitTypesDataSource(Model model);

    List<OrgUnit> getChildOrgUnits(OrgUnit orgUnit, List<OrgUnit> orgUnitsToTake, Map<Long, OrgUnitTypeWrapper> orgUnitTypesMap, Date actualDate);
}