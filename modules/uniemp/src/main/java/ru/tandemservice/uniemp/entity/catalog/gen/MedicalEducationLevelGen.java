package ru.tandemservice.uniemp.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniemp.entity.catalog.MedicalEducationLevel;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Врачебные специальности
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MedicalEducationLevelGen extends EntityBase
 implements INaturalIdentifiable<MedicalEducationLevelGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.catalog.MedicalEducationLevel";
    public static final String ENTITY_NAME = "medicalEducationLevel";
    public static final int VERSION_HASH = -1083872737;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String L_PROGRAM_SUBJECT = "programSubject";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _shortTitle;     // Сокращенное название
    private EduProgramSubject _programSubject;     // Направление подготовки
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название. Свойство не может быть null.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Направление подготовки.
     */
    public EduProgramSubject getProgramSubject()
    {
        return _programSubject;
    }

    /**
     * @param programSubject Направление подготовки.
     */
    public void setProgramSubject(EduProgramSubject programSubject)
    {
        dirty(_programSubject, programSubject);
        _programSubject = programSubject;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MedicalEducationLevelGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((MedicalEducationLevel)another).getCode());
            }
            setShortTitle(((MedicalEducationLevel)another).getShortTitle());
            setProgramSubject(((MedicalEducationLevel)another).getProgramSubject());
            setTitle(((MedicalEducationLevel)another).getTitle());
        }
    }

    public INaturalId<MedicalEducationLevelGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<MedicalEducationLevelGen>
    {
        private static final String PROXY_NAME = "MedicalEducationLevelNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof MedicalEducationLevelGen.NaturalId) ) return false;

            MedicalEducationLevelGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MedicalEducationLevelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MedicalEducationLevel.class;
        }

        public T newInstance()
        {
            return (T) new MedicalEducationLevel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "shortTitle":
                    return obj.getShortTitle();
                case "programSubject":
                    return obj.getProgramSubject();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "programSubject":
                    obj.setProgramSubject((EduProgramSubject) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "shortTitle":
                        return true;
                case "programSubject":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "shortTitle":
                    return true;
                case "programSubject":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "shortTitle":
                    return String.class;
                case "programSubject":
                    return EduProgramSubject.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MedicalEducationLevel> _dslPath = new Path<MedicalEducationLevel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MedicalEducationLevel");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniemp.entity.catalog.MedicalEducationLevel#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.catalog.MedicalEducationLevel#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Направление подготовки.
     * @see ru.tandemservice.uniemp.entity.catalog.MedicalEducationLevel#getProgramSubject()
     */
    public static EduProgramSubject.Path<EduProgramSubject> programSubject()
    {
        return _dslPath.programSubject();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uniemp.entity.catalog.MedicalEducationLevel#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends MedicalEducationLevel> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _shortTitle;
        private EduProgramSubject.Path<EduProgramSubject> _programSubject;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniemp.entity.catalog.MedicalEducationLevel#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(MedicalEducationLevelGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.catalog.MedicalEducationLevel#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(MedicalEducationLevelGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Направление подготовки.
     * @see ru.tandemservice.uniemp.entity.catalog.MedicalEducationLevel#getProgramSubject()
     */
        public EduProgramSubject.Path<EduProgramSubject> programSubject()
        {
            if(_programSubject == null )
                _programSubject = new EduProgramSubject.Path<EduProgramSubject>(L_PROGRAM_SUBJECT, this);
            return _programSubject;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uniemp.entity.catalog.MedicalEducationLevel#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(MedicalEducationLevelGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return MedicalEducationLevel.class;
        }

        public String getEntityName()
        {
            return "medicalEducationLevel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
