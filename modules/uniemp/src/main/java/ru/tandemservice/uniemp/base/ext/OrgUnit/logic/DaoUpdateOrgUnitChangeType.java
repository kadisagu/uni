/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.ext.OrgUnit.logic;

import org.hibernate.Session;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.shared.commonbase.base.util.IDaoExtension;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.organization.base.bo.OrgUnit.logic.IOrgUnitDao;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Create by ashaburov
 * Date 19.01.12
 */
public class DaoUpdateOrgUnitChangeType extends CommonDAO implements IDaoExtension
{
    @Override
    public void handle(String stateName, Map<String, Object> params)
    {
        if (IDaoExtension.AFTER_UPDATE.equals(stateName))
        {
            OrgUnit orgUnit = (OrgUnit) params.get(IOrgUnitDao.PARAM_ORG_UNIT);
            OrgUnitType orgUnitType = (OrgUnitType) params.get(IOrgUnitDao.PARAM_ORG_UNIT_TYPE);
            updateOrgUnitChangeType(orgUnit, orgUnitType);
        }
    }

    private void updateOrgUnitChangeType(OrgUnit orgUnit, OrgUnitType orgUnitType)
    {
        Session session = getSession();

        // Если на Подразделении были Должности на подразделении или Сотрудники,
        // то Должности, на которые назначены сотрудники и Должности подразделения, становятся невалидными, т.к.
        // Тип подразделения и Тип подразделения, на которые ссылаются должности сотрудника и должности подразделения не совпадают.
        // Исправляем это

        session.flush();

        MQBuilder postBuilder = new MQBuilder(EmployeePost.ENTITY_CLASS, "b");
        postBuilder.add(MQExpression.eq("b", EmployeePost.orgUnit(), orgUnit));
        List<EmployeePost> employeePostList = new ArrayList<>(postBuilder.<EmployeePost>getResultList(session));

        MQBuilder orgUnitBuilder = new MQBuilder(OrgUnitPostRelation.ENTITY_CLASS, "b");
        orgUnitBuilder.add(MQExpression.eq("b", OrgUnitPostRelation.orgUnit(), orgUnit));
        List<OrgUnitPostRelation> relationList = new ArrayList<>(orgUnitBuilder.<OrgUnitPostRelation>getResultList(session));

        MQBuilder orgUnitTypePostRelationBuilder = new MQBuilder(OrgUnitTypePostRelation.ENTITY_CLASS, "b");
        List<OrgUnitTypePostRelation> postRelationList = new ArrayList<>(orgUnitTypePostRelationBuilder.<OrgUnitTypePostRelation>getResultList(session));

        Map<OrgUnitType, Map<PostBoundedWithQGandQL, OrgUnitTypePostRelation>> orgUnitTypePostRelationMap = new HashMap<>();

        for (OrgUnitTypePostRelation relation : postRelationList)
        {
            Map<PostBoundedWithQGandQL, OrgUnitTypePostRelation> postOrgUnitTypeMap = orgUnitTypePostRelationMap.get(relation.getOrgUnitType());
            if (postOrgUnitTypeMap == null)
                postOrgUnitTypeMap = new HashMap<>();

            postOrgUnitTypeMap.put(relation.getPostBoundedWithQGandQL(), relation);
            orgUnitTypePostRelationMap.put(relation.getOrgUnitType(), postOrgUnitTypeMap);
        }

        for (EmployeePost post : employeePostList)
        {
            OrgUnitType unitOrgUnitType = post.getOrgUnit().getOrgUnitType();
            OrgUnitType postOrgUnitType = post.getPostRelation().getOrgUnitType();
            if (!unitOrgUnitType.equals(postOrgUnitType))
            {
                Map<PostBoundedWithQGandQL, OrgUnitTypePostRelation> postOrgUnitTypeMap = orgUnitTypePostRelationMap.get(unitOrgUnitType);
                if (postOrgUnitTypeMap == null)
                    postOrgUnitTypeMap = new HashMap<>();

                OrgUnitTypePostRelation postRelation = postOrgUnitTypeMap.get(post.getPostRelation().getPostBoundedWithQGandQL());
                if (postRelation == null)
                {
                    postRelation = new OrgUnitTypePostRelation();
                    postRelation.setOrgUnitType(unitOrgUnitType);
                    postRelation.setPostBoundedWithQGandQL(post.getPostRelation().getPostBoundedWithQGandQL());
                    postRelation.setMultiPost(post.getPostRelation().isMultiPost());
                    postRelation.setHeaderPost(post.getPostRelation().isHeaderPost());

                    save(postRelation);

                    postOrgUnitTypeMap.put(post.getPostRelation().getPostBoundedWithQGandQL(), postRelation);
                    orgUnitTypePostRelationMap.put(unitOrgUnitType, postOrgUnitTypeMap);
                }

                post.setPostRelation(postRelation);

                update(post);
            }
        }

        for (OrgUnitPostRelation relation : relationList)
        {
            OrgUnitType unitOrgUnitType = relation.getOrgUnit().getOrgUnitType();
            OrgUnitType postOrgUnitType = relation.getOrgUnitTypePostRelation().getOrgUnitType();
            if (!unitOrgUnitType.equals(postOrgUnitType))
            {
                Map<PostBoundedWithQGandQL, OrgUnitTypePostRelation> postOrgUnitTypeMap = orgUnitTypePostRelationMap.get(unitOrgUnitType);
                if (postOrgUnitTypeMap == null)
                    postOrgUnitTypeMap = new HashMap<>();

                OrgUnitTypePostRelation postRelation = postOrgUnitTypeMap.get(relation.getOrgUnitTypePostRelation().getPostBoundedWithQGandQL());
                if (postRelation == null)
                {
                    postRelation = new OrgUnitTypePostRelation();
                    postRelation.setOrgUnitType(unitOrgUnitType);
                    postRelation.setPostBoundedWithQGandQL(relation.getOrgUnitTypePostRelation().getPostBoundedWithQGandQL());
                    postRelation.setMultiPost(relation.getOrgUnitTypePostRelation().isMultiPost());
                    postRelation.setHeaderPost(relation.getOrgUnitTypePostRelation().isHeaderPost());

                    save(postRelation);

                    postOrgUnitTypeMap.put(relation.getOrgUnitTypePostRelation().getPostBoundedWithQGandQL(), postRelation);
                    orgUnitTypePostRelationMap.put(unitOrgUnitType, postOrgUnitTypeMap);
                }

                relation.setOrgUnitTypePostRelation(postRelation);

                update(relation);
            }
        }
    }
}
