package ru.tandemservice.uniemp.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.catalog.PaymentType;
import ru.tandemservice.uniemp.entity.catalog.PaymentUnit;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выплата
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PaymentGen extends EntityBase
 implements INaturalIdentifiable<PaymentGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.catalog.Payment";
    public static final String ENTITY_NAME = "payment";
    public static final int VERSION_HASH = -481189358;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_REQUIRED = "required";
    public static final String L_TYPE = "type";
    public static final String L_PAYMENT_UNIT = "paymentUnit";
    public static final String L_FINANCING_SOURCE = "financingSource";
    public static final String P_VALUE = "value";
    public static final String P_FIXED_VALUE = "fixedValue";
    public static final String P_DOESNT_DEPEND_ON_STAFF_RATE = "doesntDependOnStaffRate";
    public static final String P_ACTIVE = "active";
    public static final String P_ONE_TIME_PAYMENT = "oneTimePayment";
    public static final String P_NOMINATIVE_CASE_TITLE = "nominativeCaseTitle";
    public static final String P_GENITIVE_CASE_TITLE = "genitiveCaseTitle";
    public static final String P_DATIVE_CASE_TITLE = "dativeCaseTitle";
    public static final String P_ACCUSATIVE_CASE_TITLE = "accusativeCaseTitle";
    public static final String P_INSTRUMENTAL_CASE_TITLE = "instrumentalCaseTitle";
    public static final String P_PREPOSITIONAL_CASE_TITLE = "prepositionalCaseTitle";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _shortTitle;     // Сокращенное название
    private boolean _required;     // Обязательна
    private PaymentType _type;     // Тип выплаты
    private PaymentUnit _paymentUnit;     // Формат ввода выплаты
    private FinancingSource _financingSource;     // Источник финансирования
    private Double _value;     // Базовое значение
    private boolean _fixedValue;     // Фиксированное значение
    private boolean _doesntDependOnStaffRate;     // Не зависит от ставки
    private boolean _active;     // Используется
    private boolean _oneTimePayment;     // Разовая
    private String _nominativeCaseTitle;     // Именительный падеж
    private String _genitiveCaseTitle;     // Родительный падеж
    private String _dativeCaseTitle;     // Дательный падеж
    private String _accusativeCaseTitle;     // Винительный падеж
    private String _instrumentalCaseTitle;     // Творительный падеж
    private String _prepositionalCaseTitle;     // Предложный падеж
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название. Свойство не может быть null.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Обязательна. Свойство не может быть null.
     */
    @NotNull
    public boolean isRequired()
    {
        return _required;
    }

    /**
     * @param required Обязательна. Свойство не может быть null.
     */
    public void setRequired(boolean required)
    {
        dirty(_required, required);
        _required = required;
    }

    /**
     * @return Тип выплаты. Свойство не может быть null.
     */
    @NotNull
    public PaymentType getType()
    {
        return _type;
    }

    /**
     * @param type Тип выплаты. Свойство не может быть null.
     */
    public void setType(PaymentType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Формат ввода выплаты. Свойство не может быть null.
     */
    @NotNull
    public PaymentUnit getPaymentUnit()
    {
        return _paymentUnit;
    }

    /**
     * @param paymentUnit Формат ввода выплаты. Свойство не может быть null.
     */
    public void setPaymentUnit(PaymentUnit paymentUnit)
    {
        dirty(_paymentUnit, paymentUnit);
        _paymentUnit = paymentUnit;
    }

    /**
     * @return Источник финансирования.
     */
    public FinancingSource getFinancingSource()
    {
        return _financingSource;
    }

    /**
     * @param financingSource Источник финансирования.
     */
    public void setFinancingSource(FinancingSource financingSource)
    {
        dirty(_financingSource, financingSource);
        _financingSource = financingSource;
    }

    /**
     * @return Базовое значение.
     */
    public Double getValue()
    {
        return _value;
    }

    /**
     * @param value Базовое значение.
     */
    public void setValue(Double value)
    {
        dirty(_value, value);
        _value = value;
    }

    /**
     * @return Фиксированное значение. Свойство не может быть null.
     */
    @NotNull
    public boolean isFixedValue()
    {
        return _fixedValue;
    }

    /**
     * @param fixedValue Фиксированное значение. Свойство не может быть null.
     */
    public void setFixedValue(boolean fixedValue)
    {
        dirty(_fixedValue, fixedValue);
        _fixedValue = fixedValue;
    }

    /**
     * @return Не зависит от ставки. Свойство не может быть null.
     */
    @NotNull
    public boolean isDoesntDependOnStaffRate()
    {
        return _doesntDependOnStaffRate;
    }

    /**
     * @param doesntDependOnStaffRate Не зависит от ставки. Свойство не может быть null.
     */
    public void setDoesntDependOnStaffRate(boolean doesntDependOnStaffRate)
    {
        dirty(_doesntDependOnStaffRate, doesntDependOnStaffRate);
        _doesntDependOnStaffRate = doesntDependOnStaffRate;
    }

    /**
     * @return Используется. Свойство не может быть null.
     */
    @NotNull
    public boolean isActive()
    {
        return _active;
    }

    /**
     * @param active Используется. Свойство не может быть null.
     */
    public void setActive(boolean active)
    {
        dirty(_active, active);
        _active = active;
    }

    /**
     * @return Разовая. Свойство не может быть null.
     */
    @NotNull
    public boolean isOneTimePayment()
    {
        return _oneTimePayment;
    }

    /**
     * @param oneTimePayment Разовая. Свойство не может быть null.
     */
    public void setOneTimePayment(boolean oneTimePayment)
    {
        dirty(_oneTimePayment, oneTimePayment);
        _oneTimePayment = oneTimePayment;
    }

    /**
     * @return Именительный падеж.
     */
    @Length(max=255)
    public String getNominativeCaseTitle()
    {
        return _nominativeCaseTitle;
    }

    /**
     * @param nominativeCaseTitle Именительный падеж.
     */
    public void setNominativeCaseTitle(String nominativeCaseTitle)
    {
        dirty(_nominativeCaseTitle, nominativeCaseTitle);
        _nominativeCaseTitle = nominativeCaseTitle;
    }

    /**
     * @return Родительный падеж.
     */
    @Length(max=255)
    public String getGenitiveCaseTitle()
    {
        return _genitiveCaseTitle;
    }

    /**
     * @param genitiveCaseTitle Родительный падеж.
     */
    public void setGenitiveCaseTitle(String genitiveCaseTitle)
    {
        dirty(_genitiveCaseTitle, genitiveCaseTitle);
        _genitiveCaseTitle = genitiveCaseTitle;
    }

    /**
     * @return Дательный падеж.
     */
    @Length(max=255)
    public String getDativeCaseTitle()
    {
        return _dativeCaseTitle;
    }

    /**
     * @param dativeCaseTitle Дательный падеж.
     */
    public void setDativeCaseTitle(String dativeCaseTitle)
    {
        dirty(_dativeCaseTitle, dativeCaseTitle);
        _dativeCaseTitle = dativeCaseTitle;
    }

    /**
     * @return Винительный падеж.
     */
    @Length(max=255)
    public String getAccusativeCaseTitle()
    {
        return _accusativeCaseTitle;
    }

    /**
     * @param accusativeCaseTitle Винительный падеж.
     */
    public void setAccusativeCaseTitle(String accusativeCaseTitle)
    {
        dirty(_accusativeCaseTitle, accusativeCaseTitle);
        _accusativeCaseTitle = accusativeCaseTitle;
    }

    /**
     * @return Творительный падеж.
     */
    @Length(max=255)
    public String getInstrumentalCaseTitle()
    {
        return _instrumentalCaseTitle;
    }

    /**
     * @param instrumentalCaseTitle Творительный падеж.
     */
    public void setInstrumentalCaseTitle(String instrumentalCaseTitle)
    {
        dirty(_instrumentalCaseTitle, instrumentalCaseTitle);
        _instrumentalCaseTitle = instrumentalCaseTitle;
    }

    /**
     * @return Предложный падеж.
     */
    @Length(max=255)
    public String getPrepositionalCaseTitle()
    {
        return _prepositionalCaseTitle;
    }

    /**
     * @param prepositionalCaseTitle Предложный падеж.
     */
    public void setPrepositionalCaseTitle(String prepositionalCaseTitle)
    {
        dirty(_prepositionalCaseTitle, prepositionalCaseTitle);
        _prepositionalCaseTitle = prepositionalCaseTitle;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PaymentGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((Payment)another).getCode());
            }
            setShortTitle(((Payment)another).getShortTitle());
            setRequired(((Payment)another).isRequired());
            setType(((Payment)another).getType());
            setPaymentUnit(((Payment)another).getPaymentUnit());
            setFinancingSource(((Payment)another).getFinancingSource());
            setValue(((Payment)another).getValue());
            setFixedValue(((Payment)another).isFixedValue());
            setDoesntDependOnStaffRate(((Payment)another).isDoesntDependOnStaffRate());
            setActive(((Payment)another).isActive());
            setOneTimePayment(((Payment)another).isOneTimePayment());
            setNominativeCaseTitle(((Payment)another).getNominativeCaseTitle());
            setGenitiveCaseTitle(((Payment)another).getGenitiveCaseTitle());
            setDativeCaseTitle(((Payment)another).getDativeCaseTitle());
            setAccusativeCaseTitle(((Payment)another).getAccusativeCaseTitle());
            setInstrumentalCaseTitle(((Payment)another).getInstrumentalCaseTitle());
            setPrepositionalCaseTitle(((Payment)another).getPrepositionalCaseTitle());
            setTitle(((Payment)another).getTitle());
        }
    }

    public INaturalId<PaymentGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<PaymentGen>
    {
        private static final String PROXY_NAME = "PaymentNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof PaymentGen.NaturalId) ) return false;

            PaymentGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PaymentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) Payment.class;
        }

        public T newInstance()
        {
            return (T) new Payment();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "shortTitle":
                    return obj.getShortTitle();
                case "required":
                    return obj.isRequired();
                case "type":
                    return obj.getType();
                case "paymentUnit":
                    return obj.getPaymentUnit();
                case "financingSource":
                    return obj.getFinancingSource();
                case "value":
                    return obj.getValue();
                case "fixedValue":
                    return obj.isFixedValue();
                case "doesntDependOnStaffRate":
                    return obj.isDoesntDependOnStaffRate();
                case "active":
                    return obj.isActive();
                case "oneTimePayment":
                    return obj.isOneTimePayment();
                case "nominativeCaseTitle":
                    return obj.getNominativeCaseTitle();
                case "genitiveCaseTitle":
                    return obj.getGenitiveCaseTitle();
                case "dativeCaseTitle":
                    return obj.getDativeCaseTitle();
                case "accusativeCaseTitle":
                    return obj.getAccusativeCaseTitle();
                case "instrumentalCaseTitle":
                    return obj.getInstrumentalCaseTitle();
                case "prepositionalCaseTitle":
                    return obj.getPrepositionalCaseTitle();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "required":
                    obj.setRequired((Boolean) value);
                    return;
                case "type":
                    obj.setType((PaymentType) value);
                    return;
                case "paymentUnit":
                    obj.setPaymentUnit((PaymentUnit) value);
                    return;
                case "financingSource":
                    obj.setFinancingSource((FinancingSource) value);
                    return;
                case "value":
                    obj.setValue((Double) value);
                    return;
                case "fixedValue":
                    obj.setFixedValue((Boolean) value);
                    return;
                case "doesntDependOnStaffRate":
                    obj.setDoesntDependOnStaffRate((Boolean) value);
                    return;
                case "active":
                    obj.setActive((Boolean) value);
                    return;
                case "oneTimePayment":
                    obj.setOneTimePayment((Boolean) value);
                    return;
                case "nominativeCaseTitle":
                    obj.setNominativeCaseTitle((String) value);
                    return;
                case "genitiveCaseTitle":
                    obj.setGenitiveCaseTitle((String) value);
                    return;
                case "dativeCaseTitle":
                    obj.setDativeCaseTitle((String) value);
                    return;
                case "accusativeCaseTitle":
                    obj.setAccusativeCaseTitle((String) value);
                    return;
                case "instrumentalCaseTitle":
                    obj.setInstrumentalCaseTitle((String) value);
                    return;
                case "prepositionalCaseTitle":
                    obj.setPrepositionalCaseTitle((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "shortTitle":
                        return true;
                case "required":
                        return true;
                case "type":
                        return true;
                case "paymentUnit":
                        return true;
                case "financingSource":
                        return true;
                case "value":
                        return true;
                case "fixedValue":
                        return true;
                case "doesntDependOnStaffRate":
                        return true;
                case "active":
                        return true;
                case "oneTimePayment":
                        return true;
                case "nominativeCaseTitle":
                        return true;
                case "genitiveCaseTitle":
                        return true;
                case "dativeCaseTitle":
                        return true;
                case "accusativeCaseTitle":
                        return true;
                case "instrumentalCaseTitle":
                        return true;
                case "prepositionalCaseTitle":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "shortTitle":
                    return true;
                case "required":
                    return true;
                case "type":
                    return true;
                case "paymentUnit":
                    return true;
                case "financingSource":
                    return true;
                case "value":
                    return true;
                case "fixedValue":
                    return true;
                case "doesntDependOnStaffRate":
                    return true;
                case "active":
                    return true;
                case "oneTimePayment":
                    return true;
                case "nominativeCaseTitle":
                    return true;
                case "genitiveCaseTitle":
                    return true;
                case "dativeCaseTitle":
                    return true;
                case "accusativeCaseTitle":
                    return true;
                case "instrumentalCaseTitle":
                    return true;
                case "prepositionalCaseTitle":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "shortTitle":
                    return String.class;
                case "required":
                    return Boolean.class;
                case "type":
                    return PaymentType.class;
                case "paymentUnit":
                    return PaymentUnit.class;
                case "financingSource":
                    return FinancingSource.class;
                case "value":
                    return Double.class;
                case "fixedValue":
                    return Boolean.class;
                case "doesntDependOnStaffRate":
                    return Boolean.class;
                case "active":
                    return Boolean.class;
                case "oneTimePayment":
                    return Boolean.class;
                case "nominativeCaseTitle":
                    return String.class;
                case "genitiveCaseTitle":
                    return String.class;
                case "dativeCaseTitle":
                    return String.class;
                case "accusativeCaseTitle":
                    return String.class;
                case "instrumentalCaseTitle":
                    return String.class;
                case "prepositionalCaseTitle":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<Payment> _dslPath = new Path<Payment>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "Payment");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniemp.entity.catalog.Payment#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.catalog.Payment#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Обязательна. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.catalog.Payment#isRequired()
     */
    public static PropertyPath<Boolean> required()
    {
        return _dslPath.required();
    }

    /**
     * @return Тип выплаты. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.catalog.Payment#getType()
     */
    public static PaymentType.Path<PaymentType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Формат ввода выплаты. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.catalog.Payment#getPaymentUnit()
     */
    public static PaymentUnit.Path<PaymentUnit> paymentUnit()
    {
        return _dslPath.paymentUnit();
    }

    /**
     * @return Источник финансирования.
     * @see ru.tandemservice.uniemp.entity.catalog.Payment#getFinancingSource()
     */
    public static FinancingSource.Path<FinancingSource> financingSource()
    {
        return _dslPath.financingSource();
    }

    /**
     * @return Базовое значение.
     * @see ru.tandemservice.uniemp.entity.catalog.Payment#getValue()
     */
    public static PropertyPath<Double> value()
    {
        return _dslPath.value();
    }

    /**
     * @return Фиксированное значение. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.catalog.Payment#isFixedValue()
     */
    public static PropertyPath<Boolean> fixedValue()
    {
        return _dslPath.fixedValue();
    }

    /**
     * @return Не зависит от ставки. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.catalog.Payment#isDoesntDependOnStaffRate()
     */
    public static PropertyPath<Boolean> doesntDependOnStaffRate()
    {
        return _dslPath.doesntDependOnStaffRate();
    }

    /**
     * @return Используется. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.catalog.Payment#isActive()
     */
    public static PropertyPath<Boolean> active()
    {
        return _dslPath.active();
    }

    /**
     * @return Разовая. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.catalog.Payment#isOneTimePayment()
     */
    public static PropertyPath<Boolean> oneTimePayment()
    {
        return _dslPath.oneTimePayment();
    }

    /**
     * @return Именительный падеж.
     * @see ru.tandemservice.uniemp.entity.catalog.Payment#getNominativeCaseTitle()
     */
    public static PropertyPath<String> nominativeCaseTitle()
    {
        return _dslPath.nominativeCaseTitle();
    }

    /**
     * @return Родительный падеж.
     * @see ru.tandemservice.uniemp.entity.catalog.Payment#getGenitiveCaseTitle()
     */
    public static PropertyPath<String> genitiveCaseTitle()
    {
        return _dslPath.genitiveCaseTitle();
    }

    /**
     * @return Дательный падеж.
     * @see ru.tandemservice.uniemp.entity.catalog.Payment#getDativeCaseTitle()
     */
    public static PropertyPath<String> dativeCaseTitle()
    {
        return _dslPath.dativeCaseTitle();
    }

    /**
     * @return Винительный падеж.
     * @see ru.tandemservice.uniemp.entity.catalog.Payment#getAccusativeCaseTitle()
     */
    public static PropertyPath<String> accusativeCaseTitle()
    {
        return _dslPath.accusativeCaseTitle();
    }

    /**
     * @return Творительный падеж.
     * @see ru.tandemservice.uniemp.entity.catalog.Payment#getInstrumentalCaseTitle()
     */
    public static PropertyPath<String> instrumentalCaseTitle()
    {
        return _dslPath.instrumentalCaseTitle();
    }

    /**
     * @return Предложный падеж.
     * @see ru.tandemservice.uniemp.entity.catalog.Payment#getPrepositionalCaseTitle()
     */
    public static PropertyPath<String> prepositionalCaseTitle()
    {
        return _dslPath.prepositionalCaseTitle();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uniemp.entity.catalog.Payment#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends Payment> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<Boolean> _required;
        private PaymentType.Path<PaymentType> _type;
        private PaymentUnit.Path<PaymentUnit> _paymentUnit;
        private FinancingSource.Path<FinancingSource> _financingSource;
        private PropertyPath<Double> _value;
        private PropertyPath<Boolean> _fixedValue;
        private PropertyPath<Boolean> _doesntDependOnStaffRate;
        private PropertyPath<Boolean> _active;
        private PropertyPath<Boolean> _oneTimePayment;
        private PropertyPath<String> _nominativeCaseTitle;
        private PropertyPath<String> _genitiveCaseTitle;
        private PropertyPath<String> _dativeCaseTitle;
        private PropertyPath<String> _accusativeCaseTitle;
        private PropertyPath<String> _instrumentalCaseTitle;
        private PropertyPath<String> _prepositionalCaseTitle;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniemp.entity.catalog.Payment#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(PaymentGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.catalog.Payment#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(PaymentGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Обязательна. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.catalog.Payment#isRequired()
     */
        public PropertyPath<Boolean> required()
        {
            if(_required == null )
                _required = new PropertyPath<Boolean>(PaymentGen.P_REQUIRED, this);
            return _required;
        }

    /**
     * @return Тип выплаты. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.catalog.Payment#getType()
     */
        public PaymentType.Path<PaymentType> type()
        {
            if(_type == null )
                _type = new PaymentType.Path<PaymentType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Формат ввода выплаты. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.catalog.Payment#getPaymentUnit()
     */
        public PaymentUnit.Path<PaymentUnit> paymentUnit()
        {
            if(_paymentUnit == null )
                _paymentUnit = new PaymentUnit.Path<PaymentUnit>(L_PAYMENT_UNIT, this);
            return _paymentUnit;
        }

    /**
     * @return Источник финансирования.
     * @see ru.tandemservice.uniemp.entity.catalog.Payment#getFinancingSource()
     */
        public FinancingSource.Path<FinancingSource> financingSource()
        {
            if(_financingSource == null )
                _financingSource = new FinancingSource.Path<FinancingSource>(L_FINANCING_SOURCE, this);
            return _financingSource;
        }

    /**
     * @return Базовое значение.
     * @see ru.tandemservice.uniemp.entity.catalog.Payment#getValue()
     */
        public PropertyPath<Double> value()
        {
            if(_value == null )
                _value = new PropertyPath<Double>(PaymentGen.P_VALUE, this);
            return _value;
        }

    /**
     * @return Фиксированное значение. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.catalog.Payment#isFixedValue()
     */
        public PropertyPath<Boolean> fixedValue()
        {
            if(_fixedValue == null )
                _fixedValue = new PropertyPath<Boolean>(PaymentGen.P_FIXED_VALUE, this);
            return _fixedValue;
        }

    /**
     * @return Не зависит от ставки. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.catalog.Payment#isDoesntDependOnStaffRate()
     */
        public PropertyPath<Boolean> doesntDependOnStaffRate()
        {
            if(_doesntDependOnStaffRate == null )
                _doesntDependOnStaffRate = new PropertyPath<Boolean>(PaymentGen.P_DOESNT_DEPEND_ON_STAFF_RATE, this);
            return _doesntDependOnStaffRate;
        }

    /**
     * @return Используется. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.catalog.Payment#isActive()
     */
        public PropertyPath<Boolean> active()
        {
            if(_active == null )
                _active = new PropertyPath<Boolean>(PaymentGen.P_ACTIVE, this);
            return _active;
        }

    /**
     * @return Разовая. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.catalog.Payment#isOneTimePayment()
     */
        public PropertyPath<Boolean> oneTimePayment()
        {
            if(_oneTimePayment == null )
                _oneTimePayment = new PropertyPath<Boolean>(PaymentGen.P_ONE_TIME_PAYMENT, this);
            return _oneTimePayment;
        }

    /**
     * @return Именительный падеж.
     * @see ru.tandemservice.uniemp.entity.catalog.Payment#getNominativeCaseTitle()
     */
        public PropertyPath<String> nominativeCaseTitle()
        {
            if(_nominativeCaseTitle == null )
                _nominativeCaseTitle = new PropertyPath<String>(PaymentGen.P_NOMINATIVE_CASE_TITLE, this);
            return _nominativeCaseTitle;
        }

    /**
     * @return Родительный падеж.
     * @see ru.tandemservice.uniemp.entity.catalog.Payment#getGenitiveCaseTitle()
     */
        public PropertyPath<String> genitiveCaseTitle()
        {
            if(_genitiveCaseTitle == null )
                _genitiveCaseTitle = new PropertyPath<String>(PaymentGen.P_GENITIVE_CASE_TITLE, this);
            return _genitiveCaseTitle;
        }

    /**
     * @return Дательный падеж.
     * @see ru.tandemservice.uniemp.entity.catalog.Payment#getDativeCaseTitle()
     */
        public PropertyPath<String> dativeCaseTitle()
        {
            if(_dativeCaseTitle == null )
                _dativeCaseTitle = new PropertyPath<String>(PaymentGen.P_DATIVE_CASE_TITLE, this);
            return _dativeCaseTitle;
        }

    /**
     * @return Винительный падеж.
     * @see ru.tandemservice.uniemp.entity.catalog.Payment#getAccusativeCaseTitle()
     */
        public PropertyPath<String> accusativeCaseTitle()
        {
            if(_accusativeCaseTitle == null )
                _accusativeCaseTitle = new PropertyPath<String>(PaymentGen.P_ACCUSATIVE_CASE_TITLE, this);
            return _accusativeCaseTitle;
        }

    /**
     * @return Творительный падеж.
     * @see ru.tandemservice.uniemp.entity.catalog.Payment#getInstrumentalCaseTitle()
     */
        public PropertyPath<String> instrumentalCaseTitle()
        {
            if(_instrumentalCaseTitle == null )
                _instrumentalCaseTitle = new PropertyPath<String>(PaymentGen.P_INSTRUMENTAL_CASE_TITLE, this);
            return _instrumentalCaseTitle;
        }

    /**
     * @return Предложный падеж.
     * @see ru.tandemservice.uniemp.entity.catalog.Payment#getPrepositionalCaseTitle()
     */
        public PropertyPath<String> prepositionalCaseTitle()
        {
            if(_prepositionalCaseTitle == null )
                _prepositionalCaseTitle = new PropertyPath<String>(PaymentGen.P_PREPOSITIONAL_CASE_TITLE, this);
            return _prepositionalCaseTitle;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uniemp.entity.catalog.Payment#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(PaymentGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return Payment.class;
        }

        public String getEntityName()
        {
            return "payment";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
