/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.dao;

import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeWorkWeekDuration;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import org.tandemframework.shared.person.base.entity.PersonAcademicStatus;
import org.tandemframework.shared.person.catalog.entity.ScienceDegreeType;
import org.tandemframework.shared.person.catalog.entity.ScienceStatusType;
import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.uniemp.entity.catalog.*;
import ru.tandemservice.uniemp.entity.employee.*;
import ru.tandemservice.uniemp.base.ext.Employee.logic.empServiceLengthWrappers.EmployeeServiceLengthWrapper;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author E. Grigoriev
 * @since 08.07.2008
 */
public interface IUniempDAO<Model> extends IUniDao<Model>
{
    String UNIEMP_DAO_BEAN_NAME = "uniempDao";

    final SpringBeanCache<IUniempDAO<Object>> instance = new SpringBeanCache<>(UNIEMP_DAO_BEAN_NAME);

    /**
     * @return Возвращает список ставок активного ШР которые ссылкаются на данного сотрудника.
     * Исключаются ставки по совмещению.
     */
    List<StaffListAllocationItem> getEmployeePostAllocationItemList(EmployeePost employeePost);

    /**
     * @return Возвращает <tt>Map</tt> в которой ключами являются id сотрудников, а значениями список ставок сотрудника.
     */
    Map<Long, List<EmployeePostStaffRateItem>> getEmployeePostStaffRateItemMap();

    /**
     * @return Возвращает спискок ставок сотруднка.<p>
     * Если для сотрудника нет ставок, то возвращает пустой список.
     */
    List<EmployeePostStaffRateItem> getEmployeePostStaffRateItemList(EmployeePost employeePost);

    EmployeeCard updateEmployeeForEmployeeCard(Employee employee);

    void deleteOrgUnitPostRelation(OrgUnitPostRelation rel);

    <T> List<T> getOrgUnitPostRelations(OrgUnitPostRelation parentRel, Class<T> relTypeClass, String catalogProperty);

    /**
     * @return Возвращает кол-во занятых штатных единиц на активном ШР.
     */
    Double getStaffRateForOrgUnitPost(OrgUnitTypePostRelation orgUnitTypePostRelation, OrgUnit orgUnit, FinancingSource financingSource, FinancingSourceItem financingSourceItem);

    /**
     * @return Возвращает кол-во занятых штатных единиц не учитывая Ист.фин.(дет.), т.е. по всем ист.фин.(дет.)
     */
    Double getStaffRateForOrgUnitPost(OrgUnitTypePostRelation orgUnitTypePostRelation, OrgUnit orgUnit, FinancingSource financingSource);

    /**
     * @return Возвращает кол-во занятых штатных единиц.
     */
    Double getStaffRateForOrgUnitPost(StaffList staffList, OrgUnitPostRelation orgUnitPostRelation, OrgUnit orgUnit, FinancingSource financingSource, FinancingSourceItem financingSourceItem);

    void validateStaffRates(ErrorCollector errCollector, Employee employee, EmployeePost employeePost, List<EmployeePostStaffRateItem> staffRateItemList, boolean isNew, boolean hasFiledsToCollectErrors);

    EmployeeLabourContract getEmployeePostLabourContract(EmployeePost employeePost);

    List<ContractCollateralAgreement> getContractCollateralAgreementsList(EmployeeLabourContract contract);

    EmployeeType getVPOReportLineEmployeeType(EmployeeVPO1ReportLines repLine);

    List<PostBoundedWithQGandQL> getVPOReportLinePosts(EmployeeVPO1ReportLines repLine);

    double getPaymentBaseValueForPost(Payment payment, PostBoundedWithQGandQL post);

    PersonAcademicDegree getEmployeeUpperAcademicDegree(Employee employee);

    PersonAcademicStatus getEmployeeUpperAcademicStatus(Employee employee);

    /**
     * @return Возвращает список ученых степеней персоны.
     */
    List<PersonAcademicDegree> getEmployeeAcademicDegreeList(Employee employee);

    void validateVacationScheduleItemHasIntersections(VacationScheduleItem vacationScheduleItem, ErrorCollector errorCollector, String errorObject, String planDateFieldName, String factDateFieldName, String daysAmountFieldName);

    void deleteVacationSchedule(VacationSchedule vacationSchedule);

    /**
     * Возвращает MAP с учеными степенями персон
     * ключ - персона
     * значение - тип ученой степени (доктор или кандидат)
     *
     * Все степени, дата присуждения которых до reportDate
     */
    Map<Person, ScienceDegreeType> getEmployeeDegreesMap(Date reportDate);

    /**
     * Возвращает MAP с учеными званиями персон
     * ключ - персона
     * значение - тип ученого звания (Академик, профессор, доцент или СНС)
     *
     * Все звания, дата присуждения которых до reportDate
     */
    Map<Person, ScienceStatusType> getEmployeeStatusesMap(Date reportDate);

    /**
     * Возвращает MAP с учеными степенями кадровых ресурсов
     * ключ - id кадрового ресурса,
     * значение - тип ученой степени (доктор или кандидат)
     */
    Map<Person, List<ScienceDegreeType>> getEmployeeDegreesFullMap(Date reportDate);

    /**
     * Возвращает MAP с учеными званиями кадровых ресурсов
     * ключ - id кадрового ресурса,
     * значение - тип ученого звания (Академик, профессор, доцент или СНС)
     */
    Map<Person, List<ScienceStatusType>> getEmployeeStatusesFullMap(Date reportDate);

    /**
     * Возвращает список с выплатами, на которые ссылается текущая выплата (<tt>payment</tt>)<p>
     * в соответствии в настройкой "Порядок начисления выплат на ФОТ с надбавками" (<tt>PaymentOnFOTToPaymentsSettings</tt>).
     * @param payment - выплата
     * @return список выплат, относительно соответствующей настройки
     */
    public List<Payment> getPaymentOnFOTToPaymentsList(final Payment payment);

    /**
     * @param filter строка, в названии ист.фин.(дет.)
     * @return Возвращает список Источников финансирования (детально) по id Источника финансирования.
     */
    public List<FinancingSourceItem> getFinSrcItm(Long finSrcId, String filter);

    /**
     * @return <tt><b>true</tt></b>, если <tt>date</tt> является праздником в производственном календаре, иначе <tt><b>false</tt></b>
     */
    public boolean isIndustrialCalendarHolidayDay(EmployeeWorkWeekDuration weekDuration, Date date);

    /**
     * @return Возвращает список выплат сотрудника.
     */
    public List<EmployeePayment> getEmployeePostPaymentList(EmployeePost employeePost);

    /**
     * @return Возвращает основную должность КР.
     */
    public EmployeePost getMainJob(Employee employee);

    /**
     * @return Возвращает список должностей КР у которых статус внутреннего совмещения.
     */
    public List<EmployeePost> getEmployeeSecondJobList(Employee employee, String postType);

    /**
     * @return Возвращает список ставок должности по совмещению.
     */
    public List<CombinationPostStaffRateItem> getCombinationPostStaffRateItemList(CombinationPost post);

    /**
     * @return Возвращает список возможных должностей для указанного типа подразделения в соответствии с настройкой.
     */
    public List<OrgUnitTypePostRelation> getPostRelationList(OrgUnit orgUnit, PostBoundedWithQGandQL postBoundedWithQGandQL, String filter, Integer takeTopElementsCount);

    /**
     * Проверяет наличие свободных штатных единиц
     */
    public void validateStaffListRate(ErrorCollector errCollector, EmployeePost employeePost, List<EmployeePostStaffRateItem> staffRateItemList, boolean hasFiledsToCollectErrors);

    /**
     * Поднимает список Видов стажей для Должности отнесенной к ПКГ и КУ.
     * @return список Видов стажей Должности
     */
    public List<ServiceLengthType> getPostServiceLengthTypeList(PostBoundedWithQGandQL postBoundedWithQGandQL);

    /**
     * Поднимает список Видов стажей для Элемента истории должностей.
     * @return список Видов стажей Истории должностей
     */
    public List<ServiceLengthType> getEmploymentHistoryServiceLengthTypeList(EmploymentHistoryItemBase employmentHistoryItemBase);

    /**
     * Вычесляет список враперов Стажей Сотрудника.
     * Враперы строятся на основе фейковых и остальных Истории должностей.
     * @param employee кадровый ресурс
     * @param entityId если not null, то фейковый элемент истории должностей с таким id исключается
     * @return список Стажей
     */
    public List<EmployeeServiceLengthWrapper> getEmployeeServiceLengthWrapperList(Employee employee, Long entityId);
}