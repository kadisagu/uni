package ru.tandemservice.uniemp.component.reports.quantityEmployeeByDepartments.quantityEmployeeByDepartmentsPub;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.entity.report.QuantityEmpByDepReport;

public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setReport(getNotNull(QuantityEmpByDepReport.class, model.getReport().getId()));
    }
}
