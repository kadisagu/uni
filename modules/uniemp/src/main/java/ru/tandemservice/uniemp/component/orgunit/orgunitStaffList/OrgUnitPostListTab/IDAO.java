/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.OrgUnitPostListTab;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation;

import java.util.List;

/**
 * @author dseleznev
 * Created on: 16.09.2008
 */
public interface IDAO extends IUniDao<Model>
{
    List<OrgUnitPostRelation> getOrgUnitPostRelationsList(OrgUnit orgUnit);

    void preparePostListDataSource(Model model);
}
