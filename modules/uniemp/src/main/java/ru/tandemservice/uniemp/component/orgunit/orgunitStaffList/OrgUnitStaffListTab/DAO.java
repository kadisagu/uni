/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.OrgUnitStaffListTab;

import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.employeebase.catalog.entity.Post;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation;
import ru.tandemservice.uniemp.entity.employee.StaffList;

/**
 * @author dseleznev
 *         Created on: 16.09.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("rel");

    static
    {
        _orderSettings.setOrders(OrgUnitPostRelation.P_SIMPLE_TITLE, new OrderDescription("p", Post.P_TITLE));
    }

    @Override
    public void prepare(Model model)
    {
        if (null != model.getOrgUnitId())
        {
            model.setOrgUnit((OrgUnit) get(model.getOrgUnitId()));
        }
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        prepareStaffListDataSource(model);
    }

    @Override
    public void prepareStaffListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(StaffList.ENTITY_CLASS, "rel");
        builder.add(MQExpression.eq("rel", StaffList.L_ORG_UNIT, model.getOrgUnit()));
        builder.addOrder("rel", StaffList.P_NUMBER, OrderDirection.desc);

        UniBaseUtils.createPage(model.getStaffListDataSource(), builder, getSession());
    }
}