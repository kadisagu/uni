/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffListItem.StaffListPostPaymentAddEdit;

import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.PaymentType;
import ru.tandemservice.uniemp.entity.employee.StaffListItem;
import ru.tandemservice.uniemp.entity.employee.StaffListPostPayment;

import java.util.List;

/**
 * @author dseleznev
 * Created on: 07.10.2009
 */
@Input(keys = { "staffListItemId", "staffListPostPaymentId" }, bindings = { "staffListItemId", "staffListPostPaymentId" })
public class Model
{
    private Long _staffListItemId;
    private Long _staffListPostPaymentId;

    private ISelectModel _financingSourceItemsListModel;

    private StaffListItem _staffListItem;
    private StaffListPostPayment _staffListPostPayment = new StaffListPostPayment();

    private PaymentType _paymentType;

    private ISelectModel _paymentsListModel;
    private List<HSelectOption> _paymentTypesList;
    private List<FinancingSource> _financingSourcesList;

    //Getters & Setters


    public ISelectModel getFinancingSourceItemsListModel()
    {
        return _financingSourceItemsListModel;
    }

    public void setFinancingSourceItemsListModel(ISelectModel _financingSourceItemsListModel)
    {
        this._financingSourceItemsListModel = _financingSourceItemsListModel;
    }

    public Long getStaffListItemId()
    {
        return _staffListItemId;
    }

    public void setStaffListItemId(Long staffListItemId)
    {
        this._staffListItemId = staffListItemId;
    }

    public Long getStaffListPostPaymentId()
    {
        return _staffListPostPaymentId;
    }

    public void setStaffListPostPaymentId(Long staffListPostPaymentId)
    {
        _staffListPostPaymentId = staffListPostPaymentId;
    }

    public StaffListItem getStaffListItem()
    {
        return _staffListItem;
    }

    public void setStaffListItem(StaffListItem staffListItem)
    {
        this._staffListItem = staffListItem;
    }

    public StaffListPostPayment getStaffListPostPayment()
    {
        return _staffListPostPayment;
    }

    public void setStaffListPostPayment(StaffListPostPayment staffListPostPayment)
    {
        this._staffListPostPayment = staffListPostPayment;
    }

    public PaymentType getPaymentType()
    {
        return _paymentType;
    }

    public void setPaymentType(PaymentType paymentType)
    {
        this._paymentType = paymentType;
    }

    public ISelectModel getPaymentsListModel()
    {
        return _paymentsListModel;
    }

    public void setPaymentsListModel(ISelectModel paymentsListModel)
    {
        this._paymentsListModel = paymentsListModel;
    }

    public List<HSelectOption> getPaymentTypesList()
    {
        return _paymentTypesList;
    }

    public void setPaymentTypesList(List<HSelectOption> paymentTypesList)
    {
        this._paymentTypesList = paymentTypesList;
    }

    public List<FinancingSource> getFinancingSourcesList()
    {
        return _financingSourcesList;
    }

    public void setFinancingSourcesList(List<FinancingSource> financingSourcesList)
    {
        this._financingSourcesList = financingSourcesList;
    }
}