/* $Id$ */
package ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.AdditionalDataTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.employeebase.base.bo.EmployeeReportPerson.ui.Add.EmployeeReportPersonAddUI;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportDQLModifierOwner;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportPrintInfo;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.ReportDQL;
import ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.AdditionalDataTab.block.attestationData.AttestationDataBlock;
import ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.AdditionalDataTab.block.attestationData.AttestationDataParam;
import ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.AdditionalDataTab.block.contractData.ContractDataBlock;
import ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.AdditionalDataTab.block.contractData.ContractDataParam;
import ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.AdditionalDataTab.block.holidayData.HolidayDataBlock;
import ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.AdditionalDataTab.block.holidayData.HolidayDataParam;
import ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.AdditionalDataTab.block.retrainingData.RetrainingDataBlock;
import ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.AdditionalDataTab.block.retrainingData.RetrainingDataParam;
import ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.AdditionalDataTab.block.serviceLengthData.ServiceLengthDataBlock;
import ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.AdditionalDataTab.block.serviceLengthData.ServiceLengthDataParam;
import ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.AdditionalDataTab.block.sickData.SickDataBlock;
import ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.AdditionalDataTab.block.sickData.SickDataParam;
import ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.AdditionalDataTab.block.trainingData.TrainingDataBlock;
import ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.AdditionalDataTab.block.trainingData.TrainingDataParam;

/**
 * @author Vasily Zhukov
 * @since 05.08.2011
 */
public class EmpReportPersonAdditionalDataTabUI extends UIPresenter implements IReportDQLModifierOwner
{
    private SickDataParam _sickData = new SickDataParam();
    private ContractDataParam _contractData = new ContractDataParam();
    private HolidayDataParam _holidayData = new HolidayDataParam();
    private AttestationDataParam _attestationData = new AttestationDataParam();
    private TrainingDataParam _trainingData = new TrainingDataParam();
    private RetrainingDataParam _retrainingData = new RetrainingDataParam();
    private ServiceLengthDataParam _serviceLengthData = new ServiceLengthDataParam();

    @Override
    public void onComponentRefresh()
    {
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        SickDataBlock.onBeforeDataSourceFetch(dataSource, _sickData);
        ContractDataBlock.onBeforeDataSourceFetch(dataSource, _contractData);
        HolidayDataBlock.onBeforeDataSourceFetch(dataSource, _holidayData);
        AttestationDataBlock.onBeforeDataSourceFetch(dataSource, _attestationData);
        TrainingDataBlock.onBeforeDataSourceFetch(dataSource, _trainingData);
        RetrainingDataBlock.onBeforeDataSourceFetch(dataSource, _retrainingData);
        ServiceLengthDataBlock.onBeforeDataSourceFetch(dataSource, _serviceLengthData);
    }

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {

    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (printInfo.isExists(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET))
        {
            // фильтры модифицируют запросы
            _sickData.modify(dql, printInfo);
            _contractData.modify(dql, printInfo);
            _holidayData.modify(dql, printInfo);
            _attestationData.modify(dql, printInfo);
            _trainingData.modify(dql, printInfo);
            _retrainingData.modify(dql, printInfo);
            _serviceLengthData.modify(dql, printInfo);

            // печатные блоки модифицируют запросы и создают печатные колонки
        }
    }

    public void onChangeYearsNumber()
    {
        _serviceLengthData.changeYearsNumber();
    }

    // Getters

    public SickDataParam getSickData()
    {
        return _sickData;
    }

    public ContractDataParam getContractData()
    {
        return _contractData;
    }

    public HolidayDataParam getHolidayData()
    {
        return _holidayData;
    }

    public AttestationDataParam getAttestationData()
    {
        return _attestationData;
    }

    public TrainingDataParam getTrainingData()
    {
        return _trainingData;
    }

    public RetrainingDataParam getRetrainingData()
    {
        return _retrainingData;
    }

    public ServiceLengthDataParam getServiceLengthData()
    {
        return _serviceLengthData;
    }
}
