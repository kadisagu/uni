package ru.tandemservice.uniemp.entity.employee;

import org.tandemframework.core.view.formatter.DateFormatter;

import ru.tandemservice.uniemp.entity.catalog.HolidayType;
import ru.tandemservice.uniemp.entity.employee.gen.EmployeeHolidayGen;

public class EmployeeHoliday extends EmployeeHolidayGen
{
    public static final String[] KEY_HOLIDAY_TYPE = new String[] { EmployeeHoliday.L_HOLIDAY_TYPE, HolidayType.P_TITLE };

    public static final String P_ORDER_NUMBER_DATE = "orderNumberDate";

    public String getOrderNumberDate()
    {
        StringBuilder resultBuilder = new StringBuilder();
        if (getHolidayExtractDate() != null && getHolidayExtractNumber() != null)
        {
            resultBuilder.append("Приказ №");
            resultBuilder.append(getHolidayExtractNumber());
            resultBuilder.append(" от ");
            resultBuilder.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getHolidayExtractDate()));
        }
        return resultBuilder.toString();
    }
    
    public static final String P_START_DATE_STR = "startDateStr";
    
    public String getStartDateStr()
    {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(getStartDate());
    }

    public String getFormattedNameBeginEndPeriodDateString()
    {
        String period;
        if (getBeginDate() != null && getEndDate() != null)
            period = " за период с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getBeginDate()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getEndDate());
        else
            period = "";

        return getTitle() + " с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getStartDate()) + " по " +
                DateFormatter.DEFAULT_DATE_FORMATTER.format(getFinishDate()) + period;

    }
}