/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.OrgUnitPostListTab;

import org.hibernate.criterion.Restrictions;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.Post;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation;

import java.util.List;

/**
 * @author dseleznev
 *         Created on: 16.09.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("rel");

    static
    {
        _orderSettings.setOrders(OrgUnitPostRelation.P_SIMPLE_TITLE, new OrderDescription("p", Post.P_TITLE));
    }

    @Override
    public void prepare(Model model)
    {
        if (null != model.getOrgUnitId())
        {
            model.setOrgUnit((OrgUnit) get(model.getOrgUnitId()));
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<OrgUnitPostRelation> getOrgUnitPostRelationsList(OrgUnit orgUnit)
    {
        return getSession().createCriteria(OrgUnitPostRelation.class).add(Restrictions.eq(OrgUnitPostRelation.L_ORG_UNIT, orgUnit)).list();
    }


    @Override
    public void prepareListDataSource(Model model)
    {
        preparePostListDataSource(model);
    }

    @Override
    public void preparePostListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(OrgUnitPostRelation.ENTITY_CLASS, "rel");
        builder.addJoin("rel", OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_POST, "p");
        builder.add(MQExpression.eq("rel", OrgUnitPostRelation.L_ORG_UNIT, model.getOrgUnit()));
        _orderSettings.applyOrder(builder, model.getOrgUnitPostDataSource().getEntityOrder());

        UniBaseUtils.createPage(model.getOrgUnitPostDataSource(), builder, getSession());
    }

}