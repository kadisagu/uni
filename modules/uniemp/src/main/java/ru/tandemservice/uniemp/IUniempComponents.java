/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp;

import ru.tandemservice.uniemp.base.bo.EmpEmployeeCertificationItem.ui.AddEdit.EmpEmployeeCertificationItemAddEdit;
import ru.tandemservice.uniemp.base.bo.EmpEmployeeRetrainingItem.ui.AddEdit.EmpEmployeeRetrainingItemAddEdit;
import ru.tandemservice.uniemp.base.bo.EmpEmployeeTrainingItem.ui.AddEdit.EmpEmployeeTrainingItemAddEdit;

/**
 * @author E. Grigoriev
 * @since 04.07.2008
 */
public interface IUniempComponents
{
    String EMPLOYEE_ADD_MEDICAL_SPEC = "ru.tandemservice.uniemp.component.employee.science.EmployeeMedicalSpecAddEdit";
    String EMPLOYEE_ADD_SICK_LIST = "ru.tandemservice.uniemp.component.employee.EmployeeSickListAddEdit";
    String EMPLOYEE_ADD_EMPLOYEE_HOLIDAY = "ru.tandemservice.uniemp.component.employee.EmployeeHolidayAddEdit";
    String EMPLOYEE_ADD_EMPLOYEE_PAYMENT = "ru.tandemservice.uniemp.component.employee.EmployeePaymentAddEdit";
    String EMPLOYEE_ADD_EMPLOYEE_ENCOURAGEMENT = "ru.tandemservice.uniemp.component.employee.EmployeeEncouragementAddEdit";
    String EMPLOYEE_ADD_EMPLOYMENT_HISTORY_ITEM_INT = "ru.tandemservice.uniemp.component.employee.EmploymentHistoryIntAddEdit";
    String EMPLOYEE_ADD_EMPLOYMENT_HISTORY_ITEM_EXT = "ru.tandemservice.uniemp.component.employee.EmploymentHistoryExtAddEdit";
    String EMPLOYEE_ADD_EMPLOYMENT_HISTORY_ITEM_ARCH = "ru.tandemservice.uniemp.component.employee.EmploymentHistoryArchAddEdit";
    String EMPLOYEE_CONTRACT_ADDEDIT = "ru.tandemservice.uniemp.component.employee.ContractAddEdit";
    String EMPLOYEE_CONTRACT_FILE_ADD = "ru.tandemservice.uniemp.component.employee.ContractFileAdd";
    String EMPLOYEE_AGREEMENT_TO_CONTRACT_ADDEDIT = "ru.tandemservice.uniemp.component.employee.AdditionalAgreementAddEdit";
    String EMPLOYEE_AGREEMENT_TO_CONTRACT_FILE_ADD = "ru.tandemservice.uniemp.component.employee.AdditionalAgreementFileAdd";
    String EMPLOYEE_ADDITIONAL_DATA_EDIT = "ru.tandemservice.uniemp.component.employee.AdditionalDataEdit";
    String ORG_UNIT_POST_ADDEDIT = "ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.orgUnitPost.OrgUnitPostAddEdit";
    String ORG_UNIT_STAFF_LIST_ITEM_ADDEDIT = "ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffListItem.StaffListItemAddEdit";
    String ORG_UNIT_STAFF_LIST_ADDEDIT = "ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffList.StaffListAddEdit";
    String ORG_UNIT_STAFF_LIST_CHANGE_STATE = "ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffList.StaffListChangeState";
    String ORG_UNIT_STAFF_LIST_ALLOC_ITEM_ADDEDIT = "ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffListAllocItem.StaffListAllocItemAddEdit";
    String ORG_UNIT_STAFF_LIST_PAYMENT_ITEM_ADDEDIT = "ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffListAllocItem.StaffListPaymentItemAddEdit";
    String ORG_UNIT_STAFF_LIST_FAKE_PAYMENT_ITEM_ADDEDIT = "ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffListAllocItem.StaffListFakePaymentItemAddEdit";
    String ORG_UNIT_STAFF_LIST_POST_PAYMENT_ADDEDIT = "ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffListItem.StaffListPostPaymentAddEdit";
    String ORG_UNIT_STAFF_LIST_FAKE_PAYMENT_ADDEDIT = "ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffListItem.StaffListFakePaymentAddEdit";
    String EMPLOYEE_POST_ADD_EDIT = "ru.tandemservice.uniemp.component.employee.EmployeePostAddEdit";

    String PAYMENT_BASE_VALUES_PUB = "ru.tandemservice.uniemp.component.settings.PaymentBaseValuesPub";
    String PAYMENT_BASE_VALUE_ADD_EDIT = "ru.tandemservice.uniemp.component.settings.PaymentBaseValueAddEdit";

    String VPO1_SETTING_EDIT = "ru.tandemservice.uniemp.component.settings.EmployeeVPO1SettingsEdit";

    String PAYMENT_ON_FOT_TO_PAYMENTS_SETTINGS_EDIT = "ru.tandemservice.uniemp.component.settings.PaymentOnFOTToPaymentsSettings.PaymentOnFOTToPaymentsSettingsEdit";

    String REPORT_STAFF_LIST_ADD = "ru.tandemservice.uniemp.component.reports.staffList.Add";
    String REPORT_STAFF_LIST_ALLOCATION_ADD = "ru.tandemservice.uniemp.component.reports.staffListAllocation.Add";
    String REPORT_EMPLOYEE_VPO1_ADD = "ru.tandemservice.uniemp.component.reports.employeeVPO.Add";

    String EMPLOYEE_SERVICE_LENGTH_ADD_EDIT = "ru.tandemservice.uniemp.component.employee.EmployeeServiceLengthAddEdit";
    String EMPLOYEE_SERVICE_LENGTH_COUNTER = "ru.tandemservice.uniemp.component.employee.EmployeeServiceLengthCounter";

    String VACATION_SCHEDULE_ADD_EDIT = "ru.tandemservice.uniemp.component.vacationShedule.VacationScheduleAddEdit";
    String VACATION_SCHEDULE_ITEMS_ADD = "ru.tandemservice.uniemp.component.vacationShedule.VacationScheduleItemsAdd";
    String VACATION_SCHEDULE_ITEM_SPLIT = "ru.tandemservice.uniemp.component.vacationShedule.VacationScheduleItemSplit";
    String VACATION_SCHEDULE_ITEMS_EDIT = "ru.tandemservice.uniemp.component.vacationShedule.MultipleVacationScheduleItemsEdit";
    String VACATION_SCHEDULE_ITEM_ADD_EDIT = "ru.tandemservice.uniemp.component.vacationShedule.VacationScheduleItemAddEdit";
    String VACATION_SCHEDULE_ITEM_EDIT = "ru.tandemservice.uniemp.component.vacationShedule.VacationScheduleItemEdit";
    String VACATION_SCHEDULE_PRINT = "ru.tandemservice.uniemp.component.vacationShedule.VacationSchedulePrint";

    String VACATION_CERTIFICATE_PRINT_FORM = "ru.tandemservice.uniemp.component.employee.VacationCertificatePrintForm";
    String VACATION_CERTIFICATE_PRINT = "ru.tandemservice.uniemp.component.employee.VacationCertificatePrint";

    String STAFF_LIST_REGISTRY_CHANGE_STATE = "ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.StaffListRegistryChangeState";

    String EMPLOYMENT_CONTRACTS_TERMINATION_NOTICE_PRINT_FORM = "ru.tandemservice.uniemp.component.employee.employmentContracts.EmploymentContractsTerminationNoticePrintForm";
    String EMPLOYMENT_CONTRACTS_TERMINATION_NOTICE_PRINT = "ru.tandemservice.uniemp.component.employee.employmentContracts.EmploymentContractsTerminationNoticePrint";

    String EMPLOYEE_ACTING_ITEM_ADD = "ru.tandemservice.uniemp.component.employee.EmployeeActingItemAddEdit";

    String EMPLOYEE_COMBINATION_POST_ADD_EDIT = "ru.tandemservice.uniemp.component.employee.EmployeeCombinationPostAddEdit";

    @Deprecated
    String EMPLOYEE_CERTIFICATION_ITEM_ADD_EDIT = EmpEmployeeCertificationItemAddEdit.class.getSimpleName();
    @Deprecated
    String EMPLOYEE_TRAINING_ITEM_ADD_EDIT = EmpEmployeeTrainingItemAddEdit.class.getSimpleName();
    @Deprecated
    String EMPLOYEE_RETRAINING_ITEM_ADD_EDIT = EmpEmployeeRetrainingItemAddEdit.class.getSimpleName();
}