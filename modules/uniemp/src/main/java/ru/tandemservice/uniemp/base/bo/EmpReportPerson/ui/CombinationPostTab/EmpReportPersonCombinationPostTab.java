/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.CombinationPostTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.block.BlockListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.CombinationPostTab.block.combinationPostData.CombinationPostDataBlock;

/**
 * @author Vasily Zhukov
 * @since 12.04.2012
 */
@Configuration
public class EmpReportPersonCombinationPostTab extends BusinessComponentManager
{
    // tab block lists
    public static final String EMPLOYEE_COMBINATION_POST_BLOCK_LIST = "employeeCombinationPostBlockList";

    // block names
    public static final String COMBINATION_POST_DATA = "combinationPostData";

    @Bean
    public BlockListExtPoint employeeCombinationPostBlockListExtPoint()
    {
        return blockListExtPointBuilder(EMPLOYEE_COMBINATION_POST_BLOCK_LIST)
                .addBlock(htmlBlock(COMBINATION_POST_DATA, "block/combinationPostData/CombinationPostData"))
                .create();
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(CommonManager.instance().yesNoDSConfig())
                .addDataSource(selectDS(CombinationPostDataBlock.EMPLOYEE_TYPE_DS, employeeTypeDSHandler()))
                .addDataSource(selectDS(CombinationPostDataBlock.POST_DS, postDSHandler()))
                .addDataSource(selectDS(CombinationPostDataBlock.ORG_UNIT_DS, orgUnitDSHandler()))
                .addDataSource(selectDS(CombinationPostDataBlock.QUALIFICATION_LEVEL_DS, qualificationLevelDSHandler()))
                .addDataSource(selectDS(CombinationPostDataBlock.PROF_QUALIFICATION_GROUP_DS, profQualificationGroupDSHandler()))
                .addDataSource(selectDS(CombinationPostDataBlock.COMBINATION_POST_TYPE_DS, combinationPostTypeDSHandler()))
                .addDataSource(selectDS(CombinationPostDataBlock.COMBINATION_POST_STATUS_DS, combinationPostStatusDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler employeeTypeDSHandler()
    {
        return CombinationPostDataBlock.createEmployeeTypeDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler postDSHandler()
    {
        return CombinationPostDataBlock.createPostDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler orgUnitDSHandler()
    {
        return CombinationPostDataBlock.createOrgUnitDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler qualificationLevelDSHandler()
    {
        return CombinationPostDataBlock.createQualificationLevelDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler profQualificationGroupDSHandler()
    {
        return CombinationPostDataBlock.createProfQualificationGroupDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler combinationPostTypeDSHandler()
    {
        return CombinationPostDataBlock.createCombinationPostTypeDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler combinationPostStatusDSHandler()
    {
        return CombinationPostDataBlock.createCombinationPostStatusDS(getName());
    }
}
