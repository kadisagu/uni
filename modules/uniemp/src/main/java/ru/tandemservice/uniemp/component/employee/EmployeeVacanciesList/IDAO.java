/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.EmployeeVacanciesList;

import ru.tandemservice.uniemp.dao.IUniempDAO;

/**
 * @author dseleznev
 * Created on: 29.10.2008
 */
public interface IDAO extends IUniempDAO<Model>
{
}