package ru.tandemservice.uniemp.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Типы совмещения должностей"
 * Имя сущности : combinationPostType
 * Файл data.xml : uniemp.catalogs.data.xml
 */
public interface CombinationPostTypeCodes
{
    /** Константа кода (code) элемента : Совмещение (title) */
    String COMBINATION = "1";
    /** Константа кода (code) элемента : Увеличение объема работы (title) */
    String INC_WORK = "2";

    Set<String> CODES = ImmutableSet.of(COMBINATION, INC_WORK);
}
