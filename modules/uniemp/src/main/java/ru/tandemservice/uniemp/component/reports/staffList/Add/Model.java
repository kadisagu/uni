/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.staffList.Add;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.ProfQualificationGroup;
import org.tandemframework.shared.employeebase.catalog.entity.QualificationLevel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;
import ru.tandemservice.uniemp.entity.employee.StaffListItem;
import ru.tandemservice.uniemp.entity.employee.StaffListPaymentBase;
import ru.tandemservice.uniemp.entity.report.StaffListRepPaymentSettings;
import ru.tandemservice.uniemp.entity.report.StaffListReport;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author dseleznev
 * Created on: 05.08.2009
 */
public class Model
{
    private StaffListReport _report = new StaffListReport();

    // Модель, берущая на себя часть функций по работе с шаблонами фильтров
    private FiltersPresetModel _filtersPresetModel;

    // Форматтер с фиксированным числом знаков после запятой
    private DoubleFormatter _dblFormatter = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED;


    // --- ФИЛЬТРЫ ОТЧЕТА -- //

    private Date _reportDate;                              // Дата отчета, которая пойдёт в печатную форму
    private Date _formingDate;                             // Дата формирования отчета, неизменяемая
    private IdentifiableWrapper _activityType;             // Вид деятельности (Б / ВБС)
    private List<EmployeeType> _employeeTypeList;          // Набор типов должностей (ППС, РС и тд)
    private List<OrgUnit> _orgUnitList;                              // Подразделение
    private List<OrgUnit> _orgUnitExcludeList;             //Список исключенных подразделений
    private Boolean _showOrgUnitWithoutStaffList = false;  //Метка о необходимости вывода в отчете подразделений без активного ШР
    private QualificationLevel _qualificationLevel;        // Квалификационный уровень
    private ProfQualificationGroup _profQualificationGroup;// Профессионально-квалификационная группа
    private List<PostBoundedWithQGandQL> _postList;                  // Должность
    //private boolean _mergeIdenticalPosts;

    private List<IdentifiableWrapper> _activityTypesList;  // Список видов деятельности
    private IMultiSelectModel _employeeTypeListModel;      // Список типов должностей

    private IMultiSelectModel _orgUnitExcludeListModel;    //Список подразделений возможных для исключения подразделений
    private IMultiSelectModel _orgUnitListModel;                     // Список подразделений
    private List<QualificationLevel> _qualificationLevelsList; // Список квалификационных уровней
    private List<ProfQualificationGroup> _profQualificationGroupsList; // Список профессионально-квалификационных групп
    private IMultiSelectModel _employeePostListModel;                // Список должностей

    private List<OrgUnitTypeWrapper> _orgUnitTypesList;    // Список типов подразделений для SearchList'а
    private List<IEntity> _selectedOrgUnitTypesList;       // Список выбранных типов подразделений
    private List<IEntity> _selectedShowChildOrgUnitsList;  // Список типов подразделений, для которых будут отображаться дочерние подразделения
    private List<IEntity> _selectedShowChildOrgUnitsAsOwnList; // Список типов подразделений, для которых должности дочерних подразделений будут считаться их собственными

    private List<StaffListRepPaymentSettings> _paymentsList; // Список типов выплат для отчета

    private DynamicListDataSource<OrgUnitTypeWrapper> _orgUnitTypesDataSource;


    // -- ОСНОВНЫЕ ДАННЫЕ ДЛЯ ПЕЧАТНОЙ ФОРМЫ -- //

    // "Сырые" данные из базы данных, используемые для построения данных, подлежащих выводу в отчете
    private Map<Long, OrgUnit> _orgUnitsMap;
    private List<OrgUnit> _accountableOrgUnitsList;
    private List<StaffListItem> _rawStaffListItemsList;
    private List<StaffListAllocationItem> _rawStaffListAllocationItemsList;
    private List<StaffListPaymentBase> _rawStaffListPaymentsList;

    // --- Данные, подготовленные для вывода в печатной форме отчета --- //

    // --- Суммарные данные по ОУ --- //
    private Double _vuzBudgetStaffRate; // Суммарная ставка бюджета по всему ОУ
    private Double _vuzOffBudgetStaffRate; // Суммарная ставка внебюджета по всему ОУ
    private CoreCollectionUtils.Pair<Double, Double> _vuzSumSalary; // Суммарный месячный фонд оплаты труда по тарифу для ОУ (бюджет)
    private Map<Long, CoreCollectionUtils.Pair<Double, Double>> _vuzSumPaymentsMap; // Суммарные надбавки по ОУ
    private CoreCollectionUtils.Pair<Double, Double> _vuzSumTotalSalary; // Суммарный фонд оплаты труда с надбавками для ОУ

    // --- Данные по подразделениям --- //
    private Map<Long, String[]> _postDataMap; // Название, приказ и КУ
    private Map<Long, Double> _postSalarysMap; // Должностные оклады для должностей, отнесенных к ПКГ и КУ

    // Вспомогательный мап с выплатами по должностям штатного расписания, начисляемыми на базовый оклад
    Map<Long, Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>>> _paymentsRubleValuesMap = new HashMap<>();
    // Вспомогательный мап с выплатами по должностям штатного расписания, начисляемыми на месячный ФОТ
    Map<Long, Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>>> _paymentsFullRubleValuesMap = new HashMap<>();

    // Мап вложенности должностей (отсортированных по названию) в подразделениях
    private Map<Long, List<StaffListItem>> _orgUnitPostListsMap;

    // Мапы со ставками по должностям на подразделениях
    private Map<Long, Map<Long, Double>> _orgUnitPostBudgetStaffRatesMap;
    private Map<Long, Map<Long, Double>> _orgUnitPostOffBudgetStaffRatesMap;

    // Мапы с данными по месячному фонду оплаты труда по тарифу по каждой должности,
    // суммарно по подразделению и суммарно по подразделению, включая дочерние подразделения
    private Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> _orgUnitPostSalarysMap;
    private Map<Long, CoreCollectionUtils.Pair<Double, Double>> _orgUnitWithChildsSalarysMap;
    private Map<Long, CoreCollectionUtils.Pair<Double, Double>> _orgUnitSalarysMap;

    // Мапы с данными о выплатах по каждой должности,
    // суммарно по подразделению и суммарно по подразделению, включая дочерние подразделения
    private Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> _orgUnitSumPaymentsMap;
    private Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> _orgUnitWithChildsSumPaymentsMap;
    private Map<Long, Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>>> _orgUnitPostPaymentsMap;

    // Мапы с данными по месячному фонду оплаты труда с надбавками по каждой должности,
    // суммарно по подразделению и суммарно по подразделению, включая дочерние подразделения
    private Map<Long, CoreCollectionUtils.Pair<Double, Double>> _orgUnitSumTotalSalaryMap;
    private Map<Long, CoreCollectionUtils.Pair<Double, Double>> _orgUnitWithChildsSumTotalSalaryMap;
    private Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> _orgUnitPostTotalSalaryMap;


    // -- ВСПОМОГАТЕЛЬНЫЕ ДАННЫЕ ДЛЯ ПЕЧАТНОЙ ФОРМЫ -- //

    private int _columnsNumber; // Количество колонок в таблице для нумерации
    private int[] _paymentColumnWidth; // Массив пропорциональных ширин для колонок выплат
    private List<Payment> _paymentHeadersList; // Список заголовков выплат, которые попадут в отчет

    private List<Long> _otherPaymentIds; // Список ID выплат, которые должны быть отнесены к колонке "Другие выплаты"

    private List<OrgUnit> _serializedOrgstructure; // Сериализованная организационная структура
    private Map<OrgUnit, OrgUnit> _orgUnitMappingsMap; // Мап соответствий подразделений подразделениям, к которым нужно причислять данные
    private Map<Long, OrgUnitTypeWrapper> _orgUnitTypesMap; // Мап, в котором хранится родитель для каждого подразделения


    // -- ГОТОВЫЕ К ВЫВОДУ ДАННЫЕ -- //

    private String _rector; // И.О. Фамилия ректора ОУ

    private String[] _paymentHeaders; // Заголовки выплат для печатной формы
    private String[] _numberHeaders; // Заголовки для строки с нумерацией столбцов

    private String[][] _tableData; // Основные данные отчета
    private String[][] _summaryData; // Суммарные данные по ОУ

    private RtfTableRowStyle[] _rowStyles; // Стили строк таблицы


    // -- ГЕТТЕРЫ И СЕТТЕРЫ -- //

    public Boolean getShowOrgUnitWithoutStaffList()
    {
        return _showOrgUnitWithoutStaffList;
    }

    public void setShowOrgUnitWithoutStaffList(Boolean showOrgUnitWithoutStaffList)
    {
        _showOrgUnitWithoutStaffList = showOrgUnitWithoutStaffList;
    }

    public List<OrgUnit> getOrgUnitExcludeList()
    {
        return _orgUnitExcludeList;
    }

    public void setOrgUnitExcludeList(List<OrgUnit> orgUnitExcludeList)
    {
        _orgUnitExcludeList = orgUnitExcludeList;
    }

    public IMultiSelectModel getOrgUnitExcludeListModel()
    {
        return _orgUnitExcludeListModel;
    }

    public void setOrgUnitExcludeListModel(IMultiSelectModel orgUnitExcludeListModel)
    {
        _orgUnitExcludeListModel = orgUnitExcludeListModel;
    }

    public StaffListReport getReport()
    {
        return _report;
    }

    public void setReport(StaffListReport report)
    {
        this._report = report;
    }

    public FiltersPresetModel getFiltersPresetModel()
    {
        return _filtersPresetModel;
    }

    public void setFiltersPresetModel(FiltersPresetModel filtersPresetModel)
    {
        this._filtersPresetModel = filtersPresetModel;
    }

    public DoubleFormatter getDblFormatter()
    {
        return _dblFormatter;
    }

    public void setDblFormatter(DoubleFormatter dblFormatter)
    {
        this._dblFormatter = dblFormatter;
    }

    public Date getReportDate()
    {
        return _reportDate;
    }

    public void setReportDate(Date reportDate)
    {
        this._reportDate = reportDate;
    }

    public Date getFormingDate()
    {
        return _formingDate;
    }

    public void setFormingDate(Date formingDate)
    {
        this._formingDate = formingDate;
    }

    public IdentifiableWrapper getActivityType()
    {
        return _activityType;
    }

    public void setActivityType(IdentifiableWrapper activityType)
    {
        this._activityType = activityType;
    }

    public List<EmployeeType> getEmployeeTypeList()
    {
        return _employeeTypeList;
    }

    public void setEmployeeTypeList(List<EmployeeType> employeeTypeList)
    {
        this._employeeTypeList = employeeTypeList;
    }

    public QualificationLevel getQualificationLevel()
    {
        return _qualificationLevel;
    }

    public void setQualificationLevel(QualificationLevel qualificationLevel)
    {
        this._qualificationLevel = qualificationLevel;
    }

    public ProfQualificationGroup getProfQualificationGroup()
    {
        return _profQualificationGroup;
    }

    public void setProfQualificationGroup(ProfQualificationGroup profQualificationGroup)
    {
        this._profQualificationGroup = profQualificationGroup;
    }

    public List<OrgUnit> getOrgUnitList()
    {
        return _orgUnitList;
    }

    public void setOrgUnitList(List<OrgUnit> orgUnitList)
    {
        _orgUnitList = orgUnitList;
    }

    public List<PostBoundedWithQGandQL> getPostList()
    {
        return _postList;
    }

    public void setPostList(List<PostBoundedWithQGandQL> postList)
    {
        _postList = postList;
    }

    public List<IdentifiableWrapper> getActivityTypesList()
    {
        return _activityTypesList;
    }

    public void setActivityTypesList(List<IdentifiableWrapper> activityTypesList)
    {
        this._activityTypesList = activityTypesList;
    }

    public IMultiSelectModel getEmployeeTypeListModel()
    {
        return _employeeTypeListModel;
    }

    public void setEmployeeTypeListModel(IMultiSelectModel employeeTypeListModel)
    {
        this._employeeTypeListModel = employeeTypeListModel;
    }

    public IMultiSelectModel getOrgUnitListModel()
    {
        return _orgUnitListModel;
    }

    public void setOrgUnitListModel(IMultiSelectModel orgUnitListModel)
    {
        _orgUnitListModel = orgUnitListModel;
    }

    public IMultiSelectModel getEmployeePostListModel()
    {
        return _employeePostListModel;
    }

    public void setEmployeePostListModel(IMultiSelectModel employeePostListModel)
    {
        _employeePostListModel = employeePostListModel;
    }

    public List<QualificationLevel> getQualificationLevelsList()
    {
        return _qualificationLevelsList;
    }

    public void setQualificationLevelsList(List<QualificationLevel> qualificationLevelsList)
    {
        this._qualificationLevelsList = qualificationLevelsList;
    }

    public List<ProfQualificationGroup> getProfQualificationGroupsList()
    {
        return _profQualificationGroupsList;
    }

    public void setProfQualificationGroupsList(List<ProfQualificationGroup> profQualificationGroupsList)
    {
        this._profQualificationGroupsList = profQualificationGroupsList;
    }

    public List<OrgUnitTypeWrapper> getOrgUnitTypesList()
    {
        return _orgUnitTypesList;
    }

    public void setOrgUnitTypesList(List<OrgUnitTypeWrapper> orgUnitTypesList)
    {
        this._orgUnitTypesList = orgUnitTypesList;
    }

    public List<IEntity> getSelectedOrgUnitTypesList()
    {
        return _selectedOrgUnitTypesList;
    }

    public void setSelectedOrgUnitTypesList(List<IEntity> selectedOrgUnitTypesList)
    {
        this._selectedOrgUnitTypesList = selectedOrgUnitTypesList;
    }

    public List<IEntity> getSelectedShowChildOrgUnitsList()
    {
        return _selectedShowChildOrgUnitsList;
    }

    public void setSelectedShowChildOrgUnitsList(List<IEntity> selectedShowChildOrgUnitsList)
    {
        this._selectedShowChildOrgUnitsList = selectedShowChildOrgUnitsList;
    }

    public List<IEntity> getSelectedShowChildOrgUnitsAsOwnList()
    {
        return _selectedShowChildOrgUnitsAsOwnList;
    }

    public void setSelectedShowChildOrgUnitsAsOwnList(List<IEntity> selectedShowChildOrgUnitsAsOwnList)
    {
        this._selectedShowChildOrgUnitsAsOwnList = selectedShowChildOrgUnitsAsOwnList;
    }

    public List<StaffListRepPaymentSettings> getPaymentsList()
    {
        return _paymentsList;
    }

    public void setPaymentsList(List<StaffListRepPaymentSettings> paymentsList)
    {
        this._paymentsList = paymentsList;
    }

    public DynamicListDataSource<OrgUnitTypeWrapper> getOrgUnitTypesDataSource()
    {
        return _orgUnitTypesDataSource;
    }

    public void setOrgUnitTypesDataSource(DynamicListDataSource<OrgUnitTypeWrapper> orgUnitTypesDataSource)
    {
        this._orgUnitTypesDataSource = orgUnitTypesDataSource;
    }

    public Map<Long, OrgUnit> getOrgUnitsMap()
    {
        return _orgUnitsMap;
    }

    public void setOrgUnitsMap(Map<Long, OrgUnit> orgUnitsMap)
    {
        this._orgUnitsMap = orgUnitsMap;
    }

    public List<OrgUnit> getAccountableOrgUnitsList()
    {
        return _accountableOrgUnitsList;
    }

    public void setAccountableOrgUnitsList(List<OrgUnit> accountableOrgUnitsList)
    {
        this._accountableOrgUnitsList = accountableOrgUnitsList;
    }

    public List<StaffListItem> getRawStaffListItemsList()
    {
        return _rawStaffListItemsList;
    }

    public void setRawStaffListItemsList(List<StaffListItem> rawStaffListItemsList)
    {
        this._rawStaffListItemsList = rawStaffListItemsList;
    }

    public List<StaffListAllocationItem> getRawStaffListAllocationItemsList()
    {
        return _rawStaffListAllocationItemsList;
    }

    public void setRawStaffListAllocationItemsList(List<StaffListAllocationItem> rawStaffListAllocationItemsList)
    {
        this._rawStaffListAllocationItemsList = rawStaffListAllocationItemsList;
    }

    public List<StaffListPaymentBase> getRawStaffListPaymentsList()
    {
        return _rawStaffListPaymentsList;
    }

    public void setRawStaffListPaymentsList(List<StaffListPaymentBase> rawStaffListPaymentsList)
    {
        this._rawStaffListPaymentsList = rawStaffListPaymentsList;
    }

    public Double getVuzBudgetStaffRate()
    {
        return _vuzBudgetStaffRate;
    }

    public void setVuzBudgetStaffRate(Double vuzBudgetStaffRate)
    {
        this._vuzBudgetStaffRate = vuzBudgetStaffRate;
    }

    public Double getVuzOffBudgetStaffRate()
    {
        return _vuzOffBudgetStaffRate;
    }

    public void setVuzOffBudgetStaffRate(Double vuzOffBudgetStaffRate)
    {
        this._vuzOffBudgetStaffRate = vuzOffBudgetStaffRate;
    }

    public CoreCollectionUtils.Pair<Double, Double> getVuzSumSalary()
    {
        return _vuzSumSalary;
    }

    public void setVuzSumSalary(CoreCollectionUtils.Pair<Double, Double> vuzSumSalary)
    {
        this._vuzSumSalary = vuzSumSalary;
    }

    public Map<Long, CoreCollectionUtils.Pair<Double, Double>> getVuzSumPaymentsMap()
    {
        return _vuzSumPaymentsMap;
    }

    public void setVuzSumPaymentsMap(Map<Long, CoreCollectionUtils.Pair<Double, Double>> vuzSumPaymentsMap)
    {
        this._vuzSumPaymentsMap = vuzSumPaymentsMap;
    }

    public CoreCollectionUtils.Pair<Double, Double> getVuzSumTotalSalary()
    {
        return _vuzSumTotalSalary;
    }

    public void setVuzSumTotalSalary(CoreCollectionUtils.Pair<Double, Double> vuzSumTotalSalary)
    {
        this._vuzSumTotalSalary = vuzSumTotalSalary;
    }

    public Map<Long, String[]> getPostDataMap()
    {
        return _postDataMap;
    }

    public void setPostDataMap(Map<Long, String[]> postDataMap)
    {
        this._postDataMap = postDataMap;
    }

    public Map<Long, Double> getPostSalarysMap()
    {
        return _postSalarysMap;
    }

    public void setPostSalarysMap(Map<Long, Double> postSalarysMap)
    {
        this._postSalarysMap = postSalarysMap;
    }

    public Map<Long, Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>>> getPaymentsRubleValuesMap()
    {
        return _paymentsRubleValuesMap;
    }

    public void setPaymentsRubleValuesMap(Map<Long, Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>>> paymentsRubleValuesMap)
    {
        this._paymentsRubleValuesMap = paymentsRubleValuesMap;
    }

    public Map<Long, Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>>> getPaymentsFullRubleValuesMap()
    {
        return _paymentsFullRubleValuesMap;
    }

    public void setPaymentsFullRubleValuesMap(Map<Long, Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>>> paymentsFullRubleValuesMap)
    {
        this._paymentsFullRubleValuesMap = paymentsFullRubleValuesMap;
    }

    public Map<Long, List<StaffListItem>> getOrgUnitPostListsMap()
    {
        return _orgUnitPostListsMap;
    }

    public void setOrgUnitPostListsMap(Map<Long, List<StaffListItem>> orgUnitPostListsMap)
    {
        this._orgUnitPostListsMap = orgUnitPostListsMap;
    }

    public Map<Long, Map<Long, Double>> getOrgUnitPostBudgetStaffRatesMap()
    {
        return _orgUnitPostBudgetStaffRatesMap;
    }

    public void setOrgUnitPostBudgetStaffRatesMap(Map<Long, Map<Long, Double>> orgUnitPostBudgetStaffRatesMap)
    {
        this._orgUnitPostBudgetStaffRatesMap = orgUnitPostBudgetStaffRatesMap;
    }

    public Map<Long, Map<Long, Double>> getOrgUnitPostOffBudgetStaffRatesMap()
    {
        return _orgUnitPostOffBudgetStaffRatesMap;
    }

    public void setOrgUnitPostOffBudgetStaffRatesMap(Map<Long, Map<Long, Double>> orgUnitPostOffBudgetStaffRatesMap)
    {
        this._orgUnitPostOffBudgetStaffRatesMap = orgUnitPostOffBudgetStaffRatesMap;
    }

    public Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> getOrgUnitPostSalarysMap()
    {
        return _orgUnitPostSalarysMap;
    }

    public void setOrgUnitPostSalarysMap(Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> orgUnitPostSalarysMap)
    {
        this._orgUnitPostSalarysMap = orgUnitPostSalarysMap;
    }

    public Map<Long, CoreCollectionUtils.Pair<Double, Double>> getOrgUnitWithChildsSalarysMap()
    {
        return _orgUnitWithChildsSalarysMap;
    }

    public void setOrgUnitWithChildsSalarysMap(Map<Long, CoreCollectionUtils.Pair<Double, Double>> orgUnitWithChildsSalarysMap)
    {
        this._orgUnitWithChildsSalarysMap = orgUnitWithChildsSalarysMap;
    }

    public Map<Long, CoreCollectionUtils.Pair<Double, Double>> getOrgUnitSalarysMap()
    {
        return _orgUnitSalarysMap;
    }

    public void setOrgUnitSalarysMap(Map<Long, CoreCollectionUtils.Pair<Double, Double>> orgUnitSalarysMap)
    {
        this._orgUnitSalarysMap = orgUnitSalarysMap;
    }

    public Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> getOrgUnitSumPaymentsMap()
    {
        return _orgUnitSumPaymentsMap;
    }

    public void setOrgUnitSumPaymentsMap(Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> orgUnitSumPaymentsMap)
    {
        this._orgUnitSumPaymentsMap = orgUnitSumPaymentsMap;
    }

    public Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> getOrgUnitWithChildsSumPaymentsMap()
    {
        return _orgUnitWithChildsSumPaymentsMap;
    }

    public void setOrgUnitWithChildsSumPaymentsMap(Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> orgUnitWithChildsSumPaymentsMap)
    {
        this._orgUnitWithChildsSumPaymentsMap = orgUnitWithChildsSumPaymentsMap;
    }

    public Map<Long, Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>>> getOrgUnitPostPaymentsMap()
    {
        return _orgUnitPostPaymentsMap;
    }

    public void setOrgUnitPostPaymentsMap(Map<Long, Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>>> orgUnitPostPaymentsMap)
    {
        this._orgUnitPostPaymentsMap = orgUnitPostPaymentsMap;
    }

    public Map<Long, CoreCollectionUtils.Pair<Double, Double>> getOrgUnitSumTotalSalaryMap()
    {
        return _orgUnitSumTotalSalaryMap;
    }

    public void setOrgUnitSumTotalSalaryMap(Map<Long, CoreCollectionUtils.Pair<Double, Double>> orgUnitSumTotalSalaryMap)
    {
        this._orgUnitSumTotalSalaryMap = orgUnitSumTotalSalaryMap;
    }

    public Map<Long, CoreCollectionUtils.Pair<Double, Double>> getOrgUnitWithChildsSumTotalSalaryMap()
    {
        return _orgUnitWithChildsSumTotalSalaryMap;
    }

    public void setOrgUnitWithChildsSumTotalSalaryMap(Map<Long, CoreCollectionUtils.Pair<Double, Double>> orgUnitWithChildsSumTotalSalaryMap)
    {
        this._orgUnitWithChildsSumTotalSalaryMap = orgUnitWithChildsSumTotalSalaryMap;
    }

    public Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> getOrgUnitPostTotalSalaryMap()
    {
        return _orgUnitPostTotalSalaryMap;
    }

    public void setOrgUnitPostTotalSalaryMap(Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> orgUnitPostTotalSalaryMap)
    {
        this._orgUnitPostTotalSalaryMap = orgUnitPostTotalSalaryMap;
    }

    public int getColumnsNumber()
    {
        return _columnsNumber;
    }

    public void setColumnsNumber(int columnsNumber)
    {
        this._columnsNumber = columnsNumber;
    }

    public int[] getPaymentColumnWidth()
    {
        return _paymentColumnWidth;
    }

    public void setPaymentColumnWidth(int[] paymentColumnWidth)
    {
        this._paymentColumnWidth = paymentColumnWidth;
    }

    public List<Payment> getPaymentHeadersList()
    {
        return _paymentHeadersList;
    }

    public void setPaymentHeadersList(List<Payment> paymentHeadersList)
    {
        this._paymentHeadersList = paymentHeadersList;
    }

    public List<Long> getOtherPaymentIds()
    {
        return _otherPaymentIds;
    }

    public void setOtherPaymentIds(List<Long> otherPaymentIds)
    {
        this._otherPaymentIds = otherPaymentIds;
    }

    public List<OrgUnit> getSerializedOrgstructure()
    {
        return _serializedOrgstructure;
    }

    public void setSerializedOrgstructure(List<OrgUnit> serializedOrgstructure)
    {
        this._serializedOrgstructure = serializedOrgstructure;
    }

    public Map<OrgUnit, OrgUnit> getOrgUnitMappingsMap()
    {
        return _orgUnitMappingsMap;
    }

    public void setOrgUnitMappingsMap(Map<OrgUnit, OrgUnit> orgUnitMappingsMap)
    {
        this._orgUnitMappingsMap = orgUnitMappingsMap;
    }

    public Map<Long, OrgUnitTypeWrapper> getOrgUnitTypesMap()
    {
        return _orgUnitTypesMap;
    }

    public void setOrgUnitTypesMap(Map<Long, OrgUnitTypeWrapper> orgUnitTypesMap)
    {
        this._orgUnitTypesMap = orgUnitTypesMap;
    }

    public String getRector()
    {
        return _rector;
    }

    public void setRector(String rector)
    {
        this._rector = rector;
    }

    public String[] getPaymentHeaders()
    {
        return _paymentHeaders;
    }

    public void setPaymentHeaders(String[] paymentHeaders)
    {
        this._paymentHeaders = paymentHeaders;
    }

    public String[] getNumberHeaders()
    {
        return _numberHeaders;
    }

    public void setNumberHeaders(String[] numberHeaders)
    {
        this._numberHeaders = numberHeaders;
    }

    public String[][] getTableData()
    {
        return _tableData;
    }

    public void setTableData(String[][] tableData)
    {
        this._tableData = tableData;
    }

    public String[][] getSummaryData()
    {
        return _summaryData;
    }

    public void setSummaryData(String[][] summaryData)
    {
        this._summaryData = summaryData;
    }

    public RtfTableRowStyle[] getRowStyles()
    {
        return _rowStyles;
    }

    public void setRowStyles(RtfTableRowStyle[] rowStyles)
    {
        this._rowStyles = rowStyles;
    }
}