/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.qualityConsistency.Add;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.ui.UniSimpleAutocompleteModel;

/**
 * @author dseleznev
 * Created on: 15.04.2009
 */
public class OrgUnitMultiSelectModel extends UniSimpleAutocompleteModel
{
    private Model _model;
    private IDAO _dao;

    public OrgUnitMultiSelectModel(Model model, IDAO dao)
    {
        _dao = dao;
        _model = model;
    }

    @Override
    public ListResult findValues(String filter)
    {
        if (null == _model.getOrgUnitType()) return ListResult.getEmpty();
        return new ListResult<>(_dao.getOrgUnitsList(_model.getOrgUnitType(), filter));
    }

    @Override
    public String getLabelFor(Object value, int columnIndex)
    {
        return ((OrgUnit)value).getFullTitle();
    }
}