/**
 *$Id:$
 */
package ru.tandemservice.uniemp.base.bo.EmpMedicalStuffReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniemp.base.bo.EmpMedicalStuffReport.logic.EmpMedicalStuffReportDao;
import ru.tandemservice.uniemp.base.bo.EmpMedicalStuffReport.logic.IEmpMedicalStuffReportDao;

/**
 * @author Alexander Shaburov
 * @since 30.10.12
 */
@Configuration
public class EmpMedicalStuffReportManager extends BusinessObjectManager
{
    public static EmpMedicalStuffReportManager instance()
    {
        return instance(EmpMedicalStuffReportManager.class);
    }

    @Bean
    public IEmpMedicalStuffReportDao empMedicalStuffReportDao()
    {
        return new EmpMedicalStuffReportDao();
    }
}
