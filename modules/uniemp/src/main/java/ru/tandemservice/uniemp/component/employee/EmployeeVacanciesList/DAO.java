/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.EmployeeVacanciesList;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.commonbase.utils.SharedDQLUtils;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.ui.UniSelectModel;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.UniempDAO;
import ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation;
import ru.tandemservice.uniemp.entity.employee.StaffList;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;
import ru.tandemservice.uniemp.entity.employee.StaffListItem;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author dseleznev
 *         Created on: 29.10.2008
 */
public class DAO extends UniempDAO<Model> implements IDAO
{
    @Override
    public void prepare(final Model model) {
        
        model.setEmployeeTypeHierarchyList(HierarchyUtil.listHierarchyNodesWithParents(getCatalogItemList(EmployeeType.class),
                CommonCollator.TITLED_WITH_ID_COMPARATOR,false));

        model.setOrgUnitTypesList(OrgUnitManager.instance().dao().getOrgUnitActiveTypeList());

        model.setOrgunitsList(new UniSelectModel<OrgUnit, IDAO, Model>(model, this) {
            @Override
            public ListResult findValues(final String filter) {
                final MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "ou");
                builder.add(MQExpression.eq("ou", OrgUnit.P_ARCHIVAL, Boolean.FALSE));
                if (null != _model.getOrgUnitType()) {
                    builder.add(MQExpression.eq("ou", OrgUnit.L_ORG_UNIT_TYPE, _model.getOrgUnitType()));
                }
                builder.add(MQExpression.like("ou", OrgUnit.P_TITLE, CoreStringUtils.escapeLike(filter)));
                builder.addOrder("ou", OrgUnit.P_TITLE);
                return new ListResult<>(builder.getResultList(getSession()));
            }

            @Override
            public String getLabelFor(final Object value, int columnIndex) {
                return ((OrgUnit) value).getFullTitle();
            }
        });

        model.setPostsListModel(new DQLFullCheckSelectModel() {

            @Override
            public String getLabelFor(final Object value, int columnIndex)
            {
                return ((PostBoundedWithQGandQL) value).getFullTitle();
            }

            @Override
            protected DQLSelectBuilder query(final String alias, final String filter) {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(OrgUnitPostRelation.class, "ouPr")
                        .joinPath(DQLJoinType.inner, OrgUnitPostRelation.orgUnitTypePostRelation().postBoundedWithQGandQL().fromAlias("ouPr"), alias)
                        .where(likeUpper(property(alias, PostBoundedWithQGandQL.title()), value(CoreStringUtils.escapeLike(filter))))
                        .order(property(alias, PostBoundedWithQGandQL.title()));

                if (null != model.getOrgUnit()) {
                    if (!model.isChildOgrUnitVisible())
                        builder.where(eq(property("ouPr", OrgUnitPostRelation.orgUnit()), value(model.getOrgUnit())));
                    else {
                        DQLCTEBuilder2 cte = SharedDQLUtils.createChildTreeRecursiveCTE("tree", OrgUnit.class, model.getOrgUnit().getId(), OrgUnit.L_PARENT, true, null, null);
                        builder.with(cte.buildCTE());
                        builder.where(eqSubquery(property("ouPr", OrgUnitPostRelation.orgUnit().id()), DQLSubselectType.any, new DQLSelectBuilder().fromEntity("tree", "t").buildQuery()));
                    }
                }

                if (null != model.getEmployeeType()) {
                    DQLCTEBuilder2 cte = SharedDQLUtils.createChildTreeRecursiveCTE("eTypesTree", EmployeeType.class, model.getEmployeeType().getId(), EmployeeType.L_PARENT, true, null, null);
                    builder.with(cte.buildCTE());
                    builder.where(eqSubquery(property("ouPr", OrgUnitPostRelation.orgUnitTypePostRelation().postBoundedWithQGandQL().post().employeeType()),
                            DQLSubselectType.any, new DQLSelectBuilder().fromEntity("eTypesTree", "eType").buildQuery()));
                }

                return builder;
            }
        });
    }


        @Override
    @SuppressWarnings("unchecked")
    public void prepareListDataSource(final Model model)
    {
        /** Возвращает полный список должностей по штатному расписанию всего ОУ, для которых незанятая доля ставки
         * больше нуля. Занятыми считаются все ставки сотрудников, для которых состояние должности отлично от "уволен",
         * "отпуск дородовой и послеродовой", "отпуск по уходу ...", "возможный".
         *
         * Вычисляем активную версию штатного расписания (ШР). Активная версия ШР - версия, у которой выставлено
         * состояние "согласовано" и имеющаа последнюю дату согласования.
         */
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(StaffListItem.class, "sli").column(property("sli")).column(property("ai.entity"))
                .where(eq(property("sl", StaffList.staffListState().code()), value(UniempDefines.STAFF_LIST_STATUS_ACTIVE)))
                .joinPath(DQLJoinType.inner, StaffListItem.staffList().fromAlias("sli"), "sl")
                .fetchPath(DQLJoinType.inner, StaffListItem.staffList().fromAlias("sli"), "slam")
                .joinPath(DQLJoinType.inner, StaffListItem.orgUnitPostRelation().fromAlias("sli"), "ouPr")
                .fetchPath(DQLJoinType.inner,StaffListItem.orgUnitPostRelation().fromAlias("sli"), "ouPrOne")
                .joinPath(DQLJoinType.inner, OrgUnitPostRelation.orgUnitTypePostRelation().postBoundedWithQGandQL().fromAlias("ouPr"), "post")
                .fetchPath(DQLJoinType.inner, OrgUnitPostRelation.orgUnitTypePostRelation().postBoundedWithQGandQL().fromAlias("ouPr"), "postOne")
                .joinDataSource("sli", DQLJoinType.left,
                                new DQLSelectBuilder()
                                        .fromEntity(StaffListAllocationItem.class, "a")
                                        .column(property("a"), "entity")
                                        .column(property("a", StaffListAllocationItem.financingSource()), "financingSource")
                                        .column(property("a", StaffListAllocationItem.financingSourceItem()), "financingSourceItem")
                                        .column(property("a", StaffListAllocationItem.staffListItem().staffList()), "sl")
                                        .column(property("a", StaffListAllocationItem.staffListItem().orgUnitPostRelation()), "orgUnitPostRelation")
                                        .where(or(
                                                eq(property("a", StaffListAllocationItem.reserve()), value(Boolean.TRUE)),
                                                and(
                                                        isNotNull(property("a", StaffListAllocationItem.employeePost())),
                                                        eq(property("a", StaffListAllocationItem.employeePost().postStatus().active()), value(Boolean.TRUE))
                                                )
                                        ))
                                        .buildQuery(),
                                "ai",
                                and(
                                        eqNullSafe(property("ai.financingSourceItem"), property("sli", StaffListItem.financingSourceItem())),
                                        eq(property("ai.financingSource"), property("sli", StaffListItem.financingSource())),
                                        eq(property("ai.sl"), property("sli", StaffListItem.staffList())),
                                        eq(property("ai.orgUnitPostRelation"), property("ouPr"))
                                ));

        // Применение фильтров

        if (null != model.getOrgUnit())
        {
            if (!model.isChildOgrUnitVisible())
                builder.where(eq(property("sl", StaffList.orgUnit()), value(model.getOrgUnit())));
            else
            {
                DQLCTEBuilder2 cte = SharedDQLUtils.createChildTreeRecursiveCTE("tree", OrgUnit.class, model.getOrgUnit().getId(), OrgUnit.L_PARENT, true, null, null);
                builder.with(cte.buildCTE());
                builder.where(eqSubquery(property("sl", StaffList.orgUnit().id()), DQLSubselectType.any, new DQLSelectBuilder().fromEntity("tree", "t").buildQuery()));
            }
        }
        else if (null != model.getOrgUnitType())
        {
            builder.where(eq(property("sl", StaffList.orgUnit().orgUnitType()), value(model.getOrgUnitType())));
        }

        if ((null != model.getPostsList()) && !model.getPostsList().isEmpty()) {
            builder.where(in(property("post"), model.getPostsList()));
        }

        if (null != model.getEmployeeType()) {
            DQLCTEBuilder2 cte = SharedDQLUtils.createChildTreeRecursiveCTE("eTypesTree", EmployeeType.class, model.getEmployeeType().getId(), EmployeeType.L_PARENT, true, null, null);
            builder.with(cte.buildCTE());
            builder.where(eqSubquery(property("ouPr", OrgUnitPostRelation.orgUnitTypePostRelation().postBoundedWithQGandQL().post().employeeType()),
                    DQLSubselectType.any, new DQLSelectBuilder().fromEntity("eTypesTree", "eType").buildQuery()));
        }

        // Применение сортировок
        final DynamicListDataSource<ViewWrapper<StaffListItem>> dataSource = model.getDataSource();
        EntityOrder entityOrder = dataSource.getEntityOrder();
        String[] key = (String[]) entityOrder.getKey();

        if (Arrays.equals(key, StaffListItem.POST_TITLE_KEY))
        {
            builder.order(property("post", PostBoundedWithQGandQL.post().title()), entityOrder.getDirection());
        }
        else if (Arrays.equals(key, StaffListItem.POST_TYPE_KEY))
        {
            builder.order(property("post", PostBoundedWithQGandQL.post().employeeType().shortTitle()), entityOrder.getDirection());
        }
        else if (Arrays.equals(key, StaffListItem.PROF_QUALIFICATION_GROUP_KEY))
        {
            builder.order(property("post", PostBoundedWithQGandQL.profQualificationGroup().shortTitle()), entityOrder.getDirection());
        }
        else if (Arrays.equals(key, StaffListItem.QUALIFICATION_LEVEL_KEY))
        {
            builder.order(property("post", PostBoundedWithQGandQL.qualificationLevel().shortTitle()), entityOrder.getDirection());
        }
        else if (Arrays.equals(key, StaffListItem.ORG_UNIT_TITLE_KEY))
        {
            builder.order(property("ouPr", OrgUnitPostRelation.orgUnit().title()), entityOrder.getDirection());
            builder.order(property("post", PostBoundedWithQGandQL.title()));
        }

        List<ViewWrapper<StaffListItem>> resultList = Lists.newArrayList();
        Map<StaffListItem, List<StaffListAllocationItem>> staffListMap = Maps.newLinkedHashMap();
        for (Object[] row : builder.createStatement(getSession()).<Object[]>list())
            SafeMap.safeGet(staffListMap, (StaffListItem) row[0], ArrayList.class).add((StaffListAllocationItem) row[1]);

        for (Map.Entry<StaffListItem, List<StaffListAllocationItem>> entry : staffListMap.entrySet())
        {
            Double occStaffRate = 0.0;
            StaffListItem item = entry.getKey();

            for (StaffListAllocationItem allocationItem : entry.getValue())
            {
                if (null != allocationItem)
                {
                    occStaffRate += allocationItem.getStaffRate();
                }
            }
            Double remainingStaffRate = Math.round((item.getStaffRate() - occStaffRate) * 100) / 100.0;

            if (remainingStaffRate > 0)
            {
                ViewWrapper<StaffListItem> wrap = new ViewWrapper<>(item);
                wrap.setViewProperty("remStaffRate", remainingStaffRate);
                resultList.add(wrap);
            }
        }

        UniBaseUtils.createPage(dataSource, resultList);

        getSession().flush();
        getSession().clear();

    }
}