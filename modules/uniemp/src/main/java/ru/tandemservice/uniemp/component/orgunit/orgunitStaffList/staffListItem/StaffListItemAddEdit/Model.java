/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffListItem.StaffListItemAddEdit;

import java.util.List;

import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;

import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation;
import ru.tandemservice.uniemp.entity.employee.StaffList;
import ru.tandemservice.uniemp.entity.employee.StaffListItem;

/**
 * @author dseleznev
 * Created on: 19.09.2008
 */
@Input(keys = { "staffListId", "staffListItemId" }, bindings = { "staffListId", "staffListItemId" })
public class Model
{
    private Double _staffRate;
    private List<FinancingSource> _financingSourceModel;
    private ISingleSelectModel _financingSourceItemModel;

    private Long _staffListId;
    private StaffList _staffList;

    private Long _staffListItemId;
    private StaffListItem _staffListItem = new StaffListItem();

    private List<OrgUnitPostRelation> _orgUnitPostsList;

    //Getters & Setters

    public Double getStaffRate()
    {
        return _staffRate;
    }

    public void setStaffRate(Double staffRate)
    {
        _staffRate = staffRate;
    }

    public List<FinancingSource> getFinancingSourceModel()
    {
        return _financingSourceModel;
    }

    public void setFinancingSourceModel(List<FinancingSource> financingSourceModel)
    {
        _financingSourceModel = financingSourceModel;
    }

    public ISingleSelectModel getFinancingSourceItemModel()
    {
        return _financingSourceItemModel;
    }

    public void setFinancingSourceItemModel(ISingleSelectModel financingSourceItemModel)
    {
        _financingSourceItemModel = financingSourceItemModel;
    }

    public Long getStaffListId()
    {
        return _staffListId;
    }

    public void setStaffListId(Long staffListId)
    {
        this._staffListId = staffListId;
    }

    public StaffList getStaffList()
    {
        return _staffList;
    }

    public void setStaffList(StaffList staffList)
    {
        this._staffList = staffList;
    }

    public Long getStaffListItemId()
    {
        return _staffListItemId;
    }

    public void setStaffListItemId(Long staffListItemId)
    {
        this._staffListItemId = staffListItemId;
    }

    public StaffListItem getStaffListItem()
    {
        return _staffListItem;
    }

    public void setStaffListItem(StaffListItem staffListItem)
    {
        this._staffListItem = staffListItem;
    }

    public List<OrgUnitPostRelation> getOrgUnitPostsList()
    {
        return _orgUnitPostsList;
    }

    public void setOrgUnitPostsList(List<OrgUnitPostRelation> orgUnitPostsList)
    {
        this._orgUnitPostsList = orgUnitPostsList;
    }

}