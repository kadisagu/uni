/**
 * $Id$
 */
package ru.tandemservice.uniemp.dao;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.*;
import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.bo.OrgUnit.logic.OrgUnitRankComparator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.component.reports.staffList.Add.OrgUnitTypeWrapper;
import ru.tandemservice.uniemp.component.reports.staffListAllocation.Add.EmployeePostFakeWrapper;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.employee.*;
import ru.tandemservice.uniemp.entity.report.StaffListRepPaymentSettings;

import java.util.*;

/**
 * @author dseleznev
 * Created on: 10.11.2010
 */
public class StaffListReportsDAO extends UniDao implements IStaffListReportsDAO
{
    @Override
    public List<OrgUnit> getFilteredOrgUnitslList(OrgUnit orgUnitFilter, final List<OrgUnitType> orgUnitTypesList, final Date reportDate)
    {
        final List<OrgUnit> orgUnitsList = new ArrayList<>();

        final List<Long> allowedOrgUnitIdsList = new ArrayList<>();
        if (null != orgUnitFilter) allowedOrgUnitIdsList.add(orgUnitFilter.getId());
        else orgUnitFilter = TopOrgUnit.getInstance();

        for (OrgUnit orgUnit : OrgUnitManager.instance().dao().getChildOrgUnits(orgUnitFilter, true))
            if (!orgUnit.isArchival() || orgUnit.getArchivingDate().getTime() > reportDate.getTime())
                allowedOrgUnitIdsList.add(orgUnit.getId());

        BatchUtils.execute(allowedOrgUnitIdsList, DQL.MAX_VALUES_ROW_NUMBER, allowedOrgUnitIdsLocalList -> {
            AbstractExpression expr1 = MQExpression.isNull("ou", OrgUnit.P_ARCHIVING_DATE);
            AbstractExpression expr2 = MQExpression.isNotNull("ou", OrgUnit.P_ARCHIVING_DATE);
            AbstractExpression expr3 = MQExpression.greatOrEq("ou", OrgUnit.P_ARCHIVING_DATE, reportDate);

            MQBuilder orgUnitBuilder = new MQBuilder(OrgUnit.ENTITY_CLASS, "ou");
            if (null != orgUnitTypesList)
                orgUnitBuilder.add(MQExpression.in("ou", OrgUnit.L_ORG_UNIT_TYPE, orgUnitTypesList));
            if (!allowedOrgUnitIdsList.isEmpty())
                orgUnitBuilder.add(MQExpression.in("ou", OrgUnit.P_ID, allowedOrgUnitIdsLocalList));
            orgUnitBuilder.add(MQExpression.or(expr1, MQExpression.and(expr2, expr3)));

            orgUnitBuilder.addOrder("ou", OrgUnit.P_TITLE);
            orgUnitsList.addAll(orgUnitBuilder.<OrgUnit> getResultList(getSession()));
        });

        return orgUnitsList;
    }

    @Override
    public StaffList getActiveStaffList(OrgUnit orgUnit, Date reportDate)
    {
        MQBuilder builder = new MQBuilder(StaffList.ENTITY_CLASS, "sl");
        builder.add(MQExpression.greatOrEq("sl", StaffList.acceptionDate().s(), reportDate));
        builder.add(MQExpression.eq("sl", StaffList.orgUnit().s(), orgUnit));
        builder.addDescOrder("sl", StaffList.acceptionDate().s());
        if (builder.getResultCount(getSession()) > 0)
            return (StaffList)builder.first(getSession());
        else
            return null;
    }

    @Override
    public List<StaffList> getActiveStaffListsList(List<OrgUnit> orgUnitsList, final Date reportDate)
    {
        List<StaffList> resultList = new ArrayList<>();
        final List<StaffList> preResultList = new ArrayList<>();
        BatchUtils.execute(orgUnitsList, DQL.MAX_VALUES_ROW_NUMBER, orgUnitsList1 -> {
            MQBuilder builder = new MQBuilder(StaffList.ENTITY_CLASS, "sl");
            builder.addJoinFetch("sl", StaffList.orgUnit().s(), "ou");
            builder.add(MQExpression.lessOrEq("sl", StaffList.acceptionDate().s(), reportDate));
            AbstractExpression expr1 = MQExpression.isNull("sl", StaffList.archivingDate().s());
            AbstractExpression expr2 = MQExpression.isNotNull("sl", StaffList.archivingDate().s());
            AbstractExpression expr3 = MQExpression.greatOrEq("sl", StaffList.archivingDate().s(), reportDate);
            builder.add(MQExpression.or(expr1, MQExpression.and(expr2, expr3)));
            builder.add(MQExpression.in("sl", StaffList.orgUnit().s(), orgUnitsList1));
            builder.addDescOrder("sl", StaffList.acceptionDate().s());
            preResultList.addAll(builder.<StaffList> getResultList(getSession()));
        });

        Map<OrgUnit, List<StaffList>> staffListMap = new HashMap<>();
        for (StaffList staffList : preResultList)
        {
            List<StaffList> list = staffListMap.get(staffList.getOrgUnit());
            if (list == null)
                staffListMap.put(staffList.getOrgUnit(), list = new ArrayList<>());
            list.add(staffList);
        }

        for (List<StaffList> list : staffListMap.values())
            Collections.sort(list, (o1, o2) -> {
                boolean active1 = o1.getStaffListState().getCode().equals(UniempDefines.STAFF_LIST_STATUS_ACTIVE);
                boolean active2 = o2.getStaffListState().getCode().equals(UniempDefines.STAFF_LIST_STATUS_ACTIVE);

                return (active1 == active2) ? 0 : (active1 ? -1 : 1);
            });

        for (Map.Entry<OrgUnit, List<StaffList>> entry : staffListMap.entrySet())
        {
            resultList.add(entry.getValue().get(0));
        }

        return resultList;
    }

    @Override
    public List<StaffListItem> getStaffListItemsList(List<StaffList> staffListsList, final PostBoundedWithQGandQL post, final List<EmployeeType> employeeTypesList, final QualificationLevel qualificationLevel, final ProfQualificationGroup profQualificationGroup, final String financingSourceCode, final String financingSourceItemCode)
    {
        final List<StaffListItem> staffListItemsList = new ArrayList<>();

        BatchUtils.execute(staffListsList, DQL.MAX_VALUES_ROW_NUMBER, staffListsListInner -> {
            MQBuilder staffListItemsBuilder = new MQBuilder(StaffListItem.ENTITY_CLASS, "sli");
            staffListItemsBuilder.add(MQExpression.in("sli", StaffListItem.L_STAFF_LIST, staffListsListInner));

            if (financingSourceCode != null)
                staffListItemsBuilder.add(MQExpression.eq("sli", StaffListItem.financingSource().code().s(), financingSourceCode));
            if (financingSourceItemCode != null)
                staffListItemsBuilder.add(MQExpression.eq("sli", StaffListItem.financingSourceItem().code().s(), financingSourceItemCode));

            if (null != post)
                staffListItemsBuilder.add(MQExpression.eq("sli", StaffListItem.L_ORG_UNIT_POST_RELATION + "." + OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L, post));
            else
            {
                if (null != employeeTypesList && !employeeTypesList.isEmpty())
                    staffListItemsBuilder.add(MQExpression.in("sli", StaffListItem.L_ORG_UNIT_POST_RELATION + "." + OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_POST + "." + Post.L_EMPLOYEE_TYPE, employeeTypesList));

                if (null != qualificationLevel)
                    staffListItemsBuilder.add(MQExpression.eq("sli", StaffListItem.L_ORG_UNIT_POST_RELATION + "." + OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_QUALIFICATION_LEVEL, qualificationLevel));

                if (null != profQualificationGroup)
                    staffListItemsBuilder.add(MQExpression.eq("sli", StaffListItem.L_ORG_UNIT_POST_RELATION + "." + OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_PROF_QUALIFICATION_GROUP, profQualificationGroup));
            }

            staffListItemsList.addAll(staffListItemsBuilder.<StaffListItem> getResultList(getSession()));
        });

        return staffListItemsList;
    }

    @Override
    public List<StaffListAllocationItem> getStaffListAllocationItemsList(List<StaffListItem> staffListItemsList)
    {
        final List<StaffListAllocationItem> allocItemsList = new ArrayList<>();

        BatchUtils.execute(staffListItemsList, DQL.MAX_VALUES_ROW_NUMBER, staffListItemsListInner -> {
            MQBuilder allocItemsBuilder = new MQBuilder(StaffListAllocationItem.ENTITY_CLASS, "ai");
            allocItemsBuilder.add(MQExpression.in("ai", StaffListAllocationItem.L_STAFF_LIST_ITEM, staffListItemsListInner));
            allocItemsList.addAll(allocItemsBuilder.<StaffListAllocationItem> getResultList(getSession()));
        });

        return allocItemsList;
    }

    @Override
    public List<Payment> getDisplayablePaymentsForStaffList()
    {
        List<Payment> paymentsList = new ArrayList<>();

        MQBuilder builder = new MQBuilder(StaffListRepPaymentSettings.ENTITY_CLASS, "s");
        AbstractExpression expr1 = MQExpression.eq("s", StaffListRepPaymentSettings.P_TAKE_INTO_ACCOUNT, Boolean.TRUE);
        AbstractExpression expr2 = MQExpression.isNull("s", StaffListRepPaymentSettings.L_PAYMENT);
        builder.add(MQExpression.or(expr1, expr2));
        builder.addOrder("s", StaffListRepPaymentSettings.P_PRIORITY);

        for (StaffListRepPaymentSettings setting : builder.<StaffListRepPaymentSettings> getResultList(getSession()))
            if (setting.isTakeIntoAccount())
                paymentsList.add(setting.getPayment());

        return paymentsList;
    }

    @Override
    public List<Payment> getDisplayablePaymentsForStaffList(Set<Payment> additionalPaymentsSet)
    {
        List<Payment> paymentsList = new ArrayList<>();

        MQBuilder builder = new MQBuilder(StaffListRepPaymentSettings.ENTITY_CLASS, "s");
        builder.addOrder("s", StaffListRepPaymentSettings.P_PRIORITY);

        for (StaffListRepPaymentSettings setting : builder.<StaffListRepPaymentSettings> getResultList(getSession()))
            if (null == setting.getPayment() || setting.isTakeIntoAccount() || additionalPaymentsSet.contains(setting.getPayment()))
                paymentsList.add(setting.getPayment());

        return paymentsList;
    }

    @Override
    public List<StaffListPaymentBase> getStaffListPaymentsList(final List<StaffListItem> staffListItemsList, final List<Payment> paymentsList, final String financingSourceCode, final String financingSourceItemCode)
    {
        final List<StaffListPaymentBase> staffListPaymentsList = new ArrayList<>();

        BatchUtils.execute(paymentsList, DQL.MAX_VALUES_ROW_NUMBER, paymentsListInner -> BatchUtils.execute(staffListItemsList, DQL.MAX_VALUES_ROW_NUMBER, staffListItemsListInner -> {
            MQBuilder paymentsBuilder = new MQBuilder(StaffListPaymentBase.ENTITY_CLASS, "pi");
            paymentsBuilder.addLeftJoin("pi", StaffListPaymentBase.L_FINANCING_SOURCE, "pfs");
            paymentsBuilder.add(MQExpression.in("pi", StaffListPaymentBase.L_STAFF_LIST_ITEM, staffListItemsListInner));
            paymentsBuilder.add(MQExpression.in("pi", StaffListPaymentBase.L_PAYMENT, paymentsListInner));
            if (null != financingSourceCode)
            {
                AbstractExpression paymExpr1 = MQExpression.isNull("pi", StaffListPaymentBase.L_FINANCING_SOURCE);
                AbstractExpression paymExpr2 = MQExpression.eq("pi", StaffListPaymentBase.L_FINANCING_SOURCE + "." + ICatalogItem.CATALOG_ITEM_CODE, financingSourceCode);
                paymentsBuilder.add(MQExpression.or(paymExpr1, paymExpr2));
            }

            if (null != financingSourceItemCode)
            {
                AbstractExpression paymExpr1 = MQExpression.isNull("pi", StaffListPaymentBase.L_FINANCING_SOURCE_ITEM);
                AbstractExpression paymExpr2 = MQExpression.eq("pi", StaffListPaymentBase.L_FINANCING_SOURCE_ITEM + "." + ICatalogItem.CATALOG_ITEM_CODE, financingSourceItemCode);
                paymentsBuilder.add(MQExpression.or(paymExpr1, paymExpr2));
            }
            staffListPaymentsList.addAll(paymentsBuilder.<StaffListPaymentBase> getResultList(getSession()));
        }));

        return staffListPaymentsList;
    }

    @Override
    public Set<Payment> getUsedPaymentsSet(List<StaffListPaymentBase> staffListPaymentsList)
    {
        Set<Payment> paymentsSet = new HashSet<>();
        for (StaffListPaymentBase slPayment : staffListPaymentsList)
        {
            if (!paymentsSet.contains(slPayment.getPayment()))
                paymentsSet.add(slPayment.getPayment());
        }

        return paymentsSet;
    }

    @Override
    public List<EmployeePostFakeWrapper> getEmployeePostStaffRateItemList(List<StaffListItem> staffListItemsList, List<OrgUnit> orgUnitsList, final PostBoundedWithQGandQL post, final List<EmployeeType> employeeTypesList, final QualificationLevel qualificationLevel, final ProfQualificationGroup profQualificationGroup, final Date reportDate, final Boolean budgetActiveType)
    {
        final List<EmployeePostFakeWrapper> employeePostFakeWrappersList = new ArrayList<>();

        final FinancingSource finSrcBudgetCatalogItem = getCatalogItem(FinancingSource.class, UniempDefines.FINANCING_SOURCE_BUDGET);
        BatchUtils.execute(orgUnitsList, DQL.MAX_VALUES_ROW_NUMBER, orgUnitsListInner -> {
            MQBuilder builder = new MQBuilder(EmployeePostStaffRateItem.ENTITY_CLASS, "rel");
            builder.addJoinFetch("rel", EmployeePostStaffRateItem.employeePost().s(), "ep");
            builder.addJoinFetch("ep", EmployeePost.orgUnit().s(), "ou");
            builder.addJoinFetch("ou", OrgUnit.orgUnitType().s(), "outp");
            builder.addJoinFetch("ep", EmployeePost.postRelation().s(), "outpr");
            builder.addJoinFetch("outpr", OrgUnitTypePostRelation.postBoundedWithQGandQL().s(), "pb");
            builder.addJoinFetch("ep", EmployeePost.employee().s(), "e");
            builder.addJoinFetch("e", Employee.person().s(), "p");
            builder.addJoinFetch("p", Person.identityCard().s(), "ic");
            builder.add(MQExpression.in("rel", EmployeePostStaffRateItem.employeePost().orgUnit().s(), orgUnitsListInner));

            builder.add(MQExpression.lessOrEq("rel", EmployeePostStaffRateItem.employeePost().postDate().s(), reportDate));

            if (reportDate.equals(CommonBaseDateUtil.trimTime(new Date())))
                builder.add(MQExpression.eq("rel", EmployeePostStaffRateItem.employeePost().postStatus().active().s(), true));

            if (budgetActiveType != null)
                if (budgetActiveType)
                    builder.add(MQExpression.eq("rel", EmployeePostStaffRateItem.financingSource().code().s(), UniempDefines.FINANCING_SOURCE_BUDGET));
                else
                    builder.add(MQExpression.eq("rel", EmployeePostStaffRateItem.financingSource().code().s(), UniempDefines.FINANCING_SOURCE_OFF_BUDGET));

            if (null != post)
                builder.add(MQExpression.eq("rel", EmployeePostStaffRateItem.employeePost().postRelation().postBoundedWithQGandQL().s(), post));

            if (null != employeeTypesList && !employeeTypesList.isEmpty())
                builder.add(MQExpression.in("rel", EmployeePostStaffRateItem.employeePost().postRelation().postBoundedWithQGandQL().post().employeeType().s(), employeeTypesList));

            if (null != qualificationLevel)
                builder.add(MQExpression.eq("rel", EmployeePostStaffRateItem.employeePost().postRelation().postBoundedWithQGandQL().qualificationLevel().s(), qualificationLevel));

            if (null != profQualificationGroup)
                builder.add(MQExpression.eq("rel", EmployeePostStaffRateItem.employeePost().postRelation().postBoundedWithQGandQL().profQualificationGroup().s(), profQualificationGroup));

            for (EmployeePostStaffRateItem rel : builder.<EmployeePostStaffRateItem> getResultList(getSession()))
            {
                if (null != rel.getEmployeePost().getDismissalDate() && rel.getEmployeePost().getDismissalDate().getTime() < reportDate.getTime())
                    continue;

                EmployeePostFakeWrapper wrapper = new EmployeePostFakeWrapper();
                wrapper.setId(rel.getId());
                wrapper.setSalary(null != rel.getEmployeePost().getSalary() ? rel.getEmployeePost().getSalary() : 0d);
                wrapper.setLastName(rel.getEmployeePost().getEmployee().getPerson().getIdentityCard().getLastName());
                wrapper.setFirstName(rel.getEmployeePost().getEmployee().getPerson().getIdentityCard().getFirstName());
                wrapper.setMiddleName(rel.getEmployeePost().getEmployee().getPerson().getIdentityCard().getMiddleName());
                wrapper.setEmployeePostId(rel.getEmployeePost().getId());
                wrapper.setPostType(rel.getEmployeePost().getPostType());
                wrapper.setOrgUnit(rel.getEmployeePost().getOrgUnit());
                wrapper.setPostRelation(rel.getEmployeePost().getPostRelation());
                if (rel.getFinancingSource().equals(finSrcBudgetCatalogItem))
                {
                    wrapper.setBudgetStaffRate(rel.getStaffRate());
                    wrapper.setOffBudgetStaffRate(0d);
                }
                else
                {
                    wrapper.setBudgetStaffRate(0d);
                    wrapper.setOffBudgetStaffRate(rel.getStaffRate());
                }
                wrapper.setFinancingSource(rel.getFinancingSource());
                wrapper.setFinancingSourceItem(rel.getFinancingSourceItem());

                employeePostFakeWrappersList.add(wrapper);
            }
        });

        //new ArrayList<StaffListAllocationItem>();

        //формируем фейковые враперы, по резервным Должностям Штатной расстановки
        BatchUtils.execute(staffListItemsList, DQL.MAX_VALUES_ROW_NUMBER, staffListItemsListInner -> {
            MQBuilder allocItemsBuilder = new MQBuilder(StaffListAllocationItem.ENTITY_CLASS, "ai");
            allocItemsBuilder.add(MQExpression.in("ai", StaffListAllocationItem.L_STAFF_LIST_ITEM, staffListItemsListInner));
            allocItemsBuilder.add(MQExpression.eq("ai", StaffListAllocationItem.P_RESERVE, Boolean.TRUE));

            for (StaffListAllocationItem allocationItem : allocItemsBuilder.<StaffListAllocationItem>getResultList(getSession()))
            {
                EmployeePostFakeWrapper wrapper = new EmployeePostFakeWrapper();
                wrapper.setId(allocationItem.getId());
                wrapper.setSalary(allocationItem.getMonthBaseSalaryFund());
                wrapper.setLastName("Резерв");
                wrapper.setFirstName("");
                wrapper.setMiddleName("");
                wrapper.setEmployeePostId(allocationItem.getId());
                wrapper.setPostType(null);
                wrapper.setOrgUnit(allocationItem.getStaffListItem().getStaffList().getOrgUnit());
                wrapper.setPostRelation(allocationItem. getStaffListItem().getOrgUnitPostRelation().getOrgUnitTypePostRelation());
                if (allocationItem.getFinancingSource().equals(finSrcBudgetCatalogItem))
                {
                    wrapper.setBudgetStaffRate(allocationItem.getStaffRate());
                    wrapper.setOffBudgetStaffRate(0d);
                }
                else
                {
                    wrapper.setBudgetStaffRate(0d);
                    wrapper.setOffBudgetStaffRate(allocationItem.getStaffRate());
                }
                wrapper.setFinancingSource(allocationItem.getFinancingSource());
                wrapper.setFinancingSourceItem(allocationItem.getFinancingSourceItem());

                employeePostFakeWrappersList.add(wrapper);
            }
        });

        return employeePostFakeWrappersList;
    }

    @Override
    public List<OrgUnit> getAccountableOrgUnitsList(List<OrgUnitType> orgUnitTypesList, List<Long> allowedOrgUnitIdsList, Date reportDate)
    {
        if (orgUnitTypesList == null)
            throw new NullPointerException("Parameter 'orgUnitTypesList' should be not null.");

        AbstractExpression notArchiv = MQExpression.isNull("ou", OrgUnit.P_ARCHIVING_DATE);
        AbstractExpression isArchiv = MQExpression.isNotNull("ou", OrgUnit.P_ARCHIVING_DATE);
        AbstractExpression greatOrEqDate = MQExpression.greatOrEq("ou", OrgUnit.P_ARCHIVING_DATE, reportDate);

        MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "ou");
        builder.add(MQExpression.in("ou", OrgUnit.L_ORG_UNIT_TYPE, orgUnitTypesList));
        if (allowedOrgUnitIdsList != null && !allowedOrgUnitIdsList.isEmpty())
            builder.add(MQExpression.in("ou", OrgUnit.P_ID, allowedOrgUnitIdsList));
        builder.add(MQExpression.or(notArchiv, MQExpression.and(isArchiv, greatOrEqDate)));

        builder.addOrder("ou", OrgUnit.P_TITLE);
        return builder.getResultList(getSession());
    }

    @Override
    public List<StaffListItem> getStaffListItemList(List<OrgUnit> orgUnitsList, Date reportDate, String financingSourceCode, List<PostBoundedWithQGandQL> postList, List<EmployeeType> employeeTypeList, QualificationLevel qualificationLevel, ProfQualificationGroup qualificationGroup)
    {
        if (orgUnitsList == null)
            throw new NullPointerException("Parameter 'orgUnitsList' should be not null.");

        List<OrgUnit> staffOrgUnits = new ArrayList<>();
        MQBuilder staffListBuilder = new MQBuilder(StaffList.ENTITY_CLASS, "sl");
        staffListBuilder.add(MQExpression.in("sl", StaffList.staffListState().code().s(), UniempDefines.STAFF_LIST_STATUS_ACTIVE, UniempDefines.STAFF_LIST_STATUS_ARCHIVE));
        staffListBuilder.add(MQExpression.lessOrEq("sl", StaffList.acceptionDate().s(), reportDate));
        staffListBuilder.add(MQExpression.in("sl", StaffList.orgUnit().s(), orgUnitsList));
        staffListBuilder.addDescOrder("sl", StaffList.acceptionDate().s());

        List<StaffList> staffListsList = new ArrayList<>();
        for (StaffList staffList : staffListBuilder.<StaffList> getResultList(getSession()))
        {
            if (!staffOrgUnits.contains(staffList.getOrgUnit()) && (null == staffList.getArchivingDate() || (staffList.getArchivingDate().getTime() >= reportDate.getTime())))
            {
                staffListsList.add(staffList);
                staffOrgUnits.add(staffList.getOrgUnit());
            }
        }

        MQBuilder staffListItemsBuilder = new MQBuilder(StaffListItem.ENTITY_CLASS, "sli");
        staffListItemsBuilder.add(MQExpression.in("sli", StaffListItem.staffList().s(), staffListsList));
        if (financingSourceCode != null)
            staffListItemsBuilder.add(MQExpression.eq("sli", StaffListItem.financingSource().code().s(), financingSourceCode));
        if (null != postList && !postList.isEmpty())
        {
            for (PostBoundedWithQGandQL post : postList)
                staffListItemsBuilder.add(MQExpression.eq("sli", StaffListItem.orgUnitPostRelation().orgUnitTypePostRelation().postBoundedWithQGandQL().s(), post));
        }
        else
        {
            if (null != employeeTypeList && !employeeTypeList.isEmpty())
                staffListItemsBuilder.add(MQExpression.in("sli", StaffListItem.orgUnitPostRelation().orgUnitTypePostRelation().postBoundedWithQGandQL().post().employeeType().s(), employeeTypeList));

            if (null != qualificationLevel)
                staffListItemsBuilder.add(MQExpression.eq("sli", StaffListItem.orgUnitPostRelation().orgUnitTypePostRelation().postBoundedWithQGandQL().qualificationLevel().s(), qualificationLevel));

            if (null != qualificationGroup)
                staffListItemsBuilder.add(MQExpression.eq("sli", StaffListItem.orgUnitPostRelation().orgUnitTypePostRelation().postBoundedWithQGandQL().profQualificationGroup().s(), qualificationGroup));
        }

        return staffListItemsBuilder.getResultList(getSession());
    }

    @Override
    public List<StaffListPaymentBase> getStaffListPaymentList(List<StaffListItem> staffListItemsList, List<Payment> paymentsList, String financingSourceCode)
    {
        if (staffListItemsList == null)
            throw new NullPointerException("Parameter 'staffListItemsList' should be not null.");

        MQBuilder builder = new MQBuilder(StaffListPaymentBase.ENTITY_CLASS, "pi");
        builder.add(MQExpression.in("pi", StaffListPaymentBase.staffListItem().s(), staffListItemsList));
        if (paymentsList != null)
            builder.add(MQExpression.in("pi", StaffListPaymentBase.payment().s(), paymentsList));
        if (null != financingSourceCode)
        {
            AbstractExpression paymNull = MQExpression.isNull("pi", StaffListPaymentBase.financingSource().s());
            AbstractExpression paymEq = MQExpression.eq("pi", StaffListPaymentBase.financingSource().code().s(), financingSourceCode);
            builder.add(MQExpression.or(paymNull, paymEq));
        }

        return builder.getResultList(getSession());
    }

    @Override
    public List<OrgUnit> getChildOrgUnits(OrgUnit orgUnit, List<OrgUnit> orgUnitsToTake, Map<Long, OrgUnitTypeWrapper> orgUnitTypesMap, Date actualDate)
    {
        if (null != orgUnit.getParent() && !orgUnitTypesMap.get(orgUnit.getOrgUnitType().getId()).isShowChildOrgUnits())
            return new ArrayList<>();

        List<OrgUnit> result = new ArrayList<>();

        AbstractExpression expr1 = MQExpression.isNull("ou", OrgUnit.P_ARCHIVING_DATE);
        AbstractExpression expr2 = MQExpression.isNotNull("ou", OrgUnit.P_ARCHIVING_DATE);
        AbstractExpression expr3 = MQExpression.greatOrEq("ou", OrgUnit.P_ARCHIVING_DATE, actualDate);

        MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "ou");
        builder.add(MQExpression.eq("ou", OrgUnit.L_PARENT, orgUnit));
        builder.add(MQExpression.or(expr1, MQExpression.and(expr2, expr3)));
        builder.addOrder("ou", OrgUnit.L_ORG_UNIT_TYPE + "." + OrgUnitType.P_PRIORITY);
        builder.addOrder("ou", OrgUnit.P_NOMINATIVE_CASE_TITLE);
        builder.addOrder("ou", OrgUnit.P_TITLE);
        List<OrgUnit> childsList = builder.getResultList(getSession());

        Collections.sort(childsList, OrgUnitRankComparator.INSTANCE);

        for (OrgUnit child : childsList)
        {
            if (orgUnitsToTake.contains(child))
            {
                result.add(child);
                if (orgUnitTypesMap.get(child.getOrgUnitType().getId()).isShowChildOrgUnits())
                    result.addAll(getChildOrgUnits(child, orgUnitsToTake, orgUnitTypesMap, actualDate));
            }
        }

        return result;
    }


}