/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.ext.SystemAction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.SystemActionManager;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.util.SystemActionDefinition;
import ru.tandemservice.uniemp.base.ext.SystemAction.ui.Pub.SystemActionPubExt;

/**
 * @author Alexander Shaburov
 * @since 12.02.13
 */
@Configuration
public class SystemActionExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private SystemActionManager _systemActionManager;

    @Bean
    public ItemListExtension<SystemActionDefinition> buttonListExtension()
    {
        return itemListExtension(_systemActionManager.buttonListExtPoint())
                .add("uniemp_createEmployeeCards", new SystemActionDefinition("uniemp", "createEmployeeCards", "onClickCreateEmployeeCards", SystemActionPubExt.EMP_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("uniemp_recalculateStaffListPayments", new SystemActionDefinition("uniemp", "recalculateStaffListPayments", "onClickRecalculateStaffListPayments", SystemActionPubExt.EMP_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("uniemp_moveToFormationActiveStaffLists", new SystemActionDefinition("uniemp", "moveToFormationActiveStaffLists", "onClickMoveActiveStaffListVersionsToFormation", SystemActionPubExt.EMP_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("uniemp_synchronizeEmploymentHistory", new SystemActionDefinition("uniemp", "synchronizeEmploymentHistory", "onClickSynchronizeEmploymentHistory", SystemActionPubExt.EMP_SYSTEM_ACTION_PUB_ADDON_NAME))
                /*<button name="uniemp_synchronizeEmploymentHistoryStaffRates" label="Синхронизировать доли ставки в истории должностей" listener="{extension}:onClickSynchronizeEmploymentHistoryStaffRates" alert="Синхронизировать доли ставки в истории должностей?"/>*/
                .add("uniemp_syncSLCreateAndAcceptDates", new SystemActionDefinition("uniemp", "syncSLCreateAndAcceptDates", "onClickSyncStaffListCreateAndAcceptDates", SystemActionPubExt.EMP_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("uniemp_initStaffListArchivingDates", new SystemActionDefinition("uniemp", "initStaffListArchivingDates", "onClickInitStaffListArchivingDates", SystemActionPubExt.EMP_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("uniemp_reallocateStaffListAllocItems", new SystemActionDefinition("uniemp", "reallocateStaffListAllocItems", "onClickReallocateStaffListAllocItems", SystemActionPubExt.EMP_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("uniemp_сorrectEmptySalaryAllocItems", new SystemActionDefinition("uniemp", "сorrectEmptySalaryAllocItems", "onClickCorrectEmptySalaryAllocItems", SystemActionPubExt.EMP_SYSTEM_ACTION_PUB_ADDON_NAME))
                /*<button name="uniemp_synchronizeEmployeeStages" label="Синхронизировать стажи сотрудников" listener="{extension}:onClickSynchronizeEmployeeStages" alert="Синхронизировать стажи сотрудников?"/>*/
                .add("uniemp_correctEmployeeHolidayFinishDate", new SystemActionDefinition("uniemp", "correctEmployeeHolidayFinishDate", "onClickCorrectEmployeeHolidayFinishDate", SystemActionPubExt.EMP_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("uniemp_getPostCatalogs", new SystemActionDefinition("uniemp", "getPostCatalogs", "onClickGetPostCatalogs", SystemActionPubExt.EMP_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("uniemp_fillStaffListsAutomatically", new SystemActionDefinition("uniemp", "fillStaffListsAutomatically", "onClickFillStaffListsAutomatically", SystemActionPubExt.EMP_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("uniemp_actualizeOrgUnitTypePostRelation", new SystemActionDefinition("uniemp", "actualizeOrgUnitTypePostRelation", "onClickActualizeOrgUnitTypePostRelation", SystemActionPubExt.EMP_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("uniemp_bindSecondJobEmployeePost", new SystemActionDefinition("uniemp", "bindSecondJobEmployeePost", "onClickBindSecondJobEmployeePost", SystemActionPubExt.EMP_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("uniemp_setEmployeeContractEndDate", new SystemActionDefinition("uniemp", "setEmployeeContractEndDate", "onClickSetEmployeeContractEndDate", SystemActionPubExt.EMP_SYSTEM_ACTION_PUB_ADDON_NAME))
                .create();
    }
}
