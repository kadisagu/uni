/**
 *$Id:$
 */
package ru.tandemservice.uniemp.component.catalog.postBoundedWithQGandQL.PostBoundedWithQGandQLItemPub;

import ru.tandemservice.uniemp.entity.catalog.ServiceLengthType;

import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 17.09.12
 */
public class Model extends org.tandemframework.shared.employeebase.component.catalog.postBoundedWithQGandQL.PostBoundedWithQGandQLItemPub.Model
{
    private List<ServiceLengthType> _serviceLengthTypeList;

    // Getters & Setters

    public List<ServiceLengthType> getServiceLengthTypeList()
    {
        return _serviceLengthTypeList;
    }

    public void setServiceLengthTypeList(List<ServiceLengthType> serviceLengthTypeList)
    {
        _serviceLengthTypeList = serviceLengthTypeList;
    }
}
