/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffListItem.StaffListItemPub;

import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uniemp.entity.employee.StaffList;
import ru.tandemservice.uniemp.entity.employee.StaffListItem;
import ru.tandemservice.uniemp.entity.employee.StaffListPostPayment;

/**
 * @author dseleznev
 * Created on: 19.09.2008
 */

@State(keys = {PublisherActivator.PUBLISHER_ID_KEY, "staffListId"}, bindings = { "publisherId", "staffListId" })
@Output(keys = "staffListItemId", bindings = "staffListItem.id")
public class Model
{
    private Long _publisherId;
    private Long _staffListId;
    private StaffListItem _staffListItem;
    private StaffList _staffList;
    
    private DynamicListDataSource<StaffListPostPayment> _dataSource;

    public Long getPublisherId()
    {
        return _publisherId;
    }

    public void setPublisherId(Long publisherId)
    {
        this._publisherId = publisherId;
    }

    public Long getStaffListId()
    {
        return _staffListId;
    }

    public void setStaffListId(Long staffListId)
    {
        this._staffListId = staffListId;
    }

    public StaffListItem getStaffListItem()
    {
        return _staffListItem;
    }

    public void setStaffListItem(StaffListItem staffListItem)
    {
        this._staffListItem = staffListItem;
    }

    public StaffList getStaffList()
    {
        return _staffList;
    }

    public void setStaffList(StaffList staffList)
    {
        this._staffList = staffList;
    }

    public DynamicListDataSource<StaffListPostPayment> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<StaffListPostPayment> dataSource)
    {
        this._dataSource = dataSource;
    }
    
    public String getAddPaymentKey()
    {
        return "addStaffListPostPayment";
    }
    
    public String getEditKey()
    {
        return "editStaffListItem";
    }

    public String getDelKey()
    {
        return "deleteStaffListItem";
    }
}