/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffListItem.StaffListItemAddEdit;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author dseleznev
 * Created on: 19.09.2008
 */
public interface IDAO extends IUniDao<Model>
{
}