/**
 *$Id:$
 */
package ru.tandemservice.uniemp.component.catalog.postBoundedWithQGandQL.PostBoundedWithQGandQLAddEdit;

import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.component.catalog.postBoundedWithQGandQL.PostBoundedWithQGandQLAddEdit.Model;
import org.tandemframework.tapsupport.component.selection.CommonMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.catalog.ServiceLengthType;
import ru.tandemservice.uniemp.entity.employee.PostToServiceLengthTypeRelation;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 17.09.12
 */
public class DAO extends org.tandemframework.shared.employeebase.component.catalog.postBoundedWithQGandQL.PostBoundedWithQGandQLAddEdit.DAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        ru.tandemservice.uniemp.component.catalog.postBoundedWithQGandQL.PostBoundedWithQGandQLAddEdit.Model currentModel = (ru.tandemservice.uniemp.component.catalog.postBoundedWithQGandQL.PostBoundedWithQGandQLAddEdit.Model) model;

        if (model.isEditForm())
        {
            List<ServiceLengthType> serviceLengthTypeList = new DQLSelectBuilder().fromEntity(PostToServiceLengthTypeRelation.class, "b").column(property(PostToServiceLengthTypeRelation.serviceLengthType().fromAlias("b")))
                    .where(eq(property(PostToServiceLengthTypeRelation.postBoundedWithQGandQL().fromAlias("b")), value(model.getCatalogItem())))
                    .createStatement(getSession()).list();

            currentModel.setServiceLengthTypeList(serviceLengthTypeList);
        }
        else
        {
            List<ServiceLengthType> serviceLengthTypeList = new DQLSelectBuilder().fromEntity(ServiceLengthType.class, "b").column("b")
                    .where(in(property(ServiceLengthType.code().fromAlias("b")), Arrays.asList(UniempDefines.SERVICE_LENGTH_TYPE_COMMON, UniempDefines.SERVICE_LENGTH_TYPE_COMPANY)))
                    .createStatement(getSession()).list();

            currentModel.setServiceLengthTypeList(serviceLengthTypeList);
        }

        currentModel.setServiceLengthTypeModel(new CommonMultiSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(ServiceLengthType.class, "b").column("b")
                        .where(likeUpper(property(ServiceLengthType.title().fromAlias("b")), value(CoreStringUtils.escapeLike(filter, true))))
                        .order(property(ServiceLengthType.title().fromAlias("b")));

                if (set != null)
                    builder.where(in(property(ServiceLengthType.id().fromAlias("b")), set));

                return new DQLListResultBuilder(builder);
            }
        });
    }

    @Override
    public void update(Model model)
    {
        super.update(model);

        ru.tandemservice.uniemp.component.catalog.postBoundedWithQGandQL.PostBoundedWithQGandQLAddEdit.Model currentModel = (ru.tandemservice.uniemp.component.catalog.postBoundedWithQGandQL.PostBoundedWithQGandQLAddEdit.Model) model;

        if (model.isEditForm())
        {
            new DQLDeleteBuilder(PostToServiceLengthTypeRelation.class)
                    .where(eq(property(PostToServiceLengthTypeRelation.postBoundedWithQGandQL()), value(model.getCatalogItem())))
                    .createStatement(getSession()).execute();
        }

        if (currentModel.getServiceLengthTypeList() != null && !currentModel.getServiceLengthTypeList().isEmpty())
        {
            for (ServiceLengthType type : currentModel.getServiceLengthTypeList())
            {
                PostToServiceLengthTypeRelation relation = new PostToServiceLengthTypeRelation();
                relation.setPostBoundedWithQGandQL(model.getCatalogItem());
                relation.setServiceLengthType(type);

                save(relation);
            }
        }
    }
}
