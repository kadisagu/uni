/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.OrgUnitStaffListTab;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author dseleznev
 * Created on: 16.09.2008
 */
public interface IDAO extends IUniDao<Model>
{
    void prepareStaffListDataSource(Model model);
}