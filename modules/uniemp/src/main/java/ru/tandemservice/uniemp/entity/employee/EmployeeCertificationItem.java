package ru.tandemservice.uniemp.entity.employee;

import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.uniemp.entity.employee.gen.EmployeeCertificationItemGen;

/**
 * Факт аттестации сотрудника
 */
public class EmployeeCertificationItem extends EmployeeCertificationItemGen
{
    public static String P_BASICS = "basic";

    public static String P_RESOLUTION_COMMISSION = "resolutionCommission";

    /**
     * @return Строку Основание в формате "Приказ №<tt><b>basicOrderNumber</tt></b> от <tt><b>basicOrderDate</tt></b>".
     */
    public String getBasic()
    {
        StringBuilder builder = new StringBuilder();
        if (getBasicOrderNumber() != null || getBasicOrderDate() != null)
            builder.append("Приказ");
        if (getBasicOrderNumber() != null)
            builder.append(" №").append(getBasicOrderNumber());
        if (getBasicOrderDate() != null)
            builder.append(" от ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getBasicOrderDate()));

        return builder.toString();
    }

    /**
     * @return Строку Решение комиссиии в формате "<tt><b>resolution</tt></b> [; <tt><b>commissionComments</tt></b>]".
     */
    public String getResolutionCommission()
    {
        StringBuilder builder = new StringBuilder();
        builder.append(getResolution().getTitle());
        if (getCommissionComments() != null)
            builder.append("; ").append(getCommissionComments());

        return builder.toString();
    }
}