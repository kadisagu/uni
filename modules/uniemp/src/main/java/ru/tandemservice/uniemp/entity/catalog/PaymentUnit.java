package ru.tandemservice.uniemp.entity.catalog;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;

import ru.tandemservice.uniemp.entity.catalog.gen.PaymentUnitGen;

public class PaymentUnit extends PaymentUnitGen implements ITitledWithCases, ITitledWithPluralCases
{
    @Override
    public String getCaseTitle(GrammaCase caze)
    {
        return TitledWithCasesUtil.getCaseTitle(this, caze);
    }
}