/* $Id$ */
package ru.tandemservice.uniemp.entity.catalog;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;

/**
 * @author esych
 * Created on: 20.01.2011
 */
public interface ITitledWithPluralCases extends ITitled
{
    String P_PLURAL_NOMINATIVE_CASE_TITLE = "pluralNominativeCaseTitle";
    String P_PLURAL_GENITIVE_CASE_TITLE = "pluralGenitiveCaseTitle";
    String P_PLURAL_DATIVE_CASE_TITLE = "pluralDativeCaseTitle";
    String P_PLURAL_ACCUSATIVE_CASE_TITLE = "pluralAccusativeCaseTitle";
    String P_PLURAL_INSTRUMENTAL_CASE_TITLE = "pluralInstrumentalCaseTitle";
    String P_PLURAL_PREPOSITIONAL_CASE_TITLE = "pluralPrepositionalCaseTitle";

    String getPluralNominativeCaseTitle();
    void setPluralNominativeCaseTitle(String nominativeCaseTitle);

    String getPluralGenitiveCaseTitle();
    void setPluralGenitiveCaseTitle(String genitiveCaseTitle);

    String getPluralDativeCaseTitle();
    void setPluralDativeCaseTitle(String dativeCaseTitle);

    String getPluralAccusativeCaseTitle();
    void setPluralAccusativeCaseTitle(String accusativeCaseTitle);

    String getPluralInstrumentalCaseTitle();
    void setPluralInstrumentalCaseTitle(String instrumentalCaseTitle);

    String getPluralPrepositionalCaseTitle();
    void setPluralPrepositionalCaseTitle(String prepositionalCaseTitle);

    String getCaseTitle(GrammaCase caze);
}
