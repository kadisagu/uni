/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.vacationShedule.OrgUnitVacationSchedulesList;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniemp.entity.employee.VacationSchedule;

/**
 * @author dseleznev
 * Created on: 05.01.2011
 */
@Input( { @Bind(key = "orgUnitId", binding = "orgUnitId") })
public class Model
{
    private Long _orgUnitId;
    private OrgUnit _orgUnit;
    private IDataSettings _settings;

    private DynamicListDataSource<VacationSchedule> _dataSource;

    public String getAddVacationScheduleKey()
    {
        return "orgUnit_addVacationSchedule_" + getOrgUnit().getOrgUnitType().getCode();
    }

    public String getPrintVacationScheduleKey()
    {
        return "orgUnit_printVacationSchedule_" + getOrgUnit().getOrgUnitType().getCode();
    }

    public String getEditVacationScheduleKey()
    {
        return "orgUnit_editVacationSchedule_" + getOrgUnit().getOrgUnitType().getCode();
    }

    public String getDeleteVacationScheduleKey()
    {
        return "orgUnit_deleteVacationSchedule_" + getOrgUnit().getOrgUnitType().getCode();
    }

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    public DynamicListDataSource<VacationSchedule> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<VacationSchedule> dataSource)
    {
        this._dataSource = dataSource;
    }

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        this._settings = settings;
    }
}