package ru.tandemservice.uniemp.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Ставка сотрудника (связь ставки и сотрудника)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeePostStaffRateItemGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem";
    public static final String ENTITY_NAME = "employeePostStaffRateItem";
    public static final int VERSION_HASH = -693906587;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_POST = "employeePost";
    public static final String P_STAFF_RATE_INTEGER = "staffRateInteger";
    public static final String L_FINANCING_SOURCE = "financingSource";
    public static final String L_FINANCING_SOURCE_ITEM = "financingSourceItem";

    private EmployeePost _employeePost;     // Сотрудник
    private int _staffRateInteger;     // Доля ставки (целое)
    private FinancingSource _financingSource;     // Источник финансирования
    private FinancingSourceItem _financingSourceItem;     // Источник финансирования (детально)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сотрудник. Свойство не может быть null.
     */
    @NotNull
    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    /**
     * @param employeePost Сотрудник. Свойство не может быть null.
     */
    public void setEmployeePost(EmployeePost employeePost)
    {
        dirty(_employeePost, employeePost);
        _employeePost = employeePost;
    }

    /**
     * Хранится со сдвигом на 4 символа дробной части.
     *
     * @return Доля ставки (целое). Свойство не может быть null.
     */
    @NotNull
    public int getStaffRateInteger()
    {
        return _staffRateInteger;
    }

    /**
     * @param staffRateInteger Доля ставки (целое). Свойство не может быть null.
     */
    public void setStaffRateInteger(int staffRateInteger)
    {
        dirty(_staffRateInteger, staffRateInteger);
        _staffRateInteger = staffRateInteger;
    }

    /**
     * @return Источник финансирования. Свойство не может быть null.
     */
    @NotNull
    public FinancingSource getFinancingSource()
    {
        return _financingSource;
    }

    /**
     * @param financingSource Источник финансирования. Свойство не может быть null.
     */
    public void setFinancingSource(FinancingSource financingSource)
    {
        dirty(_financingSource, financingSource);
        _financingSource = financingSource;
    }

    /**
     * @return Источник финансирования (детально).
     */
    public FinancingSourceItem getFinancingSourceItem()
    {
        return _financingSourceItem;
    }

    /**
     * @param financingSourceItem Источник финансирования (детально).
     */
    public void setFinancingSourceItem(FinancingSourceItem financingSourceItem)
    {
        dirty(_financingSourceItem, financingSourceItem);
        _financingSourceItem = financingSourceItem;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EmployeePostStaffRateItemGen)
        {
            setEmployeePost(((EmployeePostStaffRateItem)another).getEmployeePost());
            setStaffRateInteger(((EmployeePostStaffRateItem)another).getStaffRateInteger());
            setFinancingSource(((EmployeePostStaffRateItem)another).getFinancingSource());
            setFinancingSourceItem(((EmployeePostStaffRateItem)another).getFinancingSourceItem());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeePostStaffRateItemGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeePostStaffRateItem.class;
        }

        public T newInstance()
        {
            return (T) new EmployeePostStaffRateItem();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "employeePost":
                    return obj.getEmployeePost();
                case "staffRateInteger":
                    return obj.getStaffRateInteger();
                case "financingSource":
                    return obj.getFinancingSource();
                case "financingSourceItem":
                    return obj.getFinancingSourceItem();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "employeePost":
                    obj.setEmployeePost((EmployeePost) value);
                    return;
                case "staffRateInteger":
                    obj.setStaffRateInteger((Integer) value);
                    return;
                case "financingSource":
                    obj.setFinancingSource((FinancingSource) value);
                    return;
                case "financingSourceItem":
                    obj.setFinancingSourceItem((FinancingSourceItem) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "employeePost":
                        return true;
                case "staffRateInteger":
                        return true;
                case "financingSource":
                        return true;
                case "financingSourceItem":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "employeePost":
                    return true;
                case "staffRateInteger":
                    return true;
                case "financingSource":
                    return true;
                case "financingSourceItem":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "employeePost":
                    return EmployeePost.class;
                case "staffRateInteger":
                    return Integer.class;
                case "financingSource":
                    return FinancingSource.class;
                case "financingSourceItem":
                    return FinancingSourceItem.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeePostStaffRateItem> _dslPath = new Path<EmployeePostStaffRateItem>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeePostStaffRateItem");
    }
            

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem#getEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> employeePost()
    {
        return _dslPath.employeePost();
    }

    /**
     * Хранится со сдвигом на 4 символа дробной части.
     *
     * @return Доля ставки (целое). Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem#getStaffRateInteger()
     */
    public static PropertyPath<Integer> staffRateInteger()
    {
        return _dslPath.staffRateInteger();
    }

    /**
     * @return Источник финансирования. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem#getFinancingSource()
     */
    public static FinancingSource.Path<FinancingSource> financingSource()
    {
        return _dslPath.financingSource();
    }

    /**
     * @return Источник финансирования (детально).
     * @see ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem#getFinancingSourceItem()
     */
    public static FinancingSourceItem.Path<FinancingSourceItem> financingSourceItem()
    {
        return _dslPath.financingSourceItem();
    }

    public static class Path<E extends EmployeePostStaffRateItem> extends EntityPath<E>
    {
        private EmployeePost.Path<EmployeePost> _employeePost;
        private PropertyPath<Integer> _staffRateInteger;
        private FinancingSource.Path<FinancingSource> _financingSource;
        private FinancingSourceItem.Path<FinancingSourceItem> _financingSourceItem;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem#getEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> employeePost()
        {
            if(_employeePost == null )
                _employeePost = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE_POST, this);
            return _employeePost;
        }

    /**
     * Хранится со сдвигом на 4 символа дробной части.
     *
     * @return Доля ставки (целое). Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem#getStaffRateInteger()
     */
        public PropertyPath<Integer> staffRateInteger()
        {
            if(_staffRateInteger == null )
                _staffRateInteger = new PropertyPath<Integer>(EmployeePostStaffRateItemGen.P_STAFF_RATE_INTEGER, this);
            return _staffRateInteger;
        }

    /**
     * @return Источник финансирования. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem#getFinancingSource()
     */
        public FinancingSource.Path<FinancingSource> financingSource()
        {
            if(_financingSource == null )
                _financingSource = new FinancingSource.Path<FinancingSource>(L_FINANCING_SOURCE, this);
            return _financingSource;
        }

    /**
     * @return Источник финансирования (детально).
     * @see ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem#getFinancingSourceItem()
     */
        public FinancingSourceItem.Path<FinancingSourceItem> financingSourceItem()
        {
            if(_financingSourceItem == null )
                _financingSourceItem = new FinancingSourceItem.Path<FinancingSourceItem>(L_FINANCING_SOURCE_ITEM, this);
            return _financingSourceItem;
        }

        public Class getEntityClass()
        {
            return EmployeePostStaffRateItem.class;
        }

        public String getEntityName()
        {
            return "employeePostStaffRateItem";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
