package ru.tandemservice.uniemp.component.reports.quantityEmployeeByDepartments.quantityEmployeeByDepartmentsList;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.entity.report.QuantityEmpByDepReport;

import java.util.Calendar;
import java.util.Date;

public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("report");

    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(QuantityEmpByDepReport.ENTITY_CLASS, "report");
        Object formingDate = model.getSettings().get("formingDate");
        if (formingDate instanceof Date)
        {
            builder.add(MQExpression.between("report", QuantityEmpByDepReport.P_FORMING_DATE, formingDate, CoreDateUtils.add((Date) formingDate, Calendar.DATE, 1)));
        }

        DynamicListDataSource<IEntity> dataSource = model.getDataSource();
        _orderSettings.applyOrder(builder, dataSource.getEntityOrder());
        UniBaseUtils.createPage(dataSource, builder, getSession());
    }
}
