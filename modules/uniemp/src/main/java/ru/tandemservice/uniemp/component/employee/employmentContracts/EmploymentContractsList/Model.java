/**
 *$Id$
 */
package ru.tandemservice.uniemp.component.employee.employmentContracts.EmploymentContractsList;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.uniemp.entity.catalog.LabourContractType;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Create by ashaburov
 * Date 27.07.11
 */
public class Model
{
    public static String DISABLED_PRINT_PROPERTY = "disabledPrint";

    private Map<Long, Date> _id2TimeMap = new HashMap<>();

    //Фильтры
    private List<IdentifiableWrapper> _deadlineListFilter;    //Содержание фильтра Срок окончания ТД
    private IdentifiableWrapper _deadlineItemFilter;          //Выбранный элемент фильтра Срок окончания ТД
    private String _contractNumberFilter;                     //Фильтр № трудового договора
    private List<LabourContractType> _contractTypeListFilter; //Содержание фильтра Тип трудового договора
    private LabourContractType _contractTypeItemFilter;       //Выбранный элемент фильтра Тип трудового договора
    private Date _dateFromFilter;                             //Фильтр Дата заключения с
    private Date _dateToFilter;                               //Фильтр Дата заключения по
    private Date _endDateFromFilter;                          //Фильтр Дата окончания с
    private Date _endDateToFilter;                            //Фильтр Дата окончания по
    private String _lastNameFilter;                           //Фильтр Фамилия
    private String _firstNameFilter;                          //Фильтр Имя
    private String _middleNameFilter;                         //Фильтр Отчество
    private ISingleSelectModel _orgUnitModelFilter;       //Содержание фильтра Подразделение
    private OrgUnit _orgUnitItemFilter;                       //Выбранный элемент фильтра Подразделение
    private boolean _decompositionOrgUnitFilter;              //Фильтр Учитывать дочерние подразделения
    private ISingleSelectModel _postRelationModelFilter;      //Содержание фильтра Должность
    private PostBoundedWithQGandQL _postRelationItemFilter;  //Выбранный элемент фильтра Должность
    private List<PostType> _postTypeListFilter;               //Содержание фильтра Тип назначения
    private PostType _postTypeItemFilter;                     //Выбранный элемент фильтра Тип назначения
    private boolean _disableEndDateFilter;                    //Доступтость фильтра Дата окончания
    private ISelectModel _archiveModel;                 // Архивность
    private DataWrapper _archiveFilter;               // Архивность
    private IMultiSelectModel _postStatusModel;               // Состояние должности сотрудника
    private List<EmployeePostStatus> _postStatusListFilter;   // Состояние должности сотрудника


    private DynamicListDataSource<EmployeeLabourContract> _dataSource;

    //Getters & Setters

    public ISelectModel getArchiveModel()
    {
        return _archiveModel;
    }

    public void setArchiveModel(ISelectModel archiveModel)
    {
        _archiveModel = archiveModel;
    }

    public DataWrapper getArchiveFilter()
    {
        return _archiveFilter;
    }

    public void setArchiveFilter(DataWrapper archiveFilter)
    {
        _archiveFilter = archiveFilter;
    }

    public IMultiSelectModel getPostStatusModel()
    {
        return _postStatusModel;
    }

    public void setPostStatusModel(IMultiSelectModel postStatusModel)
    {
        _postStatusModel = postStatusModel;
    }

    public List<EmployeePostStatus> getPostStatusListFilter()
    {
        return _postStatusListFilter;
    }

    public void setPostStatusListFilter(List<EmployeePostStatus> postStatusListFilter)
    {
        _postStatusListFilter = postStatusListFilter;
    }

    public Map<Long, Date> getId2TimeMap()
    {
        return _id2TimeMap;
    }

    public void setId2TimeMap(Map<Long, Date> id2TimeMap)
    {
        _id2TimeMap = id2TimeMap;
    }

    public boolean isDisableEndDateFilter()
    {
        return _disableEndDateFilter;
    }

    public void setDisableEndDateFilter(boolean disableEndDateFilter)
    {
        _disableEndDateFilter = disableEndDateFilter;
    }

    public List<PostType> getPostTypeListFilter()
    {
        return _postTypeListFilter;
    }

    public void setPostTypeListFilter(List<PostType> postTypeListFilter)
    {
        _postTypeListFilter = postTypeListFilter;
    }

    public PostType getPostTypeItemFilter()
    {
        return _postTypeItemFilter;
    }

    public void setPostTypeItemFilter(PostType postTypeItemFilter)
    {
        _postTypeItemFilter = postTypeItemFilter;
    }

    public String getLastNameFilter()
    {
        return _lastNameFilter;
    }

    public void setLastNameFilter(String lastNameFilter)
    {
        _lastNameFilter = lastNameFilter;
    }

    public String getFirstNameFilter()
    {
        return _firstNameFilter;
    }

    public void setFirstNameFilter(String firstNameFilter)
    {
        _firstNameFilter = firstNameFilter;
    }

    public String getMiddleNameFilter()
    {
        return _middleNameFilter;
    }

    public void setMiddleNameFilter(String middleNameFilter)
    {
        _middleNameFilter = middleNameFilter;
    }

    public ISingleSelectModel getOrgUnitModelFilter()
    {
        return _orgUnitModelFilter;
    }

    public void setOrgUnitModelFilter(ISingleSelectModel orgUnitModelFilter)
    {
        _orgUnitModelFilter = orgUnitModelFilter;
    }

    public OrgUnit getOrgUnitItemFilter()
    {
        return _orgUnitItemFilter;
    }

    public void setOrgUnitItemFilter(OrgUnit orgUnitItemFilter)
    {
        _orgUnitItemFilter = orgUnitItemFilter;
    }

    public boolean isDecompositionOrgUnitFilter()
    {
        return _decompositionOrgUnitFilter;
    }

    public void setDecompositionOrgUnitFilter(boolean decompositionOrgUnitFilter)
    {
        _decompositionOrgUnitFilter = decompositionOrgUnitFilter;
    }

    public ISingleSelectModel getPostRelationModelFilter()
    {
        return _postRelationModelFilter;
    }

    public void setPostRelationModelFilter(ISingleSelectModel postRelationModelFilter)
    {
        _postRelationModelFilter = postRelationModelFilter;
    }

    public PostBoundedWithQGandQL getPostRelationItemFilter()
    {
        return _postRelationItemFilter;
    }

    public void setPostRelationItemFilter(PostBoundedWithQGandQL postRelationItemFilter)
    {
        _postRelationItemFilter = postRelationItemFilter;
    }

    public Date getEndDateFromFilter()
    {
        return _endDateFromFilter;
    }

    public void setEndDateFromFilter(Date endDateFromFilter)
    {
        _endDateFromFilter = endDateFromFilter;
    }

    public Date getEndDateToFilter()
    {
        return _endDateToFilter;
    }

    public void setEndDateToFilter(Date endDateToFilter)
    {
        _endDateToFilter = endDateToFilter;
    }

    public Date getDateFromFilter()
    {
        return _dateFromFilter;
    }

    public void setDateFromFilter(Date dateFromFilter)
    {
        _dateFromFilter = dateFromFilter;
    }

    public Date getDateToFilter()
    {
        return _dateToFilter;
    }

    public void setDateToFilter(Date dateToFilter)
    {
        _dateToFilter = dateToFilter;
    }

    public List<LabourContractType> getContractTypeListFilter()
    {
        return _contractTypeListFilter;
    }

    public void setContractTypeListFilter(List<LabourContractType> contractTypeListFilter)
    {
        _contractTypeListFilter = contractTypeListFilter;
    }

    public LabourContractType getContractTypeItemFilter()
    {
        return _contractTypeItemFilter;
    }

    public void setContractTypeItemFilter(LabourContractType contractTypeItemFilter)
    {
        _contractTypeItemFilter = contractTypeItemFilter;
    }

    public List<IdentifiableWrapper> getDeadlineListFilter()
    {
        return _deadlineListFilter;
    }

    public void setDeadlineListFilter(List<IdentifiableWrapper> deadlineListFilter)
    {
        _deadlineListFilter = deadlineListFilter;
    }

    public IdentifiableWrapper getDeadlineItemFilter()
    {
        return _deadlineItemFilter;
    }

    public void setDeadlineItemFilter(IdentifiableWrapper deadlineItemFilter)
    {
        _deadlineItemFilter = deadlineItemFilter;
    }

    public String getContractNumberFilter()
    {
        return _contractNumberFilter;
    }

    public void setContractNumberFilter(String contractNumberFilter)
    {
        _contractNumberFilter = contractNumberFilter;
    }

    public DynamicListDataSource<EmployeeLabourContract> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<EmployeeLabourContract> dataSource)
    {
        _dataSource = dataSource;
    }
}
