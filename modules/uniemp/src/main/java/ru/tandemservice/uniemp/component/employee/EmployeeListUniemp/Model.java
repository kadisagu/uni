/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniemp.component.employee.EmployeeListUniemp;

import org.tandemframework.core.component.Output;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;

import java.util.List;

/**
 * @author lefay
 */
@Output(keys = "employeeId", bindings = "rowId")
public class Model
{
    public static final String EMPLOYEE_PROPERTY_CANT_DELETE = "cantDeleteEmployee";

    public static final Long EMPLOYEE_STAFF = 1L;

    private List<PostType> _postTypeList;
    private ISelectModel _yesNoSelectModel;
    private ISelectModel _postStatusList;
    private DynamicListDataSource<EmployeeListWrapper> _dataSource;
    
    private ISelectModel _orgUnitList;
    private ISelectModel _employeePostList;
    private ISingleSelectModel _combinationPostStatusModel;
    private ISingleSelectModel _combinationPostTypeModel;

    private IDataSettings _settings;

    private String _selectedPage;
    private Long _rowId;
    
    //Getters & Setters

    public ISingleSelectModel getCombinationPostStatusModel()
    {
        return _combinationPostStatusModel;
    }

    public void setCombinationPostStatusModel(ISingleSelectModel combinationPostStatusModel)
    {
        _combinationPostStatusModel = combinationPostStatusModel;
    }

    public ISingleSelectModel getCombinationPostTypeModel()
    {
        return _combinationPostTypeModel;
    }

    public void setCombinationPostTypeModel(ISingleSelectModel combinationPostTypeModel)
    {
        _combinationPostTypeModel = combinationPostTypeModel;
    }
    
    public DynamicListDataSource<EmployeeListWrapper> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<EmployeeListWrapper> dataSource)
    {
        _dataSource = dataSource;
    }

    public List<PostType> getPostTypeList()
    {
        return _postTypeList;
    }

    public void setPostTypeList(List<PostType> postTypeList)
    {
        _postTypeList = postTypeList;
    }

    public ISelectModel getPostStatusList()
    {
        return _postStatusList;
    }

    public void setPostStatusList(ISelectModel postStatusList)
    {
        _postStatusList = postStatusList;
    }

    public String getSelectedPage()
    {
        return _selectedPage;
    }

    public void setSelectedPage(String selectedPage)
    {
        _selectedPage = selectedPage;
    }

    public Long getRowId()
    {
        return _rowId;
    }

    public void setRowId(Long rowId)
    {
        _rowId = rowId;
    }

    public ISelectModel getEmployeePostList()
    {
        return _employeePostList;
    }

    public void setEmployeePostList(ISelectModel employeePostList)
    {
        this._employeePostList = employeePostList;
    }

    public ISelectModel getOrgUnitList()
    {
        return _orgUnitList;
    }

    public void setOrgUnitList(ISelectModel orgUnitList)
    {
        this._orgUnitList = orgUnitList;
    }

    public IDataSettings getSettings() {
        return _settings;
    }

    public void setSettings(IDataSettings _settings) {
        this._settings = _settings;
    }

    public ISelectModel getYesNoSelectModel()
    {
        return _yesNoSelectModel;
    }

    public void setYesNoSelectModel(ISelectModel yesNoSelectModel)
    {
        this._yesNoSelectModel = yesNoSelectModel;
    }
}