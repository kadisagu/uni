/**
 *$Id:$
 */
package ru.tandemservice.uniemp.base.bo.EmpMedicalStuffReport.ui.Detail;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.RowCollectionFormatter;
import org.tandemframework.core.view.formatter.RawFormatter;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import ru.tandemservice.uniemp.base.bo.EmpMedicalStuffReport.logic.EmpMedicalStuffReportDetailIgnoreSearchDSHandler;
import ru.tandemservice.uniemp.base.bo.EmpMedicalStuffReport.logic.EmpMedicalStuffReportDetailSearchDSHandler;

/**
 * @author Alexander Shaburov
 * @since 30.10.12
 */
@Configuration
public class EmpMedicalStuffReportDetail extends BusinessComponentManager
{
    public static final String EMPLOYEE_POST_SEARCH_DS = "employeePostSearchDS";
    public static final String IGNORE_EMPLOYEE_POST_SEARCH_DS = "ignoreEmployeePostSearchDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(EMPLOYEE_POST_SEARCH_DS, columnSearchDSHandler(), employeePostSearchDSHandler()))
                .addDataSource(searchListDS(IGNORE_EMPLOYEE_POST_SEARCH_DS, columnIgnoreSearchDSHandler(), ignoreEmployeePostSearchDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint columnSearchDSHandler()
    {
        IMergeRowIdResolver mergeResolver = entity -> {
            DataWrapper employeePost = (DataWrapper) entity;
            return employeePost.getProperty("employeeId").toString();
        };
        return columnListExtPointBuilder(EMPLOYEE_POST_SEARCH_DS)
                .addColumn(textColumn("employeeCode", "employeeCode").merger(mergeResolver).order())
                .addColumn(textColumn("title", "title").merger(mergeResolver).order())
                .addColumn(dateColumn("assignDate", "assignDate").merger(mergeResolver).order())
                .addColumn(textColumn("category", "category").merger(mergeResolver).order())
                .addColumn(dateColumn("categoryAssignDate", "categoryAssignDate").merger(mergeResolver).order())
                .addColumn(indicatorColumn("certificate", "certificate")
                        .addIndicator(true, new IndicatorColumn.Item("yes"))
                        .addIndicator(false, new IndicatorColumn.Item("no")))
                .addColumn(textColumn("post", "post").order())
                .addColumn(textColumn("orgUnit", "orgUnit").order())
                .addColumn(textColumn("postType", "postType").order())
                .create();
    }

    @Bean
    public ColumnListExtPoint columnIgnoreSearchDSHandler()
    {
        IMergeRowIdResolver mergeResolver = entity -> {
            DataWrapper employeePost = (DataWrapper) entity;
            return employeePost.getProperty("employeeId").toString();
        };
        return columnListExtPointBuilder(IGNORE_EMPLOYEE_POST_SEARCH_DS)
                .addColumn(textColumn("employeeCode", "employeeCode").merger(mergeResolver).order())
                .addColumn(textColumn("title", "title").merger(mergeResolver).order())
                .addColumn(textColumn("medSpec", "medSpec").formatter(new RowCollectionFormatter(RawFormatter.INSTANCE)).merger(mergeResolver).order())
                .addColumn(textColumn("post", "post").order())
                .addColumn(textColumn("orgUnit", "orgUnit").order())
                .addColumn(textColumn("postType", "postType").order())
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> employeePostSearchDSHandler()
    {
        return new EmpMedicalStuffReportDetailSearchDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> ignoreEmployeePostSearchDSHandler()
    {
        return new EmpMedicalStuffReportDetailIgnoreSearchDSHandler(getName());
    }
}
