/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.CombinationPostTab.block.combinationPostData;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.Post;
import org.tandemframework.shared.employeebase.catalog.entity.ProfQualificationGroup;
import org.tandemframework.shared.employeebase.catalog.entity.QualificationLevel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniemp.entity.catalog.CombinationPostType;
import ru.tandemservice.uniemp.entity.employee.CombinationPost;

import java.util.List;

/**
 * @author Vasily Zhukov
 * @since 12.04.2012
 */
public class CombinationPostDataBlock
{
    // names
    public static final String EMPLOYEE_TYPE_DS = "cpEmployeeTypeDS";
    public static final String POST_DS = "cpPostDS";
    public static final String ORG_UNIT_DS = "cpOrgUnitDS";
    public static final String QUALIFICATION_LEVEL_DS = "cpQualificationLevelDS";
    public static final String PROF_QUALIFICATION_GROUP_DS = "cpProfQualificationGroupDS";
    public static final String COMBINATION_POST_TYPE_DS = "cpTypeDS";
    public static final String COMBINATION_POST_STATUS_DS = "cpPostStatusDS";

    // datasource parameter names
    public static final String PARAM_EMPLOYEE_TYPE_LIST = "employeeTypeList";

    public static void onBeforeDataSourceFetch(IUIDataSource dataSource, CombinationPostDataParam param)
    {
        if (POST_DS.equals(dataSource.getName()))
            param.getCpEmployeeType().putParamIfActive(dataSource, PARAM_EMPLOYEE_TYPE_LIST);
    }

    public static IDefaultComboDataSourceHandler createEmployeeTypeDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, EmployeeType.class)
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);

                DQLSelectBuilder b1 = new DQLSelectBuilder().fromEntity(EmployeePost.class, "ep")
                        .column(DQLExpressions.property(EmployeePost.postRelation().postBoundedWithQGandQL().post().employeeType().id().fromAlias("ep")));

                DQLSelectBuilder b2 = new DQLSelectBuilder().fromEntity(CombinationPost.class, "cp")
                        .column(DQLExpressions.property(CombinationPost.postBoundedWithQGandQL().post().employeeType().id().fromAlias("cp")));

                ep.dqlBuilder.where(DQLExpressions.or(
                        DQLExpressions.in(DQLExpressions.property(EmployeeType.id().fromAlias("e")), b1.buildQuery()),
                        DQLExpressions.in(DQLExpressions.property(EmployeeType.id().fromAlias("e")), b2.buildQuery())
                ));
            }
        };
    }

    public static IDefaultComboDataSourceHandler createPostDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, Post.class)
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);

                List<EmployeeType> employeeTypeList = ep.context.get(PARAM_EMPLOYEE_TYPE_LIST);

                if (employeeTypeList != null)
                    ep.dqlBuilder.where(DQLExpressions.in(DQLExpressions.property(Post.employeeType().fromAlias("e")), employeeTypeList));
            }
        };
    }

    public static IDefaultComboDataSourceHandler createOrgUnitDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, OrgUnit.class)
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);

                DQLSelectBuilder b1 = new DQLSelectBuilder().fromEntity(EmployeePost.class, "ep").column(DQLExpressions.property(EmployeePost.orgUnit().id().fromAlias("ep")));
                DQLSelectBuilder b2 = new DQLSelectBuilder().fromEntity(EmployeePost.class, "cp").column(DQLExpressions.property(EmployeePost.orgUnit().id().fromAlias("cp")));

                ep.dqlBuilder.where(DQLExpressions.or(
                        DQLExpressions.in(DQLExpressions.property(OrgUnit.id().fromAlias("e")), b1.buildQuery()),
                        DQLExpressions.in(DQLExpressions.property(OrgUnit.id().fromAlias("e")), b2.buildQuery())
                ));
            }
        };
    }

    public static IDefaultComboDataSourceHandler createQualificationLevelDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, QualificationLevel.class)
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);

                DQLSelectBuilder b1 = new DQLSelectBuilder().fromEntity(EmployeePost.class, "ep").column(DQLExpressions.property(EmployeePost.postRelation().postBoundedWithQGandQL().qualificationLevel().id().fromAlias("ep")));
                DQLSelectBuilder b2 = new DQLSelectBuilder().fromEntity(CombinationPost.class, "cp").column(DQLExpressions.property(CombinationPost.postBoundedWithQGandQL().qualificationLevel().id().fromAlias("cp")));

                ep.dqlBuilder.where(DQLExpressions.or(
                        DQLExpressions.in(DQLExpressions.property(QualificationLevel.id().fromAlias("e")), b1.buildQuery()),
                        DQLExpressions.in(DQLExpressions.property(QualificationLevel.id().fromAlias("e")), b2.buildQuery())
                ));
            }
        };
    }

    public static IDefaultComboDataSourceHandler createProfQualificationGroupDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, ProfQualificationGroup.class)
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);

                DQLSelectBuilder b1 = new DQLSelectBuilder().fromEntity(EmployeePost.class, "ep").column(DQLExpressions.property(EmployeePost.postRelation().postBoundedWithQGandQL().profQualificationGroup().id().fromAlias("ep")));
                DQLSelectBuilder b2 = new DQLSelectBuilder().fromEntity(CombinationPost.class, "cp").column(DQLExpressions.property(CombinationPost.postBoundedWithQGandQL().profQualificationGroup().id().fromAlias("cp")));

                ep.dqlBuilder.where(DQLExpressions.or(
                        DQLExpressions.in(DQLExpressions.property(ProfQualificationGroup.id().fromAlias("e")), b1.buildQuery()),
                        DQLExpressions.in(DQLExpressions.property(ProfQualificationGroup.id().fromAlias("e")), b2.buildQuery())
                ));
            }
        };
    }

    public static IDefaultComboDataSourceHandler createCombinationPostTypeDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, CombinationPostType.class);
    }

    public static IDefaultComboDataSourceHandler createCombinationPostStatusDS(String name)
    {
        return new TwinComboDataSourceHandler(name).yesTitle("активные").noTitle("не активные");
    }
}
