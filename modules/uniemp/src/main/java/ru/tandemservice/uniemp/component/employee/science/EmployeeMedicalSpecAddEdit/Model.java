/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.science.EmployeeMedicalSpecAddEdit;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uniemp.entity.employee.EmployeeMedicalSpeciality;

/**
 * @author dseleznev
 * Created on: 21.10.2010
 */
@Input(keys = { "employeeMedicalSpecialityId", "employeeId" }, bindings = { "employeeMedicalSpecialityId", "employeeId" })
public class Model
{
    private IUploadFile _certificateFile;

    private boolean _fileUploadVisible = false;
    private boolean _deleteVisible = false;


    private Long _employeeId;
    private Long _employeeMedicalSpecialityId;
    private EmployeeMedicalSpeciality _employeeMedicalSpeciality;

    private ISelectModel _medicalEducationLevelListModel;
    private ISelectModel _medicalQualificationCategoryListModel;

    private boolean _categoryRelatedFieldsVisible;


    //Getters & Setters

    public boolean isDeleteVisible()
    {
        return _deleteVisible;
    }

    public void setDeleteVisible(boolean deleteVisible)
    {
        _deleteVisible = deleteVisible;
    }

    public boolean isFileUploadVisible()
    {
        return _fileUploadVisible;
    }

    public void setFileUploadVisible(boolean fileUploadVisible)
    {
        _fileUploadVisible = fileUploadVisible;
    }

    public IUploadFile getCertificateFile()
    {
        return _certificateFile;
    }

    public void setCertificateFile(IUploadFile certificateFile)
    {
        _certificateFile = certificateFile;
    }

    public Long getEmployeeId()
    {
        return _employeeId;
    }

    public void setEmployeeId(Long employeeId)
    {
        this._employeeId = employeeId;
    }

    public Long getEmployeeMedicalSpecialityId()
    {
        return _employeeMedicalSpecialityId;
    }

    public void setEmployeeMedicalSpecialityId(Long employeeMedicalSpecialityId)
    {
        this._employeeMedicalSpecialityId = employeeMedicalSpecialityId;
    }

    public EmployeeMedicalSpeciality getEmployeeMedicalSpeciality()
    {
        return _employeeMedicalSpeciality;
    }

    public void setEmployeeMedicalSpeciality(EmployeeMedicalSpeciality employeeMedicalSpeciality)
    {
        this._employeeMedicalSpeciality = employeeMedicalSpeciality;
    }

    public ISelectModel getMedicalEducationLevelListModel()
    {
        return _medicalEducationLevelListModel;
    }

    public void setMedicalEducationLevelListModel(ISelectModel medicalEducationLevelListModel)
    {
        this._medicalEducationLevelListModel = medicalEducationLevelListModel;
    }

    public ISelectModel getMedicalQualificationCategoryListModel()
    {
        return _medicalQualificationCategoryListModel;
    }

    public void setMedicalQualificationCategoryListModel(ISelectModel medicalQualificationCategoryListModel)
    {
        this._medicalQualificationCategoryListModel = medicalQualificationCategoryListModel;
    }

    public boolean isCategoryRelatedFieldsVisible()
    {
        return _categoryRelatedFieldsVisible;
    }

    public void setCategoryRelatedFieldsVisible(boolean categoryRelatedFieldsVisible)
    {
        this._categoryRelatedFieldsVisible = categoryRelatedFieldsVisible;
    }
}