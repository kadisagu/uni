/**
 *$Id$
 */
package ru.tandemservice.uniemp.component.employee.EmployeeActingItemAddEdit;

import org.tandemframework.common.component.selection.MQListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.CommonSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uniemp.dao.UniempDAO;
import ru.tandemservice.uniemp.entity.employee.EmployeeActingItem;

/**
 * Create by ashaburov
 * Date 03.11.11
 */
public class DAO extends UniempDAO<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        if (model.getEntityId() != null)
            model.setEntity(get(EmployeeActingItem.class, model.getEntityId()));
        else
            model.setEntity(new EmployeeActingItem());

        if (model.getEmployeePostId() != null)
            model.setEmployeePost(get(EmployeePost.class, model.getEmployeePostId()));

        model.setEmployeePostModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                MQBuilder builder = new MQBuilder(EmployeePost.ENTITY_CLASS, "b");
                builder.add(MQExpression.eq("b", EmployeePost.employee().archival().s(), Boolean.FALSE));
                String[] filters = filter.trim().split(" ");
                if(filters.length > 0)
                {
                    builder.add(MQExpression.like("b", EmployeePost.person().identityCard().lastName().s(),
                            CoreStringUtils.escapeLike(filters[0])));
                    if(filters.length >= 2)
                    {
                        builder.add(MQExpression.like("b", EmployeePost.person().identityCard().firstName().s(),
                            CoreStringUtils.escapeLike(filters[1])));
                    }
                }
                if (o != null)
                    builder.add(MQExpression.eq("b", EmployeePost.id().s(), o));
                builder.addOrder("b", EmployeePost.person().identityCard().lastName().s());
                builder.addOrder("b", EmployeePost.person().identityCard().firstName().s());
                builder.addOrder("b", EmployeePost.person().identityCard().middleName().s());

                return new MQListResultBuilder(builder);
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((EmployeePost) value).getFullTitle();
            }
        });

        model.setOrgUnitModel(new BaseSingleSelectModel()
        {
            @Override
            public Object getValue(Object primaryKey)
            {
                OrgUnit orgUnit = get(OrgUnit.class, (Long) primaryKey);
                if (findValues("").getObjects().contains(orgUnit))
                    return orgUnit;
                else
                    return null;
            }

            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "b");
                builder.add(MQExpression.eq("b", OrgUnit.archival().s(), Boolean.FALSE));
                builder.add(MQExpression.like("b", OrgUnit.title().s(), CoreStringUtils.escapeLike(filter)));
                builder.addOrder("b", OrgUnit.title().s());

                return new ListResult<>(builder.<OrgUnit>getResultList(getSession()));
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((OrgUnit) value).getTitleWithType();
            }
        });

        model.setPostModel(new CommonSingleSelectModel()
        {
            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                PostBoundedWithQGandQL postBounded = (PostBoundedWithQGandQL) value;
                return postBounded.getFullTitleWithSalary();
            }

            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                MQBuilder builder = new MQBuilder(PostBoundedWithQGandQL.ENTITY_CLASS, "b");
                builder.add(MQExpression.like("b", PostBoundedWithQGandQL.title().s(), CoreStringUtils.escapeLike(filter)));
                if (o != null)
                    builder.add(MQExpression.eq("b", PostBoundedWithQGandQL.id().s(), o));
                builder.addOrder("b", PostBoundedWithQGandQL.title().s());

                return new MQListResultBuilder(builder);
            }
        });
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        if (model.getEntity().getEndDate() != null)
            if (model.getEntity().getBeginDate().getTime() > model.getEntity().getEndDate().getTime())
                errors.add("Дата начала периода должна быть меньше даты окончания.", "beginDate", "endDate");
    }

    @Override
    public void update(Model model)
    {
        model.getEntity().setEmployeePost(model.getEmployeePost());

        saveOrUpdate(model.getEntity());
    }
}
