/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.bo.EmpEmployeeRetrainingItem.logic;

import org.tandemframework.hibsupport.dao.CommonDAO;
import ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem;

/**
 * Create by ashaburov
 * Date 28.03.12
 */
public class EmpEmployeeRetrainingItemDAO extends CommonDAO implements IEmpEmployeeRetrainingItemDAO
{
    @Override
    public void saveOrUpdateItem(EmployeeRetrainingItem item)
    {
        saveOrUpdate(item);
    }
}
