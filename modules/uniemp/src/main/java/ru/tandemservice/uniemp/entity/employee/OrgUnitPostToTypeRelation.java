package ru.tandemservice.uniemp.entity.employee;

import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import ru.tandemservice.uniemp.entity.employee.gen.OrgUnitPostToTypeRelationGen;

public class OrgUnitPostToTypeRelation extends OrgUnitPostToTypeRelationGen
{

    public OrgUnitPostToTypeRelation()
    {
    }

    public OrgUnitPostToTypeRelation(OrgUnitPostRelation rel, PostType postType)
    {
        setOrgUnitPostRelation(rel);
        setPostType(postType);
    }

}