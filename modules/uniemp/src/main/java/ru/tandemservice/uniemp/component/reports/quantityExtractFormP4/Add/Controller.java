/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.quantityExtractFormP4.Add;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author dseleznev
 * Created on: 20.04.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{

    @Override
    @SuppressWarnings("unchecked")
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        int currentYear = UniBaseUtils.getCurrentYear();
        List<IdentifiableWrapper> yearsList = new ArrayList<>();
        for (int i = currentYear; i >= currentYear - 20; i--)
            yearsList.add(new IdentifiableWrapper((long) i, String.valueOf(i)));
        model.setYearsList(yearsList);
        model.setYear(model.getYearsList().get(0));

        List<IdentifiableWrapper> monthsList = new ArrayList<>();
        monthsList.add(new IdentifiableWrapper(1L, "Январь"));
        monthsList.add(new IdentifiableWrapper(2L, "Февраль"));
        monthsList.add(new IdentifiableWrapper(3L, "Март"));
        monthsList.add(new IdentifiableWrapper(4L, "Апрель"));
        monthsList.add(new IdentifiableWrapper(5L, "Май"));
        monthsList.add(new IdentifiableWrapper(6L, "Июнь"));
        monthsList.add(new IdentifiableWrapper(7L, "Июль"));
        monthsList.add(new IdentifiableWrapper(8L, "Август"));
        monthsList.add(new IdentifiableWrapper(9L, "Сентябрь"));
        monthsList.add(new IdentifiableWrapper(10L, "Октябрь"));
        monthsList.add(new IdentifiableWrapper(11L, "Ноябрь"));
        monthsList.add(new IdentifiableWrapper(12L, "Декабрь"));
        model.setMonthsList(monthsList);

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        model.setMonth(monthsList.get(cal.get(Calendar.MONTH)));

        if(component.getUserContext().getPrincipalContext() instanceof EmployeePost)
            model.setExecutorId(component.getUserContext().getPrincipalContext().getId());
    }

    public void onClickApply(IBusinessComponent component)
    {
        // переходим на универсальный компонент рендера отчета
        activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new ParametersMap()
        .add("id", getDao().preparePrintReport(getModel(component)))
        ));
    }
}