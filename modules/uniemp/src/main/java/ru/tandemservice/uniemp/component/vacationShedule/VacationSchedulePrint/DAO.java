/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.component.vacationShedule.VacationSchedulePrint;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.base.entity.IPersistentIdentityCard;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.MergeType;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import org.tandemframework.shared.employeebase.catalog.entity.Post;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.entity.catalog.EmployeeTemplateDocument;
import ru.tandemservice.uniemp.entity.employee.VacationSchedule;
import ru.tandemservice.uniemp.entity.employee.VacationScheduleItem;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimv.entity.visa.Visa;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author dseleznev
 * Created on: 26.01.2011
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setVacationSchedule(getNotNull(VacationSchedule.class, model.getVacationScheduleId()));
    }

    @Override
    public RtfDocument print(Model model)
    {
        VacationSchedule vacationSchedule = model.getVacationSchedule();

        ITemplateDocument template = getCatalogItem(EmployeeTemplateDocument.class, "vacationSchedule");
        RtfDocument document = new RtfReader().read(template.getContent());

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        TopOrgUnit academy = TopOrgUnit.getInstance();
        injectModifier.put("academyShortTitle", academy.getShortTitle());
        injectModifier.put("academyTitle", null != academy.getNominativeCaseTitle() ? academy.getNominativeCaseTitle() : academy.getFullTitle());
        injectModifier.put("organizationPrintTitle", null != academy.getNominativeCaseTitle() ? academy.getNominativeCaseTitle() : academy.getFullTitle());

        injectModifier.put("printDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));
        injectModifier.put("okpo", getNotNullString(TopOrgUnit.getInstance(true).getOkpo()));

        injectModifier.put("crDay", RussianDateFormatUtils.getDayString(vacationSchedule.getCreateDate(), true));
        injectModifier.put("crMonthStr", RussianDateFormatUtils.getMonthName(vacationSchedule.getCreateDate(), false));
        injectModifier.put("crYr", RussianDateFormatUtils.getYearString(vacationSchedule.getCreateDate(), true));

        injectModifier.put("scheduleNumber", getNotNullString(vacationSchedule.getNumber()));
        injectModifier.put("scheduleDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(vacationSchedule.getCreateDate()));
        injectModifier.put("scheduleYear", getNotNullString(vacationSchedule.getYear()));

        if(null != vacationSchedule.getLabUnionOpinionDate())
        {
            injectModifier.put("luDay", RussianDateFormatUtils.getDayString(vacationSchedule.getLabUnionOpinionDate(), true));
            injectModifier.put("luMonthStr", RussianDateFormatUtils.getMonthName(vacationSchedule.getLabUnionOpinionDate(), false));
            injectModifier.put("luYr", RussianDateFormatUtils.getYearString(vacationSchedule.getLabUnionOpinionDate(), true));
        }
        else
        {
            putEmptyValues(injectModifier, "luDay", "luMonthStr", "luYr");
        }
        injectModifier.put("luNumber", getNotNullString(vacationSchedule.getLabUnionOpinionNumber()));
        
        /*Visas*/
        putEmptyValues(injectModifier, "rectorFio", "organizationHeadPost", "organizationHeadIof", "orgUnitHeadPost", "orgUnitHeadIof", "personnelDepartmentHeadPost", "personnelDepartmentHeadIof");

        List<Visa> visaList = UnimvDaoFacade.getVisaDao().getVisaList(vacationSchedule);
        if(!visaList.isEmpty())
        {
            if(visaList.size() > 0)
            {
                injectModifier.put("organizationHeadPost", visaList.get(0).getPossibleVisa().getTitle());
                injectModifier.put("organizationHeadIof", getFioStr(visaList.get(0).getPossibleVisa().getEntity().getPerson().getIdentityCard()));
                injectModifier.put("rectorFio", getFioStr(visaList.get(0).getPossibleVisa().getEntity().getPerson().getIdentityCard()));
            }

            if(visaList.size() > 1)
            {
                injectModifier.put("orgUnitHeadPost", visaList.get(1).getPossibleVisa().getTitle());
                injectModifier.put("orgUnitHeadIof", getFioStr(visaList.get(1).getPossibleVisa().getEntity().getPerson().getIdentityCard()));

                MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "ou");
                builder.add(MQExpression.eq("ou", OrgUnit.personnelDepartment().s(), Boolean.TRUE));
                if (builder.getResultCount(getSession()) > 0)
                {
                    OrgUnit personnelDepartmentOrgUnit = builder.<OrgUnit> getResultList(getSession()).get(0);
                    if (null != personnelDepartmentOrgUnit && personnelDepartmentOrgUnit.getId().equals(vacationSchedule.getOrgUnit().getId()))
                    {
                        injectModifier.put("personnelDepartmentHeadPost", visaList.get(1).getPossibleVisa().getTitle());
                        injectModifier.put("personnelDepartmentHeadIof", getFioStr(visaList.get(1).getPossibleVisa().getEntity().getPerson().getIdentityCard()));
                    }
                }
            }

            if(visaList.size() > 2)
            {
                injectModifier.put("personnelDepartmentHeadPost", visaList.get(2).getPossibleVisa().getTitle());
                injectModifier.put("personnelDepartmentHeadIof", getFioStr(visaList.get(2).getPossibleVisa().getEntity().getPerson().getIdentityCard()));
            }
        }

        injectModifier.modify(document);

        
        MQBuilder builder = new MQBuilder(VacationScheduleItem.ENTITY_CLASS, "vsi");
        builder.add(MQExpression.eq("vsi", VacationScheduleItem.vacationSchedule().s(), vacationSchedule));
        builder.addOrder("vsi", VacationScheduleItem.employeePost().postRelation().postBoundedWithQGandQL().post().rank().s());
        builder.addOrder("vsi", VacationScheduleItem.employeePost().postRelation().postBoundedWithQGandQL().post().title().s());
        builder.addOrder("vsi", VacationScheduleItem.employeePost().employee().person().identityCard().lastName().s());
        builder.addOrder("vsi", VacationScheduleItem.employeePost().employee().person().identityCard().firstName().s());
        builder.addOrder("vsi", VacationScheduleItem.employeePost().employee().person().identityCard().middleName().s());
        builder.addOrder("vsi", VacationScheduleItem.planDate().s());
        List<VacationScheduleItem> itemsList = builder.getResultList(getSession());

        Long prevOrgUnitId = null;
        Long prevEmployeePostId = null;
        final List<CellVerticalMergingType> orgUnitMergingStylesList = new ArrayList<>();
        final List<CellVerticalMergingType> employeePostMergingStylesList = new ArrayList<>();

        List<String[]> rowsList = new ArrayList<>();
        for(VacationScheduleItem item : itemsList)
        {
            OrgUnit itemOrgUnit = item.getEmployeePost().getOrgUnit();
            Post post = item.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL().getPost();

            String[] row = new String[10];
            row[0] = null != itemOrgUnit.getNominativeCaseTitle() ? itemOrgUnit.getNominativeCaseTitle() : itemOrgUnit.getFullTitle();
            row[1] = null != post.getNominativeCaseTitle() ? post.getNominativeCaseTitle() : post.getTitle();
            row[2] = item.getEmployeePost().getEmployee().getPerson().getFullFio();
            row[3] = item.getEmployeePost().getEmployee().getEmployeeCode();
            row[4] = String.valueOf(item.getDaysAmount());
            row[5] = getNotNullDateFormatted(item.getPlanDate());
            row[6] = getNotNullDateFormatted(item.getFactDate());
            row[7] = getNotNullString(item.getPostponeBasic());
            row[8] = getNotNullDateFormatted(item.getPostponeDate());
            row[9] = item.getComment();
            rowsList.add(row);

            if(null != prevEmployeePostId)
            {
                if(prevOrgUnitId.equals(itemOrgUnit.getId()))
                {
                    if (orgUnitMergingStylesList.get(orgUnitMergingStylesList.size() - 1).isEmpty())
                        orgUnitMergingStylesList.get(orgUnitMergingStylesList.size() - 1).setFirstCellMergingType();
                    orgUnitMergingStylesList.add(new CellVerticalMergingType(true));
                }
                else
                {
                    prevOrgUnitId = itemOrgUnit.getId();
                    orgUnitMergingStylesList.add(new CellVerticalMergingType());
                }

                if(prevEmployeePostId.equals(item.getEmployeePost().getId()))
                {
                    if(employeePostMergingStylesList.get(employeePostMergingStylesList.size() - 1).isEmpty())
                        employeePostMergingStylesList.get(employeePostMergingStylesList.size() - 1).setFirstCellMergingType();
                    employeePostMergingStylesList.add(new CellVerticalMergingType(true));
                }
                else
                {
                    prevEmployeePostId = item.getEmployeePost().getId();
                    employeePostMergingStylesList.add(new CellVerticalMergingType());
                }
            }
            else
            {
                prevOrgUnitId = itemOrgUnit.getId();
                prevEmployeePostId = item.getEmployeePost().getId();
                orgUnitMergingStylesList.add(new CellVerticalMergingType());
                employeePostMergingStylesList.add(new CellVerticalMergingType());
            }
        }
        
        RtfTableModifier tableModifier = new RtfTableModifier().put("T", rowsList.toArray(new String[rowsList.size()][]));
        
        tableModifier.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                for (int i = startIndex; i < newRowList.size(); i++)
                {
                    RtfRow row = newRowList.get(i);
                    CellVerticalMergingType orgUnitMergingType = orgUnitMergingStylesList.get(i - startIndex);
                    CellVerticalMergingType employeePostMergingType = employeePostMergingStylesList.get(i - startIndex);
                    List<RtfCell> cells = row.getCellList();
                    
                    if(orgUnitMergingType.isFirstCell()) cells.get(0).setMergeType(MergeType.VERTICAL_MERGED_FIRST);
                    else if(orgUnitMergingType.isNextCell()) cells.get(0).setMergeType(MergeType.VERTICAL_MERGED_NEXT);
                    
                    if(employeePostMergingType.isFirstCell())
                    {
                        cells.get(1).setMergeType(MergeType.VERTICAL_MERGED_FIRST);
                        cells.get(2).setMergeType(MergeType.VERTICAL_MERGED_FIRST);
                        cells.get(3).setMergeType(MergeType.VERTICAL_MERGED_FIRST);
                    }
                    else if(employeePostMergingType.isNextCell())
                    {
                        cells.get(1).setMergeType(MergeType.VERTICAL_MERGED_NEXT);
                        cells.get(2).setMergeType(MergeType.VERTICAL_MERGED_NEXT);
                        cells.get(3).setMergeType(MergeType.VERTICAL_MERGED_NEXT);
                    }
                }
            }
        });
        
        tableModifier.modify(document);

        return document;
    }

    private class CellVerticalMergingType
    {
        private boolean _firstCell = false;
        private boolean _nextCell = false;

        public CellVerticalMergingType()
        {
        }

        public CellVerticalMergingType(boolean nextCell)
        {
            _firstCell = !nextCell;
            _nextCell = nextCell;
        }
        
        public boolean isEmpty()
        {
            return !(_firstCell || _nextCell);
        }

        public boolean isFirstCell()
        {
            return _firstCell;
        }

        public boolean isNextCell()
        {
            return _nextCell;
        }

        public void setFirstCellMergingType()
        {
            _firstCell = true;
            _nextCell = false;
        }
    }

    private String getNotNullString(Object str)
    {
        if (null == str) return "";
        else if (str instanceof String) return (String)str;
        else if (str instanceof Number) return String.valueOf(str);
        return null;
    }

    private String getFioStr(IPersistentIdentityCard identityCard)
    {
        String lastName = identityCard.getLastName();
        String firstName = identityCard.getFirstName();
        String middleName = identityCard.getMiddleName();

        StringBuilder str = new StringBuilder();
        if (StringUtils.isNotEmpty(firstName))
            str.append(firstName.substring(0, 1).toUpperCase()).append(".");
        if (StringUtils.isNotEmpty(middleName))
            str.append(middleName.substring(0, 1).toUpperCase()).append(".");
        str.append(" ").append(lastName);
        return str.toString();
    }
    
    private String getNotNullDateFormatted(Date date)
    {
        if(null == date) return "";
        return RussianDateFormatUtils.getDayString(date, true) + "." + RussianDateFormatUtils.getMonthString(date, true) + ".";
    }
    
    private void putEmptyValues(RtfInjectModifier modifier, String...labels)
    {
        for(String label : labels) modifier.put(label, "");
    }
}