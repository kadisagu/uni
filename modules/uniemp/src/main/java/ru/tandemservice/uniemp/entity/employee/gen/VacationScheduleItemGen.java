package ru.tandemservice.uniemp.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uniemp.entity.employee.IChoseRowTransferAnnualHolidayExtract;
import ru.tandemservice.uniemp.entity.employee.VacationSchedule;
import ru.tandemservice.uniemp.entity.employee.VacationScheduleItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Строка графика отпусков
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class VacationScheduleItemGen extends EntityBase
 implements IChoseRowTransferAnnualHolidayExtract{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.employee.VacationScheduleItem";
    public static final String ENTITY_NAME = "vacationScheduleItem";
    public static final int VERSION_HASH = -647554875;
    private static IEntityMeta ENTITY_META;

    public static final String L_VACATION_SCHEDULE = "vacationSchedule";
    public static final String L_EMPLOYEE_POST = "employeePost";
    public static final String P_DAYS_AMOUNT = "daysAmount";
    public static final String P_PLAN_DATE = "planDate";
    public static final String P_FACT_DATE = "factDate";
    public static final String P_POSTPONE_DATE = "postponeDate";
    public static final String P_POSTPONE_BASIC = "postponeBasic";
    public static final String P_COMMENT = "comment";
    public static final String P_PLANED_VACATION_TITLE = "planedVacationTitle";

    private VacationSchedule _vacationSchedule;     // График отпусков
    private EmployeePost _employeePost;     // Сотрудник
    private int _daysAmount;     // Количество календарных дней отпуска
    private Date _planDate;     // Планируемая дата отпуска
    private Date _factDate;     // Фактическая дата отпуска
    private Date _postponeDate;     // Предполагаемая дата переноса отпуска
    private String _postponeBasic;     // Основание для переноса отпуска
    private String _comment;     // Примечание

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return График отпусков. Свойство не может быть null.
     */
    @NotNull
    public VacationSchedule getVacationSchedule()
    {
        return _vacationSchedule;
    }

    /**
     * @param vacationSchedule График отпусков. Свойство не может быть null.
     */
    public void setVacationSchedule(VacationSchedule vacationSchedule)
    {
        dirty(_vacationSchedule, vacationSchedule);
        _vacationSchedule = vacationSchedule;
    }

    /**
     * @return Сотрудник. Свойство не может быть null.
     */
    @NotNull
    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    /**
     * @param employeePost Сотрудник. Свойство не может быть null.
     */
    public void setEmployeePost(EmployeePost employeePost)
    {
        dirty(_employeePost, employeePost);
        _employeePost = employeePost;
    }

    /**
     * @return Количество календарных дней отпуска. Свойство не может быть null.
     */
    @NotNull
    public int getDaysAmount()
    {
        return _daysAmount;
    }

    /**
     * @param daysAmount Количество календарных дней отпуска. Свойство не может быть null.
     */
    public void setDaysAmount(int daysAmount)
    {
        dirty(_daysAmount, daysAmount);
        _daysAmount = daysAmount;
    }

    /**
     * @return Планируемая дата отпуска.
     */
    public Date getPlanDate()
    {
        return _planDate;
    }

    /**
     * @param planDate Планируемая дата отпуска.
     */
    public void setPlanDate(Date planDate)
    {
        dirty(_planDate, planDate);
        _planDate = planDate;
    }

    /**
     * @return Фактическая дата отпуска.
     */
    public Date getFactDate()
    {
        return _factDate;
    }

    /**
     * @param factDate Фактическая дата отпуска.
     */
    public void setFactDate(Date factDate)
    {
        dirty(_factDate, factDate);
        _factDate = factDate;
    }

    /**
     * @return Предполагаемая дата переноса отпуска.
     */
    public Date getPostponeDate()
    {
        return _postponeDate;
    }

    /**
     * @param postponeDate Предполагаемая дата переноса отпуска.
     */
    public void setPostponeDate(Date postponeDate)
    {
        dirty(_postponeDate, postponeDate);
        _postponeDate = postponeDate;
    }

    /**
     * @return Основание для переноса отпуска.
     */
    @Length(max=255)
    public String getPostponeBasic()
    {
        return _postponeBasic;
    }

    /**
     * @param postponeBasic Основание для переноса отпуска.
     */
    public void setPostponeBasic(String postponeBasic)
    {
        dirty(_postponeBasic, postponeBasic);
        _postponeBasic = postponeBasic;
    }

    /**
     * @return Примечание.
     */
    @Length(max=255)
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Примечание.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof VacationScheduleItemGen)
        {
            setVacationSchedule(((VacationScheduleItem)another).getVacationSchedule());
            setEmployeePost(((VacationScheduleItem)another).getEmployeePost());
            setDaysAmount(((VacationScheduleItem)another).getDaysAmount());
            setPlanDate(((VacationScheduleItem)another).getPlanDate());
            setFactDate(((VacationScheduleItem)another).getFactDate());
            setPostponeDate(((VacationScheduleItem)another).getPostponeDate());
            setPostponeBasic(((VacationScheduleItem)another).getPostponeBasic());
            setComment(((VacationScheduleItem)another).getComment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends VacationScheduleItemGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) VacationScheduleItem.class;
        }

        public T newInstance()
        {
            return (T) new VacationScheduleItem();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "vacationSchedule":
                    return obj.getVacationSchedule();
                case "employeePost":
                    return obj.getEmployeePost();
                case "daysAmount":
                    return obj.getDaysAmount();
                case "planDate":
                    return obj.getPlanDate();
                case "factDate":
                    return obj.getFactDate();
                case "postponeDate":
                    return obj.getPostponeDate();
                case "postponeBasic":
                    return obj.getPostponeBasic();
                case "comment":
                    return obj.getComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "vacationSchedule":
                    obj.setVacationSchedule((VacationSchedule) value);
                    return;
                case "employeePost":
                    obj.setEmployeePost((EmployeePost) value);
                    return;
                case "daysAmount":
                    obj.setDaysAmount((Integer) value);
                    return;
                case "planDate":
                    obj.setPlanDate((Date) value);
                    return;
                case "factDate":
                    obj.setFactDate((Date) value);
                    return;
                case "postponeDate":
                    obj.setPostponeDate((Date) value);
                    return;
                case "postponeBasic":
                    obj.setPostponeBasic((String) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "vacationSchedule":
                        return true;
                case "employeePost":
                        return true;
                case "daysAmount":
                        return true;
                case "planDate":
                        return true;
                case "factDate":
                        return true;
                case "postponeDate":
                        return true;
                case "postponeBasic":
                        return true;
                case "comment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "vacationSchedule":
                    return true;
                case "employeePost":
                    return true;
                case "daysAmount":
                    return true;
                case "planDate":
                    return true;
                case "factDate":
                    return true;
                case "postponeDate":
                    return true;
                case "postponeBasic":
                    return true;
                case "comment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "vacationSchedule":
                    return VacationSchedule.class;
                case "employeePost":
                    return EmployeePost.class;
                case "daysAmount":
                    return Integer.class;
                case "planDate":
                    return Date.class;
                case "factDate":
                    return Date.class;
                case "postponeDate":
                    return Date.class;
                case "postponeBasic":
                    return String.class;
                case "comment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<VacationScheduleItem> _dslPath = new Path<VacationScheduleItem>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "VacationScheduleItem");
    }
            

    /**
     * @return График отпусков. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.VacationScheduleItem#getVacationSchedule()
     */
    public static VacationSchedule.Path<VacationSchedule> vacationSchedule()
    {
        return _dslPath.vacationSchedule();
    }

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.VacationScheduleItem#getEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> employeePost()
    {
        return _dslPath.employeePost();
    }

    /**
     * @return Количество календарных дней отпуска. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.VacationScheduleItem#getDaysAmount()
     */
    public static PropertyPath<Integer> daysAmount()
    {
        return _dslPath.daysAmount();
    }

    /**
     * @return Планируемая дата отпуска.
     * @see ru.tandemservice.uniemp.entity.employee.VacationScheduleItem#getPlanDate()
     */
    public static PropertyPath<Date> planDate()
    {
        return _dslPath.planDate();
    }

    /**
     * @return Фактическая дата отпуска.
     * @see ru.tandemservice.uniemp.entity.employee.VacationScheduleItem#getFactDate()
     */
    public static PropertyPath<Date> factDate()
    {
        return _dslPath.factDate();
    }

    /**
     * @return Предполагаемая дата переноса отпуска.
     * @see ru.tandemservice.uniemp.entity.employee.VacationScheduleItem#getPostponeDate()
     */
    public static PropertyPath<Date> postponeDate()
    {
        return _dslPath.postponeDate();
    }

    /**
     * @return Основание для переноса отпуска.
     * @see ru.tandemservice.uniemp.entity.employee.VacationScheduleItem#getPostponeBasic()
     */
    public static PropertyPath<String> postponeBasic()
    {
        return _dslPath.postponeBasic();
    }

    /**
     * @return Примечание.
     * @see ru.tandemservice.uniemp.entity.employee.VacationScheduleItem#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniemp.entity.employee.VacationScheduleItem#getPlanedVacationTitle()
     */
    public static SupportedPropertyPath<String> planedVacationTitle()
    {
        return _dslPath.planedVacationTitle();
    }

    public static class Path<E extends VacationScheduleItem> extends EntityPath<E>
    {
        private VacationSchedule.Path<VacationSchedule> _vacationSchedule;
        private EmployeePost.Path<EmployeePost> _employeePost;
        private PropertyPath<Integer> _daysAmount;
        private PropertyPath<Date> _planDate;
        private PropertyPath<Date> _factDate;
        private PropertyPath<Date> _postponeDate;
        private PropertyPath<String> _postponeBasic;
        private PropertyPath<String> _comment;
        private SupportedPropertyPath<String> _planedVacationTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return График отпусков. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.VacationScheduleItem#getVacationSchedule()
     */
        public VacationSchedule.Path<VacationSchedule> vacationSchedule()
        {
            if(_vacationSchedule == null )
                _vacationSchedule = new VacationSchedule.Path<VacationSchedule>(L_VACATION_SCHEDULE, this);
            return _vacationSchedule;
        }

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.VacationScheduleItem#getEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> employeePost()
        {
            if(_employeePost == null )
                _employeePost = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE_POST, this);
            return _employeePost;
        }

    /**
     * @return Количество календарных дней отпуска. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.VacationScheduleItem#getDaysAmount()
     */
        public PropertyPath<Integer> daysAmount()
        {
            if(_daysAmount == null )
                _daysAmount = new PropertyPath<Integer>(VacationScheduleItemGen.P_DAYS_AMOUNT, this);
            return _daysAmount;
        }

    /**
     * @return Планируемая дата отпуска.
     * @see ru.tandemservice.uniemp.entity.employee.VacationScheduleItem#getPlanDate()
     */
        public PropertyPath<Date> planDate()
        {
            if(_planDate == null )
                _planDate = new PropertyPath<Date>(VacationScheduleItemGen.P_PLAN_DATE, this);
            return _planDate;
        }

    /**
     * @return Фактическая дата отпуска.
     * @see ru.tandemservice.uniemp.entity.employee.VacationScheduleItem#getFactDate()
     */
        public PropertyPath<Date> factDate()
        {
            if(_factDate == null )
                _factDate = new PropertyPath<Date>(VacationScheduleItemGen.P_FACT_DATE, this);
            return _factDate;
        }

    /**
     * @return Предполагаемая дата переноса отпуска.
     * @see ru.tandemservice.uniemp.entity.employee.VacationScheduleItem#getPostponeDate()
     */
        public PropertyPath<Date> postponeDate()
        {
            if(_postponeDate == null )
                _postponeDate = new PropertyPath<Date>(VacationScheduleItemGen.P_POSTPONE_DATE, this);
            return _postponeDate;
        }

    /**
     * @return Основание для переноса отпуска.
     * @see ru.tandemservice.uniemp.entity.employee.VacationScheduleItem#getPostponeBasic()
     */
        public PropertyPath<String> postponeBasic()
        {
            if(_postponeBasic == null )
                _postponeBasic = new PropertyPath<String>(VacationScheduleItemGen.P_POSTPONE_BASIC, this);
            return _postponeBasic;
        }

    /**
     * @return Примечание.
     * @see ru.tandemservice.uniemp.entity.employee.VacationScheduleItem#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(VacationScheduleItemGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniemp.entity.employee.VacationScheduleItem#getPlanedVacationTitle()
     */
        public SupportedPropertyPath<String> planedVacationTitle()
        {
            if(_planedVacationTitle == null )
                _planedVacationTitle = new SupportedPropertyPath<String>(VacationScheduleItemGen.P_PLANED_VACATION_TITLE, this);
            return _planedVacationTitle;
        }

        public Class getEntityClass()
        {
            return VacationScheduleItem.class;
        }

        public String getEntityName()
        {
            return "vacationScheduleItem";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getPlanedVacationTitle();
}
