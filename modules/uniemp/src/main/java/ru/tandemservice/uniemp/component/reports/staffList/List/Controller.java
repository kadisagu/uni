/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.component.reports.staffList.List;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.RawFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.IUniempComponents;
import ru.tandemservice.uniemp.entity.report.StaffListReport;

/**
 * @author dseleznev
 * Created on: 13.08.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getModel(component).setSettings(component.getSettings());
        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null)
        {
            return;
        }

        DynamicListDataSource<StaffListReport> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new IndicatorColumn("Иконка", null).defaultIndicator(new IndicatorColumn.Item("report", "Отчет")).setOrderable(false).setClickable(false).setRequired(true));
        dataSource.addColumn(new SimpleColumn("Дата формирования", StaffListReport.P_FORMING_DATE, DateFormatter.DATE_FORMATTER_WITH_TIME));
        dataSource.addColumn(new SimpleColumn("Дата отчета", StaffListReport.P_REPORT_DATE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Источник финансирования", StaffListReport.P_ACTIVITY_TYPE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Типы подразделений", StaffListReport.P_ORG_UNIT_TYPES_WRAPPED, RawFormatter.INSTANCE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Подразделение", StaffListReport.P_ORG_UNIT).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Исключить подразделение", StaffListReport.P_EXCLUDE_ORG_UNIT).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Тип должности", StaffListReport.P_EMPLOYEE_POST_TYPE_WRAPPED).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Квалификационный уровень", StaffListReport.P_QUALIFICATION_LEVEL).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Профессионально-квалификационная группа", StaffListReport.P_PROF_QUALIFICATION_GROUP).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Должность", StaffListReport.P_POST).setClickable(false).setOrderable(false));
        dataSource.addColumn(new IndicatorColumn("Печать", null, "onClickPrintReport").defaultIndicator(new IndicatorColumn.Item("printer", "Печать")).setImageHeader(false).setDisableSecondSubmit(false).setPermissionKey("printEmployeeStorableReport").setOrderable(false));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteReport", "Удалить отчет от «{0}»?", StaffListReport.P_FORMING_DATE).setPermissionKey("deleteEmployeeStorableReport"));
        model.setDataSource(dataSource);
    }

    public void onClickAddReport(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniempComponents.REPORT_STAFF_LIST_ADD));
    }

    public void onClickPrintReport(IBusinessComponent component) throws Exception
    {
        activateInRoot(component, new ComponentActivator(IUniComponents.DOWNLOAD_STORABLE_REPORT, new ParametersMap().add("reportId", component.getListenerParameter()).add("extension", "rtf")));
    }

    public void onClickDeleteReport(IBusinessComponent component)
    {
        getDao().deleteRow(component);
    }

    public void onClickSearch(IBusinessComponent component)
    {
        component.saveSettings();
        getModel(component).getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component)
    {
        component.getSettings().clear();
        onClickSearch(component);
    }
}