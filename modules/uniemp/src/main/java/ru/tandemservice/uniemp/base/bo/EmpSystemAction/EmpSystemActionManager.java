/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.bo.EmpSystemAction;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniemp.base.bo.EmpSystemAction.logic.EmpSystemActionDao;
import ru.tandemservice.uniemp.base.bo.EmpSystemAction.logic.IEmpSystemActionDao;

/**
 * @author Alexander Shaburov
 * @since 12.02.13
 */
@Configuration
public class EmpSystemActionManager extends BusinessObjectManager
{
    public static EmpSystemActionManager instance()
    {
        return instance(EmpSystemActionManager.class);
    }

    @Bean
    public IEmpSystemActionDao dao()
    {
        return new EmpSystemActionDao();
    }
}
