/**
 *$Id$
 */
package ru.tandemservice.uniemp.component.employee.EmploymentHistoryArchAddEdit;

import org.tandemframework.core.component.Input;

import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import ru.tandemservice.uniemp.component.employee.EmploymentHistoryAbsAddEdit.EmploymentHistoryAbstractModel;
import ru.tandemservice.uniemp.entity.catalog.ServiceLengthType;
import ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemArch;
import ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemBase;

import java.util.List;

/**
 * Create by ashaburov
 * Date 17.08.11
 */
@Input(keys = { "employeeId", "rowId" }, bindings = { "employeeId", "employmentHistoryItem.id" })
public class Model extends EmploymentHistoryAbstractModel
{
    private EmploymentHistoryItemArch _employmentHistoryItem = new EmploymentHistoryItemArch();

    private IMultiSelectModel _serviceLengthTypeModel;
    private List<ServiceLengthType> _serviceLengthTypeList;

    // Getters & Setters

    public IMultiSelectModel getServiceLengthTypeModel()
    {
        return _serviceLengthTypeModel;
    }

    public void setServiceLengthTypeModel(IMultiSelectModel serviceLengthTypeModel)
    {
        _serviceLengthTypeModel = serviceLengthTypeModel;
    }

    public List<ServiceLengthType> getServiceLengthTypeList()
    {
        return _serviceLengthTypeList;
    }

    public void setServiceLengthTypeList(List<ServiceLengthType> serviceLengthTypeList)
    {
        _serviceLengthTypeList = serviceLengthTypeList;
    }

    @Override
    public EmploymentHistoryItemArch getEmploymentHistoryItem()
    {
        return _employmentHistoryItem;
    }

    @Override
    public void setEmploymentHistoryItem(EmploymentHistoryItemBase item)
    {
        this._employmentHistoryItem = (EmploymentHistoryItemArch) item;
    }
}
