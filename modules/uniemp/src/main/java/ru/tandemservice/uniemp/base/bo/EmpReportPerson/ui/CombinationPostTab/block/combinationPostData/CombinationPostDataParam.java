/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.CombinationPostTab.block.combinationPostData;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.entity.IIdentifiableWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.employeebase.base.bo.EmployeeReportPerson.ui.Add.EmployeeReportPersonAddUI;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.Post;
import org.tandemframework.shared.employeebase.catalog.entity.ProfQualificationGroup;
import org.tandemframework.shared.employeebase.catalog.entity.QualificationLevel;
import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import ru.tandemservice.uniemp.entity.catalog.CombinationPostType;
import ru.tandemservice.uniemp.entity.employee.CombinationPost;

import java.util.Date;
import java.util.List;

/**
 * @author Vasily Zhukov
 * @since 12.04.2012
 */
public class CombinationPostDataParam implements IReportDQLModifier
{
    private IReportParam<DataWrapper> _hasCombinationPost = new ReportParam<>();
    private IReportParam<List<EmployeeType>> _cpEmployeeType = new ReportParam<>();
    private IReportParam<List<Post>> _cpPost = new ReportParam<>();
    private IReportParam<List<OrgUnit>> _cpOrgUnit = new ReportParam<>();
    private IReportParam<DataWrapper> _cpOrgUnitDeep = new ReportParam<>();
    private IReportParam<List<QualificationLevel>> _cpQualificationLevel = new ReportParam<>();
    private IReportParam<List<ProfQualificationGroup>> _cpProfQualificationGroup = new ReportParam<>();
    private IReportParam<List<CombinationPostType>> _cpType = new ReportParam<>();
    private IReportParam<DataWrapper> _cpPostStatus = new ReportParam<>();
    private IReportParam<Date> _cpStartDateFrom = new ReportParam<>();
    private IReportParam<Date> _cpStartDateTo = new ReportParam<>();
    private IReportParam<Date> _cpFinishDateFrom = new ReportParam<>();
    private IReportParam<Date> _cpFinishDateTo = new ReportParam<>();

    private String alias(ReportDQL dql)
    {
        // соединяем кадрового ресурса
        String employeeAlias = dql.innerJoinEntity(Employee.class, Employee.person());

        // соединяем сотрудника
        return dql.innerJoinEntity(employeeAlias, EmployeePost.class, EmployeePost.employee());
    }

    private IDQLExpression createExistsCombinationPostExpression(ReportDQL dql, String alias, IDQLExpression cpRestriction)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(CombinationPost.class, alias)
                .where(DQLExpressions.eq(DQLExpressions.property(CombinationPost.employeePost().fromAlias(alias)), DQLExpressions.property(alias(dql))));

        if (cpRestriction != null)
            builder.where(cpRestriction);

        return DQLExpressions.exists(builder.buildQuery());
    }

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {

    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (_hasCombinationPost.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "combinationPostData.hasCombinationPost", _hasCombinationPost.getData().getTitle());

            String cpAlias = dql.nextAlias();

            if (TwinComboDataSourceHandler.getSelectedValueNotNull(_hasCombinationPost.getData()))
                dql.innerJoinEntity(alias(dql), CombinationPost.class, CombinationPost.employeePost());
            else
                dql.builder.where(DQLExpressions.not(createExistsCombinationPostExpression(dql, cpAlias, null)));
        }

        if (_cpEmployeeType.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "combinationPostData.cpEmployeeType", CommonBaseStringUtil.join(_cpEmployeeType.getData(), EmployeeType.P_TITLE, ", "));

            String cpAlias = dql.nextAlias();

            dql.builder.where(DQLExpressions.or(
                    DQLExpressions.in(DQLExpressions.property(EmployeePost.postRelation().postBoundedWithQGandQL().post().employeeType().fromAlias(alias(dql))), _cpEmployeeType.getData()),
                    createExistsCombinationPostExpression(dql, cpAlias, DQLExpressions.in(DQLExpressions.property(CombinationPost.postBoundedWithQGandQL().post().employeeType().fromAlias(cpAlias)), _cpEmployeeType.getData()))
            ));
        }

        if (_cpPost.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "combinationPostData.cpPost", CommonBaseStringUtil.join(_cpPost.getData(), Post.P_TITLE, ", "));

            String cpAlias = dql.nextAlias();

            dql.builder.where(DQLExpressions.or(
                    DQLExpressions.in(DQLExpressions.property(EmployeePost.postRelation().postBoundedWithQGandQL().post().fromAlias(alias(dql))), _cpPost.getData()),
                    createExistsCombinationPostExpression(dql, cpAlias, DQLExpressions.in(DQLExpressions.property(CombinationPost.postBoundedWithQGandQL().post().fromAlias(cpAlias)), _cpPost.getData()))
            ));
        }

        if (_cpOrgUnit.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "combinationPostData.cpOrgUnit", CommonBaseStringUtil.join(_cpOrgUnit.getData(), OrgUnit.P_FULL_TITLE, ", "));

            String cpAlias = dql.nextAlias();

            if (_cpOrgUnitDeep.isActive())
            {
                printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "combinationPostData.cpOrgUnitDeep", _cpOrgUnitDeep.getData().getTitle());

                if (TwinComboDataSourceHandler.getSelectedValueNotNull(_cpOrgUnitDeep.getData()))
                {
                    List<OrgUnit> list = OrgUnitManager.instance().dao().getInnerOrgUnitDeep(_cpOrgUnit.getData());
                    dql.builder.where(DQLExpressions.or(
                            DQLExpressions.in(DQLExpressions.property(EmployeePost.orgUnit().fromAlias(alias(dql))), list),
                            createExistsCombinationPostExpression(dql, cpAlias, DQLExpressions.in(DQLExpressions.property(CombinationPost.orgUnit().fromAlias(cpAlias)), list))
                    ));
                } else
                {
                    dql.builder.where(DQLExpressions.or(
                            DQLExpressions.in(DQLExpressions.property(EmployeePost.orgUnit().fromAlias(alias(dql))), _cpOrgUnit.getData()),
                            createExistsCombinationPostExpression(dql, cpAlias, DQLExpressions.in(DQLExpressions.property(CombinationPost.orgUnit().fromAlias(cpAlias)), _cpOrgUnit.getData()))
                    ));
                }
            } else
            {
                dql.builder.where(DQLExpressions.or(
                        DQLExpressions.in(DQLExpressions.property(EmployeePost.orgUnit().fromAlias(alias(dql))), _cpOrgUnit.getData()),
                        createExistsCombinationPostExpression(dql, cpAlias, DQLExpressions.in(DQLExpressions.property(CombinationPost.orgUnit().fromAlias(cpAlias)), _cpOrgUnit.getData()))
                ));
            }
        }

        if (_cpQualificationLevel.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "combinationPostData.cpQualificationLevel", CommonBaseStringUtil.join(_cpQualificationLevel.getData(), QualificationLevel.P_TITLE, ", "));

            String cpAlias = dql.nextAlias();

            dql.builder.where(DQLExpressions.or(
                    DQLExpressions.in(DQLExpressions.property(EmployeePost.postRelation().postBoundedWithQGandQL().qualificationLevel().fromAlias(alias(dql))), _cpQualificationLevel.getData()),
                    createExistsCombinationPostExpression(dql, cpAlias, DQLExpressions.in(DQLExpressions.property(CombinationPost.postBoundedWithQGandQL().qualificationLevel().fromAlias(cpAlias)), _cpQualificationLevel.getData()))
            ));
        }

        if (_cpProfQualificationGroup.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "combinationPostData.cpProfQualificationGroup", CommonBaseStringUtil.join(_cpProfQualificationGroup.getData(), ProfQualificationGroup.P_TITLE, ", "));

            String cpAlias = dql.nextAlias();

            dql.builder.where(DQLExpressions.or(
                    DQLExpressions.in(DQLExpressions.property(EmployeePost.postRelation().postBoundedWithQGandQL().profQualificationGroup().fromAlias(alias(dql))), _cpProfQualificationGroup.getData()),
                    createExistsCombinationPostExpression(dql, cpAlias, DQLExpressions.in(DQLExpressions.property(CombinationPost.postBoundedWithQGandQL().profQualificationGroup().fromAlias(cpAlias)), _cpProfQualificationGroup.getData()))
            ));
        }

        if (_cpType.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "combinationPostData.cpType", CommonBaseStringUtil.join(_cpType.getData(), CombinationPostType.P_TITLE, ", "));

            String cpAlias = dql.nextAlias();

            dql.builder.where(createExistsCombinationPostExpression(dql, cpAlias, DQLExpressions.in(DQLExpressions.property(CombinationPost.combinationPostType().fromAlias(cpAlias)), _cpType.getData())));
        }

        if (_cpPostStatus.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "combinationPostData.cpPostStatus", _cpPostStatus.getData().getTitle());

            String cpAlias = dql.nextAlias();
            IDQLExpression dateExpr = DQLExpressions.valueDate(CoreDateUtils.getToday());

            if (TwinComboDataSourceHandler.getSelectedValueNotNull(_cpPostStatus.getData()))
                dql.builder.where(createExistsCombinationPostExpression(dql, cpAlias, DQLExpressions.and(
                        DQLExpressions.le(DQLExpressions.property(CombinationPost.beginDate().fromAlias(cpAlias)), dateExpr),
                        DQLExpressions.or(
                                DQLExpressions.isNull(DQLExpressions.property(CombinationPost.endDate().fromAlias(cpAlias))),
                                DQLExpressions.ge(DQLExpressions.property(CombinationPost.endDate().fromAlias(cpAlias)), dateExpr)
                        ))));
            else
                dql.builder.where(createExistsCombinationPostExpression(dql, cpAlias, DQLExpressions.or(
                        DQLExpressions.gt(DQLExpressions.property(CombinationPost.beginDate().fromAlias(cpAlias)), dateExpr),
                        DQLExpressions.lt(DQLExpressions.property(CombinationPost.endDate().fromAlias(cpAlias)), dateExpr)
                )));
        }

        if (_cpStartDateFrom.isActive() || _cpStartDateTo.isActive())
        {
            if (_cpStartDateFrom.isActive())
                printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "combinationPostData.cpStartDateFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(_cpStartDateFrom.getData()));

            if (_cpStartDateTo.isActive())
                printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "combinationPostData.cpStartDateTo", DateFormatter.DEFAULT_DATE_FORMATTER.format(_cpStartDateTo.getData()));

            String cpAlias = dql.nextAlias();

            IDQLExpression fromExpr = _cpStartDateFrom.isActive() ? DQLExpressions.ge(DQLExpressions.property(CombinationPost.beginDate().fromAlias(cpAlias)), DQLExpressions.valueDate(CoreDateUtils.getDayFirstTimeMoment(_cpStartDateFrom.getData()))) : null;
            IDQLExpression toExpr = _cpStartDateTo.isActive() ? DQLExpressions.le(DQLExpressions.property(CombinationPost.beginDate().fromAlias(cpAlias)), DQLExpressions.valueDate(CoreDateUtils.getDayFirstTimeMoment(_cpStartDateTo.getData()))) : null;

            dql.builder.where(createExistsCombinationPostExpression(dql, cpAlias, fromExpr == null ? toExpr : (toExpr == null ? fromExpr : DQLExpressions.and(fromExpr, toExpr))));
        }

        if (_cpFinishDateFrom.isActive() || _cpFinishDateTo.isActive())
        {
            if (_cpFinishDateFrom.isActive())
                printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "combinationPostData.cpFinishDateFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(_cpFinishDateFrom.getData()));

            if (_cpFinishDateTo.isActive())
                printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "combinationPostData.cpFinishDateTo", DateFormatter.DEFAULT_DATE_FORMATTER.format(_cpFinishDateTo.getData()));

            String cpAlias = dql.nextAlias();

            IDQLExpression fromExpr = _cpFinishDateFrom.isActive() ? DQLExpressions.ge(DQLExpressions.property(CombinationPost.endDate().fromAlias(cpAlias)), DQLExpressions.valueDate(CoreDateUtils.getDayFirstTimeMoment(_cpFinishDateFrom.getData()))) : null;
            IDQLExpression toExpr = _cpFinishDateTo.isActive() ? DQLExpressions.le(DQLExpressions.property(CombinationPost.endDate().fromAlias(cpAlias)), DQLExpressions.valueDate(CoreDateUtils.getDayFirstTimeMoment(_cpFinishDateTo.getData()))) : null;

            dql.builder.where(createExistsCombinationPostExpression(dql, cpAlias, fromExpr == null ? toExpr : (toExpr == null ? fromExpr : DQLExpressions.and(fromExpr, toExpr))));
        }
    }

    // Getters

    public IReportParam<DataWrapper> getHasCombinationPost()
    {
        return _hasCombinationPost;
    }

    public IReportParam<List<EmployeeType>> getCpEmployeeType()
    {
        return _cpEmployeeType;
    }

    public IReportParam<List<Post>> getCpPost()
    {
        return _cpPost;
    }

    public IReportParam<List<OrgUnit>> getCpOrgUnit()
    {
        return _cpOrgUnit;
    }

    public IReportParam<DataWrapper> getCpOrgUnitDeep()
    {
        return _cpOrgUnitDeep;
    }

    public IReportParam<List<QualificationLevel>> getCpQualificationLevel()
    {
        return _cpQualificationLevel;
    }

    public IReportParam<List<ProfQualificationGroup>> getCpProfQualificationGroup()
    {
        return _cpProfQualificationGroup;
    }

    public IReportParam<List<CombinationPostType>> getCpType()
    {
        return _cpType;
    }

    public IReportParam<DataWrapper> getCpPostStatus()
    {
        return _cpPostStatus;
    }

    public IReportParam<Date> getCpStartDateFrom()
    {
        return _cpStartDateFrom;
    }

    public IReportParam<Date> getCpStartDateTo()
    {
        return _cpStartDateTo;
    }

    public IReportParam<Date> getCpFinishDateFrom()
    {
        return _cpFinishDateFrom;
    }

    public IReportParam<Date> getCpFinishDateTo()
    {
        return _cpFinishDateTo;
    }
}
