package ru.tandemservice.uniemp.entity.employee;

import org.tandemframework.common.catalog.entity.ICatalogItem;

import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.Post;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.ProfQualificationGroup;
import org.tandemframework.shared.employeebase.catalog.entity.QualificationLevel;
import ru.tandemservice.uniemp.entity.employee.gen.PaymentToPostBaseValueGen;

/**
 * Базовое значение выплаты для должности
 */
public class PaymentToPostBaseValue extends PaymentToPostBaseValueGen
{
    public static final Object POST_TITLE_KEY = new String[] { L_POST_BOUNDED_WITH_Q_GAND_Q_L, PostBoundedWithQGandQL.P_TITLE };
    public static final Object POST_TYPE_KEY = new String[] { L_POST_BOUNDED_WITH_Q_GAND_Q_L, PostBoundedWithQGandQL.L_POST, Post.L_EMPLOYEE_TYPE, EmployeeType.P_SHORT_TITLE };
    public static final Object ETKS_LEVEL_KEY = new String[] { L_POST_BOUNDED_WITH_Q_GAND_Q_L, PostBoundedWithQGandQL.L_ETKS_LEVELS, ICatalogItem.CATALOG_ITEM_TITLE };
    public static final Object QUALIFICATION_LEVEL_KEY = new String[] { L_POST_BOUNDED_WITH_Q_GAND_Q_L, PostBoundedWithQGandQL.L_QUALIFICATION_LEVEL, QualificationLevel.P_SHORT_TITLE };
    public static final Object PROF_QUALIFICATION_GROUP_KEY = new String[] { L_POST_BOUNDED_WITH_Q_GAND_Q_L, PostBoundedWithQGandQL.L_PROF_QUALIFICATION_GROUP, ProfQualificationGroup.P_SHORT_TITLE };
    public static final Object BASE_SALARY_KEY = new String[] { L_POST_BOUNDED_WITH_Q_GAND_Q_L, PostBoundedWithQGandQL.P_SALARY };
}