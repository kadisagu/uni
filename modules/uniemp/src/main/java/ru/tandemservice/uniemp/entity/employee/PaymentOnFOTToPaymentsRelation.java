package ru.tandemservice.uniemp.entity.employee;

import org.tandemframework.shared.commonbase.dao.IEntityRelation;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.employee.gen.PaymentOnFOTToPaymentsRelationGen;

/**
 * Связь выплат, начисляемых на месячный фонд оплаты труда с выплатами
 */
public class PaymentOnFOTToPaymentsRelation extends PaymentOnFOTToPaymentsRelationGen implements IEntityRelation<Payment, Payment>
{
}