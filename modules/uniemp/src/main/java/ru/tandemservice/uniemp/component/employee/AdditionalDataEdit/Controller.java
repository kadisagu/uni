/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.AdditionalDataEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

/**
 * @author dseleznev
 * Created on: 31.07.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent context)
    {
        getDao().prepare(getModel(context));
    }
 
    public void onClickApply(IBusinessComponent context)
    {
        getDao().update(getModel(context));
        deactivate(context);
    }
}