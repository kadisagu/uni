/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniemp.component.catalog.medicalEducationLevel.MedicalEducationLevelAddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;

import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduProgramSubjectSelectModel;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniemp.entity.catalog.MedicalEducationLevel;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author AutoGenerator
 * Created on 21.09.2010
 */
public class DAO extends DefaultCatalogAddEditDAO<MedicalEducationLevel, Model> implements IDAO
{
    @Override
    @SuppressWarnings("deprecation")
    public void prepare(Model model)
    {
        super.prepare(model);

        model.setEduProgramSubjectIndexModel(new LazySimpleSelectModel<>(EduProgramSubjectIndex.class, "title")
                .setSearchFromStart(false)
                .setSearchProperty("title"));
        model.setEduProgramSubjectModel(new EduProgramSubjectSelectModel()
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EduProgramSubject.class, alias);

                EduProgramSubjectIndex selectedSubjectIndex = model.getEduProgramSubjectIndex();
                if (selectedSubjectIndex != null)
                {
                    builder.where(eq(property(alias, EduProgramSubject.subjectIndex()), value(selectedSubjectIndex)));
                    if (StringUtils.isNotEmpty(filter)){
                        builder.where(likeUpper(
                                DQLFunctions.concat(property(alias, EduProgramSubject.subjectCode()), value(" "), property(alias, EduProgramSubject.title())),
                                value(CoreStringUtils.escapeLike(filter, true))
                        ));
                    }
                }
                else
                    builder.where(DQLExpressions.nothing());
                return builder;
            }
        });
    }

    @Override
    public void validate(Model model, ErrorCollector errors) {
        super.validate(model, errors);
        if (model.getEduProgramSubjectIndex() != null && model.getCatalogItem().getProgramSubject() == null)
            errors.add("Для заданного перечня не выбрано направление подготовки (специальность).");
    }
}