/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.EmployeeVacanciesList;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uniemp.entity.employee.StaffListItem;

import java.util.List;

/**
 * @author dseleznev
 * Created on: 29.10.2008
 */
public class Model
{
    private List<PostBoundedWithQGandQL> _postsList;
    private OrgUnit _orgUnit;
    private OrgUnitType _orgUnitType;
    private boolean _childOgrUnitVisible;
    private IMultiSelectModel _postsListModel;
    private List<HSelectOption> _employeeTypeHierarchyList;
    private EmployeeType _employeeType;
    private ISelectModel _orgunitsList;
    private List<OrgUnitType> _orgUnitTypesList;
    private DynamicListDataSource<ViewWrapper<StaffListItem>> _dataSource;

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        this._orgUnit = orgUnit;
    }


    public OrgUnitType getOrgUnitType()
    {
        return _orgUnitType;
    }

    public void setOrgUnitType(OrgUnitType orgUnitType)
    {
        this._orgUnitType = orgUnitType;
    }

    public boolean isChildOgrUnitVisible()
    {
        return _childOgrUnitVisible;
    }

    public void setChildOgrUnitVisible(boolean childOgrUnitVisible)
    {
        _childOgrUnitVisible = childOgrUnitVisible;
    }

    public ISelectModel getOrgunitsList()
    {
        return _orgunitsList;
    }

    public void setOrgunitsList(ISelectModel orgunitsList)
    {
        this._orgunitsList = orgunitsList;
    }

    public List<OrgUnitType> getOrgUnitTypesList()
    {
        return _orgUnitTypesList;
    }

    public void setOrgUnitTypesList(List<OrgUnitType> orgUnitTypesList)
    {
        this._orgUnitTypesList = orgUnitTypesList;
    }

    public DynamicListDataSource<ViewWrapper<StaffListItem>> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<ViewWrapper<StaffListItem>> dataSource)
    {
        this._dataSource = dataSource;
    }

    public EmployeeType getEmployeeType() {
        return _employeeType;
    }

    public void setEmployeeType(EmployeeType employeeType) {
        _employeeType = employeeType;
    }

    public List<PostBoundedWithQGandQL> getPostsList() {
        return _postsList;
    }

    public void setPostsList(List<PostBoundedWithQGandQL> postsList) {
        _postsList = postsList;
    }

    public ISelectModel getPostsListModel() {
        return _postsListModel;
    }

    public void setPostsListModel(IMultiSelectModel postsListModel) {
        _postsListModel = postsListModel;
    }


    public List<HSelectOption> getEmployeeTypeHierarchyList() {
        return _employeeTypeHierarchyList;
    }

    public void setEmployeeTypeHierarchyList(List<HSelectOption> employeeTypeHierarchyList) {
        _employeeTypeHierarchyList = employeeTypeHierarchyList;
    }
}