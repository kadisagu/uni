/**
 *$Id:$
 */
package ru.tandemservice.uniemp.component.reports.employeeVPO.Add;

/**
 * @author Alexander Shaburov
 * @since 02.10.12
 */
public class ReportSexLineWrapper
{
    public ReportSexLineWrapper(int level, String reportLineName, String reportLineCode, int numberLine)
    {
        _level = level;
        _reportLineName = reportLineName;
        _reportLineCode = reportLineCode;
        _numberLine = numberLine;
    }

    private int _level;
    private String _reportLineName;
    private String _reportLineCode;
    private int _numberLine;
    private int _total;
    private int _total25;
    private int _women25;
    private int _total25_29;
    private int _women25_29;
    private int _total30_34;
    private int _women30_34;
    private int _total35_39;
    private int _women35_39;
    private int _total40_44;
    private int _women40_44;
    private int _total45_49;
    private int _women45_49;
    private int _total50_54;
    private int _women50_54;
    private int _total55_59;
    private int _women55_59;
    private int _total60_64;
    private int _women60_64;
    private int _total65;
    private int _women65;

    // Getters & Setters

    public int getLevel()
    {
        return _level;
    }

    public void setLevel(int level)
    {
        _level = level;
    }

    public String getReportLineName()
    {
        return _reportLineName;
    }

    public void setReportLineName(String reportLineName)
    {
        _reportLineName = reportLineName;
    }

    public String getReportLineCode()
    {
        return _reportLineCode;
    }

    public void setReportLineCode(String reportLineCode)
    {
        _reportLineCode = reportLineCode;
    }

    public int getNumberLine()
    {
        return _numberLine;
    }

    public void setNumberLine(int numberLine)
    {
        _numberLine = numberLine;
    }

    public int getTotal()
    {
        return _total;
    }

    public void setTotal(int total)
    {
        _total = total;
    }

    public int getTotal25()
    {
        return _total25;
    }

    public void setTotal25(int total25)
    {
        _total25 = total25;
    }

    public int getWomen25()
    {
        return _women25;
    }

    public void setWomen25(int women25)
    {
        _women25 = women25;
    }

    public int getTotal25_29()
    {
        return _total25_29;
    }

    public void setTotal25_29(int total25_29)
    {
        _total25_29 = total25_29;
    }

    public int getWomen25_29()
    {
        return _women25_29;
    }

    public void setWomen25_29(int women25_29)
    {
        _women25_29 = women25_29;
    }

    public int getTotal30_34()
    {
        return _total30_34;
    }

    public void setTotal30_34(int total30_34)
    {
        _total30_34 = total30_34;
    }

    public int getWomen30_34()
    {
        return _women30_34;
    }

    public void setWomen30_34(int women30_34)
    {
        _women30_34 = women30_34;
    }

    public int getTotal35_39()
    {
        return _total35_39;
    }

    public void setTotal35_39(int total35_39)
    {
        _total35_39 = total35_39;
    }

    public int getWomen35_39()
    {
        return _women35_39;
    }

    public void setWomen35_39(int women35_39)
    {
        _women35_39 = women35_39;
    }

    public int getTotal40_44()
    {
        return _total40_44;
    }

    public void setTotal40_44(int total40_44)
    {
        _total40_44 = total40_44;
    }

    public int getWomen40_44()
    {
        return _women40_44;
    }

    public void setWomen40_44(int women40_44)
    {
        _women40_44 = women40_44;
    }

    public int getTotal45_49()
    {
        return _total45_49;
    }

    public void setTotal45_49(int total45_49)
    {
        _total45_49 = total45_49;
    }

    public int getWomen45_49()
    {
        return _women45_49;
    }

    public void setWomen45_49(int women45_49)
    {
        _women45_49 = women45_49;
    }

    public int getTotal50_54()
    {
        return _total50_54;
    }

    public void setTotal50_54(int total50_54)
    {
        _total50_54 = total50_54;
    }

    public int getWomen50_54()
    {
        return _women50_54;
    }

    public void setWomen50_54(int women50_54)
    {
        _women50_54 = women50_54;
    }

    public int getTotal55_59()
    {
        return _total55_59;
    }

    public void setTotal55_59(int total55_59)
    {
        _total55_59 = total55_59;
    }

    public int getWomen55_59()
    {
        return _women55_59;
    }

    public void setWomen55_59(int women55_59)
    {
        _women55_59 = women55_59;
    }

    public int getTotal60_64()
    {
        return _total60_64;
    }

    public void setTotal60_64(int total60_64)
    {
        _total60_64 = total60_64;
    }

    public int getWomen60_64()
    {
        return _women60_64;
    }

    public void setWomen60_64(int women60_64)
    {
        _women60_64 = women60_64;
    }

    public int getTotal65()
    {
        return _total65;
    }

    public void setTotal65(int total65)
    {
        _total65 = total65;
    }

    public int getWomen65()
    {
        return _women65;
    }

    public void setWomen65(int women65)
    {
        _women65 = women65;
    }
}
