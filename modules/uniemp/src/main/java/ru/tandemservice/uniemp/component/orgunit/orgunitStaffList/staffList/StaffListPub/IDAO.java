/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffList.StaffListPub;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author dseleznev
 * Created on: 19.09.2008
 */
public interface IDAO extends IUniDao<Model>
{
    void updateFillStaffListAllocationAutomatically(Model model);
    
    public void updateCopyStaffListAllocationItem(Long staffListAllocationItemId);
    
    void prepareStaffListPaymentsDataSource(Model model);

    void prepareStaffListTotalPaymentsDataSource(Model model);
    
    void prepareStaffListAllocationsDataSource(Model model);

    void prepareStaffListAllocationsTotalDataSource(Model model);

    void prepareStaffListEmployeeHRDataSource(Model model);
}