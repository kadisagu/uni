/**
 *$Id:$
 */
package ru.tandemservice.uniemp.base.bo.EmpMedicalStuffReport.ui.Add;

import org.tandemframework.caf.report.RtfListDataSourcePrinter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.source.IListDataSourcePrinter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.report.summaryReports.SummaryDataWrapper;
import ru.tandemservice.uniemp.base.bo.EmpMedicalStuffReport.EmpMedicalStuffReportManager;
import ru.tandemservice.uniemp.base.bo.EmpMedicalStuffReport.ui.Detail.EmpMedicalStuffReportDetail;
import ru.tandemservice.uniemp.entity.catalog.EmployeeTemplateDocument;
import ru.tandemservice.uniemp.entity.catalog.MedicalEducationLevel;
import ru.tandemservice.uniemp.entity.employee.EmployeeMedicalSpeciality;

import java.text.NumberFormat;
import java.util.*;

/**
 * @author Alexander Shaburov
 * @since 30.10.12
 */
public class EmpMedicalStuffReportAddUI extends UIPresenter
{
    // property
    public static final String PROP_SUMM_DATA_WRAPPER_LIST = "summaryDataWrapperList";
    public static final String PROP_IGNORE_EMPLOYEE_ID_LIST = "ignorEmployeeIdList";

    private Date _reportDate = new Date();
    private List<PostType> _postTypeList;
    private List<MedicalEducationLevel> _medicalSpecialityList;
    private List<EmployeeType> _employeeTypeList;
    private List<Long> _ignorEmployeeIdList;
    private List<SummaryDataWrapper<EmployeeMedicalSpeciality>> _summaryDataWrapperList;
    private Map<Long, SummaryDataWrapper<EmployeeMedicalSpeciality>> _summaryDataWrapperMap;

    private List<EduLevel> _eduLevelList;

    @Override
    public void onComponentRefresh()
    {
        _summaryDataWrapperList = EmpMedicalStuffReportManager.instance().empMedicalStuffReportDao().getReportWrapperList(_reportDate, _postTypeList, _medicalSpecialityList,_employeeTypeList, _eduLevelList);

        _ignorEmployeeIdList = EmpMedicalStuffReportManager.instance().empMedicalStuffReportDao().getIgnoreEmployeeList(_reportDate, _postTypeList, _medicalSpecialityList,_employeeTypeList, _eduLevelList);

        _summaryDataWrapperMap = new HashMap<>();
        for (SummaryDataWrapper<EmployeeMedicalSpeciality> wrapper : _summaryDataWrapperList)
            _summaryDataWrapperMap.put(wrapper.getId(), wrapper);
    }

    public void onClickApply()
    {
        _summaryDataWrapperList = EmpMedicalStuffReportManager.instance().empMedicalStuffReportDao().getReportWrapperList(_reportDate, _postTypeList, _medicalSpecialityList,_employeeTypeList, _eduLevelList);

        _ignorEmployeeIdList = EmpMedicalStuffReportManager.instance().empMedicalStuffReportDao().getIgnoreEmployeeList(_reportDate, _postTypeList, _medicalSpecialityList,_employeeTypeList, _eduLevelList);

        getConfig().<BaseSearchListDataSource>getDataSource(EmpMedicalStuffReportAdd.EMPLOYEE_MEDICAL_SPECIALITY_SEARCH_DS).getLegacyDataSource().refresh();

        _summaryDataWrapperMap = new HashMap<>();
        for (SummaryDataWrapper<EmployeeMedicalSpeciality> wrapper : _summaryDataWrapperList)
            _summaryDataWrapperMap.put(wrapper.getId(), wrapper);
    }

    public IListDataSourcePrinter getRtfPrintDS()
    {
        return new RtfListDataSourcePrinter("report", getConfig().<BaseSearchListDataSource>getDataSource(EmpMedicalStuffReportAdd.EMPLOYEE_MEDICAL_SPECIALITY_SEARCH_DS).<SummaryDataWrapper>getLegacyDataSource())
        {
            @SuppressWarnings("unchecked")
            @Override
            public RtfDocument print()
            {
                byte[] content = DataAccessServices.dao().get(EmployeeTemplateDocument.class, EmployeeTemplateDocument.code(), "empMedicalStuffReport").getContent();
                RtfDocument document = new RtfReader().read(content);

                NumberFormat numberFormat = NumberFormat.getInstance();
                numberFormat.setMinimumIntegerDigits(2);

                int number = 1;
                List<String[]> lineList = new ArrayList<>();
                List entityList = getSource().getEntityList();
                for (SummaryDataWrapper wrapper : (List<SummaryDataWrapper>) entityList)
                {
                    String[] line = new String[7];

                    line[0] = wrapper.getTitle();
                    line[1] = numberFormat.format(number++);
                    line[2] = wrapper.getProperty("total").toString();
                    line[3] = wrapper.getProperty("high").toString();
                    line[4] = wrapper.getProperty("first").toString();
                    line[5] = wrapper.getProperty("second").toString();
                    line[6] = wrapper.getProperty("certificate").toString();

                    lineList.add(line);
                }

                new RtfTableModifier()
                        .put("T", lineList.toArray(new String[lineList.size()][7]))
                        .modify(document);

                new RtfInjectModifier()
                        .put("reportDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(_reportDate))
                        .modify(document);

                return document;
            }
        };
    }

    public void onClickTotal()
    {
        SummaryDataWrapper current = _summaryDataWrapperMap.get(getListenerParameterAsLong());

        getActivationBuilder().asRegionDialog(EmpMedicalStuffReportDetail.class)
                .parameter("medSpecIdList", current.getMatchedIds("total"))
                .parameter("reportDate", _reportDate)
                .parameter("postTypeIdList", CommonBaseEntityUtil.getIdList(_postTypeList == null ? Collections.<IEntity>emptyList() : _postTypeList))
                .parameter("columnTitle", "Всего")
                .parameter("rowTitle", current.getTitle())
                .activate();
    }

    public void onClickHigh()
    {
        SummaryDataWrapper current = _summaryDataWrapperMap.get(getListenerParameterAsLong());

        getActivationBuilder().asRegionDialog(EmpMedicalStuffReportDetail.class)
                .parameter("medSpecIdList", current.getMatchedIds("high"))
                .parameter("reportDate", _reportDate)
                .parameter("postTypeIdList", CommonBaseEntityUtil.getIdList(_postTypeList == null ? Collections.<IEntity>emptyList() : _postTypeList))
                .parameter("columnTitle", "Из общего числа имеют квалификационную категорию высшую")
                .parameter("rowTitle", current.getTitle())
                .activate();
    }

    public void onClickFirst()
    {
        SummaryDataWrapper current = _summaryDataWrapperMap.get(getListenerParameterAsLong());

        getActivationBuilder().asRegionDialog(EmpMedicalStuffReportDetail.class)
                .parameter("medSpecIdList", current.getMatchedIds("first"))
                .parameter("reportDate", _reportDate)
                .parameter("postTypeIdList", CommonBaseEntityUtil.getIdList(_postTypeList == null ? Collections.<IEntity>emptyList() : _postTypeList))
                .parameter("columnTitle", "Из общего числа имеют квалификационную категорию 1")
                .parameter("rowTitle", current.getTitle())
                .activate();
    }

    public void onClickSecond()
    {
        SummaryDataWrapper current = _summaryDataWrapperMap.get(getListenerParameterAsLong());

        getActivationBuilder().asRegionDialog(EmpMedicalStuffReportDetail.class)
                .parameter("medSpecIdList", current.getMatchedIds("second"))
                .parameter("reportDate", _reportDate)
                .parameter("postTypeIdList", CommonBaseEntityUtil.getIdList(_postTypeList == null ? Collections.<IEntity>emptyList() : _postTypeList))
                .parameter("columnTitle", "Из общего числа имеют квалификационную категорию 2")
                .parameter("rowTitle", current.getTitle())
                .activate();
    }

    public void onClickCertificate()
    {
        SummaryDataWrapper current = _summaryDataWrapperMap.get(getListenerParameterAsLong());

        getActivationBuilder().asRegionDialog(EmpMedicalStuffReportDetail.class)
                .parameter("medSpecIdList", current.getMatchedIds("certificate"))
                .parameter("reportDate", _reportDate)
                .parameter("postTypeIdList", CommonBaseEntityUtil.getIdList(_postTypeList == null ? Collections.<IEntity>emptyList() : _postTypeList))
                .parameter("columnTitle", "Из общего числа имеют сертификат специалиста")
                .parameter("rowTitle", current.getTitle())
                .activate();
    }

    public void onClickIgnoreEmployee()
    {
        getActivationBuilder().asRegionDialog(EmpMedicalStuffReportDetail.class)
                .parameter("ignoreEmployeeIdList", _ignorEmployeeIdList)
                .parameter("reportDate", _reportDate)
                .parameter("postTypeIdList", CommonBaseEntityUtil.getIdList(_postTypeList == null ? Collections.<IEntity>emptyList() : _postTypeList))
                .parameter("medicalSpecialityIdList", UniBaseDao.ids(_medicalSpecialityList))
                .activate();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(EmpMedicalStuffReportAdd.EMPLOYEE_MEDICAL_SPECIALITY_SEARCH_DS))
        {
            dataSource.put(PROP_SUMM_DATA_WRAPPER_LIST, _summaryDataWrapperList);
        }
        if (dataSource.getName().equals(EmpMedicalStuffReportAdd.ADD_DATA_SEARCH_DS))
        {
            dataSource.put(PROP_IGNORE_EMPLOYEE_ID_LIST, _ignorEmployeeIdList);
        }
    }

    // Getters & Setters

    public Date getReportDate()
    {
        return _reportDate;
    }

    public void setReportDate(Date reportDate)
    {
        _reportDate = reportDate;
    }

    public List<PostType> getPostTypeList()
    {
        return _postTypeList;
    }

    public void setPostTypeList(List<PostType> postTypeList)
    {
        _postTypeList = postTypeList;
    }

    public List<MedicalEducationLevel> getMedicalSpecialityList()
    {
        return _medicalSpecialityList;
    }

    public void setMedicalSpecialityList(List<MedicalEducationLevel> medicalSpecialityList)
    {
        _medicalSpecialityList = medicalSpecialityList;
    }

    public List<SummaryDataWrapper<EmployeeMedicalSpeciality>> getSummaryDataWrapperList()
    {
        return _summaryDataWrapperList;
    }

    public void setSummaryDataWrapperList(List<SummaryDataWrapper<EmployeeMedicalSpeciality>> summaryDataWrapperList)
    {
        _summaryDataWrapperList = summaryDataWrapperList;
    }

    public List<Long> getIgnorEmployeeIdList()
    {
        return _ignorEmployeeIdList;
    }

    public void setIgnorEmployeeIdList(List<Long> ignorEmployeeIdList)
    {
        _ignorEmployeeIdList = ignorEmployeeIdList;
    }

    public Map<Long, SummaryDataWrapper<EmployeeMedicalSpeciality>> getSummaryDataWrapperMap()
    {
        return _summaryDataWrapperMap;
    }

    public void setSummaryDataWrapperMap(Map<Long, SummaryDataWrapper<EmployeeMedicalSpeciality>> summaryDataWrapperMap)
    {
        _summaryDataWrapperMap = summaryDataWrapperMap;
    }

    public List<EmployeeType> getEmployeeTypeList()
    {
        return _employeeTypeList;
    }

    public void setEmployeeTypeList(List<EmployeeType> employeeTypeList)
    {
        _employeeTypeList = employeeTypeList;
    }

    public List<EduLevel> getEduLevelList() {
        return _eduLevelList;
    }

    public void setEduLevelList(List<EduLevel> eduLevelList) {
        _eduLevelList = eduLevelList;
    }

}
