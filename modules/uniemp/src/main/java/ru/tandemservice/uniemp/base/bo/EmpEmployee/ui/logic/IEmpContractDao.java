/**
 *$Id:$
 */
package ru.tandemservice.uniemp.base.bo.EmpEmployee.ui.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

/**
 * @author Alexander Shaburov
 * @since 22.10.12
 */
public interface IEmpContractDao extends INeedPersistenceSupport
{
}
