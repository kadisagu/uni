/**
 *$Id:$
 */
package ru.tandemservice.uniemp.base.bo.EmpEmployee.ui.LabourContract;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonBaseSearchListUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostView.EmployeePostViewUI;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniemp.IUniempComponents;
import ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;

/**
 * @author Alexander Shaburov
 * @since 22.10.12
 */
@State({
        @Bind(key = "accessible", binding = "accessible", required = true),
        @Bind(key = "employeePostId", binding = "employeePostId", required = true)
})
public class EmpEmployeeLabourContractUI extends UIPresenter
{
    private long _employeePostId;
    private boolean _accessible;

    private EmployeeLabourContract _contract;
    private EmployeePost _employeePost;

    private DynamicListDataSource<ContractCollateralAgreement> _contractAgreementDataSource;

    @Override
    public void onComponentRefresh()
    {
        _employeePost = DataAccessServices.dao().getNotNull(_employeePostId);
        _contract = DataAccessServices.dao().getUnique(EmployeeLabourContract.class, EmployeeLabourContract.employeePost().s(), _employeePost);
        
        prepareAgreementDataSource();
    }
    
    public void prepareAgreementDataSource()
    {
        DynamicListDataSource<ContractCollateralAgreement> dataSource = new DynamicListDataSource<>(this, component ->
            {
                MQBuilder builder = new MQBuilder(ContractCollateralAgreement.ENTITY_CLASS, "a");
                builder.add(MQExpression.eq("a", ContractCollateralAgreement.contract().s(), _contract));
                CommonBaseSearchListUtil.createPage(_contractAgreementDataSource, builder);
            }
            , 5);

        dataSource.addColumn(new SimpleColumn("Номер", ContractCollateralAgreement.number().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дата", ContractCollateralAgreement.date().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Описание", ContractCollateralAgreement.description().s()).setClickable(false).setOrderable(false));

        dataSource.addColumn(CommonBaseUtil.getPrintColumn("onClickPrintContract", "Печатать файл доп. соглашения").setDisabledProperty(ContractCollateralAgreement.P_PRINTING_DISABLED).setPermissionKey("printAgreement_employeePost"));

        if (_accessible)
        {
            dataSource.addColumn(new ActionColumn("Добавить/заменить файл доп. соглашения", "agrmnt_add", "onClickAddContractAgreementFile").setPermissionKey("editAgreement_employeePost"));
            dataSource.addColumn(new ActionColumn("Удалить файл доп. соглашения", "agrmnt_del", "onDelContractAgreementFile", "Удалить файл дополнительного соглашения?").setPermissionKey("editAgreement_employeePost").setDisabledProperty(ContractCollateralAgreement.P_PRINTING_DISABLED));
            dataSource.addColumn(new ActionColumn("Редактировать доп. соглашение", ActionColumn.EDIT, "onEditContractAgreement").setPermissionKey("editAgreement_employeePost"));
            dataSource.addColumn(new ActionColumn("Удалить доп. соглашение", ActionColumn.DELETE, "onDeleteRowFromList", "Удалить дополнительное соглашение {0}?", ContractCollateralAgreement.P_FULL_NUMBER).setPermissionKey("deleteAgreement_employeePost"));
        }

        OrderDirection direction = dataSource.getEntityOrder().getDirection();
        dataSource.changeOrder(1);
        dataSource.getEntityOrder().setDirection(direction);
        
        _contractAgreementDataSource = dataSource;
    }

    public void onClickPrintContract() {
        ContractCollateralAgreement contract = IUniBaseDao.instance.get().get(ContractCollateralAgreement.class, getListenerParameterAsLong());
        byte[] content = contract.getAgreementFile().getContent();
        if (content == null)
            throw new ApplicationException("Файл трудового договора пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer()
            .contentType(contract.getAgreementFileType())
            .fileName(contract.getAgreementFileName())
            .document(content), true);
    }

    public void onClickAddContract()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_CONTRACT_ADDEDIT, EmployeePostViewUI.EMPLOYEE_POST_TAB_PANEL_REGION_NAME)
                .parameter("labourContractId", null)
                .parameter("employeePostId", _employeePost.getId())
                .activate();
    }

    public void onClickEditContract()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_CONTRACT_ADDEDIT, EmployeePostViewUI.EMPLOYEE_POST_TAB_PANEL_REGION_NAME)
                .parameter("labourContractId", _contract.getId())
                .parameter("employeePostId", null)
                .activate();
    }

    public void onClickAddContractFile()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_CONTRACT_FILE_ADD, EmployeePostViewUI.EMPLOYEE_POST_TAB_PANEL_REGION_NAME)
                .parameter("labourContractId", _contract == null ? null : _contract.getId())
                .activate();
    }
    
    public void onPrintContractFile()
    {
        EmployeeLabourContract contract = _contract;

        byte[] content = contract.getContractFile().getContent();
        if (content == null)
            throw new ApplicationException("Файл трудового договора пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType(contract.getContractFileType()).fileName(contract.getContractFileName()).document(content), false);
    }

    public void onClickDeleteContractFile()
    {
        final EmployeeLabourContract item = _contract;

        if (item == null) return;

        DatabaseFile file = item.getContractFile();
        if (file != null)
        {
            item.setContractFile(null);
            item.setContractFileName(null);
            item.setContractFileType(null);
            DataAccessServices.dao().update(item);
            DataAccessServices.dao().delete(file);
        }
    }

    public void onClickAddContractAgreement()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_AGREEMENT_TO_CONTRACT_ADDEDIT, EmployeePostViewUI.EMPLOYEE_POST_TAB_PANEL_REGION_NAME)
                .parameter("rowId", null)
                .parameter("labourContractId", _contract == null ? null : _contract.getId())
                .activate();
    }

    public void onClickAddContractAgreementFile()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_AGREEMENT_TO_CONTRACT_FILE_ADD, EmployeePostViewUI.EMPLOYEE_POST_TAB_PANEL_REGION_NAME)
                .parameter("rowId", getListenerParameterAsLong())
                .activate();
    }

    public void onDelContractAgreementFile()
    {
        ISharedBaseDao.instance.get().doInTransaction(session ->  {
                ContractCollateralAgreement item = DataAccessServices.dao().getNotNull(ContractCollateralAgreement.class, getListenerParameterAsLong());
                DatabaseFile file = item.getAgreementFile();

                if (file != null)
                {
                    item.setAgreementFile(null);
                    item.setAgreementFileName(null);
                    item.setAgreementFileType(null);
                    session.update(item);
                    session.flush();
                    session.delete(file);
                }
                return null;
            });
    }

    public void onEditContractAgreement()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_AGREEMENT_TO_CONTRACT_ADDEDIT, EmployeePostViewUI.EMPLOYEE_POST_TAB_PANEL_REGION_NAME)
                .parameter("rowId", getListenerParameterAsLong())
                .parameter("labourContractId", _contract == null ? null : _contract.getId())
                .activate();
    }

    public void onDeleteRowFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
        getConfig().getBusinessComponent().refresh();
    }

    // Calculate getters

    public boolean isHasLabourContract()
    {
        return _contract != null;
    }

    public boolean isHasLabourContractFile()
    {
        return isHasLabourContract() && _contract.getContractFile() != null;
    }

    // Getters & Setters

    public DynamicListDataSource getContractAgreementDataSource()
    {
        return _contractAgreementDataSource;
    }

    public void setContractAgreementDataSource(DynamicListDataSource<ContractCollateralAgreement> contractAgreementDataSource)
    {
        _contractAgreementDataSource = contractAgreementDataSource;
    }

    public EmployeeLabourContract getContract()
    {
        return _contract;
    }

    public void setContract(EmployeeLabourContract contract)
    {
        _contract = contract;
    }

    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    public void setEmployeePost(EmployeePost employeePost)
    {
        _employeePost = employeePost;
    }

    public long getEmployeePostId()
    {
        return _employeePostId;
    }

    public void setEmployeePostId(long employeePostId)
    {
        _employeePostId = employeePostId;
    }

    public boolean isAccessible()
    {
        return _accessible;
    }

    public void setAccessible(boolean accessible)
    {
        _accessible = accessible;
    }
}
