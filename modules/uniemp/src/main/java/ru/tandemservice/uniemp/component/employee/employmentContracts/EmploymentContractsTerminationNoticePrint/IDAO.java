/**
 *$Id$
 */
package ru.tandemservice.uniemp.component.employee.employmentContracts.EmploymentContractsTerminationNoticePrint;

import java.util.Date;

import org.tandemframework.rtf.document.RtfDocument;

import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;

/**
 * Create by ashaburov
 * Date 19.08.11
 */
public interface IDAO extends IUniDao<Model>
{
    public RtfDocument modifyDocument(EmployeeLabourContract contract, String number, Date date, RtfDocument document);

    public RtfDocument getRtfDocument();
}
