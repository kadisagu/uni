/* $Id$ */
package ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.PaymentsDataTab.block.paymentData;

import java.util.List;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.dql.DQLExpressions;

import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.catalog.PaymentType;

/**
 * @author Vasily Zhukov
 * @since 05.08.2011
 */
public class PaymentDataBlock
{
    // names
    public static final String PAYMENT_TYPE_DS = "paymentTypeDS";
    public static final String PAYMENT_DS = "paymentDS";
    
    // datasource parameter names
    private static final String PARAM_PAYMENT_TYPE_LIST = "paymentTypeList";
    
    public static void onBeforeDataSourceFetch(IUIDataSource dataSource, PaymentDataParam param)
    {
        String name = dataSource.getName();

        if (PAYMENT_DS.equals(name))
        {
            param.getPaymentType().putParamIfActive(dataSource, PARAM_PAYMENT_TYPE_LIST);
        }
    }

    public static IDefaultComboDataSourceHandler createPaymentTypeDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, PaymentType.class);
    }
    
    public static IDefaultComboDataSourceHandler createPaymentDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, Payment.class)
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);

                List<PaymentType> paymentTypeList = ep.context.get(PARAM_PAYMENT_TYPE_LIST);

                if (paymentTypeList != null)
                    ep.dqlBuilder.where(DQLExpressions.in(DQLExpressions.property(Payment.type().fromAlias("e")), paymentTypeList));
            }
        };
    }
}
