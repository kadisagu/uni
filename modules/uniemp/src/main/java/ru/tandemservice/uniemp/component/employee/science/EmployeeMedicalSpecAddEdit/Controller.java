/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.science.EmployeeMedicalSpecAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.info.ErrorCollector;

import ru.tandemservice.uniemp.UniempDefines;

/**
 * @author dseleznev
 * Created on: 21.10.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onCategoryRefresh(IBusinessComponent component)
    {
        Model model = getModel(component);
        if(null == model.getEmployeeMedicalSpeciality().getMedicalQualificationCategory() || UniempDefines.MEDICAL_QUALIFICATION_CATEGORY_WITHOUT_CATEGORY.equals(model.getEmployeeMedicalSpeciality().getMedicalQualificationCategory().getCode()))
            model.setCategoryRelatedFieldsVisible(false);
        else
            model.setCategoryRelatedFieldsVisible(true);
    }

    public void onClickApply(IBusinessComponent component)
    {
        ErrorCollector errors = component.getUserContext().getErrorCollector();
        Model model = getModel(component);
        getDao().validate(model, errors);
        getDao().update(model);
        deactivate(component);
    }

    public void onChangeCertificateAvailable(IBusinessComponent component)
    {
        Model model = getModel(component);

        if (model.getEmployeeMedicalSpeciality().getExpertCertificateAvailable())
            model.setFileUploadVisible(true);
        else
        {
            model.setFileUploadVisible(false);
            model.setDeleteVisible(false);
        }
    }

    public void onClickDelete(IBusinessComponent component)
    {
        Model model = getModel(component);

        if (model.getEmployeeMedicalSpeciality().getExpertCertificate() != null)
        {
            model.getEmployeeMedicalSpeciality().setExpertCertificate(null);
        }

        model.setDeleteVisible(false);
        model.setFileUploadVisible(true);
    }
}