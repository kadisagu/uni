/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.staffListAllocation.Add;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.text.table.RtfBorder;
import org.tandemframework.rtf.document.text.table.cell.MergeType;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.cell.RtfCellBorder;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.key.TripletKey;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.component.reports.staffList.Add.OrgUnitTypeWrapper;
import ru.tandemservice.uniemp.component.reports.staffList.Add.RtfTableRowStyle;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.employee.*;
import ru.tandemservice.uniemp.entity.report.StaffListRepPaymentSettings;

import java.util.*;

/**
 * @author dseleznev
 * Created on: 13.11.2010
 */
public class StaffListAllocationReportUtil
{
    public static FinancingSource finSrcBudgetCatalogItem = IUniBaseDao.instance.get().getCatalogItem(FinancingSource.class, UniempDefines.FINANCING_SOURCE_BUDGET);

    public static Long time;
    public static Long absoluteTime;

    // Индекс колонки с выплатами
    public static final int PAYMENTS_COLUMN_IDX = 6;

    // Идентификаторы для wrapper-списка видов деятельности
    public static final Long ACTIVITY_TYPE_BUDGET = 1L;
    public static final Long ACTIVITY_TYPE_OFF_BUDGET = 2L;

    // Идентификатор для типа надбавок, доплат "Другие надбавки"
    public static final Long OTHER_PAYMENTS_ID = 0L;

    // Идентификаторы для разных типов сумм по подразделениям
    private static final Long ORG_UNIT_WITH_CHILDS_SUM_ID = 0L; // Подразделения с учетом дочерних
    private static final Long ORG_UNIT_SUM_ID = 1L; // Только подразделения

    public static final RtfTableRowStyle[] ORG_UNIT_HEADER_ROW_STYLES = new RtfTableRowStyle[]
                                                                                             {
        new RtfTableRowStyle(true, false, 28, RtfTableRowStyle.TEXT_ALIGN_CENTER, true, null, 400),
        new RtfTableRowStyle(true, true, 24, RtfTableRowStyle.TEXT_ALIGN_LEFT, true, 100, null),
        new RtfTableRowStyle(true, true, 20, RtfTableRowStyle.TEXT_ALIGN_LEFT, true, 400, null),
        new RtfTableRowStyle(true, true, 16, RtfTableRowStyle.TEXT_ALIGN_LEFT, true, 600, null),
        new RtfTableRowStyle(true, true, 16, RtfTableRowStyle.TEXT_ALIGN_LEFT, true, 800, null),
        new RtfTableRowStyle(true, true, 16, RtfTableRowStyle.TEXT_ALIGN_LEFT, true, 1000, null)
                                                                                             };

    public static final RtfTableRowStyle POST_ROW_STYLE = new RtfTableRowStyle(false, true, null, RtfTableRowStyle.TEXT_ALIGN_LEFT, true, null, null, 1, null, new Integer[] { 20, 20, 20, 20 }, 1);
    public static final RtfTableRowStyle VACANCY_ROW_STYLE = new RtfTableRowStyle(false, true, null, RtfTableRowStyle.TEXT_ALIGN_LEFT, true, null, null, 1, null, new Integer[] { 20, 20, 20, 20 }, 2);
    public static final RtfTableRowStyle VACANCY_TOTAL_ROW_STYLE = new RtfTableRowStyle(false, true, null, RtfTableRowStyle.TEXT_ALIGN_LEFT, true, null, null, 0, null, new Integer[] { 20, 20, 20, 20 }, 1);
    public static final RtfTableRowStyle EMPLOYEE_POST_ROW_STYLE = new RtfTableRowStyle(false, true, null, RtfTableRowStyle.TEXT_ALIGN_LEFT, true, null, null, null, null, new Integer[] { 20, 20, 20, 20 }, null);
    public static final RtfTableRowStyle TOTAL_ROW_STYLE = new RtfTableRowStyle(false, true, null, RtfTableRowStyle.TEXT_ALIGN_RIGHT, false, null, null, new Integer[][] { { 0, 4 } }, new Integer[] { 20, 30, 30, 30 });

    public static Comparator<StaffListItem> POST_TITLE_COMPARATOR = (o1, o2) -> {
        PostBoundedWithQGandQL p1 = o1.getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL();
        PostBoundedWithQGandQL p2 = o2.getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL();

        if (null != p1.getPost().getRank() && null != p2.getPost().getRank() && !p1.getPost().getRank().equals(p2.getPost().getRank()))
            return p1.getPost().getRank().compareTo(p2.getPost().getRank());
        else if (null != p1.getPost().getRank() && null == p2.getPost().getRank()) return -1;
        else if (null != p2.getPost().getRank() && null == p1.getPost().getRank()) return 1;
        else if (!p2.getQualificationLevel().equals(p1.getQualificationLevel()))
            return p2.getQualificationLevel().getShortTitle().compareTo(p1.getQualificationLevel().getShortTitle());
        else
            return p1.getTitle().compareTo(p2.getTitle());
    };

    public static Comparator<EmployeePostFakeWrapper> EMPLOYEE_POST_STAFF_RATES_FULL_FIO_COMPARATOR = CommonCollator.comparing(EmployeePostFakeWrapper::getFullFio);

    /**
     * Подготавливает заголовок таблицы с доплатами (надбавками)
     * в зависимости от настроек фильтров выплат
     */
    public static void preparePaymentsHeader(Model model)
    {
        List<Long> otherPaymentIds = new ArrayList<>();
        List<Payment> paymentHeadersList = new ArrayList<>();

        for (StaffListRepPaymentSettings setting : model.getPaymentsList())
        {
            if (null != setting.getPayment())
            {
                if (setting.isIndividualColumn())
                    paymentHeadersList.add(setting.getPayment());
                else
                    otherPaymentIds.add(setting.getPayment().getId());
            }
            else
            {
                Payment otherPayment = new Payment();
                otherPayment.setId(OTHER_PAYMENTS_ID);
                otherPayment.setShortTitle("другие надбавки");
                paymentHeadersList.add(otherPayment);
            }
        }

        model.setPaymentHeadersList(paymentHeadersList);
        model.setOtherPaymentIds(otherPaymentIds);

        model.setColumnsNumber(10 + model.getPaymentHeadersList().size());
        String[] numbersHeader = new String[model.getColumnsNumber()];
        for (int i = 0; i < model.getColumnsNumber(); i++)
            numbersHeader[i] = String.valueOf(i + 1);
        model.setNumberHeaders(numbersHeader);

        String[] paymentHeaders = new String[model.getPaymentHeadersList().size()];
        for (Payment payment : model.getPaymentHeadersList())
            paymentHeaders[model.getPaymentHeadersList().indexOf(payment)] = payment.getShortTitle();

        int[] paymentColumnWidths = new int[model.getPaymentHeadersList().size()];
        Arrays.fill(paymentColumnWidths, 1);
        model.setPaymentColumnWidth(paymentColumnWidths);

        model.setPaymentHeaders(paymentHeaders);
    }

    /**
     * Возвращает подразделение, на котором должна отобразиться должность или выплата
     * в зависимости от настроек фильтров
     */
    private static OrgUnit getOrgUnitWherePostShouldBeShown(OrgUnit orgUnit, Map<Long, OrgUnitTypeWrapper> orgUnitTypesMap, Map<OrgUnit, OrgUnit> orgUnitMappingsMap)
    {
        if (orgUnitMappingsMap.containsKey(orgUnit))
            return orgUnitMappingsMap.get(orgUnit);

        OrgUnit orgUnitToIterate = orgUnit;
        OrgUnit parentOrgUnit = orgUnitToIterate.getParent();
        OrgUnitTypeWrapper ouTypeWrapper = orgUnitTypesMap.get(orgUnitToIterate.getOrgUnitType().getId());
        boolean useOrgUnit = null != ouTypeWrapper && ouTypeWrapper.isUseOrgUnit();
        boolean individualSection = null != ouTypeWrapper && ouTypeWrapper.isShowChildOrgUnits();
        boolean showChildPostsAsOwn = null != ouTypeWrapper && ouTypeWrapper.isShowChildOrgUnitPostsAsOwn();
        OrgUnit targetOrgUnit = null;

        if (individualSection)
        {
            targetOrgUnit = orgUnitToIterate;
        }
        else
        {

            while ((null != parentOrgUnit && null != parentOrgUnit.getParent()) && !(individualSection && showChildPostsAsOwn))
            {
                orgUnitToIterate = parentOrgUnit;
                parentOrgUnit = orgUnitToIterate.getParent();
                if (null == parentOrgUnit || !useOrgUnit) break;
                ouTypeWrapper = orgUnitTypesMap.get(orgUnitToIterate.getOrgUnitType().getId());
                useOrgUnit = null != ouTypeWrapper && ouTypeWrapper.isUseOrgUnit();
                individualSection = null != ouTypeWrapper && ouTypeWrapper.isShowChildOrgUnits();
                showChildPostsAsOwn = null != ouTypeWrapper && ouTypeWrapper.isShowChildOrgUnitPostsAsOwn();
            }

            if (individualSection && showChildPostsAsOwn)
                targetOrgUnit = orgUnitToIterate;
        }

        orgUnitMappingsMap.put(orgUnit, targetOrgUnit);
        return targetOrgUnit;
    }

    /**
     * Обнуляет в модели переменные и мапы, используемые для отчетной статистики
     */
    private static void initStatisticVariables(Model model)
    {
        model.setVuzSumSalary(getNewPairInstance(0d, 0d));
        model.setVuzSumTotalSalary(getNewPairInstance(0d, 0d));
        model.setVuzBudgetStaffRate(0d);
        model.setVuzOffBudgetStaffRate(0d);
        model.setVuzEmployeePostBudgetStaffRate(0d);
        model.setVuzEmployeePostOffBudgetStaffRate(0d);
        model.setPostSalarysMap(new HashMap<>());
        model.setVuzSumPaymentsMap(new HashMap<>());

        model.setOrgUnitPostBudgetStaffRatesMap(new HashMap<>());
        model.setOrgUnitPostOffBudgetStaffRatesMap(new HashMap<>());
        model.setOrgUnitEmployeePostBudgetStaffRatesMap(new HashMap<>());
        model.setOrgUnitEmployeePostOffBudgetStaffRatesMap(new HashMap<>());

        model.setOrgUnitPostListsMap(new HashMap<>());
        model.setOrgUnitPostSortedListsMap(new HashMap<>());
        model.setStaffListItemsMap(new HashMap<>());
        model.setPostDataMap(new HashMap<>());
        model.setEmployeePostDataMap(new HashMap<>());
        model.setOrgUnitEmployeePostStaffRatesListsMap(new HashMap<>());

        model.setOrgUnitSumPaymentsMap(new HashMap<>());
        model.setOrgUnitWithChildsSumPaymentsMap(new HashMap<>());
        model.setOrgUnitPostPaymentsMap(new HashMap<>());

        model.setOrgUnitPostSalarysMap(new HashMap<>());
        model.setOrgUnitWithChildsSalarysMap(new HashMap<>());
        model.setOrgUnitSalarysMap(new HashMap<>());

        model.setOrgUnitSumTotalSalaryMap(new HashMap<>());
        model.setOrgUnitWithChildsSumTotalSalaryMap(new HashMap<>());
        model.setOrgUnitPostTotalSalaryMap(new HashMap<>());

        model.setOrgUnitMappingsMap(new HashMap<>());
    }

    private static void prepareGeneralData(Model model)
    {
        Set<StaffListItem> alreadyAddedStaffListItems = new HashSet<>();
        Map<CoreCollectionUtils.Pair<Long, Long>, CoreCollectionUtils.Pair<Double, Double>> vacancyRates = new HashMap<>();

        for (StaffListItem staffListItem : model.getRawStaffListItemsList())
        {
            Long staffListItemId = staffListItem.getId();
            OrgUnit orgUnit = staffListItem.getStaffList().getOrgUnit();
            PostBoundedWithQGandQL post = staffListItem.getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL();

            if (!model.getPostSalarysMap().containsKey(staffListItemId))
                model.getPostSalarysMap().put(staffListItemId, staffListItem.getSalary());

            model.getStaffListItemsMap().put(staffListItemId, staffListItem);
            List<StaffListItem> postsList = model.getOrgUnitPostListsMap().get(orgUnit.getId());
            if (null == postsList) postsList = new ArrayList<>();
            postsList.add(staffListItem);
            model.getOrgUnitPostListsMap().put(orgUnit.getId(), postsList);

            orgUnit = getOrgUnitWherePostShouldBeShown(orgUnit, model.getOrgUnitTypesMap(), model.getOrgUnitMappingsMap());

            if (null != orgUnit)
            {
                Long orgUnitId = orgUnit.getId();
                List<StaffListItem> sortedPostsList = model.getOrgUnitPostSortedListsMap().get(orgUnitId);
                if (null == sortedPostsList) sortedPostsList = new ArrayList<>();
                sortedPostsList.add(staffListItem);
                model.getOrgUnitPostSortedListsMap().put(orgUnitId, sortedPostsList);

                CoreCollectionUtils.Pair<Long, Long> vacancyPairKey = getNewPairInstance(orgUnitId, post.getId());
                CoreCollectionUtils.Pair<Double, Double> vacancyPair = vacancyRates.get(vacancyPairKey);
                if (null == vacancyPair)
                {
                    alreadyAddedStaffListItems.add(staffListItem);

                    if (staffListItem.getFinancingSource().equals(finSrcBudgetCatalogItem))
                        vacancyPair = getNewPairInstance(staffListItem.getStaffRate(), 0d);
                    else
                        vacancyPair = getNewPairInstance(0d, staffListItem.getStaffRate());
                }
                if (!alreadyAddedStaffListItems.contains(staffListItem))
                {
                    alreadyAddedStaffListItems.add(staffListItem);

                    if (staffListItem.getFinancingSource().equals(finSrcBudgetCatalogItem))
                    {
                        vacancyPair.setX(vacancyPair.getX() + staffListItem.getStaffRate());
                        vacancyPair.setY(vacancyPair.getY() + 0d);
                    }
                    else
                    {
                        vacancyPair.setX(vacancyPair.getX() + 0d);
                        vacancyPair.setY(vacancyPair.getY() + staffListItem.getStaffRate());
                    }
                }
                vacancyRates.put(vacancyPairKey, vacancyPair);

                if (!model.getPostDataMap().containsKey(staffListItemId))
                    model.getPostDataMap().put(staffListItemId, new String[] { post.getTitle(), post.getProfQualificationGroup().getTitle().length() > 3 ? post.getProfQualificationGroup().getTitle().substring(0, 3) : post.getProfQualificationGroup().getTitle(), post.getQualificationLevel().getShortTitle() });

                if (staffListItem.getFinancingSource().equals(finSrcBudgetCatalogItem))
                {
                    setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitPostBudgetStaffRatesMap(), staffListItem.getStaffRate(), 1, orgUnitId, staffListItemId);
                    setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitPostOffBudgetStaffRatesMap(), 0d, 1, orgUnitId, staffListItemId);
                    setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitPostBudgetStaffRatesMap(), staffListItem.getStaffRate(), 1, orgUnitId, ORG_UNIT_SUM_ID);
                    setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitPostOffBudgetStaffRatesMap(), 0d, 1, orgUnitId, ORG_UNIT_SUM_ID);
                }
                else
                {
                    setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitPostBudgetStaffRatesMap(), 0d, 1, orgUnitId, staffListItemId);
                    setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitPostOffBudgetStaffRatesMap(), staffListItem.getStaffRate(), 1, orgUnitId, staffListItemId);
                    setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitPostBudgetStaffRatesMap(), 0d, 1, orgUnitId, ORG_UNIT_SUM_ID);
                    setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitPostOffBudgetStaffRatesMap(), staffListItem.getStaffRate(), 1, orgUnitId, ORG_UNIT_SUM_ID);
                }

                // Вычисляем полные суммы ставок по каждому из стоящих выше в иерархии подразделений
                OrgUnit parentOrgUnit = orgUnit;
                while (null != parentOrgUnit)
                {
                    if (staffListItem.getFinancingSource().equals(finSrcBudgetCatalogItem))
                    {
                        setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitPostBudgetStaffRatesMap(), staffListItem.getStaffRate(), 1, parentOrgUnit.getId(), ORG_UNIT_WITH_CHILDS_SUM_ID);
                        setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitPostOffBudgetStaffRatesMap(), 0d, 1, parentOrgUnit.getId(), ORG_UNIT_WITH_CHILDS_SUM_ID);
                    }
                    else
                    {
                        setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitPostBudgetStaffRatesMap(), 0d, 1, parentOrgUnit.getId(), ORG_UNIT_WITH_CHILDS_SUM_ID);
                        setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitPostOffBudgetStaffRatesMap(), staffListItem.getStaffRate(), 1, parentOrgUnit.getId(), ORG_UNIT_WITH_CHILDS_SUM_ID);
                    }
                    parentOrgUnit = parentOrgUnit.getParent();
                }

                // Вычисляем полные суммы ставок по ОУ вцелом
                if (staffListItem.getFinancingSource().equals(finSrcBudgetCatalogItem))
                {
                    model.setVuzBudgetStaffRate(model.getVuzBudgetStaffRate() + staffListItem.getStaffRate());
                    model.setVuzOffBudgetStaffRate(model.getVuzOffBudgetStaffRate() + 0d);
                }
                else
                {
                    model.setVuzBudgetStaffRate(model.getVuzBudgetStaffRate() + 0d);
                    model.setVuzOffBudgetStaffRate(model.getVuzOffBudgetStaffRate() + staffListItem.getStaffRate());
                }
            }
        }

        model.setVacancyRates(vacancyRates);

        // Сортируем должности в рамках подразделения
        for (List<StaffListItem> postsList : model.getOrgUnitPostSortedListsMap().values())
        {
            Collections.sort(postsList, POST_TITLE_COMPARATOR);
        }
    }

    private static void prepareEmployeePostsData(Model model)
    {
        for (EmployeePostFakeWrapper rel : model.getRawEmployeePostStaffRatesList())
        {
            Long employeePostStaffRatesRelId = rel.getId();
            OrgUnit orgUnit = rel.getOrgUnit();
            PostBoundedWithQGandQL post = rel.getPostRelation().getPostBoundedWithQGandQL();
            orgUnit = getOrgUnitWherePostShouldBeShown(orgUnit, model.getOrgUnitTypesMap(), model.getOrgUnitMappingsMap());

            if (null != orgUnit)
            {
                Long orgUnitId = orgUnit.getId();
                Map<Long, List<EmployeePostFakeWrapper>> postsMap = model.getOrgUnitEmployeePostStaffRatesListsMap().get(orgUnitId);
                if (null == postsMap) postsMap = new HashMap<>();
                List<EmployeePostFakeWrapper> postsList = postsMap.get(post.getId());
                if (null == postsList) postsList = new ArrayList<>();
                postsList.add(rel);
                //if (rel.getBudgetStaffRate() > 0 && rel.getOffBudgetStaffRate() > 0) postsList.add(rel);

                if (!model.getEmployeePostDataMap().containsKey(employeePostStaffRatesRelId))
                {
                    String postTypeString = "Основная";
                    if (rel.getPostType() == null)
                        postTypeString = "";
                    else
                    {
                        if (UniDefines.POST_TYPE_SECOND_JOB.equals(rel.getPostType().getCode()))
                            postTypeString = "Совместит.";
                        if (UniDefines.POST_TYPE_SECOND_JOB_OUTER.equals(rel.getPostType().getCode()))
                            postTypeString = "Внешний совместит.";
                        if (UniDefines.POST_TYPE_SECOND_JOB_INNER.equals(rel.getPostType().getCode()))
                            postTypeString = "Внутренний совместит.";
                    }

                    if (rel.getBudgetStaffRate() > 0)
                        model.getEmployeePostDataMap().put(new CoreCollectionUtils.Pair<>(employeePostStaffRatesRelId, true), new String[] { rel.getFullFio(), postTypeString, post.getQualificationLevel().getShortTitle(), DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(rel.getBudgetStaffRate()) });
                    if (rel.getOffBudgetStaffRate() > 0)
                        model.getEmployeePostDataMap().put(new CoreCollectionUtils.Pair<>(employeePostStaffRatesRelId, false), new String[] { rel.getFullFio(), postTypeString, post.getQualificationLevel().getShortTitle(), DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(rel.getOffBudgetStaffRate()) });
                }

                postsMap.put(post.getId(), postsList);
                model.getOrgUnitEmployeePostStaffRatesListsMap().put(orgUnitId, postsMap);

                setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitEmployeePostBudgetStaffRatesMap(), rel.getBudgetStaffRate(), 1, orgUnitId, employeePostStaffRatesRelId);
                setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitEmployeePostOffBudgetStaffRatesMap(), rel.getOffBudgetStaffRate(), 1, orgUnitId, employeePostStaffRatesRelId);

                setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitEmployeePostBudgetStaffRatesMap(), rel.getBudgetStaffRate(), 1, orgUnitId, ORG_UNIT_SUM_ID);
                setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitEmployeePostOffBudgetStaffRatesMap(), rel.getOffBudgetStaffRate(), 1, orgUnitId, ORG_UNIT_SUM_ID);

                // Вычисляем полные суммы ставок по каждому из стоящих выше в иерархии подразделений
                OrgUnit parentOrgUnit = orgUnit;
                while (null != parentOrgUnit)
                {
                    setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitEmployeePostBudgetStaffRatesMap(), rel.getBudgetStaffRate(), 1, parentOrgUnit.getId(), ORG_UNIT_WITH_CHILDS_SUM_ID);
                    setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitEmployeePostOffBudgetStaffRatesMap(), rel.getOffBudgetStaffRate(), 1, parentOrgUnit.getId(), ORG_UNIT_WITH_CHILDS_SUM_ID);
                    parentOrgUnit = parentOrgUnit.getParent();
                }

                // Вычисляем полные суммы ставок по вузу в целом
                /*model.setVuzBudgetStaffRate(model.getVuzEmployeePostBudgetStaffRate() + rel.getBudgetStaffRate());
                model.setVuzOffBudgetStaffRate(model.getVuzEmployeePostOffBudgetStaffRate() + rel.getOffBudgetStaffRate());*/
            }
        }

        Map<TripletKey<Long, FinancingSource, FinancingSourceItem>, StaffListAllocationItem> employeePostAllocItemsMap = new HashMap<>();
        //Map<Long, CoreCollectionUtils.Pair<Double, Double>> vacancyRates = new HashMap<Long, CoreCollectionUtils.Pair<Double, Double>>();
        // TODO: We should propose one of two staff rates calculating models

        for (StaffListAllocationItem allocationItem : model.getRawStaffListAllocationItemsList())
        {
            StaffListItem staffListItem = allocationItem.getStaffListItem();
            OrgUnit orgUnit = staffListItem.getStaffList().getOrgUnit();
            orgUnit = getOrgUnitWherePostShouldBeShown(orgUnit, model.getOrgUnitTypesMap(), model.getOrgUnitMappingsMap());
            //Long staffListItemId = staffListItem.getId();

            if (null != orgUnit)
            {
                /*CoreCollectionUtils.Pair<Double, Double> vacancyPair = vacancyRates.get(staffListItemId);
                if (null == vacancyPair) vacancyPair = getNewPairInstance(staffListItem.getBudgetStaffRate(), staffListItem.getOffBudgetStaffRate());
                vacancyPair.setX(vacancyPair.getX() - allocationItem.getBudgetStaffRate());
                vacancyPair.setY(vacancyPair.getY() - allocationItem.getOffBudgetStaffRate());
                vacancyRates.put(staffListItemId, vacancyPair);*/

                if (null != allocationItem.getEmployeePost())
                {
                    employeePostAllocItemsMap.put(new TripletKey<>(allocationItem.getEmployeePost().getId(), allocationItem.getFinancingSource(), allocationItem.getFinancingSourceItem()), allocationItem);

                    //                    Long orgUnitId = orgUnit.getId();
                    //                    setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitEmployeePostBudgetStaffRatesMap(), allocationItem.getBudgetStaffRate(), 1, orgUnitId, ORG_UNIT_SUM_ID);
                    //                    setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitEmployeePostOffBudgetStaffRatesMap(), allocationItem.getOffBudgetStaffRate(), 1, orgUnitId, ORG_UNIT_SUM_ID);
                    //
                    //                    // Вычисляем полные суммы ставок по каждому из стоящих выше в иерархии подразделений
                    //                    OrgUnit parentOrgUnit = orgUnit;
                    //                    while (null != parentOrgUnit)
                    //                    {
                    //                        setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitEmployeePostBudgetStaffRatesMap(), allocationItem.getBudgetStaffRate(), 1, parentOrgUnit.getId(), ORG_UNIT_WITH_CHILDS_SUM_ID);
                    //                        setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitEmployeePostOffBudgetStaffRatesMap(), allocationItem.getOffBudgetStaffRate(), 1, parentOrgUnit.getId(), ORG_UNIT_WITH_CHILDS_SUM_ID);
                    //                        parentOrgUnit = parentOrgUnit.getParent();
                    //                    }
                    //
                    //                    // Вычисляем полные суммы ставок по вузу в целом
                    //                    model.setVuzBudgetStaffRate(model.getVuzEmployeePostBudgetStaffRate() + allocationItem.getBudgetStaffRate());
                    //                    model.setVuzOffBudgetStaffRate(model.getVuzEmployeePostOffBudgetStaffRate() + allocationItem.getOffBudgetStaffRate());
                }
                if (allocationItem.isReserve())
                {
                    employeePostAllocItemsMap.put(new TripletKey<>(allocationItem.getId(), allocationItem.getFinancingSource(), allocationItem.getFinancingSourceItem()), allocationItem);
                }
            }
        }
        model.setEmployeePostAllocItemsMap(employeePostAllocItemsMap);
        //model.setVacancyRates(vacancyRates);

        // Сортируем сотрудников по ФИО сотрудника на должности в рамках подразделения
        for (Map<Long, List<EmployeePostFakeWrapper>> wrappersList : model.getOrgUnitEmployeePostStaffRatesListsMap().values())
        {
            for (List<EmployeePostFakeWrapper> postsList : wrappersList.values())
            {
                Collections.sort(postsList, EMPLOYEE_POST_STAFF_RATES_FULL_FIO_COMPARATOR);
            }
        }
    }

    /**
     * Сводит выплаты в один мап с учетом перестановок должностей по оргюнитам в соответствии с настройкой
     */
    private static void reorganizePayments(Model model)
    {
        Map<EmployeePostFakeWrapper, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> paymentsRubleValuesMap = new HashMap<>();
        Map<EmployeePostFakeWrapper, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> paymentsFullRubleValuesMap = new HashMap<>();

        Map<Long, Map<TripletKey<Long, Long, Long>, List<StaffListPaymentBase>>> orgUnitPostPaymentsMap = new HashMap<>();
        Map<TripletKey<Long, Long, Long>, List<StaffListPaymentBase>> employeePostIndividualPaymentsMap = new HashMap<>();
        Map<StaffListPaymentBase, List<EmployeePostFakeWrapper>> fullSalaryPaymentsToStaffRatesMap = new HashMap<>();

        for (StaffListPaymentBase item : model.getRawStaffListPaymentsList())
        {
            if (item instanceof StaffListFakePayment && (null == ((StaffListFakePayment)item).getStaffListAllocationItem() || ((StaffListFakePayment)item).getStaffListAllocationItem().getEmployeePost() == null))
                if (!(null != ((StaffListFakePayment)item).getStaffListAllocationItem() && ((StaffListFakePayment)item).getStaffListAllocationItem().isReserve()))
                continue;

            Long employeePostId = null;
            FinancingSource financingSource = null;
            FinancingSourceItem financingSourceItem = null;

            if (item instanceof StaffListFakePayment)
            {
                StaffListAllocationItem allocItem = ((StaffListFakePayment)item).getStaffListAllocationItem();
                if (null != allocItem)
                    employeePostId = allocItem.isReserve() ? allocItem.getId() : allocItem.getEmployeePost().getId();
                //TODO REFACTORING
                financingSource = allocItem.getFinancingSource();
                financingSourceItem = allocItem.getFinancingSourceItem();
            }

            if (null != employeePostId)
            {
                List<StaffListPaymentBase> individualPaymentsList = employeePostIndividualPaymentsMap.get(new TripletKey<>(employeePostId, financingSource.getId(), financingSourceItem != null ? financingSourceItem.getId() : null));
                if (null == individualPaymentsList) individualPaymentsList = new ArrayList<>();
                individualPaymentsList.add(item);
                employeePostIndividualPaymentsMap.put(new TripletKey<>(employeePostId, financingSource.getId(), financingSourceItem != null ? financingSourceItem.getId() : null), individualPaymentsList);
            }
            else
            {
                //TODO REFACTORING
                Long orgUnitId = item.getStaffListItem().getStaffList().getOrgUnit().getId();
                Long orgUnitTypePostRelationId = item.getStaffListItem().getOrgUnitPostRelation().getOrgUnitTypePostRelation().getId();
                Long finSrcId = item.getStaffListItem().getFinancingSource().getId();
                Long finSrcItemId = item.getStaffListItem().getFinancingSourceItem() != null ? item.getStaffListItem().getFinancingSourceItem().getId() : null;

                Map<TripletKey<Long, Long, Long>, List<StaffListPaymentBase>> postPaymentsMap = orgUnitPostPaymentsMap.get(orgUnitId);
                if (null == postPaymentsMap) postPaymentsMap = new HashMap<>();

                List<StaffListPaymentBase> paymentsList = postPaymentsMap.get(new TripletKey<>(orgUnitTypePostRelationId, finSrcId, finSrcItemId));
                if (null == paymentsList) paymentsList = new ArrayList<>();

                paymentsList.add(item);
                postPaymentsMap.put(new TripletKey<>(orgUnitTypePostRelationId, finSrcId, finSrcItemId), paymentsList);
                orgUnitPostPaymentsMap.put(orgUnitId, postPaymentsMap);
            }
        }

        Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> basePaymentValue = new HashMap<>();

        for (EmployeePostFakeWrapper rel : model.getRawEmployeePostStaffRatesList())
        {
            Long orgUnitId = rel.getOrgUnit().getId();
            Long finSrcId = rel.getFinancingSource().getId();
            Long finSrcItemId = rel.getFinancingSourceItem() != null ? rel.getFinancingSourceItem().getId() : null;
            OrgUnitTypePostRelation orgUnitTypePostRelation = rel.getPostRelation();
            List<StaffListPaymentBase> employeePostPayments = new ArrayList<>();

            if (null != orgUnitPostPaymentsMap.get(orgUnitId) && null != orgUnitPostPaymentsMap.get(orgUnitId).get(new TripletKey<>(orgUnitTypePostRelation.getId(), finSrcId, finSrcItemId)))
                employeePostPayments.addAll(orgUnitPostPaymentsMap.get(orgUnitId).get(new TripletKey<>(orgUnitTypePostRelation.getId(), finSrcId, finSrcItemId)));

            if (null != employeePostIndividualPaymentsMap.get(new TripletKey<>(rel.getEmployeePostId(), finSrcId, finSrcItemId)))
                employeePostPayments.addAll(employeePostIndividualPaymentsMap.get(new TripletKey<>(rel.getEmployeePostId(), finSrcId, finSrcItemId)));

            for (StaffListPaymentBase payment : employeePostPayments)
            {
                Long paymentId = payment.getPayment().getId();
                if (model.getOtherPaymentIds().contains(paymentId)) paymentId = OTHER_PAYMENTS_ID;

                Double budgetStaffRate = rel.getBudgetStaffRate();
                Double offBudgetStaffRate = rel.getOffBudgetStaffRate();

                StaffListAllocationItem allocationItem = model.getEmployeePostAllocItemsMap().get(new TripletKey<>(rel.getEmployeePostId(), rel.getFinancingSource(), rel.getFinancingSourceItem()));
                if (null != allocationItem)
                {
                    if (payment.getFinancingSource().equals(finSrcBudgetCatalogItem))
                    {
                        budgetStaffRate = allocationItem.getStaffRate();
                        offBudgetStaffRate = 0d;
                    }
                    else
                    {
                        budgetStaffRate = 0d;
                        offBudgetStaffRate = allocationItem.getStaffRate();
                    }
                }

                if (employeePostIndividualPaymentsMap.get(new TripletKey<>(rel.getEmployeePostId(), finSrcId, finSrcItemId)) != null
                        && employeePostIndividualPaymentsMap.get(new TripletKey<>(rel.getEmployeePostId(), finSrcId, finSrcItemId)).contains(payment))
                {
                    if (UniempDefines.FINANCING_SOURCE_BUDGET.equals(payment.getFinancingSource().getCode()))
                    {
                        budgetStaffRate = ((StaffListFakePayment) payment).getStaffRate();
                        offBudgetStaffRate = 0d;
                    }
                    else
                    {
                        budgetStaffRate = 0d;
                        offBudgetStaffRate = ((StaffListFakePayment) payment).getStaffRate();
                    }
                }

                if (UniempDefines.PAYMENT_UNIT_FULL_PERCENT.equals(payment.getPayment().getPaymentUnit().getCode()))
                {
                    List<EmployeePostFakeWrapper> staffRatesRelsList = fullSalaryPaymentsToStaffRatesMap.get(payment);
                    if (null == staffRatesRelsList)
                        staffRatesRelsList = new ArrayList<>();
                    staffRatesRelsList.add(rel);
                    fullSalaryPaymentsToStaffRatesMap.put(payment, staffRatesRelsList);
                }
                else
                {
                    Double budgetBaseSalaryPart = budgetStaffRate * payment.getStaffListItem().getSalary();
                    Double offBudgetBaseSalaryPart = offBudgetStaffRate * payment.getStaffListItem().getSalary();

                    Map<Long, CoreCollectionUtils.Pair<Double, Double>> employeePostPaymentValuesMap = paymentsRubleValuesMap.get(rel);
                    if (null == employeePostPaymentValuesMap)
                        employeePostPaymentValuesMap = new HashMap<>();

                    CoreCollectionUtils.Pair<Double, Double> paymentsSum = employeePostPaymentValuesMap.get(paymentId);
                    if (null == paymentsSum)
                        paymentsSum = new CoreCollectionUtils.Pair<>(0d, 0d);

                    Double budgetPaymentValue = 0d;
                    Double offBudgetPaymentValue = 0d;

                    if (UniempDefines.PAYMENT_UNIT_RUBLES.equals(payment.getPayment().getPaymentUnit().getCode()))
                    {
                        if (null == payment.getFinancingSource())
                        {
                            double budgetPercent = budgetStaffRate / (budgetStaffRate + offBudgetStaffRate);
                            double offBudgetPercent = 1 - budgetPercent;

                            budgetPaymentValue = payment.getAmount() * (payment.getPayment().isDoesntDependOnStaffRate() ? budgetPercent : budgetStaffRate);
                            offBudgetPaymentValue = payment.getAmount() * (payment.getPayment().isDoesntDependOnStaffRate() ? offBudgetPercent : offBudgetStaffRate);
                        }
                        else if (UniempDefines.FINANCING_SOURCE_BUDGET.equals(payment.getFinancingSource().getCode()))
                            budgetPaymentValue = payment.getAmount() * (payment.getPayment().isDoesntDependOnStaffRate() ? 1 : budgetStaffRate + offBudgetStaffRate);
                        else
                            offBudgetPaymentValue = payment.getAmount() * (payment.getPayment().isDoesntDependOnStaffRate() ? 1 : (budgetStaffRate + offBudgetStaffRate));
                    }
                    else if (UniempDefines.PAYMENT_UNIT_COEFFICIENT.equals(payment.getPayment().getPaymentUnit().getCode()))
                    {
                        if (null == payment.getFinancingSource())
                        {
                            budgetPaymentValue = Math.round(budgetBaseSalaryPart * payment.getAmount() * 100) / 100d;
                            offBudgetPaymentValue = Math.round(offBudgetBaseSalaryPart * payment.getAmount() * 100) / 100d;
                        }
                        else if (UniempDefines.FINANCING_SOURCE_BUDGET.equals(payment.getFinancingSource().getCode()))
                            budgetPaymentValue = Math.round((budgetBaseSalaryPart + offBudgetBaseSalaryPart) * payment.getAmount() * 100) / 100d;
                        else
                            offBudgetPaymentValue = Math.round((budgetBaseSalaryPart + offBudgetBaseSalaryPart) * payment.getAmount() * 100) / 100d;
                    }
                    else if (UniempDefines.PAYMENT_UNIT_BASE_PERCENT.equals(payment.getPayment().getPaymentUnit().getCode()))
                    {
                        if (null == payment.getFinancingSource())
                        {
                            budgetPaymentValue = Math.round(budgetBaseSalaryPart * payment.getAmount()) / 100d;
                            offBudgetPaymentValue = Math.round(offBudgetBaseSalaryPart * payment.getAmount()) / 100d;
                        }
                        else if (UniempDefines.FINANCING_SOURCE_BUDGET.equals(payment.getFinancingSource().getCode()))
                            budgetPaymentValue = Math.round((budgetBaseSalaryPart + offBudgetBaseSalaryPart) * payment.getAmount()) / 100d;
                        else
                            offBudgetPaymentValue = Math.round((budgetBaseSalaryPart + offBudgetBaseSalaryPart) * payment.getAmount()) / 100d;
                    }

                    paymentsSum.setX(paymentsSum.getX() + budgetPaymentValue);
                    paymentsSum.setY(paymentsSum.getY() + offBudgetPaymentValue);

                    {
                        Map<Long, CoreCollectionUtils.Pair<Double, Double>> valueMap = basePaymentValue.get(rel.getId());
                        if (valueMap == null)
                            valueMap = new HashMap<>();
                        CoreCollectionUtils.Pair<Double, Double> pair = valueMap.get(payment.getId());
                        if (pair == null)
                            pair = new CoreCollectionUtils.Pair<>(0d, 0d);
                        pair.setX(pair.getX() + budgetPaymentValue);
                        pair.setY(pair.getY() + offBudgetPaymentValue);
                        valueMap.put(payment.getId(), pair);
                        basePaymentValue.put(rel.getId(), valueMap);
                    }

                    employeePostPaymentValuesMap.put(paymentId, paymentsSum);
                    paymentsRubleValuesMap.put(rel, employeePostPaymentValuesMap);
                }
            }
        }

        for (Map.Entry<StaffListPaymentBase, List<EmployeePostFakeWrapper>> staffRatesEntry : fullSalaryPaymentsToStaffRatesMap.entrySet())
        {
            Long paymentId = staffRatesEntry.getKey().getPayment().getId();
            if (model.getOtherPaymentIds().contains(paymentId)) paymentId = OTHER_PAYMENTS_ID;

            for (EmployeePostFakeWrapper rel : staffRatesEntry.getValue())
            {
                Double budgetStaffRate = rel.getBudgetStaffRate();
                Double offBudgetStaffRate = rel.getOffBudgetStaffRate();

                StaffListAllocationItem allocationItem = model.getEmployeePostAllocItemsMap().get(new TripletKey<>(rel.getEmployeePostId(), rel.getFinancingSource(), rel.getFinancingSourceItem()));
                if (null != allocationItem)
                {
                    if (staffRatesEntry.getKey().getFinancingSource().equals(finSrcBudgetCatalogItem))
                    {
                        budgetStaffRate = allocationItem.getStaffRate();
                        offBudgetStaffRate = 0d;
                    }
                    else
                    {
                        budgetStaffRate = 0d;
                        offBudgetStaffRate = allocationItem.getStaffRate();
                    }
                }

                Double budgetBaseSalaryPart = budgetStaffRate * staffRatesEntry.getKey().getStaffListItem().getSalary();
                Double offBudgetBaseSalaryPart = offBudgetStaffRate * staffRatesEntry.getKey().getStaffListItem().getSalary();

                Map<Long, CoreCollectionUtils.Pair<Double, Double>> employeePostPaymentValuesMap = paymentsFullRubleValuesMap.get(rel);
                if (null == employeePostPaymentValuesMap) employeePostPaymentValuesMap = new HashMap<>();

                CoreCollectionUtils.Pair<Double, Double> fullPaymentsSum = employeePostPaymentValuesMap.get(paymentId);
                if (null == fullPaymentsSum) fullPaymentsSum = new CoreCollectionUtils.Pair<>(0d, 0d);

                CoreCollectionUtils.Pair<Double, Double> paymentsSum = getNewPairInstance(0d, 0d);
                if (basePaymentValue.containsKey(rel.getId()))
                {
                    Map<Long, CoreCollectionUtils.Pair<Double, Double>> paymentValueMap = basePaymentValue.get(rel.getId());

                    paymentsSum = calculatePaymentSumm(staffRatesEntry.getKey(), paymentValueMap, fullSalaryPaymentsToStaffRatesMap.keySet());

//                    for (CoreCollectionUtils.Pair<Double, Double> paymentEntry : paymentsRubleValuesMap.get(rel).values())
//                    {
//                        paymentsSum.setX(paymentsSum.getX() + paymentEntry.getX());
//                        paymentsSum.setY(paymentsSum.getY() + paymentEntry.getY());
//                    }
                }

                fullPaymentsSum.setX(fullPaymentsSum.getX() + Math.round((paymentsSum.getX() + budgetBaseSalaryPart) * staffRatesEntry.getKey().getAmount()) / 100d);
                fullPaymentsSum.setY(fullPaymentsSum.getY() + Math.round((paymentsSum.getY() + offBudgetBaseSalaryPart) * staffRatesEntry.getKey().getAmount()) / 100d);

                employeePostPaymentValuesMap.put(paymentId, fullPaymentsSum);
                paymentsFullRubleValuesMap.put(rel, employeePostPaymentValuesMap);
            }

        }

        model.setPaymentsRubleValuesMap(paymentsRubleValuesMap);
        model.setPaymentsFullRubleValuesMap(paymentsFullRubleValuesMap);
    }

    /**
     * Подготавливает данные о надбавках и доплатах
     */
    private static void preparePaymentsData(Model model)
    {
        reorganizePayments(model);


        Set<EmployeePostFakeWrapper> alreadyAddedWrappers = new HashSet<>();

        // TODO: potentially weak place
        for (Map.Entry<Long, Map<Long, Double>> orgUnitEntry : model.getOrgUnitPostBudgetStaffRatesMap().entrySet())//model.getOrgUnitPostStaffRatesMap().entrySet())
        {
            OrgUnit orgUnit = getOrgUnitWherePostShouldBeShown(model.getOrgUnitsMap().get(orgUnitEntry.getKey()), model.getOrgUnitTypesMap(), model.getOrgUnitMappingsMap());

            if (null != orgUnit)
            {
                Long orgUnitId = orgUnit.getId();
                Map<Long, List<EmployeePostFakeWrapper>> relsMap = model.getOrgUnitEmployeePostStaffRatesListsMap().get(orgUnitId);
                if (null == relsMap) relsMap = new HashMap<>();

                for (Long staffListItemId : orgUnitEntry.getValue().keySet())
                {
                    StaffListItem slItem = model.getStaffListItemsMap().get(staffListItemId);
                    if (null == slItem) continue;

                    Long postBoundedId = slItem.getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL().getId();
                    List<EmployeePostFakeWrapper> relsList = relsMap.get(slItem.getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL().getId());
                    if (null == relsList) relsList = new ArrayList<>();

                    CoreCollectionUtils.Pair<Long, Long> vacancyPairKey = getNewPairInstance(orgUnitId, postBoundedId);
                    CoreCollectionUtils.Pair<Double, Double> vacancyPair = model.getVacancyRates().get(vacancyPairKey);

                    for (EmployeePostFakeWrapper rel : relsList)
                    {
                        if (alreadyAddedWrappers.contains(rel)) continue;

                        if (null == vacancyPair)
                        {
                            if (slItem.getFinancingSource().equals(finSrcBudgetCatalogItem))
                                vacancyPair = getNewPairInstance(slItem.getStaffRate(), 0d);
                            else
                                vacancyPair = getNewPairInstance(0d, slItem.getStaffRate());
                        }

                        alreadyAddedWrappers.add(rel);
                        vacancyPair.setX(vacancyPair.getX() - rel.getBudgetStaffRate());
                        vacancyPair.setY(vacancyPair.getY() - rel.getOffBudgetStaffRate());

                        Map<Long, CoreCollectionUtils.Pair<Double, Double>> paymentsMap = new HashMap<>();

                        if (model.getPaymentsRubleValuesMap().containsKey(rel))
                            paymentsMap.putAll(model.getPaymentsRubleValuesMap().get(rel));

                        if (model.getPaymentsFullRubleValuesMap().containsKey(rel))
                            paymentsMap.putAll(model.getPaymentsFullRubleValuesMap().get(rel));

                        for (Map.Entry<Long, CoreCollectionUtils.Pair<Double, Double>> paymentEntry : paymentsMap.entrySet())
                        {
                            Long paymentId = paymentEntry.getKey();
                            CoreCollectionUtils.Pair<Double, Double> paymentItem = paymentEntry.getValue();

                            setTwoLevelsPairMapValuesInSafeMode(model.getOrgUnitPostPaymentsMap(), paymentItem, 1, rel.getId(), paymentId);

                            // Вычисление месячного фонда оплаты труда с надбавками для должности
                            CoreCollectionUtils.Pair<Double, Double> monthSalary = model.getOrgUnitPostTotalSalaryMap().get(rel);
                            if (null == monthSalary) monthSalary = getNewPairInstance(0d, 0d);
                            monthSalary.setX(monthSalary.getX() + paymentItem.getX());
                            monthSalary.setY(monthSalary.getY() + paymentItem.getY());
                            model.getOrgUnitPostTotalSalaryMap().put(rel, monthSalary);

                            // Вычисляем сумму надбавок должностям по подразделению для указанной выплаты
                            setTwoLevelsPairMapValuesInSafeMode(model.getOrgUnitSumPaymentsMap(), paymentItem, 1, orgUnitId, paymentId);

                            // Вычисление месячного фонда оплаты труда с надбавками подразделению
                            setSingleLevelPairMapValuesInSafeMode(model.getOrgUnitSumTotalSalaryMap(), paymentItem, 1, orgUnitId);

                            // Вычисляем полные суммы выплат по каждому из стоящих выше в иерархии подразделений
                            OrgUnit parentOrgUnit = orgUnit.getParent();
                            while (null != parentOrgUnit)
                            {
                                setTwoLevelsPairMapValuesInSafeMode(model.getOrgUnitWithChildsSumPaymentsMap(), paymentItem, 1, parentOrgUnit.getId(), paymentId);
                                parentOrgUnit = parentOrgUnit.getParent();
                            }

                            // Вычисляем полные суммы выплат в целом по ОУ
                            //if (model.getSerializedOrgstructure().contains(model.getOrgUnitsMap().get(orgUnitId)))
                            setSingleLevelPairMapValuesInSafeMode(model.getVuzSumPaymentsMap(), paymentItem, 1, paymentId);
                        }
                        model.getVacancyRates().put(vacancyPairKey, vacancyPair);
                    }
                }
            }
        }

        Map<Long, CoreCollectionUtils.Pair<Double, Double>> orgUnitSumVacancyRates = new HashMap<>();
        for (Map.Entry<CoreCollectionUtils.Pair<Long, Long>, CoreCollectionUtils.Pair<Double, Double>> vacancyEntry : model.getVacancyRates().entrySet())
        {
            OrgUnit orgUnit = model.getOrgUnitsMap().get(vacancyEntry.getKey().getX());
            orgUnit = getOrgUnitWherePostShouldBeShown(orgUnit, model.getOrgUnitTypesMap(), model.getOrgUnitMappingsMap());

            if (null != orgUnit)
            {
                Long orgnitId = orgUnit.getId();
                CoreCollectionUtils.Pair<Double, Double> sumVacancy = orgUnitSumVacancyRates.get(orgnitId);
                if (null == sumVacancy) sumVacancy = getNewPairInstance(0d, 0d);
                sumVacancy.setX(sumVacancy.getX() + vacancyEntry.getValue().getX());
                sumVacancy.setY(sumVacancy.getY() + vacancyEntry.getValue().getY());
                orgUnitSumVacancyRates.put(orgnitId, sumVacancy);
            }
        }

        model.setOrgUnitSumVacancyRates(orgUnitSumVacancyRates);
    }

    /**
     * Вычисляет месячный фонд оплаты труда (без надбавок и с ними) по каждой должности,
     * суммарный месячный фонд оплаты труда (без надбавок и с ними) по подразделениям,
     * суммарный месячный фонд оплаты труда (без надбавок и с ними) по подразделениям с учетом дочерних подразделений.
     */
    private static void prepareSummaryData(Model model)
    {
        for (Map.Entry<Long, List<StaffListItem>> entry : model.getOrgUnitPostListsMap().entrySet())
        {
            OrgUnit orgUnit = model.getOrgUnitsMap().get(entry.getKey());
            orgUnit = getOrgUnitWherePostShouldBeShown(orgUnit, model.getOrgUnitTypesMap(), model.getOrgUnitMappingsMap());

            if (null != orgUnit)
            {
                Long orgUnitId = entry.getKey();
                CoreCollectionUtils.Pair<Double, Double> orgUnitSumSalary = model.getOrgUnitSalarysMap().get(orgUnitId);
                CoreCollectionUtils.Pair<Double, Double> orgUnitSumTotalSalary = model.getOrgUnitSumTotalSalaryMap().get(orgUnitId);
                if (null == orgUnitSumTotalSalary) orgUnitSumTotalSalary = getNewPairInstance(0d, 0d);
                if (null == orgUnitSumSalary) orgUnitSumSalary = getNewPairInstance(0d, 0d);

                List<StaffListItem> list = entry.getValue();
                Map<Long, List<EmployeePostFakeWrapper>> relsMap = model.getOrgUnitEmployeePostStaffRatesListsMap().get(orgUnitId);
                if (null == relsMap) relsMap = new HashMap<>();
                Set<Long> alreadyAddedPosts = new HashSet<>();

                List<Long> postList = new ArrayList<>();
                for (StaffListItem staffListItem : list)
                {
                    Long postId = staffListItem.getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL().getId();
                    if (!postList.contains(postId))
                        postList.add(postId);
                    else
                        continue;

                    alreadyAddedPosts.add(postId);
                    List<EmployeePostFakeWrapper> relsList = relsMap.get(postId);
                    if (null == relsList) relsList = new ArrayList<>();

                    for (EmployeePostFakeWrapper rel : relsList)
                    {

                        // Вычисление месячного фонда оплаты труда по тарифу для должномти
                        Double budgetRatedSalary = staffListItem.getSalary() * rel.getBudgetStaffRate();
                        Double offBudgetRatedSalary = staffListItem.getSalary() * rel.getOffBudgetStaffRate();
                        model.getOrgUnitPostSalarysMap().put(rel, getNewPairInstance(budgetRatedSalary, offBudgetRatedSalary));
                        orgUnitSumSalary.setX(orgUnitSumSalary.getX() + budgetRatedSalary);
                        orgUnitSumSalary.setY(orgUnitSumSalary.getY() + offBudgetRatedSalary);

                        // Вычисление месячного фонда оплаты труда с надбавками для должности
                        CoreCollectionUtils.Pair<Double, Double> postTotalSalary = model.getOrgUnitPostTotalSalaryMap().get(rel);
                        if (null == postTotalSalary) postTotalSalary = getNewPairInstance(0d, 0d);

                        postTotalSalary.setX(postTotalSalary.getX() + budgetRatedSalary);
                        postTotalSalary.setY(postTotalSalary.getY() + offBudgetRatedSalary);
                        model.getOrgUnitPostTotalSalaryMap().put(rel, postTotalSalary);
                        orgUnitSumTotalSalary.setX(orgUnitSumTotalSalary.getX() + budgetRatedSalary);
                        orgUnitSumTotalSalary.setY(orgUnitSumTotalSalary.getY() + offBudgetRatedSalary);
                    }
                }

                // Дополняем суммы по каждому подразделению и вузу в целом окладами работников, невошедших в штатное расписание
                //Set<EmployeePostFakeWrapper> alreadyAddedRels = new HashSet<EmployeePostFakeWrapper>();
                for(Map.Entry<Long, List<EmployeePostFakeWrapper>> relsEntry : relsMap.entrySet())
                {
                    if(!alreadyAddedPosts.contains(relsEntry.getKey()))
                    {
                        List<EmployeePostFakeWrapper> orphanRelsList = relsEntry.getValue();
                        if (null == orphanRelsList) orphanRelsList = new ArrayList<>();

                        for (EmployeePostFakeWrapper rel : orphanRelsList)
                        {
                            /*if(alreadyAddedRels.contains(rel)) continue;
                            alreadyAddedRels.add(rel);*/

                            double sumStaffRate = rel.getBudgetStaffRate() + rel.getOffBudgetStaffRate();
                            double offBudgetStaffRatePart = 0 == sumStaffRate ? 0d : rel.getOffBudgetStaffRate() / (sumStaffRate);
                            double budgetStaffRatePart = 0 == sumStaffRate ? 0d : rel.getBudgetStaffRate() / (sumStaffRate);

                            // Вычисление месячного фонда оплаты труда по тарифу для должномти
                            Double budgetRatedSalary = rel.getSalary() * budgetStaffRatePart;
                            Double offBudgetRatedSalary = rel.getSalary() * offBudgetStaffRatePart;
                            model.getOrgUnitPostSalarysMap().put(rel, getNewPairInstance(budgetRatedSalary, offBudgetRatedSalary));
                            orgUnitSumSalary.setX(orgUnitSumSalary.getX() + budgetRatedSalary);
                            orgUnitSumSalary.setY(orgUnitSumSalary.getY() + offBudgetRatedSalary);

                            // Вычисление месячного фонда оплаты труда с надбавками для должности
                            CoreCollectionUtils.Pair<Double, Double> postTotalSalary = model.getOrgUnitPostTotalSalaryMap().get(rel);
                            if (null == postTotalSalary) postTotalSalary = getNewPairInstance(0d, 0d);

                            postTotalSalary.setX(postTotalSalary.getX() + budgetRatedSalary);
                            postTotalSalary.setY(postTotalSalary.getY() + offBudgetRatedSalary);
                            model.getOrgUnitPostTotalSalaryMap().put(rel, postTotalSalary);
                            orgUnitSumTotalSalary.setX(orgUnitSumTotalSalary.getX() + budgetRatedSalary);
                            orgUnitSumTotalSalary.setY(orgUnitSumTotalSalary.getY() + offBudgetRatedSalary);
                        }
                    }
                }

                model.getOrgUnitSalarysMap().put(orgUnitId, orgUnitSumSalary);
                model.getOrgUnitSumTotalSalaryMap().put(orgUnitId, orgUnitSumTotalSalary);

                model.getVuzSumSalary().setX(model.getVuzSumSalary().getX() + orgUnitSumSalary.getX());
                model.getVuzSumSalary().setY(model.getVuzSumSalary().getY() + orgUnitSumSalary.getY());
                model.getVuzSumTotalSalary().setX(model.getVuzSumTotalSalary().getX() + orgUnitSumTotalSalary.getX());
                model.getVuzSumTotalSalary().setY(model.getVuzSumTotalSalary().getY() + orgUnitSumTotalSalary.getY());

                // Вычисление суммарных значений для подразделений, включая дочерние подразделения
                OrgUnit parent = model.getOrgUnitsMap().get(orgUnitId);
                while (null != parent)
                {
                    // Вычисление месячного фонда оплаты труда по подразделению, включая дочерние подразделения
                    setSingleLevelPairMapValuesInSafeMode(model.getOrgUnitWithChildsSalarysMap(), orgUnitSumSalary, 1, parent.getId());

                    // Вычисление месячного фонда оплаты труда с надбавками по подразделению, включая дочерние подразделения
                    setSingleLevelPairMapValuesInSafeMode(model.getOrgUnitWithChildsSumTotalSalaryMap(), orgUnitSumTotalSalary, 1, parent.getId());

                    parent = parent.getParent();
                }
            }
        }
    }

    /**
     * Подготавливает визуальные данные для вывода в печатную форму отчета
     */
    public static void renderTableData(Model model)
    {
        // Begin rows rendering
        List<String[]> data = new ArrayList<>();
        Map<Long, OrgUnit> parentsMap = new HashMap<>();
        //Map<Long, Integer> orgUnitLevelsMap = new HashMap<>();
        List<RtfTableRowStyle> rowStyles = new ArrayList<>();

        for (OrgUnit orgUnit : model.getSerializedOrgstructure())
        {
            int level = -1;
            OrgUnit parent = orgUnit.getParent();

            while (null != parent)
            {
                level++;
                parent = parent.getParent();
            }

            if (level == 0)
            {
                parentsMap.put(orgUnit.getId(), null);
            }
            else if (null != orgUnit.getParent() && parentsMap.containsKey(orgUnit.getParent().getId()))
                parentsMap.put(orgUnit.getId(), orgUnit.getParent());

            //orgUnitLevelsMap.put(orgUnit.getId(), level);

            /*{
                // Выводим стори "Итого с дочерними подразделениями" для всех подразделений более
                // высокого уровня, чем последний из отрисованных
                if (prevLevel > level)
                {
                    OrgUnit prevOrgUnit = model.getSerializedOrgstructure().get(model.getSerializedOrgstructure().indexOf(orgUnit) - 1);
                    OrgUnit prevParentOrgUnit = parentsMap.get(prevOrgUnit.getId());

                    while (null != prevParentOrgUnit && orgUnitLevelsMap.get(prevParentOrgUnit.getId()) >= level)
                    {
                        if (null == model.getActivityType())
                        {
                            data.add(generateTotalOrgUnitReportLine(model, "Итого по {orgUnitType_D} (бюджет):", prevParentOrgUnit, ORG_UNIT_WITH_CHILDS_SUM_ID, model.getOrgUnitWithChildsSalarysMap(), model.getOrgUnitWithChildsSumTotalSalaryMap(), model.getOrgUnitWithChildsSumPaymentsMap(), ACTIVITY_TYPE_BUDGET));
                            data.add(generateTotalOrgUnitReportLine(model, "Итого по {orgUnitType_D} (вне бюджета):", prevParentOrgUnit, ORG_UNIT_WITH_CHILDS_SUM_ID, model.getOrgUnitWithChildsSalarysMap(), model.getOrgUnitWithChildsSumTotalSalaryMap(), model.getOrgUnitWithChildsSumPaymentsMap(), ACTIVITY_TYPE_OFF_BUDGET));
                            data.add(generateTotalOrgUnitReportLine(model, "Итого по {orgUnitType_D}:", prevParentOrgUnit, ORG_UNIT_WITH_CHILDS_SUM_ID, model.getOrgUnitWithChildsSalarysMap(), model.getOrgUnitWithChildsSumTotalSalaryMap(), model.getOrgUnitWithChildsSumPaymentsMap(), null));
                            rowStyles.add(StaffListReportUtil.TOTAL_ROW_STYLE);
                            rowStyles.add(StaffListReportUtil.TOTAL_ROW_STYLE);
                            rowStyles.add(StaffListReportUtil.TOTAL_ROW_STYLE);
                        }
                        else
                        {
                            data.add(generateTotalOrgUnitReportLine(model, "Итого по {orgUnitType_D} (" + (ACTIVITY_TYPE_BUDGET.equals(model.getActivityType().getId()) ? "бюджет" : "вне бюджета") + "):", prevParentOrgUnit, ORG_UNIT_WITH_CHILDS_SUM_ID, model.getOrgUnitWithChildsSalarysMap(), model.getOrgUnitWithChildsSumTotalSalaryMap(), model.getOrgUnitWithChildsSumPaymentsMap(), model.getActivityType().getId()));
                            rowStyles.add(StaffListReportUtil.TOTAL_ROW_STYLE);
                        }
                        prevParentOrgUnit = parentsMap.get(prevParentOrgUnit.getId());
                    }
                }
                else if(model.getSerializedOrgstructure().indexOf(orgUnit) == model.getSerializedOrgstructure().size() - 1)
                {
                    if(level > 0) lastOrgUnit = orgUnit;
                }
            }*/

            OrgUnitTypeWrapper ouTypeWrapper = model.getOrgUnitTypesMap().get(orgUnit.getOrgUnitType().getId());
            if (null == ouTypeWrapper || ouTypeWrapper.isShowChildOrgUnits())
            {
                // Выводим строку с названием подразделения, с применением стиля соответствующего уровню подразделения в оргструктуре
                data.add(new String[] { null != orgUnit.getNominativeCaseTitle() ? orgUnit.getNominativeCaseTitle() : orgUnit.getFullTitle() });
                rowStyles.add(ORG_UNIT_HEADER_ROW_STYLES[level]);

                // Выводим строки по должностям подразделений
                Map<Long, List<EmployeePostFakeWrapper>> relsMap = model.getOrgUnitEmployeePostStaffRatesListsMap().get(orgUnit.getId());
                if (null == relsMap) relsMap = new HashMap<>();
                Set<EmployeePostFakeWrapper> alreadyAddedWrappers = new HashSet<>();
                Set<CoreCollectionUtils.Pair<Long, Long>> alreadyAddedPosts = new HashSet<>();

                if (model.getOrgUnitPostListsMap().containsKey(orgUnit.getId()))
                {
                    for (StaffListItem staffListItem : model.getOrgUnitPostSortedListsMap().get(orgUnit.getId()))
                    {
                        List<EmployeePostFakeWrapper> relsList = relsMap.get(staffListItem.getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL().getId());
                        if (null == relsList) relsList = new ArrayList<>();

                        for (EmployeePostFakeWrapper wrapper : relsList)
                        {
                            if (alreadyAddedWrappers.contains(wrapper)) continue;

                            alreadyAddedWrappers.add(wrapper);
                            List<String[]> staffListItemLines = generatePostReportLine(model, orgUnit.getId(), staffListItem.getId(), wrapper);
                            for (int i = 0; i < staffListItemLines.size(); i += 2)
                            {
                                rowStyles.add(POST_ROW_STYLE); // staff list post
                                rowStyles.add(null); // employee post
                            }
                            data.addAll(staffListItemLines);
                        }
                    }

                    for (List<EmployeePostFakeWrapper> wrappersList : relsMap.values())
                    {
                        for (EmployeePostFakeWrapper wrapper : wrappersList)
                        {
                            if (alreadyAddedWrappers.contains(wrapper)) continue;

                            alreadyAddedWrappers.add(wrapper);
                            List<String[]> staffListItemLines = generatePostReportLine(model, orgUnit.getId(), null, wrapper);
                            for (int i = 0; i < staffListItemLines.size(); i += 2)
                            {
                                rowStyles.add(POST_ROW_STYLE); // staff list post
                                rowStyles.add(null); // employee post
                            }
                            data.addAll(staffListItemLines);
                        }
                    }

                    // выводим строку "итого" по подразделению
                    if (null == model.getActivityType())
                    {
                        data.add(generateTotalOrgUnitReportLine(model, "Итого по {orgUnit_D} (бюджет)", orgUnit, ORG_UNIT_SUM_ID, model.getOrgUnitSalarysMap(), model.getOrgUnitSumTotalSalaryMap(), model.getOrgUnitSumPaymentsMap(), ACTIVITY_TYPE_BUDGET));
                        data.add(generateTotalOrgUnitReportLine(model, "Итого по {orgUnit_D} (вне бюджета)", orgUnit, ORG_UNIT_SUM_ID, model.getOrgUnitSalarysMap(), model.getOrgUnitSumTotalSalaryMap(), model.getOrgUnitSumPaymentsMap(), ACTIVITY_TYPE_OFF_BUDGET));
                        data.add(generateTotalOrgUnitReportLine(model, "Итого по {orgUnit_D}", orgUnit, ORG_UNIT_SUM_ID, model.getOrgUnitSalarysMap(), model.getOrgUnitSumTotalSalaryMap(), model.getOrgUnitSumPaymentsMap(), null));
                        rowStyles.add(TOTAL_ROW_STYLE);
                        rowStyles.add(TOTAL_ROW_STYLE);
                        rowStyles.add(TOTAL_ROW_STYLE);
                    }
                    else
                    {
                        data.add(generateTotalOrgUnitReportLine(model, "Итого по {orgUnit_D} (" + (ACTIVITY_TYPE_BUDGET.equals(model.getActivityType().getId()) ? "бюджет" : "вне бюджета") + ")", orgUnit, ORG_UNIT_SUM_ID, model.getOrgUnitSalarysMap(), model.getOrgUnitSumTotalSalaryMap(), model.getOrgUnitSumPaymentsMap(), model.getActivityType().getId()));
                        rowStyles.add(TOTAL_ROW_STYLE);
                    }

                    if (model.getOrgUnitPostSortedListsMap().containsKey(orgUnit.getId()))
                    {
                        for (StaffListItem staffListItem : model.getOrgUnitPostSortedListsMap().get(orgUnit.getId()))
                        {
                            List<String[]> staffListItemLines = generateVacancyReportLine(model, orgUnit.getId(), staffListItem, alreadyAddedPosts);
                            for (String[] staffListItemLine : staffListItemLines) rowStyles.add(VACANCY_ROW_STYLE); // staff list post
                            data.addAll(staffListItemLines);
                        }
                    }

                    // выводим строку "итого" вакансий по подразделению
                    data.add(generateTotalVacancyOrgUnitReportLine(model, orgUnit, ORG_UNIT_SUM_ID, model.getOrgUnitSumVacancyRates()));
                    rowStyles.add(VACANCY_TOTAL_ROW_STYLE);
                }
            }
        }

        model.setTableData(data.toArray(new String[][] {}));
        model.setRowStyles(rowStyles.toArray(new RtfTableRowStyle[rowStyles.size()]));

        List<String[]> summaryDataList = new ArrayList<>();
        if (null == model.getActivityType())
        {
            summaryDataList.add(generateVuzTotalReportLine(model, "Общий итог по бюджету:", ACTIVITY_TYPE_BUDGET));
            summaryDataList.add(generateVuzTotalReportLine(model, "Общий итог вне бюджета:", ACTIVITY_TYPE_OFF_BUDGET));
            summaryDataList.add(generateVuzTotalReportLine(model, "Общий итог:", null));
        }
        else
        {
            summaryDataList.add(generateVuzTotalReportLine(model, "Общий итог " + (ACTIVITY_TYPE_BUDGET.equals(model.getActivityType().getId()) ? "по бюджету" : "вне бюджета") + ":", model.getActivityType().getId()));
        }

        model.setSummaryData(summaryDataList.toArray(new String[][] {}));
    }

    /**
     * Генерирует строку данных по должности для таблицы.
     */
    public static List<String[]> generatePostReportLine(Model model, Long orgUnitId, Long staffListItemId, EmployeePostFakeWrapper employeePostWrapper)
    {
        List<Boolean> dummyArr = new ArrayList<>();
        if (employeePostWrapper.getBudgetStaffRate() > 0) dummyArr.add(true);
        if (employeePostWrapper.getOffBudgetStaffRate() > 0) dummyArr.add(false);
        List<String[]> resultLines = new ArrayList<>();

        Boolean secondJob = false;
        if (employeePostWrapper.getPostType() != null)
        {
            if (UniDefines.POST_TYPE_SECOND_JOB.equals(employeePostWrapper.getPostType().getCode())) secondJob = true;
            if (UniDefines.POST_TYPE_SECOND_JOB_INNER.equals(employeePostWrapper.getPostType().getCode())) secondJob = true;
            if (UniDefines.POST_TYPE_SECOND_JOB_OUTER.equals(employeePostWrapper.getPostType().getCode())) secondJob = true;
        }

        StaffListItem staffListItem = model.getStaffListItemsMap().get(staffListItemId);
        String orgUnitTitle = null != model.getOrgUnitsMap().get(orgUnitId).getNominativeCaseTitle() ? model.getOrgUnitsMap().get(orgUnitId).getNominativeCaseTitle() : model.getOrgUnitsMap().get(orgUnitId).getFullTitle();
        EmployeePostStaffRateItem rel = null != staffListItemId ? null : UniDaoFacade.getCoreDao().get(EmployeePostStaffRateItem.class, employeePostWrapper.getId());

        for (Boolean budget : dummyArr)
        {
            Double staffRate = budget ? employeePostWrapper.getBudgetStaffRate() : employeePostWrapper.getOffBudgetStaffRate();
            StringBuilder postLineBuilder = new StringBuilder();
            if (null != staffListItemId) postLineBuilder.append(model.getPostDataMap().get(staffListItemId)[0]);
            else postLineBuilder.append(rel.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL().getTitle());
            postLineBuilder.append("  (").append(orgUnitTitle).append(")");
            postLineBuilder.append(" Всего ставок=").append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(staffRate));
            postLineBuilder.append(". Занято=").append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(staffRate));
            postLineBuilder.append(" из них совм.=").append(secondJob ? DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(staffRate) : "0");
            postLineBuilder.append(". Свободно=0 - ").append(budget ? "Бюджет" : "Внебюджет");
            resultLines.add(new String[] { "", postLineBuilder.toString() });

            String[] employeePostLine = new String[model.getColumnsNumber()];
            String[] employeePostData = model.getEmployeePostDataMap().get(new CoreCollectionUtils.Pair<>(employeePostWrapper.getId(), budget));
            employeePostLine[0] = "";
            employeePostLine[1] = "1.";
            employeePostLine[2] = employeePostData[0]; // ФИО
            employeePostLine[3] = employeePostData[1]; // Тип должности (основная, совместит)
            employeePostLine[4] = employeePostData[3]; // Ставка

            if (null == staffListItem)
                employeePostLine[5] = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(rel.getEmployeePost().getSalary());
            else
                employeePostLine[5] = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(staffListItem.getSalary());

            // Надбавки, доплаты
            Long activityTypeId = model.getActivityType() != null ? (budget ? ACTIVITY_TYPE_BUDGET : ACTIVITY_TYPE_OFF_BUDGET) : null;
            int columnNumber = PAYMENTS_COLUMN_IDX;
            if (!model.getPaymentHeadersList().isEmpty())
            {
                Map<Long, CoreCollectionUtils.Pair<Double, Double>> paymentsMap = model.getOrgUnitPostPaymentsMap().get(employeePostWrapper.getId());
                if (null == paymentsMap) paymentsMap = new HashMap<>();
                for (Payment payment : model.getPaymentHeadersList())
                    employeePostLine[columnNumber++] = getPrintableValue(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED, paymentsMap.get(payment.getId()), activityTypeId, true);
            }
            else
            {
                columnNumber++;
            }

            employeePostLine[columnNumber++] = null != model.getOrgUnitPostSalarysMap().get(employeePostWrapper) ? getPrintableValue(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED, model.getOrgUnitPostSalarysMap().get(employeePostWrapper), activityTypeId, false) : employeePostLine[5];
            employeePostLine[columnNumber++] = null != model.getOrgUnitPostTotalSalaryMap().get(employeePostWrapper) ? getPrintableValue(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED, model.getOrgUnitPostTotalSalaryMap().get(employeePostWrapper), activityTypeId, false) : employeePostLine[5];
            resultLines.add(employeePostLine);
        }

        return resultLines;
    }

    /**
     * Генерирует строку данных по вакантной должности.
     */
    public static List<String[]> generateVacancyReportLine(Model model, Long orgUnitId, StaffListItem staffListItem, Set<CoreCollectionUtils.Pair<Long, Long>> alreadyAddedPosts)
    {
        CoreCollectionUtils.Pair<Long, Long> vacancyPairKey = getNewPairInstance(orgUnitId, staffListItem.getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL().getId());
        if (alreadyAddedPosts.contains(vacancyPairKey)) return new ArrayList<>();

        alreadyAddedPosts.add(vacancyPairKey);
        CoreCollectionUtils.Pair<Double, Double> staffListItemPair = model.getVacancyRates().get(vacancyPairKey);
        if (null == staffListItemPair || (staffListItemPair.getX() <= 0d && staffListItemPair.getY() <= 0d))
            return new ArrayList<>();

            List<Boolean> dummyArr = new ArrayList<>();
            if (staffListItemPair.getX() > 0) dummyArr.add(true);
            if (staffListItemPair.getY() > 0) dummyArr.add(false);
            List<String[]> resultLines = new ArrayList<>();

            String orgUnitTitle = null != model.getOrgUnitsMap().get(orgUnitId).getNominativeCaseTitle() ? model.getOrgUnitsMap().get(orgUnitId).getNominativeCaseTitle() : model.getOrgUnitsMap().get(orgUnitId).getFullTitle();

            for (Boolean budget : dummyArr)
            {
                Double staffRate = budget ? staffListItemPair.getX() : staffListItemPair.getY();
                StringBuilder postLineBuilder = new StringBuilder(model.getPostDataMap().get(staffListItem.getId())[0]).append(" ");
                postLineBuilder.append(" (").append(orgUnitTitle).append(")");
                postLineBuilder.append(" Всего ставок=").append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(staffRate));
                postLineBuilder.append(". - ").append(budget ? "Бюджет" : "Внебюджет");
                resultLines.add(new String[] { "", postLineBuilder.toString() });
            }

            return resultLines;
    }

    /**
     * Генерирует строку итоговых данных по подразделению.
     */
    public static String[] generateTotalOrgUnitReportLine(Model model, String title, OrgUnit orgUnit, Long sumLevel, Map<Long, CoreCollectionUtils.Pair<Double, Double>> orgUnitSalarysMap, Map<Long, CoreCollectionUtils.Pair<Double, Double>> orgUnitTotalSalarysMap, Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> orgUnitPaymentsMap, Long activityTypeId)
    {
        double sumBudgetStaffRate = 0d;
        double sumOffBudgetStaffRate = 0d;
        String[] line = new String[model.getColumnsNumber()];

        if (null != model.getOrgUnitEmployeePostBudgetStaffRatesMap().get(orgUnit.getId()) && null != model.getOrgUnitEmployeePostBudgetStaffRatesMap().get(orgUnit.getId()).get(sumLevel))
            sumBudgetStaffRate = model.getOrgUnitEmployeePostBudgetStaffRatesMap().get(orgUnit.getId()).get(sumLevel);
        if (null != model.getOrgUnitEmployeePostOffBudgetStaffRatesMap().get(orgUnit.getId()) && null != model.getOrgUnitEmployeePostOffBudgetStaffRatesMap().get(orgUnit.getId()).get(sumLevel))
            sumOffBudgetStaffRate = model.getOrgUnitEmployeePostOffBudgetStaffRatesMap().get(orgUnit.getId()).get(sumLevel);

        line[0] = title.replace("{orgUnit_D}", null != orgUnit.getDativeCaseTitle() ? orgUnit.getDativeCaseTitle() : orgUnit.getFullTitle());

        if (null == activityTypeId)
            line[4] = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(sumBudgetStaffRate + sumOffBudgetStaffRate);
        else if (ACTIVITY_TYPE_BUDGET.equals(activityTypeId))
            line[4] = sumBudgetStaffRate != 0d ? DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(sumBudgetStaffRate) : "";
            else if (ACTIVITY_TYPE_OFF_BUDGET.equals(activityTypeId))
                line[4] = sumOffBudgetStaffRate != 0d ? DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(sumOffBudgetStaffRate) : "";

                // Надбавки, доплаты
                int columnNumber = PAYMENTS_COLUMN_IDX;
                if (!model.getPaymentHeadersList().isEmpty())
                {
                    Map<Long, CoreCollectionUtils.Pair<Double, Double>> paymentsMap = orgUnitPaymentsMap.containsKey(orgUnit.getId()) ? orgUnitPaymentsMap.get(orgUnit.getId()) : new HashMap<>();
                    for (Payment payment : model.getPaymentHeadersList())
                        line[columnNumber++] = paymentsMap.containsKey(payment.getId()) ? getPrintableValue(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED, paymentsMap.get(payment.getId()), activityTypeId, false) : "";
                }
                else
                {
                    columnNumber++;
                }

                line[columnNumber++] = getPrintableValue(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED, orgUnitSalarysMap.get(orgUnit.getId()), activityTypeId, false);
                line[columnNumber++] = getPrintableValue(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED, orgUnitTotalSalarysMap.get(orgUnit.getId()), activityTypeId, false);

                return line;
    }

    /**
     * Генерирует строку итоговых данных по подразделению.
     */
    public static String[] generateTotalVacancyOrgUnitReportLine(Model model, OrgUnit orgUnit, Long sumLevel, Map<Long, CoreCollectionUtils.Pair<Double, Double>> orgUnitSumVacancyRates)
    {
        StringBuilder resultBuilder = new StringBuilder("Всего: ");
        Double budgetSumStaffRate = 0d;
        Double offBudgetSumStaffRate = 0d;
        Double budgetVacancyStaffRate = 0d;
        Double offBudgetVacancyStaffRate = 0d;

        if (null != model.getOrgUnitPostBudgetStaffRatesMap().get(orgUnit.getId()))
            budgetSumStaffRate = model.getOrgUnitPostBudgetStaffRatesMap().get(orgUnit.getId()).get(ORG_UNIT_SUM_ID);
        if (null != model.getOrgUnitPostOffBudgetStaffRatesMap().get(orgUnit.getId()))
            offBudgetSumStaffRate = model.getOrgUnitPostOffBudgetStaffRatesMap().get(orgUnit.getId()).get(ORG_UNIT_SUM_ID);
        if (null != orgUnitSumVacancyRates.get(orgUnit.getId()))
        {
            budgetVacancyStaffRate = orgUnitSumVacancyRates.get(orgUnit.getId()).getX();
            offBudgetVacancyStaffRate = orgUnitSumVacancyRates.get(orgUnit.getId()).getY();
        }

        resultBuilder.append("по бюджету = ").append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(null != budgetSumStaffRate && budgetSumStaffRate >= 0d ? budgetSumStaffRate : 0d));
        resultBuilder.append(", по внебюджету = ").append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(null != offBudgetSumStaffRate && offBudgetSumStaffRate >= 0d ? offBudgetSumStaffRate : 0d));
        resultBuilder.append("                                                                                                         ");
        resultBuilder.append("                                                                                                         ");
        resultBuilder.append("                                                                                                         ");
        resultBuilder.append("\nВакантно: ");
        resultBuilder.append("по бюджету = ").append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(budgetVacancyStaffRate < 0d ? 0d : budgetVacancyStaffRate));
        resultBuilder.append(", по внебюджету = ").append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(offBudgetVacancyStaffRate < 0d ? 0d : offBudgetVacancyStaffRate));

        return new String[] { resultBuilder.toString() };
    }

    /**
     * Генерирует строку итоговых данных по ОУ в целом.
     */
    public static String[] generateVuzTotalReportLine(Model model, String title, Long activityTypeId)
    {
        String[] line = new String[model.getColumnsNumber()];

        line[0] = title;

        // Надбавки, доплаты
        int columnNumber = 2;
        if (!model.getPaymentHeadersList().isEmpty())
        {
            for (Payment payment : model.getPaymentHeadersList())
                line[columnNumber++] = getPrintableValue(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED, model.getVuzSumPaymentsMap().get(payment.getId()), activityTypeId, false);
        }
        else
        {
            columnNumber++;
        }

        line[columnNumber++] = getPrintableValue(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED, model.getVuzSumSalary(), activityTypeId, false);
        line[columnNumber++] = getPrintableValue(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED, model.getVuzSumTotalSalary(), activityTypeId, false);

        return line;
    }

    /**
     * Подготавливает данные по штатной расписстановке
     */
    public static void prepareDataForPrinting(Model model)
    {
        initStatisticVariables(model);
        prepareGeneralData(model);
        prepareEmployeePostsData(model);
        preparePaymentsData(model);
        prepareSummaryData(model);
    }

    private static String getPrintableValue(DoubleFormatter formatter, CoreCollectionUtils.Pair<Double, Double> pair, Long activityTypeId, boolean replaceZeroesWithSpace)
    {
        Double result = 0d;
        if (null != pair)
        {
            if (null == activityTypeId)
                result = (null != pair.getX() ? pair.getX() : 0d) + (null != pair.getY() ? pair.getY() : 0d);
            else if (ACTIVITY_TYPE_BUDGET.equals(activityTypeId)) result = null != pair.getX() ? pair.getX() : 0d;
            else result = null != pair.getY() ? pair.getY() : 0d;
        }
        return result.equals(0d) && replaceZeroesWithSpace ? "" : formatter.format(result);
    }

    /**
     * Применяет указанный стиль к строке RTF-таблицы
     */
    public static void applyRtfRowStyles(RtfRow row, RtfTableRowStyle style)
    {
        if (null != style.getFontSize()) createCorrectRtfStyle(row, IRtfData.FS, style.getFontSize(), 14, style.isMergeAllRowCells());
        if (null != style.getShift()) createRtfStyle(row, IRtfData.FI, style.getShift());
        if (style.isBold()) createCorrectRtfStyle(row, IRtfData.B, null, null, style.isMergeAllRowCells());
        if (style.isItalic()) createCorrectRtfStyle(row, IRtfData.I, null, null, style.isMergeAllRowCells());
        if (null != style.getTextAlign()) createCorrectRtfStyle(row, style.getTextAlign(), null, null, style.isMergeAllRowCells());
        if (null != style.getRowHeight()) row.getFormatting().setHeight(style.getRowHeight());

        if (style.isMergeAllRowCells())
        {
            if (null == style.getMergingAllStartCell() && null == style.getMergingAllFinalCell())
                RtfUtil.unitAllCell(row, 0);
            else
            {
                int startMergingCellIdx = null != style.getMergingAllStartCell() ? style.getMergingAllStartCell() : 0;
                int finalMergingCellIdx = null != style.getMergingAllFinalCell() ? style.getMergingAllFinalCell() : row.getCellList().size() - 1;
                mergeCellsHorizontal(row, startMergingCellIdx, finalMergingCellIdx);
            }
        }
        else if (null != style.getMergingArr())
        {
            List<RtfCell> cellsToRemove = new ArrayList<>();
            for (Integer[] singleMerge : style.getMergingArr())
            {
                int destCellWidth = 0;
                for (int i = singleMerge[0]; i < singleMerge[0] + singleMerge[1]; i++)
                {
                    RtfCell cell = row.getCellList().get(i);
                    destCellWidth += cell.getWidth();
                    if (i != singleMerge[0]) cellsToRemove.add(cell);
                }
                row.getCellList().get(singleMerge[0]).setWidth(destCellWidth);
            }
            row.getCellList().removeAll(cellsToRemove);
        }

        if (null != style.getBorderWidths())
        {
            for (RtfCell cell : row.getCellList())
            {
                RtfCellBorder cellBorder = cell.getCellBorder();

                if (null != cellBorder.getTop())
                {
                    RtfBorder oldTop = cellBorder.getTop();
                    RtfBorder newTop = RtfBorder.getInstance(style.getBorderWidths()[0], oldTop.getColorIndex(), oldTop.getStyle());
                    cellBorder.setTop(newTop);
                }
                if (null != cellBorder.getBottom())
                {
                    RtfBorder oldBottom = cellBorder.getBottom();
                    RtfBorder newBottom = RtfBorder.getInstance(style.getBorderWidths()[2], oldBottom.getColorIndex(), oldBottom.getStyle());
                    cellBorder.setBottom(newBottom);
                }

                if (null != cellBorder.getLeft() && row.getCellList().indexOf(cell) == 0)
                {
                    RtfBorder oldLeft = cellBorder.getLeft();
                    RtfBorder newLeft = RtfBorder.getInstance(style.getBorderWidths()[3], oldLeft.getColorIndex(), oldLeft.getStyle());
                    cellBorder.setLeft(newLeft);
                }

                if (null != cellBorder.getRight() && row.getCellList().indexOf(cell) == row.getCellList().size() - 1)
                {
                    RtfBorder oldRight = cellBorder.getRight();
                    RtfBorder newRight = RtfBorder.getInstance(style.getBorderWidths()[3], oldRight.getColorIndex(), oldRight.getStyle());
                    cellBorder.setRight(newRight);
                }
            }
        }
    }

    public static void mergeCellsHorizontal(RtfRow row, int col1, int col2)
    {
        List<RtfCell> cells = row.getCellList();
        cells.get(col1).setMergeType(MergeType.HORIZONTAL_MERGED_FIRST);
        for (int i = col1 + 1; i <= col2; i++)
            cells.get(i).setMergeType(MergeType.HORIZONTAL_MERGED_NEXT);
    }

    /**
     * Добавляет одиночный RTF-тэг ячейкам указанной табличной строки
     */
    private static void createRtfStyle(RtfRow row, int controlIndex, Integer value)
    {
        IRtfControl contr = (null != value) ? RtfBean.getElementFactory().createRtfControl(controlIndex, value) :
            RtfBean.getElementFactory().createRtfControl(controlIndex);
        row.getCellList().get(0).getElementList().add(0, contr);
    }

    /**
     * Добавляет парный RTF-тэг ячейкам указанной табличной строки (открытие и закрытие)
     */
    private static void createCorrectRtfStyle(RtfRow row, int controlIndex, Integer value, Integer oldValue, boolean applyToSingleCell)
    {
        RtfCell firstRowCell = row.getCellList().get(0);
        RtfCell lastRowCell = row.getCellList().get(row.getCellList().size() - 1);

        IRtfControl contrO = (null != value) ? RtfBean.getElementFactory().createRtfControl(controlIndex, value) :
            RtfBean.getElementFactory().createRtfControl(controlIndex);
        firstRowCell.getElementList().add(0, contrO);

        IRtfControl contrC = (null != oldValue) ? RtfBean.getElementFactory().createRtfControl(controlIndex, oldValue) :
            RtfBean.getElementFactory().createRtfControl(controlIndex, 0);

        if (applyToSingleCell) firstRowCell.getElementList().add(contrC);
        else lastRowCell.getElementList().add(contrC);
    }

    /**
     * Рассчитывает сумму выплат для выплаты на МФОТ. Учитывает Ист.фин., настройку Порядок начислений выплат на МФОТ и не берет разовые выплаты.<p/>
     * Если настройка не заполнена, то учитываются все выплаты не на МФОТ.
     *
     * @param basePayment           выплата на МФОТ, для которой расчитывается сумма выплат
     * @param paymentValueMap       мапа выплаты и ее суммы
     * @param fullSalaryPaymentList список выплат на МФОТ
     * @return Пару бюджет\вснебюджет Сумм выплат актуальную для указаной выплаты.
     */
    private static CoreCollectionUtils.Pair<Double, Double> calculatePaymentSumm(StaffListPaymentBase basePayment, Map<Long, CoreCollectionUtils.Pair<Double, Double>> paymentValueMap, Collection<StaffListPaymentBase> fullSalaryPaymentList)
    {
        CoreCollectionUtils.Pair<Double, Double> resilt = new CoreCollectionUtils.Pair<>(0d, 0d);

        List<Payment> dependPaymentList = UniempDaoFacade.getUniempDAO().getPaymentOnFOTToPaymentsList(basePayment.getPayment());

        if (dependPaymentList.isEmpty())
        {
            for (Map.Entry<Long, CoreCollectionUtils.Pair<Double, Double>> paymentEntry : paymentValueMap.entrySet())
            {
                StaffListPaymentBase innerPayment = IUniBaseDao.instance.get().get(StaffListPaymentBase.class, paymentEntry.getKey());

                if (!isTakePayment(basePayment, innerPayment))
                    continue;

                resilt.setX(resilt.getX() + paymentEntry.getValue().getX());
                resilt.setY(resilt.getY() + paymentEntry.getValue().getY());
            }
        } else
        {
            for (Map.Entry<Long, CoreCollectionUtils.Pair<Double, Double>> paymentEntry : paymentValueMap.entrySet())
            {
                StaffListPaymentBase innerPayment = IUniBaseDao.instance.get().get(StaffListPaymentBase.class, paymentEntry.getKey());

                if (!dependPaymentList.contains(innerPayment.getPayment()))
                    continue;

                if (!isTakePayment(basePayment, innerPayment))
                    continue;

                resilt.setX(resilt.getX() + paymentEntry.getValue().getX());
                resilt.setY(resilt.getY() + paymentEntry.getValue().getY());
            }

            for (StaffListPaymentBase innerBasePayment : fullSalaryPaymentList)
            {
                if (!innerBasePayment.getStaffListItem().equals(basePayment.getStaffListItem()))
                    continue;

                if (!dependPaymentList.contains(innerBasePayment.getPayment()))
                    continue;

                if (!isTakePayment(basePayment, innerBasePayment))
                    continue;

                if (innerBasePayment.getFinancingSource().getCode().equals(UniempDefines.FINANCING_SOURCE_BUDGET))
                    resilt.setX(resilt.getX() + innerBasePayment.getTotalValue());
                else
                    resilt.setY(resilt.getY() + innerBasePayment.getTotalValue());
            }
        }

        return resilt;
    }

    private static boolean isTakePayment(StaffListPaymentBase payment, StaffListPaymentBase possiblePayment)
    {
        if (possiblePayment.getPayment().isOneTimePayment())
            return false;

        CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem> innerKey = new CoreCollectionUtils.Pair<>(possiblePayment.getFinancingSource(), possiblePayment.getFinancingSourceItem());
        CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem> key = new CoreCollectionUtils.Pair<>(payment.getFinancingSource(), payment.getFinancingSourceItem());

        return innerKey.equals(key);

    }

    /**
     * Вспомогательный метод для работы со структурами мапов для одиночной степени вложенности,
     * призванный уменьшить число строк в коде
     */
    private static void setSingleLevelDoubleMapValuesInSafeMode(Map<Long, Double> map, Double value, int operation, Long id)
    {
        Double totalValue = map.get(id);
        if (null == totalValue) totalValue = 0d;

        switch (operation)
        {
            case 0: totalValue = value; break;
            case 1: totalValue += null != value ? value : 0d; break;
            default: totalValue = value; break;
        }

        map.put(id, totalValue);
    }

    /**
     * Вспомогательный метод для работы со структурами мапов для двойной степени вложенности,
     * призванный уменьшить число строк в коде
     */
    private static void setTwoLevelsDoubleMapValuesInSafeMode(Map<Long, Map<Long, Double>> map, Double value, int operation, Long id0, Long id1)
    {
        Map<Long, Double> l1Map = map.get(id0);
        if (null == l1Map) l1Map = new HashMap<>();
        setSingleLevelDoubleMapValuesInSafeMode(l1Map, value, operation, id1);
        map.put(id0, l1Map);
    }

    /**
     * Вспомогательный метод для работы со структурами мапов для одиночной степени вложенности,
     * призванный уменьшить число строк в коде
     */
    private static void setSingleLevelPairMapValuesInSafeMode(Map<Long, CoreCollectionUtils.Pair<Double, Double>> map, CoreCollectionUtils.Pair<Double, Double> value, int operation, Long id)
    {
        CoreCollectionUtils.Pair<Double, Double> totalValue = map.get(id);
        if (null == totalValue) totalValue = new CoreCollectionUtils.Pair<>(0d, 0d);

        switch (operation)
        {
            case 0: totalValue = value; break;
            case 1:
                totalValue.setX(totalValue.getX() + (null != value ? value.getX() : 0d));
                totalValue.setY(totalValue.getY() + (null != value ? value.getY() : 0d));
                break;
            default: totalValue = value; break;
        }

        map.put(id, totalValue);
    }

    /**
     * Вспомогательный метод для работы со структурами мапов для двойной степени вложенности,
     * призванный уменьшить число строк в коде
     */
    private static void setTwoLevelsPairMapValuesInSafeMode(Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> map, CoreCollectionUtils.Pair<Double, Double> value, int operation, Long id0, Long id1)
    {
        Map<Long, CoreCollectionUtils.Pair<Double, Double>> l1Map = map.get(id0);
        if (null == l1Map) l1Map = new HashMap<>();
        setSingleLevelPairMapValuesInSafeMode(l1Map, value, operation, id1);
        map.put(id0, l1Map);
    }

    private static CoreCollectionUtils.Pair<Double, Double> getNewPairInstance(Double X, Double Y)
    {
        return new CoreCollectionUtils.Pair<>(X, Y);
    }

    private static CoreCollectionUtils.Pair<Long, Long> getNewPairInstance(Long X, Long Y)
    {
        return new CoreCollectionUtils.Pair<>(X, Y);
    }

    public static void nextTimeValue(String caption)
    {
        if (null == time) return;
        System.out.println(String.valueOf(System.currentTimeMillis() - time) + " ms \t\t" + caption);
        time = System.currentTimeMillis();
    }

    public static void absoluteTimeValue(String caption)
    {
        if (null == absoluteTime) return;
        System.out.println(String.valueOf(System.currentTimeMillis() - absoluteTime) + " ms \t\t" + caption);
    }
}