/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.EmployeePaymentAddEdit;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uniemp.dao.IUniempDAO;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.employee.EmployeePayment;

import java.util.Date;
import java.util.List;

/**
 * @author dseleznev
 * Created on: 23.12.2008
 */
public interface IDAO extends IUniempDAO<Model>
{
    /**
     * Возвращает список выплат, у которых дата окончания больше или равна указанной дате и тип выплаты содержится в указаном списке выплат.<p/>
     * Если <tt>paymentList == null && paymentList.isEmpty</tt>, то берем все актуальные выплаты на дату.
     * @param employeePost сотрудник, выплаты которого поднимаются
     * @param beginDate дата, относительно которой берутся выплаты
     * @param paymentList список выплат
     * @return Список выплат актуальные на указанную дату и нужного типа.<p/>
     * Если <tt>beginDate == null</tt>, то возвращается пустой список.
     */
    List<EmployeePayment> getActualPaymentList(EmployeePost employeePost, Date beginDate, List<Payment> paymentList);
}