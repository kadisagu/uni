/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.settings.StaffRateStepSetting;

import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.settings.IDataSettingsManager;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniemp.UniempDefines;

/**
 * @author dseleznev
 * Created on: 07.10.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{

    @Override
    public void prepare(Model model)
    {
        model.setStaffRateStep((Double)DataSettingsFacade.getSettings("general", UniempDefines.STAFF_RATE_STEP_SETTINGS_PREFIX).get(UniempDefines.STAFF_RATE_STEP_POST_SETING_NAME));
    }

    @Override
    public void update(Model model)
    {
        if(null != model.getStaffRateStep())
            model.setStaffRateStep(Math.round(model.getStaffRateStep()*100) / 100d);
        
        IDataSettings settings = DataSettingsFacade.getSettings("general", UniempDefines.STAFF_RATE_STEP_SETTINGS_PREFIX);
        settings.set(UniempDefines.STAFF_RATE_STEP_POST_SETING_NAME, model.getStaffRateStep());
        DataSettingsFacade.saveSettings(settings);
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        if (null != model.getStaffRateStep() && (model.getStaffRateStep() < 0 || model.getStaffRateStep() > 1))
            errors.add("Шаг ставки должен быть в пределах от 0 до 1", "step");
    }

}