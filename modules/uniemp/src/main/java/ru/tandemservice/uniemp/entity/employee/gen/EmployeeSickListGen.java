package ru.tandemservice.uniemp.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import ru.tandemservice.uniemp.entity.catalog.SickListBasics;
import ru.tandemservice.uniemp.entity.employee.EmployeeSickList;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Лист нетрудоспособности
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeSickListGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.employee.EmployeeSickList";
    public static final String ENTITY_NAME = "employeeSickList";
    public static final int VERSION_HASH = -1510256781;
    private static IEntityMeta ENTITY_META;

    public static final String P_NUMBER = "number";
    public static final String L_EMPLOYEE = "employee";
    public static final String P_OPEN_DATE = "openDate";
    public static final String P_CLOSE_DATE = "closeDate";
    public static final String P_MEDICAL_FOUNDATION = "medicalFoundation";
    public static final String L_BASIC = "basic";
    public static final String P_PROLONG_NUMBER = "prolongNumber";

    private String _number;     // Номер листа нетрудоспособности
    private Employee _employee;     // Кадровый ресурс
    private Date _openDate;     // Дата открытия
    private Date _closeDate;     // Дата закрытия
    private String _medicalFoundation;     // Мед. учреждение
    private SickListBasics _basic;     // Причины нетрудоспособности
    private String _prolongNumber;     // Номер листа продления

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Номер листа нетрудоспособности. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер листа нетрудоспособности. Свойство не может быть null.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Кадровый ресурс. Свойство не может быть null.
     */
    @NotNull
    public Employee getEmployee()
    {
        return _employee;
    }

    /**
     * @param employee Кадровый ресурс. Свойство не может быть null.
     */
    public void setEmployee(Employee employee)
    {
        dirty(_employee, employee);
        _employee = employee;
    }

    /**
     * @return Дата открытия. Свойство не может быть null.
     */
    @NotNull
    public Date getOpenDate()
    {
        return _openDate;
    }

    /**
     * @param openDate Дата открытия. Свойство не может быть null.
     */
    public void setOpenDate(Date openDate)
    {
        dirty(_openDate, openDate);
        _openDate = openDate;
    }

    /**
     * @return Дата закрытия. Свойство не может быть null.
     */
    @NotNull
    public Date getCloseDate()
    {
        return _closeDate;
    }

    /**
     * @param closeDate Дата закрытия. Свойство не может быть null.
     */
    public void setCloseDate(Date closeDate)
    {
        dirty(_closeDate, closeDate);
        _closeDate = closeDate;
    }

    /**
     * @return Мед. учреждение. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getMedicalFoundation()
    {
        return _medicalFoundation;
    }

    /**
     * @param medicalFoundation Мед. учреждение. Свойство не может быть null.
     */
    public void setMedicalFoundation(String medicalFoundation)
    {
        dirty(_medicalFoundation, medicalFoundation);
        _medicalFoundation = medicalFoundation;
    }

    /**
     * @return Причины нетрудоспособности. Свойство не может быть null.
     */
    @NotNull
    public SickListBasics getBasic()
    {
        return _basic;
    }

    /**
     * @param basic Причины нетрудоспособности. Свойство не может быть null.
     */
    public void setBasic(SickListBasics basic)
    {
        dirty(_basic, basic);
        _basic = basic;
    }

    /**
     * @return Номер листа продления.
     */
    @Length(max=255)
    public String getProlongNumber()
    {
        return _prolongNumber;
    }

    /**
     * @param prolongNumber Номер листа продления.
     */
    public void setProlongNumber(String prolongNumber)
    {
        dirty(_prolongNumber, prolongNumber);
        _prolongNumber = prolongNumber;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EmployeeSickListGen)
        {
            setNumber(((EmployeeSickList)another).getNumber());
            setEmployee(((EmployeeSickList)another).getEmployee());
            setOpenDate(((EmployeeSickList)another).getOpenDate());
            setCloseDate(((EmployeeSickList)another).getCloseDate());
            setMedicalFoundation(((EmployeeSickList)another).getMedicalFoundation());
            setBasic(((EmployeeSickList)another).getBasic());
            setProlongNumber(((EmployeeSickList)another).getProlongNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeSickListGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeSickList.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeSickList();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "number":
                    return obj.getNumber();
                case "employee":
                    return obj.getEmployee();
                case "openDate":
                    return obj.getOpenDate();
                case "closeDate":
                    return obj.getCloseDate();
                case "medicalFoundation":
                    return obj.getMedicalFoundation();
                case "basic":
                    return obj.getBasic();
                case "prolongNumber":
                    return obj.getProlongNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "employee":
                    obj.setEmployee((Employee) value);
                    return;
                case "openDate":
                    obj.setOpenDate((Date) value);
                    return;
                case "closeDate":
                    obj.setCloseDate((Date) value);
                    return;
                case "medicalFoundation":
                    obj.setMedicalFoundation((String) value);
                    return;
                case "basic":
                    obj.setBasic((SickListBasics) value);
                    return;
                case "prolongNumber":
                    obj.setProlongNumber((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "number":
                        return true;
                case "employee":
                        return true;
                case "openDate":
                        return true;
                case "closeDate":
                        return true;
                case "medicalFoundation":
                        return true;
                case "basic":
                        return true;
                case "prolongNumber":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "number":
                    return true;
                case "employee":
                    return true;
                case "openDate":
                    return true;
                case "closeDate":
                    return true;
                case "medicalFoundation":
                    return true;
                case "basic":
                    return true;
                case "prolongNumber":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "number":
                    return String.class;
                case "employee":
                    return Employee.class;
                case "openDate":
                    return Date.class;
                case "closeDate":
                    return Date.class;
                case "medicalFoundation":
                    return String.class;
                case "basic":
                    return SickListBasics.class;
                case "prolongNumber":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeeSickList> _dslPath = new Path<EmployeeSickList>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeSickList");
    }
            

    /**
     * @return Номер листа нетрудоспособности. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeSickList#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Кадровый ресурс. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeSickList#getEmployee()
     */
    public static Employee.Path<Employee> employee()
    {
        return _dslPath.employee();
    }

    /**
     * @return Дата открытия. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeSickList#getOpenDate()
     */
    public static PropertyPath<Date> openDate()
    {
        return _dslPath.openDate();
    }

    /**
     * @return Дата закрытия. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeSickList#getCloseDate()
     */
    public static PropertyPath<Date> closeDate()
    {
        return _dslPath.closeDate();
    }

    /**
     * @return Мед. учреждение. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeSickList#getMedicalFoundation()
     */
    public static PropertyPath<String> medicalFoundation()
    {
        return _dslPath.medicalFoundation();
    }

    /**
     * @return Причины нетрудоспособности. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeSickList#getBasic()
     */
    public static SickListBasics.Path<SickListBasics> basic()
    {
        return _dslPath.basic();
    }

    /**
     * @return Номер листа продления.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeSickList#getProlongNumber()
     */
    public static PropertyPath<String> prolongNumber()
    {
        return _dslPath.prolongNumber();
    }

    public static class Path<E extends EmployeeSickList> extends EntityPath<E>
    {
        private PropertyPath<String> _number;
        private Employee.Path<Employee> _employee;
        private PropertyPath<Date> _openDate;
        private PropertyPath<Date> _closeDate;
        private PropertyPath<String> _medicalFoundation;
        private SickListBasics.Path<SickListBasics> _basic;
        private PropertyPath<String> _prolongNumber;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Номер листа нетрудоспособности. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeSickList#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(EmployeeSickListGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Кадровый ресурс. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeSickList#getEmployee()
     */
        public Employee.Path<Employee> employee()
        {
            if(_employee == null )
                _employee = new Employee.Path<Employee>(L_EMPLOYEE, this);
            return _employee;
        }

    /**
     * @return Дата открытия. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeSickList#getOpenDate()
     */
        public PropertyPath<Date> openDate()
        {
            if(_openDate == null )
                _openDate = new PropertyPath<Date>(EmployeeSickListGen.P_OPEN_DATE, this);
            return _openDate;
        }

    /**
     * @return Дата закрытия. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeSickList#getCloseDate()
     */
        public PropertyPath<Date> closeDate()
        {
            if(_closeDate == null )
                _closeDate = new PropertyPath<Date>(EmployeeSickListGen.P_CLOSE_DATE, this);
            return _closeDate;
        }

    /**
     * @return Мед. учреждение. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeSickList#getMedicalFoundation()
     */
        public PropertyPath<String> medicalFoundation()
        {
            if(_medicalFoundation == null )
                _medicalFoundation = new PropertyPath<String>(EmployeeSickListGen.P_MEDICAL_FOUNDATION, this);
            return _medicalFoundation;
        }

    /**
     * @return Причины нетрудоспособности. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeSickList#getBasic()
     */
        public SickListBasics.Path<SickListBasics> basic()
        {
            if(_basic == null )
                _basic = new SickListBasics.Path<SickListBasics>(L_BASIC, this);
            return _basic;
        }

    /**
     * @return Номер листа продления.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeSickList#getProlongNumber()
     */
        public PropertyPath<String> prolongNumber()
        {
            if(_prolongNumber == null )
                _prolongNumber = new PropertyPath<String>(EmployeeSickListGen.P_PROLONG_NUMBER, this);
            return _prolongNumber;
        }

        public Class getEntityClass()
        {
            return EmployeeSickList.class;
        }

        public String getEntityName()
        {
            return "employeeSickList";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
