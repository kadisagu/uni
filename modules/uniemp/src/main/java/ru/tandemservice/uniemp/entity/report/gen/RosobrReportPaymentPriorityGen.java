package ru.tandemservice.uniemp.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.report.RosobrReportPaymentPriority;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Приоритет следования выплат в отчете
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RosobrReportPaymentPriorityGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.report.RosobrReportPaymentPriority";
    public static final String ENTITY_NAME = "rosobrReportPaymentPriority";
    public static final int VERSION_HASH = 249202157;
    private static IEntityMeta ENTITY_META;

    public static final String L_PAYMENT = "payment";
    public static final String P_SCIENCE_DEGREE_PAYMENT = "scienceDegreePayment";
    public static final String P_PRIORITY = "priority";

    private Payment _payment;     // Выплата
    private boolean _scienceDegreePayment;     // Считать выплатой за ученую степень
    private int _priority;     // Приоритет

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выплата. Свойство не может быть null.
     */
    @NotNull
    public Payment getPayment()
    {
        return _payment;
    }

    /**
     * @param payment Выплата. Свойство не может быть null.
     */
    public void setPayment(Payment payment)
    {
        dirty(_payment, payment);
        _payment = payment;
    }

    /**
     * @return Считать выплатой за ученую степень. Свойство не может быть null.
     */
    @NotNull
    public boolean isScienceDegreePayment()
    {
        return _scienceDegreePayment;
    }

    /**
     * @param scienceDegreePayment Считать выплатой за ученую степень. Свойство не может быть null.
     */
    public void setScienceDegreePayment(boolean scienceDegreePayment)
    {
        dirty(_scienceDegreePayment, scienceDegreePayment);
        _scienceDegreePayment = scienceDegreePayment;
    }

    /**
     * @return Приоритет. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Приоритет. Свойство не может быть null и должно быть уникальным.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof RosobrReportPaymentPriorityGen)
        {
            setPayment(((RosobrReportPaymentPriority)another).getPayment());
            setScienceDegreePayment(((RosobrReportPaymentPriority)another).isScienceDegreePayment());
            setPriority(((RosobrReportPaymentPriority)another).getPriority());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RosobrReportPaymentPriorityGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RosobrReportPaymentPriority.class;
        }

        public T newInstance()
        {
            return (T) new RosobrReportPaymentPriority();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "payment":
                    return obj.getPayment();
                case "scienceDegreePayment":
                    return obj.isScienceDegreePayment();
                case "priority":
                    return obj.getPriority();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "payment":
                    obj.setPayment((Payment) value);
                    return;
                case "scienceDegreePayment":
                    obj.setScienceDegreePayment((Boolean) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "payment":
                        return true;
                case "scienceDegreePayment":
                        return true;
                case "priority":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "payment":
                    return true;
                case "scienceDegreePayment":
                    return true;
                case "priority":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "payment":
                    return Payment.class;
                case "scienceDegreePayment":
                    return Boolean.class;
                case "priority":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RosobrReportPaymentPriority> _dslPath = new Path<RosobrReportPaymentPriority>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RosobrReportPaymentPriority");
    }
            

    /**
     * @return Выплата. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.report.RosobrReportPaymentPriority#getPayment()
     */
    public static Payment.Path<Payment> payment()
    {
        return _dslPath.payment();
    }

    /**
     * @return Считать выплатой за ученую степень. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.report.RosobrReportPaymentPriority#isScienceDegreePayment()
     */
    public static PropertyPath<Boolean> scienceDegreePayment()
    {
        return _dslPath.scienceDegreePayment();
    }

    /**
     * @return Приоритет. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniemp.entity.report.RosobrReportPaymentPriority#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    public static class Path<E extends RosobrReportPaymentPriority> extends EntityPath<E>
    {
        private Payment.Path<Payment> _payment;
        private PropertyPath<Boolean> _scienceDegreePayment;
        private PropertyPath<Integer> _priority;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выплата. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.report.RosobrReportPaymentPriority#getPayment()
     */
        public Payment.Path<Payment> payment()
        {
            if(_payment == null )
                _payment = new Payment.Path<Payment>(L_PAYMENT, this);
            return _payment;
        }

    /**
     * @return Считать выплатой за ученую степень. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.report.RosobrReportPaymentPriority#isScienceDegreePayment()
     */
        public PropertyPath<Boolean> scienceDegreePayment()
        {
            if(_scienceDegreePayment == null )
                _scienceDegreePayment = new PropertyPath<Boolean>(RosobrReportPaymentPriorityGen.P_SCIENCE_DEGREE_PAYMENT, this);
            return _scienceDegreePayment;
        }

    /**
     * @return Приоритет. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniemp.entity.report.RosobrReportPaymentPriority#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(RosobrReportPaymentPriorityGen.P_PRIORITY, this);
            return _priority;
        }

        public Class getEntityClass()
        {
            return RosobrReportPaymentPriority.class;
        }

        public String getEntityName()
        {
            return "rosobrReportPaymentPriority";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
