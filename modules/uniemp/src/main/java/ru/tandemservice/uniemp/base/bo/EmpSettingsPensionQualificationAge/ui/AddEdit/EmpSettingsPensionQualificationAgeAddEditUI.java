/**
 *$Id:$
 */
package ru.tandemservice.uniemp.base.bo.EmpSettingsPensionQualificationAge.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniemp.base.bo.EmpSettingsPensionQualificationAge.EmpSettingsPensionQualificationAgeManager;
import ru.tandemservice.uniemp.entity.employee.PensionQualificationAgeSettings;

/**
 * @author Alexander Shaburov
 * @since 11.09.12
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "id")
})
public class EmpSettingsPensionQualificationAgeAddEditUI extends UIPresenter
{
    private Long _id;
    private PensionQualificationAgeSettings _entity;
    private boolean _addForm;

    @Override
    public void onComponentRefresh()
    {
        _addForm = _id == null;

        if (!_addForm)
            _entity = DataAccessServices.dao().getNotNull(_id);
        else
            _entity = new PensionQualificationAgeSettings();
    }

    public void onClickApply()
    {
        ErrorCollector errorCollector = EmpSettingsPensionQualificationAgeManager.instance().pensionQualificationAgeDao().validate(_entity);

        if (errorCollector.hasErrors())
            return;

        EmpSettingsPensionQualificationAgeManager.instance().pensionQualificationAgeDao().saveOrUpdate(_entity);
        deactivate();
    }

    // Getters & Setters

    public PensionQualificationAgeSettings getEntity()
    {
        return _entity;
    }

    public void setEntity(PensionQualificationAgeSettings entity)
    {
        _entity = entity;
    }

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        _id = id;
    }

    public boolean isAddForm()
    {
        return _addForm;
    }

    public void setAddForm(boolean addForm)
    {
        _addForm = addForm;
    }
}
