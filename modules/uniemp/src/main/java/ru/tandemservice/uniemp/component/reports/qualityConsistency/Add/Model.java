/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.qualityConsistency.Add;

import java.util.Date;
import java.util.List;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

/**
 * @author dseleznev
 * Created on: 14.04.2009
 */
public class Model
{
    private Date _reportDate;

    private boolean _orgUnitActive;

    private OrgUnitType _orgUnitType;
    private OrgUnit _orgUnit;

    private List<OrgUnitType> _orgUnitTypesList;
    private ISelectModel _orgUnitsListModel;

    public Date getReportDate()
    {
        return _reportDate;
    }

    public void setReportDate(Date reportDate)
    {
        this._reportDate = reportDate;
    }

    public boolean isOrgUnitActive()
    {
        return _orgUnitActive;
    }

    public void setOrgUnitActive(boolean orgUnitActive)
    {
        this._orgUnitActive = orgUnitActive;
    }

    public OrgUnitType getOrgUnitType()
    {
        return _orgUnitType;
    }

    public void setOrgUnitType(OrgUnitType orgUnitType)
    {
        this._orgUnitType = orgUnitType;
    }

    public List<OrgUnitType> getOrgUnitTypesList()
    {
        return _orgUnitTypesList;
    }

    public void setOrgUnitTypesList(List<OrgUnitType> orgUnitTypesList)
    {
        this._orgUnitTypesList = orgUnitTypesList;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        this._orgUnit = orgUnit;
    }

    public ISelectModel getOrgUnitsListModel()
    {
        return _orgUnitsListModel;
    }

    public void setOrgUnitsListModel(ISelectModel orgUnitsListModel)
    {
        this._orgUnitsListModel = orgUnitsListModel;
    }
}