/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.component.employee.AdditionalAgreementAddEdit;

import ru.tandemservice.uniemp.dao.UniempDAO;
import ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;

/**
 * @author E. Grigoriev
 * @since 11.07.2008
 */
public class DAO extends UniempDAO<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        if (model.getRowId() != null)
            model.setAgreement((ContractCollateralAgreement)get(model.getRowId()));
    }

    @Override
    public void update(Model model)
    {
        if (model.getRowId() != null)
            getSession().update(model.getAgreement());
        else
        {
            model.getAgreement().setContract((EmployeeLabourContract)get(model.getLabourContractId()));
            getSession().save(model.getAgreement());
        }
    }
}