/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.AdditionalDataEdit;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author dseleznev
 * Created on: 31.07.2008
 */
public interface IDAO extends IUniDao<Model>
{
}