/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.bo.EmpSystemAction.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

/**
 * @author Alexander Shaburov
 * @since 12.02.13
 */
public interface IEmpSystemActionDao extends INeedPersistenceSupport
{
    void updateEmployeeCards();

    void updateStaffListPayments();

    void updateActiveStaffLists();

    void updateSynchronizeEmploymentHistory();

    void updateSynchronizeEmploymentHistoryStaffRates();

    void updateSyncStaffListCreateAndAcceptDates();

    void updateInitStaffListArchivingDates();

    void updateReallocateStaffListAllocItems();

    void updateCorrectEmptySalaryAllocItems();

    void updateCorrectEmployeeHolidayFinishDate();

    void getPostCatalogs();

    void updateFillStaffListsAutomatically();

    void updateOrgUnitTypePostRelation();

    void updateSecondJobEmployeePost();

    void updateEmployeeContractEndDate();
}
