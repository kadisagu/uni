package ru.tandemservice.uniemp.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;
import ru.tandemservice.uniemp.entity.employee.StaffListFakePayment;
import ru.tandemservice.uniemp.entity.employee.StaffListPostPayment;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Фиксированная выплата должности штатного расписания
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StaffListFakePaymentGen extends StaffListPostPayment
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.employee.StaffListFakePayment";
    public static final String ENTITY_NAME = "staffListFakePayment";
    public static final int VERSION_HASH = -2018877730;
    private static IEntityMeta ENTITY_META;

    public static final String P_STAFF_RATE_INTEGER = "staffRateInteger";
    public static final String L_STAFF_LIST_ALLOCATION_ITEM = "staffListAllocationItem";

    private int _staffRateInteger;     // Количество штатных единиц (целое)
    private StaffListAllocationItem _staffListAllocationItem;     // Должность штатной расстановки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Количество штатных единиц (целое). Свойство не может быть null.
     */
    @NotNull
    public int getStaffRateInteger()
    {
        return _staffRateInteger;
    }

    /**
     * @param staffRateInteger Количество штатных единиц (целое). Свойство не может быть null.
     */
    public void setStaffRateInteger(int staffRateInteger)
    {
        dirty(_staffRateInteger, staffRateInteger);
        _staffRateInteger = staffRateInteger;
    }

    /**
     * @return Должность штатной расстановки.
     */
    public StaffListAllocationItem getStaffListAllocationItem()
    {
        return _staffListAllocationItem;
    }

    /**
     * @param staffListAllocationItem Должность штатной расстановки.
     */
    public void setStaffListAllocationItem(StaffListAllocationItem staffListAllocationItem)
    {
        dirty(_staffListAllocationItem, staffListAllocationItem);
        _staffListAllocationItem = staffListAllocationItem;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof StaffListFakePaymentGen)
        {
            setStaffRateInteger(((StaffListFakePayment)another).getStaffRateInteger());
            setStaffListAllocationItem(((StaffListFakePayment)another).getStaffListAllocationItem());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StaffListFakePaymentGen> extends StaffListPostPayment.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StaffListFakePayment.class;
        }

        public T newInstance()
        {
            return (T) new StaffListFakePayment();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "staffRateInteger":
                    return obj.getStaffRateInteger();
                case "staffListAllocationItem":
                    return obj.getStaffListAllocationItem();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "staffRateInteger":
                    obj.setStaffRateInteger((Integer) value);
                    return;
                case "staffListAllocationItem":
                    obj.setStaffListAllocationItem((StaffListAllocationItem) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "staffRateInteger":
                        return true;
                case "staffListAllocationItem":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "staffRateInteger":
                    return true;
                case "staffListAllocationItem":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "staffRateInteger":
                    return Integer.class;
                case "staffListAllocationItem":
                    return StaffListAllocationItem.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StaffListFakePayment> _dslPath = new Path<StaffListFakePayment>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StaffListFakePayment");
    }
            

    /**
     * @return Количество штатных единиц (целое). Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListFakePayment#getStaffRateInteger()
     */
    public static PropertyPath<Integer> staffRateInteger()
    {
        return _dslPath.staffRateInteger();
    }

    /**
     * @return Должность штатной расстановки.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListFakePayment#getStaffListAllocationItem()
     */
    public static StaffListAllocationItem.Path<StaffListAllocationItem> staffListAllocationItem()
    {
        return _dslPath.staffListAllocationItem();
    }

    public static class Path<E extends StaffListFakePayment> extends StaffListPostPayment.Path<E>
    {
        private PropertyPath<Integer> _staffRateInteger;
        private StaffListAllocationItem.Path<StaffListAllocationItem> _staffListAllocationItem;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Количество штатных единиц (целое). Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListFakePayment#getStaffRateInteger()
     */
        public PropertyPath<Integer> staffRateInteger()
        {
            if(_staffRateInteger == null )
                _staffRateInteger = new PropertyPath<Integer>(StaffListFakePaymentGen.P_STAFF_RATE_INTEGER, this);
            return _staffRateInteger;
        }

    /**
     * @return Должность штатной расстановки.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListFakePayment#getStaffListAllocationItem()
     */
        public StaffListAllocationItem.Path<StaffListAllocationItem> staffListAllocationItem()
        {
            if(_staffListAllocationItem == null )
                _staffListAllocationItem = new StaffListAllocationItem.Path<StaffListAllocationItem>(L_STAFF_LIST_ALLOCATION_ITEM, this);
            return _staffListAllocationItem;
        }

        public Class getEntityClass()
        {
            return StaffListFakePayment.class;
        }

        public String getEntityName()
        {
            return "staffListFakePayment";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
