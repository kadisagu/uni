/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.orgUnitPost.OrgUnitPostAddEdit;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.catalog.entity.ScienceDegree;
import org.tandemframework.shared.person.catalog.entity.ScienceStatus;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation;
import ru.tandemservice.uniemp.entity.employee.OrgUnitPostToScDegreeRelation;
import ru.tandemservice.uniemp.entity.employee.OrgUnitPostToScStatusRelation;
import ru.tandemservice.uniemp.entity.employee.OrgUnitPostToTypeRelation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author dseleznev
 *         Created on: 16.09.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    public static class CatalogItemMultiSelectModel extends FullCheckSelectModel
    {
        private IDAO _dao;
        private Class<? extends ICatalogItem> _clazz;

        public CatalogItemMultiSelectModel(IDAO dao, Class<? extends ICatalogItem> clazz)
        {
            super(IEntity.P_ID, ICatalogItem.CATALOG_ITEM_TITLE);
            _dao = dao;
            _clazz = clazz;
        }

        @Override
        public ListResult findValues(String filter)
        {
            return new ListResult<>(_dao.getCatalogItemsList(filter, _clazz));
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepare(final Model model)
    {
        model.setOrgUnitPostRelation(new OrgUnitPostRelation()); // TODO: temporary fix, should be removed
        if (null != model.getOrgUnitId())
        {
            model.setOrgUnit((OrgUnit) get(model.getOrgUnitId()));
            model.getOrgUnitPostRelation().setOrgUnit(model.getOrgUnit());
        }

        if (null != model.getOrgUnitPostRelationId())
        {
            model.setOrgUnitPostRelation(get(OrgUnitPostRelation.class, model.getOrgUnitPostRelationId()));
            model.setSelectedScientificStatusesList(getSession().createCriteria(OrgUnitPostToScStatusRelation.class).add(Restrictions.eq(OrgUnitPostToScStatusRelation.L_ORG_UNIT_POST_RELATION, model.getOrgUnitPostRelation())).setProjection(Projections.property(OrgUnitPostToScStatusRelation.L_ACADEMIC_STATUS)).list());
            model.setSelectedScientificDegreesList(getSession().createCriteria(OrgUnitPostToScDegreeRelation.class).add(Restrictions.eq(OrgUnitPostToScDegreeRelation.L_ORG_UNIT_POST_RELATION, model.getOrgUnitPostRelation())).setProjection(Projections.property(OrgUnitPostToScDegreeRelation.L_ACADEMIC_DEGREE)).list());
            model.setSelectedPostTypesLevels(getSession().createCriteria(OrgUnitPostToTypeRelation.class).add(Restrictions.eq(OrgUnitPostToTypeRelation.L_ORG_UNIT_POST_RELATION, model.getOrgUnitPostRelation())).setProjection(Projections.property(OrgUnitPostToTypeRelation.L_POST_TYPE)).list());
        }

        model.setPostRelationListModel(new FullCheckSelectModel(OrgUnitTypePostRelation.P_TITLE_WITH_SALARY)
        {
            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder subBuilder = new MQBuilder(OrgUnitPostRelation.ENTITY_CLASS, "oupr", new String[] {OrgUnitPostRelation.orgUnitTypePostRelation().id().s()});
                subBuilder.add(MQExpression.eq("oupr", OrgUnitPostRelation.orgUnit().s(), model.getOrgUnit()));
                if(null != model.getOrgUnitPostRelationId())
                    subBuilder.add(MQExpression.notEq("oupr", OrgUnitPostRelation.id().s(), model.getOrgUnitPostRelationId()));

                MQBuilder builder = new MQBuilder(OrgUnitTypePostRelation.ENTITY_CLASS, "rel");
                builder.add(MQExpression.eq("rel", OrgUnitTypePostRelation.orgUnitType().s(), model.getOrgUnit().getOrgUnitType()));
                builder.add(MQExpression.notIn("rel", OrgUnitTypePostRelation.id().s(), subBuilder));

                if(!StringUtils.isEmpty(filter))
                    builder.add(MQExpression.like("rel", OrgUnitTypePostRelation.postBoundedWithQGandQL().title().s(), CoreStringUtils.escapeLike(filter)));

                return new ListResult<>(builder.getResultList(getSession()));
            }
        });

        model.setScientificStatusesModel(new CatalogItemMultiSelectModel(this, ScienceStatus.class));
        model.setScientificDegreesModel(new CatalogItemMultiSelectModel(this, ScienceDegree.class));
        model.setPostTypesModel(new CatalogItemMultiSelectModel(this, PostType.class));
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<ICatalogItem> getCatalogItemsList(String filter, Class<? extends ICatalogItem> clazz)
    {
        Criteria crit = getSession().createCriteria(clazz);
        if (filter.length() > 0)
            crit.add(Restrictions.like(ICatalogItem.CATALOG_ITEM_TITLE, CoreStringUtils.escapeLike(filter)));
        crit.addOrder(Order.asc(ICatalogItem.CATALOG_ITEM_TITLE));
        return crit.list();
    }

    @Override
    public void update(OrgUnitPostRelation rel, Model model)
    {
        if (null != rel.getId())
            getSession().update(rel);
        else
            getSession().save(rel);

        Map<ScienceStatus, OrgUnitPostToScStatusRelation> scStatusMap = getScStatusMap(rel);
        Map<ScienceDegree, OrgUnitPostToScDegreeRelation> scDegreeMap = getScDegreeMap(rel);
        Map<PostType, OrgUnitPostToTypeRelation> typesMap = getPostTypesMap(rel);

        for (ScienceStatus item : model.getSelectedScientificStatusesList())
            if (!scStatusMap.containsKey(item))
                getSession().save(new OrgUnitPostToScStatusRelation(rel, item));
            else
                scStatusMap.remove(item);

        for (ScienceDegree item : model.getSelectedScientificDegreesList())
            if (!scDegreeMap.containsKey(item))
                getSession().save(new OrgUnitPostToScDegreeRelation(rel, item));
            else
                scDegreeMap.remove(item);

        for (PostType item : model.getSelectedPostTypesLevels())
            if (!typesMap.containsKey(item))
                getSession().save(new OrgUnitPostToTypeRelation(rel, item));
            else
                typesMap.remove(item);

        for (OrgUnitPostToScStatusRelation relToDelete : scStatusMap.values())
            getSession().delete(relToDelete);

        for (OrgUnitPostToScDegreeRelation relToDelete : scDegreeMap.values())
            getSession().delete(relToDelete);

        for (OrgUnitPostToTypeRelation relToDelete : typesMap.values())
            getSession().delete(relToDelete);
    }

    @SuppressWarnings("unchecked")
    private Map<ScienceStatus, OrgUnitPostToScStatusRelation> getScStatusMap(OrgUnitPostRelation parent)
    {
        Map<ScienceStatus, OrgUnitPostToScStatusRelation> map = new HashMap<>();
        for (OrgUnitPostToScStatusRelation rel : (List<OrgUnitPostToScStatusRelation>) getSession().createCriteria(OrgUnitPostToScStatusRelation.class).add(Restrictions.eq(OrgUnitPostToScStatusRelation.L_ORG_UNIT_POST_RELATION, parent)).list())
            map.put(rel.getAcademicStatus(), rel);
        return map;
    }

    @SuppressWarnings("unchecked")
    private Map<ScienceDegree, OrgUnitPostToScDegreeRelation> getScDegreeMap(OrgUnitPostRelation parent)
    {
        Map<ScienceDegree, OrgUnitPostToScDegreeRelation> map = new HashMap<>();
        for (OrgUnitPostToScDegreeRelation rel : (List<OrgUnitPostToScDegreeRelation>) getSession().createCriteria(OrgUnitPostToScDegreeRelation.class).add(Restrictions.eq(OrgUnitPostToScDegreeRelation.L_ORG_UNIT_POST_RELATION, parent)).list())
            map.put(rel.getAcademicDegree(), rel);
        return map;
    }

    @SuppressWarnings("unchecked")
    private Map<PostType, OrgUnitPostToTypeRelation> getPostTypesMap(OrgUnitPostRelation parent)
    {
        Map<PostType, OrgUnitPostToTypeRelation> map = new HashMap<>();
        for (OrgUnitPostToTypeRelation rel : (List<OrgUnitPostToTypeRelation>) getSession().createCriteria(OrgUnitPostToTypeRelation.class).add(Restrictions.eq(OrgUnitPostToTypeRelation.L_ORG_UNIT_POST_RELATION, parent)).list())
            map.put(rel.getPostType(), rel);
        return map;
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        OrgUnitPostRelation rel = model.getOrgUnitPostRelation();

        Integer minAge = rel.getMinAge();
        Integer maxAge = rel.getMaxAge();

        if (rel.isAcademicStatusRequired() && model.getSelectedScientificStatusesList().isEmpty())
            errors.add("Параметр «Ученое звание» указан как значимый. Необходимо указать хотя бы одно ученое звание.", "scStatuses");

        if (rel.isAcademicDegreeRequired() && model.getSelectedScientificDegreesList().isEmpty())
            errors.add("Параметр «Ученая степень» указан как значимый. Необходимо указать хотя бы одну ученую степень.", "scDegrees");

        if (rel.isAgeRequired() && null == minAge && null == maxAge)
            errors.add("Параметр «Возраст» указан как значимый. Необходимо указать хотя бы один из возрастных пределов.", "ageMin", "ageMax");

        if (rel.isPostTypeRequired() && model.getSelectedPostTypesLevels().isEmpty())
            errors.add("Параметр «Тип назначения на должность» указан как значимый. Необходимо указать хотя бы один из них.", "postTypes");

        if (null != maxAge && null != minAge && maxAge < minAge)
            errors.add("Минимальный возраст должен быть меньше максимального", "ageMin", "ageMax");
    }

}