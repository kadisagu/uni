package ru.tandemservice.uniemp.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.employee.PaymentOnFOTToPaymentsRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь выплат, начисляемых на месячный фонд оплаты труда с выплатами
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PaymentOnFOTToPaymentsRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.employee.PaymentOnFOTToPaymentsRelation";
    public static final String ENTITY_NAME = "paymentOnFOTToPaymentsRelation";
    public static final int VERSION_HASH = 1495204999;
    private static IEntityMeta ENTITY_META;

    public static final String L_FIRST = "first";
    public static final String L_SECOND = "second";

    private Payment _first;     // Выплата
    private Payment _second;     // Выплата

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выплата. Свойство не может быть null.
     */
    @NotNull
    public Payment getFirst()
    {
        return _first;
    }

    /**
     * @param first Выплата. Свойство не может быть null.
     */
    public void setFirst(Payment first)
    {
        dirty(_first, first);
        _first = first;
    }

    /**
     * @return Выплата. Свойство не может быть null.
     */
    @NotNull
    public Payment getSecond()
    {
        return _second;
    }

    /**
     * @param second Выплата. Свойство не может быть null.
     */
    public void setSecond(Payment second)
    {
        dirty(_second, second);
        _second = second;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PaymentOnFOTToPaymentsRelationGen)
        {
            setFirst(((PaymentOnFOTToPaymentsRelation)another).getFirst());
            setSecond(((PaymentOnFOTToPaymentsRelation)another).getSecond());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PaymentOnFOTToPaymentsRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PaymentOnFOTToPaymentsRelation.class;
        }

        public T newInstance()
        {
            return (T) new PaymentOnFOTToPaymentsRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "first":
                    return obj.getFirst();
                case "second":
                    return obj.getSecond();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "first":
                    obj.setFirst((Payment) value);
                    return;
                case "second":
                    obj.setSecond((Payment) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "first":
                        return true;
                case "second":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "first":
                    return true;
                case "second":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "first":
                    return Payment.class;
                case "second":
                    return Payment.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PaymentOnFOTToPaymentsRelation> _dslPath = new Path<PaymentOnFOTToPaymentsRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PaymentOnFOTToPaymentsRelation");
    }
            

    /**
     * @return Выплата. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.PaymentOnFOTToPaymentsRelation#getFirst()
     */
    public static Payment.Path<Payment> first()
    {
        return _dslPath.first();
    }

    /**
     * @return Выплата. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.PaymentOnFOTToPaymentsRelation#getSecond()
     */
    public static Payment.Path<Payment> second()
    {
        return _dslPath.second();
    }

    public static class Path<E extends PaymentOnFOTToPaymentsRelation> extends EntityPath<E>
    {
        private Payment.Path<Payment> _first;
        private Payment.Path<Payment> _second;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выплата. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.PaymentOnFOTToPaymentsRelation#getFirst()
     */
        public Payment.Path<Payment> first()
        {
            if(_first == null )
                _first = new Payment.Path<Payment>(L_FIRST, this);
            return _first;
        }

    /**
     * @return Выплата. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.PaymentOnFOTToPaymentsRelation#getSecond()
     */
        public Payment.Path<Payment> second()
        {
            if(_second == null )
                _second = new Payment.Path<Payment>(L_SECOND, this);
            return _second;
        }

        public Class getEntityClass()
        {
            return PaymentOnFOTToPaymentsRelation.class;
        }

        public String getEntityName()
        {
            return "paymentOnFOTToPaymentsRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
