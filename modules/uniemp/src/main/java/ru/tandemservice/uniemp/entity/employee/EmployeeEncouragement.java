package ru.tandemservice.uniemp.entity.employee;

import ru.tandemservice.uniemp.entity.catalog.EncouragementType;
import ru.tandemservice.uniemp.entity.employee.gen.EmployeeEncouragementGen;

public class EmployeeEncouragement extends EmployeeEncouragementGen
{
    public static final String[] KEY_ENCOURAGEMENT_TYPE_TITLE = new String[] { L_TYPE, EncouragementType.P_TITLE };
}