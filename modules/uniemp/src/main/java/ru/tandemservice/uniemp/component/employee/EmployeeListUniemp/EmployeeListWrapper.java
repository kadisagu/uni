/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.EmployeeListUniemp;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.employeebase.base.entity.Employee;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dseleznev
 * Created on: 09.10.2010
 */
public class EmployeeListWrapper extends IdentifiableWrapper<Employee>
{
    private static final long serialVersionUID = 4721225369045138222L;
    public static final String P_EMPLOYEE_CODE = "employeeCode";
    public static final String P_FULL_FIO = "title";

    private String _employeeCode;
    private List<EmployeePostListWrapper> _postsList;
    private boolean _cantDeleteEmployee;

    public EmployeeListWrapper(Long id, String title, String employeeCode)
    {
        super(id, title);
        _employeeCode = employeeCode;
    }

    public String getEmployeeCode()
    {
        return _employeeCode;
    }

    public void setEmployeeCode(String employeeCode)
    {
        this._employeeCode = employeeCode;
    }

    public List<EmployeePostListWrapper> getPostsList()
    {
        if(null == _postsList) _postsList = new ArrayList<>();
        return _postsList;
    }

    public void setPostsList(List<EmployeePostListWrapper> postsList)
    {
        this._postsList = postsList;
    }

    public boolean isCantDeleteEmployee()
    {
        return _cantDeleteEmployee;
    }

    public void setCantDeleteEmployee(boolean cantDeleteEmployee)
    {
        this._cantDeleteEmployee = cantDeleteEmployee;
    }
}