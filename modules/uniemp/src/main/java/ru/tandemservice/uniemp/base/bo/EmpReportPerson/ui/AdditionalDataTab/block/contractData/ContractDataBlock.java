/* $Id$ */
package ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.AdditionalDataTab.block.contractData;

import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.datasource.IUIDataSource;

import ru.tandemservice.uniemp.entity.catalog.LabourContractType;

/**
 * @author Vasily Zhukov
 * @since 05.08.2011
 */
public class ContractDataBlock
{
    public static final String LAB_CONTR_TYPE_DS = "labContrTypeDS";

    public static void onBeforeDataSourceFetch(IUIDataSource dataSource, ContractDataParam param)
    {
    }

    public static IDefaultComboDataSourceHandler createLabContrTypeDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, LabourContractType.class);
    }
}
