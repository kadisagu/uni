/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniemp.component.settings.PaymentBaseValueAddEdit;

import java.util.List;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.employee.PaymentToPostBaseValue;

/**
 * @author dseleznev
 * Created on: 29.09.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        if (null != model.getPaymentId())
            model.setPayment(get(Payment.class, model.getPaymentId()));

        if (null != model.getPaymentToPostBaseValueRelId())
            model.setPaymentToPostBaseValue(get(PaymentToPostBaseValue.class, model.getPaymentToPostBaseValueRelId()));

        if(null != model.getPayment())
            model.getPaymentToPostBaseValue().setPayment(model.getPayment());
        
        if (null == model.getPayment() && null != model.getPaymentToPostBaseValue().getId())
            model.setPayment(model.getPaymentToPostBaseValue().getPayment());

        model.setPostsListModel(new BaseSingleSelectModel()
        {
            @Override
            public ListResult<PostBoundedWithQGandQL> findValues(String filter)
            {
                MQBuilder subBuilder = new MQBuilder(PaymentToPostBaseValue.ENTITY_CLASS, "rel", new String[] { PaymentToPostBaseValue.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.P_ID });
                subBuilder.add(MQExpression.eq("rel", PaymentToPostBaseValue.L_PAYMENT, model.getPayment()));
                if (null != model.getPaymentToPostBaseValue().getId())
                    subBuilder.add(MQExpression.notEq("rel", PaymentToPostBaseValue.P_ID, model.getPaymentToPostBaseValue().getId()));

                MQBuilder builder = new MQBuilder(PostBoundedWithQGandQL.ENTITY_CLASS, "p");
                builder.add(MQExpression.notIn("p", PostBoundedWithQGandQL.P_ID, subBuilder));
                builder.add(MQExpression.like("p", PostBoundedWithQGandQL.P_TITLE, CoreStringUtils.escapeLike(filter)));

                List<PostBoundedWithQGandQL> list = builder.getResultList(getSession(), 0, 25);
                return new ListResult<>(list, builder.getResultCount(getSession()));
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                return get(PostBoundedWithQGandQL.class, (Long)primaryKey);
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((PostBoundedWithQGandQL)value).getFullTitleWithSalary();
            }
        });
    }

    @Override
    public void update(Model model)
    {
        getSession().saveOrUpdate(model.getPaymentToPostBaseValue());
    }
}