/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.service;

import java.util.Date;

import org.tandemframework.core.exception.ApplicationException;

import ru.tandemservice.uni.services.UniService;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.catalog.StaffListState;
import ru.tandemservice.uniemp.entity.employee.VacationSchedule;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.services.visatask.ITouchVisaTaskHandler;

/**
 * Стандартный сервис завершения процедуры согласования приказа
 *
 * @author iolshvang
 * @since 07.04.2011
 */
public class AcceptVisaVacationScheduleService extends UniService implements ITouchVisaTaskHandler
{
    private IAbstractDocument _vacationSchedule;

    @Override
    public void init(IAbstractDocument document)
    {
        if (_vacationSchedule != null) {
            throw new DoubleInitializeServiceError(this);
        }
        _vacationSchedule = (IAbstractDocument)document;
    }

    @Override
    protected void doValidate()
    {
        if (UniempDefines.STAFF_LIST_STATUS_ACCEPTED.equals(_vacationSchedule.getState().getCode()))
            throw new ApplicationException("График уже в состоянии «" + _vacationSchedule.getState().getTitle() + "».");
    }

    @Override
    protected void doExecute() throws Exception
    {
        _vacationSchedule.setState(getCoreDao().getCatalogItem(StaffListState.class, UniempDefines.STAFF_LIST_STATUS_ACCEPTED));
        ((VacationSchedule)_vacationSchedule).setAcceptDate(new Date());
        getCoreDao().update(_vacationSchedule);
    }
}