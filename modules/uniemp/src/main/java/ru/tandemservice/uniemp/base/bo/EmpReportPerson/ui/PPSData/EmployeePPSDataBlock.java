package ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.PPSData;

import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;

import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import ru.tandemservice.uniemp.entity.catalog.MedicalEducationLevel;
import ru.tandemservice.uniemp.entity.catalog.MedicalQualificationCategory;

/**
 * @author Vasily Zhukov
 * @since 08.02.2011
 */
public class EmployeePPSDataBlock
{
    // names
    public static final String MEDICAL_EDUCATION_LEVEL_DS = "medicalEducationLevelDS";
    public static final String MEDICAL_QUALIFICATION_CATEGORY_DS = "medicalQualificationCategoryDS";


    public static IDefaultComboDataSourceHandler createMedicalEducationLevelDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, MedicalEducationLevel.class);
    }

    public static IDefaultComboDataSourceHandler createMedicalQualificationCategoryDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, MedicalQualificationCategory.class);
    }
}
