/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.EmployeeHolidayAddEdit;

import java.util.Date;

import org.tandemframework.shared.employeebase.catalog.entity.EmployeeWorkWeekDuration;
import ru.tandemservice.uniemp.dao.IUniempDAO;

/**
 * @author dseleznev
 * Created on: 22.12.2008
 */
public interface IDAO extends IUniempDAO<Model>
{
    public boolean isIndustrialCalendarHoliday(EmployeeWorkWeekDuration weekDuration, Date date);
}