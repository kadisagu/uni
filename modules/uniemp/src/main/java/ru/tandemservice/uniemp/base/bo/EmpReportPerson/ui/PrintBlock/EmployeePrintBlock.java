/* $Id$ */
package ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.PrintBlock;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.base.bo.EmployeeReportPerson.ui.Add.EmployeeReportPersonAddUI;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.print.ReportPrintListColumn;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.print.ReportPrintPathColumn;
import ru.tandemservice.uniemp.entity.employee.CombinationPost;
import ru.tandemservice.uniemp.entity.employee.EmployeeCard;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Vasily Zhukov
 * @since 11.08.2011
 */
public class EmployeePrintBlock implements IReportPrintBlock
{
    private IReportPrintCheckbox _block = new ReportPrintCheckbox();
    private IReportPrintCheckbox _staffRate = new ReportPrintCheckbox();
    private IReportPrintCheckbox _scientificWork = new ReportPrintCheckbox();
    private IReportPrintCheckbox _invention = new ReportPrintCheckbox();
    private IReportPrintCheckbox _combinationPost = new ReportPrintCheckbox();

    @Override
    public String getCheckJS()
    {
        List<String> ids = new ArrayList<>();
        for (int i = 1; i <= 4; i++)
            ids.add("chEmpl" + i);
        return ReportJavaScriptUtil.getCheckJS(ids);
    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        // соединяем кр и должность через left join, потому что могли быть выбраны фильтры "не является КР" и "не занимает должность" 
        String employeeAlias = dql.leftJoinEntity(Employee.class, Employee.person());
        String employeePostAlias = dql.leftJoinEntity(employeeAlias, EmployeePost.class, EmployeePost.employee());

        if (_staffRate.isActive())
        {
            String nextAlias = dql.nextAlias();

            int employeePostIndex = dql.addListAggregateColumn(employeePostAlias);

            // добавляем колонку для вычисления ставки
            // ставки могут быть одинаковые для разных employeePost, поэтому указываем ключ колонки employeePostIndex
            final int i = dql.addListAggregateColumn(new DQLSelectBuilder().fromEntity(EmployeePostStaffRateItem.class, nextAlias)
                    .column(DQLFunctions.sum(DQLExpressions.property(EmployeePostStaffRateItem.staffRateInteger().fromAlias(nextAlias))))
                    .where(DQLExpressions.eq(DQLExpressions.property(EmployeePostStaffRateItem.employeePost().fromAlias(nextAlias)), DQLExpressions.property(employeePostAlias)))
                    .buildQuery(), employeePostIndex);

            printInfo.addPrintColumn(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, new ReportPrintListColumn(
                    "staffRate", i, null, source -> source == null ? null : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(((Number)source).doubleValue() / 10000.0)));
        }

        if (_scientificWork.isActive())
        {
            String a1 = dql.leftJoinEntity(employeeAlias, EmployeeCard.class, EmployeeCard.employee());

            final int i = dql.addLastAggregateColumn(a1, EmployeeCard.scientificWork());

            printInfo.addPrintColumn(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, new ReportPrintPathColumn("scientificWork", i, null, new IFormatter<Boolean>()
            {
                @Override
                public String format(Boolean source)
                {
                    return source == null ? null : (source ? "Есть" : "Нет");
                }
            }));
        }

        if (_invention.isActive())
        {
            String a1 = dql.leftJoinEntity(employeeAlias, EmployeeCard.class, EmployeeCard.employee());

            final int i = dql.addLastAggregateColumn(a1, EmployeeCard.invention());

            printInfo.addPrintColumn(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, new ReportPrintPathColumn("invention", i, null, new IFormatter<Boolean>()
            {
                @Override
                public String format(Boolean source)
                {
                    return source == null ? null : (source ? "Есть" : "Нет");
                }
            }));
        }

        if (_combinationPost.isActive())
        {
            // соединяем таблицу PersonSportAchievement к запросу
            String a1 = dql.leftJoinEntity(employeePostAlias, CombinationPost.class, CombinationPost.employeePost());

            // добавляем печатную колонку title к запросу
            final int i = dql.addListAggregateColumn(a1);

            // добавляем печатную колонку на указанный лист
            printInfo.addPrintColumn(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, new ReportPrintColumn("combinationPost")
            {
                @SuppressWarnings("unchecked")
                @Override
                public Object createColumnValue(Object[] data)
                {
                    List<String> result = new ArrayList<>();

                    for (CombinationPost item : (List<CombinationPost>) data[i])
                        if (item != null)
                            result.add(item.getPostBoundedWithQGandQL().getFullTitle() + " : " + item.getOrgUnit().getTitleWithType() + " - " + item.getCombinationPostType().getShortTitle());

                    Collections.sort(result);
                    return StringUtils.join(result, ";\n");
                }
            });
        }
    }

    // Getters

    public IReportPrintCheckbox getBlock()
    {
        return _block;
    }

    public IReportPrintCheckbox getStaffRate()
    {
        return _staffRate;
    }

    public IReportPrintCheckbox getScientificWork()
    {
        return _scientificWork;
    }

    public IReportPrintCheckbox getInvention()
    {
        return _invention;
    }

    public IReportPrintCheckbox getCombinationPost()
    {
        return _combinationPost;
    }
}
