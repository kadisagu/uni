package ru.tandemservice.uniemp.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.catalog.GlobalFinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Источник финансирования (настройка названий)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FinancingSourceGen extends EntityBase
 implements INaturalIdentifiable<FinancingSourceGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.catalog.FinancingSource";
    public static final String ENTITY_NAME = "financingSource";
    public static final int VERSION_HASH = -664583368;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String L_PARENT = "parent";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_NOMINATIVE_CASE_TITLE = "nominativeCaseTitle";
    public static final String P_GENITIVE_CASE_TITLE = "genitiveCaseTitle";
    public static final String P_DATIVE_CASE_TITLE = "dativeCaseTitle";
    public static final String P_ACCUSATIVE_CASE_TITLE = "accusativeCaseTitle";
    public static final String P_INSTRUMENTAL_CASE_TITLE = "instrumentalCaseTitle";
    public static final String P_PREPOSITIONAL_CASE_TITLE = "prepositionalCaseTitle";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private GlobalFinancingSource _parent;     // Элемент системного справочника
    private String _shortTitle;     // Сокращенное название
    private String _nominativeCaseTitle;     // Именительный падеж
    private String _genitiveCaseTitle;     // Родительный падеж
    private String _dativeCaseTitle;     // Дательный падеж
    private String _accusativeCaseTitle;     // Винительный падеж
    private String _instrumentalCaseTitle;     // Творительный падеж
    private String _prepositionalCaseTitle;     // Предложный падеж
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Элемент системного справочника. Свойство не может быть null.
     */
    @NotNull
    public GlobalFinancingSource getParent()
    {
        return _parent;
    }

    /**
     * @param parent Элемент системного справочника. Свойство не может быть null.
     */
    public void setParent(GlobalFinancingSource parent)
    {
        dirty(_parent, parent);
        _parent = parent;
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название. Свойство не может быть null.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Именительный падеж.
     */
    @Length(max=255)
    public String getNominativeCaseTitle()
    {
        return _nominativeCaseTitle;
    }

    /**
     * @param nominativeCaseTitle Именительный падеж.
     */
    public void setNominativeCaseTitle(String nominativeCaseTitle)
    {
        dirty(_nominativeCaseTitle, nominativeCaseTitle);
        _nominativeCaseTitle = nominativeCaseTitle;
    }

    /**
     * @return Родительный падеж.
     */
    @Length(max=255)
    public String getGenitiveCaseTitle()
    {
        return _genitiveCaseTitle;
    }

    /**
     * @param genitiveCaseTitle Родительный падеж.
     */
    public void setGenitiveCaseTitle(String genitiveCaseTitle)
    {
        dirty(_genitiveCaseTitle, genitiveCaseTitle);
        _genitiveCaseTitle = genitiveCaseTitle;
    }

    /**
     * @return Дательный падеж.
     */
    @Length(max=255)
    public String getDativeCaseTitle()
    {
        return _dativeCaseTitle;
    }

    /**
     * @param dativeCaseTitle Дательный падеж.
     */
    public void setDativeCaseTitle(String dativeCaseTitle)
    {
        dirty(_dativeCaseTitle, dativeCaseTitle);
        _dativeCaseTitle = dativeCaseTitle;
    }

    /**
     * @return Винительный падеж.
     */
    @Length(max=255)
    public String getAccusativeCaseTitle()
    {
        return _accusativeCaseTitle;
    }

    /**
     * @param accusativeCaseTitle Винительный падеж.
     */
    public void setAccusativeCaseTitle(String accusativeCaseTitle)
    {
        dirty(_accusativeCaseTitle, accusativeCaseTitle);
        _accusativeCaseTitle = accusativeCaseTitle;
    }

    /**
     * @return Творительный падеж.
     */
    @Length(max=255)
    public String getInstrumentalCaseTitle()
    {
        return _instrumentalCaseTitle;
    }

    /**
     * @param instrumentalCaseTitle Творительный падеж.
     */
    public void setInstrumentalCaseTitle(String instrumentalCaseTitle)
    {
        dirty(_instrumentalCaseTitle, instrumentalCaseTitle);
        _instrumentalCaseTitle = instrumentalCaseTitle;
    }

    /**
     * @return Предложный падеж.
     */
    @Length(max=255)
    public String getPrepositionalCaseTitle()
    {
        return _prepositionalCaseTitle;
    }

    /**
     * @param prepositionalCaseTitle Предложный падеж.
     */
    public void setPrepositionalCaseTitle(String prepositionalCaseTitle)
    {
        dirty(_prepositionalCaseTitle, prepositionalCaseTitle);
        _prepositionalCaseTitle = prepositionalCaseTitle;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FinancingSourceGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((FinancingSource)another).getCode());
            }
            setParent(((FinancingSource)another).getParent());
            setShortTitle(((FinancingSource)another).getShortTitle());
            setNominativeCaseTitle(((FinancingSource)another).getNominativeCaseTitle());
            setGenitiveCaseTitle(((FinancingSource)another).getGenitiveCaseTitle());
            setDativeCaseTitle(((FinancingSource)another).getDativeCaseTitle());
            setAccusativeCaseTitle(((FinancingSource)another).getAccusativeCaseTitle());
            setInstrumentalCaseTitle(((FinancingSource)another).getInstrumentalCaseTitle());
            setPrepositionalCaseTitle(((FinancingSource)another).getPrepositionalCaseTitle());
            setTitle(((FinancingSource)another).getTitle());
        }
    }

    public INaturalId<FinancingSourceGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<FinancingSourceGen>
    {
        private static final String PROXY_NAME = "FinancingSourceNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof FinancingSourceGen.NaturalId) ) return false;

            FinancingSourceGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FinancingSourceGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FinancingSource.class;
        }

        public T newInstance()
        {
            return (T) new FinancingSource();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "parent":
                    return obj.getParent();
                case "shortTitle":
                    return obj.getShortTitle();
                case "nominativeCaseTitle":
                    return obj.getNominativeCaseTitle();
                case "genitiveCaseTitle":
                    return obj.getGenitiveCaseTitle();
                case "dativeCaseTitle":
                    return obj.getDativeCaseTitle();
                case "accusativeCaseTitle":
                    return obj.getAccusativeCaseTitle();
                case "instrumentalCaseTitle":
                    return obj.getInstrumentalCaseTitle();
                case "prepositionalCaseTitle":
                    return obj.getPrepositionalCaseTitle();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "parent":
                    obj.setParent((GlobalFinancingSource) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "nominativeCaseTitle":
                    obj.setNominativeCaseTitle((String) value);
                    return;
                case "genitiveCaseTitle":
                    obj.setGenitiveCaseTitle((String) value);
                    return;
                case "dativeCaseTitle":
                    obj.setDativeCaseTitle((String) value);
                    return;
                case "accusativeCaseTitle":
                    obj.setAccusativeCaseTitle((String) value);
                    return;
                case "instrumentalCaseTitle":
                    obj.setInstrumentalCaseTitle((String) value);
                    return;
                case "prepositionalCaseTitle":
                    obj.setPrepositionalCaseTitle((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "parent":
                        return true;
                case "shortTitle":
                        return true;
                case "nominativeCaseTitle":
                        return true;
                case "genitiveCaseTitle":
                        return true;
                case "dativeCaseTitle":
                        return true;
                case "accusativeCaseTitle":
                        return true;
                case "instrumentalCaseTitle":
                        return true;
                case "prepositionalCaseTitle":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "parent":
                    return true;
                case "shortTitle":
                    return true;
                case "nominativeCaseTitle":
                    return true;
                case "genitiveCaseTitle":
                    return true;
                case "dativeCaseTitle":
                    return true;
                case "accusativeCaseTitle":
                    return true;
                case "instrumentalCaseTitle":
                    return true;
                case "prepositionalCaseTitle":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "parent":
                    return GlobalFinancingSource.class;
                case "shortTitle":
                    return String.class;
                case "nominativeCaseTitle":
                    return String.class;
                case "genitiveCaseTitle":
                    return String.class;
                case "dativeCaseTitle":
                    return String.class;
                case "accusativeCaseTitle":
                    return String.class;
                case "instrumentalCaseTitle":
                    return String.class;
                case "prepositionalCaseTitle":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FinancingSource> _dslPath = new Path<FinancingSource>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FinancingSource");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniemp.entity.catalog.FinancingSource#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Элемент системного справочника. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.catalog.FinancingSource#getParent()
     */
    public static GlobalFinancingSource.Path<GlobalFinancingSource> parent()
    {
        return _dslPath.parent();
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.catalog.FinancingSource#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Именительный падеж.
     * @see ru.tandemservice.uniemp.entity.catalog.FinancingSource#getNominativeCaseTitle()
     */
    public static PropertyPath<String> nominativeCaseTitle()
    {
        return _dslPath.nominativeCaseTitle();
    }

    /**
     * @return Родительный падеж.
     * @see ru.tandemservice.uniemp.entity.catalog.FinancingSource#getGenitiveCaseTitle()
     */
    public static PropertyPath<String> genitiveCaseTitle()
    {
        return _dslPath.genitiveCaseTitle();
    }

    /**
     * @return Дательный падеж.
     * @see ru.tandemservice.uniemp.entity.catalog.FinancingSource#getDativeCaseTitle()
     */
    public static PropertyPath<String> dativeCaseTitle()
    {
        return _dslPath.dativeCaseTitle();
    }

    /**
     * @return Винительный падеж.
     * @see ru.tandemservice.uniemp.entity.catalog.FinancingSource#getAccusativeCaseTitle()
     */
    public static PropertyPath<String> accusativeCaseTitle()
    {
        return _dslPath.accusativeCaseTitle();
    }

    /**
     * @return Творительный падеж.
     * @see ru.tandemservice.uniemp.entity.catalog.FinancingSource#getInstrumentalCaseTitle()
     */
    public static PropertyPath<String> instrumentalCaseTitle()
    {
        return _dslPath.instrumentalCaseTitle();
    }

    /**
     * @return Предложный падеж.
     * @see ru.tandemservice.uniemp.entity.catalog.FinancingSource#getPrepositionalCaseTitle()
     */
    public static PropertyPath<String> prepositionalCaseTitle()
    {
        return _dslPath.prepositionalCaseTitle();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uniemp.entity.catalog.FinancingSource#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends FinancingSource> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private GlobalFinancingSource.Path<GlobalFinancingSource> _parent;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<String> _nominativeCaseTitle;
        private PropertyPath<String> _genitiveCaseTitle;
        private PropertyPath<String> _dativeCaseTitle;
        private PropertyPath<String> _accusativeCaseTitle;
        private PropertyPath<String> _instrumentalCaseTitle;
        private PropertyPath<String> _prepositionalCaseTitle;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniemp.entity.catalog.FinancingSource#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(FinancingSourceGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Элемент системного справочника. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.catalog.FinancingSource#getParent()
     */
        public GlobalFinancingSource.Path<GlobalFinancingSource> parent()
        {
            if(_parent == null )
                _parent = new GlobalFinancingSource.Path<GlobalFinancingSource>(L_PARENT, this);
            return _parent;
        }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.catalog.FinancingSource#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(FinancingSourceGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Именительный падеж.
     * @see ru.tandemservice.uniemp.entity.catalog.FinancingSource#getNominativeCaseTitle()
     */
        public PropertyPath<String> nominativeCaseTitle()
        {
            if(_nominativeCaseTitle == null )
                _nominativeCaseTitle = new PropertyPath<String>(FinancingSourceGen.P_NOMINATIVE_CASE_TITLE, this);
            return _nominativeCaseTitle;
        }

    /**
     * @return Родительный падеж.
     * @see ru.tandemservice.uniemp.entity.catalog.FinancingSource#getGenitiveCaseTitle()
     */
        public PropertyPath<String> genitiveCaseTitle()
        {
            if(_genitiveCaseTitle == null )
                _genitiveCaseTitle = new PropertyPath<String>(FinancingSourceGen.P_GENITIVE_CASE_TITLE, this);
            return _genitiveCaseTitle;
        }

    /**
     * @return Дательный падеж.
     * @see ru.tandemservice.uniemp.entity.catalog.FinancingSource#getDativeCaseTitle()
     */
        public PropertyPath<String> dativeCaseTitle()
        {
            if(_dativeCaseTitle == null )
                _dativeCaseTitle = new PropertyPath<String>(FinancingSourceGen.P_DATIVE_CASE_TITLE, this);
            return _dativeCaseTitle;
        }

    /**
     * @return Винительный падеж.
     * @see ru.tandemservice.uniemp.entity.catalog.FinancingSource#getAccusativeCaseTitle()
     */
        public PropertyPath<String> accusativeCaseTitle()
        {
            if(_accusativeCaseTitle == null )
                _accusativeCaseTitle = new PropertyPath<String>(FinancingSourceGen.P_ACCUSATIVE_CASE_TITLE, this);
            return _accusativeCaseTitle;
        }

    /**
     * @return Творительный падеж.
     * @see ru.tandemservice.uniemp.entity.catalog.FinancingSource#getInstrumentalCaseTitle()
     */
        public PropertyPath<String> instrumentalCaseTitle()
        {
            if(_instrumentalCaseTitle == null )
                _instrumentalCaseTitle = new PropertyPath<String>(FinancingSourceGen.P_INSTRUMENTAL_CASE_TITLE, this);
            return _instrumentalCaseTitle;
        }

    /**
     * @return Предложный падеж.
     * @see ru.tandemservice.uniemp.entity.catalog.FinancingSource#getPrepositionalCaseTitle()
     */
        public PropertyPath<String> prepositionalCaseTitle()
        {
            if(_prepositionalCaseTitle == null )
                _prepositionalCaseTitle = new PropertyPath<String>(FinancingSourceGen.P_PREPOSITIONAL_CASE_TITLE, this);
            return _prepositionalCaseTitle;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uniemp.entity.catalog.FinancingSource#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(FinancingSourceGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return FinancingSource.class;
        }

        public String getEntityName()
        {
            return "financingSource";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
