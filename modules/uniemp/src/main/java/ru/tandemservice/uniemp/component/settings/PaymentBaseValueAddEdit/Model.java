/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniemp.component.settings.PaymentBaseValueAddEdit;

import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.employee.PaymentToPostBaseValue;

/**
 * @author dseleznev
 * Created on: 29.09.2009
 */
@Input(keys = { "paymentToPostBaseValueRelId", "paymentId" }, bindings = { "paymentToPostBaseValueRelId", "paymentId" })
public class Model
{
    private Long _paymentId;
    private Payment _payment;
    private Long _paymentToPostBaseValueRelId;
    private PaymentToPostBaseValue _paymentToPostBaseValue = new PaymentToPostBaseValue();
    private ISelectModel _postsListModel;

    public Long getPaymentId()
    {
        return _paymentId;
    }

    public void setPaymentId(Long paymentId)
    {
        this._paymentId = paymentId;
    }

    public Payment getPayment()
    {
        return _payment;
    }

    public void setPayment(Payment payment)
    {
        this._payment = payment;
    }

    public Long getPaymentToPostBaseValueRelId()
    {
        return _paymentToPostBaseValueRelId;
    }

    public void setPaymentToPostBaseValueRelId(Long paymentToPostBaseValueRelId)
    {
        this._paymentToPostBaseValueRelId = paymentToPostBaseValueRelId;
    }

    public PaymentToPostBaseValue getPaymentToPostBaseValue()
    {
        return _paymentToPostBaseValue;
    }

    public void setPaymentToPostBaseValue(PaymentToPostBaseValue paymentToPostBaseValue)
    {
        this._paymentToPostBaseValue = paymentToPostBaseValue;
    }

    public ISelectModel getPostsListModel()
    {
        return _postsListModel;
    }

    public void setPostsListModel(ISelectModel postsListModel)
    {
        this._postsListModel = postsListModel;
    }
}