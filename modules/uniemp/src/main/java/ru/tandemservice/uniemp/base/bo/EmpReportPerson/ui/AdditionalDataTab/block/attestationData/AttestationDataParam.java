/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.AdditionalDataTab.block.attestationData;

import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import ru.tandemservice.uniemp.entity.employee.EmployeeCertificationItem;

import java.util.Date;

/**
 * @author Vasily Zhukov
 * @since 09.04.2012
 */
public class AttestationDataParam implements IReportDQLModifier
{
    private IReportParam<Date> _hasAttestationFrom = new ReportParam<>();
    private IReportParam<Date> _hasAttestationTo = new ReportParam<>();
    private IReportParam<Date> _noAttestationFrom = new ReportParam<>();
    private IReportParam<Date> _noAttestationTo = new ReportParam<>();

    private String employeePostAlias(ReportDQL dql)
    {
        // соединяем кадрового ресурса
        String employeeAlias = dql.innerJoinEntity(Employee.class, Employee.person());

        // соединяем должность
        return dql.innerJoinEntity(employeeAlias, EmployeePost.class, EmployeePost.employee());
    }

    private String certAlias(ReportDQL dql)
    {
        // соединяем должность
        String employeePostAlias = employeePostAlias(dql);

        // соединяем аттестацию
        return dql.innerJoinEntity(employeePostAlias, EmployeeCertificationItem.class, EmployeeCertificationItem.employeePost());
    }

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {

    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (_hasAttestationFrom.isActive())
        {
            dql.builder.where(DQLExpressions.ge(DQLExpressions.property(EmployeeCertificationItem.certificationDate().fromAlias(certAlias(dql))), DQLExpressions.valueDate(_hasAttestationFrom.getData())));
        }

        if (_hasAttestationTo.isActive())
        {
            dql.builder.where(DQLExpressions.le(DQLExpressions.property(EmployeeCertificationItem.certificationDate().fromAlias(certAlias(dql))), DQLExpressions.valueDate(_hasAttestationTo.getData())));
        }

        if (_noAttestationFrom.isActive() || _noAttestationTo.isActive())
        {
            String alias = dql.nextAlias();

            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmployeeCertificationItem.class, alias)
                    .where(DQLExpressions.eq(DQLExpressions.property(EmployeeCertificationItem.employeePost().fromAlias(alias)), DQLExpressions.property(employeePostAlias(dql))));

            if (_noAttestationFrom.isActive())
                builder.where(DQLExpressions.ge(DQLExpressions.property(EmployeeCertificationItem.certificationDate().fromAlias(alias)), DQLExpressions.valueDate(_noAttestationFrom.getData())));

            if (_noAttestationTo.isActive())
                builder.where(DQLExpressions.le(DQLExpressions.property(EmployeeCertificationItem.certificationDate().fromAlias(alias)), DQLExpressions.valueDate(_noAttestationTo.getData())));

            dql.builder.where(DQLExpressions.notExists(builder.buildQuery()));
        }
    }

    // Getters

    public IReportParam<Date> getHasAttestationFrom()
    {
        return _hasAttestationFrom;
    }

    public IReportParam<Date> getHasAttestationTo()
    {
        return _hasAttestationTo;
    }

    public IReportParam<Date> getNoAttestationFrom()
    {
        return _noAttestationFrom;
    }

    public IReportParam<Date> getNoAttestationTo()
    {
        return _noAttestationTo;
    }
}
