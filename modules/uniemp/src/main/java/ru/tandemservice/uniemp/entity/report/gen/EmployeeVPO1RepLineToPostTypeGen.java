package ru.tandemservice.uniemp.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import ru.tandemservice.uniemp.entity.catalog.EmployeeVPO1ReportLines;
import ru.tandemservice.uniemp.entity.report.EmployeeVPO1RepLineToPostType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь строки отчета «ВПО-1 - Сведения о персонале учреждения» с типом должности
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeVPO1RepLineToPostTypeGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.report.EmployeeVPO1RepLineToPostType";
    public static final String ENTITY_NAME = "employeeVPO1RepLineToPostType";
    public static final int VERSION_HASH = 1941031100;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_V_P_O1_REPORT_LINES = "employeeVPO1ReportLines";
    public static final String L_EMPLOYEE_TYPE = "employeeType";

    private EmployeeVPO1ReportLines _employeeVPO1ReportLines;     // Строка отчета
    private EmployeeType _employeeType;     // Тип должности/профессии сотрудников ОУ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Строка отчета. Свойство не может быть null.
     */
    @NotNull
    public EmployeeVPO1ReportLines getEmployeeVPO1ReportLines()
    {
        return _employeeVPO1ReportLines;
    }

    /**
     * @param employeeVPO1ReportLines Строка отчета. Свойство не может быть null.
     */
    public void setEmployeeVPO1ReportLines(EmployeeVPO1ReportLines employeeVPO1ReportLines)
    {
        dirty(_employeeVPO1ReportLines, employeeVPO1ReportLines);
        _employeeVPO1ReportLines = employeeVPO1ReportLines;
    }

    /**
     * @return Тип должности/профессии сотрудников ОУ. Свойство не может быть null.
     */
    @NotNull
    public EmployeeType getEmployeeType()
    {
        return _employeeType;
    }

    /**
     * @param employeeType Тип должности/профессии сотрудников ОУ. Свойство не может быть null.
     */
    public void setEmployeeType(EmployeeType employeeType)
    {
        dirty(_employeeType, employeeType);
        _employeeType = employeeType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EmployeeVPO1RepLineToPostTypeGen)
        {
            setEmployeeVPO1ReportLines(((EmployeeVPO1RepLineToPostType)another).getEmployeeVPO1ReportLines());
            setEmployeeType(((EmployeeVPO1RepLineToPostType)another).getEmployeeType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeVPO1RepLineToPostTypeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeVPO1RepLineToPostType.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeVPO1RepLineToPostType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "employeeVPO1ReportLines":
                    return obj.getEmployeeVPO1ReportLines();
                case "employeeType":
                    return obj.getEmployeeType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "employeeVPO1ReportLines":
                    obj.setEmployeeVPO1ReportLines((EmployeeVPO1ReportLines) value);
                    return;
                case "employeeType":
                    obj.setEmployeeType((EmployeeType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "employeeVPO1ReportLines":
                        return true;
                case "employeeType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "employeeVPO1ReportLines":
                    return true;
                case "employeeType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "employeeVPO1ReportLines":
                    return EmployeeVPO1ReportLines.class;
                case "employeeType":
                    return EmployeeType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeeVPO1RepLineToPostType> _dslPath = new Path<EmployeeVPO1RepLineToPostType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeVPO1RepLineToPostType");
    }
            

    /**
     * @return Строка отчета. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.report.EmployeeVPO1RepLineToPostType#getEmployeeVPO1ReportLines()
     */
    public static EmployeeVPO1ReportLines.Path<EmployeeVPO1ReportLines> employeeVPO1ReportLines()
    {
        return _dslPath.employeeVPO1ReportLines();
    }

    /**
     * @return Тип должности/профессии сотрудников ОУ. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.report.EmployeeVPO1RepLineToPostType#getEmployeeType()
     */
    public static EmployeeType.Path<EmployeeType> employeeType()
    {
        return _dslPath.employeeType();
    }

    public static class Path<E extends EmployeeVPO1RepLineToPostType> extends EntityPath<E>
    {
        private EmployeeVPO1ReportLines.Path<EmployeeVPO1ReportLines> _employeeVPO1ReportLines;
        private EmployeeType.Path<EmployeeType> _employeeType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Строка отчета. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.report.EmployeeVPO1RepLineToPostType#getEmployeeVPO1ReportLines()
     */
        public EmployeeVPO1ReportLines.Path<EmployeeVPO1ReportLines> employeeVPO1ReportLines()
        {
            if(_employeeVPO1ReportLines == null )
                _employeeVPO1ReportLines = new EmployeeVPO1ReportLines.Path<EmployeeVPO1ReportLines>(L_EMPLOYEE_V_P_O1_REPORT_LINES, this);
            return _employeeVPO1ReportLines;
        }

    /**
     * @return Тип должности/профессии сотрудников ОУ. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.report.EmployeeVPO1RepLineToPostType#getEmployeeType()
     */
        public EmployeeType.Path<EmployeeType> employeeType()
        {
            if(_employeeType == null )
                _employeeType = new EmployeeType.Path<EmployeeType>(L_EMPLOYEE_TYPE, this);
            return _employeeType;
        }

        public Class getEntityClass()
        {
            return EmployeeVPO1RepLineToPostType.class;
        }

        public String getEntityName()
        {
            return "employeeVPO1RepLineToPostType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
