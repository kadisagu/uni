/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniemp.base.ext.Employee.ui.PostView;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.caf.ui.support.IUISettings;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.common.component.selection.MQListResultBuilder;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostView.EmployeePostViewUI;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.IndustrialCalendar;
import org.tandemframework.shared.employeebase.base.entity.IndustrialCalendarHoliday;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeWorkWeekDuration;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeeWorkWeekDurationCodes;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.IHolidayDuration;
import org.tandemframework.shared.organization.base.util.OrgUnitAutocompleteModel;
import org.tandemframework.shared.person.base.bo.Person.ui.AcademicDegreeAddEdit.PersonAcademicDegreeAddEdit;
import org.tandemframework.shared.person.base.bo.Person.ui.AcademicStatusAddEdit.PersonAcademicStatusAddEdit;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import org.tandemframework.shared.person.base.entity.PersonAcademicStatus;
import org.tandemframework.tapsupport.component.selection.CommonSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.IUniempComponents;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.base.bo.EmpEmployeeCertificationItem.ui.AddEdit.EmpEmployeeCertificationItemAddEdit;
import ru.tandemservice.uniemp.base.bo.EmpEmployeeCertificationItem.ui.AddEdit.EmpEmployeeCertificationItemAddEditUI;
import ru.tandemservice.uniemp.base.bo.EmpEmployeePost.ui.PrintPersonCard.EmpEmployeePostPrintPersonCard;
import ru.tandemservice.uniemp.base.bo.EmpEmployeePost.ui.PrintPersonCard.EmpEmployeePostPrintPersonCardUI;
import ru.tandemservice.uniemp.base.bo.EmpEmployeeRetrainingItem.ui.AddEdit.EmpEmployeeRetrainingItemAddEdit;
import ru.tandemservice.uniemp.base.bo.EmpEmployeeRetrainingItem.ui.AddEdit.EmpEmployeeRetrainingItemAddEditUI;
import ru.tandemservice.uniemp.base.bo.EmpEmployeeTrainingItem.ui.AddEdit.EmpEmployeeTrainingItemAddEdit;
import ru.tandemservice.uniemp.base.bo.EmpEmployeeTrainingItem.ui.AddEdit.EmpEmployeeTrainingItemAddEditUI;
import ru.tandemservice.uniemp.dao.UniempDAO;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.CombinationPostType;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.catalog.PaymentUnit;
import ru.tandemservice.uniemp.entity.catalog.StaffListState;
import ru.tandemservice.uniemp.entity.employee.*;

import java.util.*;

/**
 * @author Vasily Zhukov
 * @since 30.03.2012
 */
public class EmployeePostViewExtUI extends UIAddon
{
    private EmployeePostViewLegacyModel model;

    private EmployeeHoliday _longTimeEmployeeHoliday;
    private String _period;

    public EmployeePostViewExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onComponentRefresh()
    {
        prepareLegacyModel();

        prepareEmployeeHoliday();
    }

    // Util

    public boolean isHasLongTimeEmployeeHoliday()
    {
        return _longTimeEmployeeHoliday != null;
    }

    public boolean isHasChildCare()
    {
        return _longTimeEmployeeHoliday != null && _longTimeEmployeeHoliday.getHolidayType().isChildCare();
    }

    public boolean isHasLabourContract()
    {
        return model.getLabourContract() != null;
    }

    public boolean isHasLabourContractFile()
    {
        return isHasLabourContract() && model.getLabourContract().getContractFile() != null;
    }

    public Map<String, Object> getContractParamenersMap()
    {
        return new ParametersMap()
                .add("accessible", model.isAccessible())
                .add("employeePostId", model.getEmployeePost().getId());
    }

    // Getters

    public EmployeePostViewLegacyModel getModel()
    {
        return model;
    }

    public EmployeeHoliday getLongTimeEmployeeHoliday()
    {
        return _longTimeEmployeeHoliday;
    }

    public String getPeriod()
    {
        return _period;
    }

    // Listeners

    public void onClickPrintPersonCard()
    {
        getActivationBuilder().asRegion(EmpEmployeePostPrintPersonCard.class, EmployeePostViewUI.EMPLOYEE_POST_TAB_PANEL_REGION_NAME)
                .parameter(EmpEmployeePostPrintPersonCardUI.EMPLOYEE_POST_ID, getModel().getEmployeePost().getId())
                .activate();
    }

    public void onClickAddEmployeeActingItem()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_ACTING_ITEM_ADD, EmployeePostViewUI.EMPLOYEE_POST_TAB_PANEL_REGION_NAME)
                .parameter("id", null)
                .parameter("employeePostId", model.getEmployeePost().getId())
                .activate();
    }

    public void onClickAddCertificationItem()
    {
        getActivationBuilder().asRegion(EmpEmployeeCertificationItemAddEdit.class, EmployeePostViewUI.EMPLOYEE_POST_TAB_PANEL_REGION_NAME)
                .parameter(EmpEmployeeCertificationItemAddEditUI.ITEM_ID, null)
                .parameter(EmpEmployeeCertificationItemAddEditUI.EMPLOYEE_POST_ID, model.getEmployeePost().getId())
                .activate();
    }

    public void onClickAddCombinationPost()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_COMBINATION_POST_ADD_EDIT, EmployeePostViewUI.EMPLOYEE_POST_TAB_PANEL_REGION_NAME)
                .parameter("id", null)
                .parameter("employeePostId", model.getEmployeePost().getId())
                .activate();
    }

    public void onClickAddEmployeeVacationPlaned()
    {
        getActivationBuilder().asRegionDialog(IUniempComponents.VACATION_SCHEDULE_ITEM_ADD_EDIT)
                .parameter("vacationScheduleItemId", null)
                .parameter("editForm", false) // ???
                .parameter("employeePostId", model.getEmployeePost().getId())
                .activate();
    }

    public void onClickAddEmployeeHoliday()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_ADD_EMPLOYEE_HOLIDAY, EmployeePostViewUI.EMPLOYEE_POST_TAB_PANEL_REGION_NAME)
                .parameter("employeePostId", model.getEmployeePost().getId())
                .parameter("rowId", null)
                .activate();
    }

    public void onClickAddEmployeePayment()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_ADD_EMPLOYEE_PAYMENT, EmployeePostViewUI.EMPLOYEE_POST_TAB_PANEL_REGION_NAME)
                .parameter("employeePostId", model.getEmployeePost().getId())
                .parameter("rowId", null)
                .activate();
    }

    public void onClickAddContract()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_CONTRACT_ADDEDIT, EmployeePostViewUI.EMPLOYEE_POST_TAB_PANEL_REGION_NAME)
                .parameter("labourContractId", null)
                .parameter("employeePostId", model.getEmployeePost().getId())
                .activate();
    }

    public void onClickEditContract()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_CONTRACT_ADDEDIT, EmployeePostViewUI.EMPLOYEE_POST_TAB_PANEL_REGION_NAME)
                .parameter("labourContractId", model.getLabourContract().getId())
                .parameter("employeePostId", null)
                .activate();
    }

    public void onClickAddContractFile()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_CONTRACT_FILE_ADD, EmployeePostViewUI.EMPLOYEE_POST_TAB_PANEL_REGION_NAME)
                .parameter("labourContractId", model.getLabourContract() == null ? null : model.getLabourContract().getId())
                .activate();
    }
    
    public void onPrintContractFile()
    {
        EmployeeLabourContract contract = model.getLabourContract();

        byte[] content = contract.getContractFile().getContent();
        if (content == null)
            throw new ApplicationException("Файл договора пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType(contract.getContractFileType()).fileName(contract.getContractFileName()).document(content), false);
    }

    public void onClickDeleteContractFile()
    {
        final EmployeeLabourContract item = model.getLabourContract();

        if (item == null) return;

        DatabaseFile file = item.getContractFile();
        if (file != null)
        {
            item.setContractFile(null);
            item.setContractFileName(null);
            item.setContractFileType(null);
            DataAccessServices.dao().update(item);
            DataAccessServices.dao().delete(file);
        }
    }

    public void onClickAddContractAgreement()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_AGREEMENT_TO_CONTRACT_ADDEDIT, EmployeePostViewUI.EMPLOYEE_POST_TAB_PANEL_REGION_NAME)
                .parameter("rowId", null)
                .parameter("labourContractId", model.getLabourContract() == null ? null : model.getLabourContract().getId())
                .activate();
    }

    public void onClickAddMedicalSpec()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_ADD_MEDICAL_SPEC, EmployeePostViewUI.EMPLOYEE_POST_TAB_PANEL_REGION_NAME)
                .parameter("employeeMedicalSpecialityId", null)
                .parameter("employeeId", model.getEmployee().getId())
                .activate();
    }

    public void onClickAddEmployeeDegree()
    {
        getActivationBuilder().asRegion(PersonAcademicDegreeAddEdit.class, EmployeePostViewUI.EMPLOYEE_POST_TAB_PANEL_REGION_NAME)
                .parameter("personAcademicDegreeId", null)
                .parameter("personId", model.getEmployee().getPerson().getId())
                .activate();
    }

    public void onClickAddEmployeeStatus()
    {
        getActivationBuilder().asRegion(PersonAcademicStatusAddEdit.class, EmployeePostViewUI.EMPLOYEE_POST_TAB_PANEL_REGION_NAME)
                .parameter("personAcademicStatusId", null)
                .parameter("personId", model.getEmployee().getPerson().getId())
                .activate();
    }

    public void onClickEditAdditionalData()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_ADDITIONAL_DATA_EDIT, EmployeePostViewUI.EMPLOYEE_POST_TAB_PANEL_REGION_NAME)
                .parameter("employeeCardId", model.getEmployeeCard().getId())
                .parameter("employeeId", model.getEmployee().getId())
                .activate();
    }

    public void onClickAddSickList()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_ADD_SICK_LIST, EmployeePostViewUI.EMPLOYEE_POST_TAB_PANEL_REGION_NAME)
                .parameter("employeeId", model.getEmployee().getId())
                .parameter("rowId", null)
                .activate();
    }

    public void onClickAddTrainingItem()
    {
        getActivationBuilder().asRegion(EmpEmployeeTrainingItemAddEdit.class, EmployeePostViewUI.EMPLOYEE_POST_TAB_PANEL_REGION_NAME)
                .parameter(EmpEmployeeRetrainingItemAddEditUI.EMPLOYEE_ID, model.getEmployee().getId())
                .parameter(EmpEmployeeRetrainingItemAddEditUI.ITEM_ID, null)
                .activate();
    }

    public void onClickAddRetrainingItem()
    {
        getActivationBuilder().asRegion(EmpEmployeeRetrainingItemAddEdit.class, EmployeePostViewUI.EMPLOYEE_POST_TAB_PANEL_REGION_NAME)
                .parameter(EmpEmployeeRetrainingItemAddEditUI.EMPLOYEE_ID, model.getEmployee().getId())
                .parameter(EmpEmployeeRetrainingItemAddEditUI.ITEM_ID, null)
                .activate();
    }

    public void onClickClearTrainingSettings()
    {
        final IUISettings settings = getPresenter().getSettings();

        settings.remove("eduDocumentTrainingDateTo");
        settings.remove("eduDocumentTrainingDateFrom");
        settings.remove("finishTrainingDateTo");
        settings.remove("finishTrainingDateFrom");
        settings.remove("startTrainingDateTo");
        settings.remove("startTrainingDateFrom");
        settings.save();
    }

    public void onClickClearRetrainingSettings()
    {
        final IUISettings settings = getPresenter().getSettings();

        settings.remove("eduDocumentRetrainingDateTo");
        settings.remove("eduDocumentRetrainingDateFrom");
        settings.remove("finishRetrainingDateTo");
        settings.remove("finishRetrainingDateFrom");
        settings.remove("startRetrainingDateTo");
        settings.remove("startRetrainingDateFrom");
        settings.save();
    }

    // DataSource Listeners

    public void onDeleteCombinationPost()
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StaffListAllocationItem.class, "b").column("b");
        builder.where(DQLExpressions.eqValue(DQLExpressions.property(StaffListAllocationItem.combinationPost().id().fromAlias("b")), getListenerParameterAsLong()));
        builder.where(DQLExpressions.eqValue(DQLExpressions.property(StaffListAllocationItem.staffListItem().staffList().staffListState().code().fromAlias("b")), UniempDefines.STAFF_LIST_STATUS_ACTIVE));

        List<StaffListAllocationItem> itemList = builder.createStatement(getSession()).list();

        //нельзя удалять Должности по совмещению, если на них ссылается Должность штатной расстановки активного ШР
        if (itemList.isEmpty())
            DataAccessServices.dao().delete(getListenerParameterAsLong());
        else
        {
            OrgUnit orgUnit = itemList.get(0).getStaffListItem().getStaffList().getOrgUnit();
            throw new ApplicationException("Невозможно удалить должность, поскольку на нее ссылается должность штатной расстановки подразделения «" + orgUnit.getTitle() + "».");
        }
    }

    public void onDeleteRowFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
        getPresenterConfig().getBusinessComponent().refresh();
    }

    public void onClickEditMedicalSpec()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_ADD_MEDICAL_SPEC, EmployeePostViewUI.EMPLOYEE_POST_TAB_PANEL_REGION_NAME)
                .parameter("employeeMedicalSpecialityId", getListenerParameterAsLong())
                .parameter("employeeId", model.getEmployee().getId())
                .activate();
    }

    public void onClickAddContractAgreementFile()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_AGREEMENT_TO_CONTRACT_FILE_ADD, EmployeePostViewUI.EMPLOYEE_POST_TAB_PANEL_REGION_NAME)
                .parameter("rowId", getListenerParameterAsLong())
                .activate();
    }

    public void onDelContractAgreementFile()
    {
        getSession().doWork(connection -> {
            ContractCollateralAgreement item = DataAccessServices.dao().getNotNull(ContractCollateralAgreement.class, getListenerParameterAsLong());
            DatabaseFile file = item.getAgreementFile();

            if (file != null)
            {
                item.setAgreementFile(null);
                item.setAgreementFileName(null);
                item.setAgreementFileType(null);
                getSession().update(item);
                getSession().flush();
                getSession().delete(file);
            }
        });
    }

    public void onEditContractAgreement()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_AGREEMENT_TO_CONTRACT_ADDEDIT, EmployeePostViewUI.EMPLOYEE_POST_TAB_PANEL_REGION_NAME)
                .parameter("rowId", getListenerParameterAsLong())
                .parameter("labourContractId", model.getLabourContract() == null ? null : model.getLabourContract().getId())
                .activate();
    }

    public void onClickEditScDegree()
    {
        getActivationBuilder().asRegion(PersonAcademicDegreeAddEdit.class, EmployeePostViewUI.EMPLOYEE_POST_TAB_PANEL_REGION_NAME)
                .parameter("personAcademicDegreeId", getListenerParameterAsLong())
                .parameter("personId", null)
                .activate();
    }

    public void onClickEditScStatus()
    {
        getActivationBuilder().asRegion(PersonAcademicStatusAddEdit.class, EmployeePostViewUI.EMPLOYEE_POST_TAB_PANEL_REGION_NAME)
                .parameter("personAcademicStatusId", getListenerParameterAsLong())
                .parameter("personId", null)
                .activate();
    }

    public void onClickEditSickList()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_ADD_SICK_LIST, EmployeePostViewUI.EMPLOYEE_POST_TAB_PANEL_REGION_NAME)
                .parameter("employeeId", model.getEmployee().getId())
                .parameter("rowId", getListenerParameterAsLong())
                .activate();
    }

    public void onClickEditEmployeeVacationPlanned()
    {
        getActivationBuilder().asRegionDialog(IUniempComponents.VACATION_SCHEDULE_ITEM_EDIT)
                .parameter("vacationScheduleItemId", getListenerParameterAsLong())
                .parameter("employeePostId", model.getEmployeePost().getId())
                .activate();
    }

    public void onClickPrintVacationCertificate()
    {
        getActivationBuilder().asRegion(IUniempComponents.VACATION_CERTIFICATE_PRINT_FORM, EmployeePostViewUI.EMPLOYEE_POST_TAB_PANEL_REGION_NAME)
                .parameter("employeeHolidayId", getListenerParameterAsLong())
                .activate();
    }

    public void onClickEditEmployeeHoliday()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_ADD_EMPLOYEE_HOLIDAY, EmployeePostViewUI.EMPLOYEE_POST_TAB_PANEL_REGION_NAME)
                .parameter("employeePostId", model.getEmployeePost().getId())
                .parameter("rowId", getListenerParameterAsLong())
                .activate();
    }

    public void onClickEditEmployeePayment()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_ADD_EMPLOYEE_PAYMENT, EmployeePostViewUI.EMPLOYEE_POST_TAB_PANEL_REGION_NAME)
                .parameter("employeePostId", model.getEmployeePost().getId())
                .parameter("rowId", getListenerParameterAsLong())
                .activate();
    }

    public void onClickEditActingItem()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_ACTING_ITEM_ADD, EmployeePostViewUI.EMPLOYEE_POST_TAB_PANEL_REGION_NAME)
                .parameter("id", getListenerParameterAsLong())
                .parameter("employeePostId", model.getEmployeePost().getId())
                .activate();
    }

    public void onClickEditCombinationPost()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_COMBINATION_POST_ADD_EDIT, EmployeePostViewUI.EMPLOYEE_POST_TAB_PANEL_REGION_NAME)
                .parameter("id", getListenerParameterAsLong())
                .parameter("employeePostId", model.getEmployeePost().getId())
                .activate();
    }

    public void onClickEditCertificationItem()
    {
        getActivationBuilder().asRegion(EmpEmployeeCertificationItemAddEdit.class, EmployeePostViewUI.EMPLOYEE_POST_TAB_PANEL_REGION_NAME)
                .parameter(EmpEmployeeCertificationItemAddEditUI.EMPLOYEE_POST_ID, model.getEmployeePost().getId())
                .parameter(EmpEmployeeCertificationItemAddEditUI.ITEM_ID, getListenerParameterAsLong())
                .activate();
    }

    public void onClickEditTrainingItem()
    {
        getActivationBuilder().asRegion(EmpEmployeeTrainingItemAddEdit.class, EmployeePostViewUI.EMPLOYEE_POST_TAB_PANEL_REGION_NAME)
                .parameter(EmpEmployeeTrainingItemAddEditUI.EMPLOYEE_ID, model.getEmployee().getId())
                .parameter(EmpEmployeeTrainingItemAddEditUI.ITEM_ID, getListenerParameterAsLong())
                .activate();
    }

    public void onClickPrintEduDocumentTraining()
    {
        EmployeeTrainingItem item = DataAccessServices.dao().getNotNull(EmployeeTrainingItem.class, getListenerParameterAsLong());

        byte[] content = item.getEduDocumentFile().getContent();
        if (content == null)
            throw new ApplicationException("Файл документа пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().fileName(item.getEduDocumentFileName()).contentType(item.getEduDocumentFileType()).document(content), false);
    }

    public void onClickEditRetrainingItem()
    {
        getActivationBuilder().asRegion(EmpEmployeeRetrainingItemAddEdit.class, EmployeePostViewUI.EMPLOYEE_POST_TAB_PANEL_REGION_NAME)
                .parameter(EmpEmployeeRetrainingItemAddEditUI.EMPLOYEE_ID, model.getEmployee().getId())
                .parameter(EmpEmployeeRetrainingItemAddEditUI.ITEM_ID, getListenerParameterAsLong())
                .activate();
    }

    public void onClickPrintEduDocumentRetraining()
    {
        EmployeeRetrainingItem item = DataAccessServices.dao().getNotNull(EmployeeRetrainingItem.class, getListenerParameterAsLong());

        byte[] content = item.getEduDocumentFile().getContent();
        if (content == null)
            throw new ApplicationException("Файл документа пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().fileName(item.getEduDocumentFileName()).contentType(item.getEduDocumentFileType()).document(content), false);
    }

    // Private

    private void prepareLegacyModel()
    {
        // controller
        model = new EmployeePostViewLegacyModel();
        model.setBaseModel((EmployeePostViewUI) getPresenter());

        // dao prepare
        if (getPresenter().getSettings().get("combinationShowNotActiveFiltr") == null)
            getPresenter().getSettings().set("combinationShowNotActiveFiltr", true);
        EmployeeCard employeeCard = UniempDAO.getEmployeeCard(model.getEmployee(), getSession());
        if (employeeCard != null)
            model.setEmployeeCard(employeeCard);
        model.setEmployeeCardTypePPS(true);

        Criteria criteria = getSession().createCriteria(EmployeeLabourContract.class);
        criteria.add(Restrictions.eq(EmployeeLabourContract.L_EMPLOYEE_POST, model.getEmployeePost()));
        EmployeeLabourContract contract = (EmployeeLabourContract) criteria.uniqueResult();
        if (contract != null)
            model.setLabourContract(contract);

        model.setOrgUnitModelFilter(new OrgUnitAutocompleteModel(OrgUnit.P_TITLE_WITH_TYPE));
        model.setPostModelFilter(new CommonSingleSelectModel(PostBoundedWithQGandQL.fullTitle().s())
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                MQBuilder builder = new MQBuilder(PostBoundedWithQGandQL.ENTITY_CLASS, "b");
                builder.add(MQExpression.like("b", PostBoundedWithQGandQL.title().s(), CoreStringUtils.escapeLike(filter)));
                if (o != null)
                    builder.add(MQExpression.eq("b", PostBoundedWithQGandQL.id().s(), o));
                builder.addOrder("b", PostBoundedWithQGandQL.title().s());

                return new MQListResultBuilder(builder);
            }
        });
        model.setPostTypeModelFilter(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(CombinationPostType.class, "t").column("t");
                builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property("t", CombinationPostType.title())), DQLExpressions.value(CoreStringUtils.escapeLike(filter))));
                if (o != null)
                    builder.where(DQLExpressions.eq(DQLExpressions.property("t", CombinationPostType.id()), DQLExpressions.commonValue(o)));
                builder.order(DQLExpressions.property("t", CombinationPostType.title()));
                return new DQLListResultBuilder(builder);
            }
        });

        // controller
        IBusinessComponent component = getPresenterConfig().getBusinessComponent();
        prepareStaffRateDataSource(component);
        prepareMedicalSpecDataSource(component);
        prepareScienseDegreeDataSource(component);
        prepareScienseStatusDataSource(component);
        prepareSickListDataSource(component);
        prepareVacationPlannedDataSource(component);
        prepareHolidaysDataSource(component);
        preparePaymentsDataSource(component);
        prepareActingDataSource(component);
        prepareСombinationPostDataSource(component);
        prepareCertificationDataSource(component);
        prepareTrainingDataSource(component);
        prepareRetrainingDataSource(component);
    }

    private void prepareStaffRateDataSource(IBusinessComponent component)
    {
        DynamicListDataSource<EmployeePostStaffRateItem> dataSource = new DynamicListDataSource<>(component, context -> {
            List<EmployeePostStaffRateItem> staffRateItems = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(model.getEmployeePost());
            model.getStaffRateDataSource().setCountRow(staffRateItems.size());
            UniBaseUtils.createPage(model.getStaffRateDataSource(), staffRateItems);
        });

        dataSource.addColumn(new SimpleColumn("Ставка", EmployeePostStaffRateItem.P_STAFF_RATE));
        dataSource.addColumn(new SimpleColumn("Источник финансирования", EmployeePostStaffRateItem.financingSource().title().s()));
        dataSource.addColumn(new SimpleColumn("Источник финансирования (детально)", EmployeePostStaffRateItem.financingSourceItem().title().s()));

        model.setStaffRateDataSource(dataSource);
    }

    private void prepareMedicalSpecDataSource(IBusinessComponent component)
    {
        DynamicListDataSource<EmployeeMedicalSpeciality> dataSource = new DynamicListDataSource<>(component, context -> {
            MQBuilder builder = new MQBuilder(EmployeeMedicalSpeciality.ENTITY_CLASS, "e");
            builder.add(MQExpression.eq("e", EmployeeMedicalSpeciality.L_EMPLOYEE, model.getEmployee()));
            new OrderDescriptionRegistry("e").applyOrder(builder, model.getMedicalSpecDataSource().getEntityOrder());
            UniBaseUtils.createPage(model.getMedicalSpecDataSource(), builder, getSession());
        }, 5);

        dataSource.addColumn(new SimpleColumn("Врачебная специальность", EmployeeMedicalSpeciality.medicalEducationLevel().title().s()));
        dataSource.addColumn(new SimpleColumn("Дата присуждения", EmployeeMedicalSpeciality.assignDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата подтверждения", EmployeeMedicalSpeciality.acceptionDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Квалификационная категория", EmployeeMedicalSpeciality.medicalQualificationCategory().shortTitle().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата присуждения категории", EmployeeMedicalSpeciality.categoryAssignDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата подтверждения категории", EmployeeMedicalSpeciality.categoryAcceptionDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new BooleanColumn("Имеется сертификат специалиста", EmployeeMedicalSpeciality.P_CERTIFICATE_AVAILABLE));
        dataSource.addColumn(CommonBaseUtil.getPrintColumn("uniempEmployeePostViewExtUI:onClickPrintMedicalSpec", "Печатать файл сертификата").setDisabledProperty(EmployeeMedicalSpeciality.P_PRINTING_DISABLED).setPermissionKey("printMedicalSpecialityCertificate__employeePost"));

        if (model.isAccessible())
        {
            dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "uniempEmployeePostViewExtUI:onClickEditMedicalSpec").setPermissionKey("editMedicalSpec_employeePost"));
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "uniempEmployeePostViewExtUI:onDeleteRowFromList", "Удалить врачебную специальность?").setPermissionKey("deleteMedicalSpec_employeePost"));
        }

        model.setMedicalSpecDataSource(dataSource);
    }

    public void onClickPrintMedicalSpec() {
        EmployeeMedicalSpeciality speciality = IUniBaseDao.instance.get().get(EmployeeMedicalSpeciality.class, getListenerParameterAsLong());
        byte[] content = speciality.getExpertCertificate().getContent();
        if (content == null)
            throw new ApplicationException("Файл сертификата пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer()
            .contentType(speciality.getCertificateFileType())
            .fileName(speciality.getCertificateFileName())
            .document(content), true);
    }

    private void prepareScienseDegreeDataSource(IBusinessComponent component)
    {
        DynamicListDataSource<PersonAcademicDegree> dataSource = new DynamicListDataSource<>(component, component1 -> {
            OrderDirection orderDirection = model.getScienceDegreeDataSource().getEntityOrder() != null ? model.getScienceDegreeDataSource().getEntityOrder().getDirection() : OrderDirection.desc;
            MQBuilder builder = new MQBuilder(PersonAcademicDegree.ENTITY_CLASS, "sd");
            builder.add(MQExpression.eq("sd", PersonAcademicDegree.person(), model.getEmployee().getPerson()));
            builder.addOrder("sd", PersonAcademicDegree.P_DATE, orderDirection);
            UniBaseUtils.createPage(model.getScienceDegreeDataSource(), builder.<PersonAcademicDegree>getResultList(getSession()));
        }, 5);

//        dataSource.addColumn(new SimpleColumn("Ученая степень", PersonAcademicDegree.academicDegree().title().s()).setOrderable(false));
        PublisherLinkColumn linkColumn = new PublisherLinkColumn("Ученая степень", PersonAcademicDegree.academicDegree().title().s());
        linkColumn.setResolver(new DefaultPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                ParametersMap map = new ParametersMap();
                map.add(PublisherActivator.PUBLISHER_ID_KEY, entity.getId());
                map.add(ISecureRoleContext.SECURE_ROLE_CONTEXT, ((EmployeePostViewUI) getPresenter()).getSecureRoleContext());
                map.add("accessible", model.isAccessible());
                return map;
            }
        });

        dataSource.addColumn(linkColumn);
        dataSource.addColumn(new SimpleColumn("Отрасль науки", PersonAcademicDegree.scienceBranch().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дата присуждения", PersonAcademicDegree.date().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(true));

        dataSource.addColumn(CommonBaseUtil.getPrintColumn("uniempEmployeePostViewExtUI:onClickPrintPersonAcademicDegreeDocument", "Печатать файл диплома").setDisabledProperty(PersonAcademicStatus.P_PRINTING_DISABLED).setPermissionKey("printDegree_employeePost"));

        if (model.isAccessible())
        {
            dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "uniempEmployeePostViewExtUI:onClickEditScDegree").setPermissionKey("editDegree_employeePost"));
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "uniempEmployeePostViewExtUI:onDeleteRowFromList", "Удалить научную степень?").setPermissionKey("deleteDegree_employeePost"));
        }

        model.setScienceDegreeDataSource(dataSource);
    }

    public void onClickPrintPersonAcademicDegreeDocument() {
        PersonAcademicDegree academicStatus = IUniBaseDao.instance.get().get(PersonAcademicDegree.class, getListenerParameterAsLong());
        byte[] content = academicStatus.getDiplomFile().getContent();
        if (content == null)
            throw new ApplicationException("Файл диплома пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer()
            .contentType(academicStatus.getDiplomFileType())
            .fileName(academicStatus.getDiplomFileName())
            .document(content), true);
    }

    private void prepareScienseStatusDataSource(IBusinessComponent component)
    {
        DynamicListDataSource<PersonAcademicStatus> dataSource = new DynamicListDataSource<>(component, component1 -> {
            OrderDirection orderDirection = model.getScienceStatusDataSource().getEntityOrder() != null ? model.getScienceStatusDataSource().getEntityOrder().getDirection() : OrderDirection.desc;
            MQBuilder builder = new MQBuilder(PersonAcademicStatus.ENTITY_CLASS, "ad");
            builder.add(MQExpression.eq("ad", PersonAcademicStatus.person(), model.getEmployee().getPerson()));
            builder.addOrder("ad", PersonAcademicStatus.P_DATE, orderDirection);
            UniBaseUtils.createPage(model.getScienceStatusDataSource(), builder.<PersonAcademicStatus>getResultList(getSession()));
        }, 5);

//        dataSource.addColumn(new SimpleColumn("Ученое звание", PersonAcademicStatus.academicStatus().title().s()).setOrderable(false));
        PublisherLinkColumn linkColumn = new PublisherLinkColumn("Ученое звание", PersonAcademicStatus.academicStatus().title().s());
        linkColumn.setResolver(new DefaultPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                ParametersMap map = new ParametersMap();
                map.add(PublisherActivator.PUBLISHER_ID_KEY, entity.getId());
                map.add(ISecureRoleContext.SECURE_ROLE_CONTEXT, ((EmployeePostViewUI) getPresenter()).getSecureRoleContext());
                map.add("accessible", model.isAccessible());
                return map;
            }
        });

        dataSource.addColumn(linkColumn);
        dataSource.addColumn(new SimpleColumn("Дата присуждения", PersonAcademicStatus.date().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(true));
        dataSource.addColumn(CommonBaseUtil.getPrintColumn("uniempEmployeePostViewExtUI:onClickPrintPersonAcademicStatusDocument", "Печатать файл аттестата").setDisabledProperty(PersonAcademicStatus.P_PRINTING_DISABLED).setPermissionKey("printStatus_employeePost"));
        if (model.isAccessible())
        {
            dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "uniempEmployeePostViewExtUI:onClickEditScStatus").setPermissionKey("editStatus_employeePost"));
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "uniempEmployeePostViewExtUI:onDeleteRowFromList", "Удалить ученое звание?").setPermissionKey("deleteStatus_employeePost"));
        }

        model.setScienceStatusDataSource(dataSource);
    }

    public void onClickPrintPersonAcademicStatusDocument() {
        PersonAcademicStatus academicStatus = IUniBaseDao.instance.get().get(PersonAcademicStatus.class, getListenerParameterAsLong());
        byte[] content = academicStatus.getCertificateFile().getContent();
        if (content == null)
            throw new ApplicationException("Файл аттестата пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer()
            .contentType(academicStatus.getCertificateFileType())
            .fileName(academicStatus.getCertificateFileName())
            .document(content), true);
    }

    private void prepareSickListDataSource(IBusinessComponent component)
    {
        DynamicListDataSource<EmployeeSickList> dataSource = new DynamicListDataSource<>(component, component1 -> {
            OrderDirection orderDirection = model.getEmployeeSickListDataSource().getEntityOrder() != null ? model.getEmployeeSickListDataSource().getEntityOrder().getDirection() : OrderDirection.desc;
            MQBuilder builder = new MQBuilder(EmployeeSickList.ENTITY_CLASS, "esl");
            builder.add(MQExpression.eq("esl", EmployeeSickList.L_EMPLOYEE, model.getEmployee()));
            builder.addOrder("esl", EmployeeSickList.P_CLOSE_DATE, orderDirection);
            UniBaseUtils.createPage(model.getEmployeeSickListDataSource(), builder.<EmployeeSickList>getResultList(getSession()));
        }, 10);

        dataSource.addColumn(new SimpleColumn("Номер", EmployeeSickList.number().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дата открытия", EmployeeSickList.openDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дата закрытия", EmployeeSickList.closeDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Мед. учреждение", EmployeeSickList.medicalFoundation().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Основание", EmployeeSickList.basic().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Номер листа продления", EmployeeSickList.prolongNumber().s()).setClickable(false).setOrderable(false));

        if (model.isAccessible())
        {
            dataSource.addColumn(new ActionColumn("", ActionColumn.EDIT, "uniempEmployeePostViewExtUI:onClickEditSickList").setPermissionKey("editSickList_employeePost"));
            dataSource.addColumn(new ActionColumn("", ActionColumn.DELETE, "uniempEmployeePostViewExtUI:onDeleteRowFromList", "Удалить лист нетрудоспособности?").setPermissionKey("deleteSickList_employeePost"));
        }

        dataSource.changeOrder(1);//TODO: hack
        model.setEmployeeSickListDataSource(dataSource);
    }

    private void prepareVacationPlannedDataSource(IBusinessComponent component)
    {
        {
            MQBuilder ssBuilder = new MQBuilder(StaffListState.ENTITY_CLASS, "ss");
            ssBuilder.add(MQExpression.eq("ss", StaffListState.code().s(), UniempDefines.STAFF_LIST_STATUS_FORMING));
            StaffListState formingStatus = (StaffListState) ssBuilder.uniqueResult(getSession());
            MQBuilder vsBuilder = new MQBuilder(VacationSchedule.ENTITY_CLASS, "vs");
            vsBuilder.add(MQExpression.eq("vs", VacationSchedule.orgUnit().s(), model.getEmployeePost().getOrgUnit()));
            vsBuilder.add(MQExpression.eq("vs", VacationSchedule.state().s(), formingStatus));
            model.setAddingVacationPlannedVisible(vsBuilder.getResultCount(getSession()) == 0);
        }

        DynamicListDataSource<VacationScheduleItem> dataSource = new DynamicListDataSource<>(component, component1 -> {
            MQBuilder builder = new MQBuilder(VacationScheduleItem.ENTITY_CLASS, "vsi");
            builder.add(MQExpression.eq("vsi", VacationScheduleItem.L_EMPLOYEE_POST, model.getEmployeePost()));
            builder.addDescOrder("vsi", VacationScheduleItem.planDate().s());
            UniBaseUtils.createPage(model.getEmployeeVacationPlannedListDataSource(), builder, getSession());
        }, 5);

        IStyleResolver styleResolver = rowEntity -> {
            VacationScheduleItem item = (VacationScheduleItem) rowEntity;

            if ((null != item.getFactDate() && item.getFactDate().getTime() > System.currentTimeMillis()) || (null == item.getFactDate() && null != item.getPlanDate() && item.getPlanDate().getTime() > System.currentTimeMillis()))
                return "font-weight:bold;";
            else
                return null;
        };

        AbstractColumn dateColumn = new SimpleColumn("Планируемая дата начала", VacationScheduleItem.P_PLAN_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false);
        dateColumn.setStyleResolver(styleResolver);
        dataSource.addColumn(dateColumn);

        dataSource.addColumn(new SimpleColumn("Количествово дней", VacationScheduleItem.P_DAYS_AMOUNT).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Фактическая дата начала", VacationScheduleItem.P_FACT_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Основание переноса", VacationScheduleItem.P_POSTPONE_BASIC).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дата переноса", VacationScheduleItem.P_POSTPONE_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Комментарий", VacationScheduleItem.comment()).setClickable(false).setOrderable(false));
        if (model.isAccessible())
        {
            dataSource.addColumn(new ActionColumn("Редактировать запись об отпуске", ActionColumn.EDIT, "uniempEmployeePostViewExtUI:onClickEditEmployeeVacationPlanned").setPermissionKey("editVacationScheduleItem_employeePost").setDisableHandler(model.getEditDisabledEntityHandler()));
            dataSource.addColumn(new ActionColumn("Удалить запись об отпуске", ActionColumn.DELETE, "uniempEmployeePostViewExtUI:onDeleteRowFromList", "Удалить запись о планируемом отпуске?").setPermissionKey("deleteVacationScheduleItem_employeePost").setDisableHandler(model.getDeleteDisabledEntityHandler()));
        }
        model.setEmployeeVacationPlannedListDataSource(dataSource);
    }

    private void prepareHolidaysDataSource(IBusinessComponent component)
    {
        DynamicListDataSource<EmployeeHoliday> dataSource = new DynamicListDataSource<>(component, component1 -> {
            MQBuilder builder = new MQBuilder(EmployeeHoliday.ENTITY_CLASS, "e");
            builder.add(MQExpression.eq("e", EmployeeHoliday.L_EMPLOYEE_POST, model.getEmployeePost()));
            new OrderDescriptionRegistry("e").applyOrder(builder, model.getEmployeeHolidaysListDataSource().getEntityOrder());
            UniBaseUtils.createPage(model.getEmployeeHolidaysListDataSource(), builder, getSession());
        }, 15);

        dataSource.addColumn(new SimpleColumn("Название", EmployeeHoliday.P_TITLE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата начала периода", EmployeeHoliday.P_BEGIN_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата окончания периода", EmployeeHoliday.P_END_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Вид отпуска", EmployeeHoliday.KEY_HOLIDAY_TYPE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Длительность отпуска", EmployeeHoliday.P_DURATION).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата начала отпуска", EmployeeHoliday.P_START_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата окончания отпуска", EmployeeHoliday.P_FINISH_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        PublisherLinkColumn linkColumn = new PublisherLinkColumn("Основание", EmployeeHoliday.P_ORDER_NUMBER_DATE);
        linkColumn.setOrderable(false);
        linkColumn.setDisableHandler(entity -> {
            EmployeeHoliday holiday = (EmployeeHoliday) entity;
            if (holiday.getHolidayExtract() == null || holiday.getHolidayExtract().getProperty("paragraph") == null || holiday.getHolidayExtract().getProperty("paragraph.order") == null)
                return true;

            String orderNumber = (String) holiday.getHolidayExtract().getProperty("paragraph.order.number");
            Date orderDate = (Date) holiday.getHolidayExtract().getProperty("paragraph.order.commitDate");

            return !orderNumber.equals(holiday.getHolidayExtractNumber()) || !(orderDate.getTime() == holiday.getHolidayExtractDate().getTime());
        });
        linkColumn.setResolver(new DefaultPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                if (((EmployeeHoliday) entity).getHolidayExtract().getProperty("type.parent.code").equals("3"))
                    return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, ((EmployeeHoliday) entity).getHolidayExtract().getId());
                else
                    return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, ((EmployeeHoliday) entity).getHolidayExtract().getProperty("paragraph.order.id"));
            }

            @Override
            public String getComponentName(IEntity entity)
            {
                if (((EmployeeHoliday) entity).getHolidayExtract().getProperty("type.parent.code").equals("3"))
                    return "ru.tandemservice.moveemployee.component.singleemplextract.e" +
                            ((EmployeeHoliday) entity).getHolidayExtract().getProperty("type.index") +
                            ".Pub";
                else
                    return null;
            }
        });
        dataSource.addColumn(linkColumn);
//        dataSource.addColumn(new BlockColumn("numberDate", "Основание"));
        IndicatorColumn column = new IndicatorColumn("print", "Печатать отпускное удостоверение", null, "uniempEmployeePostViewExtUI:onClickPrintVacationCertificate", (String) null);
        column.setOrderable(false);
        column.setImageHeader(false);
        column.defaultIndicator(new IndicatorColumn.Item("printer", "Печать"));
        column.setPermissionKey("printVacationCertificate_employeePost");
        dataSource.addColumn(column);

        if (model.isAccessible())
        {
            dataSource.addColumn(new ActionColumn("Редактировать запись об отпуске", ActionColumn.EDIT, "uniempEmployeePostViewExtUI:onClickEditEmployeeHoliday").setPermissionKey("editEmployeeHoliday_employeePost"));
            dataSource.addColumn(new ActionColumn("Удалить запись об отпуске", ActionColumn.DELETE, "uniempEmployeePostViewExtUI:onDeleteRowFromList", "Удалить запись об отпуске «{0}» от {1}?", new Object[]{EmployeeHoliday.P_TITLE, EmployeeHoliday.P_START_DATE_STR}).setPermissionKey("deleteEmployeeHoliday_employeePost"));
        }

        model.setEmployeeHolidaysListDataSource(dataSource);
    }

    private void preparePaymentsDataSource(IBusinessComponent component)
    {
        DynamicListDataSource<EmployeePayment> dataSource = new DynamicListDataSource<>(component, component1 -> {
            MQBuilder builder = new MQBuilder(EmployeePayment.ENTITY_CLASS, "ep");
            builder.add(MQExpression.eq("ep", EmployeePayment.L_EMPLOYEE_POST, model.getEmployeePost()));
            new OrderDescriptionRegistry("ep").applyOrder(builder, model.getEmployeePaymentsListDataSource().getEntityOrder());
            UniBaseUtils.createPage(model.getEmployeePaymentsListDataSource(), builder, getSession());
        }, 15);

        dataSource.addColumn(new SimpleColumn("Название", EmployeePayment.KEY_PAYMENT_TITLE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Тип выплаты", EmployeePayment.KEY_PAYMENT_TYPE_TITLE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата назначения", EmployeePayment.P_ASSIGN_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Назначена с даты", EmployeePayment.P_BEGIN_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Назначена по дату", EmployeePayment.P_END_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Размер выплаты", EmployeePayment.P_PAYMENT_VALUE, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Формат ввода выплаты", new String[]{EmployeePayment.L_PAYMENT, Payment.L_PAYMENT_UNIT, PaymentUnit.P_TITLE}).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Сумма, руб.", EmployeePayment.P_AMOUNT, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Источник финансирования", EmployeePayment.KEY_FINANCING_SOURCE_TITLE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Источник финансирования (детально)", EmployeePayment.financingSourceItem().title().s()).setClickable(false));

        if (model.isAccessible())
        {
            dataSource.addColumn(new ActionColumn("Редактировать выплату", ActionColumn.EDIT, "uniempEmployeePostViewExtUI:onClickEditEmployeePayment").setPermissionKey("editEmployeePayment_employeePost"));
            dataSource.addColumn(new ActionColumn("Удалить выплату", ActionColumn.DELETE, "uniempEmployeePostViewExtUI:onDeleteRowFromList", "Удалить выплату «{0}» от {1}?", new Object[]{EmployeePayment.payment().title().s(), EmployeePayment.P_START_DATE_STR}).setPermissionKey("deleteEmployeePayment_employeePost"));
        }

        dataSource.changeOrder(2);
        dataSource.changeOrder(2);
        model.setEmployeePaymentsListDataSource(dataSource);
    }

    private void prepareActingDataSource(IBusinessComponent component)
    {
        DynamicListDataSource<EmployeeActingItem> dataSource = new DynamicListDataSource<>(component, component1 -> {
            MQBuilder builder = new MQBuilder(EmployeeActingItem.ENTITY_CLASS, "b");
            builder.add(MQExpression.eq("b", EmployeeActingItem.employeePost().s(), model.getEmployeePost()));

            //применяем фильтры
            IUISettings settings = getPresenter().getSettings();
            if (settings.get("beginDateFrom") != null)
                builder.add(MQExpression.greatOrEq("b", EmployeeActingItem.beginDate().s(), settings.get("beginDateFrom")));
            if (settings.get("beginDateTo") != null)
                builder.add(MQExpression.lessOrEq("b", EmployeeActingItem.beginDate().s(), settings.get("beginDateTo")));
            if (settings.get("endDateFrom") != null)
                builder.add(MQExpression.greatOrEq("b", EmployeeActingItem.endDate().s(), settings.get("endDateFrom")));
            if (settings.get("endDateTo") != null)
                builder.add(MQExpression.lessOrEq("b", EmployeeActingItem.endDate().s(), settings.get("endDateTo")));

            if (settings.get("orgUnitFilter") != null)
                builder.add(MQExpression.eq("b", EmployeeActingItem.orgUnit().s(), settings.get("orgUnitFilter")));
            if (settings.get("postFilter") != null)
                builder.add(MQExpression.eq("b", EmployeeActingItem.postBoundedWithQGandQL().s(), settings.get("postFilter")));

            new OrderDescriptionRegistry("b").applyOrder(builder, model.getEmployeeActingListDataSource().getEntityOrder());

            UniBaseUtils.createPage(model.getEmployeeActingListDataSource(), builder, getSession());
        });

        HeadColumn headColumn = new HeadColumn("headColumn", "Период исполнения обязанностей");
        headColumn.setHeaderAlign("center");
        headColumn.addColumn(new SimpleColumn("Дата начала", EmployeeActingItem.beginDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER));
        headColumn.addColumn(new SimpleColumn("Дата окончания", EmployeeActingItem.endDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER));
        dataSource.addColumn(headColumn);
        dataSource.addColumn(new SimpleColumn("Должность", EmployeeActingItem.postBoundedWithQGandQL().title().s()));
        dataSource.addColumn(new SimpleColumn("Подразделение", EmployeeActingItem.orgUnit().s() + "." + OrgUnit.P_TITLE_WITH_TYPE));
        dataSource.addColumn(new SimpleColumn("ФИО отсутствующего сотрудника", EmployeeActingItem.missingEmployeePost().person().fullFio().s()));
        dataSource.addColumn(new ActionColumn("Редактирование", ActionColumn.EDIT, "uniempEmployeePostViewExtUI:onClickEditActingItem").setPermissionKey("editEmployeeActingItem_employeePost"));
        dataSource.addColumn(new ActionColumn("Удаление", ActionColumn.DELETE, "uniempEmployeePostViewExtUI:onDeleteRowFromList").setPermissionKey("deleteEmployeeActingItem_employeePost"));

        model.setEmployeeActingListDataSource(dataSource);
    }

    private void prepareСombinationPostDataSource(IBusinessComponent component)
    {
        DynamicListDataSource<CombinationPost> dataSource = new DynamicListDataSource<>(component, component1 -> {
            MQBuilder builder = new MQBuilder(CombinationPost.ENTITY_CLASS, "b");
            builder.add(MQExpression.eq("b", CombinationPost.employeePost().s(), model.getEmployeePost()));

            //фильтры
            IUISettings settings = getPresenter().getSettings();
            if (settings.get("combinationPostTypeFiltr") != null)
                builder.add(MQExpression.eq("b", CombinationPost.combinationPostType().s(), settings.get("combinationPostTypeFiltr")));
            if (settings.get("combinationPostFiltr") != null)
                builder.add(MQExpression.eq("b", CombinationPost.postBoundedWithQGandQL().s(), settings.get("combinationPostFiltr")));
            if (settings.get("combinationOrgUnitFiltr") != null)
                builder.add(MQExpression.eq("b", CombinationPost.orgUnit().s(), settings.get("combinationOrgUnitFiltr")));

            if (settings.get("combinationBeginDateFrom") != null)
                builder.add(MQExpression.greatOrEq("b", CombinationPost.beginDate().s(), settings.get("combinationBeginDateFrom")));
            if (settings.get("combinationBeginDateTo") != null)
                builder.add(MQExpression.lessOrEq("b", CombinationPost.beginDate().s(), settings.get("combinationBeginDateTo")));
            if (settings.get("combinationEndDateFrom") != null)
                builder.add(MQExpression.greatOrEq("b", CombinationPost.endDate().s(), settings.get("combinationEndDateFrom")));
            if (settings.get("combinationEndDateTo") != null)
                builder.add(MQExpression.lessOrEq("b", CombinationPost.endDate().s(), settings.get("combinationEndDateTo")));

            if (settings.get("combinationShowNotActiveFiltr") == null || !((Boolean) settings.get("combinationShowNotActiveFiltr")))
            {
                Date now = Calendar.getInstance().getTime();

                AbstractExpression beginDateLessOrEq = MQExpression.lessOrEq("b", CombinationPost.beginDate(), now);
                AbstractExpression endDateIsNull = MQExpression.isNull("b", CombinationPost.endDate());
                AbstractExpression endDateGreatOrEq = MQExpression.greatOrEq("b", CombinationPost.endDate(), now);

                // совмещение идет: дата начала уже была, дата окончания отсутствует или еще не наступила
                builder.add(MQExpression.and(beginDateLessOrEq, MQExpression.or(endDateIsNull, endDateGreatOrEq)));

                // основная должность активна
                builder.add(MQExpression.eq("b", CombinationPost.employeePost().postStatus().active(), Boolean.TRUE));
            }

            new OrderDescriptionRegistry("b").applyOrder(builder, model.getCombinationPostDataSource().getEntityOrder());

            UniBaseUtils.createPage(model.getCombinationPostDataSource(), builder, getSession());

            MQBuilder staffRateBuilder = new MQBuilder(CombinationPostStaffRateItem.ENTITY_CLASS, "sr");

            Map<CombinationPost, List<CombinationPostStaffRateItem>> combinationPostStaffRateMap = new HashMap<>();
            for (CombinationPostStaffRateItem item : staffRateBuilder.<CombinationPostStaffRateItem>getResultList(getSession()))
            {
                List<CombinationPostStaffRateItem> list = combinationPostStaffRateMap.get(item.getCombinationPost());
                if (list == null)
                    list = new ArrayList<>();
                list.add(item);
                combinationPostStaffRateMap.put(item.getCombinationPost(), list);
            }

            for (ViewWrapper<CombinationPost> wrapper : ViewWrapper.<CombinationPost>getPatchedList(model.getCombinationPostDataSource()))
            {
                Double staffRate = 0d;
                for (CombinationPostStaffRateItem staffRateItem : combinationPostStaffRateMap.get(wrapper.getEntity()))
                    staffRate += staffRateItem.getStaffRate();

                wrapper.setViewProperty(EmployeePostViewLegacyModel.COMBINATION_POST_STAFF_RATE, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(staffRate) + (wrapper.getEntity().isFreelance() ? " (Вне штата)" : ""));

                StringBuilder orderNumberDateBuilder = new StringBuilder();
                if (wrapper.getEntity().getOrderNumber() != null)
                    orderNumberDateBuilder.append("Приказ №").append(wrapper.getEntity().getOrderNumber());
                if (wrapper.getEntity().getCommitDateSystem() != null)
                    orderNumberDateBuilder.append(" от ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(wrapper.getEntity().getCommitDateSystem()));

                wrapper.setViewProperty(EmployeePostViewLegacyModel.COMBINATION_POST_ORDER, orderNumberDateBuilder.toString());

                StringBuilder agreementBuilder = new StringBuilder();
                if (wrapper.getEntity().getCollateralAgreementNumber() != null)
                    agreementBuilder.append("Доп. соглашение к ТД №").append(wrapper.getEntity().getCollateralAgreementNumber());
                if (wrapper.getEntity().getCollateralAgreementDate() != null)
                    agreementBuilder.append(" от ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(wrapper.getEntity().getCollateralAgreementDate()));

                wrapper.setViewProperty(EmployeePostViewLegacyModel.COMBINATION_POST_AGREEMENT, agreementBuilder.toString());
            }
        });

        dataSource.addColumn(new SimpleColumn("Должность, отнесенная к ПКГ и КУ", CombinationPost.postBoundedWithQGandQL().fullTitle().s()));
        dataSource.addColumn(new SimpleColumn("Подразделение", CombinationPost.orgUnit().s() + "." + OrgUnit.P_TITLE_WITH_TYPE));
        dataSource.addColumn(new SimpleColumn("Тип совмещения", CombinationPost.combinationPostType().title()));
        dataSource.addColumn(new PublisherLinkColumn("На время отсутствия сотрудника", CombinationPost.L_MISSING_EMPLOYEE_POST + "." + EmployeePost.P_TITLE_WITH_SHORT_POST_TYPE_AND_STATUS).setResolver(new DefaultPublisherLinkResolver()
        {
            @SuppressWarnings("unchecked")
            @Override
            public Object getParameters(IEntity entity)
            {
                EmployeePost missingEmployeePost = ((ViewWrapper<CombinationPost>) entity).getEntity().getMissingEmployeePost();
                return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, missingEmployeePost == null ? null : missingEmployeePost.getId()).add("selectedTab", "employeePostTab");
            }

        }));
//        dataSource.addColumn(new SimpleColumn("Состояние должности", CombinationPost.employeePost().postStatus().title().s()));
        dataSource.addColumn(new SimpleColumn("Ставка", EmployeePostViewLegacyModel.COMBINATION_POST_STAFF_RATE));
        dataSource.addColumn(new SimpleColumn("Оклад", CombinationPost.salary().s(), DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS));
        HeadColumn headColumn = new HeadColumn("headColumn", "Период работы по совмещению");
        headColumn.setHeaderAlign("center");
        headColumn.addColumn(new SimpleColumn("Дата начала", CombinationPost.beginDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER));
        headColumn.addColumn(new SimpleColumn("Дата окончания", CombinationPost.endDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER));
        dataSource.addColumn(headColumn);
        dataSource.addColumn(new SimpleColumn("Приказ", EmployeePostViewLegacyModel.COMBINATION_POST_ORDER));
        dataSource.addColumn(new SimpleColumn("Доп. соглашение к трудовому договору", EmployeePostViewLegacyModel.COMBINATION_POST_AGREEMENT));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "uniempEmployeePostViewExtUI:onClickEditCombinationPost").setPermissionKey("editEmployeeCombinationPost_employeePost"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "uniempEmployeePostViewExtUI:onDeleteCombinationPost", "Удалить должность по совмещению «{0}»?", CombinationPost.postBoundedWithQGandQL().title().s()).setPermissionKey("deleteEmployeeCombinationPost_employeePost"));

        model.setCombinationPostDataSource(dataSource);
    }

    private void prepareCertificationDataSource(IBusinessComponent component)
    {
        DynamicListDataSource<EmployeeCertificationItem> dataSource = new DynamicListDataSource<>(component, component1 -> {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmployeeCertificationItem.class, "b").column("b");
            builder.where(DQLExpressions.eqValue(DQLExpressions.property(EmployeeCertificationItem.employeePost().fromAlias("b")), model.getEmployeePost()));

            //применяем фильтры
            IUISettings settings = getPresenter().getSettings();
            if (settings.get("certificationDateFrom") != null)
                builder.where(DQLExpressions.ge(DQLExpressions.property(EmployeeCertificationItem.certificationDate().fromAlias("b")), DQLExpressions.commonValue(settings.get("certificationDateFrom"))));
            if (settings.get("certificationDateTo") != null)
                builder.where(DQLExpressions.le(DQLExpressions.property(EmployeeCertificationItem.certificationDate().fromAlias("b")), DQLExpressions.commonValue(settings.get("certificationDateTo"))));
            if (settings.get("certificationDocumentDateFrom") != null)
                builder.where(DQLExpressions.ge(DQLExpressions.property(EmployeeCertificationItem.documentDate().fromAlias("b")), DQLExpressions.commonValue(settings.get("certificationDocumentDateFrom"))));
            if (settings.get("certificationDocumentDateTo") != null)
                builder.where(DQLExpressions.le(DQLExpressions.property(EmployeeCertificationItem.documentDate().fromAlias("b")), DQLExpressions.commonValue(settings.get("certificationDocumentDateTo"))));
            if (settings.get("certificationDocumentNumberFiltr") != null)
                builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(EmployeeCertificationItem.documentNumber().fromAlias("b"))), DQLExpressions.value(CoreStringUtils.escapeLike((String) settings.get("certificationDocumentNumberFiltr")))));

            new DQLOrderDescriptionRegistry(EmployeeCertificationItem.class, "b").applyOrder(builder, model.getCertificationDataSource().getEntityOrder());

            UniBaseUtils.createPage(model.getCertificationDataSource(), builder, getSession());
        });

        dataSource.addColumn(new SimpleColumn("Дата аттестации", EmployeeCertificationItem.certificationDate(), DateFormatter.DEFAULT_DATE_FORMATTER));
        dataSource.addColumn(new SimpleColumn("Решение комиссии", EmployeeCertificationItem.P_RESOLUTION_COMMISSION).setOrderable(false));
        HeadColumn headColumn = new HeadColumn("headColumn", "Документ");
        headColumn.setHeaderAlign("center");
        headColumn.addColumn(new SimpleColumn("Номер", EmployeeCertificationItem.documentNumber()));
        headColumn.addColumn(new SimpleColumn("Дата", EmployeeCertificationItem.documentDate(), DateFormatter.DEFAULT_DATE_FORMATTER));
        dataSource.addColumn(headColumn);
        dataSource.addColumn(new SimpleColumn("Основание", EmployeeCertificationItem.P_BASICS).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "uniempEmployeePostViewExtUI:onClickEditCertificationItem").setPermissionKey("editEmployeeCertificationItem_employeePost"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "uniempEmployeePostViewExtUI:onDeleteRowFromList", "Удалить данные об аттестации от {0}?", EmployeeCertificationItem.certificationDate()).setPermissionKey("deleteEmployeeCertificationItem_employeePost"));

        model.setCertificationDataSource(dataSource);
    }

    private void prepareTrainingDataSource(IBusinessComponent component)
    {
        DynamicListDataSource<EmployeeTrainingItem> dataSource = new DynamicListDataSource<>(component, component1 -> {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmployeeTrainingItem.class, "b").column("b");
            builder.where(DQLExpressions.eqValue(DQLExpressions.property(EmployeeTrainingItem.employee().fromAlias("b")), model.getEmployee()));

            //применяем фильтры
            IUISettings settings = getPresenter().getSettings();
            if (settings.get("startTrainingDateFrom") != null)
                builder.where(DQLExpressions.ge(DQLExpressions.property(EmployeeTrainingItem.startDate().fromAlias("b")), DQLExpressions.commonValue(settings.get("startTrainingDateFrom"))));
            if (settings.get("startTrainingDateTo") != null)
                builder.where(DQLExpressions.le(DQLExpressions.property(EmployeeTrainingItem.startDate().fromAlias("b")), DQLExpressions.commonValue(settings.get("startTrainingDateTo"))));
            if (settings.get("finishTrainingDateFrom") != null)
                builder.where(DQLExpressions.ge(DQLExpressions.property(EmployeeTrainingItem.finishDate().fromAlias("b")), DQLExpressions.commonValue(settings.get("finishTrainingDateFrom"))));
            if (settings.get("finishTrainingDateTo") != null)
                builder.where(DQLExpressions.le(DQLExpressions.property(EmployeeTrainingItem.finishDate().fromAlias("b")), DQLExpressions.commonValue(settings.get("finishTrainingDateTo"))));
//        if (settings.get("eduDocumentTrainingNumberFiltr") != null)
//            builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(EmployeeTrainingItem.eduDocumentNumber().fromAlias("b"))), DQLExpressions.value(CoreStringUtils.escapeLike((String) settings.get("eduDocumentTrainingNumberFiltr")))));
            if (settings.get("eduDocumentTrainingDateFrom") != null)
                builder.where(DQLExpressions.ge(DQLExpressions.property(EmployeeTrainingItem.eduDocumentDate().fromAlias("b")), DQLExpressions.commonValue(settings.get("eduDocumentTrainingDateFrom"))));
            if (settings.get("eduDocumentTrainingDateTo") != null)
                builder.where(DQLExpressions.le(DQLExpressions.property(EmployeeTrainingItem.eduDocumentDate().fromAlias("b")), DQLExpressions.commonValue(settings.get("eduDocumentTrainingDateTo"))));

            new DQLOrderDescriptionRegistry(EmployeeTrainingItem.class, "b").applyOrder(builder, model.getTrainingDataSource().getEntityOrder());

            UniBaseUtils.createPage(model.getTrainingDataSource(), builder, getSession());
        });

        HeadColumn headDateColumn = new HeadColumn("headDateColumn", "Период обучения");
        headDateColumn.setHeaderAlign("center");
        headDateColumn.addColumn(new SimpleColumn("Дата начала", EmployeeTrainingItem.startDate(), DateFormatter.DEFAULT_DATE_FORMATTER));
        headDateColumn.addColumn(new SimpleColumn("Дата окончания", EmployeeTrainingItem.finishDate(), DateFormatter.DEFAULT_DATE_FORMATTER));
        dataSource.addColumn(headDateColumn);
        dataSource.addColumn(new SimpleColumn("Вид повышения квалификации", EmployeeTrainingItem.P_TRAINING_TITLE).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Образовательное учреждение", EmployeeTrainingItem.P_EDU_INSTITUTION_AND_ADDRESS).setOrderable(false));
        HeadColumn headDocumentColumn = new HeadColumn("headDocumentColumn", "Документ");
        headDocumentColumn.setHeaderAlign("center");
        headDocumentColumn.addColumn(new SimpleColumn("Тип", EmployeeTrainingItem.eduDocumentType().title()));
        headDocumentColumn.addColumn(new SimpleColumn("Номер", EmployeeTrainingItem.P_SERIA_AND_NUMBER).setOrderable(false));
        headDocumentColumn.addColumn(new SimpleColumn("Дата", EmployeeTrainingItem.eduDocumentDate(), DateFormatter.DEFAULT_DATE_FORMATTER));
        dataSource.addColumn(headDocumentColumn);
        dataSource.addColumn(new SimpleColumn("Основание", EmployeeTrainingItem.P_BASICS).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Файл копии документа о повышении квалификации", "printer", "uniempEmployeePostViewExtUI:onClickPrintEduDocumentTraining").setDisableHandler(entity -> ((EmployeeTrainingItem) entity).getEduDocumentFile() == null).setPermissionKey("printEmployeeTrainingItem_employeePost"));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "uniempEmployeePostViewExtUI:onClickEditTrainingItem").setPermissionKey("editEmployeeTrainingItem_employeePost"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "uniempEmployeePostViewExtUI:onDeleteRowFromList", "Удалить данные о повышении квалификации с {0} по {1}?", EmployeeTrainingItem.startDate(), EmployeeTrainingItem.finishDate()).setPermissionKey("deleteEmployeeTrainingItem_employeePost"));

        model.setTrainingDataSource(dataSource);
    }

    private void prepareRetrainingDataSource(IBusinessComponent component)
    {
        DynamicListDataSource<EmployeeRetrainingItem> dataSource = new DynamicListDataSource<>(component, component1 -> {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmployeeRetrainingItem.class, "b").column("b");
            builder.where(DQLExpressions.eqValue(DQLExpressions.property(EmployeeRetrainingItem.employee().fromAlias("b")), model.getEmployee()));

            //применяем фильтры
            IUISettings settings = getPresenter().getSettings();
            if (settings.get("startRetrainingDateFrom") != null)
                builder.where(DQLExpressions.ge(DQLExpressions.property(EmployeeRetrainingItem.startDate().fromAlias("b")), DQLExpressions.commonValue(settings.get("startRetrainingDateFrom"))));
            if (settings.get("startRetrainingDateTo") != null)
                builder.where(DQLExpressions.le(DQLExpressions.property(EmployeeRetrainingItem.startDate().fromAlias("b")), DQLExpressions.commonValue(settings.get("startRetrainingDateTo"))));
            if (settings.get("finishRetrainingDateFrom") != null)
                builder.where(DQLExpressions.ge(DQLExpressions.property(EmployeeRetrainingItem.finishDate().fromAlias("b")), DQLExpressions.commonValue(settings.get("finishRetrainingDateFrom"))));
            if (settings.get("finishRetrainingDateTo") != null)
                builder.where(DQLExpressions.le(DQLExpressions.property(EmployeeRetrainingItem.finishDate().fromAlias("b")), DQLExpressions.commonValue(settings.get("finishRetrainingDateTo"))));
//        if (settings.get("eduDocumentRetrainingNumberFiltr") != null)
//            builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(EmployeeRetrainingItem.eduDocumentNumber().fromAlias("b"))), DQLExpressions.value(CoreStringUtils.escapeLike((String) settings.get("eduDocumentRetrainingNumberFiltr")))));
            if (settings.get("eduDocumentRetrainingDateFrom") != null)
                builder.where(DQLExpressions.ge(DQLExpressions.property(EmployeeRetrainingItem.eduDocumentDate().fromAlias("b")), DQLExpressions.commonValue(settings.get("eduDocumentRetrainingDateFrom"))));
            if (settings.get("eduDocumentRetrainingDateTo") != null)
                builder.where(DQLExpressions.le(DQLExpressions.property(EmployeeRetrainingItem.eduDocumentDate().fromAlias("b")), DQLExpressions.commonValue(settings.get("eduDocumentRetrainingDateTo"))));

            new DQLOrderDescriptionRegistry(EmployeeRetrainingItem.class, "b").applyOrder(builder, model.getRetrainingDataSource().getEntityOrder());

            UniBaseUtils.createPage(model.getRetrainingDataSource(), builder, getSession());
        });

        HeadColumn headDateColumn = new HeadColumn("headDateColumn", "Период переподготовки");
        headDateColumn.setHeaderAlign("center");
        headDateColumn.addColumn(new SimpleColumn("Дата начала", EmployeeRetrainingItem.startDate(), DateFormatter.DEFAULT_DATE_FORMATTER));
        headDateColumn.addColumn(new SimpleColumn("Дата окончания", EmployeeRetrainingItem.finishDate(), DateFormatter.DEFAULT_DATE_FORMATTER));
        dataSource.addColumn(headDateColumn);
        dataSource.addColumn(new SimpleColumn("Специальность (Направление)", EmployeeRetrainingItem.P_SPECIALITY_TITLE).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Образовательное учреждение", EmployeeRetrainingItem.P_EDU_INSTITUTION_AND_ADDRESS).setOrderable(false));
        HeadColumn headDocumentColumn = new HeadColumn("headDocumentColumn", "Документ");
        headDocumentColumn.setHeaderAlign("center");
        headDocumentColumn.addColumn(new SimpleColumn("Тип", EmployeeRetrainingItem.eduDocumentType().title()));
        headDocumentColumn.addColumn(new SimpleColumn("Номер", EmployeeRetrainingItem.P_SERIA_AND_NUMBER).setOrderable(false));
        headDocumentColumn.addColumn(new SimpleColumn("Дата", EmployeeRetrainingItem.eduDocumentDate(), DateFormatter.DEFAULT_DATE_FORMATTER));
        dataSource.addColumn(headDocumentColumn);
        dataSource.addColumn(new SimpleColumn("Основание", EmployeeRetrainingItem.P_BASICS).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Файл копии документа о профессиональной переподготовке", "printer", "uniempEmployeePostViewExtUI:onClickPrintEduDocumentRetraining").setDisableHandler(entity -> ((EmployeeRetrainingItem) entity).getEduDocumentFile() == null).setPermissionKey("printEmployeeRetrainingItem_employeePost"));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "uniempEmployeePostViewExtUI:onClickEditRetrainingItem").setPermissionKey("editEmployeeRetrainingItem_employeePost"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "uniempEmployeePostViewExtUI:onDeleteRowFromList", "Удалить данные о профессиональной переподготовке с {0} по {1}?", EmployeeRetrainingItem.startDate(), EmployeeRetrainingItem.finishDate()).setPermissionKey("deleteEmployeeRetrainingItem_employeePost"));

        model.setRetrainingDataSource(dataSource);
    }

    // Internal Private (не относится к LegacyModel)

    private void prepareEmployeeHoliday()
    {
        EmployeePostViewUI presenter = getPresenter();

        MQBuilder builder = new MQBuilder(EmployeeHoliday.ENTITY_NAME, "h");
        builder.add(MQExpression.eq("h", EmployeeHoliday.L_EMPLOYEE_POST, presenter.getEmployeePost()));
        builder.add(MQExpression.eq("h", EmployeeHoliday.holidayType().longTime(), Boolean.TRUE));
        Date now = new Date();
        builder.add(MQExpression.lessOrEq("h", EmployeeHoliday.startDate(), now));
        _longTimeEmployeeHoliday = null;
        _period = null;
        for (EmployeeHoliday holiday : builder.<EmployeeHoliday>getResultList(getSession()))
        {
            Date end = getHolidayEnd(getSession(), presenter.getEmployeePost().getWorkWeekDuration(), holiday);
            if (end != null && CoreDateUtils.getDayFirstTimeMoment(end).after(CoreDateUtils.getDayFirstTimeMoment(now)))
            {
                _longTimeEmployeeHoliday = null;
                _period = "c " + DateFormatter.DEFAULT_DATE_FORMATTER.format(holiday.getStartDate()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(end);
                break;
            }
        }
    }

    private static Date getHolidayEnd(Session session, EmployeeWorkWeekDuration weekDuration, EmployeeHoliday holiday)
    {
        return holiday.getStartDate() != null && weekDuration != null ? calcHolidayEnd(session, weekDuration, holiday.getStartDate(), holiday.getDuration()) : null;
    }

    //вычисление даты "по" которую длится отпуск с учетом праздничных дней производ. календаря заданного типа раб.недели
    private static Date calcHolidayEnd(Session session, EmployeeWorkWeekDuration weekDuration, Date beginDate, Integer duration)
    {
        if (!EmployeeWorkWeekDurationCodes.WEEK_5.equals(weekDuration.getCode())
                && !EmployeeWorkWeekDurationCodes.WEEK_6.equals(weekDuration.getCode()))
        {
            return getDateWithSomeDaysAdded(beginDate, duration - 1);
        }
        GregorianCalendar endCalendar = (GregorianCalendar) GregorianCalendar.getInstance();
        endCalendar.setTime(beginDate);
        Map<Integer, IndustrialCalendar> industrialCalendars = new HashMap<>();
        {
            MQBuilder builder = new MQBuilder(IndustrialCalendar.ENTITY_CLASS, "ic");
            builder.add(MQExpression.eq("ic", IndustrialCalendar.weekDuration(), weekDuration));
            builder.add(MQExpression.greatOrEq("ic", IndustrialCalendar.year(), endCalendar.get(Calendar.YEAR)));
            for (IndustrialCalendar ic : builder.<IndustrialCalendar>getResultList(session))
            {
                industrialCalendars.put(ic.getYear(), ic);
            }
        }
        for (int i = 1; i < duration; i++)
        {
            IndustrialCalendar calendar = industrialCalendars.get(endCalendar.get(Calendar.YEAR));
            if (null == calendar)
            {
                endCalendar.add(Calendar.DAY_OF_YEAR, 1);
                continue;
            }

            MQBuilder builder = new MQBuilder(IndustrialCalendarHoliday.ENTITY_CLASS, "ich");
            builder.add(MQExpression.eq("ich", IndustrialCalendarHoliday.calendar(), calendar));
            builder.add(MQExpression.eq("ich", IndustrialCalendarHoliday.holiday().month(), endCalendar.get(Calendar.MONTH) + 1));
            builder.add(MQExpression.eq("ich", IndustrialCalendarHoliday.holiday().beginDay(), endCalendar.get(Calendar.DAY_OF_MONTH)));
            builder.add(MQExpression.eq("ich", IndustrialCalendarHoliday.holiday().preholiday(), Boolean.FALSE));
            List<IHolidayDuration> holidays = builder.getResultList(session);

            for (IHolidayDuration holiday : holidays)
            {
                int begin = holiday.getBeginDay();
                int end = null == holiday.getEndDay() ? begin : holiday.getEndDay();
                endCalendar.add(Calendar.DAY_OF_YEAR, Math.max(1, end - begin + 1));
            }
            endCalendar.add(Calendar.DAY_OF_YEAR, 1);
        }
        return endCalendar.getTime();
    }

    private static Date getDateWithSomeDaysAdded(Date date, int daysAmount)
    {
        Calendar calendar = CoreDateUtils.createCalendar(date);
        calendar.add(Calendar.DAY_OF_YEAR, daysAmount);
        return calendar.getTime();
    }
}
