/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.profLectStaffConsistency.Add;

import java.util.Date;
import java.util.List;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;

/**
 * @author dseleznev
 * Created on: 13.04.2009
 */
public class Model
{
    private Date _reportDate;

    private boolean _orgUnitTypeActive;
    private boolean _orgUnitActive;

    private OrgUnitType _orgUnitType;
    private List<OrgUnit> _orgUnitsList;

    private List<OrgUnitType> _orgUnitTypesList;
    private IMultiSelectModel _orgUnitsListModel;

    public Date getReportDate()
    {
        return _reportDate;
    }

    public void setReportDate(Date reportDate)
    {
        this._reportDate = reportDate;
    }

    public boolean isOrgUnitTypeActive()
    {
        return _orgUnitTypeActive;
    }

    public void setOrgUnitTypeActive(boolean orgUnitTypeActive)
    {
        this._orgUnitTypeActive = orgUnitTypeActive;
    }

    public boolean isOrgUnitActive()
    {
        return _orgUnitActive;
    }

    public void setOrgUnitActive(boolean orgUnitActive)
    {
        this._orgUnitActive = orgUnitActive;
    }

    public OrgUnitType getOrgUnitType()
    {
        return _orgUnitType;
    }

    public void setOrgUnitType(OrgUnitType orgUnitType)
    {
        this._orgUnitType = orgUnitType;
    }

    public List<OrgUnitType> getOrgUnitTypesList()
    {
        return _orgUnitTypesList;
    }

    public void setOrgUnitTypesList(List<OrgUnitType> orgUnitTypesList)
    {
        this._orgUnitTypesList = orgUnitTypesList;
    }

    public List<OrgUnit> getOrgUnitsList()
    {
        return _orgUnitsList;
    }

    public void setOrgUnitsList(List<OrgUnit> orgUnitsList)
    {
        this._orgUnitsList = orgUnitsList;
    }

    public IMultiSelectModel getOrgUnitsListModel()
    {
        return _orgUnitsListModel;
    }

    public void setOrgUnitsListModel(IMultiSelectModel orgUnitsListModel)
    {
        this._orgUnitsListModel = orgUnitsListModel;
    }
}