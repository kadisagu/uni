/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.component.employee.ContractAddEdit;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uniemp.dao.UniempDAO;
import ru.tandemservice.uniemp.entity.catalog.LabourContractType;

/**
 * @author E. Grigoriev
 * @since 09.07.2008
 */
public class DAO extends UniempDAO<Model> implements IDAO
{
    public static final long MAX_FILE_SIZE = 1024 * 1024; //2 Mb

    @Override
    public void prepare(Model model)
    {
        model.setTypeList(getCatalogItemList(LabourContractType.class));
        if (model.getLabourContract().getId() != null)
            model.setLabourContract(get(model.getLabourContract().getId()));
        else {
            model.getLabourContract().setEmployeePost(get(EmployeePost.class, model.getEmployeePostId()));
            model.getLabourContract().setBeginDate(model.getLabourContract().getEmployeePost().getPostDate());
            model.getLabourContract().setEndDate(model.getLabourContract().getEmployeePost().getDismissalDate() != null ? model.getLabourContract().getEmployeePost().getDismissalDate() : null);
        }
    }

    @Override
    public void update(Model model)
    {
        if (model.getLabourContract().getId() != null)
            getSession().update(model.getLabourContract());
        else
            getSession().save(model.getLabourContract());
    }

}