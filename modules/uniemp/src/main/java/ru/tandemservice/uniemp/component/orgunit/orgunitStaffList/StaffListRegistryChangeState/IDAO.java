/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.StaffListRegistryChangeState;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author ashaburov
 * Created on: 08.08.2011
 */
public interface IDAO extends IUniDao<Model>
{
}