package ru.tandemservice.uniemp.entity.employee;

import org.tandemframework.shared.employeebase.base.entity.Employee;
import ru.tandemservice.uniemp.entity.employee.gen.EmployeeCardGen;

public class EmployeeCard extends EmployeeCardGen
{
    public EmployeeCard()
    {
    }
    
    public EmployeeCard(Employee employee)
    {
        setEmployee(employee);
    }
    
}