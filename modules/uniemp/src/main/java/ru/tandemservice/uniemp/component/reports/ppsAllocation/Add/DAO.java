/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.ppsAllocation.Add;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.Post;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeeTypeCodes;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import org.tandemframework.shared.person.catalog.entity.ScienceBranch;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.catalog.EmployeeTemplateDocument;
import ru.tandemservice.uniemp.util.UniempReportUtil;

import java.util.*;

/**
 * @author dseleznev
 * Created on: 20.04.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public Integer preparePrintReport(Model model)
    {
        RtfInjectModifier paramModifier = new RtfInjectModifier();
        paramModifier.put("reportYear", String.valueOf(model.getYear().getId()));

        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.getName2data().putAll(prepareTablesData(model.getYear().getId().intValue(), paramModifier));

        return UniempReportUtil.preparePrintingReport(getCatalogItem(EmployeeTemplateDocument.class, UniempDefines.TEMPLATE_PPS_ALLOCATION), paramModifier, tableModifier);
    }

    private Map<String, String[][]> prepareTablesData(Integer year, RtfInjectModifier paramModifier)
    {
        Calendar cal = Calendar.getInstance();
        cal.clear();
        cal.set(Calendar.YEAR, year + 1);
        cal.add(Calendar.MILLISECOND, -1);
        
        Map<String, String[][]> result = new HashMap<>();

        List<String[]> lines = new ArrayList<>();
        
        Map<Long, CoreCollectionUtils.Pair<Integer, Integer>> statMap = new HashMap<>();
        Map<Long, PersonAcademicDegree> scienceDegreesMap = getEmployeeDegreesMap(cal.getTime());
        List<EmployeePost> postsList = getEmployeePostsList(cal.getTime());

        Integer totalDoctorsCnt = 0;
        Integer totalMastersCnt = 0;
        for(EmployeePost post : postsList)
        {
            PersonAcademicDegree degree = scienceDegreesMap.get(post.getEmployee().getPerson().getId());
            if(null != degree)
            {
                Long scBranchId = degree.getScienceBranch() != null ? degree.getScienceBranch().getId() : null;
                CoreCollectionUtils.Pair<Integer, Integer> pair = statMap.get(scBranchId);
                if(null == pair) pair = new CoreCollectionUtils.Pair<>(0, 0);
                
                if(UniDefines.SCIENCE_DEGREE_TYPE_DOCTOR.equals(degree.getAcademicDegree().getType().getCode()))
                {
                    pair.setX(pair.getX() + 1);
                    totalDoctorsCnt++;
                }
                else if(UniDefines.SCIENCE_DEGREE_TYPE_MASTER.equals(degree.getAcademicDegree().getType().getCode()))
                {
                    pair.setY(pair.getY() + 1);
                    totalMastersCnt++;
                }
                
                statMap.put(scBranchId, pair);
            }
        }
        
        paramModifier.put("totalDoctors", String.valueOf(totalDoctorsCnt));
        paramModifier.put("totalMasters", String.valueOf(totalMastersCnt));
        
        int cnt = 1;
        for(ScienceBranch branch : getCatalogItemListOrderByCode(ScienceBranch.class))
        {
            CoreCollectionUtils.Pair<Integer, Integer> pair = statMap.get(branch.getId());
            if(null == pair) pair = new CoreCollectionUtils.Pair<>(0, 0);
            lines.add(new String[] {branch.getTitle(), String.valueOf(++cnt), pair.getX() != 0 ? String.valueOf(pair.getX()) : "", pair.getY() != 0 ? String.valueOf(pair.getY()) : ""});
        }

        result.put("T", lines.toArray(new String[][] {}));

        return result;
    }
    
    /**
     * Возвращает список сотрудников ППС
     */
    private List<EmployeePost> getEmployeePostsList(Date reportDate)
    {
        MQBuilder builder = new MQBuilder(EmployeePost.ENTITY_CLASS, "ep");
        builder.add(MQExpression.in("ep", EmployeePost.L_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_POST + "." + Post.L_EMPLOYEE_TYPE, EmployeeManager.instance().dao().getEmployeeTypeWithAllChilds(getCatalogItem(EmployeeType.class, EmployeeTypeCodes.EDU_STAFF))));
        builder.add(MQExpression.lessOrEq("ep", EmployeePost.P_POST_DATE, reportDate));
        AbstractExpression expr1 = MQExpression.isNull("ep", EmployeePost.P_DISMISSAL_DATE);
        AbstractExpression expr2 = MQExpression.greatOrEq("ep", EmployeePost.P_DISMISSAL_DATE, reportDate);
        builder.add(MQExpression.or(expr1, expr2));
        return builder.getResultList(getSession());
    }
    
    /**
     * Возвращает MAP с учеными степенями персон
     * ключ - id персоны,
     * значение - ученая степень
     */
    private Map<Long, PersonAcademicDegree> getEmployeeDegreesMap(Date reportDate)
    {
        Map<Long, PersonAcademicDegree> resultMap = new HashMap<>();
        MQBuilder builder = new MQBuilder(PersonAcademicDegree.ENTITY_CLASS, "aed");
        builder.add(MQExpression.lessOrEq("aed", PersonAcademicDegree.P_DATE, reportDate));
        List<PersonAcademicDegree> degreesList = builder.getResultList(getSession());
        
        for(PersonAcademicDegree degree : degreesList)
        {
            PersonAcademicDegree degr = resultMap.get(degree.getPerson().getId());
            if(null == degr || degree.getAcademicDegree().getType().getLevel() > degr.getAcademicDegree().getType().getLevel())
                degr = degree;
            resultMap.put(degree.getPerson().getId(), degr);
        }
        
        return resultMap;
    }
/*    
    private Map<Long, CoreCollectionUtils.Pair<Integer, Integer>> getScienceBranchCountersMap(Date reportDate)
    {
        Map<Long, CoreCollectionUtils.Pair<Integer, Integer>> result = new HashMap<Long, CoreCollectionUtils.Pair<Integer,Integer>>();
        
        MQBuilder builder = new MQBuilder(PersonAcademicDegree.ENTITY_CLASS, "d");
        builder.add(MQExpression.lessOrEq("d", PersonAcademicDegree.P_DATE, reportDate));
        AbstractExpression expr1 = MQExpression.eq("d", PersonAcademicDegree.L_ACADEMIC_DEGREE + "." + ScienceDegree.L_TYPE + "." + ScienceDegreeType.P_CODE, UniempDefines.SCIENCE_DEGREE_TYPE_DOCTOR);
        AbstractExpression expr2 = MQExpression.eq("d", PersonAcademicDegree.L_ACADEMIC_DEGREE + "." + ScienceDegree.L_TYPE + "." + ScienceDegreeType.P_CODE, UniempDefines.SCIENCE_DEGREE_TYPE_MASTER);
        builder.add(MQExpression.or(expr1, expr2));
        List<PersonAcademicDegree> degreesList = builder.getResultList(getSession());
        
        for(PersonAcademicDegree degree : degreesList)
        {
            Long scBranchId = degree.getScienceBranch().getId();
            CoreCollectionUtils.Pair<Integer, Integer> pair = result.get(scBranchId);
            if(null == pair) pair = new CoreCollectionUtils.Pair<Integer, Integer>(0, 0);
            
            if(UniempDefines.SCIENCE_DEGREE_TYPE_DOCTOR.equals(degree.getAcademicDegree().getType().getCode()))
                pair.setX(pair.getX() + 1);
            else if(UniempDefines.SCIENCE_DEGREE_TYPE_MASTER.equals(degree.getAcademicDegree().getType().getCode()))
                pair.setY(pair.getY() + 1);
            
            result.put(scBranchId, pair);
        }
        return result;
    }*/
}