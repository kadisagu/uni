/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.science.EmployeeMedicalSpecAddEdit;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author dseleznev
 * Created on: 21.10.2010
 */
public interface IDAO extends IUniDao<Model>
{
}