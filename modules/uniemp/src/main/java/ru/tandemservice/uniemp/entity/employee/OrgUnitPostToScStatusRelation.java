package ru.tandemservice.uniemp.entity.employee;

import org.tandemframework.shared.person.catalog.entity.ScienceStatus;
import ru.tandemservice.uniemp.entity.employee.gen.OrgUnitPostToScStatusRelationGen;

public class OrgUnitPostToScStatusRelation extends OrgUnitPostToScStatusRelationGen
{

    public OrgUnitPostToScStatusRelation()
    {
    }

    public OrgUnitPostToScStatusRelation(OrgUnitPostRelation rel, ScienceStatus academicStatus)
    {
        setOrgUnitPostRelation(rel);
        setAcademicStatus(academicStatus);
    }

}