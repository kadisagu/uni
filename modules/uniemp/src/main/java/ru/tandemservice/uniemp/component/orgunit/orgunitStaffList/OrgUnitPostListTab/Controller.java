/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.OrgUnitPostListTab;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BooleanColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import ru.tandemservice.uniemp.IUniempComponents;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation;

/**
 * @author dseleznev
 * Created on: 16.09.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
        prepareOrgUnitPostDataSource(component);
    }

    private void prepareOrgUnitPostDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getOrgUnitPostDataSource() != null)
        {
            return;
        }

        DynamicListDataSource<OrgUnitPostRelation> dataSource = new DynamicListDataSource<>(component, this);
        dataSource.addColumn(new SimpleColumn("Название должности", OrgUnitPostRelation.P_SIMPLE_TITLE));
        dataSource.addColumn(new SimpleColumn("Тип должн.", OrgUnitPostRelation.POST_TYPE_KEY).setClickable(false));
        dataSource.addColumn(new SimpleColumn("ПКГ", OrgUnitPostRelation.PROF_QUALIFICATION_GROUP_KEY).setClickable(false));
        dataSource.addColumn(new SimpleColumn("КУ", OrgUnitPostRelation.QUALIFICATION_LEVEL_KEY).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Базовый оклад", OrgUnitPostRelation.BASE_SALARY_KEY).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Р", OrgUnitPostRelation.ETKS_KEY).setClickable(false).setOrderable(false));
        dataSource.addColumn(new BooleanColumn("Руководящая", OrgUnitTypePostRelation.headerPost().s()));

        if (!model.getOrgUnit().isArchival())
        {
            dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditPost").setPermissionKey("orgUnit_editPost_" + getModel(component).getOrgUnit().getOrgUnitType().getCode()));
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeletePost", "Удалить должность «{0}»?", new Object[] { OrgUnitPostRelation.P_TITLE_WITH_SALARY }).setPermissionKey("orgUnit_deletePost_" + getModel(component).getOrgUnit().getOrgUnitType().getCode()).setDisabledProperty(OrgUnitPostRelation.P_USED_IN_ANY_STAFF_LIST));
        }

        getModel(component).setOrgUnitPostDataSource(dataSource);
    }

    @Override
    public void updateListDataSource(IBusinessComponent component)
    {
        getDao().prepareListDataSource(getModel(component));
    }

    public void onClickAddPost(IBusinessComponent component)
    {
        getModel(component).setOrgUnitPostRelationId(null);
        component.createChildRegion("postListScope", new ComponentActivator(IUniempComponents.ORG_UNIT_POST_ADDEDIT));
    }

    public void onClickEditPost(IBusinessComponent component)
    {
        getModel(component).setOrgUnitPostRelationId((Long)component.getListenerParameter());
        component.createChildRegion("postListScope", new ComponentActivator(IUniempComponents.ORG_UNIT_POST_ADDEDIT));
    }

    public void onClickDeletePost(IBusinessComponent component)
    {
        UniempDaoFacade.getUniempDAO().deleteOrgUnitPostRelation(getDao().get(OrgUnitPostRelation.class, (Long)component.getListenerParameter()));
    }
}
