/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.component.vacationShedule.VacationScheduleItemsAdd;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.ui.formatters.EmployeeCodeFormatter;
import ru.tandemservice.unibase.UniBaseUtils;

/**
 * @author dseleznev
 * Created on: 18.01.2011
 */
public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
        prepareListDataSource(component);
    }

    public void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;
        DynamicListDataSource<EmployeePost> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new CheckboxColumn("selected", "Выбор", false));
        dataSource.addColumn(new SimpleColumn("Табельный номер", EmployeePost.employee().employeeCode().s(), new EmployeeCodeFormatter()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("ФИО сотрудника", EmployeePost.employee().person().fullFio().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Состояние", EmployeePost.postStatus().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Тип назначения", EmployeePost.postType().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Должность", EmployeePost.postRelation().postBoundedWithQGandQL().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Подразделение", EmployeePost.orgUnit().fullTitle().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Кол-во дней отпуска", EmployeePost.postRelation().postBoundedWithQGandQL().post().employeeType().holidayDuration().daysAmount().s()).setClickable(false));
        model.setDataSource(dataSource);
    }

    @Override
    public void updateListDataSource(IBusinessComponent component)
    {
        getDao().prepareListDataSource(getModel(component));
    }

    public void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);
        component.getDesktop().setRefreshScheduled(true);
        ErrorCollector errCollector = component.getUserContext().getErrorCollector();
        getDao().validate(model, errCollector);
        if (errCollector.isHasFieldErrors()) return;
        getDao().update(model);
        deactivate(component);
    }

    public void onClickSearch(IBusinessComponent component)
    {
        getModel(component).getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.setLastNameFilter(null);
        model.setHolidayDurationFilter(null);
        model.setAcitvePostStatusFilter(null);
        model.setPostStatusFilter(null);
        model.setPostTypeFilter(null);
        model.setPostFilter(null);
        onClickSearch(component);
    }
}