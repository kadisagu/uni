/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.ext.OrgUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.util.IDaoExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uniemp.base.ext.OrgUnit.logic.DaoUpdateOrgUnitChangeType;

/**
 * Create by ashaburov
 * Date 19.01.12
 */
@Configuration
public class OrgUnitExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private OrgUnitManager _orgUnitManager;

    @Bean
    public IDaoExtension methodUpdateOrgUnitChangeTypeDao()
    {
        return new DaoUpdateOrgUnitChangeType();
    }

    @Bean
    public ItemListExtension<IDaoExtension> methodUpdateOrgUnitChangeType()
    {
        return itemListExtension(_orgUnitManager.methodUpdateOrgUnitChangeType())
                .add("orgUnitPostRelationFix", methodUpdateOrgUnitChangeTypeDao())
                .create();
    }
}
