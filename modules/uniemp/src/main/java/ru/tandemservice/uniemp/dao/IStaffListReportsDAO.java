/**
 * $Id$
 */
package ru.tandemservice.uniemp.dao;

import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.ProfQualificationGroup;
import org.tandemframework.shared.employeebase.catalog.entity.QualificationLevel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.uniemp.component.reports.staffList.Add.OrgUnitTypeWrapper;
import ru.tandemservice.uniemp.component.reports.staffListAllocation.Add.EmployeePostFakeWrapper;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.employee.StaffList;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;
import ru.tandemservice.uniemp.entity.employee.StaffListItem;
import ru.tandemservice.uniemp.entity.employee.StaffListPaymentBase;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author dseleznev
 * Created on: 10.11.2010
 */
public interface IStaffListReportsDAO
{
    String STAFF_LIST_REPORTS_DAO_BEAN_NAME = "staffListReportsDao";

    List<OrgUnit> getFilteredOrgUnitslList(OrgUnit orgUnitFilter, final List<OrgUnitType> orgUnitTypesList, final Date reportDate);

    StaffList getActiveStaffList(OrgUnit orgUnit, Date reportDate);

    List<StaffList> getActiveStaffListsList(List<OrgUnit> orgUnitsList, Date reportDate);

    List<StaffListItem> getStaffListItemsList(List<StaffList> staffListsList, final PostBoundedWithQGandQL post, final List<EmployeeType> employeeTypesList, final QualificationLevel qualificationLevel, final ProfQualificationGroup profQualificationGroup, String financingSourceCode, String financingSourceItemCode);

    List<StaffListAllocationItem> getStaffListAllocationItemsList(List<StaffListItem> staffListItemsList);

    List<Payment> getDisplayablePaymentsForStaffList();

    List<Payment> getDisplayablePaymentsForStaffList(Set<Payment> additionalPaymentsSet);

    List<StaffListPaymentBase> getStaffListPaymentsList(final List<StaffListItem> staffListItemsList, List<Payment> paymentsList, String financingSourceCode, String financingSourceItemCode);

    Set<Payment> getUsedPaymentsSet(List<StaffListPaymentBase> staffListPaymentsList);

    List<EmployeePostFakeWrapper> getEmployeePostStaffRateItemList(List<StaffListItem> staffListItemsList, List<OrgUnit> orgUnitsList, final PostBoundedWithQGandQL post, final List<EmployeeType> employeeTypesList, final QualificationLevel qualificationLevel, final ProfQualificationGroup profQualificationGroup, final Date reportDate, final Boolean budgetActiveType);

    /**
     * Поднимает из Базы данных Подразделения, которые относятся к указанным Типам и Дата архивации больше или равна reportDate, если Подразделение архивно.
     * @param orgUnitTypesList список Типов подразделений, к которым относятся необходимые Подразделения
     * @param allowedOrgUnitIdsList необходимые Подразделения
     * @param reportDate дата архивации Подразделения, если необходимы архивные Подразделения
     * @return Возвращает список Подразделений.
     */
    List<OrgUnit> getAccountableOrgUnitsList(List<OrgUnitType> orgUnitTypesList, List<Long> allowedOrgUnitIdsList, Date reportDate);

    /**
     * Возвращает список Должностей ШР, ограниченных либо списком Должностей, либо остальными указанными параметрами.
     * @param orgUnitsList подразделения, ШР которого рассматриваем
     * @param reportDate дата архивации ШР, если необходима
     * @param financingSourceCode код источника финансирования
     * @param postList список должностей, Должности ШР которых необходимы
     * @param employeeTypeList список типов должностей, Должности ШР которых необходимы
     * @param qualificationLevel квалификационный уровень, Должности ШР которых необходимы
     * @param qualificationGroup ПКГ, Должности ШР которых необходимы
     * @return Возвращает список Должностей Штатного расписания.
     */
    List<StaffListItem> getStaffListItemList(List<OrgUnit> orgUnitsList, Date reportDate, String financingSourceCode, List<PostBoundedWithQGandQL> postList, List<EmployeeType> employeeTypeList, QualificationLevel qualificationLevel, ProfQualificationGroup qualificationGroup);

    /**
     * Возвращает список Базовых выплат ШР, ограниченных указанными.
     * @param staffListItemsList список Должностей ШР, выплаты которых рассматриваем
     * @param paymentsList список необходимых выплат
     * @param financingSourceCode код источника финансирования
     * @return Возвращает список Базовых выплат ШР.
     */
    List<StaffListPaymentBase> getStaffListPaymentList(List<StaffListItem> staffListItemsList, List<Payment> paymentsList, String financingSourceCode);

    /**
     * Возвращает список дочерних подразделений для указанного подразделения,
     * включая дочерние подразделения дочерних подразделений и т.д.<p/>
     * Другими словами, возвращает иерархию оргструктуры в последовательном виде.
     * @param orgUnit главное подразделение(академия)
     * @param orgUnitsToTake список подразделений, которые необходимы
     * @param orgUnitTypesMap мапа, где ключ - id, значение - OrgUnitTypeWrapper
     * @param actualDate дата архивации
     * @return Возвращает список Подразделений.
     */
    List<OrgUnit> getChildOrgUnits(OrgUnit orgUnit, List<OrgUnit> orgUnitsToTake, Map<Long, OrgUnitTypeWrapper> orgUnitTypesMap, Date actualDate);
}