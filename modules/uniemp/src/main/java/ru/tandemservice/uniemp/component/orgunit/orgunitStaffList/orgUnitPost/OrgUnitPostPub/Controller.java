/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.orgUnitPost.OrgUnitPostPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.uniemp.IUniempComponents;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;

/**
 * @author dseleznev
 * Created on: 19.09.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickEdit(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniempComponents.ORG_UNIT_POST_ADDEDIT, new ParametersMap().add("orgUnitId", getModel(component).getOrgUnit().getId())));
    }

    public void onClickDelete(IBusinessComponent component)
    {
        UniempDaoFacade.getUniempDAO().deleteOrgUnitPostRelation(getModel(component).getOrgUnitPostRel());
        deactivate(component);
    }

}