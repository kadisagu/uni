/**
 *$Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffList.StaffListPub;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.employee.CombinationPostStaffRateItem;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Create by ashaburov
 * Date 10.05.12
 * <p/>
 * Врапер для элементов в списке Штатная расстановка в Штатном расписании.<p/>
 * Врапер нужен для вывода в списке строк разных типов:
 * <ol>
 * <li>строки должности</li>
 * <li>строки выплат должности по источникам финансирования</li>
 * <li>итоговой строки по источникам финансирования</li>
 * <li>итоговой строки</li>
 * </ol>
 */
public class AllocationWrapper extends DataWrapper
{
    //Constructor

    public AllocationWrapper(Long id, String title)
    {
        super(id, title);
    }


    //Property

    public static String P_POST_TITLE = "title";
    public static String P_POST_TYPE = "postType";
    public static String P_PROF_QUALIFICATION_GROUP = "profQualiGroup";
    public static String P_QUALIFICATION_LEVEL = "qualiLevel";
    public static String P_SALARY = "salary";
    public static String P_ETKS = "etks";
    public static String P_STAFF_RATE = "staffRate";
    public static String P_FINANCING_SOURCE = "financingSourceTitle";
    public static String P_FINANCING_SOURCE_ITEM = "financingSourceItemTitle";
    public static String P_MONTH_BASE_SALARY_FUND = "monthBaseSalaryFund";
    public static String P_TARGET_SALARY_FUND = "targetSalaryFund";
    public static String P_CANT_COPY = "cantCopy";
    public static String P_CANT_DELETE = "cantDelete";
    public static String P_CANT_EDIT= "cantEdit";


    //Fields

    private Map<String, Double> _codeValueMap = new HashMap<>();//код выплаты на сумму выплаты
    private StaffListAllocationItem _allocationItem;//должность штатного расписания
    private FinancingSource _financingSource;//источник финансирования
    private FinancingSourceItem _financingSourceItem;//источник финансирования (детально)
    private boolean _finSrcTotal;//врапер Итоговой строки по источникам финансирования
    private boolean _total;//врапер Итоговыой строки
    private boolean _finSrc;//врапер строки выплат должности
    private boolean _reserve;//строка является резеровм - фейком
    private Double _totalStaffRate = 0d;//итоговое значение ставок для итоговых строк
    private Double _totalMonthBaseSalary = 0d;//итоговое значение месячного фонда оплаты труда для итоговых строк
    private Double _totalTargetSalary = 0d;//итоговое значение месячного фонда оплаты труда с надбавками для итоговых строк
    private boolean _bold;//строчка должна быть жырной
    private boolean _cantCopy;//строчка не может быть скопирована
    private boolean _cantDelete;//строчка не может быть удалена
    private boolean _cantEdit;//строчка не может редактироваться
    private boolean _staffRateInvalid;//превышение ставок
    private boolean _postStaffRateInvalid;//ставка сотрудника по ист.фин. первышет его соответствующую ставку в ШР


    //Methods

    @Override
    public Object getProperty(Object propertyPath)
    {
        if (_codeValueMap.containsKey((String)propertyPath))
            return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(_codeValueMap.get(propertyPath));

        return super.getProperty(propertyPath);
    }

    /**
     * Связывает указаный код выплаты с её суммой.<p/>
     * Если для кода уже сохранено значение, то оно суммируются с указанным.
     * @param paymentCode код выплаты
     * @param value       сумма выплаты
     */
    public void putPaymentValue(String paymentCode, Double value)
    {
        if (paymentCode == null)
            throw new NullPointerException("'paymentCode' must not be null");

        _codeValueMap.put(paymentCode, _codeValueMap.get(paymentCode) != null ? _codeValueMap.get(paymentCode) + value : value);
    }

    /**
     * @param financingSource     источник финансирования
     * @param financingSourceItem источник финансирования (детально)
     * @return <tt>true</tt>, если указанные истчники финансирования совпадают с источниками финансирования врапера, иначе <tt>false</tt>
     */
    public boolean equalsFinSrs(FinancingSource financingSource, FinancingSourceItem financingSourceItem)
    {
        CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem> key = new CoreCollectionUtils.Pair<>(getFinancingSource(), getFinancingSourceItem());
        CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem> egKey = new CoreCollectionUtils.Pair<>(financingSource, financingSourceItem);

        return key.equals(egKey);
    }


    //Property Getters
    //методы возвращают различные значения в зависимости от того в качестве чего используется врапер:
    //1. стрка должности,
    //2. строка для выплат должности,
    //3. итоговая строка по источникам финансирования,
    //4. итоговая строка

    public String getPostType()
    {
        if (!_total && !_finSrcTotal)
            return getAllocationItem() != null ? getAllocationItem().getStaffListItem().getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL().getPost().getEmployeeType().getShortTitle() : "";
        else
            return "";
    }

    public String getProfQualiGroup()
    {
        if (!_total && !_finSrcTotal)
            return getAllocationItem() != null ? getAllocationItem().getStaffListItem().getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL().getProfQualificationGroup().getShortTitle() : "";
        else
            return "";
    }

    public String getQualiLevel()
    {
        if (!_total && !_finSrcTotal)
            return getAllocationItem() != null ? getAllocationItem().getStaffListItem().getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL().getQualificationLevel().getShortTitle() : "";
        else
            return "";
    }

    public String getSalary()
    {
        if (!_total && !_finSrcTotal)
            return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(_allocationItem.getBaseSalary());
        else
            return "";
    }

    public String getEtks()
    {
        if (!_total && !_finSrcTotal)
            return getAllocationItem() != null ? getAllocationItem().getStaffListItem().getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL().getEtksLevels() != null ? getAllocationItem().getStaffListItem().getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL().getEtksLevels().getTitle() : "" : "";
        else
            return "";
    }

    public String getStaffRate()
    {
        if (_finSrc)
            return "";
        else if (!_total && !_finSrcTotal)
            return getAllocationItem() != null ? DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(getAllocationItem().getStaffRate()) : "";
        else
            return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(_totalStaffRate);
    }

    public String getFinancingSourceTitle()
    {
        if (!_total)
            return getFinancingSource().getTitle();
        else
            return "";
    }

    public String getFinancingSourceItemTitle()
    {
        if (!_total)
            return getFinancingSourceItem() != null ? getFinancingSourceItem().getTitle() : "";
        else
            return "";
    }

    public String getMonthBaseSalaryFund()
    {
        if (_finSrc)
            return "";
        else if (!_total && !_finSrcTotal)
        {
            Double salary = getAllocationItem() != null ? getAllocationItem().getMonthBaseSalaryFund() : 0d;
            return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(salary);
        }
        else
        {
            return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(_totalMonthBaseSalary);
        }
    }

    public String getTargetSalaryFund()
    {
        if (_finSrc)
        {
            Double salary = 0d;
            for (Map.Entry<String, Double> entry : _codeValueMap.entrySet())
                salary += entry.getValue();

            return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(salary);
        }
        else if (!_total && !_finSrcTotal)
        {
            Double salary = getAllocationItem() != null ? getAllocationItem().getMonthBaseSalaryFund() : 0d;
            for (Map.Entry<String, Double> entry : _codeValueMap.entrySet())
                salary += entry.getValue();

            return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(salary);
        }
        else
        {
            return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(_totalTargetSalary);
        }
    }

    public String getFioTitle()
    {
        if (!_finSrcTotal && !_total && !_finSrc)
        {
            if (_allocationItem != null && _allocationItem.getEmployeePost() != null)
            {
                StringBuilder fioBuilder = new StringBuilder();
                if (!_allocationItem.isReserve())
                {
                    fioBuilder.append(_allocationItem.getEmployeePost().getPerson().getFio());

                    if (_allocationItem.isCombination())
                    {
                        if (_allocationItem.getCombinationPost() != null)
                        {
                            boolean isActive = !_allocationItem.getCombinationPost().getBeginDate().after(new Date()) && (_allocationItem.getCombinationPost().getEndDate() == null || _allocationItem.getCombinationPost().getEndDate().getTime() >= CommonBaseDateUtil.trimTime(new Date()).getTime());
                            fioBuilder.append(" - ").append(_allocationItem.getCombinationPost().getCombinationPostType().getShortTitle()).append(" ").append(isActive ? _allocationItem.getEmployeePost().getPostStatus().getShortTitle() : "н");
                        }
                    }
                    else
                    {
                        fioBuilder.append(" - ").append(_allocationItem.getEmployeePost().getPostType().getShortTitle()).append(" ").append(_allocationItem.getEmployeePost().getPostStatus().getShortTitle());
                    }
                }
                else
                {
                    fioBuilder.append("Резерв");
                }

                return  fioBuilder.toString();
            }
            else if (_reserve)
            {
                return "Резерв";
            }

        }

        return "";
    }

    public String getOnMouseOverListener()
    {
        if (!_finSrcTotal && !_total && !_finSrc)
        {
            if (_allocationItem != null && _allocationItem.getEmployeePost() != null)
            {
                String msgText = "";

                OrgUnit firstOrgUnit = _allocationItem.getStaffListItem().getStaffList().getOrgUnit();
                OrgUnit secondOrgUnit = _allocationItem.getEmployeePost().getOrgUnit();
                if (!firstOrgUnit.equals(secondOrgUnit))
                    msgText += "Сотрудник " + _allocationItem.getEmployeePost().getPerson().getFio() + " числится на подразделении " + _allocationItem.getEmployeePost().getOrgUnit().getTitle() + ".";

                OrgUnitTypePostRelation firstPost = _allocationItem.getStaffListItem().getOrgUnitPostRelation().getOrgUnitTypePostRelation();
                OrgUnitTypePostRelation secondPost = _allocationItem.getEmployeePost().getPostRelation();
                if (_allocationItem.getEmployeePost() != null && !firstPost.equals(secondPost))
                    msgText += " Сотрудник " + _allocationItem.getEmployeePost().getPerson().getFio() + " числится на должности " + _allocationItem.getEmployeePost().getPostRelation().getTitle() + ".";

                if (_postStaffRateInvalid)
                {
                    Double staffRate = 0d;
                    if (_allocationItem.isCombination())
                    {
                        for (CombinationPostStaffRateItem item : UniempDaoFacade.getUniempDAO().getCombinationPostStaffRateItemList(_allocationItem.getCombinationPost()))
                            staffRate += item.getStaffRate();
                    }
                    else
                    {
                        for (EmployeePostStaffRateItem item : UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(_allocationItem.getEmployeePost()))
                            staffRate += item.getStaffRate();
                    }

                    BigDecimal x = new BigDecimal(staffRate);
                    x = x.setScale(3, BigDecimal.ROUND_HALF_UP);

                    StringBuilder msgTextBuilder =  new StringBuilder(" Суммарная ставка в штатной расстановке превышает ставку ");
                    msgTextBuilder.append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(x.doubleValue()));
                    msgTextBuilder.append(_allocationItem.isCombination() ? " по совмещению сотрудника " : " сотрудника ");
                    msgTextBuilder.append(_allocationItem.getEmployeePost().getPerson().getFio());

                    msgText += msgTextBuilder.toString();
                }

                return  msgText.isEmpty() ? "" : "showErrFrame(event, '" + msgText + "');";
            }
        }

        return "";
    }

    public String getOnMouseOverStaffRateListener()
    {
        if (_postStaffRateInvalid && _allocationItem.getEmployeePost() != null)
        {
            Double staffRate = 0d;
            if (_allocationItem.isCombination())
            {
                for (CombinationPostStaffRateItem item : UniempDaoFacade.getUniempDAO().getCombinationPostStaffRateItemList(_allocationItem.getCombinationPost()))
                    staffRate += item.getStaffRate();
            }
            else
            {
                for (EmployeePostStaffRateItem item : UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(_allocationItem.getEmployeePost()))
                    staffRate += item.getStaffRate();
            }

            BigDecimal x = new BigDecimal(staffRate);
            x = x.setScale(3, BigDecimal.ROUND_HALF_UP);

            StringBuilder msgTextBuilder =  new StringBuilder("Суммарная ставка в штатной расстановке превышает ставку ");
            msgTextBuilder.append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(x.doubleValue()));
            msgTextBuilder.append(_allocationItem.isCombination() ? " по совмещению сотрудника " : " сотрудника ");
            msgTextBuilder.append(_allocationItem.getEmployeePost().getPerson().getFio());

            return  "showErrFrame(event, '" + msgTextBuilder.toString() + "');";
        }

        return "";
    }

    public Map<String, Object> getParametersMap()
    {
        if (!_finSrcTotal && !_total && !_finSrc)
        {
            if (_allocationItem != null && _allocationItem.getEmployeePost() != null)
            {
                Map<String, Object> map = new HashMap<>();
                map.put(PublisherActivator.PUBLISHER_ID_KEY, _allocationItem.getEmployeePost() != null ? _allocationItem.getEmployeePost().getId() : null);
                map.put("selectedTab", "employeePostTab");
                if (_allocationItem.isCombination())
                    map.put("selectedDataTab", "Combination");

                return map;
            }
        }

        return null;
    }

    public Double getMonthBaseSalary()
    {
        if (_finSrc)
            return 0d;
        else if (!_total && !_finSrcTotal)
        {
            return getAllocationItem() != null ? getAllocationItem().getMonthBaseSalaryFund() : 0d;
        }
        else
        {
            return _totalMonthBaseSalary;
        }
    }

    public Double getTargetSalary()
    {
        if (_finSrc)
        {
            Double salary = 0d;
            for (Map.Entry<String, Double> entry : _codeValueMap.entrySet())
                salary += entry.getValue();

            return salary;
        }
        else if (!_total && !_finSrcTotal)
        {
            Double salary = getAllocationItem() != null ? getAllocationItem().getMonthBaseSalaryFund() : 0d;
            for (Map.Entry<String, Double> entry : _codeValueMap.entrySet())
                salary += entry.getValue();

            return salary;
        }
        else
        {
            return _totalTargetSalary;
        }
    }


    //Getters & Setters

    public Map<String, Double> getCodeValueMap()
    {
        return _codeValueMap;
    }

    public void setCodeValueMap(Map<String, Double> codeValueMap)
    {
        _codeValueMap = codeValueMap;
    }

    public StaffListAllocationItem getAllocationItem()
    {
        return _allocationItem;
    }

    public void setAllocationItem(StaffListAllocationItem allocationItem)
    {
        _allocationItem = allocationItem;
    }

    public FinancingSource getFinancingSource()
    {
        return _financingSource;
    }

    public void setFinancingSource(FinancingSource financingSource)
    {
        _financingSource = financingSource;
    }

    public FinancingSourceItem getFinancingSourceItem()
    {
        return _financingSourceItem;
    }

    public void setFinancingSourceItem(FinancingSourceItem financingSourceItem)
    {
        _financingSourceItem = financingSourceItem;
    }

    public boolean isFinSrcTotal()
    {
        return _finSrcTotal;
    }

    public void setFinSrcTotal(boolean finSrcTotal)
    {
        _finSrcTotal = finSrcTotal;
    }

    public boolean isTotal()
    {
        return _total;
    }

    public void setTotal(boolean total)
    {
        _total = total;
    }

    public boolean isFinSrc()
    {
        return _finSrc;
    }

    public void setFinSrc(boolean finSrc)
    {
        _finSrc = finSrc;
    }

    public Double getTotalStaffRate()
    {
        return _totalStaffRate;
    }

    public void setTotalStaffRate(Double totalStaffRate)
    {
        _totalStaffRate = totalStaffRate;
    }

    public Double getTotalMonthBaseSalary()
    {
        return _totalMonthBaseSalary;
    }

    public void setTotalMonthBaseSalary(Double totalMonthBaseSalary)
    {
        _totalMonthBaseSalary = totalMonthBaseSalary;
    }

    public Double getTotalTargetSalary()
    {
        return _totalTargetSalary;
    }

    public void setTotalTargetSalary(Double totalTargetSalary)
    {
        _totalTargetSalary = totalTargetSalary;
    }

    public boolean isBold()
    {
        return _bold;
    }

    public void setBold(boolean bold)
    {
        _bold = bold;
    }

    public boolean isReserve()
    {
        return _reserve;
    }

    public void setReserve(boolean reserve)
    {
        _reserve = reserve;
    }

    public boolean isStaffRateInvalid()
    {
        return _staffRateInvalid;
    }

    public void setStaffRateInvalid(boolean staffRateInvalid)
    {
        _staffRateInvalid = staffRateInvalid;
    }

    public boolean isCantCopy()
    {
        return _cantCopy;
    }

    public void setCantCopy(boolean cantCopy)
    {
        _cantCopy = cantCopy;
    }

    public boolean isCantDelete()
    {
        return _cantDelete;
    }

    public void setCantDelete(boolean cantDelete)
    {
        _cantDelete = cantDelete;
    }

    public boolean isCantEdit()
    {
        return _cantEdit;
    }

    public void setCantEdit(boolean cantEdit)
    {
        _cantEdit = cantEdit;
    }

    public boolean isPostStaffRateInvalid()
    {
        return _postStaffRateInvalid;
    }

    public void setPostStaffRateInvalid(boolean postStaffRateInvalid)
    {
        _postStaffRateInvalid = postStaffRateInvalid;
    }
}
