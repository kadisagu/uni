/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.component.vacationShedule.VacationScheduleAddEdit;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.UniempDAO;
import ru.tandemservice.uniemp.entity.catalog.StaffListState;
import ru.tandemservice.uniemp.entity.employee.VacationSchedule;
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising;
import ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa;
import ru.tandemservice.unimv.entity.visa.Visa;

import java.util.*;

/**
 * @author dseleznev
 * Created on: 10.01.2011
 */
public class DAO extends UniempDAO<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        if (null != model.getVacationScheduleId())
            model.setVacationSchedule(getNotNull(VacationSchedule.class, model.getVacationScheduleId()));
        if (null != model.getOrgUnitId())
            model.setOrgUnit(get(OrgUnit.class, model.getOrgUnitId()));

        if (null == model.getVacationSchedule())
        {
            int year = CoreDateUtils.getYear(new Date());
            MQBuilder builder = new MQBuilder("select max(vs." + VacationSchedule.year().s() + ") from " + VacationSchedule.ENTITY_CLASS + " vs");
            builder.add(MQExpression.eq("vs", VacationSchedule.orgUnit(), model.getOrgUnit()));
            Number maxYear = (Number)builder.uniqueResult(getSession());
            if (null != maxYear) year = maxYear.intValue() + 1;

            model.setVacationSchedule(new VacationSchedule());
            model.getVacationSchedule().setOrgUnit(model.getOrgUnit());
            model.getVacationSchedule().setCreateDate(new Date());
            model.getVacationSchedule().setYear(year);

            model.getVacationSchedule().setState(getCatalogItem(StaffListState.class, UniempDefines.STAFF_LIST_STATUS_FORMING));
        }
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        if (null != model.getVacationSchedule().getNumber())
        {
            MQBuilder yearToNumberValdationBuilder = new MQBuilder(VacationSchedule.ENTITY_CLASS, "vs");
            yearToNumberValdationBuilder.add(MQExpression.eq("vs", VacationSchedule.year().s(), model.getVacationSchedule().getYear()));
            yearToNumberValdationBuilder.add(MQExpression.eq("vs", VacationSchedule.number().s(), model.getVacationSchedule().getNumber()));
            if (null != model.getVacationScheduleId())
                yearToNumberValdationBuilder.add(MQExpression.notEq("vs", VacationSchedule.id().s(), model.getVacationScheduleId()));
            if (yearToNumberValdationBuilder.getResultCount(getSession()) > 0)
                errors.add("Указанный номер уже назначен другому графику отпусков в рамках указанного года.", "number");
        }

        MQBuilder yearToOrgUnitValidationBuilder = new MQBuilder(VacationSchedule.ENTITY_CLASS, "vs");
        yearToOrgUnitValidationBuilder.add(MQExpression.eq("vs", VacationSchedule.year().s(), model.getVacationSchedule().getYear()));
        yearToOrgUnitValidationBuilder.add(MQExpression.eq("vs", VacationSchedule.orgUnit().s(), model.getVacationSchedule().getOrgUnit()));
        if (null != model.getVacationScheduleId())
            yearToOrgUnitValidationBuilder.add(MQExpression.notEq("vs", VacationSchedule.id().s(), model.getVacationScheduleId()));
        if (yearToOrgUnitValidationBuilder.getResultCount(getSession()) > 0)
            errors.add("На указанный год для данного подразделения уже создан график отпусков.", "year");
    }

    @Override
    public void update(Model model)
    {
        getSession().saveOrUpdate(model.getVacationSchedule());

        if (null == model.getVacationScheduleId())
        {
            List<Long> postToAddIds = new ArrayList<>();
            Map<Long, EmployeePostPossibleVisa> possibleVisasMap = new HashMap<>();

            EmployeePost rector = (EmployeePost) TopOrgUnit.getInstance().getHead();
            if (null != rector) postToAddIds.add(rector.getId());

            if (null != model.getOrgUnit().getHead())
                postToAddIds.add(model.getOrgUnit().getHead().getId());

            MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "ou");
            builder.add(MQExpression.eq("ou", OrgUnit.personnelDepartment().s(), Boolean.TRUE));
            if (builder.getResultCount(getSession()) > 0)
            {
                EmployeePost personnelDepartmentHead = (EmployeePost)builder.<OrgUnit> getResultList(getSession()).get(0).getHead();
                if (null != personnelDepartmentHead)
                    postToAddIds.add(personnelDepartmentHead.getId());
            }

            MQBuilder resultBuilder = new MQBuilder(EmployeePostPossibleVisa.ENTITY_CLASS, "v");
            resultBuilder.add(MQExpression.in("v", EmployeePostPossibleVisa.entity().id().s(), postToAddIds));
            for (EmployeePostPossibleVisa possVisa : resultBuilder.<EmployeePostPossibleVisa> getResultList(getSession()))
            {
                Long id = possVisa.getEntity().getId();
                if (null == possibleVisasMap.get(id))
                    possibleVisasMap.put(id, possVisa);
            }

            Set<Long> alreadyAddedVisas = new HashSet<>();
            for (Long postId : postToAddIds)
            {
                if (null != possibleVisasMap.get(postId))
                {
                    EmployeePostPossibleVisa possVisa = possibleVisasMap.get(postId);
                    if(alreadyAddedVisas.contains(possVisa.getId())) continue;
                    alreadyAddedVisas.add(possVisa.getId());

                    Visa visa = new Visa();
                    visa.setMandatory(false);
                    visa.setPossibleVisa(possVisa);
                    visa.setDocument(model.getVacationSchedule());
                    visa.setIndex(getCount(Visa.class, Visa.L_DOCUMENT, model.getVacationSchedule()));
                    visa.setGroupMemberVising(getCatalogItem(GroupsMemberVising.class, "2"));
                    save(visa);
                }
            }
        }
    }
}