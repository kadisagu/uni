/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.bo.EmpEmployeeRetrainingItem.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.fias.base.bo.util.NonActiveSelectValueStyle;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.person.catalog.entity.*;
import ru.tandemservice.uniemp.base.bo.EmpEmployeeTrainingItem.ui.AddEdit.EmpEmployeeTrainingItemAddEditUI;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Create by ashaburov
 * Date 28.03.12
 */
@Configuration
public class EmpEmployeeRetrainingItemAddEdit extends BusinessComponentManager
{
    public static final String COUNTRIES_DS = "countriesDS";
    public static final String SETTLEMENT_DS = "addressItemDS";
    public static final String EDU_INSTITUTION_TYPE_DS = "eduInstitutionTypeDS";
    public static final String EDU_INSTITUTION_KIND_DS = "eduInstitutionKindDS";
    public static final String EDU_INSTITUTION_DS = "eduInstitutionDS";
    public static final String EDU_DOCUMENT_TYPE_DS = "eduDocumentTypeDS";
    public static final String EMPLOYEE_SPECIALITY_DS = "employeeSpecialityDS";
    public static final String DIPLOMA_QUALIFICATIONS_DS = "diplomaQualificationsDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(COUNTRIES_DS, countriesDSHandler()))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(SETTLEMENT_DS, getName(), AddressItem.settlementComboDSHandler(getName())).valueStyleSource(value ->
                    {
                        AddressItem addressItem = (AddressItem) value;
                        //Серым шрифтом выделены утратившие актуальность (по классификатору) населенные пункты
                        return !addressItem.isActive() ? NonActiveSelectValueStyle.INSTANCE : null;
                    }
                ))
                .addDataSource(selectDS(EDU_INSTITUTION_TYPE_DS, eduInstitutionTypeDSHandler()))
                .addDataSource(selectDS(EDU_INSTITUTION_KIND_DS, eduInstitutionKindDSHandler()))
                .addDataSource(selectDS(EDU_INSTITUTION_DS, eduInstitutionDSHandler()))
                .addDataSource(selectDS(EDU_DOCUMENT_TYPE_DS, eduDocumentTypeDSHandler()))
                .addDataSource(selectDS(EMPLOYEE_SPECIALITY_DS, employeeSpecialityDSHandler()))
                .addDataSource(selectDS(DIPLOMA_QUALIFICATIONS_DS, diplomaQualificationsDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler countriesDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), AddressCountry.class);
    }

    @Bean
    public IDefaultComboDataSourceHandler eduInstitutionTypeDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), EducationalInstitutionTypeKind.class)
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);

                ep.dqlBuilder.where(isNull(property(EducationalInstitutionTypeKind.parent().fromAlias("e"))));
            }
        };
    }

    @Bean
    public IDefaultComboDataSourceHandler eduInstitutionKindDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), EducationalInstitutionTypeKind.class)
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);

                EducationalInstitutionTypeKind type = ep.context.get(EmpEmployeeTrainingItemAddEditUI.SELECTED_EDU_INSTITUTION_TYPE);

                if (type != null)
                    ep.dqlBuilder.where(eqValue(property(EducationalInstitutionTypeKind.parent().fromAlias("e")), type));
            }
        };
    }

    @Bean
    public IDefaultComboDataSourceHandler eduInstitutionDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), EduInstitution.class)
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);

                AddressCountry country = ep.context.get(EmpEmployeeTrainingItemAddEditUI.SELECTED_COUNTRY);

                if (country != null)
                    ep.dqlBuilder.where(eqValue(property(EduInstitution.address().country().fromAlias("e")), country));

                AddressItem addressItems = ep.context.get(EmpEmployeeTrainingItemAddEditUI.SELECTED_ADDRESS_ITEM);

                if (addressItems != null)
                    ep.dqlBuilder.where(eqValue(property(EduInstitution.address().settlement().fromAlias("e")), addressItems));

                EducationalInstitutionTypeKind type = ep.context.get(EmpEmployeeTrainingItemAddEditUI.SELECTED_EDU_INSTITUTION_TYPE);

                if (type != null)
                    ep.dqlBuilder.where(eqValue(property(EduInstitution.eduInstitutionKind().parent().fromAlias("e")), type));

                EducationalInstitutionTypeKind kind = ep.context.get(EmpEmployeeTrainingItemAddEditUI.SELECTED_EDU_INSTITUTION_KIND);

                if (kind != null)
                    ep.dqlBuilder.where(eqValue(property(EduInstitution.eduInstitutionKind().fromAlias("e")), kind));
            }
        };
    }

    @Bean
    public IDefaultComboDataSourceHandler eduDocumentTypeDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), EducationDocumentType.class);
    }

    @Bean
    public IDefaultComboDataSourceHandler employeeSpecialityDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), EmployeeSpeciality.class);
    }

    @Bean
    public IDefaultComboDataSourceHandler diplomaQualificationsDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), DiplomaQualifications.class);
    }
}
