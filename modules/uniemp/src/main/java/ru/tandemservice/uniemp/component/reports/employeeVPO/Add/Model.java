/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.employeeVPO.Add;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uniemp.entity.catalog.EmployeeVPO1ReportLines;
import ru.tandemservice.uniemp.entity.report.EmployeeVPO1Report;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author dseleznev
 * Created on: 21.01.2010
 */
public class Model
{
    // PUB REPORT DATA

    private EmployeeVPO1Report _report = new EmployeeVPO1Report();
    private Date _formingDate;
    private Date _reportDate;
    private Date _ageDate;

    // PRINT REPORT DATA

    private List<EmployeeVPO1ReportLines> _reportLineList;
    private Map<String, List<Long>> _reportLineCodePostIdMap;

    private List<EmployeePost> _employeePostList;
    private List<EmployeePost> _employeePostCombList;
    private List<EmployeePost> _postList;
    private List<Person> _highEducationPersonList;
    private Map<EmployeePost, Double> _employeePostStaffRateMap;
    private Map<Person, List<String>> _personAcademicStatusCodeMap;
    private Map<Person, List<String>> _personAcademicDegreeCodeMap;
    private List<Person> _retrainingPersonList;

    private List<ReportEducationLineWrapper> _reportEducationLineWrappers;
    private List<ReportEducationLineWrapper> _reportEducationCombinationLineWrappers;
    private List<ReportSrviceLengthLineWrapper> _reportSrviceLengthLineWrappers;
    private List<ReportSexLineWrapper> _reportSexLineWrappers;

    // Getters & Setters

    public List<Person> getHighEducationPersonList()
    {
        return _highEducationPersonList;
    }

    public void setHighEducationPersonList(List<Person> highEducationPersonList)
    {
        _highEducationPersonList = highEducationPersonList;
    }

    public Map<EmployeePost, Double> getEmployeePostStaffRateMap()
    {
        return _employeePostStaffRateMap;
    }

    public void setEmployeePostStaffRateMap(Map<EmployeePost, Double> employeePostStaffRateMap)
    {
        _employeePostStaffRateMap = employeePostStaffRateMap;
    }

    public Map<Person, List<String>> getPersonAcademicStatusCodeMap()
    {
        return _personAcademicStatusCodeMap;
    }

    public void setPersonAcademicStatusCodeMap(Map<Person, List<String>> personAcademicStatusCodeMap)
    {
        _personAcademicStatusCodeMap = personAcademicStatusCodeMap;
    }

    public Map<Person, List<String>> getPersonAcademicDegreeCodeMap()
    {
        return _personAcademicDegreeCodeMap;
    }

    public void setPersonAcademicDegreeCodeMap(Map<Person, List<String>> personAcademicDegreeCodeMap)
    {
        _personAcademicDegreeCodeMap = personAcademicDegreeCodeMap;
    }

    public List<Person> getRetrainingPersonList()
    {
        return _retrainingPersonList;
    }

    public void setRetrainingPersonList(List<Person> retrainingPersonList)
    {
        _retrainingPersonList = retrainingPersonList;
    }

    public List<EmployeePost> getEmployeePostList()
    {
        return _employeePostList;
    }

    public void setEmployeePostList(List<EmployeePost> employeePostList)
    {
        _employeePostList = employeePostList;
    }

    public List<EmployeePost> getEmployeePostCombList()
    {
        return _employeePostCombList;
    }

    public void setEmployeePostCombList(List<EmployeePost> employeePostCombList)
    {
        _employeePostCombList = employeePostCombList;
    }

    public List<EmployeePost> getPostList()
    {
        return _postList;
    }

    public void setPostList(List<EmployeePost> postList)
    {
        _postList = postList;
    }

    public List<ReportSexLineWrapper> getReportSexLineWrappers()
    {
        return _reportSexLineWrappers;
    }

    public void setReportSexLineWrappers(List<ReportSexLineWrapper> reportSexLineWrappers)
    {
        _reportSexLineWrappers = reportSexLineWrappers;
    }

    public List<ReportSrviceLengthLineWrapper> getReportSrviceLengthLineWrappers()
    {
        return _reportSrviceLengthLineWrappers;
    }

    public void setReportSrviceLengthLineWrappers(List<ReportSrviceLengthLineWrapper> reportSrviceLengthLineWrappers)
    {
        _reportSrviceLengthLineWrappers = reportSrviceLengthLineWrappers;
    }

    public List<ReportEducationLineWrapper> getReportEducationCombinationLineWrappers()
    {
        return _reportEducationCombinationLineWrappers;
    }

    public void setReportEducationCombinationLineWrappers(List<ReportEducationLineWrapper> reportEducationCombinationLineWrappers)
    {
        _reportEducationCombinationLineWrappers = reportEducationCombinationLineWrappers;
    }

    public List<ReportEducationLineWrapper> getReportEducationLineWrappers()
    {
        return _reportEducationLineWrappers;
    }

    public void setReportEducationLineWrappers(List<ReportEducationLineWrapper> reportEducationLineWrappers)
    {
        _reportEducationLineWrappers = reportEducationLineWrappers;
    }

    public List<EmployeeVPO1ReportLines> getReportLineList()
    {
        return _reportLineList;
    }

    public void setReportLineList(List<EmployeeVPO1ReportLines> reportLineList)
    {
        _reportLineList = reportLineList;
    }

    public Map<String, List<Long>> getReportLineCodePostIdMap()
    {
        return _reportLineCodePostIdMap;
    }

    public void setReportLineCodePostIdMap(Map<String, List<Long>> reportLineCodePostIdMap)
    {
        _reportLineCodePostIdMap = reportLineCodePostIdMap;
    }

    public Date getAgeDate()
    {
        return _ageDate;
    }

    public void setAgeDate(Date ageDate)
    {
        _ageDate = ageDate;
    }

    public Date getFormingDate()
    {
        return _formingDate;
    }

    public void setFormingDate(Date formingDate)
    {
        this._formingDate = formingDate;
    }

    public Date getReportDate()
    {
        return _reportDate;
    }

    public void setReportDate(Date reportDate)
    {
        this._reportDate = reportDate;
    }

    public EmployeeVPO1Report getReport()
    {
        return _report;
    }

    public void setReport(EmployeeVPO1Report report)
    {
        this._report = report;
    }
}
