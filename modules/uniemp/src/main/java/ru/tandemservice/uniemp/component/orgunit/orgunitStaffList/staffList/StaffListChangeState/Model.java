/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffList.StaffListChangeState;

import java.util.List;

import org.tandemframework.core.component.State;

import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.catalog.StaffListState;
import ru.tandemservice.uniemp.entity.employee.StaffList;

/**
 * @author dseleznev
 * Created on: 20.09.2008
 */
@State(keys = "staffListId", bindings = "staffListId")
public class Model
{
    private Long _staffListId;
    private StaffList _staffList;
    private List<StaffListState> _staffListStatesList;

    public Long getStaffListId()
    {
        return _staffListId;
    }

    public void setStaffListId(Long staffListId)
    {
        this._staffListId = staffListId;
    }

    public StaffList getStaffList()
    {
        return _staffList;
    }

    public void setStaffList(StaffList staffList)
    {
        this._staffList = staffList;
    }

    public List<StaffListState> getStaffListStatesList()
    {
        return _staffListStatesList;
    }

    public void setStaffListStatesList(List<StaffListState> staffListStatesList)
    {
        this._staffListStatesList = staffListStatesList;
    }
    
    public boolean isSettingToActive()
    {
        return null != getStaffList().getStaffListState() && UniempDefines.STAFF_LIST_STATUS_ACTIVE.equals(getStaffList().getStaffListState().getCode());
    }

    public boolean isSettingToArchive()
    {
        return null != getStaffList().getStaffListState() && UniempDefines.STAFF_LIST_STATUS_ARCHIVE.equals(getStaffList().getStaffListState().getCode());
    }
}