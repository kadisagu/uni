/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.bo.EmpEmployeeCertificationItem;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniemp.base.bo.EmpEmployeeCertificationItem.logic.EmpEmployeeCertificationItemDAO;
import ru.tandemservice.uniemp.base.bo.EmpEmployeeCertificationItem.logic.IEmpEmployeeCertificationItemDAO;

/**
 * Create by ashaburov
 * Date 22.03.12
 */
@Configuration
public class EmpEmployeeCertificationItemManager extends BusinessObjectManager
{
    public static EmpEmployeeCertificationItemManager instance()
    {
        return instance(EmpEmployeeCertificationItemManager.class);
    }

    @Bean
    public IEmpEmployeeCertificationItemDAO dao()
    {
        return new EmpEmployeeCertificationItemDAO();
    }
}
