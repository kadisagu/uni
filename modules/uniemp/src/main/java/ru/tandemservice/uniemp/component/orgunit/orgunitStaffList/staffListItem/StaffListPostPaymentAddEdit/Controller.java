/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffListItem.StaffListPostPaymentAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.uniemp.entity.catalog.Payment;

/**
 * @author dseleznev
 * Created on: 07.10.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickApply(IBusinessComponent component)
    {
        ErrorCollector errors = component.getUserContext().getErrorCollector();
        Model model = getModel(component);
        getDao().validate(model, errors);
        if (errors.hasErrors()) return;
        
        getDao().update(getModel(component));
        deactivate(component);
    }

    public void onChangePayment(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (null != model.getStaffListPostPayment().getPayment())
        {
            Payment payment = model.getStaffListPostPayment().getPayment();
            model.getStaffListPostPayment().setAmount(null != payment.getValue() ? payment.getValue() : 0d);
            model.getStaffListPostPayment().setFinancingSource(model.getStaffListPostPayment().getPayment().getFinancingSource());
        }
        else
        {
            model.getStaffListPostPayment().setAmount(0);
            model.getStaffListPostPayment().setFinancingSource(null);
        }
    }
}