/**
 *$Id$
 */
package ru.tandemservice.uniemp.entity.employee;

/**
 * Create by ashaburov
 * Date 16.09.11
 * <p>
 * Общий интерфейс объектов поля выписки О переносе ежегодного отпуска.<p>
 * Интерфейс используется для возможности хранении в поле выписки разных объектов из uniemp.
 */
public interface IChoseRowTransferAnnualHolidayExtract
{
}
