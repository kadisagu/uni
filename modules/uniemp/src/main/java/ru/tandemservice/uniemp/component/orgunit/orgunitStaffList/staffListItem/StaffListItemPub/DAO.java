/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffListItem.StaffListItemPub;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.employee.StaffList;
import ru.tandemservice.uniemp.entity.employee.StaffListItem;
import ru.tandemservice.uniemp.entity.employee.StaffListPostPayment;
import ru.tandemservice.uniemp.util.StaffListPaymentsUtil;

import java.util.List;

/**
 * @author dseleznev
 * Created on: 19.09.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setStaffList(get(StaffList.class, model.getStaffListId()));
        model.setStaffListItem(get(StaffListItem.class, model.getPublisherId()));
    }
    
    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(StaffListPostPayment.ENTITY_CLASS, "pi");
        builder.add(MQExpression.eq("pi", StaffListPostPayment.L_STAFF_LIST_ITEM, model.getStaffListItem()));
        builder.addOrder("pi", StaffListPostPayment.L_PAYMENT + "." + Payment.P_TITLE);
//        builder.addOrder("pi", StaffListPostPayment.financingSource().title());
//        builder.addOrder("pi", StaffListPostPayment.financingSourceItem().title());
        List<StaffListPostPayment> paymentsList = builder.<StaffListPostPayment>getResultList(getSession());
        for(StaffListPostPayment payment : paymentsList) payment.setTargetAllocItem(null);
        UniBaseUtils.createPage(model.getDataSource(), paymentsList);
    }
    
    @Override
    public void deleteStaffListPostPayment(StaffListItem staffListItem, Long paymentId)
    {
        delete(paymentId);
        StaffListPaymentsUtil.recalculateAllPaymentsValueForStaffListItem(staffListItem, getSession());
    }
}