/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.staffList.Add;

import org.tandemframework.rtf.data.IRtfData;

/**
 * @author dseleznev
 * Created on: 05.08.2009
 */
public class RtfTableRowStyle
{
    public static final int TEXT_ALIGN_LEFT = IRtfData.QL;
    public static final int TEXT_ALIGN_CENTER = IRtfData.QC;
    public static final int TEXT_ALIGN_RIGHT = IRtfData.QR;

    private boolean _bold;
    private boolean _italic;
    private boolean _mergeAllRowCells;
    private Integer _fontSize;
    private Integer[] _borderWidths;
    private Integer _textAlign;
    private Integer _rowHeight;
    private Integer _shift;
    private Integer[][] _mergingArr;
    private Integer _mergingAllStartCell;
    private Integer _mergingAllFinalCell;
    private Integer _backGroundColor;
    
    public RtfTableRowStyle()
    {
        setBold(false);
        setItalic(false);
        setMergeAllRowCells(false);
    }
    
    public RtfTableRowStyle(boolean bold, boolean italic, Integer fontSize, int textAlign, boolean mergeAllRowCells, Integer shift, Integer rowHeight)
    {
        setBold(bold);
        setItalic(italic);
        setFontSize(fontSize);
        setTextAlign(textAlign);
        setMergeAllRowCells(mergeAllRowCells);
        setRowHeight(rowHeight);
        setShift(shift);
    }
    
    public RtfTableRowStyle(boolean bold, boolean italic, Integer fontSize, int textAlign, boolean mergeAllRowCells, Integer shift, Integer rowHeight, Integer[][] mergingArr, Integer[] borderWidths)
    {
        this(bold, italic, fontSize, textAlign, mergeAllRowCells, shift, rowHeight);
        setMergingArr(mergingArr);
        setBorderWidths(borderWidths);
    }

    public RtfTableRowStyle(boolean bold, boolean italic, Integer fontSize, int textAlign, boolean mergeAllRowCells, Integer shift, Integer rowHeight, Integer mergingAllStartCell, Integer mergingAllFinalCell, Integer[] borderWidths, Integer backGroundColor)
    {
        this(bold, italic, fontSize, textAlign, mergeAllRowCells, shift, rowHeight);
        setMergingAllStartCell(mergingAllStartCell);
        setMergingAllFinalCell(mergingAllFinalCell);
        setBackGroundColor(backGroundColor);
        setBorderWidths(borderWidths);
    }

    public boolean isBold()
    {
        return _bold;
    }

    public void setBold(boolean bold)
    {
        this._bold = bold;
    }

    public boolean isItalic()
    {
        return _italic;
    }

    public void setItalic(boolean italic)
    {
        this._italic = italic;
    }

    public boolean isMergeAllRowCells()
    {
        return _mergeAllRowCells;
    }

    public void setMergeAllRowCells(boolean mergeAllRowCells)
    {
        this._mergeAllRowCells = mergeAllRowCells;
    }

    public Integer getFontSize()
    {
        return _fontSize;
    }

    public void setFontSize(Integer fontSize)
    {
        this._fontSize = fontSize;
    }

    public Integer[] getBorderWidths()
    {
        return _borderWidths;
    }

    public void setBorderWidths(Integer[] borderWidths)
    {
        this._borderWidths = borderWidths;
    }

    public Integer getTextAlign()
    {
        return _textAlign;
    }

    public void setTextAlign(Integer textAlign)
    {
        this._textAlign = textAlign;
    }

    public Integer getRowHeight()
    {
        return _rowHeight;
    }

    public void setRowHeight(Integer rowHeight)
    {
        this._rowHeight = rowHeight;
    }

    public Integer getShift()
    {
        return _shift;
    }

    public void setShift(Integer shift)
    {
        this._shift = shift;
    }

    public Integer[][] getMergingArr()
    {
        return _mergingArr;
    }

    public void setMergingArr(Integer[][] mergingArr)
    {
        this._mergingArr = mergingArr;
    }

    public Integer getMergingAllStartCell()
    {
        return _mergingAllStartCell;
    }

    public void setMergingAllStartCell(Integer mergingAllStartCell)
    {
        this._mergingAllStartCell = mergingAllStartCell;
    }

    public Integer getMergingAllFinalCell()
    {
        return _mergingAllFinalCell;
    }

    public void setMergingAllFinalCell(Integer mergingAllFinalCell)
    {
        this._mergingAllFinalCell = mergingAllFinalCell;
    }

    public Integer getBackGroundColor()
    {
        return _backGroundColor;
    }

    public void setBackGroundColor(Integer backGroundColor)
    {
        this._backGroundColor = backGroundColor;
    }
}