/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniemp.base.ext.Employee.logic;

import org.tandemframework.core.CoreCollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author dseleznev
 * Created on: 11.01.2009
 */
public class EmploymentDuration
{
    private List<CoreCollectionUtils.Pair<Date, Date>> _periods = new ArrayList<>();

    private Integer _employmentDurationFullDaysAmount;

    private Integer _employmentDurationYearsAmount;

    private Integer _employmentDurationMonthsAmount;

    private Integer _employmentDurationDaysAmount;

    public void addDatePeriod(Date startDate, Date endDate)
    {
        if(startDate.getTime() <= new Date().getTime())
            _periods.add(new CoreCollectionUtils.Pair<>(startDate, endDate));
    }

    public void calculateEmploymentPeriod()
    {
        EmploymentCalculator calc = new EmploymentCalculator();
        calc.calculateFullEmploymentPeriod(_periods);
        _employmentDurationFullDaysAmount = calc.getFullDaysAmount();
        _employmentDurationYearsAmount = calc.getYearsAmount();
        _employmentDurationMonthsAmount = calc.getMonthsAmount();
        _employmentDurationDaysAmount = calc.getDaysAmount();
    }

    public Integer getEmploymentDurationFullDaysAmount()
    {
        return _employmentDurationFullDaysAmount;
    }

    public Integer getEmploymentDurationYearsAmount()
    {
        return _employmentDurationYearsAmount;
    }

    public Integer getEmploymentDurationMonthsAmount()
    {
        return _employmentDurationMonthsAmount;
    }

    public Integer getEmploymentDurationDaysAmount()
    {
        return _employmentDurationDaysAmount;
    }

    public String getEmploymentDurationFullDaysAmountStr()
    {
        return _employmentDurationFullDaysAmount + "         ";
    }

    public String getEmploymentDurationYearsAmountStr()
    {
        return _employmentDurationYearsAmount + "         ";
    }

    public String getEmploymentDurationMonthsAmountStr()
    {
        return _employmentDurationMonthsAmount + "         ";
    }

    public String getEmploymentDurationDaysAmountStr()
    {
        return _employmentDurationDaysAmount + "         ";
    }
}
