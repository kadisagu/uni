/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffListAllocItem.StaffListFakePaymentItemAddEdit;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;
import ru.tandemservice.uniemp.entity.employee.StaffListFakePayment;
import ru.tandemservice.uniemp.util.StaffListPaymentsUtil;

/**
 * @author dseleznev
 * Created on: 11.05.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        if (null != model.getStaffListAllocationItemId())
            model.setStaffListAllocationItem(get(StaffListAllocationItem.class, model.getStaffListAllocationItemId()));

        model.setPaymentsListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(StaffListFakePayment.ENTITY_CLASS, "slfp");
                builder.add(MQExpression.eq("slfp", StaffListFakePayment.L_STAFF_LIST_ITEM, model.getStaffListAllocationItem().getStaffListItem()));
                AbstractExpression expr1 = MQExpression.isNull("slfp", StaffListFakePayment.L_STAFF_LIST_ALLOCATION_ITEM);
                AbstractExpression expr2 = MQExpression.notEq("slfp", StaffListFakePayment.L_STAFF_LIST_ALLOCATION_ITEM, model.getStaffListAllocationItem());
                builder.add(MQExpression.or(expr1, expr2));
                builder.addOrder("slfp", StaffListFakePayment.L_PAYMENT + "." + Payment.P_TITLE);
                return new ListResult<>(builder.getResultList(getSession()));
            }
        });

        model.setFinancingSourcesItemList(getCatalogItemList(FinancingSourceItem.class));
        model.setFinancingSourcesList(getCatalogItemList(FinancingSource.class));
    }

    @Override
    public void update(Model model)
    {
        model.getStaffListFakePayment().setStaffListAllocationItem(model.getStaffListAllocationItem());
        getSession().saveOrUpdate(model.getStaffListFakePayment());
        StaffListPaymentsUtil.recalculateAllPaymentsValue(model.getStaffListAllocationItem(), getSession());
    }
}