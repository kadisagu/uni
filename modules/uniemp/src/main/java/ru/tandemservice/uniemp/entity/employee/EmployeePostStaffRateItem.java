package ru.tandemservice.uniemp.entity.employee;

import java.math.BigDecimal;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.employee.gen.EmployeePostStaffRateItemGen;

/**
 * Ставка сотрудника (связь ставки и сотрудника)
 */
public class EmployeePostStaffRateItem extends EmployeePostStaffRateItemGen
{
    public static String P_STAFF_RATE = "staffRate";

    public EmployeePostStaffRateItem()
    {
        super();
    }

    /**
     * Заполняются соответствующие поля, а размер доли ставки выставляется в 0.
     */
    public EmployeePostStaffRateItem(EmployeePost employeePost, FinancingSource financingSource)
    {
        super();
        setEmployeePost(employeePost);
        setFinancingSource(financingSource);
        setStaffRateInteger(0);
    }


    public void setStaffRate(double value)
    {
        setStaffRateInteger((int) (value * 10000));
    }

    public double getStaffRate()
    {
        BigDecimal x = new java.math.BigDecimal(getStaffRateInteger() * 0.0001);
        x = x.setScale(3, BigDecimal.ROUND_HALF_UP);

        return x.doubleValue();
    }
}