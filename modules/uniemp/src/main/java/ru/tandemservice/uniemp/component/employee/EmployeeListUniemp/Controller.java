/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniemp.component.employee.EmployeeListUniemp;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.RawFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.Add.EmployeeAdd;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.DataEdit.EmployeeDataEdit;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostView.EmployeePostView;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.entity.employee.CombinationPost;

/**
 * @author lefay
 */
public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.setSettings(UniBaseUtils.getDataSettings(component, getSettingsKey()));

        getDao().prepare(model);

        prepareDataSource(component);
    }

    public String getSettingsKey()
    {
        return "EmployeeList.filter";
    }

    @Override
    public void updateListDataSource(IBusinessComponent context)
    {
        getDao().prepareListDataSource(getModel(context), isArchival());
    }

    private void prepareDataSource(IBusinessComponent context)
    {
        if (getModel(context).getDataSource() != null) return;
        DynamicListDataSource<EmployeeListWrapper> dataSource = new DynamicListDataSource<>(context, this);
        dataSource.addColumn(IndicatorColumn.createIconColumn("employee", "Сотрудник"));
        dataSource.addColumn(new SimpleColumn("Табельный номер", EmployeeListWrapper.P_EMPLOYEE_CODE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("ФИО", EmployeeListWrapper.P_FULL_FIO));
        PublisherLinkColumn linkColumn = new PublisherLinkColumn("Должности", EmployeePostListWrapper.P_HTML_FORMATTED_TITLE, "postsList");
        linkColumn.setOrderable(false);
        linkColumn.setFormatter(RawFormatter.INSTANCE);
        linkColumn.setResolver(new DefaultPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                Object o = getDao().get(entity.getId());

                if (o instanceof EmployeePost)
                    return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, entity.getId());
                else if (o instanceof CombinationPost)
                    return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, ((CombinationPost) o).getEmployeePost().getId()).add("selectedTab", "employeePostTab").add("selectedDataTab", "Combination");
                else
                    return null;

            }

            @Override
            public String getComponentName(IEntity entity)
            {
                Object o = getDao().get(entity.getId());

                if (o instanceof CombinationPost)
                    return EmployeePostView.class.getSimpleName();
                else
                    return null;
            }
        });
        dataSource.addColumn(linkColumn);
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить сотрудника №{0} — «{1}»?", EmployeeListWrapper.P_EMPLOYEE_CODE, EmployeeListWrapper.P_FULL_FIO).setDisabledProperty(Model.EMPLOYEE_PROPERTY_CANT_DELETE).setPermissionKey("deleteEmployeeFromList"));
        getModel(context).setDataSource(dataSource);
    }

    public void onClickSearch(IBusinessComponent context)
    {
        getModel(context).getDataSource().showFirstPage();
        getModel(context).getDataSource().refresh();
        DataSettingsFacade.saveSettings(getModel(context).getSettings());
    }

    public void onClickClear(IBusinessComponent context)
    {
        getModel(context).getSettings().clear();
        onClickSearch(context);
    }

    public void onClickAdd(IBusinessComponent context)
    {
        context.createDefaultChildRegion(new ComponentActivator(EmployeeAdd.class.getSimpleName()));
    }

    public void onClickEdit(IBusinessComponent context)
    {
        context.createDefaultChildRegion(new ComponentActivator(EmployeeDataEdit.class.getPackage().getName(), new ParametersMap()
                .add("employeeId", context.getListenerParameter())
        ));
    }

    public void onClickDelete(IBusinessComponent context)
    {
        getDao().deleteRow(context);
    }

    protected boolean isArchival()
    {
        return false;
    }
}