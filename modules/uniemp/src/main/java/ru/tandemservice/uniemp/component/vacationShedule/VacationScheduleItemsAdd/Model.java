/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.component.vacationShedule.VacationScheduleItemsAdd;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import ru.tandemservice.uniemp.entity.employee.VacationSchedule;

/**
 * @author dseleznev
 * Created on: 18.01.2011
 */
@Input(keys = "vacationScheduleId", bindings = "vacationScheduleId")
public class Model
{
    public static final Long EMPLOYEE_STATUS_ACTIVE = 0L;
    public static final Long EMPLOYEE_STATUS_NON_ACTIVE = 1L;
    public static final List<IEntity> EMPLOYEE_STATUS_OPTION_LIST = Arrays.asList(new IEntity[] { 
            new IdentifiableWrapper<>(EMPLOYEE_STATUS_ACTIVE, "активные сотрудники"),
            new IdentifiableWrapper<>(EMPLOYEE_STATUS_NON_ACTIVE, "неактивные сотрудники")
    });

    private Long vacationScheduleId;
    private VacationSchedule vacationSchedule;

    private Date _planDate;

    private String _lastNameFilter;
    private Integer _holidayDurationFilter;
    private IdentifiableWrapper<IEntity> _acitvePostStatusFilter;
    private EmployeePostStatus _postStatusFilter;
    private PostType _postTypeFilter;
    private PostBoundedWithQGandQL _postFilter;

    private ISelectModel _postStatusesListModel;
    private ISelectModel _postTypesListModel;
    private ISelectModel _postsListModel;

    private DynamicListDataSource<EmployeePost> _dataSource;

    public Long getVacationScheduleId()
    {
        return vacationScheduleId;
    }

    public void setVacationScheduleId(Long vacationScheduleId)
    {
        this.vacationScheduleId = vacationScheduleId;
    }

    public VacationSchedule getVacationSchedule()
    {
        return vacationSchedule;
    }

    public void setVacationSchedule(VacationSchedule vacationSchedule)
    {
        this.vacationSchedule = vacationSchedule;
    }

    public Date getPlanDate()
    {
        return _planDate;
    }

    public void setPlanDate(Date planDate)
    {
        this._planDate = planDate;
    }

    public String getLastNameFilter()
    {
        return _lastNameFilter;
    }

    public void setLastNameFilter(String lastNameFilter)
    {
        this._lastNameFilter = lastNameFilter;
    }

    public Integer getHolidayDurationFilter()
    {
        return _holidayDurationFilter;
    }

    public void setHolidayDurationFilter(Integer holidayDurationFilter)
    {
        this._holidayDurationFilter = holidayDurationFilter;
    }

    public IdentifiableWrapper<IEntity> getAcitvePostStatusFilter()
    {
        return _acitvePostStatusFilter;
    }

    public void setAcitvePostStatusFilter(IdentifiableWrapper<IEntity> acitvePostStatusFilter)
    {
        this._acitvePostStatusFilter = acitvePostStatusFilter;
    }

    public EmployeePostStatus getPostStatusFilter()
    {
        return _postStatusFilter;
    }

    public void setPostStatusFilter(EmployeePostStatus postStatusFilter)
    {
        this._postStatusFilter = postStatusFilter;
    }

    public PostType getPostTypeFilter()
    {
        return _postTypeFilter;
    }

    public void setPostTypeFilter(PostType postTypeFilter)
    {
        this._postTypeFilter = postTypeFilter;
    }

    public PostBoundedWithQGandQL getPostFilter()
    {
        return _postFilter;
    }

    public void setPostFilter(PostBoundedWithQGandQL postFilter)
    {
        this._postFilter = postFilter;
    }

    public ISelectModel getPostStatusesListModel()
    {
        return _postStatusesListModel;
    }

    public void setPostStatusesListModel(ISelectModel postStatusesListModel)
    {
        this._postStatusesListModel = postStatusesListModel;
    }

    public ISelectModel getPostTypesListModel()
    {
        return _postTypesListModel;
    }

    public void setPostTypesListModel(ISelectModel postTypesListModel)
    {
        this._postTypesListModel = postTypesListModel;
    }

    public ISelectModel getPostsListModel()
    {
        return _postsListModel;
    }

    public void setPostsListModel(ISelectModel postsListModel)
    {
        this._postsListModel = postsListModel;
    }

    public DynamicListDataSource<EmployeePost> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<EmployeePost> dataSource)
    {
        this._dataSource = dataSource;
    }

    public List<IEntity> getAcitvePostStatusList()
    {
        return EMPLOYEE_STATUS_OPTION_LIST;
    }
}