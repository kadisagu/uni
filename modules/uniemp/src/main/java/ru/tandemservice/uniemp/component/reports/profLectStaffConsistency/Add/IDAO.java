/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.profLectStaffConsistency.Add;

import java.util.List;
import java.util.Set;

import org.tandemframework.core.entity.IEntity;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author dseleznev
 * Created on: 13.04.2009
 */
public interface IDAO extends IUniDao<Model>
{
    @SuppressWarnings({"unchecked"})
    List<IEntity> getValues(Class clazz, Set idsList);

    List<OrgUnit> getOrgUnitsList(OrgUnitType orgUnitType, String filter);

    Integer preparePrintReport(Model model);
}