/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.settings.StaffRateStepSetting;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.info.ErrorCollector;

/**
 * @author dseleznev
 * Created on: 07.10.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickApply(IBusinessComponent component)
    {
        ErrorCollector errors = component.getUserContext().getErrorCollector();
        Model model = getModel(component);
        getDao().validate(model, errors);
        if (errors.hasErrors()) return;
        
        getDao().update(getModel(component));
    }

}