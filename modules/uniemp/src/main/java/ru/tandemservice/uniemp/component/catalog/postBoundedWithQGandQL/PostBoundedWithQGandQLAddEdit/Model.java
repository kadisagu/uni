/**
 *$Id:$
 */
package ru.tandemservice.uniemp.component.catalog.postBoundedWithQGandQL.PostBoundedWithQGandQLAddEdit;

import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import ru.tandemservice.uniemp.entity.catalog.ServiceLengthType;

import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 17.09.12
 */
public class Model extends org.tandemframework.shared.employeebase.component.catalog.postBoundedWithQGandQL.PostBoundedWithQGandQLAddEdit.Model
{
    private IMultiSelectModel _serviceLengthTypeModel;
    private List<ServiceLengthType> _serviceLengthTypeList;

    // Getters & Setters

    public IMultiSelectModel getServiceLengthTypeModel()
    {
        return _serviceLengthTypeModel;
    }

    public void setServiceLengthTypeModel(IMultiSelectModel serviceLengthTypeModel)
    {
        _serviceLengthTypeModel = serviceLengthTypeModel;
    }

    public List<ServiceLengthType> getServiceLengthTypeList()
    {
        return _serviceLengthTypeList;
    }

    public void setServiceLengthTypeList(List<ServiceLengthType> serviceLengthTypeList)
    {
        _serviceLengthTypeList = serviceLengthTypeList;
    }
}
