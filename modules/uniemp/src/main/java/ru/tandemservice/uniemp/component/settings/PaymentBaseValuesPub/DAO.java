/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniemp.component.settings.PaymentBaseValuesPub;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.employee.PaymentToPostBaseValue;

/**
 * @author dseleznev
 * Created on: 29.09.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setPayment(get(Payment.class, model.getPaymentId()));
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(PaymentToPostBaseValue.ENTITY_CLASS, "r");
        builder.add(MQExpression.eq("r", PaymentToPostBaseValue.L_PAYMENT, model.getPayment()));
        new OrderDescriptionRegistry("r").applyOrder(builder, model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }
}