/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.StaffListRegistryChangeState;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;

import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.catalog.StaffListState;
import ru.tandemservice.uniemp.entity.employee.StaffList;

/**
 * @author dseleznev
 * Created on: 20.09.2008
 */
@Input(@Bind(key = "staffListIdList", binding = "staffListIdList"))
public class Model
{
    private StaffListState _choseStaffListState;                        //выбранное состояние
    private StaffListState _staffListState;                             //исходное состояние
    private List<StaffList> _staffListList = new ArrayList<>();//список версий ШР, у которых меняем статус
    private List<Long> _staffListIdList;                                //список идентификаторов версий ШР, пришедших с предыдущей формы
    private List<StaffListState> _staffListStatesList;                  //список статусов, которые можно выбирать
    private Date _acceptionDate = new Date();                           //дата согласования
    private Date _archivingDate = new Date();                           //дата архивации

    //Getters & Setters

    public Date getAcceptionDate()
    {
        return _acceptionDate;
    }

    public void setAcceptionDate(Date acceptionDate)
    {
        _acceptionDate = acceptionDate;
    }

    public Date getArchivingDate()
    {
        return _archivingDate;
    }

    public void setArchivingDate(Date archivingDate)
    {
        _archivingDate = archivingDate;
    }

    public List<Long> getStaffListIdList()
    {
        return _staffListIdList;
    }

    public void setStaffListIdList(List<Long> staffListIdList)
    {
        _staffListIdList = staffListIdList;
    }

    public StaffListState getChoseStaffListState()
    {
        return _choseStaffListState;
    }

    public void setChoseStaffListState(StaffListState choseStaffListState)
    {
        _choseStaffListState = choseStaffListState;
    }

    public List<StaffList> getStaffListList()
    {
        return _staffListList;
    }

    public void setStaffListList(List<StaffList> staffListList)
    {
        _staffListList = staffListList;
    }

    public StaffListState getStaffListState()
    {
        return _staffListState;
    }

    public void setStaffListState(StaffListState staffListState)
    {
        _staffListState = staffListState;
    }

    public List<StaffListState> getStaffListStatesList()
    {
        return _staffListStatesList;
    }

    public void setStaffListStatesList(List<StaffListState> staffListStatesList)
    {
        this._staffListStatesList = staffListStatesList;
    }
    
    public boolean isSettingToActive()
    {
        return null != getChoseStaffListState() && UniempDefines.STAFF_LIST_STATUS_ACTIVE.equals(getChoseStaffListState().getCode());
    }

    public boolean isSettingToArchive()
    {
        return null != getChoseStaffListState() && UniempDefines.STAFF_LIST_STATUS_ARCHIVE.equals(getChoseStaffListState().getCode());
    }
}