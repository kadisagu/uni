/**
 *$Id$
 */
package ru.tandemservice.uniemp.component.employee.EmployeeCombinationPostAddEdit;

import org.tandemframework.core.info.InfoCollector;
import ru.tandemservice.uniemp.dao.IUniempDAO;

/**
 * Create by ashaburov
 * Date 08.12.11
 */
public interface IDAO extends IUniempDAO<Model>
{
    public void prepareStaffRateDataSource(Model model);

    public void validateCombinationPeriods(Model model, InfoCollector infoCollector);
}
