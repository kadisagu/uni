/**
 *$Id$
 */
package ru.tandemservice.uniemp.component.employee.EmployeeActingItemAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.info.ErrorCollector;

/**
 * Create by ashaburov
 * Date 03.11.11
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onChangeActingEmployeePost(IBusinessComponent component)
    {
        Model model = getModel(component);

        if (model.getEntity().getMissingEmployeePost() == null)
            return;

        model.getEntity().setOrgUnit(model.getEntity().getMissingEmployeePost().getOrgUnit());
        model.getEntity().setPostBoundedWithQGandQL(model.getEntity().getMissingEmployeePost().getPostRelation().getPostBoundedWithQGandQL());
    }

    public void onClickApply(IBusinessComponent component)
    {
        ErrorCollector errorCollector = component.getUserContext().getErrorCollector();
        Model model = getModel(component);

        getDao().validate(model, errorCollector);

        if (errorCollector.hasErrors())
            return;

        getDao().update(model);
        deactivate(component);
    }
}
