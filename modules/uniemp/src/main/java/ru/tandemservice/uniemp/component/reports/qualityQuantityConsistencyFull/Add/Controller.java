/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.qualityQuantityConsistencyFull.Add;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.uni.IUniComponents;

import java.util.Date;

/**
 * @author dseleznev
 * Created on: 17.04.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getModel(component).setReportDate(new Date());
    }
 
    public void onClickApply(IBusinessComponent component)
    {
        // переходим на универсальный компонент рендера отчета
        activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new ParametersMap()
                .add("id", getDao().preparePrintReport(getModel(component)))
        ));
    }
}