/**
 *$Id:$
 */
package ru.tandemservice.uniemp.component.catalog.postBoundedWithQGandQL.PostBoundedWithQGandQLItemPub;

import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.employeebase.component.catalog.postBoundedWithQGandQL.PostBoundedWithQGandQLItemPub.Model;
import ru.tandemservice.uniemp.entity.catalog.ServiceLengthType;
import ru.tandemservice.uniemp.entity.employee.PostToServiceLengthTypeRelation;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 17.09.12
 */
public class DAO extends org.tandemframework.shared.employeebase.component.catalog.postBoundedWithQGandQL.PostBoundedWithQGandQLItemPub.DAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        List<PostToServiceLengthTypeRelation> relationList = new DQLSelectBuilder().fromEntity(PostToServiceLengthTypeRelation.class, "b").column("b")
                .where(eq(property(PostToServiceLengthTypeRelation.postBoundedWithQGandQL().fromAlias("b")), value(model.getCatalogItem())))
                .createStatement(getSession()).list();

        ru.tandemservice.uniemp.component.catalog.postBoundedWithQGandQL.PostBoundedWithQGandQLItemPub.Model currentModel = (ru.tandemservice.uniemp.component.catalog.postBoundedWithQGandQL.PostBoundedWithQGandQLItemPub.Model) model;

        currentModel.setServiceLengthTypeList(CommonBaseUtil.<ServiceLengthType>getPropertiesList(relationList, PostToServiceLengthTypeRelation.serviceLengthType().s()));
    }
}
