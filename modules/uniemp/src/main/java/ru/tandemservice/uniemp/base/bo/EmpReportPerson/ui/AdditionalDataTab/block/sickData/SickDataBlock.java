/* $Id$ */
package ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.AdditionalDataTab.block.sickData;

import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.datasource.IUIDataSource;

import ru.tandemservice.uniemp.entity.catalog.SickListBasics;

/**
 * @author Vasily Zhukov
 * @since 05.08.2011
 */
public class SickDataBlock
{
    public static final String SICK_BASIC_DS = "sickBasicDS";

    public static void onBeforeDataSourceFetch(IUIDataSource dataSource, SickDataParam param)
    {
    }

    public static IDefaultComboDataSourceHandler createSickBasicDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, SickListBasics.class);
    }
}
