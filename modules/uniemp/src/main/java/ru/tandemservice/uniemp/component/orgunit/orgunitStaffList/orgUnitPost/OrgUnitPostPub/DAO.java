/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.orgUnitPost.OrgUnitPostPub;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation;

/**
 * @author dseleznev
 * Created on: 19.09.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{

    @Override
    public void prepare(Model model)
    {
        model.setOrgUnitPostRel(get(OrgUnitPostRelation.class, model.getPublisherId()));
        //model.setOrgUnit((OrgUnit)get(model.getOrgUnitId())); // ???
        model.setOrgUnit(model.getOrgUnitPostRel().getOrgUnit());
    }

}