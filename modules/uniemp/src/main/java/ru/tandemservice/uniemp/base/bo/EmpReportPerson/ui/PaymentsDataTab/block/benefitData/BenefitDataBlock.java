/* $Id$ */
package ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.PaymentsDataTab.block.benefitData;

import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.datasource.IUIDataSource;

import ru.tandemservice.uniemp.entity.catalog.EncouragementType;

/**
 * @author Vasily Zhukov
 * @since 05.08.2011
 */
public class BenefitDataBlock
{
    public static final String ENCOURAGEMENT_TYPE_DS = "emplEncrgmntTypeDS";

    public static void onBeforeDataSourceFetch(IUIDataSource dataSource, BenefitDataParam param)
    {
    }

    public static IDefaultComboDataSourceHandler createEncouragementTypeDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, EncouragementType.class);
    }
}
