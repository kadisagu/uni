/**
 *$Id:$
 */
package ru.tandemservice.uniemp.events;

import org.hibernate.Session;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.transaction.sync.ParamTransactionCompleteListener;
import ru.tandemservice.uniemp.entity.catalog.ServiceLengthType;
import ru.tandemservice.uniemp.entity.employee.EmpHistoryToServiceLengthTypeRelation;
import ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemInner;
import ru.tandemservice.uniemp.entity.employee.PostToServiceLengthTypeRelation;

import java.util.Collection;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Перехватывается создание новых Элементов истории должностей сотрудника.<p/>
 * Если элемент создается не с формы ред.\доб., (об этом узнаем по отсутствию созданных для  него релейшенов на Виды стажа и метка Не учитывать виды стажа в false),
 * то создаем релейшены на Виды стажа в листенере, основываясь на Видах стажа для Должности по ПКГ и КУ.
 *
 * @author Alexander Shaburov
 * @since 18.09.12
 */
public class EmploymentHistorySaveListener extends ParamTransactionCompleteListener<Boolean>
{
    public void init()
    {
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, EmploymentHistoryItemInner.class, this);
    }

    @Override
    public Collection<Long> getIds(DSetEvent event)
    {
        return new DQLSelectBuilder().fromDataSource(event.getMultitude().getSource(), "i").column("i.id")
                .where(eq(property(EmploymentHistoryItemInner.emptyServiceLengthType().fromAlias("i")), value(false)))
                .createStatement(event.getContext()).list();
    }

    @Override
    public Boolean beforeCompletion(final Session session, final Collection<Long> params)
    {
        BatchUtils.execute(params, 128, new BatchUtils.Action<Long>()
        {
            @Override
            public void execute(Collection<Long> elements)
            {
                for (Long id : elements)
                {
                    List<Long> relationList = new DQLSelectBuilder().fromEntity(EmpHistoryToServiceLengthTypeRelation.class, "b").column("b.id")
                            .where(eq(property(EmpHistoryToServiceLengthTypeRelation.employmentHistoryItemBase().id().fromAlias("b")), value(id)))
                            .createStatement(session).list();

                    if (relationList.isEmpty())
                    {
                        EmploymentHistoryItemInner item = (EmploymentHistoryItemInner) session.get(EmploymentHistoryItemInner.class, id);

                        List<ServiceLengthType> serviceLengthTypeList = new DQLSelectBuilder().fromEntity(PostToServiceLengthTypeRelation.class, "b").column(property(PostToServiceLengthTypeRelation.serviceLengthType().fromAlias("b")))
                                .where(eq(property(PostToServiceLengthTypeRelation.postBoundedWithQGandQL().fromAlias("b")), value(item.getPostBoundedWithQGandQL())))
                                .createStatement(session).list();

                        if (serviceLengthTypeList.isEmpty())
                        {
                            item.setEmptyServiceLengthType(true);
                            session.saveOrUpdate(item);
                        }
                        else
                        {
                            for (ServiceLengthType serviceLengthType : serviceLengthTypeList)
                            {
                                EmpHistoryToServiceLengthTypeRelation relation = new EmpHistoryToServiceLengthTypeRelation();
                                relation.setEmploymentHistoryItemBase(item);
                                relation.setServiceLengthType(serviceLengthType);

                                session.save(relation);
                            }
                        }

                        session.flush();
                    }
                }
            }
        });

        return super.beforeCompletion(session, params);
    }
}
