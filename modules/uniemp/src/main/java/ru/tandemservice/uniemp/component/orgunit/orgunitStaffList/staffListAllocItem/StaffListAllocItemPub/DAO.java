/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffListAllocItem.StaffListAllocItemPub;

import java.util.List;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;
import ru.tandemservice.uniemp.entity.employee.StaffListFakePayment;
import ru.tandemservice.uniemp.entity.employee.StaffListPaymentBase;
import ru.tandemservice.uniemp.entity.employee.StaffListPostPayment;
import ru.tandemservice.uniemp.util.StaffListPaymentsUtil;

/**
 * @author dseleznev
 * Created on: 08.04.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setStaffListAllocationItem(get(StaffListAllocationItem.class, model.getPublisherId()));
        
        MQBuilder builder = new MQBuilder(StaffListFakePayment.ENTITY_CLASS, "slfp");
        builder.add(MQExpression.eq("slfp", StaffListFakePayment.L_STAFF_LIST_ITEM, model.getStaffListAllocationItem().getStaffListItem()));
        if(builder.getResultCount(getSession()) > 0) model.setStaffListItemHasFakePayments(true);
        else model.setStaffListItemHasFakePayments(false);
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder fakeBuilder = new MQBuilder(StaffListFakePayment.ENTITY_CLASS, "slfp", new String[] {StaffListFakePayment.P_ID});
        fakeBuilder.add(MQExpression.eq("slfp", StaffListFakePayment.L_STAFF_LIST_ITEM, model.getStaffListAllocationItem().getStaffListItem()));
        AbstractExpression expr1 = MQExpression.notEq("slfp", StaffListFakePayment.L_STAFF_LIST_ALLOCATION_ITEM, model.getStaffListAllocationItem());
        AbstractExpression expr2 = MQExpression.isNull("slfp", StaffListFakePayment.L_STAFF_LIST_ALLOCATION_ITEM);
        fakeBuilder.add(MQExpression.or(expr1, expr2));
        
        MQBuilder builder = new MQBuilder(StaffListPaymentBase.ENTITY_CLASS, "pi");
        builder.add(MQExpression.notIn("pi", StaffListPaymentBase.P_ID, fakeBuilder));
        builder.add(MQExpression.eq("pi", StaffListPaymentBase.L_STAFF_LIST_ITEM, model.getStaffListAllocationItem().getStaffListItem()));
        builder.addOrder("pi", StaffListPaymentBase.L_PAYMENT + "." + Payment.P_TITLE);
        
        List<StaffListPaymentBase> paymentsList = builder.getResultList(getSession());
        for(StaffListPaymentBase payment : paymentsList)
        {
            if(payment instanceof StaffListPostPayment) 
                payment.setTargetAllocItem(model.getStaffListAllocationItem());
            payment.setConsiderAsAnAllocContext(true);
        }
        
        UniBaseUtils.createPage(model.getDataSource(), paymentsList);
    }

    @Override
    public void deleteStaffListPaymentItem(Long id, StaffListAllocationItem allocationItem)
    {
        StaffListPaymentBase payment = get(id);
        if(payment instanceof StaffListFakePayment)
        {
            ((StaffListFakePayment)payment).setStaffListAllocationItem(null);
            update(payment);
        }
        else
            delete(id);

        StaffListPaymentsUtil.recalculateAllPaymentsValue(allocationItem, getSession());
    }
}