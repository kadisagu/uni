/**
 *$Id$
 */
package ru.tandemservice.uniemp.component.settings.PaymentOnFOTToPaymentsSettings.PaymentOnFOTToPaymentsSettingsPub;

import ru.tandemservice.uni.component.settings.abstractRelation.AbstractRelationList.IAbstractRelationListDAO;
import ru.tandemservice.uniemp.entity.catalog.Payment;

/**
 * Create by: ashaburov
 * Date: 26.04.11
 */
public interface IDAO extends IAbstractRelationListDAO<Model>
{
    public void updateRelations(Payment payment);
}
