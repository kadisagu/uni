/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniemp.component.settings.StaffListPaymentsList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import org.tandemframework.shared.commonbase.base.util.CommonBaseDefine;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.entity.catalog.Payment;

/**
 * @author dseleznev
 * Created on: 16.03.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<Payment> dataSource = UniBaseUtils.createDataSource(component, getDao());

        dataSource.addColumn(new SimpleColumn("Название", Payment.P_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Тип выплаты", Payment.TYPE_KEY).setClickable(false).setOrderable(false));

        CheckboxColumn col1 = new CheckboxColumn("takeIntoAccountPaymentColumn", "Учитывать в штатном расписании", false);
        col1.setSelectedObjects(model.getSelectedTakeIntoAccountPayments());
        col1.setDisabledProperty(Model.P_PAYMENT_SETTINGS_DISABLED);
        col1.setClickable(false).setOrderable(false);
        dataSource.addColumn(col1);

        CheckboxColumn col2 = new CheckboxColumn("individualColumnsColumn", "Выводить отдельным столбцом", false);
        col2.setSelectedObjects(model.getSelectedIndividualColumnPayments());
        col2.setDisabledProperty(Model.P_PAYMENT_SETTINGS_DISABLED);
        col2.setClickable(false).setOrderable(false);
        dataSource.addColumn(col2);

        dataSource.addColumn(new ActionColumn("Вверх", CommonBaseDefine.ICO_UP, "onClickPaymentUp"));
        dataSource.addColumn(new ActionColumn("Вниз", CommonBaseDefine.ICO_DOWN, "onClickPaymentDown"));

        model.setDataSource(dataSource);
    }

    public void onClickApply(IBusinessComponent component)
    {
        getDao().update(getModel(component));
    }

    public void onClickPaymentUp(IBusinessComponent component)
    {
        getDao().updatePriority(getModel(component), (Long)component.getListenerParameter(), true);
    }

    public void onClickPaymentDown(IBusinessComponent component)
    {
        getDao().updatePriority(getModel(component), (Long)component.getListenerParameter(), false);
    }
}