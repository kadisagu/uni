/**
 *$Id$
 */
package ru.tandemservice.uniemp.component.employee.employmentContracts.EmploymentContractsTerminationNoticePrintForm;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.column.IValueMapHolder;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.dao.UniempDAO;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;
import java.util.Collections;

/**
 * Create by ashaburov
 * Date 29.07.11
 */
public class DAO extends UniempDAO<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        if (model.getMassPrint())
        {
            List<EmployeeLabourContract> contractList = new ArrayList<>();
            for (Long id : model.getContractIdList())
                contractList.add(get(EmployeeLabourContract.class, id));

            model.setEmployeeLabourContractList(contractList);
        }
        else
            model.setEmployeeLabourContract(get(EmployeeLabourContract.class, model.getContractId()));
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        UniBaseUtils.createPage(model.getContractListDataSourse(), model.getEmployeeLabourContractList());
    }

    @SuppressWarnings("unchecked")
    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        if (model.getMassPrint())
        {
            final IValueMapHolder<Object> numberHolder = (IValueMapHolder) model.getContractListDataSourse().getColumn("number");
            final Map<Long, Object> numberHolderMap = (null == numberHolder ? Collections.<Long, Object>emptyMap() : numberHolder.getValueMap());

            for (IEntity firstEntity : model.getEmployeeLabourContractList())
                for (IEntity secondEntity : model.getEmployeeLabourContractList())
                    if (!firstEntity.equals(secondEntity))
                        if (numberHolderMap.get(firstEntity.getId()).equals(numberHolderMap.get(secondEntity.getId())))
                            errors.add("Номера уведомлений не должны совпадать.", "number_id_" + firstEntity.getId());

        }
    }
}
