/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniemp.base.ext.Employee.ui.View;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.block.BlockListExtension;
import org.tandemframework.caf.ui.config.button.ButtonListExtension;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.View.EmployeeView;

/**
 * @author Vasily Zhukov
 * @since 30.03.2012
 */
@Configuration
public class EmployeeViewExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "uniemp" + EmployeeViewExtUI.class.getSimpleName();

    @Autowired
    private EmployeeView _employeeView;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_employeeView.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, EmployeeViewExtUI.class))
                .create();
    }

    @Bean
    public TabPanelExtension tabPanelExtension()
    {
        return tabPanelExtensionBuilder(_employeeView.employeeTabPanelExtPoint())
                .addAllAfter(EmployeeView.EMPLOYEE_DATA_TAB)
                .addTab(htmlTab("postsList", "EmployeePostsTab").permissionKey("viewTabEmployeePostPostsList_employee"))
                .addTab(htmlTab("PPS", "EmployeePPSTab").permissionKey("viewTabPPS_employee"))
                .addTab(htmlTab("SickLists", "EmployeeSickListTab").permissionKey("viewTabSickList_employee"))
                .addTab(htmlTab("EmploymentHistory", "EmploymentHistoryTab").permissionKey("viewTabEmploymentHistory_employee"))
                .addTab(htmlTab("Encouragements", "EmployeeEncouragementsTab").permissionKey("viewTabEmployeeEncouragements_employee"))
                .addTab(htmlTab("ServiceLenght", "EmployeeServiceLengthTab").permissionKey("viewTabEmployeeServiceLength_employee"))
                .addTab(htmlTab("Training", "EmployeeTrainingTab").permissionKey("viewTabEmployeeTraining_employee"))
                .create();
    }
    
    @Bean 
    public BlockListExtension blockListExtension()
    {
        return blockListExtensionBuilder(_employeeView.employeePubBlockListExtPoint())
                .create();
    }
    
    @Bean
    public ButtonListExtension buttonListExtension()
    {
        return buttonListExtensionBuilder(_employeeView.employeePubButtonListExtPoint())
                .overwriteButton(submitButton(EmployeeView.EMPLOYEE_POST_ADD).visible(false))
                .create();
    }
}
