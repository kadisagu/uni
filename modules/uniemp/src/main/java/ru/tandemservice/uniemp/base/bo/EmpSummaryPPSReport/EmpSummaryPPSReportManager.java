/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.bo.EmpSummaryPPSReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniemp.base.bo.EmpSummaryPPSReport.logic.EmpSummaryPPSReportDao;
import ru.tandemservice.uniemp.base.bo.EmpSummaryPPSReport.logic.IEmpSummaryPPSReportDao;

/**
 * @author Alexander Shaburov
 * @since 20.11.12
 */
@Configuration
public class EmpSummaryPPSReportManager extends BusinessObjectManager
{
    public static EmpSummaryPPSReportManager instance()
    {
        return instance(EmpSummaryPPSReportManager.class);
    }

    @Bean
    public IEmpSummaryPPSReportDao empSummaryPPSReportDao()
    {
        return new EmpSummaryPPSReportDao();
    }
}
