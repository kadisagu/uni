/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.StaffListRegistry;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.Post;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.entity.catalog.StaffListState;
import ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation;
import ru.tandemservice.uniemp.entity.employee.StaffList;
import ru.tandemservice.uniemp.entity.employee.StaffListItem;

import java.util.List;

/**
 * @author dseleznev
 * Created on: 20.09.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setOrgUnitTypesList(OrgUnitManager.instance().dao().getListOrgUnitTypeWithEmployee());

        model.setOrgUnitList(new FullCheckSelectModel(OrgUnit.P_FULL_TITLE)
        {
            @Override
            public ListResult findValues(final String filter)
            {
                final Object orgUnitType = model.getSettings().get("orgUnitType");
                final MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "ou");
                builder.add(MQExpression.eq("ou", OrgUnit.P_ARCHIVAL, Boolean.FALSE));
                if(null != orgUnitType)
                {
                    builder.add(MQExpression.eq("ou", OrgUnit.L_ORG_UNIT_TYPE, orgUnitType));
                }
                builder.add(MQExpression.like("ou", OrgUnit.P_TITLE, CoreStringUtils.escapeLike(filter)));
                builder.addOrder("ou", OrgUnit.P_TITLE);
                return new ListResult<>(builder.getResultList(getSession()));
            }
        });

        model.setEmployeeTypesList(HierarchyUtil.listHierarchyNodesWithParents(getList(EmployeeType.class), true));
        model.setPostsAutocompleteModel(new BaseSingleSelectModel()
        {
            @Override
            public ListResult findValues(final String filter)
            {
                final IDataSettings settings = model.getSettings();
                final Object employeeType = settings.get("employeeType");

                final MQBuilder builder = new MQBuilder(Post.ENTITY_CLASS, "p");
                if(null != employeeType)
                {
                    builder.add(MQExpression.eq("p", Post.L_EMPLOYEE_TYPE, employeeType));
                }
                builder.add(MQExpression.like("p", Post.P_TITLE, CoreStringUtils.escapeLike(filter)));
                builder.addOrder("p", Post.P_TITLE);
                return new ListResult<>(builder.getResultList(getSession()));
            }

            @Override
            public Object getValue(final Object primaryKey)
            {
                final IEntity entity = get((Long)primaryKey);
                if (findValues("").getObjects().contains(entity))
                {
                    return entity;
                }
                else
                {
                    return null;
                }
            }
        });
        model.setStateModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(StaffListState.ENTITY_CLASS, "b");
                builder.add(MQExpression.like("b", StaffListState.P_TITLE, CoreStringUtils.escapeLike(filter)));

                return new ListResult<>(builder.<Object>getResultList(getSession()));
            }
        });
    }

    @Override
    public void prepareListDataSource(final Model model)
    {
        final IDataSettings settings = model.getSettings();
        final Object orgUnit = settings.get("orgUnit");
        final Object orgUnitType = settings.get("orgUnitType");
        final Object employeeType = settings.get("employeeType");
        final Object showChilds = settings.get("showChilds");
        final Object post = settings.get("post");
        final List<StaffListState> states = settings.get("states");
        final Object formativeDateFrom = settings.get("formativeDateFrom");
        final Object formativeDateTo = settings.get("formativeDateTo");
        final Object acceptionDateFrom = settings.get("acceptionDateFrom");
        final Object acceptionDateTo = settings.get("acceptionDateTo");
        final Object archivingDateFrom = settings.get("archivingDateFrom");
        final Object archivingDateTo = settings.get("archivingDateTo");

        final MQBuilder builder = new MQBuilder(StaffListItem.ENTITY_CLASS, "sli");
//        builder.add(MQExpression.eq("sli", StaffListItem.L_STAFF_LIST + "." + StaffList.L_STAFF_LIST_STATE, getCatalogItem(StaffListState.class, UniempDefines.STAFF_LIST_STATUS_ACTIVE)));

        if((null != orgUnit) && (null != showChilds) && (Boolean)showChilds)
        {
            final List<OrgUnit> childs = OrgUnitManager.instance().dao().getChildOrgUnits((OrgUnit)orgUnit, true);
            childs.add(0, (OrgUnit)orgUnit);
            builder.add(MQExpression.in("sli", StaffListItem.L_STAFF_LIST + "." + StaffList.L_ORG_UNIT, childs));
        }
        else
        {

            if (null != orgUnit)
            {
                builder.add(MQExpression.eq("sli", StaffListItem.L_STAFF_LIST + "." + StaffList.L_ORG_UNIT, orgUnit));
            }
            else if (null != orgUnitType)
            {
                builder.add(MQExpression.eq("sli", StaffListItem.L_STAFF_LIST + "." + StaffList.L_ORG_UNIT + "." + OrgUnit.L_ORG_UNIT_TYPE, orgUnitType));
            }
        }

        if(null != employeeType)
        {
            final MQBuilder subBuilder = new MQBuilder(EmployeeType.ENTITY_CLASS, "et", new String[] {EmployeeType.P_ID});
            final AbstractExpression expr1 = MQExpression.eq("et", EmployeeType.P_ID, ((EmployeeType)employeeType).getId());
            final AbstractExpression expr2 = MQExpression.eq("et", EmployeeType.L_PARENT, employeeType);
            subBuilder.add(MQExpression.or(expr1, expr2));
            builder.add(MQExpression.in("sli", StaffListItem.L_ORG_UNIT_POST_RELATION + "." + OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_POST + "." + Post.L_EMPLOYEE_TYPE + ".id", subBuilder));
        }

        if(null != post)
        {
            builder.add(MQExpression.eq("sli", StaffListItem.L_ORG_UNIT_POST_RELATION + "." + OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_POST, post));
        }

        if (states != null && states.size() != 0)
            builder.add(MQExpression.in("sli", StaffListItem.staffList().staffListState().s(), states));

        if (formativeDateFrom != null)
            builder.add(MQExpression.greatOrEq("sli", StaffListItem.staffList().creationDate().s(), formativeDateFrom));

        if (formativeDateTo != null)
            builder.add(MQExpression.lessOrEq("sli", StaffListItem.staffList().creationDate().s(), formativeDateTo));

        if (acceptionDateFrom != null)
            builder.add(MQExpression.greatOrEq("sli", StaffListItem.staffList().acceptionDate().s(), acceptionDateFrom));

        if (acceptionDateTo != null)
            builder.add(MQExpression.lessOrEq("sli", StaffListItem.staffList().acceptionDate().s(), acceptionDateTo));

        if (archivingDateFrom != null)
            builder.add(MQExpression.greatOrEq("sli", StaffListItem.staffList().archivingDate().s(), archivingDateFrom));

        if (archivingDateTo != null)
            builder.add(MQExpression.lessOrEq("sli", StaffListItem.staffList().archivingDate().s(), archivingDateTo));

        builder.addOrder("sli", StaffListItem.L_STAFF_LIST + "." + StaffList.L_ORG_UNIT + "." + OrgUnit.P_TITLE);
        builder.addDescOrder("sli", StaffListItem.staffList().number().s());
        builder.addOrder("sli", StaffListItem.L_STAFF_LIST + "." + StaffList.L_ORG_UNIT + "." + OrgUnit.L_ORG_UNIT_TYPE + "." + OrgUnitType.P_TITLE);
        builder.addOrder("sli", StaffListItem.L_ORG_UNIT_POST_RELATION + "." + OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_POST + "." + Post.P_TITLE);
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }

}