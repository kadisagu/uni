/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.component.reports.staffListAllocation.List;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uniemp.entity.report.StaffListAllocationReport;

/**
 * @author dseleznev
 * Created on: 10.11.2010
 */
public class Model
{
    private IDataSettings _settings;
    private DynamicListDataSource<StaffListAllocationReport> _dataSource;

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public DynamicListDataSource<StaffListAllocationReport> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<StaffListAllocationReport> dataSource)
    {
        _dataSource = dataSource;
    }
}