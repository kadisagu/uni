/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.orgUnitPost.OrgUnitPostPub;

import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation;

/**
 * @author dseleznev
 * Created on: 19.09.2008
 */
@State(keys = { PublisherActivator.PUBLISHER_ID_KEY, "orgUnitId" }, bindings = { "publisherId", "orgUnitId" })
@Output(keys = "orgUnitPostRelationId", bindings = "orgUnitPostRel.id")
public class Model
{
    private Long _publisherId;
    private Long _orgUnitId;
    private OrgUnitPostRelation _orgUnitPostRel;
    private OrgUnit _orgUnit;

    public Long getPublisherId()
    {
        return _publisherId;
    }

    public void setPublisherId(Long publisherId)
    {
        this._publisherId = publisherId;
    }

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        this._orgUnitId = orgUnitId;
    }

    public OrgUnitPostRelation getOrgUnitPostRel()
    {
        return _orgUnitPostRel;
    }

    public void setOrgUnitPostRel(OrgUnitPostRelation orgUnitPostRel)
    {
        this._orgUnitPostRel = orgUnitPostRel;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        this._orgUnit = orgUnit;
    }

    public String getScienceStatusColumn()
    {
        return getOrgUnitPostRel().getScienceStatusesListStr().replaceAll(", ", ",<br/>");
    }

    public String getScienceDegreesColumn()
    {
        return getOrgUnitPostRel().getScienceDegreesListStr().replaceAll(", ", ",<br/>");
    }

    public String getPostTypesColumn()
    {
        return getOrgUnitPostRel().getPostTypesListStr().replaceAll(", ", ",<br/>");
    }
    
    public String getViewKey()
    {
        return "orgUnit_viewPost_" + getOrgUnit().getOrgUnitType().getCode();
    }

    public String getEditKey()
    {
        return "orgUnit_editPost_" + getOrgUnit().getOrgUnitType().getCode();
    }

    public String getDelKey()
    {
        return "orgUnit_deletePost_" + getOrgUnit().getOrgUnitType().getCode();
    }

}