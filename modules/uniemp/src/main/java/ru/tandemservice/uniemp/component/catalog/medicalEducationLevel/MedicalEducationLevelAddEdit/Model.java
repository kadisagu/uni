/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniemp.component.catalog.medicalEducationLevel.MedicalEducationLevelAddEdit;

import org.tandemframework.tapsupport.component.selection.ISelectModel;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniemp.entity.catalog.MedicalEducationLevel;

/**
 * @author AutoGenerator
 * Created on 21.09.2010
 */
public class Model extends DefaultCatalogAddEditModel<MedicalEducationLevel>
{
    private ISelectModel _eduProgramSubjectIndexModel;
    private ISelectModel _eduProgramSubjectModel;
    private EduProgramSubjectIndex _eduProgramSubjectIndex;

    public ISelectModel getEduProgramSubjectIndexModel () { return _eduProgramSubjectIndexModel; }
    public void setEduProgramSubjectIndexModel(ISelectModel eduProgramSubjectIndexModel) { this._eduProgramSubjectIndexModel = eduProgramSubjectIndexModel; }

    public ISelectModel getEduProgramSubjectModel() { return _eduProgramSubjectModel; }
    public void setEduProgramSubjectModel(ISelectModel eduProgramSubjectModel) { this._eduProgramSubjectModel = eduProgramSubjectModel; }

    public EduProgramSubjectIndex getEduProgramSubjectIndex() { return _eduProgramSubjectIndex; }
    public void setEduProgramSubjectIndex(EduProgramSubjectIndex eduProgramSubjectIndex) { this._eduProgramSubjectIndex = eduProgramSubjectIndex; }

}