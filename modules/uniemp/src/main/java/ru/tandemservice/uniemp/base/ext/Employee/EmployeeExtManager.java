/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniemp.base.ext.Employee;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Vasily Zhukov
 * @since 30.03.2012
 */
@Configuration
public class EmployeeExtManager extends BusinessObjectExtensionManager
{
}
