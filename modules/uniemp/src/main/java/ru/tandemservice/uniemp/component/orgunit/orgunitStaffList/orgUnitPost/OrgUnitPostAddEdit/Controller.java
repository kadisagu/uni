/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.orgUnitPost.OrgUnitPostAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.info.ErrorCollector;

import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;

/**
 * @author dseleznev
 * Created on: 16.09.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getModel(component).setPostTitleProperty(getMessage("orgUnitPost.titleProperty"));
        getDao().prepare(getModel(component));
    }

    public void onRefreshPost(IBusinessComponent component)
    {
        OrgUnitTypePostRelation rel = getModel(component).getOrgUnitPostRelation().getOrgUnitTypePostRelation();
        if (null != rel)
            getModel(component).getOrgUnitPostRelation().setHeaderPost(rel.isHeaderPost());
    }

    public void onClickApply(IBusinessComponent component)
    {
        ErrorCollector errors = component.getUserContext().getErrorCollector();
        Model model = getModel(component);
        getDao().validate(model, errors);
        if (errors.hasErrors()) return;

        getDao().update(getModel(component).getOrgUnitPostRelation(), getModel(component));
        deactivate(component);
    }

}