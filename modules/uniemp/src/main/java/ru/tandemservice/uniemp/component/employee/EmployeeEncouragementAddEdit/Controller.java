/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.EmployeeEncouragementAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.info.ErrorCollector;

import ru.tandemservice.uniemp.entity.catalog.EncouragementType;

/**
 * @author dseleznev
 * Created on: 23.12.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickApply(IBusinessComponent component)
    {
        ErrorCollector errCollector = component.getUserContext().getErrorCollector();
        getDao().validate(getModel(component), errCollector);

        if (!errCollector.hasErrors())
        {
            getDao().update(getModel(component));
            deactivate(component);
        }
    }

    public void onChangeType(IBusinessComponent component)
    {
        Model model = getModel(component);
        EncouragementType type = model.getEmployeeEncouragement().getType();
        model.getEmployeeEncouragement().setDocument(null!=type?null!=type.getDocument()?type.getDocument():"":"");
    }
}