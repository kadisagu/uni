/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.qualityConsistency.Add;

import java.util.List;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author dseleznev
 * Created on: 14.04.2009
 */
public interface IDAO extends IUniDao<Model>
{
    List<OrgUnit> getOrgUnitsList(OrgUnitType orgUnitType, String filter);
    
    Integer preparePrintReport(Model model);
}