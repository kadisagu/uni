/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.bo.EmpSummaryPPSReport.ui.Detail;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.RowCollectionFormatter;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import ru.tandemservice.uniemp.base.bo.EmpSummaryPPSReport.logic.EmpSummaryPPSReportDetailSearchDSHandler;

/**
 * @author Alexander Shaburov
 * @since 21.11.12
 */
@Configuration
public class EmpSummaryPPSReportDetail extends BusinessComponentManager
{
    // ds
    public static final String EMPLOYEE_POST_SEARCH_DS = "employeePostSearchDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(EMPLOYEE_POST_SEARCH_DS, employeePostSearchDSColumns(), employeePostSearchDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint employeePostSearchDSColumns()
    {
        IMergeRowIdResolver mergeResolver = entity -> {
            DataWrapper employeePost = (DataWrapper) entity;
            return employeePost.getProperty("employeeId").toString();
        };

        return columnListExtPointBuilder(EMPLOYEE_POST_SEARCH_DS)
                .addColumn(textColumn("employeeCode", "employeeCode").merger(mergeResolver).order())
                .addColumn(textColumn("title", "title").merger(mergeResolver).order())
                // todo SH-923: вкрутить запятую на место
                .addColumn(textColumn("scienceDegree", "scienceDegree").formatter(RowCollectionFormatter.INSTANCE).merger(mergeResolver))
                .addColumn(textColumn("scienceStatus", "scienceStatus").formatter(RowCollectionFormatter.INSTANCE).merger(mergeResolver))
                .addColumn(textColumn("post", "post").order())
                .addColumn(textColumn("orgUnit", "orgUnit").order())
                .addColumn(textColumn("postType", "postType").order())
                .addColumn(textColumn("staffRate", "staffRate").formatter(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).order())
                .addColumn(indicatorColumn("hourlyPaid", "hourlyPaid")
                        .addIndicator(true, new IndicatorColumn.Item("yes"))
                        .addIndicator(false, new IndicatorColumn.Item("no")))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> employeePostSearchDSHandler()
    {
        return new EmpSummaryPPSReportDetailSearchDSHandler(getName());
    }
}
