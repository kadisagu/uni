/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.qualityConsistencyFull.Add;

import java.util.Date;

/**
 * @author dseleznev
 * Created on: 16.04.2009
 */
public class Model
{
    private Date _reportDate;

    public Date getReportDate()
    {
        return _reportDate;
    }

    public void setReportDate(Date reportDate)
    {
        this._reportDate = reportDate;
    }
}