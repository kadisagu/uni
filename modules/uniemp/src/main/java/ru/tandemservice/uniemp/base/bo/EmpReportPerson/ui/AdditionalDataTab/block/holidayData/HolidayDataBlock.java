/* $Id$ */
package ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.AdditionalDataTab.block.holidayData;

import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.datasource.IUIDataSource;

import ru.tandemservice.uniemp.entity.catalog.HolidayType;

/**
 * @author Vasily Zhukov
 * @since 05.08.2011
 */
public class HolidayDataBlock
{
    public static final String HOLIDAY_TYPE_DS = "holidayTypeDS";

    public static void onBeforeDataSourceFetch(IUIDataSource dataSource, HolidayDataParam param)
    {
    }

    public static IDefaultComboDataSourceHandler createHolidayTypeDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, HolidayType.class);
    }
}
