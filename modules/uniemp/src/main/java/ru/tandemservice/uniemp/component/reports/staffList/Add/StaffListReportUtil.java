/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.staffList.Add;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.text.table.RtfBorder;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.cell.RtfCellBorder;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.employee.StaffListFakePayment;
import ru.tandemservice.uniemp.entity.employee.StaffListItem;
import ru.tandemservice.uniemp.entity.employee.StaffListPaymentBase;
import ru.tandemservice.uniemp.entity.report.StaffListRepPaymentSettings;

import java.util.*;

/**
 * @author dseleznev
 * Created on: 07.08.2009
 */
public class StaffListReportUtil
{
    public static FinancingSource finSrcBudgetCatalogItem = IUniBaseDao.instance.get().getCatalogItem(FinancingSource.class, UniempDefines.FINANCING_SOURCE_BUDGET);

    // Индекс колонки с выплатами
    public static final int PAYMENTS_COLUMN_IDX = 7;
    
    // Идентификаторы для wrapper-списка видов деятельности
    public static final Long ACTIVITY_TYPE_BUDGET = 1L;
    public static final Long ACTIVITY_TYPE_OFF_BUDGET = 2L;
    
    // Идентификатор для типа надбавок, доплат "Другие надбавки"
    public static final Long OTHER_PAYMENTS_ID = 0L;
    
    // Идентификаторы для разных типов сумм по подразделениям
    private static final Long ORG_UNIT_WITH_CHILDS_SUM_ID = 0L; // Подразделения с учетом дочерних
    private static final Long ORG_UNIT_SUM_ID = 1L; // Только подразделения
    
    public static final RtfTableRowStyle[] ORG_UNIT_HEADER_ROW_STYLES = new RtfTableRowStyle[]
    {
        new RtfTableRowStyle(true, false, 28, RtfTableRowStyle.TEXT_ALIGN_CENTER, true, null, 400),
        new RtfTableRowStyle(true, true, 24, RtfTableRowStyle.TEXT_ALIGN_LEFT, true, 100, null),
        new RtfTableRowStyle(true, true, 20, RtfTableRowStyle.TEXT_ALIGN_LEFT, true, 400, null),
        new RtfTableRowStyle(true, true, 16, RtfTableRowStyle.TEXT_ALIGN_LEFT, true, 600, null),
        new RtfTableRowStyle(true, true, 16, RtfTableRowStyle.TEXT_ALIGN_LEFT, true, 800, null),
        new RtfTableRowStyle(true, true, 16, RtfTableRowStyle.TEXT_ALIGN_LEFT, true, 1000, null)
    };

    public static final RtfTableRowStyle TOTAL_ROW_STYLE = new RtfTableRowStyle(false, true, null, RtfTableRowStyle.TEXT_ALIGN_RIGHT, false, null, null, new Integer[][]{{0, 2}}, new Integer[]{20, 30, 30, 30});
    public static final RtfTableRowStyle TOTAL_FACULTY_ROW_STYLE = new RtfTableRowStyle(true, true, null, RtfTableRowStyle.TEXT_ALIGN_RIGHT, false, null, null, new Integer[][]{{0, 2}}, new Integer[]{20, 30, 30, 30});
    
    public static Comparator<StaffListItem> POST_TITLE_COMPARATOR = (o1, o2) -> {
        PostBoundedWithQGandQL p1 = o1.getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL();
        PostBoundedWithQGandQL p2 = o2.getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL();

        if (null != p1.getPost().getRank() && null != p2.getPost().getRank() && !p1.getPost().getRank().equals(p2.getPost().getRank()))
            return p1.getPost().getRank().compareTo(p2.getPost().getRank());
        else if (null != p1.getPost().getRank() && null == p2.getPost().getRank())
            return -1;
        else if (null != p2.getPost().getRank() && null == p1.getPost().getRank())
            return 1;
        else if (!p2.getQualificationLevel().equals(p1.getQualificationLevel()))
            return p2.getQualificationLevel().getShortTitle().compareTo(p1.getQualificationLevel().getShortTitle());
        else
            return p1.getTitle().compareTo(p2.getTitle());
    };
    

    /**
     * Подготавливает сериализованный список подразделений, где для каждого подразделения верхнего уровня
     * следует список дочерних подразделений,а для дочерних подразделений - список дочерних подразделений
     * для каждого из них, в соответствии с настройками отображения и использования типов подразделений в
     * отчете.
     */
    public static void prepareSerializedOrgstructure(IDAO dao, Model model, List<OrgUnit> orgUnitsToTake)
    {
        Map<Long, OrgUnit> orgUnitsMap = new HashMap<>();
        List<OrgUnit> serializedOrgstructure = new ArrayList<>();
        TopOrgUnit academy = TopOrgUnit.getInstance();
        if (model.getOrgUnitList().size() == 0)
            serializedOrgstructure = dao.getChildOrgUnits(academy, orgUnitsToTake, model.getOrgUnitTypesMap(), model.getReportDate());
        else
            for (OrgUnit orgUnit : model.getOrgUnitList())
            {
                List<OrgUnit> orgUnitList = new ArrayList<>();
                orgUnitList.addAll(dao.getChildOrgUnits(orgUnit, orgUnitsToTake, model.getOrgUnitTypesMap(), model.getReportDate()));
                orgUnitList.add(0, orgUnit);
                serializedOrgstructure.addAll(orgUnitList);
            }

        for (OrgUnit orgUnit : serializedOrgstructure)
            orgUnitsMap.put(orgUnit.getId(), orgUnit);

        //алгоритм удаляет из списка подразделения без активного ШР,
        //таким образом, что бы в списке остались такие подразделения, у которых нет активного ШР, но у их дочерних подразделений имеется активное ШР
        List<OrgUnit> finishedOrgUnitList = new ArrayList<>(serializedOrgstructure);
        if (!model.getShowOrgUnitWithoutStaffList())
        {
            List<OrgUnit> orgUnitsWithStaffList = dao.getOrgUnitWithActiveStaffList();
            for (OrgUnit orgUnit : serializedOrgstructure)
            {
                boolean remove = false;
                if (!orgUnitsWithStaffList.contains(orgUnit))
                {
                    remove = true;
                    for (OrgUnit innerOrgUnit : serializedOrgstructure)
                    {
                        OrgUnit parent = innerOrgUnit.getParent();
                        while (parent != null)
                        {
                            if (orgUnit.equals(parent))
                                if (orgUnitsWithStaffList.contains(innerOrgUnit))
                                    remove = false;
                            parent = !academy.equals(parent.getParent()) ? parent.getParent() : null;
                        }
                    }
                }

                if (remove)
                    finishedOrgUnitList.remove(orgUnit);
            }
        }

        model.setSerializedOrgstructure(finishedOrgUnitList);
        model.setOrgUnitsMap(orgUnitsMap);
    }
    
    /**
     * Подготавливает заголовок таблицы с доплатами (надбавками) 
     * в зависимости от настроек фильтров выплат
     */
    public static void preparePaymentsHeader(Model model)
    {
        List<Long> otherPaymentIds = new ArrayList<>();
        List<Payment> paymentHeadersList = new ArrayList<>();

        for (StaffListRepPaymentSettings setting : model.getPaymentsList())
        {
            if (null != setting.getPayment())
            {
                if (setting.isIndividualColumn()) paymentHeadersList.add(setting.getPayment());
                else otherPaymentIds.add(setting.getPayment().getId());
            }
            else
            {
                Payment otherPayment = new Payment();
                otherPayment.setId(OTHER_PAYMENTS_ID);
                otherPayment.setShortTitle("другие надбавки");
                paymentHeadersList.add(otherPayment);
            }
        }

        model.setPaymentHeadersList(paymentHeadersList);
        model.setOtherPaymentIds(otherPaymentIds);

        model.setColumnsNumber(11 + model.getPaymentHeadersList().size());
        String[] numbersHeader = new String[model.getColumnsNumber()];
        for (int i = 0; i < model.getColumnsNumber(); i++) numbersHeader[i] = String.valueOf(i + 1);
        model.setNumberHeaders(numbersHeader);

        String[] paymentHeaders = new String[model.getPaymentHeadersList().size()];
        for (Payment payment : model.getPaymentHeadersList())
            paymentHeaders[model.getPaymentHeadersList().indexOf(payment)] = payment.getShortTitle();

        int[] paymentColumnWidths = new int[model.getPaymentHeadersList().size()];
        Arrays.fill(paymentColumnWidths, 1);
        model.setPaymentColumnWidth(paymentColumnWidths);

        model.setPaymentHeaders(paymentHeaders);
    }
    
    /**
     * Возвращает подразделение, на котором должна отобразиться должность или выплата 
     * в зависимости от настроек фильтров
     */
    private static OrgUnit getOrgUnitWherePostShouldBeShown(OrgUnit orgUnit, Map<Long, OrgUnitTypeWrapper> orgUnitTypesMap, Map<OrgUnit, OrgUnit> orgUnitMappingsMap)
    {
        if (null != orgUnitMappingsMap.get(orgUnit))
            return orgUnitMappingsMap.get(orgUnit);

        OrgUnit orgUnitToIterate = orgUnit;
        OrgUnit parentOrgUnit = orgUnitToIterate.getParent();
        OrgUnitTypeWrapper ouTypeWrapper = orgUnitTypesMap.get(orgUnitToIterate.getOrgUnitType().getId());
        boolean useOrgUnit = null != ouTypeWrapper && ouTypeWrapper.isUseOrgUnit();
        boolean individualSection = null != ouTypeWrapper && ouTypeWrapper.isShowChildOrgUnits();
        boolean showChildPostsAsOwn = null != ouTypeWrapper && ouTypeWrapper.isShowChildOrgUnitPostsAsOwn();
        OrgUnit targetOrgUnit = null;

        if (individualSection)
        {
            targetOrgUnit = orgUnitToIterate;
        }
        else
        {

            while ((null != parentOrgUnit && null != parentOrgUnit.getParent()) && !(individualSection && showChildPostsAsOwn))
            {
                orgUnitToIterate = parentOrgUnit;
                parentOrgUnit = orgUnitToIterate.getParent();
                if (null == parentOrgUnit || !useOrgUnit) break;
                ouTypeWrapper = orgUnitTypesMap.get(orgUnitToIterate.getOrgUnitType().getId());
                useOrgUnit = null != ouTypeWrapper && ouTypeWrapper.isUseOrgUnit();
                individualSection = null != ouTypeWrapper && ouTypeWrapper.isShowChildOrgUnits();
                showChildPostsAsOwn = null != ouTypeWrapper && ouTypeWrapper.isShowChildOrgUnitPostsAsOwn();
            }

            if (individualSection && showChildPostsAsOwn)
                targetOrgUnit = orgUnitToIterate;
        }

        orgUnitMappingsMap.put(orgUnit, targetOrgUnit);
        return targetOrgUnit;
    }
    
    /**
     * Обнуляет в модели переменные и мапы, используемые для отчетной статистики
     */
    private static void initStatisticVariables(Model model)
    {
        model.setVuzSumSalary(getNewPairInstance(0d, 0d));
        model.setVuzSumTotalSalary(getNewPairInstance(0d, 0d));
        model.setVuzBudgetStaffRate(0d);
        model.setVuzOffBudgetStaffRate(0d);
        model.setPostSalarysMap(new HashMap<>());
        model.setVuzSumPaymentsMap(new HashMap<>());

        model.setOrgUnitPostBudgetStaffRatesMap(new HashMap<>());
        model.setOrgUnitPostOffBudgetStaffRatesMap(new HashMap<>());
        model.setOrgUnitPostListsMap(new HashMap<>());
        model.setPostDataMap(new HashMap<>());

        model.setOrgUnitSumPaymentsMap(new HashMap<>());
        model.setOrgUnitWithChildsSumPaymentsMap(new HashMap<>());
        model.setOrgUnitPostPaymentsMap(new HashMap<>());

        model.setOrgUnitPostSalarysMap(new HashMap<>());
        model.setOrgUnitWithChildsSalarysMap(new HashMap<>());
        model.setOrgUnitSalarysMap(new HashMap<>());

        model.setOrgUnitSumTotalSalaryMap(new HashMap<>());
        model.setOrgUnitWithChildsSumTotalSalaryMap(new HashMap<>());
        model.setOrgUnitPostTotalSalaryMap(new HashMap<>());

        model.setOrgUnitMappingsMap(new HashMap<>());
    }
    
    /**
     * Подготавливает основные данные по должностям, отнесенным к ПКГ и КУ,
     * сведения о ставках по каждой должности на подразделениях, 
     * данные об окладе должностей, отнесенных к ПКГ и КУ, а так же
     * суммарные ставки по подразделениям, 
     * суммарные ставки по подразделениям, включая дочерние подразделения,
     * суммарные ставки по ОУ.
     */
    private static void prepareGeneralData(Model model)
    {
        Map<Long, OrgUnit> orgUnitsMap = new HashMap<>();

        for(StaffListItem staffListItem : model.getRawStaffListItemsList())
        {
            Long staffListItemId = staffListItem.getId();
            OrgUnit orgUnit = staffListItem.getStaffList().getOrgUnit();
            PostBoundedWithQGandQL post = staffListItem.getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL();

            if(!orgUnitsMap.containsKey(orgUnit.getId())) orgUnitsMap.put(orgUnit.getId(), orgUnit);
            orgUnit = getOrgUnitWherePostShouldBeShown(orgUnit, model.getOrgUnitTypesMap(), model.getOrgUnitMappingsMap());

            if(!model.getPostSalarysMap().containsKey(staffListItemId))
                model.getPostSalarysMap().put(staffListItemId, staffListItem.getSalary());

            if (null != orgUnit)
            {
                Long orgUnitId = orgUnit.getId();
                List<StaffListItem> postsList = model.getOrgUnitPostListsMap().get(orgUnitId);
                if (null == postsList) postsList = new ArrayList<>();
                postsList.add(staffListItem);

                if (!model.getPostDataMap().containsKey(staffListItemId))
                    model.getPostDataMap().put(staffListItemId, new String[] { post.getTitle(), post.getProfQualificationGroup().getTitle().length() > 3 ? post.getProfQualificationGroup().getTitle().substring(0, 3) : post.getProfQualificationGroup().getTitle(), post.getQualificationLevel().getShortTitle() });

                if (staffListItem.getFinancingSource().equals(finSrcBudgetCatalogItem))
                {
                    setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitPostBudgetStaffRatesMap(), staffListItem.getStaffRate(), 1, orgUnitId, staffListItemId);
                    setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitPostOffBudgetStaffRatesMap(), 0d, 1, orgUnitId, staffListItemId);
                    setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitPostBudgetStaffRatesMap(), staffListItem.getStaffRate(), 1, orgUnitId, ORG_UNIT_SUM_ID);
                    setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitPostOffBudgetStaffRatesMap(), 0d, 1, orgUnitId, ORG_UNIT_SUM_ID);
                }
                else
                {
                    setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitPostBudgetStaffRatesMap(), 0d, 1, orgUnitId, staffListItemId);
                    setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitPostOffBudgetStaffRatesMap(), staffListItem.getStaffRate(), 1, orgUnitId, staffListItemId);
                    setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitPostBudgetStaffRatesMap(), 0d, 1, orgUnitId, ORG_UNIT_SUM_ID);
                    setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitPostOffBudgetStaffRatesMap(), staffListItem.getStaffRate(), 1, orgUnitId, ORG_UNIT_SUM_ID);
                }

                model.getOrgUnitPostListsMap().put(orgUnitId, postsList);

                // Вычисляем полные суммы ставок по каждому из стоящих выше в иерархии подразделений
                OrgUnit parentOrgUnit = orgUnit;
                while (null != parentOrgUnit)
                {
                    if (staffListItem.getFinancingSource().equals(finSrcBudgetCatalogItem))
                    {
                        setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitPostBudgetStaffRatesMap(), staffListItem.getStaffRate(), 1, parentOrgUnit.getId(), ORG_UNIT_WITH_CHILDS_SUM_ID);
                        setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitPostOffBudgetStaffRatesMap(), 0d, 1, parentOrgUnit.getId(), ORG_UNIT_WITH_CHILDS_SUM_ID);
                    }
                    else
                    {
                        setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitPostBudgetStaffRatesMap(), 0d, 1, parentOrgUnit.getId(), ORG_UNIT_WITH_CHILDS_SUM_ID);
                        setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitPostOffBudgetStaffRatesMap(), staffListItem.getStaffRate(), 1, parentOrgUnit.getId(), ORG_UNIT_WITH_CHILDS_SUM_ID);
                    }
                    parentOrgUnit = parentOrgUnit.getParent();
                }

                if (model.getSerializedOrgstructure().contains(orgUnit))
                {
                    if (staffListItem.getFinancingSource().equals(finSrcBudgetCatalogItem))
                    {
                        model.setVuzBudgetStaffRate(model.getVuzBudgetStaffRate() + staffListItem.getStaffRate());
                        model.setVuzOffBudgetStaffRate(model.getVuzOffBudgetStaffRate() + 0d);
                    }
                    else
                    {
                        model.setVuzBudgetStaffRate(model.getVuzBudgetStaffRate() + 0d);
                        model.setVuzOffBudgetStaffRate(model.getVuzOffBudgetStaffRate() + staffListItem.getStaffRate());
                    }
                }
            }
        }

        // Сортируем подразделения по печатному названию по каждому уровню
        for(List<StaffListItem> postsList : model.getOrgUnitPostListsMap().values())
        {
            Collections.sort(postsList, POST_TITLE_COMPARATOR);
        }
    }
    
    /**
     * Подготавливает данные о занятости ставок каждой из должностей по подразделениям
     */
    /*private static final void prepareOccupatedStaffRates(Model model)
    {
        for(StaffListAllocationItem item : model.getRawStaffListAllocationItemsList())
        {
            if (null != item.getEmployeePost())
            {
                Long staffListItemId = item.getId();
                Long orgUnitId = item.getStaffListItem().getStaffList().getOrgUnit().getId();

                setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitPostOccBudgetStaffRatesMap(), item.getBudgetStaffRate(), 1, orgUnitId, staffListItemId);
                setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitPostOccOffBudgetStaffRatesMap(), item.getOffBudgetStaffRate(), 1, orgUnitId, staffListItemId);
                setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitPostOccBudgetStaffRatesMap(), item.getBudgetStaffRate(), 1, orgUnitId, ORG_UNIT_SUM_ID);
                setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitPostOccOffBudgetStaffRatesMap(), item.getOffBudgetStaffRate(), 1, orgUnitId, ORG_UNIT_SUM_ID);

                // Вычисляем полные суммы ставок по каждому из стоящих выше в иерархии подразделений
                OrgUnit parentOrgUnit = model.getOrgUnitsMap().get(orgUnitId);
                while (null != parentOrgUnit)
                {
                    setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitPostOccBudgetStaffRatesMap(), item.getBudgetStaffRate(), 1, parentOrgUnit.getId(), ORG_UNIT_WITH_CHILDS_SUM_ID);
                    setTwoLevelsDoubleMapValuesInSafeMode(model.getOrgUnitPostOccOffBudgetStaffRatesMap(), item.getOffBudgetStaffRate(), 1, parentOrgUnit.getId(), ORG_UNIT_WITH_CHILDS_SUM_ID);
                    parentOrgUnit = parentOrgUnit.getParent();
                }
                
                model.setVuzOccBudgetStaffRate(model.getVuzOccBudgetStaffRate() + item.getBudgetStaffRate());
                model.setVuzOccOffBudgetStaffRate(model.getVuzOccOffBudgetStaffRate() + item.getOffBudgetStaffRate());
            }
        }
    }*/
    
    /**
     * Сводит выплаты в один мап с учетом перестановок должностей по оргюнитам в соответствии с настройкой 
     */
    private static void reorganizePayments(Model model)
    {
        Map<Long, Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>>> paymentsRubleValuesMap = new HashMap<>();
        Map<Long, Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>>> paymentsFullRubleValuesMap = new HashMap<>();
        List<StaffListPaymentBase> fullSalaryPaymentsList = new ArrayList<>();

        Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> basePaymentValue = new HashMap<>();

        for (StaffListPaymentBase item : model.getRawStaffListPaymentsList())
        {
            OrgUnit orgUnit = item.getStaffListItem().getStaffList().getOrgUnit();
            orgUnit = getOrgUnitWherePostShouldBeShown(orgUnit, model.getOrgUnitTypesMap(), model.getOrgUnitMappingsMap());
            Long staffListItemId = item.getStaffListItem().getId();
            Long paymentId = item.getPayment().getId();

            if(model.getOtherPaymentIds().contains(paymentId)) paymentId = OTHER_PAYMENTS_ID;

            if (null != orgUnit)
            {
                Long orgUnitId = orgUnit.getId();
                if (UniempDefines.PAYMENT_UNIT_FULL_PERCENT.equals(item.getPayment().getPaymentUnit().getCode()))
                    fullSalaryPaymentsList.add(item);
                else
                {
                    Double budgetStaffRate;
                    Double offBudgetStaffRate;

                    if(item instanceof StaffListFakePayment)
                    {
                        if (item.getFinancingSource().equals(finSrcBudgetCatalogItem))
                        {
                            budgetStaffRate = ((StaffListFakePayment)item).getStaffRate();
                            offBudgetStaffRate = 0d;
                        }
                        else
                        {
                            budgetStaffRate = 0d;
                            offBudgetStaffRate = ((StaffListFakePayment)item).getStaffRate();
                        }
                    }
                    else
                    {
                        if (item.getFinancingSource().equals(finSrcBudgetCatalogItem))
                        {
                            budgetStaffRate = item.getStaffListItem().getStaffRate();
                            offBudgetStaffRate = 0d;
                        }
                        else
                        {
                            budgetStaffRate = 0d;
                            offBudgetStaffRate = item.getStaffListItem().getStaffRate();
                        }
                    }

                    Double budgetBaseSalaryPart = budgetStaffRate * item.getStaffListItem().getSalary();
                    Double offBudgetBaseSalaryPart = offBudgetStaffRate * item.getStaffListItem().getSalary();

                    Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> orgUnitPaymentRubleValuesMap = paymentsRubleValuesMap.get(orgUnitId);
                    if (null == orgUnitPaymentRubleValuesMap) orgUnitPaymentRubleValuesMap = new HashMap<>();

                    Map<Long, CoreCollectionUtils.Pair<Double, Double>> postPaymentRubleValueMap = orgUnitPaymentRubleValuesMap.get(staffListItemId);
                    if (null == postPaymentRubleValueMap) postPaymentRubleValueMap = new HashMap<>();

                    CoreCollectionUtils.Pair<Double, Double> paymentsSum = postPaymentRubleValueMap.get(paymentId);
                    if (null == paymentsSum) paymentsSum = new CoreCollectionUtils.Pair<>(0d, 0d);

                    Double budgetPaymentValue = 0d;
                    Double offBudgetPaymentValue = 0d;

                    if (UniempDefines.PAYMENT_UNIT_RUBLES.equals(item.getPayment().getPaymentUnit().getCode()))
                    {
                        if (null == item.getFinancingSource())
                        {
                            double budgetPercent = budgetStaffRate / (budgetStaffRate + offBudgetStaffRate);
                            double offBudgetPercent = 1 - budgetPercent;

                            budgetPaymentValue = item.getAmount() * (item.getPayment().isDoesntDependOnStaffRate() ? budgetPercent : budgetStaffRate);
                            offBudgetPaymentValue = item.getAmount() * (item.getPayment().isDoesntDependOnStaffRate() ? offBudgetPercent : offBudgetStaffRate);
                        }
                        else if (UniempDefines.FINANCING_SOURCE_BUDGET.equals(item.getFinancingSource().getCode()))
                            budgetPaymentValue = item.getAmount() * (item.getPayment().isDoesntDependOnStaffRate() ? 1 : budgetStaffRate + offBudgetStaffRate);
                        else
                            offBudgetPaymentValue = item.getAmount() * (item.getPayment().isDoesntDependOnStaffRate() ? 1 : (budgetStaffRate + offBudgetStaffRate));
                    }
                    else if (UniempDefines.PAYMENT_UNIT_COEFFICIENT.equals(item.getPayment().getPaymentUnit().getCode()))
                    {
                        if (null == item.getFinancingSource())
                        {
                            budgetPaymentValue = Math.round(budgetBaseSalaryPart * item.getAmount() * 100) / 100d;
                            offBudgetPaymentValue = Math.round(offBudgetBaseSalaryPart * item.getAmount() * 100) / 100d;
                        }
                        else if (UniempDefines.FINANCING_SOURCE_BUDGET.equals(item.getFinancingSource().getCode()))
                            budgetPaymentValue = Math.round((budgetBaseSalaryPart + offBudgetBaseSalaryPart) * item.getAmount() * 100) / 100d;
                        else
                            offBudgetPaymentValue = Math.round((budgetBaseSalaryPart + offBudgetBaseSalaryPart) * item.getAmount() * 100) / 100d;
                    }
                    else if (UniempDefines.PAYMENT_UNIT_BASE_PERCENT.equals(item.getPayment().getPaymentUnit().getCode()))
                    {
                        if (null == item.getFinancingSource())
                        {
                            budgetPaymentValue = Math.round(budgetBaseSalaryPart * item.getAmount()) / 100d;
                            offBudgetPaymentValue = Math.round(offBudgetBaseSalaryPart * item.getAmount()) / 100d;
                        }
                        else if (UniempDefines.FINANCING_SOURCE_BUDGET.equals(item.getFinancingSource().getCode()))
                            budgetPaymentValue = Math.round((budgetBaseSalaryPart + offBudgetBaseSalaryPart) * item.getAmount()) / 100d;
                        else
                            offBudgetPaymentValue = Math.round((budgetBaseSalaryPart + offBudgetBaseSalaryPart) * item.getAmount()) / 100d;
                    }

                    paymentsSum.setX(paymentsSum.getX() + budgetPaymentValue);
                    paymentsSum.setY(paymentsSum.getY() + offBudgetPaymentValue);

                    {
                        Map<Long,CoreCollectionUtils.Pair<Double,Double>> valueMap = basePaymentValue.get(staffListItemId);
                        if (valueMap == null)
                            valueMap = new HashMap<>();
                        CoreCollectionUtils.Pair<Double, Double> pair = valueMap.get(item.getId());
                        if (pair == null)
                            pair = new CoreCollectionUtils.Pair<>(0d, 0d);
                        pair.setX(pair.getX() + budgetPaymentValue);
                        pair.setY(pair.getY() + offBudgetPaymentValue);
                        valueMap.put(item.getId(), pair);
                        basePaymentValue.put(staffListItemId, valueMap);
                    }

                    postPaymentRubleValueMap.put(paymentId, paymentsSum);
                    orgUnitPaymentRubleValuesMap.put(staffListItemId, postPaymentRubleValueMap);
                    paymentsRubleValuesMap.put(orgUnitId, orgUnitPaymentRubleValuesMap);
                }
            }
        }

        for (StaffListPaymentBase payment : fullSalaryPaymentsList)
        {
            OrgUnit orgUnit = payment.getStaffListItem().getStaffList().getOrgUnit();
            orgUnit = getOrgUnitWherePostShouldBeShown(orgUnit, model.getOrgUnitTypesMap(), model.getOrgUnitMappingsMap());
            Long staffListItemId = payment.getStaffListItem().getId();
            Long paymentId = payment.getPayment().getId();

            if (model.getOtherPaymentIds().contains(paymentId)) paymentId = OTHER_PAYMENTS_ID;

            if (null != orgUnit)
            {
                Long orgUnitId = orgUnit.getId();
                Double budgetBaseSalaryPart;
                Double offBudgetBaseSalaryPart;
                if (payment.getFinancingSource().equals(finSrcBudgetCatalogItem))
                {
                    budgetBaseSalaryPart = payment.getStaffListItem().getStaffRate() * payment.getStaffListItem().getSalary();
                    offBudgetBaseSalaryPart = 0d * payment.getStaffListItem().getSalary();
                }
                else
                {
                    budgetBaseSalaryPart = 0d * payment.getStaffListItem().getSalary();
                    offBudgetBaseSalaryPart = payment.getStaffListItem().getStaffRate() * payment.getStaffListItem().getSalary();
                }

                Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> orgUnitFullPaymentRubleValuesMap = paymentsFullRubleValuesMap.get(orgUnitId);
                if (null == orgUnitFullPaymentRubleValuesMap)
                    orgUnitFullPaymentRubleValuesMap = new HashMap<>();

                Map<Long, CoreCollectionUtils.Pair<Double, Double>> postPaymentFullRubleValueMap = orgUnitFullPaymentRubleValuesMap.get(staffListItemId);
                if (null == postPaymentFullRubleValueMap)
                    postPaymentFullRubleValueMap = new HashMap<>();

                CoreCollectionUtils.Pair<Double, Double> fullPaymentsSum = postPaymentFullRubleValueMap.get(paymentId);
                if (null == fullPaymentsSum)
                    fullPaymentsSum = new CoreCollectionUtils.Pair<>(0d, 0d);

                CoreCollectionUtils.Pair<Double, Double> paymentsSum = getNewPairInstance(0d, 0d);
                if (basePaymentValue.get(staffListItemId) != null)
                {
                    Map<Long, CoreCollectionUtils.Pair<Double, Double>> paymentValueMap = basePaymentValue.get(staffListItemId);

                    paymentsSum = calculatePaymentSumm(payment, paymentValueMap, fullSalaryPaymentsList);
//                    for (CoreCollectionUtils.Pair<Double, Double> paymentEntry : paymentValueMap.values())
//                    {
//                        paymentsSum.setX(paymentsSum.getX() + paymentEntry.getX());
//                        paymentsSum.setY(paymentsSum.getY() + paymentEntry.getY());
//                    }
                }

                fullPaymentsSum.setX(fullPaymentsSum.getX() + Math.round((paymentsSum.getX() + budgetBaseSalaryPart) * payment.getAmount()) / 100d);
                fullPaymentsSum.setY(fullPaymentsSum.getY() + Math.round((paymentsSum.getY() + offBudgetBaseSalaryPart) * payment.getAmount()) / 100d);

                postPaymentFullRubleValueMap.put(paymentId, fullPaymentsSum);
                orgUnitFullPaymentRubleValuesMap.put(staffListItemId, postPaymentFullRubleValueMap);
                paymentsFullRubleValuesMap.put(orgUnitId, orgUnitFullPaymentRubleValuesMap);
            }
        }

        model.setPaymentsRubleValuesMap(paymentsRubleValuesMap);
        model.setPaymentsFullRubleValuesMap(paymentsFullRubleValuesMap);
    }
    
    /**
     * Подготавливает данные о надбавках и доплатах
     */
    private static void preparePaymentsData(Model model)
    {
        reorganizePayments(model);

        // TODO: potentially weak place
        for (Map.Entry<Long, Map<Long, Double>> orgUnitEntry : model.getOrgUnitPostBudgetStaffRatesMap().entrySet())//model.getOrgUnitPostStaffRatesMap().entrySet())
        {
            Long orgUnitId = orgUnitEntry.getKey();
            for (Long staffListItemId : orgUnitEntry.getValue().keySet())
            {
                Map<Long, CoreCollectionUtils.Pair<Double, Double>> paymentsMap = new HashMap<>();
                
                if(model.getPaymentsRubleValuesMap().containsKey(orgUnitId) && model.getPaymentsRubleValuesMap().get(orgUnitId).containsKey(staffListItemId))
                    paymentsMap.putAll(model.getPaymentsRubleValuesMap().get(orgUnitId).get(staffListItemId));
                
                if(model.getPaymentsFullRubleValuesMap().containsKey(orgUnitId) && model.getPaymentsFullRubleValuesMap().get(orgUnitId).containsKey(staffListItemId))
                    paymentsMap.putAll(model.getPaymentsFullRubleValuesMap().get(orgUnitId).get(staffListItemId));
                
                for(Map.Entry<Long, CoreCollectionUtils.Pair<Double, Double>> paymentEntry : paymentsMap.entrySet())
                {
                    Long paymentId = paymentEntry.getKey();
                    CoreCollectionUtils.Pair<Double, Double> paymentItem = paymentEntry.getValue();
                    
                    setThreeLevelsPairMapValuesInSafeMode(model.getOrgUnitPostPaymentsMap(), paymentItem, 1, orgUnitId, staffListItemId, paymentId);

                    // Вычисление месячного фонда оплаты труда с надбавками для должности
                    setTwoLevelsPairMapValuesInSafeMode(model.getOrgUnitPostTotalSalaryMap(), paymentItem, 1, orgUnitId, staffListItemId);

                    // Вычисляем сумму надбавок должностям по подразделению для указанной выплаты
                    setTwoLevelsPairMapValuesInSafeMode(model.getOrgUnitSumPaymentsMap(), paymentItem, 1, orgUnitId, paymentId);

                    // Вычисление месячного фонда оплаты труда с надбавками подразделению
                    setSingleLevelPairMapValuesInSafeMode(model.getOrgUnitSumTotalSalaryMap(), paymentItem, 1, orgUnitId);

                    // Вычисляем полные суммы выплат по каждому из стоящих выше в иерархии подразделений
                    OrgUnit parentOrgUnit = model.getOrgUnitsMap().get(orgUnitId);
                    while (null != parentOrgUnit)
                    {
                        setTwoLevelsPairMapValuesInSafeMode(model.getOrgUnitWithChildsSumPaymentsMap(), paymentItem, 1, parentOrgUnit.getId(), paymentId);
                        parentOrgUnit = parentOrgUnit.getParent();
                    }

                    // Вычисляем полные суммы выплат в целом по ОУ
                    if (model.getSerializedOrgstructure().contains(model.getOrgUnitsMap().get(orgUnitId)))
                        setSingleLevelPairMapValuesInSafeMode(model.getVuzSumPaymentsMap(), paymentItem, 1, paymentId);
                }
            }
        }
    }
    
    /** 
     * Вычисляет месячный фонд оплаты труда (без надбавок и с ними) по каждой должности,
     * суммарный месячный фонд оплаты труда (без надбавок и с ними) по подразделениям,
     * суммарный месячный фонд оплаты труда (без надбавок и с ними) по подразделениям с учетом дочерних подразделений. 
     */
    private static void prepareSummaryData(Model model)
    {
        for(Map.Entry<Long, List<StaffListItem>> entry : model.getOrgUnitPostListsMap().entrySet())
        {
            Long orgUnitId = entry.getKey();
            CoreCollectionUtils.Pair<Double, Double> orgUnitSumSalary = model.getOrgUnitSalarysMap().get(orgUnitId);
            CoreCollectionUtils.Pair<Double, Double> orgUnitSumTotalSalary = model.getOrgUnitSumTotalSalaryMap().get(orgUnitId);
            if(null == orgUnitSumTotalSalary) orgUnitSumTotalSalary = getNewPairInstance(0d, 0d);
            if(null == orgUnitSumSalary) orgUnitSumSalary = getNewPairInstance(0d, 0d);
            
            List<StaffListItem> list = entry.getValue();
            Map<Long, CoreCollectionUtils.Pair<Double, Double>> postSumSalarysMap = model.getOrgUnitPostSalarysMap().get(orgUnitId);
            if(null == postSumSalarysMap) postSumSalarysMap = new HashMap<>();
            model.getOrgUnitPostSalarysMap().put(orgUnitId, postSumSalarysMap);
            
            Map<Long, CoreCollectionUtils.Pair<Double, Double>> postTotalSalaryMap = model.getOrgUnitPostTotalSalaryMap().get(orgUnitId);
            if(null == postTotalSalaryMap) postTotalSalaryMap = new HashMap<>();

            for(StaffListItem staffListItem : list)
            {
                // Вычисление месячного фонда оплаты труда по тарифу для должномти
                Long staffListItemId = staffListItem.getId();

                Double budgetRatedSalary;
                Double offBudgetRatedSalary;
                if (staffListItem.getFinancingSource().equals(finSrcBudgetCatalogItem))
                {
                    budgetRatedSalary = staffListItem.getSalary() * staffListItem.getStaffRate();
                    offBudgetRatedSalary = staffListItem.getSalary() * 0d;
                }
                else
                {
                    budgetRatedSalary = staffListItem.getSalary() * 0d;
                    offBudgetRatedSalary = staffListItem.getSalary() * staffListItem.getStaffRate();
                }
                postSumSalarysMap.put(staffListItemId, getNewPairInstance(budgetRatedSalary, offBudgetRatedSalary));
                orgUnitSumSalary.setX(orgUnitSumSalary.getX() + budgetRatedSalary);
                orgUnitSumSalary.setY(orgUnitSumSalary.getY() + offBudgetRatedSalary);
                
                // Вычисление месячного фонда оплаты труда с надбавками для должности
                CoreCollectionUtils.Pair<Double, Double> postTotalSalary = postTotalSalaryMap.get(staffListItemId);
                if(null == postTotalSalary) postTotalSalary = getNewPairInstance(0d, 0d);
                
                postTotalSalary.setX(postTotalSalary.getX() + budgetRatedSalary);
                postTotalSalary.setY(postTotalSalary.getY() + offBudgetRatedSalary);
                postTotalSalaryMap.put(staffListItemId, postTotalSalary);
                orgUnitSumTotalSalary.setX(orgUnitSumTotalSalary.getX() + budgetRatedSalary);
                orgUnitSumTotalSalary.setY(orgUnitSumTotalSalary.getY() + offBudgetRatedSalary);
            }
            model.getOrgUnitSalarysMap().put(orgUnitId, orgUnitSumSalary);
            model.getOrgUnitPostTotalSalaryMap().put(orgUnitId, postTotalSalaryMap);
            model.getOrgUnitSumTotalSalaryMap().put(orgUnitId, orgUnitSumTotalSalary);

            if (model.getSerializedOrgstructure().contains(model.getOrgUnitsMap().get(orgUnitId)))
            {
                model.getVuzSumSalary().setX(model.getVuzSumSalary().getX() + orgUnitSumSalary.getX());
                model.getVuzSumSalary().setY(model.getVuzSumSalary().getY() + orgUnitSumSalary.getY());
                model.getVuzSumTotalSalary().setX(model.getVuzSumTotalSalary().getX() + orgUnitSumTotalSalary.getX());
                model.getVuzSumTotalSalary().setY(model.getVuzSumTotalSalary().getY() + orgUnitSumTotalSalary.getY());
            }

            // Вычисление суммарных значений для подразделений, включая дочерние подразделения
            OrgUnit parent = model.getOrgUnitsMap().get(orgUnitId);
            while(null != parent)
            {
                // Вычисление месячного фонда оплаты труда по подразделению, включая дочерние подразделения
                setSingleLevelPairMapValuesInSafeMode(model.getOrgUnitWithChildsSalarysMap(), orgUnitSumSalary, 1, parent.getId());

                // Вычисление месячного фонда оплаты труда с надбавками по подразделению, включая дочерние подразделения
                setSingleLevelPairMapValuesInSafeMode(model.getOrgUnitWithChildsSumTotalSalaryMap(), orgUnitSumTotalSalary, 1, parent.getId());
                
                parent = parent.getParent();
            }
        }
    }
    
    /**
     * Подготавливает данные по штатному расписанию
     */
    public static void prepareDataForPrinting(Model model)
    {
        initStatisticVariables(model);
        prepareGeneralData(model);
        preparePaymentsData(model);
        prepareSummaryData(model);
    }
    
    /**
     * Подготавливает визуальные данные для вывода в печатную форму отчета
     */
    public static void renderTableData(Model model)
    {
        // Begin rows rendering
        List<String[]> data = new ArrayList<>();
        Map<Long, OrgUnit> parentsMap = new HashMap<>();
        Map<Long, Integer> orgUnitLevelsMap = new HashMap<>();
        List<RtfTableRowStyle> rowStyles = new ArrayList<>();
        
        OrgUnit lastOrgUnit = null;
        
        int cnt = 1;
        int prevLevel = - 1;
        for(OrgUnit orgUnit : model.getSerializedOrgstructure())
        {
            int level = -1;
            OrgUnit parent = orgUnit.getParent();
            
            while(null != parent)
            {
                level++;
                parent = parent.getParent();
            }
            
            if (level == 0)
            {
                parentsMap.put(orgUnit.getId(), null);
                cnt = 1;
            }
            else if(null != orgUnit.getParent() && parentsMap.containsKey(orgUnit.getParent().getId())) 
                parentsMap.put(orgUnit.getId(), orgUnit.getParent());
            
            orgUnitLevelsMap.put(orgUnit.getId(), level);
            
            {
                // Выводим стори "Итого с дочерними подразделениями" для всех подразделений более 
                // высокого уровня, чем последний из отрисованных
                if (prevLevel > level)
                {
                    OrgUnit prevOrgUnit = model.getSerializedOrgstructure().get(model.getSerializedOrgstructure().indexOf(orgUnit) - 1);
                    OrgUnit prevParentOrgUnit = parentsMap.get(prevOrgUnit.getId()); 
                    
                    while (null != prevParentOrgUnit && orgUnitLevelsMap.get(prevParentOrgUnit.getId()) >= level)
                    {
                        if (null == model.getActivityType())
                        {
                            data.add(generateTotalOrgUnitReportLine(model, "Итого по {orgUnitType_D} (бюджет):", prevParentOrgUnit, ORG_UNIT_WITH_CHILDS_SUM_ID, model.getOrgUnitWithChildsSalarysMap(), model.getOrgUnitWithChildsSumTotalSalaryMap(), model.getOrgUnitWithChildsSumPaymentsMap(), ACTIVITY_TYPE_BUDGET));
                            data.add(generateTotalOrgUnitReportLine(model, "Итого по {orgUnitType_D} (вне бюджета):", prevParentOrgUnit, ORG_UNIT_WITH_CHILDS_SUM_ID, model.getOrgUnitWithChildsSalarysMap(), model.getOrgUnitWithChildsSumTotalSalaryMap(), model.getOrgUnitWithChildsSumPaymentsMap(), ACTIVITY_TYPE_OFF_BUDGET));
                            data.add(generateTotalOrgUnitReportLine(model, "Итого по {orgUnitType_D}:", prevParentOrgUnit, ORG_UNIT_WITH_CHILDS_SUM_ID, model.getOrgUnitWithChildsSalarysMap(), model.getOrgUnitWithChildsSumTotalSalaryMap(), model.getOrgUnitWithChildsSumPaymentsMap(), null));
                            rowStyles.add(StaffListReportUtil.TOTAL_ROW_STYLE);
                            rowStyles.add(StaffListReportUtil.TOTAL_ROW_STYLE);
                            rowStyles.add(StaffListReportUtil.TOTAL_ROW_STYLE);
                        }
                        else
                        {
                            data.add(generateTotalOrgUnitReportLine(model, "Итого по {orgUnitType_D} (" + (ACTIVITY_TYPE_BUDGET.equals(model.getActivityType().getId()) ? "бюджет" : "вне бюджета") + "):", prevParentOrgUnit, ORG_UNIT_WITH_CHILDS_SUM_ID, model.getOrgUnitWithChildsSalarysMap(), model.getOrgUnitWithChildsSumTotalSalaryMap(), model.getOrgUnitWithChildsSumPaymentsMap(), model.getActivityType().getId()));
                            rowStyles.add(StaffListReportUtil.TOTAL_ROW_STYLE);
                        }
                        prevParentOrgUnit = parentsMap.get(prevParentOrgUnit.getId());
                    }
                }
                else if(model.getSerializedOrgstructure().indexOf(orgUnit) == model.getSerializedOrgstructure().size() - 1)
                {
                    if(level > 0) lastOrgUnit = orgUnit;
                }
            }

            OrgUnitTypeWrapper ouTypeWrapper = model.getOrgUnitTypesMap().get(orgUnit.getOrgUnitType().getId());
            if (null == ouTypeWrapper || ouTypeWrapper.isShowChildOrgUnits())
            {
                // Выводим строку с названием подразделения, с применением стиля соответствующего уровню подразделения в оргструктуре
                data.add(new String[] { null != orgUnit.getNominativeCaseTitle() ? orgUnit.getNominativeCaseTitle() : orgUnit.getFullTitle() });
                rowStyles.add(StaffListReportUtil.ORG_UNIT_HEADER_ROW_STYLES[level]);

                // Выводим строки по должностям подразделений
                if (model.getOrgUnitPostListsMap().containsKey(orgUnit.getId()))
                {
                    for (StaffListItem staffListItem : model.getOrgUnitPostListsMap().get(orgUnit.getId()))
                    {
                        Double budgetStaffRate;
                        Double offBudgetStaffRate;
                        if (staffListItem.getFinancingSource().equals(finSrcBudgetCatalogItem))
                        {
                            budgetStaffRate = staffListItem.getStaffRate();
                            offBudgetStaffRate = 0d;
                        }
                        else
                        {
                            budgetStaffRate = 0d;
                            offBudgetStaffRate = staffListItem.getStaffRate();
                        }

                        if (budgetStaffRate > 0)
                        {
                            if (null == model.getActivityType())
                            {
                                data.add(generateReportLine(model, cnt++, orgUnit.getId(), staffListItem.getId(), null, ACTIVITY_TYPE_BUDGET));
                                rowStyles.add(null);
                            }
                            else if (ACTIVITY_TYPE_BUDGET.equals(model.getActivityType().getId()))
                            {
                                data.add(generateReportLine(model, cnt++, orgUnit.getId(), staffListItem.getId(), ACTIVITY_TYPE_BUDGET, ACTIVITY_TYPE_BUDGET));
                                rowStyles.add(null);
                            }
                        }
                        if (offBudgetStaffRate > 0)
                        {
                            if (null == model.getActivityType())
                            {
                                data.add(generateReportLine(model, cnt++, orgUnit.getId(), staffListItem.getId(), null, ACTIVITY_TYPE_OFF_BUDGET));
                                rowStyles.add(null);
                            }
                            else if (ACTIVITY_TYPE_OFF_BUDGET.equals(model.getActivityType().getId()))
                            {
                                data.add(generateReportLine(model, cnt++, orgUnit.getId(), staffListItem.getId(), ACTIVITY_TYPE_OFF_BUDGET, ACTIVITY_TYPE_OFF_BUDGET));
                                rowStyles.add(null);
                            }
                        }
                    }

                    // выводим строку "итого" по подразделению
                    if (null == model.getActivityType())
                    {
                        data.add(generateTotalOrgUnitReportLine(model, "Итого по бюджету:", orgUnit, ORG_UNIT_SUM_ID, model.getOrgUnitSalarysMap(), model.getOrgUnitSumTotalSalaryMap(), model.getOrgUnitSumPaymentsMap(), ACTIVITY_TYPE_BUDGET));
                        data.add(generateTotalOrgUnitReportLine(model, "Итого вне бюджета:", orgUnit, ORG_UNIT_SUM_ID, model.getOrgUnitSalarysMap(), model.getOrgUnitSumTotalSalaryMap(), model.getOrgUnitSumPaymentsMap(), ACTIVITY_TYPE_OFF_BUDGET));
                        data.add(generateTotalOrgUnitReportLine(model, "Итого:", orgUnit, ORG_UNIT_SUM_ID, model.getOrgUnitSalarysMap(), model.getOrgUnitSumTotalSalaryMap(), model.getOrgUnitSumPaymentsMap(), null));
                        rowStyles.add(StaffListReportUtil.TOTAL_ROW_STYLE);
                        rowStyles.add(StaffListReportUtil.TOTAL_ROW_STYLE);
                        rowStyles.add(StaffListReportUtil.TOTAL_ROW_STYLE);
                    }
                    else
                    {
                        data.add(generateTotalOrgUnitReportLine(model, "Итого " + (ACTIVITY_TYPE_BUDGET.equals(model.getActivityType().getId()) ? "по бюджету" : "вне бюджета") + ":", orgUnit, ORG_UNIT_SUM_ID, model.getOrgUnitSalarysMap(), model.getOrgUnitSumTotalSalaryMap(), model.getOrgUnitSumPaymentsMap(), model.getActivityType().getId()));
                        rowStyles.add(StaffListReportUtil.TOTAL_ROW_STYLE);
                    }
                }
            }

            prevLevel = level;
        }

        if(null != lastOrgUnit && model.getSerializedOrgstructure().size() > 1)
        {
            OrgUnit prevOrgUnit = model.getSerializedOrgstructure().get(model.getSerializedOrgstructure().indexOf(lastOrgUnit) - 1);
            OrgUnit prevParentOrgUnit = parentsMap.get(prevOrgUnit.getId()); 

            while (null != prevParentOrgUnit && orgUnitLevelsMap.get(prevParentOrgUnit.getId()) >= 0)
            {
                if (null == model.getActivityType())
                {
                    data.add(generateTotalOrgUnitReportLine(model, "Итого по {orgUnitType_D} (бюджет):", prevParentOrgUnit, ORG_UNIT_WITH_CHILDS_SUM_ID, model.getOrgUnitWithChildsSalarysMap(), model.getOrgUnitWithChildsSumTotalSalaryMap(), model.getOrgUnitWithChildsSumPaymentsMap(), ACTIVITY_TYPE_BUDGET));
                    data.add(generateTotalOrgUnitReportLine(model, "Итого по {orgUnitType_D} (вне бюджета):", prevParentOrgUnit, ORG_UNIT_WITH_CHILDS_SUM_ID, model.getOrgUnitWithChildsSalarysMap(), model.getOrgUnitWithChildsSumTotalSalaryMap(), model.getOrgUnitWithChildsSumPaymentsMap(), ACTIVITY_TYPE_OFF_BUDGET));
                    data.add(generateTotalOrgUnitReportLine(model, "Итого по {orgUnitType_D}:", prevParentOrgUnit, ORG_UNIT_WITH_CHILDS_SUM_ID, model.getOrgUnitWithChildsSalarysMap(), model.getOrgUnitWithChildsSumTotalSalaryMap(), model.getOrgUnitWithChildsSumPaymentsMap(), null));
                    rowStyles.add(StaffListReportUtil.TOTAL_ROW_STYLE);
                    rowStyles.add(StaffListReportUtil.TOTAL_ROW_STYLE);
                    rowStyles.add(StaffListReportUtil.TOTAL_ROW_STYLE);
                }
                else
                {
                    data.add(generateTotalOrgUnitReportLine(model, "Итого по {orgUnitType_D} (" + (ACTIVITY_TYPE_BUDGET.equals(model.getActivityType().getId()) ? "бюджет" : "вне бюджета") + "):", prevParentOrgUnit, ORG_UNIT_WITH_CHILDS_SUM_ID, model.getOrgUnitWithChildsSalarysMap(), model.getOrgUnitWithChildsSumTotalSalaryMap(), model.getOrgUnitWithChildsSumPaymentsMap(), model.getActivityType().getId()));
                    rowStyles.add(StaffListReportUtil.TOTAL_ROW_STYLE);
                }
                prevParentOrgUnit = parentsMap.get(prevParentOrgUnit.getId());
            }
        }

        model.setTableData(data.toArray(new String[][]{}));
        model.setRowStyles(rowStyles.toArray(new RtfTableRowStyle[rowStyles.size()]));

        List<String[]> summaryDataList = new ArrayList<>();
        if(null == model.getActivityType())
        {
            summaryDataList.add(generateVuzTotalReportLine(model, "Общий итог по бюджету:", ACTIVITY_TYPE_BUDGET));
            summaryDataList.add(generateVuzTotalReportLine(model, "Общий итог вне бюджета:", ACTIVITY_TYPE_OFF_BUDGET));
            summaryDataList.add(generateVuzTotalReportLine(model, "Общий итог:", null));
        }
        else
        {
            summaryDataList.add(generateVuzTotalReportLine(model, "Общий итог " + (ACTIVITY_TYPE_BUDGET.equals(model.getActivityType().getId()) ? "по бюджету" : "вне бюджета") + ":", model.getActivityType().getId()));
        }

        model.setSummaryData(summaryDataList.toArray(new String[][] {}));
    }
    
    /**
     * Генерирует строку данных по должности для таблицы.
     */
    public static String[] generateReportLine(Model model, int cnt, Long orgUnitId, Long staffListItemId, Long activityTypeId, Long postActivityTypeId)
    {
        String[] line = new String[model.getColumnsNumber()];

        line[0] = cnt + ".";
        line[1] = model.getPostDataMap().get(staffListItemId)[0];
        line[2] = model.getPostDataMap().get(staffListItemId)[1];
        line[3] = model.getPostDataMap().get(staffListItemId)[2];
        
        if(ACTIVITY_TYPE_BUDGET.equals(postActivityTypeId))
        {
            line[4] = model.getDblFormatter().format(model.getOrgUnitPostBudgetStaffRatesMap().get(orgUnitId).get(staffListItemId));
            line[5] = "Б";
        }
        else
        {
            line[4] = model.getDblFormatter().format(model.getOrgUnitPostOffBudgetStaffRatesMap().get(orgUnitId).get(staffListItemId));
            line[5] = "ВБС";
        }
        
        line[6] = model.getDblFormatter().format(null != model.getPostSalarysMap().get(staffListItemId) ? model.getPostSalarysMap().get(staffListItemId) : 0d);
        
        // Надбавки, доплаты
        int columnNumber = PAYMENTS_COLUMN_IDX;
        if (!model.getPaymentHeadersList().isEmpty())
        {
            Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> orgUnitPaymentsMap = model.getOrgUnitPostPaymentsMap().get(orgUnitId);
            Map<Long, CoreCollectionUtils.Pair<Double, Double>> paymentsMap = (null != orgUnitPaymentsMap && orgUnitPaymentsMap.containsKey(staffListItemId)) ? orgUnitPaymentsMap.get(staffListItemId) : new HashMap<>();
            for (Payment payment : model.getPaymentHeadersList()) //TODO activityType
                line[columnNumber++] = getPrintableValue(model.getDblFormatter(), paymentsMap.get(payment.getId()), activityTypeId, true);
        }
        else
        {
            columnNumber++;
        }
        
        line[columnNumber++] = null != model.getOrgUnitPostSalarysMap().get(orgUnitId) ? getPrintableValue(model.getDblFormatter(), model.getOrgUnitPostSalarysMap().get(orgUnitId).get(staffListItemId), activityTypeId, false) : model.getDblFormatter().format(0d);
        line[columnNumber++] = null != model.getOrgUnitPostTotalSalaryMap().get(orgUnitId) ? getPrintableValue(model.getDblFormatter(), model.getOrgUnitPostTotalSalaryMap().get(orgUnitId).get(staffListItemId), activityTypeId, false) : model.getDblFormatter().format(0d);

        return line;
    }
    
    /**
     * Генерирует строку итоговых данных по подразделению.
     */
    public static String[] generateTotalOrgUnitReportLine(Model model, String title, OrgUnit orgUnit, Long sumLevel, Map<Long, CoreCollectionUtils.Pair<Double, Double>> orgUnitSalarysMap, Map<Long, CoreCollectionUtils.Pair<Double, Double>> orgUnitTotalSalarysMap, Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> orgUnitPaymentsMap, Long activityTypeId)
    {
        String[] line = new String[model.getColumnsNumber()];
        double sumBudgetStaffRate = null != model.getOrgUnitPostBudgetStaffRatesMap().get(orgUnit.getId()) ? model.getOrgUnitPostBudgetStaffRatesMap().get(orgUnit.getId()).get(sumLevel) : 0d;
        double sumOffBudgetStaffRate = null != model.getOrgUnitPostOffBudgetStaffRatesMap().get(orgUnit.getId()) ? model.getOrgUnitPostOffBudgetStaffRatesMap().get(orgUnit.getId()).get(sumLevel) : 0d;

        line[0] = title.replace("{orgUnitType_D}", orgUnit.getOrgUnitType().getDative());
        
        if(null == activityTypeId) line[4] = model.getDblFormatter().format(sumBudgetStaffRate + sumOffBudgetStaffRate);
        else if(ACTIVITY_TYPE_BUDGET.equals(activityTypeId))
            line[4] = sumBudgetStaffRate != 0d ? model.getDblFormatter().format(sumBudgetStaffRate) : "";
        else if(ACTIVITY_TYPE_OFF_BUDGET.equals(activityTypeId))
            line[4] = sumOffBudgetStaffRate != 0d ? model.getDblFormatter().format(sumOffBudgetStaffRate) : "";
            
        // Надбавки, доплаты
        int columnNumber = PAYMENTS_COLUMN_IDX;
        if (!model.getPaymentHeadersList().isEmpty())
        {
            Map<Long, CoreCollectionUtils.Pair<Double, Double>> paymentsMap = orgUnitPaymentsMap.containsKey(orgUnit.getId()) ? orgUnitPaymentsMap.get(orgUnit.getId()) : new HashMap<>();
            for (Payment payment : model.getPaymentHeadersList())
                line[columnNumber++] = paymentsMap.containsKey(payment.getId()) ? getPrintableValue(model.getDblFormatter(), paymentsMap.get(payment.getId()), activityTypeId, false) : "";
        }
        else
        {
            columnNumber++;
        }
        
        line[columnNumber++] = getPrintableValue(model.getDblFormatter(), orgUnitSalarysMap.get(orgUnit.getId()), activityTypeId, false);
        line[columnNumber++] = getPrintableValue(model.getDblFormatter(), orgUnitTotalSalarysMap.get(orgUnit.getId()), activityTypeId, false);

        return line;
    }
    
    /**
     * Генерирует строку итоговых данных по ОУ в целом.
     */
    public static String[] generateVuzTotalReportLine(Model model, String title, Long activityTypeId)
    {
        String[] line = new String[model.getColumnsNumber()];
        
        line[0] = title;
        if(null == activityTypeId) line[3] = model.getDblFormatter().format(model.getVuzBudgetStaffRate() + model.getVuzOffBudgetStaffRate());
        else line[3] = model.getDblFormatter().format(ACTIVITY_TYPE_BUDGET.equals(activityTypeId) ? model.getVuzBudgetStaffRate() : model.getVuzOffBudgetStaffRate());
        
        // Надбавки, доплаты
        int columnNumber = PAYMENTS_COLUMN_IDX - 1;
        if (!model.getPaymentHeadersList().isEmpty())
        {
            for (Payment payment : model.getPaymentHeadersList())
                line[columnNumber++] = getPrintableValue(model.getDblFormatter(), model.getVuzSumPaymentsMap().get(payment.getId()), activityTypeId, false);
        }
        else
        {
            columnNumber++;
        }
        
        line[columnNumber++] = getPrintableValue(model.getDblFormatter(), model.getVuzSumSalary(), activityTypeId, false);
        line[columnNumber++] = getPrintableValue(model.getDblFormatter(), model.getVuzSumTotalSalary(), activityTypeId, false);

        return line;
    }
    
    /**
     * Применяет указанный стиль к строке RTF-таблицы
     */
    public static void applyRtfRowStyles(RtfRow row, RtfTableRowStyle style)
    {
        if(style.isMergeAllRowCells()) RtfUtil.unitAllCell(row, 0);
        if(null != style.getFontSize()) createCorrectRtfStyle(row, IRtfData.FS, style.getFontSize(), 14, style.isMergeAllRowCells());
        if(null != style.getShift()) createRtfStyle(row, IRtfData.FI, style.getShift());
        if(style.isBold()) createCorrectRtfStyle(row, IRtfData.B, null, null, style.isMergeAllRowCells());
        if(style.isItalic()) createCorrectRtfStyle(row, IRtfData.I, null, null, style.isMergeAllRowCells());
        if(null != style.getTextAlign()) createCorrectRtfStyle(row, style.getTextAlign(), null, null, style.isMergeAllRowCells());
        if(null != style.getRowHeight()) row.getFormatting().setHeight(style.getRowHeight());
        
        if(!style.isMergeAllRowCells() && null != style.getMergingArr())
        {
            List<RtfCell> cellsToRemove = new ArrayList<>();
            for(Integer[] singleMerge : style.getMergingArr())
            {
                int destCellWidth = 0;
                for(int i = singleMerge[0]; i < singleMerge[0] + singleMerge[1]; i++)
                {
                    RtfCell cell = row.getCellList().get(i);
                    destCellWidth += cell.getWidth();
                    if(i != singleMerge[0]) cellsToRemove.add(cell);
                }
                row.getCellList().get(singleMerge[0]).setWidth(destCellWidth);
            }
            row.getCellList().removeAll(cellsToRemove);
        }
        
        if(null != style.getBorderWidths())
        {
            for (RtfCell cell : row.getCellList())
            {
                RtfCellBorder cellBorder = cell.getCellBorder();
                
                if(null != cellBorder.getTop())
                {
                    RtfBorder oldTop = cellBorder.getTop();
                    RtfBorder newTop = RtfBorder.getInstance(style.getBorderWidths()[0], oldTop.getColorIndex(), oldTop.getStyle());
                    cellBorder.setTop(newTop);
//                            setWidth(style.getBorderWidths()[0]);
                }
                if(null != cellBorder.getBottom())
                {
                    RtfBorder oldBottom = cellBorder.getBottom();
                    RtfBorder newBottom = RtfBorder.getInstance(style.getBorderWidths()[2], oldBottom.getColorIndex(), oldBottom.getStyle());
                    cellBorder.setBottom(newBottom);
//                            setWidth(style.getBorderWidths()[2]);
                }
                
                if(null != cellBorder.getLeft() && row.getCellList().indexOf(cell) == 0)
                {
                    RtfBorder oldLeft = cellBorder.getLeft();
                    RtfBorder newLeft = RtfBorder.getInstance(style.getBorderWidths()[3], oldLeft.getColorIndex(), oldLeft.getStyle());
                    cellBorder.setLeft(newLeft);
//                            setWidth(style.getBorderWidths()[3]);
                }
                
                if(null != cellBorder.getRight() && row.getCellList().indexOf(cell) == row.getCellList().size() - 1)
                {
                    RtfBorder oldRight = cellBorder.getRight();
                    RtfBorder newRight = RtfBorder.getInstance(style.getBorderWidths()[3], oldRight.getColorIndex(), oldRight.getStyle());
                    cellBorder.setRight(newRight);
//                            setWidth(style.getBorderWidths()[3]);
                }
            }
        }
    }

    /**
     * Рассчитывает сумму выплат для выплаты на МФОТ. Учитывает Ист.фин., настройку Порядок начислений выплат на МФОТ и не берет разовые выплаты.<p/>
     * Если настройка не заполнена, то учитываются все выплаты не на МФОТ.
     * @param basePayment выплата на МФОТ, для которой расчитывается сумма выплат
     * @param paymentValueMap мапа выплаты и ее суммы
     * @param fullSalaryPaymentList список выплат на МФОТ
     * @return Пару бюджет\вснебюджет Сумм выплат актуальную для указаной выплаты.
     */
    private static CoreCollectionUtils.Pair<Double, Double> calculatePaymentSumm(StaffListPaymentBase basePayment, Map<Long, CoreCollectionUtils.Pair<Double, Double>> paymentValueMap, List<StaffListPaymentBase> fullSalaryPaymentList)
    {
        CoreCollectionUtils.Pair<Double, Double> resilt = new CoreCollectionUtils.Pair<>(0d, 0d);

        List<Payment> dependPaymentList = UniempDaoFacade.getUniempDAO().getPaymentOnFOTToPaymentsList(basePayment.getPayment());

        if (dependPaymentList.isEmpty())
        {
            for (Map.Entry<Long, CoreCollectionUtils.Pair<Double, Double>> paymentEntry : paymentValueMap.entrySet())
            {
                StaffListPaymentBase innerPayment = IUniBaseDao.instance.get().get(StaffListPaymentBase.class, paymentEntry.getKey());

                if (!isTakePayment(basePayment, innerPayment))
                    continue;

                resilt.setX(resilt.getX() + paymentEntry.getValue().getX());
                resilt.setY(resilt.getY() + paymentEntry.getValue().getY());
            }
        }
        else
        {
            for (Map.Entry<Long, CoreCollectionUtils.Pair<Double, Double>> paymentEntry : paymentValueMap.entrySet())
            {
                StaffListPaymentBase innerPayment = IUniBaseDao.instance.get().get(StaffListPaymentBase.class, paymentEntry.getKey());

                if (!dependPaymentList.contains(innerPayment.getPayment()))
                    continue;

                if (!isTakePayment(basePayment, innerPayment))
                    continue;

                resilt.setX(resilt.getX() + paymentEntry.getValue().getX());
                resilt.setY(resilt.getY() + paymentEntry.getValue().getY());
            }

            for (StaffListPaymentBase innerBasePayment : fullSalaryPaymentList)
            {
                if (!innerBasePayment.getStaffListItem().equals(basePayment.getStaffListItem()))
                    continue;

                if (!dependPaymentList.contains(innerBasePayment.getPayment()))
                    continue;

                if (!isTakePayment(basePayment, innerBasePayment))
                    continue;

                if(innerBasePayment.getFinancingSource().getCode().equals(UniempDefines.FINANCING_SOURCE_BUDGET))
                    resilt.setX(resilt.getX() + innerBasePayment.getTotalValue());
                else
                    resilt.setY(resilt.getY() + innerBasePayment.getTotalValue());
            }
        }

        return resilt;
    }

    private static boolean isTakePayment(StaffListPaymentBase payment, StaffListPaymentBase possiblePayment)
    {
        if (possiblePayment.getPayment().isOneTimePayment())
            return false;

        CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem> innerKey = new CoreCollectionUtils.Pair<>(possiblePayment.getFinancingSource(), possiblePayment.getFinancingSourceItem());
        CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem> key = new CoreCollectionUtils.Pair<>(payment.getFinancingSource(), payment.getFinancingSourceItem());

        return innerKey.equals(key);
    }
    
    /**
     * Добавляет одиночный RTF-тэг ячейкам указанной табличной строки 
     */
    private static void createRtfStyle(RtfRow row, int controlIndex, Integer value)
    {
        IRtfControl contr = (null != value) ? RtfBean.getElementFactory().createRtfControl(controlIndex, value) :
                RtfBean.getElementFactory().createRtfControl(controlIndex);
//        if(null != value) contr.setValue(value);
        row.getCellList().get(0).getElementList().add(0, contr);
    }
    
    /**
     * Добавляет парный RTF-тэг ячейкам указанной табличной строки (открытие и закрытие)
     */
    private static void createCorrectRtfStyle(RtfRow row, int controlIndex, Integer value, Integer oldValue, boolean applyToSingleCell)
    {
        RtfCell firstRowCell = row.getCellList().get(0);
        RtfCell lastRowCell = row.getCellList().get(row.getCellList().size() - 1);
        
        IRtfControl contrO = (null != value) ? RtfBean.getElementFactory().createRtfControl(controlIndex, value) :
                RtfBean.getElementFactory().createRtfControl(controlIndex);
//        if(null != value) contrO.setValue(value);
        firstRowCell.getElementList().add(0, contrO);
        
        IRtfControl contrC = (null != oldValue) ? RtfBean.getElementFactory().createRtfControl(controlIndex, oldValue) :
                RtfBean.getElementFactory().createRtfControl(controlIndex, 0);
//        if(null != oldValue) contrC.setValue(oldValue);
//        else contrC.setValue(0);
        
        if(applyToSingleCell) firstRowCell.getElementList().add(contrC);
        else lastRowCell.getElementList().add(contrC);
    }
    
    /**
     * Вспомогательный метод для работы со структурами мапов для одиночной степени вложенности, 
     * призванный уменьшить число строк в коде
     */
    private static void setSingleLevelDoubleMapValuesInSafeMode(Map<Long, Double> map, Double value, int operation, Long id)
    {
        Double totalValue = map.get(id);
        if (null == totalValue) totalValue = 0d;

        switch (operation)
        {
        case 0: totalValue = value; break;
        case 1: totalValue += null != value ? value : 0d; break;
        default: totalValue = value; break;
        }

        map.put(id, totalValue);
    }
    
    /**
     * Вспомогательный метод для работы со структурами мапов для двойной степени вложенности, 
     * призванный уменьшить число строк в коде
     */
    private static void setTwoLevelsDoubleMapValuesInSafeMode(Map<Long, Map<Long, Double>> map, Double value, int operation, Long id0, Long id1)
    {
        Map<Long, Double> l1Map = map.get(id0);
        if (null == l1Map) l1Map = new HashMap<>();
        setSingleLevelDoubleMapValuesInSafeMode(l1Map, value, operation, id1);
        map.put(id0, l1Map);
    }
    
    /**
     * Вспомогательный метод для работы со структурами мапов для одиночной степени вложенности, 
     * призванный уменьшить число строк в коде
     */
    private static void setSingleLevelPairMapValuesInSafeMode(Map<Long, CoreCollectionUtils.Pair<Double, Double>> map, CoreCollectionUtils.Pair<Double, Double> value, int operation, Long id)
    {
        CoreCollectionUtils.Pair<Double, Double> totalValue = map.get(id);
        if (null == totalValue) totalValue = new CoreCollectionUtils.Pair<>(0d, 0d);

        switch (operation)
        {
        case 0: totalValue = value; break;
        case 1: 
            totalValue.setX(totalValue.getX() + (null != value ? value.getX() : 0d)); 
            totalValue.setY(totalValue.getY() + (null != value ? value.getY() : 0d));
            break;
        default: totalValue = value; break;
        }

        map.put(id, totalValue);
    }
    
    /**
     * Вспомогательный метод для работы со структурами мапов для двойной степени вложенности, 
     * призванный уменьшить число строк в коде
     */
    private static void setTwoLevelsPairMapValuesInSafeMode(Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> map, CoreCollectionUtils.Pair<Double, Double> value, int operation, Long id0, Long id1)
    {
        Map<Long, CoreCollectionUtils.Pair<Double, Double>> l1Map = map.get(id0);
        if (null == l1Map) l1Map = new HashMap<>();
        setSingleLevelPairMapValuesInSafeMode(l1Map, value, operation, id1);
        map.put(id0, l1Map);
    }
    
    /**
     * Вспомогательный метод для работы со структурами мапов для тройной степени вложенности, 
     * призванный уменьшить число строк в коде
     */
    private static void setThreeLevelsPairMapValuesInSafeMode(Map<Long, Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>>> map, CoreCollectionUtils.Pair<Double, Double> value, int operation, Long id0, Long id1, Long id2)
    {
        Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>> l1Map = map.get(id0);
        if (null == l1Map) l1Map = new HashMap<>();
        setTwoLevelsPairMapValuesInSafeMode(l1Map, value, operation, id1, id2);
        map.put(id0, l1Map);
    }
    
//    @SuppressWarnings("unchecked")
//    private static final CoreCollectionUtils.Pair<Double, Double> getMultiLevelMapValue(Map<Long, Map<Long, Map<Long, CoreCollectionUtils.Pair<Double, Double>>>> map, Long orgUnitId, Long staffListItemId, Long paymentId)
//    {
//        if(!map.containsKey(orgUnitId) || !map.get(orgUnitId).containsKey(staffListItemId) || !map.get(orgUnitId).get(staffListItemId).containsKey(paymentId))
//            return new CoreCollectionUtils.Pair<Double, Double>(0d, 0d);
//
//        return map.get(orgUnitId).get(staffListItemId).get(paymentId);
//    }
//    
    private static CoreCollectionUtils.Pair<Double, Double> getNewPairInstance(Double X, Double Y)
    {
        return new CoreCollectionUtils.Pair<>(X, Y);
    }
    
    private static String getPrintableValue(DoubleFormatter formatter, CoreCollectionUtils.Pair<Double, Double> pair, Long activityType, boolean replaceZeroesWithSpace)
    {
        Double result = 0d;
        if(null != pair)
        {
            if(null == activityType) result = (null != pair.getX() ? pair.getX() : 0d) + (null != pair.getY() ? pair.getY() : 0d);
            else if(ACTIVITY_TYPE_BUDGET.equals(activityType)) result = null != pair.getX() ? pair.getX() : 0d;
            else result = null != pair.getY() ? pair.getY() : 0d;
        }
        return result.equals(0d) && replaceZeroesWithSpace ? "" : formatter.format(result);
    }

}