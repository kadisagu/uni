/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffListAllocItem.StaffListAllocItemAddEdit;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.SalaryRaisingCoefficient;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.employee.*;
import ru.tandemservice.uniemp.util.StaffListPaymentsUtil;

import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eqValue;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author dseleznev
 * Created on: 08.04.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        if (null != model.getStaffListId())
            model.setStaffList(get(StaffList.class, model.getStaffListId()));

        if (null != model.getStaffListAllocationItemId())
        {
            model.setStaffListAllocationItem(get(StaffListAllocationItem.class, model.getStaffListAllocationItemId()));
            model.setEmployeePost(new DataWrapper(model.getStaffListAllocationItem().isCombination() ? model.getStaffListAllocationItem().getCombinationPost() : model.getStaffListAllocationItem().getEmployeePost()));
            model.setCantBeEdited(model.getStaffListAllocationItem().getStaffListItem().getStaffList().isCantBeEdited());
            model.setStaffList(model.getStaffListAllocationItem().getStaffListItem().getStaffList());
            model.setReserve(model.getStaffListAllocationItem().isReserve());
        }

        if (model.getReserve() == null)
            model.setReserve(false);

        if (model.getReserve())
            model.setStaffRateStr("Количество штатных единиц (резерв)");
        else
            model.setStaffRateStr("Количество штатных единиц");

        model.setStaffListItemsListModel(new BaseSingleSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                // TODO: take into account occupied staffRates for post
                MQBuilder builder = new MQBuilder(StaffListItem.ENTITY_CLASS, "sl");
                builder.add(MQExpression.eq("sl", StaffListItem.L_STAFF_LIST, model.getStaffList()));
                builder.add(MQExpression.notIn("sl", StaffListItem.P_ID, StaffListPaymentsUtil.getInvalidPostIds(model.getStaffList(), getSession()/*, true, true*/)));
                builder.add(MQExpression.eq("sl", StaffListItem.L_FINANCING_SOURCE, model.getStaffListAllocationItem().getFinancingSource()));
                builder.add(MQExpression.eq("sl", StaffListItem.L_FINANCING_SOURCE_ITEM, model.getStaffListAllocationItem().getFinancingSourceItem()));
                builder.add(MQExpression.like("sl", StaffListItem.L_ORG_UNIT_POST_RELATION + "." + OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.P_TITLE, CoreStringUtils.escapeLike(filter)));
                builder.addOrder("sl", StaffListItem.L_ORG_UNIT_POST_RELATION + "." + OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.P_TITLE);
                List<StaffListItem> list = builder.getResultList(getSession());
                return new ListResult<>(list, builder.getResultCount(getSession()));
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                StaffListItem staffListItem = get(StaffListItem.class, (Long) primaryKey);
                if (findValues("").getObjects().contains(staffListItem))
                    return staffListItem;

                return null;
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((StaffListItem)value).getOrgUnitPostRelation().getTitleWithSalary();
            }
        });

        model.setEmployeePostsListModel(new BaseSingleSelectModel()
        {
            @Override
            public ListResult<DataWrapper> findValues(String filter)
            {
                if (null == model.getStaffListAllocationItem().getStaffListItem() || 0 == model.getStaffListAllocationItem().getStaffRate())
                    return ListResult.getEmpty();

                OrgUnit orgUnit = model.getStaffList().getOrgUnit();
                PostBoundedWithQGandQL postBoundedWithQGandQL = model.getStaffListAllocationItem().getStaffListItem().getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL();
                FinancingSource financingSource = model.getStaffListAllocationItem().getFinancingSource();
                FinancingSourceItem financingSourceItem = model.getStaffListAllocationItem().getFinancingSourceItem();
                double staffRate = model.getStaffListAllocationItem().getStaffRate();

                DQLSelectBuilder epBuilder = new DQLSelectBuilder().fromEntity(EmployeePostStaffRateItem.class, "r").column(property(EmployeePostStaffRateItem.employeePost().fromAlias("r")))
                        .where(eqValue(property(EmployeePostStaffRateItem.employeePost().orgUnit().fromAlias("r")), orgUnit))
                        .where(eqValue(property(EmployeePostStaffRateItem.employeePost().postRelation().postBoundedWithQGandQL().fromAlias("r")), postBoundedWithQGandQL))
                        .where(eqValue(property(EmployeePostStaffRateItem.financingSource().fromAlias("r")), financingSource))
                        .where(eqValue(property(EmployeePostStaffRateItem.financingSourceItem().fromAlias("r")), financingSourceItem))
                        .where(eqValue(property(EmployeePostStaffRateItem.staffRateInteger().fromAlias("r")), staffRate * 10000));

                DQLSelectBuilder cpBuilder = new DQLSelectBuilder().fromEntity(CombinationPostStaffRateItem.class, "c").column(property(CombinationPostStaffRateItem.combinationPost().fromAlias("c")))
                        .where(eqValue(property(CombinationPostStaffRateItem.combinationPost().orgUnit().fromAlias("c")), orgUnit))
                        .where(eqValue(property(CombinationPostStaffRateItem.combinationPost().postBoundedWithQGandQL().fromAlias("c")), postBoundedWithQGandQL))
                        .where(eqValue(property(CombinationPostStaffRateItem.financingSource().fromAlias("c")), financingSource))
                        .where(eqValue(property(CombinationPostStaffRateItem.financingSourceItem().fromAlias("c")), financingSourceItem))
                        .where(eqValue(property(CombinationPostStaffRateItem.staffRateInteger().fromAlias("c")), staffRate * 10000));

                List<DataWrapper> wrapperList = new ArrayList<>();
                for (EmployeePost post : epBuilder.createStatement(getSession()).<EmployeePost>list())
                    wrapperList.add(new DataWrapper(post));

                for (CombinationPost post : cpBuilder.createStatement(getSession()).<CombinationPost>list())
                    wrapperList.add(new DataWrapper(post));

                return new ListResult<>(wrapperList);
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                for (DataWrapper wrapper : findValues("").getObjects())
                    if (wrapper.getId().equals(primaryKey))
                        return wrapper;

                return null;
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                Object object = ((DataWrapper) value).getWrapped();
                if (object instanceof CombinationPost)
                {
                    CombinationPost combinationPost = (CombinationPost) object;
                    return combinationPost.getEmployeePost().getFio() + " - " + combinationPost.getCombinationPostType().getShortTitle() + " " + combinationPost.getEmployeePost().getPostStatus().getShortTitle();
                }
                else if (object instanceof EmployeePost)
                {
                    EmployeePost employeePost = (EmployeePost) object;
                    return employeePost.getFio() + " - " + employeePost.getPostType().getShortTitle() + " " + employeePost.getPostStatus().getShortTitle();
                }

                return null;
            }
        });

        model.setRaisingCoefficientListModel(new BaseSingleSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (null == model.getStaffListAllocationItem().getStaffListItem().getOrgUnitPostRelation())
                {
                    return ListResult.getEmpty();
                }
                PostBoundedWithQGandQL pbw = model.getStaffListAllocationItem().getStaffListItem().getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL();
                MQBuilder builder = new MQBuilder(SalaryRaisingCoefficient.ENTITY_CLASS, "rc");
                builder.add(MQExpression.eq("rc", SalaryRaisingCoefficient.L_POST, pbw.getPost()));
                builder.add(MQExpression.eq("rc", SalaryRaisingCoefficient.L_QUALIFICATION_LEVEL, pbw.getQualificationLevel()));
                builder.add(MQExpression.like("rc", SalaryRaisingCoefficient.P_TITLE, CoreStringUtils.escapeLike(filter)));
                builder.addOrder("rc", SalaryRaisingCoefficient.P_RAISING_COEFFICIENT);
                builder.addOrder("rc", SalaryRaisingCoefficient.P_TITLE);
                return new ListResult<>(builder.getResultList(getSession()));
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                IEntity entity = get(SalaryRaisingCoefficient.class, (Long)primaryKey);
                if (findValues("").getObjects().contains(entity)) return entity;
                model.getStaffListAllocationItem().setRaisingCoefficient(null);
                return null;
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((SalaryRaisingCoefficient) value).getFullTitle();
            }
        });


        model.setFinancingSourceItemList(new BaseSingleSelectModel()
        {
            @Override
            public Object getValue(Object primaryKey)
            {
                FinancingSourceItem item = get((Long) primaryKey);

                if (findValues("").getObjects().contains(item))
                    return item;

                return null;
            }

            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(FinancingSourceItem.ENTITY_CLASS, "b");
                builder.add(MQExpression.eq("b", FinancingSourceItem.L_FINANCING_SOURCE, model.getStaffListAllocationItem().getFinancingSource()));
                builder.add(MQExpression.like("b", FinancingSourceItem.P_TITLE, CoreStringUtils.escapeLike(filter)));

                return new ListResult<>(builder.<FinancingSourceItem>getResultList(getSession()));
            }
        });

        model.setFinancingSourcesList(getCatalogItemList(FinancingSource.class));
    }

    @Override
    public void update(Model model)
    {
        model.getStaffListAllocationItem().setReserve(model.getReserve());

        if (model.getEmployeePost() != null)
        {
            if (model.getEmployeePost().getWrapped() instanceof CombinationPost)
            {
                CombinationPost combinationPost = model.getEmployeePost().getWrapped();
                model.getStaffListAllocationItem().setEmployeePost(combinationPost.getEmployeePost());
                model.getStaffListAllocationItem().setCombinationPost(combinationPost);
                model.getStaffListAllocationItem().setCombination(true);
            }
            else if (model.getEmployeePost().getWrapped() instanceof EmployeePost)
            {
                EmployeePost employeePost = model.getEmployeePost().getWrapped();
                model.getStaffListAllocationItem().setEmployeePost(employeePost);
                model.getStaffListAllocationItem().setCombinationPost(null);
                model.getStaffListAllocationItem().setCombination(false);
            }
        }

        getSession().saveOrUpdate(model.getStaffListAllocationItem());
        StaffListPaymentsUtil.recalculateAllPaymentsValue(model.getStaffListAllocationItem(), getSession());
    }
    /*
    private List<OrgUnitPostRelation> getOrgUnitPostsList(StaffList staffList, Long staffListAllocationItemId)
    {
        List<Long> excludedPostIds = StaffListPaymentsUtil.getInvalidPostIds(staffList, getSession(), true, true);
        MQBuilder builder = new MQBuilder(StaffListItem.ENTITY_CLASS, "sli", new String[] { StaffListItem.L_ORG_UNIT_POST_RELATION });
        builder.add(MQExpression.eq("sli", StaffListItem.L_STAFF_LIST, staffList));
        builder.add(MQExpression.notIn("sli", StaffListItem.L_ORG_UNIT_POST_RELATION + ".id", excludedPostIds));
        builder.addOrder("sli", StaffListItem.L_ORG_UNIT_POST_RELATION + "." + OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_POST + "." + Post.P_TITLE);
        return builder.getResultList(getSession());
    }*/

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        IDataSettings settings = DataSettingsFacade.getSettings("general", UniempDefines.STAFF_RATE_STEP_SETTINGS_PREFIX);
        Double minStaffRate = null != settings.get(UniempDefines.STAFF_RATE_STEP_POST_SETING_NAME) ? (Double)settings.get(UniempDefines.STAFF_RATE_STEP_POST_SETING_NAME) : 0d;

        StaffListAllocationItem item = model.getStaffListAllocationItem();

        Double staffRate = item.getStaffRate();

        if (staffRate <= 0)
            errors.add("Количество штатных единиц не может быть меньше или равно нулю.", "staffRate");

        if (staffRate > 1 && !model.getReserve())
            errors.add("Количество штатных единиц не должно превышать 1.", "staffRate");

        Double reminder = Math.round((item.getStaffRate() % minStaffRate) * 100) / 100d;
        if (reminder != 0 && !minStaffRate.equals(reminder))
            errors.add("Количество штатных единиц должно быть кратно " + minStaffRate + ".", "offStaffRate");

        if (!item.getFinancingSource().equals(item.getStaffListItem().getFinancingSource()))
            if (item.getFinancingSourceItem()  != null && !item.getFinancingSourceItem().equals(item.getStaffListItem().getFinancingSourceItem()))
                errors.add("В Штатном расписании не достаточно количество штатных единиц с указанным источником финансирования.", "financingSource", "financingSourceItem");
            else
                errors.add("В Штатном расписании не достаточно количество штатных единиц с указанным источником финансирования.", "financingSource");

        if (!StaffListPaymentsUtil.isStaffListAllocationItemValid(model.getStaffListAllocationItem(), getSession()))
            errors.add("Превышено максимально допустимое количество штатных единиц.", "staffRate");
    }
}