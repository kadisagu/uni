/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.qualityConsistencyFull.Add;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.Post;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeeTypeCodes;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.OrgUnitTypeRestriction;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.ScienceDegreeType;
import org.tandemframework.shared.person.catalog.entity.ScienceStatusType;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.IUniempDAO;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.EmployeeTemplateDocument;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.uniemp.util.UniempReportUtil;

import java.util.*;

/**
 * @author dseleznev
 * Created on: 16.04.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final int FACULTY_HEADER = 1;
    private static final int CATHEDRA_HEADER = 2;
    private static final int SECOND_JOB_HEADER = 3;
    private static final int TOTAL_HEADER = 4;

    @Override
    public Integer preparePrintReport(Model model)
    {
        RtfInjectModifier paramModifier = new RtfInjectModifier();
        paramModifier.put("reportDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getReportDate()));

        RtfTableModifier tableModifier = new RtfTableModifier();
        final Map<Integer, Integer> stylesMap = new HashMap<>();
        tableModifier.getName2data().putAll(prepareTablesData(model.getReportDate(), stylesMap));
        tableModifier.put("T", new RtfRowIntercepterBase()
        {
            // Format rows according to template
            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                int cnt = -startIndex;
                for(RtfRow row : newRowList)
                {
                    processRtfRow(row, stylesMap, cnt);
                    cnt++;
                }
            }
        });

        return UniempReportUtil.preparePrintingReport(getCatalogItem(EmployeeTemplateDocument.class, UniempDefines.TEMPLATE_QUALITY_CONSISTENCY_FULL), paramModifier, tableModifier);
    }

    private void processRtfRow(RtfRow row, Map<Integer, Integer> stylesMap, Integer cnt)
    {
        if(stylesMap.containsKey(cnt))
        {
            if(FACULTY_HEADER == stylesMap.get(cnt) || CATHEDRA_HEADER == stylesMap.get(cnt) || SECOND_JOB_HEADER == stylesMap.get(cnt))
            {
                RtfUtil.unitAllCell(row, 0);
            }

            if(FACULTY_HEADER == stylesMap.get(cnt))
            {
                RtfCell cell = row.getCellList().get(0);
                cell.getElementList().add(0, RtfBean.getElementFactory().createRtfControl(IRtfData.QC));
                cell.getElementList().add(0, RtfBean.getElementFactory().createRtfControl(IRtfData.I));
                IRtfControl contrI = RtfBean.getElementFactory().createRtfControl(IRtfData.I, 0);
//                contrI.setValue(0);
                cell.getElementList().add(contrI);
                IRtfControl contrQC = RtfBean.getElementFactory().createRtfControl(IRtfData.QC, 0);
//                contrQC.setValue(0);
                cell.getElementList().add(contrQC);
                //row.getFormatting().setRowAlignment(RowAlignment.CENTER);
            }

            // Устанавливаем жирным любую строчку, которая имеет стиль
            row.getCellList().get(0).getElementList().add(0, RtfBean.getElementFactory().createRtfControl(IRtfData.B));
            IRtfControl contrB = RtfBean.getElementFactory().createRtfControl(IRtfData.B, 0);
//            contrB.setValue(0);
            row.getCellList().get(row.getCellList().size() - 1).getElementList().add(contrB);
        }
    }

    private Map<String, String[][]> prepareTablesData(Date reportDate, Map<Integer, Integer> stylesMap)
    {
        Map<String, String[][]> result = new HashMap<>();

        List<OrgUnit> parentOrgUnitsList = getParentOrgUnits();
        Map<OrgUnit, List<OrgUnit>> orgUnitsMap = getOrgUnitsListsMap(parentOrgUnitsList);

        Map<EmployeePost, List<EmployeePostStaffRateItem>> staffRatesMap = new HashMap<>();
        List<EmployeePost> postsList = getEmployeePostsList(reportDate, staffRatesMap);

        Map<OrgUnit, List<EmployeePost>> mainJobsMap = new HashMap<>();
        Map<OrgUnit, List<EmployeePost>> secondJobsMap = new HashMap<>();
        splitPostsByEmployeeType(postsList, mainJobsMap, secondJobsMap);

        Map<Person, List<ScienceDegreeType>> scienceDegreesMap = IUniempDAO.instance.get().getEmployeeDegreesFullMap(reportDate);
        Map<Person, List<ScienceStatusType>> scienceStatusesMap = IUniempDAO.instance.get().getEmployeeStatusesFullMap(reportDate);

        List<String[]> lines = new ArrayList<>();
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());

        Integer lineNumber = 0;
        int totalPostsCnt = 0;
        int totalScDegreesCnt = 0;
        int totalScStatusesCnt = 0;
        int totalSecondPostsCnt = 0;
        int totalSecondScDegreesCnt = 0;
        int totalSecondScStatusesCnt = 0;

        for (OrgUnit parentOrgUnit : parentOrgUnitsList)
        {
            int totalParPostsCnt = 0;
            int totalParScDegreesCnt = 0;
            int totalParScStatusesCnt = 0;
            int totalSecondParPostsCnt = 0;
            int totalSecondParScDegreesCnt = 0;
            int totalSecondParScStatusesCnt = 0;

            lines.add(new String[] { parentOrgUnit.getFullTitle() });
            stylesMap.put(lineNumber++, FACULTY_HEADER);

            if (orgUnitsMap.containsKey(parentOrgUnit))
            {

                for (OrgUnit orgUnit : orgUnitsMap.get(parentOrgUnit))
                {
                    lines.add(new String[] { orgUnit.getFullTitle() });
                    stylesMap.put(lineNumber++, CATHEDRA_HEADER);

                    int postCnt = 1;
                    Integer mainScDegreesCnt = 0;
                    Integer mainScStatusesCnt = 0;

                    if (mainJobsMap.containsKey(orgUnit))
                    {
                        for (EmployeePost post : mainJobsMap.get(orgUnit))
                        {
                            lineNumber++;
                            mainScDegreesCnt += scienceDegreesMap.containsKey(post.getEmployee().getPerson()) ? 1 : 0;
                            mainScStatusesCnt += scienceStatusesMap.containsKey(post.getEmployee().getPerson()) ? 1 : 0;
                            lines.add(getSinglePostLine(post, staffRatesMap, scienceDegreesMap, scienceStatusesMap, postCnt++, reportDate));
                        }
                    }

                    lines.add(new String[] { "Всего", String.valueOf(mainJobsMap.containsKey(orgUnit) ? mainJobsMap.get(orgUnit).size() : ""), mainScDegreesCnt > 0 ? String.valueOf(mainScDegreesCnt) : "", mainScStatusesCnt > 0 ? String.valueOf(mainScStatusesCnt): "" });
                    stylesMap.put(lineNumber++, TOTAL_HEADER);

                    lines.add(new String[] { "Совместители:" });
                    stylesMap.put(lineNumber++, SECOND_JOB_HEADER);

                    postCnt = 1;
                    Integer secondScDegreesCnt = 0;
                    Integer secondScStatusesCnt = 0;

                    if (secondJobsMap.containsKey(orgUnit))
                    {
                        for (EmployeePost post : secondJobsMap.get(orgUnit))
                        {
                            lineNumber++;
                            secondScDegreesCnt += scienceDegreesMap.containsKey(post.getEmployee().getPerson()) ? 1 : 0;
                            secondScStatusesCnt += scienceStatusesMap.containsKey(post.getEmployee().getPerson()) ? 1 : 0;
                            lines.add(getSinglePostLine(post, staffRatesMap, scienceDegreesMap, scienceStatusesMap, postCnt++, reportDate));
                        }
                    }

                    lines.add(new String[] { "Всего", String.valueOf(secondJobsMap.containsKey(orgUnit) ? secondJobsMap.get(orgUnit).size() : ""), secondScDegreesCnt > 0 ? String.valueOf(secondScDegreesCnt) : "", secondScStatusesCnt > 0 ? String.valueOf(secondScStatusesCnt) : "" });
                    stylesMap.put(lineNumber++, TOTAL_HEADER);

                    int totalChildPostsCnt = (mainJobsMap.containsKey(orgUnit) ? mainJobsMap.get(orgUnit).size() : 0) + (secondJobsMap.containsKey(orgUnit) ? secondJobsMap.get(orgUnit).size() : 0);
                    int totalChildScDegreesCnt = mainScDegreesCnt + secondScDegreesCnt;
                    int totalChildScStatusesCnt = mainScStatusesCnt + secondScStatusesCnt;

                    totalParScDegreesCnt += totalChildScDegreesCnt;
                    totalParScStatusesCnt += totalChildScStatusesCnt;
                    totalParPostsCnt += totalChildPostsCnt;
                    totalSecondParPostsCnt += secondJobsMap.containsKey(orgUnit) ? secondJobsMap.get(orgUnit).size() : 0;
                    totalSecondParScDegreesCnt += secondScDegreesCnt;
                    totalSecondParScStatusesCnt += secondScStatusesCnt;

                    lines.add(new String[] { "Итого по кафедре", totalChildPostsCnt > 0 ? String.valueOf(totalChildPostsCnt) : "", totalChildScDegreesCnt > 0 ? String.valueOf(totalChildScDegreesCnt) : "", totalChildScStatusesCnt > 0 ? String.valueOf(totalChildScStatusesCnt) : "" });
                    stylesMap.put(lineNumber++, TOTAL_HEADER);
                }
            }

            if (!OrgUnitTypeCodes.TOP.equals(parentOrgUnit.getOrgUnitType().getCode()))
            {
                lines.add(new String[] { "ИТОГО ПО " + parentOrgUnit.getOrgUnitType().getDative().toUpperCase(), totalParPostsCnt > 0 ? String.valueOf(totalParPostsCnt) : "", totalParScDegreesCnt > 0 ? String.valueOf(totalParScDegreesCnt) : "", totalParScStatusesCnt > 0 ? String.valueOf(totalParScStatusesCnt) : "" });
                stylesMap.put(lineNumber++, TOTAL_HEADER);

                lineNumber++;
                lines.add(new String[] { "в т.ч. совместителей", String.valueOf(totalSecondParPostsCnt), totalSecondParScDegreesCnt > 0 ? String.valueOf(totalSecondParScDegreesCnt) : "", totalSecondParScStatusesCnt > 0 ? String.valueOf(totalSecondParScStatusesCnt) : "" });
            }

            totalPostsCnt += totalParPostsCnt;
            totalScDegreesCnt += totalParScDegreesCnt;
            totalScStatusesCnt += totalParScStatusesCnt;
            totalSecondPostsCnt += totalSecondParPostsCnt;
            totalSecondScDegreesCnt += totalSecondParScDegreesCnt;
            totalSecondScStatusesCnt += totalSecondParScStatusesCnt;
        }

        lines.add(new String[] { "ИТОГО ПО ОУ", totalPostsCnt > 0 ? String.valueOf(totalPostsCnt) : "", totalScDegreesCnt > 0 ? String.valueOf(totalScDegreesCnt) : "", totalScStatusesCnt > 0 ? String.valueOf(totalScStatusesCnt) : "" });
        stylesMap.put(lineNumber, TOTAL_HEADER);
        lines.add(new String[] { "в т.ч. совместителей", totalSecondPostsCnt > 0 ? String.valueOf(totalSecondPostsCnt) : "", totalSecondScDegreesCnt > 0 ? String.valueOf(totalSecondScDegreesCnt) : "", totalSecondScStatusesCnt > 0 ? String.valueOf(totalSecondScStatusesCnt) : "" });

        result.put("T", lines.toArray(new String[][] {}));

        return result;
    }

    private String[] getSinglePostLine(EmployeePost post, Map<EmployeePost, List<EmployeePostStaffRateItem>> staffRatesMap, Map<Person, List<ScienceDegreeType>> scienceDegreesMap, Map<Person, List<ScienceStatusType>> scienceStatusesMap, int postCnt, Date reportDate)
    {
        String fio = String.valueOf(postCnt) + "." + post.getPerson().getFullFio();
        String postTitle = post.getPostRelation().getPostBoundedWithQGandQL().getPost().getTitle();

        if (staffRatesMap.containsKey(post))
        {
            Double staffRate = 0.0d;
            for (EmployeePostStaffRateItem item : staffRatesMap.get(post))
                staffRate += item.getStaffRate();
            if (staffRate < 1) postTitle += " - " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(staffRate) + " ст.";
        }

        String scDegreeTitle = "";
        if (scienceDegreesMap.containsKey(post.getEmployee().getPerson()))
        {
            for(ScienceDegreeType type : scienceDegreesMap.get(post.getEmployee().getPerson()))
                scDegreeTitle += (scDegreeTitle.length() > 0 ? ", " : "") + type.getTitle();
        }

        String scStatusTitle = "";
        if (scienceStatusesMap.containsKey(post.getEmployee().getPerson()))
        {
            for(ScienceStatusType type : scienceStatusesMap.get(post.getEmployee().getPerson()))
                scStatusTitle += (scStatusTitle.length() > 0 ? ", " : "") + type.getTitle();
        }

        String age = "-";
        if (null != post.getEmployee().getPerson().getIdentityCard().getBirthDate())
        {
            long dateDifference = reportDate.getTime() - post.getEmployee().getPerson().getIdentityCard().getBirthDate().getTime();
            age = String.valueOf(dateDifference / 31557600000L);
        }

        return new String[] { fio, postTitle, scDegreeTitle, scStatusTitle, age };
    }

    /**
     * Возвращает список подразделений, включающих в себя кафедры
     */
    private List<OrgUnit> getParentOrgUnits()
    {
        MQBuilder sub = new MQBuilder(OrgUnitTypeRestriction.ENTITY_CLASS, "outr", new String[] { OrgUnitTypeRestriction.L_PARENT + "." + OrgUnitType.P_ID });
        sub.add(MQExpression.eq("outr", OrgUnitTypeRestriction.L_CHILD + "." + OrgUnitType.P_CODE, OrgUnitTypeCodes.CATHEDRA));

        MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "ou");
        builder.add(MQExpression.in("ou", OrgUnit.L_ORG_UNIT_TYPE + "." + OrgUnitType.P_ID, sub));
        builder.addOrder("ou", OrgUnit.P_TITLE);

        return builder.getResultList(getSession());
    }

    /**
     * Возвращает Map списков кафедр для каждого подразделения
     */
    private Map<OrgUnit, List<OrgUnit>> getOrgUnitsListsMap(List<OrgUnit> orgUnitsList)
    {
        MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "ou");
        builder.add(MQExpression.in("ou", OrgUnit.L_PARENT, orgUnitsList));
        builder.add(MQExpression.eq("ou", OrgUnit.L_ORG_UNIT_TYPE + "." + OrgUnitType.P_CODE, OrgUnitTypeCodes.CATHEDRA));
        builder.addOrder("ou", OrgUnit.P_TITLE);
        List<OrgUnit> resultList = builder.getResultList(getSession());

        Map<OrgUnit, List<OrgUnit>> result = new HashMap<>();
        for(OrgUnit unit : resultList)
        {
            List<OrgUnit> childsList = result.get(unit.getParent());
            if(null == childsList) childsList = new ArrayList<>();
            childsList.add(unit);
            result.put(unit.getParent(), childsList);
        }
        return result;
    }

    /**
     * Возвращает список сотрудников ППС, приписанных к кафедрам
     * Параллельно заполняет MAP со ставками сотрудников
     */
    private List<EmployeePost> getEmployeePostsList(Date reportDate, Map<EmployeePost, List<EmployeePostStaffRateItem>> staffRates)
    {
        MQBuilder builder = new MQBuilder(EmployeePost.ENTITY_CLASS, "ep");
        builder.add(MQExpression.in("ep", EmployeePost.L_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_POST + "." + Post.L_EMPLOYEE_TYPE, EmployeeManager.instance().dao().getEmployeeTypeWithAllChilds(getCatalogItem(EmployeeType.class, EmployeeTypeCodes.EDU_STAFF))));
        builder.add(MQExpression.lessOrEq("ep", EmployeePost.P_POST_DATE, reportDate));
        AbstractExpression expr1 = MQExpression.isNull("ep", EmployeePost.P_DISMISSAL_DATE);
        AbstractExpression expr2 = MQExpression.greatOrEq("ep", EmployeePost.P_DISMISSAL_DATE, reportDate);
        builder.add(MQExpression.or(expr1, expr2));
        builder.add(MQExpression.eq("ep", EmployeePost.L_ORG_UNIT + "." + OrgUnit.L_ORG_UNIT_TYPE + "." + OrgUnitType.P_CODE, OrgUnitTypeCodes.CATHEDRA));
        builder.addOrder("ep", EmployeePost.L_ORG_UNIT + "." + OrgUnit.P_TITLE);
        builder.addOrder("ep", EmployeePost.L_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_POST + "." + Post.P_RANK);
        builder.addOrder("ep", EmployeePost.L_EMPLOYEE + "." + Employee.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_LAST_NAME);
        builder.addOrder("ep", EmployeePost.L_EMPLOYEE + "." + Employee.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_FIRST_NAME);
        List<EmployeePost> postsList = builder.getResultList(getSession());

        for (EmployeePost post : postsList)
        {
            List<EmployeePostStaffRateItem> item = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(post);
            if (item.size() != 0)
                staffRates.put(post, item);
        }

        return postsList;
    }

    /**
     * Разбивает должности на совместителей и основных
     */
    private void splitPostsByEmployeeType(List<EmployeePost> employeePostsList, Map<OrgUnit, List<EmployeePost>> mainJobsMap, Map<OrgUnit, List<EmployeePost>> secondJobsMap)
    {
        for(EmployeePost post : employeePostsList)
        {
            OrgUnit orgUnit = post.getOrgUnit();
            if(UniDefines.POST_TYPE_SECOND_JOB.equals(post.getPostType().getCode()) || UniDefines.POST_TYPE_SECOND_JOB_INNER.equals(post.getPostType().getCode()) || UniDefines.POST_TYPE_SECOND_JOB_OUTER.equals(post.getPostType().getCode()))
            {
                List<EmployeePost> postsList = secondJobsMap.get(orgUnit);
                if(null == postsList) postsList = new ArrayList<>();
                postsList.add(post);
                secondJobsMap.put(orgUnit, postsList);
            }
            else
            {
                List<EmployeePost> postsList = mainJobsMap.get(orgUnit);
                if(null == postsList) postsList = new ArrayList<>();
                postsList.add(post);
                mainJobsMap.put(orgUnit, postsList);
            }
        }
    }
}