/**
 *$Id:$
 */
package ru.tandemservice.uniemp.component.reports.employeeVPO.Add;

/**
 * @author Alexander Shaburov
 * @since 02.10.12
 */
public class ReportSrviceLengthLineWrapper
{
    public ReportSrviceLengthLineWrapper(int level, String reportLineName, String reportLineCode, int numberLine)
    {
        _level = level;
        _reportLineName = reportLineName;
        _reportLineCode = reportLineCode;
        _numberLine = numberLine;
    }

    private int _level;
    private String _reportLineName;
    private String _reportLineCode;
    private int _numberLine;
    private int _common;
    private int _common3;
    private int _common3_5;
    private int _common5_10;
    private int _common10_15;
    private int _common15_20;
    private int _common20;
    private int _educator;
    private int _educator3;
    private int _educator3_5;
    private int _educator5_10;
    private int _educator10_15;
    private int _educator15_20;
    private int _educator20;

    // Getters & Setters

    public int getCommon10_15()
    {
        return _common10_15;
    }

    public void setCommon10_15(int common10_15)
    {
        _common10_15 = common10_15;
    }

    public int getEducator10_15()
    {
        return _educator10_15;
    }

    public void setEducator10_15(int educator10_15)
    {
        _educator10_15 = educator10_15;
    }

    public int getLevel()
    {
        return _level;
    }

    public void setLevel(int level)
    {
        _level = level;
    }

    public String getReportLineName()
    {
        return _reportLineName;
    }

    public void setReportLineName(String reportLineName)
    {
        _reportLineName = reportLineName;
    }

    public String getReportLineCode()
    {
        return _reportLineCode;
    }

    public void setReportLineCode(String reportLineCode)
    {
        _reportLineCode = reportLineCode;
    }

    public int getNumberLine()
    {
        return _numberLine;
    }

    public void setNumberLine(int numberLine)
    {
        _numberLine = numberLine;
    }

    public int getCommon()
    {
        return _common;
    }

    public void setCommon(int common)
    {
        _common = common;
    }

    public int getCommon3()
    {
        return _common3;
    }

    public void setCommon3(int common3)
    {
        _common3 = common3;
    }

    public int getCommon3_5()
    {
        return _common3_5;
    }

    public void setCommon3_5(int common3_5)
    {
        _common3_5 = common3_5;
    }

    public int getCommon5_10()
    {
        return _common5_10;
    }

    public void setCommon5_10(int common5_10)
    {
        _common5_10 = common5_10;
    }

    public int getCommon15_20()
    {
        return _common15_20;
    }

    public void setCommon15_20(int common15_20)
    {
        _common15_20 = common15_20;
    }

    public int getCommon20()
    {
        return _common20;
    }

    public void setCommon20(int common20)
    {
        _common20 = common20;
    }

    public int getEducator()
    {
        return _educator;
    }

    public void setEducator(int educator)
    {
        _educator = educator;
    }

    public int getEducator3()
    {
        return _educator3;
    }

    public void setEducator3(int educator3)
    {
        _educator3 = educator3;
    }

    public int getEducator3_5()
    {
        return _educator3_5;
    }

    public void setEducator3_5(int educator3_5)
    {
        _educator3_5 = educator3_5;
    }

    public int getEducator5_10()
    {
        return _educator5_10;
    }

    public void setEducator5_10(int educator5_10)
    {
        _educator5_10 = educator5_10;
    }

    public int getEducator15_20()
    {
        return _educator15_20;
    }

    public void setEducator15_20(int educator15_20)
    {
        _educator15_20 = educator15_20;
    }

    public int getEducator20()
    {
        return _educator20;
    }

    public void setEducator20(int educator20)
    {
        _educator20 = educator20;
    }
}
