/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.vacationShedule.VacationScheduleRegistry;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author dseleznev
 * Created on: 26.01.2011
 */
public interface IDAO extends IUniDao<Model>
{
}