/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.qualityQuantityConsistencyFull.Add;

import java.util.Date;

import org.tandemframework.shared.organization.base.entity.OrgUnit;

/**
 * @author dseleznev
 * Created on: 17.04.2009
 */
public class OrgUnitPPSStatistics
{
    private Date _reportDate;
    private OrgUnit _orgUnit;
    private int _ppsMainJobAmount = 0;
    private int _ppsSecondJobAmount = 0;
    private int _masterMainJobAmount = 0;
    private int _masterSecondJobAmount = 0;
    private int _doctorMainJobAmount = 0;
    private int _doctorSecondJobAmount = 0;
    private int _sumAge = 0;
    private int _ageCounter = 0;

    public OrgUnitPPSStatistics(OrgUnit orgUnit, Date reportDate)
    {
        _orgUnit = orgUnit;
        _reportDate = reportDate;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        this._orgUnit = orgUnit;
    }

    public void increasePpsMainJobAmount()
    {
        _ppsMainJobAmount++;
    }

    public int getPpsMainJobAmount()
    {
        return _ppsMainJobAmount;
    }

    public void setPpsMainJobAmount(int ppsMainJobAmount)
    {
        this._ppsMainJobAmount = ppsMainJobAmount;
    }

    public void increasePpsSecondJobAmount()
    {
        _ppsSecondJobAmount++;
    }

    public int getPpsSecondJobAmount()
    {
        return _ppsSecondJobAmount;
    }

    public void setPpsSecondJobAmount(int ppsSecondJobAmount)
    {
        this._ppsSecondJobAmount = ppsSecondJobAmount;
    }

    public void increaseMasterMainJobAmount()
    {
        _masterMainJobAmount++;
    }

    public int getMasterMainJobAmount()
    {
        return _masterMainJobAmount;
    }

    public void setMasterMainJobAmount(int masterMainJobAmount)
    {
        this._masterMainJobAmount = masterMainJobAmount;
    }

    public void increaseMasterSecondJobAmount()
    {
        _masterSecondJobAmount++;
    }

    public int getMasterSecondJobAmount()
    {
        return _masterSecondJobAmount;
    }

    public void setMasterSecondJobAmount(int masterSecondJobAmount)
    {
        this._masterSecondJobAmount = masterSecondJobAmount;
    }

    public void increaseDoctorMainJobAmount()
    {
        _doctorMainJobAmount++;
    }

    public int getDoctorMainJobAmount()
    {
        return _doctorMainJobAmount;
    }

    public void setDoctorMainJobAmount(int doctorMainJobAmount)
    {
        this._doctorMainJobAmount = doctorMainJobAmount;
    }

    public void increaseDoctorSecondJobAmount()
    {
        _doctorSecondJobAmount++;
    }

    public int getDoctorSecondJobAmount()
    {
        return _doctorSecondJobAmount;
    }

    public void setDoctorSecondJobAmount(int doctorSecondJobAmount)
    {
        this._doctorSecondJobAmount = doctorSecondJobAmount;
    }

    public void addEmployeeAge(int age)
    {
        _sumAge += age;
        _ageCounter++;
    }

    public void addEmployeeBirthDate(Date birthDate)
    {
        long dateDifference = _reportDate.getTime() - birthDate.getTime();
        _sumAge += dateDifference / 31557600000L;
        _ageCounter++;
    }

    public int getPpsAmount()
    {
        return getPpsMainJobAmount() + getPpsSecondJobAmount();
    }

    public int getMasterAmount()
    {
        return getMasterMainJobAmount() + getMasterSecondJobAmount();
    }

    public int getDoctorAmount()
    {
        return getDoctorMainJobAmount() + getDoctorSecondJobAmount();
    }

    public double getDegreesPercent()
    {
        if (getPpsAmount() == 0) return 0;
        return 100d * (getDoctorAmount() + getMasterAmount()) / (double) getPpsAmount();
    }

    public double getDegreesMainPercent()
    {
        if (getPpsMainJobAmount() == 0) return 0;
        return 100d * (getDoctorMainJobAmount() + getMasterMainJobAmount()) / (double) getPpsMainJobAmount();
    }

    public double getDoctorsPercent()
    {
        if (getPpsAmount() == 0) return 0;
        return 100d * getDoctorAmount() / (double) getPpsAmount();
    }

    public double getDoctorsMainPercent()
    {
        if (getPpsMainJobAmount() == 0) return 0;
        return 100d * getDoctorMainJobAmount() / (double) getPpsMainJobAmount();
    }

    public double getAverageAge()
    {
        if (_ageCounter == 0) return 0;
        return _sumAge / (double) _ageCounter;
    }

    public void addValues(OrgUnitPPSStatistics stat)
    {
        _ppsMainJobAmount += stat.getPpsMainJobAmount();
        _ppsSecondJobAmount += stat.getPpsSecondJobAmount();
        _masterMainJobAmount += stat.getMasterMainJobAmount();
        _masterSecondJobAmount += stat.getMasterSecondJobAmount();
        _doctorMainJobAmount += stat.getDoctorMainJobAmount();
        _doctorSecondJobAmount += stat.getDoctorSecondJobAmount();
        if(stat.getAverageAge() > 0) _ageCounter++;
        _sumAge += stat.getAverageAge();
    }
}