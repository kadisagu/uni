/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.component.settings.ChooseEmployeeManagingDepartment;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDao;

import java.util.List;

/**
 * @author alikhanov
 * @since 14.09.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "ou");
        builder.add(MQExpression.eq("ou", OrgUnit.P_ARCHIVAL, false));
        builder.add(MQExpression.eq("ou", OrgUnit.P_PERSONNEL_DEPARTMENT, true));
        model.setSelectedOrgUnit((OrgUnit) builder.uniqueResult(getSession()));
        model.setOrgUnitsModel(new FullCheckSelectModel(OrgUnit.fullTitle().s())
        {
            @Override
            public ListResult<OrgUnit> findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "ou");
                builder.add(MQExpression.eq("ou", OrgUnit.P_ARCHIVAL, false));
                builder.add(MQExpression.like("ou", OrgUnit.P_TITLE, CoreStringUtils.escapeLike(filter)));
                builder.addOrder("ou", OrgUnit.P_TITLE);
                return new ListResult<>(builder.<OrgUnit>getResultList(getSession()));
            }
        });
    }

    @Override
    public void update(Model model)
    {
        MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "ou");
        builder.add(MQExpression.eq("ou", OrgUnit.P_PERSONNEL_DEPARTMENT, true));
        List<OrgUnit> personnelDepartments = builder.getResultList(getSession());
        for (OrgUnit unit : personnelDepartments)
        {
            unit.setPersonnelDepartment(false);
            update(unit);
        }

        if (model.getSelectedOrgUnit() != null)
        {
            model.getSelectedOrgUnit().setPersonnelDepartment(true);
            update(model.getSelectedOrgUnit());
        }

    }
}
