package ru.tandemservice.uniemp.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.report.StaffListRepPaymentSettings;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Настройка перечня и порядка выплат в штатном расписании
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StaffListRepPaymentSettingsGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.report.StaffListRepPaymentSettings";
    public static final String ENTITY_NAME = "staffListRepPaymentSettings";
    public static final int VERSION_HASH = -1041500722;
    private static IEntityMeta ENTITY_META;

    public static final String L_PAYMENT = "payment";
    public static final String P_TAKE_INTO_ACCOUNT = "takeIntoAccount";
    public static final String P_INDIVIDUAL_COLUMN = "individualColumn";
    public static final String P_PRIORITY = "priority";

    private Payment _payment;     // Выплата
    private boolean _takeIntoAccount;     // Учитывать в штатном расписании
    private boolean _individualColumn;     // Выводить отдельным столбцом
    private int _priority;     // Приоритет

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выплата.
     */
    public Payment getPayment()
    {
        return _payment;
    }

    /**
     * @param payment Выплата.
     */
    public void setPayment(Payment payment)
    {
        dirty(_payment, payment);
        _payment = payment;
    }

    /**
     * @return Учитывать в штатном расписании. Свойство не может быть null.
     */
    @NotNull
    public boolean isTakeIntoAccount()
    {
        return _takeIntoAccount;
    }

    /**
     * @param takeIntoAccount Учитывать в штатном расписании. Свойство не может быть null.
     */
    public void setTakeIntoAccount(boolean takeIntoAccount)
    {
        dirty(_takeIntoAccount, takeIntoAccount);
        _takeIntoAccount = takeIntoAccount;
    }

    /**
     * @return Выводить отдельным столбцом. Свойство не может быть null.
     */
    @NotNull
    public boolean isIndividualColumn()
    {
        return _individualColumn;
    }

    /**
     * @param individualColumn Выводить отдельным столбцом. Свойство не может быть null.
     */
    public void setIndividualColumn(boolean individualColumn)
    {
        dirty(_individualColumn, individualColumn);
        _individualColumn = individualColumn;
    }

    /**
     * @return Приоритет. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Приоритет. Свойство не может быть null и должно быть уникальным.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StaffListRepPaymentSettingsGen)
        {
            setPayment(((StaffListRepPaymentSettings)another).getPayment());
            setTakeIntoAccount(((StaffListRepPaymentSettings)another).isTakeIntoAccount());
            setIndividualColumn(((StaffListRepPaymentSettings)another).isIndividualColumn());
            setPriority(((StaffListRepPaymentSettings)another).getPriority());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StaffListRepPaymentSettingsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StaffListRepPaymentSettings.class;
        }

        public T newInstance()
        {
            return (T) new StaffListRepPaymentSettings();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "payment":
                    return obj.getPayment();
                case "takeIntoAccount":
                    return obj.isTakeIntoAccount();
                case "individualColumn":
                    return obj.isIndividualColumn();
                case "priority":
                    return obj.getPriority();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "payment":
                    obj.setPayment((Payment) value);
                    return;
                case "takeIntoAccount":
                    obj.setTakeIntoAccount((Boolean) value);
                    return;
                case "individualColumn":
                    obj.setIndividualColumn((Boolean) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "payment":
                        return true;
                case "takeIntoAccount":
                        return true;
                case "individualColumn":
                        return true;
                case "priority":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "payment":
                    return true;
                case "takeIntoAccount":
                    return true;
                case "individualColumn":
                    return true;
                case "priority":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "payment":
                    return Payment.class;
                case "takeIntoAccount":
                    return Boolean.class;
                case "individualColumn":
                    return Boolean.class;
                case "priority":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StaffListRepPaymentSettings> _dslPath = new Path<StaffListRepPaymentSettings>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StaffListRepPaymentSettings");
    }
            

    /**
     * @return Выплата.
     * @see ru.tandemservice.uniemp.entity.report.StaffListRepPaymentSettings#getPayment()
     */
    public static Payment.Path<Payment> payment()
    {
        return _dslPath.payment();
    }

    /**
     * @return Учитывать в штатном расписании. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.report.StaffListRepPaymentSettings#isTakeIntoAccount()
     */
    public static PropertyPath<Boolean> takeIntoAccount()
    {
        return _dslPath.takeIntoAccount();
    }

    /**
     * @return Выводить отдельным столбцом. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.report.StaffListRepPaymentSettings#isIndividualColumn()
     */
    public static PropertyPath<Boolean> individualColumn()
    {
        return _dslPath.individualColumn();
    }

    /**
     * @return Приоритет. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniemp.entity.report.StaffListRepPaymentSettings#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    public static class Path<E extends StaffListRepPaymentSettings> extends EntityPath<E>
    {
        private Payment.Path<Payment> _payment;
        private PropertyPath<Boolean> _takeIntoAccount;
        private PropertyPath<Boolean> _individualColumn;
        private PropertyPath<Integer> _priority;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выплата.
     * @see ru.tandemservice.uniemp.entity.report.StaffListRepPaymentSettings#getPayment()
     */
        public Payment.Path<Payment> payment()
        {
            if(_payment == null )
                _payment = new Payment.Path<Payment>(L_PAYMENT, this);
            return _payment;
        }

    /**
     * @return Учитывать в штатном расписании. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.report.StaffListRepPaymentSettings#isTakeIntoAccount()
     */
        public PropertyPath<Boolean> takeIntoAccount()
        {
            if(_takeIntoAccount == null )
                _takeIntoAccount = new PropertyPath<Boolean>(StaffListRepPaymentSettingsGen.P_TAKE_INTO_ACCOUNT, this);
            return _takeIntoAccount;
        }

    /**
     * @return Выводить отдельным столбцом. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.report.StaffListRepPaymentSettings#isIndividualColumn()
     */
        public PropertyPath<Boolean> individualColumn()
        {
            if(_individualColumn == null )
                _individualColumn = new PropertyPath<Boolean>(StaffListRepPaymentSettingsGen.P_INDIVIDUAL_COLUMN, this);
            return _individualColumn;
        }

    /**
     * @return Приоритет. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniemp.entity.report.StaffListRepPaymentSettings#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(StaffListRepPaymentSettingsGen.P_PRIORITY, this);
            return _priority;
        }

        public Class getEntityClass()
        {
            return StaffListRepPaymentSettings.class;
        }

        public String getEntityName()
        {
            return "staffListRepPaymentSettings";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
