package ru.tandemservice.uniemp.entity.employee;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uniemp.entity.catalog.LabourContractType;
import ru.tandemservice.uniemp.entity.employee.gen.EmployeeLabourContractGen;

public class EmployeeLabourContract extends EmployeeLabourContractGen
{

    public static final String[] PERSON_FIO_KEY = new String[] {EmployeeLabourContract.L_EMPLOYEE_POST, EmployeePost.L_PERSON, Person.P_FULL_FIO};
    public static final String[] POST_TITLE_KEY = new String[] {EmployeeLabourContract.L_EMPLOYEE_POST, EmployeePost.L_POST_RELATION, OrgUnitTypePostRelation.P_TITLE};
    public static final String[] ORGUNIT_TITLE_KEY = new String[] {EmployeeLabourContract.L_EMPLOYEE_POST, EmployeePost.L_ORG_UNIT, OrgUnit.P_TITLE};
    public static final String[] POST_TYPE_TITLE_KEY = new String[] {EmployeeLabourContract.L_EMPLOYEE_POST, EmployeePost.L_POST_TYPE, PostType.P_TITLE};
    public static final String[] NUMBER_KEY = new String[] {EmployeeLabourContract.P_NUMBER};
    public static final String[] DATE_KEY = new String[] {EmployeeLabourContract.P_DATE};
    public static final String[] BEGIN_DATE_KEY = new String[] {EmployeeLabourContract.P_BEGIN_DATE};
    public static final String[] END_DATE_KEY = new String[] {EmployeeLabourContract.P_END_DATE};
    public static final String[] EMPLOYMENT_DATE_KEY = new String[] {EmployeeLabourContract.P_EMPLOYMENT_DATE};
    public static final String[] TYPE_KEY = new String[] { EmployeeLabourContract.L_TYPE, LabourContractType.P_TITLE};
}