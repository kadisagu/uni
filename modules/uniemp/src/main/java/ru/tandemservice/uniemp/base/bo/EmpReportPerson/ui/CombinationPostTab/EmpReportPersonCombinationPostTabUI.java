/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.CombinationPostTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.employeebase.base.bo.EmployeeReportPerson.ui.Add.EmployeeReportPersonAddUI;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportDQLModifierOwner;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportPrintInfo;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.ReportDQL;
import ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.CombinationPostTab.block.combinationPostData.CombinationPostDataBlock;
import ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.CombinationPostTab.block.combinationPostData.CombinationPostDataParam;

/**
 * @author Vasily Zhukov
 * @since 12.04.2012
 */
public class EmpReportPersonCombinationPostTabUI extends UIPresenter implements IReportDQLModifierOwner
{
    private CombinationPostDataParam _combinationPostData = new CombinationPostDataParam();

    @Override
    public void onComponentRefresh()
    {
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        CombinationPostDataBlock.onBeforeDataSourceFetch(dataSource, _combinationPostData);
    }

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {

    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (printInfo.isExists(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET))
        {
            // фильтры модифицируют запросы
            _combinationPostData.modify(dql, printInfo);

            // печатные блоки модифицируют запросы и создают печатные колонки
        }
    }

    // Getters

    public CombinationPostDataParam getCombinationPostData()
    {
        return _combinationPostData;
    }
}
