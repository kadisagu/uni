/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.vacationShedule.OrgUnitVacationSchedulesList;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author dseleznev
 * Created on: 05.01.2011
 */
public interface IDAO extends IUniDao<Model>
{
}