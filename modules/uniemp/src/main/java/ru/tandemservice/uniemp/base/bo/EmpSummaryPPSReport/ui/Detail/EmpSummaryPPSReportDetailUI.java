/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.bo.EmpSummaryPPSReport.ui.Detail;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.list.source.AbstractListDataSource;

import java.util.Collection;
import java.util.Date;

/**
 * @author Alexander Shaburov
 * @since 21.11.12
 */
@Input({
        @Bind(key = "employeeIdList", binding = "employeeIdList", required = true),
        @Bind(key = "reportDate", binding = "reportDate", required = true),
        @Bind(key = "tableTitle", binding = "tableTitle", required = true),
        @Bind(key = "columnTitle", binding = "columnTitle", required = true),
        @Bind(key = "rowTitle", binding = "rowTitle", required = true)
})
public class EmpSummaryPPSReportDetailUI extends UIPresenter
{
    public static final String PROP_EMPLOYEE_ID_LIST = "employeeIdList";
    public static final String PROP_REPORT_DATE = "reportDate";

    private Collection<Long> _employeeIdList;

    private Date _reportDate;
    private String _tableTitle;
    private String _columnTitle;
    private String _rowTitle;

    @Override
    public void onComponentRefresh()
    {
        AbstractListDataSource<IEntity> ds = getConfig().<BaseSearchListDataSource>getDataSource(EmpSummaryPPSReportDetail.EMPLOYEE_POST_SEARCH_DS).getLegacyDataSource();
        ds.setOrder(ds.getColumn("title"), OrderDirection.asc);
    }

    public String getSticker()
    {
        return "Детализация «" + _tableTitle + "» графы «" + _columnTitle + "» по строке «" + _rowTitle + "»";
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(PROP_EMPLOYEE_ID_LIST, _employeeIdList);
        dataSource.put(PROP_REPORT_DATE, _reportDate);
    }

    // Getters & Setters

    public Collection<Long> getEmployeeIdList()
    {
        return _employeeIdList;
    }

    public void setEmployeeIdList(Collection<Long> employeeIdList)
    {
        _employeeIdList = employeeIdList;
    }

    public String getTableTitle()
    {
        return _tableTitle;
    }

    public void setTableTitle(String tableTitle)
    {
        _tableTitle = tableTitle;
    }

    public String getColumnTitle()
    {
        return _columnTitle;
    }

    public void setColumnTitle(String columnTitle)
    {
        _columnTitle = columnTitle;
    }

    public String getRowTitle()
    {
        return _rowTitle;
    }

    public void setRowTitle(String rowTitle)
    {
        _rowTitle = rowTitle;
    }

    public Date getReportDate()
    {
        return _reportDate;
    }

    public void setReportDate(Date reportDate)
    {
        _reportDate = reportDate;
    }
}
