/**
 *$Id$
 */
package ru.tandemservice.uniemp.component.employee.employmentContracts.EmploymentContractsTerminationNoticePrint;

import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;

import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;

/**
 * Create by ashaburov
 * Date 19.08.11
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        try { BusinessComponentUtils.downloadDocument(buildDocumentRenderer(component), true); }
        catch(Throwable t) { throw CoreExceptionUtils.getRuntimeException(t); }
    }

    public IDocumentRenderer buildDocumentRenderer(IBusinessComponent component)
    {
        IDAO dao = getDao();
        RtfDocument document = dao.getRtfDocument();
        Model model = getModel(component);

        if (model.getMassPrint())
        {
            RtfDocument resultDocument = document.getClone();
            resultDocument.getElementList().clear();

            for (int i = 0; i < model.getEmployeeLabourContractList().size(); i++)
            {
                RtfDocument doc = (RtfDocument)document.getClone();
                EmployeeLabourContract contract = model.getEmployeeLabourContractList().get(i);
                dao.modifyDocument(contract, model.getNumberMap().get(contract.getId()), model.getDateMap().get(contract.getId()), doc);
                resultDocument.getElementList().addAll(doc.getElementList());
                if (i < model.getEmployeeLabourContractList().size() - 1)
                    resultDocument.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
            }

            resultDocument.setSettings(document.getSettings());
            resultDocument.setHeader(document.getHeader());

            deactivate(component);
            return new CommonBaseRenderer().rtf().fileName("EmploymentContractsTerminationNotice.rtf").document(resultDocument);
        }
        else
        {
            EmployeeLabourContract contract = model.getEmployeeLabourContractList().get(0);
            deactivate(component);
            return new CommonBaseRenderer().rtf().fileName("EmploymentContractsTerminationNotice.rtf").document(dao.modifyDocument(contract, model.getNumberMap().get(contract.getId()), model.getDateMap().get(contract.getId()), document));
        }
    }
}
