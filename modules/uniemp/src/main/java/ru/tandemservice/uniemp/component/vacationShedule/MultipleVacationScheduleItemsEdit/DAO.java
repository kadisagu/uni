/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.component.vacationShedule.MultipleVacationScheduleItemsEdit;

import java.util.ArrayList;
import java.util.Collection;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.VacationScheduleItem;

/**
 * @author dseleznev
 * Created on: 21.01.2011
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setVacationScheduleItemsList(new ArrayList<VacationScheduleItem>());

        if (null != model.getVacationScheduleItemIdsList() && !model.getVacationScheduleItemIdsList().isEmpty())
        {
            BatchUtils.execute(model.getVacationScheduleItemIdsList(), 100, new BatchUtils.Action<Long>()
            {
                @Override
                public void execute(Collection<Long> vacationScheduleItemIdsList)
                {
                    MQBuilder vacationScheduleItemsBuilder = new MQBuilder(VacationScheduleItem.ENTITY_CLASS, "vsi");
                    vacationScheduleItemsBuilder.add(MQExpression.in("vsi", VacationScheduleItem.id().s(), vacationScheduleItemIdsList));
                    model.getVacationScheduleItemsList().addAll(vacationScheduleItemsBuilder.<VacationScheduleItem> getResultList(getSession()));
                }
            });
        }
    }

    @Override
    public void update(Model model)
    {
        ErrorCollector errs = UserContext.getInstance().getErrorCollector();

        for (VacationScheduleItem item : model.getVacationScheduleItemsList())
        {
            item.setPlanDate(model.getVacationPlanDate());
            UniempDaoFacade.getUniempDAO().validateVacationScheduleItemHasIntersections(item, errs, "Указанный планируемый отпуск у сотрудника (" + item.getEmployeePost().getEmployeePostStr() + ")", "planDate", "", "");
        }

        if (errs.hasErrors()) return;

        for (VacationScheduleItem item : model.getVacationScheduleItemsList())
        {
            item.setPlanDate(model.getVacationPlanDate());
            update(item);
        }
    }
}