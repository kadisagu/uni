/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.bo.EmpServiceLengthReport.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uniemp.base.bo.EmpServiceLengthReport.EmpServiceLengthReportManager;
import ru.tandemservice.uniemp.base.bo.EmpServiceLengthReport.logic.EmpServiceLengthReportModel;

/**
 * @author Alexander Shaburov
 * @since 25.01.13
 */
public class EmpServiceLengthReportAddUI extends UIPresenter
{
    public static final String EMP_TYPE = "empType";

    private Object _model;

    @Override
    public void onComponentActivate()
    {
        _model = EmpServiceLengthReportManager.instance().dao().prepareModel();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        EmpServiceLengthReportModel model = (EmpServiceLengthReportModel) _model;

        if (model.getEmpTypeList() != null && !model.getEmpTypeList().isEmpty())
            dataSource.put(EMP_TYPE, model.getEmpTypeList());
    }

    public void onClickApply()
    {
        byte[] report;
        try
        {
            report = EmpServiceLengthReportManager.instance().dao().createReport((EmpServiceLengthReportModel) _model);
        }
        catch (Exception e)
        {
            throw CoreExceptionUtils.getRuntimeException(e);
        }

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().xls().fileName("empServiceLengthReport.xls").document(report), false);
    }

    // Getters & Setters

    public Object getModel()
    {
        return _model;
    }

    public void setModel(Object model)
    {
        _model = model;
    }
}
