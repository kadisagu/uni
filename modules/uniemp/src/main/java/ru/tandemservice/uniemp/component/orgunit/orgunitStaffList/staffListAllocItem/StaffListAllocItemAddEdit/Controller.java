/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffListAllocItem.StaffListAllocItemAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.info.ErrorCollector;

import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;

/**
 * @author dseleznev
 * Created on: 08.04.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onChangePost(IBusinessComponent component)
    {
        StaffListAllocationItem allocItem = getModel(component).getStaffListAllocationItem();

        if (null != allocItem.getRaisingCoefficient())
            allocItem.setMonthBaseSalaryFund(Math.round(allocItem.getRaisingCoefficient().getRecommendedSalary() * allocItem.getStaffRate() * 100) / 100d);
        else if (null != allocItem.getStaffListItem())
            allocItem.setMonthBaseSalaryFund(Math.round(allocItem.getStaffListItem().getSalary() * allocItem.getStaffRate() * 100) / 100d);
        else
            allocItem.setMonthBaseSalaryFund(0d);
    }

    public void onClickApply(IBusinessComponent component)
    {
        ErrorCollector errors = component.getUserContext().getErrorCollector();
        getDao().validate(getModel(component), errors);
        if (!errors.hasErrors())
        {
            getDao().update(getModel(component));
            deactivate(component);
        }
    }
}