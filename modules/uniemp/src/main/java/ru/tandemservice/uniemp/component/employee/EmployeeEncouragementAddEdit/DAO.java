/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.EmployeeEncouragementAddEdit;

import java.util.Date;

import org.tandemframework.core.info.ErrorCollector;

import org.tandemframework.shared.employeebase.base.entity.Employee;
import ru.tandemservice.uniemp.dao.UniempDAO;
import ru.tandemservice.uniemp.entity.catalog.EncouragementType;
import ru.tandemservice.uniemp.entity.employee.EmployeeEncouragement;

/**
 * @author dseleznev
 * Created on: 23.12.2008
 */
public class DAO extends UniempDAO<Model> implements IDAO
{

    @Override
    public void prepare(final Model model)
    {
        if (null != model.getEmployeeId())
            model.setEmployee(get(Employee.class, model.getEmployeeId()));

        if (null != model.getEmployeeEncouragement().getId())
            model.setEmployeeEncouragement(get(EmployeeEncouragement.class, model.getEmployeeEncouragement().getId()));
        else
        {
            model.getEmployeeEncouragement().setAssignDate(new Date());
            model.getEmployeeEncouragement().setEmployee(model.getEmployee());
        }

        model.setEncouragementTypesList(getCatalogItemList(EncouragementType.class));
    }

    @Override
    public void update(Model model)
    {
        getSession().saveOrUpdate(model.getEmployeeEncouragement());
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        // TODO: no any validations
    }

}