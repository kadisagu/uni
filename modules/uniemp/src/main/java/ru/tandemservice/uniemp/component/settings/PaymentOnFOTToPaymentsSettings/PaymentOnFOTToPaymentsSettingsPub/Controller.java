/**
 *$Id$
 */
package ru.tandemservice.uniemp.component.settings.PaymentOnFOTToPaymentsSettings.PaymentOnFOTToPaymentsSettingsPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.RowCollectionFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.MultiValuesColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.component.settings.abstractRelation.AbstractRelationList.AbstractRelationListController;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.IUniempComponents;
import ru.tandemservice.uniemp.entity.catalog.Payment;

/**
 * Create by: ashaburov
 * Date: 26.04.11
 */
public class Controller extends AbstractRelationListController<IDAO, Model>
{
    @Override
    protected String getRelationPubComponent()
    {
        return null;
    }

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Выплата", Payment.P_TITLE));
        dataSource.addColumn(new MultiValuesColumn("Включать в расчет выплаты", Model.P_PAYMENTS_LIST).setFormatter(RowCollectionFormatter.INSTANCE));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit"));
        dataSource.addColumn(new ActionColumn("Очистить", ActionColumn.DELETE, "onClickClear"));
        model.setDataSource(dataSource);
    }

    public void onClickEdit(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IUniempComponents.PAYMENT_ON_FOT_TO_PAYMENTS_SETTINGS_EDIT, new ParametersMap().add("paymentId", component.getListenerParameter())));
    }

    public void onClickClear(IBusinessComponent component)
    {
        Payment payment = getDao().get(Payment.class, component.<Long>getListenerParameter());

        getDao().updateRelations(payment);

        onRefreshComponent(component);
    }

}
