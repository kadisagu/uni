package ru.tandemservice.uniemp.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.catalog.entity.ScienceDegree;
import ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation;
import ru.tandemservice.uniemp.entity.employee.OrgUnitPostToScDegreeRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь должности на подразделении с учеными степенями
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OrgUnitPostToScDegreeRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.employee.OrgUnitPostToScDegreeRelation";
    public static final String ENTITY_NAME = "orgUnitPostToScDegreeRelation";
    public static final int VERSION_HASH = -1123532014;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORG_UNIT_POST_RELATION = "orgUnitPostRelation";
    public static final String L_ACADEMIC_DEGREE = "academicDegree";

    private OrgUnitPostRelation _orgUnitPostRelation;     // Должность на подразделении
    private ScienceDegree _academicDegree;     // Ученая степень

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Должность на подразделении. Свойство не может быть null.
     */
    @NotNull
    public OrgUnitPostRelation getOrgUnitPostRelation()
    {
        return _orgUnitPostRelation;
    }

    /**
     * @param orgUnitPostRelation Должность на подразделении. Свойство не может быть null.
     */
    public void setOrgUnitPostRelation(OrgUnitPostRelation orgUnitPostRelation)
    {
        dirty(_orgUnitPostRelation, orgUnitPostRelation);
        _orgUnitPostRelation = orgUnitPostRelation;
    }

    /**
     * @return Ученая степень. Свойство не может быть null.
     */
    @NotNull
    public ScienceDegree getAcademicDegree()
    {
        return _academicDegree;
    }

    /**
     * @param academicDegree Ученая степень. Свойство не может быть null.
     */
    public void setAcademicDegree(ScienceDegree academicDegree)
    {
        dirty(_academicDegree, academicDegree);
        _academicDegree = academicDegree;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof OrgUnitPostToScDegreeRelationGen)
        {
            setOrgUnitPostRelation(((OrgUnitPostToScDegreeRelation)another).getOrgUnitPostRelation());
            setAcademicDegree(((OrgUnitPostToScDegreeRelation)another).getAcademicDegree());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OrgUnitPostToScDegreeRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OrgUnitPostToScDegreeRelation.class;
        }

        public T newInstance()
        {
            return (T) new OrgUnitPostToScDegreeRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "orgUnitPostRelation":
                    return obj.getOrgUnitPostRelation();
                case "academicDegree":
                    return obj.getAcademicDegree();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "orgUnitPostRelation":
                    obj.setOrgUnitPostRelation((OrgUnitPostRelation) value);
                    return;
                case "academicDegree":
                    obj.setAcademicDegree((ScienceDegree) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "orgUnitPostRelation":
                        return true;
                case "academicDegree":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "orgUnitPostRelation":
                    return true;
                case "academicDegree":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "orgUnitPostRelation":
                    return OrgUnitPostRelation.class;
                case "academicDegree":
                    return ScienceDegree.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OrgUnitPostToScDegreeRelation> _dslPath = new Path<OrgUnitPostToScDegreeRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OrgUnitPostToScDegreeRelation");
    }
            

    /**
     * @return Должность на подразделении. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.OrgUnitPostToScDegreeRelation#getOrgUnitPostRelation()
     */
    public static OrgUnitPostRelation.Path<OrgUnitPostRelation> orgUnitPostRelation()
    {
        return _dslPath.orgUnitPostRelation();
    }

    /**
     * @return Ученая степень. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.OrgUnitPostToScDegreeRelation#getAcademicDegree()
     */
    public static ScienceDegree.Path<ScienceDegree> academicDegree()
    {
        return _dslPath.academicDegree();
    }

    public static class Path<E extends OrgUnitPostToScDegreeRelation> extends EntityPath<E>
    {
        private OrgUnitPostRelation.Path<OrgUnitPostRelation> _orgUnitPostRelation;
        private ScienceDegree.Path<ScienceDegree> _academicDegree;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Должность на подразделении. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.OrgUnitPostToScDegreeRelation#getOrgUnitPostRelation()
     */
        public OrgUnitPostRelation.Path<OrgUnitPostRelation> orgUnitPostRelation()
        {
            if(_orgUnitPostRelation == null )
                _orgUnitPostRelation = new OrgUnitPostRelation.Path<OrgUnitPostRelation>(L_ORG_UNIT_POST_RELATION, this);
            return _orgUnitPostRelation;
        }

    /**
     * @return Ученая степень. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.OrgUnitPostToScDegreeRelation#getAcademicDegree()
     */
        public ScienceDegree.Path<ScienceDegree> academicDegree()
        {
            if(_academicDegree == null )
                _academicDegree = new ScienceDegree.Path<ScienceDegree>(L_ACADEMIC_DEGREE, this);
            return _academicDegree;
        }

        public Class getEntityClass()
        {
            return OrgUnitPostToScDegreeRelation.class;
        }

        public String getEntityName()
        {
            return "orgUnitPostToScDegreeRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
