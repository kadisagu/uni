/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.bo.EmpEmployeeCertificationItem.logic;

import org.tandemframework.hibsupport.dao.CommonDAO;
import ru.tandemservice.uniemp.entity.employee.EmployeeCertificationItem;

/**
 * Create by ashaburov
 * Date 22.03.12
 */
public class EmpEmployeeCertificationItemDAO extends CommonDAO implements IEmpEmployeeCertificationItemDAO
{
    @Override
    public void saveOrUpdateItem(EmployeeCertificationItem item)
    {
        saveOrUpdate(item);
    }
}
