/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.EmployeeEncouragementAddEdit;

import java.util.List;

import org.tandemframework.core.component.Input;

import org.tandemframework.shared.employeebase.base.entity.Employee;
import ru.tandemservice.uniemp.entity.catalog.EncouragementType;
import ru.tandemservice.uniemp.entity.employee.EmployeeEncouragement;

/**
 * @author dseleznev
 * Created on: 23.12.2008
 */
@Input(keys = { "employeeId", "rowId" }, bindings = { "employeeId", "employeeEncouragement.id" })
public class Model
{
    private Long _employeeId;
    private Employee _employee;
    private EmployeeEncouragement _employeeEncouragement = new EmployeeEncouragement();
    private List<EncouragementType> _encouragementTypesList;

    public Long getEmployeeId()
    {
        return _employeeId;
    }

    public void setEmployeeId(Long employeeId)
    {
        this._employeeId = employeeId;
    }

    public Employee getEmployee()
    {
        return _employee;
    }

    public void setEmployee(Employee employee)
    {
        this._employee = employee;
    }

    public EmployeeEncouragement getEmployeeEncouragement()
    {
        return _employeeEncouragement;
    }

    public void setEmployeeEncouragement(EmployeeEncouragement employeeEncouragement)
    {
        this._employeeEncouragement = employeeEncouragement;
    }

    public List<EncouragementType> getEncouragementTypesList()
    {
        return _encouragementTypesList;
    }

    public void setEncouragementTypesList(List<EncouragementType> encouragementTypesList)
    {
        this._encouragementTypesList = encouragementTypesList;
    }

}