/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniemp.component.catalog.payment.PaymentPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.BooleanColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.uniemp.entity.catalog.Payment;

/**
 * @author AutoGenerator
 * Created on 08.12.2008
 */
public class Controller extends DefaultCatalogPubController<Payment, Model, IDAO>
{
    @Override
    protected void addColumnsBeforeEditColumn(IBusinessComponent context, DynamicListDataSource<Payment> dataSource)
    {
        dataSource.addColumn(new SimpleColumn("Тип выплаты", Payment.TYPE_KEY).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Базовое значение", Payment.P_VALUE,  DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Формат ввода выплаты", Payment.UNIT_KEY).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Источник финансирования", Payment.FINANCING_SOURCE_KEY).setClickable(false));
        dataSource.addColumn(new BooleanColumn("Используется", Payment.P_ACTIVE));
        dataSource.addColumn(new BooleanColumn("Обязательная", Payment.P_REQUIRED));
        dataSource.addColumn(new BooleanColumn("Фиксированное значение", Payment.P_FIXED_VALUE));
        dataSource.addColumn(new BooleanColumn("Не зависит от ставки", Payment.P_DOESNT_DEPEND_ON_STAFF_RATE));
        dataSource.addColumn(new BooleanColumn("Разовая", Payment.P_ONE_TIME_PAYMENT));
    }
}