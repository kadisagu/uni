/* $Id$ */
package ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.PaymentsDataTab.block.paymentData;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.employeebase.base.bo.EmployeeReportPerson.ui.Add.EmployeeReportPersonAddUI;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.catalog.PaymentType;
import ru.tandemservice.uniemp.entity.employee.EmployeePayment;

import java.util.Date;
import java.util.List;

/**
 * @author Vasily Zhukov
 * @since 05.08.2011
 */
public class PaymentDataParam implements IReportDQLModifier
{
    private IReportParam<List<PaymentType>> _paymentType = new ReportParam<>();
    private IReportParam<List<Payment>> _payment = new ReportParam<>();
    private IReportParam<Date> _paymentBeginFrom = new ReportParam<>();
    private IReportParam<Date> _paymentEndTo = new ReportParam<>();
    private IReportParam<Date> _beginDateFrom = new ReportParam<>();
    private IReportParam<Date> _beginDateTo = new ReportParam<>();
    private IReportParam<Date> _endDateFrom = new ReportParam<>();
    private IReportParam<Date> _endDateTo = new ReportParam<>();
    private IReportParam<Date> _paymentAssignDateFrom = new ReportParam<>();
    private IReportParam<Date> _paymentAssignDateTo = new ReportParam<>();
    private IReportParam<Double> _paymentValFrom = new ReportParam<>();
    private IReportParam<Double> _paymentValTo = new ReportParam<>();

    private String alias(ReportDQL dql)
    {
        // соединяем кадрового ресурса
        String employeeAlias = dql.innerJoinEntity(Employee.class, Employee.person());

        // соединяем должность
        String employeePostAlias = dql.innerJoinEntity(employeeAlias, EmployeePost.class, EmployeePost.employee());

        // соединяем выплату сотрудника
        return dql.innerJoinEntity(employeePostAlias, EmployeePayment.class, EmployeePayment.employeePost());
    }

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {

    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (_paymentType.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "paymentData.paymentType", CommonBaseStringUtil.join(_paymentType.getData(), PaymentType.P_TITLE, ", "));

            dql.builder.where(DQLExpressions.in(DQLExpressions.property(EmployeePayment.payment().type().fromAlias(alias(dql))), _paymentType.getData()));
        }

        if (_payment.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "paymentData.payment", CommonBaseStringUtil.join(_payment.getData(), Payment.P_TITLE, ", "));

            dql.builder.where(DQLExpressions.in(DQLExpressions.property(EmployeePayment.payment().fromAlias(alias(dql))), _payment.getData()));
        }

        if (_paymentBeginFrom.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "paymentData.paymentBeginFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(_paymentBeginFrom.getData()));

            dql.builder.where(DQLExpressions.ge(DQLExpressions.property(EmployeePayment.assignDate().fromAlias(alias(dql))), DQLExpressions.valueDate(CoreDateUtils.getDayFirstTimeMoment(_paymentBeginFrom.getData()))));
        }

        if (_paymentEndTo.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "paymentData.paymentEndTo", DateFormatter.DEFAULT_DATE_FORMATTER.format(_paymentEndTo.getData()));

            dql.builder.where(DQLExpressions.lt(DQLExpressions.property(EmployeePayment.assignDate().fromAlias(alias(dql))), DQLExpressions.valueDate(CoreDateUtils.getNextDayFirstTimeMoment(_paymentEndTo.getData(), 1))));
        }
        
        if (_beginDateFrom.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "paymentData.beginDateFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(_beginDateFrom.getData()));

            dql.builder.where(DQLExpressions.ge(DQLExpressions.property(EmployeePayment.beginDate().fromAlias(alias(dql))), DQLExpressions.valueDate(CoreDateUtils.getDayFirstTimeMoment(_beginDateFrom.getData()))));
        }

        if (_beginDateTo.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "paymentData.beginDateTo", DateFormatter.DEFAULT_DATE_FORMATTER.format(_beginDateTo.getData()));

            dql.builder.where(DQLExpressions.lt(DQLExpressions.property(EmployeePayment.beginDate().fromAlias(alias(dql))), DQLExpressions.valueDate(CoreDateUtils.getNextDayFirstTimeMoment(_beginDateTo.getData(), 1))));
        }

        if (_endDateFrom.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "paymentData.endDateFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(_endDateFrom.getData()));

            dql.builder.where(DQLExpressions.ge(DQLExpressions.property(EmployeePayment.endDate().fromAlias(alias(dql))), DQLExpressions.valueDate(CoreDateUtils.getDayFirstTimeMoment(_endDateFrom.getData()))));
        }

        if (_endDateTo.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "paymentData.endDateTo", DateFormatter.DEFAULT_DATE_FORMATTER.format(_endDateTo.getData()));

            dql.builder.where(DQLExpressions.lt(DQLExpressions.property(EmployeePayment.endDate().fromAlias(alias(dql))), DQLExpressions.valueDate(CoreDateUtils.getNextDayFirstTimeMoment(_endDateTo.getData(), 1))));
        }

        if (_paymentAssignDateFrom.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "paymentData.paymentAssignDateFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(_paymentAssignDateFrom.getData()));

            dql.builder.where(DQLExpressions.ge(DQLExpressions.property(EmployeePayment.assignDate().fromAlias(alias(dql))), DQLExpressions.valueDate(CoreDateUtils.getDayFirstTimeMoment(_paymentAssignDateFrom.getData()))));
        }

        if (_paymentAssignDateTo.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "paymentData.paymentAssignDateTo", DateFormatter.DEFAULT_DATE_FORMATTER.format(_paymentAssignDateTo.getData()));

            dql.builder.where(DQLExpressions.lt(DQLExpressions.property(EmployeePayment.assignDate().fromAlias(alias(dql))), DQLExpressions.valueDate(CoreDateUtils.getNextDayFirstTimeMoment(_paymentAssignDateTo.getData(), 1))));
        }

        if (_paymentValFrom.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "paymentData.paymentValFrom", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(_paymentValFrom.getData()));

            dql.builder.where(DQLExpressions.ge(DQLExpressions.property(EmployeePayment.amount().fromAlias(alias(dql))), DQLExpressions.value((Number) _paymentValFrom.getData())));
        }

        if (_paymentValTo.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "paymentData.paymentValTo", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(_paymentValTo.getData()));

            dql.builder.where(DQLExpressions.le(DQLExpressions.property(EmployeePayment.amount().fromAlias(alias(dql))), DQLExpressions.value((Number) _paymentValTo.getData())));
        }
    }

    // Getters

    public IReportParam<List<PaymentType>> getPaymentType()
    {
        return _paymentType;
    }

    public IReportParam<List<Payment>> getPayment()
    {
        return _payment;
    }

    public IReportParam<Date> getPaymentBeginFrom()
    {
        return _paymentBeginFrom;
    }

    public IReportParam<Date> getPaymentEndTo()
    {
        return _paymentEndTo;
    }

    public IReportParam<Date> getBeginDateFrom()
    {
        return _beginDateFrom;
    }

    public IReportParam<Date> getBeginDateTo()
    {
        return _beginDateTo;
    }

    public IReportParam<Date> getEndDateFrom()
    {
        return _endDateFrom;
    }

    public IReportParam<Date> getEndDateTo()
    {
        return _endDateTo;
    }

    public IReportParam<Date> getPaymentAssignDateFrom()
    {
        return _paymentAssignDateFrom;
    }

    public IReportParam<Date> getPaymentAssignDateTo()
    {
        return _paymentAssignDateTo;
    }

    public IReportParam<Double> getPaymentValFrom()
    {
        return _paymentValFrom;
    }

    public IReportParam<Double> getPaymentValTo()
    {
        return _paymentValTo;
    }
}
