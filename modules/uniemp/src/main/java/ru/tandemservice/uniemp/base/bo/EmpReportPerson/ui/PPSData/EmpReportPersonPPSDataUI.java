package ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.PPSData;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.employeebase.base.bo.EmployeeReportPerson.ui.Add.EmployeeReportPersonAddUI;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportDQLModifierOwner;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportPrintInfo;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.ReportDQL;


/**
 * @author Vasily Zhukov
 * @since 07.02.2011
 */
public class EmpReportPersonPPSDataUI extends UIPresenter implements IReportDQLModifierOwner
{
    private String _selectedTab;

    private EmployeePPSDataParam _employeePPSData = new EmployeePPSDataParam();

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {
        
    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (printInfo.isExists(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET))
        {
            // фильтры модифицируют запросы
            _employeePPSData.modify(dql, printInfo);

            // печатные блоки модифицируют запросы и создают печатные колонки
        }
    }

    // Getters

    public EmployeePPSDataParam getEmployeePPSData()
    {
        return _employeePPSData;
    }

    // Getters & Setters

    public String getSelectedTab()
    {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        _selectedTab = selectedTab;
    }
}
