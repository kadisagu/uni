/**
 *$Id$
 */
package ru.tandemservice.uniemp.component.settings.PaymentOnFOTToPaymentsSettings.PaymentOnFOTToPaymentsSettingsPub;

import ru.tandemservice.uni.component.settings.abstractRelation.AbstractRelationList.AbstractRelationListModel;

/**
 * Create by: ashaburov
 * Date: 26.04.11
 */
public class Model extends AbstractRelationListModel
{
    public static String P_PAYMENTS_LIST = "paymentsList";
}
