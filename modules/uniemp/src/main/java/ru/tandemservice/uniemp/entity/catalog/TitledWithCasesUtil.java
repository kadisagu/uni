/* $Id$ */
package ru.tandemservice.uniemp.entity.catalog;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;

/**
 * @author esych
 * Created on: 20.01.2011
 */
public class TitledWithCasesUtil
{
    static public String getCaseTitle(IEntity obj, GrammaCase caze)
    {
        if (obj instanceof ITitledWithCases && (GrammaCase.NOMINATIVE == caze || GrammaCase.GENITIVE == caze
                || GrammaCase.DATIVE == caze || GrammaCase.ACCUSATIVE == caze || GrammaCase.INSTRUMENTAL == caze
                || GrammaCase.PREPOSITIONAL == caze))
            switch(caze)
            {
                case NOMINATIVE:
                    return ((ITitledWithCases)obj).getNominativeCaseTitle();
                case GENITIVE:
                    return ((ITitledWithCases)obj).getGenitiveCaseTitle();
                case DATIVE:
                    return ((ITitledWithCases)obj).getDativeCaseTitle();
                case ACCUSATIVE:
                    return ((ITitledWithCases)obj).getAccusativeCaseTitle();
                case INSTRUMENTAL:
                    return ((ITitledWithCases)obj).getInstrumentalCaseTitle();
                case PREPOSITIONAL:
                    return ((ITitledWithCases)obj).getPrepositionalCaseTitle();
            }

        if (obj instanceof ITitledWithPluralCases && (GrammaCase.NOMINATIVE_PL == caze || GrammaCase.GENITIVE_PL == caze
                || GrammaCase.DATIVE_PL == caze || GrammaCase.ACCUSATIVE_PL == caze || GrammaCase.INSTRUMENTAL_PL == caze
                || GrammaCase.PREPOSITIONAL_PL == caze))
            switch(caze)
            {
                case NOMINATIVE_PL:
                    return ((ITitledWithPluralCases)obj).getPluralNominativeCaseTitle();
                case GENITIVE_PL:
                    return ((ITitledWithPluralCases)obj).getPluralGenitiveCaseTitle();
                case DATIVE_PL:
                    return ((ITitledWithPluralCases)obj).getPluralDativeCaseTitle();
                case ACCUSATIVE_PL:
                    return ((ITitledWithPluralCases)obj).getPluralAccusativeCaseTitle();
                case INSTRUMENTAL_PL:
                    return ((ITitledWithPluralCases)obj).getPluralInstrumentalCaseTitle();
                case PREPOSITIONAL_PL:
                    return ((ITitledWithPluralCases)obj).getPluralPrepositionalCaseTitle();
            }

        return obj instanceof ITitled ? ((ITitled)obj).getTitle() : "";
    }
}
