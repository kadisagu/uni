/* $Id$ */
package ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.PrintBlock;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.employeebase.base.bo.EmployeeReportPerson.ui.Add.EmployeeReportPersonAddUI;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportDQLModifierOwner;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportPrintInfo;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.ReportDQL;


/**
 * @author Vasily Zhukov
 * @since 11.08.2011
 */
public class EmpReportPersonPrintBlockUI extends UIPresenter implements IReportDQLModifierOwner
{
    private EmployeePrintBlock _emplPrintBlock = new EmployeePrintBlock();

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {
        
    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (printInfo.isExists(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET))
        {
            // фильтры модифицируют запросы

            // печатные блоки модифицируют запросы и создают печатные колонки
            _emplPrintBlock.modify(dql, printInfo);
        }
    }
    
    // Getters

    public EmployeePrintBlock getEmplPrintBlock()
    {
        return _emplPrintBlock;
    }
}
