package ru.tandemservice.uniemp.entity.catalog;

import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import ru.tandemservice.uniemp.entity.catalog.gen.EmployeeTemplateDocumentGen;

/**
 * Печатный шаблон
 */
public class EmployeeTemplateDocument extends EmployeeTemplateDocumentGen implements ITemplateDocument
{
    @Override
    public byte[] getContent()
    {
        return CommonBaseUtil.getTemplateContent(this);
    }
}