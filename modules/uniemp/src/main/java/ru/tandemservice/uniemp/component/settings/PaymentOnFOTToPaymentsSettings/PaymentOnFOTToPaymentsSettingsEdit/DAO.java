/**
 *$Id$
 */
package ru.tandemservice.uniemp.component.settings.PaymentOnFOTToPaymentsSettings.PaymentOnFOTToPaymentsSettingsEdit;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import org.tandemframework.shared.commonbase.dao.IEntityRelation;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.employee.PaymentOnFOTToPaymentsRelation;

import java.util.ArrayList;
import java.util.List;

/**
 * Create by: ashaburov
 * Date: 26.04.11
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        final Payment payment = get(Payment.class, model.getPaymentId());

        model.setPayment(payment);

        model.setSelectedPaymentsList(payment.getPaymentsRelList());

        model.setPaymentsListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder subbuilder = new MQBuilder(PaymentOnFOTToPaymentsRelation.ENTITY_CLASS, "rel", new String[]{ PaymentOnFOTToPaymentsRelation.first().code().s() });
                subbuilder.add(MQExpression.eq("rel", IEntityRelation.L_SECOND, payment));

                MQBuilder builder = new MQBuilder(Payment.ENTITY_CLASS, "p");
                builder.add(MQExpression.notEq("p", Payment.code().s(), payment.getCode()));
                builder.add(MQExpression.notIn("p", Payment.code().s(), subbuilder));
                builder.add(MQExpression.like("p", Payment.P_TITLE, CoreStringUtils.escapeLike(filter)));
                builder.addOrder("p", Payment.title().s());
                return new ListResult<>(builder.<Payment>getResultList(getSession()));
            }
        });
    }

    @Override
    public void update(Model model)
    {
        List<Long> checkedPaymentList = new ArrayList<>();
        MQBuilder builder = new MQBuilder(PaymentOnFOTToPaymentsRelation.ENTITY_CLASS, "rel");
        builder.add(MQExpression.eq("rel", IEntityRelation.L_FIRST, model.getPayment()));
        List<PaymentOnFOTToPaymentsRelation> relList = builder.getResultList(getSession());

        for(PaymentOnFOTToPaymentsRelation rel : relList)
        {
            if(!model.getSelectedPaymentsList().contains(rel))
                getSession().delete(rel);
            else
                checkedPaymentList.add(rel.getSecond().getId());
        }
        getSession().flush();

        for(Payment payment : model.getSelectedPaymentsList())
        {
            if(!checkedPaymentList.contains(payment.getId()))
            {
                PaymentOnFOTToPaymentsRelation rel = new PaymentOnFOTToPaymentsRelation();
                rel.setFirst(model.getPayment());
                rel.setSecond(payment);
                getSession().save(rel);
            }
        }
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        if (!model.getSelectedPaymentsList().isEmpty())
        {
            MQBuilder builder = new MQBuilder(PaymentOnFOTToPaymentsRelation.ENTITY_CLASS, "rels");
            builder.add(MQExpression.eq("rels", PaymentOnFOTToPaymentsRelation.first().paymentUnit().code().s(), UniempDefines.PAYMENT_UNIT_FULL_PERCENT));
            builder.add(MQExpression.eq("rels", PaymentOnFOTToPaymentsRelation.second().paymentUnit().code().s(), UniempDefines.PAYMENT_UNIT_FULL_PERCENT));
            List<PaymentOnFOTToPaymentsRelation> relationList = builder.getResultList(getSession());

            Payment paymentFirst;

            for (PaymentOnFOTToPaymentsRelation relation : relationList)
            {
                if (relation.getSecond().equals(model.getPayment()))
                {
                    paymentFirst = relation.getFirst();

                    checkCycle(paymentFirst, relationList, model, errors);
                }
            }
        }
    }

    @Override
    public void checkCycle(Payment payment, List<PaymentOnFOTToPaymentsRelation> relationList, Model model, ErrorCollector errorCollector)
    {
        if (model.getSelectedPaymentsList().contains(payment))
        {
            errorCollector.add("Выплата «" + payment.getTitle() + "» уже включает в себя текущую выплату «" +
                    model.getPayment().getTitle() + "», а потому, не может быть включена в расчет текущей выплаты.", "selectedPaymentsList");
            return;
        }

        for (PaymentOnFOTToPaymentsRelation relation : relationList)
        {
            if (relation.getSecond().equals(payment))
            {
                checkCycle(relation.getFirst(), relationList, model, errorCollector);
            }
        }
    }
}
