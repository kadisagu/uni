/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.component.employee.EmployeeServiceLengthAddEdit;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import org.tandemframework.shared.employeebase.catalog.entity.codes.PostTypeCodes;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.entity.catalog.ServiceLengthType;
import ru.tandemservice.uniemp.entity.employee.EmpHistoryToServiceLengthTypeRelation;
import ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemFake;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eqValue;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author euroelessar
 * @since 05.04.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        if (null != model.getItemId())
        {
            model.setEmploymentHistoryItemFake(getNotNull(EmploymentHistoryItemFake.class, model.getItemId()));
            model.setServiceLengthType(
                    new DQLSelectBuilder().fromEntity(EmpHistoryToServiceLengthTypeRelation.class, "b").column(property(EmpHistoryToServiceLengthTypeRelation.serviceLengthType().fromAlias("b")))
                            .where(eqValue(property(EmpHistoryToServiceLengthTypeRelation.employmentHistoryItemBase().id().fromAlias("b")), model.getItemId()))
                            .createStatement(getSession()).<ServiceLengthType>uniqueResult()
            );
        }
        else
        {
            model.setEmploymentHistoryItemFake(new EmploymentHistoryItemFake());
            model.getEmploymentHistoryItemFake().setEmployee(getNotNull(Employee.class, model.getEmployeeId()));
        }
        model.setServiceLengthTypeList(getCatalogItemList(ServiceLengthType.class));
    }

    @Override
    public void update(Model model)
    {
        ErrorCollector errors = UserContext.getInstance().getErrorCollector();
        EmploymentHistoryItemFake itemFake = model.getEmploymentHistoryItemFake();
        if (null != itemFake.getDismissalDate()
                && itemFake.getAssignDate().compareTo(itemFake.getDismissalDate()) >= 0)
        {
            errors.add("Дата начала должна быть меньше даты окончания.", "assignDate", "dismissalDate");
        }
        else
        {
            MQBuilder builder = new MQBuilder(EmploymentHistoryItemFake.ENTITY_CLASS, "b");
            if (null != model.getItemId())
                builder.add(MQExpression.notEq("b", EmploymentHistoryItemFake.P_ID, model.getItemId()));
            builder.add(MQExpression.eq("b", EmploymentHistoryItemFake.L_EMPLOYEE, itemFake.getEmployee()));
            builder.add(MQExpression.exists(
                    new MQBuilder(EmpHistoryToServiceLengthTypeRelation.ENTITY_CLASS, "r")
                            .add(MQExpression.eqProperty("r", EmpHistoryToServiceLengthTypeRelation.employmentHistoryItemBase().id(), "b", EmploymentHistoryItemFake.id()))
                            .add(MQExpression.eq("r", EmpHistoryToServiceLengthTypeRelation.L_SERVICE_LENGTH_TYPE, model.getServiceLengthType()))
            ));

            if(null == itemFake.getDismissalDate())
            {
                builder.add(MQExpression.or(
                        MQExpression.isNull("b", EmploymentHistoryItemFake.P_DISMISSAL_DATE),
                        MQExpression.and(
                                MQExpression.isNotNull("b", EmploymentHistoryItemFake.P_DISMISSAL_DATE),
                                MQExpression.or(
                                MQExpression.greatOrEq("b", EmploymentHistoryItemFake.P_ASSIGN_DATE, itemFake.getAssignDate()),
                                MQExpression.greatOrEq("b", EmploymentHistoryItemFake.P_DISMISSAL_DATE, itemFake.getAssignDate())))));
            }
            else
            {
                builder.add(MQExpression.or(
                        MQExpression.and(
                                MQExpression.isNull("b", EmploymentHistoryItemFake.P_DISMISSAL_DATE),
                                MQExpression.lessOrEq("b", EmploymentHistoryItemFake.P_ASSIGN_DATE, itemFake.getAssignDate()),
                                MQExpression.lessOrEq("b", EmploymentHistoryItemFake.P_ASSIGN_DATE, itemFake.getDismissalDate())),
                        MQExpression.and(
                                MQExpression.isNotNull("b", EmploymentHistoryItemFake.P_DISMISSAL_DATE),
                                MQExpression.or(
                                    MQExpression.lessOrEq("b", EmploymentHistoryItemFake.P_ASSIGN_DATE, itemFake.getDismissalDate()),
                                    MQExpression.lessOrEq("b", EmploymentHistoryItemFake.P_DISMISSAL_DATE, itemFake.getDismissalDate())),
                                MQExpression.or(
                                    MQExpression.greatOrEq("b", EmploymentHistoryItemFake.P_ASSIGN_DATE, itemFake.getAssignDate()),
                                    MQExpression.greatOrEq("b", EmploymentHistoryItemFake.P_DISMISSAL_DATE, itemFake.getAssignDate()))
                                )));
            }
            if (builder.getResultCount(getSession()) > 0)
                errors.add("Данный стаж пересекается с другим стажем этого вида", "assignDate", "dismissalDate");
        }

        if (errors.hasErrors())
            return;

        if (model.getItemId() != null)
        {
            new DQLDeleteBuilder(EmpHistoryToServiceLengthTypeRelation.class)
                    .where(eqValue(property(EmpHistoryToServiceLengthTypeRelation.employmentHistoryItemBase().id()), model.getItemId()))
                    .createStatement(getSession()).execute();
        }

        itemFake.setPostType(getCatalogItem(PostType.class, PostTypeCodes.MAIN_JOB));
        saveOrUpdate(itemFake);

        {
            EmpHistoryToServiceLengthTypeRelation relation = new EmpHistoryToServiceLengthTypeRelation();
            relation.setEmploymentHistoryItemBase(itemFake);
            relation.setServiceLengthType(model.getServiceLengthType());

            save(relation);
        }


    }
}