/**
 *$Id$
 */
package ru.tandemservice.uniemp.component.settings.PaymentOnFOTToPaymentsSettings.PaymentOnFOTToPaymentsSettingsEdit;

import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;

import ru.tandemservice.uniemp.entity.catalog.Payment;

/**
 * Create by: ashaburov
 * Date: 26.04.11
 */
@Input({@Bind(key = "paymentId")})
public class Model
{
    private Long _paymentId;

    private Payment _payment;

    private IMultiSelectModel _paymentsListModel;
    private List<Payment> _selectedPaymentsList;


    //Getters & Setters

    public Payment getPayment()
    {
        return _payment;
    }

    public void setPayment(Payment payment)
    {
        _payment = payment;
    }

    public IMultiSelectModel getPaymentsListModel()
    {
        return _paymentsListModel;
    }

    public void setPaymentsListModel(IMultiSelectModel paymentsListModel)
    {
        _paymentsListModel = paymentsListModel;
    }

    public List<Payment> getSelectedPaymentsList()
    {
        return _selectedPaymentsList;
    }

    public void setSelectedPaymentsList(List<Payment> selectedPaymentsList)
    {
        _selectedPaymentsList = selectedPaymentsList;
    }

    public Long getPaymentId()
    {
        return _paymentId;
    }

    public void setPaymentId(Long paymentId)
    {
        _paymentId = paymentId;
    }
}
