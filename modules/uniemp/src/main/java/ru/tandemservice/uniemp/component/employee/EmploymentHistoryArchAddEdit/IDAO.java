/**
 *$Id$
 */
package ru.tandemservice.uniemp.component.employee.EmploymentHistoryArchAddEdit;

import ru.tandemservice.uniemp.component.employee.EmploymentHistoryAbsAddEdit.EmploymentHistoryAbstractIDAO;

/**
 * Create by ashaburov
 * Date 17.08.11
 */
public interface IDAO extends EmploymentHistoryAbstractIDAO<Model>
{
}
