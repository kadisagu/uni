/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.EmployeeHolidayAddEdit;

import java.util.List;

import org.tandemframework.core.component.Input;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uniemp.entity.catalog.HolidayType;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;

/**
 * @author dseleznev
 * Created on: 22.12.2008
 */
@Input(keys = { "employeePostId", "rowId" }, bindings = { "employeePostId", "employeeHoliday.id" })
public class Model
{
    private Long _employeePostId;
    private EmployeePost _employeePost;
    private EmployeeHoliday _employeeHoliday = new EmployeeHoliday();
    private List<HolidayType> _holidayTypesList;
    private boolean _periodRequired;

    public boolean isPeriodRequired()
    {
        return _periodRequired;
    }

    public void setPeriodRequired(boolean periodRequired)
    {
        _periodRequired = periodRequired;
    }

    public Long getEmployeePostId()
    {
        return _employeePostId;
    }

    public void setEmployeePostId(Long employeePostId)
    {
        this._employeePostId = employeePostId;
    }

    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    public void setEmployeePost(EmployeePost employeePost)
    {
        this._employeePost = employeePost;
    }

    public EmployeeHoliday getEmployeeHoliday()
    {
        return _employeeHoliday;
    }

    public void setEmployeeHoliday(EmployeeHoliday employeeHoliday)
    {
        this._employeeHoliday = employeeHoliday;
    }

    public List<HolidayType> getHolidayTypesList()
    {
        return _holidayTypesList;
    }

    public void setHolidayTypesList(List<HolidayType> holidayTypesList)
    {
        this._holidayTypesList = holidayTypesList;
    }

}