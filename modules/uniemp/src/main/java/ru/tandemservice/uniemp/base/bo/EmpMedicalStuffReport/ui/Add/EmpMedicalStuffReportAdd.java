/**
 *$Id:$
 */
package ru.tandemservice.uniemp.base.bo.EmpMedicalStuffReport.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.view.list.column.IStyleResolver;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import ru.tandemservice.uni.report.summaryReports.SummaryDataWrapper;
import ru.tandemservice.uniemp.entity.catalog.MedicalEducationLevel;
import ru.tandemservice.uniemp.entity.employee.EmployeeMedicalSpeciality;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 30.10.12
 */
@Configuration
public class EmpMedicalStuffReportAdd extends BusinessComponentManager
{
    // dataSource
    public static final String POST_TYPE_DS = "postTypeDS";
    public static final String MEDICAL_SPECIALITY_DS = "medicalSpecialityDS";
    public static final String EMPLOYEE_TYPE_DS = "employeeTypeDS";
    public static final String EDU_LEVEL_DS = "eduLevelDS";
    public static final String EMPLOYEE_MEDICAL_SPECIALITY_SEARCH_DS = "employeeMedicalSpecialitySearchDS";
    public static final String ADD_DATA_SEARCH_DS = "addDataSearchDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(POST_TYPE_DS, postTypeDSHandler()))
                .addDataSource(selectDS(MEDICAL_SPECIALITY_DS, medicalSpecialityDSHandler()))
                .addDataSource(selectDS(EMPLOYEE_TYPE_DS, employeeTypeDSHandler()).addColumn(EmployeeType.shortTitle().s()))
                .addDataSource(selectDS(EDU_LEVEL_DS, eduLevelDSHandler()))
                .addDataSource(searchListDS(EMPLOYEE_MEDICAL_SPECIALITY_SEARCH_DS, employeeMedicalSpecialitySearchColumns(), employeeMedicalSpecialitySearchDSHandler()))
                .addDataSource(searchListDS(ADD_DATA_SEARCH_DS, addDataSearchColumns(), addDataSearchDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> employeeTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EmployeeType.class)
                .order(EmployeeType.title())
                .filter(EmployeeType.title());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> eduLevelDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduLevel.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                dql.where(eq(property(alias, EduLevel.level().s()), value(Boolean.TRUE)));
            }
        }
                .order(EduLevel.title())
                .filter(EduLevel.title());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> employeeMedicalSpecialitySearchDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                List<SummaryDataWrapper<EmployeeMedicalSpeciality>> wrapperList = context.get(EmpMedicalStuffReportAddUI.PROP_SUMM_DATA_WRAPPER_LIST);

                return ListOutputBuilder.get(input, wrapperList == null ? Collections.emptyList() : wrapperList).pageable(false).build();
            }
        };
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> addDataSearchDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                List<Long> idList = context.get(EmpMedicalStuffReportAddUI.PROP_IGNORE_EMPLOYEE_ID_LIST);

                DataWrapper wrapper = new DataWrapper(1L, "Численность сотрудников, имеющих несколько врач. специальностей с равными категориями");
                wrapper.setProperty("ignoreEmployee", String.valueOf(idList.size()));

                List<DataWrapper> wrapperList = new ArrayList<>();
                wrapperList.add(wrapper);

                return ListOutputBuilder.get(input, wrapperList).pageable(false).build();
            }
        };

    }

    @Bean
    public ColumnListExtPoint employeeMedicalSpecialitySearchColumns()
    {
        IStyleResolver styleResolver = rowEntity -> {
            ITitled wrapper = (ITitled) rowEntity;
            if (wrapper.getTitle().equals("Итого") || wrapper.getTitle().equals("Из них женщин"))
                return "font-weight:bold;";

            return null;
        };
        return columnListExtPointBuilder(EMPLOYEE_MEDICAL_SPECIALITY_SEARCH_DS)
                .addColumn(textColumn("medicalSpecialityTitle", "title").styleResolver(styleResolver))
                .addColumn(actionColumn("total", "total", "onClickTotal").styleResolver(styleResolver))
                .addColumn(headerColumn("detail")
                        .addSubColumn(headerColumn("highCat")
                                .addSubColumn(actionColumn("high", "high", "onClickHigh").styleResolver(styleResolver).create())
                                .addSubColumn(actionColumn("first", "first", "onClickFirst").styleResolver(styleResolver).create())
                                .addSubColumn(actionColumn("second", "second", "onClickSecond").styleResolver(styleResolver).create())
                                .create())
                        .addSubColumn(actionColumn("certificate", "certificate", "onClickCertificate").styleResolver(styleResolver).create()))
                .create();
    }

    @Bean
    public ColumnListExtPoint addDataSearchColumns()
    {
        return columnListExtPointBuilder(ADD_DATA_SEARCH_DS)
                .addColumn(textColumn("title", "title").styleResolver(rowEntity -> "font-weight:bold;"))
                .addColumn(actionColumn("ignoreEmployee", "ignoreEmployee", "onClickIgnoreEmployee"))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> postTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), PostType.class)
                .filter(PostType.title())
                .order(PostType.title());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> medicalSpecialityDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), MedicalEducationLevel.class)
                .filter(MedicalEducationLevel.title())
                .order(MedicalEducationLevel.title());
    }
}
