/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.bo.EmpSummaryPPSReport.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import ru.tandemservice.uni.report.summaryReports.SummaryDataWrapper;
import ru.tandemservice.uniemp.base.bo.EmpSummaryPPSReport.EmpSummaryPPSReportManager;
import ru.tandemservice.uniemp.base.bo.EmpSummaryPPSReport.logic.EmployeeWrapper;
import ru.tandemservice.uniemp.base.bo.EmpSummaryPPSReport.ui.Detail.EmpSummaryPPSReportDetail;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Alexander Shaburov
 * @since 20.11.12
 */
public class EmpSummaryPPSReportAddUI extends UIPresenter
{
    public static final String PROP_PPS_LIST = "ppsList";
    public static final String PROP_PPS_WITH_SCIENCE_LIST = "ppsWithScienceList";
    public static final String PROP_PPS_WITH_HIGH_SCIENCE_LIST = "ppsWithHighScienceList";

    private Date _reportDate = new Date();
    private List<PostType> _postTypeList;

    private List<SummaryDataWrapper<EmployeeWrapper>> _summaryPPSList;
    private List<SummaryDataWrapper<EmployeeWrapper>> _summaryPPSWithScienceList;
    private List<SummaryDataWrapper<EmployeeWrapper>> _summaryPPSWithHighScienceList;
    private Map<Long, SummaryDataWrapper<EmployeeWrapper>> _summaryPPSMap;
    private Map<Long, SummaryDataWrapper<EmployeeWrapper>> _summaryPPSWithScienceMap;
    private Map<Long, SummaryDataWrapper<EmployeeWrapper>> _summaryPPSWithHighScienceMap;

    @Override
    public void onComponentRefresh()
    {
        List<EmployeeWrapper> employeeWrapperList = EmpSummaryPPSReportManager.instance().empSummaryPPSReportDao().getEmployeeWrapperList(_reportDate);

        _summaryPPSList = EmpSummaryPPSReportManager.instance().empSummaryPPSReportDao().getSummaryPPSList(_reportDate, _postTypeList, employeeWrapperList);
        _summaryPPSWithScienceList = EmpSummaryPPSReportManager.instance().empSummaryPPSReportDao().getSummaryPPSWithScienceList(_reportDate, _postTypeList, employeeWrapperList);
        _summaryPPSWithHighScienceList = EmpSummaryPPSReportManager.instance().empSummaryPPSReportDao().getSummaryPPSWithHighScienceList(_reportDate, _postTypeList, employeeWrapperList);
        
        _summaryPPSMap = new HashMap<>();
        for (SummaryDataWrapper<EmployeeWrapper> wrapper : _summaryPPSList)
            _summaryPPSMap.put(wrapper.getId(), wrapper);
        _summaryPPSWithScienceMap = new HashMap<>();
        for (SummaryDataWrapper<EmployeeWrapper> wrapper : _summaryPPSWithScienceList)
            _summaryPPSWithScienceMap.put(wrapper.getId(), wrapper);
        _summaryPPSWithHighScienceMap = new HashMap<>();
        for (SummaryDataWrapper<EmployeeWrapper> wrapper : _summaryPPSWithHighScienceList)
            _summaryPPSWithHighScienceMap.put(wrapper.getId(), wrapper);
    }

    public void onClickApply()
    {
        List<EmployeeWrapper> employeeWrapperList = EmpSummaryPPSReportManager.instance().empSummaryPPSReportDao().getEmployeeWrapperList(_reportDate);

        _summaryPPSList = EmpSummaryPPSReportManager.instance().empSummaryPPSReportDao().getSummaryPPSList(_reportDate, _postTypeList, employeeWrapperList);
        _summaryPPSWithScienceList = EmpSummaryPPSReportManager.instance().empSummaryPPSReportDao().getSummaryPPSWithScienceList(_reportDate, _postTypeList, employeeWrapperList);
        _summaryPPSWithHighScienceList = EmpSummaryPPSReportManager.instance().empSummaryPPSReportDao().getSummaryPPSWithHighScienceList(_reportDate, _postTypeList, employeeWrapperList);
        
        _summaryPPSMap = new HashMap<>();
        for (SummaryDataWrapper<EmployeeWrapper> wrapper : _summaryPPSList)
            _summaryPPSMap.put(wrapper.getId(), wrapper);
        _summaryPPSWithScienceMap = new HashMap<>();
        for (SummaryDataWrapper<EmployeeWrapper> wrapper : _summaryPPSWithScienceList)
            _summaryPPSWithScienceMap.put(wrapper.getId(), wrapper);
        _summaryPPSWithHighScienceMap = new HashMap<>();
        for (SummaryDataWrapper<EmployeeWrapper> wrapper : _summaryPPSWithHighScienceList)
            _summaryPPSWithHighScienceMap.put(wrapper.getId(), wrapper);
    }

    public void onClickPrintReport()
    {
        RtfDocument document = EmpSummaryPPSReportManager.instance().empSummaryPPSReportDao().getReportDocument(_reportDate, _postTypeList);

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("summaryPPS_" + new DateFormatter("dd_MM_yyyy").format(_reportDate) + ".rtf").document(document), false);
    }

    public boolean isColumnVisible(String postTypeCode)
    {
        if (_postTypeList == null || _postTypeList.isEmpty())
            return true;

        for (PostType postType : !_postTypeList.isEmpty() ? _postTypeList : DataAccessServices.dao().getList(PostType.class, PostType.title().s()))
            if (postType.getCode().equals(postTypeCode))
                return true;
        return false;
    }

    // listeners for columns

    public void onClickPPS1()
    {
        onClickPPS("1");
    }

    public void onClickPPS2()
    {
        onClickPPS("2");
    }

    public void onClickPPS3()
    {
        onClickPPS("3");
    }

    public void onClickPPS4()
    {
        onClickPPS("4");
    }

    public void onClickPPS5()
    {
        onClickPPS("5");
    }

    public void onClickPPS6()
    {
        onClickPPS("6");
    }

    public void onClickPPS7()
    {
        onClickPPS("7");
    }

    public void onClickPPS8()
    {
        onClickPPS("8");
    }

    public void onClickPPSHourlyPaid()
    {
        onClickPPS("hourlyPaid");
    }
    
    private void onClickPPS(String columnName)
    {
        SummaryDataWrapper current = _summaryPPSMap.get(getListenerParameterAsLong());

        String columnTitle;
        if (columnName.equals("hourlyPaid"))
            columnTitle = "Почасовая оплата";
        else
            columnTitle = DataAccessServices.dao().get(PostType.class, PostType.code().s(), columnName).getTitle();


        getActivationBuilder().asRegionDialog(EmpSummaryPPSReportDetail.class)
                .parameter("employeeIdList", current.getMatchedIds(columnName))
                .parameter("reportDate", _reportDate)
                .parameter("tableTitle", "Численность профессорско-преподавательского состава")
                .parameter("columnTitle", columnTitle)
                .parameter("rowTitle", current.getTitle())
                .activate();
    }

    public void onClickPPSWithScience1()
    {
        onClickPPSWithScience("1");
    }

    public void onClickPPSWithScience2()
    {
        onClickPPSWithScience("2");
    }

    public void onClickPPSWithScience3()
    {
        onClickPPSWithScience("3");
    }

    public void onClickPPSWithScience4()
    {
        onClickPPSWithScience("4");
    }

    public void onClickPPSWithScience5()
    {
        onClickPPSWithScience("5");
    }

    public void onClickPPSWithScience6()
    {
        onClickPPSWithScience("6");
    }

    public void onClickPPSWithScience7()
    {
        onClickPPSWithScience("7");
    }

    public void onClickPPSWithScience8()
    {
        onClickPPSWithScience("8");
    }

    public void onClickPPSWithScienceHourlyPaid()
    {
        onClickPPSWithScience("hourlyPaid");
    }

    private void onClickPPSWithScience(String columnName)
    {
        SummaryDataWrapper current = _summaryPPSWithScienceMap.get(getListenerParameterAsLong());

        String columnTitle;
        if (columnName.equals("hourlyPaid"))
            columnTitle = "Почасовая оплата";
        else
            columnTitle = DataAccessServices.dao().get(PostType.class, PostType.code().s(), columnName).getTitle();


        getActivationBuilder().asRegionDialog(EmpSummaryPPSReportDetail.class)
                .parameter("employeeIdList", current.getMatchedIds(columnName))
                .parameter("reportDate", _reportDate)
                .parameter("tableTitle", "Численность профессорско-преподавательского состава с учёной степенью и/или званием")
                .parameter("columnTitle", columnTitle)
                .parameter("rowTitle", current.getTitle())
                .activate();
    }

    public void onClickPPSWithHighScience1()
    {
        onClickPPSWithHighScience("1");
    }

    public void onClickPPSWithHighScience2()
    {
        onClickPPSWithHighScience("2");
    }

    public void onClickPPSWithHighScience3()
    {
        onClickPPSWithHighScience("3");
    }

    public void onClickPPSWithHighScience4()
    {
        onClickPPSWithHighScience("4");
    }

    public void onClickPPSWithHighScience5()
    {
        onClickPPSWithHighScience("5");
    }

    public void onClickPPSWithHighScience6()
    {
        onClickPPSWithHighScience("6");
    }

    public void onClickPPSWithHighScience7()
    {
        onClickPPSWithHighScience("7");
    }

    public void onClickPPSWithHighScience8()
    {
        onClickPPSWithHighScience("8");
    }

    public void onClickPPSWithHighScienceHourlyPaid()
    {
        onClickPPSWithHighScience("hourlyPaid");
    }

    private void onClickPPSWithHighScience(String columnName)
    {
        SummaryDataWrapper current = _summaryPPSWithHighScienceMap.get(getListenerParameterAsLong());

        String columnTitle;
        if (columnName.equals("hourlyPaid"))
            columnTitle = "Почасовая оплата";
        else
            columnTitle = DataAccessServices.dao().get(PostType.class, PostType.code().s(), columnName).getTitle();


        getActivationBuilder().asRegionDialog(EmpSummaryPPSReportDetail.class)
                .parameter("employeeIdList", current.getMatchedIds(columnName))
                .parameter("reportDate", _reportDate)
                .parameter("tableTitle", "Численность профессорско-преподавательского состава с учёной степенью доктора наук и/или званием профессора")
                .parameter("columnTitle", columnTitle)
                .parameter("rowTitle", current.getTitle())
                .activate();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(PROP_PPS_LIST, _summaryPPSList);
        dataSource.put(PROP_PPS_WITH_SCIENCE_LIST, _summaryPPSWithScienceList);
        dataSource.put(PROP_PPS_WITH_HIGH_SCIENCE_LIST, _summaryPPSWithHighScienceList);
    }

    // Getters & Setters

    public Date getReportDate()
    {
        return _reportDate;
    }

    public void setReportDate(Date reportDate)
    {
        _reportDate = reportDate;
    }

    public List<PostType> getPostTypeList()
    {
        return _postTypeList;
    }

    public void setPostTypeList(List<PostType> postTypeList)
    {
        _postTypeList = postTypeList;
    }
}
