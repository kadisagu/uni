/**
 *$Id$
 */
package ru.tandemservice.uniemp.dao;

import org.tandemframework.common.component.selection.MQListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.tapsupport.component.selection.CommonSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;

/**
 * Create by ashaburov
 * Date 07.10.11
 * <p/>
 * Модель Источников финансирования.
 */
public class FinancingSourceSingleSelectModel extends CommonSingleSelectModel
{
    @Override
    protected IListResultBuilder createBuilder(String filter, Object o)
    {
        MQBuilder builder = new MQBuilder(FinancingSource.ENTITY_CLASS, "b");
        builder.add(MQExpression.like("b", FinancingSource.title().s(), CoreStringUtils.escapeLike(filter)));
        if (o != null)
            builder.add(MQExpression.eq("b", FinancingSource.id().s(), o));
        builder.addOrder("b", FinancingSource.title().s(), OrderDirection.asc);

        return new MQListResultBuilder(builder);
    }
}
