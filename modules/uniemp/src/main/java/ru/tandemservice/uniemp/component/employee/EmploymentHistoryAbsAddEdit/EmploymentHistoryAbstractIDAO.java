/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.EmploymentHistoryAbsAddEdit;

import ru.tandemservice.uniemp.dao.IUniempDAO;

/**
 * @author dseleznev
 * Created on: 24.12.2008
 */
public interface EmploymentHistoryAbstractIDAO<M extends EmploymentHistoryAbstractModel> extends IUniempDAO<M>
{
}