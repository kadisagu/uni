package ru.tandemservice.uniemp.entity.catalog;

import org.tandemframework.core.tool.tree.IHierarchyItem;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.gen.EmployeeVPO1ReportLinesGen;

import java.util.List;

/**
 * Строки отчета «ВПО-1 - Сведения о персонале учреждения»
 */
public class EmployeeVPO1ReportLines extends EmployeeVPO1ReportLinesGen implements IHierarchyItem
{
    public static final String L_POST_TYPE = "postType";
    public static final String P_POSTS_LIST = "postsList";
    
    public EmployeeType getPostType()
    {
        return UniempDaoFacade.getUniempDAO().getVPOReportLineEmployeeType(this);
    }
    
    public List<PostBoundedWithQGandQL> getPostsList()
    {
        return UniempDaoFacade.getUniempDAO().getVPOReportLinePosts(this);
    }

    @Override
    public IHierarchyItem getHierarhyParent()
    {
        return this.getParent();
    }
}