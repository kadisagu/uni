/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.staffList.Add;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author dseleznev
 * Created on: 05.08.2009
 */
public interface IDAO extends IUniDao<Model>
{
    void prepareOrgUnitTypesDataSource(Model model);
    
    List<OrgUnit> getChildOrgUnits(OrgUnit orgUnit, List<OrgUnit> orgUnitsToTake, Map<Long, OrgUnitTypeWrapper> orgUnitTypesMap, Date actualDate);

    public List<OrgUnit> getOrgUnitWithActiveStaffList();
}