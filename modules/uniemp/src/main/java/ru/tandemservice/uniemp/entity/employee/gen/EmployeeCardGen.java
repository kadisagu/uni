package ru.tandemservice.uniemp.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import ru.tandemservice.uniemp.entity.employee.EmployeeCard;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Дополнительная информация о сотруднике
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeCardGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.employee.EmployeeCard";
    public static final String ENTITY_NAME = "employeeCard";
    public static final int VERSION_HASH = 1611071088;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE = "employee";
    public static final String P_SCIENTIFIC_WORK = "scientificWork";
    public static final String P_INVENTION = "invention";

    private Employee _employee;     // Кадровый ресурс
    private boolean _scientificWork;     // Научные труды
    private boolean _invention;     // Изобретения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Кадровый ресурс. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public Employee getEmployee()
    {
        return _employee;
    }

    /**
     * @param employee Кадровый ресурс. Свойство не может быть null и должно быть уникальным.
     */
    public void setEmployee(Employee employee)
    {
        dirty(_employee, employee);
        _employee = employee;
    }

    /**
     * @return Научные труды. Свойство не может быть null.
     */
    @NotNull
    public boolean isScientificWork()
    {
        return _scientificWork;
    }

    /**
     * @param scientificWork Научные труды. Свойство не может быть null.
     */
    public void setScientificWork(boolean scientificWork)
    {
        dirty(_scientificWork, scientificWork);
        _scientificWork = scientificWork;
    }

    /**
     * @return Изобретения. Свойство не может быть null.
     */
    @NotNull
    public boolean isInvention()
    {
        return _invention;
    }

    /**
     * @param invention Изобретения. Свойство не может быть null.
     */
    public void setInvention(boolean invention)
    {
        dirty(_invention, invention);
        _invention = invention;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EmployeeCardGen)
        {
            setEmployee(((EmployeeCard)another).getEmployee());
            setScientificWork(((EmployeeCard)another).isScientificWork());
            setInvention(((EmployeeCard)another).isInvention());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeCardGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeCard.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeCard();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "employee":
                    return obj.getEmployee();
                case "scientificWork":
                    return obj.isScientificWork();
                case "invention":
                    return obj.isInvention();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "employee":
                    obj.setEmployee((Employee) value);
                    return;
                case "scientificWork":
                    obj.setScientificWork((Boolean) value);
                    return;
                case "invention":
                    obj.setInvention((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "employee":
                        return true;
                case "scientificWork":
                        return true;
                case "invention":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "employee":
                    return true;
                case "scientificWork":
                    return true;
                case "invention":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "employee":
                    return Employee.class;
                case "scientificWork":
                    return Boolean.class;
                case "invention":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeeCard> _dslPath = new Path<EmployeeCard>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeCard");
    }
            

    /**
     * @return Кадровый ресурс. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeCard#getEmployee()
     */
    public static Employee.Path<Employee> employee()
    {
        return _dslPath.employee();
    }

    /**
     * @return Научные труды. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeCard#isScientificWork()
     */
    public static PropertyPath<Boolean> scientificWork()
    {
        return _dslPath.scientificWork();
    }

    /**
     * @return Изобретения. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeCard#isInvention()
     */
    public static PropertyPath<Boolean> invention()
    {
        return _dslPath.invention();
    }

    public static class Path<E extends EmployeeCard> extends EntityPath<E>
    {
        private Employee.Path<Employee> _employee;
        private PropertyPath<Boolean> _scientificWork;
        private PropertyPath<Boolean> _invention;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Кадровый ресурс. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeCard#getEmployee()
     */
        public Employee.Path<Employee> employee()
        {
            if(_employee == null )
                _employee = new Employee.Path<Employee>(L_EMPLOYEE, this);
            return _employee;
        }

    /**
     * @return Научные труды. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeCard#isScientificWork()
     */
        public PropertyPath<Boolean> scientificWork()
        {
            if(_scientificWork == null )
                _scientificWork = new PropertyPath<Boolean>(EmployeeCardGen.P_SCIENTIFIC_WORK, this);
            return _scientificWork;
        }

    /**
     * @return Изобретения. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeCard#isInvention()
     */
        public PropertyPath<Boolean> invention()
        {
            if(_invention == null )
                _invention = new PropertyPath<Boolean>(EmployeeCardGen.P_INVENTION, this);
            return _invention;
        }

        public Class getEntityClass()
        {
            return EmployeeCard.class;
        }

        public String getEntityName()
        {
            return "employeeCard";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
