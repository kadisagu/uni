/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.component.employee.VacationCertificatePrintForm;

import java.util.Date;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;

import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;

/**
 * @author dseleznev
 * Created on: 24.03.2011
 */
@Input(@Bind(key = "employeeHolidayId", binding = "employeeHolidayId"))
public class Model
{
    private Long _employeeHolidayId;
    private EmployeeHoliday _employeeHoliday;

    private Date _printDate;
    private String _printNumber;

    private Date _orderDate;
    private String _orderNumber;

    public Long getEmployeeHolidayId()
    {
        return _employeeHolidayId;
    }

    public void setEmployeeHolidayId(Long employeeHolidayId)
    {
        this._employeeHolidayId = employeeHolidayId;
    }

    public EmployeeHoliday getEmployeeHoliday()
    {
        return _employeeHoliday;
    }

    public void setEmployeeHoliday(EmployeeHoliday employeeHoliday)
    {
        this._employeeHoliday = employeeHoliday;
    }

    public Date getPrintDate()
    {
        return _printDate;
    }

    public void setPrintDate(Date printDate)
    {
        this._printDate = printDate;
    }

    public String getPrintNumber()
    {
        return _printNumber;
    }

    public void setPrintNumber(String printNumber)
    {
        this._printNumber = printNumber;
    }

    public Date getOrderDate()
    {
        return _orderDate;
    }

    public void setOrderDate(Date orderDate)
    {
        this._orderDate = orderDate;
    }

    public String getOrderNumber()
    {
        return _orderNumber;
    }

    public void setOrderNumber(String orderNumber)
    {
        this._orderNumber = orderNumber;
    }
}