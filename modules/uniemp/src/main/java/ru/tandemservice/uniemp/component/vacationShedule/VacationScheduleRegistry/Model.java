/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.vacationShedule.VacationScheduleRegistry;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uniemp.entity.employee.VacationSchedule;

/**
 * @author dseleznev
 * Created on: 26.01.2011
 */
public class Model
{
    private IDataSettings _settings;
    private ISelectModel _orgUnitList;

    private DynamicListDataSource<VacationSchedule> _dataSource;

    public DynamicListDataSource<VacationSchedule> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<VacationSchedule> dataSource)
    {
        this._dataSource = dataSource;
    }

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        this._settings = settings;
    }

    public ISelectModel getOrgUnitList()
    {
        return _orgUnitList;
    }

    public void setOrgUnitList(ISelectModel orgUnitList)
    {
        this._orgUnitList = orgUnitList;
    }
}