/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.bo.EmpSummaryPPSReport.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeePostStatusCodes;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeeTypeCodes;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import org.tandemframework.shared.person.base.entity.PersonAcademicStatus;
import org.tandemframework.shared.person.catalog.entity.ScienceDegree;
import org.tandemframework.shared.person.catalog.entity.ScienceStatus;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.base.bo.EmpSummaryPPSReport.ui.Detail.EmpSummaryPPSReportDetailUI;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 01.04.13
 */
public class EmpSummaryPPSReportDetailSearchDSHandler extends DefaultSearchDataSourceHandler
{
    public EmpSummaryPPSReportDetailSearchDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final Collection<Long> employeeIdList = context.get(EmpSummaryPPSReportDetailUI.PROP_EMPLOYEE_ID_LIST);
        final Date reportDate = context.get(EmpSummaryPPSReportDetailUI.PROP_REPORT_DATE);

        final List<EmployeePost> employeePostList = new ArrayList<>();
        final Map<Long, List<ScienceDegree>> personDegreeMap = new HashMap<>();
        final Map<Long, List<ScienceStatus>> personStatusMap = new HashMap<>();
        final Map<Long, BigDecimal> postStaffRateMap = new HashMap<>();

        BatchUtils.execute(employeeIdList, 128, new BatchUtils.Action<Long>()
        {
            @Override
            public void execute(Collection<Long> elements)
            {
                employeePostList.addAll(new DQLSelectBuilder().fromEntity(EmployeePost.class, "ep").column(property("ep"))
                        .where(in(property(EmployeePost.employee().id().fromAlias("ep")), elements))
                        .where(eq(property(EmployeePost.postRelation().postBoundedWithQGandQL().post().employeeType().code().fromAlias("ep")), value(EmployeeTypeCodes.EDU_STAFF)))
                        .where(ne(property(EmployeePost.postStatus().code().fromAlias("ep")), value(EmployeePostStatusCodes.STATUS_POSSIBLE)))
                        .where(le(property(EmployeePost.postDate().fromAlias("ep")), value(reportDate, PropertyType.DATE)))
                        .where(or(
                                isNull(property(EmployeePost.dismissalDate().fromAlias("ep"))),
                                ge(property(EmployeePost.dismissalDate().fromAlias("ep")), value(reportDate, PropertyType.DATE))
                        ))
                        .createStatement(context.getSession()).<EmployeePost>list());
            }
        });

        BatchUtils.execute(UniBaseUtils.getPropertiesSet(employeePostList, EmployeePost.employee().person().id().s()), 128, new BatchUtils.Action<Object>()
        {
            @Override
            public void execute(Collection<Object> elements)
            {
                List<PersonAcademicDegree> list = new DQLSelectBuilder().fromEntity(PersonAcademicDegree.class, "d").column(property("d"))
                        .where(in(property(PersonAcademicDegree.person().id().fromAlias("d")), elements))
                        .createStatement(context.getSession()).list();

                for (PersonAcademicDegree academicDegree : list)
                {
                    List<ScienceDegree> degreeList = personDegreeMap.get(academicDegree.getPerson().getId());
                    if (degreeList == null)
                        personDegreeMap.put(academicDegree.getPerson().getId(), degreeList = new ArrayList<>());
                    degreeList.add(academicDegree.getAcademicDegree());
                }
            }
        });

        BatchUtils.execute(UniBaseUtils.getPropertiesSet(employeePostList, EmployeePost.employee().person().id().s()), 128, new BatchUtils.Action<Object>()
        {
            @Override
            public void execute(Collection<Object> elements)
            {
                List<PersonAcademicStatus> list = new DQLSelectBuilder().fromEntity(PersonAcademicStatus.class, "s").column(property("s"))
                        .where(in(property(PersonAcademicStatus.person().id().fromAlias("s")), elements))
                        .createStatement(context.getSession()).list();

                for (PersonAcademicStatus academicDegree : list)
                {
                    List<ScienceStatus> statusList = personStatusMap.get(academicDegree.getPerson().getId());
                    if (statusList == null)
                        personStatusMap.put(academicDegree.getPerson().getId(), statusList = new ArrayList<>());
                    statusList.add(academicDegree.getAcademicStatus());
                }
            }
        });

        BatchUtils.execute(UniBaseDao.ids(employeePostList), 128, new BatchUtils.Action<Long>()
        {
            @Override
            public void execute(Collection<Long> elements)
            {
                List<EmployeePostStaffRateItem> list = new DQLSelectBuilder().fromEntity(EmployeePostStaffRateItem.class, "r").column("r")
                        .where(in(property(EmployeePostStaffRateItem.employeePost().id().fromAlias("r")), elements))
                        .createStatement(context.getSession()).list();

                for (EmployeePostStaffRateItem item : list)
                {
                    BigDecimal rate = postStaffRateMap.get(item.getEmployeePost().getId());
                    if (rate == null)
                        postStaffRateMap.put(item.getEmployeePost().getId(), new BigDecimal(item.getStaffRate()).setScale(2, RoundingMode.HALF_UP));
                    else
                        postStaffRateMap.put(item.getEmployeePost().getId(), new BigDecimal(rate.doubleValue() + item.getStaffRate()).setScale(2, RoundingMode.HALF_UP));
                }
            }
        });

        List<DataWrapper> wrapperList = new ArrayList<>();
        for (EmployeePost post : employeePostList)
        {
            DataWrapper wrapper = new DataWrapper(post.getId(), post.getPerson().getFullFio());
            wrapper.setProperty("employeeCode", post.getEmployee().getEmployeeCode());
            wrapper.setProperty("scienceDegree", CommonBaseUtil.getPropertiesList(personDegreeMap.get(post.getPerson().getId()), ScienceDegree.title().s()));
            wrapper.setProperty("scienceStatus", CommonBaseUtil.getPropertiesList(personStatusMap.get(post.getPerson().getId()), ScienceStatus.title().s()));
            wrapper.setProperty("post", post.getPostRelation().getPostBoundedWithQGandQL().getFullTitleWithSalary());
            wrapper.setProperty("orgUnit", post.getOrgUnit().getTitleWithType());
            wrapper.setProperty("postType", post.getPostType().getTitle());
            wrapper.setProperty("staffRate", postStaffRateMap.get(post.getId()) == null ? 0d : postStaffRateMap.get(post.getId()));
            wrapper.setProperty("hourlyPaid", post.isHourlyPaid());

            wrapper.setProperty("employeeId", post.getEmployee().getId());

            wrapperList.add(wrapper);
        }

        Collections.sort(wrapperList, new EntityComparator<>(input.getEntityOrder(), new EntityOrder("employeeId")));

        return ListOutputBuilder.get(input, wrapperList).pageable(true).build();
    }
}
