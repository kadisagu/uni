/**
 *$Id$
 */
package ru.tandemservice.uniemp.dao;

import org.tandemframework.common.component.selection.MQListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.tapsupport.component.selection.CommonSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;

import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;

/**
 * Create by ashaburov
 * Date 07.10.11
 */
public class FinancingSourceItemSingleSelectModel extends CommonSingleSelectModel
{
    private FinancingSource _financingSource;

    public FinancingSourceItemSingleSelectModel(FinancingSource financingSource)
    {
        _financingSource = financingSource;
    }

    public FinancingSourceItemSingleSelectModel()
    {
        _financingSource = null;
    }

    @Override
    protected IListResultBuilder createBuilder(String filter, Object o)
    {
        MQBuilder builder = new MQBuilder(FinancingSourceItem.ENTITY_CLASS, "b");
        builder.add(MQExpression.like("b", FinancingSourceItem.title().s(), CoreStringUtils.escapeLike(filter)));
        if (_financingSource != null)
            builder.add(MQExpression.eq("b", FinancingSourceItem.financingSource().s(), _financingSource));
        if (o != null)
            builder.add(MQExpression.eq("b", FinancingSourceItem.id().s(), o));
        builder.addOrder("b", FinancingSourceItem.title().s(), OrderDirection.asc);

        return new MQListResultBuilder(builder);
    }
}
