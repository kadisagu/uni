/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniemp.component.vacationShedule.VacationSchedulePub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.employee.VacationSchedule;
import ru.tandemservice.uniemp.entity.employee.VacationScheduleItem;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.component.visa.VisaList.IVisaOwnerModel;

import java.util.Map;

/**
 * @author dseleznev
 * Created on: 10.01.2011
 */
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "vacationScheduleId"),
        @Bind(key = "selectedTab", binding = "selectedTab")
})
public class Model implements IVisaOwnerModel
{
    private IDataSettings _settings;
    private CommonPostfixPermissionModel _secModel;

    private Long _vacationScheduleId;
    private VacationSchedule _vacationSchedule;

    private String _selectedTab;
    private Long _visingStatus;
    private DynamicListDataSource<VacationScheduleItem> _dataSource;
    private Map<String, Object> _visaListParameters = ParametersMap.createWith("visaOwnerModel", this);

    public boolean isNeedVising()
    {
        return _visingStatus != null;
    }

    // IVisaOwnerModel

    @Override
    public IAbstractDocument getDocument()
    {
        return _vacationSchedule;
    }

    @Override
    public boolean isVisaListModificable()
    {
        return UniempDefines.STAFF_LIST_STATUS_FORMING.equals(_vacationSchedule.getState().getCode());
    }

    @Override
    public String getSecPostfix()
    {
        return "vacationSchedule";
    }

    // Getters & Setters

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }

    public Long getVacationScheduleId()
    {
        return _vacationScheduleId;
    }

    public void setVacationScheduleId(Long vacationScheduleId)
    {
        this._vacationScheduleId = vacationScheduleId;
    }

    public VacationSchedule getVacationSchedule()
    {
        return _vacationSchedule;
    }

    public void setVacationSchedule(VacationSchedule vacationSchedule)
    {
        _vacationSchedule = vacationSchedule;
    }

    public String getSelectedTab()
    {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        _selectedTab = selectedTab;
    }

    public Long getVisingStatus()
    {
        return _visingStatus;
    }

    public void setVisingStatus(Long visingStatus)
    {
        _visingStatus = visingStatus;
    }

    public DynamicListDataSource<VacationScheduleItem> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<VacationScheduleItem> dataSource)
    {
        _dataSource = dataSource;
    }

    public Map<String, Object> getVisaListParameters()
    {
        return _visaListParameters;
    }

    public void setVisaListParameters(Map<String, Object> visaListParameters)
    {
        _visaListParameters = visaListParameters;
    }
}