package ru.tandemservice.uniemp.entity.employee;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.sec.ISecLocalEntityOwner;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.gen.StaffListGen;

import java.util.Collection;
import java.util.Collections;

public class StaffList extends StaffListGen implements ISecLocalEntityOwner
{
    public static final String P_CANT_BE_DELETED = "cantBeDeleted";
    public static final String P_CANT_BE_EDITED = "cantBeEdited";
    public static final String P_CREATION_DATE_STR = "creationDateStr";
    public static final String P_TITLE = "title";

    public boolean isCantBeDeleted()
    {
        return UniempDaoFacade.getStaffListDAO().getStaffListItemsList(this).size() > 0 || UniempDaoFacade.getStaffListDAO().getStaffListAllocationItemsList(this).size() > 0 || isCantBeEdited();
    }

    public boolean isCantBeEdited()
    {
        return getStaffListState().getCode().equals(UniempDefines.STAFF_LIST_STATUS_ACCEPTING) || getStaffListState().getCode().equals(UniempDefines.STAFF_LIST_STATUS_ACCEPTED) || getStaffListState().getCode().equals(UniempDefines.STAFF_LIST_STATUS_ACTIVE) || getStaffListState().getCode().equals(UniempDefines.STAFF_LIST_STATUS_ARCHIVE) || getStaffListState().getCode().equals(UniempDefines.STAFF_LIST_STATUS_REJECTED);
    }
    
    public String getCreationDateStr()
    {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(getCreationDate());
    }
    
    public String getTitle()
    {
        return "№" + getNumber() + " от " + getCreationDateStr() + ", " + getOrgUnit().getFullTitle();
    }
    
    @Override
    public Collection<IEntity> getSecLocalEntities()
    {
        return Collections.<IEntity>singletonList(this.getOrgUnit());
    }
}