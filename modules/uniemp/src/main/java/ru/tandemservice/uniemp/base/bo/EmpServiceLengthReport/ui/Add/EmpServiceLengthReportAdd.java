/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.bo.EmpServiceLengthReport.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.uniemp.entity.catalog.ServiceLengthType;

/**
 * @author Alexander Shaburov
 * @since 25.01.13
 */
@Configuration
public class EmpServiceLengthReportAdd extends BusinessComponentManager
{
    public static final String ORG_UNIT_DS = "orgUnitDS";
    public static final String EMP_TYPE_DS = "empTypeDS";
    public static final String POST_DS = "postDS";
    public static final String EMP_POST_STATUS_DS = "empPostStatusDS";
    public static final String SEX_DS = "sexDS";
    public static final String SERV_LENGH_TYPE_DS = "servLenghTypeDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ORG_UNIT_DS, orgUnitDSHandler()).addColumn(OrgUnit.titleWithType().s()))
                .addDataSource(selectDS(EMP_TYPE_DS, empTypeDSHandler()))
                .addDataSource(selectDS(POST_DS, postDSHandler()).addColumn(PostBoundedWithQGandQL.fullTitle().s()))
                .addDataSource(selectDS(EMP_POST_STATUS_DS, empPostStatusDSHandler()))
                .addDataSource(selectDS(SEX_DS, sexDSHandler()))
                .addDataSource(selectDS(SERV_LENGH_TYPE_DS, servLenghTypeDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> orgUnitDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), OrgUnit.class)
                .order(OrgUnit.title())
                .filter(OrgUnit.title())
                .pageable(true);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> empTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EmployeeType.class)
                .order(EmployeeType.title())
                .filter(EmployeeType.title());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> postDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), PostBoundedWithQGandQL.class)
                .where(PostBoundedWithQGandQL.post().employeeType(), EmpServiceLengthReportAddUI.EMP_TYPE)
                .order(PostBoundedWithQGandQL.title())
                .filter(PostBoundedWithQGandQL.title())
                .pageable(true);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> empPostStatusDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EmployeePostStatus.class)
                .order(EmployeePostStatus.title())
                .filter(EmployeePostStatus.title());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> sexDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), Sex.class)
                .order(Sex.code())
                .filter(Sex.title());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> servLenghTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), ServiceLengthType.class)
                .order(ServiceLengthType.title())
                .filter(ServiceLengthType.title());
    }
}
