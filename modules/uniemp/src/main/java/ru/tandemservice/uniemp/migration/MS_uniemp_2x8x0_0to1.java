package ru.tandemservice.uniemp.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * @author Alexey Lopatin
 * @since 18.03.2015
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniemp_2x8x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        Statement stmt = tool.getConnection().createStatement();
        ResultSet src = stmt.executeQuery("select id, assigndate_p, dismissaldate_p from employmenthistoryitembase_t where dismissaldate_p is not null and dismissaldate_p < assigndate_p");
        PreparedStatement update = tool.prepareStatement("update employmenthistoryitembase_t set assigndate_p = ?, dismissaldate_p = ? where id = ?");

        while (src.next())
        {
            Long id = src.getLong(1);
            Date assignDate = src.getDate(2);
            Date dismissalDate = src.getDate(3);

            update.setDate(1, dismissalDate);
            update.setDate(2, assignDate);
            update.setLong(3, id);
            update.executeUpdate();
        }
    }
}