/**
 *$Id:$
 */
package ru.tandemservice.uniemp.base.bo.EmpMedicalStuffReport.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import ru.tandemservice.uni.report.summaryReports.SummaryDataWrapper;
import ru.tandemservice.uniemp.entity.catalog.MedicalEducationLevel;
import ru.tandemservice.uniemp.entity.employee.EmployeeMedicalSpeciality;

import java.util.Date;
import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 30.10.12
 */
public interface IEmpMedicalStuffReportDao extends INeedPersistenceSupport
{
    /**
     * По списку Врачебных специальностей, удовлетворяющих дате отчета и фильтрам, строит список строк отчета.
     * @param reportDate дата на которую строится отчет
     * @param postTypeList фильтр по Типу назначения на должность
     * @param medicalEducationLevelList фильтр по Врачебным специальностям
     * @return список строк отчета
     */
    List<SummaryDataWrapper<EmployeeMedicalSpeciality>> getReportWrapperList(Date reportDate, List<PostType> postTypeList, List<MedicalEducationLevel> medicalEducationLevelList, List<EmployeeType> employeeTypeList, List<EduLevel> eduLevelList);

    /**
     * Список сотрудников, которые не учитываются в отчете, т.к. у них есть Врачебные специальности с одинаковыми Категориями.
     *
     * @param reportDate дата на которую строится отчет
     * @param postTypeList фильтр по Типу назначения на должность
     * @param medicalEducationLevelList фильтр по Врачебным специальностям
     * @return список игнорируемых в отчете кадровых ресурсов
     */
    List<Long> getIgnoreEmployeeList(Date reportDate, List<PostType> postTypeList, List<MedicalEducationLevel> medicalEducationLevelList, List<EmployeeType> employeeTypeList,  List<EduLevel> eduLevelList);
}
