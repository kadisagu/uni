/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.EmploymentHistoryAbsAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.info.ErrorCollector;

/**
 * @author dseleznev
 * Created on: 24.12.2008
 */
public class EmploymentHistoryAbstractController<IDAO extends EmploymentHistoryAbstractIDAO<M>, M extends EmploymentHistoryAbstractModel> extends AbstractBusinessController<IDAO, M>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickApply(IBusinessComponent component)
    {
        ErrorCollector errCollector = component.getUserContext().getErrorCollector();
        getDao().validate(getModel(component), errCollector);

        if (!errCollector.hasErrors())
        {
            getDao().update(getModel(component));
            deactivate(component);
        }
    }
}