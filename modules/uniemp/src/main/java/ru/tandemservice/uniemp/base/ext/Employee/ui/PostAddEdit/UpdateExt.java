/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.ext.Employee.ui.PostAddEdit;

import org.tandemframework.caf.ui.addon.IUIAddon;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostAddEdit.EmployeePostAddEditUI;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostAddEdit.IOnUpdateEmployeePostExt;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;
import ru.tandemservice.uniemp.entity.employee.StaffListItem;
import ru.tandemservice.uniemp.util.StaffListPaymentsUtil;

import java.util.*;

/**
 * Create by ashaburov
 * Date 28.04.12
 */
public class UpdateExt implements IOnUpdateEmployeePostExt
{
    @SuppressWarnings("unchecked")
    @Override
    public void onUpdateExt(IUIAddon addon)
    {
        EmployeePostAddEditExtUI presenerExt = (EmployeePostAddEditExtUI) addon;

        EmployeePostAddEditUI baseModel = presenerExt.getPresenter();
        EmployeePostAddEditLegacyModel model = presenerExt.getModel();

        /*Сохраняем ссылки на сотрудника в штатной расстановке(назначаем на ставки ШР)*/

        //обновляем StaffRateList(переносим значения из valueMap'ов колонок сечлиста в объекты списка _staffRateList),
        //что бы работать сразу с объектами списка
        presenerExt.prepareStaffRateList();

        //достаем элементы штатной расстановки, которые выбраны в мультиселектах
        final IValueMapHolder<List<IdentifiableWrapper>> empHRHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("employeeHR");
        final Map<Long, List<IdentifiableWrapper>> empHRMap = (null == empHRHolder ? Collections.<Long, List<IdentifiableWrapper>>emptyMap() : empHRHolder.getValueMap());
        //Мапа, id энтини сечлиста на выбранные в мультиселекте объекты штатной расстановки
        final Map<Long, List<StaffListAllocationItem>> allocItemsMap = new HashMap<>();

        //достаем из враперов объекты Штатной расстановки
        for (IEntity entity : model.getStaffRateItemList())
        {
            List<StaffListAllocationItem> allocationItemList = new ArrayList<>();
            List<IdentifiableWrapper> wrapperList = empHRMap.get(entity.getId()) != null ? empHRMap.get(entity.getId()) : new ArrayList<>();
            for (IdentifiableWrapper wrapper : wrapperList)
            {
                if (wrapper.getId() != 0L)
                {
                    StaffListAllocationItem item = IUniBaseDao.instance.get().get(StaffListAllocationItem.class, wrapper.getId());
                    allocationItemList.add(item);
                }
            }

            allocItemsMap.put(entity.getId(), allocationItemList);
        }

        //если редактируем должность, то перед тем как проставить новые ссылки на этого сотрудника(у объектов штатной расстановки)
        //удаляем старые ставки этого сотрудника из активного ШР
        if (!baseModel.isAddForm() && (baseModel.getEmployeePost().isFreelance() || model.isThereAnyActiveStaffList()))
        {
            List<StaffListAllocationItem> choseAllocItem = new ArrayList<>();
            for (EmployeePostStaffRateItem staffRateItem : model.getStaffRateItemList())
                for (StaffListAllocationItem allocItem : allocItemsMap.get(staffRateItem.getId()))
                    choseAllocItem.add(allocItem);

            for (StaffListAllocationItem allocationItem : UniempDaoFacade.getUniempDAO().getEmployeePostAllocationItemList(baseModel.getEmployeePost()))
            {
                if (!choseAllocItem.contains(allocationItem))
                {
                    allocationItem.setEmployeePost(null);
                    DataAccessServices.dao().delete(allocationItem);
                }
            }
        }

        //если была доступна колонка Кадровой расстановки
        if (model.isThereAnyActiveStaffList())
        {
            for (EmployeePostStaffRateItem staffRateItem : model.getStaffRateItemList())
            {
                //достаем должность ШР
                StaffListItem staffListItem = UniempDaoFacade.getStaffListDAO().getStaffListItem(baseModel.getEmployeePost().getOrgUnit(), baseModel.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL(), staffRateItem.getFinancingSource(), staffRateItem.getFinancingSourceItem());

                //создаем новую ставку ШР
                StaffListAllocationItem allocationItem = new StaffListAllocationItem();
                //удаляем выбранные ставки с неакт. сотрудниками, если они выбранны
                for (StaffListAllocationItem allocItem : allocItemsMap.get(staffRateItem.getId()))
                {
                    //если выбранная ставка это не ставка редактируемого сотрудника, то удаляем ее
                    if (!model.getSelfAllocItemList().contains(allocItem))
                        DataAccessServices.dao().delete(allocItem);
                    else
                        //есди это ставка редактируемого сотрудника, то обновляем ее
                        allocationItem = allocItem;
                }
                //заполняем поля ставки
                allocationItem.setStaffRate(staffRateItem.getStaffRate());
                allocationItem.setFinancingSource(staffRateItem.getFinancingSource());
                allocationItem.setFinancingSourceItem(staffRateItem.getFinancingSourceItem());
                allocationItem.setStaffListItem(staffListItem);
                allocationItem.setEmployeePost(baseModel.getEmployeePost());
                allocationItem.setRaisingCoefficient(baseModel.getEmployeePost().getRaisingCoefficient());
                if (allocationItem.getRaisingCoefficient() != null)
                    allocationItem.setMonthBaseSalaryFund(allocationItem.getRaisingCoefficient().getRecommendedSalary() * allocationItem.getStaffRate());
                else if (allocationItem.getStaffListItem() != null)
                    allocationItem.setMonthBaseSalaryFund(allocationItem.getStaffListItem().getSalary() * allocationItem.getStaffRate());
                DataAccessServices.dao().saveOrUpdate(allocationItem);

                StaffListPaymentsUtil.recalculateAllPaymentsValue(allocationItem, presenerExt.getSession());
            }
        }

        /*Сохраняем ставки сотрудника(релейшены)*/
//        //если редактируем должность, то перед тем как создать новые релейшены(ставки) удаляем старые
//        if (baseModel.isEditForm())
//        {
////            List<EmployeePostStaffRateItem> staffRateItems = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(baseModel.getEmployeePost());
//            for (EmployeePostStaffRateItem item : model.getOldStaffRateItemList())
//                delete(item);
//
////            getSession().flush();
//        }

        //создаем указанные ставки(релейшены)
        for (EmployeePostStaffRateItem item : model.getStaffRateItemList())
        {
            if (item.getEmployeePost() != null && item.getFinancingSource() != null)
            {
                EmployeePostStaffRateItem newItem = new EmployeePostStaffRateItem();
                newItem.update(item);
                newItem.setEmployeePost(((EmployeePostAddEditUI) presenerExt.getPresenter()).getEmployeePost());

                DataAccessServices.dao().save(newItem);
            }
        }
    }
}
