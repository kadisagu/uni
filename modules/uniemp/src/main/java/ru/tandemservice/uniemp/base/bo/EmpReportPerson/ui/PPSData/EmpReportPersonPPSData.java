package ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.PPSData;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;

/**
 * @author Vasily Zhukov
 * @since 07.02.2011
 */
@Configuration
public class EmpReportPersonPPSData extends BusinessComponentManager
{

    @Bean
    public IDefaultComboDataSourceHandler medicalEducationLevelDSHandler()
    {
        return EmployeePPSDataBlock.createMedicalEducationLevelDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler medicalQualificationCategoryDSHandler()
    {
        return EmployeePPSDataBlock.createMedicalQualificationCategoryDS(getName());
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(CommonManager.instance().yesNoDSConfig())
                .addDataSource(selectDS(EmployeePPSDataBlock.MEDICAL_EDUCATION_LEVEL_DS, medicalEducationLevelDSHandler()))
                .addDataSource(selectDS(EmployeePPSDataBlock.MEDICAL_QUALIFICATION_CATEGORY_DS, medicalQualificationCategoryDSHandler()))
                .create();
    }
}
