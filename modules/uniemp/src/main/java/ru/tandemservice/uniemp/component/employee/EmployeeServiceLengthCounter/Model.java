/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.component.employee.EmployeeServiceLengthCounter;

import org.tandemframework.core.component.Input;
import ru.tandemservice.uniemp.base.ext.Employee.logic.empServiceLengthWrappers.EmployeeServiceLengthWrapper;
import ru.tandemservice.uniemp.entity.catalog.ServiceLengthType;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author euroelessar
 * @since 07.04.2010
 */
@Input(keys = { "employeeId" }, bindings = { "employeeId" } )
public class Model
{
    private Long _employeeId;
    private Map<ServiceLengthType, List<EmployeeServiceLengthWrapper>> _serviceLengthMap;
    private ServiceLengthType _serviceLengthType;     
    private List<ServiceLengthType> _serviceLengthTypeList;
    private Date _date = new Date();
    private String _text;

    public Long getEmployeeId()
    {
        return _employeeId;
    }

    public void setEmployeeId(Long employeeId)
    {
        _employeeId = employeeId;
    }

    public Map<ServiceLengthType, List<EmployeeServiceLengthWrapper>> getServiceLengthMap()
    {
        return _serviceLengthMap;
    }

    public void setServiceLengthMap(Map<ServiceLengthType, List<EmployeeServiceLengthWrapper>> serviceLengthMap)
    {
        _serviceLengthMap = serviceLengthMap;
    }

    public ServiceLengthType getServiceLengthType()
    {
        return _serviceLengthType;
    }

    public void setServiceLengthType(ServiceLengthType serviceLengthType)
    {
        _serviceLengthType = serviceLengthType;
    }

    public List<ServiceLengthType> getServiceLengthTypeList()
    {
        return _serviceLengthTypeList;
    }

    public void setServiceLengthTypeList(List<ServiceLengthType> serviceLengthTypeList)
    {
        _serviceLengthTypeList = serviceLengthTypeList;
    }

    public Date getDate()
    {
        return _date;
    }

    public void setDate(Date date)
    {
        _date = date;
    }

    public String getText()
    {
        return _text;
    }

    public void setText(String text)
    {
        _text = text;
    }
}
