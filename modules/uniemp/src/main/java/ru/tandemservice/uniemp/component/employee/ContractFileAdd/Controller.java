/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.ContractFileAdd;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

/**
 * @author dseleznev
 * Created on: 12.11.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }
    
    public void onClickApply(IBusinessComponent context)
    {
        getDao().update(getModel(context));
        deactivate(context);
    }
}