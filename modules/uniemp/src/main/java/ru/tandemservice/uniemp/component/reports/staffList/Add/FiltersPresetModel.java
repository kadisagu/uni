/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.staffList.Add;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.settings.IDataSettingsManager;

import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.report.FiltersPreset;

/**
 * @author dseleznev
 * Created on: 08.08.2009
 */
public class FiltersPresetModel
{
    // ID элемента "Сохранить как..." в списке шаблонов фильтров
    public static final Long NEW_PRESET_ITEM_ID = 0L;
    
    private String _settingsKey; // Ключ хранимых настроек для текущего шаблона фильтров
    private IDataSettings _settings; // Хранимые настройки для текущего шаблона фильтров
    private IFiltersPresetManager _presetManager;

    private String _newFiltersPresetName; // Название нового фильтра (используется при сохранении)
    private FiltersPreset _filtersPreset; // Текущий выбранный шаблон фильтров
    private List<FiltersPreset> _filterPresetsList; // Список доступных шаблонов фильтров
    
    private String[] _filtersToRefreshArray; // Список полей, подлежащих обновлению при смене шаблона фильтров

    
    public FiltersPresetModel(String settingsKey, IFiltersPresetManager presetManager, String[] filtersToRefreshArray, Session session)
    {
        _settingsKey = settingsKey;
        _presetManager = presetManager;
        _filtersToRefreshArray = filtersToRefreshArray;
        
        initPresetsList(null);
        
        setSettings(DataSettingsFacade.getSettings("general", getDummySettingsKeyName()));
    }
    
    /**
     * Инициализирует список шаблонов фильтров и выбирает один из них, 
     * если это указано входным параметром. 
     * Вызывается при создании модели и при смене шаблона фильтров.
     */
    public void initPresetsList(FiltersPreset filtersPreset)
    {
        List<FiltersPreset> presetsList = new ArrayList<>();
        presetsList.add(new FiltersPreset(0L, "-- Сохранить как... --", getSettingsKey()));
        presetsList.addAll(UniDaoFacade.getEmployeeDao().getFilterPresetsList(getSettingsKey()));
        
        setFilterPresetsList(presetsList);
        setFiltersPreset(filtersPreset);
    }
    
    /**
     * Обрабатывает событие изменения текущего шаблона фильтров
     */
    public void onChangePreset()
    {
        if(null != getFiltersPreset() && !NEW_PRESET_ITEM_ID.equals(getFiltersPreset().getId()))
        {
            setSettings(DataSettingsFacade.getSettings("general", getFullSettingsKeyName()));
            getPresetManager().initFilters(getSettings());
        }
    }
    
    /**
     * Обрабатывает событие сохранения шаблона фильтров
     */
    public void updatePreset()
    {
        if(null == getFiltersPreset()) 
            throw new ApplicationException("Для сохранения шаблона необходимо указать выбрать \"-- Сохранить как... --\" из списка шаблонов.");
        
        if(NEW_PRESET_ITEM_ID.equals(getFiltersPreset().getId()) && null == getNewFiltersPresetName())
            throw new ApplicationException("Необходимо указать имя сохраняемому шаблону.");

        if(NEW_PRESET_ITEM_ID.equals(getFiltersPreset().getId()))
            setFiltersPreset(new FiltersPreset(getNewFiltersPresetName(), getSettingsKey(), UniDaoFacade.getEmployeeDao().getFreeFiltersPresetNumber(getSettingsKey())));
        
        setSettings(DataSettingsFacade.getSettings("general", getSettingsKey() + "." + getFiltersPreset().getSettingsNumber()));
        getPresetManager().setFilters(getSettings());
        DataSettingsFacade.saveSettings(getSettings());
        
        UniDaoFacade.getEmployeeDao().updateFiltersPreset(getFiltersPreset());
        initPresetsList(getFiltersPreset());
        setNewFiltersPresetName(null);
    }
    
    /**
     * Обрабатывает событие удаления шаблона фильтров
     */
    public void deletePreset()
    {
        UniDaoFacade.getEmployeeDao().deleteFiltersPreset(getFiltersPreset());
        initPresetsList(null);
    }
    
    public String getSettingsKey()
    {
        return _settingsKey;
    }

    public void setSettingsKey(String settingsKey)
    {
        this._settingsKey = settingsKey;
    }

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        this._settings = settings;
    }

    public IFiltersPresetManager getPresetManager()
    {
        return _presetManager;
    }

    public void setPresetManager(IFiltersPresetManager presetManager)
    {
        this._presetManager = presetManager;
    }

    public String getNewFiltersPresetName()
    {
        return _newFiltersPresetName;
    }

    public void setNewFiltersPresetName(String newFiltersPresetName)
    {
        this._newFiltersPresetName = newFiltersPresetName;
    }

    public FiltersPreset getFiltersPreset()
    {
        return _filtersPreset;
    }

    public void setFiltersPreset(FiltersPreset filtersPreset)
    {
        this._filtersPreset = filtersPreset;
    }

    public List<FiltersPreset> getFilterPresetsList()
    {
        return _filterPresetsList;
    }

    public void setFilterPresetsList(List<FiltersPreset> filterPresetsList)
    {
        this._filterPresetsList = filterPresetsList;
    }
    
    public String[] getFiltersToRefreshArray()
    {
        return _filtersToRefreshArray;
    }

    public void setFiltersToRefreshArray(String[] filtersToRefreshArray)
    {
        this._filtersToRefreshArray = filtersToRefreshArray;
    }
    
    public String getFiltersToRefresh()
    {
        StringBuilder builder = new StringBuilder();
        for(String item : getFiltersToRefreshArray()) 
            builder.append(builder.length() > 0 ? "," : "").append(item);
        return builder.toString();
    }
    
    public boolean isNewPresetSelected()
    {
        return null != getFiltersPreset() && NEW_PRESET_ITEM_ID.equals(getFiltersPreset().getId());
    }
    
    public boolean isPresetDeleteDisabled()
    {
        return null == getFiltersPreset() || NEW_PRESET_ITEM_ID.equals(getFiltersPreset().getId());
    }
    
    public String getFullSettingsKeyName()
    {
        return getSettingsKey() + "." + getFiltersPreset().getSettingsNumber();
    }
    
    public String getDummySettingsKeyName()
    {
        return getSettingsKey() + ".DUMMY";
    }
}