/**
 *$Id$
 */
package ru.tandemservice.uniemp.component.employee.employmentContracts.EmploymentContractsTerminationNoticePrintForm;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import ru.tandemservice.uniemp.IUniempComponents;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;

import java.util.*;

/**
 * Create by ashaburov
 * Date 29.07.11
 */
public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getContractListDataSourse() != null)
            return;

        DynamicListDataSource<EmployeeLabourContract> dataSource = new DynamicListDataSource<>(component, this);
        dataSource.addColumn(new SimpleColumn("ФИО", EmployeeLabourContract.employeePost().person().fio().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Подразделение", EmployeeLabourContract.employeePost().orgUnit().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Должность", EmployeeLabourContract.employeePost().postRelation().postBoundedWithQGandQL().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn("number", "Номер уведомления").setOrderable(false));
        dataSource.addColumn(new ActionColumn("Заполнить номера для всех", "add_item", "onClickFillNumber"));
        dataSource.addColumn(new BlockColumn("date", "Дата формирования").setOrderable(false));
        dataSource.addColumn(new ActionColumn("Копировать дату для всех", "clone", "onClickFillDate"));

        model.setContractListDataSourse(dataSource);
    }

    @Override
    public void updateListDataSource(IBusinessComponent component)
    {
        getDao().prepareListDataSource(getModel(component));
    }

    @SuppressWarnings("unchecked")
    public void onClickFillNumber(IBusinessComponent component)
    {
        Model model = getModel(component);
        final IValueMapHolder<Object> numberHolder = (IValueMapHolder) model.getContractListDataSourse().getColumn("number");
        final Map<Long, Object> numberHolderMap = (null == numberHolder) ? Collections.<Long, Object>emptyMap() : numberHolder.getValueMap();

        Long entityId = component.getListenerParameter();
        String number = (String) numberHolderMap.get(entityId);

        if (number == null)
            return;

        //определяем только из цифр состоит номер уведомления или нет
        try
        {
            Long numberL = Long.parseLong(number);

            //если номер уведомления это просто цифры,
            //то заполняем последующие номера инкрементом
            boolean fillnext = false;
            for (IEntity entity : model.getEmployeeLabourContractList())
            {
                if (entity.getId().equals(entityId))
                    fillnext = true;

                if (fillnext)
                    numberHolderMap.put(entity.getId(), numberL++);
            }
        }
        catch (NumberFormatException e)
        {
            //если номер уведомления состоит из цифр и букв
            List<Object> charList = new ArrayList<>();
            charList.add(String.valueOf(number.charAt(0)));
            Boolean isNumeric = StringUtils.isNumeric(String.valueOf(number.charAt(0)));
            Integer parts = 0;
            //создаем список содержащий последовательно цифровые и буквенные части номера
            for (int i = 1; i < number.length(); i++)
            {
                char ch = number.charAt(i);

                if (!isNumeric.equals(StringUtils.isNumeric(String.valueOf(ch))))
                {
                    charList.add("");
                    parts++;
                }

                charList.set(parts, (String) charList.get(parts) + ch);

                isNumeric = StringUtils.isNumeric(String.valueOf(ch));
            }

            //определяем последнюю цифровую часть номера
            Integer digitPart = null;
            for (Object object : charList)
                if (StringUtils.isNumeric((String) object))
                    digitPart = charList.indexOf(object);

            if (digitPart == null)
            {
                //если цифровой части в номере нет, то копируем его
                boolean fillnext = false;
                for (IEntity entity : model.getEmployeeLabourContractList())
                {
                    if (entity.getId().equals(entityId))
                        fillnext = true;

                    if (fillnext)
                        numberHolderMap.put(entity.getId(), number);
                }
            }
            else
            {
                //если цифровая часть в номере есть, то заполняем номера инкрементируя последнюю цифровую часть номера
                long incPart = Long.parseLong((String) charList.get(digitPart));
                boolean fillnext = false;
                for (IEntity entity : model.getEmployeeLabourContractList())
                {
                    if (entity.getId().equals(entityId))
                        fillnext = true;

                    if (fillnext)
                    {
                        StringBuilder resultStr = new StringBuilder();
                        for (int i = 0; i < charList.size(); i++)
                            if (i == digitPart)
                                resultStr.append(incPart++);
                            else
                                resultStr.append((String) charList.get(i));
                        numberHolderMap.put(entity.getId(), resultStr.toString());
                    }
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    public void onClickFillDate(IBusinessComponent component)
    {
        Model model = getModel(component);
        final IValueMapHolder<Object> dateHolder = (IValueMapHolder) model.getContractListDataSourse().getColumn("date");
        final Map<Long, Object> dateHolderMap = (null == dateHolder ? Collections.<Long, Object>emptyMap() : dateHolder.getValueMap());

        Date date = (Date) dateHolderMap.get(component.<Long>getListenerParameter());

        Map<Long, Date> dateMap = new HashMap<>();
        for (IEntity entity : model.getEmployeeLabourContractList())
            dateMap.put(entity.getId(), date);

        ((BlockColumn) model.getContractListDataSourse().getColumn("date")).setValueMap(dateMap);
    }

    @SuppressWarnings("unchecked")
    public void onClickPrint(IBusinessComponent component)
    {
        Model model = getModel(component);
        ErrorCollector errorCollector = component.getUserContext().getErrorCollector();

        getDao().validate(model, errorCollector);

        if (model.getMassPrint())
        {
            final IValueMapHolder<String> numberHolder = (IValueMapHolder) model.getContractListDataSourse().getColumn("number");
            final Map<Long, String> numberHolderMap = (null == numberHolder ? Collections.<Long, String>emptyMap() : numberHolder.getValueMap());

            final IValueMapHolder<Date> dateHolder = (IValueMapHolder) model.getContractListDataSourse().getColumn("date");
            final Map<Long, Date> dateHolderMap = (null == dateHolder ? Collections.<Long, Date>emptyMap() : dateHolder.getValueMap());

            activateInRoot(component, new ComponentActivator(IUniempComponents.EMPLOYMENT_CONTRACTS_TERMINATION_NOTICE_PRINT, new ParametersMap()
                .add("massPrint", model.getMassPrint())
                .add("employeeLabourContractList", model.getEmployeeLabourContractList())
                .add("numberMap", numberHolderMap)
                .add("dateMap", dateHolderMap)));
            deactivate(component);
        }
        else
        {
            List<EmployeeLabourContract> contractList = new ArrayList<>();
            contractList.add(model.getEmployeeLabourContract());
            HashMap<Long, String> numberMap = new HashMap<>();
            numberMap.put(model.getEmployeeLabourContract().getId(), model.getNumberSingleNotice());
            HashMap<Long, Date> dateMap = new HashMap<>();
            dateMap.put(model.getEmployeeLabourContract().getId(), model.getDateSingleNotice());
            activateInRoot(component, new ComponentActivator(IUniempComponents.EMPLOYMENT_CONTRACTS_TERMINATION_NOTICE_PRINT, new ParametersMap()
                .add("massPrint", model.getMassPrint())
                .add("employeeLabourContractList", contractList)
                .add("numberMap", numberMap)
                .add("dateMap", dateMap)));
            deactivate(component);
        }


    }
}
