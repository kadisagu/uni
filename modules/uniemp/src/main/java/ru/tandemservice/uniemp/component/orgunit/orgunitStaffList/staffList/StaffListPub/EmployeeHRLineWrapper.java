/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffList.StaffListPub;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.employee.CombinationPost;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;
import ru.tandemservice.uniemp.entity.employee.StaffListItem;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author dseleznev
 * Created on: 20.04.2010
 */
public class EmployeeHRLineWrapper extends DataWrapper
{
    private static final long serialVersionUID = 5436036570814627804L;

    // Property names
    public static final String P_ID = "id";
    public static final String P_POST_TITLE = "postTitle";
    public static final String P_POST_TYPE = "postType";
    public static final String P_PROF_QUALIFICATION_GROUP_SHORT_TITLE = "profQualificationGroupShortTitle";
    public static final String P_QUALIFICATION_LEVEL_SHORT_TITLE = "qualificationLevelShortTitle";
    public static final String P_BASE_SALARY = "baseSalary";
    public static final String P_ETKS_LEVEL_TITLE = "etksLevelShortTitle";
    public static final String P_STAFF_RATE_STR = "staffRateStr";
    public static final String P_FINANCING_SOURCE = "financingSourceTitle";
    public static final String P_FINANCING_SOURCE_ITEM = "financingSourceItemTitle";
    public static final String P_MONTH_BASE_SALARY_FUND = "monthBaseSalaryFund";
    public static final String P_TARGET_SALARY = "targetSalary";
    public static final String P_EMPLOYEE_POST = "employeePost";
    public static final String P_EMPLOYEE_FIO = "employeeFIO";

    public static final String P_INVALID = "invalid";
    public static final String P_ERROR_MESSAGE = "errMessage";

    // Properties
    private StaffListItem _staffListItem;
    private StaffListAllocationItem _staffListAllocationItem;

    private double _staffRate;
    private FinancingSource _financingSource;
    private FinancingSourceItem _financingSourceItem;
    private double _monthBaseSalaryFund;
    private double _targetSalary;
    private EmployeePost _employeePost;
    private boolean _allocItem;
    private boolean _reserve;
    private CombinationPost _combinationPost;
    private boolean _combination;
    private Map<String, Double> _codeValueMap = new HashMap<>();
    private boolean _finSrc;
    private boolean _invalid;
    private String _errMessage;

    public EmployeeHRLineWrapper(Long id, StaffListItem staffListItem, StaffListAllocationItem item, double staffRate, EmployeePost employeePost, FinancingSource financingSource, FinancingSourceItem financingSourceItem ,boolean allocItem, boolean reserve, CombinationPost combinationPost, boolean combination)
    {
        super(id, staffListItem.getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL().getTitle());

        _staffListItem = staffListItem;
        _staffListAllocationItem = item;
        _staffRate = staffRate;
        _financingSource = financingSource;
        _financingSourceItem = financingSourceItem;
        _employeePost = employeePost;
        _allocItem = allocItem;
        _reserve = reserve;
        _combinationPost = combinationPost;
        _combination = combination;
    }


    //Methods

    @Override
    public Object getProperty(Object propertyPath)
    {
        if (_codeValueMap.containsKey(propertyPath))
            return _codeValueMap.get(propertyPath);

        return super.getProperty(propertyPath);
    }

    /**
     * Связывает указаный код выплаты с её суммой.<p/>
     * Если для кода уже сохранено значение, то оно суммируются с указанным.
     *
     * @param paymentCode код выплаты
     * @param value       сумма выплаты
     */
    public void putPaymentValue(String paymentCode, Double value)
    {
        if (paymentCode == null)
            throw new NullPointerException("'paymentCode' must not be null");

        _codeValueMap.put(paymentCode, _codeValueMap.get(paymentCode) != null ? _codeValueMap.get(paymentCode) + value : value);
    }

    /**
     * @param financingSource     источник финансирования
     * @param financingSourceItem источник финансирования (детально)
     * @return <tt>true</tt>, если указанные истчники финансирования совпадают с источниками финансирования врапера, иначе <tt>false</tt>
     */
    public boolean equalsFinSrs(FinancingSource financingSource, FinancingSourceItem financingSourceItem)
    {
        CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem> key = new CoreCollectionUtils.Pair<>(getFinancingSource(), getFinancingSourceItem());
        CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem> egKey = new CoreCollectionUtils.Pair<>(financingSource, financingSourceItem);

        return key.equals(egKey);
    }


    // Calculatable properties


    public String getFinancingSourceTitle()
    {
        return getFinancingSource().getTitle();
    }

    public String getFinancingSourceItemTitle()
    {
        return getFinancingSourceItem() != null ? getFinancingSourceItem().getTitle() : "";
    }

    public boolean isAllocItem()
    {
        return _allocItem;
    }

    public void setAllocItem(boolean allocItem)
    {
        this._allocItem = allocItem;
    }
    
    public PostBoundedWithQGandQL getPost()
    {
        if (_staffListAllocationItem != null)
            return _staffListAllocationItem.getPost();

        return getStaffListItem().getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL();
    }

    public String getPostTitle()
    {
        return getPost().getTitle() + (null != getEmployeePost() || isReserve() ? "" : " (вакансия)");
    }
 
    public String getPostType()
    {
        return getPost().getPost().getEmployeeType().getShortTitle();
    }
    
    public String getProfQualificationGroupShortTitle()
    {
        return getPost().getProfQualificationGroup().getShortTitle();
    }
    
    public String getQualificationLevelShortTitle()
    {
        return getPost().getQualificationLevel().getShortTitle();
    }
    
    public Double getBaseSalary()
    {
        if (_staffListAllocationItem != null)
            return _staffListAllocationItem.getBaseSalary();

        return _staffListItem.getSalary();
    }
    
    public String getEtksLevelShortTitle()
    {
        return null != getPost().getEtksLevels() ? getPost().getEtksLevels().getTitle() : null;
    }
    
    public String getEmployeeFIO()
    {
        return isReserve() ? "Резерв" : (null != getEmployeePost() ? getEmployeePost().getEmployee().getPerson().getFio() + " - " + (isCombination() ? (getCombinationPost() != null ? getCombinationPost().getCombinationPostType().getShortTitle() + " " + (getCombinationPost().getEndDate() == null ? (!getCombinationPost().getBeginDate().after(new java.util.Date()) ? getEmployeePost().getPostStatus().getShortTitle() : "н" ) : ((getCombinationPost().getEndDate().getTime() >= CommonBaseDateUtil.trimTime(new Date()).getTime()) ? (!getCombinationPost().getBeginDate().after(new java.util.Date()) ? getEmployeePost().getPostStatus().getShortTitle() : "н" ) : "н")) : "н") : getEmployeePost().getPostType().getShortTitle() + " " + getEmployeePost().getPostStatus().getShortTitle()) : null);
    }

    public CombinationPost getCombinationPost()
    {
        return _combinationPost;
    }

    /**
     * @param combinationPost Должность по совмещению.
     */
    public void setCombinationPost(CombinationPost combinationPost)
    {

        _combinationPost = combinationPost;
    }

    /**
     * @return Является должностью по совмещению. Свойство не может быть null.
     */
    @NotNull
    public boolean isCombination()
    {
        return _combination;
    }

    /**
     * @param combination Является должностью по совмещению. Свойство не может быть null.
     */
    public void setCombination(boolean combination)
    {

        _combination = combination;
    }


    // Getters and setters

    public boolean isInvalid()
    {
        return _invalid;
    }

    public void setInvalid(boolean invalid)
    {
        _invalid = invalid;
    }

    public String getErrMessage()
    {
        return _errMessage;
    }

    public void setErrMessage(String errMessage)
    {
        _errMessage = errMessage;
    }

    public Map<String, Double> getCodeValueMap()
    {
        return _codeValueMap;
    }

    public void setCodeValueMap(Map<String, Double> codeValueMap)
    {
        _codeValueMap = codeValueMap;
    }

    public boolean isFinSrc()
    {
        return _finSrc;
    }

    public void setFinSrc(boolean finSrc)
    {
        _finSrc = finSrc;
    }

    public boolean isReserve()
    {
        return _reserve;
    }

    public void setReserve(boolean reserve)
    {
        _reserve = reserve;
    }

    public Double getStaffRate()
    {
        return _staffRate;
    }

    public String getStaffRateStr()
    {
        if (_finSrc)
            return "";

        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(_staffRate);
    }

    public void setStaffRate(double staffRate)
    {
        _staffRate = staffRate;
    }

    public FinancingSource getFinancingSource()
    {
        return _financingSource;
    }

    public void setFinancingSource(FinancingSource financingSource)
    {
        _financingSource = financingSource;
    }

    public FinancingSourceItem getFinancingSourceItem()
    {
        return _financingSourceItem;
    }

    public void setFinancingSourceItem(FinancingSourceItem financingSourceItem)
    {
        _financingSourceItem = financingSourceItem;
    }

    public StaffListItem getStaffListItem()
    {
        return _staffListItem;
    }

    public void setPost(StaffListItem staffListItem)
    {
        this._staffListItem = staffListItem;
    }

    public Double getMonthBaseSalaryFund()
    {
        if (_finSrc)
            return null;

        return _monthBaseSalaryFund;
    }

    public void setMonthBaseSalaryFund(double monthBaseSalaryFund)
    {
        this._monthBaseSalaryFund = monthBaseSalaryFund;
    }

    public double getTargetSalary()
    {
        if (_finSrc)
        {
            double summ = 0d;
            for (Map.Entry<String, Double> entry : _codeValueMap.entrySet())
                summ += entry.getValue();

            return summ;
        }
        else
        {
            double summ = _monthBaseSalaryFund;
            for (Map.Entry<String, Double> entry : _codeValueMap.entrySet())
                summ += entry.getValue();

            return summ;
        }
    }

    public void setTargetSalary(double targetSalary)
    {
        this._targetSalary = targetSalary;
    }

    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    public void setEmployeePost(EmployeePost employeePost)
    {
        this._employeePost = employeePost;
    }

    public StaffListAllocationItem getStaffListAllocationItem()
    {
        return _staffListAllocationItem;
    }

    public void setStaffListAllocationItem(StaffListAllocationItem staffListAllocationItem)
    {
        _staffListAllocationItem = staffListAllocationItem;
    }
}