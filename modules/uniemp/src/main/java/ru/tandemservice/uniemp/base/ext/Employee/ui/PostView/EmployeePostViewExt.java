/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniemp.base.ext.Employee.ui.PostView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.block.BlockListExtension;
import org.tandemframework.caf.ui.config.button.ButtonListExtension;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostView.EmployeePostView;
import ru.tandemservice.uniemp.base.bo.EmpEmployee.ui.LabourContract.EmpEmployeeLabourContract;

/**
 * @author Vasily Zhukov
 * @since 30.03.2012
 */
@Configuration
public class EmployeePostViewExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "uniemp" + EmployeePostViewExtUI.class.getSimpleName();

    public static final String EMPLOYEE_POST_STAFF_RATE_BLOCK = "employeeStaffRateBlock";

    // buttons
    public static final String EMPLOYEE_POST_PRINT_PERSON_CARD_BUTTON = "employeePostPrintPersonCard";
    
    @Autowired
    private EmployeePostView _employeePostView;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_employeePostView.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, EmployeePostViewExtUI.class))
                .create();
    }

    @Bean
    public TabPanelExtension tabPanelExtension()
    {
        /*
        <html-tab name="employeePostDataTab" label="Общая информация" html-template-name="ru.tandemservice.uni.component.employee.EmployeePostPub.EmployeePostTab_Data" permission-key="viewTabEmployeePostCommon_employeePost" replace="true"/>
        <html-tab name="PPS" label="ППС" html-template-name="ru.tandemservice.uniemp.component.employee.EmployeePostAdditionalInfoPub.EmployeePostPPSTab" permission-key="viewTabPPS_employeePost" visible="fast:modelMap[employeePostAdditionalInfoPub].employeeCardTypePPS" after="postsList"/>
        <html-tab name="SickLists" label="Учет листов нетрудоспособности" html-template-name="ru.tandemservice.uniemp.component.employee.EmployeePostAdditionalInfoPub.EmployeePostSickListTab" permission-key="viewTabSickList_employeePost" after="PPS"/>
        <html-tab name="Contract" label="Трудовой договор" html-template-name="ru.tandemservice.uniemp.component.employee.EmployeePostAdditionalInfoPub.EmployeePostLabourContractTab" permission-key="viewTabLabourContract_employeePost" after="SickLists"/>
        <html-tab name="Holidays" label="Отпуска" html-template-name="ru.tandemservice.uniemp.component.employee.EmployeePostAdditionalInfoPub.EmployeeHolidaysTab" permission-key="viewTabEmployeeHolidays_employeePost" after="Contract"/>
        <html-tab name="Payments" label="Выплаты" html-template-name="ru.tandemservice.uniemp.component.employee.EmployeePostAdditionalInfoPub.EmployeePaymentsTab" permission-key="viewTabEmployeePayments_employeePost" after="Holidays"/>
        <html-tab name="Acting" label="Исполнение обязанностей" html-template-name="ru.tandemservice.uniemp.component.employee.EmployeePostAdditionalInfoPub.EmployeeActingTab" permission-key="viewTabEmployeeActingItem_employeePost" after="Payments"/>
        <html-tab name="Combination" label="Совмещение" html-template-name="ru.tandemservice.uniemp.component.employee.EmployeePostAdditionalInfoPub.EmployeeCombinationPostTab" permission-key="viewTabEmployeeCombinationPost_employeePost" after="Acting"/>
        <html-tab name="Certification" label="Аттестация" html-template-name="ru.tandemservice.uniemp.component.employee.EmployeePostAdditionalInfoPub.EmployeeCertificationTab" permission-key="viewTabEmployeeCertificationItem_employeePost" after="Combination"/>
        <html-tab name="Training" label="ПК и ПП" html-template-name="ru.tandemservice.uniemp.component.employee.EmployeePostAdditionalInfoPub.EmployeeTrainingTab" permission-key="viewTabEmployeeTraining_employeePost" after="Certification"/>
        */

        return tabPanelExtensionBuilder(_employeePostView.employeePostTabPanelExtPoint())
                .addAllAfter(EmployeePostView.EMPLOYEE_POST_DATA_TAB)
                .addTab(htmlTab("PPS", "EmployeePostPPSTab").permissionKey("viewTabPPS_employeePost"))
                .addTab(htmlTab("SickLists", "EmployeePostSickListTab").permissionKey("viewTabSickList_employeePost"))
                .addTab(componentTab("Contract", EmpEmployeeLabourContract.class).parameters("addon:" + ADDON_NAME + ".contractParamenersMap").permissionKey("viewTabLabourContract_employeePost"))
                .addTab(htmlTab("Holidays", "EmployeeHolidaysTab").permissionKey("viewTabEmployeeHolidays_employeePost"))
                .addTab(htmlTab("Payments", "EmployeePaymentsTab").permissionKey("viewTabEmployeePayments_employeePost"))
                .addTab(htmlTab("Acting", "EmployeeActingTab").permissionKey("viewTabEmployeeActingItem_employeePost"))
                .addTab(htmlTab("Combination", "EmployeeCombinationPostTab").permissionKey("viewTabEmployeeCombinationPost_employeePost"))
                .addTab(htmlTab("Certification", "EmployeeCertificationTab").permissionKey("viewTabEmployeeCertificationItem_employeePost"))
                .addTab(htmlTab("Training", "EmployeeTrainingTab").permissionKey("viewTabEmployeeTraining_employeePost"))
                .create();
    }
    
    @Bean
    public BlockListExtension blockListExtension()
    {
        return blockListExtensionBuilder(_employeePostView.employeePostPubBlockListExtPoint())
                .addBlock(htmlBlock(EMPLOYEE_POST_STAFF_RATE_BLOCK, "EmployeePostStaffRateBlock").after(EmployeePostView.EMPLOYEE_POST_ATTRS_BLOCK))
                .create();
    }

    @Bean
    public ButtonListExtension buttonListExtension()
    {
        return buttonListExtensionBuilder(_employeePostView.employeePostDataActionButtonListExtPoint())
                .addButton(submitButton(EMPLOYEE_POST_PRINT_PERSON_CARD_BUTTON, "uniempEmployeePostViewExtUI:onClickPrintPersonCard").permissionKey("ui:secModel.printPersonCard"))
                .create();
    }
}
