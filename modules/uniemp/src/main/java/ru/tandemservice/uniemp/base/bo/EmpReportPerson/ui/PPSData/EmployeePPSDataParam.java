package ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.PPSData;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.employeebase.base.bo.EmployeeReportPerson.ui.Add.EmployeeReportPersonAddUI;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import ru.tandemservice.uniemp.entity.catalog.MedicalEducationLevel;
import ru.tandemservice.uniemp.entity.catalog.MedicalQualificationCategory;
import ru.tandemservice.uniemp.entity.employee.EmployeeCard;
import ru.tandemservice.uniemp.entity.employee.EmployeeMedicalSpeciality;

import java.util.Date;
import java.util.List;

/**
 * @author Vasily Zhukov
 * @since 08.02.2011
 */
public class EmployeePPSDataParam implements IReportDQLModifier
{
    private IReportParam<DataWrapper> _scientificWork = new ReportParam<>();
    private IReportParam<DataWrapper> _invention = new ReportParam<>();
    private IReportParam<List<MedicalEducationLevel>> _medicalEducationLevel = new ReportParam<>();
    private IReportParam<Date> _assignDateFrom = new ReportParam<>();
    private IReportParam<Date> _assignDateTo = new ReportParam<>();
    private IReportParam<Date> _acceptionDateFrom = new ReportParam<>();
    private IReportParam<Date> _acceptionDateTo = new ReportParam<>();
    private IReportParam<List<MedicalQualificationCategory>> _medicalQualificationCategory = new ReportParam<>();
    private IReportParam<Date> _categoryAssignDateFrom = new ReportParam<>();
    private IReportParam<Date> _categoryAssignDateTo = new ReportParam<>();
    private IReportParam<Date> _categoryAcceptionDateFrom = new ReportParam<>();
    private IReportParam<Date> _categoryAcceptionDateTo = new ReportParam<>();
    private IReportParam<DataWrapper> _expertCertificate = new ReportParam<>();

    private String card(ReportDQL dql)
    {
        // соединяем кадрового ресурса
        String employeeAlias = dql.innerJoinEntity(Employee.class, Employee.person());

        // соединяем карточку кр
        String employeeCardAlias = dql.leftJoinEntity(employeeAlias, EmployeeCard.class, EmployeeCard.employee());

        return employeeCardAlias;
    }

    private String med(ReportDQL dql)
    {
        // соединяем кадрового ресурса
        String employeeAlias = dql.innerJoinEntity(Employee.class, Employee.person());

        // соединяем врачебную специальность кр
        String employeeMedAlias = dql.innerJoinEntity(employeeAlias, EmployeeMedicalSpeciality.class, EmployeeMedicalSpeciality.employee());

        return employeeMedAlias;
    }

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {

    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (_scientificWork.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "employeePPSData.scientificWork", _scientificWork.getData().getTitle());

            if (_scientificWork.getData().getId().equals(TwinComboDataSourceHandler.NO_ID))
            {
                dql.builder.where(DQLExpressions.or(
                        DQLExpressions.eq(DQLExpressions.property(EmployeeCard.scientificWork().fromAlias(card(dql))), DQLExpressions.value(TwinComboDataSourceHandler.getSelectedValueNotNull(_scientificWork.getData()))),
                        DQLExpressions.isNull(EmployeeCard.id().fromAlias(card(dql)))
                ));
            } else
            {
                dql.builder.where(DQLExpressions.eq(DQLExpressions.property(EmployeeCard.scientificWork().fromAlias(card(dql))), DQLExpressions.value(TwinComboDataSourceHandler.getSelectedValueNotNull(_scientificWork.getData()))));
            }
        }

        if (_invention.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "employeePPSData.invention", _invention.getData().getTitle());

            if (_invention.getData().getId().equals(TwinComboDataSourceHandler.NO_ID))
            {
                dql.builder.where(DQLExpressions.or(
                        DQLExpressions.eq(DQLExpressions.property(EmployeeCard.invention().fromAlias(card(dql))), DQLExpressions.value(TwinComboDataSourceHandler.getSelectedValueNotNull(_invention.getData()))),
                        DQLExpressions.isNull(EmployeeCard.id().fromAlias(card(dql)))
                ));
            } else
            {
                dql.builder.where(DQLExpressions.eq(DQLExpressions.property(EmployeeCard.invention().fromAlias(card(dql))), DQLExpressions.value(TwinComboDataSourceHandler.getSelectedValueNotNull(_invention.getData()))));
            }
        }

        if (_medicalEducationLevel.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "employeePPSData.medicalEducationLevel", CommonBaseStringUtil.join(_medicalEducationLevel.getData(), MedicalEducationLevel.P_TITLE, ", "));

            dql.builder.where(DQLExpressions.in(DQLExpressions.property(EmployeeMedicalSpeciality.medicalEducationLevel().fromAlias(med(dql))), _medicalEducationLevel.getData()));
        }

        if (_assignDateFrom.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "employeePPSData.assignDateFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(_assignDateFrom.getData()));

            dql.builder.where(DQLExpressions.ge(DQLExpressions.property(EmployeeMedicalSpeciality.assignDate().fromAlias(med(dql))), DQLExpressions.valueDate(CoreDateUtils.getDayFirstTimeMoment(_assignDateFrom.getData()))));
        }

        if (_assignDateTo.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "employeePPSData.assignDateTo", DateFormatter.DEFAULT_DATE_FORMATTER.format(_assignDateTo.getData()));

            dql.builder.where(DQLExpressions.lt(DQLExpressions.property(EmployeeMedicalSpeciality.assignDate().fromAlias(med(dql))), DQLExpressions.valueDate(CoreDateUtils.getNextDayFirstTimeMoment(_assignDateTo.getData(), 1))));
        }

        if (_acceptionDateFrom.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "employeePPSData.acceptionDateFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(_acceptionDateFrom.getData()));

            dql.builder.where(DQLExpressions.ge(DQLExpressions.property(EmployeeMedicalSpeciality.acceptionDate().fromAlias(med(dql))), DQLExpressions.valueDate(CoreDateUtils.getDayFirstTimeMoment(_acceptionDateFrom.getData()))));
        }

        if (_acceptionDateTo.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "employeePPSData.acceptionDateTo", DateFormatter.DEFAULT_DATE_FORMATTER.format(_acceptionDateTo.getData()));

            dql.builder.where(DQLExpressions.lt(DQLExpressions.property(EmployeeMedicalSpeciality.acceptionDate().fromAlias(med(dql))), DQLExpressions.valueDate(CoreDateUtils.getNextDayFirstTimeMoment(_acceptionDateTo.getData(), 1))));
        }

        if (_medicalQualificationCategory.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "employeePPSData.medicalQualificationCategory", CommonBaseStringUtil.join(_medicalQualificationCategory.getData(), MedicalQualificationCategory.P_TITLE, ", "));

            dql.builder.where(DQLExpressions.in(DQLExpressions.property(EmployeeMedicalSpeciality.medicalQualificationCategory().fromAlias(med(dql))), _medicalQualificationCategory.getData()));
        }

        if (_categoryAssignDateFrom.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "employeePPSData.categoryAssignDateFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(_categoryAssignDateFrom.getData()));

            dql.builder.where(DQLExpressions.ge(DQLExpressions.property(EmployeeMedicalSpeciality.categoryAssignDate().fromAlias(med(dql))), DQLExpressions.valueDate(CoreDateUtils.getDayFirstTimeMoment(_categoryAssignDateFrom.getData()))));
        }

        if (_categoryAssignDateTo.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "employeePPSData.categoryAssignDateTo", DateFormatter.DEFAULT_DATE_FORMATTER.format(_categoryAssignDateTo.getData()));

            dql.builder.where(DQLExpressions.lt(DQLExpressions.property(EmployeeMedicalSpeciality.categoryAssignDate().fromAlias(med(dql))), DQLExpressions.valueDate(CoreDateUtils.getNextDayFirstTimeMoment(_categoryAssignDateTo.getData(), 1))));
        }

        if (_categoryAcceptionDateFrom.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "employeePPSData.categoryAcceptionDateFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(_categoryAcceptionDateFrom.getData()));

            dql.builder.where(DQLExpressions.ge(DQLExpressions.property(EmployeeMedicalSpeciality.categoryAcceptionDate().fromAlias(med(dql))), DQLExpressions.valueDate(CoreDateUtils.getDayFirstTimeMoment(_categoryAcceptionDateFrom.getData()))));
        }

        if (_categoryAcceptionDateTo.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "employeePPSData.categoryAcceptionDateTo", DateFormatter.DEFAULT_DATE_FORMATTER.format(_categoryAcceptionDateTo.getData()));

            dql.builder.where(DQLExpressions.lt(DQLExpressions.property(EmployeeMedicalSpeciality.categoryAcceptionDate().fromAlias(med(dql))), DQLExpressions.valueDate(CoreDateUtils.getNextDayFirstTimeMoment(_categoryAcceptionDateTo.getData(), 1))));
        }

        if (_expertCertificate.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "employeePPSData.expertCertificate", _expertCertificate.getData().getTitle());

            dql.builder.where(DQLExpressions.eq(DQLExpressions.property(EmployeeMedicalSpeciality.expertCertificateAvailable().fromAlias(med(dql))), DQLExpressions.value(TwinComboDataSourceHandler.getSelectedValue(_expertCertificate.getData()))));
        }
    }
    // Getters

    public IReportParam<DataWrapper> getScientificWork()
    {
        return _scientificWork;
    }

    public IReportParam<DataWrapper> getInvention()
    {
        return _invention;
    }

    public IReportParam<List<MedicalEducationLevel>> getMedicalEducationLevel()
    {
        return _medicalEducationLevel;
    }

    public IReportParam<Date> getAssignDateFrom()
    {
        return _assignDateFrom;
    }

    public IReportParam<Date> getAssignDateTo()
    {
        return _assignDateTo;
    }

    public IReportParam<Date> getAcceptionDateFrom()
    {
        return _acceptionDateFrom;
    }

    public IReportParam<Date> getAcceptionDateTo()
    {
        return _acceptionDateTo;
    }

    public IReportParam<List<MedicalQualificationCategory>> getMedicalQualificationCategory()
    {
        return _medicalQualificationCategory;
    }

    public IReportParam<Date> getCategoryAssignDateFrom()
    {
        return _categoryAssignDateFrom;
    }

    public IReportParam<Date> getCategoryAssignDateTo()
    {
        return _categoryAssignDateTo;
    }

    public IReportParam<Date> getCategoryAcceptionDateFrom()
    {
        return _categoryAcceptionDateFrom;
    }

    public IReportParam<Date> getCategoryAcceptionDateTo()
    {
        return _categoryAcceptionDateTo;
    }

    public IReportParam<DataWrapper> getExpertCertificate()
    {
        return _expertCertificate;
    }
}
