/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.EmployeePaymentAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.employee.EmployeePayment;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;

import java.util.List;

/**
 * @author dseleznev
 * Created on: 23.12.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickApply(IBusinessComponent component)
    {
        ErrorCollector errCollector = component.getUserContext().getErrorCollector();
        getDao().validate(getModel(component), errCollector);

        if (!errCollector.hasErrors())
        {
            getDao().update(getModel(component));
            deactivate(component);
        }
    }
    
    public void onPaymentChange(IBusinessComponent component)
    {
        Model model = getModel(component);
        EmployeePayment employeePayment = model.getEmployeePayment();
        if(null != employeePayment.getPayment())
        {
            model.setShowRecalculateButton(false);

            employeePayment.setPaymentValue(employeePayment.getPayment().getValue());

            employeePayment.setFinancingSource(employeePayment.getPayment().getFinancingSource());

            //вычесляем ссуммарную ставку
            List<EmployeePostStaffRateItem> staffRateItemList = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(model.getEmployeePost());
            Double summStaffRate = 0.0d;
            for (EmployeePostStaffRateItem staffRateItem : staffRateItemList)
                summStaffRate += staffRateItem.getStaffRate();

            //учитываем метку в выплате "Не зависит от ставки"
            double staffRate = employeePayment.getPayment().isDoesntDependOnStaffRate() ? 1d : summStaffRate;

            //вычесляем оклад - базовый оклад * ставку
            Double salary = null;
            if (model.getEmployeePost().getSalary() != null)
                salary = model.getEmployeePost().getSalary();
            else if (model.getEmployeePost().getRaisingCoefficient() != null)
                salary = model.getEmployeePost().getRaisingCoefficient().getRecommendedSalary() * staffRate;
            else if (model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL().getSalary() != null)
                salary = model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL().getSalary() * staffRate;

            //поднимаем Базовое значение выплаты для Должности
            double basePaymentValue = UniempDaoFacade.getUniempDAO().getPaymentBaseValueForPost(employeePayment.getPayment(), model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL());

            //вычесляем Значение выплаты
            Double paymentValue = basePaymentValue != 0d ? basePaymentValue : null;
            if (paymentValue == null)
                paymentValue = employeePayment.getPaymentValue();

            //вычесляем Сумму выплаты
            if (paymentValue != null )
            {
                if (employeePayment.getPayment().getPaymentUnit().getCode().equals(UniempDefines.PAYMENT_UNIT_RUBLES))
                {
                    employeePayment.setAmount(paymentValue * staffRate);
                }
                else if (salary != null)
                {
                    if (employeePayment.getPayment().getPaymentUnit().getCode().equals(UniempDefines.PAYMENT_UNIT_COEFFICIENT))
                        employeePayment.setAmount(paymentValue * salary);
                    else if (employeePayment.getPayment().getPaymentUnit().getCode().equals(UniempDefines.PAYMENT_UNIT_BASE_PERCENT))
                        employeePayment.setAmount(paymentValue / 100d * salary);
                    else//иначе выплата расчитывается на МФОТ с надбавками. Нужно воспользоваться настройкой "Порядок начисления выплат на ФОТ"
                    {
                        List<Payment> dependPaymentList = UniempDaoFacade.getUniempDAO().getPaymentOnFOTToPaymentsList(employeePayment.getPayment());
                        List<EmployeePayment> actualPaymentList = getDao().getActualPaymentList(model.getEmployeePost(), employeePayment.getBeginDate(), dependPaymentList);

                        Double paymentSumm = 0d;
                        for (EmployeePayment payment : actualPaymentList)
                            paymentSumm += payment.getAmount();

                        employeePayment.setAmount(paymentValue / 100d * (salary + paymentSumm));
                    }
                }
            }
            else
                employeePayment.setAmount(null);

            if (employeePayment.getPayment().isFixedValue())
                model.setPaymentValueDisabled(true);
            else
                model.setPaymentValueDisabled(false);

            if (employeePayment.getPayment().isFixedValue() && employeePayment.getPaymentValue() == null)
                employeePayment.setPaymentValue(0d);
        }
        else
        {
            employeePayment.setAmount(null);
            model.setPaymentValueDisabled(false);
        }
    }

    public void onClickRecalculate(IBusinessComponent component)
    {
        Model model = getModel(component);
        EmployeePayment employeePayment = model.getEmployeePayment();

        //вычесляем ссуммарную ставку
        List<EmployeePostStaffRateItem> staffRateItemList = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(model.getEmployeePost());
        Double summStaffRate = 0.0d;
        for (EmployeePostStaffRateItem staffRateItem : staffRateItemList)
            summStaffRate += staffRateItem.getStaffRate();

        //учитываем метку в выплате "Не зависит от ставки"
        double staffRate = employeePayment.getPayment().isDoesntDependOnStaffRate() ? 1d : summStaffRate;

        //вычесляем оклад - базовый оклад * ставку
        Double salary = null;
        if (model.getEmployeePost().getSalary() != null)
            salary = model.getEmployeePost().getSalary();
        else if (model.getEmployeePost().getRaisingCoefficient() != null)
            salary = model.getEmployeePost().getRaisingCoefficient().getRecommendedSalary() * staffRate;
        else if (model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL().getSalary() != null)
            salary = model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL().getSalary() * staffRate;

        //поднимаем Базовое значение выплаты для Должности
        Double basePaymentValue = UniempDaoFacade.getUniempDAO().getPaymentBaseValueForPost(employeePayment.getPayment(), model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL());

        //вычесляем Значение выплаты
        Double paymentValue = basePaymentValue != 0d ? basePaymentValue : employeePayment.getPaymentValue();

        //вычесляем Сумму выплаты
        if (paymentValue != null )
        {
            if (employeePayment.getPayment().getPaymentUnit().getCode().equals(UniempDefines.PAYMENT_UNIT_RUBLES))
            {
                employeePayment.setAmount(paymentValue * staffRate);
            }
            else if (salary != null)
            {
                if (employeePayment.getPayment().getPaymentUnit().getCode().equals(UniempDefines.PAYMENT_UNIT_COEFFICIENT))
                    employeePayment.setAmount(paymentValue * salary);
                else if (employeePayment.getPayment().getPaymentUnit().getCode().equals(UniempDefines.PAYMENT_UNIT_BASE_PERCENT))
                    employeePayment.setAmount(paymentValue / 100d * salary);
                else//иначе выплата расчитывается на МФОТ с надбавками. Нужно воспользоваться настройкой "Порядок начисления выплат на ФОТ"
                {
                    List<Payment> dependPaymentList = UniempDaoFacade.getUniempDAO().getPaymentOnFOTToPaymentsList(employeePayment.getPayment());
                    List<EmployeePayment> actualPaymentList = getDao().getActualPaymentList(model.getEmployeePost(), employeePayment.getBeginDate(), dependPaymentList);

                    Double paymentSumm = 0d;
                    for (EmployeePayment payment : actualPaymentList)
                        paymentSumm += payment.getAmount();

                    employeePayment.setAmount(paymentValue / 100d * (salary + paymentSumm));
                }
            }
        }
        else
            employeePayment.setAmount(null);
    }

}