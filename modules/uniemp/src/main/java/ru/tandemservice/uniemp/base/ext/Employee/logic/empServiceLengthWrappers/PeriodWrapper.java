/**
 *$Id:$
 */
package ru.tandemservice.uniemp.base.ext.Employee.logic.empServiceLengthWrappers;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Период дат.
 * Используется для подсчета Стажа Кадрового ресурса.
 *
 * @author Alexander Shaburov
 * @since 21.09.12
 */
public class PeriodWrapper
{
    // fields

    private Date _beginDate;
    private Date _endDate;

    // constructor

    /**
     * @param beginDate начало периода, not null
     * @param endDate конец периода
     * @throws NullPointerException if beginDate is null
     */
    public PeriodWrapper(Date beginDate, Date endDate)
    {
        if (beginDate == null)
            throw new NullPointerException();

        _beginDate = beginDate;
        _endDate = endDate;
    }

    // logic

    /**
     * Проверяет пересекаются ли периоды.<p/>
     * <b>Note:</b> Если периоды идут друг за другом, то считается, что они пересекаются.
     * @return true, если this пересекается с anotheWrapper, иначе false
     */
    public boolean intersectTo(PeriodWrapper anotheWrapper)
    {
        if (_endDate != null && anotheWrapper.getEndDate() != null)
        {
            if (_beginDate.equals(anotheWrapper.getBeginDate()) && _endDate.equals(anotheWrapper.getEndDate()))
                return true;

            Calendar begin1 = new GregorianCalendar();
            begin1.setTime(_beginDate);
            begin1.add(Calendar.DATE, -1);

            Calendar end1 = new GregorianCalendar();
            end1.setTime(_endDate);
            end1.add(Calendar.DATE, 1);

            Calendar begin2 = new GregorianCalendar();
            begin2.setTime(anotheWrapper.getBeginDate());
            begin2.add(Calendar.DATE, -1);

            Calendar end2 = new GregorianCalendar();
            end2.setTime(anotheWrapper.getEndDate());
            end2.add(Calendar.DATE, 1);

            if (between(begin1, begin2, end2) || between(end1, begin2, end2))
                return true;
            else if (between(begin2, begin1, end1) || between(end2, begin1, end1))
                return true;

            return false;
        }
        else
        {
            if (_endDate == null && anotheWrapper.getEndDate() == null)
            {
                return true;
            }
            else if (_endDate == null)
            {
                Calendar begin = new GregorianCalendar();
                begin.setTime(_beginDate);
                begin.add(Calendar.DATE, -1);

                Calendar end = new GregorianCalendar();
                end.setTime(anotheWrapper.getEndDate());
                end.add(Calendar.DATE, 1);

                return begin.before(end);
            }
            else if (anotheWrapper.getEndDate() == null)
            {
                Calendar begin = new GregorianCalendar();
                begin.setTime(anotheWrapper.getBeginDate());
                begin.add(Calendar.DATE, -1);

                Calendar end = new GregorianCalendar();
                end.setTime(_endDate);
                end.add(Calendar.DATE, 1);

                return begin.before(end);
            }
        }

        throw new NullPointerException();
    }

    private boolean between(Calendar date, Calendar beginDate, Calendar endDate)
    {
        return beginDate.before(date) && endDate.after(date);
    }
    
    public static Date max(Date date1, Date date2)
    {
        if (date1 == null || date2 == null)
            return null;
        else 
            if (date1.getTime() >= date2.getTime())
                return date1;
            else
                return date2;
    }           
    
    public static Date min(Date date1, Date date2)
    {
        if (date1.getTime() <= date2.getTime())
            return date1;
        else
            return date2;
    }

    // Getters & Setters

    public Date getBeginDate()
    {
        return _beginDate;
    }

    public Date getEndDate()
    {
        return _endDate;
    }
}
