/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.EmployeePaymentAddEdit;

import org.apache.cxf.common.util.StringUtils;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.UniempDAO;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.catalog.PaymentType;
import ru.tandemservice.uniemp.entity.employee.EmployeePayment;

import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author dseleznev
 * Created on: 23.12.2008
 */
public class DAO extends UniempDAO<Model> implements IDAO
{

    @Override
    public void prepare(final Model model)
    {
        if (null != model.getEmployeePostId())
            model.setEmployeePost(get(EmployeePost.class, model.getEmployeePostId()));

        if (null != model.getEmployeePayment().getId())
        {
            model.setEmployeePayment(get(EmployeePayment.class, model.getEmployeePayment().getId()));

            if (model.getEmployeePayment().getPayment().getPaymentUnit().getCode().equals(UniempDefines.PAYMENT_UNIT_RUBLES) && model.getEmployeePayment().getPayment().isDoesntDependOnStaffRate())
                model.setShowRecalculateButton(false);
        }
        else
        {
            model.getEmployeePayment().setAssignDate(new Date());
            model.getEmployeePayment().setEmployeePost(model.getEmployeePost());

            model.setShowRecalculateButton(false);
        }

        model.setPaymentTypesList(HierarchyUtil.listHierarchyNodesWithParents(getList(PaymentType.class), true));
        model.setFinancingSourcesList(getCatalogItemListOrderByCode(FinancingSource.class));
        model.setFinancingSourceItemsListModel(new BaseSingleSelectModel()
        {
            @Override
            public ListResult<FinancingSourceItem> findValues(String filter)
            {
                if (null == model.getEmployeePayment().getFinancingSource()) return ListResult.getEmpty();
                MQBuilder builder = new MQBuilder(FinancingSourceItem.ENTITY_CLASS, "fs");
                builder.add(MQExpression.eq("fs", FinancingSourceItem.financingSource().s(), model.getEmployeePayment().getFinancingSource()));
                if(!StringUtils.isEmpty(filter))
                    builder.add(MQExpression.like("fs", FinancingSourceItem.title().s(), CoreStringUtils.escapeLike(filter)));
                builder.addOrder("fs", FinancingSourceItem.title().s());
                return new ListResult<>(builder.<FinancingSourceItem> getResultList(getSession(), 0, 50), builder.getResultCount(getSession()));
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                if (null == model.getEmployeePayment().getFinancingSource()) return null;
                MQBuilder builder = new MQBuilder(FinancingSourceItem.ENTITY_CLASS, "fs");
                builder.add(MQExpression.eq("fs", FinancingSourceItem.financingSource(), model.getEmployeePayment().getFinancingSource()));
                builder.add(MQExpression.eq("fs", FinancingSourceItem.id().s(), primaryKey));
                if (builder.getResultCount(getSession()) > 0) return get(FinancingSourceItem.class, (Long)primaryKey);
                return null;
            }
        });

        if (null != model.getEmployeePayment().getPayment())
            model.setPaymentType(model.getEmployeePayment().getPayment().getType());

        model.setPaymentsModel(new BaseSingleSelectModel()
        {
            @Override
            @SuppressWarnings("unchecked")
            public ListResult findValues(String filter)
            {
                if (model.getPaymentType() == null)
                    return ListResult.getEmpty();

                return new ListResult<Payment>(getSession().createCriteria(Payment.class)
                        .add(Restrictions.eq(Payment.type().s(), model.getPaymentType()))
                        .add(Restrictions.eq(Payment.active().s(), Boolean.TRUE))
                        .addOrder(Order.asc(Payment.title().s())).list());
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                IEntity entity = get((Long)primaryKey);
                if (findValues("").getObjects().contains(entity))
                    return entity;
                else
                    return null;
            }
        });
    }

    @Override
    public void update(Model model)
    {
        if (null != model.getEmployeePayment().getAmount())
            model.getEmployeePayment().setAmount(Math.round(model.getEmployeePayment().getAmount() * 100) / 100.0);

        if(model.getEmployeePayment().getPayment().isOneTimePayment())
        {
            model.getEmployeePayment().setBeginDate(model.getEmployeePayment().getAssignDate());
            model.getEmployeePayment().setEndDate(null);
        }

        getSession().saveOrUpdate(model.getEmployeePayment());
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        EmployeePayment payment = model.getEmployeePayment();
        if (null != payment.getBeginDate() && null != payment.getEndDate() && payment.getBeginDate().getTime() > payment.getEndDate().getTime())
            errors.add("Дата начала периода должна быть меньше даты его окончания", "beginDate", "endDate");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmployeePayment.class, "b").column("b");
        builder.where(DQLExpressions.eqValue(DQLExpressions.property(EmployeePayment.employeePost().fromAlias("b")), model.getEmployeePost()));
        builder.where(DQLExpressions.ne(DQLExpressions.property(EmployeePayment.id().fromAlias("b")), DQLExpressions.value(model.getEmployeePayment().getId())));

        List<EmployeePayment> employeePaymentList = builder.createStatement(getSession()).list();

        for (EmployeePayment employeePayment : employeePaymentList)
            if (employeePayment.getPayment().equals(payment.getPayment()))
                if (employeePayment.getFinancingSource().equals(payment.getFinancingSource()))
                    if ((employeePayment.getFinancingSourceItem() == null && payment.getFinancingSourceItem() == null) || (employeePayment.getFinancingSourceItem() != null && employeePayment.getFinancingSourceItem().equals(payment.getFinancingSourceItem())))
                    {
                        Date begin1 = employeePayment.getBeginDate();
                        Date end1 = employeePayment.getEndDate();
                        Date begin2 = payment.getBeginDate();
                        Date end2 = payment.getEndDate();

                        StringBuilder errorBuilder = new StringBuilder("Период выплаты пересекается с периодом такой же выплаты в списке «Выплаты» на карточке сотрудника.");

                        if (end1 == null && end2 == null)
                            errors.add(errorBuilder.toString(),
                                    "beginDate", "endDate");
                        else if (end1 == null)
                        {
                            if (begin1.getTime() <= end2.getTime())
                                errors.add(errorBuilder.toString(),
                                    "beginDate", "endDate");
                        }
                        else if (end2 == null)
                        {
                            if (begin2.getTime() <= end1.getTime())
                                errors.add(errorBuilder.toString(),
                                    "beginDate", "endDate");
                        }
                        else
                        {
                            if (CommonBaseDateUtil.isBetween(begin1, begin2, end2) || CommonBaseDateUtil.isBetween(end1, begin2, end2))
                                errors.add(errorBuilder.toString(),
                                    "beginDate", "endDate");
                            else {
                                if (CommonBaseDateUtil.isBetween(begin2, begin1, end1) || CommonBaseDateUtil.isBetween(end2, begin1, end1))
                                    errors.add(errorBuilder.toString(),
                                        "beginDate", "endDate");
                            }
                        }
                    }
    }

    @Override
    public List<EmployeePayment> getActualPaymentList(EmployeePost employeePost, Date beginDate, List<Payment> paymentList)
    {
        if (beginDate == null)
            return Collections.emptyList();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmployeePayment.class, "b").column("b");
        builder.where(DQLExpressions.eqValue(DQLExpressions.property(EmployeePayment.employeePost().fromAlias("b")), employeePost));
        builder.where(DQLExpressions.eqValue(DQLExpressions.property(EmployeePayment.payment().oneTimePayment().fromAlias("b")), false));
        builder.where(DQLExpressions.le(DQLExpressions.property(EmployeePayment.beginDate().fromAlias("b")), DQLExpressions.valueDate(beginDate)));
        IDQLExpression ge = DQLExpressions.ge(DQLExpressions.property(EmployeePayment.endDate().fromAlias("b")), DQLExpressions.valueDate(beginDate));
        IDQLExpression isNull = DQLExpressions.isNull(DQLExpressions.property(EmployeePayment.endDate().fromAlias("b")));
        builder.where(DQLExpressions.or(isNull, ge));

        if (paymentList != null && !paymentList.isEmpty())
            builder.where(DQLExpressions.in(DQLExpressions.property(EmployeePayment.payment().fromAlias("b")), paymentList));

        return builder.createStatement(getSession()).list();
    }
}