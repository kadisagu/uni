package ru.tandemservice.uniemp.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EtksLevels;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.SalaryRaisingCoefficient;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniemp.entity.catalog.CombinationPostType;
import ru.tandemservice.uniemp.entity.employee.CombinationPost;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Должность по совмещению
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class CombinationPostGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.employee.CombinationPost";
    public static final String ENTITY_NAME = "combinationPost";
    public static final int VERSION_HASH = 1233435969;
    private static IEntityMeta ENTITY_META;

    public static final String L_COMBINATION_POST_TYPE = "combinationPostType";
    public static final String L_EMPLOYEE_POST = "employeePost";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_POST_BOUNDED_WITH_Q_GAND_Q_L = "postBoundedWithQGandQL";
    public static final String L_MISSING_EMPLOYEE_POST = "missingEmployeePost";
    public static final String L_SALARY_RAISING_COEFFICIENT = "salaryRaisingCoefficient";
    public static final String L_ETKS_LEVELS = "etksLevels";
    public static final String P_SALARY = "salary";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_ORDER_NUMBER = "orderNumber";
    public static final String P_COMMIT_DATE_SYSTEM = "commitDateSystem";
    public static final String P_COLLATERAL_AGREEMENT_NUMBER = "collateralAgreementNumber";
    public static final String P_COLLATERAL_AGREEMENT_DATE = "collateralAgreementDate";
    public static final String P_FREELANCE = "freelance";

    private CombinationPostType _combinationPostType;     // Типы совмещения должностей
    private EmployeePost _employeePost;     // Сотрудник
    private OrgUnit _orgUnit;     // Подразделение
    private PostBoundedWithQGandQL _postBoundedWithQGandQL;     // Должность, отнесенная к ПКГ и КУ
    private EmployeePost _missingEmployeePost;     // Сотрудник, на время отсутствия которого совмещается должность
    private SalaryRaisingCoefficient _salaryRaisingCoefficient;     // Повышающий коэффициент
    private EtksLevels _etksLevels;     // Разряд ЕТКС
    private Double _salary;     // Оклад с повышающим коэффициентом
    private Date _beginDate;     // Дата начала работы
    private Date _endDate;     // Дата окончания работы
    private String _orderNumber;     // Номер приказа
    private Date _commitDateSystem;     // Дата проведения приказа
    private String _collateralAgreementNumber;     // Номер доп. соглашения
    private Date _collateralAgreementDate;     // Дата доп. соглашения
    private boolean _freelance;     // Вне штата

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Типы совмещения должностей. Свойство не может быть null.
     */
    @NotNull
    public CombinationPostType getCombinationPostType()
    {
        return _combinationPostType;
    }

    /**
     * @param combinationPostType Типы совмещения должностей. Свойство не может быть null.
     */
    public void setCombinationPostType(CombinationPostType combinationPostType)
    {
        dirty(_combinationPostType, combinationPostType);
        _combinationPostType = combinationPostType;
    }

    /**
     * @return Сотрудник. Свойство не может быть null.
     */
    @NotNull
    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    /**
     * @param employeePost Сотрудник. Свойство не может быть null.
     */
    public void setEmployeePost(EmployeePost employeePost)
    {
        dirty(_employeePost, employeePost);
        _employeePost = employeePost;
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    @NotNull
    public PostBoundedWithQGandQL getPostBoundedWithQGandQL()
    {
        return _postBoundedWithQGandQL;
    }

    /**
     * @param postBoundedWithQGandQL Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    public void setPostBoundedWithQGandQL(PostBoundedWithQGandQL postBoundedWithQGandQL)
    {
        dirty(_postBoundedWithQGandQL, postBoundedWithQGandQL);
        _postBoundedWithQGandQL = postBoundedWithQGandQL;
    }

    /**
     * @return Сотрудник, на время отсутствия которого совмещается должность.
     */
    public EmployeePost getMissingEmployeePost()
    {
        return _missingEmployeePost;
    }

    /**
     * @param missingEmployeePost Сотрудник, на время отсутствия которого совмещается должность.
     */
    public void setMissingEmployeePost(EmployeePost missingEmployeePost)
    {
        dirty(_missingEmployeePost, missingEmployeePost);
        _missingEmployeePost = missingEmployeePost;
    }

    /**
     * @return Повышающий коэффициент.
     */
    public SalaryRaisingCoefficient getSalaryRaisingCoefficient()
    {
        return _salaryRaisingCoefficient;
    }

    /**
     * @param salaryRaisingCoefficient Повышающий коэффициент.
     */
    public void setSalaryRaisingCoefficient(SalaryRaisingCoefficient salaryRaisingCoefficient)
    {
        dirty(_salaryRaisingCoefficient, salaryRaisingCoefficient);
        _salaryRaisingCoefficient = salaryRaisingCoefficient;
    }

    /**
     * @return Разряд ЕТКС.
     */
    public EtksLevels getEtksLevels()
    {
        return _etksLevels;
    }

    /**
     * @param etksLevels Разряд ЕТКС.
     */
    public void setEtksLevels(EtksLevels etksLevels)
    {
        dirty(_etksLevels, etksLevels);
        _etksLevels = etksLevels;
    }

    /**
     * @return Оклад с повышающим коэффициентом.
     */
    public Double getSalary()
    {
        return _salary;
    }

    /**
     * @param salary Оклад с повышающим коэффициентом.
     */
    public void setSalary(Double salary)
    {
        dirty(_salary, salary);
        _salary = salary;
    }

    /**
     * @return Дата начала работы. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала работы. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания работы.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания работы.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Номер приказа.
     */
    @Length(max=255)
    public String getOrderNumber()
    {
        return _orderNumber;
    }

    /**
     * @param orderNumber Номер приказа.
     */
    public void setOrderNumber(String orderNumber)
    {
        dirty(_orderNumber, orderNumber);
        _orderNumber = orderNumber;
    }

    /**
     * @return Дата проведения приказа.
     */
    public Date getCommitDateSystem()
    {
        return _commitDateSystem;
    }

    /**
     * @param commitDateSystem Дата проведения приказа.
     */
    public void setCommitDateSystem(Date commitDateSystem)
    {
        dirty(_commitDateSystem, commitDateSystem);
        _commitDateSystem = commitDateSystem;
    }

    /**
     * @return Номер доп. соглашения.
     */
    @Length(max=255)
    public String getCollateralAgreementNumber()
    {
        return _collateralAgreementNumber;
    }

    /**
     * @param collateralAgreementNumber Номер доп. соглашения.
     */
    public void setCollateralAgreementNumber(String collateralAgreementNumber)
    {
        dirty(_collateralAgreementNumber, collateralAgreementNumber);
        _collateralAgreementNumber = collateralAgreementNumber;
    }

    /**
     * @return Дата доп. соглашения.
     */
    public Date getCollateralAgreementDate()
    {
        return _collateralAgreementDate;
    }

    /**
     * @param collateralAgreementDate Дата доп. соглашения.
     */
    public void setCollateralAgreementDate(Date collateralAgreementDate)
    {
        dirty(_collateralAgreementDate, collateralAgreementDate);
        _collateralAgreementDate = collateralAgreementDate;
    }

    /**
     * @return Вне штата. Свойство не может быть null.
     */
    @NotNull
    public boolean isFreelance()
    {
        return _freelance;
    }

    /**
     * @param freelance Вне штата. Свойство не может быть null.
     */
    public void setFreelance(boolean freelance)
    {
        dirty(_freelance, freelance);
        _freelance = freelance;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof CombinationPostGen)
        {
            setCombinationPostType(((CombinationPost)another).getCombinationPostType());
            setEmployeePost(((CombinationPost)another).getEmployeePost());
            setOrgUnit(((CombinationPost)another).getOrgUnit());
            setPostBoundedWithQGandQL(((CombinationPost)another).getPostBoundedWithQGandQL());
            setMissingEmployeePost(((CombinationPost)another).getMissingEmployeePost());
            setSalaryRaisingCoefficient(((CombinationPost)another).getSalaryRaisingCoefficient());
            setEtksLevels(((CombinationPost)another).getEtksLevels());
            setSalary(((CombinationPost)another).getSalary());
            setBeginDate(((CombinationPost)another).getBeginDate());
            setEndDate(((CombinationPost)another).getEndDate());
            setOrderNumber(((CombinationPost)another).getOrderNumber());
            setCommitDateSystem(((CombinationPost)another).getCommitDateSystem());
            setCollateralAgreementNumber(((CombinationPost)another).getCollateralAgreementNumber());
            setCollateralAgreementDate(((CombinationPost)another).getCollateralAgreementDate());
            setFreelance(((CombinationPost)another).isFreelance());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends CombinationPostGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) CombinationPost.class;
        }

        public T newInstance()
        {
            return (T) new CombinationPost();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "combinationPostType":
                    return obj.getCombinationPostType();
                case "employeePost":
                    return obj.getEmployeePost();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "postBoundedWithQGandQL":
                    return obj.getPostBoundedWithQGandQL();
                case "missingEmployeePost":
                    return obj.getMissingEmployeePost();
                case "salaryRaisingCoefficient":
                    return obj.getSalaryRaisingCoefficient();
                case "etksLevels":
                    return obj.getEtksLevels();
                case "salary":
                    return obj.getSalary();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "orderNumber":
                    return obj.getOrderNumber();
                case "commitDateSystem":
                    return obj.getCommitDateSystem();
                case "collateralAgreementNumber":
                    return obj.getCollateralAgreementNumber();
                case "collateralAgreementDate":
                    return obj.getCollateralAgreementDate();
                case "freelance":
                    return obj.isFreelance();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "combinationPostType":
                    obj.setCombinationPostType((CombinationPostType) value);
                    return;
                case "employeePost":
                    obj.setEmployeePost((EmployeePost) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "postBoundedWithQGandQL":
                    obj.setPostBoundedWithQGandQL((PostBoundedWithQGandQL) value);
                    return;
                case "missingEmployeePost":
                    obj.setMissingEmployeePost((EmployeePost) value);
                    return;
                case "salaryRaisingCoefficient":
                    obj.setSalaryRaisingCoefficient((SalaryRaisingCoefficient) value);
                    return;
                case "etksLevels":
                    obj.setEtksLevels((EtksLevels) value);
                    return;
                case "salary":
                    obj.setSalary((Double) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "orderNumber":
                    obj.setOrderNumber((String) value);
                    return;
                case "commitDateSystem":
                    obj.setCommitDateSystem((Date) value);
                    return;
                case "collateralAgreementNumber":
                    obj.setCollateralAgreementNumber((String) value);
                    return;
                case "collateralAgreementDate":
                    obj.setCollateralAgreementDate((Date) value);
                    return;
                case "freelance":
                    obj.setFreelance((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "combinationPostType":
                        return true;
                case "employeePost":
                        return true;
                case "orgUnit":
                        return true;
                case "postBoundedWithQGandQL":
                        return true;
                case "missingEmployeePost":
                        return true;
                case "salaryRaisingCoefficient":
                        return true;
                case "etksLevels":
                        return true;
                case "salary":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "orderNumber":
                        return true;
                case "commitDateSystem":
                        return true;
                case "collateralAgreementNumber":
                        return true;
                case "collateralAgreementDate":
                        return true;
                case "freelance":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "combinationPostType":
                    return true;
                case "employeePost":
                    return true;
                case "orgUnit":
                    return true;
                case "postBoundedWithQGandQL":
                    return true;
                case "missingEmployeePost":
                    return true;
                case "salaryRaisingCoefficient":
                    return true;
                case "etksLevels":
                    return true;
                case "salary":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "orderNumber":
                    return true;
                case "commitDateSystem":
                    return true;
                case "collateralAgreementNumber":
                    return true;
                case "collateralAgreementDate":
                    return true;
                case "freelance":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "combinationPostType":
                    return CombinationPostType.class;
                case "employeePost":
                    return EmployeePost.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "postBoundedWithQGandQL":
                    return PostBoundedWithQGandQL.class;
                case "missingEmployeePost":
                    return EmployeePost.class;
                case "salaryRaisingCoefficient":
                    return SalaryRaisingCoefficient.class;
                case "etksLevels":
                    return EtksLevels.class;
                case "salary":
                    return Double.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "orderNumber":
                    return String.class;
                case "commitDateSystem":
                    return Date.class;
                case "collateralAgreementNumber":
                    return String.class;
                case "collateralAgreementDate":
                    return Date.class;
                case "freelance":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<CombinationPost> _dslPath = new Path<CombinationPost>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "CombinationPost");
    }
            

    /**
     * @return Типы совмещения должностей. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.CombinationPost#getCombinationPostType()
     */
    public static CombinationPostType.Path<CombinationPostType> combinationPostType()
    {
        return _dslPath.combinationPostType();
    }

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.CombinationPost#getEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> employeePost()
    {
        return _dslPath.employeePost();
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.CombinationPost#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.CombinationPost#getPostBoundedWithQGandQL()
     */
    public static PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
    {
        return _dslPath.postBoundedWithQGandQL();
    }

    /**
     * @return Сотрудник, на время отсутствия которого совмещается должность.
     * @see ru.tandemservice.uniemp.entity.employee.CombinationPost#getMissingEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> missingEmployeePost()
    {
        return _dslPath.missingEmployeePost();
    }

    /**
     * @return Повышающий коэффициент.
     * @see ru.tandemservice.uniemp.entity.employee.CombinationPost#getSalaryRaisingCoefficient()
     */
    public static SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> salaryRaisingCoefficient()
    {
        return _dslPath.salaryRaisingCoefficient();
    }

    /**
     * @return Разряд ЕТКС.
     * @see ru.tandemservice.uniemp.entity.employee.CombinationPost#getEtksLevels()
     */
    public static EtksLevels.Path<EtksLevels> etksLevels()
    {
        return _dslPath.etksLevels();
    }

    /**
     * @return Оклад с повышающим коэффициентом.
     * @see ru.tandemservice.uniemp.entity.employee.CombinationPost#getSalary()
     */
    public static PropertyPath<Double> salary()
    {
        return _dslPath.salary();
    }

    /**
     * @return Дата начала работы. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.CombinationPost#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания работы.
     * @see ru.tandemservice.uniemp.entity.employee.CombinationPost#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Номер приказа.
     * @see ru.tandemservice.uniemp.entity.employee.CombinationPost#getOrderNumber()
     */
    public static PropertyPath<String> orderNumber()
    {
        return _dslPath.orderNumber();
    }

    /**
     * @return Дата проведения приказа.
     * @see ru.tandemservice.uniemp.entity.employee.CombinationPost#getCommitDateSystem()
     */
    public static PropertyPath<Date> commitDateSystem()
    {
        return _dslPath.commitDateSystem();
    }

    /**
     * @return Номер доп. соглашения.
     * @see ru.tandemservice.uniemp.entity.employee.CombinationPost#getCollateralAgreementNumber()
     */
    public static PropertyPath<String> collateralAgreementNumber()
    {
        return _dslPath.collateralAgreementNumber();
    }

    /**
     * @return Дата доп. соглашения.
     * @see ru.tandemservice.uniemp.entity.employee.CombinationPost#getCollateralAgreementDate()
     */
    public static PropertyPath<Date> collateralAgreementDate()
    {
        return _dslPath.collateralAgreementDate();
    }

    /**
     * @return Вне штата. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.CombinationPost#isFreelance()
     */
    public static PropertyPath<Boolean> freelance()
    {
        return _dslPath.freelance();
    }

    public static class Path<E extends CombinationPost> extends EntityPath<E>
    {
        private CombinationPostType.Path<CombinationPostType> _combinationPostType;
        private EmployeePost.Path<EmployeePost> _employeePost;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> _postBoundedWithQGandQL;
        private EmployeePost.Path<EmployeePost> _missingEmployeePost;
        private SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> _salaryRaisingCoefficient;
        private EtksLevels.Path<EtksLevels> _etksLevels;
        private PropertyPath<Double> _salary;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<String> _orderNumber;
        private PropertyPath<Date> _commitDateSystem;
        private PropertyPath<String> _collateralAgreementNumber;
        private PropertyPath<Date> _collateralAgreementDate;
        private PropertyPath<Boolean> _freelance;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Типы совмещения должностей. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.CombinationPost#getCombinationPostType()
     */
        public CombinationPostType.Path<CombinationPostType> combinationPostType()
        {
            if(_combinationPostType == null )
                _combinationPostType = new CombinationPostType.Path<CombinationPostType>(L_COMBINATION_POST_TYPE, this);
            return _combinationPostType;
        }

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.CombinationPost#getEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> employeePost()
        {
            if(_employeePost == null )
                _employeePost = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE_POST, this);
            return _employeePost;
        }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.CombinationPost#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.CombinationPost#getPostBoundedWithQGandQL()
     */
        public PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
        {
            if(_postBoundedWithQGandQL == null )
                _postBoundedWithQGandQL = new PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL>(L_POST_BOUNDED_WITH_Q_GAND_Q_L, this);
            return _postBoundedWithQGandQL;
        }

    /**
     * @return Сотрудник, на время отсутствия которого совмещается должность.
     * @see ru.tandemservice.uniemp.entity.employee.CombinationPost#getMissingEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> missingEmployeePost()
        {
            if(_missingEmployeePost == null )
                _missingEmployeePost = new EmployeePost.Path<EmployeePost>(L_MISSING_EMPLOYEE_POST, this);
            return _missingEmployeePost;
        }

    /**
     * @return Повышающий коэффициент.
     * @see ru.tandemservice.uniemp.entity.employee.CombinationPost#getSalaryRaisingCoefficient()
     */
        public SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> salaryRaisingCoefficient()
        {
            if(_salaryRaisingCoefficient == null )
                _salaryRaisingCoefficient = new SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient>(L_SALARY_RAISING_COEFFICIENT, this);
            return _salaryRaisingCoefficient;
        }

    /**
     * @return Разряд ЕТКС.
     * @see ru.tandemservice.uniemp.entity.employee.CombinationPost#getEtksLevels()
     */
        public EtksLevels.Path<EtksLevels> etksLevels()
        {
            if(_etksLevels == null )
                _etksLevels = new EtksLevels.Path<EtksLevels>(L_ETKS_LEVELS, this);
            return _etksLevels;
        }

    /**
     * @return Оклад с повышающим коэффициентом.
     * @see ru.tandemservice.uniemp.entity.employee.CombinationPost#getSalary()
     */
        public PropertyPath<Double> salary()
        {
            if(_salary == null )
                _salary = new PropertyPath<Double>(CombinationPostGen.P_SALARY, this);
            return _salary;
        }

    /**
     * @return Дата начала работы. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.CombinationPost#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(CombinationPostGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания работы.
     * @see ru.tandemservice.uniemp.entity.employee.CombinationPost#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(CombinationPostGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Номер приказа.
     * @see ru.tandemservice.uniemp.entity.employee.CombinationPost#getOrderNumber()
     */
        public PropertyPath<String> orderNumber()
        {
            if(_orderNumber == null )
                _orderNumber = new PropertyPath<String>(CombinationPostGen.P_ORDER_NUMBER, this);
            return _orderNumber;
        }

    /**
     * @return Дата проведения приказа.
     * @see ru.tandemservice.uniemp.entity.employee.CombinationPost#getCommitDateSystem()
     */
        public PropertyPath<Date> commitDateSystem()
        {
            if(_commitDateSystem == null )
                _commitDateSystem = new PropertyPath<Date>(CombinationPostGen.P_COMMIT_DATE_SYSTEM, this);
            return _commitDateSystem;
        }

    /**
     * @return Номер доп. соглашения.
     * @see ru.tandemservice.uniemp.entity.employee.CombinationPost#getCollateralAgreementNumber()
     */
        public PropertyPath<String> collateralAgreementNumber()
        {
            if(_collateralAgreementNumber == null )
                _collateralAgreementNumber = new PropertyPath<String>(CombinationPostGen.P_COLLATERAL_AGREEMENT_NUMBER, this);
            return _collateralAgreementNumber;
        }

    /**
     * @return Дата доп. соглашения.
     * @see ru.tandemservice.uniemp.entity.employee.CombinationPost#getCollateralAgreementDate()
     */
        public PropertyPath<Date> collateralAgreementDate()
        {
            if(_collateralAgreementDate == null )
                _collateralAgreementDate = new PropertyPath<Date>(CombinationPostGen.P_COLLATERAL_AGREEMENT_DATE, this);
            return _collateralAgreementDate;
        }

    /**
     * @return Вне штата. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.CombinationPost#isFreelance()
     */
        public PropertyPath<Boolean> freelance()
        {
            if(_freelance == null )
                _freelance = new PropertyPath<Boolean>(CombinationPostGen.P_FREELANCE, this);
            return _freelance;
        }

        public Class getEntityClass()
        {
            return CombinationPost.class;
        }

        public String getEntityName()
        {
            return "combinationPost";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
