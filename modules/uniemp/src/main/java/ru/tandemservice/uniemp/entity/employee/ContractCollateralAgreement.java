package ru.tandemservice.uniemp.entity.employee;

import org.tandemframework.core.view.formatter.DateFormatter;

import ru.tandemservice.uniemp.entity.employee.gen.ContractCollateralAgreementGen;

public class ContractCollateralAgreement extends ContractCollateralAgreementGen
{
    public static String P_PRINTING_DISABLED = "printingDisabled";
    
    public boolean isPrintingDisabled()
    {
        return null == getAgreementFile();
    }
    
    public static String P_FULL_NUMBER = "fullNumber";
    
    public String getFullNumber()
    {
        return "№ " + getNumber() + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDate());
    }
    
}