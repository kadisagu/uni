package ru.tandemservice.uniemp.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.SalaryRaisingCoefficient;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.employee.CombinationPost;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;
import ru.tandemservice.uniemp.entity.employee.StaffListItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Должность штатной расстановки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StaffListAllocationItemGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem";
    public static final String ENTITY_NAME = "staffListAllocationItem";
    public static final int VERSION_HASH = -902867883;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_POST = "employeePost";
    public static final String L_STAFF_LIST_ITEM = "staffListItem";
    public static final String P_STAFF_RATE_INTEGER = "staffRateInteger";
    public static final String L_FINANCING_SOURCE = "financingSource";
    public static final String L_FINANCING_SOURCE_ITEM = "financingSourceItem";
    public static final String L_RAISING_COEFFICIENT = "raisingCoefficient";
    public static final String P_MONTH_BASE_SALARY_FUND = "monthBaseSalaryFund";
    public static final String P_TARGET_SALARY = "targetSalary";
    public static final String P_RESERVE = "reserve";
    public static final String L_COMBINATION_POST = "combinationPost";
    public static final String P_COMBINATION = "combination";

    private EmployeePost _employeePost;     // Сотрудник на должности
    private StaffListItem _staffListItem;     // Должность штатного расписания
    private int _staffRateInteger;     // Количество штатных единиц (целое)
    private FinancingSource _financingSource;     // Источник финансирования
    private FinancingSourceItem _financingSourceItem;     // Источник финансирования (детально)
    private SalaryRaisingCoefficient _raisingCoefficient;     // Повышающий коэффициент
    private double _monthBaseSalaryFund;     // Оклад с повышающим коэффициентом
    private double _targetSalary;     // Месячный фонд оплаты труда с надбавками
    private boolean _reserve;     // Ставка зарезервирована
    private CombinationPost _combinationPost;     // Должность по совмещению
    private boolean _combination;     // Является должностью по совмещению

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сотрудник на должности.
     */
    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    /**
     * @param employeePost Сотрудник на должности.
     */
    public void setEmployeePost(EmployeePost employeePost)
    {
        dirty(_employeePost, employeePost);
        _employeePost = employeePost;
    }

    /**
     * @return Должность штатного расписания. Свойство не может быть null.
     */
    @NotNull
    public StaffListItem getStaffListItem()
    {
        return _staffListItem;
    }

    /**
     * @param staffListItem Должность штатного расписания. Свойство не может быть null.
     */
    public void setStaffListItem(StaffListItem staffListItem)
    {
        dirty(_staffListItem, staffListItem);
        _staffListItem = staffListItem;
    }

    /**
     * @return Количество штатных единиц (целое). Свойство не может быть null.
     */
    @NotNull
    public int getStaffRateInteger()
    {
        return _staffRateInteger;
    }

    /**
     * @param staffRateInteger Количество штатных единиц (целое). Свойство не может быть null.
     */
    public void setStaffRateInteger(int staffRateInteger)
    {
        dirty(_staffRateInteger, staffRateInteger);
        _staffRateInteger = staffRateInteger;
    }

    /**
     * @return Источник финансирования. Свойство не может быть null.
     */
    @NotNull
    public FinancingSource getFinancingSource()
    {
        return _financingSource;
    }

    /**
     * @param financingSource Источник финансирования. Свойство не может быть null.
     */
    public void setFinancingSource(FinancingSource financingSource)
    {
        dirty(_financingSource, financingSource);
        _financingSource = financingSource;
    }

    /**
     * @return Источник финансирования (детально).
     */
    public FinancingSourceItem getFinancingSourceItem()
    {
        return _financingSourceItem;
    }

    /**
     * @param financingSourceItem Источник финансирования (детально).
     */
    public void setFinancingSourceItem(FinancingSourceItem financingSourceItem)
    {
        dirty(_financingSourceItem, financingSourceItem);
        _financingSourceItem = financingSourceItem;
    }

    /**
     * @return Повышающий коэффициент.
     */
    public SalaryRaisingCoefficient getRaisingCoefficient()
    {
        return _raisingCoefficient;
    }

    /**
     * @param raisingCoefficient Повышающий коэффициент.
     */
    public void setRaisingCoefficient(SalaryRaisingCoefficient raisingCoefficient)
    {
        dirty(_raisingCoefficient, raisingCoefficient);
        _raisingCoefficient = raisingCoefficient;
    }

    /**
     * @return Оклад с повышающим коэффициентом. Свойство не может быть null.
     */
    @NotNull
    public double getMonthBaseSalaryFund()
    {
        return _monthBaseSalaryFund;
    }

    /**
     * @param monthBaseSalaryFund Оклад с повышающим коэффициентом. Свойство не может быть null.
     */
    public void setMonthBaseSalaryFund(double monthBaseSalaryFund)
    {
        dirty(_monthBaseSalaryFund, monthBaseSalaryFund);
        _monthBaseSalaryFund = monthBaseSalaryFund;
    }

    /**
     * @return Месячный фонд оплаты труда с надбавками. Свойство не может быть null.
     */
    @NotNull
    public double getTargetSalary()
    {
        return _targetSalary;
    }

    /**
     * @param targetSalary Месячный фонд оплаты труда с надбавками. Свойство не может быть null.
     */
    public void setTargetSalary(double targetSalary)
    {
        dirty(_targetSalary, targetSalary);
        _targetSalary = targetSalary;
    }

    /**
     * @return Ставка зарезервирована. Свойство не может быть null.
     */
    @NotNull
    public boolean isReserve()
    {
        return _reserve;
    }

    /**
     * @param reserve Ставка зарезервирована. Свойство не может быть null.
     */
    public void setReserve(boolean reserve)
    {
        dirty(_reserve, reserve);
        _reserve = reserve;
    }

    /**
     * @return Должность по совмещению.
     */
    public CombinationPost getCombinationPost()
    {
        return _combinationPost;
    }

    /**
     * @param combinationPost Должность по совмещению.
     */
    public void setCombinationPost(CombinationPost combinationPost)
    {
        dirty(_combinationPost, combinationPost);
        _combinationPost = combinationPost;
    }

    /**
     * @return Является должностью по совмещению. Свойство не может быть null.
     */
    @NotNull
    public boolean isCombination()
    {
        return _combination;
    }

    /**
     * @param combination Является должностью по совмещению. Свойство не может быть null.
     */
    public void setCombination(boolean combination)
    {
        dirty(_combination, combination);
        _combination = combination;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StaffListAllocationItemGen)
        {
            setEmployeePost(((StaffListAllocationItem)another).getEmployeePost());
            setStaffListItem(((StaffListAllocationItem)another).getStaffListItem());
            setStaffRateInteger(((StaffListAllocationItem)another).getStaffRateInteger());
            setFinancingSource(((StaffListAllocationItem)another).getFinancingSource());
            setFinancingSourceItem(((StaffListAllocationItem)another).getFinancingSourceItem());
            setRaisingCoefficient(((StaffListAllocationItem)another).getRaisingCoefficient());
            setMonthBaseSalaryFund(((StaffListAllocationItem)another).getMonthBaseSalaryFund());
            setTargetSalary(((StaffListAllocationItem)another).getTargetSalary());
            setReserve(((StaffListAllocationItem)another).isReserve());
            setCombinationPost(((StaffListAllocationItem)another).getCombinationPost());
            setCombination(((StaffListAllocationItem)another).isCombination());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StaffListAllocationItemGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StaffListAllocationItem.class;
        }

        public T newInstance()
        {
            return (T) new StaffListAllocationItem();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "employeePost":
                    return obj.getEmployeePost();
                case "staffListItem":
                    return obj.getStaffListItem();
                case "staffRateInteger":
                    return obj.getStaffRateInteger();
                case "financingSource":
                    return obj.getFinancingSource();
                case "financingSourceItem":
                    return obj.getFinancingSourceItem();
                case "raisingCoefficient":
                    return obj.getRaisingCoefficient();
                case "monthBaseSalaryFund":
                    return obj.getMonthBaseSalaryFund();
                case "targetSalary":
                    return obj.getTargetSalary();
                case "reserve":
                    return obj.isReserve();
                case "combinationPost":
                    return obj.getCombinationPost();
                case "combination":
                    return obj.isCombination();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "employeePost":
                    obj.setEmployeePost((EmployeePost) value);
                    return;
                case "staffListItem":
                    obj.setStaffListItem((StaffListItem) value);
                    return;
                case "staffRateInteger":
                    obj.setStaffRateInteger((Integer) value);
                    return;
                case "financingSource":
                    obj.setFinancingSource((FinancingSource) value);
                    return;
                case "financingSourceItem":
                    obj.setFinancingSourceItem((FinancingSourceItem) value);
                    return;
                case "raisingCoefficient":
                    obj.setRaisingCoefficient((SalaryRaisingCoefficient) value);
                    return;
                case "monthBaseSalaryFund":
                    obj.setMonthBaseSalaryFund((Double) value);
                    return;
                case "targetSalary":
                    obj.setTargetSalary((Double) value);
                    return;
                case "reserve":
                    obj.setReserve((Boolean) value);
                    return;
                case "combinationPost":
                    obj.setCombinationPost((CombinationPost) value);
                    return;
                case "combination":
                    obj.setCombination((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "employeePost":
                        return true;
                case "staffListItem":
                        return true;
                case "staffRateInteger":
                        return true;
                case "financingSource":
                        return true;
                case "financingSourceItem":
                        return true;
                case "raisingCoefficient":
                        return true;
                case "monthBaseSalaryFund":
                        return true;
                case "targetSalary":
                        return true;
                case "reserve":
                        return true;
                case "combinationPost":
                        return true;
                case "combination":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "employeePost":
                    return true;
                case "staffListItem":
                    return true;
                case "staffRateInteger":
                    return true;
                case "financingSource":
                    return true;
                case "financingSourceItem":
                    return true;
                case "raisingCoefficient":
                    return true;
                case "monthBaseSalaryFund":
                    return true;
                case "targetSalary":
                    return true;
                case "reserve":
                    return true;
                case "combinationPost":
                    return true;
                case "combination":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "employeePost":
                    return EmployeePost.class;
                case "staffListItem":
                    return StaffListItem.class;
                case "staffRateInteger":
                    return Integer.class;
                case "financingSource":
                    return FinancingSource.class;
                case "financingSourceItem":
                    return FinancingSourceItem.class;
                case "raisingCoefficient":
                    return SalaryRaisingCoefficient.class;
                case "monthBaseSalaryFund":
                    return Double.class;
                case "targetSalary":
                    return Double.class;
                case "reserve":
                    return Boolean.class;
                case "combinationPost":
                    return CombinationPost.class;
                case "combination":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StaffListAllocationItem> _dslPath = new Path<StaffListAllocationItem>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StaffListAllocationItem");
    }
            

    /**
     * @return Сотрудник на должности.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem#getEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> employeePost()
    {
        return _dslPath.employeePost();
    }

    /**
     * @return Должность штатного расписания. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem#getStaffListItem()
     */
    public static StaffListItem.Path<StaffListItem> staffListItem()
    {
        return _dslPath.staffListItem();
    }

    /**
     * @return Количество штатных единиц (целое). Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem#getStaffRateInteger()
     */
    public static PropertyPath<Integer> staffRateInteger()
    {
        return _dslPath.staffRateInteger();
    }

    /**
     * @return Источник финансирования. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem#getFinancingSource()
     */
    public static FinancingSource.Path<FinancingSource> financingSource()
    {
        return _dslPath.financingSource();
    }

    /**
     * @return Источник финансирования (детально).
     * @see ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem#getFinancingSourceItem()
     */
    public static FinancingSourceItem.Path<FinancingSourceItem> financingSourceItem()
    {
        return _dslPath.financingSourceItem();
    }

    /**
     * @return Повышающий коэффициент.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem#getRaisingCoefficient()
     */
    public static SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> raisingCoefficient()
    {
        return _dslPath.raisingCoefficient();
    }

    /**
     * @return Оклад с повышающим коэффициентом. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem#getMonthBaseSalaryFund()
     */
    public static PropertyPath<Double> monthBaseSalaryFund()
    {
        return _dslPath.monthBaseSalaryFund();
    }

    /**
     * @return Месячный фонд оплаты труда с надбавками. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem#getTargetSalary()
     */
    public static PropertyPath<Double> targetSalary()
    {
        return _dslPath.targetSalary();
    }

    /**
     * @return Ставка зарезервирована. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem#isReserve()
     */
    public static PropertyPath<Boolean> reserve()
    {
        return _dslPath.reserve();
    }

    /**
     * @return Должность по совмещению.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem#getCombinationPost()
     */
    public static CombinationPost.Path<CombinationPost> combinationPost()
    {
        return _dslPath.combinationPost();
    }

    /**
     * @return Является должностью по совмещению. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem#isCombination()
     */
    public static PropertyPath<Boolean> combination()
    {
        return _dslPath.combination();
    }

    public static class Path<E extends StaffListAllocationItem> extends EntityPath<E>
    {
        private EmployeePost.Path<EmployeePost> _employeePost;
        private StaffListItem.Path<StaffListItem> _staffListItem;
        private PropertyPath<Integer> _staffRateInteger;
        private FinancingSource.Path<FinancingSource> _financingSource;
        private FinancingSourceItem.Path<FinancingSourceItem> _financingSourceItem;
        private SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> _raisingCoefficient;
        private PropertyPath<Double> _monthBaseSalaryFund;
        private PropertyPath<Double> _targetSalary;
        private PropertyPath<Boolean> _reserve;
        private CombinationPost.Path<CombinationPost> _combinationPost;
        private PropertyPath<Boolean> _combination;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сотрудник на должности.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem#getEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> employeePost()
        {
            if(_employeePost == null )
                _employeePost = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE_POST, this);
            return _employeePost;
        }

    /**
     * @return Должность штатного расписания. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem#getStaffListItem()
     */
        public StaffListItem.Path<StaffListItem> staffListItem()
        {
            if(_staffListItem == null )
                _staffListItem = new StaffListItem.Path<StaffListItem>(L_STAFF_LIST_ITEM, this);
            return _staffListItem;
        }

    /**
     * @return Количество штатных единиц (целое). Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem#getStaffRateInteger()
     */
        public PropertyPath<Integer> staffRateInteger()
        {
            if(_staffRateInteger == null )
                _staffRateInteger = new PropertyPath<Integer>(StaffListAllocationItemGen.P_STAFF_RATE_INTEGER, this);
            return _staffRateInteger;
        }

    /**
     * @return Источник финансирования. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem#getFinancingSource()
     */
        public FinancingSource.Path<FinancingSource> financingSource()
        {
            if(_financingSource == null )
                _financingSource = new FinancingSource.Path<FinancingSource>(L_FINANCING_SOURCE, this);
            return _financingSource;
        }

    /**
     * @return Источник финансирования (детально).
     * @see ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem#getFinancingSourceItem()
     */
        public FinancingSourceItem.Path<FinancingSourceItem> financingSourceItem()
        {
            if(_financingSourceItem == null )
                _financingSourceItem = new FinancingSourceItem.Path<FinancingSourceItem>(L_FINANCING_SOURCE_ITEM, this);
            return _financingSourceItem;
        }

    /**
     * @return Повышающий коэффициент.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem#getRaisingCoefficient()
     */
        public SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> raisingCoefficient()
        {
            if(_raisingCoefficient == null )
                _raisingCoefficient = new SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient>(L_RAISING_COEFFICIENT, this);
            return _raisingCoefficient;
        }

    /**
     * @return Оклад с повышающим коэффициентом. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem#getMonthBaseSalaryFund()
     */
        public PropertyPath<Double> monthBaseSalaryFund()
        {
            if(_monthBaseSalaryFund == null )
                _monthBaseSalaryFund = new PropertyPath<Double>(StaffListAllocationItemGen.P_MONTH_BASE_SALARY_FUND, this);
            return _monthBaseSalaryFund;
        }

    /**
     * @return Месячный фонд оплаты труда с надбавками. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem#getTargetSalary()
     */
        public PropertyPath<Double> targetSalary()
        {
            if(_targetSalary == null )
                _targetSalary = new PropertyPath<Double>(StaffListAllocationItemGen.P_TARGET_SALARY, this);
            return _targetSalary;
        }

    /**
     * @return Ставка зарезервирована. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem#isReserve()
     */
        public PropertyPath<Boolean> reserve()
        {
            if(_reserve == null )
                _reserve = new PropertyPath<Boolean>(StaffListAllocationItemGen.P_RESERVE, this);
            return _reserve;
        }

    /**
     * @return Должность по совмещению.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem#getCombinationPost()
     */
        public CombinationPost.Path<CombinationPost> combinationPost()
        {
            if(_combinationPost == null )
                _combinationPost = new CombinationPost.Path<CombinationPost>(L_COMBINATION_POST, this);
            return _combinationPost;
        }

    /**
     * @return Является должностью по совмещению. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem#isCombination()
     */
        public PropertyPath<Boolean> combination()
        {
            if(_combination == null )
                _combination = new PropertyPath<Boolean>(StaffListAllocationItemGen.P_COMBINATION, this);
            return _combination;
        }

        public Class getEntityClass()
        {
            return StaffListAllocationItem.class;
        }

        public String getEntityName()
        {
            return "staffListAllocationItem";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
