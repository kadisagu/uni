/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.EmployeeListUniemp;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;

/**
 * @author dseleznev
 * Created on: 09.10.2010
 */
public class EmployeePostListWrapper extends IdentifiableWrapper<EmployeePost>
{
    private static final long serialVersionUID = 6560971823418575493L;
    public static final String P_FULL_TITLE = "fullTitle";
    public static final String P_HTML_FORMATTED_TITLE = "htmlFormattedTitle";

    private String _htmlFormattedTitle;
    private String _fullTitle;
    private String _postTitle;
    private String _orgUnitTitle;
    private String _orgUnitTypeTitle;
    private String _employeeTypeShortTitle;
    private String _qualifLevelShortTitle;
    private String _profQualifGroupShortTitle;
    private String _etksLevelTitle;
    private String _postTypeShortTitle;
    private String _postStatusShortTitle;
    private Boolean _combinationPost;
    
    public EmployeePostListWrapper(Long id, String title)
    {
        super(id, title);
    }

    public EmployeePostListWrapper(Long id, String postTitle, String orgUnitTitle, String orgUnitTypeTitle, String employeeTypeShortTitle, String qualifLevelShortTitle, String profQualifGroupShortTitle, String etksLevelTitle, String postTypeShortTitle, String postStatusShortTitle, boolean isCombinationPost)
    {
        super(id, postTitle);

        _postTitle = postTitle;
        _orgUnitTitle = orgUnitTitle;
        _orgUnitTypeTitle = orgUnitTypeTitle;
        _employeeTypeShortTitle = employeeTypeShortTitle;
        _qualifLevelShortTitle = qualifLevelShortTitle;
        _profQualifGroupShortTitle = profQualifGroupShortTitle;
        _etksLevelTitle = etksLevelTitle;
        _postTypeShortTitle = postTypeShortTitle;
        _postStatusShortTitle = postStatusShortTitle;
        _combinationPost = isCombinationPost;

        StringBuilder titleBuilder = new StringBuilder();
        if (isCombinationPost)
        {
            titleBuilder.append(postTitle).append(" (");
            titleBuilder.append(employeeTypeShortTitle).append(", КУ-").append(qualifLevelShortTitle);
            titleBuilder.append(", ПКГ-").append(profQualifGroupShortTitle);
            titleBuilder.append(") : ");
            titleBuilder.append(orgUnitTitle).append(" (").append(orgUnitTypeTitle).append(") - ");
            titleBuilder.append(postTypeShortTitle);

            StringBuilder formattedBuilder = new StringBuilder();
            formattedBuilder.append("<div style=\"padding-left:25px\">");/*font-size:12px;*/
            formattedBuilder.append(postTitle).append(" (");
            formattedBuilder.append(employeeTypeShortTitle).append(", КУ-").append(qualifLevelShortTitle);
            formattedBuilder.append(", ПКГ-").append(profQualifGroupShortTitle);
            formattedBuilder.append(") : ");
            formattedBuilder.append(orgUnitTitle).append(" (").append(orgUnitTypeTitle).append(") - ");
            formattedBuilder.append(postTypeShortTitle);
            formattedBuilder.append("</div>");
            _htmlFormattedTitle = formattedBuilder.toString();
        }
        else
        {
            titleBuilder.append(postTitle).append(" (");
            titleBuilder.append(employeeTypeShortTitle).append(", КУ-").append(qualifLevelShortTitle);
            titleBuilder.append(", ПКГ-").append(profQualifGroupShortTitle).append(null != etksLevelTitle ? ", Р-" : "");
            titleBuilder.append(null != etksLevelTitle ? etksLevelTitle : "").append(") : ");
            titleBuilder.append(orgUnitTitle).append(" (").append(orgUnitTypeTitle).append(") - ");
            titleBuilder.append(postTypeShortTitle).append(" ").append(postStatusShortTitle);
            _htmlFormattedTitle = titleBuilder.toString();
        }
        _fullTitle = titleBuilder.toString();
    }

    public String getHtmlFormattedTitle()
    {
        return _htmlFormattedTitle;
    }

    public void setHtmlFormattedTitle(String htmlFormattedTitle)
    {
        _htmlFormattedTitle = htmlFormattedTitle;
    }

    public Boolean getCombinationPost()
    {
        return _combinationPost;
    }

    public void setCombinationPost(Boolean combinationPost)
    {
        _combinationPost = combinationPost;
    }

    public String getFullTitle()
    {
        return _fullTitle;
    }

    public String getPostTitle()
    {
        return _postTitle;
    }

    public String getOrgUnitTitle()
    {
        return _orgUnitTitle;
    }

    public void setOrgUnitTitle(String orgUnitTitle)
    {
        this._orgUnitTitle = orgUnitTitle;
    }

    public String getOrgUnitTypeTitle()
    {
        return _orgUnitTypeTitle;
    }

    public void setOrgUnitTypeTitle(String orgUnitTypeTitle)
    {
        this._orgUnitTypeTitle = orgUnitTypeTitle;
    }

    public String getEmployeeTypeShortTitle()
    {
        return _employeeTypeShortTitle;
    }

    public String getQualifLevelShortTitle()
    {
        return _qualifLevelShortTitle;
    }

    public String getProfQualifGroupShortTitle()
    {
        return _profQualifGroupShortTitle;
    }

    public String getEtksLevelTitle()
    {
        return _etksLevelTitle;
    }

    public String getPostTypeShortTitle()
    {
        return _postTypeShortTitle;
    }

    public String getPostStatusShortTitle()
    {
        return _postStatusShortTitle;
    }
}