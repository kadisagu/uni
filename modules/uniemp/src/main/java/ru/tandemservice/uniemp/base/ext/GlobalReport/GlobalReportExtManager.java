/* $Id$ */
package ru.tandemservice.uniemp.base.ext.GlobalReport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.IItemListExtensionBuilder;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.GlobalReportManager;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.logic.GlobalReportDefinition;
import org.tandemframework.shared.person.base.bo.ReportPerson.ui.Add.ReportPersonAdd;
import ru.tandemservice.uniemp.base.bo.EmpMedicalStuffReport.ui.Add.EmpMedicalStuffReportAdd;
import ru.tandemservice.uniemp.base.bo.EmpServiceLengthReport.ui.Add.EmpServiceLengthReportAdd;
import ru.tandemservice.uniemp.base.bo.EmpSummaryPPSReport.ui.Add.EmpSummaryPPSReportAdd;
import ru.tandemservice.uniemp.base.ext.GlobalReport.ui.List.UniempGlobalReportListAddon;

/**
 * @author azhebko
 * @since 27.03.2014
 */
@Configuration
public class GlobalReportExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private GlobalReportManager _globalReportManager;

    @Bean
    public ItemListExtension<GlobalReportDefinition> reportListExtension()
    {
        IItemListExtensionBuilder<GlobalReportDefinition> itemListExtension = itemListExtension(_globalReportManager.reportListExtPoint());
        itemListExtension
                .add("employeeSampleReport", new GlobalReportDefinition("employee", "employeeSample", ReportPersonAdd.class.getSimpleName(), "employeeSampleParamMap", UniempGlobalReportListAddon.NAME))
                .add("profLecStaffConsistencyReport", new GlobalReportDefinition("employee", "profLecStaffConsistency", ru.tandemservice.uniemp.component.reports.profLectStaffConsistency.Add.Model.class.getPackage().getName()))
                .add("qualityConsistencyReport", new GlobalReportDefinition("employee", "qualityConsistency", ru.tandemservice.uniemp.component.reports.qualityConsistency.Add.Model.class.getPackage().getName()))
                .add("qualityConsistencyFullReport", new GlobalReportDefinition("employee", "qualityConsistencyFull", ru.tandemservice.uniemp.component.reports.qualityConsistencyFull.Add.Model.class.getPackage().getName()))
                .add("qualityQuantityConsistencyFullReport", new GlobalReportDefinition("employee", "qualityQuantityConsistencyFull", ru.tandemservice.uniemp.component.reports.qualityQuantityConsistencyFull.Add.Model.class.getPackage().getName()))
                .add("ppsAllocationReport", new GlobalReportDefinition("employee", "ppsAllocation", ru.tandemservice.uniemp.component.reports.ppsAllocation.Add.Model.class.getPackage().getName()))
                .add("employeeMovePPSReport", new GlobalReportDefinition("employee", "employeeMovePPS", ru.tandemservice.uniemp.component.reports.employeeMovePPS.Add.Model.class.getPackage().getName()))
                .add("qualityConsistencyForListReport", new GlobalReportDefinition("employee", "qualityConsistencyForList", ru.tandemservice.uniemp.component.reports.qualityConsistencyForList.Add.Model.class.getPackage().getName()))
                .add("quantityExtractFormP4Report", new GlobalReportDefinition("employee", "quantityExtractFormP4", ru.tandemservice.uniemp.component.reports.quantityExtractFormP4.Add.Model.class.getPackage().getName()))
                .add("employeeVPO1Report", new GlobalReportDefinition("employee", "employeeVPO1Report", ru.tandemservice.uniemp.component.reports.employeeVPO.List.Model.class.getPackage().getName()))
                .add("quantityEmployeeByDepartmentsReport", new GlobalReportDefinition("employee", "quantityEmployeeByDepartments", ru.tandemservice.uniemp.component.reports.quantityEmployeeByDepartments.quantityEmployeeByDepartmentsList.Model.class.getPackage().getName()))
                .add("empMedicalStuffReport", new GlobalReportDefinition("employee", "empMedicalStuffReport", EmpMedicalStuffReportAdd.class.getSimpleName()))
                .add("empSummaryPPSReport", new GlobalReportDefinition("employee", "empSummaryPPSReport", EmpSummaryPPSReportAdd.class.getSimpleName()))
                .add("empServiceLengthReport", new GlobalReportDefinition("employee", "empServiceLengthReport", EmpServiceLengthReportAdd.class.getSimpleName()));

        if (Boolean.valueOf(ApplicationRuntime.getProperty("employeeReport.staffList.enabled")))
            itemListExtension.add("staffListReport", new GlobalReportDefinition("employee", "staffList", ru.tandemservice.uniemp.component.reports.staffList.List.Model.class.getPackage().getName()));

        if (Boolean.valueOf(ApplicationRuntime.getProperty("employeeReport.staffListAllocation.enabled")))
            itemListExtension.add("staffListAllocationReport", new GlobalReportDefinition("employee", "staffListAllocation", ru.tandemservice.uniemp.component.reports.staffListAllocation.List.Model.class.getPackage().getName()));

        return itemListExtension.create();
    }
}