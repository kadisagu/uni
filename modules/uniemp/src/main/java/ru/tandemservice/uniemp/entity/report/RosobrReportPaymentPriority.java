package ru.tandemservice.uniemp.entity.report;

import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.report.gen.RosobrReportPaymentPriorityGen;

/**
 * Приоритет следования выплат в отчете
 */
public class RosobrReportPaymentPriority extends RosobrReportPaymentPriorityGen
{
    public RosobrReportPaymentPriority()
    {
        super();
    }
    
    public RosobrReportPaymentPriority(Payment payment)
    {
        setPayment(payment);
    }
}