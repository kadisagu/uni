/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.ppsAllocation.Add;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author dseleznev
 * Created on: 20.04.2009
 */
public interface IDAO extends IUniDao<Model>
{
    Integer preparePrintReport(Model model);
}