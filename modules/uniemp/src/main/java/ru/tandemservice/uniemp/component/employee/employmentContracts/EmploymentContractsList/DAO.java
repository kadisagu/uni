/**
 *$Id$
 */
package ru.tandemservice.uniemp.component.employee.employmentContracts.EmploymentContractsList;

import com.google.common.collect.ImmutableList;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitAutocompleteModel;
import org.tandemframework.tapsupport.component.selection.CommonMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.dao.UniempDAO;
import ru.tandemservice.uniemp.entity.catalog.LabourContractType;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.upper;

/**
 * Create by ashaburov
 * Date 27.07.11
 */
public class DAO extends UniempDAO<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        //заполняем модель фильтра Срок окончания ТД
        String[] appendStr = new String[] {"1 день", "2 дня", "3 дня", "4 дня", "5 дней", "6 дней", "1 неделя", "2 недели", "3 недели", "месяц"};
        List<IdentifiableWrapper> deadlineList = new ArrayList<>();
        for (long i = 1; i < 11; i++)
        {
            IdentifiableWrapper wrapper = new IdentifiableWrapper(i, appendStr[(int)i - 1]);
            deadlineList.add(wrapper);
        }
        model.setDeadlineListFilter(deadlineList);

        //заполняем модель фильтра Тип трудового договора
        model.setContractTypeListFilter(getCatalogItemList(LabourContractType.class));

        //заполняем модель фильтра Подразделение
        model.setOrgUnitModelFilter(new OrgUnitAutocompleteModel());

        //заполняем модель фильтра Должность
        model.setPostRelationModelFilter(new UniQueryFullCheckSelectModel(PostBoundedWithQGandQL.fullTitle().s())
        {
            @Override
            protected MQBuilder query(String alias, String filter)
            {
                MQBuilder builder = new MQBuilder(PostBoundedWithQGandQL.ENTITY_CLASS, alias);
                MQBuilder subBuilder = new MQBuilder(OrgUnitTypePostRelation.ENTITY_CLASS, "rel", new String[] { OrgUnitTypePostRelation.postBoundedWithQGandQL().id().s() });
                if (null != filter)
                {
                    subBuilder.add(MQExpression.like("rel", OrgUnitTypePostRelation.postBoundedWithQGandQL().title().s(), CoreStringUtils.escapeLike(filter)));
                }

                Object orgUnitFilter = model.getOrgUnitItemFilter();
                if (null != orgUnitFilter)
                {
                    subBuilder.add(MQExpression.eq("rel", OrgUnitTypePostRelation.orgUnitType().s(), ((OrgUnit)orgUnitFilter).getOrgUnitType()));
                }
                subBuilder.setNeedDistinct(true);
                builder.add(MQExpression.in(alias, "id", subBuilder));
                builder.addOrder(alias, PostBoundedWithQGandQL.title().s());
                return builder;
            }
        });

        //заполняем модель фильтра Тип назначения
        model.setPostTypeListFilter(getCatalogItemList(PostType.class));

        // модель фильтра архивности
        model.setArchiveModel(TwinComboDataSourceHandler.getYesNoDefaultSelectModel());
        if (model.getArchiveFilter() == null)
            model.setArchiveFilter(TwinComboDataSourceHandler.getNoOption());

        // модель фильтра Состояние должности сотрудника
        model.setPostStatusModel(new CommonMultiSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmployeePostStatus.class, "b").column(property("b"));

                if (set != null)
                {
                    builder.where(in(property(EmployeePostStatus.id().fromAlias("b")), set));
                    return new DQLListResultBuilder(builder, 50);
                }

                if (!StringUtils.isEmpty(filter))
                    builder.where(like(upper(property(EmployeePostStatus.title().fromAlias("b"))), value(CoreStringUtils.escapeLike(filter, true))));

                return new DQLListResultBuilder(builder, 50);
            }
        });

        //заполняем мапу с промежутками времени для фильтра Срок окончания ТД
        for (long i = 1; i < 11; i++)
        {
            if (i <= 6)
            {
                Calendar date = GregorianCalendar.getInstance();
                date.setTime(new Date());
                date.add(Calendar.DAY_OF_YEAR, (int)i);
                model.getId2TimeMap().put(i, date.getTime());
            }
            else if (i >= 7 && i <= 9)
            {
                Calendar date = GregorianCalendar.getInstance();
                date.setTime(new Date());
                date.add(Calendar.WEEK_OF_YEAR, (int)i - 6);
                model.getId2TimeMap().put(i, date.getTime());
            }
            else if (i == 10)
            {
                Calendar date = GregorianCalendar.getInstance();
                date.setTime(new Date());
                date.add(Calendar.MONTH, 1);
                model.getId2TimeMap().put(i, date.getTime());
            }
        }
    }

    @Override
    public void prepareListDataSource(final Model model)
    {
        MQBuilder builder = new MQBuilder(EmployeeLabourContract.ENTITY_CLASS, "b");

        //Применяем фильтры
        if (model.getDeadlineItemFilter() != null)
        {
            builder.add(MQExpression.lessOrEq("b", EmployeeLabourContract.P_END_DATE, model.getId2TimeMap().get(model.getDeadlineItemFilter().getId())));
            builder.add(MQExpression.greatOrEq("b", EmployeeLabourContract.P_END_DATE, new Date()));
        }
        if (model.getContractNumberFilter() != null)
            builder.add(MQExpression.like("b", EmployeeLabourContract.P_NUMBER, model.getContractNumberFilter()));
        if (model.getContractTypeItemFilter() != null)
            builder.add(MQExpression.eq("b", EmployeeLabourContract.L_TYPE, model.getContractTypeItemFilter()));
        if (model.getDateFromFilter() != null)
            builder.add(MQExpression.greatOrEq("b", EmployeeLabourContract.P_DATE, model.getDateFromFilter()));
        if (model.getDateToFilter() != null)
            builder.add(MQExpression.lessOrEq("b", EmployeeLabourContract.P_DATE, model.getDateToFilter()));
        if (model.getEndDateFromFilter() != null && model.getDeadlineItemFilter() == null)
            builder.add(MQExpression.greatOrEq("b", EmployeeLabourContract.P_END_DATE, model.getEndDateFromFilter()));
        if (model.getEndDateToFilter() != null && model.getDeadlineItemFilter() == null)
            builder.add(MQExpression.lessOrEq("b", EmployeeLabourContract.P_END_DATE, model.getEndDateToFilter()));
        if (model.getFirstNameFilter() != null)
            builder.add(MQExpression.like("b", EmployeeLabourContract.employeePost().person().identityCard().firstName().s(), model.getFirstNameFilter()));
        if (model.getMiddleNameFilter() != null)
            builder.add(MQExpression.like("b", EmployeeLabourContract.employeePost().person().identityCard().middleName().s(), model.getMiddleNameFilter()));
        if (model.getLastNameFilter() != null)
            builder.add(MQExpression.like("b", EmployeeLabourContract.employeePost().person().identityCard().lastName().s(), model.getLastNameFilter()));
        if (model.getOrgUnitItemFilter() != null)
        {
            if (model.isDecompositionOrgUnitFilter())
            {
                List<OrgUnit> childOrgUnitList = OrgUnitManager.instance().dao().getChildOrgUnits(model.getOrgUnitItemFilter(), true);
                childOrgUnitList.add(model.getOrgUnitItemFilter());
                AbstractExpression[] expressions = childOrgUnitList.stream()
                    .map(orgUnit -> MQExpression.eq("b", EmployeeLabourContract.employeePost().orgUnit().s(), orgUnit))
                    .toArray(AbstractExpression[]::new);
                builder.add(MQExpression.or(expressions));
            }
            else
                builder.add(MQExpression.eq("b", EmployeeLabourContract.employeePost().orgUnit().s(), model.getOrgUnitItemFilter()));
        }
        if (model.getPostRelationItemFilter() != null)
            builder.add(MQExpression.eq("b", EmployeeLabourContract.employeePost().postRelation().postBoundedWithQGandQL().s(), model.getPostRelationItemFilter()));
        if (model.getPostTypeItemFilter() != null)
            builder.add(MQExpression.eq("b", EmployeeLabourContract.employeePost().postType().s(), model.getPostTypeItemFilter()));
        if (model.getArchiveFilter() != null)
            builder.add(MQExpression.eq("b", EmployeeLabourContract.employeePost().employee().archival().s(), TwinComboDataSourceHandler.getSelectedValue(model.getArchiveFilter().getId())));
        if (model.getPostStatusListFilter() != null && !model.getPostStatusListFilter().isEmpty())
            builder.add(MQExpression.in("b", EmployeeLabourContract.employeePost().postStatus().s(), model.getPostStatusListFilter()));

        //приминяем сортировку
        final DynamicListDataSource<EmployeeLabourContract> dataSource = model.getDataSource();
        if (EmployeeLabourContract.PERSON_FIO_KEY.equals(dataSource.getEntityOrder().getKey()))
            builder.addOrder("b", EmployeeLabourContract.employeePost().person().identityCard().fullFio().s(), dataSource.getEntityOrder().getDirection());
        else if (EmployeeLabourContract.POST_TITLE_KEY.equals(dataSource.getEntityOrder().getKey()))
            builder.addOrder("b", EmployeeLabourContract.employeePost().postRelation().postBoundedWithQGandQL().title().s(), dataSource.getEntityOrder().getDirection());
        else if (EmployeeLabourContract.ORGUNIT_TITLE_KEY.equals(dataSource.getEntityOrder().getKey()))
            builder.addOrder("b", EmployeeLabourContract.employeePost().orgUnit().title().s(), dataSource.getEntityOrder().getDirection());
        else if (EmployeeLabourContract.POST_TYPE_TITLE_KEY.equals(dataSource.getEntityOrder().getKey()))
            builder.addOrder("b", EmployeeLabourContract.employeePost().postType().title().s(), dataSource.getEntityOrder().getDirection());
        else if (EmployeeLabourContract.NUMBER_KEY.equals(dataSource.getEntityOrder().getKey()))
            builder.addOrder("b", EmployeeLabourContract.number().s(), dataSource.getEntityOrder().getDirection());
        else if (EmployeeLabourContract.DATE_KEY.equals(dataSource.getEntityOrder().getKey()))
            builder.addOrder("b", EmployeeLabourContract.date().s(), dataSource.getEntityOrder().getDirection());
        else if (EmployeeLabourContract.BEGIN_DATE_KEY.equals(dataSource.getEntityOrder().getKey()))
            builder.addOrder("b", EmployeeLabourContract.beginDate().s(), dataSource.getEntityOrder().getDirection());
        else if (EmployeeLabourContract.END_DATE_KEY.equals(dataSource.getEntityOrder().getKey()))
            builder.addOrder("b", EmployeeLabourContract.endDate().s(), dataSource.getEntityOrder().getDirection());
        else if (EmployeeLabourContract.TYPE_KEY.equals(dataSource.getEntityOrder().getKey()))
            builder.addOrder("b", EmployeeLabourContract.type().title().s(), dataSource.getEntityOrder().getDirection());


        UniBaseUtils.createPage(model.getDataSource(), builder.<EmployeeLabourContract>getResultList(getSession()));

        for (ViewWrapper wrapper : ViewWrapper.<EmployeeLabourContract>getPatchedList(model.getDataSource()))
        {
            boolean result = ((EmployeeLabourContract) wrapper.getEntity()).getEndDate() == null;

            wrapper.setViewProperty(Model.DISABLED_PRINT_PROPERTY, result);
        }
    }
}
