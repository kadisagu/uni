/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffList.StaffListPub;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.Post;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.catalog.PaymentType;
import ru.tandemservice.uniemp.entity.employee.*;
import ru.tandemservice.uniemp.entity.report.StaffListRepPaymentSettings;
import ru.tandemservice.uniemp.util.StaffListPaymentsUtil;

import java.util.*;

/**
 * @author dseleznev
 * Created on: 19.09.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("rel");
    //private static final OrderDescriptionRegistry _paymOrderSettings = new OrderDescriptionRegistry("rel");
    //private static final OrderDescriptionRegistry _allocOrderSettings = new OrderDescriptionRegistry("rel");
    private static final OrderDescriptionRegistry _empHROrderSettings = new OrderDescriptionRegistry("rel");

    private class EmployeeHRWrapperComparator implements Comparator<EmployeeHRLineWrapper>
    {
        private OrderDirection _direction = OrderDirection.asc;
        private String _sortProperty = EmployeeHRLineWrapper.P_POST_TITLE;

        public EmployeeHRWrapperComparator(OrderDirection direction, String sortProperty)
        {
            _direction = direction;
            _sortProperty = sortProperty;
        }

        @Override
        public int compare(EmployeeHRLineWrapper o1, EmployeeHRLineWrapper o2)
        {
            String o1prop = (String) o1.getProperty(_sortProperty);
            String o2prop = (String) o2.getProperty(_sortProperty);

            if(null == o1prop && null != o2prop) return 1;
            else if(null != o1prop && null == o2prop) return -1;
            else if(null == o1prop) return 0;

            return OrderDirection.asc == _direction ? o1prop.compareToIgnoreCase(o2prop) : o2prop.compareToIgnoreCase(o1prop);
        }
    }

    @Override
    public void prepare(final Model model)
    {
        model.setStaffList(getNotNull(StaffList.class, model.getPublisherId()));
        model.setPaymentsList(getUsedOrActivePaymentsList(model.getStaffList()));
        model.setCompensPaymentsList(getUsedOrActiveCompensPaymentsList(model.getPaymentsList()));
        model.setEmployeeTypesList(HierarchyUtil.listHierarchyNodesWithParents(getList(EmployeeType.class), true));
        model.setStaffListEmpty(StaffListPaymentsUtil.isStaffListEmpty(model.getStaffList(), getSession()));

        model.setPostsList(new BaseSingleSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(OrgUnitPostRelation.ENTITY_CLASS, "rel");
                builder.add(MQExpression.eq("rel", OrgUnitPostRelation.L_ORG_UNIT, model.getStaffList().getOrgUnit()));

                if (null != model.getEmployeeTypeFilter())
                {
                    builder.add(MQExpression.eq("rel", OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_POST + "." + Post.L_EMPLOYEE_TYPE, model.getEmployeeTypeFilter()));
                }
                if (filter != null)
                {
                    builder.add(MQExpression.like("rel", OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_POST + "." + Post.P_TITLE, "%" + filter));
                }
                return new ListResult<>(builder.getResultList(getSession()));
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                IEntity entity = get(OrgUnitPostRelation.class, (Long)primaryKey);
                if (findValues("").getObjects().contains(entity))
                {
                    return entity;
                }
                return null;
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((OrgUnitPostRelation)value).getTitleWithSalary();
            }
        });

        model.setHrPostsList(new BaseSingleSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(OrgUnitPostRelation.ENTITY_CLASS, "rel");
                builder.add(MQExpression.eq("rel", OrgUnitPostRelation.L_ORG_UNIT, model.getStaffList().getOrgUnit()));

                if (null != model.getHrEmployeeTypeFilter())
                {
                    builder.add(MQExpression.eq("rel", OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_POST + "." + Post.L_EMPLOYEE_TYPE, model.getHrEmployeeTypeFilter()));
                }
                if (filter != null)
                {
                    builder.add(MQExpression.like("rel", OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_POST + "." + Post.P_TITLE, "%" + filter));
                }
                return new ListResult<>(builder.getResultList(getSession()));
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                IEntity entity = get(OrgUnitPostRelation.class, (Long)primaryKey);
                if (findValues("").getObjects().contains(entity))
                {
                    return entity;
                }
                return null;
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((OrgUnitPostRelation)value).getTitleWithSalary();
            }
        });
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(StaffListItem.ENTITY_CLASS, "rel");
        builder.add(MQExpression.eq("rel", StaffListItem.L_STAFF_LIST, model.getStaffList()));
        _orderSettings.applyOrder(builder, model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }

    @Override
    public void prepareStaffListPaymentsDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(StaffListItem.ENTITY_CLASS, "rel");
        builder.add(MQExpression.eq("rel", StaffListItem.L_STAFF_LIST, model.getStaffList()));
//        _paymOrderSettings.applyOrder(builder, model.getPaymentsDataSource().getEntityOrder());

        //сортировка
        final DynamicListDataSource<PaymentWrapper> dataSource = model.getPaymentsDataSource();
        if (PaymentWrapper.P_POST_TITLE.equals(dataSource.getEntityOrder().getKey()))
            builder.addOrder("rel", StaffListItem.orgUnitPostRelation().orgUnitTypePostRelation().postBoundedWithQGandQL().title().s(), dataSource.getEntityOrder().getDirection());
        else if (PaymentWrapper.P_POST_TYPE.equals(dataSource.getEntityOrder().getKey()))
            builder.addOrder("rel", StaffListItem.orgUnitPostRelation().orgUnitTypePostRelation().postBoundedWithQGandQL().post().employeeType().shortTitle().s(), dataSource.getEntityOrder().getDirection());
        else if (PaymentWrapper.P_PROF_QUALIFICATION_GROUP.equals(dataSource.getEntityOrder().getKey()))
            builder.addOrder("rel", StaffListItem.orgUnitPostRelation().orgUnitTypePostRelation().postBoundedWithQGandQL().profQualificationGroup().shortTitle().s(), dataSource.getEntityOrder().getDirection());
        else if (PaymentWrapper.P_QUALIFICATION_LEVEL.equals(dataSource.getEntityOrder().getKey()))
            builder.addOrder("rel", StaffListItem.orgUnitPostRelation().orgUnitTypePostRelation().postBoundedWithQGandQL().qualificationLevel().shortTitle().s(), dataSource.getEntityOrder().getDirection());
        else if (PaymentWrapper.P_FINANCING_SOURCE.equals(dataSource.getEntityOrder().getKey()))
            builder.addOrder("rel", StaffListItem.financingSource().title().s(), dataSource.getEntityOrder().getDirection());
//        else if ("FINANCING_SOURCE_ITEM".equals(dataSource.getEntityOrder().getKey()))
//            builder.addOrder("rel", StaffListItem.financingSourceItem().title().s(), dataSource.getEntityOrder().getDirection());

        List<StaffListItem> staffListItemList = builder.getResultList(getSession());

        //Map<Long, StaffListItem> idStaffListItemMap = new HashMap<Long, StaffListItem>();
        //for (StaffListItem staffListItem : staffListItemList)
        //    idStaffListItemMap.put(staffListItem.getId(), staffListItem);

        List<StaffListPostPayment> paymentsList = UniempDaoFacade.getStaffListDAO().getStaffListPostPaymentsList(model.getStaffList());
        for (StaffListPostPayment payment : paymentsList)
            payment.setTargetAllocItem(null);

        Map<StaffListItem, Map<CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem>, List<StaffListPostPayment>>> itemPaymentsMap = new HashMap<>();
        for (StaffListPostPayment payment : paymentsList)
        {
            CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem> payKey = new CoreCollectionUtils.Pair<>(payment.getFinancingSource(), payment.getFinancingSourceItem());
            StaffListItem item = payment.getStaffListItem();

            Map<CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem>, List<StaffListPostPayment>> pairListMap = itemPaymentsMap.get(item);
            if (pairListMap == null)
                pairListMap = new HashMap<>();

            List<StaffListPostPayment> list = pairListMap.get(payKey);
            if (list == null)
                list = new ArrayList<>();

            list.add(payment);

            pairListMap.put(payKey, list);
            itemPaymentsMap.put(item, pairListMap);
        }

        long id = -1;

        PaymentWrapper totalWrapper = new PaymentWrapper(id--, "ИТОГО");
        totalWrapper.setTotal(true);
        totalWrapper.setBold(true);
        Map<FinancingSource, PaymentWrapper> finSrcPaymentMap = new HashMap<>();
        for (FinancingSource finSrc : getCatalogItemList(FinancingSource.class))
        {
            PaymentWrapper wrapper = new PaymentWrapper(id--, "Итого");
            wrapper.setFinancingSource(finSrc);
            wrapper.setBold(true);
            wrapper.setFinSrcTotal(true);

            finSrcPaymentMap.put(finSrc, wrapper);
        }

        List<PaymentWrapper> wrapperList = new ArrayList<>();
        for (StaffListItem staffListItem : staffListItemList)
        {
            PaymentWrapper paymentWrapper = new PaymentWrapper(id--, staffListItem.getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL().getTitle());
            paymentWrapper.setStaffListItem(staffListItem);
            paymentWrapper.setFinancingSource(staffListItem.getFinancingSource());
            paymentWrapper.setFinancingSourceItem(staffListItem.getFinancingSourceItem());
            wrapperList.add(paymentWrapper);

            if (itemPaymentsMap.get(staffListItem) != null)
                for (Map.Entry<CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem>, List<StaffListPostPayment>> entry : itemPaymentsMap.get(staffListItem).entrySet())
                {
                    if (paymentWrapper.equalsFinSrs(entry.getKey().getX(), entry.getKey().getY()))
                    {
                        for (StaffListPostPayment payment : entry.getValue())
                        {
                            paymentWrapper.putPaymentValue(payment.getPayment().getCode(), payment.getTotalValue());

                            PaymentWrapper wrapper = finSrcPaymentMap.get(payment.getFinancingSource());
                            wrapper.putPaymentValue(payment.getPayment().getCode(), payment.getTotalValue());
                            totalWrapper.putPaymentValue(payment.getPayment().getCode(), payment.getTotalValue());
                        }
                    }
                    else
                    {
                        PaymentWrapper innerWrapper = new PaymentWrapper(id--, staffListItem.getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL().getTitle());
                        innerWrapper.setFinancingSource(entry.getKey().getX());
                        innerWrapper.setFinancingSourceItem(entry.getKey().getY());
                        innerWrapper.setStaffListItem(staffListItem);
                        innerWrapper.setFinSrc(true);

                        for (StaffListPostPayment payment : entry.getValue())
                        {
                            innerWrapper.putPaymentValue(payment.getPayment().getCode(), payment.getTotalValue());

                            PaymentWrapper wrapper = finSrcPaymentMap.get(payment.getFinancingSource());
                            wrapper.putPaymentValue(payment.getPayment().getCode(), payment.getTotalValue());
                            totalWrapper.putPaymentValue(payment.getPayment().getCode(), payment.getTotalValue());
                        }

                        wrapperList.add(innerWrapper);
                    }
                }
        }

        for (PaymentWrapper paymentWrapper : wrapperList)
        {
            FinancingSource key = paymentWrapper.getFinancingSource();

            PaymentWrapper wrapper = finSrcPaymentMap.get(key);

            wrapper.setTotalTargetSalary(wrapper.getTotalTargetSalary() + paymentWrapper.getTargetSalary());
            wrapper.setTotalMonthBaseSalary(wrapper.getTotalMonthBaseSalary() + paymentWrapper.getMonthBaseSalary());

            totalWrapper.setTotalTargetSalary(totalWrapper.getTotalTargetSalary() + paymentWrapper.getTargetSalary());
            totalWrapper.setTotalMonthBaseSalary(totalWrapper.getTotalMonthBaseSalary() + paymentWrapper.getMonthBaseSalary());
        }

        for (StaffListItem item : staffListItemList)
        {
            FinancingSource key = item.getFinancingSource();

            PaymentWrapper wrapper = finSrcPaymentMap.get(key);

            wrapper.setTotalStaffRate(wrapper.getTotalStaffRate() + item.getStaffRate());

            totalWrapper.setTotalStaffRate(totalWrapper.getTotalStaffRate() + item.getStaffRate());
        }

        for (Map.Entry<FinancingSource, PaymentWrapper> entry : finSrcPaymentMap.entrySet())
            wrapperList.add(entry.getValue());

        wrapperList.add(totalWrapper);

        model.getPaymentsDataSource().setCountRow(wrapperList.size());
        UniBaseUtils.createPage(model.getPaymentsDataSource(), wrapperList);
    }

    @Override
    public void prepareStaffListTotalPaymentsDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(StaffListItem.ENTITY_CLASS, "rel");
        builder.add(MQExpression.eq("rel", StaffListItem.L_STAFF_LIST, model.getStaffList()));
        List<StaffListItem> staffListItemList = builder.getResultList(getSession());

        List<IdentifiableWrapper> wrapperList = new ArrayList<>();

        List<Payment> payments = model.getPaymentsList();
        List<StaffListPostPayment> paymentsList = UniempDaoFacade.getStaffListDAO().getStaffListPostPaymentsList(model.getStaffList());
        Map<Long, Map<String, StaffListPostPayment>> id2payments = new HashMap<>();
        Map<FinancingSource, Map<String, Double>> finSrcPaymentsMap = new LinkedHashMap<>();
        Map<CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem>, Map<String, Double>> finSrcItmPaymentsMap = new LinkedHashMap<>();
        Map<String, Double> totalPaymentValueMap = new LinkedHashMap<>();
        for (StaffListPostPayment item : paymentsList)
        {
            {
                FinancingSource finSrcKey = item.getFinancingSource();
                String codeKey = item.getPayment().getCode();
                Map<String, Double> codePaymentMap = finSrcPaymentsMap.get(finSrcKey);
                if (codePaymentMap == null)
                    codePaymentMap = new LinkedHashMap<>();
                Double totalValue = codePaymentMap.get(codeKey);
                if (totalValue == null)
                    totalValue = 0d;
                codePaymentMap.put(codeKey, totalValue += item.getTotalValue());
                finSrcPaymentsMap.put(finSrcKey, codePaymentMap);
            }

            if (item.getFinancingSourceItem() != null)
            {
                CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem> finSrcPairKey = new CoreCollectionUtils.Pair<>(item.getFinancingSource(), item.getFinancingSourceItem());
                String codeKey = item.getPayment().getCode();
                Map<String, Double> codePaymentMap = finSrcItmPaymentsMap.get(finSrcPairKey);
                if (codePaymentMap == null)
                    codePaymentMap = new LinkedHashMap<>();
                Double totalValue = codePaymentMap.get(codeKey);
                if (totalValue == null)
                    totalValue = 0d;
                codePaymentMap.put(codeKey, totalValue += item.getTotalValue());
                finSrcItmPaymentsMap.put(finSrcPairKey, codePaymentMap);
            }

            String codeKey = item.getPayment().getCode();
            Double totalValue = totalPaymentValueMap.get(codeKey);
            if (totalValue == null)
                totalValue = 0.0d;
            totalPaymentValueMap.put(codeKey, totalValue + item.getTotalValue());

            Long id = item.getStaffListItem().getId();
            Map<String, StaffListPostPayment> paymentsMap = id2payments.get(id);
            if (null == paymentsMap)
            {
                paymentsMap = new HashMap<>();
            }
            paymentsMap.put(item.getPayment().getCode(), item);
            id2payments.put(id, paymentsMap);
        }

        CoreCollectionUtils.Pair<Double, Double> totalSalary = new CoreCollectionUtils.Pair<>(0d, 0d);
        Map<FinancingSource, Double> finSrcMonthSalaryMap = new LinkedHashMap<>();
        Map<FinancingSource, Double> finSrcTargetSalaryMap = new LinkedHashMap<>();
        Map<CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem>, Double> finSrcItmMonthSalaryMap = new LinkedHashMap<>();
        Map<CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem>, Double> finSrcItmTargetSalaryMap = new LinkedHashMap<>();
        for (StaffListItem staffListItem : staffListItemList)
        {
            FinancingSource key = staffListItem.getFinancingSource();
            Double salary = finSrcMonthSalaryMap.get(key);
            if (salary == null)
                salary = 0d;
            finSrcMonthSalaryMap.put(key, salary + staffListItem.getMonthBaseSalaryFund());

            if (staffListItem.getFinancingSourceItem() != null)
            {
                CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem> pairKey = new CoreCollectionUtils.Pair<>(staffListItem.getFinancingSource(), staffListItem.getFinancingSourceItem());
                salary = finSrcItmMonthSalaryMap.get(pairKey);
                if (salary == null)
                    salary = 0d;
                finSrcItmMonthSalaryMap.put(pairKey, salary + staffListItem.getMonthBaseSalaryFund());
            }

            totalSalary.setX(totalSalary.getX() + staffListItem.getTargetSalaryFund());
            totalSalary.setY(totalSalary.getY() + staffListItem.getMonthBaseSalaryFund());
        }

        Map<Long, FinancingSource> idFinSrcMap = new LinkedHashMap<>();
        Map<Long, CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem>> idFinSrcItmMap = new LinkedHashMap<>();
        for (Map.Entry<CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem>, Map<String, Double>> entry : finSrcItmPaymentsMap.entrySet())
        {
            idFinSrcItmMap.put(entry.getKey().getY().getId(), entry.getKey());
            wrapperList.add(new IdentifiableWrapper(entry.getKey().getY().getId(), ""));
        }
        for (Map.Entry<FinancingSource, Map<String, Double>> entry : finSrcPaymentsMap.entrySet())
        {
            idFinSrcMap.put(entry.getKey().getId(), entry.getKey());
            wrapperList.add(new IdentifiableWrapper(entry.getKey().getId(), "bold"));
        }
        for (Map.Entry<CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem>, Double> entry : finSrcItmMonthSalaryMap.entrySet())
        {
            if (!idFinSrcItmMap.containsKey(entry.getKey().getY().getId()))
            {
                idFinSrcItmMap.put(entry.getKey().getY().getId(), entry.getKey());
                wrapperList.add(new IdentifiableWrapper(entry.getKey().getY().getId(), ""));
            }
        }
        for (Map.Entry<FinancingSource, Double> entry : finSrcMonthSalaryMap.entrySet())
        {
            if (!idFinSrcMap.containsKey(entry.getKey().getId()))
            {
                idFinSrcMap.put(entry.getKey().getId(), entry.getKey());
                wrapperList.add(new IdentifiableWrapper(entry.getKey().getId(), "bold"));
            }
        }

        wrapperList.add(new IdentifiableWrapper(1L, "bold"));

        for (IdentifiableWrapper wrapper : wrapperList)
        {
            FinancingSource finSrcKey = idFinSrcMap.get(wrapper.getId());
            CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem> finSrcItmKey = idFinSrcItmMap.get(wrapper.getId());
            Double salary = 0d;
            for (Payment payment : payments)
            {
                if (finSrcKey != null)
                    salary += finSrcPaymentsMap.get(finSrcKey) != null ? (finSrcPaymentsMap.get(finSrcKey).get(payment.getCode()) != null ? finSrcPaymentsMap.get(finSrcKey).get(payment.getCode()) : 0d) : 0d;
                if (finSrcItmKey != null)
                    salary += finSrcItmPaymentsMap.get(finSrcItmKey) != null ? (finSrcItmPaymentsMap.get(finSrcItmKey).get(payment.getCode()) != null ? finSrcItmPaymentsMap.get(finSrcItmKey).get(payment.getCode()) : 0d) : 0d;
            }
            if (finSrcKey != null)
                finSrcTargetSalaryMap.put(finSrcKey, salary + (finSrcMonthSalaryMap.get(finSrcKey) != null ? finSrcMonthSalaryMap.get(finSrcKey) : 0d));
            if (finSrcItmKey != null)
                finSrcItmTargetSalaryMap.put(finSrcItmKey, salary + (finSrcItmMonthSalaryMap.get(finSrcItmKey) != null ? finSrcItmMonthSalaryMap.get(finSrcItmKey) : 0d));
        }


        // inject view properties
        model.getPaymentsTotalDataSource().setCountRow(wrapperList.size());
        UniBaseUtils.createPage(model.getPaymentsTotalDataSource(), wrapperList);

        for (ViewWrapper<IEntity> wrapper : ViewWrapper.getPatchedList(model.getPaymentsTotalDataSource()))
        {
            Long id = wrapper.getEntity().getId();
            CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem> finSrcItmKey = idFinSrcItmMap.get(id);
            FinancingSource finSrcKey = idFinSrcMap.get(id);
            Map<String, Double> paymentsMap = null;
            if (id == 1)
                paymentsMap = totalPaymentValueMap;
            if (paymentsMap == null)
                paymentsMap = finSrcItmPaymentsMap.get(finSrcItmKey);
            if (paymentsMap == null)
                paymentsMap = finSrcPaymentsMap.get(finSrcKey);
            for (Payment payment : payments)
            {
                if ((null == paymentsMap) || (null == paymentsMap.get(payment.getCode())))
                    wrapper.setViewProperty(payment.getCode(), null);
                else
                    wrapper.setViewProperty(payment.getCode(), null != paymentsMap.get(payment.getCode()) ? paymentsMap.get(payment.getCode()) : 0);
            }

            Double monthSalary = id == 1 ? totalSalary.getY() :
                    (finSrcItmMonthSalaryMap.get(finSrcItmKey) != null ? finSrcItmMonthSalaryMap.get(finSrcItmKey) : finSrcMonthSalaryMap.get(finSrcKey));
            Double targetSalary = id == 1 ? totalSalary.getX() :
                    (finSrcItmTargetSalaryMap.get(finSrcItmKey) != null ? finSrcItmTargetSalaryMap.get(finSrcItmKey) : finSrcTargetSalaryMap.get(finSrcKey));
            wrapper.setViewProperty("FINANCING_SOURCE", id == 1 ? "ИТОГО" : (idFinSrcMap.get(id) != null ? idFinSrcMap.get(id).getTitle() : idFinSrcItmMap.get(id).getX().getTitle()));
            wrapper.setViewProperty("FINANCING_SOURCE_ITEM", id == 1 ? null : (idFinSrcItmMap.get(id) != null ? idFinSrcItmMap.get(id).getY().getTitle() : ""));
            wrapper.setViewProperty(StaffListItem.P_MONTH_BASE_SALARY_FUND, monthSalary);
            wrapper.setViewProperty(StaffListItem.P_TARGET_SALARY_FUND, targetSalary);
        }
    }

    @Override
    public void prepareStaffListAllocationsDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(StaffListAllocationItem.ENTITY_CLASS, "rel");
        builder.add(MQExpression.eq("rel", StaffListAllocationItem.L_STAFF_LIST_ITEM + "." + StaffListItem.L_STAFF_LIST, model.getStaffList()));

        if (null != model.getEmployeeTypeFilter())
        {
            builder.add(MQExpression.eq("rel", StaffListAllocationItem.L_STAFF_LIST_ITEM + "." + StaffListItem.L_ORG_UNIT_POST_RELATION + "." + OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_POST + "." + Post.L_EMPLOYEE_TYPE, model.getEmployeeTypeFilter()));
        }
        if (null != model.getPostFilter())
        {
            builder.add(MQExpression.eq("rel", StaffListAllocationItem.L_STAFF_LIST_ITEM + "." + StaffListItem.L_ORG_UNIT_POST_RELATION, model.getPostFilter()));
        }
        if (null != model.getLastNameFilter())
        {
            builder.add(MQExpression.like("rel", StaffListAllocationItem.L_EMPLOYEE_POST + "." + EmployeePost.L_EMPLOYEE + "." + Employee.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_LAST_NAME, CoreStringUtils.escapeLike(model.getLastNameFilter())));
        }

        //сортировка
        final DynamicListDataSource<AllocationWrapper> dataSource = model.getAllocationDataSource();
        if (AllocationWrapper.P_POST_TITLE.equals(dataSource.getEntityOrder().getKey()))
            builder.addOrder("rel", StaffListAllocationItem.staffListItem().orgUnitPostRelation().orgUnitTypePostRelation().postBoundedWithQGandQL().title().s(), dataSource.getEntityOrder().getDirection());
        else if (AllocationWrapper.P_POST_TYPE.equals(dataSource.getEntityOrder().getKey()))
            builder.addOrder("rel", StaffListAllocationItem.staffListItem().orgUnitPostRelation().orgUnitTypePostRelation().postBoundedWithQGandQL().post().employeeType().shortTitle().s(), dataSource.getEntityOrder().getDirection());
        else if (AllocationWrapper.P_PROF_QUALIFICATION_GROUP.equals(dataSource.getEntityOrder().getKey()))
            builder.addOrder("rel", StaffListAllocationItem.staffListItem().orgUnitPostRelation().orgUnitTypePostRelation().postBoundedWithQGandQL().profQualificationGroup().shortTitle().s(), dataSource.getEntityOrder().getDirection());
        else if (AllocationWrapper.P_QUALIFICATION_LEVEL.equals(dataSource.getEntityOrder().getKey()))
            builder.addOrder("rel", StaffListAllocationItem.staffListItem().orgUnitPostRelation().orgUnitTypePostRelation().postBoundedWithQGandQL().qualificationLevel().shortTitle().s(), dataSource.getEntityOrder().getDirection());
        else if (AllocationWrapper.P_FINANCING_SOURCE.equals(dataSource.getEntityOrder().getKey()))
            builder.addOrder("rel", StaffListAllocationItem.financingSource().title().s(), dataSource.getEntityOrder().getDirection());

        List<StaffListAllocationItem> builderResultList = builder.getResultList(getSession());
        List<StaffListPaymentBase> paymentsList = UniempDaoFacade.getStaffListDAO().getStaffListAllocPaymentsList(model.getStaffList());
        List<Long> uncopyablePostIds = StaffListPaymentsUtil.getUncopyableStaffListAllocationItemIds(model.getStaffList(), getSession());
        List<Long> invalidPostIds = StaffListPaymentsUtil.getInvalidPostIds(model.getStaffList(), getSession());
        //Map<Long, Double> occupatedListItemsMap = StaffListPaymentsUtil.getOccupatedStaffListItemsMap(model.getStaffList(), getSession());
        List<Long> postStaffRateInvalidIdList = StaffListPaymentsUtil.getEmployeePostStaffRateInvalidIdList(builderResultList, getSession());

        List<AllocationWrapper> wrapperList = new ArrayList<>();
        Map<StaffListItem, Map<CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem>, List<StaffListPaymentBase>>> itemPaymentsMap = new HashMap<>();

        for (StaffListPaymentBase payment : paymentsList)
        {
            CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem> payKey = new CoreCollectionUtils.Pair<>(payment.getFinancingSource(), payment.getFinancingSourceItem());
            StaffListItem item = payment.getStaffListItem();

            Map<CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem>, List<StaffListPaymentBase>> pairListMap = itemPaymentsMap.get(item);
            if (pairListMap == null)
                pairListMap = new HashMap<>();

            List<StaffListPaymentBase> list = pairListMap.get(payKey);
            if (list == null)
                list = new ArrayList<>();

            list.add(payment);

            pairListMap.put(payKey, list);
            itemPaymentsMap.put(item, pairListMap);
        }

        long id = -1;

        AllocationWrapper totalWrapper = new AllocationWrapper(id--, "ИТОГО");
        totalWrapper.setTotal(true);
        totalWrapper.setBold(true);
        totalWrapper.setCantCopy(true);
        totalWrapper.setCantDelete(true);
        totalWrapper.setCantEdit(true);
        Map<FinancingSource, AllocationWrapper> finSrcAllocMap = new HashMap<>();
        for (FinancingSource finSrc : getCatalogItemList(FinancingSource.class))
        {
            AllocationWrapper wrapper = new AllocationWrapper(id--, "Итого");
            wrapper.setFinancingSource(finSrc);
            wrapper.setBold(true);
            wrapper.setFinSrcTotal(true);
            wrapper.setCantCopy(true);
            wrapper.setCantDelete(true);
            wrapper.setCantEdit(true);

            finSrcAllocMap.put(finSrc, wrapper);
        }

        for (StaffListAllocationItem allocationItem : builderResultList)
        {
            model.getIdAllocItem().put(allocationItem.getId(), allocationItem);

            boolean postStaffRateInvalid = allocationItem.getEmployeePost() != null &&
                    (allocationItem.isCombination() && allocationItem.getCombinationPost() != null ?
                            (postStaffRateInvalidIdList.contains(allocationItem.getCombinationPost().getId())) :
                            postStaffRateInvalidIdList.contains(allocationItem.getEmployeePost().getId()));

            AllocationWrapper wrapper = new AllocationWrapper(id--, getAllocationItemPostTitle(allocationItem));
            wrapper.setReserve(allocationItem.isReserve());
            wrapper.setAllocationItem(allocationItem);
            wrapper.setFinancingSource(allocationItem.getFinancingSource());
            wrapper.setFinancingSourceItem(allocationItem.getFinancingSourceItem());
            wrapper.setCantCopy(uncopyablePostIds.contains(allocationItem.getId()));
            wrapper.setStaffRateInvalid(invalidPostIds.contains(allocationItem.getStaffListItem().getId()));
            wrapper.setPostStaffRateInvalid(postStaffRateInvalid);
            wrapper.setCantDelete(allocationItem.isCantBeDeleted());
            wrapper.setCantEdit(allocationItem.getStaffListItem().getStaffList().getStaffListState().getCode().endsWith(UniempDefines.STAFF_LIST_STATUS_ARCHIVE));

            wrapperList.add(wrapper);

            if (itemPaymentsMap.get(allocationItem.getStaffListItem()) != null)
                for (Map.Entry<CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem>, List<StaffListPaymentBase>> entry : itemPaymentsMap.get(allocationItem.getStaffListItem()).entrySet())
                {
                    if (wrapper.equalsFinSrs(entry.getKey().getX(), entry.getKey().getY()))
                    {
                        for (StaffListPaymentBase payment : entry.getValue())
                        {
                            AllocationWrapper finSrcWrapper = finSrcAllocMap.get(payment.getFinancingSource());

                            if (payment instanceof StaffListFakePayment)
                            {
                                if (((StaffListFakePayment)payment).getStaffListAllocationItem() != null && ((StaffListFakePayment) payment).getStaffListAllocationItem().equals(allocationItem))
                                {
                                    payment.setTargetAllocItem(allocationItem);
                                    wrapper.putPaymentValue(payment.getPayment().getCode(), payment.getTotalValue());

                                    finSrcWrapper.putPaymentValue(payment.getPayment().getCode(), payment.getTotalValue());
                                    totalWrapper.putPaymentValue(payment.getPayment().getCode(), payment.getTotalValue());
                                }
                            }
                            else
                            {
                                payment.setTargetAllocItem(allocationItem);
                                wrapper.putPaymentValue(payment.getPayment().getCode(), payment.getTotalValue());

                                finSrcWrapper.putPaymentValue(payment.getPayment().getCode(), payment.getTotalValue());
                                totalWrapper.putPaymentValue(payment.getPayment().getCode(), payment.getTotalValue());
                            }
                        }
                    }
                    else
                    {
                        AllocationWrapper innerWrapper = new AllocationWrapper(id--, getAllocationItemPostTitle(allocationItem));
                        innerWrapper.setFinancingSource(entry.getKey().getX());
                        innerWrapper.setFinancingSourceItem(entry.getKey().getY());
                        innerWrapper.setAllocationItem(allocationItem);
                        innerWrapper.setFinSrc(true);
                        innerWrapper.setCantCopy(true);
                        innerWrapper.setCantDelete(true);
                        innerWrapper.setCantEdit(true);

                        for (StaffListPaymentBase payment : entry.getValue())
                        {
                            AllocationWrapper finSrcWrapper = finSrcAllocMap.get(payment.getFinancingSource());

                            if (payment instanceof StaffListFakePayment)
                            {
                                if (((StaffListFakePayment)payment).getStaffListAllocationItem() != null && ((StaffListFakePayment) payment).getStaffListAllocationItem().equals(allocationItem))
                                {
                                    payment.setTargetAllocItem(allocationItem);
                                    innerWrapper.putPaymentValue(payment.getPayment().getCode(), payment.getTotalValue());

                                    finSrcWrapper.putPaymentValue(payment.getPayment().getCode(), payment.getTotalValue());
                                    totalWrapper.putPaymentValue(payment.getPayment().getCode(), payment.getTotalValue());
                                }
                            }
                            else
                            {
                                payment.setTargetAllocItem(allocationItem);
                                innerWrapper.putPaymentValue(payment.getPayment().getCode(), payment.getTotalValue());

                                finSrcWrapper.putPaymentValue(payment.getPayment().getCode(), payment.getTotalValue());
                                totalWrapper.putPaymentValue(payment.getPayment().getCode(), payment.getTotalValue());
                            }
                        }

                        wrapperList.add(innerWrapper);
                    }
                }
        }

        for (AllocationWrapper allocationWrapper : wrapperList)
        {
            AllocationWrapper wrapper = finSrcAllocMap.get(allocationWrapper.getFinancingSource());

            wrapper.setTotalTargetSalary(wrapper.getTotalTargetSalary() + allocationWrapper.getTargetSalary());
            wrapper.setTotalMonthBaseSalary(wrapper.getTotalMonthBaseSalary() + allocationWrapper.getMonthBaseSalary());

            totalWrapper.setTotalTargetSalary(totalWrapper.getTotalTargetSalary() + allocationWrapper.getTargetSalary());
            totalWrapper.setTotalMonthBaseSalary(totalWrapper.getTotalMonthBaseSalary() + allocationWrapper.getMonthBaseSalary());
        }

        for (StaffListAllocationItem item : builderResultList)
        {
            AllocationWrapper wrapper = finSrcAllocMap.get(item.getFinancingSource());

            wrapper.setTotalStaffRate(wrapper.getTotalStaffRate() + item.getStaffRate());

            totalWrapper.setTotalStaffRate(totalWrapper.getTotalStaffRate() + item.getStaffRate());
        }

        for (Map.Entry<FinancingSource, AllocationWrapper> entry : finSrcAllocMap.entrySet())
            wrapperList.add(entry.getValue());

        wrapperList.add(totalWrapper);

        model.getAllocationDataSource().setCountRow(wrapperList.size());
        UniBaseUtils.createPage(model.getAllocationDataSource(), wrapperList);
    }

    /**
     * @param item должность штатной расстановки
     * @return Название Должности по ПКГ и КУ.
     */
    protected String getAllocationItemPostTitle(StaffListAllocationItem item)//выносим в метод для возможности переопределения
    {
        return item.getStaffListItem().getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL().getTitle();
    }

    @Override
    public void prepareStaffListAllocationsTotalDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(StaffListAllocationItem.ENTITY_CLASS, "rel");
        builder.add(MQExpression.eq("rel", StaffListAllocationItem.L_STAFF_LIST_ITEM + "." + StaffListItem.L_STAFF_LIST, model.getStaffList()));

        if (null != model.getEmployeeTypeFilter())
        {
            builder.add(MQExpression.eq("rel", StaffListAllocationItem.L_STAFF_LIST_ITEM + "." + StaffListItem.L_ORG_UNIT_POST_RELATION + "." + OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_POST + "." + Post.L_EMPLOYEE_TYPE, model.getEmployeeTypeFilter()));
        }
        if (null != model.getPostFilter())
        {
            builder.add(MQExpression.eq("rel", StaffListAllocationItem.L_STAFF_LIST_ITEM + "." + StaffListItem.L_ORG_UNIT_POST_RELATION, model.getPostFilter()));
        }
        if (null != model.getLastNameFilter())
        {
            builder.add(MQExpression.like("rel", StaffListAllocationItem.L_EMPLOYEE_POST + "." + EmployeePost.L_EMPLOYEE + "." + Employee.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_LAST_NAME, CoreStringUtils.escapeLike(model.getLastNameFilter())));
        }

        List<StaffListAllocationItem> builderResultList = builder.getResultList(getSession());

        List<IdentifiableWrapper> wrapperList = new ArrayList<>();

        List<Payment> payments = model.getPaymentsList();
        List<StaffListPaymentBase> paymentsList = UniempDaoFacade.getStaffListDAO().getStaffListAllocPaymentsList(model.getStaffList());
        Map<Long, Map<String, List<StaffListPaymentBase>>> staffListItemId2payments = new HashMap<>();
        Map<Long, Map<String, List<StaffListPaymentBase>>> staffListAllocationId2payments = new HashMap<>();
        Map<FinancingSource, Map<String, Double>> finSrcPaymentsMap = new LinkedHashMap<>();
        Map<CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem>, Map<String, Double>> finSrcItmPaymentsMap = new LinkedHashMap<>();
        Map<String, Double> totalPaymentValueMap = new LinkedHashMap<>();
        for (StaffListPaymentBase item : paymentsList)
        {
            if (item instanceof StaffListFakePayment)
            {
                StaffListFakePayment fakePayment = (StaffListFakePayment)item;
                if (null != fakePayment.getStaffListAllocationItem())
                {
                    Long id = fakePayment.getStaffListAllocationItem().getId();
                    Map<String, List<StaffListPaymentBase>> paymentsMap = staffListAllocationId2payments.get(id);
                    if (null == paymentsMap)
                        paymentsMap = new HashMap<>();

                    List<StaffListPaymentBase> paymentList = paymentsMap.get(item.getPayment().getCode());
                    if (paymentList == null)
                        paymentList = new ArrayList<>();

                    paymentList.add(item);
                    paymentsMap.put(item.getPayment().getCode(), paymentList);
                    staffListAllocationId2payments.put(id, paymentsMap);
                }
            }
            else
            {
                Long id = item.getStaffListItem().getId();
                Map<String, List<StaffListPaymentBase>> paymentsMap = staffListItemId2payments.get(id);
                if (null == paymentsMap)
                {
                    paymentsMap = new HashMap<>();
                }

                List<StaffListPaymentBase> paymentList = paymentsMap.get(item.getPayment().getCode());
                if (paymentList == null)
                    paymentList = new ArrayList<>();

                paymentList.add(item);
                paymentsMap.put(item.getPayment().getCode(), paymentList);
                staffListItemId2payments.put(id, paymentsMap);
            }
        }

        CoreCollectionUtils.Pair<Double, Double> totalSalary = new CoreCollectionUtils.Pair<>(0d, 0d);
        Map<FinancingSource, Double> finSrcTargetSalaryMap = new LinkedHashMap<>();
        Map<FinancingSource, Double> finSrcMonthSalaryMap = new LinkedHashMap<>();
        Map<CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem>, Double> finSrcItmMonthSalaryMap = new LinkedHashMap<>();
        Map<CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem>, Double> finSrcItmTargetSalaryMap = new LinkedHashMap<>();
        for (StaffListAllocationItem allocationItem : builderResultList)
        {
            FinancingSource key = allocationItem.getFinancingSource();

            Double monthSalary = finSrcMonthSalaryMap.get(key);
            if (monthSalary == null)
                monthSalary = 0.0d;
            finSrcMonthSalaryMap.put(key, monthSalary += allocationItem.getMonthBaseSalaryFund());

            if (allocationItem.getFinancingSourceItem() != null)
            {
                CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem> pairKey = new CoreCollectionUtils.Pair<>(allocationItem.getFinancingSource(), allocationItem.getFinancingSourceItem());
                monthSalary = finSrcItmMonthSalaryMap.get(pairKey);
                if (monthSalary == null)
                    monthSalary = 0d;
                finSrcItmMonthSalaryMap.put(pairKey, monthSalary + allocationItem.getMonthBaseSalaryFund());
            }

            totalSalary.setX(totalSalary.getX() + allocationItem.getTargetSalary());
            totalSalary.setY(totalSalary.getY() + allocationItem.getMonthBaseSalaryFund());
        }

        for (StaffListAllocationItem item : builderResultList)
        {
            Long allocId = item.getId();
            Long id = item.getStaffListItem().getId();

            Map<String, List<StaffListPaymentBase>> paymentsMap = staffListItemId2payments.get(id);
            Map<String, List<StaffListPaymentBase>> allocPaymentsMap = staffListAllocationId2payments.get(allocId);
            for (Payment payment : payments)
            {

                if ((null != allocPaymentsMap) && (null != allocPaymentsMap.get(payment.getCode())))
                {
                    for (StaffListPaymentBase paymentItem : allocPaymentsMap.get(payment.getCode()))
                    {
                        paymentItem.setTargetAllocItem(item);

                        FinancingSource key = paymentItem.getFinancingSource();
                        String codeKey = paymentItem.getPayment().getCode();
                        Map<String, Double> codePaymentMap = finSrcPaymentsMap.get(key);
                        if (codePaymentMap == null)
                            codePaymentMap = new LinkedHashMap<>();
                        Double tatalValue = codePaymentMap.get(codeKey);
                        if (tatalValue == null)
                            tatalValue = 0d;
                        codePaymentMap.put(codeKey, tatalValue + paymentItem.getTotalValue());
                        finSrcPaymentsMap.put(key, codePaymentMap);

                        if (paymentItem.getFinancingSourceItem() != null)
                        {
                            CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem> pairkey = new CoreCollectionUtils.Pair<>(paymentItem.getFinancingSource(),paymentItem.getFinancingSourceItem());
                            codeKey = paymentItem.getPayment().getCode();
                            codePaymentMap = finSrcItmPaymentsMap.get(pairkey);
                            if (codePaymentMap == null)
                                codePaymentMap = new LinkedHashMap<>();
                            tatalValue = codePaymentMap.get(codeKey);
                            if (tatalValue == null)
                                tatalValue = 0d;
                            codePaymentMap.put(codeKey, tatalValue + paymentItem.getTotalValue());
                            finSrcItmPaymentsMap.put(pairkey, codePaymentMap);
                        }

                        String code = paymentItem.getPayment().getCode();
                        Double total = totalPaymentValueMap.get(code);
                        if (total == null)
                            total = 0d;
                        totalPaymentValueMap.put(code , total + paymentItem.getTotalValue());
                    }
                }

                if ((null != paymentsMap) && (null != paymentsMap.get(payment.getCode())))
                {
                    for (StaffListPaymentBase paymentItem : paymentsMap.get(payment.getCode()))
                    {
                        paymentItem.setTargetAllocItem(item);

                        FinancingSource key = paymentItem.getFinancingSource();
                        String codeKey = paymentItem.getPayment().getCode();
                        Map<String, Double> codePaymentMap = finSrcPaymentsMap.get(key);
                        if (codePaymentMap == null)
                            codePaymentMap = new LinkedHashMap<>();
                        Double tatalValue = codePaymentMap.get(codeKey);
                        if (tatalValue == null)
                            tatalValue = 0d;
                        codePaymentMap.put(codeKey, tatalValue + paymentItem.getTotalValue());
                        finSrcPaymentsMap.put(key, codePaymentMap);

                        if (paymentItem.getFinancingSourceItem() != null)
                        {
                            CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem> pairkey = new CoreCollectionUtils.Pair<>(paymentItem.getFinancingSource(),paymentItem.getFinancingSourceItem());
                            codeKey = paymentItem.getPayment().getCode();
                            codePaymentMap = finSrcItmPaymentsMap.get(pairkey);
                            if (codePaymentMap == null)
                                codePaymentMap = new LinkedHashMap<>();
                            tatalValue = codePaymentMap.get(codeKey);
                            if (tatalValue == null)
                                tatalValue = 0d;
                            codePaymentMap.put(codeKey, tatalValue + paymentItem.getTotalValue());
                            finSrcItmPaymentsMap.put(pairkey, codePaymentMap);
                        }

                        String code = paymentItem.getPayment().getCode();
                        Double total = totalPaymentValueMap.get(code);
                        if (total == null)
                            total = 0d;
                        totalPaymentValueMap.put(code , total + paymentItem.getTotalValue());
                    }
                }
            }
        }

        Map<Long, FinancingSource> idFinSrcMap = new LinkedHashMap<>();
        Map<Long, CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem>> idFinSrcItmMap = new LinkedHashMap<>();
        for (Map.Entry<CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem>, Map<String, Double>> entry : finSrcItmPaymentsMap.entrySet())
        {
            idFinSrcItmMap.put(entry.getKey().getY().getId(), entry.getKey());
            wrapperList.add(new IdentifiableWrapper(entry.getKey().getY().getId(), ""));
        }
        for (Map.Entry<CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem>, Double> entry : finSrcItmMonthSalaryMap.entrySet())
        {
            if (!idFinSrcItmMap.containsKey(entry.getKey().getY().getId()))
            {
                idFinSrcItmMap.put(entry.getKey().getY().getId(), entry.getKey());
                wrapperList.add(new IdentifiableWrapper(entry.getKey().getY().getId(), ""));
            }
        }
        for (Map.Entry<FinancingSource, Map<String, Double>> entry : finSrcPaymentsMap.entrySet())
        {
            idFinSrcMap.put(entry.getKey().getId(), entry.getKey());
            wrapperList.add(new IdentifiableWrapper(entry.getKey().getId(), "bold"));
        }
        for (Map.Entry<FinancingSource, Double> entry : finSrcMonthSalaryMap.entrySet())
        {
            if (!idFinSrcMap.containsKey(entry.getKey().getId()))
            {
                idFinSrcMap.put(entry.getKey().getId(), entry.getKey());
                wrapperList.add(new IdentifiableWrapper(entry.getKey().getId(), "bold"));
            }
        }

        wrapperList.add(new IdentifiableWrapper(1L, "bold"));

        for (IdentifiableWrapper wrapper : wrapperList)
        {
            FinancingSource finSrcKey = idFinSrcMap.get(wrapper.getId());
            CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem> finSrcItmKey = idFinSrcItmMap.get(wrapper.getId());
            Double salary = 0d;
            for (Payment payment : payments)
            {
                if (finSrcKey != null)
                    salary += finSrcPaymentsMap.get(finSrcKey) != null ? (finSrcPaymentsMap.get(finSrcKey).get(payment.getCode()) != null ? finSrcPaymentsMap.get(finSrcKey).get(payment.getCode()) : 0d) : 0d;
                if (finSrcItmKey != null)
                    salary += finSrcItmPaymentsMap.get(finSrcItmKey) != null ? (finSrcItmPaymentsMap.get(finSrcItmKey).get(payment.getCode()) != null ? finSrcItmPaymentsMap.get(finSrcItmKey).get(payment.getCode()) : 0d) : 0d;
            }
            if (finSrcKey != null)
                finSrcTargetSalaryMap.put(finSrcKey, salary + (finSrcMonthSalaryMap.get(finSrcKey) != null ? finSrcMonthSalaryMap.get(finSrcKey) : 0d));
            if (finSrcItmKey != null)
                finSrcItmTargetSalaryMap.put(finSrcItmKey, salary + (finSrcItmMonthSalaryMap.get(finSrcItmKey) != null ? finSrcItmMonthSalaryMap.get(finSrcItmKey) : 0d));
        }

        model.getAllocationTotalDataSource().setCountRow(wrapperList.size());
        UniBaseUtils.createPage(model.getAllocationTotalDataSource(), wrapperList);

        // inject view properties
        for (ViewWrapper<IEntity> wrapper : ViewWrapper.getPatchedList(model.getAllocationTotalDataSource()))
        {
            Long id = wrapper.getEntity().getId();
            CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem> finSrcItmKey = idFinSrcItmMap.get(id);
            FinancingSource finSrcKey = idFinSrcMap.get(id);

            Map<String, Double> paymentMap = null;
            if (id == 1)
                paymentMap = totalPaymentValueMap;
            if (paymentMap == null)
                paymentMap = finSrcItmPaymentsMap.get(finSrcItmKey);
            if (paymentMap == null)
                paymentMap = finSrcPaymentsMap.get(finSrcKey);
            for (Payment payment : payments)
            {
                if ((null == paymentMap) || (null == paymentMap.get(payment.getCode())))
                    wrapper.setViewProperty(payment.getCode(), null);
                else
                    wrapper.setViewProperty(payment.getCode(), null != paymentMap.get(payment.getCode()) ? paymentMap.get(payment.getCode()) : 0);
            }

            Double monthSalary = id == 1 ? totalSalary.getY() :
                    (finSrcItmMonthSalaryMap.get(finSrcItmKey) != null ? finSrcItmMonthSalaryMap.get(finSrcItmKey) : finSrcMonthSalaryMap.get(finSrcKey));
            Double targetSalary = id == 1 ? totalSalary.getX() :
                    (finSrcItmTargetSalaryMap.get(finSrcItmKey) != null ? finSrcItmTargetSalaryMap.get(finSrcItmKey) : finSrcTargetSalaryMap.get(finSrcKey));
            wrapper.setViewProperty("L_FINANCING_SOURCE", id == 1 ? "ИТОГО" : (idFinSrcMap.get(id) != null ? idFinSrcMap.get(id).getTitle() : idFinSrcItmMap.get(id).getX().getTitle()));
            wrapper.setViewProperty("L_FINANCING_SOURCE_ITEM", id == 1 ? null : (idFinSrcItmMap.get(id) != null ? idFinSrcItmMap.get(id).getY().getTitle() : ""));
            wrapper.setViewProperty(StaffListAllocationItem.P_MONTH_BASE_SALARY_FUND, monthSalary);
            wrapper.setViewProperty(StaffListAllocationItem.P_TARGET_SALARY, targetSalary);
        }
    }

    @Override
    public void prepareStaffListEmployeeHRDataSource(Model model)
    {
        Map<StaffListItem, CoreCollectionUtils.Pair<List<StaffListPaymentBase>, List<StaffListPaymentBase>>> paymentsMap = getPaymentsMap(model.getStaffList(), model.getCompensPaymentsList());

        MQBuilder staffListItemsBuilder = new MQBuilder(StaffListItem.ENTITY_CLASS, "sli");
        staffListItemsBuilder.add(MQExpression.eq("sli", StaffListItem.L_STAFF_LIST, model.getStaffList()));
        if (null != model.getHrEmployeeTypeFilter())
        {
            staffListItemsBuilder.add(MQExpression.eq("sli", StaffListItem.L_ORG_UNIT_POST_RELATION + "." + OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_POST + "." + Post.L_EMPLOYEE_TYPE, model.getHrEmployeeTypeFilter()));
        }
        if (null != model.getHrPostFilter())
        {
            staffListItemsBuilder.add(MQExpression.eq("sli", StaffListItem.L_ORG_UNIT_POST_RELATION, model.getHrPostFilter()));
        }
        List<StaffListItem> staffListItemsList = staffListItemsBuilder.getResultList(getSession());

        Map<StaffListItem, Double> freeStaffRates = new HashMap<>();
        for(StaffListItem item : staffListItemsList)
        {
            freeStaffRates.put(item, item.getStaffRate());
        }


        MQBuilder builder = new MQBuilder(StaffListAllocationItem.ENTITY_CLASS, "rel");
        builder.add(MQExpression.eq("rel", StaffListAllocationItem.L_STAFF_LIST_ITEM + "." + StaffListItem.L_STAFF_LIST, model.getStaffList()));

        if (null != model.getHrEmployeeTypeFilter())
        {
            builder.add(MQExpression.eq("rel", StaffListAllocationItem.L_STAFF_LIST_ITEM + "." + StaffListItem.L_ORG_UNIT_POST_RELATION + "." + OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_POST + "." + Post.L_EMPLOYEE_TYPE, model.getHrEmployeeTypeFilter()));
        }
        if (null != model.getHrPostFilter())
        {
            builder.add(MQExpression.eq("rel", StaffListAllocationItem.L_STAFF_LIST_ITEM + "." + StaffListItem.L_ORG_UNIT_POST_RELATION, model.getHrPostFilter()));
        }
        if (null != model.getHrLastNameFilter())
        {
            builder.add(MQExpression.like("rel", StaffListAllocationItem.L_EMPLOYEE_POST + "." + EmployeePost.L_EMPLOYEE + "." + Employee.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_LAST_NAME, CoreStringUtils.escapeLike(model.getHrLastNameFilter())));
        }

        _empHROrderSettings.applyOrder(builder, model.getEmployeeHRDataSource().getEntityOrder());

        List<EmployeeHRLineWrapper> resultList = new ArrayList<>();

        for (StaffListAllocationItem item : builder.<StaffListAllocationItem>getResultList(getSession()))
        {
            if (null != item.getEmployeePost() || item.isReserve())
            {
                resultList.add(new EmployeeHRLineWrapper(item.getId(), item.getStaffListItem(), item, item.getStaffRate(), item.getEmployeePost() != null ? item.getEmployeePost() : null, item.getFinancingSource(), item.getFinancingSourceItem(),true, item.isReserve(), item.getCombinationPost(), item.isCombination()));

                Double postFreeStaffRates = freeStaffRates.get(item.getStaffListItem());
                if (null == postFreeStaffRates)
                    postFreeStaffRates = 0d;

                postFreeStaffRates -= item.getStaffRate();
                freeStaffRates.put(item.getStaffListItem(), postFreeStaffRates);
            }
        }

        if (null == model.getHrLastNameFilter())
        {
            for (StaffListItem item : staffListItemsList)
            {
                Double staffRate = freeStaffRates.get(item);

                if (staffRate > 0)
                    resultList.add(new EmployeeHRLineWrapper(item.getId(), item, null, staffRate, null, item.getFinancingSource(), item.getFinancingSourceItem(), false, false, null, false));
            }
        }

        Map<Long, Map<CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem>, Map<String, Double>>> paymValuesMap = new HashMap<>();
        for(EmployeeHRLineWrapper wrapper : resultList)
        {
            Long id = wrapper.getId();
            Map<CoreCollectionUtils.Pair<FinancingSource,FinancingSourceItem>, Map<String, Double>> staffListItemPaymentsMap = paymValuesMap.get(id);
            if (null == staffListItemPaymentsMap)
                staffListItemPaymentsMap = new HashMap<>();

            paymValuesMap.put(id, staffListItemPaymentsMap);

//            double sumPayments = 0d;

            double staffRate = wrapper.isAllocItem() ? (wrapper.getStaffRate()) : 1;
            double baseSalaryRated = staffRate * (wrapper.getStaffListAllocationItem() != null ? (wrapper.getStaffListAllocationItem().getBaseSalary() == null ? 0 : wrapper.getStaffListAllocationItem().getBaseSalary()) : (wrapper.getPost().getSalary() == null ? 0 : wrapper.getPost().getSalary()));

            CoreCollectionUtils.Pair<List<StaffListPaymentBase>, List<StaffListPaymentBase>> pair = paymentsMap.get(wrapper.getStaffListItem());
            if (null != pair)
            {
                for(StaffListPaymentBase paym : pair.getX())
                {
//                    Double paymValue = 0d;
//                    String paymUnitCode = paym.getPayment().getPaymentUnit().getCode();

//                    if (UniempDefines.PAYMENT_UNIT_RUBLES.equals(paymUnitCode) || paym.getAmount() == 0)
//                    {
//                        paymValue = Math.round(paym.getAmount() * (paym.getPayment().isDoesntDependOnStaffRate() ? 1 : staffRate) * 1000d) / 1000d;
//                    }
//                    else if (UniempDefines.PAYMENT_UNIT_COEFFICIENT.equals(paymUnitCode))
//                    {
//                        paymValue = Math.round(baseSalaryRated * paym.getAmount() * 1000) / 1000d;
//                    }
//                    else if (UniempDefines.PAYMENT_UNIT_BASE_PERCENT.equals(paymUnitCode))
//                    {
//                        paymValue = Math.round(baseSalaryRated * paym.getAmount() * 10) / 1000d;
//                    }
//
//                    sumPayments += paymValue;

                    CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem> finSrcPair = new CoreCollectionUtils.Pair<>(paym.getFinancingSource(), paym.getFinancingSourceItem());
                    Map<String, Double> codeValueMap = staffListItemPaymentsMap.get(finSrcPair);
                    if (codeValueMap == null)
                        codeValueMap = new HashMap<>();

                    paym.setTargetAllocItem(wrapper.getStaffListAllocationItem());
                    codeValueMap.put(paym.getPayment().getCode(), paym.getTotalValue());
                    staffListItemPaymentsMap.put(finSrcPair, codeValueMap);
                }

                for(StaffListPaymentBase paym : pair.getY())
                {
//                    Double paymValue = Math.round((sumPayments + staffRate * wrapper.getStaffListItem().getSalary()) * paym.getAmount() * 10d )/1000d;

                    CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem> finSrcPair = new CoreCollectionUtils.Pair<>(paym.getFinancingSource(), paym.getFinancingSourceItem());
                    Map<String, Double> codeValueMap = staffListItemPaymentsMap.get(finSrcPair);
                    if (codeValueMap == null)
                        codeValueMap = new HashMap<>();

                    paym.setTargetAllocItem(wrapper.getStaffListAllocationItem());
                    codeValueMap.put(paym.getPayment().getCode(), paym.getTotalValue());
                    staffListItemPaymentsMap.put(finSrcPair, codeValueMap);
                }
            }

            wrapper.setMonthBaseSalaryFund(baseSalaryRated);
        }

        Collections.sort(resultList, new EmployeeHRWrapperComparator(model.getEmployeeHRDataSource().getEntityOrder().getDirection(), model.getEmployeeHRDataSource().getEntityOrder().getKeyString()));

        long id = -1;

        for (EmployeeHRLineWrapper wrapper : new ArrayList<>(resultList))
        {
            Map<CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem>, Map<String, Double>> finSrcCodeValueMap = paymValuesMap.get(wrapper.getId());
            for (Map.Entry<CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem>, Map<String, Double>> entry : finSrcCodeValueMap.entrySet())
            {
                if (wrapper.equalsFinSrs(entry.getKey().getX(),entry.getKey().getY()))
                {

                    for (Map.Entry<String, Double> codeValueEntry : entry.getValue().entrySet())
                        wrapper.putPaymentValue(codeValueEntry.getKey(), codeValueEntry.getValue());
                }
                else
                {
                    EmployeeHRLineWrapper finSrcWrapper = new EmployeeHRLineWrapper(id--, wrapper.getStaffListItem(), null, wrapper.getStaffRate(), wrapper.getEmployeePost() != null ? wrapper.getEmployeePost() : null, entry.getKey().getX(), entry.getKey().getY(), true, wrapper.isReserve(), wrapper.getCombinationPost(), wrapper.isCombination());
                    finSrcWrapper.setFinSrc(true);

                    for (Map.Entry<String, Double> codeValueEntry : entry.getValue().entrySet())
                        finSrcWrapper.putPaymentValue(codeValueEntry.getKey(), codeValueEntry.getValue());

                    resultList.add(resultList.indexOf(wrapper) + 1, finSrcWrapper);
                }
            }

            Double staffRate = freeStaffRates.get(wrapper.getStaffListItem());
            if (staffRate < 0)
            {
                wrapper.setInvalid(true);
                wrapper.setErrMessage("Превышено допустимое значение ставок на должности");
            } else
            {
                wrapper.setInvalid(false);
                wrapper.setErrMessage("");
            }
        }

        UniBaseUtils.createPage(model.getEmployeeHRDataSource(), resultList);
    }

    @Override
    public void updateFillStaffListAllocationAutomatically(Model model)
    {
        StaffListPaymentsUtil.fillStaffListAutomatically(model.getStaffList(), getSession());
    }

    @Override
    @SuppressWarnings("deprecation")
    public void updateCopyStaffListAllocationItem(Long staffListAllocationItemId)
    {
        StaffListPaymentsUtil.copyStaffListAllocationItem(staffListAllocationItemId, getSession());
    }

    private List<Payment> getUsedOrActivePaymentsList(StaffList staffList)
    {
        MQBuilder usedPaymentsBuilder = new MQBuilder(StaffListPaymentBase.ENTITY_CLASS, "pb", new String[] { StaffListPaymentBase.L_PAYMENT + "." + Payment.P_ID });
        usedPaymentsBuilder.add(MQExpression.eq("pb", StaffListPaymentBase.L_STAFF_LIST_ITEM + "." + StaffListItem.L_STAFF_LIST, staffList));
        usedPaymentsBuilder.setNeedDistinct(true);

        MQBuilder paymentsBuilder = new MQBuilder(Payment.ENTITY_CLASS, "p");
        AbstractExpression expr1 = MQExpression.eq("p", Payment.P_ACTIVE, Boolean.TRUE);
        AbstractExpression expr2 = MQExpression.in("p", Payment.P_ID, usedPaymentsBuilder);
        paymentsBuilder.add(MQExpression.or(expr1, expr2));

        List<Payment> payments = paymentsBuilder.getResultList(getSession());

        MQBuilder settingsBuilder = new MQBuilder(StaffListRepPaymentSettings.ENTITY_CLASS, "s");
        settingsBuilder.addOrder("s", StaffListRepPaymentSettings.P_PRIORITY);
        List<StaffListRepPaymentSettings> paymentPrioritySettingsList = settingsBuilder.getResultList(getSession());

        if(paymentPrioritySettingsList.isEmpty()) return payments;

        List<Payment> result = new ArrayList<>();
        for(StaffListRepPaymentSettings setting : paymentPrioritySettingsList)
            if(null != setting.getPayment() && payments.contains(setting.getPayment()))
                result.add(setting.getPayment());

        return result;
    }

    private List<Payment> getUsedOrActiveCompensPaymentsList(List<Payment> allUsedActivePaymentsList)
    {
        List<Payment> result = new ArrayList<>();

        for (Payment payment : allUsedActivePaymentsList)
        {
            boolean compensPayment = false;
            PaymentType parentPaymentType = payment.getType().getParent();

            while (null != parentPaymentType)
            {
                if(UniempDefines.PAYMENT_TYPE_COMPENSATION.equals(parentPaymentType.getCode()))
                    compensPayment = true;
                parentPaymentType = parentPaymentType.getParent();
            }

            if (compensPayment) result.add(payment);
        }

        return result;
    }

    /*private List<PaymentType> getChildPaymentTypes(PaymentType paymentType)
    {
        List<PaymentType> childsList = getSession().createCriteria(PaymentType.class).add(Restrictions.eq(PaymentType.L_PARENT, paymentType)).list();

        List<PaymentType> childsChildsList = new ArrayList<PaymentType>();
        for(PaymentType child : childsList)
            childsChildsList.addAll(getChildPaymentTypes(child));
        childsList.addAll(childsChildsList);

        return childsList;
    }*/

    private Map<StaffListItem, CoreCollectionUtils.Pair<List<StaffListPaymentBase>, List<StaffListPaymentBase>>> getPaymentsMap(StaffList staffList, List<Payment> paymentsToAccount)
    {
        Map<StaffListItem, CoreCollectionUtils.Pair<List<StaffListPaymentBase>, List<StaffListPaymentBase>>> paymentsMap = new HashMap<>();

        MQBuilder builder = new MQBuilder(StaffListPaymentBase.ENTITY_CLASS, "slp");
        builder.add(MQExpression.eq("slp", StaffListPaymentBase.L_STAFF_LIST_ITEM + "." + StaffListItem.L_STAFF_LIST, staffList));

        for(StaffListPaymentBase paym : builder.<StaffListPaymentBase>getResultList(getSession()))
        {
            if(paymentsToAccount.contains(paym.getPayment()))
            {
                CoreCollectionUtils.Pair<List<StaffListPaymentBase>, List<StaffListPaymentBase>> pair = paymentsMap.get(paym.getStaffListItem());
                if(null == pair) pair = new CoreCollectionUtils.Pair<List<StaffListPaymentBase>, List<StaffListPaymentBase>>(new ArrayList<StaffListPaymentBase>(), new ArrayList<StaffListPaymentBase>());
                if(UniempDefines.PAYMENT_UNIT_FULL_PERCENT.equals(paym.getPayment().getPaymentUnit().getCode()))
                    pair.getY().add(paym);
                else
                    pair.getX().add(paym);
                paymentsMap.put(paym.getStaffListItem(), pair);
            }
        }
        return paymentsMap;
    }
}