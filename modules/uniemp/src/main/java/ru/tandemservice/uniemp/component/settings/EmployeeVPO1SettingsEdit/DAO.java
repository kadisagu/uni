/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniemp.component.settings.EmployeeVPO1SettingsEdit;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.Post;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.EmployeeVPO1ReportLines;
import ru.tandemservice.uniemp.entity.report.EmployeeVPO1RepLineToPost;
import ru.tandemservice.uniemp.entity.report.EmployeeVPO1RepLineToPostType;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author dseleznev
 * Created on: 21.01.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setReportLines(get(EmployeeVPO1ReportLines.class, model.getEmployeeVPO1ReportLinesId()));
        model.setEmployeeTypesList(HierarchyUtil.listHierarchyNodesWithParents(getList(EmployeeType.class), true));
        model.setEmployeeTypeFilter(UniempDaoFacade.getUniempDAO().getVPOReportLineEmployeeType(model.getReportLines()));

        model.setSelectedPostsList(getSelectedPostsList(model));
        model.setPostsListModel(new FullCheckSelectModel(PostBoundedWithQGandQL.P_FULL_TITLE_WITH_SALARY)
        {
            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(PostBoundedWithQGandQL.ENTITY_CLASS, "rel");
                builder.add(MQExpression.like("rel", PostBoundedWithQGandQL.P_TITLE, CoreStringUtils.escapeLike(filter)));
                if(null != model.getEmployeeTypeFilter())
                    builder.add(MQExpression.eq("rel", PostBoundedWithQGandQL.L_POST + "." + Post.L_EMPLOYEE_TYPE, model.getEmployeeTypeFilter()));

                builder.addOrder("rel", PostBoundedWithQGandQL.P_TITLE);
                return new ListResult<>(builder.getResultList(getSession()));
            }
        });
    }

    @Override
    public void update(Model model)
    {
//        MQBuilder typeBuilder = new MQBuilder(EmployeeVPO1RepLineToPostType.ENTITY_CLASS, "rel");
//        typeBuilder.add(MQExpression.eq("rel", EmployeeVPO1RepLineToPostType.L_EMPLOYEE_V_P_O1_REPORT_LINES, model.getReportLines()));
//        if (typeBuilder.getResultCount(getSession()) > 0)
//            for (EmployeeVPO1RepLineToPostType rel : typeBuilder.<EmployeeVPO1RepLineToPostType> getResultList(getSession()))
//                delete(rel);

        new DQLDeleteBuilder(EmployeeVPO1RepLineToPostType.class)
                .where(eq(property(EmployeeVPO1RepLineToPostType.employeeVPO1ReportLines()), value(model.getReportLines())))
                .createStatement(getSession()).execute();

        if (null != model.getEmployeeTypeFilter())
        {
            EmployeeVPO1RepLineToPostType postTypeRel = new EmployeeVPO1RepLineToPostType();
            postTypeRel.setEmployeeVPO1ReportLines(model.getReportLines());
            postTypeRel.setEmployeeType(model.getEmployeeTypeFilter());
            getSession().save(postTypeRel);
        }

        List<Long> postsToStay = new ArrayList<>();
        MQBuilder builder = new MQBuilder(EmployeeVPO1RepLineToPost.ENTITY_CLASS, "rel");
        builder.add(MQExpression.eq("rel", EmployeeVPO1RepLineToPost.L_EMPLOYEE_V_P_O1_REPORT_LINES, model.getReportLines()));
        List<EmployeeVPO1RepLineToPost> relsList = builder.getResultList(getSession());
        for(EmployeeVPO1RepLineToPost rel : relsList)
            if(!model.getSelectedPostsList().contains(rel.getPostBoundedWithQGandQL()))
                getSession().delete(rel);
            else
                postsToStay.add(rel.getPostBoundedWithQGandQL().getId());

        for (PostBoundedWithQGandQL post : model.getSelectedPostsList())
            if (!postsToStay.contains(post.getId()))
            {
                EmployeeVPO1RepLineToPost rel = new EmployeeVPO1RepLineToPost();
                rel.setEmployeeVPO1ReportLines(model.getReportLines());
                rel.setPostBoundedWithQGandQL(post);
                getSession().save(rel);
            }
    }

    private List<PostBoundedWithQGandQL> getSelectedPostsList(Model model)
    {
        MQBuilder builder = new MQBuilder(EmployeeVPO1RepLineToPost.ENTITY_CLASS, "rel", new String[] {EmployeeVPO1RepLineToPost.L_POST_BOUNDED_WITH_Q_GAND_Q_L});
        builder.add(MQExpression.eq("rel", EmployeeVPO1RepLineToPost.L_EMPLOYEE_V_P_O1_REPORT_LINES, model.getReportLines()));
        builder.addOrder("rel", EmployeeVPO1RepLineToPost.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.P_TITLE);
        return builder.getResultList(getSession());
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        List<Long> excludeIdList = new ArrayList<>();
        excludeIdList.add(model.getEmployeeVPO1ReportLinesId());

        EmployeeVPO1ReportLines parent = model.getReportLines().getParent();
        while (parent != null)
        {
            excludeIdList.add(parent.getId());
            parent = parent.getParent();
        }

        excludeIdList.addAll(getChildReportLineIdList(model.getEmployeeVPO1ReportLinesId()));

        if (model.getSelectedPostsList() != null && !model.getSelectedPostsList().isEmpty())
        {
            List<EmployeeVPO1RepLineToPost> intersectList = new DQLSelectBuilder().fromEntity(EmployeeVPO1RepLineToPost.class, "b").column(property("b"))
                    .where(notIn(property(EmployeeVPO1RepLineToPost.employeeVPO1ReportLines().id().fromAlias("b")), excludeIdList))
                    .where(in(property(EmployeeVPO1RepLineToPost.postBoundedWithQGandQL().fromAlias("b")), model.getSelectedPostsList()))
                    .createStatement(getSession()).list();

            if (!intersectList.isEmpty())
            {
                if (intersectList.size() == 1)
                {
                    errors.add("Должность " + intersectList.get(0).getPostBoundedWithQGandQL().getFullTitleWithSalary() + " уже указана в настройке для строки " + intersectList.get(0).getEmployeeVPO1ReportLines().getTitle() + ".");
                }
                else
                {
                    errors.add("Должности " + UniStringUtils.join(intersectList, EmployeeVPO1RepLineToPost.postBoundedWithQGandQL().fullTitleWithSalary().s(), ", ")  + " уже указаны в настройке для строк " + UniStringUtils.joinUnique(intersectList, EmployeeVPO1RepLineToPost.employeeVPO1ReportLines().title().s(), ", ") + ".");
                }

                return;
            }

            Set<EmployeeType> typeSet = new HashSet<>();

            if (model.getEmployeeTypeFilter() != null)
                typeSet.add(model.getEmployeeTypeFilter());
            for (PostBoundedWithQGandQL post : model.getSelectedPostsList())
                typeSet.add(post.getPost().getEmployeeType());

            List<EmployeeVPO1RepLineToPostType> intersectTypeList = new DQLSelectBuilder().fromEntity(EmployeeVPO1RepLineToPostType.class, "b").column("b")
                    .where(notIn(property(EmployeeVPO1RepLineToPostType.employeeVPO1ReportLines().id().fromAlias("b")), excludeIdList))
                    .where(notExists(new DQLSelectBuilder().fromEntity(EmployeeVPO1RepLineToPost.class, "p").column(property(EmployeeVPO1RepLineToPost.id().fromAlias("p")))
                            .where(notIn(property(EmployeeVPO1RepLineToPost.employeeVPO1ReportLines().id().fromAlias("p")), excludeIdList))
                            .where(eq(property(EmployeeVPO1RepLineToPost.employeeVPO1ReportLines().id().fromAlias("p")), property(EmployeeVPO1RepLineToPostType.employeeVPO1ReportLines().id().fromAlias("b"))))
                            .buildQuery()))
                    .where(in(property(EmployeeVPO1RepLineToPostType.employeeType().fromAlias("b")), typeSet))
                    .createStatement(getSession()).list();

            if (!intersectTypeList.isEmpty())
            {
                errors.add("Тип должности " + intersectTypeList.get(0).getEmployeeType().getTitle() + " уже указан в настройке для строки " + intersectTypeList.get(0).getEmployeeVPO1ReportLines().getTitle() + ".");
            }
        }
        else if ((model.getSelectedPostsList() == null || model.getSelectedPostsList().isEmpty()) && model.getEmployeeTypeFilter() != null)
        {
            List<EmployeeVPO1ReportLines> intersectTypeList = new DQLSelectBuilder().fromEntity(EmployeeVPO1ReportLines.class, "b").column("b")
                    .where(notIn(property(EmployeeVPO1ReportLines.id().fromAlias("b")), excludeIdList))
                    .where(or(
                            exists(new DQLSelectBuilder().fromEntity(EmployeeVPO1RepLineToPostType.class, "t").column(property(EmployeeVPO1RepLineToPostType.id().fromAlias("t")))
                                    .where(eq(property(EmployeeVPO1RepLineToPostType.employeeVPO1ReportLines().id().fromAlias("t")), property(EmployeeVPO1ReportLines.id().fromAlias("b"))))
                                    .where(eqValue(property(EmployeeVPO1RepLineToPostType.employeeType().fromAlias("t")), model.getEmployeeTypeFilter()))
                                    .buildQuery()),
                            exists(new DQLSelectBuilder().fromEntity(EmployeeVPO1RepLineToPost.class, "p").column(property(EmployeeVPO1RepLineToPost.id().fromAlias("p")))
                                    .where(eq(property(EmployeeVPO1RepLineToPost.employeeVPO1ReportLines().id().fromAlias("p")), property(EmployeeVPO1ReportLines.id().fromAlias("b"))))
                                    .where(eqValue(property(EmployeeVPO1RepLineToPost.postBoundedWithQGandQL().post().employeeType().fromAlias("p")), model.getEmployeeTypeFilter()))
                                    .buildQuery())
                    ))
                    .createStatement(getSession()).list();

            if (!intersectTypeList.isEmpty())
            {
                errors.add("Тип должности " + model.getEmployeeTypeFilter().getTitle() + " уже указан в настройке для строки " + intersectTypeList.get(0).getTitle() + ".");
            }
        }
    }

    protected List<Long> getChildReportLineIdList(long reportLineId)
    {
        List<Long> resultIdList = new ArrayList<>();

        List<Long> idList = new DQLSelectBuilder().fromEntity(EmployeeVPO1ReportLines.class, "b").column(property(EmployeeVPO1ReportLines.id().fromAlias("b")))
                .where(eqValue(property(EmployeeVPO1ReportLines.parent().id().fromAlias("b")), reportLineId))
                .createStatement(getSession()).list();

        resultIdList.addAll(idList);
        for (Long id : idList)
            resultIdList.addAll(getChildReportLineIdList(id));

        return resultIdList;
    }
}