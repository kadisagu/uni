package ru.tandemservice.uniemp.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uniemp.entity.catalog.LabourContractType;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Трудовой договор
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeLabourContractGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract";
    public static final String ENTITY_NAME = "employeeLabourContract";
    public static final int VERSION_HASH = -601055619;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_POST = "employeePost";
    public static final String P_NUMBER = "number";
    public static final String P_DATE = "date";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_EMPLOYMENT_DATE = "employmentDate";
    public static final String L_TYPE = "type";
    public static final String L_CONTRACT_FILE = "contractFile";
    public static final String P_CONTRACT_FILE_NAME = "contractFileName";
    public static final String P_CONTRACT_FILE_TYPE = "contractFileType";

    private EmployeePost _employeePost;     // Сотрудник
    private String _number;     // Номер
    private Date _date;     // Дата заключения
    private Date _beginDate;     // Дата начала
    private Date _endDate;     // Дата окончания
    private Date _employmentDate;     // Дата приема на работу
    private LabourContractType _type;     // Тип трудового договора
    private DatabaseFile _contractFile;     // Файл договора
    private String _contractFileName;     // Название файла
    private String _contractFileType;     // Тип файла

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сотрудник. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    /**
     * @param employeePost Сотрудник. Свойство не может быть null и должно быть уникальным.
     */
    public void setEmployeePost(EmployeePost employeePost)
    {
        dirty(_employeePost, employeePost);
        _employeePost = employeePost;
    }

    /**
     * @return Номер. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер. Свойство не может быть null.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Дата заключения. Свойство не может быть null.
     */
    @NotNull
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date Дата заключения. Свойство не может быть null.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Дата приема на работу.
     */
    public Date getEmploymentDate()
    {
        return _employmentDate;
    }

    /**
     * @param employmentDate Дата приема на работу.
     */
    public void setEmploymentDate(Date employmentDate)
    {
        dirty(_employmentDate, employmentDate);
        _employmentDate = employmentDate;
    }

    /**
     * @return Тип трудового договора. Свойство не может быть null.
     */
    @NotNull
    public LabourContractType getType()
    {
        return _type;
    }

    /**
     * @param type Тип трудового договора. Свойство не может быть null.
     */
    public void setType(LabourContractType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Файл договора.
     */
    public DatabaseFile getContractFile()
    {
        return _contractFile;
    }

    /**
     * @param contractFile Файл договора.
     */
    public void setContractFile(DatabaseFile contractFile)
    {
        dirty(_contractFile, contractFile);
        _contractFile = contractFile;
    }

    /**
     * @return Название файла.
     */
    @Length(max=255)
    public String getContractFileName()
    {
        return _contractFileName;
    }

    /**
     * @param contractFileName Название файла.
     */
    public void setContractFileName(String contractFileName)
    {
        dirty(_contractFileName, contractFileName);
        _contractFileName = contractFileName;
    }

    /**
     * @return Тип файла.
     */
    @Length(max=255)
    public String getContractFileType()
    {
        return _contractFileType;
    }

    /**
     * @param contractFileType Тип файла.
     */
    public void setContractFileType(String contractFileType)
    {
        dirty(_contractFileType, contractFileType);
        _contractFileType = contractFileType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EmployeeLabourContractGen)
        {
            setEmployeePost(((EmployeeLabourContract)another).getEmployeePost());
            setNumber(((EmployeeLabourContract)another).getNumber());
            setDate(((EmployeeLabourContract)another).getDate());
            setBeginDate(((EmployeeLabourContract)another).getBeginDate());
            setEndDate(((EmployeeLabourContract)another).getEndDate());
            setEmploymentDate(((EmployeeLabourContract)another).getEmploymentDate());
            setType(((EmployeeLabourContract)another).getType());
            setContractFile(((EmployeeLabourContract)another).getContractFile());
            setContractFileName(((EmployeeLabourContract)another).getContractFileName());
            setContractFileType(((EmployeeLabourContract)another).getContractFileType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeLabourContractGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeLabourContract.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeLabourContract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "employeePost":
                    return obj.getEmployeePost();
                case "number":
                    return obj.getNumber();
                case "date":
                    return obj.getDate();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "employmentDate":
                    return obj.getEmploymentDate();
                case "type":
                    return obj.getType();
                case "contractFile":
                    return obj.getContractFile();
                case "contractFileName":
                    return obj.getContractFileName();
                case "contractFileType":
                    return obj.getContractFileType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "employeePost":
                    obj.setEmployeePost((EmployeePost) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "employmentDate":
                    obj.setEmploymentDate((Date) value);
                    return;
                case "type":
                    obj.setType((LabourContractType) value);
                    return;
                case "contractFile":
                    obj.setContractFile((DatabaseFile) value);
                    return;
                case "contractFileName":
                    obj.setContractFileName((String) value);
                    return;
                case "contractFileType":
                    obj.setContractFileType((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "employeePost":
                        return true;
                case "number":
                        return true;
                case "date":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "employmentDate":
                        return true;
                case "type":
                        return true;
                case "contractFile":
                        return true;
                case "contractFileName":
                        return true;
                case "contractFileType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "employeePost":
                    return true;
                case "number":
                    return true;
                case "date":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "employmentDate":
                    return true;
                case "type":
                    return true;
                case "contractFile":
                    return true;
                case "contractFileName":
                    return true;
                case "contractFileType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "employeePost":
                    return EmployeePost.class;
                case "number":
                    return String.class;
                case "date":
                    return Date.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "employmentDate":
                    return Date.class;
                case "type":
                    return LabourContractType.class;
                case "contractFile":
                    return DatabaseFile.class;
                case "contractFileName":
                    return String.class;
                case "contractFileType":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeeLabourContract> _dslPath = new Path<EmployeeLabourContract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeLabourContract");
    }
            

    /**
     * @return Сотрудник. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract#getEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> employeePost()
    {
        return _dslPath.employeePost();
    }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Дата заключения. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Дата приема на работу.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract#getEmploymentDate()
     */
    public static PropertyPath<Date> employmentDate()
    {
        return _dslPath.employmentDate();
    }

    /**
     * @return Тип трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract#getType()
     */
    public static LabourContractType.Path<LabourContractType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Файл договора.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract#getContractFile()
     */
    public static DatabaseFile.Path<DatabaseFile> contractFile()
    {
        return _dslPath.contractFile();
    }

    /**
     * @return Название файла.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract#getContractFileName()
     */
    public static PropertyPath<String> contractFileName()
    {
        return _dslPath.contractFileName();
    }

    /**
     * @return Тип файла.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract#getContractFileType()
     */
    public static PropertyPath<String> contractFileType()
    {
        return _dslPath.contractFileType();
    }

    public static class Path<E extends EmployeeLabourContract> extends EntityPath<E>
    {
        private EmployeePost.Path<EmployeePost> _employeePost;
        private PropertyPath<String> _number;
        private PropertyPath<Date> _date;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<Date> _employmentDate;
        private LabourContractType.Path<LabourContractType> _type;
        private DatabaseFile.Path<DatabaseFile> _contractFile;
        private PropertyPath<String> _contractFileName;
        private PropertyPath<String> _contractFileType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сотрудник. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract#getEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> employeePost()
        {
            if(_employeePost == null )
                _employeePost = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE_POST, this);
            return _employeePost;
        }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(EmployeeLabourContractGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Дата заключения. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(EmployeeLabourContractGen.P_DATE, this);
            return _date;
        }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(EmployeeLabourContractGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(EmployeeLabourContractGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Дата приема на работу.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract#getEmploymentDate()
     */
        public PropertyPath<Date> employmentDate()
        {
            if(_employmentDate == null )
                _employmentDate = new PropertyPath<Date>(EmployeeLabourContractGen.P_EMPLOYMENT_DATE, this);
            return _employmentDate;
        }

    /**
     * @return Тип трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract#getType()
     */
        public LabourContractType.Path<LabourContractType> type()
        {
            if(_type == null )
                _type = new LabourContractType.Path<LabourContractType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Файл договора.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract#getContractFile()
     */
        public DatabaseFile.Path<DatabaseFile> contractFile()
        {
            if(_contractFile == null )
                _contractFile = new DatabaseFile.Path<DatabaseFile>(L_CONTRACT_FILE, this);
            return _contractFile;
        }

    /**
     * @return Название файла.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract#getContractFileName()
     */
        public PropertyPath<String> contractFileName()
        {
            if(_contractFileName == null )
                _contractFileName = new PropertyPath<String>(EmployeeLabourContractGen.P_CONTRACT_FILE_NAME, this);
            return _contractFileName;
        }

    /**
     * @return Тип файла.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract#getContractFileType()
     */
        public PropertyPath<String> contractFileType()
        {
            if(_contractFileType == null )
                _contractFileType = new PropertyPath<String>(EmployeeLabourContractGen.P_CONTRACT_FILE_TYPE, this);
            return _contractFileType;
        }

        public Class getEntityClass()
        {
            return EmployeeLabourContract.class;
        }

        public String getEntityName()
        {
            return "employeeLabourContract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
